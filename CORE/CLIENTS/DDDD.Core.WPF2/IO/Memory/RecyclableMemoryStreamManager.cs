﻿using DDDD.Core.Environment;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.IO.Memory
{

    public sealed class RecyclableMemoryStreamManager
    {

        #region --------------------- CTOR -------------------------

        public RecyclableMemoryStreamManager()
          : this(131072, 1048576, 134217728)
        {
        }

        public RecyclableMemoryStreamManager(int blockSize, int largeBufferMultiple, int maximumBufferSize)
        {
            if (blockSize <= 0)
                throw new ArgumentOutOfRangeException("blockSize", (object)blockSize, "blockSize must be a positive number");
            if (largeBufferMultiple <= 0)
                throw new ArgumentOutOfRangeException("largeBufferMultiple", "largeBufferMultiple must be a positive number");
            if (maximumBufferSize < blockSize)
                throw new ArgumentOutOfRangeException("maximumBufferSize", "maximumBufferSize must be at least blockSize");
            this.blockSize = blockSize;
            this.largeBufferMultiple = largeBufferMultiple;
            this.maximumBufferSize = maximumBufferSize;
            if (!this.IsLargeBufferMultiple(maximumBufferSize))
                throw new ArgumentException("maximumBufferSize is not a multiple of largeBufferMultiple", "maximumBufferSize");
            this.smallPool = new ConcurrentStack<byte[]>();
            int length = maximumBufferSize / largeBufferMultiple;
            this.largeBufferInUseSize = new long[length + 1];
            this.largeBufferFreeSize = new long[length];
            this.largePools = new ConcurrentStack<byte[]>[length];
            for (int index = 0; index < this.largePools.Length; ++index)
                this.largePools[index] = new ConcurrentStack<byte[]>();
            Events.Write.MemoryStreamManagerInitialized(blockSize, largeBufferMultiple, maximumBufferSize);
        }




        #endregion --------------------- CTOR -------------------------


        public const int DefaultBlockSize = 131072;
        public const int DefaultLargeBufferMultiple = 1048576;
        public const int DefaultMaximumBufferSize = 134217728;
        private readonly int blockSize;
        private readonly long[] largeBufferFreeSize;
        private readonly long[] largeBufferInUseSize;
        private readonly int largeBufferMultiple;
        private readonly ConcurrentStack<byte[]>[] largePools;
        private readonly int maximumBufferSize;
        private readonly ConcurrentStack<byte[]> smallPool;
        private long smallPoolFreeSize;
        private long smallPoolInUseSize;

        public int BlockSize
        {
            get
            {
                return this.blockSize;
            }
        }

        public int LargeBufferMultiple
        {
            get
            {
                return this.largeBufferMultiple;
            }
        }

        public int MaximumBufferSize
        {
            get
            {
                return this.maximumBufferSize;
            }
        }

        public long SmallPoolFreeSize
        {
            get
            {
                return this.smallPoolFreeSize;
            }
        }

        public long SmallPoolInUseSize
        {
            get
            {
                return this.smallPoolInUseSize;
            }
        }

        public long LargePoolFreeSize
        {
            get
            {
                return ((IEnumerable<long>)this.largeBufferFreeSize).Sum();
            }
        }

        public long LargePoolInUseSize
        {
            get
            {
                return ((IEnumerable<long>)this.largeBufferInUseSize).Sum();
            }
        }

        public long SmallBlocksFree
        {
            get
            {
                return (long)this.smallPool.Count;
            }
        }

        public long LargeBuffersFree
        {
            get
            {
                long num = 0;
                foreach (ConcurrentStack<byte[]> largePool in this.largePools)
                    num += (long)largePool.Count;
                return num;
            }
        }

        public long MaximumFreeSmallPoolBytes { get; set; }

        public long MaximumFreeLargePoolBytes { get; set; }

        public long MaximumStreamCapacity { get; set; }

        public bool GenerateCallStacks { get; set; }

        public bool AggressiveBufferReturn { get; set; }

        public event EventHandler BlockCreated;

        public event EventHandler BlockDiscarded;

        public event EventHandler LargeBufferCreated;

        public event EventHandler StreamCreated;

        public event EventHandler StreamDisposed;

        public event EventHandler StreamFinalized;

        public event StreamLengthReportHandler StreamLength;

        public event EventHandler StreamConvertedToArray;

        public event LargeBufferDiscardedEventHandler LargeBufferDiscarded;

        public event UsageReportEventHandler UsageReport;


        internal byte[] GetBlock()
        {
            byte[] result;
            if (!this.smallPool.TryPop(out result))
            {
                result = new byte[this.BlockSize];
                RecyclableMemoryStreamManager.Events.Write.MemoryStreamNewBlockCreated(this.smallPoolInUseSize);
                // ISSUE: reference to a compiler-generated field
                if (this.BlockCreated != null)
                {
                    // ISSUE: reference to a compiler-generated field
                    this.BlockCreated();
                }
            }
            else
                Interlocked.Add(ref this.smallPoolFreeSize, (long)-this.BlockSize);
            Interlocked.Add(ref this.smallPoolInUseSize, (long)this.BlockSize);
            return result;
        }

        internal byte[] GetLargeBuffer(int requiredSize, string tag)
        {
            requiredSize = this.RoundToLargeBufferMultiple(requiredSize);
            int index = requiredSize / this.largeBufferMultiple - 1;
            byte[] result;
            if (index < this.largePools.Length)
            {
                if (!this.largePools[index].TryPop(out result))
                {
                    result = new byte[requiredSize];
                    RecyclableMemoryStreamManager.Events.Write.MemoryStreamNewLargeBufferCreated(requiredSize, this.LargePoolInUseSize);
                    // ISSUE: reference to a compiler-generated field
                    if (this.LargeBufferCreated != null)
                    {
                        // ISSUE: reference to a compiler-generated field
                        this.LargeBufferCreated();
                    }
                }
                else
                    Interlocked.Add(ref this.largeBufferFreeSize[index], (long)-result.Length);
            }
            else
            {
                index = this.largeBufferInUseSize.Length - 1;
                result = new byte[requiredSize];
                string allocationStack = (string)null;
                if (this.GenerateCallStacks)
                    allocationStack =  Environment2.StackTrace;//PclExport.Instance.GetStackTrace();
                RecyclableMemoryStreamManager.Events.Write.MemoryStreamNonPooledLargeBufferCreated(requiredSize, tag, allocationStack);
                // ISSUE: reference to a compiler-generated field
                if (this.LargeBufferCreated != null)
                {
                    // ISSUE: reference to a compiler-generated field
                    this.LargeBufferCreated();
                }
            }
            Interlocked.Add(ref this.largeBufferInUseSize[index], (long)result.Length);
            return result;
        }
        

        private int RoundToLargeBufferMultiple(int requiredSize)
        {
            return (requiredSize + this.LargeBufferMultiple - 1) / this.LargeBufferMultiple * this.LargeBufferMultiple;
        }

        private bool IsLargeBufferMultiple(int value)
        {
            if (value != 0)
                return value % this.LargeBufferMultiple == 0;
            return false;
        }

        internal void ReturnLargeBuffer(byte[] buffer, string tag)
        {
            if (buffer == null)
                throw new ArgumentNullException("buffer");
            if (!this.IsLargeBufferMultiple(buffer.Length))
                throw new ArgumentException("buffer did not originate from this memory manager. The size is not a multiple of " + (object)this.LargeBufferMultiple);
            int index = buffer.Length / this.largeBufferMultiple - 1;
            if (index < this.largePools.Length)
            {
                if ((long)((this.largePools[index].Count + 1) * buffer.Length) <= this.MaximumFreeLargePoolBytes || this.MaximumFreeLargePoolBytes == 0L)
                {
                    this.largePools[index].Push(buffer);
                    Interlocked.Add(ref this.largeBufferFreeSize[index], (long)buffer.Length);
                }
                else
                {
                    RecyclableMemoryStreamManager.Events.Write.MemoryStreamDiscardBuffer(RecyclableMemoryStreamManager.Events.MemoryStreamBufferType.Large, tag, RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason.EnoughFree);
                    // ISSUE: reference to a compiler-generated field
                    if (this.LargeBufferDiscarded != null)
                    {
                        // ISSUE: reference to a compiler-generated field
                        this.LargeBufferDiscarded(RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason.EnoughFree);
                    }
                }
            }
            else
            {
                index = this.largeBufferInUseSize.Length - 1;
                RecyclableMemoryStreamManager.Events.Write.MemoryStreamDiscardBuffer(RecyclableMemoryStreamManager.Events.MemoryStreamBufferType.Large, tag, RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason.TooLarge);
                // ISSUE: reference to a compiler-generated field
                if (this.LargeBufferDiscarded != null)
                {
                    // ISSUE: reference to a compiler-generated field
                    this.LargeBufferDiscarded(RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason.TooLarge);
                }
            }
            Interlocked.Add(ref this.largeBufferInUseSize[index], (long)-buffer.Length);
            // ISSUE: reference to a compiler-generated field
            if (this.UsageReport == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.UsageReport(this.smallPoolInUseSize, this.smallPoolFreeSize, this.LargePoolInUseSize, this.LargePoolFreeSize);
        }

        internal void ReturnBlocks(ICollection<byte[]> blocks, string tag)
        {
            if (blocks == null)
                throw new ArgumentNullException("blocks");
            Interlocked.Add(ref this.smallPoolInUseSize, (long)-(blocks.Count * this.BlockSize));
            foreach (byte[] block in (IEnumerable<byte[]>)blocks)
            {
                if (block == null || block.Length != this.BlockSize)
                    throw new ArgumentException("blocks contains buffers that are not BlockSize in length");
            }
            foreach (byte[] block in (IEnumerable<byte[]>)blocks)
            {
                if (this.MaximumFreeSmallPoolBytes == 0L || this.SmallPoolFreeSize < this.MaximumFreeSmallPoolBytes)
                {
                    Interlocked.Add(ref this.smallPoolFreeSize, (long)this.BlockSize);
                    this.smallPool.Push(block);
                }
                else
                {
                    RecyclableMemoryStreamManager.Events.Write.MemoryStreamDiscardBuffer(RecyclableMemoryStreamManager.Events.MemoryStreamBufferType.Small, tag, RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason.EnoughFree);
                    // ISSUE: reference to a compiler-generated field
                    if (this.BlockDiscarded != null)
                    {
                        // ISSUE: reference to a compiler-generated field
                        this.BlockDiscarded();
                        break;
                    }
                    break;
                }
            }
            // ISSUE: reference to a compiler-generated field
            if (this.UsageReport == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.UsageReport(this.smallPoolInUseSize, this.smallPoolFreeSize, this.LargePoolInUseSize, this.LargePoolFreeSize);
        }

        internal void ReportStreamCreated()
        {
            // ISSUE: reference to a compiler-generated field
            if (this.StreamCreated == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.StreamCreated();
        }

        internal void ReportStreamDisposed()
        {
            // ISSUE: reference to a compiler-generated field
            if (this.StreamDisposed == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.StreamDisposed();
        }

        internal void ReportStreamFinalized()
        {
            // ISSUE: reference to a compiler-generated field
            if (this.StreamFinalized == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.StreamFinalized();
        }

        internal void ReportStreamLength(long bytes)
        {
            // ISSUE: reference to a compiler-generated field
            if (this.StreamLength == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.StreamLength(bytes);
        }

        internal void ReportStreamToArray()
        {
            // ISSUE: reference to a compiler-generated field
            if (this.StreamConvertedToArray == null)
                return;
            // ISSUE: reference to a compiler-generated field
            this.StreamConvertedToArray();
        }



        public MemoryStream GetStream()
        {
            return (MemoryStream)new RecyclableMemoryStream(this);
        }

        public MemoryStream GetStream(string tag)
        {
            return (MemoryStream)new RecyclableMemoryStream(this, tag);
        }

        public MemoryStream GetStream(string tag, int requiredSize)
        {
            return (MemoryStream)new RecyclableMemoryStream(this, tag, requiredSize);
        }

        public MemoryStream GetStream(string tag, int requiredSize, bool asContiguousBuffer)
        {
            if (!asContiguousBuffer || requiredSize <= this.BlockSize)
                return this.GetStream(tag, requiredSize);
            return (MemoryStream)new RecyclableMemoryStream(this, tag, requiredSize, this.GetLargeBuffer(requiredSize, tag));
        }

        public MemoryStream GetStream(string tag, byte[] buffer, int offset, int count)
        {
            RecyclableMemoryStream recyclableMemoryStream = new RecyclableMemoryStream(this, tag, count);
            byte[] buffer1 = buffer;
            int offset1 = offset;
            int count1 = count;
            recyclableMemoryStream.Write(buffer1, offset1, count1);
            long num = 0;
            recyclableMemoryStream.Position = num;
            return (MemoryStream)recyclableMemoryStream;
        }





        public delegate void EventHandler();

        public delegate void LargeBufferDiscardedEventHandler(RecyclableMemoryStreamManager.Events.MemoryStreamDiscardReason reason);

        public delegate void StreamLengthReportHandler(long bytes);

        public delegate void UsageReportEventHandler(long smallPoolInUseBytes, long smallPoolFreeBytes, long largePoolInUseBytes, long largePoolFreeBytes);

        public sealed class Events
        {
            public static Events Write = new Events();

            public enum MemoryStreamBufferType
            {
                Small,
                Large,
            }

            public enum MemoryStreamDiscardReason
            {
                TooLarge,
                EnoughFree,
            }



            public void MemoryStreamCreated(Guid guid, string tag, int requestedSize)
            {
            }

            public void MemoryStreamDisposed(Guid guid, string tag)
            {
            }

            public void MemoryStreamDoubleDispose(Guid guid, string tag, string allocationStack, string disposeStack1, string disposeStack2)
            {
            }

            public void MemoryStreamFinalized(Guid guid, string tag, string allocationStack)
            {
            }

            public void MemoryStreamToArray(Guid guid, string tag, string stack, int size)
            {
            }

            public void MemoryStreamManagerInitialized(int blockSize, int largeBufferMultiple, int maximumBufferSize)
            {
            }

            public void MemoryStreamNewBlockCreated(long smallPoolInUseBytes)
            {
            }

            public void MemoryStreamNewLargeBufferCreated(int requiredSize, long largePoolInUseBytes)
            {
            }

            public void MemoryStreamNonPooledLargeBufferCreated(int requiredSize, string tag, string allocationStack)
            {
            }

            public void MemoryStreamDiscardBuffer(MemoryStreamBufferType bufferType, string tag, MemoryStreamDiscardReason reason)
            {
            }

            public void MemoryStreamOverCapacity(int requestedCapacity, long maxCapacity, string tag, string allocationStack)
            {
            }

           
        }
    }

}
