﻿using System;
using System.Globalization;
using System.IO;

namespace DDDD.Core.IO.Text 
{


    internal static class StringWriterThreadStatic
    {
        [ThreadStatic]
        static StringWriter cache;

        public static StringWriter Allocate()
        {
            var ret = cache;
            if (ret == null)
                return new StringWriter(CultureInfo.InvariantCulture);

            var sb = ret.GetStringBuilder();
            sb.Length = 0;
            cache = null;  //don't re-issue cached instance until it's freed
            return ret;
        }

        public static void Free(StringWriter writer)
        {
            cache = writer;
        }

        public static string ReturnAndFree(StringWriter writer)
        {
            var ret = writer.ToString();
            cache = writer;
            return ret;
        }
    }
}
