﻿using System.Text;

namespace DDDD.Core.IO.Text 
{

    public sealed class StringHelper
    {
        public StringHelper()
        {
            this.Encoding = new UTF8Encoding(false, true);
        }

        public const int BYTEBUFFERLEN = 256;
        public const int CHARBUFFERLEN = 128;

        Encoder m_encoder;
        Decoder m_decoder;

        byte[] m_byteBuffer;
        char[] m_charBuffer;

        public UTF8Encoding Encoding 
        {
            get; private set; 
        }

        public Encoder Encoder 
        { 
            get { if (m_encoder == null) m_encoder = this.Encoding.GetEncoder(); return m_encoder;  } 
        }
        
        public Decoder Decoder 
        {
            get { if (m_decoder == null) m_decoder = this.Encoding.GetDecoder(); return m_decoder; }
        }

        public byte[] ByteBuffer
        { 
            get { if (m_byteBuffer == null) m_byteBuffer = new byte[BYTEBUFFERLEN]; return m_byteBuffer; } 
        }
        
        public char[] CharBuffer 
        { 
            get { if (m_charBuffer == null) m_charBuffer = new char[CHARBUFFERLEN]; return m_charBuffer; } 
        }

    }
}
