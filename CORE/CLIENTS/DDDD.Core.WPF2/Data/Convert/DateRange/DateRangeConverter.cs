﻿
using System;
using System.ComponentModel;
using System.Globalization;
using System.Security;

using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;

namespace DDDD.Core.Data
{
	/// <summary>
	/// Type converter for the <see cref="T:DateRange" /> structure
	/// </summary>
	public class DateRangeConverter : TypeConverter
	{
		/// <summary>
		/// Initializes a new <see cref="T:DateRangeConverter" />
		/// </summary>
		public DateRangeConverter()
		{
		}

		/// <summary>
		/// Returns true if the object can convert from the type.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="sourceType"> A <see cref="T:System.Type" /> that represents the type you want to convert from.</param>
		/// <returns>True if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == typeof(string))
			{
				return true;
			}
			return base.CanConvertFrom(context, sourceType);
		}

		/// <summary>
		/// Returns true if the object can convert to that type.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="destinationType"> A <see cref="T:System.Type" /> that represents the type you want to convert to.</param>
		/// <returns>True if this converter can perform the conversion; otherwise, false.</returns>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			if (destinationType == typeof(string))
			{
				return true;
			}
			return false;
		}

		/// <summary>
		/// Converts from one type to another.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="culture">A <see cref="T:System.Globalization.CultureInfo" /> object. If null is passed, the current culture is assumed.</param>        
		/// <param name="value">The object to convert.</param>
		/// <returns>An object that represents the converted value.</returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			Validator.ATNullReferenceArgDbg(value, nameof(DateRangeConverter), nameof(ConvertTo), nameof(value));
			//CoreUtilities.ValidateNotNull(value, "value");
			string str = value as string;
			if (str == null)
			{
				this.GetConvertFromException(value);
			}
			return DateRange.FromString(str, culture);
		}

		/// <summary>
		/// Converts the object to the requested type.
		/// </summary>
		/// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
		/// <param name="culture">A <see cref="T:System.Globalization.CultureInfo" /> object. If null is passed, the current culture is assumed.</param>
		/// <param name="destinationType">A <see cref="T:System.Type" /> that represents the type you want to convert to.</param>
		/// <param name="value">The object to convert.</param>
		/// <returns>An object that represents the converted value.</returns>
		[SecuritySafeCritical]
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			Validator.ATNullReferenceArgDbg(value, nameof(DateRangeConverter), nameof(ConvertTo), nameof(value));
			Validator.ATNullReferenceArgDbg(destinationType, nameof(DateRangeConverter), nameof(ConvertTo), nameof(destinationType));


			//CoreUtilities.ValidateNotNull(value, "value");
			//CoreUtilities.ValidateNotNull(destinationType, "destinationType");


			if (!(value is DateRange))
			{
				this.GetConvertToException(value, destinationType);
			}
			DateRange dateRange = (DateRange)value;
			if (destinationType != typeof(string))
			{
				object[] fullName = new object[] { typeof(DateRange), destinationType.GetWorkingTypeFromNullableType().FullName }; //   CoreUtilities.GetUnderlyingType(destinationType).FullName };
				throw new ArgumentException(DateRange.GetString("LE_CannotConvertType", fullName));
			}
			return dateRange.ToString(culture, null, true);
		}

		private void GetConvertFromException(object value)
		{
			throw new ArgumentException();
		}

		private void GetConvertToException(object value, Type destinationType)
		{
			throw new ArgumentException();
		}
	}
}
