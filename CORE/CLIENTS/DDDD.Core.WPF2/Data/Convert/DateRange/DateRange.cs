﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;

using DDDD.Core.Converters;

namespace DDDD.Core.Data 
{
	/// <summary>
	/// Structure used to represents a range of dates.
	/// </summary>
	[TypeConverter(typeof(DateRangeConverter))]
	public struct DateRange : IEquatable<DateRange>, IComparable<DateRange>
	{
		private DateTime _start;

		private DateTime _end;

		private readonly static DateRange _Infinite;

		/// <summary>
		/// Returns or sets the latest/end date for the range.
		/// </summary>
		[TypeConverter(typeof(DateTimeTypeConverter))]
		public DateTime End
		{
			get
			{
				return this._end;
			}
			set
			{
				this._end = value;
			}
		}

		/// <summary>
		/// Returns a data range from DateTime.MinValue to DateTime.MaxValue
		/// </summary>
		public static DateRange Infinite
		{
			get
			{
				return DateRange._Infinite;
			}
		}

		/// <summary>
		/// Returns true if the Start and End are the same values.
		/// </summary>
		public bool IsEmpty
		{
			get
			{
				return this._start == this._end;
			}
		}

		/// <summary>
		/// Returns or sets the earliest/start date for the range.
		/// </summary>
		[TypeConverter(typeof(DateTimeTypeConverter))]
		public DateTime Start
		{
			get
			{
				return this._start;
			}
			set
			{
				this._start = value;
			}
		}

		static DateRange()
		{
			DateRange._Infinite = new DateRange(DateTime.MinValue, DateTime.MaxValue);
		}

		/// <summary>
		/// Initializes a new <see cref="T:DateRange" />
		/// </summary>
		/// <param name="date">The date to use for the start and end dates.</param>
		public DateRange(DateTime date) : this(date, date)
		{
		}

		/// <summary>
		/// Initializes a new <see cref="T:DateRange" />
		/// </summary>
		/// <param name="start">The start date</param>
		/// <param name="end">The end date</param>
		public DateRange(DateTime start, DateTime end)
		{
			this._start = start;
			this._end = end;
		}

		internal static DateRange[] CombineRanges(params ICollection<DateRange>[] ranges)
		{
			int count = 0;
			ICollection<DateRange>[] collectionArrays = ranges;
			for (int i = 0; i < (int)collectionArrays.Length; i++)
			{
				count = count + collectionArrays[i].Count;
			}
			DateRange[] end = new DateRange[count];
			int num = 0;
			ICollection<DateRange>[] collectionArrays1 = ranges;
			for (int j = 0; j < (int)collectionArrays1.Length; j++)
			{
				ICollection<DateRange> dateRanges = collectionArrays1[j];
				dateRanges.CopyTo(end, num);
				num = num + dateRanges.Count;
			}
			if ((int)end.Length < 2)
			{
				return end;
			}
			for (int k = 0; k < (int)end.Length; k++)
			{
				end[k].Normalize();
			}
			Array.Sort<DateRange>(end);
			int num1 = -1;
			for (int l = 0; l < (int)end.Length; l++)
			{
				DateRange dateRange = end[l];
				if (num1 < 0 || !(dateRange.Start <= end[num1].End))
				{
					num1++;
					end[num1] = dateRange;
				}
				else if (dateRange.End > end[num1].End)
				{
					end[num1].End = dateRange.End;
				}
			}
			Array.Resize<DateRange>(ref end, num1 + 1);
			return end;
		}

		/// <summary>
		/// Compares this instance to the specified <see cref="T:DateRange" />
		/// </summary>
		/// <param name="other">The range to compare</param>
		/// <returns>A signed number indicating the relative values of this and the specified range.</returns>
		public int CompareTo(DateRange other)
		{
			int num = this._start.CompareTo(other._start);
			if (num == 0)
			{
				num = this._end.CompareTo(other._end);
			}
			return num;
		}

		/// <summary>
		/// Indicates if the specified date falls within the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> dates for this range.
		/// </summary>
		/// <param name="date">The date to evaluate</param>
		/// <returns>True if the specified date is greater than or equal to the <see cref="P:DateRange.Start" /> and on or before the <see cref="P:DateRange.End" />.</returns>
		public bool Contains(DateTime date)
		{
			DateRange dateRange = DateRange.Normalize(this);
			if (date < dateRange._start)
			{
				return false;
			}
			return date <= dateRange._end;
		}

		/// <summary>
		/// Indicates if the dates of the specified range fall completely within the dates of this range instance.
		/// </summary>
		/// <param name="range">The range to evaluate</param>
		/// <returns>True if the start and end date fall entirely within or equal to the start and end of this range instance.</returns>
		public bool Contains(DateRange range)
		{
			range.Normalize();
			DateRange dateRange = DateRange.Normalize(this);
			if (range._start < dateRange._start)
			{
				return false;
			}
			return range._end <= dateRange._end;
		}

		/// <summary>
		/// Indicates if the specified date falls within the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> dates for this range.
		/// </summary>
		/// <param name="date">The date to evaluate</param>
		/// <returns>True if the specified date is greater than or equal to the <see cref="P:DateRange.Start" /> and before the <see cref="P:DateRange.End" />.</returns>
		internal bool ContainsExclusive(DateTime date)
		{
			if (date < this._start)
			{
				return false;
			}
			return date < this._end;
		}

		/// <summary>
		/// Compares the specified object to this object to see if they are equivalent.
		/// </summary>
		/// <param name="obj">The object to compare</param>
		/// <returns>True if the objects are equal; otherwise false</returns>
		public override bool Equals(object obj)
		{
			if (!(obj is DateRange))
			{
				return false;
			}
			return this == (DateRange)obj;
		}

		/// <summary>
		/// Compares two <see cref="T:DateRange" />
		/// </summary>
		/// <param name="other">The object to compare to this instance</param>
		/// <returns></returns>
		public bool Equals(DateRange other)
		{
			return this == other;
		}

		internal static DateRange FromString(string value, CultureInfo culture)
		{
			DateTime dateTime;
			culture = culture ?? CultureInfo.CurrentCulture;
			string[] strArrays = value.Split(new char[] { '-' });
			if ((int)strArrays.Length >= 1 && (int)strArrays.Length <= 2)
			{
				bool flag = false;
				string[] strArrays1 = strArrays;
				int num = 0;
				while (num < (int)strArrays1.Length)
				{
					string str = strArrays1[num];
					if (string.IsNullOrEmpty(str) || str.Trim().Length == 0)
					{
						flag = true;
						break;
					}
					else
					{
						num++;
					}
				}
				if (!flag)
				{
					bool length = (int)strArrays.Length == 2;
					DateTime minValue = DateTime.MinValue;
					if (DateTime.TryParseExact(strArrays[0], "r", culture, DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AllowInnerWhite | DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal, out dateTime))
					{
						if (length)
						{
							minValue = DateTime.ParseExact(strArrays[1], "r", culture, DateTimeStyles.AllowLeadingWhite | DateTimeStyles.AllowTrailingWhite | DateTimeStyles.AllowInnerWhite | DateTimeStyles.AllowWhiteSpaces | DateTimeStyles.AssumeUniversal);
						}
					}
					else if (!DateTime.TryParseExact(strArrays[0], "d", culture, DateTimeStyles.AllowWhiteSpaces, out dateTime))
					{
						dateTime = DateTime.Parse(strArrays[0], culture);
						if (length)
						{
							minValue = DateTime.Parse(strArrays[1], culture);
						}
					}
					else if (length)
					{
						minValue = DateTime.ParseExact(strArrays[1], "d", culture, DateTimeStyles.AllowWhiteSpaces);
					}
					if (!length)
					{
						minValue = dateTime;
					}
					return new DateRange(dateTime, minValue);
				}
			}
			object[] objArray = new object[] { value };
			throw new FormatException(DateRange.GetString("LE_InvalidDateRangeString", objArray));
		}

		/// <summary>
		/// Returns the hash code of the structure.
		/// </summary>
		/// <returns>A hash code for this instance</returns>
		public override int GetHashCode()
		{
			return this._start.GetHashCode() ^ this._end.GetHashCode();
		}

		internal static string GetString(string name, params object[] args)
		{
			return    name; //    SR.GetString(name,  args) -  [12/14/2016 A1]
		}

		/// <summary>
		/// Attempts to update the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> if the specified range intersects with this instance
		/// </summary>
		/// <param name="range">The range to intersect</param>
		/// <returns>True if the ranges intersect; false if the ranges do not intersect.</returns>
		/// <remarks>
		/// <p class="body">If the specified range does not intersect then this instance's Start and End will not be changed. If they 
		/// do intersect then the Start and End will be updated to reflect the intersection of the normalized two ranges.</p>
		/// </remarks>
		public bool Intersect(DateRange range)
		{
			range.Normalize();
			DateRange dateRange = DateRange.Normalize(this);
			if (dateRange._end < range._start || dateRange._start > range._end)
			{
				return false;
			}
			if (range._start <= dateRange._start)
			{
				this._start = dateRange._start;
			}
			else
			{
				this._start = range._start;
			}
			if (range._end >= dateRange._end)
			{
				this._end = dateRange._end;
			}
			else
			{
				this._end = range._end;
			}
			return true;
		}

		internal bool Intersect(DateRange range, bool normalizeIntersection)
		{
			bool flag = (normalizeIntersection ? false : this._start > this._end);
			if (!this.Intersect(range))
			{
				return false;
			}
			if (flag)
			{
				Swap<DateTime>(ref this._start, ref this._end);
			}
			return true;
		}

		/// <summary>
		/// Swaps the values of the specified 
		/// </summary>
		/// <typeparam name="T">The type of variable to be swapped</typeparam>
		/// <param name="value1">The member to be updated with the value of <paramref name="value2" /></param>
		/// <param name="value2">The member to be updated with the value of <paramref name="value1" /></param>
		internal static void Swap<T>(ref T value1, ref T value2)
		{
			T t = value2;
			value2 = value1;
			value1 = t;
		}


		/// <summary>
		/// Indicates if the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> of the specified <see cref="T:DateRange" /> intersects with this object's dates.
		/// </summary>
		/// <param name="range">The range to evaluate.</param>
		/// <returns>True if any date within the specified range overlaps with the start/end of this range.</returns>
		/// <exception cref="T:System.ArgumentNullException">The 'range' cannot be null.</exception>
		public bool IntersectsWith(DateRange range)
		{
			range.Normalize();
			DateRange dateRange = DateRange.Normalize(this);
			if (dateRange._end < range._start)
			{
				return false;
			}
			return !(dateRange._start > range._end);
		}

		/// <summary>
		/// Indicates if the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> of the specified <see cref="T:DateRange" /> intersects with this object's dates.
		/// </summary>
		/// <param name="range">The range to evaluate.</param>
		/// <returns>True if any date within the specified range overlaps with the start/end of this range considering the End of the ranges as exclusive.</returns>
		/// <exception cref="T:System.ArgumentNullException">The 'range' cannot be null.</exception>
		internal bool IntersectsWithExclusive(DateRange range)
		{
			range.Normalize();
			DateRange dateRange = DateRange.Normalize(this);
			if (dateRange._end <= range._start)
			{
				return false;
			}
			return !(dateRange._start >= range._end);
		}

		/// <summary>
		/// Ensures that the start date is less than or equal to the end date.
		/// </summary>
		[EditorBrowsable(EditorBrowsableState.Advanced)]
		public void Normalize()
		{
			if (this._end < this._start)
			{
				DateTime dateTime = this._start;
				this._start = this._end;
				this._end = dateTime;
			}
		}

		/// <summary>
		/// Returns a DateRange where the <see cref="P:DateRange.Start" /> is less than or equal to the end time
		/// </summary>
		/// <param name="range">The range to normalize</param>
		public static DateRange Normalize(DateRange range)
		{
			range.Normalize();
			return range;
		}

		/// <summary>
		/// Moves the date range by the specified offset.
		/// </summary>
		/// <param name="offset">The timespan used to adjust the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" /> dates</param>
		public void Offset(TimeSpan offset)
		{
			this._start = this._start.Add(offset);
			this._end = this._end.Add(offset);
		}

		/// <summary>
		/// Compares the values of two <see cref="T:DateRange" /> structures for equality
		/// </summary>
		/// <param name="range1">The first structure</param>
		/// <param name="range2">The other structure</param>
		/// <returns>true if the two instances are equal; otherwise false</returns>
		public static bool operator ==(DateRange range1, DateRange range2)
		{
			if (range1._start != range2._start)
			{
				return false;
			}
			return range1._end == range2._end;
		}

		/// <summary>
		/// Compares the values of two <see cref="T:DateRange" /> structures for inequality
		/// </summary>
		/// <param name="range1">The first structure</param>
		/// <param name="range2">The other structure</param>
		/// <returns>true if the two instances are not equal; otherwise false</returns>
		public static bool operator !=(DateRange range1, DateRange range2)
		{
			return !(range1 == range2);
		}

		/// <summary>
		/// Removes the time portion from the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" />
		/// </summary>
		public void RemoveTime()
		{
			this._start = this._start.Date;
			this._end = this._end.Date;
		}

		/// <summary>
		/// Returns a string representation of the object.
		/// </summary>
		/// <returns>A string that represents this <see cref="T:DateRange" /></returns>
		public override string ToString()
		{
			return this.ToString(null);
		}

		/// <summary>
		/// Returns a string representation of the range using the specified format to format the <see cref="P:DateRange.Start" /> and <see cref="P:DateRange.End" />
		/// </summary>
		/// <param name="format">The format used to format the Start and End dates.</param>
		/// <returns>A string that represents this <see cref="T:DateRange" /></returns>
		public string ToString(string format)
		{
			return this.ToString(CultureInfo.CurrentCulture, format, false);
		}

		internal string ToString(CultureInfo culture, string format, bool allowConvertToUniversal)
		{
			culture = culture ?? CultureInfo.CurrentCulture;
			bool flag = (this._start.TimeOfDay != TimeSpan.Zero ? true : this._end.TimeOfDay != TimeSpan.Zero);
			if (format == null)
			{
				format = (flag ? "r" : "d");
			}
			DateTime universalTime = this._start;
			DateTime dateTime = this._end;
			if (flag && allowConvertToUniversal)
			{
				universalTime = universalTime.ToUniversalTime();
				dateTime = dateTime.ToUniversalTime();
			}
			if (universalTime == dateTime)
			{
				return universalTime.ToString(format, culture);
			}
			return string.Concat(universalTime.ToString(format, culture), "-", dateTime.ToString(format, culture));
		}
	}
}
