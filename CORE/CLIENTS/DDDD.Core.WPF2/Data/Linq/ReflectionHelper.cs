﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;


using LinqBinary = System.Data.Linq.Binary;

namespace DDDD.Core.Data.Linq
{
	public class ReflectionHelper
	{
		public class Expressor<T>
		{
			public static FieldInfo FieldExpressor(Expression<Func<T, object>> func)
			{
				return (FieldInfo)((MemberExpression)((UnaryExpression)func.Body).Operand).Member;
			}

			public static MethodInfo PropertyExpressor(Expression<Func<T, object>> func)
			{
				return ((PropertyInfo)((MemberExpression)func.Body).Member).GetGetMethod();
			}

			public static MethodInfo MethodExpressor(Expression<Func<T, object>> func)
			{
				var ex = func.Body;

				if (ex is UnaryExpression)
					ex = ((UnaryExpression)ex).Operand;

				//if (ex is MemberExpression)
				//	return ((PropertyInfo)((MemberExpression)ex).Member).GetGetMethod();

				return ((MethodCallExpression)ex).Method;
			}
		}

		public static MemberInfo MemeberInfo(LambdaExpression func)
		{
			var ex = func.Body;

			if (ex is UnaryExpression)
				ex = ((UnaryExpression)ex).Operand;

			return
				ex is MemberExpression ? ((MemberExpression)ex).Member :
				ex is MethodCallExpression ? ((MethodCallExpression)ex).Method :
								 (MemberInfo)((NewExpression)ex).Constructor;
		}

		public class Binary : Expressor<BinaryExpression>
		{
			public static MethodInfo Conversion = PropertyExpressor(e => e.Conversion);
			public static MethodInfo Left = PropertyExpressor(e => e.Left);
			public static MethodInfo Right = PropertyExpressor(e => e.Right);
		}

		public class Unary : Expressor<UnaryExpression>
		{
			public static MethodInfo Operand = PropertyExpressor(e => e.Operand);
		}

		public class LambdaExpr : Expressor<LambdaExpression>
		{
			public static MethodInfo Body = PropertyExpressor(e => e.Body);
			public static MethodInfo Parameters = PropertyExpressor(e => e.Parameters);
		}

		public class Constant : Expressor<ConstantExpression>
		{
			public static MethodInfo Value = PropertyExpressor(e => e.Value);
		}

		public class QueryableInt : Expressor<IQueryable>
		{
			public static MethodInfo Expression = PropertyExpressor(e => e.Expression);
		}

		public class MethodCall : Expressor<MethodCallExpression>
		{
			public static MethodInfo Object = PropertyExpressor(e => e.Object);
			public static MethodInfo Arguments = PropertyExpressor(e => e.Arguments);
		}

		public class Conditional : Expressor<ConditionalExpression>
		{
			public static MethodInfo Test = PropertyExpressor(e => e.Test);
			public static MethodInfo IfTrue = PropertyExpressor(e => e.IfTrue);
			public static MethodInfo IfFalse = PropertyExpressor(e => e.IfFalse);
		}

		public class Invocation : Expressor<InvocationExpression>
		{
			public static MethodInfo Expression = PropertyExpressor(e => e.Expression);
			public static MethodInfo Arguments = PropertyExpressor(e => e.Arguments);
		}

		public class ListInit : Expressor<ListInitExpression>
		{
			public static MethodInfo NewExpression = PropertyExpressor(e => e.NewExpression);
			public static MethodInfo Initializers = PropertyExpressor(e => e.Initializers);
		}

		public class ElementInit : Expressor<System.Linq.Expressions.ElementInit>
		{
			public static MethodInfo Arguments = PropertyExpressor(e => e.Arguments);
		}

		public class Member : Expressor<MemberExpression>
		{
			public static MethodInfo Expression = PropertyExpressor(e => e.Expression);
		}

		public class MemberInit : Expressor<MemberInitExpression>
		{
			public static MethodInfo NewExpression = PropertyExpressor(e => e.NewExpression);
			public static MethodInfo Bindings = PropertyExpressor(e => e.Bindings);
		}

		public class New : Expressor<NewExpression>
		{
			public static MethodInfo Arguments = PropertyExpressor(e => e.Arguments);
		}

		public class NewArray : Expressor<NewArrayExpression>
		{
			public static MethodInfo Expressions = PropertyExpressor(e => e.Expressions);
		}

		public class TypeBinary : Expressor<TypeBinaryExpression>
		{
			public static MethodInfo Expression = PropertyExpressor(e => e.Expression);
		}

		public class IndexExpressor<T>
		{
			public static MethodInfo IndexerExpressor(Expression<Func<ReadOnlyCollection<T>, object>> func)
			{
				return ((MethodCallExpression)((UnaryExpression)func.Body).Operand).Method;
			}

			public static MethodInfo Item = IndexerExpressor(c => c[0]);
		}

		public class MemberAssignmentBind : Expressor<MemberAssignment>
		{
			public static MethodInfo Expression = PropertyExpressor(e => e.Expression);
		}

		public class MemberListBind : Expressor<MemberListBinding>
		{
			public static MethodInfo Initializers = PropertyExpressor(e => e.Initializers);
		}

		public class MemberMemberBind : Expressor<MemberMemberBinding>
		{
			public static MethodInfo Bindings = PropertyExpressor(e => e.Bindings);
		}

#if FW4 || SILVERLIGHT

		public class Block : Expressor<BlockExpression>
		{
			public static MethodInfo Expressions = PropertyExpressor(e => e.Expressions);
			public static MethodInfo Variables   = PropertyExpressor(e => e.Variables);
		}

#endif

		public static MethodInfo ExprItem = IndexExpressor<Expression>.Item;
		public static MethodInfo ParamItem = IndexExpressor<ParameterExpression>.Item;
		public static MethodInfo ElemItem = IndexExpressor<ElementInit>.Item;

		public class DataReader : Expressor<IDataReader>
		{
			public static MethodInfo GetValue = MethodExpressor(rd => rd.GetValue(0));
			public static MethodInfo IsDBNull = MethodExpressor(rd => rd.IsDBNull(0));
		}
 

		///		MapSchema 
		///		QueryContext
		public class Functions
		{
			public class String : Expressor<string>
			{
				//public static MethodInfo Contains   = MethodExpressor(s => s.Contains(""));
				//public static MethodInfo StartsWith = MethodExpressor(s => s.StartsWith(""));
				//public static MethodInfo EndsWith   = MethodExpressor(s => s.EndsWith(""));

#if !SILVERLIGHT
				public static MethodInfo Like11 = MethodExpressor(s => System.Data.Linq.SqlClient.SqlMethods.Like("", ""));
				public static MethodInfo Like12 = MethodExpressor(s => System.Data.Linq.SqlClient.SqlMethods.Like("", "", ' '));
#endif

				public static MethodInfo Like21 = MethodExpressor(s => Sql.Like(s, ""));
				public static MethodInfo Like22 = MethodExpressor(s => Sql.Like(s, "", ' '));
			}
		}
	}
}
