﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

using DDDD.Core.Collections.Concurrent;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Data.DA2.Query;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using System.Collections.ObjectModel;

namespace DDDD.Core.Data.DA2
{

    /// <summary>
    /// DB Proxy - represents  CRUD commands interface 
    ///          - to some Database of some DBProvider with the rights of the user connection.           
    /// </summary>
    public class DBProxy
    {

        // DBProxy       
        //{
        // DbConnection
        // DbTrasaction IDbTransaction
        //      DbCommand
        //      SqlDataReader , DbDataReader, IDataReader
        // }


        const string ClsName = nameof(DBProxy);

        /// <summary>
        /// Query Delimeter - New query from New Line
        /// </summary>
        const string QDm = "\r\n";

        #region --------------- CACHE - Connected and Closed DBProxies ----------------


        private static readonly object syncRoot = new object();

        // propxy for some DB in disconnected state of course

        static readonly SafeDictionary2<string, DBProxy> dbProxies
         = new SafeDictionary2<string, DBProxy>();


        /// <summary>
        /// When initing DBProxy wil be marked as DefaultProxy - it will 
        /// be setted to this static property.
        /// <para/> So we can init DBProxy once,use Connection String
        /// ... and on next time we use it shortly by this property, without initing and any args to do it. 
        /// </summary>
        public static DBProxy DefProx
        { get; private set; }



        /// <summary>
        /// Get existed DBPropxy object  from Cache by its Conection string..
        /// If not exist item with such string return null.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static DBProxy Get(string connectionString)
        {
            if (dbProxies.ContainsKey(connectionString))
            {
                return dbProxies[connectionString];
            }

            return null; // NOT FOUND BY SUCH STRING
        }


        /// <summary>
        /// Create, cache and then Get DBProxy object
        /// </summary>
        /// <param name="providerType"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static DBProxy GetInit<T>(string connectionString, bool IsDefaultProxy = false)
            where T : SqlDataProvider
        {
            var providerType = typeof(T);
            var key = connectionString;
            DBProxy dbprox = null;
            if (dbProxies.TryGetValue(key, out dbprox) == false)
            {
                lock (syncRoot)
                {
                    if (dbProxies.ContainsKey(key) == false)
                    {
                        //create and add to cache
                        dbprox = new DBProxy();
                        // ConnectionString
                        dbprox.ConnectionString = connectionString;
                        //get and set Provider                          
                        dbprox.Provider = SqlDataProvider.Get(providerType);
                        //Load DBProperties in new dbprox
                        dbprox.DProperties = dbprox.Provider.GetPropertiesFromDb(dbprox);

                        dbProxies.Add(key, dbprox);

                        if (IsDefaultProxy)
                        {
                            DefProx = dbprox;
                        }
                    }
                }
                return dbProxies[key];
            }
            else
            { return dbprox; }

        }




        #endregion ------------ CACHE - Connected and Closed DBProxies ----------------


        #region ------------ DBProperties -------------------

        /// <summary>
        /// Database Server Properties, suppoting info
        /// </summary>
        public DBProperties DProperties
        { get; internal set; }

        #endregion ------------ DBProperties -------------------


        #region ----------- Data Provider -----------------
        internal SqlDataProvider Provider
        { get; private set; }

        #endregion ----------- Data Provider -----------------


        #region ----------- Connection-----------------
        // Connection

        /// <summary>
        /// Db Connection String
        /// </summary>
        public string ConnectionString
        { get; protected set; }

        [ThreadStatic]
        protected static IDbConnection Connection = null;



        #endregion ----------- Connection-----------------


        #region ------------- TransactionScope ----------------

        //        [ThreadStatic]
        //protected static TransactionScope TransactScope = null;

        #endregion ------------- TransactionScope ----------------


        #region ------------ Transaction ----------------

        // Transaction
        // Actions

        //[ThreadStatic]
        //protected static DBProxy ProxyOnTransact = null;


        [ThreadStatic]
        protected static IDbTransaction Transaction = null;


        /// <summary>
        /// Run Trsnsaction. 
        /// <para/>Inside transAct use methods with Name[Trc] suffix(like SelectItemTrc ...)
        /// </summary>
        /// <param name="isolate"></param>
        /// <param name="transAct"></param>
        /// <param name="rollbackAct"></param>
        public void RunTransaction(IsolationLevel isolate, Action transAct, Action rollbackAct)
        {
            using (Connection = Provider
                    .CreateConnection(ConnectionString))
            {
                Connection.Open();
                using (Transaction = Connection.BeginTransaction(isolate))
                {
                    try
                    {
                        // do all Transaction Operations with xTrc methods
                        transAct();

                        Transaction.Commit();
                    }
                    catch (Exception exc)
                    {
                        Transaction.Rollback();
                        if (Connection.State != ConnectionState.Closed)
                        { Connection.Close(); }

                        if (rollbackAct != null) rollbackAct();

                        throw exc;
                    }

                }//dispose transaction
                Transaction = null;// clear transaction

                if (Connection.State != ConnectionState.Closed)
                { Connection.Close(); }

            }//dispose connection
            Connection = null; //// clear connection

        }

        void TransactionTest1()
        {
            RunTransaction(IsolationLevel.ReadCommitted,
                () =>
                { // HERE ALL TRANSACT ACTIONS
                    SelectItemTrc<string>("Select ***");

                    InsertItemTrc((new object() as IBObject), Guid.Empty);

                    //UpdateItemTrc<string>("Select ***");
                }
                , null    // rollback                        
                );
        }

        #endregion ------------ Transaction ----------------

        #region --------------SELECT NO TRANSACTION-----------------

        #region ------------ SELECT MANY LISTS -------------


        /// <summary>
        /// Select several Collections 
        /// per one batch query, and one connection Opening/Closing.
        /// </summary>
        /// <param name="selects"></param>
        /// <returns></returns>
        public KeyValuePair<Type, List<object>>[] SelectManyLists(params KeyValuePair<Type, DSelectQuery>[] selects)
        {
            var resultSets = new KeyValuePair<Type, List<object>>[selects.Length];

            // collect common selects text
            var comonSqlCmdsText = "";
            for (int i = 0; i < selects.Length; i++)
            {
                var selectCmdText = selects[i].Value.ToSQLString(Provider.Key);
                comonSqlCmdsText += "\r\n" + selectCmdText;
            }

            using (var cnctn = Provider
                   .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = comonSqlCmdsText;

                //per each query
                for (int i = 0; i < selects.Length; i++)
                {
                    IDataReader reader = null;
                    if (i == 0)
                    {
                        reader = command.ExecuteReader();
                    }
                    else
                    {
                        reader.NextResult();
                    }

                    var tpmp = TypeDAMap.GetInit(selects[i].Key, Provider.Key);
                    var result = tpmp.MapListFromReader(reader, Provider.Key);
                    resultSets[i] = new KeyValuePair<Type, List<object>>(selects[i].Key, result);
                }

                command.Dispose();
                cnctn.Close();
            }

            return resultSets;
        }


        #endregion------------ SELECT MANY LISTS -------------

        #region ----------- SELECT ITEM/LIST  NO TRANSACTION ---------------

        /// <summary>
        /// Select Item of {T}  by [selectcommand] query text
        /// , and Map result to {T} by your custom mapping Func [readItemFunc].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlStatement"></param>
        /// <param name="readItemFunc"></param>
        /// <returns></returns>
        public T SelectItem<T>(string selectcommand, Func<IDataReader, T> readItemFunc)
        {

            using (var cnctn = Provider
                   .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = selectcommand;
                var reader = command.ExecuteReader();

                var result = readItemFunc(reader);
                command.Dispose();
                cnctn.Close();

                return result;
            }
        }



        /// <summary>
        /// Select Item of {object}  by [selectquery] query text in [targetType] DB Table.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selectcommand"></param>
        /// <returns></returns>
        public object SelectItem(Type targetType, string selectcommand)
        {
            object result = null;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = selectcommand;
                var reader = command.ExecuteReader();
                 
                //read record
                reader.Read();

                var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
                result = tpmp.MapItemFromReader(reader,  Provider.Key);

                //reader.Close();
                command.Dispose();
                cnctn.Close();
            } // here connection disposing

            return result;
        }

        /// <summary>
        ///  Select Item of {T}  by [selectquery] query text.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectCommand"></param>
        /// <returns></returns>
        public T SelectItem<T>(string selectCommand)
        {
            return (T)SelectItem(typeof(T), selectCommand);
        }

        /// <summary>
        ///  Select List of {T}  by [selectquery] query object .
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectquery"></param>
        /// <returns></returns>
        public T SelectItem<T>(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return (T)SelectItem<T>(selectCmdText);
        }

        /// <summary>
        /// Select List of {object}  by [selectcommand] in [targetType] Table, with timeout [timeoutSec] using.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selectcommand"></param>
        /// <param name="timeoutSec"></param>
        /// <returns></returns>
        public List<object> SelectList(Type targetType, string selectcommand, int timeoutSec = -1)
        {
            List<object> result = new List<object>();
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = selectcommand;

                // set timeout
                if (timeoutSec != -1)
                { command.CommandTimeout = timeoutSec; }

                var reader = command.ExecuteReader();

                var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
                result = tpmp.MapListFromReader(reader, Provider.Key);

                //reader.Close();
                command.Dispose();
                cnctn.Close();
            } // here connection disposing

            return result;
        }

        /// <summary>
        ///  Select List of {T}  by [selectquery] query object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectquery"></param>
        /// <returns></returns>
        public List<T> SelectList<T>(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return (List<T>)SelectList<T>(selectCmdText);
        }

        public List<object> SelectList(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return (List<object>)SelectList<object>(selectCmdText);
        }

        /// <summary>
        ///  Select List of {T} by [selectcommand] string - query text.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectcommand"></param>
        /// <returns></returns>
        public List<T> SelectList<T>(string selectcommand)
        {
            var targetType = typeof(T);
            List<T> result = null;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = selectcommand;
                var reader = command.ExecuteReader();

                var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
                result = tpmp.MapListFromReader<T>(reader, Provider.Key);

                //reader.Close();
                command.Dispose();
                cnctn.Close();
            } // here connection disposing

            return result;
        }

        /// <summary>
        /// Select ObservableCollection of {T}  by [selectquery] query object.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectquery"></param>
        /// <returns></returns>
        public ObservableCollection<T> SelectObservable<T>(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return  SelectObservable<T>(selectCmdText);
        } /// Select ObservableCollection of {T}  by [selectquery] query object.


        /// <summary>
        /// Select ObservableCollection of {object}  by [selectquery] query object.
        /// </summary>
        /// <param name="selectquery"></param>
        /// <returns></returns>
        public ObservableCollection<object> SelectObservable(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return SelectObservable<object>(selectCmdText);
        }

        /// <summary>
        /// Select ObservableCollection of {T}  by [selectcommand] string - query text
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selectcommand"></param>
        /// <returns></returns>
        public ObservableCollection<T> SelectObservable<T>(string selectcommand)
        {
            var targetType = typeof(T);
            ObservableCollection<T> result = null;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = selectcommand;
                var reader = command.ExecuteReader();

                var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
                result = tpmp.MapObservableFromReader<T>(reader, Provider.Key);

                //reader.Close();
                command.Dispose();
                cnctn.Close();
            } // here connection disposing

            return result;
        }

        #endregion   ----------- SELECT ITEM/LIST  NO TRANSACTION ---------------

        #endregion --------------SELECT NO TRANSACTION-----------------

        #region ------------- SELECT IN TRANSACTION ---------------

        #region ------------ SELECT MANY LISTS IN TRANSACTION -------------


        /// <summary>
        /// Select several Collections 
        /// per one batch query, and one connection Opening/Closing.
        /// </summary>
        /// <param name="selects"></param>
        /// <returns></returns>
        public KeyValuePair<Type, List<object>>[] SelectManyListsTrc(params KeyValuePair<Type, DSelectQuery>[] selects)
        {
            var resultSets = new KeyValuePair<Type, List<object>>[selects.Length];

            // collect common selects text
            var comonSqlCmdsText = "";
            for (int i = 0; i < selects.Length; i++)
            {
                var selectCmdText = selects[i].Value.ToSQLString(Provider.Key);
                comonSqlCmdsText += "\r\n" + selectCmdText;
            }


            var command = Provider.CreateCommand(Connection);
            command.CommandText = comonSqlCmdsText;
            command.Transaction = Transaction;

            //per each query
            for (int i = 0; i < selects.Length; i++)
            {
                IDataReader reader = null;
                if (i == 0)
                {
                    reader = command.ExecuteReader();
                }
                else
                {
                    reader.NextResult();
                }

                var tpmp = TypeDAMap.GetInit(selects[i].Key, Provider.Key);
                var result = tpmp.MapListFromReader(reader, Provider.Key);
                resultSets[i] = new KeyValuePair<Type, List<object>>(selects[i].Key, result);
            }

            command.Dispose();

            return resultSets;
        }


        #endregion------------ SELECT MANY LISTS IN TRANSACTION -------------

        #region ----------- SELECT ITEM/LIST   IN  TRANSACTION ---------------
        public object SelectItemTrc(Type targetType, string selectcommand)
        {
            object result = null;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = selectcommand;
            command.Transaction = Transaction;
            var reader = command.ExecuteReader();

            //GetTable of Select columns
            //var table = reader.GetSchemaTable();

            //read record
            reader.Read();

            var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
            result = tpmp.MapItemFromReader(reader , Provider.Key);

            //reader.Close();
            //command.Dispose();

            // here we do not  dispose  connection

            return result;
        }

        public T SelectItemTrc<T>(string selectCommand)
        {
            return (T)SelectItemTrc(typeof(T), selectCommand);
        }

        public T SelectItemTrc<T>(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return (T)SelectItem<T>(selectCmdText);
        }

        public List<object> SelectListTrc(Type targetType, string selectcommand)
        {
            List<object> result = new List<object>();

            var command = Provider.CreateCommand(Connection);
            command.CommandText = selectcommand;
            command.Transaction = Transaction;
            var reader = command.ExecuteReader();

            var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
            result = tpmp.MapListFromReader(reader, Provider.Key);

            //reader.Close();
            //command.Dispose();

            // here we do not  dispose  connection

            return result;
        }

        public List<T> SelectListTrc<T>(DSelectQuery selectquery)
        {
            var selectCmdText = selectquery.ToSQLString(Provider.Key);
            return (List<T>)SelectListTrc<T>(selectCmdText);
        }

        public List<T> SelectListTrc<T>(string selectcommand)
        {
            var targetType = typeof(T);
            List<T> result = null;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = selectcommand;
            command.Transaction = Transaction;
            var reader = command.ExecuteReader();

            var tpmp = TypeDAMap.GetInit(targetType, Provider.Key);
            result = tpmp.MapListFromReader<T>(reader, Provider.Key);

            // here we do not  dispose  connection and  closing reader

            return result;
        }

        #endregion   ----------- SELECT ITEM/LIST   IN  TRANSACTION ---------------

        #endregion------------- SELECT  IN TRANSACTION ---------------



        #region  ---------- INSERT  NO TRANSACTION ----------------



        /// <summary>
        /// Insert BOItem NO Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertObject"></param>
        /// <returns></returns>
        public int InsertItem(IBObject insertObject
                                , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs
            if (insertObject is IVobject)
                insertObject = (IBObject)((IVobject)insertObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (insertObject is IPersistableBO), "InsertItem -target boItem is not IPersistableBO - is not DB Table ");


            int result = 0;


            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DInsertQuery.GetQuery(
                                          insertObject
                                        , Provider.Key
                                        , changeTrackingContext);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }



        /// <summary>
        /// Insert BOItem  NO Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItemQuery"></param>
        /// <returns></returns>
        public int InsertItem(DInsertQuery insertItemQuery
                                , Guid? changeTrackingContext = null)
        {

            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = insertItemQuery
                                        .WithChangeTrackingContext(changeTrackingContext)
                                        .ToSQLString(Provider.Key);
                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }



        /// <summary>
        /// Insert List of BOItems NO Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItems"></param>
        /// <param name="insertGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        public int InsertList(List<IBObject> insertItems
                                 , int insertGroupSize = 100
                                 , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < insertItems.Count; i++)
            {
                if (insertItems[i] is IVobject)
                    insertItems[i] = ((IVobject)insertItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (insertItems[i] is IPersistableBO), " InsertList -target boItem is not IPersistableBO - is not DB Table ");
            }

            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                for (int i = 0; i < insertItems.Count; i += insertGroupSize)
                {
                    var insertItemsGroup = insertItems
                                            .Skip(i)
                                            .Take(insertGroupSize);

                    var insertsGroupQuery = insertItemsGroup
                                                   .Aggregate(String.Empty
                                                   , (aggr, insertObject) =>
                                                        aggr
                                                      + DInsertQuery.GetQuery(insertObject
                                                           , this.Provider.Key
                                                           , changeTrackingContext)
                                                      + QDm);

                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = insertsGroupQuery;
                    result += command.ExecuteNonQuery();
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;
        }


        /// <summary>
        /// Insert BOItems  NO Transaction 
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItemQueries"></param>
        public int InsertList(List<DInsertQuery> insertItemQueries
                                 , int insertGroupSize = 100
                                 , Guid? changeTrackingContext = null
                                 )
        {
            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                for (int i = 0; i < insertItemQueries.Count; i += insertGroupSize)
                {
                    var insertGroupQueries = insertItemQueries
                                     .Skip(i)
                                     .Take(insertGroupSize);

                    var insertsGroupQuery = insertGroupQueries
                                                   .Aggregate(String.Empty
                                                   , (aggr, insertItemQuery) =>
                                                        aggr
                                                      + insertItemQuery
                                                        .WithChangeTrackingContext(changeTrackingContext)
                                                        .ToSQLString(this.Provider.Key)
                                                      + QDm);

                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = insertsGroupQuery;
                    result += command.ExecuteNonQuery();
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;
        }




        #endregion---------- INSERT  NO TRANSACTION ----------------

        #region  ---------- INSERT IN TRANSACTION ----------------

        /// <summary>
        /// Insert BOItem inside Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertObject"></param>
        /// <returns></returns>
        public int InsertItemTrc(IBObject insertObject
                                , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs
            if (insertObject is IVobject)
                insertObject = (IBObject)((IVobject)insertObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (insertObject is IPersistableBO), "InsertItemTrc -target boItem is not IPersistableBO - is not DB Table ");

            int result = 0;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = DInsertQuery.GetQuery(
                                    insertObject
                                  , Provider.Key
                                  , changeTrackingContext);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();
            command.Dispose();

            return result;
        }



        /// <summary>
        /// Insert BOItem inside Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItemQuery"></param>
        /// <returns></returns>
        public int InsertItemTrc(DInsertQuery insertItemQuery
                                , Guid? changeTrackingContext = null)
        {

            int result = 0;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = insertItemQuery
                                    .WithChangeTrackingContext(changeTrackingContext)
                                    .ToSQLString(Provider.Key);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();
            command.Dispose();

            return result;
        }



        /// <summary>
        /// Insert List of BOItems inside Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItems"></param>
        /// <param name="insertGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        public int InsertListTrc(List<IBObject> insertItems
                                 , int insertGroupSize = 100
                                 , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < insertItems.Count; i++)
            {
                if (insertItems[i] is IVobject)
                    insertItems[i] = ((IVobject)insertItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (insertItems[i] is IPersistableBO), " InsertListTrc -target boItem is not IPersistableBO - is not DB Table ");
            }


            //Insert in transaction
            int result = 0;

            for (int i = 0; i < insertItems.Count; i += insertGroupSize)
            {
                var insertItemsGroup = insertItems
                                        .Skip(i)
                                        .Take(insertGroupSize);

                var insertsGroupQuery = insertItemsGroup
                                               .Aggregate(String.Empty
                                               , (aggr, insertObject) =>
                                                    aggr
                                                  + DInsertQuery.GetQuery(insertObject
                                                       , this.Provider.Key
                                                       , changeTrackingContext)
                                                  + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = insertsGroupQuery;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();

            }

            return result;
        }

        /// <summary>
        /// Insert Observable Collection of BOItems inside Transaction
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItems"></param>
        /// <param name="insertGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int InsertObservableTrc(ObservableCollection<IBObject> insertItems
                                 , int insertGroupSize = 100
                                 , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < insertItems.Count; i++)
            {
                if (insertItems[i] is IVobject)
                    insertItems[i] = ((IVobject)insertItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (insertItems[i] is IPersistableBO), " InsertListTrc -target boItem is not IPersistableBO - is not DB Table ");
            }


            //Insert in transaction
            int result = 0;

            for (int i = 0; i < insertItems.Count; i += insertGroupSize)
            {
                var insertItemsGroup = insertItems
                                        .Skip(i)
                                        .Take(insertGroupSize);

                var insertsGroupQuery = insertItemsGroup
                                               .Aggregate(String.Empty
                                               , (aggr, insertObject) =>
                                                    aggr
                                                  + DInsertQuery.GetQuery(insertObject
                                                       , this.Provider.Key
                                                       , changeTrackingContext)
                                                  + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = insertsGroupQuery;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }



        /// <summary>
        /// Insert BOItems inside Transaction 
        /// Here we also can use changeTrackingContext - for EX. to set/determine User authority by UserID.
        /// </summary>
        /// <param name="insertItemQueries"></param>
        public int InsertListTrc(List<DInsertQuery> insertItemQueries
                                 , int insertGroupSize = 100
                                 , Guid? changeTrackingContext = null
                                 )
        {
            //Insert in transaction
            int result = 0;

            for (int i = 0; i < insertItemQueries.Count; i += insertGroupSize)
            {
                var insertGroupQueries = insertItemQueries
                                        .Skip(i)
                                        .Take(insertGroupSize);

                var insertsGroupQuery = insertGroupQueries
                                               .Aggregate(String.Empty
                                               , (aggr, insertItemQuery) =>
                                                    aggr
                                                  + insertItemQuery
                                                    .WithChangeTrackingContext(changeTrackingContext)
                                                    .ToSQLString(this.Provider.Key)
                                                  + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = insertsGroupQuery;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }

        #endregion---------- INSERT IN TRANSACTION ----------------



        #region  ---------- UPDATE  NO TRANSACTION ----------------


        /// <summary>
        ///  Update item - with prepared UpdateQuery
        /// </summary>
        /// <param name="updateItemQuery"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateItem(DUpdateQuery updateItemQuery
                            , Guid? changeTrackingContext = null)
        {
            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                updateItemQuery.WithChangeTrackingContext(changeTrackingContext);

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = updateItemQuery
                                      .ToSQLString(Provider.Key);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;

        }


        /// <summary>
        /// Update item -  [boItem] record - for all fields besides PK fields
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateItem(IBObject boItem
                            , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query
            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItem -target boItem is not IPersistableBO - is not DB Table ");


            int result = 0;

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                //Проверить поля объекта на тип данных даты 
                //и отсутствие значения времени                         
                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DUpdateQuery.GetQuery(boItem
                                        , Provider.Key
                                        , changeTrackingContext);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;

        }

        /// <summary>
        /// Update item - get only [updateProps] domain property values, no PK fields, 
        /// from [boItem] and update its record in table-[boItem.Contract] with these values.         
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <param name="UpdateFields"></param>
        /// <returns></returns>
        public int UpdateItem(IBObject boItem
                            , List<String> updateProps
                            , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query

            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItem -target boItem is not IPersistableBO - is not DB Table ");


            int result = 0;

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                //Проверить поля объекта на тип данных даты 

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DUpdateQuery.GetQuery(boItem
                                    , updateProps
                                    , Provider.Key
                                    , changeTrackingContext);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }




        /// <summary>
        ///  Update items - update with [propertyValues] values  table-[boContract] 
        ///  with [filter].
        /// </summary>
        /// <param name="boContract"></param>
        /// <param name="propertyValues"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int UpdateItems(Type boContract
                              , Dictionary<string, object> propertyValues
                              , DFilterSet filter
                              , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query
            Validator.ATInvalidOperationDbg<DBProxy>(
                     boContract.IsImplementInterface<IPersistableBO>() == false
                , nameof(UpdateItems), " Only types thas implement IPersistableBO can be used here - as table Contract");


            int result = 0;

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DUpdateQuery.GetQuery(
                                                  boContract
                                                , propertyValues
                                                , filter
                                                , Provider.Key
                                                , changeTrackingContext);
                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }


        /// <summary>
        /// Update items - get update fields[propNames]  values from [boItem] 
        /// and update table-[boItem.Contract] with [filter]
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="properties"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int UpdateItems(IBObject boItem
                               , List<string> propNames
                               , DFilterSet filter
                               , Guid? changeTrackingContext = null
            )
        {

            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItems -target boItem is not IPersistableBO - is not DB Table ");

            int result = 0;

            // collect domain property values no PK
            var propValues = (boItem as IPersistableBO)
                             .GetDomainPropValuesNoPK();

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DUpdateQuery.GetQuery(
                                       boItem
                                    , propNames
                                    , filter
                                    , Provider.Key
                                    , changeTrackingContext);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }




        /// <summary>
        /// Update list of items - update list of IBObject items,
        /// for all domain fields no PK fields per item
        /// </summary>
        /// <param name="updateItems"></param>
        /// <param name="updateGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateList(List<IBObject> updateItems
                              , int updateGroupSize = 100
                              , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < updateItems.Count; i++)
            {
                if (updateItems[i] is IVobject)
                    updateItems[i] = ((IVobject)updateItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (updateItems[i] is IPersistableBO), " UpdateList -target boItem is not IPersistableBO - is not DB Table ");
            }


            int result = 0;

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                for (int i = 0; i < updateItems.Count; i += updateGroupSize)
                {
                    var updateItemsGroup = updateItems
                                            .Skip(i)
                                            .Take(updateGroupSize);

                    var updateGroupQuery = updateItemsGroup
                                                   .Aggregate(String.Empty
                                                   , (aggr, updateObject) =>
                                                        aggr
                                                      + DUpdateQuery.GetQuery(updateObject
                                                           , this.Provider.Key
                                                           , changeTrackingContext)
                                                      + QDm);

                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = updateGroupQuery;
                    result += command.ExecuteNonQuery();
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;
        }


        /// <summary>
        /// Update items - update with list of update queries.
        /// </summary>
        /// <param name="updateItemQueries"></param>
        /// <param name="updateGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateList(List<DUpdateQuery> updateItemQueries
                              , int updateGroupSize = 100
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;

            using (var cnctn = Provider
                              .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                for (int i = 0; i < updateItemQueries.Count; i += updateGroupSize)
                {
                    var updateItemsQueriesGroup = updateItemQueries
                                                    .Skip(i)
                                                    .Take(updateGroupSize);

                    var updateGroupQueryText = updateItemsQueriesGroup
                                                   .Aggregate(String.Empty
                                                   , (aggr, updateQuery) =>
                                                        aggr
                                                      + updateQuery
                                                        .WithChangeTrackingContext(changeTrackingContext)
                                                        .ToSQLString(this.Provider.Key)
                                                      + QDm);

                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = updateGroupQueryText;
                    result += command.ExecuteNonQuery(); //ExecuteScalar
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;
        }



        #endregion---------- UPDATE  NO TRANSACTION ----------------

        #region  ---------- UPDATE IN TRANSACTION ----------------


        /// <summary>
        ///  Update item(IN TRANSACTION) - with prepared UpdateQuery
        /// </summary>
        /// <param name="updateItemQuery"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateItemTrc(DUpdateQuery updateItemQuery, Guid? changeTrackingContext)
        {
            int result = 0;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = updateItemQuery
                                    .WithChangeTrackingContext(changeTrackingContext)
                                    .ToSQLString(Provider.Key);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();

            command.Dispose();

            return result;

        }


        /// <summary>
        /// Update item(IN TRANSACTION) -  [boItem] record - for all fields besides PK fields
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateItemTrc(IBObject boItem, Guid? changeTrackingContext)
        {
            // if not IPersistableBO it can be used in update query
            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItem -target boItem is not IPersistableBO - is not DB Table ");

            int result = 0;

            //Проверить поля объекта на тип данных даты 
            //и отсутствие значения времени          
            var command = Provider.CreateCommand(Connection);
            command.CommandText = DUpdateQuery.GetQuery(boItem
                                        , Provider.Key
                                        , changeTrackingContext);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();

            command.Dispose();

            return result;
        }

        /// <summary>
        /// Update item(IN TRANSACTION) - get only [updateProps] domain property values, no PK fields, 
        /// from [boItem] and update its record in table-[boItem.Contract] with these values.         
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <param name="UpdateFields"></param>
        /// <returns></returns>
        public int UpdateItemTrc(IBObject boItem
                                    , List<String> updateProps
                                    , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query

            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItem -target boItem is not IPersistableBO - is not DB Table ");

            int result = 0;

            //Проверить поля объекта на тип данных даты              
            var command = Provider.CreateCommand(Connection);
            command.CommandText = DUpdateQuery.GetQuery(boItem
                                    , updateProps
                                    , Provider.Key
                                    , changeTrackingContext);

            command.Transaction = Transaction;

            result = command.ExecuteNonQuery();

            command.Dispose();


            return result;
        }



        /// <summary>
        ///  Update items(IN TRANSACTION) - update with [propertyValues] values  table-[boContract] 
        ///  with [filter].
        /// </summary>
        /// <param name="boContract"></param>
        /// <param name="propertyValues"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int UpdateItemsTrc(Type boContract
                                  , Dictionary<string, object> propertyValues
                                  , DFilterSet filter
                                  , Guid? changeTrackingContext = null
           )
        {
            // if not IPersistableBO it can be used in update query
            Validator.ATInvalidOperationDbg<DBProxy>(
                     boContract.IsImplementInterface<IPersistableBO>() == false
                , nameof(UpdateItems), " Only types thas implement IPersistableBO can be used here - as table Contract");

            int result = 0;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = DUpdateQuery.GetQuery(
                                       boContract
                                    , propertyValues
                                    , filter
                                    , Provider.Key, changeTrackingContext);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();

            command.Dispose();

            return result;
        }


        /// <summary>
        /// Update items(IN TRANSACTION) - get update fields[propNames]  values from [boItem] 
        /// and update table-[boItem.Contract] with [filter]
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="properties"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int UpdateItemsTrc(IBObject boItem
                               , List<string> propNames
                               , DFilterSet filter
                               , Guid? changeTrackingContext = null
            )
        {

            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "UpdateItems -target boItem is not IPersistableBO - is not DB Table ");

            int result = 0;
            var command = Provider.CreateCommand(Connection);
            command.CommandText = DUpdateQuery.GetQuery(
                                      boItem
                                    , propNames
                                    , filter
                                    , Provider.Key, changeTrackingContext);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();

            command.Dispose();

            return result;
        }




        /// <summary>
        /// Update list(IN TRANSACTION) - update list of IBObject items,
        /// for all domain fields no PK fields per item
        /// </summary>
        /// <param name="updateItems"></param>
        /// <param name="updateGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateListTrc(List<IBObject> updateItems
                              , int updateGroupSize = 100
                              , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < updateItems.Count; i++)
            {
                if (updateItems[i] is IVobject)
                    updateItems[i] = ((IVobject)updateItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (updateItems[i] is IPersistableBO), " UpdateList -target boItem is not IPersistableBO - is not DB Table ");
            }

            int result = 0;

            for (int i = 0; i < updateItems.Count; i += updateGroupSize)
            {
                var updateItemsGroup = updateItems
                                        .Skip(i)
                                        .Take(updateGroupSize);

                var updateGroupQuery = updateItemsGroup
                                                .Aggregate(String.Empty
                                                , (aggr, updateObject) =>
                                                    aggr
                                                    + DUpdateQuery.GetQuery(updateObject
                                                        , Provider.Key
                                                        , changeTrackingContext)
                                                    + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = updateGroupQuery;
                command.Transaction = Transaction;

                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }

        public int UpdateObservableTrc(ObservableCollection<IBObject> updateItems
                              , int updateGroupSize = 100
                              , Guid? changeTrackingContext = null)
        {
            //check  convert from IVObject  if it needs            
            for (int i = 0; i < updateItems.Count; i++)
            {
                if (updateItems[i] is IVobject)
                    updateItems[i] = ((IVobject)updateItems[i]).CloneToTarget();

                Validator.AssertFalseDbg<InvalidOperationException>(
                (updateItems[i] is IPersistableBO), " UpdateList -target boItem is not IPersistableBO - is not DB Table ");
            }

            int result = 0;

            for (int i = 0; i < updateItems.Count; i += updateGroupSize)
            {
                var updateItemsGroup = updateItems
                                        .Skip(i)
                                        .Take(updateGroupSize);

                var updateGroupQuery = updateItemsGroup
                                                .Aggregate(String.Empty
                                                , (aggr, updateObject) =>
                                                    aggr
                                                    + DUpdateQuery.GetQuery(updateObject
                                                        , Provider.Key
                                                        , changeTrackingContext)
                                                    + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = updateGroupQuery;
                command.Transaction = Transaction;

                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }





        /// <summary>
        ///  Update list(IN TRANSACTION) - update with list of update queries.
        /// </summary>
        /// <param name="updateItemQueries"></param>
        /// <param name="updateGroupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int UpdateListTrc(List<DUpdateQuery> updateItemQueries
                              , int updateGroupSize = 100
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;


            for (int i = 0; i < updateItemQueries.Count; i += updateGroupSize)
            {
                var updateItemsQueriesGroup = updateItemQueries
                                                .Skip(i)
                                                .Take(updateGroupSize);

                var updateGroupQueryText = updateItemsQueriesGroup
                                                .Aggregate(String.Empty
                                                , (aggr, updateQuery) =>
                                                    aggr
                                                    + updateQuery
                                                    .WithChangeTrackingContext(changeTrackingContext)
                                                    .ToSQLString(this.Provider.Key)
                                                    + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = updateGroupQueryText;
                command.Transaction = Transaction;

                result += command.ExecuteNonQuery(); //ExecuteScalar
                command.Dispose();
            }

            return result;
        }





        #endregion---------- UPDATE IN TRANSACTION ----------------



        #region  ---------- DELETE  NO TRANSACTION ----------------


        /// <summary>
        /// Delete item - with prepared DeleteQuery
        /// </summary>
        /// <param name="deleteItemQuery"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteItem(DDeleteQuery deleteItemQuery
                                , Guid? changeTrackingContext = null)
        {
            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();


                var command = Provider.CreateCommand(cnctn);
                command.CommandText = deleteItemQuery
                                      .WithChangeTrackingContext(changeTrackingContext)
                                      .ToSQLString(Provider.Key);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;
        }

        /// <summary>
        /// Delete item - delete [boItem] record in DB Table
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteItem(IBObject boItem
                              , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query
            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "DeleteItem -target boItem is not IPersistableBO - is not DB Table ");


            int result = 0;

            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();

                //Проверить поля объекта на тип данных даты 
                //и отсутствие значения времени                          

                var command = Provider.CreateCommand(cnctn);
                command.CommandText = DDeleteQuery.GetQuery(boItem
                                , Provider.Key
                                , changeTrackingContext);

                result = command.ExecuteNonQuery();

                command.Dispose();
                cnctn.Close();
            }

            return result;

        }

        /// <summary>
        ///  Delete list - delete [deleteItems] records in DB Table 
        ///  with Grouping Delete queries by [groupSize].
        /// </summary>
        /// <param name="deleteItems"></param>
        /// <param name="groupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteList(List<IBObject> deleteItems
                              , int groupSize = 500
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                for (int i = 0; i < deleteItems.Count; i += 500)
                {

                    var groupDeleteQueryText =
                        // group items to delete 
                        deleteItems
                        .Skip(i).Take(groupSize)
                        // Build - aggregate query for selected group
                        .Aggregate(String.Empty,
                        (aggr, deleteObject) =>
                        aggr
                        + DDeleteQuery.GetQuery(deleteObject
                                    , Provider.Key
                                    , changeTrackingContext)
                        + QDm);
                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = groupDeleteQueryText;
                    result += command.ExecuteNonQuery();
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;

        }

        /// <summary>
        ///  Delete list - delete [deleteItems] records in DB Table  
        ///  with prepared Delete Queries and
        ///  with Grouping Delete queries by [groupSize].
        /// </summary>
        /// <param name="deleteItemQueries"></param>
        /// <param name="groupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteList(List<DDeleteQuery> deleteItemQueries
                              , int groupSize = 500
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;
            using (var cnctn = Provider
                    .CreateConnection(ConnectionString))
            {
                cnctn.Open();
                for (int i = 0; i < deleteItemQueries.Count; i += 500)
                {
                    var groupDeleteQueryText =
                        // group items to delete 
                        deleteItemQueries
                        .Skip(i).Take(groupSize)
                        // Build - aggregate query for selected group
                        .Aggregate(String.Empty,
                        (aggr, deleteItemQuery) =>
                        aggr
                        + deleteItemQuery
                           .WithChangeTrackingContext(changeTrackingContext)
                           .ToSQLString(Provider.Key)
                        + QDm);

                    var command = Provider.CreateCommand(cnctn);
                    command.CommandText = groupDeleteQueryText;
                    result += command.ExecuteNonQuery();
                    command.Dispose();
                }

                cnctn.Close();
            }

            return result;
        }


        #endregion---------- DELETE  NO TRANSACTION ----------------

        #region  ---------- DELETE IN TRANSACTION ----------------

        /// <summary>
        /// Delete item(IN TRANSACTION) - with prepared DeleteQuery
        /// </summary>
        /// <param name="deleteItemQuery"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteItemTrc(DDeleteQuery deleteItemQuery
                                , Guid? changeTrackingContext = null)
        {
            int result = 0;

            var command = Provider.CreateCommand(Connection);
            command.CommandText = deleteItemQuery
                                    .WithChangeTrackingContext(changeTrackingContext)
                                    .ToSQLString(Provider.Key);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();

            command.Dispose();

            return result;
        }

        /// <summary>
        /// Delete item(IN TRANSACTION) - delete [boItem] record in DB Table
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteItemTrc(IBObject boItem
                              , Guid? changeTrackingContext = null)
        {
            // if not IPersistableBO it can be used in update query
            //check  convert from IVObject  if it needs
            if (boItem is IVobject)
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "DeleteItem -target boItem is not IPersistableBO - is not DB Table ");


            int result = 0;

            //Проверить поля объекта на тип данных даты 
            //и отсутствие значения времени                          

            var command = Provider.CreateCommand(Connection);
            command.CommandText = DDeleteQuery.GetQuery(boItem
                            , Provider.Key
                            , changeTrackingContext);
            command.Transaction = Transaction;
            result = command.ExecuteNonQuery();
            command.Dispose();

            return result;
        }

        /// <summary>
        ///  Delete list(IN TRANSACTION) - delete [deleteItems] records in DB Table 
        ///  with Grouping Delete queries by [groupSize].
        /// </summary>
        /// <param name="deleteItems"></param>
        /// <param name="groupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteListTrc(List<IBObject> deleteItems
                              , int groupSize = 500
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;

            for (int i = 0; i < deleteItems.Count; i += 500)
            {
                var groupDeleteQueryText =
                    // group items to delete 
                    deleteItems
                    .Skip(i).Take(groupSize)
                    // Build - aggregate query for selected group
                    .Aggregate(String.Empty,
                    (aggr, deleteObject) =>
                    aggr
                    + DDeleteQuery.GetQuery(deleteObject
                                , Provider.Key
                                , changeTrackingContext)
                    + QDm);
                var command = Provider.CreateCommand(Connection);
                command.CommandText = groupDeleteQueryText;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }

        /// <summary>
        ///  Delete observable Collection(IN TRANSACTION) - delete [deleteItems] records in DB Table 
        ///  with Grouping Delete queries by [groupSize].
        /// </summary>
        /// <param name="deleteItems"></param>
        /// <param name="groupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteObservableTrc(ObservableCollection<IBObject> deleteItems
                            , int groupSize = 500
                            , Guid? changeTrackingContext = null)
        {
            int result = 0;

            for (int i = 0; i < deleteItems.Count; i += 500)
            {
                var groupDeleteQueryText =
                    // group items to delete 
                    deleteItems
                    .Skip(i).Take(groupSize)
                    // Build - aggregate query for selected group
                    .Aggregate(String.Empty,
                    (aggr, deleteObject) =>
                    aggr
                    + DDeleteQuery.GetQuery(deleteObject
                                , Provider.Key
                                , changeTrackingContext)
                    + QDm);
                var command = Provider.CreateCommand(Connection);
                command.CommandText = groupDeleteQueryText;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }



        /// <summary>
        ///  Delete list(IN TRANSACTION) - delete [deleteItems] records in DB Table  
        ///  with prepared Delete Queries and
        ///  with Grouping Delete queries by [groupSize].
        /// </summary>
        /// <param name="deleteItemQueries"></param>
        /// <param name="groupSize"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public int DeleteListTrc(List<DDeleteQuery> deleteItemQueries
                              , int groupSize = 500
                              , Guid? changeTrackingContext = null)
        {
            int result = 0;

            for (int i = 0; i < deleteItemQueries.Count; i += 500)
            {
                var groupDeleteQueryText =
                    // group items to delete 
                    deleteItemQueries
                    .Skip(i).Take(groupSize)
                    // Build - aggregate query for selected group
                    .Aggregate(String.Empty,
                    (aggr, deleteItemQuery) =>
                    aggr
                    + deleteItemQuery
                       .WithChangeTrackingContext(changeTrackingContext)
                       .ToSQLString(Provider.Key)
                    + QDm);

                var command = Provider.CreateCommand(Connection);
                command.CommandText = groupDeleteQueryText;
                command.Transaction = Transaction;
                result += command.ExecuteNonQuery();
                command.Dispose();
            }

            return result;
        }




        #endregion---------- DELETE IN TRANSACTION ----------------



        //
        // BOContextContainer
        //
        #region ------------- BOCONTEXTCONTAINER NO TRANSACTION ----------------- 


        const string Target = nameof(Target);

        /// <summary>
        /// Select  BOContextContainer
        /// </summary>
        /// <param name="persistBO"></param>
        /// <param name="changeTrackingContext"></param>
        /// <returns></returns>
        public BOContextContainer SelectBOContext(IPersistableBO persistBO
                                    , Guid? changeTrackingContext = null)
        {
            BOContextContainer Context = ServiceModel.GetItemDataContext(persistBO);
            //Проверка параметра Имени пользователя
            //DataContextInfoTp Context = ServiceModelAnalyzer.GetItemDataContext(persistBO);

            ///1 Get Target Object ItemKeyValue --> 
            var ItemPKeyValue = (Context.TargetContainer.Item as IPersistableBO)
                                                      .GetPrimaryKey();

            //1 Получаем объект  TargetBO заново по ключу
            DSelectQuery Query = DSelectQuery.SelectAll()
                                .From(Context.TargetContainer.Item.Contract); // Key

            foreach (var keyVal in ItemPKeyValue)
            {
                if (Query.WhereSet == null)
                    Query = Query.Where(keyVal.Key, Compare.Equals, DConst.Const(keyVal.Value));
                else
                    Query = Query.AndWhere(keyVal.Key, Compare.Equals, DConst.Const(keyVal.Value));
            }

            //Запуск и получение данных
            Context.TargetContainer.Item = SelectItem<IBObject>(Query); //, Context.TargetContainer.FullName

            //2 Потом составляем запрос для каждого связанного(AssociatedBO) типа VObject в контексте, по этому ключу(1) 
            foreach (var item in Context.AllContainers)
            {
                if (item.Key == Target) continue;

                //Compose QUERY HERE
                Query = DSelectQuery.SelectAll()
                        .From(
                    ServiceModel.GetByIndex(
                        NamingConvention.GetServiceModelIndex(item.Key))
                    .GetBOContractByName(item.Key));

                foreach (var keyVal in ItemPKeyValue)
                {
                    if (Query.WhereSet == null)
                        Query = Query.Where(keyVal.Key, Compare.Equals, DConst.Const(keyVal.Value));
                    else
                        Query = Query.AndWhere(keyVal.Key, Compare.Equals, DConst.Const(keyVal.Value));
                }

                //Запуск и получение данных
                item.Value.Collection = SelectObservable<IBObject>(Query);

            }

            return Context;
        }



        public void UpdateBOContext(BOContextContainer contextConiner
                                    , Guid userID
                                    , IsolationLevel isolation = IsolationLevel.ReadCommitted
                                    , bool callDataContextAudit = false
                                    , bool useUpdateCommandsInDataContext = true
                                    )
        {
            RunTransaction(isolation,
                () =>  //committing
                {
                    // For Add DataContext
                    if (contextConiner.TargetContainer.Update.FlowContext.Mode == FlowProcessMode.Add)
                    {
                        if (contextConiner.TargetContainer.Item != null)
                            InsertItemTrc(contextConiner.TargetContainer.Item, userID);

                        if (contextConiner.TargetContainer.Item != null
                        && (contextConiner.TargetContainer.Item as IPersistableBO).IsPKIdentity())
                        {
                            contextConiner.UpdateByTargetIdentityPK();
                        }

                        // Обработка Ассоциированных коллекций
                        foreach (var itemBOInfo in contextConiner.AllContainers)
                        {
                            if (itemBOInfo.Key == Target) continue;

                            // Добавить все коллекции ассоциативных
                            if (itemBOInfo.Value.Update.Commands.IsContainsInsertCommand)
                            {
                                InsertObservableTrc(itemBOInfo.Value
                                    .Update.Commands.InsertCollection, 100, userID);
                            }
                        }
                    }// If    we AddingContext-  Add 

                    //For Update DataContext
                    else if (contextConiner.TargetContainer.Update.FlowContext.Mode == FlowProcessMode.Update)
                    {
                        // Update TargetBO record - 
                        if (contextConiner.TargetContainer.Item != null)
                        {
                            UpdateItemTrc(contextConiner.TargetContainer.Item, userID);
                            //UpdateObservableTrc(contextConiner.TargetContainer.UpdateContext.Commands.UpdateCollection, 100, userID);                            
                        }

                        //AssociatedBOs -                             
                        foreach (var item in contextConiner.AllContainers)
                        {
                            if (item.Key == Target) continue;

                            //Insert objects
                            if (item.Value.Update.Commands.IsContainsInsertCommand)
                            { InsertObservableTrc(item.Value.Update.Commands.InsertCollection, 100, userID); }

                            //Delete bjects
                            if (item.Value.Update.Commands.IsContainsDeleteCommand)
                            { DeleteObservableTrc(item.Value.Update.Commands.DeleteCollection, 100, userID); }

                            //Update objects
                            if (item.Value.Update.Commands.IsContainsUpdateCommand)
                            { UpdateObservableTrc(item.Value.Update.Commands.UpdateCollection, 100, userID); }
                        }

                    } // If    we AddingContext-  Add

                }
                ,
                null// rollback Action
                );

        }


        public void DeleteBOContext(BOContextContainer contextContainer
                                    , Guid userID
                                    , IsolationLevel isolation = IsolationLevel.ReadCommitted
                                   )
        {
            RunTransaction(isolation,
                () =>  //committing
                {
                    //сначала удаляются все ассоциативные коллекции
                    foreach (var item in contextContainer.AllContainers)
                    {
                        if (item.Key == Target) continue;

                        if (item.Value.Collection.Count > 0)
                        {
                            DeleteObservableTrc(item.Value.Collection, 100, userID);
                        }
                    }

                    //удалить сам тарджет итем
                    DeleteItemTrc(contextContainer.TargetContainer.Item, userID);

                }
                , null // rollback Action                
                );
        }



        #endregion------------- BOCONTEXTCONTAINER NO TRANSACTION ----------------- 







        /* SQl CRUD commands
        * 


               //public const string SelectDataContext = nameof(SelectDataContext
               /// <summary>
               /// 12  Update Service Data Model by DataContext collections
               /// </summary>
               //public const string UpdateModelByDataContext = nameof(UpdateModelByDataContext);
               ///// <summary>            
               /////13 Delete DataContext collections from DB
               ///// </summary>
               //public const string DeleteDataContext = nameof(DeleteDataContext);

               /// <summary>            
               ///14 Select sevetal collections of BO, each collection will be getted by its own select query. 2-5  COllections with Queries
               /// </summary>
               //public const string SelectBOCollections = nameof(SelectBOCollections);
               //public const string SelectCollectionSlowlyButSurely = nameof(SelectCollectionSlowlyButSurely);
               //public const string GetChangeTrackingCurrentVersion = nameof(GetChangeTrackingCurrentVersion);
               //public const string GetSyncChanges = nameof(GetSyncChanges);

               //GetRoles            
               //public const string Max = nameof(Max);
               //public const string SelectInt = nameof(SelectInt);
               //public const string InsertCollectionWithOneQuery = nameof(InsertCollectionWithOneQuery);
               //public const string DeleteCollectionFast = nameof(DeleteCollectionFast);
               //public const string GetServerDateTime = nameof(GetServerDateTime);
               //public const string GetCodeIdPrefix = nameof(GetCodeIdPrefix);

               //public const string CreateDatabaseBackupForClient = nameof(CreateDatabaseBackupForClient);
               //public const string DeleteDatabaseBackupByID = nameof(DeleteDatabaseBackupByID);
               //public const string GetBDBackupChunk = nameof(GetBDBackupChunk);
               //public const string GetCreateDatabaseBackupForClientCmdResult = nameof(GetCreateDatabaseBackupForClientCmdResult);

               //public const string CreateTempDBCopy = nameof(CreateTempDBCopy);
               //public const string CreateTempDBBackup = nameof(CreateTempDBBackup);
               //public const string RestoreTempDBBackup = nameof(RestoreTempDBBackup);

         */


    }
}



#region --------------- GARBAGE -----------------


///// <summary>
///// Begin Transaction if connection exist and Opened, and previos Transaction is finished.        
///// </summary>
///// </summary>
///// <returns></returns>
//public virtual IDbTransaction BeginTransaction()
//{
//    if (Connection != null 
//        && Connection.State == ConnectionState.Open
//        && Transaction == null)
//    {
//        Transaction = Connection.BeginTransaction();
//        return Transaction;
//    }

//    throw new InvalidOperationException("Connection muswt be opened");
//}

///// <summary>
///// Begin Transaction if connection exist and Opened, and previos Transaction is finished.        
///// Here we also use Isolation Level for new  Transaction.
///// </summary>
///// <param name="isolate"></param>
///// <returns></returns>
//public virtual IDbTransaction BeginTransaction(IsolationLevel isolate)
//{
//    if (Connection != null && Connection.State == ConnectionState.Open)
//    {
//        Transaction = Connection.BeginTransaction(isolate);
//        return Transaction;
//    }

//    throw new InvalidOperationException("Connection muswt be opened");
//}


///// <summary>
///// Commit   Beginned Transaction, and then Dispose  Transaction  object
///// </summary>
//public virtual void CommitTransact()
//{
//    if (Transaction != null )
//    {
//        Transaction.Commit();
//        Transaction.Dispose();
//        Transaction = null;
//    }            
//}

///// <summary>
///// Rollback  Beginned Transaction, and then Dispose  Transaction  object
///// </summary>
//public virtual void RollbackTransact()
//{
//    if (Transaction != null)
//    {
//        Transaction.Rollback();
//        Transaction.Dispose();
//        Transaction = null;
//    }
//}



#endregion --------------- GARBAGE -----------------
