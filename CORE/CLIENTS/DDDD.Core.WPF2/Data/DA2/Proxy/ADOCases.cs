﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2.Proxy
{
    class ADO_NETCases
    {


        /*       GetData()   
         *        
         void GetData() ExecuteObjectInternal()
        {
            string connectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=usersdb;Integrated Security=True";

            string sqlExpression = "SELECT * FROM Users";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(sqlExpression, connection);
                SqlDataReader reader = command.ExecuteReader();

                if (reader.HasRows) // если есть данные
                {
                    // выводим названия столбцов
                    Console.WriteLine("{0}\t{1}\t{2}", reader.GetName(0), reader.GetName(1), reader.GetName(2));

                    while (reader.Read()) // построчно считываем данные
                    {
                        object id = reader.GetValue(0);
                        object name = reader.GetValue(1);
                        object age = reader.GetValue(2);

                        Console.WriteLine("{0} \t{1} \t{2}", id, name, age);
                    }
                }

                reader.Close();
                
                //get command returnd affected rows
                command.Dispose();
            }

        }
        */


        /*     ExecuteObjectInternal     
        private object ExecuteObjectInternal(object entity, Type type, object[] parameters)
        {
            if (_prepared)
                InitParameters(CommandAction.Select);

            using (var dr = ExecuteReaderInternal( "CommandBehavior.SingleRow")) // Sybase provider does not support this flag.
                return ExecuteOperation(OperationType.Read, () =>
                {
            while (dr.Read())
                return
                    entity == null
                        ? _mappingSchema.MapDataReaderToObject(dr, type, parameters)
                        : _mappingSchema.MapDataReaderToObject(dr, entity, parameters);

            return null;
        });
        }



    public DCMessage SelectItem(ref DCMessage command)
    {
        try
        {
            //  --------- 1 Getting Parameters ( if Using Command with Params) ------
            //Проверка параметра Имени пользователя
            //DBSelectQuery selectQuery = command.GetParamVal<DBSelectQuery>(CommandParams.SelectQueryParam);               
            string selectQuery = command.GetParamVal<string>("SelectQueryParam");
            string dataContractTypeName = command.GetParamVal<string>("DataContractTypeName");

            //----------- 2 Custom Command Processing -------------------
            //object item = null;

            using (DbManager dbmngr = DbManager.Create(new SqlDataProvider(), "DomainConnectionString"))
            {
                object result = SelectItem(selectQuery, dataContractTypeName, dbmngr);

                command.SetResult(result);
            }

            //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
            return command.SayCommandCompleteSuccesfully();

        }
        catch (Exception exc)
        {
            command.SayCommandThrowUnknownError(nameof(SqlCRUDService), exc.Message); // Error Info in Command Processing  
            throw; // Throw to Root EcecuteCommand method
        }
    }


        */

        /* Stored Proccedures Example
         *
         protected void btnSubmit_Click(object sender, EventArgs e)
         {
            //Read the connection string from Web.Config file
            string ConnectionString = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
            //Create the SqlCommand object
            SqlCommand cmd = new SqlCommand("spAddEmployee", con);

            //Specify that the SqlCommand is a stored procedure
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            //Add the input parameters to the command object
            cmd.Parameters.AddWithValue("@Name", txtEmployeeName.Text);
            cmd.Parameters.AddWithValue("@Gender", ddlGender.SelectedValue);
            cmd.Parameters.AddWithValue("@Salary", txtSalary.Text);

            //Add the output parameter to the command object
            SqlParameter outPutParameter = new SqlParameter();
            outPutParameter.ParameterName = "@EmployeeId";
            outPutParameter.SqlDbType = System.Data.SqlDbType.Int;
            outPutParameter.Direction = System.Data.ParameterDirection.Output;
            cmd.Parameters.Add(outPutParameter);

            //Open the connection and execute the query
            con.Open();
            cmd.ExecuteNonQuery();

            //Retrieve the value of the output parameter
            string EmployeeId = outPutParameter.Value.ToString();
            lblMessage.Text = "Employee Id = " + EmployeeId;
            }
         }
         
         */

        /* Stored Procedure with output select
         

            CREATE PROCEDURE TestProcedure
            AS
            BEGIN
                SELECT ID, Name 
                FROM Test
            END

            SqlConnection connection = new SqlConnection(ConnectionString);
            command = new SqlCommand("TestProcedure", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();

            List<Test> TestList = new List<Test>();
            Test test = null;

            while (reader.Read())
            {
                test = new Test();
                test.ID = int.Parse(reader["ID"].ToString());
                test.Name = reader["Name"].ToString();
                TestList.Add(test);
            }

            gvGrid.DataSource = TestList;
            gvGrid.DataBind();
         
        Using dataTable:

            SqlConnection connection = new SqlConnection(ConnectionString);
            command = new SqlCommand("TestProcedure", connection);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            connection.Open();
            DataTable dt = new DataTable();
            dt.Load(command.ExecuteReader());
            gvGrid.DataSource = dt;
            gvGrid.DataBind();
             

         */


        /* Stored Procedure with output select  MVC5  EF6
           Usin ASP.NET MVC5 EF6 
            
            // GET: api/GetStudent

            public Response Get() {
                return StoredProcedure.GetStudent();
            }

            public static Response GetStudent() 
            {
                   using (var db = new dal()) 
                   {
                    var student = db.Database.SqlQuery<GetStudentVm>("GetStudent").ToList();
                    return new Response {
                        Sucess = true,
                        Message = student.Count() + " Student found",
                        Data = student
                    };
                  }
            }        

        */

        /* Convert to List<T> from DataTable
        
        /// <summary>
        /// Converts datatable to List<someType> if possible.
        /// </summary>
        public static List<T> ConvertToList<T>(DataTable dt)
        {
            try // Necesarry unfotunately.
            {
                var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();

                var properties = typeof(T).GetProperties();

                return dt.AsEnumerable().Select(row =>
                    {
                        var objT = Activator.CreateInstance<T>();

                        foreach (var pro in properties)
                        {
                            if (columnNames.Contains(pro.Name))
                            {
                                if (row[pro.Name].GetType() == typeof(System.DBNull)) pro.SetValue(objT, null, null);
                                else pro.SetValue(objT, row[pro.Name], null);
                            }
                        }

                        return objT;
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to write data to list. Often this occurs due to type errors (DBNull, nullables), changes in SP's used or wrongly formatted SP output.");
                Console.WriteLine("ConvertToList Exception: " + e.ToString());
                return new List<T>();
            }
        }

         */
         
        /*Transaction Use - IDBTransaction
            string SQL1 = "INSERT INTO tbl_cust(cust_id,cust_name) values ('000001','YoungMcD') ";
            string SQL2 = "UPDATE tbl_cust SET custname='OldMcDonald' WHERE cust_id='000001'";
            string SQL3 = "SELECT * FROM tbl_supplier WHERE supplier_code ='000001'";

            // write connstring
            string conn = System.Configuration.ConfigurationManager.ConnectionStrings["connstr"].ConnectionString;
            // end of connection string

            // setting connection
            SqlConnection db = new SqlConnection(conn);
            SqlTransaction transaction1;

            db.Open();
            transaction1 = db.BeginTransaction();

            try
            {
                // insert to table
                SqlCommand Com1 = new SqlCommand(SQL1, db, transaction1);
                Com1.ExecuteNonQuery();

                SqlCommand Com2 = new SqlCommand(SQL2, db, transaction1);
                Com2.ExecuteNonQuery();

                SqlCommand Com3 = new SqlCommand(SQL3, db, transaction1);
                Com3.ExecuteNonQuery();

                transaction1.Commit();

                db.Close();
            }
            catch
            {
                transaction1.Rollback();
                db.Close();
                msg = "error";
                goto endret;
            }

         */

        /* TransactionScope
         *
         // This function takes arguments for 2 connection strings and commands to create a transaction 
// involving two SQL Servers. It returns a value > 0 if the transaction is committed, 0 if the 
// transaction is rolled back. To test this code, you can connect to two different databases 
// on the same server by altering the connection string, or to another 3rd party RDBMS by 
// altering the code in the connection2 code block.
static public int CreateTransactionScope(
    string connectString1, string connectString2,
    string commandText1, string commandText2)
{
    // Initialize the return value to zero and create a StringWriter to display results.
    int returnValue = 0;
    System.IO.StringWriter writer = new System.IO.StringWriter();

    try
    {
        // Create the TransactionScope to execute the commands, guaranteeing
        // that both commands can commit or roll back as a single unit of work.
        using (TransactionScope scope = new TransactionScope())
        {
            using (SqlConnection connection1 = new SqlConnection(connectString1))
            {
                // Opening the connection automatically enlists it in the 
                // TransactionScope as a lightweight transaction.
                connection1.Open();

                // Create the SqlCommand object and execute the first command.
                SqlCommand command1 = new SqlCommand(commandText1, connection1);
                returnValue = command1.ExecuteNonQuery();
                writer.WriteLine("Rows to be affected by command1: {0}", returnValue);

                // If you get here, this means that command1 succeeded. By nesting
                // the using block for connection2 inside that of connection1, you
                // conserve server and network resources as connection2 is opened
                // only when there is a chance that the transaction can commit.   
                using (SqlConnection connection2 = new SqlConnection(connectString2))
                {
                    // The transaction is escalated to a full distributed
                    // transaction when connection2 is opened.
                    connection2.Open();

                    // Execute the second command in the second database.
                    returnValue = 0;
                    SqlCommand command2 = new SqlCommand(commandText2, connection2);
                    returnValue = command2.ExecuteNonQuery();
                    writer.WriteLine("Rows to be affected by command2: {0}", returnValue);
                }
            }

            // The Complete method commits the transaction. If an exception has been thrown,
            // Complete is not  called and the transaction is rolled back.
            scope.Complete();
        }
    }
    catch (TransactionAbortedException ex)
    {
        writer.WriteLine("TransactionAbortedException Message: {0}", ex.Message);
    }

    // Display messages.
    Console.WriteLine(writer.ToString());

    return returnValue;
}


        using (TransactionScope distibutedTransaction = new TransactionScope())

   2:  {

   3:      using (SqlConnection MSSqlConnection = new SqlConnection("ConnectionString1"))
   4:      {

   5:          SqlCommand command = new SqlCommand();

   6:          command.CommandText = "INSERT INTO A VALUES (1, 'abc');";

   7:          command.Connection = MSSqlConnection;
   
   9:          command.Connection.Open();

  10:          command.ExecuteNonQuery();

  11:      }
  12:   

  13:      using (OracleConnection oracleConnection = new OracleConnection("ConnectionString2"))

  14:      {

  15:          OracleCommand command = new OracleCommand();

  16:          command.CommandText = "INSERT INTO B VALUES (1, 2, 3);";

  17:          command.Connection = oracleConnection;
  
  19:          command.Connection.Open();

  20:          command.ExecuteNonQuery();

  21:      }
  22:   

  23:      distibutedTransaction.Complete();

  24:  }

         
         */

        /* DataTable / DataAdapter / DataReaderr
           
            public DataTable Read1(string query)
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Connection.Open();
                    var table = new DataTable();
                    using (var r = cmd.ExecuteReader())
                        table.Load(r);
                    return table;
                }
            }

            public DataTable Read2<S>(string query) where S : IDbDataAdapter, IDisposable, new()
            {
                using (var da = new S())
                {
                    using (da.SelectCommand = conn.CreateCommand())
                    {
                        da.SelectCommand.CommandText = query;
                        da.SelectCommand.Connection.Open();
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds.Tables[0];
                    }
                }
            }

            public IEnumerable<S> Read3<S>(string query, Func<IDataRecord, S> selector)
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Connection.Open();
                    using (var r = cmd.ExecuteReader())
                        while (r.Read())
                            yield return selector(r);
                }
            }


            Stopwatch sw = Stopwatch.StartNew();
            for (int i = 0; i < 100; i++)
            {
                Read1(query); // ~8900 - 9200ms

                Read1(query).Rows.Cast<DataRow>().Select(selector).ToArray(); // ~9000 - 9400ms

                Read2<MySqlDataAdapter>(query); // ~1750 - 2000ms

                Read2<MySqlDataAdapter>(query).Rows.Cast<DataRow>().Select(selector).ToArray(); // ~1850 - 2000ms

                Read3(query, selector).ToArray(); // ~1550 - 1750ms

                Read4(query, selector); // ~1550 - 1700ms

                Read5(query, selector); // ~1550 - 1650ms
            }

            sw.Stop();
            MessageBox.Show(sw.Elapsed.TotalMilliseconds.ToString());

         */

        /* Obtaining BLOB Values from a Database
          https://docs.microsoft.com/en-us/previous-versions/dotnet/netframework-1.1/87z0hy49(v=vs.71)?redirectedfrom=MSDN#Y132
        
            SqlConnection pubsConn = new SqlConnection("Data Source=localhost;Integrated Security=SSPI;Initial Catalog=pubs;");
            SqlCommand logoCMD = new SqlCommand("SELECT pub_id, logo FROM pub_info", pubsConn);

            FileStream fs;                          // Writes the BLOB to a file (*.bmp).
            BinaryWriter bw;                        // Streams the BLOB to the FileStream object.

            int bufferSize = 100;                   // Size of the BLOB buffer.
            byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
            long retval;                            // The bytes returned from GetBytes.
            long startIndex = 0;                    // The starting position in the BLOB output.

            string pub_id = "";                     // The publisher id to use in the file name.

            // Open the connection and read data into the DataReader.
            pubsConn.Open();
            SqlDataReader myReader = logoCMD.ExecuteReader(CommandBehavior.SequentialAccess);

            while (myReader.Read())
            {
              // Get the publisher id, which must occur before getting the logo.
              pub_id = myReader.GetString(0);  

              // Create a file to hold the output.
              fs = new FileStream("logo" + pub_id + ".bmp", FileMode.OpenOrCreate, FileAccess.Write);
              bw = new BinaryWriter(fs);

              // Reset the starting byte for the new BLOB.
              startIndex = 0;

              // Read the bytes into outbyte[] and retain the number of bytes returned.
              retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);

              // Continue reading and writing while there are bytes beyond the size of the buffer.
              while (retval == bufferSize)
              {
                bw.Write(outbyte);
                bw.Flush();

                // Reposition the start index to the end of the last buffer and fill the buffer.
                startIndex += bufferSize;
                retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);
              }

              // Write the remaining buffer.
              bw.Write(outbyte, 0, (int)retval - 1);
              bw.Flush();

              // Close the output file.
              bw.Close();
              fs.Close();
            }

            // Close the reader and the connection.
            myReader.Close();
            pubsConn.Close();

         */

        /*  Getting binary data using SqlDataReader
         SqlConnection pubsConn = new SqlConnection("Data Source=localhost;Integrated Security=SSPI;Initial Catalog=pubs;");
    SqlCommand logoCMD = new SqlCommand("SELECT pub_id, logo FROM pub_info", pubsConn);

    FileStream fs;                          // Writes the BLOB to a file (*.bmp).
    BinaryWriter bw;                        // Streams the BLOB to the FileStream object.

    int bufferSize = 100;                   // Size of the BLOB buffer.
    byte[] outbyte = new byte[bufferSize];  // The BLOB byte[] buffer to be filled by GetBytes.
    long retval;                            // The bytes returned from GetBytes.
    long startIndex = 0;                    // The starting position in the BLOB output.

    string pub_id = "";                     // The publisher id to use in the file name.

    // Open the connection and read data into the DataReader.
    pubsConn.Open();
    SqlDataReader myReader = logoCMD.ExecuteReader(CommandBehavior.SequentialAccess);

    while (myReader.Read())
    {
      // Get the publisher id, which must occur before getting the logo.
      pub_id = myReader.GetString(0);  

      // Create a file to hold the output.
      fs = new FileStream("logo" + pub_id + ".bmp", FileMode.OpenOrCreate, FileAccess.Write);
      bw = new BinaryWriter(fs);

      // Reset the starting byte for the new BLOB.
      startIndex = 0;

      // Read the bytes into outbyte[] and retain the number of bytes returned.
      retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);

      // Continue reading and writing while there are bytes beyond the size of the buffer.
      while (retval == bufferSize)
      {
        bw.Write(outbyte);
        bw.Flush();

        // Reposition the start index to the end of the last buffer and fill the buffer.
        startIndex += bufferSize;
        retval = myReader.GetBytes(1, startIndex, outbyte, 0, bufferSize);
      }

      // Write the remaining buffer.
      if(retval > 0) // if file size can divide to buffer size
          bw.Write(outbyte, 0, (int)retval); //original MSDN source had retval-1, a bug
      bw.Flush();

      // Close the output file.
      bw.Close();
      fs.Close();
    }

    // Close the reader and the connection.
    myReader.Close();
    pubsConn.Close();

         */


    }
}
