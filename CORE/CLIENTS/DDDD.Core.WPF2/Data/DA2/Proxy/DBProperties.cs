﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace  DDDD.Core.Data.DA2 
{
    public class DBProperties
    {
        static readonly string CName = nameof(DBProperties);


        internal protected DBProperties(string databaseName
            , string productName
            , string productLevel
            , string serverEdition
            , string parameterFormat
            , Version version
            , DBSchemaTypes supports
            , bool caseSensitive
            , DBParameterLayout layout
            , System.Data.DbType[] supportedDbTypes
            , TopType[] supportedTopTypes)
        {
            
            Set<Version>(Ks.ProductVersion, version);
            Set<string>(Ks.ServerEdition, serverEdition);
            Set<string>(Ks.ProductLevel, productLevel);
            Set<string>(Ks.ProductName, productName);
            Set<string>(Ks.ParameterFormat, parameterFormat);
            Set<DBSchemaTypes>(Ks.SupportedSchemas, supports);
            Set<bool>(Ks.CaseSensitiveNames, caseSensitive);
            Set<DBParameterLayout>(Ks.ParameterLayout, layout);
            Set<DbType[]>(Ks.SupportedDbTypes, supportedDbTypes);
            Set<TopType[]>(Ks.SupportedTopTypes, supportedTopTypes);            
            
        }



        /// <summary>
        /// Keys for Properties values
        /// </summary>
        public class Ks
        {
            /// <summary>
            /// Key for  -  string  ProductName parameter value
            ///  <para/> Gets the name of the database engine product
            /// </summary>
            public const string ProductName = nameof(ProductName);

            /// <summary>
            /// Key for  - DBSchemaTypes SupportedSchemas parameter value
            /// <para/> Gets the supported schema types for this database - Tables, Views, Procedures, Functions etc...
            /// </summary>
            public const string SupportedSchemas = nameof(SupportedSchemas);

            /// <summary>
            /// Key for  -  string ServerEdition parameter value
            /// <para/>
            /// </summary>
            public const string ServerEdition = nameof(ServerEdition);

            /// <summary>
            /// Key for  -  string ProductLevel  parameter value
            /// <para/> 
            /// </summary>
            public const string ProductLevel = nameof(ProductLevel);

            /// <summary>
            /// Key for  -  Version ProductVersion  parameter value
            /// <para/>
            /// </summary>
            public const string  ProductVersion = nameof(ProductVersion);

            /// <summary>
            /// Key for  -  string ParameterFormat  parameter value
            /// <para/>
            /// </summary>
            public const string ParameterFormat = nameof(ParameterFormat);

            /// <summary>
            /// Key for  - DBParameterLayout ParameterLayout   parameter value
            /// <para/>
            /// </summary>
            public const string ParameterLayout = nameof(ParameterLayout);

            /// <summary>
            /// Key for  -  bool CaseSensitiveNames  parameter value
            /// <para/>
            /// </summary>
            public const string CaseSensitiveNames = nameof(CaseSensitiveNames);

            /// <summary>
            /// Key for  -  System.Data.DbType[] SupportedDbTypes   parameter value
            /// <para/>
            /// </summary>
            public const string SupportedDbTypes = nameof(SupportedDbTypes);

            /// <summary>
            /// Key for  -  TopType[] SupportedTopTypes     parameter value
            /// <para/>
            /// </summary>
            public const string  SupportedTopTypes = nameof(SupportedTopTypes);

        }


        /// <summary>
        /// Get Property value with T Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propKey"></param>
        /// <returns></returns>
        public T Get<T>(string propKey)
        {
            T rest = default(T);
            if (Properties.ContainsKey(propKey) == false)
                return rest;

            rest = (T)Properties[propKey];            
            return rest;
        }


        /// <summary>
        /// Set Property value with T Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propKey"></param>
        /// <param name="value"></param>
        public void Set<T>(string propKey, T value)
        {           
            if (Properties.ContainsKey(propKey) == true)
                Properties[propKey] =  value;
            else
                Properties.Add(propKey, value);            
        }
        

        /// <summary>
        /// All DataBase Parameter values in One dictionary
        /// </summary>
        public Dictionary<string, object> Properties
        { get; } = new Dictionary<string, object>();
        
        /// <summary>
        /// Empty unknown database properties.
        /// </summary>
        public static readonly DBProperties Unknown 
            = new DBProperties( "Unknown"
                              , "Unknown"
                              , "Unknown"
                              , "Unknown"
                              , "{0}"
                              , new Version(0, 0)
                              , DBSchemaTypes.UnKnown
                              , true
                              , DBParameterLayout.Named
                              , null
                              , null);
               

        #region public override string ToString() + 1 overload

        /// <summary>
        /// Builds a summary string of the properties of this database
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(CName + "{" );
            this.ToString(sb);
            sb.Append("}");
            return sb.ToString();
        }

        /// <summary>
        /// Appends key properties and their values to the StringBuilder. 
        /// Inheritors can override this method to append other properties
        /// </summary>
        /// <param name="sb"></param>
        protected virtual void ToString(StringBuilder sb)
        {
            sb.Append($"{product}:");
            sb.Append(Ks.ProductName);
            sb.Append($", {level}:");
            sb.Append(Ks.ProductLevel);
            sb.Append($", {edition}:");
            sb.Append(Ks.ServerEdition);
            sb.Append($", {version}:");
            sb.Append(Ks.ProductVersion);
        }


        const string edition = nameof(edition);
        const string version = nameof(version);
        const string product = nameof(product);
        const string level = nameof(level);
        #endregion


        #region -----------  Check Supports DBSchemaTypes/DbType/TopType ------------


        /// <summary>
        /// Returns true if the Database Provider supports the required schema type
        /// </summary>
        /// <param name="schema">The schema type to check (can be multiple)</param>
        /// <returns>True if the schema type is supported</returns>
        public bool CheckSupports(DBSchemaTypes schema)
        {
            return (Get<DBSchemaTypes>(Ks.SupportedSchemas) & schema) > 0;
        }

        /// <summary>
        /// Returns true if the database provider supports the required data type
        /// </summary>
        /// <param name="type">The data type to check</param>
        /// <returns>True if it is supported</returns>
        public bool CheckSupportsType(DbType type)
        {
            return Array.IndexOf<DbType>(Get<DbType[]>(Ks.SupportedDbTypes), type) > -1;
        }



        /// <summary>
        /// Returns true if the database provider supports the required Top type
        /// <param name="topType"></param>
        /// <returns></returns>
        public bool CheckSupportsTopType(TopType topType)
        {
            return Array.IndexOf<TopType>(Get<TopType[]>(Ks.SupportedTopTypes), topType) > -1;
        }

        #endregion -----------  Check Supports DBSchemaTypes/DbType/TopType ------------



    }
}