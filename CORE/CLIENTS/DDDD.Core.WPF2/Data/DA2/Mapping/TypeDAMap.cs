﻿
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

using DDDD.Core.Collections.Concurrent;
using DDDD.Core.Diagnostics;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using System.Collections.ObjectModel;

namespace DDDD.Core.Data.DA2.Mapping
{

    /// <summary>
    /// Set Value to DA contract Property:
    /// get value from IDataReader and set by ITypeAccessor
    /// </summary>
    /// <param name="accessor"></param>
    /// <param name="targetItem"></param>
    /// <param name="member"></param>
    /// <param name="ordinal"></param>
    /// <param name="reader"></param>
    public delegate void DASetMemberValueDelegate(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader);

    /// <summary>
    /// Set Primitive Type variable value directly by reader.
    /// </summary>
    /// <param name="targetItem"></param>
    /// <param name="member"></param>
    /// <param name="ordinal"></param>
    /// <param name="reader"></param>
    public delegate void DASetPrimValueDelegate(ref object targetItem, int ordinal, IDataReader reader);


    /// <summary>
    /// As Analyser - analyse Type  Data Access  Mapping attributes. 
    /// <para/> As Informer - aggregate attrinbutes Info.
    /// <para/> As Mapper - contains functors per  [Provider.property]
    ///  per different Providers
    ///  <para/> Contains cache of  TypeDAMap -ed Types 
    /// </summary>
    public sealed class TypeDAMap
    {

        const string ClsName = nameof(TypeDAMap);

        static readonly object syncRoot = new object();


        #region ------------- CTOR -----------------
        TypeDAMap() { }

        TypeDAMap(Type primType, DASetPrimValueDelegate primSetterFunc)
        {
            TargetTypeEx = primType.GetTypeInfoEx();
            IsPrimitive = true;
            PrimSetterFunc = primSetterFunc;
        }
        #endregion ------------- CTOR -----------------

        #region --------- STATIC MEMBERS -----------------


        //Boolean  | Byte    |  Char |  DateTime  
        //Decimal  | Double  | Float |  Guid 
        //Int16    | Int32   | Int64  

        //Bytes-byte[] | chars -char[] | string

        //Boolean?  | Byte?   |  Char? |  DateTime?   
        //Decimal?  | Double? | Float? | Guid? 
        //Int16?    | Int32?  | Int64? 

        static Dictionary<Type, TypeDAMap> CreatePrimitives()
        {
            var primtvs = new Dictionary<Type, TypeDAMap>();

            primtvs.Add(Tps.T_bool, new TypeDAMap(Tps.T_bool, SetBoolean));
            primtvs.Add(Tps.T_byte, new TypeDAMap(Tps.T_byte, SetByte));
            primtvs.Add(Tps.T_char, new TypeDAMap(Tps.T_char, SetChar));
            primtvs.Add(Tps.T_DateTime, new TypeDAMap(Tps.T_DateTime, SetDateTime));
            primtvs.Add(Tps.T_decimal, new TypeDAMap(Tps.T_decimal, SetDecimal));
            primtvs.Add(Tps.T_Double, new TypeDAMap(Tps.T_Double, SetDouble));
            primtvs.Add(Tps.T_float, new TypeDAMap(Tps.T_float, SetFloat));
            primtvs.Add(Tps.T_Guid, new TypeDAMap(Tps.T_Guid, SetGuid));
            primtvs.Add(Tps.T_Int16, new TypeDAMap(Tps.T_Int16, SetInt16));
            primtvs.Add(Tps.T_Int32, new TypeDAMap(Tps.T_Int32, SetInt32));
            primtvs.Add(Tps.T_Int64, new TypeDAMap(Tps.T_Int64, SetInt64));

            primtvs.Add(Tps.T_byteArray, new TypeDAMap(Tps.T_byteArray, SetBytes));
            primtvs.Add(Tps.T_charArray, new TypeDAMap(Tps.T_charArray, SetChars));
            primtvs.Add(Tps.T_string, new TypeDAMap(Tps.T_string, SetString));

            primtvs.Add(Tps.T_boolNul, new TypeDAMap(Tps.T_boolNul, SetBoolean_Null));
            primtvs.Add(Tps.T_byteNul, new TypeDAMap(Tps.T_byteNul, SetByte_Null));
            primtvs.Add(Tps.T_charNul, new TypeDAMap(Tps.T_charNul, SetChar_Null));
            primtvs.Add(Tps.T_DateTimeNul, new TypeDAMap(Tps.T_DateTimeNul, SetDateTime_Null));
            primtvs.Add(Tps.T_decimalNul, new TypeDAMap(Tps.T_decimalNul, SetDecimal_Null));
            primtvs.Add(Tps.T_DoubleNul, new TypeDAMap(Tps.T_DoubleNul, SetDouble_Null));
            primtvs.Add(Tps.T_floatNul, new TypeDAMap(Tps.T_floatNul, SetFloat_Null));
            primtvs.Add(Tps.T_GuidNul, new TypeDAMap(Tps.T_GuidNul, SetGuid_Null));
            primtvs.Add(Tps.T_Int16Nul, new TypeDAMap(Tps.T_Int16Nul, SetInt64_Null));
            primtvs.Add(Tps.T_Int32Nul, new TypeDAMap(Tps.T_Int32Nul, SetInt32_Null));
            primtvs.Add(Tps.T_Int64Nul, new TypeDAMap(Tps.T_Int64Nul, SetInt64_Null));

            return primtvs;

        }







        /// <summary>
        /// BO Properties with SQLColumn attribute. 
        /// </summary>
        public List<PropertyInfo> DomainProperties
        { get; private set; } = new List<PropertyInfo>();

        //public List<string> DomainPropertyNamesNoPK
        //{ get; private set; } = new List<string>();




        /// <summary>
        /// TypeDAMap objects of Primitives Types - all IDataReader.Set[xType-primitive]s and their Nullable Types too.
        /// <para/> In such TypeDAMap object setter func only one  - PrimSetterFunc.
        /// <para/> Such TypeDAMap object is used when IDataReader Set[xType-primitive] to the same type variable
        /// , not to the contract member value
        /// </summary>
        internal static readonly Dictionary<Type, TypeDAMap> Primitives
             = CreatePrimitives();



        #endregion --------- STATIC MEMBERS -----------------


        #region ------------ Properties -------------------
        /// <summary>
        /// Is TargetType is Primitive/ not Contract with Members
        /// </summary>
        public bool IsPrimitive
        { get; private set; }


        /// <summary>
        /// Provider that used on TypeDAMap creation 
        /// </summary>
        public string MapProvider
        { get; private set; }


        /// <summary>
        /// Primitive  Type Setter Func- when mapper created for Primitive- IsPrimitive : true
        /// </summary>
        public DASetPrimValueDelegate PrimSetterFunc
        { get; private set; }


        /// <summary>
        /// TargetType for TypeMapper operations
        /// </summary>
        public Type TargetType
        {
            get { return TargetTypeEx.OriginalType; }
        }

        /// <summary>
        /// TypeInfoEx about TargetType
        /// </summary>
        public TypeInfoEx TargetTypeEx
        { get; private set; }

        /// <summary>
        /// Type Accessor - is used to set property values of TargetType
        /// </summary>
        public ITypeAccessor Accessor
        { get; private set; }

        /// <summary>
        ///  Default Ctor Func of TargetType
        /// </summary>
        Func<object> DefCtorFunc
        { get; set; }

        /// <summary>
        ///  Create BObject new Instance -  [TargetType] instance.
        /// </summary>
        /// <returns></returns>
        public object CreateObject()
        {
            return DefCtorFunc();
        }


        /// <summary>
        ///  Cache of TypeMappers
        /// </summary>
        static readonly SafeDictionary2<int, TypeDAMap> TypeMappersCache =
            new SafeDictionary2<int, TypeDAMap>();

        /// <summary>
        /// Mapping  Analyser
        /// </summary>
        public Analyser MapAnalyzer
        { get; private set; }




        /// <summary>
        /// Target Mapper Mapping info by each Analyse Points
        /// </summary>
        Dictionary<string, object> MapInfo
        { get; set; } = new Dictionary<string, object>();





        /// <summary>
        /// Standart property  - where PropType can be read throw the default IDataReader interface
        /// <para/> All keys of mapping Info are in [TypeDAMap.Aks.P_cccc ].
        /// All keys contains summary info about its result type and if it always exist in  BObject.MapInfo dictionary.
        /// </summary>
        public List<string> StandartTypeMembers
        { get; private set; } = new List<string>();


        /// <summary>
        ///  Specific property - where PropType can be read only through the specific provider's IDataReader interface
        /// </summary>
        public List<string> SpecTypeMembers
        { get; private set; } = new List<string>();

        internal Dictionary<string, DASetMemberValueDelegate> MemberSetters
        { get; } = new Dictionary<string, DASetMemberValueDelegate>();


        #endregion ----------- Properties -------------------


        /// <summary>
        /// Get  TypeDAMap object
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        public static TypeDAMap GetInit(Type targetType, string providerKey, bool useThreadSafetyWay = true)
        {
            var IDKey = Tps.HashCode(targetType.FullName);

            //check for already exist  
            TypeDAMap mapper = null;

            //Check return Primitive Type Mapper
            if (Primitives.ContainsKey(targetType))
                return Primitives[targetType];

            if (TypeMappersCache.TryGetValue(IDKey, out mapper) == false)
            {
                OperationInvoke.CallWithLockAndModes(ClsName, nameof(GetInit), useThreadSafetyWay
                    , syncRoot
                    , () => TypeMappersCache.NotContainsKey(IDKey) // lock use check  Expression
                    , (Action)(() =>
                    {

                        mapper = new TypeDAMap();
                        // Extended TypeInfo about TargetType
                        mapper.TargetTypeEx = targetType.GetTypeInfoEx();
                        // Map Provider
                        mapper.MapProvider = providerKey;

                        // Get Accessor - only TargetType Members without base class Members
                        mapper.Accessor = mapper.TargetTypeEx.GetAccessor(TypeMemberSelectorEn.DefaultNoBase);

                        // Get DafultCtor Func
                        mapper.DefCtorFunc = TypeActivator.TryGetDefCtorFunc(targetType);

                        // Collect Attributes 
                        // Analyse Map Attributes
                        mapper.MapAnalyzer = new Analyser(mapper);
                        mapper.MapAnalyzer.AnalyseType();

                        // Collect provider specific Types
                        FilterProvSpecMembers(mapper, providerKey);

                        //if  side support  set SETTERS FUNC - collect it
                        // Set Settter Funcs for Members
                        SetSettersFuncs(mapper, providerKey);

                        //Save to cache
                        TypeMappersCache.Add(IDKey, mapper);
                    })
                    //, exceptionAction, error detail message
                    );

            }
            // if that is  new Provider ? 
            if (mapper.MapInfo.NotContainsKey(providerKey))
            {
                //SET MAPPING FOR THIS PROVIDER BECAUSE IT WA NOT FOUND
                OperationInvoke.CallWithLockAndModes(ClsName, nameof(GetInit), useThreadSafetyWay
                   , syncRoot
                   , () => mapper.MapInfo.NotContainsKey(providerKey) // lock use check  Expression
                   , (Action)(() =>
                   {
                       // all attributes already collected and analysed
                       //filter here only
                       FilterProvSpecMembers(mapper, providerKey);

                       // Set Settter Funcs for Members
                       SetSettersFuncs(mapper, providerKey);
                   })
                   //, exceptionAction, error detail message
                   );
                return mapper;
            }
            else return mapper; // already mapped for this provider

            //return TypeMappersCache[IDKey];
        }

        /// <summary>
        /// Get Existed TypeDAMap or return null. 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static TypeDAMap GetExisted(Type targetType)
        {
            var IDKey = Tps.HashCode(targetType.FullName);

            //check for already exist  
            TypeDAMap mapper = null;

            TypeMappersCache.TryGetValue(IDKey, out mapper);
            return mapper;
        }

        private static void FilterProvSpecMembers(TypeDAMap mapper, string providerKey)
        {
            var provider = SqlDataProvider.GetByKey(providerKey);
            for (int i = 0; i < mapper.Accessor.Properties.Count; i++)
            {
                var prop = mapper.Accessor.Properties[i];

                if (provider.IsSpecificType(prop.Value.TypeInf.OriginalType))
                {
                    if (mapper.SpecTypeMembers.Contains(prop.Key) == false)
                    {
                        mapper.SpecTypeMembers.Add(prop.Key);
                    }
                    else if (mapper.StandartTypeMembers.Contains(prop.Key) == false)
                    {
                        mapper.StandartTypeMembers.Add(prop.Key);
                    }
                }
            }
        }



        #region -------------- Analyze Contract Mapping ---------------

        /// <summary>
        /// Analising Info Point Keys-constants
        /// </summary>
        public class Aks
        {

            #region ------------ANALYSE POINT KEYS  CONSTS----------------

            //Entity Type Mapping properties: 
            //   1- TableOrView - [table] [view]            
            //   2- TableOrViewName
            //   3- DomainPrefix
            //   4- EntityName                 
            //   5- BORole -   DataTable/FactTable /  Register / V_Registry/ ValueObject / V_Navigation
            //   5- BOOptions - Navigatable / Editable ...
            //   6- VObjectMap - target Table
            //   7- ForeignKeyTables  - - Type[]
            //   8- ServiceModelName - string
            //   9- ServiceModelIndex - int               
            //   10- PrimaryKeys
            //   11- ForeignKeys
            // Member: IsNullable
            //         BOPeropertyRole



            public const string Table = "table";
            public const string View = "view";


            //  Entity Type Properties - [E_PointKey]
            /// <summary>
            /// [bool value;always exists] - Is contract mapped for Table or View
            /// </summary>
            public const string E_TableOrView = "TableOrView";
            /// <summary>
            /// [BORole value; always exists] - Get  contract BORole.
            /// </summary>
            public const string E_BORole = "BORole";

            /// <summary>
            /// [ BOOptionsEn value; always exists] - Get  contract BOOptionsEn.
            /// </summary>
            public const string E_BOOptions = "BOOptions";

            /// <summary>
            /// [Type value;may not exists] - Get target Table Type if BObject is VObject -ValueObject
            /// </summary>
            public const string E_VObjectMapTable = "VObjectMapTable";
            /// <summary>
            /// [List{Type} value;may not exists] - Get Foreign Table Types, if BObject-table/view  contains association with other Tables/Views
            /// </summary>
            public const string E_ForeignKeyTables = "ForeignKeyTables";
            /// <summary>
            /// [string value;always exists] - Get ServiceModelName of BObject contract
            /// </summary>
            public const string E_ServiceModelName = "ServiceModelName";
            /// <summary>
            /// [int value;always exists] - Get ServiceModel Index of BObject contract
            /// </summary>
            public const string E_ServiceModelIndex = "ServiceModelIndex";
            /// <summary>
            /// [Type value;always exists] - Get ServiceModel Type of BObject contract
            /// </summary>
            public const string E_ServiceModelType = "ServiceModelType";

            /// <summary>
            /// [string value;may not exists] - Specific model Role if BObject contract is part of specific ServiceModel
            /// </summary>
            public const string E_SpecificModelRole = "SpecificModelRole";
            /// <summary>
            /// [string value;always exists] - Name of BObject contract (Table or View)
            /// </summary>
            public const string E_TableOrViewName = "TableOrViewName";
            /// <summary>
            /// [string value;always exists] - entityName of BObject contract (Table or View)
            /// </summary>
            public const string E_EntityName = "EntityName";
            /// <summary>
            /// [string value;always exists] - edomainPrefix of BObject contract (Table or View)
            /// </summary>
            public const string E_DomainPrefix = "DomainPrefix";
            /// <summary>
            /// [List{string} value; may not exists] - list of Primary Key properties of BObject contract
            /// </summary>
            public const string E_PrimaryKeys = "PrimaryKeys";
            /// <summary>
            /// [List{string} value; may not exists] - list of Foreign Kes properties of BObject contract
            /// </summary>
            public const string E_ForeignKeys = "ForeignKeys";


            /// <summary>
            /// Keys for Entity attributes Keys 
            /// and Entity Analysed Points
            /// </summary>
            /// <param name="pointKey"></param>
            /// <returns></returns>
            public static string EntKey(string pointKey)
            {
                return $"E.{pointKey}";
            }

            //  Entity Member Properties - [P.Property.PointKey]
            /// <summary>
            /// [bool value;always exists] - BObject Property Is Nullable as table/View.field or not
            /// </summary>
            public const string P_IsNullable = "IsNullable";
            /// <summary>
            /// [bool value;always exists] - BObject Property  Role(BOPropertyRole) on some properties
            /// </summary>
            public const string P_BOPropertyRole = "BOPropertyRole";
            /// <summary>
            /// [bool value;always exists] - If BObject Property marked as SQL Column-true/ if it's not marked - false
            /// </summary>
            public const string P_IsSqlColumn = "IsSqlColumn";

            // public const string P_DefaultValue = "DefaultValue";
            // public const string P_IdentityStep = "IdentityStep";

            /// <summary>
            /// Keys for Entity Properties attributes Keys 
            /// and Entity Properties Analysed Points
            /// </summary>
            /// <param name="property"></param>
            /// <param name="pointKey"></param>
            /// <returns></returns>
            public static string PropKey(string property, string pointKey)
            {
                return $"P.{property}.{pointKey}";
            }
            #endregion ------------ANALYSE POINT KEYS ----------------

        }




        /// <summary>
        /// Analyze Point Keys for Entity and its Properties
        /// </summary>
        public class Analyser
        {
            public Analyser(TypeDAMap mapper)
            { Mapper = mapper; }

            #region ------------  Poperty MapInfo ---------------

            private class PropertyMapInfo
            {
                public PropertyMapInfo()
                {
                }
                public PropertyInfo Property { get; set; }
                public string PropertyName { get; set; }
                public bool IsNullable { get; set; }
                public PropertyRoleEn PropertyRole { get; set; }
                public bool IsSqlColumn { get; set; }

            }
            #endregion ------------  Poperty MapInfo ---------------

            /// <summary>
            /// Analyser  Mapper  
            /// </summary>
            internal TypeDAMap Mapper { get; private set; }

            /// <summary>
            /// temporary collection of Mapping Attributes for TsargetType and its Properties.
            /// <para/> Type attribute key - [Type#AttribName]; 
            /// <para/> Type member attribute key - [member#AttribName]
            /// </summary>
            [ThreadStatic]
            static Dictionary<string, Attribute> MapAttribs
            = new Dictionary<string, Attribute>();


            #region --------------- Collect Attributes from Type/members ---------------

            static void CollectAttributes(TypeDAMap mapper)
            {
                // Get Contract Attributes
                var typeAttribs = mapper.TargetType.GetCustomAttributes(false);
                for (int i = 0; i < typeAttribs.Length; i++)
                {
                    WriteTypeAtribToMapper(mapper, typeAttribs[i] as Attribute);
                }

                // Collect Properties Attributes
                for (int i = 0; i < mapper.Accessor.Properties.Count; i++)
                {
                    var memberAttribs = mapper.Accessor.Properties[i].Value.
                        MemberInf.GetCustomAttributes(false);
                    if (memberAttribs == null && memberAttribs.Length == 0) continue;

                    //write to Map Info Dictionary
                    for (int j = 0; j < memberAttribs.Length; j++)
                    {
                        WriteMemberAtribToMapper(mapper, memberAttribs[j] as Attribute
                                                , mapper.Accessor.Properties[i].Key);

                    }
                }
            }

            static void WriteMemberAtribToMapper(TypeDAMap mapper, Attribute attrib, string member)
            {
                var writeKey = Aks.PropKey(member, attrib.GetType().Name);
                MapAttribs.Add(writeKey, attrib);
            }

            static void WriteTypeAtribToMapper(TypeDAMap mapper, Attribute attrib)
            {
                var writeKey = Aks.EntKey(attrib.GetType().Name);
                MapAttribs.Add(writeKey, attrib);
            }

            #endregion --------------- Collect Attributes from Type/members ---------------

            public void AnalyseType()
            {
                //atributes into temporsary dictionary
                CollectAttributes(Mapper);

                AnalyseMappingAttributes(Mapper);

                //Clear static attributes
                MapAttribs.Clear();
            }

            static void AnalyseMappingAttributes(TypeDAMap mapper)
            {
                //Entity Type Mapping properties: 
                //   1- TableOrView - [table] [view]            
                //   2- TableOrViewName
                //   3- DomainPrefix
                //   4- EntityName                 
                //   5- BORole - DataTable/FactTable /  Register / V_Registry/ ValueObject / V_Navigation
                //   6- BOOptions - Editable Navigable...
                //   7- VObjectMap - target Table
                //   8- ForeignKeyTables  - - Type[]
                //   9- ServiceModelName - string
                //   10- ServiceModelIndex - int               
                //   11- PrimaryKeys
                //   12- ForeignKeys
                // Member: IsNullable
                //         BOPeropertyRole
                //         IsSqlColumn  


                //   1- TableOrView - [table] [view] -   always should contains                               
                var attribKey1 = Aks.EntKey(nameof(TableNameAttribute));
                var attrib1 = MapAttribs[attribKey1] as TableNameAttribute;
                var boNameInfo = NamingConvention.GetInfoByBOName(attrib1.Name);

                // 1 Table or View
                var pointKey1 = Aks.EntKey(Aks.E_TableOrView);
                mapper.MapInfo.Add(pointKey1, boNameInfo.TableOrView);

                //   2- TableOrViewName
                var pointKey2 = Aks.EntKey(Aks.E_TableOrViewName);
                mapper.MapInfo.Add(pointKey2, boNameInfo.ContractName);

                //   3- DomainPrefix
                var pointKey3 = Aks.EntKey(Aks.E_DomainPrefix);
                mapper.MapInfo.Add(pointKey3, boNameInfo.DomainPrefix);
                //if (boNameInfo.IsTable)    {}

                //   4- EntityName 
                var pointKey4 = Aks.EntKey(Aks.E_EntityName);
                mapper.MapInfo.Add(pointKey4, boNameInfo.EntityName);

                //   5- BORole -  T_DataTable/T_FactTable | T_Register | V_Register |V_ValueObject | V_Navigation
                AnalyzeEnt_BORole(mapper);

                // 6 BOOptions  E_BOOptions
                AnalyzeEnt_BOOptions(mapper);

                //   7- VObjectMap Table
                AnalyzeEnt_VObjectMap(mapper);

                //   8- ForeignKeyTables - Type[]
                AnalyzeEnt_ForeignKeyTables(mapper);

                //   9- ServiceModelName
                AnalyzeEnt_ServiceModelName(mapper);

                //   10- ServiceModelIndex
                AnalyzeEnt_ServiceModelIndex(mapper);

                // 10.1 Service model Type
                AnalyzeEnt_ServiceModelType(mapper);

                //analyse members-Properties, fill Mapinfo nad save-return to PropertyMapInfo 
                //       - IsNullable -
                //       - BOPeropertyRole - PrimaryKey/ ForeignKey                
                //       - IsSQLColumn
                var proptyMaps = new List<PropertyMapInfo>();
                for (int i = 0; i < mapper.Accessor.Properties.Count; i++)
                {
                    proptyMaps.Add(
                     AnalyzePropertyMapping(mapper.Accessor.Properties[i], mapper));
                }


                //collect Domain Properties                
                for (int i = 0; i < proptyMaps.Count; i++)
                {
                    if (proptyMaps[i].IsSqlColumn)
                    {
                        mapper.DomainProperties.Add(proptyMaps[i].Property);
                    }
                }


                //collect DomainPropertyNamesNoPK
                //for (int i = 0; i < proptyMaps.Count; i++)
                //{
                //    if (proptyMaps[i].PropertyRole != PropertyRoleEn.PKeyField)                    
                //    {   mapper.DomainPropertyNamesNoPK.Add(proptyMaps[i].PropertyName);
                //    }
                //}


                //   11- PrimaryKeys - always
                //   12- ForeignKeys - always
                var PKeys = new List<string>(); // can be enpty - for View for Example
                var FKeys = new List<string>(); // can be enpty - for View for Example                
                for (int i = 0; i < proptyMaps.Count; i++)
                {
                    if (proptyMaps[i].PropertyRole == PropertyRoleEn.PKeyField)
                    { PKeys.Add(proptyMaps[i].PropertyName); }
                    else if (proptyMaps[i].PropertyRole == PropertyRoleEn.FKeyField)
                    { FKeys.Add(proptyMaps[i].PropertyName); }
                }
                //   11- PrimaryKeys - always
                var pointKey10 = Aks.EntKey(Aks.E_PrimaryKeys);
                mapper.MapInfo.Add(pointKey10, PKeys);
                //   12- ForeignKeys - always
                var pointKey11 = Aks.EntKey(Aks.E_ForeignKeys);
                mapper.MapInfo.Add(pointKey11, FKeys);
            }

            private static void AnalyzeEnt_ServiceModelType(TypeDAMap mapper)
            {
                // always exists
                var attribKey = Aks.EntKey(nameof(ServiceModelAttribute));
                var pointKey = Aks.EntKey(Aks.E_ServiceModelType);
                var attrib = MapAttribs[attribKey] as ServiceModelAttribute;
                mapper.MapInfo.Add(pointKey, attrib.ServiceModelType);
            }

            private static void AnalyzeEnt_BORole(TypeDAMap mapper)
            {    // always exists
                var attribKey = Aks.EntKey(nameof(BORolesAttribute));
                var pointKey = Aks.EntKey(Aks.E_BORole);
                if (MapAttribs.ContainsKey(attribKey))
                {
                    var attrib = MapAttribs[attribKey] as BORolesAttribute;
                    mapper.MapInfo.Add(pointKey, attrib.ObjectRole);
                }
                else
                {
                    mapper.MapInfo.Add(pointKey, BORoleEn.NotDefined);
                }
            }

            private static PropertyMapInfo AnalyzePropertyMapping(KeyValuePair<string, TypeMember> property, TypeDAMap mapper)
            {
                // - IsNullable    - always exists
                // - BOPeropertyRole - PrimaryKey/ ForeignKey
                // - IsSQLColumn   - always exists

                var reslt = new PropertyMapInfo();
                var propName = property.Key;
                reslt.PropertyName = property.Key;
                reslt.Property = property.Value.MemberInf as PropertyInfo;

                // IsNullable
                var attribP1Key = Aks.PropKey(propName, nameof(NullableAttribute));
                var pointP1Key = Aks.PropKey(propName, Aks.P_IsNullable);
                if (MapAttribs.ContainsKey(attribP1Key))
                {
                    mapper.MapInfo.Add(pointP1Key, true);
                    reslt.IsNullable = true;
                }
                else
                {
                    mapper.MapInfo.Add(pointP1Key, false);
                    reslt.IsNullable = false;
                }

                // BOPeropertyRole
                var attribP2Key = Aks.PropKey(propName, nameof(BOPropertyRolesAttribute));
                var pointP2Key = Aks.PropKey(propName, Aks.P_BOPropertyRole);
                if (MapAttribs.ContainsKey(attribP2Key))
                {
                    var attribP2 = MapAttribs[attribP2Key] as BOPropertyRolesAttribute;
                    mapper.MapInfo.Add(pointP2Key, attribP2.PropertyRole);
                    reslt.PropertyRole = attribP2.PropertyRole;
                }
                else
                {
                    mapper.MapInfo.Add(pointP2Key, PropertyRoleEn.NotDefined);
                    reslt.PropertyRole = PropertyRoleEn.NotDefined;
                }

                //12 IsSQLColumn Key will ALWAYS EXISTS in MapInfo
                var attribP3Key = Aks.PropKey(propName, nameof(SQLColumnAttribute));
                var point3Key = Aks.PropKey(propName, Aks.P_IsSqlColumn);
                if (MapAttribs.ContainsKey(attribP3Key))
                {
                    mapper.MapInfo.Add(point3Key, true);
                    reslt.IsSqlColumn = true;
                }
                else
                {
                    mapper.MapInfo.Add(point3Key, false);
                    reslt.IsSqlColumn = false;
                }

                return reslt;
            }

            private static void AnalyzeEnt_ServiceModelIndex(TypeDAMap mapper)
            {   // always exists
                var attribKey = Aks.EntKey(nameof(ServiceModelAttribute));
                var attrib = MapAttribs[attribKey] as ServiceModelAttribute;
                var pointKey = Aks.EntKey(Aks.E_ServiceModelIndex);
                mapper.MapInfo.Add(pointKey, attrib.ServiceIndex);
            }

            private static void AnalyzeEnt_ServiceModelName(TypeDAMap mapper)
            {   // always exists
                var attribKey = Aks.EntKey(nameof(ServiceModelAttribute));
                var attrib = MapAttribs[attribKey] as ServiceModelAttribute;
                var pointKey = Aks.EntKey(Aks.E_ServiceModelName);
                mapper.MapInfo.Add(pointKey, attrib.ServiceModel);
            }

            private static void AnalyzeEnt_ForeignKeyTables(TypeDAMap mapper)
            {   // may not exist
                var attribKey = Aks.EntKey(nameof(ForeignKeyTablesAttribute));
                if (MapAttribs.ContainsKey(attribKey))
                {// 
                    var attrib = MapAttribs[attribKey] as ForeignKeyTablesAttribute;
                    var pointKey = Aks.EntKey(Aks.E_ForeignKeyTables);
                    mapper.MapInfo.Add(pointKey, attrib.TableTypes);
                }
            }

            private static void AnalyzeEnt_VObjectMap(TypeDAMap mapper)
            {   // may not exist
                var attribKey = Aks.EntKey(nameof(VObjectMapAttribute));
                if (MapAttribs.ContainsKey(attribKey))
                {
                    var attrib = MapAttribs[attribKey] as VObjectMapAttribute;
                    var pointKey = Aks.EntKey(Aks.E_VObjectMapTable);
                    mapper.MapInfo.Add(pointKey, attrib.DALTableEntity);
                }
            }

            private static void AnalyzeEnt_BOOptions(TypeDAMap mapper)
            {  // always exists
                var attribKey = Aks.EntKey(nameof(BOOptionsAttribute));
                var pointKey = Aks.EntKey(Aks.E_BOOptions);
                if (MapAttribs.ContainsKey(attribKey))
                {
                    var attrib = MapAttribs[attribKey] as BOOptionsAttribute;
                    mapper.MapInfo.Add(pointKey, attrib.Options);
                }
                else { mapper.MapInfo.Add(pointKey, BOOptionsEn.Navigable); }
            }




        }


        #endregion -------------- Analyze Contract Mapping ---------------
        static void SetSettersFuncs(TypeDAMap mapper, string providerKey)
        {
            // provider mapped key: [provider] 
            // member for provider mapped key : [provider.member] 

            var provider = SqlDataProvider.GetByKey(providerKey);

            // set Setter Func for each property of TargetType
            for (int i = 0; i < mapper.Accessor.Properties.Count; i++)
            {
                var prop = mapper.Accessor.Properties[i];
                if (mapper.SpecTypeMembers.Contains(prop.Key) == false)
                {// STANDART READERR TYPE
                    SqlDataProvider.AddMemberSetFunc(mapper, mapper.Accessor.Properties[i], providerKey);
                }
                else  //GET SETTER for PROVIDER SPECIFIC TYPE 
                {
                    provider.AddSetMemberFunc(mapper.MemberSetters, prop.Key, prop.Value.TypeInf.OriginalType);
                }
            }

            //OK TargetType for THIS PROVIDER MAPPED -TRUE
            mapper.MapInfo.Add(providerKey, true);
        }

        //static void SelectAddSetFunc(TypeDAMap mapper, KeyValuePair<string, TypeMember> property, string providerKey)
        //{
        //    // Boolean  | Byte   |  Char | DateTime
        //    // Decimal  | Double | Float | Guid 
        //    // Int16    |  Int32 | Int64 

        //    // member for provider mapped key : [provider.member] 
        //    var memberKey = providerKey + "." + property.Key;


        //    if (mapper.TargetType == Tps.T_bool)
        //    { mapper.MemberSetters.Add(memberKey,SqlDataProvider.SetBoolean); return; }
        //    else if (mapper.TargetType == Tps.T_byte)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetByte); return; }            
        //    else if (mapper.TargetType == Tps.T_char)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetChar); return; }            
        //    else if (mapper.TargetType == Tps.T_DateTime)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDateTime); return; }
        //    else if (mapper.TargetType == Tps.T_decimal)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDecimal); return; }
        //    else if (mapper.TargetType == Tps.T_Double)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDouble); return; }
        //    else if (mapper.TargetType == Tps.T_float)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetFloat); return; }
        //    else if (mapper.TargetType == Tps.T_Guid)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetGuid); return; }
        //    else if (mapper.TargetType == Tps.T_Int16)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt16); return; }
        //    else if (mapper.TargetType == Tps.T_Int32)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt32); return; }
        //    else if (mapper.TargetType == Tps.T_Int64)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt64); return; }

        //    // - always NULLABLE ARRAYS: Bytes | Chars | String
        //    else if (mapper.TargetType == Tps.T_charArray)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetChars); return; }
        //    else if (mapper.TargetType == Tps.T_byteArray)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetBytes); return; }
        //    else if (mapper.TargetType == Tps.T_string)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetString); return; }


        //    // ------ NULLABLE SETTERS
        //    //Boolean?  | Byte?   |  Char? |  DateTime?   
        //    //Decimal?  | Double? | Float? | Guid? 
        //    //Int16?    | Int32?  | Int64? 
        //    if (mapper.TargetType == Tps.T_boolNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetBoolean_Null); return; }
        //    else if (mapper.TargetType == Tps.T_byteNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetByte_Null); return; }
        //    else if (mapper.TargetType == Tps.T_charNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetChar_Null); return; }
        //    else if (mapper.TargetType == Tps.T_DateTimeNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDateTime_Null); return; }
        //    else if (mapper.TargetType == Tps.T_decimalNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDecimal_Null); return; }
        //    else if (mapper.TargetType == Tps.T_DoubleNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetDouble_Null); return; }
        //    else if (mapper.TargetType == Tps.T_floatNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetFloat_Null); return; }
        //    else if (mapper.TargetType == Tps.T_GuidNul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetGuid_Null); return; }
        //    else if (mapper.TargetType == Tps.T_Int16Nul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt16_Null); return; }
        //    else if (mapper.TargetType == Tps.T_Int32Nul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt32_Null); return; }
        //    else if (mapper.TargetType == Tps.T_Int64Nul)
        //    { mapper.MemberSetters.Add(memberKey, SqlDataProvider.SetInt64_Null); return; }

        //}


        // ------------------------------
        //
        //   IDataReader VALUE SETTERS : 
        //        static methods - to set TargetType property value 
        // 
        //-------------------------------

        //Boolean  | Byte    |  Char |  DateTime  
        //Decimal  | Double  | Float |  Guid 
        //Int16    | Int32   | Int64  

        //Bytes-byte[] | chars -char[] | string

        //Boolean?  | Byte?   |  Char? |  DateTime?   
        //Decimal?  | Double? | Float? | Guid? 
        //Int16?    | Int32?  | Int64? 



        #region -------------  IDataReader VALUE SETTERS with  ITypeAccessor --------------------

        //Boolean  | Byte    |  Char |  DateTime  
        //Decimal  | Double  | Float |  Guid 
        //Int16    | Int32   | Int64  

        static void SetBoolean(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetBoolean(ordinal);
        }
        static void SetByte(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetByte(ordinal);
        }
        static void SetChar(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetChar(ordinal);
        }
        static void SetDateTime(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetDateTime(ordinal);
        }
        static void SetDecimal(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetDecimal(ordinal);
        }
        static void SetDouble(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetDouble(ordinal);
        }
        static void SetFloat(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetFloat(ordinal);
        }
        static void SetGuid(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetGuid(ordinal);
        }
        static void SetInt16(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetInt16(ordinal);
        }
        static void SetInt32(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetInt32(ordinal);
        }
        static void SetInt64(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetInt64(ordinal);
        }


        #endregion -------------  IDataReader VALUE SETTERS with ITypeAccessor--------------------

        #region -------- IDataReader  VALUE SETTERS for  Char/Byte Arrays/string -always nullable  with  ITypeAccessor-----------

        static void SetBytes(ref object targetItem, int ordinal, IDataReader reader)
        {
            if (reader.IsDBNull(ordinal) == false)
            {
                long size = reader.GetBytes(ordinal, 0, null, 0, 0);  //get the length of data
                byte[] barrayValue = new byte[size];

                int bufferSize = 1024;
                long bytesRead = 0;
                int curPos = 0;

                while (bytesRead < size)
                {
                    bytesRead += reader.GetBytes(ordinal, curPos, barrayValue, curPos, bufferSize);
                    curPos += bufferSize;
                }

                targetItem = barrayValue;
            }
        }
        static void SetChars(ref object targetItem, int ordinal, IDataReader reader)
        {   //accessor.SetMember<char[]>(member, ref targetItem, reader.GetChars(ordinal));

        }
        static void SetString(ref object targetItem, int ordinal, IDataReader reader)
        {
            targetItem = reader.GetString(ordinal);
        }

        #endregion  -------- IDataReader  VALUE SETTERS for  Char/Byte Arrays/string -always nullable  with  ITypeAccessor -----------

        #region -------------IDataReader  Nullalble  VALUE SETTERS with ITypeAccessor --------------------
        //Boolean?  | Byte?   |  Char? |  DateTime?   
        //Decimal?  | Double? | Float? | Guid? 
        //Int16?    | Int32?  | Int64? 

        static void SetBoolean_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Boolean?)value;
            targetItem = nullVal;
        }

        static void SetByte_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Byte?)value;
            targetItem = nullVal;
        }

        static void SetChar_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (char?)value;
            targetItem = nullVal;
        }

        static void SetDateTime_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (DateTime?)value;
            targetItem = nullVal;
        }

        static void SetDecimal_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Decimal?)value;
            targetItem = nullVal;
        }
        static void SetDouble_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Double?)value;
            targetItem = nullVal;
        }
        static void SetFloat_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (float?)value;
            targetItem = nullVal;
        }
        static void SetGuid_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Guid?)value;
            targetItem = nullVal;
        }

        static void SetInt16_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int16?)value;
            targetItem = nullVal;
        }
        static void SetInt32_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int32?)value;
            targetItem = nullVal;
        }
        static void SetInt64_Null(ref object targetItem, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int64?)value;
            targetItem = nullVal;
        }

        #endregion   -------------IDataReader  Nullalble  VALUE SETTERS with ITypeAccessor --------------------


        #region -------------- Map List/Item from Reader------------------

        //Map List from Reader Select
        //Map Item from Reader Select


        public List<T> MapListFromReader<T>(IDataReader reader, string providerKey)
        {
            var resultItems = new List<T>();
            
            //read record
            while (reader.Read())
            {                
                   var newItem = MapItemFromReader<T>(reader,  providerKey);
                resultItems.Add((T)newItem);
            }

            return resultItems;
        }

        public ObservableCollection<T> MapObservableFromReader<T>(IDataReader reader, string providerKey)
        {
            var resultItems = new ObservableCollection<T>();
                        
            //read record
            while (reader.Read())
            {
                var newItem = MapItemFromReader<T>(reader, providerKey);
                resultItems.Add((T)newItem);
            }

            return resultItems;
        }


        /// <summary>
        /// If we Collect, on first reader.row,- columns ordinal for Accessor  Properties
        /// </summary>
        public bool HasOrdinalsCollected
        { get; set; }

        void CollectOrdinals(IDataReader reader)
        {
            for (int i = 0; i < Accessor.Properties.Count; i++)
            {
                var columnKey = Accessor.Properties[i].Key;
                Accessor.Properties[i].Value.Ordinal =
                reader.GetOrdinal(columnKey);//.Ordinal;
                
            }
            HasOrdinalsCollected = true;
        }
        public T MapItemFromReader<T>(IDataReader reader, string providerKey)
        {
            var newItem = DefCtorFunc();
            if (HasOrdinalsCollected == false)
            {  CollectOrdinals(reader); }

            for (int i = 0; i < Accessor.Properties.Count; i++)
            {
                var memberOrdinal = Accessor.Properties[i].Value.Ordinal;

                //Map for Primitive Type
                if (IsPrimitive)
                {                    
                    PrimSetterFunc(ref newItem, memberOrdinal, reader);
                    return (T)newItem;
                }
                // member key for provider = provider. member 
                var memberSetterKey = providerKey + "."
                                + Accessor.Properties[i].Key;
                var memberKey = Accessor.Properties[i].Key;

                MemberSetters[memberSetterKey](Accessor, newItem, memberKey, memberOrdinal, reader);
            }
            return (T)newItem;
        }


        /// <summary>
        /// Map result records from reader to List of TargetType objects
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public List<object> MapListFromReader(IDataReader reader, string providerKey)
        {
            var resultItems = new List<object>();
                      
            //read record
            while (reader.Read())
            {
                var newItem = MapItemFromReader(reader,  providerKey);
                resultItems.Add(newItem);
            }
            return resultItems;
        }



        /// <summary>
        /// Map result record from reader to TargetType object
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        public object MapItemFromReader(IDataReader reader, string providerKey)
        {
            var newItem = DefCtorFunc();
            if (HasOrdinalsCollected == false)
            { CollectOrdinals(reader); }


            for (int i = 0; i < Accessor.Properties.Count; i++)
            {
                var memberOrdinal = Accessor.Properties[i].Value.Ordinal;
                
                //Map for Primitive Type
                if (IsPrimitive)
                {
                    PrimSetterFunc(ref newItem, memberOrdinal, reader);
                    return  newItem;
                }

                // member key for provider = provider. member 
                var memberSetterKey =  providerKey + "."
                                + Accessor.Properties[i].Key;
                var memberKey = Accessor.Properties[i].Key;

               MemberSetters[memberSetterKey](Accessor, newItem, memberKey, memberOrdinal, reader);
            }

            return newItem;
        }



        #endregion -------------- Map List/Item from Reader------------------



        #region ----------- Get Entity/Entity.Property Map Info values --------------

        /// <summary>
        /// Get Entity(BObjectBase) mapping info by Point Key  with the help of this method.        
        /// <para/> All keys of mapping Info  are in [TypeDAMap.Aks.E_cccc ] 
        /// All keys contains summary info about its result type and if it always exist in  BObject.MapInfo dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetEntMapInf<T>(string pointKey)
        {
            var entkey = Aks.EntKey(pointKey);
            if (MapInfo.ContainsKey(entkey) == false)
            { return default(T); }

            return (T)MapInfo[entkey];
        }


        /// <summary>
        /// Get Entity.Property(BObjectBase.Property)  mapping info by Point Key with the help of this method.        
        /// <para/> All keys of mapping Info are in [TypeDAMap.Aks.P_cccc ].
        /// All keys contains summary info about its result type and if it always exist in  BObject.MapInfo dictionary.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetPropMapInf<T>(string propName, string pointKey)
        {
            // //MapProvider not used
            var infKey = Aks.PropKey(propName, pointKey);
            if (MapInfo.ContainsKey(infKey) == false)
            { return default(T); }

            return (T)MapInfo[infKey];
        }


        #endregion----------- Get Entity/Entity. Property Map Info values --------------


        internal Dictionary<string, object> GetPropValues(IBObject currentItem, List<string> propNames)
        {
            if (propNames.Count == 0) return new Dictionary<string, object>(); ;

            var pkValue = new Dictionary<String, object>();
            for (int i = 0; i < propNames.Count; i++)
            {
                var memberVal = Accessor.GetMember((string)propNames[i], currentItem);
                pkValue.Add((string)propNames[i], memberVal);
            }

            return pkValue;
        }

        /// <summary>
        /// Get PK value of  IPersistableBO  or IVObject
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        internal Dictionary<string, object> GetItemPrimaryKey(IBObject currentItem)
        {
            //// VO also can contains PK -virtual properties
            //if ((currentItem is IPersistableBO) == false) 
            //    return new Dictionary<string, object>();

            var pkMembers = GetEntMapInf<List<string>>(Aks.E_PrimaryKeys);
            return GetPropValues(currentItem, pkMembers);
        }



        /// <summary>
        /// Get Domain Properties besides PK value of IPersistableBO  or IVObject
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        internal Dictionary<string, object> GetDomainPropValuesNoPK(IBObject currentItem)
        {
            //// VO also can contains PK -virtual properties
            //if ((currentItem is IPersistableBO) == false) 
            //    return new Dictionary<string, object>();

            var pkMembers = GetDomainPropertyNamesNoPK();
            return GetPropValues(currentItem, pkMembers);
        }







        /// <summary>
        /// Is Primary Key Identity -only if this contract is IPersistableBO or IVObject       
        /// </summary>
        /// <returns></returns>
        internal bool IsPrimaryKeyIdentity()
        {
            var pkMembers = GetEntMapInf<List<string>>(Aks.E_PrimaryKeys);

            if (pkMembers.Count == 1)
            {
                var memberRole = GetPropMapInf<PropertyRoleEn>(pkMembers[0], Aks.P_BOPropertyRole);
                if (memberRole.HasFlag(PropertyRoleEn.Identity))
                { return true; }
            }
            return false;
        }




        internal List<string> GetParentVos()
        {
            List<String> Parents = new List<string>();
            List<TypeMember> FKeysProperties = GetPropertiesInRole(PropertyRoleEn.VOFKeyField);

            if (FKeysProperties != null && FKeysProperties.Count > 0)//по каждому внешнему ключу
            {
                foreach (var item in FKeysProperties)
                {
                    BOPropertyRolesAttribute VoFKey = (item.MemberInf as PropertyInfo)
                        .GetFirstPropertyAttribute<BOPropertyRolesAttribute>();

                    if (VoFKey != null) { Parents.Add(VoFKey.AdditionalParam); }
                }
            }

            return Parents;
        }

        internal bool IsVObject()
        {
            var roles = GetEntMapInf<BORoleEn>(Aks.E_BORole);
            return roles.HasFlag(BORoleEn.VObject);
        }

        internal bool IsRegister()
        {
            var roles = GetEntMapInf<BORoleEn>(Aks.E_BORole);
            return roles.HasFlag(BORoleEn.Register);
        }


        internal Type GetServiceModelType()
        {
            return GetEntMapInf<Type>(Aks.E_ServiceModelType);
        }

        internal IServiceModel GetServiceModel()
        {
            var svcModelindex = GetEntMapInf<int>(Aks.E_ServiceModelIndex);
            return ServiceModel.GetByIndex(svcModelindex);
        }

        internal List<string> GetPropertyNames()
        {
            return Accessor.Properties
               .Select(pr => pr.Key).ToList();
        }


        internal List<string> GetDomainPropertyNames()
        {
            return DomainProperties.Select(pr => pr.Name)
                           .ToList();
        }

        internal List<PropertyInfo> GetDomainPropertiesNoPK()
        {
            var pKeys = GetEntMapInf<List<string>>(Aks.E_PrimaryKeys);
            if (pKeys != null)
            {
                return DomainProperties
                       .Where(pr => pKeys.Contains(pr.Name) == false)
                       .ToList();
            }
            else
            {
                return DomainProperties;
            }

        }

        internal List<string> GetDomainPropertyNamesNoPK()
        {
            return GetDomainPropertiesNoPK().Select(pr => pr.Name)
                                        .ToList();
        }

        internal bool IsPropertyNullable(string propName)
        {
            return GetPropMapInf<bool>(propName, Aks.P_IsNullable);
        }



        internal List<TypeMember> GetPropertiesInRole(PropertyRoleEn propertyRole)
        {
            var propsInRole = new List<TypeMember>();

            foreach (var item in Accessor.Members)
            {
                var memberRole = GetPropMapInf<PropertyRoleEn>(item.Key, Aks.P_BOPropertyRole);
                if (memberRole.HasFlag(propertyRole))
                {
                    propsInRole.Add(item.Value);
                }
            }

            return propsInRole;
        }


    }
}
