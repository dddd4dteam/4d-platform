﻿using System;
using System.Globalization;

namespace DDDD.Core.Data.DA2.Mapping
{


    /// <summary>
    /// Build Name for DB Table/View from EntityName  as Concept base Name.
    /// <para/> We can know from end DB object Name: 
    ///  - Is Register |  - View or table  |  - DomainPrefix  |  - Is VObject View
    /// <para/> All of theese info Points we can get if follow by a strong naming convention
    /// , coded in this class, when we building Info Systems.
    /// </summary>
    public class NamingConvention
    {

        // -------------BO NAMING ------------------------- 
        // NAMING  PARTS:
        // Name:  [1]_[2]_[3]_[4] or [1]_[2]_[3]
        // [1] - [Domainn Prefix] with  'V' as first symbol or not: if View or Table instead.
        // [2] - [Service Model Index] - abstract part of Whole Model - Service Model and its Index marker.
        //           For eacample : Sxxs(for Eaxample S1 or S23)
        // [3] - [Concept Entity Name]
        // [4] - [Register/Vo] keywords : if Table/View is Data Register, or if View is Value Object.

        // Data Register is seldom updated list.It ususaaly belongs to some concept object Kind Tables
        // Value object - mapped View that can convert on time its value to mapped table value. Its usually concept object info  and all of its Kind info from 1:1 associated tables.


        // Naming Cases / Examples:
        // TABLE: 
        //   BO of TABLE naming rules EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        // VIEW :
        //   BO of View naming rules EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas]
        //                                [PV_S9_Meeting_Vo] or [VP_S9_Meeting_Vo]- partial View
        //                                 
        // Obsolete style:  V without DomainPrefix - [crs] - its error for today
        // [V_S1_GeographyItem_Vo] - value Object View  -- obsolete - do not use  it


        public const char Dlm = '_'; //delimeter
        const string ServiceIndexPrefixSym = "S";
        const string ViewPrefixSym = "V";
        const string ViewPartialPrefixSym = "VP";
        const string ViewPartial2PrefixSym = "PV";
        const string RegisterKW = "Register";
        const string VoKW = "Vo";


        //NOTE: just the same  consts exist in  BONameInfo class
        const string ViewKW = "View";
        const string TableKW = "Table";



        /// <summary>
        ///  Get Table Or View key from BO Name - here it is [crs] ... look naming examples
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas] 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string GetTableOrView(string[] parts)
        { // only for Table and View name 
            //var parts = name.Split(Dlm);
            var firstPart = parts[0];

            if (
                 firstPart == ViewPrefixSym
               || firstPart == ViewPartialPrefixSym
               || firstPart == ViewPartial2PrefixSym
                ) return ViewKW;

            else return TableKW;
        }

        public static bool GetIsPartialView(string[] parts)
        {
            //var parts = name.Split(Dlm);
            var firstPart = parts[0];

            if (
               firstPart == ViewPartialPrefixSym
               || firstPart == ViewPartial2PrefixSym
                ) return true;

            return false;// simple View or Table

        }

        public static bool GetIsHierarchicalView(string[] parts)
        {
            //var parts = name.Split(Dlm);
            var endPart = parts[parts.Length - 1];

            if (endPart == "Ancestor"
                || endPart == "Descendant"
                || endPart == "Hierarchy"
                )
            {
                return true;
            }
            return false;// simple View or Table

        }


        /// <summary>
        ///  Get  DomainPrefix from BO Name
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas]
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static string GetDomainPrefix(string[] parts)
        { //  
            //var parts = boContractName.Split(Dlm);
            var firstPart = parts[0];
            return parts[0].Replace(ViewPrefixSym, "");
        }

        /// <summary>
        /// Get ServiceModel index from BO Name - here it is [1] ... look naming  examples
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas]
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static int GetSvcModelIndexFromBOName(string[] parts)
        {
            // for Table and View 
            //var parts = boContractName.Split(Dlm);
            var numberStr = parts[1].Replace(ServiceIndexPrefixSym, "");
            return int.Parse(numberStr);
        }



        /// <summary>
        /// Get Entity Name from BO Name - here they are [GeographyItem], [GeographyHierarchy]... look naming  examples.
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas] 
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static string GetEntityName(string[] parts)
        {
            //-  EntityName - 
            //var parts = boContractName.Split(Dlm);
            return parts[2];
        }


        /// <summary>
        /// Get Is BO  ValueObject(boolean flag) or not, from BO Name - here its true only for [Vcrs_S1_GeographyItem_Vo] ... look naming  examples.
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas] 
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static bool? GetIsVObject(string[] parts)
        {
            //-   can be empty - then null
            //var parts = boContractName.Split(Dlm);
            if (parts.Length == 3) return null;

            var thirdPart = parts[3];
            if (thirdPart == VoKW)
            {
                return true;
            }
            else return false;
        }


        /// <summary>
        /// Get Is BO Register(boolean flag) or not, from BO Name - here its true for [crs_S1_GeographyItemType_Register] and so on... look naming  examples.
        /// <para/> BO naming rules examples:
        /// <para/> BO of TABLE EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
        /// <para/> BO of View  EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas] 
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static bool? GetIsRegister(string[] parts)
        {
            //-   can be empty - then null
            //var parts = boContractName.Split(Dlm);
            if (parts.Length == 3) return null;

            var thirdPart = parts[3];
            if (thirdPart == RegisterKW)
            {
                return true;
            }
            else return false;
        }



        /// <summary>
        /// Get all information from BO Name - BONameInfo object:
        /// <para/> ContractName, TableOrView ,DomainPrefix,  ServiceModelIndex
        /// , EntityName ,IsRegister , IsValueObject.  
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static BONameInfo GetInfoByBOName(string boContractName) // no namespace only Type.Name
        {
            var boNameInfo = new BONameInfo();
            var parts = boContractName.Split(Dlm);

            boNameInfo.ContractName = boContractName;
            boNameInfo.TableOrView = GetTableOrView(parts);
            boNameInfo.DomainPrefix = GetDomainPrefix(parts);
            boNameInfo.ServiceModelIndex = GetSvcModelIndexFromBOName(parts);
            boNameInfo.EntityName = GetEntityName(parts);
            boNameInfo.IsPartialView = GetIsPartialView(parts);
            boNameInfo.IsRegister = GetIsRegister(parts);
            boNameInfo.IsValueObject = GetIsVObject(parts);

            GetBOItemRoles(boNameInfo, parts);

            return boNameInfo;
        }





        //// ------------ SEVICE MODEL NAMING --------------
        //// S[index]_[Name]

        /// <summary>
        /// Get index of ServiceModel from its name - here they are [5] and [23]...  look naming  examples
        /// <para/> ServiceModel Naming rule is: S[x]_[Name] where x is number
        /// <para/> EX: S5_Clients, S23_Suppliers
        /// </summary>
        /// <param name="boContractName"></param>
        /// <returns></returns>
        public static int GetServiceModelIndex(string svcModelName)
        {
            var parts = svcModelName.Split(Dlm);
            var numberStr = parts[0].Replace(ServiceIndexPrefixSym, "");
            return int.Parse(numberStr);
        }

        /// <summary>
        /// Collect BoROles to boinfo.Roles collection
        /// </summary>
        /// <param name="boinfo"></param>
        /// <param name="parts"></param>
        public static void GetBOItemRoles(BONameInfo boinfo, string[] parts)
        {
            //var roles = new List<BORoleEn>();
            if (boinfo.IsTable)
            {
                // Table + Register/DataTable  
                if (GetIsRegister(parts) == true)
                { boinfo.Roles.Add(BORoleEn.Register); }
                else { boinfo.Roles.Add(BORoleEn.DataTable); }
            }
            else if (boinfo.IsView)
            {
                if (GetIsPartialView(parts)) // by prefix
                    boinfo.Roles.Add(BORoleEn.ViewPartial);
                // VObject / Register/ NavigationView 
                else if (GetIsVObject(parts) == true)// by suffix
                {
                    boinfo.Roles.Add(BORoleEn.VObject);
                }
                else if (GetIsRegister(parts) == true)
                {
                    boinfo.Roles.Add(BORoleEn.Register);
                }
                else
                {
                    boinfo.Roles.Add(BORoleEn.NavigationView);
                }
                if (GetIsHierarchicalView(parts))
                {
                    boinfo.Roles.Add(BORoleEn.SpecificStructureComponent);
                }
            }
        }


    }

}
