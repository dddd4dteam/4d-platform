﻿namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// Value Object - mapped object.
    /// Can create target table object - VALUE from itselt data. 
    /// Its View class of DB DAL. 
    /// </summary>
    public interface IVobject
    {
        // Summary:
        //     Creates a new table object form Value Part of current item
        //
        // Returns:
        //     Creates a new table object-Targer for current Value Part of current item

        /// <summary>
        ///  Creates a new table object form Value Part of current item
        /// </summary>
        /// <returns></returns>
        IPersistableBO CloneToTarget();



        /// <summary>
        /// Хеш код получается из значения ключевых полей объекта/ если их не обозначено то по типу БО  
        /// </summary>
        /// <returns></returns>
        //int GetObjectHashCode();  -----> Extension's realization 



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        //Dictionary<String, object> GetPrimaryKey(); ------> Extension's realization

    }
}
