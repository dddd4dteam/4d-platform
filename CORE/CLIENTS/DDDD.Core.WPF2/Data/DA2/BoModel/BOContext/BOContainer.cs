﻿using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{
    // Name Conversions:
    // DataContextInfoTp -->      BOContextContainer    BObjectContext
    // BOInfoTp              -->  BOContainer
    // NavigationDataContext -->  BONavContextContainer



   

    // BOInfoTp -->BOContainer
    public class BOContainer 
    {

        const string Target = nameof(Target);
        #region -------------- CTOR -------------

        public BOContainer()
        {
        }
        public BOContainer(string boKey, string fullname)
        {
            Key = boKey;
            FullName = fullname;
            if (Key == Target)
            {  IsTargetInContext = true; }
        }
        public BOContainer(string boKey
                        , string fullname
                        , bool IsPKIdentity
                        , BOAssociation associationInfo
                        , BOContainerUpdate updateContext)
        {
            Key = boKey;
            FullName = fullname;
            AssociationInfo = associationInfo;
            Update = updateContext;
            
            if (Key == Target)
            { IsTargetInContext = true; }
        }


        #endregion -------------- CTOR -------------

        /// <summary>
        /// Type.Name as usually
        /// </summary>
        public string Key 
        { get; set; }


        /// <summary>
        /// Type.FullName 
        /// </summary>
        public string FullName
        { get; set; }


        /// <summary>
        /// If Key is "Target" then [IsTargetInContext] is true
        /// </summary>
        public bool IsTargetInContext
        { get; private set; }


        /// <summary>
        /// AssociationInfo  DB Foreign Key links of this BO Contract 
        /// Default value is  new AssociationInfoTp
        /// </summary>
        public BOAssociation AssociationInfo
        { get; set; } = new BOAssociation();


        /// <summary>
        /// Def value is null
        /// </summary>
        public BOContainerUpdate Update
        { get; set; } = null;

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IBObject> OriginalCollection
        { get; set; } = new ObservableCollection<IBObject>();


        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IBObject> Collection
        { get; set; } = new ObservableCollection<IBObject>();

        /// <summary>
        /// First or default element  of [OriginalCollection] 
        /// </summary>
        public IBObject OriginalItem
        { get{
                return OriginalCollection.FirstOrDefault();                
             }

            set
            {
                if (OriginalCollection.Count >= 1)
                {
                    OriginalCollection[0] = value;
                }
                OriginalCollection.Add(value);
            }
        }

        /// <summary>
        /// First or default element  of [Collection] 
        /// </summary>
        public IBObject Item
        { get{ 
                return Collection.FirstOrDefault();
             }
            set
            {
                if (Collection.Count >= 1)
                {
                    Collection[0] = value;
                }
                Collection.Add( value );
            }
        }
                     
       
       
        public Guid ParentDataContextGuid
        { get; set; }

        public string ParentDataContextKey
        { get; set; }
          

        /// --------AUDIT OPERATIONS ---------

        #region ---------- AUDIT of BOContainer -------------------


        /// <summary>
        /// Благодаря Аудиторской коллекции -копии Основных объектов, мы имеем возможность провести сравнение и выявить изменения. 
        /// Результат сравнения Три коллекции в строго заданном порядке. 
        /// Получить коллекции для проведения команды обновления - 1Коллекция для Вставки/2 КОллекция для Обновления /3 Коллекция для Удаления
        /// </summary>
        /// <returns></returns> 
        public void DoCollectionAudit(FlowProcessMode Mode)//, List<IPersistableBO> NewCollection
        {
             
                if (Mode == FlowProcessMode.Update)
                {
                    // Не создана коллекция первончальных данных/  Ни формируемая коллекция
                    if (OriginalCollection == null || Collection == null)
                    {
#if CLIENT && SL5
                                MessageInformer.ShowError(String.Format(" Ошибка формирования коллекции  данных. Для BObject типа [ {0} ]", bObjct.FullName));
#endif
                        return;
                    }

                    //Просто нет данных для формирования Комманд изменения данных. Т.е. Ничего не было и ничего не добавили
                    if (OriginalCollection.Count == 0 && Collection.Count == 0) { return; }

                     BORoleEn objectRoles = BORoleEn.NotDefined;

                    //Получить Бизнес Роли БОбъекта
                    if (OriginalCollection.Count > 0)
                    {
                        objectRoles = (OriginalCollection[0]).GetBORoles();
                    }
                    else if (Collection.Count > 0)
                    {
                        objectRoles = (Collection[0]).GetBORoles();
                    }


                    if (!objectRoles.HasFlag(BORoleEn.VObject))
                        { throw new Exception("Current object Isn't  IVobject "); }

                //UpdateContext.Commands.InsertCollection = new ObservableCollection<IPersistableBO>(); //Добавить 
                //UpdateContext.Commands.UpdateCollection = new ObservableCollection<IPersistableBO>(); //ОБновить
                Update.Commands.DeleteCollection = OriginalCollection.CloneBOCollection();
                //Удалить в старую(текущей в БД) коллекцию, если они не содержатся в новой Коллекции


                    // ищем объекты новой коллекции в оригинале и разносим их по группам  Есть в ригинале -(Update)/Нет в оригинале-(Insert)/То что остается в оригинале- Удалить                   
                    foreach (object currentItem in Collection)
                    { //Получить объект по ключу                 

                        IPersistableBO originalItem = (IPersistableBO)OriginalCollection
                            .Where(obj => 
                                (obj as IPersistableBO).GetBObjectHashCode() 
                                == (currentItem as IPersistableBO).GetBObjectHashCode()
                            ).FirstOrDefault();

                        if (originalItem != null)  //Есть в оригинале то Update/  и убрать из Коллекции удаления
                        {
                            Update.Commands.UpdateCollection.Add((IPersistableBO)(currentItem as IPersistableBO).Clone());
                            Update.Commands.DeleteCollection.RemoveByHashCode(originalItem.GetBObjectHashCode()); //Уменьшить удаляемую коллекцию 
                        }
                        else if (originalItem == null) // Если нет в оригинале - значит это новый - т.е. Insert
                        { Update.Commands.InsertCollection.Add((IBObject)(currentItem as IPersistableBO).Clone()); }
                    }
                }
                else if (Mode == FlowProcessMode.Add)
                {
                    if (Collection == null || Collection.Count == 0)
                        { return; }  //|| bObjct.Collection.Count == 0  throw new Exception("There is no items in Data Collection "); 

                    var objectRoles = (Collection[0]).GetBORoles();
                    if (!objectRoles.HasFlag(BORoleEn.VObject)) 
                        { throw new Exception("Current object Isn't  IVobject "); }

                    Update.Commands.InsertCollection = Collection.CloneCollectionAsPersistableBO();
                }

                return; 
        }

        public   void DoCollectionAudit(  FlowProcessMode Mode, bool UseUpdateCollection)//, List<IPersistableBO> NewCollection
        {
             
                if (Mode == FlowProcessMode.Update)
                {
                    // Не создана коллекция первончальных данных/  Ни формируемая коллекция
                    if ( OriginalCollection == null ||   Collection == null)
                    {
#if CLIENT && SL5
                        MessageInformer.ShowError(String.Format(" Ошибка формирования коллекции  данных. Для BObject типа [ {0} ]", bObjct.FullName));
#endif
                        return;
                    }

                    //Просто нет данных для формирования Комманд изменения данных. Т.е. Ничего не было и ничего не добавили
                    if ( OriginalCollection.Count == 0 &&  Collection.Count == 0) { return; }

                     BORoleEn  objectRoles =  BORoleEn.NotDefined;

                    //Получить Бизнес Роли БОбъекта
                    if ( OriginalCollection.Count > 0)
                    {
                        objectRoles = ( OriginalCollection[0]  ).GetBORoles();
                    }
                    else if ( Collection.Count > 0)
                    {
                        objectRoles = ( Collection[0]  ).GetBORoles();
                    }

                    if (!objectRoles.HasFlag(BORoleEn.VObject)) 
                    { throw new Exception("Current object Isn't  IVobject "); }

                    // UpdateContext.Commands.InsertCollection = new ObservableCollection<IPersistableBO>(); //Добавить 
                    //UpdateContext.Commands.UpdateCollection = new ObservableCollection<IPersistableBO>(); //ОБновить
                     Update.Commands.DeleteCollection =  OriginalCollection.CloneBOCollection(); //Удалить в старую(текущей в БД) коллекцию, если они не содержатся в новой Коллекции

                    // ищем объекты новой коллекции в оригинале и разносим их по группам  Есть в ригинале -(Update)/Нет в оригинале-(Insert)/То что остается в оригинале- Удалить                   
                    foreach (object currentItem in  Collection)
                    { //Получить объект по ключу                 

                        IPersistableBO originalItem = (IPersistableBO) OriginalCollection.Where(obj => (obj as IPersistableBO).GetBObjectHashCode() == (currentItem as IPersistableBO).GetBObjectHashCode()).FirstOrDefault();

                        if (originalItem != null)  //Есть в оригинале то Update/  и убрать из Коллекции удаления
                        {
                            if (UseUpdateCollection) // Условное создание  комманд обновления для элементов которые все таки еще остаются в измененной коллекции
                            {  Update.Commands.UpdateCollection.Add((IPersistableBO)(currentItem as IPersistableBO).Clone()); }

                             Update.Commands.DeleteCollection.RemoveByHashCode(originalItem.GetBObjectHashCode()); //Уменьшить удаляемую коллекцию 
                        }
                        else if (originalItem == null) // Если нет в оригинале - значит это новый - т.е. Insert
                        {  Update.Commands.InsertCollection.Add((IPersistableBO)(currentItem as IPersistableBO).Clone()); }
                    }
                }
                else if (Mode == FlowProcessMode.Add)
                {
                    if ( Collection == null || Collection.Count == 0) { return; }  //|| bObjct.Collection.Count == 0  throw new Exception("There is no items in Data Collection "); 

                   var objectRoles = ( Collection[0] ).GetBORoles();
                    if (!objectRoles.HasFlag(BORoleEn.VObject)) 
                { throw new Exception("Current object Isn't  IVobject "); }

                    Update.Commands.InsertCollection =  Collection.CloneCollectionAsPersistableBO();

                } 

                return;
            
        }




        #endregion ---------- AUDIT of BOContainer ------------------


    }

   
     
    

}
