﻿using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// Context COntainer contains of
    /// -  Target Container 
    /// - Associated Containers
    /// Data structure of the following components: 
    ///<para/> - one TargetItem - BObject instance
    ///<para/> - and several child/Description BOCollections which asssociated(DB realations or abstract between Vobjects and Views)  with TargetItem object on its ID.
    /// </summary>
    public class BOContextContainer
    {
        public BOContextContainer()
        { }


        /// <summary>
        /// Guid Key of BOContextContainer - default to use Key
        /// </summary>
        public Guid KeyGd
        { get; } = Guid.NewGuid();


        /// <summary>
        /// String Key of BOContextContainer
        /// </summary>
        public string KeyStr
        { get; set; } 

        const string Target = nameof(Target);

        /// <summary>
        /// AllContainers - Target BOContainer - with [Target] Key
        /// and Associated BOContainers by their Keys
        /// </summary>
        public Dictionary<string, BOContainer> AllContainers
        { get; set; } = new Dictionary<string, BOContainer>();

        /// <summary>
        /// Target BO Container  - only if it is Target BOContainer. 
        /// </summary>
        [IgnoreMember]
        public BOContainer TargetContainer
        {
            get
            {
                if (AllContainers.ContainsKey(Target))
                {
                    return AllContainers[Target];
                }
                return null;
            }
            set
            {
                if (AllContainers.ContainsKey(Target))
                {
                    AllContainers.Add(Target, value);
                    return;
                }
                AllContainers[Target] = value;
            }
        }


        /// <summary>
        /// Create new BOContextContainer for dataContextContract 
        /// </summary>
        /// <param name="dataContextContract"></param>
        /// <returns></returns>
        public static BOContextContainer New(Type dataContextContract)
        {
            // 1 Cоздать пустой Дата Контекст
            return new BOContextContainer()
            {                 
                TargetContainer = dataContextContract.GetBObjectContainer(),
            };
        }

        /// <summary>
        /// Create new BOContextContainer for dataContextItem.Contract
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <returns></returns>
        public static BOContextContainer New(IPersistableBO dataContextItem)
        {
            return New(dataContextItem.Contract);
        }

        /// <summary>
        /// Update this BOContextContainer by ID value - forein keys of Insert items in 
        /// associatedItem.UpdateContext.Commands.InsertCollection  will be set to ID value
        ///  Обновить Дата Контекст данными ИД от TargetBO 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="ID"></param>
        public void UpdateByTargetIdentityPK(object ID)
        {
            var TargetKeyPropertyVal = (TargetContainer.Item as IPersistableBO).GetPrimaryKey()
                                    .FirstOrDefault();

            foreach (var associatedItem in AllContainers)
            {
                if (associatedItem.Key == Target) continue;

                if (associatedItem.Value.Update.Commands.IsContainsInsertCommand)
                {
                    foreach (var insertItem in associatedItem.Value
                                .Update.Commands.InsertCollection)
                    {
                        insertItem.SetPropertyValue(TargetKeyPropertyVal.Key, ID);
                    }
                }
            }

        }


        /// <summary>
        /// Update this BOContextContainer by ID value - forein keys of Insert items in 
        /// associatedItem.UpdateContext.Commands.InsertCollection  will be set to TargetContainer.Item.ID - identity value 
        /// </summary>
        public void UpdateByTargetIdentityPK()
        {
            var TargetKeyPropertyVal = (TargetContainer.Item as IPersistableBO).GetPrimaryKey()
                                    .FirstOrDefault();
            UpdateByTargetIdentityPK(TargetKeyPropertyVal.Value);
        }

        /// <summary>
        /// Preapare to Update : save Collection to Original Collection
        /// </summary>
        /// <param name="?"></param>
        public void PrepareToUpdate()
        {
            //      
            foreach (var associatedItem in AllContainers)
            {
                if (associatedItem.Key == Target) continue;

                if (associatedItem.Value.Collection.Count > 0)
                {
                    associatedItem.Value.OriginalCollection = associatedItem.Value.Collection
                        .CloneCollectionAsPersistableBO();
                }
                //else if (  associatedItem.Value.Collection.Count == 0)
                //{
                //    associatedItem.Value.OriginalCollection = new ObservableCollection<IBObject>();
                //}
            }
        }

        /// <summary>
        /// Clear AssociatedObjects. Collections- BOContainers.Collections
        /// </summary>
        public void ClearAOCollections()
        {
            foreach (var AssociatedItem in AllContainers)
            {
                if (AssociatedItem.Key == Target) continue;

                if (AssociatedItem.Value.Collection != null && AssociatedItem.Value.Collection.Count > 0)
                {
                    AssociatedItem.Value.Collection.Clear();
                }
            }
        }


        #region ----------- AUDIT OPERATIONS ------------------


        /// <summary>
        /// 
        /// </summary>
        /// <param name="DataContext"></param>
        /// <param name="UseUpdateCommands"></param>
        public void DoDataContextAudit(bool UseUpdateCommands)
        {
            try
            {
                //Аудит для таргет объекта/ для Коллекции
                if (TargetContainer != null && TargetContainer.Collection != null)
                { TargetContainer.DoCollectionAudit(TargetContainer.Update.FlowContext.Mode); }

                //Ассоциированные БОбъекты
                foreach (var item in AllContainers)
                {
                    // only for associated BOContainers
                    if (item.Key == Target) continue;

                    if (item.Value != null) //0 //&& item.Collection.Count != item.OriginalCollection.Count
                        item.Value.DoCollectionAudit(item.Value.Update.FlowContext.Mode, UseUpdateCommands);

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion ----------- AUDIT OPERATIONS ------------------




    }

}
