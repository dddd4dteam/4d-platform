﻿using System.Collections.ObjectModel;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// 
    /// </summary>
    public class BOCommands
    {
        public BOCommands() { }


        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IBObject> InsertCollection
        { get; set; } = new ObservableCollection<IBObject>();
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsContainsInsertCommand
        { get { return (InsertCollection.Count > 0); } }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IBObject> UpdateCollection
        { get; set; } = new ObservableCollection<IBObject>();
        
        /// <summary>
        /// 
        /// </summary>
        public bool IsContainsUpdateCommand
        { get { return (UpdateCollection.Count > 0); } }


        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<IBObject> DeleteCollection
        { get; set; } = new ObservableCollection<IBObject>();
        

        /// <summary>
        /// 
        /// </summary>
        public bool IsContainsDeleteCommand
        { get { return (DeleteCollection.Count > 0); } }


    }
}
