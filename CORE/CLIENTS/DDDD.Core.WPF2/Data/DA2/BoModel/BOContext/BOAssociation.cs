﻿using System.Collections.Generic;

namespace DDDD.Core.Data.DA2
{
    public class BOAssociation
    {

        #region ---------- CTOR --------------
        public BOAssociation()
        { }
        public BOAssociation(List<string> itemParents)
        {
            Parents = itemParents;
        }

        #endregion ---------- CTOR --------------

        /// <summary>
        /// List or Parent Contract names - Tables or VObjects abstraction.
        /// </summary>
        public List<string> Parents
        { get; set; } = new List<string>();

        /// <summary>
        /// 
        /// </summary>
        public int LevelToTarget
        { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool LevelToTargetSpecified
        {
            get { return (LevelToTarget > 0); }
        }

    }
}
