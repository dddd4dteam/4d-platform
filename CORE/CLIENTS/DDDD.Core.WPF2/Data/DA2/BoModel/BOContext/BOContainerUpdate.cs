﻿namespace DDDD.Core.Data.DA2
{
    public class BOContainerUpdate
    {
        public BOContainerUpdate()
        { }

        /// <summary>
        /// Order 
        /// </summary>
        public int Order
        { get; set; }

        public FlowProcessInfo FlowContext
        { get; set; }

        public BOCommands Commands
        { get; set; }

    }
}
