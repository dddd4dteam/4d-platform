﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;
using DDDD.Core.Extensions;
using System.Reflection;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    ///  BONavContextContaier - we can use in colelctions binding     
    /// </summary>
    public partial class BONavContextContaier : INotifyPropertyChanged
    {

        #region  -------------------------- CTOR -------------------------

        public BONavContextContaier(BOContextContainer serviceContextContainer)
        {
            if (serviceContextContainer == null
                || serviceContextContainer.TargetContainer  == null
                ) return;

            //TargetItem - может быт пустым  == null -  для инициализации Контекста навигации исходя из одного лиш Типа Целевого Контракта
            if (serviceContextContainer.TargetContainer .Item != null)
                TargetItem = serviceContextContainer.TargetContainer.Item;//


            if (serviceContextContainer.AllContainers.Count == 0) return;

            SelectedItemsInCollections = new Dictionary<String, object>();
            AssociatedCollections = new Dictionary<String, ObservableCollection<object>>();

            //Перегрузить все Коллекции Ассоциаций
            foreach (var BOItem in serviceContextContainer.AllContainers)
            {
                if (BOItem.Value.Collection != null && BOItem.Value.Collection.Count > 0)
                {
                    ObservableCollection<object> newCollection = BOItem.Value.Collection.CloneCollection()
                                                            .ToObservableCollection();
                    AssociatedCollections.Add(BOItem.Key, newCollection);
                    SelectedItemsInCollections.Add(BOItem.Key, null);
                }

                else if (BOItem.Value.Collection != null) //&& BOItem.Collection.Count == 0
                {
                    ObservableCollection<object> newCollection = new ObservableCollection<object>();
                    AssociatedCollections.Add(BOItem.Key, newCollection);
                    SelectedItemsInCollections.Add(BOItem.Key, null);
                }
            }

        }

        #endregion -------------------------- CTOR -------------------------



        #region ----------------------------- PROPERTIES -------------------------------------


        #region     ----------------------------PROP ITEM  :  TargetItem ---------------------------------
        // PARAMS :
        // PropName   -      TargetItem         EX: AddNewButton     
        // PropType   -      object         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Целевой объект контескта         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Целевой объект контескта
        /// </summary>   
        object _TargetItem = new object();
        public object TargetItem
        {
            get { return _TargetItem; }
            set
            {
                _TargetItem = value; //Field changing
                OnPropertyChanged(() => TargetItem);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  TargetItem ---------------------------------




        #region ------------------NOTIFIABLE MODEL ITEM: AssociatedCollections------------------------------

        // ItemName - AssociatedCollections  -  Ex: Client 
        // Contract - Dictionary<String,ObservableCollection<object>>  -  Ex: V_S8_Client_Vo

        Dictionary<String, ObservableCollection<object>> _AssociatedCollections = new Dictionary<String, ObservableCollection<object>>();

        /// <summary>
        /// Ассоциированные коллекции с типом данного целевого объекта        
        /// </summary>
        public Dictionary<String, ObservableCollection<object>> AssociatedCollections
        {
            get { return _AssociatedCollections; }
            set
            {
                _AssociatedCollections = value; //Field changing
                OnPropertyChanged(() => AssociatedCollections);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: AssociatedCollections------------------------------



        #region ------------------NOTIFIABLE MODEL ITEM: SelectedItemsInCollections------------------------------

        // ItemName - SelectedItemsInCollections  -  Ex: Client 
        // Contract - Dictionary<String,object>  -  Ex: V_S8_Client_Vo

        Dictionary<String, object> _SelectedItemsInCollections = new Dictionary<String, object>();

        /// <summary>
        /// Выбираемые элементы в коллекции- привязываемые к гридам, спискам. Каждый выбранный элемент имеет такой же ключ доступа как и его Коллелкция
        /// </summary>
        public Dictionary<String, object> SelectedItemsInCollections
        {
            get { return _SelectedItemsInCollections; }
            set
            {
                _SelectedItemsInCollections = value; //Field changing
                OnPropertyChanged(() => SelectedItemsInCollections);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: SelectedItemsInCollections------------------------------


        #endregion ----------------------------- PROPERTIES -------------------------------------



        #region -----------INotifyPropertyChanged Members -------------------

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }


        public event PropertyChangedEventHandler PropertyChanged;

        #endregion -----------INotifyPropertyChanged Members -------------------





        public void Reload(String AssociationKey, IEnumerable Collection)
        {
            if (AssociatedCollections.ContainsKey(AssociationKey) && AssociatedCollections[AssociationKey] != null)
            {
                AssociatedCollections[AssociationKey].Reload(Collection);
                AssociatedCollections[AssociationKey].RaiseCollectionCahnged();
            }

        }
    }
}
