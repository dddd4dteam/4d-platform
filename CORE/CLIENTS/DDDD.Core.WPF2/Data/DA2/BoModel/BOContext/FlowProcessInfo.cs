﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{


    /// <summary>
    /// Параметры ОБработчика Флу процессов 
    /// </summary>
    /// <summary>
    /// Режим в котором у нас будут прикрепляться BObjects в Items Store
    /// </summary>     
    public enum FlowProcessMode
    {
        NotDefined,

        Add,

        Update,

        View
    }

    /// <summary>
    /// 
    /// </summary>
    public class FlowProcessInfo
    {
        public FlowProcessInfo()
        { }
        public FlowProcessInfo(Guid flowID)
        {
            FlowProcessID = flowID;
        }
        public FlowProcessInfo(Guid flowID
                              , FlowProcessMode flowMode
                              , string serviceModel)
        {
            FlowProcessID = flowID;
            Mode = flowMode;
            ServiceModel = serviceModel;
        }

        public FlowProcessMode Mode
        { get; set; } = FlowProcessMode.NotDefined;

        public Guid FlowProcessID
        { get; set; }

        public string ServiceModel
        { get; set; }

    }


    


}
