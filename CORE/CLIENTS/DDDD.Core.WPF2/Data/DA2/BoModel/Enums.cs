﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// Property Role - 
    /// PrimaryKey or ForeignKey/ DataField
    ///   / field with SQL facade 
    ///   / field with code facade    
    /// <summary>
    [Flags]
    public enum PropertyRoleEn
    {


        /// <summary>
        ///  Not Defined value
        /// </summary>
        NotDefined = 1,

        /// <summary>
        /// Data field 
        /// </summary>
        DataField = 2,


        /// <summary>
        /// Primary Key  Field
        /// </summary>
        PKeyField = 4,

        /// <summary>
        /// PK identity field
        /// </summary>
        Identity = 8,

        /// <summary>
        /// Foreign Key  Field
        /// </summary>
        FKeyField = 16,

        /// <summary>
        /// Value Object foregn Key Field
        /// </summary>
        VOFKeyField = 32,

        /// <summary>
        /// Facade in SQL  Data Field
        /// </summary>
        FacadeSQLDataField = 64,

        /// <summary>
        /// Facade in Code Data Field
        /// </summary>
        FacadeCodeDataField = 128,
    }

    //----- Q: If target Type is some Tree kind    
    //----  Q: in runtime and after aplication finished its work.


    /// <summary>
    /// BO Role -tell us how to store and cache(in redis for example) BO  target Type 
    /// <para/>  BO alaways is one of thes:  {Data || Register || VObject }  
    /// <para/> Only View Type can contains VObject role( Valu Object )
    /// <para/> Seldom View Type can represent Register (non insertable/updatable)
    /// <summary>
    [Flags]
    public enum BORoleEn
    {

        /// <summary>
        /// Not Defined Data values
        /// </summary>
        NotDefined = 1,

        /// <summary>
        /// Register DB View or DB Table
        /// </summary>
        Register = 2,

        /// <summary>
        /// Value Object - when we mapping View to DataTable,
        /// <para/> So we can simply convert View to DataTable object and save it into DB
        /// </summary>
        VObject = 4,

        /// <summary>
        /// Data-Fact Table
        /// </summary>
        DataTable = 8,


        /// <summary>
        /// DB View for navigation only on its records
        /// </summary>
        NavigationView = 16,


        /// <summary>
        /// By default BO is structure object -not DAL model object, so it has no relations to Model Manager
        /// По умолчанию - это объект структуры - т.е. у него нет каких то связей с DAL напрямую в менеджере модели
        /// </summary>
        Structure = 32,

        /// <summary>
        /// Partitioning View classes
        /// </summary>
        ViewPartial = 64,

        /// <summary>
        /// component class of Specifically structured Service Model
        /// Fro Exa,ple Hierarchy Service Model
        /// </summary>
        SpecificStructureComponent = 128
        //SpecifiedStructureComponent,        
        //PartitioningView
    }



    /// <summary>
    /// BO Options 
    /// </summary>
    [Flags]
    public enum BOOptionsEn
    {
        //
        NotDefinied = 0,
        //
        Editable = 1,
        //
        Navigable = 2
    }


}
