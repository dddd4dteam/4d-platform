﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{
    public class ObjectList : List<object> { }

    public class ObjectsOriginalFromCollectionProperties : Dictionary<string, ObjectList> { }

    public class ObjectsAddedToCollectionProperties : Dictionary<string, ObjectList> { }

    public class ObjectsRemovedFromCollectionProperties : Dictionary<string, ObjectList> { }

    public class PropertyValueStatesDictionary : Dictionary<string, PropertyValueState> { }

}
