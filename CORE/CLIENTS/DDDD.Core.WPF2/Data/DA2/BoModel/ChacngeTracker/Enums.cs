﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{


    #region --------------------- EnumForObjectState ------------------------------

    /// <summary>
    /// Изменение состояния
    /// </summary>
    [Flags]
    public enum ObjectState
    {
        
        Unchanged = 0x1,
 
        Added = 0x2,

       
        Modified = 0x4,

        
        Deleted = 0x8
    }

    #endregion --------------------- EnumForObjectState ------------------------------


}
