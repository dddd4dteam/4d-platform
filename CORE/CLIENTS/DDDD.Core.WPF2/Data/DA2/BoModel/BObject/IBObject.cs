﻿using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{

    public interface IBObject : ICloneable, INotifyPropertyChanged
    {

        /// <summary>
        /// BOContract - should return static value of end BO class
        /// </summary>
        Type Contract { get; }

        /// <summary>
        /// BO contract Mapping Info
        /// </summary>
        TypeDAMap Mapping { get; }
        
        /// <summary>
        /// 
        /// </summary>
        ObjectChangeTracker ChangeTracker { get; }
       
        /// <summary>
        /// 
        /// </summary>
        void ClearPropertyChangedEvent();

        IServiceModel GetServiceModel();

        /// <summary>
        /// Get BO Roles - flag enum value.
        /// </summary>
        /// <returns></returns>
        BORoleEn GetBORoles();


        /// <summary>
        /// Get BO Options - flagged enum value.
        /// </summary>
        /// <returns></returns>
        BOOptionsEn GetBOOptions();
        
        /// <summary>
        ///  Get Domain Property Names -properties with SqlColumn attribute
        /// </summary>
        /// <returns></returns>
        List<string> GetDomainPropertyNames();
       

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<string> GetDomainPropertyNamesNoPK();
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyRole"></param>
        /// <returns></returns>
        List<TypeMember> GetPropertiesInRole(PropertyRoleEn propertyRole);
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<PropertyInfo> GetDomainProperties();


        /// <summary>
        /// 
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        object GetPropertyValue(string propName);

        /// <summary>
        /// Set Domain Poperty [value]
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="value"></param>
        void SetPropertyValue(string propName, object value);

        /// <summary>
        /// Is property marked with NullableAttribute 
        /// </summary>
        /// <param name="updField"></param>
        /// <returns></returns>
        bool IsPropertyNullable(string propName);



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<PropertyInfo> GetDomainPropertiesNoPK();
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        List<PropertyInfo> GetValidatableProps();
        
        /// <summary>
        /// 
        /// </summary>
        void ResetChangeTracking();
        
        /// <summary>
        /// 
        /// </summary>
        void StartChangeTracking();
        
        /// <summary>
        /// 
        /// </summary>
        void StopChangeTracking();
    }

}
