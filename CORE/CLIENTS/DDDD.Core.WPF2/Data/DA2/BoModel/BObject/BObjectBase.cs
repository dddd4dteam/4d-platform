﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using System.ComponentModel;
using System.Diagnostics;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Serialization;
using DDDD.Core.Diagnostics;
using DDDD.Core.Data.Hash;

namespace DDDD.Core.Data.DA2
{

    class BOSon : BObjectBase
    {
        #region -----------LAZY MAPPING ------------
       
        static readonly string defaultProvider = ProviderKey.Default;
        static readonly Type boContarct =typeof(BOSon);

         
        static readonly Lazy<TypeDAMap> mapping =
        new Lazy<TypeDAMap>(() => { return TypeDAMap.GetInit(boContarct, defaultProvider, true); }
                , isThreadSafe: true);

        /// <summary>
		/// BO contract Mapping Info 
		/// </summary>
		public override TypeDAMap Mapping
        { get { return mapping.Value; } }

        /// <summary>
        /// BO contract  - Type
        /// </summary>
        public override Type Contract
        { get { return boContarct; } }
        
        #endregion-----------LAZY MAPPING ------------


        public override object Clone()
        {
            throw new NotImplementedException();
        }
    }


    /// <summary>
    /// BO - Business Object abstract class.
    /// <para/> Base class for DAL layer on server side and BLlayer on client side.
    /// <para/> On clinet side use CLIENT condition, on server use SERVER condition.
    /// <para/> On client side CHangeTracker exist only.
    /// </summary>
    public abstract partial class BObjectBase : IBObject  //, INotifyPropertyChanged  IPhoenixObject, IComparable, IRSSable, ISecurable, ICachable   
    {
        #region -------------- CTOR -----------------
        protected BObjectBase()
        {
            Initialize();
        }

        #endregion -------------- CTOR -----------------

       

        /// <summary>
        /// BO Type Default Data Provider.         /// 
        /// </summary>
        public virtual string DefaultProvider
        { get; protected set; } = ProviderKey.Default;


        /// <summary>
        /// BOType once inited
        /// </summary>
        public virtual Type Contract
        { get;  }

        // type level only static mapInfo
        public abstract TypeDAMap Mapping
        {  get; }



        /// <summary>
        /// Initialization can be overriden in end contract. Will be called in base() ctor.
        /// </summary>
        protected virtual void Initialize()
        {
            //Mapping = TypeDAMap.GetExisted(GetType());
            //ChangeTracker.ObjectTrackingEnabled = true;
            //ChangeTracker.StartTracking();
        }

        /// <summary>
        /// Get BO ServiceModel
        /// </summary>
        /// <returns></returns>
        public  IServiceModel GetServiceModel()
        { return Mapping.GetServiceModel(); }


        /// <summary>
        /// Get BO roles , flags enum value.
        /// </summary>
        /// <returns></returns>
        public BORoleEn GetBORoles( )
        {
            var role = Mapping.GetEntMapInf<BORoleEn>(TypeDAMap.Aks.E_BORole);
            return role;            
        }


        /// <summary>
        /// Get BO options , flags enum value.
        /// </summary>
        /// <returns></returns>
        public BOOptionsEn GetBOOptions()
        {
            var options = Mapping.GetEntMapInf<BOOptionsEn>(TypeDAMap.Aks.E_BOOptions);
            return options;            
        }


        /// <summary>
        /// Get all BO property names.
        /// </summary>
        /// <returns></returns>
        public List<String> GetPropertyNames()
        {
            return Mapping.GetPropertyNames();
        }


        /// <summary>
        /// Get domain onlly properties of end BO contract
        /// We use doninn properties to update data/entity/table. 
        /// </summary>
        /// <returns></returns>
        public List<PropertyInfo> GetDomainProperties()
        {
            return Mapping.DomainProperties;
        }

        /// <summary>
        /// Get Property value by [propName]
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        public object GetPropertyValue(string propName)
        {
            return Mapping.Accessor.GetMember(propName, this);
        }

        /// <summary>
        /// Set Property value with [propName] by [value]
        /// </summary>
        /// <param name="propName"></param>
        /// <param name="value"></param>
        public void SetPropertyValue(string propName, object value)
        {
            object item = this;
            Mapping.Accessor.SetMember(propName,ref item, value);
        }



        /// <summary>
        /// Is Property  Nullable{T} - for ex [int?]  
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        public bool IsPropertyNullable(string propName)
        {
            return Mapping.IsPropertyNullable(propName);
        }



        /// <summary>
        /// Get BO Domain property names.
        /// <para/ >Domain properties- properties of end BObjectbase contract Type.
        /// </summary>
        /// <returns></returns>
        public List<string> GetDomainPropertyNames()
        {
            return Mapping.GetDomainPropertyNames();                       
        }



        /// <summary>
        /// Get Domain Properties without PK properties -  Names only.
        /// <para/ >Domain properties- properties of end BObjectbase contract Type.
        /// </summary>
        /// <returns></returns>
        public List<string> GetDomainPropertyNamesNoPK()
        {
            return Mapping.GetDomainPropertyNamesNoPK(); 
        }

       



        /// <summary>
        /// Get domain onlly properties of end BO contract
        /// We use doninn properties to update data/entity/table.          
        /// </summary>
        /// <returns></returns>
        public List<PropertyInfo> GetDomainPropertiesNoPK()
        {
           return  Mapping.GetDomainPropertiesNoPK();          
        }

        /// <summary>
        /// Get Properties with Role - [propertyRole]
        /// </summary>
        /// <param name="propertyRole"></param>
        /// <returns></returns>
        public List<TypeMember> GetPropertiesInRole(PropertyRoleEn propertyRole)
        {
            return Mapping.GetPropertiesInRole(propertyRole);
        }
        

        /// <summary>
        /// Get all validatable properties on client
        /// </summary>
        /// <returns></returns>
        public List<PropertyInfo> GetValidatableProps()
        {
            // by dafault all domain prperties are  validatable
            return Mapping.GetDomainPropertiesNoPK();            
        }


        /// <summary>
        /// Is BObject Value object
        /// </summary>
        /// <returns></returns>
        public  bool IsBO_VObject()
        {            
            return Mapping.IsVObject();
        }

        /// <summary>
        /// Is BObject Register object
        /// </summary>
        /// <returns></returns>
        public bool IsBO_Register()
        {
            return Mapping.IsRegister();
        }

        /// <summary>
        /// Get  Property Value of BObject instance  using  TypeAccessor ..        /// 
        /// </summary>        
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public object GetValue(string propertyName)
        {  
            return Mapping.Accessor.GetMember(propertyName, this);
        }

       

       


        /// <summary>
        ///Get parent VO-(Value Object)  items for current IVobject        
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        public   List<String> GetParentVos()
        {
            return Mapping.GetParentVos();
        }



        /// <summary>
        /// Clone current BO instance to new instance        /// 
        /// </summary>
        /// <returns></returns>
        public abstract object Clone();


        #region -----------INotifyPropertyChanged Members -------------------

        /// <summary>
        /// OnPropertyChanged called when property changed its value :
        ///  - ChangeTracker Record mew changed value
        ///  - and  PropertyChanged event rising
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        [Conditional("CLIENT")]
        protected virtual void OnPropertyChangedClt<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            //var vl = ((MemberExpression)property.Body).

            Validator.ATNullReferenceArgDbg<BObjectBase>(propertyInfo, nameof(OnPropertyChangedClt), nameof(propertyInfo));
            
            object propval = this.GetValue(propertyInfo.Name);
            ChangeTracker.RecordCurrentValue(propertyInfo.Name, propval);           

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));                       
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void ClearPropertyChangedEvent()
        {
            if ( PropertyChanged != null )
            {
               PropertyChanged = null;
            }
        }

        #endregion -----------INotifyPropertyChanged Members -------------------
        

        #region --------------- CHANGE TRACKER -----------------

#if CLIENT

        private ObjectChangeTracker changeTrackerField;

        /// <summary>
        /// BO  Property  Chacnge Tracjer
        /// </summary>
        [IgnoreMember]
        public ObjectChangeTracker ChangeTracker
        {
            get
            {
                if ((this.changeTrackerField == null))
                {
                    this.changeTrackerField = new ObjectChangeTracker(this);
                    //this.changeTrackerField.StartTracking();
                }
                return changeTrackerField;
            }
        }

       

        /// <summary>
        /// Start  Object Change Tracker
        /// </summary>
        public void StartChangeTracking()
        {
            ChangeTracker.StartTracking();
        }


        /// <summary>
        /// Stop  Object Change Tracker
        /// </summary>
        public void StopChangeTracking()
        {
            ChangeTracker.StopTracking();
        }


        /// <summary>
        ///  Restart  Object Change Tracker
        /// </summary>
        public void ResetChangeTracking()
        {
            ChangeTracker.ResetTracking();
        }


#endif



        #endregion --------------- CHANGE TRACKER -----------------


        #region ------------ LOCALIZATION ----------------

        //language - use global Culture

        // BoNameLoc
        // BoDescLoc
        // BoPropElmNameLoc
        // BoPropElmDescLoc

        /*
          "BoNameLoc\\" + tab.TableName -     tab.TableName);
             "BoDescLoc\\" + tab.TableName -  tab.TableName);
           
                "BoPropElmNameLoc\\" + tab.TableName + "|" + pop.MemberName,
                              - tab.TableName + " - " + pop.MemberName);
                 "BoPropElmDescLoc\\" + tab.TableName + "|" + pop.MemberName,
                              - tab.TableName + " - " + pop.MemberName);
             
         */

        /// <summary>
        /// Get Lozalization string for BObject Name
        /// </summary>
        /// <returns></returns>
        public string GetLoc_BoNameLoc( )
        {
            var svcModel = GetServiceModel();
            var key = "BoNameLoc\\" +  Contract.Name;
            return svcModel.ResManager.GetString(key);
        }

        /// <summary>
        /// Get Lozalization string for BObject Description
        /// </summary>
        /// <returns></returns>
        public string GetLoc_BoDescLoc()
        {
            var svcModel = GetServiceModel();
            var key = "BoDescLoc\\" + Contract.Name;
            return svcModel.ResManager.GetString(key);
        }

        /// <summary>
        /// Get Lozalization string for BObject's property Name
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        public string GetLoc_BoPropElmNameLoc(string propName)
        {
            var svcModel = GetServiceModel();
            var key = "BoPropElmNameLoc\\" + Contract.Name + "|" + propName;                               
            return svcModel.ResManager.GetString(key);
        }

        /// <summary>
        /// Get Lozalization string for BObject's property Description
        /// </summary>
        /// <param name="propName"></param>
        /// <returns></returns>
        public string GetLoc_BoPropElmDescLoc(string propName)
        {
            var svcModel = GetServiceModel();
            var key = "BoPropElmDescLoc\\" + Contract.Name + "|" + propName;
            return svcModel.ResManager.GetString(key);
        }

        #endregion ------------ LOCALIZATION ----------------

    }
}
