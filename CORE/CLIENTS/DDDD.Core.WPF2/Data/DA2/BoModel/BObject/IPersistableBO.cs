﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// Для всех  сохраняемых БО-тов
    /// </summary>
    public interface IPersistableBO : IBObject
    {

        /// <summary>
        /// Список полей у которых есть атрибут PrimaryKey/ BoPropertyRole.PKeyRole
        /// </summary>
        List<String> PrimaryKey { get; }


        /// <summary>
        /// Get  PK(Primary Key) values of IPersistableBO [curentItem]
        /// If its not IPersistableBO  then returns empty Dictionary  object.
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        Dictionary<string, object> GetPrimaryKey();


        /// <summary>
        /// Get domain property values besides  PK(Primary Key) values.
        /// </summary>
        /// <returns></returns>
        Dictionary<string, object> GetDomainPropValuesNoPK();



        /// <summary>
        /// Is Primary Key of  IPersistableBO - base Identity Counter
        /// , such property has PropertyRoleEn.Identity flag  in attribute.
        /// </summary>
        /// <returns></returns>
        bool IsPKIdentity();


        /// <summary>
        /// Get IPersistableBO instance hash Code
        /// </summary>
        /// <returns></returns>
        int GetBObjectHashCode();  //- it's only for  IPersistableBO
        
       


        //#if SERVER
        //#endif

    }
}
