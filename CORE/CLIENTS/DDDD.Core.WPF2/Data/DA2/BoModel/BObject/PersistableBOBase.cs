﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using DDDD.Core.Extensions;
using DDDD.Core.Data.Hash;

namespace DDDD.Core.Data.DA2
{
    [DataContract]
    public abstract partial class PersistableBOBase : BObjectBase, IPersistableBO
    {

        protected PersistableBOBase():base()
        {
            //ChangeTracker.StartTracking();             
        }
        
        /// <summary>
        /// Presistable BO Primary Key 
        /// </summary>
        public abstract List<string> PrimaryKey { get;}


        /// <summary>        
        /// Get Primary Key values of IPersistableBO [curentItem]
        /// If its not IPersistableBO  then returns empty Dictionary  object.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetPrimaryKey()
        {
            return Mapping.GetItemPrimaryKey(this);
        }

        /// <summary>
        /// Get Domain Properties besides PK value of IPersistableBO 
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> GetDomainPropValuesNoPK()
        {
            return Mapping.GetDomainPropValuesNoPK(this);
        }

        /// <summary>        
        /// Is Primary Key of IPersistableBO -  base Identity Counter
        /// , such property has PropertyRoleEn.Identity flag  in attribute.
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>
        public bool IsPKIdentity()
        {
            return Mapping.IsPrimaryKeyIdentity();
        }
       

        /// <summary>
        /// Get IPersistableBO instance hash Code
        /// </summary>
        /// <param name="currentItem"></param>
        /// <returns></returns>        
        public int GetBObjectHashCode()
        {
            Dictionary<string, object> pKey = GetPrimaryKey();
            Int32 PKeyHashCode = -1;
            //если у объекта нет свойств первичного ключа(это могут быть только БО c ролью Struct)  то вернется -1
            if (pKey == null) return PKeyHashCode;

            var pkString = "";
            foreach (var item in pKey)
            {
                pkString += item.Value.S() + ".";
                //PKeyHashCode += 78 ^ propertyValue.GetHashCode();
            }
            return Hashes.StringHashCode2(pkString);
        }
           
        


    }
}
