﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Collections.ObjectModel;
using DDDD.Core.Extensions;

namespace DDDD.Core.Data.DA2
{
    /// <summary>
    /// Service Data Model Analyser 
    /// </summary>
    public class ServiceModelAnalyzer
    {

        #region  -------------------------- CTOR ---------------------------------
        #endregion -------------------------- CTOR ---------------------------------



        #region  ---------------------------- GetItem/ServiceDataContext ----------------------------------

        /// <summary>
        /// Для получения контекста Данных для Какого-то конкретного объекта
        /// FlowProcess.Mode == ADDING . DataContext.TargetBO.Item не заполняется
        /// </summary>
        /// <param name="objectInstance"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem
                                                    , FlowProcessInfo FlowProcInfo)
        {   //Для 
           
                Int32 OrderIndex = 1; Int32 LevelToTarget = 1; 

                // 1 Cоздать пустой Дата Контекст
                var dataContext = BOContextContainer.New(dataContextItem);

                // 2 Обозначение Контекста Данных
                dataContext.Key = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

                // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
                dataContext.TargetContainer.Item = dataContextItem;
                //dataContext.TargetContainer.IsTargetInContext = true;
                dataContext.TargetContainer.ParentDataContextGuid = dataContext.Guid;
                dataContext.TargetContainer.ParentDataContextKey = dataContext.Key;
                
                // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
                IServiceModel svcManager = dataContextItem.GetServiceModel();
                List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

                // Associated with the current target item other VOs
                List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(dataContextItem.Contract);

                // Получить BOContainer из простого Type для каждого ассоциированного объекта
                foreach (var item in associatedVObjects)
                {
                    BOContainer associatedBOItem = item.GetBObjectContainer();
                    associatedBOItem.Collection = new ObservableCollection<IBObject>(); //Инициализация пустым но Экземпляром Коллекции для будующего связывания коллекций

                    //Привязать associatedBOItem  к текущему Контексту Данных
                    associatedBOItem.ParentDataContextKey = dataContext.Key;
                    associatedBOItem.ParentDataContextGuid = dataContext.Guid;

                    dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
                }

                // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
                foreach (var item in dataContext.AllContainers)
                {
                    item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
                }


                //Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
                dataContext.TargetContainer.UpdateContext.Order = OrderIndex;     //Первая Команды для Таргета
                foreach (var item in dataContext.AllContainers)
                {
                    OrderIndex++;
                    item.Value.UpdateContext.Order = OrderIndex;                 //Команды для Подчиненных Объектов
                }

                //И Затем связать данный Контекст с каким-то Флу процессом
                dataContext.AttachBOContextContainerToFlowProcess(FlowProcInfo);

                return dataContext;            
        }

        public static BOContextContainer GetItemDataContext(Type dataContextItemTp
                                                    , FlowProcessInfo flowProcInfo)
        {   //Для 

            Int32 OrderIndex = 1; Int32 LevelToTarget = 1;

            // 1 Cоздать пустой Дата Контекст
            var dataContext = BOContextContainer.New(dataContextItemTp);

            // 2 Обозначение Контекста Данных
            dataContext.Key = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

            // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
            dataContext.TargetContainer.Item = null;   // в этом разница между перегрузкой с объектом IPersistableBO 
            //dataContext.TargetContainer.IsTargetInContext = true;
            dataContext.TargetContainer.ParentDataContextGuid = dataContext.Guid;
            dataContext.TargetContainer.ParentDataContextKey = dataContext.Key;

            // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
            IServiceModel svcManager = dataContextItemTp.GetBObjectServiceModel();
            List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

            // Associated with the current target item other VOs
            List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(dataContextItemTp);

            // Получить BOContainer из простого Type для каждого ассоциированного объекта
            foreach (var item in associatedVObjects)
            {
                BOContainer associatedBOItem = item.GetBObjectContainer();
                 
                //Привязать associatedBOItem  к текущему Контексту Данных
                associatedBOItem.ParentDataContextKey = dataContext.Key;
                associatedBOItem.ParentDataContextGuid = dataContext.Guid;

                dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
            }

            // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
            foreach (var item in dataContext.AllContainers)
            {
                item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
            }

            //Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
            dataContext.TargetContainer.UpdateContext.Order = OrderIndex;     //Первая Команды для Таргета
            foreach (var item in dataContext.AllContainers)
            {
                OrderIndex++;
                item.Value.UpdateContext.Order = OrderIndex;                 //Команды для Подчиненных Объектов
            }

            //И Затем связать данный Контекст с каким-то Флу процессом
            dataContext.AttachBOContextContainerToFlowProcess(flowProcInfo);

            return dataContext;
        }



        /// <summary>
        /// Составить Дата Контект для какого-то IPersistableBO
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="FlowProcInfo"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem)
        {
            
                //Не связываем ни с каким флу процецессом данный Контекст
                //FlowProcessInfo FlowProcInfo = new FlowProcessInfo( Guid.NewGuid() , FlowProcessMode.Update , NavigationServiceEx.Current.CurrentPage.PublishedInfo.HostService ); // page.PublishedInfo.HostService

                Int32 OrderIndex = 1; Int32 LevelToTarget = 1;      //
                Type TargetType = dataContextItem.GetType();

                // 1 Cоздать пустой Дата Контекст
                var dataContext = BOContextContainer.New(dataContextItem);

                // 2 Обозначение Контекста Данных
                dataContext.Key = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

                // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
                dataContext.TargetContainer.Item = dataContextItem;
                //dataContext.TargetContainer.IsTargetInContext = true;
                dataContext.TargetContainer.ParentDataContextGuid = dataContext.Guid;
                dataContext.TargetContainer.ParentDataContextKey = dataContext.Key;


                // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
                IServiceModel svcManager = TargetType.GetBObjectServiceModel();
                List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

                // Associated with the current target item other VOs
                List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(TargetType);

                // Получить BOContainer из простого Type для каждого ассоциированного объекта
                foreach (var item in associatedVObjects)
                {
                    BOContainer associatedBOItem = item.GetBObjectContainer();
                    associatedBOItem.Collection = new ObservableCollection<IBObject>(); //Инициализация пустым но Экземпляром Коллекции для будующего связывания коллекций

                    // Привязать associatedBOItem  к текущему Контексту Данных
                    associatedBOItem.ParentDataContextKey = dataContext.Key;
                    associatedBOItem.ParentDataContextGuid = dataContext.Guid;

                    dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
                }

                // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
                foreach (var item in dataContext.AllContainers)
                {
                    item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
                }


                // Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
                dataContext.TargetContainer.UpdateContext.Order = OrderIndex;     //Первая Команды для Таргета
                foreach (var item in dataContext.AllContainers)
                {
                    OrderIndex++;
                    item.Value.UpdateContext.Order = OrderIndex;                 //Команды для Подчиненных Объектов
                }

                // И Затем связать данный Контекст с каким-то Флу процессом
                // dataContext.AttachDataContextToFlowProcess(FlowProcInfo);

                return dataContext;
            
        }



        /// <summary>
        /// Составить Дата Контект для какого-то IPersistableBO, но исключить из полного набора Ассоциаций Некоторые из них(Коллекции ассоциации)
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="excludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem, params String[] excludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return ExcludeAssociations(context, excludingAssociations);
        }



        /// <summary>
        /// Получить Дата контекст из типа. Чистый  Дата Контекст
        /// </summary>
        /// <param name="dataContextItemTp"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(Type dataContextItemTp)
        {             
                //Не связываем ни с каким флу процецессом данный Контекст
                //FlowProcessInfo FlowProcInfo = new FlowProcessInfo(Guid.NewGuid(), FlowProcessMode.Update, NavigationServiceEx.Current.CurrentPage.PublishedInfo.HostService); // page.PublishedInfo.HostService

                Int32 OrderIndex = 1; Int32 LevelToTarget = 1; //
                Type TargetType = dataContextItemTp;

                // 1 Cоздать пустой Дата Контекст
                var dataContext = BOContextContainer.New(dataContextItemTp);
                
                // 2 Обозначение Контекста Данных
                dataContext.Key = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

                // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
                dataContext.TargetContainer .Item = null;   // в этом разница между перегрузкой с объектом IPersistableBO 
                //dataContext.TargetContainer.IsTargetInContext = true;
                dataContext.TargetContainer.ParentDataContextGuid = dataContext.Guid;
                dataContext.TargetContainer.ParentDataContextKey = dataContext.Key;

                // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
                IServiceModel svcManager = TargetType.GetBObjectServiceModel();
                List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

                // Associated with the current target item other VOs
                List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(TargetType);


                // Получить BOContainer из простого Type для каждого ассоциированного объекта
                foreach (var item in associatedVObjects)
                {
                    BOContainer associatedBOItem = item.GetBObjectContainer();
                     
                    // Привязать associatedBOItem  к текущему Контексту Данных
                    associatedBOItem.ParentDataContextKey = dataContext.Key;
                    associatedBOItem.ParentDataContextGuid = dataContext.Guid;

                    dataContext.AllContainers.Add(associatedBOItem.Key,associatedBOItem);
                }

                // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
                foreach (var item in dataContext.AllContainers)
                {
                    item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
                }

                // Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
                dataContext.TargetContainer.UpdateContext.Order = OrderIndex;     //Первая Команды для Таргета
                foreach (var item in dataContext.AllContainers)
                {
                    OrderIndex++;
                    item.Value.UpdateContext.Order = OrderIndex;                 //Команды для Подчиненных Объектов
                }

                // И Затем связать данный Контекст с каким-то Флу процессом
                // dataContext.AttachDataContextToFlowProcess(FlowProcInfo);
                
                return dataContext;
            
        }



        /// <summary>
        /// Получить Дата контекст из типа, но исключить из полного набора Ассоциаций Некоторые из них(Коллекции ассоциации)
        /// </summary>
        /// <param name="dataContextItemTp"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(Type dataContextItemTp, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItemTp);
            if (context == null) return null;

            return ExcludeAssociations(context, ExcludingAssociations);

        }


        /// <summary>
        /// Получить Контекст Данных для 
        /// FlowProcess.Mode == ADDING . DataContext.TargetBO.Item не заполняется
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public static BOContextContainer GetServiceDataContext(int svcModelIndex, FlowProcessInfo flowProcInfo)
        {

            IServiceModel svcManager = ServiceModel.GetByIndex(svcModelIndex);
            Type TargetType = svcManager.TargetModelBO;
            BOContextContainer dataContext = GetItemDataContext(TargetType, flowProcInfo);

            return dataContext;

        }

        #endregion ---------------------------- GetItem/ServiceDataContext ---------------------------------



        #region ------------------------ Exclude Associations----------------------------

        /// <summary>
        /// Исключить какие-то ассоциации из Дата Контект-а
        /// Для того чтобы осуществить Частичную подгрузку ассоциаций контекста к примеру 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        static BOContextContainer ExcludeAssociations(BOContextContainer dataContext, params String[] ExcludingAssociations)
        {
            if (dataContext == null) return null;
            if (ExcludingAssociations == null || ExcludingAssociations.Length  == 0) return dataContext;

            foreach (var excludingItem in ExcludingAssociations)
            {
                for (Int32 i = 0; i < dataContext.AllContainers.Count; i++)
                {
                    if (dataContext.AllContainers.ElementAt(i).Key == excludingItem)
                    {
                        dataContext.AllContainers.Remove(excludingItem);
                        break;
                    }
                }
            }

            return dataContext;
        }



        /// <summary>
        /// Исключить ассаоциации в  Дата Контексте Навигации- это значит просто обнулить все элементы илючаемых коллекций,
        /// но не удалять из словаря Ассоциаций и не обнулять значение Коллекции по ключу
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
          static BONavContextContaier ExcludeAssociations(BONavContextContaier navDataContext, params String[] ExcludingAssociations)
        {
            if (navDataContext == null) return null;
            if (ExcludingAssociations == null || ExcludingAssociations.Length == 0) return navDataContext;

            foreach (var excludingItem in ExcludingAssociations)
            {
                if (navDataContext.AssociatedCollections.ContainsKey(excludingItem))
                {
                    navDataContext.AssociatedCollections[excludingItem].Clear();
                }
            }

            return navDataContext;
        }


        #endregion ------------------------ Exclude Associations----------------------------



        #region  ---------------------------- GetItemBONavContextContaier -------------------------------------

        /// <summary>
        /// Получить контекст навигации , и ещё мы исключаем набор Коллекций ассоциации
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(IPersistableBO dataContextItem, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            BONavContextContaier NavigationDtContext = context.ToNavigationContext();

            return ExcludeAssociations(NavigationDtContext, ExcludingAssociations);
        }


        /// <summary>
        /// Получить контекст навигации 
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(IPersistableBO dataContextItem)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return context.ToNavigationContext();
        }


        /// <summary>
        /// Получить контекст навигации, где Target.Item == null, и ещё мы исключаем набор Коллекций ассоциации
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(Type dataContextItem, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            BONavContextContaier NavigationDtContext = context.ToNavigationContext();

            return ExcludeAssociations(NavigationDtContext, ExcludingAssociations);
        }


        /// <summary>
        /// Получить контекст навигации, где Target.Item == null   
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(Type dataContextItem)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return context.ToNavigationContext();
        }


        #endregion ---------------------------- GetItemBONavContextContaier -------------------------------------



        #region  ------------------------------- Обновить Дата Контекст ---------------------------

       

        /// <summary>
        ///  Просто обновить данными из контекста BOContextContainer наш BONavContextContaier DataContext
        /// </summary>
        /// <param name="DataContext"></param>
        /// <param name="ID"></param>
        public static void UpdateBONavContextContaier(ref BONavContextContaier NavDataContext, BOContextContainer dataContext)
        {
           
            if (NavDataContext == null || dataContext == null) return;

            //Для Таргета - всегда обновляется если он есть у Дата контекста
            if (dataContext.TargetContainer  != null && dataContext.TargetContainer .Item != null)
            {
                NavDataContext.TargetItem = dataContext.TargetContainer.Item;
            }

            //для коллекций- все коллекции которые есть  проверяем слева и справа и в случае наличия-совпадения обновляем
            foreach (var item in dataContext.AllContainers)
            {
                Type itemType = item.Value.Collection.GetItemType();

                if (itemType != null && NavDataContext.AssociatedCollections.ContainsKey(itemType.Name))
                {
                    NavDataContext.AssociatedCollections[itemType.Name].Reload(item.Value.Collection);
                }
            }           
        }

        #endregion --------------------------- Обновить Дата Контекст --------------------------



        






    }



}
