﻿using DDDD.Core.Collections.Concurrent;
using DDDD.Core.Diagnostics;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.Data.DA2.Mapping;
using System.Globalization;
using System.Collections;

namespace DDDD.Core.Data.DA2
{   /* Use of ServiceModel:
          
    // SERVICE MODELS WILL BE LOADED AUTOMATICALLY 
    // From DAL ASSEMBLY on AppBootstrapper.LoadAppComponents or Loadxxx
    // DAL assembly - sshould be one separate assembly.


        //Register Component Class - ServiceModels - will be loaded from DAL assembly
        // DAL assembly - sshould be one separate assembly.
        ComponentsContainer.RegisterComponentClass<IServiceModel>(
            ComponentClassEn.ServiceModels.S(), Init_DCServices_ComponentsClass);
             
     */


    public abstract class ServiceModel  : IServiceModel
    {
        protected ServiceModel()
        {
            Initialize();
        }

        protected virtual void Initialize()
        {
            var tp = GetType();
            SvcModelType = tp;
            SvcModelKey = tp.Name;
            SvcModelIndex = NamingConvention.GetServiceModelIndex(SvcModelKey);
        }
       

        //protected static readonly object syncRoot = new object();

        #region ------------ CACHE and GET METHHODS -----------------

        /// <summary>
        ///  Cache of ServiceModel by   ServiceModelIndex
        /// </summary>
        static readonly SafeDictionary2<int, IServiceModel> ServiceModelsCache =
            new SafeDictionary2<int, IServiceModel>();
        
        internal static void AddSvcModelOnAppBoot(IServiceModel svcModelItem)
        {
            ServiceModelsCache.Add(svcModelItem.SvcModelIndex, svcModelItem);
        }

        /// <summary>
        /// Get ServiceModel by its Index
        /// </summary>
        /// <param name="svcModelIndex"></param>
        /// <returns></returns>
        public static IServiceModel GetByIndex(int svcModelIndex)
        {
           return  ServiceModelsCache[svcModelIndex];
        }


        #endregion ------------ CACHE and GET METHHODS -----------------



        /// <summary>
        /// ServiceModel Type
        /// </summary>
        public Type SvcModelType
        { get; private set; }


        /// <summary>
        /// ServiceModel Type - standart or specific service Model
        /// </summary>
        public abstract ServiceModelTypeEn ServiceModelType { get; }

        /// <summary>
        /// Generate DB Script from snippet text - as usually used in Spicific Service Moel
        /// </summary>
        /// <param name="marekerReplaceVals"></param>
        /// <returns></returns>
        public virtual string GenerateDBScript(Dictionary<string, string> marekerReplaceVals)
        {
            throw new NotImplementedException($" {SvcModelKey} :  GenerateDBScript() - custom script - is not implemented ");
        }


        /// <summary>
        /// ServiceModel  Key is  its Type.Name
        /// </summary>
        public string SvcModelKey
        { get; private set; }


        /// <summary>
        /// ServiceModel Index is x number from -  S[x]_[Name]- Naming rule.
        /// <para/>For EX: [S23_Suppliers] -  x is [23].
        /// </summary>
        public int SvcModelIndex
        { get; private set; } 

        

        /// <summary>
        /// Target  BO in this ServiceModel 
        /// </summary>
        public virtual Type TargetModelBO
        { get; protected set; }


        /// <summary>
        /// ServiceModel BO Contracts.
        /// </summary>
        protected virtual Dictionary<string, Type> SvcModelContracts
        { get; set; } = new Dictionary<string, Type>()
        {   
            { "", typeof(object)  } 
        };


        /// <summary>
        /// For Specific Service Models we also need to fill this  Dictionary 
        /// with ComponentName-role as key 
        /// </summary>
        protected virtual Dictionary<string, Type> Components
        { get; set; } = new Dictionary<string, Type>();



        /// <summary>
        /// Get BO Contract - Type by Type.Name only value from current ServiceModel; this value is Table or View name.
        /// </summary>
        /// <param name="boNameOnly">This value is DB Table or View name.<</param>
        /// <returns></returns>
        public Type GetBOContractByName(string boNameOnly)
        {
            if (boNameOnly.IsNotWorkable() || SvcModelContracts.NotContainsKey(boNameOnly))
                throw new InvalidOperationException(" Service Model do Not COntain Key: " + boNameOnly);
           
            return SvcModelContracts[boNameOnly]; 
        }


        /// <summary>
        /// Get Compomnent contract Name by componentKey - if Service Model is  Specific Service Model
        /// <para/>  Standart Service Model  cant use this way  to get contracts 
        /// </summary>
        /// <param name="modelComponentKey">EX:  ItemTypeEntity/ HierarchyEntity/ AncestorsView  in Hierarchy Service Model for example </param>
        /// <returns></returns>
        public string GetComponentName(string modelComponentKey)
        {
            return GetComponentType(modelComponentKey).Name;
        }


        /// <summary>
        /// Get Compomnent contract by componentKey - if Service Model is  Specific Data Structure Model
        /// <para/>  Standart Service Model  cant use this way  to get contracts 
        /// </summary>
        /// <param name="modelComponentKey"> EX:  ItemTypeEntity/ HierarchyEntity/ AncestorsView  in Hierarchy Service Model for example</param>
        /// <returns></returns>
        public Type GetComponentType(string modelComponentKey)
        {
            if (modelComponentKey.IsNotWorkable() || Components.NotContainsKey(modelComponentKey))
                throw new InvalidOperationException(" Components  do Not COntain Key: " + modelComponentKey);


            return Components[modelComponentKey];
        }



        /// <summary>
        /// Get BO Contract - Type by Type.Name only value  from all ServiceModels:
        /// <para/> Detect SelerviceModel Index from Type.Name and then
        ///  ,Get Type from  ServiceModel with such index. 
        /// </summary>
        /// <param name="boNameOnly"> This value is DB Table or View name.</param>
        /// <returns></returns>
        public static Type GetBOContractByNameGlobal(string boNameOnly)
        {
            var index = NamingConvention.GetServiceModelIndex(boNameOnly);
            return ServiceModelsCache[index].GetBOContractByName(boNameOnly);
        }


        //public abstract List<Type> GetModelContracts();


        


#if SERVER // Только на сервере

        /// <summary>
        /// Resource Manager that contains localized names and descriptions of contracts and their properties
        /// </summary>
       public virtual ResourceManager ResourceManager
       { get; protected set; }


#endif



        /// <summary>
        /// Get BO by Role.
        /// </summary>
        /// <param name="boRole"></param>
        /// <returns></returns>
        public List<Type> GetObjectsByRole(BORoleEn boRole)
        {
            var resltList = new List<Type>();
            foreach (var boContract in SvcModelContracts)
            {
                if (boContract.Value.IsBObjectInRole(boRole))
                {
                    resltList.Add(boContract.Value);
                }
            }             
            return resltList;
        }






        #region  ---------------------------- GetItem/ServiceDataContext ----------------------------------

        /// <summary>
        /// Для получения контекста Данных для Какого-то конкретного объекта
        /// FlowProcess.Mode == ADDING . DataContext.TargetBO.Item не заполняется
        /// </summary>
        /// <param name="objectInstance"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem
                                                    , FlowProcessInfo FlowProcInfo)
        {   //Для 

            Int32 OrderIndex = 1; Int32 LevelToTarget = 1;

            // 1 Cоздать пустой Дата Контекст
            var dataContext = BOContextContainer.New(dataContextItem);

            // 2 Обозначение Контекста Данных
            dataContext.KeyStr = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

            // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
            dataContext.TargetContainer.Item = dataContextItem;
            //dataContext.TargetContainer.IsTargetInContext = true;
            dataContext.TargetContainer.ParentDataContextGuid = dataContext.KeyGd;
            dataContext.TargetContainer.ParentDataContextKey = dataContext.KeyStr;

            // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
            IServiceModel svcManager = dataContextItem.GetServiceModel();
            List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

            // Associated with the current target item other VOs
            List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(dataContextItem.Contract);

            // Получить BOContainer из простого Type для каждого ассоциированного объекта
            foreach (var item in associatedVObjects)
            {
                BOContainer associatedBOItem = item.GetBObjectContainer();
               
                //Привязать associatedBOItem  к текущему Контексту Данных
                associatedBOItem.ParentDataContextKey = dataContext.KeyStr;
                associatedBOItem.ParentDataContextGuid = dataContext.KeyGd;

                dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
            }

            // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
            foreach (var item in dataContext.AllContainers)
            {
                item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
            }


            //Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
            dataContext.TargetContainer.Update.Order = OrderIndex;     //Первая Команды для Таргета
            foreach (var item in dataContext.AllContainers)
            {
                OrderIndex++;
                item.Value.Update.Order = OrderIndex;                 //Команды для Подчиненных Объектов
            }

            //И Затем связать данный Контекст с каким-то Флу процессом
            dataContext.AttachBOContextContainerToFlowProcess(FlowProcInfo);

            return dataContext;
        }

        public static BOContextContainer GetItemDataContext(Type dataContextItemTp
                                                    , FlowProcessInfo flowProcInfo)
        {   //Для 

            Int32 OrderIndex = 1; Int32 LevelToTarget = 1;

            // 1 Cоздать пустой Дата Контекст
            var dataContext = BOContextContainer.New(dataContextItemTp);

            // 2 Обозначение Контекста Данных
            dataContext.KeyStr = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

            // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
            dataContext.TargetContainer.Item = null;   // в этом разница между перегрузкой с объектом IPersistableBO 
            //dataContext.TargetContainer.IsTargetInContext = true;
            dataContext.TargetContainer.ParentDataContextGuid = dataContext.KeyGd;
            dataContext.TargetContainer.ParentDataContextKey = dataContext.KeyStr;

            // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
            IServiceModel svcManager = dataContextItemTp.GetBObjectServiceModel();
            List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

            // Associated with the current target item other VOs
            List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(dataContextItemTp);

            // Получить BOContainer из простого Type для каждого ассоциированного объекта
            foreach (var item in associatedVObjects)
            {
                BOContainer associatedBOItem = item.GetBObjectContainer();

                //Привязать associatedBOItem  к текущему Контексту Данных
                associatedBOItem.ParentDataContextKey = dataContext.KeyStr;
                associatedBOItem.ParentDataContextGuid = dataContext.KeyGd;

                dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
            }

            // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
            foreach (var item in dataContext.AllContainers)
            {
                item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
            }

            //Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
            dataContext.TargetContainer.Update.Order = OrderIndex;     //Первая Команды для Таргета
            foreach (var item in dataContext.AllContainers)
            {
                OrderIndex++;
                item.Value.Update.Order = OrderIndex;                 //Команды для Подчиненных Объектов
            }

            //И Затем связать данный Контекст с каким-то Флу процессом
            dataContext.AttachBOContextContainerToFlowProcess(flowProcInfo);

            return dataContext;
        }



        /// <summary>
        /// Составить Дата Контект для какого-то IPersistableBO
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="FlowProcInfo"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem)
        {

            //Не связываем ни с каким флу процецессом данный Контекст
            //FlowProcessInfo FlowProcInfo = new FlowProcessInfo( Guid.NewGuid() , FlowProcessMode.Update , NavigationServiceEx.Current.CurrentPage.PublishedInfo.HostService ); // page.PublishedInfo.HostService

            Int32 OrderIndex = 1; Int32 LevelToTarget = 1;      //
            Type TargetType = dataContextItem.GetType();

            // 1 Cоздать пустой Дата Контекст
            var dataContext = BOContextContainer.New(dataContextItem);

            // 2 Обозначение Контекста Данных
            dataContext.KeyStr = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

            // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
            dataContext.TargetContainer.Item = dataContextItem;
            //dataContext.TargetContainer.IsTargetInContext = true;
            dataContext.TargetContainer.ParentDataContextGuid = dataContext.KeyGd;
            dataContext.TargetContainer.ParentDataContextKey = dataContext.KeyStr;


            // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
            IServiceModel svcManager = TargetType.GetBObjectServiceModel();
            List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

            // Associated with the current target item other VOs
            List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(TargetType);

            // Получить BOContainer из простого Type для каждого ассоциированного объекта
            foreach (var item in associatedVObjects)
            {
                BOContainer associatedBOItem = item.GetBObjectContainer();
               
                // Привязать associatedBOItem  к текущему Контексту Данных
                associatedBOItem.ParentDataContextKey = dataContext.KeyStr;
                associatedBOItem.ParentDataContextGuid = dataContext.KeyGd;

                dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
            }

            // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
            foreach (var item in dataContext.AllContainers)
            {
                item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
            }


            // Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
            dataContext.TargetContainer.Update.Order = OrderIndex;     //Первая Команды для Таргета
            foreach (var item in dataContext.AllContainers)
            {
                OrderIndex++;
                item.Value.Update.Order = OrderIndex;                 //Команды для Подчиненных Объектов
            }

            // И Затем связать данный Контекст с каким-то Флу процессом
            // dataContext.AttachDataContextToFlowProcess(FlowProcInfo);

            return dataContext;

        }



        /// <summary>
        /// Составить Дата Контект для какого-то IPersistableBO, но исключить из полного набора Ассоциаций Некоторые из них(Коллекции ассоциации)
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="excludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(IPersistableBO dataContextItem, params String[] excludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return ExcludeAssociations(context, excludingAssociations);
        }



        /// <summary>
        /// Получить Дата контекст из типа. Чистый  Дата Контекст
        /// </summary>
        /// <param name="dataContextItemTp"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(Type dataContextItemTp)
        {
            //Не связываем ни с каким флу процецессом данный Контекст
            //FlowProcessInfo FlowProcInfo = new FlowProcessInfo(Guid.NewGuid(), FlowProcessMode.Update, NavigationServiceEx.Current.CurrentPage.PublishedInfo.HostService); // page.PublishedInfo.HostService

            Int32 OrderIndex = 1; Int32 LevelToTarget = 1; //
            Type TargetType = dataContextItemTp;

            // 1 Cоздать пустой Дата Контекст
            var dataContext = BOContextContainer.New(dataContextItemTp);

            // 2 Обозначение Контекста Данных
            dataContext.KeyStr = dataContext.TargetContainer.Key;  // Ключ контекста такой же как и у целевого Объекта

            // 3  Присвоить TargetBO. Обозначим TargetBO как Целевой ОБъект и Привяжем его самого к созданному контексту данных 
            dataContext.TargetContainer.Item = null;   // в этом разница между перегрузкой с объектом IPersistableBO 
                                                       //dataContext.TargetContainer.IsTargetInContext = true;
            dataContext.TargetContainer.ParentDataContextGuid = dataContext.KeyGd;
            dataContext.TargetContainer.ParentDataContextKey = dataContext.KeyStr;

            // Где для других объектов IPersistableBO  данный будет в списке Parents-ов в атрибуте роли свойства с  ролью VOFkey и именем таргет типа
            IServiceModel svcManager = TargetType.GetBObjectServiceModel();
            List<Type> allServiceVObjects = svcManager.GetObjectsByRole(BORoleEn.VObject);

            // Associated with the current target item other VOs
            List<Type> associatedVObjects = allServiceVObjects.GetAssociatedVObjects(TargetType);


            // Получить BOContainer из простого Type для каждого ассоциированного объекта
            foreach (var item in associatedVObjects)
            {
                BOContainer associatedBOItem = item.GetBObjectContainer();

                // Привязать associatedBOItem  к текущему Контексту Данных
                associatedBOItem.ParentDataContextKey = dataContext.KeyStr;
                associatedBOItem.ParentDataContextGuid = dataContext.KeyGd;

                dataContext.AllContainers.Add(associatedBOItem.Key, associatedBOItem);
            }

            // Проставить Уровень Удаленности от Таргет - Пока не играет никакой роли
            foreach (var item in dataContext.AllContainers)
            {
                item.Value.AssociationInfo.LevelToTarget = LevelToTarget;
            }

            // Пронумеровать BOinfo.UpdateContext.Order   Для управления очередностью исполнения команд в транзакции на сервере 1-n                
            dataContext.TargetContainer.Update.Order = OrderIndex;     //Первая Команды для Таргета
            foreach (var item in dataContext.AllContainers)
            {
                OrderIndex++;
                item.Value.Update.Order = OrderIndex;                 //Команды для Подчиненных Объектов
            }

            // И Затем связать данный Контекст с каким-то Флу процессом
            // dataContext.AttachDataContextToFlowProcess(FlowProcInfo);

            return dataContext;

        }



        /// <summary>
        /// Получить Дата контекст из типа, но исключить из полного набора Ассоциаций Некоторые из них(Коллекции ассоциации)
        /// </summary>
        /// <param name="dataContextItemTp"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BOContextContainer GetItemDataContext(Type dataContextItemTp, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItemTp);
            if (context == null) return null;

            return ExcludeAssociations(context, ExcludingAssociations);

        }


        /// <summary>
        /// Получить Контекст Данных для 
        /// FlowProcess.Mode == ADDING . DataContext.TargetBO.Item не заполняется
        /// </summary>
        /// <param name="service"></param>
        /// <returns></returns>
        public static BOContextContainer GetServiceDataContext(int svcModelIndex, FlowProcessInfo flowProcInfo)
        {

            IServiceModel svcManager = ServiceModel.GetByIndex(svcModelIndex);
            Type TargetType = svcManager.TargetModelBO;
            BOContextContainer dataContext = GetItemDataContext(TargetType, flowProcInfo);

            return dataContext;

        }

        #endregion ---------------------------- GetItem/ServiceDataContext ---------------------------------



        #region ------------------------ Exclude Associations----------------------------

        /// <summary>
        /// Исключить какие-то ассоциации из Дата Контект-а
        /// Для того чтобы осуществить Частичную подгрузку ассоциаций контекста к примеру 
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        static BOContextContainer ExcludeAssociations(BOContextContainer dataContext, params String[] ExcludingAssociations)
        {
            if (dataContext == null) return null;
            if (ExcludingAssociations == null || ExcludingAssociations.Length == 0) return dataContext;

            foreach (var excludingItem in ExcludingAssociations)
            {
                for (Int32 i = 0; i < dataContext.AllContainers.Count; i++)
                {
                    if (dataContext.AllContainers.ElementAt(i).Key == excludingItem)
                    {
                        dataContext.AllContainers.Remove(excludingItem);
                        break;
                    }
                }
            }

            return dataContext;
        }



        /// <summary>
        /// Исключить ассаоциации в  Дата Контексте Навигации- это значит просто обнулить все элементы илючаемых коллекций,
        /// но не удалять из словаря Ассоциаций и не обнулять значение Коллекции по ключу
        /// </summary>
        /// <param name="dataContext"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        static BONavContextContaier ExcludeAssociations(BONavContextContaier navDataContext, params String[] ExcludingAssociations)
        {
            if (navDataContext == null) return null;
            if (ExcludingAssociations == null || ExcludingAssociations.Length == 0) return navDataContext;

            foreach (var excludingItem in ExcludingAssociations)
            {
                if (navDataContext.AssociatedCollections.ContainsKey(excludingItem))
                {
                    navDataContext.AssociatedCollections[excludingItem].Clear();
                }
            }

            return navDataContext;
        }


        #endregion ------------------------ Exclude Associations----------------------------



        #region  ---------------------------- GetItemBONavContextContaier -------------------------------------

        /// <summary>
        /// Получить контекст навигации , и ещё мы исключаем набор Коллекций ассоциации
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(IPersistableBO dataContextItem, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            BONavContextContaier NavigationDtContext = context.ToNavigationContext();

            return ExcludeAssociations(NavigationDtContext, ExcludingAssociations);
        }


        /// <summary>
        /// Получить контекст навигации 
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(IPersistableBO dataContextItem)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return context.ToNavigationContext();
        }


        /// <summary>
        /// Получить контекст навигации, где Target.Item == null, и ещё мы исключаем набор Коллекций ассоциации
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(Type dataContextItem, params String[] ExcludingAssociations)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            BONavContextContaier NavigationDtContext = context.ToNavigationContext();

            return ExcludeAssociations(NavigationDtContext, ExcludingAssociations);
        }


        /// <summary>
        /// Получить контекст навигации, где Target.Item == null   
        /// </summary>
        /// <param name="dataContextItem"></param>
        /// <param name="ExcludingAssociations"></param>
        /// <returns></returns>
        public static BONavContextContaier GetItemBONavContextContaier(Type dataContextItem)
        {
            BOContextContainer context = GetItemDataContext(dataContextItem);
            if (context == null) return null;

            return context.ToNavigationContext();
        }


        #endregion ---------------------------- GetItemBONavContextContaier -------------------------------------



        #region  ------------------------------- Обновить Дата Контекст ---------------------------



        /// <summary>
        ///  Просто обновить данными из контекста BOContextContainer наш BONavContextContaier DataContext
        /// </summary>
        /// <param name="DataContext"></param>
        /// <param name="ID"></param>
        public static void UpdateBONavContextContaier(ref BONavContextContaier NavDataContext, BOContextContainer dataContext)
        {

            if (NavDataContext == null || dataContext == null) return;

            //Для Таргета - всегда обновляется если он есть у Дата контекста
            if (dataContext.TargetContainer != null && dataContext.TargetContainer.Item != null)
            {
                NavDataContext.TargetItem = dataContext.TargetContainer.Item;
            }

            //для коллекций- все коллекции которые есть  проверяем слева и справа и в случае наличия-совпадения обновляем
            foreach (var item in dataContext.AllContainers)
            {
                Type itemType = item.Value.Collection.GetItemType();

                if (itemType != null && NavDataContext.AssociatedCollections.ContainsKey(itemType.Name))
                {
                    NavDataContext.AssociatedCollections[itemType.Name].Reload(item.Value.Collection);
                }
            }
        }



        #endregion --------------------------- Обновить Дата Контекст --------------------------



        #region ------------ LOCALIZATION -------------------
        
        // GetString for boContract and its Property
        // GetCultureSet - all culture strings     
         

#if SERVER || WPF
              

        /// <summary>
        ///  Resource Manager of current ServiceModel contains :  
        ///  BoNameLoc, BoDescLoc,  BoPropNameLoc , BoPropDescLoc
        /// </summary>
        public virtual ResourceManager ResManager
        { get; protected set; } //= new  ResourceManager("CRM.DOM.DAL.NEW.S1_Geography.S1_Geography", typeof(S1_Geography).Assembly);



#endif
        /// <summary>
        /// Get BO localisation string for its Name and  Description
        /// </summary>
        /// <param name="pointSelector"></param>
        /// <param name="cultureKey"></param>
        /// <param name="boContract"></param>
        /// <returns></returns>
        public string GetBOLocalStr(BOLocalKeyEn pointSelector, string cultureKey, Type boContract )
        {
            if (ResManager == null) return null;

            //  BoNameLoc\boContractName
            //  BoDescLoc\boContractName 
            return ResManager.GetString(pointSelector.S() + @"\" + boContract.Name , new CultureInfo(cultureKey));
        }

        /// <summary>
        /// Get BO.Propperty localisation string for its Name and  Description 
        /// </summary>
        /// <param name="pointSelector"></param>
        /// <param name="cultureKey"></param>
        /// <param name="boContract"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>

        public string GetBOPropLocalStr(BOPropLocalKeyEn pointSelector, string cultureKey, Type boContract,string propertyName)
        {
            if (ResManager == null) return null;
            
            // BoPropDescLoc\boContractName|propertyName
            // BoPropNameLoc\boContractName|propertyName
            return ResManager.GetString(pointSelector.S() + @"\" + boContract.Name + "|" + propertyName , new CultureInfo(cultureKey)  );
        }


        /// <summary>
        /// Get resouces for culture as  Dictionary string, string
        /// </summary>
        /// <param name="cultureKey"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetResourceForCulture(string cultureKey)
        {
            if (ResManager == null) return null;

            var resSet = ResManager.GetResourceSet(new CultureInfo(cultureKey), true, true);
            var cultureResources = new  Dictionary<string,string>();
            resSet.AddToDictionary(cultureResources);           
            return cultureResources;
        }


        

        #endregion ------------ LOCALIZATION -------------------

    }
}
