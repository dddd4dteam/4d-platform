﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Meta.Systems;
using Core.Commanding;
using Core.Meta.Install;
using Core.Meta.Tiers;
using Core.Meta.Versions;
using Core.Identification;
using Core;

#if SERVER

using Core.Configuration;
using Core.Configuration.Sections;
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using Core.Meta.VVM;

#endif

using Core.Meta.Parametrization;
using Core.Composition.Commanding;
using Core.Composition.DataServices;
using Core.Data.Query;
using Core.BoModel;

using System.Collections;
using Core.Composition.Modularity;
using Core.InfrastructureService;
using System.Resources;
using Core.Localization;
using System.Collections.ObjectModel;


namespace Core.CommandManager
{
    /// <summary>
    /// Менеджер Комманд Core-а
    /// </summary>
    [CommandManager(CurrentCommandManager: CommandManager_RegisterEn.CoreCommandManager)]
    public class CoreCommandManager : ICommandManager
    {

#if (SILVELRIGHT || WINDOWS_PHONE)

        public static CoreCommandManager Current
        {
            get { return SystemDispatcher.CoreCmdManager; }
        }
#endif

        /// <summary>
        /// Текущий Менеджер Иерархии
        /// </summary>
        public static CoreCommandManager Current
        {
            get { return SystemDispatcher.CoreCmdManager; }
        }


        /// <summary>
        /// Имя Менеджера Комманд 
        /// </summary>
        public String Name
        {
            get { return this.GetType().GetTypeAttribute<CommandManagerAttribute>().FirstOrDefault().CommandManager.ToString(); }
        }
       

        /// <summary>
        /// Команды менеджера Команд
        /// </summary>
        public enum CommandsEn
        {
            NotDefined,
            
            GetUserAuthenticatedServerResources,
            
            GetUserResourcesInPassingMode,
            
            GetAllRegisters,
            
            LoadModulesResources,
            
            GetRolesInGroup,

            GetUsersInGroup
            
        }


        /// <summary>
        /// Параметры Команд менеджера Команд
        /// </summary>
        public enum CommandParamsEn
        {   
            Param1,
            
            NotDefined,
            
            UserName,
            
            Password,
            
            Modules,
            
            Services,
            
            Culture,
            
            UserGroupName
        }


#region --------- command 1 GetUserAuthenticatedServerResources------------------


#if SERVER  // ----- SERVER PART ---------


        /// <summary>
        /// Получить Все досптупные пользователю Ресурсы по его аутентификации/авторизации 
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetUserAuthenticatedServerResources", CommandSideEn.Server)]
        public CommandContainer GetUserAuthenticatedServerResources( ref CommandContainer command)
        {   
            try
            {
                //  -------- 1  Getting Parameters ( if Using Command with Params) ------
                //Проверка параметра Имени пользователя
                String userName = command.GetParam<String>(CommandParamsEn.UserName);
                //Проверка параметра Пароля
                String password = command.GetParam<String>(CommandParamsEn.Password);


                //------------2  Custom Command Processing -------------------
                UserAuthenticatedResources resultInfoContainer = new UserAuthenticatedResources();
               
                //Получаем пользовательский аккаунт                
                resultInfoContainer.UserAccount = SystemDispatcher.Instance.Composition.IdentificationProvider.GetUserAccountInfo(userName, password, AuthenticationProviderTypeEn.AspNetProvider); // "v.shumeyko",
                if (resultInfoContainer.UserAccount == null)
                {
                    command.SayCommandThrowUnknownError();
                    command.ProcessingInfo.ServerExceptionMessage = "Пользователь с такими логином не зарегистрирован в системе";
                    return command;
                }
                if (resultInfoContainer.UserAccount.UserAuthenticationInfo.IsLockedOut)
                {
                    command.SayCommandThrowUnknownError();
                    command.ProcessingInfo.ServerExceptionMessage = "Пользователь заблокирован. Повторите попытку через 30 минут.";
                    return command;
                }

                //Получить текущую серверную дату 
                if (resultInfoContainer.UserAccount != null)
                { resultInfoContainer.UserAccount.CurrentServerDate = DateTime.Today; }

                if (resultInfoContainer.UserAccount.UserDomainInfo == null)
                {
                    command.SayCommandThrowUnknownError();
                    return command;
                }

                //Получаем список доступных модулей : 1 Из зарелизенных Модулей 2 Получаем доступные для Пользователя 
                List<InstallContextElementTp> enabledUserModules = SystemDispatcher.Instance.ConfigDispatcher.GetReleasedModules(TierEn.WebServerTier, ElementComponentEn.ServerProvider, ReleaseStageEn.ReleaseCandidate).
                                    GetEnabledElements(resultInfoContainer.UserAccount.UserDomainInfo);

                resultInfoContainer.Menus = enabledUserModules.GetElementsMenuSection();


                // Custom Subordinate Users Loading into  UserAuthenticatedResources container 
                LoadCustomSubordinateUsersModel(ref   resultInfoContainer);




                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                command.Result.Item = resultInfoContainer;
                
                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            { 
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }

        }


#endif  // ---------------------- CLIENT PART ------------------------


        /// <summary>
        /// Клиентская часть метода ПОлучения доступных ресурсов пользователя - Упаковка Команды для Исполнения команды сервером; ; OUTPUT - ResultDataType.Item
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetUserAuthenticatedServerResources", CommandSideEn.Client)]
        public CommandContainer GetUserAuthenticatedServerResources(String UserName, String Password)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetUserAuthenticatedServerResources, CommandManager_RegisterEn.CoreCommandManager);
            
            try
            {
            // ----------------  1 Adding Parameters  ----------------------------- 
                outcomand.AddParam(CommandParamsEn.UserName, UserName);
                outcomand.AddParam(CommandParamsEn.Password, Password);
            //------------------2 Setting DataItems ------------------------------


            //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetUserAuthenticatedServerResources");     
            }
        }


#endregion ------------------------------ 1 GetUserAuthenticatedServerResources------------------------


#region --------- command 2 GetAllRegisters------------------


#if SERVER  // ----- SERVER PART ---------


        /// <summary>
        /// Получить Все досптупные регистры
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetAllRegisters", CommandSideEn.Server)]
        public CommandContainer GetAllRegisters(ref CommandContainer command)
        {
            try
            {
                BOInfoTp rez = new BOInfoTp();
                List<Type> Registers = SystemDispatcher.Instance.GetObjectsFromDALByRole(Meta.BO.BORoleEn.Register);

                using (DbManager dbMngr = new DbManager(new SqlDataProvider(), ConfigurationDispatcher.DomainConnectionString))
                {
                    //Проверка параметра Имени пользователя
                    DBSelectQuery query = DBSelectQuery.SelectAll();
                    command.Result.DictionaryByInt = new Dictionary<int, object>();
                    for (int i = 0; i < Registers.Count(); i++)
                    {
                        query = DBSelectQuery.SelectAll().From(Registers[i]);

                        IEnumerable registerObjectList = SystemDispatcher.CrudCmdManager.SelectCollection(query,dbMngr);

                        command.Result.DictionaryByInt.Add(i, new BOInfoTp()
                        {
                            Key = Registers[i].Name,
                            FullName = Registers[i].FullName,
                            Collection = registerObjectList.ToObservableCollection()// ToList<object>()
                        });
                    }
                }

              
              //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
              
              //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
              return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }

        }


#endif  // ---------------------- CLIENT PART ------------------------


        /// <summary>
        /// Клиентская часть метода ПОлучения доступных регистров - Упаковка Команды для Исполнения команды сервером
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetAllRegisters", CommandSideEn.Client)]
        public CommandContainer GetAllRegisters()
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetAllRegisters, CommandManager_RegisterEn.CoreCommandManager);

            try
            {
                
                // ----------------  1 Adding Parameters  ----------------------------- 
               
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetUserAuthenticatedServerResources");
            }
        }


        #endregion ------------------------------ 2 GetAllRegisters------------------------
        
        
#region --------- command 3 LoadModulesResources-----------------


#if SERVER  // ----- SERVER PART ---------


        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("LoadModulesResources", CommandSideEn.Server)]
        public CommandContainer LoadModulesResources( ref CommandContainer command)
        {   
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------
                
                //Результатный словарь
                Dictionary<String,String> commonServiceResources = new Dictionary<string,string>();


                List<String> ModulesString = command.GetCollectionParam<String>(CommandParamsEn.Modules); //ServiceModelManager_RegisterEn
                List<ServiceModelManager_RegisterEn> Modules = new List<ServiceModelManager_RegisterEn>();
                foreach (string item in ModulesString)
                { Modules.Add((ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), item, true)); }

                                
                //List<ServiceModelManager_RegisterEn> Modules =                 
                List<String> ServicesString = command.GetCollectionParam<String>(CommandParamsEn.Services);
                List<ServiceModelManager_RegisterEn> Services = new List<ServiceModelManager_RegisterEn>();
                foreach (string item in ServicesString)
                { Services.Add((ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), item, true)); }


               
                String cultureString = command.GetParam<String>(CommandParamsEn.Culture);
                CultureKeys culture = (CultureKeys)Enum.Parse(typeof(CultureKeys), cultureString, true);

                //1 Собрать ресурсы по необходимым моделям сервисов
                foreach (var item in Services)
                {
                    IServiceModelManager ItemServiceModelManager = SystemDispatcher.Instance.Composition.GetServiceModelManager(item);                     
                    ResourceSet resourceSet = ItemServiceModelManager.ResourceManager.GetResourceSet(culture.GetCulture(), true, true);
                    if (resourceSet != null)
                    {
                        commonServiceResources.AddResourcesToDictionary(resourceSet);
                    }
                }
                

                //2 Собрать ресурсы по всем Модулям
                foreach (var item in Modules)
                {
                    IModuleExt module = SystemDispatcher.Instance.Composition.GetModule(item);
                    if (module == null)
                    {
                        throw new InvalidOperationException(String.Format(@"Server Part of {0} Module  was not loaded  from module Directory: {1}", item.ToString(), BootSettingsBuilder.GetComposedSt(CompositionSettingsEn.DomModulesDir)));
                    }

                    ResourceSet ModuleResources = module.ResourceManager.GetResourceSet(culture.GetCulture(), true, true);
                    if (ModuleResources != null)
                    {
                        commonServiceResources.AddResourcesToDictionary(ModuleResources);
                    }    
                }               
                
                command.Result.DictionaryByString = commonServiceResources.ToObjectDictionary<String>();
                

                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            { 
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }


#endif  // ---------------------- CLIENT PART ------------------------


        /// <summary>
        /// CLIENT COMMAND PART ; OUTPUT - ResultDataType.DictionaryByString 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("LoadModulesResources", CommandSideEn.Client)]
        public CommandContainer LoadModulesResources(List<ServiceModelManager_RegisterEn> Modules, List<ServiceModelManager_RegisterEn>  Services, CultureKeys culture)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.LoadModulesResources, CommandManager_RegisterEn.CoreCommandManager);

            try
            {
                //Modules.  .To<string>();
                // ----------------  1 Adding Parameters  ----------------------------- 
                List<String> ModulesString = new List<String>();
                foreach (object item in Modules)
                { ModulesString.Add(item.ToString()); }
                outcomand.AddCollectionParam(CommandParamsEn.Modules, ModulesString.ToList<object>());


                List<String> ServicesString = new List<String>();
                foreach (object item in Services)
                { ServicesString.Add(item.ToString()); }
                outcomand.AddCollectionParam(CommandParamsEn.Services, ServicesString.ToList<object>());
                
                
                outcomand.AddParam(CommandParamsEn.Culture, culture.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client Method LoadModuleResources");
            }
        }


        #endregion ------------------------------ 3 LoadModuleResources------------------------


    #region --------- command 4 GetRolesInGroup------------------

        //Name: GetUsersInGroup
        // Params: 
        // param1 -  Name1  - Description1
        // param2 -  Name2  - Description2
        
#if SERVER  // ----- SERVER PART ---------
        
        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetRolesInGroup", CommandSideEn.Server)]
        public CommandContainer GetRolesInGroup(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) -------------
                // Проверка параметра Имени пользователя
                String UserGroupNameVal = command.GetParam<String>(CommandParamsEn.UserGroupName);

                if ( UserGroupNameVal == null) 
                { command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(),"Неправильное имя группы ролей");
                  return command;
                }                                

                // Получаем пользовательский аккаунт                
                List<RoleTp>  rolesInGroup = SystemDispatcher.Instance.Composition.IdentificationProvider.GetRolesForGroup( UserGroupNameVal );// GetUserAccountInfo(userName, password, AuthenticationProviderTypeEn.AspNetProvider); // "v.shumeyko",                
                if( rolesInGroup != null && rolesInGroup.Count > 0 )
                { command.Result.ObservableCollection = rolesInGroup.ToObservableCollection();
                }
                
                
                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();

            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }


#endif  // ---------------------- CLIENT PART ------------------------


        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns> Server: ObservableCollelction<RoleTp> </returns>
        [CommandExecutionMethod("GetRolesInGroup", CommandSideEn.Client)]
        public CommandContainer GetRolesInGroup(String GroupName)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetRolesInGroup, CommandManager_RegisterEn.CoreCommandManager);

            try
            {
                if (GroupName == null) return null;
                
                // ----------------  1 Adding Parameters  ----------------------------- 
                outcomand.AddParam(CommandParamsEn.UserGroupName, GroupName);
                //------------------2 Setting DataItems ------------------------------
                
                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {   throw new Exception("Error in Client Method GetUsersInGroup");
            }
        }

        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        /// <param name="GroupName"></param>
        /// <returns> Server: ObservableCollelction<RoleTp> </returns>
        [CommandExecutionMethod("GetRolesInGroup", CommandSideEn.Client)]
        public CommandContainer GetRolesInGroup(Enum GroupName)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetRolesInGroup, CommandManager_RegisterEn.CoreCommandManager);

            try
            {
                if (GroupName == null) return null;

                // ----------------  1 Adding Parameters  ----------------------------- 
                outcomand.AddParam(CommandParamsEn.UserGroupName, GroupName.ToString());
                //------------------2 Setting DataItems ------------------------------

                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client Method GetUsersInGroup");
            }
        }



        #endregion ------------------------------ 4 GetRolesInGroup------------------------
        
        
    #region --------- command 5 GetUsersInGroup------------------
        //
        //Name: GetUsersInGroup
        // Params: 
        // param1 -  Name1  - Description1
        // param2 -  Name2  - Description2


#if SERVER  // ----- SERVER PART ---------


        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetUsersInGroup", CommandSideEn.Server)]
        public CommandContainer GetUsersInGroup( ref CommandContainer command)
        {   
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------
                //Проверка параметра Имени пользователя
                //  --------- 1 Getting Parameters ( if Using Command with Params) -------------
                // Проверка параметра Имени пользователя
                String UserGroupNameVal = command.GetParam<String>(CommandParamsEn.UserGroupName);

                if (UserGroupNameVal == null)
                {
                    command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), "Неправильное имя группы ролей");
                    return command;
                }

                // Получаем пользовательский аккаунт                
                //List<RoleTp> rolesInGroup = SystemDispatcher.Instance.Composition.IdentificationProvider.GetRolesForGroup(UserGroupNameVal);// GetUserAccountInfo(userName, password, AuthenticationProviderTypeEn.AspNetProvider); // "v.shumeyko",                                
                // SystemDispatcher.Instance.Composition.IdentificationProvider.GetUserDomainsForRoles(rolesInGroup).ToObservableCollection();

                command.Result.ObservableCollection = GetUsersInGroup(null, UserGroupNameVal);

                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            { 
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }



        /// <summary>
        /// /// SERVER COMMAND IN CONTEXT PART
        /// Получить информацию по пользователям в группе для Данного пользователя 
        /// </summary>
        /// <param name="UserName">can be null </param>
        /// <param name="UserGroupName"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetUsersInGroup", CommandSideEn.ServerInContext)]
        public ObservableCollection<object> GetUsersInGroup(String UserName, String UserGroupName)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------
                //Проверка параметра Имени пользователя
                //  --------- 1 Getting Parameters ( if Using Command with Params) -------------
                
                if (UserGroupName == null)
                {  return null;
                }
                
                UserDomainInfo idInf = SystemDispatcher.Instance.Composition.IdentificationProvider.GetUserDomainInfo("r.hairulin");

                return SystemDispatcher.Instance.Composition.IdentificationProvider.GetUserDomainsInGroup(UserGroupName).ToObservableCollection();

                // Получаем все роли данной группы
                //List<RoleTp> rolesInGroup = SystemDispatcher.Instance.Composition.IdentificationProvider.GetRolesForGroup(UserGroupName);// GetUserAccountInfo(userName, password, AuthenticationProviderTypeEn.AspNetProvider); // "v.shumeyko",                                

                //return SystemDispatcher.Instance.Composition.IdentificationProvider.GetUserDomainsForRoles(rolesInGroup).ToObservableCollection();
                
                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                
            }
            catch (Exception exc)
            {                
                throw; // Throw to Root EcecuteCommand method
            }
        }

#endif  // ---------------------- CLIENT PART ------------------------


        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetUsersInGroup", CommandSideEn.Client)]
        public CommandContainer GetUsersInGroup(String GroupName)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetUsersInGroup, CommandManager_RegisterEn.CoreCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                outcomand.AddParam(CommandParamsEn.UserGroupName, GroupName);

                //------------------2 Setting DataItems ------------------------------
                
                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client Method GetUsersInGroup");
            }
        }


        #endregion ------------------------------ 5 GetUsersInGroup------------------------
               

    #region ----------------------------  CUSTOM Subordiantes For ROLES LOADING ---------------------------



#if SERVER
        /// <summary>
        /// Кастомнная модель подгрузки подчиненных для Групппы относящейся к пользователю в какой-то группе ролей
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="UseSavingUserLoginToIS"></param>
        /// <param name="UserAuthenticatedCompleted"></param>
        protected static void LoadCustomSubordinateUsersModel(ref UserAuthenticatedResources userAccoutResourtces)
        {

            List<UserDomainInfo> subordinates = new List<UserDomainInfo>();
            UserAccountInfo userAccoutInform = userAccoutResourtces.UserAccount;

            // Кастомнная модель подгрузки подчиненных для Пользователя находящегося в какой-то группе ролей

            //1 Сначала добавим тех подчиненных что уже  были получены
            List<String> containedAlreadyUserNames = new List<String>();
            if (userAccoutInform.SubordinateDomainUsers != null)
            {
                foreach (var item in userAccoutInform.SubordinateDomainUsers)
                {
                    subordinates.Add(item);
                }
                containedAlreadyUserNames = subordinates.Select(itm=>itm.UserName).ToList();

            }
            

            //2 Искусственное добавление для этих ролей(Ком Дир. и Деп Прод.) всех юзеров из группы ТП  
            if (
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_ProductDepartmentEmployee"                
                ||
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_CommercialDirector"
               )               
            {
                ObservableCollection<object> GroupTrRepresentersUsers = CoreCommandManager.Current.GetUsersInGroup(null, "GP_DOM_TrRepresentative");
                

                if (GroupTrRepresentersUsers != null && userAccoutInform.SubordinateDomainUsers == null)
                {
                    userAccoutInform.SubordinateDomainUsers = new List<UserDomainInfo>();
                }

                 
                //Теперь добавим всех Пользователей Полученных по Названию Группы Ролей  
                foreach (var item in GroupTrRepresentersUsers)
                {
                    //Если этот пользовател ещё не был добавлен в список по предыдущим обработкам то добавить теперь
                    if (  containedAlreadyUserNames.Count == 0
                        ||
                          containedAlreadyUserNames.Count > 0 && containedAlreadyUserNames.Contains(  (item as UserDomainInfo).UserName) == false 
                        )
                    {
                        subordinates.Add(item);
                    }
                    
                }
            }

            List<UserDomainInfo> subordinatesResult = new List<UserDomainInfo>();

            subordinates.OrderByField("UserName", Data.Order.Ascending);

            //3 AllUsersVirtual - Виртуальная роль для пользователей групп 1/2/3/4
            if (
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_ProductDepartmentEmployee"
                ||
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_CommercialDirector"
                ||
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_RegionalDirector"
                ||
                userAccoutInform.UserDomainInfo.DomainGroup.GroupName == "GP_DOM_RegionalManager"
               )
            {
                //Добавить ещё одного формального подчиненного - Все                

                subordinatesResult.Add(IdentificationService.CreateViertualUser_AllUsers());
            }

            subordinatesResult.AddRange(subordinates);

            //Обновим аккаунт полученными Пользователями
            userAccoutInform.SubordinateDomainUsers.Reload(subordinatesResult);
               
            
        }

#endif

       

        
#endregion ----------------------------  CUSTOM Subordiantes For ROLES LOADING ---------------------------

      




        /// <summary>
        /// Запуск команды
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public CommandContainer ExecuteCommand(ref CommandContainer command)
        {   
            try
            {
                return (this as ICommandManager).ExecuteServerSide(ref command);
            }
            catch (Exception)
            {   
                return command; //Возвращаем все равно клиенту результат ошибки выполнения 
            }
        }








    }
}
