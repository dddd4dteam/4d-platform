﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{

    public  class Cultures
    {
        public static readonly string en = nameof(en);
        public static readonly string ru = nameof(ru);
        public static readonly string pl = nameof(pl);
        public static readonly string En = nameof(En);
        public static readonly string Ru = nameof(Ru);
        public static readonly string Pl = nameof(Pl);

        public static readonly string pl_PL = GetCultureStr(nameof(pl_PL));
        public static readonly string ru_RU = GetCultureStr(nameof(ru_RU));
        public static readonly string en_US = GetCultureStr(nameof(en_US));
        
        static string GetCultureStr(string preName) 
        {
            return preName.Replace('_', '-');
        }

    }

    /// <summary>
    /// Localized resources selectors for: 
    ///   BO.Name, BO.Desc      
    /// </summary>
    public enum BOLocalKeyEn
    {
        /// <summary>
        /// BO Name - selector for Localized BO Name
        /// </summary>
        BoNameLoc 

        /// <summary>
        /// BO Description - selector for Localized BO Description
        /// </summary>
        ,BoDescLoc  
        
    }


    /// <summary>
    ///   Localized resources selectors for:  
    ///   BO.Property.Name , BO.Property.Desc
    /// </summary>
    public enum BOPropLocalKeyEn
    { 

        /// <summary>
        /// BO Property Description - selector for Localized BO Property Description
        /// </summary>
          BoPropDescLoc

        /// <summary>
        /// BO Property Name - selector for Localized BO Property Name 
        /// </summary>
        , BoPropNameLoc
    }


    /// <summary>
    /// Service Model Type: standart or specific Service Model
    /// </summary>
    public enum ServiceModelTypeEn
    {
         /// <summary>
         /// Not defined Service Model Type
         /// </summary>
         NotDefined = 0 
        
        ,
        /// <summary>
        ///  Standart Service Model use BoContract.Name as key for contracts 
        /// </summary>
        StandartServiceModel = 2
         
        ,
        /// <summary>
        ///  Specific SerciveModel - represented custom Data Structure  ServiceModel( like HierarchyService model) 
        /// <para/> If Service Model is  Specific  Model it use ComponentKeys-roles for Component contacts,
        /// <para/>  Standart Service Model  cant use this way  to get contracts 
        /// </summary>
        SpecificServiceModel = 4
       
    }

}
