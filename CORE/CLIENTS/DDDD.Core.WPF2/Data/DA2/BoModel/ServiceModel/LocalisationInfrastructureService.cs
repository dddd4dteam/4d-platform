﻿using System;using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Windows.Controls; 
using System.ComponentModel.Composition; 
using System.Globalization; 
using System.Threading; 
using System.Runtime.Serialization; 
using System.Threading.Tasks;

using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;



namespace DDDD.Core.Data.DA2
{

    
    


    /// <summary>
    /// Ключи культур
    /// </summary>    
    
    public enum CultureKeys
    {
        en,
        ru,
        pl,
        pl_PL,
        ru_RU,
        en_US,
        En,
        Ru,
        Pl,
    }


    
    
    
    
    /// <summary>
    /// Сервис Локализации 
    /// </summary>
    //[InfrastructureService(InfrastructureService_RegisterEn.Localization)]
    public class LocalizationService: SingletonService<LocalizationService> //: IInfrastructureService
    {



        public LocalizationService()
        {

#if CLIENT //&& SL5 //На клиенте

            //CultureLoadedServices = new Dictionary<CultureKeys, List<ServiceModelManager_RegisterEn>>();
            CultureResources = new Dictionary<CultureKeys, Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>>();

#endif

        }
         

        
        List<Type> interfaceServiceContracts = new List<Type>();


        /// <summary>
        /// Получить Контракты модели сервиса Идентификации
        /// </summary>
        /// <returns></returns>
        public List<Type> GetClientContracts()
        {
            try
            {
                return interfaceServiceContracts;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        /// Добавить Контракт Интерфейса - для заполнения контрактами данный сервис SystemDispatcher-ом
        /// </summary>
        /// <param name="serviceContract"></param>
        internal void AddInterfaceServiceContract(Type serviceContract)
        {
            try
            {
                interfaceServiceContracts.Add(serviceContract);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



#if SERVER || WPF
        ResourceManager _resources = null;
        /// <summary>
        /// Менеджер ресурсов Для Контрактов Сервиса Инфраструктуры
        /// </summary>
        public ResourceManager ResourceManager
        {
            get { return _resources; }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="?"></param>
        public void SetCulture(CultureKeys NewCulture)
        {             
                Thread.CurrentThread.CurrentCulture = NewCulture.GetCulture();
                Thread.CurrentThread.CurrentUICulture = NewCulture.GetCulture();
 
        }



         

        #region --------------------------- Get String  METHODS ----------------------------

        /// <summary>
        /// Получить строку ресурса для Имени Контракта с  Детерминированной Культурой 
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetString(LocalResKeys locResKey, Type BOContract, CultureKeys Culture)
        { 
                ResourceManager rm = null;

                var serviceModel = BOContract.GetBObjectServiceModel();//  TypeAttribute<ServiceModelAttribute>().FirstOrDefault();
                if (serviceModel != null)
                {
                    //IServiceModel  ServModelManager = SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel.ServiceModel);
                    rm = serviceModel.ResourceManager;
                }
                else
                {
                    InfrastructureServiceModelAttribute infrastructureService = BOContract.GetTypeAttribute<InfrastructureServiceModelAttribute>().FirstOrDefault();
                    if (infrastructureService != null)
                    {
                        IInfrastructureService infrastructureServiceModel = SystemDispatcher.Instance.Composition.GetInfrastructureService(infrastructureService.ServiceModel);
                        rm = infrastructureServiceModel.ResourceManager;
                    }
                }

                if (rm == null) return null;

                return rm.GetString(locResKey.ToString() + "\\" + BOContract.Name, Culture.GetCulture());
             
        }



        /// <summary>
        /// Получить строку ресурса для Имени Контракта с Культурой по умолчанию(для интерфейса, Culture, UICulture) 
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <returns></returns>
        public string GetString(LocalResKeys locResKey, Type BOContract)
        {  
                ResourceManager rm = null;

                ServiceModelAttribute serviceModel = BOContract.GetTypeAttribute<ServiceModelAttribute>().FirstOrDefault();
                if (serviceModel != null)
                {
                    IServiceModelManager ServModelManager = SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel.ServiceModel);
                    rm = ServModelManager.ResourceManager;
                }
                else
                {
                    InfrastructureServiceModelAttribute infrastructureService = BOContract.GetTypeAttribute<InfrastructureServiceModelAttribute>().FirstOrDefault();
                    if (infrastructureService != null)
                    {
                        IInfrastructureService infrastructureServiceModel = SystemDispatcher.Instance.Composition.GetInfrastructureService(infrastructureService.ServiceModel);
                        rm = infrastructureServiceModel.ResourceManager;
                    }
                }

                if (rm == null) return null;


                return rm.GetString(locResKey.ToString() + "\\" + BOContract.Name,   Thread.CurrentThread.CurrentCulture);
                         
        }
        

        /// <summary>
        /// Получить строчку ресурса для Свойства Контракта с Детерминированной Культурой 
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="State"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetString(LocalResKeys locResKey, Type BOContract, string Property, CultureKeys Culture)
        {             
            ResourceManager rm = null;

            ServiceModelAttribute serviceModel = BOContract.GetTypeAttribute<ServiceModelAttribute>().FirstOrDefault();
            if (serviceModel != null)
            {
                IServiceModelManager ServModelManager = SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel.ServiceModel);
                rm = ServModelManager.ResourceManager;
            }
            else
            {
                InfrastructureServiceModelAttribute infrastructureService = BOContract.GetTypeAttribute<InfrastructureServiceModelAttribute>().FirstOrDefault();
                if (infrastructureService != null)
                {
                    IInfrastructureService infrastructureServiceModel = SystemDispatcher.Instance.Composition.GetInfrastructureService(infrastructureService.ServiceModel);
                        
                    rm = infrastructureServiceModel.ResourceManager;
                }
            }

            if (rm == null) return null;

            return rm.GetString(locResKey.ToString() + "\\" + BOContract.Name + "|" + Property, Culture.GetCulture());
             
        }



        /// <summary>
        /// Получить строчку ресурса для Свойства Контракта с Культурой по умолчанию(для интерфейса, Culture, UICulture)
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="State"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetString(LocalResKeys locResKey, Type BOContract, string Property)
        {
             
                ResourceManager rm = null;

                ServiceModelAttribute serviceModel = BOContract.GetTypeAttribute<ServiceModelAttribute>().FirstOrDefault();
                if (serviceModel != null)
                {
                    IServiceModelManager ServModelManager = SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel.ServiceModel);
                    rm = ServModelManager.ResourceManager;
                }
                else
                {
                    InfrastructureServiceModelAttribute infrastructureService = BOContract.GetTypeAttribute<InfrastructureServiceModelAttribute>().FirstOrDefault();
                    if (infrastructureService != null)
                    {
                        IInfrastructureService infrastructureServiceModel = SystemDispatcher.Instance.Composition.GetInfrastructureService(infrastructureService.ServiceModel);
                        rm = infrastructureServiceModel.ResourceManager;
                    }
                }

                if (rm == null) return null;


                    return rm.GetString(locResKey.ToString() + "\\" + BOContract.Name + "|" + Property, Thread.CurrentThread.CurrentCulture);
              

        }



        #endregion --------------------------- Get String  METHODS ----------------------------

#endif


     

#if CLIENT 
        
        /// <summary>
        /// Строковые Ресурсы для всех культур
        /// Одной культуре - / Список Загруженных Моделей Сервисов / Словарь всех строковых ресурсов для всех моделей сервисов
        /// </summary>
        internal Dictionary<CultureKeys, Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<String, String>> > CultureResources 
        { get; set; }


        ///// <summary>
        ///// Список привязанных на определенный момент сервисов, ресурсы Которых уже есть в CulturesResources 
        ///// </summary>
        //internal Dictionary<CultureKeys, List<ServiceModelManager_RegisterEn>> CultureLoadedServices { get; set; }


        /// <summary>
        /// Задать Культура меняется
        /// </summary>
        /// <param name="culture"></param>
        public async Task ChangeCultureAsync(CultureKeys culture)
        { 

                if (!CultureResources.ContainsKey(culture)) //Если для таковой культуры небыло подгружено ресурсов - то Загрузка по чистой
                {
                    //Current Culture RegisteredServices
                    List<ServiceModelManager_RegisterEn> RegisteredServices = CultureResources[CurrentCulture].Item1; //CultureLoadedServices[CurrentCulture];

                    //Currently Loaded Modules
                    List<ServiceModelManager_RegisterEn> LoadedModules = new List<ServiceModelManager_RegisterEn>();
                    if (ModularityService.Current.LoadedModules != null && ModularityService.Current.LoadedModules.Count > 0)
                    { LoadedModules = ModularityService.Current.LoadedModules.Keys.ToList(); }

                    //NewCulture for all currentlyUsed Services Resources and Modules Resources 
                    await LoadCultureResourcesAsync(LoadedModules, RegisteredServices, culture);
                }
                else if (CultureResources.ContainsKey(culture) ) //А вот если она есть тонадо поняь что грузить
                {
                    //Сервисы - находится разница
                    List<ServiceModelManager_RegisterEn> ServicesDifference = new List<ServiceModelManager_RegisterEn>();
                    if( !   CultureResources[CurrentCulture].Item1.IsEquals(CultureResources[culture].Item1))
                        //CultureLoadedServices[CurrentCulture].IsEquals(CultureLoadedServices[culture] ) )  //Если они действительно различаются хоть на один элемент
                    { ServicesDifference = CultureResources[culture].Item1.GetDifference(CultureResources[CurrentCulture].Item1); } 
                    

                    //Модули - просто полный список
                    List<ServiceModelManager_RegisterEn> LoadedModules = new List<ServiceModelManager_RegisterEn>();
                    if (ModularityService.Current.LoadedModules != null && ModularityService.Current.LoadedModules.Count > 0)
                    { LoadedModules = ModularityService.Current.LoadedModules.Keys.ToList(); }

                    //Загрузка Ресурсов Культуры
                    await LoadCultureResourcesAsync(LoadedModules, ServicesDifference, culture);
                }                
            
        }


        /// <summary>
        /// Получить Текущую Культуру
        /// </summary>
        /// <returns></returns>
        public CultureKeys  CurrentCulture
        { 
          get { return Thread.CurrentThread.CurrentUICulture.GetCultureKey(); }
        }


        #region ----------------- Contains / GetResource --------------------
  
        
        /// <summary>
        /// Содержит ключ локализации в ресурсах с задаваемой культуры
        /// </summary>
        /// <param name="Culture"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsKey(CultureKeys Culture, String Key)
        {
             
                if (CultureResources.ContainsKey(Culture) == false)
                    return false;

                if (CultureResources[Culture].Item2.ContainsKey(Key) == false)
                    return false;

                return true;
             
        }


        /// <summary>
        /// Содержит ключ локализации в ресурсах текущей культуры
        /// </summary>
        /// <param name="Culture"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public bool ContainsKey(String Key)
        {
          
                if (CultureResources[CurrentCulture].Item2.ContainsKey(Key) == false)
                    return false;

                return true;

        }


        /// <summary>
        /// Получение названия/локализации для самой сущности
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetStringRes(LocalResKeys locResKey, Type BOContract, CultureKeys Culture)
        {
                if (BOContract == null)
                    return null;

                String fullKey = locResKey.ToString() + "\\" + BOContract.Name;

                if (ContainsKey(Culture, fullKey))
                    return GetStringRes(Culture, fullKey);
                return null;
            
        }



        /// <summary>
        /// Получить строку ресурса для Имени Контракта с Культурой по умолчанию(для интерфейса, Culture, UICulture) 
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <returns></returns>
        public string GetStringRes(LocalResKeys locResKey, Type BOContract)
        {
                if ( BOContract == null )
                    return null;

                String fullKey = locResKey.ToString() + "\\" + BOContract.Name;

                if (ContainsKey(Thread.CurrentThread.CurrentCulture.GetCultureKey(), fullKey))
                    return GetStringRes(Thread.CurrentThread.CurrentCulture.GetCultureKey(), fullKey);
                
                return null;
        }


        /// <summary>
        /// Получить строчку ресурса для Свойства Контракта с Детерминированной Культурой 
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="State"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetStringRes(LocalResKeys locResKey, Type BOContract, string Property, CultureKeys Culture)
        {
            if ( BOContract == null || Property == null || Property == "" )
                return null;

            String fullKey = locResKey.ToString() + "\\" + BOContract.Name + "|" + Property;

            if (ContainsKey(Culture, fullKey))
                return GetStringRes(Culture, fullKey);
            return null;
           
        }



        

        /// <summary>
        /// Получить строчку ресурса для Свойства Контракта с Культурой по умолчанию(для интерфейса, Culture, UICulture)
        /// </summary>
        /// <param name="locResKey"></param>
        /// <param name="BOContract"></param>
        /// <param name="State"></param>
        /// <param name="Culture"></param>
        /// <returns></returns>
        public string GetStringRes(LocalResKeys locResKey, Type BOContract, string Property)
        {
            
                if (locResKey == null || BOContract == null || Property == null || Property == "")
                    return null;

                String fullKey = locResKey.ToString() + "\\" + BOContract.Name + "|" + Property;

                if (ContainsKey(Thread.CurrentThread.CurrentCulture.GetCultureKey(), fullKey))
                    return GetStringRes(Thread.CurrentThread.CurrentCulture.GetCultureKey(), fullKey);
                return null;            

        }
              

        /// <summary>
        /// Получить строковый ресурс Для Задаваемых Ключа культуры и Клча ресурса
        /// </summary>
        /// <param name="Culture"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public String GetStringRes(CultureKeys Culture, String Key)
        {
           
                if (CultureResources.ContainsKey(Culture) == false)
                    return null;

                if (CultureResources[Culture].Item2.ContainsKey(Key) == false)
                    return null;

                return CultureResources[Culture].Item2[Key];
            
        }


        /// <summary>
        /// Получить строковый ресурс Для текущей Культуры
        /// </summary>
        /// <param name="Culture"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        public String GetStringRes(String Key)
        {
           
                if (CultureResources[CurrentCulture].Item2.ContainsKey(Key) == false)
                    return null;

                return CultureResources[CurrentCulture].Item2[Key];
            
        }

        #endregion ----------------- Contains / GetResource --------------------


        #region ------------------- Add Resource ----------------------



        /// <summary>
        /// Добавить свой ресурс
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public void AddResource(CultureKeys culture, String Key, String Value)
        { 
            if (!CultureResources.ContainsKey(culture))
            { CultureResources.Add(culture, new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(new List<ServiceModelManager_RegisterEn>(), new Dictionary<string, string>())); }

            if (CultureResources[culture].Item2.ContainsKey(Key)) return;
            else CultureResources[culture].Item2.Add(Key, Value);
            
        }


        #endregion ------------------- Add Resource --------------------


        #region ------------------------ Loading Reources -----------------------------

        /// <summary>
        /// Загрузка ресурсов для Модуля 
        /// </summary>
        /// <param name="Module"> Модуль для Которого будем загружать ресурсы</param>
        public async Task LoadModuleResourcesAsync(ServiceModelManager_RegisterEn Module)
        {
            try
            {
                if (!CultureResources.ContainsKey(CurrentCulture))
                {
                    CultureResources.Add(CurrentCulture,
                        new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(
                            new List<ServiceModelManager_RegisterEn>(), new Dictionary<string, string>()));
                }
                if (ModularityService.Current.IsModuleLoaded(Module) == false)
                {
                    // список Рельной загрузки Сервисных ресурсов для Команды получения ресурсов по даннной культуре
                    List<ServiceModelManager_RegisterEn> realNeedToLoadServiceModels =
                        new List<ServiceModelManager_RegisterEn>();

                    //1 Получить все сявзанные с данным модулем Сервисы Моделей - Которые по идее стоит подгружать для данного модуля
                    List<ServiceModelManager_RegisterEn> currentModuleServicesToLoad =
                        new List<ServiceModelManager_RegisterEn>();
                    IServiceModelManager CurrentServiceModel =
                        SystemDispatcher.Instance.Composition.GetServiceModelManager(Module);
                    currentModuleServicesToLoad.AddRange(CurrentServiceModel.DependsOn); // Это зависисмости
                    currentModuleServicesToLoad.Add(Module); // И сама модель сервиса   
                    if (CultureResources[CurrentCulture].Item1.Count > 0)
                    {
                        //2 Все уже загруженные  по данной культуре ресурсы  моделей Сервисов 
                        List<ServiceModelManager_RegisterEn> loadedModulesList = CultureResources[CurrentCulture].Item1;

                        //И вычитая из списка стоящих к загрузке сервисов Модуля получаем - список Рельной загрузки                         
                        currentModuleServicesToLoad = loadedModulesList.GetDifference(currentModuleServicesToLoad);
                    }

                    //Если после удаления уже загруженных моделей останется что-то, что Необходимо для работы данного Модуля
                    //то Подгрузить ресурсы модуля по Культуре и используя список Моделей Сервисов
                    if (currentModuleServicesToLoad.Count > 0)
                    {
                        List<ServiceModelManager_RegisterEn> ModulesToLoad =
                            new List<ServiceModelManager_RegisterEn>() {Module};

                        //Запускаем команду загрузки ресурсов с сервера для списка Сервисов Рельной загрузки
                        CommandContainer cmdLoadModuleResources =
                            SystemDispatcher.CoreCmdManager.LoadModulesResources(ModulesToLoad,
                                currentModuleServicesToLoad, CurrentCulture);

                        CommandContainer cmd = await SystemDispatcher.Instance.ShellControl.ExecuteCommandAsync(cmdLoadModuleResources);
                        if (cmd.Result.IsContainsData &&
                                    cmd.Result.ResultDataType == CommandContainer.ResultDataTypeEn.DictionaryByString)
                        {
                            // 1 Добавление/ОБновление Ресурсов у Культуры
                            Dictionary<String, String> loadedResources =
                                cmd.Result.DictionaryByString.ToDictionary<String, String, object>();
                            if (!CultureResources.ContainsKey(CurrentCulture))
                            {
                                CultureResources.Add(CurrentCulture,
                                    new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(
                                        new List<ServiceModelManager_RegisterEn>(),
                                        new Dictionary<string, string>()));

                                // Добавить загруженные ресурсы в словарь всех ресурсов культуры
                                CultureResources[CurrentCulture].Item2.AddRange(loadedResources);
                            }
                            else
                            {
                                //Добавить только те что новые ресурсы чтоб не было конфиликтов с Ключами
                                Dictionary<String, String> existedResources =
                                    CultureResources[CurrentCulture].Item2;
                                foreach (var item in loadedResources) //Обновить список ресурсов
                                {
                                    if (!existedResources.ContainsKey(item.Key))
                                    {
                                        existedResources.Add(item.Key, item.Value);
                                    }
                                }

                                //CultureResources[CurrentCulture].Item2.Clear();
                                //CultureResources[CurrentCulture].Item2.AddRange(existedResources);
                            }
                            // 2 Расширить список загруженных/обслуженных Моделей у Культуры и саму культуру если её не было 
                            if (!CultureResources.ContainsKey(CurrentCulture))
                            {
                                CultureResources.Add(CurrentCulture,
                                    new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(
                                        new List<ServiceModelManager_RegisterEn>(),
                                        new Dictionary<string, string>()));

                                CultureResources[CurrentCulture].Item1.AddRange(currentModuleServicesToLoad);
                            }
                            else
                            {
                                CultureResources[CurrentCulture].Item1.AddRange(currentModuleServicesToLoad);
                            }
                        }

                    }

                } //Будем загружать ресурсы для Модуля

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        /// <summary>
        /// Загрузка ресурсов для Другой Культуры 
        /// </summary>
        /// <param name="ModulesToLoad">Модули для загрузки/текущие</param>
        /// <param name="ServicesToLoad">Сервисы для загрузки/текущие</param>
        /// <param name="NewCulture"></param>
        public async Task LoadCultureResourcesAsync(List<ServiceModelManager_RegisterEn> ModulesToLoad, List<ServiceModelManager_RegisterEn> ServicesToLoad, CultureKeys NewCulture)
        {
            try
            {

                if (!CultureResources.ContainsKey(CurrentCulture))
                { CultureResources.Add(CurrentCulture, new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(new List<ServiceModelManager_RegisterEn>(), new Dictionary<string, string>())); }



                //Запускаем команду загрузки ресурсов с сервера для списка Сервисов Рельной загрузки
                CommandContainer cmdLoadModuleResources = SystemDispatcher.CoreCmdManager.LoadModulesResources(ModulesToLoad, ServicesToLoad, NewCulture);

                CommandContainer cmd = await SystemDispatcher.Instance.ShellControl.ExecuteCommandAsync(cmdLoadModuleResources);
                if (cmd.Result.IsContainsData && cmd.Result.ResultDataType == CommandContainer.ResultDataTypeEn.DictionaryByString)
                {
                    // 1 Добавление/ОБновление Ресурсов у Культуры
                    Dictionary<String, String> loadedResources = cmd.Result.DictionaryByString.ToDictionary<String, String, object>();
                    if (!CultureResources.ContainsKey(NewCulture))
                    {  //Добавить загруженные ресурсы в словарь всех ресурсов культуры , Как новый словарь
                        CultureResources.Add(CurrentCulture, new Tuple<List<ServiceModelManager_RegisterEn>, Dictionary<string, string>>(new List<ServiceModelManager_RegisterEn>(), new Dictionary<string, string>()));
                        CultureResources[NewCulture].Item2.AddRange(loadedResources);
                    }
                    else
                    {  //Добавить только те что новые ресурсы чтоб не было конфиликтов с Ключами
                        Dictionary<String, String> existedResources = CultureResources[NewCulture].Item2;
                        foreach (var item in loadedResources)
                        {
                            if (!existedResources.ContainsKey(item.Key))
                            { existedResources.Add(item.Key, item.Value); }
                        }

                        CultureResources[NewCulture].Item2.Clear();
                        CultureResources[NewCulture].Item2.AddRange(existedResources);//Обновить список ресурсов
                    }

                    // 2 Расширить список загруженных/обслуженных Моделей у Культуры
                    CultureResources[NewCulture].Item1.AddRange(ServicesToLoad);

                    //3 Сменить культуру у Thread.CurrentUICulture
                    Thread.CurrentThread.CurrentUICulture = NewCulture.GetCulture();
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

        #endregion ------------------------ Load Reources -----------------------------
        
#endif




    }


    


}
