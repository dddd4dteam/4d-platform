﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2 
{

    /// <summary>
    ///  BO Service Model -known set of BO contracts, mapped from  DB Tables and Views/ custom structs
    /// </summary>
    public interface IServiceModel
    {

        /// <summary>
        /// ServiceModel  Key is  its Type.Name
        /// </summary>
        string SvcModelKey { get; }
        
        //List<string> DependsOn { get; }

        /// <summary>
        /// ServiceModel Index is x number from -  S[x]_[Name]- Naming rule.
        /// <para/>For EX: [S23_Suppliers] -  x is [23].
        /// </summary>
        int SvcModelIndex { get; }


        /// <summary>
        /// Target BO contract in Service Model.
        /// </summary>
        Type TargetModelBO { get; }


        /// <summary>
        /// ServiceModel Type - standart or specific service Model        
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        ServiceModelTypeEn ServiceModelType { get; }


        /// <summary>
        /// Get Compomnent contract by componentKey - if Service Model is  Specific Data Structure Model
        /// <para/>  Standart Service Model  cant use this way  to get contracts 
        /// </summary>
        /// <param name="modelComponentKey">EX:  ItemTypeEntity/ HierarchyEntity/ AncestorsView  in Hierarchy Service Model for example</param>
        /// <returns></returns> 
        Type GetComponentType(string modelComponentKey);

        /// <summary>
        /// Get Compomnent contract Name by componentKey - if Service Model is  Specific Data Structure Model
        /// <para/>  Standart Service Model  cant use this way  to get contracts 
        /// </summary>
        /// <param name="modelComponent">EX:  ItemTypeEntity/ HierarchyEntity/ AncestorsView  in Hierarchy Service Model for example</param>
        /// <returns></returns>
        string GetComponentName(string modelComponentKey);



        /// <summary>
        /// Get  BO contract by Name only from this ServiceModel
        /// </summary>
        /// <returns></returns>        
        Type GetBOContractByName(string boNameOnly);

        //List<Type> GetModelContracts();


#if SERVER || WPF // Только на сервере
       
        /// <summary>
        /// Resource Manager that contains localized names and descriptions of contracts and their properties
        /// </summary>
        ResourceManager ResManager  { get;  }


#endif



        /// <summary>
        /// Get BO contracts by role        
        /// </summary>
        /// <param name="boRole"></param>
        /// <returns></returns>
        List<Type> GetObjectsByRole(BORoleEn boRole);
         

    }
}
