﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Core.Meta.BO;
using System.Runtime.Serialization;

namespace Core.Composition.DataServices
{
  
    //RegisterBOMapping Attribute
    //ValueObjectMapping Attribute
    //TreeNodeMapping Attribute

    /// <summary>
    /// Обозначение компонента В Специфицированной структуре
    /// </summary>    
    [AttributeUsage(AttributeTargets.Class , AllowMultiple =false)]
    public class DataStructureItemAttribute : Attribute
    {
        public DataStructureItemAttribute(String MappingKey)
        {
            _dataStructureMappingKey = MappingKey;
        }

        private  String _dataStructureMappingKey;
        
        /// <summary>
        /// Ключ
        /// </summary>
        public String  StructureMappingKey
        {
            get { return _dataStructureMappingKey; }
        }
    }


}
