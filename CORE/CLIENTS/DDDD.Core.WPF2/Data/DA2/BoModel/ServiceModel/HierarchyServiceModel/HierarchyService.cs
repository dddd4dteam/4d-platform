﻿using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Data.DA2.Query;
using DDDD.Core.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace DDDD.Core.Data.DA2
{

    /// <summary>
    ///  Service that can work with Hierarchy ServiceModels
    /// </summary>
    public class HierarchyService:SingletonService<HierarchyService>
    {
        public class Cmd
        {
            public const string GetItemPathToRoot = nameof(GetItemPathToRoot);
            public const string GetItemChilds = nameof(GetItemChilds);
            public const string GetItemParent = nameof(GetItemParent);
            public const string GetItemDescendants = nameof(GetItemDescendants);
            public const string GetItemAncestors = nameof(GetItemAncestors);
            public const string GetItemsOnLevel = nameof(GetItemsOnLevel);
            public const string GetItemType = nameof(GetItemType);
            public const string GetItemsByType = nameof(GetItemsByType);
            public const string GetItemsOnLevelByType = nameof(GetItemsOnLevelByType);
            public const string LoadItemCollections = nameof(LoadItemCollections);
        }

        public class CmdPrm
        {
            public const string Param1 = nameof(Param1);
            public const string ServiceModel = nameof(ServiceModel);
            public const string ItemEntity = nameof(ItemEntity);
            public const string ItemTypeEntity = nameof(ItemTypeEntity);
            public const string AncestorsView = nameof(AncestorsView);
            public const string DescndantsView = nameof(DescndantsView);
            public const string Query = nameof(Query);
        }

        class Field
        {
            public const string IDAncestors = nameof(IDAncestors);
            public const string Level = nameof(Level);

        }



        #region --------- command 1 GetItemPathToRoot------------------

        /// <summary>
        /// Path to Root in Tree
        /// </summary>
        /// <param name="boEntityItem"></param>
        /// <returns></returns>
        public Dictionary<int, IBObject> GetItemPathToRoot(IPersistableBO boEntityItem)
        {
            //        // ----------------  1 Adding Parameters  ----------------------------- 
            HierarchyServiceModel serviceModel = boEntityItem.GetServiceModel() as HierarchyServiceModel;
            Validator.ATNullReferenceArgDbg<HierarchyService>(serviceModel, nameof(GetItemPathToRoot), nameof(serviceModel));

            //   1 get an  ancestorView Type
            Type ancestorViewType = serviceModel.Get_AncestorsView();
          
            //  Get PKey Property value            
            Int32 keyPropertyValue = (Int32)boEntityItem.GetPrimaryKey().First().Value;

            //   Create  query  
            DSelectQuery query = DSelectQuery.SelectAll()
                .From(ancestorViewType)
                .Where(Field.IDAncestors, Compare.Equals, DConst.Const(keyPropertyValue))
                .OrderBy(Field.Level, Order.Descending);

            //Run Query with Defaulkt Proxy 
            var pathToRoot = DBProxy.DefProx.SelectList(query);

            // Save as Leveled-int Dictionary elements,
            // and each ancestor view item convert to EntityItem   
            var dictInt = new Dictionary<int, IBObject>();
            for (int i = 0; i < pathToRoot.Count; i++)
            {
                var item = pathToRoot[i] as IHierarchicalView;
                dictInt.Add(item.Level,item.ToEntityItem() );
            }
          
            return dictInt;
        }


        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        //[CommandExecutionMethod("GetItemPathToRoot", CommandSideEn.Server)]
        //public CommandContainer GetItemPathToRoot(ref CommandContainer command)
        //{
        //    try
        //    {
        //        //  --------- 1 Getting Parameters ( if Using Command with Params) ------

        //        //1 Получаем запрос для  Анцестра Иерархиии
        //        DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

        //        //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
        //        ServiceModelManager_RegisterEn serviceModel = command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


        //        //----------- 2 Custom Command Processing -------------------                
        //        IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

        //        Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


        //        //Выполним запрос через CRUDManager 
        //        CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
        //        SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


        //        //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
        //        //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
        //        command.Result.ObservableCollection = new ObservableCollection<object>();
        //        foreach (var item in comdContainer.Result.ObservableCollection)
        //        { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }

        //        //command.Result.DictionaryByInt = new Dictionary<int, object>();
        //        //foreach (var item in comdContainer.Result.DictionaryByInt)
        //        //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }

        //        //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
        //        return command.SayCommandCompleteSuccesfully();
        //    }
        //    catch (Exception exc)
        //    {
        //        command.SayCommandThrowUnknownError(CommandManager_RegisterEn.HierarchyCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
        //        throw; // Throw to Root EcecuteCommand method
        //    }
        //}




        ///// <summary>
        ///// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        ///// </summary>
        ///// <param name="Param1"></param>        
        ///// <returns></returns>
        //[DCM ("GetItemPathToRoot", CommandSideEn.Client)]
        //public CommandContainer GetItemPathToRoot(IBObject NodeItem)
        //{
        //    // --------------------- 0 Creating CommandContainer -------------------------
        //    CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemPathToRoot, CommandManager_RegisterEn.HierarchyCommandManager);

        //    try
        //    {
        //        // ----------------  1 Adding Parameters  ----------------------------- 
        //        IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

        //        if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
        //        { throw new Exception("It is not a Hierarchy Service"); }


        //        if (NodeItem.GetType().FullName != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.ItemEntity).FullName)
        //        { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

        //        //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
        //        Type AncestorType = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.AncestorsView);
        //        Validator.AssertNotNull<Type>(AncestorType, "AncestorTypeName"); //Проверка на пустой параметр


        //        //Получаем ключевое свойство
        //        PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
        //        ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
        //        //И значение свойства 
        //        Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });

        //        //Составляем запрос 
        //        DSelectQuery query = DBSelectQuery.SelectAll().From(AncestorType).
        //            Where("IDAncestors", Data.Compare.Equals, DConst.Int32(keyPropertyValue)).OrderBy("Level", Order.Descending);


        //        //Добавил параметр запроса
        //        outcomand.AddParam(CommandParamsEn.Query, query);

        //        //Добавим имя МоделиСервиса данного Узла
        //        outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel);
        //        //------------------2 Setting DataItems ------------------------------


        //        //------------------3 Returning Custom Command -----------------------
        //        return outcomand;
        //    }
        //    catch (Exception)
        //    {
        //        throw new Exception("Error in Client RegisterPages GetItemPathToRoot");
        //    }
        //}


        #endregion ------------------------------ 1 GetItemPathToRoot------------------------

        //GetItemChilds
        //GetItemParent
        //GetItemDescendants
        //GetItemAncestors
        //GetItemsOnLevel
        //GetItemType
        //GetItemsByType
        //GetItemsOnLevelByType
        //LoadItemCollections
    }
}
