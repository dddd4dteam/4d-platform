﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Composition.Commanding;
using Core.Commanding;
using System.ComponentModel.Composition;
using Core.Meta.Systems;
using Core.Composition.DataServices;
using Core.Data.Query;
using System.Reflection;
using Core.BoModel;
using Core;
using Core.Meta.VVM;
using System.Collections.ObjectModel;


namespace Core.CommandManager
{

    /// <summary>
    /// Компоненты которые участвуют в работе данной Структуры. Эти компоненты должен определить у себя Сервис,
    /// Выступающий в качестве представителья данной структуры и клиента к даному Менеджеру команд 
    /// </summary>
    /// public enum HierarchyStructComponentsEn
    /// {
    //      ItemEntity,

    //      HierarchyEntity,

    //      ItemTypeEntity,

    //      AncestorsView,

    //      DescndantsView
    /// }


    /// <summary>
    /// Итак это у нас менеджер комманд для Сервисов Иерархий 
    /// </summary>
    //[Export(typeof(ICommandManager))]
    [CommandManager(CurrentCommandManager: CommandManager_RegisterEn.HierarchyCommandManager)]
    public class HierarchyCommandManager : ICommandManager
    {

        /// <summary>
        /// Текущий Менеджер Иерархии
        /// </summary>
        public static HierarchyCommandManager Current
        {
            get { return SystemDispatcher.HierarchyCmdManager; }
        }

        /// <summary>
        /// Имя Менеджера Комманд 
        /// </summary>
        public String Name
        {
            get { return this.GetType().GetTypeAttribute<CommandManagerAttribute>().FirstOrDefault().CommandManager.ToString(); }
        }


        /// <summary>
        /// Компоненты которые участвуют в работе данной Структуры. Эти компоненты должен определить у себя Сервис,
        /// Выступающий в качестве представителья данной структуры и клиента к даному Менеджеру команд 
        /// </summary>
        public enum ComponentsEn
        {
            ItemEntity,
            HierarchyEntity,
            ItemTypeEntity,
            AncestorsView,
            DescndantsView,
            ItemsOnLevel
        }


        public enum CommandsEn
        {
            GetItemPathToRoot,
            GetItemChilds,
            GetItemParent,
            GetItemDescendants,
            GetItemAncestors,
            GetItemsOnLevel,
            GetItemType,
            GetItemsByType,
            GetItemsOnLevelByType,
            LoadItemCollections
        }

        /// <summary>
        /// Парметры команд которыми пользуется серверная часть для прочтения Контейнера Команд Клиента, а клиен может использовать их как макросы подстановок( not released).
        /// </summary>
        public enum CommandParamsEn
        {
            Param1,

            ServiceModel,

            ItemEntity,

            ItemTypeEntity,

            AncestorsView,

            DescndantsView,

            Query,

        }


        public HierarchyCommandManager() { }





        #region --------- command 1 GetItemPathToRoot------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemPathToRoot", CommandSideEn.Server)]
        public CommandContainer GetItemPathToRoot(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }
                
                //command.Result.DictionaryByInt = new Dictionary<int, object>();
                //foreach (var item in comdContainer.Result.DictionaryByInt)
                //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.HierarchyCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemPathToRoot", CommandSideEn.Client)]
        public CommandContainer GetItemPathToRoot(IBObject NodeItem)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemPathToRoot, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }


                if (NodeItem.GetType().FullName != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.ItemEntity).FullName)
                { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type AncestorType = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.AncestorsView);
                ArgumentValidator.AssertNotNull<Type>(AncestorType, "AncestorTypeName"); //Проверка на пустой параметр


                //Получаем ключевое свойство
                PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });

                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(AncestorType).
                    Where("IDAncestors", Data.Compare.Equals, DBConst.Int32(keyPropertyValue)).OrderBy("Level", Data.Order.Descending);
                

                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel);
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemPathToRoot");
            }
        }


        #endregion ------------------------------ 1 GetItemPathToRoot------------------------

        #region --------- command 2 GetItemChilds------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemChilds", CommandSideEn.Server)]
        public CommandContainer GetItemChilds(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )                  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                //    command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }

        

        /// <summary>
        /// CLIENT COMMAND PART   ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemChilds", CommandSideEn.Client)]
        public CommandContainer GetItemChilds(IBObject NodeItem)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemChilds, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                ArgumentValidator.AssertNotNull<IBObject>( NodeItem, "Node Item Cannot be NULL"); //Проверка на пустой параметр

                if (NodeItem == null) 
                { throw new Exception("It is not a Hierarchy Service"); }

                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (NodeItem.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemEntity))
                { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type DescendantTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.DescndantsView);
                ArgumentValidator.AssertNotNull<Type>(DescendantTypeName, "DescendantTypeName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(DescendantTypeName).
                    Where("IDDescendants", Data.Compare.Equals, DBConst.Int32(keyPropertyValue)).
                    AndWhere("Level", Data.Compare.Equals, DBConst.Int32(1));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemChilds");
            }
        }


        #endregion ------------------------------ 2 GetItemChilds------------------------

        #region --------- command 3 GetItemParent------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemParent", CommandSideEn.Server)]
        public CommandContainer GetItemParent(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                //    command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }
                
                //command.Result.DictionaryByInt = new Dictionary<int, object>();
                //foreach (var item in comdContainer.Result.DictionaryByInt)
                //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemParent", CommandSideEn.Client)]
        public CommandContainer GetItemParent(IBObject NodeItem)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemParent, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (NodeItem.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemEntity))
                { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type AncestorTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.AncestorsView);
                ArgumentValidator.AssertNotNull<Type>(AncestorTypeName, "AncestorTypeName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(AncestorTypeName).
                    Where("IDAncestors", Data.Compare.Equals, DBConst.Int32(keyPropertyValue)).
                    AndWhere("Level", Data.Compare.Equals, DBConst.Int32(1));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemParent");
            }
        }


        #endregion ------------------------------ 3 GetItemParent------------------------

        #region --------- command 4 GetItemDescendants------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemDescendants", CommandSideEn.Server)]
        public CommandContainer GetItemDescendants(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);                
                //    command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }

                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART  ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemDescendants", CommandSideEn.Client)]
        public CommandContainer GetItemDescendants(IBObject NodeItem)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemDescendants, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (NodeItem.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemEntity))
                { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type DescndantTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.DescndantsView);
                ArgumentValidator.AssertNotNull<Type>(DescndantTypeName, "DescndantTypeName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(DescndantTypeName).
                    Where("IDDescendants", Data.Compare.Equals, DBConst.Int32(keyPropertyValue));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemDescendants");
            }
        }


        #endregion ------------------------------ 4 GetItemDescendants------------------------

        #region --------- command 5 GetItemAncestors------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemAncestors", CommandSideEn.Server)]
        public CommandContainer GetItemAncestors(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                
                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                
                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }

                //command.Result.DictionaryByInt = new Dictionary<int, object>();
                //foreach (var item in comdContainer.Result.DictionaryByInt)
                //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }



        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemAncestors", CommandSideEn.Client)]
        public CommandContainer GetItemAncestors(IBObject NodeItem)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemAncestors, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = NodeItem.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (NodeItem.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemEntity))
                { throw new Exception("It is not an [ItemEntity] component Type. You can use only objects with [ItemEntity] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type AncestorTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.AncestorsView);
                ArgumentValidator.AssertNotNull<Type>(AncestorTypeName, "AncestorTypeName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (NodeItem as IBObject).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(NodeItem, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(AncestorTypeName).
                    Where("IDAncestors", Data.Compare.Equals, DBConst.Int32(keyPropertyValue));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemAncestors");
            }
        }


        #endregion ------------------------------ 5 GetItemAncestors------------------------

        #region --------- command 6 GetItemsOnLevel------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsOnLevel", CommandSideEn.Server)]
        public CommandContainer GetItemsOnLevel(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                //    command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query);// ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }
                

                //command.Result.DictionaryByInt = new Dictionary<int, object>();
                //foreach (var item in comdContainer.Result.DictionaryByInt)
                //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsOnLevel", CommandSideEn.Client)]
        public CommandContainer GetItemsOnLevel(int Level, IHierarchyServiceModelManager serviceManager)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemsOnLevel, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type ItemsOnLevelTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.ItemsOnLevel);
                ArgumentValidator.AssertNotNull<Type>(ItemsOnLevelTypeName, "ItemsOnLevelTypeName"); //Проверка на пустой параметр

                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(ItemsOnLevelTypeName).
                    Where("Level", Data.Compare.Equals, DBConst.Int32(Level));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemsOnLevel");
            }
        }


        #endregion ------------------------------ 6 GetItemsOnLevel------------------------

        #region --------- command 8 GetItemsByType------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsByType", CommandSideEn.Server)]
        public CommandContainer GetItemsByType(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                //  command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }

                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsByType", CommandSideEn.Client)]
        public CommandContainer GetItemsByType(IPersistableBO ItemType)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemsByType, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = ItemType.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (ItemType.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemTypeEntity))
                { throw new Exception("It is not an [ItemsOnLevel] component Type. You can use only objects with [ItemsOnLevel] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type ItemEntityName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.ItemEntity);
                ArgumentValidator.AssertNotNull<Type>(ItemEntityName, "ItemEntityName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (ItemType as IPersistableBO).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(ItemType, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(ItemEntityName).
                    Where(keyProperty.Name, Data.Compare.Equals, DBConst.Int32(keyPropertyValue));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemsByType");
            }
        }


        #endregion ------------------------------ 8 GetItemsByType------------------------

        #region --------- command 9 GetItemsOnLevelByType------------------




        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsOnLevelByType", CommandSideEn.Server)]
        public CommandContainer GetItemsOnLevelByType(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                DBSelectQuery query = command.GetParam<DBSelectQuery>(CommandParamsEn.Query);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = (ServiceModelManager_RegisterEn)Enum.Parse(typeof(ServiceModelManager_RegisterEn), command.GetParam<String>(CommandParamsEn.ServiceModel), false);
                //    command.GetParam<ServiceModelManager_RegisterEn>(CommandParamsEn.ServiceModel);


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);

                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdContainer = SystemDispatcher.CrudCmdManager.SelectCollection(query); //, ancestorType
                SystemDispatcher.CrudCmdManager.SelectCollection(ref comdContainer);


                //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                
                //Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у
                command.Result.ObservableCollection = new ObservableCollection<object>();
                foreach (var item in comdContainer.Result.ObservableCollection)
                { command.Result.ObservableCollection.Add((item as IHierarchicalView).ToEntityItem()); }
                
                
                //command.Result.DictionaryByInt = new Dictionary<int, object>();
                //foreach (var item in comdContainer.Result.DictionaryByInt)
                //{ command.Result.DictionaryByInt.Add(item.Key, (item.Value as IHierarchicalView).ToEntityItem()); }


                //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : ObservableCollection
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("GetItemsOnLevelByType", CommandSideEn.Client)]
        public CommandContainer GetItemsOnLevelByType(IPersistableBO ItemType, int Level)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.GetItemsOnLevelByType, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = ItemType.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (ItemType.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemTypeEntity))
                { throw new Exception("It is not an [ItemsOnLevel] component Type. You can use only objects with [ItemsOnLevel] component Type"); }

                //Теперь мы знаем что у нас это [ItemEntity] и тогда 1 получим имя АнцесторВью и 2 - Получим  Where условие для  данного объекта в ключевом поле
                Type ItemsOnLevelTypeName = ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponentType(ComponentsEn.ItemsOnLevel);
                ArgumentValidator.AssertNotNull<Type>(ItemsOnLevelTypeName, "ItemsOnLevelTypeName"); //Проверка на пустой параметр

                //Получаем ключевое свойство
                PropertyInfo keyProperty = (ItemType as IPersistableBO).GetPropertiesInRole(Meta.BO.PropertyRoleEn.PKeyField).FirstOrDefault();
                ArgumentValidator.AssertNotNull<PropertyInfo>(keyProperty, "KeyProperty");
                //И значение свойства 
                Int32 keyPropertyValue = (Int32)keyProperty.GetValue(ItemType, new object[] { });


                //Составляем запрос 
                DBSelectQuery query = DBSelectQuery.SelectAll().From(ItemsOnLevelTypeName).
                    Where(keyProperty.Name, Data.Compare.Equals, DBConst.Int32(keyPropertyValue)).
                    AndWhere("Level", Data.Compare.Equals, DBConst.Int32(Level));


                //Добавил параметр запроса
                outcomand.AddParam(CommandParamsEn.Query, query);

                //Добавим имя МоделиСервиса данного Узла
                outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages GetItemsOnLevelByType");
            }
        }


        #endregion ------------------------------ 9 GetItemsOnLevelByType------------------------

        #region --------- command 10 LoadItemCollections------------------




        /// <summary>
        /// SERVER COMMAND PART   
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [CommandExecutionMethod("LoadItemCollections", CommandSideEn.Server)]
        public CommandContainer LoadItemCollections(ref CommandContainer command)
        {
            try
            {
                //  --------- 1 Getting Parameters ( if Using Command with Params) ------

                //1 Получаем запрос для  Анцестра Иерархиии
                IPersistableBO Item = command.GetParam<IPersistableBO>(CommandParamsEn.ItemEntity);

                //2 Имя модели сервиса данного типа - чтобы получить тип Ancestor-a(реализующего IHierarchicalView )  
                ServiceModelManager_RegisterEn serviceModel = Item.GetObjectServiceManager().ServiceModel;


                //----------- 2 Custom Command Processing -------------------                
                IHierarchyServiceModelManager HierarchyServiceManager = (IHierarchyServiceModelManager)SystemDispatcher.Instance.Composition.GetServiceModelManager(serviceModel);
                Type ancestorType = HierarchyServiceManager.GetDataStructureComponentType(ComponentsEn.AncestorsView);


                //Выполним запрос через CRUDManager 
                CommandContainer comdItemPathToRootCollection = GetItemPathToRoot(Item);
                GetItemPathToRoot(ref comdItemPathToRootCollection);

                command.Result.DictionaryByInt = new Dictionary<int, object>();
                command.Result.DictionaryByInt.Add(0, new BOInfoTp()
                {
                    Key = "PathToRoot",
                    Collection = comdItemPathToRootCollection.Result.ObservableCollection
                });
                
                
                // Далее получить верхний уровень - 0 уровень Элементы у которых нет Родителей              
                CommandContainer cmdTopLevelCollection = GetItemsOnLevel(0, HierarchyServiceManager);
                GetItemsOnLevel(ref cmdTopLevelCollection);

                // LoadItemCollections - OUTPUT : DictionaryByInt 
                command.Result.DictionaryByInt.Add(1, new BOInfoTp()
                {
                    Key = "Level1",
                    Collection = cmdTopLevelCollection.Result.ObservableCollection
                });

                 
                for (int i = 0; i < (comdItemPathToRootCollection.Result.ObservableCollection.Count - 1); i++)
                {
                    CommandContainer cmdPathToRootItemChilds = GetItemChilds((IBObject)comdItemPathToRootCollection.Result.ObservableCollection[i]);
                    GetItemChilds(ref cmdPathToRootItemChilds);
                    command.Result.DictionaryByInt.Add
                                    ( i+2, new BOInfoTp()
                                        {
                                            Key = "Level" + (i + 2).ToString(),
                                            Collection = cmdPathToRootItemChilds.Result.ObservableCollection
                                        }
                                    );
                }


                // -----------------   3 Dod not forget Setting RESULTS to the Container -------------------                
                // Экспортируем в Коллекцию  типа Узла и сортированную по Уровню- Level-у


                // -------------------- 4 Say that we Do all Command Logic/ Return  ------------------------                 
                return command.SayCommandCompleteSuccesfully();
            }
            catch (Exception exc)
            {
                command.SayCommandThrowUnknownError(CommandManager_RegisterEn.CoreCommandManager.ToString(), exc.Message); // Error Info in Command Processing  
                throw; // Throw to Root EcecuteCommand method
            }
        }




        /// <summary>
        /// CLIENT COMMAND PART ...  OUTPUT : DictionaryByInt   
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [CommandExecutionMethod("LoadItemCollections", CommandSideEn.Client)]
        public CommandContainer LoadItemCollections(IPersistableBO Item)
        {
            // --------------------- 0 Creating CommandContainer -------------------------
            CommandContainer outcomand = new CommandContainer(CommandsEn.LoadItemCollections, CommandManager_RegisterEn.HierarchyCommandManager);

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                IServiceModelManager serviceManager = Item.GetObjectServiceManager();

                if (!serviceManager.IsServiceType(ServiceTypeEn.HierarchyStructureServiceType))
                { throw new Exception("It is not a Hierarchy Service"); }

                if (Item.GetType().Name != ((IHierarchyServiceModelManager)serviceManager).GetDataStructureComponent(ComponentsEn.ItemEntity))
                { throw new Exception("It is not an [ItemsOnLevel] component Type. You can use only objects with [ItemsOnLevel] component Type"); }
                
                outcomand.AddParam(CommandParamsEn.ItemEntity, Item);
                
                //outcomand.AddParam(CommandParamsEn.ServiceModel, serviceManager.ServiceModel.ToString());
                //------------------2 Setting DataItems ------------------------------


                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages LoadItemCollections");
            }
        }


        #endregion ------------------------------ 10 LoadItemCollections------------------------










        /// <summary>
        /// Запуск команды
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public CommandContainer ExecuteCommand(ref CommandContainer command)
        {
            try
            {
                return (this as ICommandManager).ExecuteServerSide(ref command);
            }
            catch (Exception)
            {
                return command; //Возвращаем все равно клиенту результат ошибки выполнения 
            }
        }


    }
}
