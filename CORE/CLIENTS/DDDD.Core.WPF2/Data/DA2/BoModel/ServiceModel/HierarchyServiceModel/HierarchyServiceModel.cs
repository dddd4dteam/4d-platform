﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2 
{

    /// <summary>
    ///  NOT USED NOW
    /// </summary>
    //public interface IHierarchicalItem : IBObject
    //{
    //    /// <summary>
    //    /// Уровыень на котором находится Иерархический элемент
    //    /// </summary>
    //    Int32 Level { get; }
    //}


    /// <summary>
    /// HierarchicalView
    /// </summary>
    public interface IHierarchicalView : IBObject
    {

        /// <summary>
        /// Convert  this View object to  [ItemEntity] Type object
        /// </summary>
        /// <returns></returns>
        IBObject ToEntityItem();

        /// <summary>
        /// Deep level in Tree
        /// </summary>
        int Level { get; }
    }





    /// <summary>
    /// Specific ServiceModel -Hierarchy - Tree Data Structure implementation for RDBMS
    /// </summary>
    public abstract class HierarchyServiceModel : ServiceModel//, IHierarchyServiceModel
    {


        /// <summary>
        /// Generate DB Script from snippet text - as usually used in Spicific Service Moel
        /// </summary>
        /// <param name="marekerReplaceVals"></param>
        /// <returns></returns>
        public override string GenerateDBScript(Dictionary<string, string> marekerReplaceVals)
        {
            return null;// TO DO             
        }


        /// <summary>
        /// ServiceModel Type - standart or specific service Model        
        /// </summary>
        public override ServiceModelTypeEn ServiceModelType
        { get; } = ServiceModelTypeEn.SpecificServiceModel;



        /// <summary>
        /// Default Components for HierarchyServiceModel. 
        /// values-contract  here is null and should be filled in concrete ServiceModel.
        /// </summary>
        protected override Dictionary<string, Type> Components
        { get; set; } = new Dictionary<string, Type>()
        {
             { Component.ItemEntity, null } 
            ,{ Component.HierarchyEntity, null }
            ,{ Component.ItemTypeEntity, null }
            ,{ Component.AncestorsView, null }
            ,{ Component.DescndantsView, null }
            ,{ Component.ItemsOnLevel, null }
        };

        /// <summary>
        /// Hierarchy Service Model  components   
        /// </summary>
        public class Component
        {
            public const string ItemEntity = nameof(ItemEntity);
            public const string HierarchyEntity = nameof(HierarchyEntity);
            public const string ItemTypeEntity = nameof(ItemTypeEntity);
            public const string AncestorsView = nameof(AncestorsView);
            public const string DescndantsView = nameof(DescndantsView);
            public const string ItemsOnLevel = nameof(ItemsOnLevel);
        }

        public Type Get_ItemEntity()
        {
            return GetComponentType(Component.ItemEntity); 
        }

        public Type Get_HierarchyEntity()
        {
            return GetComponentType(Component.HierarchyEntity);
        }

        public Type Get_ItemTypeEntity()
        {
            return GetComponentType(Component.ItemTypeEntity);
        }

        public Type Get_AncestorsView()
        {
            return GetComponentType(Component.AncestorsView);
        }
        public Type Get_DescndantsView()
        {
            return GetComponentType(Component.DescndantsView);
        }
        public Type Get_ItemsOnLevel()
        {
            return GetComponentType(Component.ItemsOnLevel);
        }


       
       
      


    }
}
