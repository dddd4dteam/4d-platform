﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Data.DA2.Query;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

namespace DDDD.Core.Data.DA2.DCServices
{

    //contains Current - singleton instance property
    //Execute command
    public class SqlCRUDService : SingletonDCService<SqlCRUDService>
    {       

        #region  ------------------------  Singleton Service Initialization  -------------------------

        protected override void Initialize()
        {
            if (IsInited) return;

            base.Initialize();
            SvcDescription = "MS SQL  CRUD DC(Dynamic Command) service Service";

            IsInited = true;
        }


        #endregion ------------------------  Singleton Instance  Initialization  -------------------------


        #region ----------- COMMANDS AND COMMAND PARAMETER KEYS/NAMES ---------

        /// <summary>
        /// DC(Dynamic Command) Service Command Names/Keys
        /// </summary>
        public class Cmd
        {

            public const string NotDefined = nameof(NotDefined);


            public const string SelectItem = nameof(SelectItem);
            public const string SelectCollection = nameof(SelectCollection);
            public const string InsertItem = nameof(InsertItem);
            public const string InsertCollection = nameof(InsertCollection);
            public const string DeleteCollection = nameof(DeleteCollection);
            public const string UpdateItem = nameof(UpdateItem);
            public const string UpdateItemFields = nameof(UpdateItemFields);
            public const string UpdateCollection = nameof(UpdateCollection);
            public const string UpdateFieldsByFilter = nameof(UpdateFieldsByFilter);


            public const string SelectDataContext = nameof(SelectDataContext);
            /// <summary>
            /// 12  Update Service Data Model by DataContext collections
            /// </summary>
            public const string UpdateModelByDataContext = nameof(UpdateModelByDataContext);
            /// <summary>            
            ///13 Delete DataContext collections from DB
            /// </summary>
            public const string DeleteDataContext = nameof(DeleteDataContext);

            /// <summary>            
            ///14 Select several collections of BO, each collection will be getted by its own select query. 2-5  COllections with Queries
            /// </summary>
            public const string SelectBOCollections = nameof(SelectBOCollections);
            public const string SelectCollectionSlowlyButSurely = nameof(SelectCollectionSlowlyButSurely);
            public const string GetChangeTrackingCurrentVersion = nameof(GetChangeTrackingCurrentVersion);
            public const string GetSyncChanges = nameof(GetSyncChanges);

            //GetRoles            
            public const string Max = nameof(Max);
            public const string SelectInt = nameof(SelectInt);
            public const string InsertCollectionWithOneQuery = nameof(InsertCollectionWithOneQuery);
            public const string DeleteCollectionFast = nameof(DeleteCollectionFast);
            public const string GetServerDateTime = nameof(GetServerDateTime);
            public const string GetCodeIdPrefix = nameof(GetCodeIdPrefix);

            public const string CreateDatabaseBackupForClient = nameof(CreateDatabaseBackupForClient);
            public const string DeleteDatabaseBackupByID = nameof(DeleteDatabaseBackupByID);
            public const string GetBDBackupChunk = nameof(GetBDBackupChunk);
            public const string GetCreateDatabaseBackupForClientCmdResult = nameof(GetCreateDatabaseBackupForClientCmdResult);

            public const string CreateTempDBCopy = nameof(CreateTempDBCopy);
            public const string CreateTempDBBackup = nameof(CreateTempDBBackup);
            public const string RestoreTempDBBackup = nameof(RestoreTempDBBackup);

        }

        /// <summary>
        /// DC(Dynamic Command)Command Parameters  Names/Keys
        /// </summary>
        public class CmdPrm
        {
            public const string NotDefined = nameof(NotDefined);
            public const string Param1 = nameof(Param1);       // заглушка

            public const string InsertQueryObjectParam = nameof(InsertQueryObjectParam);


            public const string QueryBObjectParam = nameof(QueryBObjectParam);
            public const string DataContextParam = nameof(DataContextParam);

            /// <summary>
            /// 
            /// </summary>
            public const string DataContractTypeName = nameof(DataContractTypeName);

            /// <summary>
            /// 
            /// </summary>
            public const string UpdateItemFieldsListParam = nameof(UpdateItemFieldsListParam);


            #region ----------Select Collection Params---------------

            ///////////////////////////Usual Param//////////////////////////////
            /// <summary>
            /// Параметр обычного запроса Селекта Коллекции - без Пейджинга
            /// </summary>
            public const string SelectQueryParam = nameof(SelectQueryParam);


            ///////////////////////////Pageable Params//////////////////////////////            
            /// <summary>
            /// Параметр Запроса Количества элементов  в Педжинговом Запросе у Селекта Коллекции (При пейджинге)
            /// </summary>
            public const string SelectQueryResultSizeParam = nameof(SelectQueryResultSizeParam);

            /// <summary>
            /// Параметр  Педжингового запроса у Селекта Коллекции (При пейджинге)
            /// </summary>
            public const string PageableSelectQueryParam = nameof(PageableSelectQueryParam);

            /// <summary>
            /// Параметр с информацией о пейджинге
            /// </summary>
            public const string PagingInfoParam = nameof(PagingInfoParam);

            #endregion ----------Select Collection Params---------------

            #region ----------------------- Select BOCollections  ----------------------------

            public const string SelectQuery = nameof(SelectQuery);
            public const string SelectBOQuery1 = nameof(SelectBOQuery1);
            public const string SelectBOQuery2 = nameof(SelectBOQuery2);
            public const string SelectBOQuery3 = nameof(SelectBOQuery3);
            public const string SelectBOQuery4 = nameof(SelectBOQuery4);
            public const string SelectBOQuery5 = nameof(SelectBOQuery5);

            public const string SelectBOTable1 = nameof(SelectBOTable1);
            public const string SelectBOTable2 = nameof(SelectBOTable2);
            public const string SelectBOTable3 = nameof(SelectBOTable3);
            public const string SelectBOTable4 = nameof(SelectBOTable4);
            public const string SelectBOTable5 = nameof(SelectBOTable5);

            #endregion --------------------SelectBOCollections  ----------------------------

            public const string TableNamesList = nameof(TableNamesList);
            public const string ChangeTrackingCurrentVersion = nameof(ChangeTrackingCurrentVersion);
            public const string ChangeTrackingPreviousVersion = nameof(ChangeTrackingPreviousVersion);

            public const string UserID = nameof(UserID);
            public const string Field = nameof(Field);

            public const string BDBackupGuid = nameof(BDBackupGuid);
            public const string UserGroupName = nameof(UserGroupName);
            public const string PartitioningViewNamesList = nameof(PartitioningViewNamesList);
            public const string ChunkNum = nameof(ChunkNum);
            public const string BackupGuid = nameof(BackupGuid);
            public const string CommandGuid = nameof(CommandGuid);
            public const string ChangeTrackingContext = nameof(ChangeTrackingContext);
        }

        #endregion ----------- COMMANDS AND COMMAND PARAMETER KEYS/NAMES ---------


        // ::: Server DCAction need not try/catch block
        //     that's already done in base class
        // ::: CLent Part DCAction do not need try/catch block  too- 
        //     that must be done in upper in call stack class .
        // ::: Only  ServerInContext methods should  contains try/catch redirect logic block


        #region --------- command 1 SelectItem------------------


        /// <summary>
        /// Cmd.SelectItem -  DC Action  ServerInContext overload
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd.SelectItem, DCSideEn.ServerInContext)]
        public object SelectItem(DSelectQuery selectQuery, String dataContractType, DBProxy dbProxy)
        {
            try
            {
                String SqlQueryText = selectQuery.ToSQLString( dbProxy.Provider.Key);
                var contractType = TypeInfoEx.GetByLN(dataContractType);
                return dbProxy.SelectItem(contractType.OriginalType, SqlQueryText);
                    //dbmngr.SetCommand(SqlQueryText)
                    //.ExecuteObject(dataContractType.GetDomainContractType());
            }
            catch (Exception exc)
            {
                throw exc; // Throw to Root EcecuteCommand method
            }
        }


        /// <summary>
        /// Cmd.SelectItem -  DC Action  ServerInContext2 overload
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <param name="selectFilter"></param>
        /// <param name="dbProxy"></param>
        /// <returns></returns>
        [DC(Cmd.SelectItem, DCSideEn.ServerInContext2)]
        public object SelectItem<TContract>(DFilterSet selectFilter, DBProxy dbProxy)
        {
            try
            {
                DSelectQuery SelectQuery = DSelectQuery.SelectAll()
                    .From<TContract>(); // owner - may be dbo if we need it
                SelectQuery.WhereSet = selectFilter;
                String SqlQueryText = SelectQuery.ToSQLString(dbProxy.Provider.Key);
                return dbProxy.SelectItem<TContract>(SqlQueryText); //  SetCommand(SqlQueryText)
                                                        //.ExecuteObject(typeof(TContract));
            }
            catch (Exception exc)
            {
                throw exc; // Throw to Root EcecuteCommand method
            }
        }


        /// <summary>
        ///  Cmd.SelectItem -  DC Action SERVER PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd.SelectItem, DCSideEn.Server)]
        public IDCMessage SelectItem(ref IDCMessage command)
        {
           

            //  --------- 1 Getting Parameters ( if Using Command with Params) ------
            //Проверка параметра Имени пользователя
            DSelectQuery selectQuery = command.GetParamVal<DSelectQuery>("SelectQueryParam");
            string dataContractTypeName = command.GetParamVal<string>("DataContractTypeName");

            //----------- 2 Custom Command Processing -------------------
            //object item = null;
            object result = SelectItem(selectQuery, dataContractTypeName, DBProxy.DefProx);
            command.SetResult(result);
            
            //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
            command.ProcessingSuccessMessage = "Completed successfully";
            return command;

 
        }




        /// <summary>
        /// Cmd.SelectItem - DC Action  CLIENT PART 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [DC(Cmd.SelectItem, DCSideEn.Client)]
        public IDCMessage SelectItem(DSelectQuery selectQuery, Type dataContractType)
        {
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery, nameof(SelectItem), nameof(selectQuery));
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(dataContractType, nameof(SelectItem), nameof(dataContractType));
            
            // --------------------- 0 Creating DCMessage -------------------------
            IDCMessage outcomand =  DCMessage.Create(SvcName, Cmd.SelectItem );            
           
            // ----------------  1 Adding Parameters  ----------------------------- 
            outcomand.NewInParam(CmdPrm.SelectQueryParam, selectQuery);
            outcomand.NewInParam(CmdPrm.DataContractTypeName, dataContractType.FullName);
            //------------------2 Setting DataItems ------------------------------
                
            //------------------3 Returning Custom Command -----------------------
            return outcomand;
           
        }


        #endregion ------------------------------ 1 SelectItem------------------------



        #region --------- command 2 SelectCollection------------------

        /// <summary>
        /// CLIENT COMMAND PART 
        /// Просто выборка коллекции с определенными фильтрами
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <returns></returns>
        [DC(Cmd.SelectCollection, DCSideEn.Client)]
        public DCMessage SelectCollection(DSelectQuery selectQuery)//, Type dataContractType
        {
            // ----------------  1 Adding Parameters  ----------------------------- 
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery,nameof(SelectCollection),nameof(selectQuery));
            Validator.ATNullReferenceArgDbg<DTable>(selectQuery.SelectionTable, nameof(SelectCollection), nameof(selectQuery.SelectionTable));
         
            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand =  DCMessage.Create(Cmd.SelectCollection,SvcName );
            //outcomand.CommandInfo.CRUDType = CRUDTypeEn.Read;
                       
            // ----------------  1 Adding Parameters  ----------------------------- 
            outcomand.NewInParam(CmdPrm.SelectQueryParam, selectQuery);

            //------------------2 Setting DataItems ------------------------------
            
            //Отменяем всякий там пейджинг для селекта
            //outcomand.CommandInfo.LoadingType = LoadingCollectionTypeEn.WholeResultCollection;//adding paging to the currentCommand
                       
            //------------------3 Returning Custom Command -----------------------
            return outcomand;

        }



        /// <summary>
        /// CLIENT COMMAND PART 
        /// В данном случае всегда получается размер коллекции
        /// </summary>
        /// <param name="selectQuery">Простой запрос селекта от Клиента</param>
        /// <param name="paging"> Информация о выборке страниц </param>
        /// <returns>(CollectionSize != null) и ObservableCollection </returns>
        [DC(Cmd.SelectCollection, DCSideEn.ClientPagableInit)]
        public DCMessage SelectCollection(DSelectQuery selectQuery, DPageInfo paging)//Type dataContractType, 
        {
            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(Cmd.SelectCollection, SvcName );
            //outcomand.CommandInfo.CRUDType = CRUDTypeEn.Read;
            
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery, nameof(SelectCollection), nameof(selectQuery));
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery.SelectionTable, nameof(SelectCollection), nameof(selectQuery.SelectionTable));
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(paging, nameof(SelectCollection), nameof(paging));
            
            try
            {

                // ----------------  1 Adding Parameters  ----------------------------- 
                //Сформировать передаваемые два запроса               
                DSelectQuery PageableSelectQuery = selectQuery.CloneClause() as DSelectQuery;
                // PageIndex-1 потому что это параметр означает Сколько страниц пропустить 
                DSelectQuery.PageableQuery(ref PageableSelectQuery, paging.PageSize, paging.PageIndex - 1);
                
                DSelectQuery CountSelectQuery = selectQuery.CloneClause() as DSelectQuery;
                CountSelectQuery.SelectSet.ReplaceByCount();


                // ----------------  1 Adding Parameters  ----------------------------- 
                outcomand.NewInParam(CmdPrm.PageableSelectQueryParam, PageableSelectQuery);
                outcomand.NewInParam(CmdPrm.SelectQueryResultSizeParam, CountSelectQuery);
                

                //------------------2 Setting DataItems ------------------------------
                //Говорим о том что у нас используется пейджинг - 
                //и что также нужно использовать запрос о размере результата 

                //outcomand.CommandInfo.LoadingType = LoadingCollectionTypeEn.FirstPageOfCollection; // CompareTo//  UsePagingQuery = true;
                //outcomand.CommandInfo.UseResultSizeQuery = true;

                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages SelectCollection");
            }
        }



        /// <summary>
        /// CLIENT COMMAND PART 
        /// В данном случае не всегда получается размер коллекции. 
        /// </summary>
        /// <param name="selectQuery">Простой запрос селекта от Клиента с набором фильтров</param>
        /// <param name="paging">Информация о выборке страниц</param>
        /// <param name="UsePaging">Получить ли CollectionSize</param>
        /// <returns>CollectionSize? и ObservableCollection</returns>
        [DC(Cmd.SelectCollection, DCSideEn.ClientPagableTransition)]
        public DCMessage SelectCollection(DSelectQuery selectQuery, DPageInfo paging, bool UseResultSize)//Type dataContractType, 
        {
            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(Cmd.SelectCollection, SvcName);            
            //outcomand.CommandInfo.CRUDType = CRUDTypeEn.Read;

            try
            {
                // ----------------  1 Adding Parameters  ----------------------------- 
                Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery, nameof(SelectCollection), nameof(selectQuery));
                Validator.ATNullReferenceArgDbg<SqlCRUDService>(selectQuery.SelectionTable, nameof(SelectCollection), nameof(selectQuery.SelectionTable));
                Validator.ATNullReferenceArgDbg<SqlCRUDService>(paging, nameof(SelectCollection), nameof(paging));
                
                //------------------2 Setting DataItems ------------------------------
                //Сформировать передаваемые два запроса

                //1 Говорим о том что у нас используется пейджинг - 
                //Постраничный запрос
                DSelectQuery PageableSelectQuery = selectQuery.CloneClause() as DSelectQuery;
                // PageIndex-1 потому что это параметр означает Сколько страниц пропустить 
                DSelectQuery.PageableQuery(ref PageableSelectQuery, paging.PageSize, paging.PageIndex - 1);                
                outcomand.NewInParam(CmdPrm.PageableSelectQueryParam, PageableSelectQuery);

                //2 Решить вопрос о том - будет ли выполняться запрос на получение размера коллекции 
                if (UseResultSize)
                {
                    //outcomand.CommandInfo.LoadingType = LoadingCollectionTypeEn.FirstPageOfCollection;

                    //Запрос размера результата
                    DSelectQuery CountSelectQuery = selectQuery.CloneClause() as DSelectQuery;
                    CountSelectQuery.SelectSet.ReplaceByCount();

                    outcomand.NewInParam(CmdPrm.SelectQueryResultSizeParam, CountSelectQuery);
                }
                else
                { 
                    //outcomand.CommandInfo.LoadingType = LoadingCollectionTypeEn.SomePageOfResultCollection; 
                }

                //------------------3 Returning Custom Command -----------------------
                return outcomand;
            }
            catch (Exception)
            {
                throw new Exception("Error in Client RegisterPages SelectCollection");
            }
        }



        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd.SelectCollection, DCSideEn.Server)]
        public DCMessage SelectCollection(ref DCMessage command)
        {
            //IN PARAMS:  
            //  SelectQueryParam  
            //  PageableSelectQueryParam
            //  SelectQueryResultSizeParam

            
            if (command. ContainsParam(CmdPrm.SelectQueryParam))
            { //standart select 
                var MainQuery = command.GetParamVal<DSelectQuery>(CmdPrm.SelectQueryParam);               
                //2 Выполнить основной запрос
                command.SetResult( SelectCollection(MainQuery, DBProxy.DefProx) ); 

            }
            else if (command.ContainsParam(CmdPrm.PageableSelectQueryParam))
            { //pageable select 

                var PageableSelectQuery  = command.GetParamVal<DSelectQuery>(CmdPrm.PageableSelectQueryParam);
                //2 Выполнить основной запрос
                command.SetResult(
                      SelectCollection(PageableSelectQuery, DBProxy.DefProx)  );

                //only for first page query client should add this query to server
                if (command.ContainsParam(CmdPrm.SelectQueryResultSizeParam))
                { //pageable select  with result size query
                    var SelectQueryResultSize = command.GetParamVal<DSelectQuery>(CmdPrm.SelectQueryResultSizeParam);
                    command.NewOutParam("ResultSizeValue",
                      DBProxy.DefProx.SelectItem<int?>(SelectQueryResultSize)
                        );
                }

            }
                                 
            return command.SayCommandCompleteSuccesfully();            
        }


        /// <summary>
        /// SERVER InContext method - for select as command inside Transaction 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="dataContractType"></param>
        /// <param name="dbmngr"></param>
        /// <returns></returns>
        [DC(Cmd.SelectCollection, DCSideEn.ServerInContext)]
        public IEnumerable SelectCollection(DSelectQuery selectQuery, DBProxy dbProxy, int TimeoutInSecs = 30)
        {
            try
            {
                var boType = ServiceModel.GetBOContractByNameGlobal(selectQuery.SelectionTable.TableTypeName);                                        
                String SqlQueryText = selectQuery.ToSQLString(dbProxy.Provider.Key);
                return dbProxy.SelectList(boType, SqlQueryText, TimeoutInSecs);
                
            }
            catch (Exception exc)
            {
                throw exc; // Throw to Root EcecuteCommand method
            }
        }


        /// <summary>
        /// SERVER InContext2 method - for select as command inside Transaction 
        /// </summary>
        /// <param name="selectQuery"></param>
        /// <param name="tblType"></param>
        /// <param name="dbmngr"></param>
        /// <returns></returns>
        //[DC(Cmd.SelectCollection, DCSideEn.ServerInContext2)]
        //public IEnumerable SelectCollection(String selectQuery, Type tblType, DBProxy dbProxy)
        //{
        //    try
        //    {
        //        return   dbProxy.SelectList(tblType, selectQuery);
        //    }
        //    catch (Exception exc)
        //    {
        //        throw exc; // Throw to Root EcecuteCommand method
        //    }
        //}

        /// <summary>
        /// SERVER InContext3 method - for select as command inside Transaction 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="selectQuery"></param>
        /// <param name="dbmngr"></param>
        /// <returns></returns>
        //[DC(Cmd.SelectCollection, DCSideEn.ServerInContext3)]
        //public IEnumerable SelectCollection<TTable>(String selectQuery, DBProxy  dbProxy)//String dataContractType, 
        //{
        //    try
        //    {
        //        return dbProxy.SelectList<TTable>(selectQuery);                    
        //    }
        //    catch (Exception exc)
        //    {
        //        throw exc; // Throw to Root EcecuteCommand method
        //    }
        //}


        #endregion ------------------------------ 2 SelectCollection------------------------


        #region --------- command 3 InsertItem------------------
            
        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [DC(Cmd.InsertItem, DCSideEn.Client)]
        public DCMessage InsertItem(IBObject insertObject)
        {
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(insertObject, nameof(InsertItem), nameof(insertObject));

            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(Cmd.InsertItem, SvcName);
          
            // ----------------  1 Adding Parameters  ----------------------------- 
            var queryInsert = DInsertQuery.InsertInto(insertObject);// GetQuery(insertObject, ProviderKey.DefaultKey);
            queryInsert.ToSQLString(ProviderKey.Default); // prebuild Query

            outcomand.NewInParam(CmdPrm.InsertQueryObjectParam, queryInsert);

            //------------------2 Setting DataItems ------------------------------

            //------------------3 Returning Custom Command -----------------------
            return outcomand;
            
        }
        


        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd.InsertItem, DCSideEn.Server)]
        public DCMessage InsertItem(ref DCMessage command)
        {            
            //1 --------------- GettingParams -----------------
            var insertQuery = command.GetParamVal<DInsertQuery>(CmdPrm.InsertQueryObjectParam);
                
            var reslt = DBProxy.DefProx.InsertItem(insertQuery);
                 
            command.SetResult(reslt);
            command.SayCommandCompleteSuccesfully();
                
            return command;           
        }


        #endregion ------------------------------ 3 InsertItem------------------------


        #region --------- command 4 InsertCollection------------------
               

        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [DC(Cmd.InsertCollection, DCSideEn.Client)]
        public DCMessage InsertCollection(List<IBObject> insertColl,Guid changeTrackingContext)
        {
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(insertColl, nameof(InsertCollection), nameof(insertColl));

            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(Cmd.InsertCollection,  SvcName);
                
            // ----------------  1 Adding Parameters  ----------------------------- 
            outcomand.NewInParam(CmdPrm.QueryBObjectParam, insertColl);
            outcomand.NewInParam(CmdPrm.ChangeTrackingContext , changeTrackingContext);
            
            //------------------2 Setting DataItems ------------------------------

            //------------------3 Returning Custom Command -----------------------
            return outcomand;             
        }


        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>

        [DC(Cmd.InsertCollection, DCSideEn.Server)]
        public DCMessage InsertCollection(ref DCMessage command)
        {
            // HERE WE alse check for Synchronization context 
          

            //  --------- 1 Getting Parameters ( if Using Command with Params) ------
            //Проверка параметра Имени пользователя
            var insertColl = command.GetParamVal< List<IBObject> >(CmdPrm.QueryBObjectParam);
            var changeTrackingContext = command.GetParamVal<Guid>(CmdPrm.ChangeTrackingContext);

            var dbPropxy = DBProxy.DefProx;
            dbPropxy.RunTransaction(IsolationLevel.ReadCommitted
                , () => 
                {
                    foreach (IBObject pop in insertColl)
                    {
                        try
                        {
                            dbPropxy.InsertItemTrc(pop, changeTrackingContext);
                        }
                        catch (Exception exc)
                        {
                            // Предполагается что если при синхронизации произошла подобная ошибка
                            // , то это нормально,
                            // т.к. синхронизация могла производиться повторно, при возникновении ошибок,
                            // либо после того как был произведен Insert на клиенте 
                            //был сделан Update на сервере другим человеком
                            // до синхронизации сервер -> клиент. В этом случае CHANGETABLE вернет измененную запись как Insert, а по факту нужен Update
                            //if (command.CommandInfo.IsSync && exc.Message.Contains("Violation of PRIMARY KEY constraint"))
                            //{
                            //    if (!pop.GetType()
                            //       .GetTypeAttribute<NotUpdatableTableAttribute>().Any())
                            //        UpdateItem(pop, dbMngr, command.CommandInfo.ChangeTrackingContext);
                            //}
                            //else
                            //    throw;
                        }
                    }  
                }
                ,null
                );

            // -------------------- 4 Say that we Do all Command Logic/Return -----------------------------                                 
            return command.SayCommandCompleteSuccesfully();
           
        }




        /// <summary>
        /// CLIENT COMMAND PART 
        /// </summary>
        [DC(Cmd.InsertCollectionWithOneQuery, DCSideEn.Client)]
        public DCMessage InsertCollectionWithOneQuery(List<IBObject> insertColl, Guid changeTrackingContext)
        {
            Validator.ATNullReferenceArgDbg<SqlCRUDService>(insertColl, nameof(InsertCollectionWithOneQuery), nameof(insertColl));

            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(Cmd.InsertCollectionWithOneQuery, SvcName);

            // ----------------  1 Adding Parameters  ----------------------------- 
            outcomand.NewInParam(CmdPrm.QueryBObjectParam, insertColl);
            outcomand.NewInParam(CmdPrm.ChangeTrackingContext, changeTrackingContext);
            
            //------------------3 Returning Custom Command -----------------------
            return outcomand;             
        }
               

        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd. InsertCollectionWithOneQuery, DCSideEn.Server)]
        public DCMessage InsertCollectionWithOneQuery(ref DCMessage command)
        {           
            //  --------- 1 Getting Parameters ( if Using Command with Params) ------
            //Проверка параметра Имени пользователя
            var insertColl = command.GetParamVal<List<IBObject>>(CmdPrm.QueryBObjectParam);       // 
            var changeTrackingContext = command.GetParamVal<Guid>(CmdPrm.ChangeTrackingContext); // 

            var dbProxy = DBProxy.DefProx;

            dbProxy.RunTransaction(IsolationLevel.ReadCommitted
                , () =>
                {
                    //Insert in transaction with Grouping items per insertGroupSize in each INSERT Query
                    dbProxy.InsertListTrc(insertColl
                                            , insertGroupSize: 100
                                            , changeTrackingContext);
                }
                , null//no Rollback Action
                );                

            // -------------------- 4 Say that we Do all Command Logic/Return -----------------------------                                 
            return command.SayCommandCompleteSuccesfully();            

        }


        #endregion ------------------------------ 4 InsertCollection------------------------

              

        #region --------- command 7 UpdateItem------------------

        /// <summary>
        /// CLIENT COMMAND PART 
        /// Out result - Boolean
        /// </summary>
        /// <param name="Param1"></param>        
        /// <returns></returns>
        [DC(Cmd.UpdateItem, DCSideEn.Client)]
        public DCMessage UpdateItem(IBObject boItem, Guid? changeTrackingContext)
        {
            // --------------------- 0 Creating DCMessage -------------------------
            DCMessage outcomand = DCMessage.Create(SvcName, Cmd.UpdateItem);

            Validator.ATNullReferenceArgDbg<SqlCRUDService>(boItem, nameof(UpdateItem), nameof(boItem));

            // ----------------  1 Adding Parameters  ----------------------------- 
            outcomand.NewInParam(CmdPrm.QueryBObjectParam, boItem);
            
            if (changeTrackingContext.HasValue)
            { outcomand.NewInParam(CmdPrm.ChangeTrackingContext, changeTrackingContext);
            }
            
            
            //------------------2 Setting DataItems ------------------------------

            //------------------3 Returning Custom Command -----------------------
            return outcomand;
        }



        /// <summary>
        /// SERVER COMMAND PART
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [DC(Cmd.UpdateItem, DCSideEn.Server)]
        public DCMessage UpdateItem(ref DCMessage command)
        {             
            //  --------- 1 Getting Parameters ( if Using Command with Params) ------
            // Проверка параметра Имени пользователя
            IBObject item = command.GetParamVal<IBObject>(CmdPrm.QueryBObjectParam);

            Guid? changeTrackingContext = null;
            if (command.ContainsParam(CmdPrm.ChangeTrackingContext))
            {   changeTrackingContext = command.GetParamVal<Guid>(CmdPrm.ChangeTrackingContext);
            }

            //Есть ли поля для Обновления в данной таблице - т.е. если все поля первичные ключи - значнит полй нет и обновлять не нужно --> и тогда мы не обновляем                 
            //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                    
            if ( (int)DBProxy.DefProx.UpdateItem(item, changeTrackingContext) > 0) 
            {  command.SetResult( true );
               command.SayCommandCompleteSuccesfully();               
            }
            // else - exceptopm sshould be called in upper levell call in  stackcall

            //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
            return command;
        }




        #endregion ------------------------------ 7 UpdateItem------------------------




        //////// NEW STEP TO DO
        ///
        //#region --------- command 8 UpdateItemFields------------------

        ///// <summary>
        ///// CLIENT COMMAND PART 
        ///// Out result - Boolean
        ///// </summary>
        ///// <param name="Param1"></param>        
        ///// <returns></returns>
        //[DC(Cmd.UpdateItemFields, DCSideEn.Client)]
        //public DCMessage UpdateItemFields(IBObject Object, params String[] ObjectUpdateFields)
        //{
        //    // --------------------- 0 Creating DCMessage -------------------------
        //    DCMessage outcomand = new DCMessage(CommandsEn.UpdateItemFields, CommandManager_RegisterEn.CRUDCommandManager);


        //    Validator.AssertNotNullAndOfType<IBObject>(Object, CommandParamsEn.QueryBObjectParam.ToString());

        //    if (ObjectUpdateFields == null || ObjectUpdateFields.Count() == 0)
        //        throw new ArgumentException("Expected argument ObjectUpdateFields");


        //    // ----------------  1 Adding Parameters  ----------------------------- 
        //    outcomand.AddParam(CommandParamsEn.QueryBObjectParam, Object);
        //    outcomand.AddCollectionParam(CommandParamsEn.UpdateItemFieldsListParam, ObjectUpdateFields.ToList<object>());

        //    //------------------2 Setting DataItems ------------------------------


        //    //------------------3 Returning Custom Command -----------------------
        //    return outcomand;

        //}



        ///// <summary>
        ///// SERVER COMMAND PART
        ///// </summary>
        ///// <param name="command"></param>
        ///// <returns></returns>
        //[DC(Cmd.UpdateItemFields, DCSideEn.Server)]
        //public DCMessage UpdateItemFields(ref DCMessage command)
        //{          
        //    //  --------- 1 Getting Parameters ( if Using Command with Params) ------
        //    // Проверка параметра Имени пользователя
        //    IBObject Object = command.GetParam<IBObject>(CommandParamsEn.QueryBObjectParam);
        //    List<String> UpdateFields = command.GetCollectionParam<String>(CommandParamsEn.UpdateItemFieldsListParam);
            
        //    using (DbManager dbMngr = new DbManager(new SqlDataProvider(), ConfigurationDispatcher.DomainConnectionString))
        //    {
        //        //Есть ли поля для Обновления в данной таблице - т.е. если все поля первичные ключи - значнит полй нет и обновлять не нужно --> и тогда мы не обновляем                 
        //        //-----------------   3 Dod not forget Setting RESULTS to the Container --------------------                    
        //        if ((int)UpdateItemFields(Object, dbMngr, command.CommandInfo.ChangeTrackingContext, UpdateFields.ToArray()) > 0) { command.Result.BooleanResult = true; }
        //    }

        //    //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
        //    return command.SayCommandCompleteSuccesfully();
           
        //}


        ///// <summary>
        ///// SERVER InContext method - for select as command inside Transaction 
        ///// </summary>
        ///// <param name="command"></param>
        ///// <param name="dataContractType"></param>
        ///// <param name="dbmngr"></param>
        ///// <returns></returns>
        //[DC(Cmd.UpdateItemFields, DCSideEn.ServerInContext2)]
        //public object UpdateItemFields(IBObject UpdateObject, DbManager dbmngr, Guid ChangeTrackingContext, params String[] UpdateFields)
        //{
        //        IPersistableBO tableObject = null;

        //        if (UpdateObject.GetBoRoles().Contains(Meta.BO.BORoleEn.VObject))
        //            tableObject = (IPersistableBO)((IVobject)UpdateObject).CloneToTarget();
        //        else
        //            tableObject = (IPersistableBO)UpdateObject;

        //        //Сверим условия перед запросом
        //        if (tableObject == null || UpdateFields == null || UpdateFields.Count() == 0) return 0;



        //        DUpdateQuery updateQuery = DUpdateQuery.Update(tableObject.GetType()) // .Name   Объявляем для Какой Таблицы будет Обновление  
        //            .BuildUpdateSetFields(tableObject, UpdateFields) // Построим запрос обновить Конкретные поля
        //            .BuildUpdateWhereFieldsOnAnd(tableObject, tableObject.PrimaryKey) // Добавим Where условие для обновления именно данного одного объекта
        //            .WithChangeTrackingContext(ChangeTrackingContext);


        //        dbmngr.Command.CommandText = updateQuery.ToSQLString(SystemDispatcher.Instance.DefaultDomainDB);  //Set Command To manager
        //        dbmngr.ExecuteScalar();
        //        return 1; // Affected Rows

           
        //}



       

        //#endregion ------------------------------ 8 UpdateItemFields ------------------------


        //#region --------- command 9 UpdateCollection---------------------




        ///// <summary>
        ///// SERVER COMMAND PART
        ///// </summary>
        ///// <param name="command"></param>
        ///// <returns></returns>
        //[DC(Cmd.UpdateCollection, DCSideEn.Server)]
        //public DCMessage UpdateCollection(ref DCMessage command)
        //{           
        //    //  --------- 1 Getting Parameters ( if Using Command with Params) ------
        //    //Проверка параметра Имени пользователя
        //    List<IBObject> Coll = command.GetCollectionParam<object>(CommandParamsEn.QueryBObjectParam).ToList<IBObject>(); // IBObjectCollection

        //    using (DbManager dbMngr = new DbManager(new SqlDataProvider(), ConfigurationDispatcher.DomainConnectionString))
        //    {
        //        dbMngr.BeginTransaction();
        //        foreach (IBObject pop in Coll) //.Items.Values
        //        {
        //            ArgumentValidator.AssertGreaterThan((int)
        //                UpdateItem(pop, dbMngr, command.CommandInfo.ChangeTrackingContext), 0, "returned by UpdateItem strings");
        //        }
        //        dbMngr.CommitTransaction();
        //    }

        //    //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
        //    return command.SayCommandCompleteSuccesfully();
           
        //}



        ///// <summary>
        ///// Обновить Коллекцию
        ///// </summary>
        ///// <param name="insertObjects"></param>
        ///// <param name="dbmngr"></param>
        //[DC(Cmd.UpdateCollection, DCSideEn.ServerInContext)]
        //public void UpdateCollection(List<IBObject> updateObjects, DbManager dbmngr, Guid UserID)
        //{            
        //    foreach (var item in updateObjects)
        //    {
        //        UpdateItem(item, dbmngr, UserID);
        //    }           
        //}




        ///// <summary>
        ///// CLIENT COMMAND PART 
        ///// </summary>
        ///// <param name="Param1"></param>        
        ///// <returns></returns>
        //[DC(Cmd.UpdateCollection , DCSideEn.Client)]
        //public DCMessage UpdateCollection(List<IBObject> Coll)//IBObjectCollection
        //{
        //    // --------------------- 0 Creating DCMessage -------------------------
        //    DCMessage outcomand = new DCMessage(CommandsEn.UpdateCollection, CommandManager_RegisterEn.CRUDCommandManager);

        //    ArgumentValidator.AssertNotNullAndOfType<List<IBObject>>(Coll, CommandParamsEn.QueryBObjectParam.ToString());

        //    // ----------------  1 Adding Parameters  ----------------------------- 
        //    outcomand.AddCollectionParam(CommandParamsEn.QueryBObjectParam, Coll.ToObjectList());

        //    //------------------2 Setting DataItems ------------------------------
            
        //    //------------------3 Returning Custom Command -----------------------
        //    return outcomand;

        //}


        //#endregion ------------------------------ 9 UpdateColllection------------------------
        
        //#region --------- command 10 UpdateFieldsByFilter------------------



        ///// <summary>
        ///// SERVER InContext method - for select as command inside Transaction 
        ///// </summary>
        ///// <param name="command"></param>
        ///// <param name="dataContractType"></param>
        ///// <param name="dbmngr"></param>
        ///// <returns></returns>
        //[DC(Cmd.UpdateFieldsByFilter, DCSideEn.ServerInContext)]
        //public object UpdateFieldsByFilter(DUpdateQuery Query, DbManager dbmngr)
        //{            
        //    String SqlQueryText = Query.ToSQLString(SystemDispatcher.Instance.DefaultDomainDB);
        //    return dbmngr.SetCommand(SqlQueryText).ExecuteNonQuery();            
        //}

        ///// <summary>
        ///// SERVER COMMAND PART
        ///// </summary>
        ///// <param name="command"></param>
        ///// <returns></returns>
        //[DC(Cmd.UpdateFieldsByFilter, DCSideEn.Server)]
        //public DCMessage UpdateFieldsByFilter(ref DCMessage command)
        //{
          
        //        //  --------- 1 Getting Parameters ( if Using Command with Params) ------
        //        //Проверка параметра Имени пользователя
        //        DBUpdateQuery query = command.GetParam<DBUpdateQuery>(CommandParamsEn.SelectQueryParam).WithChangeTrackingContext(command.CommandInfo.ChangeTrackingContext);

        //        using (DbManager dbmngr = new DbManager(new SqlDataProvider(), ConfigurationDispatcher.DomainConnectionString))
        //        {
        //            UpdateFieldsByFilter(query, dbmngr);
        //        }

        //        //-------------------- 4 Say that we Do all Command Logic/ Return  -----------------------------                 
        //        return command.SayCommandCompleteSuccesfully();
            
        //}




        ///// <summary>
        ///// CLIENT COMMAND PART 
        ///// </summary>
        ///// <param name="Param1"></param>        
        ///// <returns></returns>
        //[DC(Cmd.UpdateFieldsByFilter, DCSideEn.Client)]
        //public DCMessage UpdateFieldsByFilter(IDictionary<string, object> Fields, DFilterSet Filter, Type dataContractType)
        //{
        //    // --------------------- 0 Creating DCMessage -------------------------
        //    DCMessage outcomand = new DCMessage(CommandsEn.UpdateFieldsByFilter, CommandManager_RegisterEn.CRUDCommandManager);

        //    try
        //    {
        //        ArgumentValidator.AssertNotNullAndOfType<IDictionary<string, object>>(Fields, CommandParamsEn.QueryBObjectParam.ToString());
        //        ArgumentValidator.AssertGreaterThanOrEqual(Fields.Keys.Count, 1,
        //                                                   CommandParamsEn.QueryBObjectParam.ToString());
        //        // ----------------  1 Adding Parameters  ----------------------------- 

        //        DBUpdateQuery query = DBUpdateQuery.Update(dataContractType);//.Name
        //        if (Fields.First().Value is string)
        //            query = query.Set(Fields.First().Key, DBConst.String((string)Fields.First().Value));
        //        else if (Fields[Fields.First().Key] is DateTime)
        //            query = query.Set(Fields.First().Key, DBConst.DateTime((DateTime)Fields.First().Value));
        //        else if (Fields[Fields.First().Key] is double)
        //            query = query.Set(Fields.First().Key, DBConst.Double((double)Fields.First().Value));
        //        else if (Fields[Fields.First().Key] is Guid)
        //            query = query.Set(Fields.First().Key, DBConst.Guid((Guid)Fields.First().Value));
        //        else if (Fields[Fields.First().Key] is int)
        //            query = query.Set(Fields.First().Key, DBConst.Int32((int)Fields.First().Value));
        //        else if (Fields[Fields.First().Key] is bool)
        //            query = (bool)Fields.First().Value
        //                            ? query.Set(Fields.First().Key, DBConst.Const(1))
        //                            : query.Set(Fields.First().Key, DBConst.Const(0));

        //        foreach (var field in Fields.Keys.Where(k => k != Fields.First().Key))
        //            if (Fields[field] is string)
        //                query = query.AndSet(field, DBConst.String((string)Fields[field]));
        //            else if (Fields[field] is DateTime)
        //                query = query.AndSet(field, DBConst.DateTime((DateTime)Fields[field]));
        //            else if (Fields[field] is double)
        //                query = query.AndSet(field, DBConst.Double((double)Fields[field]));
        //            else if (Fields[field] is Guid)
        //                query = query.AndSet(field, DBConst.Guid((Guid)Fields[field]));
        //            else if (Fields[field] is int)
        //                query = query.AndSet(field, DBConst.Int32((int)Fields[field]));
        //            else if (Fields[field] is bool)
        //                query = (bool)Fields[field]
        //                            ? query.AndSet(field, DBConst.Const(1))
        //                            : query.AndSet(field, DBConst.Const(0));
        //        query.WhereSet = Filter;

        //        outcomand.AddParam(CommandParamsEn.SelectQueryParam, query);

        //        //------------------2 Setting DataItems ------------------------------


        //        //------------------3 Returning Custom Command -----------------------
        //        return outcomand;
        //    }
        //    catch (Exception)
        //    {
        //        throw new Exception("Error in Client RegisterPages UpdateFieldsByFilter");
        //    }
        //}


        //#endregion ------------------------------ 10 UpdateFieldsByFilter------------------------




    }
}
