﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a method signature that returns the required value of a Parameter for methods created with the DBParam.ParamWithDelegate(...)
    /// </summary>
    /// <returns>The value of any required parameter</returns>
    public delegate object ParamValue();
}
