﻿

using System;
using System.Runtime.Serialization;


namespace DDDD.Core.Data.DA2.Query
{


    ///// <summary>
    ///// All the different returned range options - limits
    ///// </summary>
    //[DataContract]
    //public enum TopType
    //{
    //    /// <summary>
    //    /// Explicit number to return
    //    /// </summary>
    //    [EnumMember]
    //    Count,
    //    /// <summary>
    //    /// The maximum percentage to return
    //    /// </summary>
    //    [EnumMember]
    //    Percent,

    //    /// <summary>
    //    /// Within a specific range - not curently implemented as not fully suported in all db engines.
    //    /// </summary>
    //    [EnumMember]
    //    Range
    //}





    /// <summary>
    /// Developer can add some not defined statement inside this enum; and then Add new defenitions  of BeginWrite_StatementX and EndWrite_StatementX methods for statement's Building  
    /// </summary>
    [DataContract]
    public enum StatementsEn //: int
    {
        [EnumMember]
        NotDefined, // = 0
        [EnumMember]
        RowNumber_Statement //  = 1
    }



    /// <summary>
    /// Enumeration of all Comparison operators.
    /// </summary>
    /// <remarks>There are 5 different types of operator defined, and each can be cast to the standard 'Operator' enumeration without clashing with other types</remarks>
    [DataContract]
    public enum Compare //: int
    {
        /// <summary>
        /// =
        /// </summary>
        [EnumMember]
        Equals, //= 1

        /// <summary>
        /// &lt;
        /// </summary>
        [EnumMember]
        LessThan, // = 2

        /// <summary>
        /// &gt;
        /// </summary>
        [EnumMember]
        GreaterThan, // = 3

        /// <summary>
        /// &lt;=
        /// </summary
        [EnumMember]
        LessThanEqual, // = 4

        /// <summary>
        /// &gt;=
        /// </summary>
        [EnumMember]
        GreaterThanEqual, // = 5

        /// <summary>
        /// !=, &lt;&gt;
        /// </summary>
        [EnumMember]
        NotEqual, // = 6

        /// <summary>
        /// special 'LIKE' operator
        /// </summary>
        [EnumMember]
        Like, // = 7

        /// <summary>
        /// special 'IS' operator
        /// </summary>
        [EnumMember]
        Is, // = 8

        /// <summary>
        /// special 'IN' operator (EXISTS IN)
        /// </summary>
        [EnumMember]
        In, // = 9

        /// <summary>
        /// opposite of 'IN'
        /// </summary>
        [EnumMember]
        NotIn  //= 10
    }



    /// <summary>
    /// Enumeration of the Teriary operators (3 arguments)
    /// </summary>
    [DataContract]
    public enum TertiaryOp : int
    {
        /// <summary>
        /// special BETWEEN ... AND ...
        /// </summary>
        [EnumMember]
        Between// = 115
    }


    /// <summary>
    /// The operator enumeration contains all 5 operator types in a single enumeration
    /// </summary>
    [DataContract]
    public enum Operator : int
    {
        /// <summary>=</summary>
        [EnumMember]
        Equals, //= 1

        /// <summary>&lt;</summary>
        [EnumMember]
        LessThan, // = 2

        /// <summary>&gt;</summary>
        [EnumMember]
        GreaterThan, //= 3

        /// <summary>&lt;=</summary>
        [EnumMember]
        LessThanEqual, // = 4

        /// <summary>&gt;=</summary>
        [EnumMember]
        GreaterThanEqual, // = 5

        /// <summary>!=</summary>
        [EnumMember]
        NotEqual, // = 6

        /// <summary>LIKE</summary>
        [EnumMember]
        Like, //= 7

        /// <summary>IS</summary>
        [EnumMember]
        Is, // = 8
        /// <summary>IN</summary>
        [EnumMember]
        In, // = 9
        /// <summary>NOT IN</summary>
        [EnumMember]
        NotIn, //= 10

        /// <summary>+</summary>
        [EnumMember]
        Add, // = 100

        /// <summary>-</summary>
        [EnumMember]
        Subtract, // = 101

        /// <summary>*</summary>
        [EnumMember]
        Multiply, // = 102

        /// <summary>/</summary>
        [EnumMember]
        Divide, // = 103

        /// <summary>&lt;&lt;</summary>
        [EnumMember]
        BitShiftLeft, // = 104

        /// <summary>&gt;&gt;</summary>
        [EnumMember]
        BitShiftRight, // = 105

        /// <summary>%</summary>
        [EnumMember]
        Modulo, // = 106

        /// <summary>&amp;</summary>
        [EnumMember]
        BitwiseAnd, // = 107

        /// <summary>|</summary>
        [EnumMember]
        BitwiseOr, // = 108

        /// <summary>+ for strings</summary>
        [EnumMember]
        Concat, // = 109

        /// <summary>!</summary>
        [EnumMember]
        Not, // = 121

        /// <summary>EXISTS</summary>
        [EnumMember]
        Exists, // = 122

        /// <summary>BETWEEN ... AND ...</summary>
        [EnumMember]
        Between, //= 123

        /// <summary>&amp;&amp;</summary>
        [EnumMember]
        And, //= 124

        /// <summary>||</summary>
        [EnumMember]
        Or, //= 125

        /// <summary>!|</summary>
        [EnumMember]
        XOr  //= 126

    }


    /// <summary>
    /// Enumeration of the Sorting Order - Default is providied to that the database can define its optimum ordering
    /// </summary>
    [DataContract]
    public enum Order
    {
        /// <summary>
        /// A..Z, 1..9 etc.
        /// </summary>
        [EnumMember]
        Ascending,
        /// <summary>
        /// Z..A, 9..1 etc.
        /// </summary>
        [EnumMember]
        Descending,
        /// <summary>
        /// undefined - upto the engine implementation
        /// </summary>
        [EnumMember]
        Default
    }

    /// <summary>
    /// Enumeration of the avaiable join types
    /// </summary>
    [DataContract]
    public enum JoinType
    {
        /// <summary>
        /// Intersection - match both sides to be included
        /// </summary>
        [EnumMember]
        InnerJoin,
        /// <summary>
        /// All left rows and any rows on the right side that match
        /// </summary>
        [EnumMember]
        LeftOuter,

        /// <summary>
        /// All left rows joined to all right rows
        /// </summary>
        [EnumMember]
        CrossProduct,

        /// <summary>
        /// All right rows, and any left rows that match
        /// </summary>
        [EnumMember]
        RightOuter,

        /// <summary>
        /// Undefined constraint
        /// </summary>
        [EnumMember]
        Join
    }

    /// <summary>
    /// Basic set of known functions
    /// </summary>
    [DataContract]
    public enum Function
    {
        /// <summary>Special custom function that is known to be implemented by the author but is not standard</summary>
        [EnumMember]
        Unknown, //= -1

        /// <summary>
        /// Gets the current date
        /// </summary>
        [EnumMember]
        GetDate,// = 0

        /// <summary>
        /// Gets the auto numbered ID of the last inserted row
        /// </summary>
        [EnumMember]
        LastID,// = 1

        /// <summary>
        /// Checks the first argument and returns it unless it's null - returns the second. 
        /// </summary>
        [EnumMember]
        IsNull// = 2

    }


    /// <summary>
    /// All the collections that can be accessed from the .NET meta data implementation
    /// </summary>
    /// <remarks>The DBSchemaProviders use this implementation to quickly query meta-data information from a database connection</remarks>
    [DataContract]
    public enum DMetaDataCollectionType
    {
        /// <summary>
        /// A table of information about the currently connected engine
        /// </summary>
        [EnumMember]
        DataSourceInformation,

        /// <summary>
        /// All the (accessible) databases in the connected engine
        /// </summary>
        [EnumMember]
        Databases,

        /// <summary>
        /// All the tables in the connected engine(restricted to a database)
        /// </summary>
        [EnumMember]
        Tables,

        /// <summary>
        /// All the columns in the connected engine(restricted to a database and table)
        /// </summary>
        [EnumMember]
        Columns,

        /// <summary>
        /// All the views in the connected engine (restricted to a database)
        /// </summary>
        [EnumMember]
        Views,

        /// <summary>
        /// Al the columns in the connected engine (restricted to database and view)
        /// </summary>
        [EnumMember]
        ViewColumns,

        /// <summary>
        /// All the functions and stored procedures in the connected engine (restricted to database).
        /// </summary>
        [EnumMember]
        Procedures,

        /// <summary>
        /// All the parameters in a procedure or function in the connected engine (restricted to database and procedure)
        /// </summary>
        [EnumMember]
        ProcedureParameters,

        /// <summary>
        /// All the foreign keys in the connected engine (restricted to database)
        /// </summary>
        [EnumMember]
        ForeignKeys,

        /// <summary>
        /// All the indexes in the connected engine (restricted to atabase and table)
        /// </summary>
        [EnumMember]
        Indexes,

        /// <summary>
        /// All the columns in a specific index (restricted to a database and table and index)
        /// </summary>
        [EnumMember]
        IndexColumns,

        /// <summary>
        /// A set of all the Metadata collections that the current database engine supports retrieval of.
        /// </summary>
        [EnumMember]
        MetaDataCollections,

        /// <summary>
        /// Not known MetaData collection.
        /// </summary>
        [EnumMember]
        Other
    }


    /// <summary>
    /// Defines the type of parameter referencing used by the database engine.
    /// </summary>
    [DataContract]
    public enum DParameterLayout
    {
        /// <summary>
        /// The order the parameters are declated in does not matter. The names of the parameters are used as identifiers.
        /// </summary>
        [EnumMember]
        Named,
        /// <summary>
        /// The names of the parameters are ignored and the position (order of referencing and declaration) is the way values are determined
        /// </summary>
        [EnumMember]
        Positional
    }



    /// <summary>
    /// Enumeration of the Unary operators (take a single argument)
    /// </summary>
    /// <remarks>There are 5 different types of operator defined, and each can be cast to the standard 'Operator' enumeration without clashing with other types</remarks>
    [DataContract]
    public enum UnaryOp // : int
    {
        /// <summary>
        /// !
        /// </summary>
        [EnumMember]
        Not,

        /// <summary>
        /// EXISTS
        /// </summary>
        [EnumMember]
        Exists
    }

    /// <summary>
    /// Enumeration of all binary operators (take 2 parameters and calcuate a result
    /// </summary>
    /// <remarks>There are 5 different types of operator defined, and each can be cast to the standard 'Operator' enumeration without clashing with other types</remarks>
    [DataContract]
    public enum BinaryOp //: int
    {
        /// <summary>
        /// +
        /// </summary>
        [EnumMember]
        Add, //= 100

        /// <summary>
        /// -
        /// </summary>
        [EnumMember]
        Subtract, // = 101

        /// <summary>
        /// *
        /// </summary>
        [EnumMember]
        Multiply, // = 102

        /// <summary>
        /// /
        /// </summary>
        [EnumMember]
        Divide, // = 103

        /// <summary>
        /// &lt;&lt;
        /// </summary>
        [EnumMember]
        BitShiftLeft, // = 104

        /// <summary>
        /// &gt;&gt;
        /// </summary>
        [EnumMember]
        BitShiftRight, //= 105

        /// <summary>
        /// %
        /// </summary>
        [EnumMember]
        Modulo,  // = 106

        /// <summary>
        /// &amp;
        /// </summary>
        [EnumMember]
        BitwiseAnd,  // = 107

        /// <summary>
        /// |
        /// </summary>
        [EnumMember]
        BitwiseOr,  // = 108

        /// <summary>
        /// + for strings
        /// </summary>
        [EnumMember]
        Concat  //= 109
    }


    /// <summary>
    /// Boolean logic table operators for 2 arguments
    /// </summary>
    /// <remarks>There are 5 different types of operator defined, and each can be cast to the standard 'Operator' enumeration without clashing with other types</remarks>
    [DataContract]
    public enum BooleanOp //: int
    {
        /// <summary>
        /// &amp;&amp;
        /// </summary>
        [EnumMember]
        And, //= 117

        /// <summary>
        /// ||
        /// </summary>
        [EnumMember]
        Or, // = 118

        /// <summary>
        /// special XOr operator
        /// </summary>
        [EnumMember]
        XOr, // = 119
    }




    /// <summary>
    /// Defines the set of aggregate functions
    /// </summary>
    [DataContract]
    public enum AggregateFunction
    {
        /// <summary>COUNT(*)</summary>
        [EnumMember]
        Count,
        /// <summary>SUM(column)</summary>
        [EnumMember]
        Sum,
        /// <summary>AVG(column)</summary>
        [EnumMember]
        Avg,
        /// <summary>MIN(column)</summary>
        [EnumMember]
        Min,
        /// <summary>MAX(column)</summary>
        [EnumMember]
        Max
    }



 


    /// <summary>
    /// A set of flags to describe the definition of a DBSchemaColumn.
    /// </summary>
    [Flags()]
    [DataContract]
    public enum DBSchemaColumnFlags
    {
        /// <summary>
        /// If set, this column cannot be written to
        /// </summary>       
        [EnumMember]
        ReadOnly = 1,

        /// <summary>
        /// If set, the database engine with automatically assign a value for this column
        /// </summary>         
        [EnumMember]
        AutoAssign = 2,

        /// <summary>
        /// If set, this is (one of) the unique primary key columns on the table
        /// </summary>         
        [EnumMember]
        PrimaryKey = 4,

        /// <summary>
        /// If set, the value of this column in a row can be NULL
        /// </summary>        
        [EnumMember]
        Nullable = 8,

        /// <summary>
        /// If set, this column has a default value that will be used if no other value is defined.
        /// </summary>        
        [EnumMember]
        HasDefault = 16
    }



}
