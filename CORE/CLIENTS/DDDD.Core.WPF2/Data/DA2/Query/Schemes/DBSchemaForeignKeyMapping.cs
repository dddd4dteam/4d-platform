﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Defines the mapping between the foreign key 
    /// tables column and the primary key tables column
    /// </summary>
    public class DBSchemaForeignKeyMapping
    {
        private string _fkcol, _pkcol;

        /// <summary>
        /// Gets or sets the name of the column on the ForeignKey Table
        /// </summary>
        [XmlAttribute("foreign-column")]
        public string ForeignColumn { get { return _fkcol; } set { _fkcol = value; } }

        /// <summary>
        /// Gets or sets the name of the column on the PrimaryKey Table
        /// </summary>
        [XmlAttribute("primary-column")]
        public string PrimaryColumn { get { return _pkcol; } set { _pkcol = value; } }

    }

    /// <summary>
    /// A collection of ForeignKeyMapping's
    /// </summary>
    public class DBSchemaForeignKeyMappingCollection : List<DBSchemaForeignKeyMapping>
    {
    }


}
