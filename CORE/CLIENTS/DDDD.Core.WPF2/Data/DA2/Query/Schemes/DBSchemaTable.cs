﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines the schema information of the table in a database
    /// </summary>
    [XmlRoot("table", Namespace = "http://schemas.perceiveit.co.uk/Query/schema/")]
#if SERVER
    [Serializable()]
#endif
    public class DBSchemaTable : DBSchemaItem
    {
        #region ivars

        private DBSchemaTableColumnCollection _cols;
        private DBSchemaIndexCollection _indexes;
        private DBSchemaForeignKeyCollection _fks;

        #endregion

        //
        // public properties
        //

        #region public DBSchemaTableColumnCollection Columns { get; set; }

        /// <summary>
        /// Gets the collection of DBSchemaColumns in this table
        /// </summary>
        [XmlArray("columns")]
        [XmlArrayItem("column", typeof(DBSchemaTableColumn))]
        public DBSchemaTableColumnCollection Columns
        {
            get { return this._cols; }
            set { this._cols = value; }
        }

        #endregion

        #region public DBSchemaItemRefCollection Indexes {get; set;}

        /// <summary>
        /// Gets the collection of DBSchemaIndexes in this table
        /// </summary>
        [XmlArray("indexes")]
        [XmlArrayItem("index", typeof(DBSchemaIndex))]
        public DBSchemaIndexCollection Indexes
        {
            get { return this._indexes; }
            set { this._indexes = value; }
        }

        #endregion

        #region public DBSchemaForeignKeyCollection ForeignKeys {get;set;}

        /// <summary>
        /// Gets the collection of DBSchemaForeignKeys in this table
        /// </summary>
        [XmlArray("foreign-keys")]
        [XmlArrayItem("fk", typeof(DBSchemaForeignKey))]
        public DBSchemaForeignKeyCollection ForeignKeys
        {
            get { return this._fks; }
            set { this._fks = value; }
        }

        #endregion

        //
        // .ctors
        //

        #region public DBSchemaTable()

        /// <summary>
        /// Creates a new unnamed DBSchemaTable
        /// </summary>
        public DBSchemaTable()
            : base(DBSchemaTypes.Table)
        {
            this.Columns = new DBSchemaTableColumnCollection();
        }

        #endregion

        #region public DBSchemaTable(string owner, string name)
        /// <summary>
        /// Creates an new DBSchemaTable
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        public DBSchemaTable(string owner, string name)
            : this()
        {
            this.Name = name;
            this.Schema = owner;
        }

        #endregion

        #region protected DBSchemaTable(DBSchemaTypes type, string owner, string name)

        /// <summary>
        /// protected constructor that inheritors can use to specifiy the DBSchemaTypes
        /// </summary>
        /// <param name="type"></param>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        protected DBSchemaTable(DBSchemaTypes type, string owner, string name)
            : base(name, owner, type)
        {
        }

        #endregion

        //
        // overriden methods
        //



    }

}
