﻿using System.Xml.Serialization;

namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Base abstract class for the DBSchemaSproc and DBSchemaFunction - contans a set of parameters
    /// </summary>
    public abstract class DBSchemaRoutine : DBSchemaItem
    {
        #region ivars

        private DBSchemaParameterCollection _params = new DBSchemaParameterCollection();


        #endregion

        //
        // properties
        //


        #region public DBSchemaParameterCollection Parameters

        /// <summary>
        /// Gets the collection of parameters associated with this item
        /// </summary>
        [XmlArray("params")]
        [XmlArrayItem("param")]
        public DBSchemaParameterCollection Parameters
        {
            get { return this._params; }
            set { this._params = value; }
        }

        #endregion


        //
        // ctors
        //

        #region public DBSchemaRoutine()

        /// <summary>
        /// Creates a new empty DBSchemaRoutine
        /// </summary>
        public DBSchemaRoutine()
            : this(DBSchemaTypes.Function, string.Empty, string.Empty)
        {
        }

        #endregion

        #region public DBSchemaRoutine(string owner, string name)
        /// <summary>
        /// Creates a new DBSchemaRoutine with the specified nama and owner
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        public DBSchemaRoutine(string owner, string name)
            : this(DBSchemaTypes.Function, owner, name)
        {
        }

        #endregion

        #region protected DBSchemaRoutine(DBSchemaTypes type, string owner, string name)

        /// <summary>
        /// protected primary constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        protected DBSchemaRoutine(DBSchemaTypes type, string owner, string name)
            : base(owner, name, type)
        {
        }

        #endregion
    }

}
