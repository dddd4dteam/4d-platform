﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// represents a defined function in the database schema
    /// </summary>
    public class DBSchemaFunction : DBSchemaRoutine
    {
        #region ivars

        private DBSchemaParameter _retparam = null;

        #endregion
        /// <summary>
        /// Gets or sets the return parameter of this function
        /// </summary>
        public DBSchemaParameter ReturnParameter
        {
            get { return _retparam; }
            set { _retparam = value; }
        }


        //
        // ctors
        //

        #region public DBSchemaFunction()

        /// <summary>
        /// Creates a new empty DBSchemaFunction
        /// </summary>
        public DBSchemaFunction()
            : this(DBSchemaTypes.Function, string.Empty, string.Empty)
        {
        }

        #endregion

        #region public DBSchemaFunction(string owner, string name)
        /// <summary>
        /// Creates a new DBSchemaFunction with the specified nama and owner
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        public DBSchemaFunction(string owner, string name)
            : this(DBSchemaTypes.Function, owner, name)
        {
        }

        #endregion

        #region protected DBSchemaFunction(DBSchemaTypes type, string owner, string name)

        /// <summary>
        /// protected primary constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        protected DBSchemaFunction(DBSchemaTypes type, string owner, string name)
            : base(type, owner, name)
        {
        }

        #endregion


        //
        // object overrides
        //

        #region protected override void ToString(StringBuilder sb)

        /// <summary>
        /// Overrides the base ToString implementation to add the parameters onto the collection
        /// </summary>
        /// <param name="sb"></param>
        protected override void ToString(StringBuilder sb)
        {
            base.ToString(sb);

            if (null != this.ReturnParameter)
            {
                sb.Append(" (Returns:");
                sb.Append(this.ReturnParameter);
                sb.Append(")");

            }
        }

        #endregion


    }


}
