﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Defines the schema of a stored procedure for a database definition
    /// </summary>
    [XmlRoot("sproc", Namespace = "http://schemas.perceiveit.co.uk/Query/schema/")]
    public class DBSchemaSproc : DBSchemaRoutine
    {
        #region ivars

        private DBSchemaViewColumnCollection _results = new DBSchemaViewColumnCollection();


        #endregion

        //
        // public properties
        //

        #region public DBSchemaViewColumnCollection Results {get;set;}

        /// <summary>
        /// Gets or sets the collection of column results for the StoredProcedure
        /// </summary>
        [XmlArray("results")]
        [XmlArrayItem("column")]
        public DBSchemaViewColumnCollection Results
        {
            get { return this._results; }
            set { this._results = value; }
        }

        #endregion

        //
        // .ctors
        //

        #region public DBSchemaSproc()

        /// <summary>
        /// Create a new empty Stored Procedure reference
        /// </summary>
        public DBSchemaSproc()
            : this(DBSchemaTypes.StoredProcedure, String.Empty, String.Empty)
        {
        }

        #endregion

        #region public DBSchemaSproc(string owner, string name)

        /// <summary>
        /// Creates a new StoredProcedure reference with the
        /// specified name and owner
        /// </summary>
        /// <param name="name"></param>
        /// <param name="owner"></param>
        public DBSchemaSproc(string owner, string name)
            : this(DBSchemaTypes.StoredProcedure, owner, name)
        {
            this.Name = name;
            this.Schema = owner;
        }

        #endregion

        #region protected DBSchemaSproc(DBSchemaTypes type, string owner, string name)

        /// <summary>
        /// proected primary constructor
        /// </summary>
        /// <param name="type"></param>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        protected DBSchemaSproc(DBSchemaTypes type, string owner, string name)
            : base(type, owner, name)
        {
        }

        #endregion




    }

}
