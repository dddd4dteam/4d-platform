﻿using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Pagable Query  Data Load info 
    /// </summary>
    [DataContract]
    public partial class DPageInfo
    {

        #region ---------- CTOR ------------------


        public DPageInfo() { }


        /// <summary>
        ///  Для Первичного запроса
        ///  При первичном запросе нам необходимо будет найти размер всего результирующего набора запроса.
        ///  Конструктор задает для загрузки только 1 страницу для загрузки, если нужно больше то это делается вручную позже  
        /// </summary>
        /// <param name="crPageSize"></param>
        /// <param name="crPageIndex"></param>
        public DPageInfo(Int32 crPageSize)
        {
            PageSize = crPageSize;
            PageIndex = 1;
            PagesToLoad = 1;
        }

        /// <summary>
        ///  Для Вторичного запроса
        ///  При вторичном запросе будет подгружен набор  только для  необходимой страницы
        /// </summary>
        /// <param name="crPageSize">Размер набора на  странице</param>
        /// <param name="crPageIndex">Требуемая страница всегда нам указывается сразу клиентом </param>
        public DPageInfo(Int32 crPageSize, Int32 crPageIndex) {
            PageSize = crPageSize;
            PageIndex = crPageIndex;
            PagesToLoad = 1;
        }


        /// <summary>
        ///  Для Вторичного запроса с указанием количества страниц для загрузки  
        ///  При вторичном запросе будет подгружен набор  только для  необходимых страниц
        /// </summary>
        /// <param name="crPageSize">Размер набора на  странице</param>
        /// <param name="crPageIndex">Требуемая страница всегда нам указывается сразу клиентом </param>
        public DPageInfo(Int32 crPageSize, Int32 crPageIndex, Int32 crPagesToLoad)
        {
            PageSize = crPageSize;
            PageIndex = crPageIndex;
            PagesToLoad = crPagesToLoad;
        }
        
        #endregion ---------- CTOR ------------------


        #region ---------- PROPERTIES ------------------

        #region ------------------------ INPUT PARAMS ------------------------

       
        /// <summary>
        /// Размер страницы - число элементов/записей на странице
        /// </summary>
        [DataMember]
        public Int32 PageSize { get; set; }


        /// <summary>
        /// Индекс необходимой страницы. Диапазон (1-n)
        /// Если используется Пейджинг то он не должен быть 0
        /// PageIndex- Если это просто первый запрос- когда мы только будем создавать VirtualCollection- это запрос с уточнением -  то это свойство = 1  
        ///            Если это вторичный запрос - то  значение свойства берется из одноименного свойства Коллекции 
        /// - При Первичном запросе - После загрузки элементов для первой страницы мы уже не будем обращаться к серверу, 
        ///            коллекция подгрузится из кеша VirtualCollection. Поэтому 1 - это железно для Первичного запроса
        /// -    А при первичном запросе нем еще необходимо будет найти размер всего результирующего набора запроса
        /// </summary>
        [DataMember]
        public Int32 PageIndex { get; set; }


        /// <summary>
        ///  Skipped Pages before get from Result Collection Items.
        /// </summary>
        public Int32 SkipPages 
        { 
            get{
                if (PageIndex > 0)
                {   return PageIndex - 1;
                }   else return 0;
               }  
        }


        /// <summary>
        /// Кол-во страниц которе нам захочется подгрузить начиная и включая набор элементов с PageIndex
        /// </summary>
        [DataMember]
        public Int32 PagesToLoad 
        { get; set; }


        #region  -----------------  Info in Records --------------------------

        /// <summary>
        /// Skipping Records before get what we need
        /// </summary>
        public Int32 SkipRecords
        {
            get { return PageSize * (PageIndex - 1); }
        }

        /// <summary>
        /// TOP operator - is count of Loading records
        /// </summary>
        public Int32 LoadRecords
        {
            get { return PageSize * PagesToLoad; }
        }

        #endregion-----------------  Info in Records --------------------------

        

        #endregion ------------------------ INPUT PARAMS ---------------------------


        #region  --------------------- SERVER SETTED PROPERTIES --------------------
        /// <summary>
        /// Размер всего набора- Количество всех записей в результате запроса
        /// Если больше 0 = значит результат нет ни одной записи
        /// Если null значит не выставлен
        /// Если > 0 значит количество записей в результате запроса вычислено
        /// Вычисляется на сервере отдельным запросом - с добавлением к текущему запросу RowNumber
        /// </summary>
        [DataMember]
        public Int32? SetSize { get; set; }
        
        #endregion --------------------- SERVER SETTED PROPERTIES --------------------


        #region  --------------------- Calulated PROPERTIES --------------------
        
        /// <summary>
        /// С какой записи в общем наборе -  это для вычисленной выборки 
        /// </summary>        
        public Int32 FromRecordInSet { get { return (PageSize * PageIndex) + 1; } }

        /// <summary>
        /// По какую запись в общем наборе - это для вычисленной выборки 
        /// </summary>        
        public Int32 TillRecordInSet { get { return (PageSize * (PageIndex + 1)); } }

        // TASK: в конечном варанте надо вычислить SkipPages and PageSize -найти формулу 
        // ведь если мне надо загрузить несколько страниц я не могу сказать что размер страницы в параметре контсруктора может использ-оваться в конечном параметре запроса


        #endregion --------------------- Calulated PROPERTIES --------------------

        #endregion ---------- PROPERTIES ------------------
                                 

        #region  -------------------- METHODS -----------------------

        /// <summary>
        /// Сменить на более точный размер полгружаемого объема записей 
        /// </summary>
        /// <param name="newPagesToLoad"></param>
        public void ChangePagesToLoad(Int32 newPagesToLoad)
        {
            PagesToLoad = newPagesToLoad;
        }


        /// <summary>
        /// Статическая альтернатива Конструктору 1
        /// </summary>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public static DPageInfo Pages()
        {
            return new DPageInfo();
        }



        /// <summary>
        /// Статическая альтернатива Конструктору 2
        /// </summary>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public static DPageInfo Pages(Int32 PageSize)
        {
            return new DPageInfo(PageSize);
        }


        /// <summary>
        /// Статическая альтернатива Конструктору 3
        /// </summary>
        /// <param name="crPageSize"></param>
        /// <param name="crPageIndex"></param>
        /// <returns></returns>
        public static DPageInfo Pages(Int32 PageSize, Int32 PageIndex)
        {
            return new DPageInfo(PageSize, PageIndex);
        }


        /// <summary>
        /// Статическая альтернатива Конструктору 4
        /// </summary>
        /// <param name="PageSize"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PagesToLoad"></param>
        /// <returns></returns>
        public static DPageInfo Pages(Int32 PageSize, Int32 PageIndex, Int32 PagesToLoad)
        {
            return new DPageInfo(PageSize, PageIndex, PagesToLoad);
        }



        #endregion -------------------- METHODS -----------------------
              

    }
}
