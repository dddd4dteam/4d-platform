﻿using System;


namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Команда SQL - Выборка, Вставка, Обновление , Удаление 
    /// </summary>
    public enum QueryCommandEn
    {
        /// <summary>
        /// 
        /// </summary>
        NotDefined,

        /// <summary>
        /// Команда Выборки Значений
        /// </summary>
        SelectQuery,

        /// <summary>
        /// Команда Вставки 
        /// </summary>
        InsertQuery,

        /// <summary>
        /// Команда Обновления 
        /// </summary>
        UpdateQuery,

        /// <summary>
        /// Команда Удаления Значений
        /// </summary>
        DeleteQuery
    }



    /// <summary>
    ///  Аргумент для оперирования запросами в Пользовательских событиях
    /// </summary>
    public class DQueryEventArgs  : EventArgs
    {
       #region ----------------------------- CTOR -------------------------------------

        public DQueryEventArgs(DSelectQuery NewSelectQuery)
        {
            NewQuery = NewSelectQuery;
        }

        public DQueryEventArgs(DInsertQuery NewInsertQuery)
        {
            NewQuery = NewInsertQuery;
        }

        public DQueryEventArgs(DUpdateQuery NewUpdateQuery)
        {
            NewQuery = NewUpdateQuery;
        }


        public DQueryEventArgs(DDeleteQuery NewDeleteQuery)
        {
            NewQuery = NewDeleteQuery;
        }
        

        #endregion ----------------------------- CTOR -------------------------------------
                   

       #region --------------------- PROPERTIES ---------------------
            
            /// <summary>
            /// Тип команды оператора SQL
            /// </summary>
            public QueryCommandEn QueryCommandType
            {
                get {
                    if (NewQuery is DSelectQuery)
                    {
                        return QueryCommandEn.SelectQuery;
                    }
                    else if (NewQuery is DInsertQuery)
                    {
                        return QueryCommandEn.InsertQuery;
                    }
                    else if (NewQuery is DUpdateQuery)
                    {
                        return QueryCommandEn.UpdateQuery;
                    }
                    else if (NewQuery is DDeleteQuery)
                    {
                        return QueryCommandEn.DeleteQuery                         ;
                    }
                    else return 0;
                }
            }


            /// <summary>
            /// Новый Запрос 
            /// </summary>
            public DQuery NewQuery
            {
                get;
                private set;
            }
            

            #endregion --------------------- PROPERTIES ---------------------
               
    }
}
