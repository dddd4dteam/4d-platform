﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Extensions;
using DDDD.Core.Data.DA2.Mapping;
using System.IO;

namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Sql Key Word string consts
    /// </summary>
    internal class Skw
    {

        public const string TOP = " TOP ";
        public const string PERCENT = " PERCENT ";
        public const string DISTINCT = " DISTINCT ";
        public const string IS = " IS ";
        public const string NOT = " NOT ";
        public const string LIKE = " LIKE ";
        public const string IN = " IN ";
        public const string NOT_IN = " NOT IN ";
        public const string EXISTS = " EXISTS ";
        public const string NULL = "NULL";
        public const string RETURN = "RETURN ";

        public const string DROP = "DROP ";
        public const string TABLE = "TABLE ";
        public const string VIEW = "VIEW ";
        public const string FUNCTION = "FUNCTION ";
        public const string PROCEDURE = "PROCEDURE ";
        public const string INDEX = "INDEX ";

        public const string BEGIN = "BEGIN";
        public const string END = "END";
        public const string BETWEEN = " BETWEEN ";
        public const string AND = " AND ";
        public const string OR = " OR ";
        public const string XOR = " XOR ";
        public const string EXEC = "EXEC ";


        public const string UPDATE = "UPDATE ";
        public const string INSERT = "INSERT ";
        public const string INSERT_INTO = "INSERT INTO ";
        public const string DELETE = "DELETE ";
        public const string DELETE_FROM = "DELETE FROM ";
        
        public const string CREATE = "CREATE ";
        public const string CREATE_TABLE = "CREATE TABLE";
        public const string CREATE_VIEW = "CREATE VIEW";
        public const string CREATE_PROCEDURE = "CREATE PROCEDURE";
        public const string CREATE_FUNCTION = "CREATE FUNCTION";
        public const string CREATE_INDEX = "CREATE INDEX";


        public const string SELECT = "SELECT ";
        public const string All = "*";
        public const string FROM = "FROM ";

        public const string JOIN = " JOIN ";
        public const string INNER_JOIN = " INNER JOIN ";
        public const string LEFT_OUTER_JOIN = " LEFT OUTER JOIN ";
        public const string RIGHT_OUTER_JOIN = " RIGHT OUTER JOIN ";
        public const string ON = " ON ";
        public const string OVER = " OVER ";

        public const string WHERE = "WHERE ";
        public const string ORDER_BY = "ORDER BY ";
        public const string ASC = " ASC";
        public const string DESC = " DESC";

        public const string GROUP_BY = "GROUP BY ";
        public const string HAVING = "HAVING ";

        public const string SET = " SET ";

        public const string VALUES_OPEN = "VALUES (";

        public const string GETDATE = "GETDATE";
        public const string SCOPE_IDENTITY = "SCOPE_IDENTITY";
        public const string ISNULL = "ISNULL";
        public const string Row_NumberF = " row_number() ";


        public const string NL = "\r\n";
        public const char TAB = '\t';
        public const string AS = " AS ";
    }


    /// <summary>
    /// Data query Statement Builder
    /// </summary>
    public abstract  class DStatementBuilder : IDisposable
    {

        //
        // .ctor(s)
        //

        #region ---------------- CTOR -------------------

        /* -UNUSED  CTOR
           //DBDatabase database, DBDatabaseProperties properties, System.IO.TextWriter writer, bool ownsWriter

        //if (null == database)
        //    throw new ArgumentNullException("database");

        //if (null == properties)
        //    throw new ArgumentNullException("properties");

        //if (null == writer)
        //    throw new ArgumentException("Cannot create a DBStatementBuilder with a null TextWriter", "writer");

        //this._tw = writer;
        //this._ownswriter = ownsWriter;
        //this._db = database;
        //this._dbprops = properties;

        //DBDatabase database, DBDatabaseProperties properties, System.IO.TextWriter writer, bool ownsWriter
        //database, properties, writer, ownsWriter
         */
         
        protected DStatementBuilder()
        {
        }

        #endregion ---------------- CTOR -------------------

        #region ------------  GET with Init/Clear -------------
        public static DStatementBuilder Get(string providerKey)
        {
            if (providerKey == ProviderKey.MsSql 
                || providerKey == ProviderKey.MsSql2008
                || providerKey == ProviderKey.MsSql2012
                || providerKey == ProviderKey.MsSql2014
                || providerKey == ProviderKey.MsSql2017
                )
            {
                return new DBMsSqlClientStatementBuilder();
            }
            else if (providerKey == ProviderKey.SqlCe)
            {
                return new DBMsSqlClientStatementBuilder();
            }
            else if (providerKey == ProviderKey.SQLite)
            {
                return new DBSQLiteStatementBuilder();
            }

            else if (providerKey == ProviderKey.MySql)
            {
                return new DBMySqlStatementBuilder();
            }
            else if (providerKey == ProviderKey.PostgreSQL)
            {
                return new DBPostgreSqlStatementBuilder();
            }
            else if (providerKey == ProviderKey.Oracle)
            {
                return new DBOracleStatementBuilder();
            }
            else if (providerKey == ProviderKey.OleDb)
            {
                return new DBOleDbStatementBuilder();
            }
            else if (providerKey == ProviderKey.Mongo)
            {
                return new DBMongoStatementBuilder();
            }
            else if (providerKey == ProviderKey.Redis)
            {
                return new DBRedisStatementBuilder();
            }
            
            // By Default  DB2/ Informix /Firebird/ODDBC/ OleDB Accesds
            return new DBMsSqlClientStatementBuilder(); ;
        }

        public static void InitOnThread(
            DBProperties dbPropps
            , TextWriter writer = null
            , bool isOwnWriter = false
            , string parameterPrefix = "_param"            
            , params StatementParameter[] parameters 
            )
        {
            DStatementBuilder.DBProps = dbPropps;
            
            if (writer!= null)
            {  Writer = writer;
            }
            else Writer = new StringWriter();

            OwnsWriter = isOwnWriter;
            ParameterNamePrefix =  parameterPrefix;
            
            Parameters.Clear();
            if (parameters!= null &&  parameters.Length > 0)
            {
                for (int i = 0; i < parameters.Length; i++)
                {
                    Parameters.Add(parameters[i]);
                }
            }
            
            Depth = 0; 
            StatementInset = 0;            
            Nextpid = 1;
        }

        public static void ClearOnThread()
        {
            DStatementBuilder.DBProps = null;
            Writer =null;
            OwnsWriter = false;
            ParameterNamePrefix = "_param";
            Parameters.Clear();             
            Depth = 0;
            StatementInset = 0;
            Nextpid = 1;
        }


        #endregion ------------  GET with Init/Clear -------------


        //
        // inner classes
        //

        #region protected class StatementParameter

        /// <summary>
        /// Inner class for collecting all the parameters 
        /// defined in a statement
        /// </summary>
        public class StatementParameter
        {

            #region public string ParameterName {get;}

            private string _paramName;

            public string ParameterName
            {
                get { return this._paramName; }
                set { this._paramName = value; }
            }

            #endregion

            #region public IDBValueSource ValueSource

            private IDValueSource _valuesource;

            public IDValueSource ValueSource
            {
                get { return _valuesource; }
                set { _valuesource = value; }
            }

            #endregion

            #region public int Index

            private int _index;

            public int ParameterIndex
            {
                get { return _index; }
                set { _index = value; }
            }

            #endregion

        }

        #endregion

        #region protected class StatementParameterList

        /// <summary>
        /// Inner list of statement parameter references
        /// </summary>
        public class StatementParameterList : System.Collections.ObjectModel.KeyedCollection<string, StatementParameter>
        {
            protected override string GetKeyForItem(StatementParameter item)
            {
                return item.ParameterName;
            }

            protected override void RemoveItem(int index)
            {
                throw new NotSupportedException("Cannot remove items from this collection");
            }
        }

        #endregion

        //
        // properties
        // 
        #region --------------  PROPERTIES/Fields ---------------------

        [ThreadStatic]
        protected static System.IO.TextWriter Writer = null;

        [ThreadStatic]
        protected static bool OwnsWriter = false;

        [ThreadStatic]
        protected static DBProperties DBProps = null;

        /// <summary>
        /// list of parameters in the built statement
        /// </summary>
        [ThreadStatic]
        protected static StatementParameterList Parameters 
            = new StatementParameterList();

        [ThreadStatic]
        protected static int Depth;
        
        [ThreadStatic]
        protected static int StatementInset;

        [ThreadStatic]
        protected static int Nextpid = 1;

        [ThreadStatic]
        protected static string ParameterNamePrefix = "_param";

        
        
       
        

        /// <summary>
        /// Identifies if the connected database supports multiple properties
        /// </summary>
        public virtual bool SupportsMultipleStatements
        {
            get
            {
                if (DBProps == null) return false;

                return DBProps.CheckSupports(DBSchemaTypes.CommandScripts);
            }
        }

        public virtual bool HasParameters
        {
            get { return Parameters != null && Parameters.Count > 0; }
        }

        /// <summary>        
        /// DateTime format for Cukture FormatProvider
        /// </summary>
        protected virtual String DateFormatString
        {
            // get; set;  
            get
            {

                //YYYY-MM-DD HH:MI:SS(24h) 	ODBC Canonical 	SELECT CONVERT(VARCHAR(19), GETDATE(), 120) 	1972-01-01 13:42:24
                return "yyyy-MM-dd hh:mm:ss"; //En
            }
        }
        

        #endregion --------------  PROPERTIES/Fields ---------------------



        //
        // support methods
        //


        #region protected int GetNextID()
        protected int GetNextID()
        {
            int id = Nextpid;
            Nextpid++;
            return id;
        }

        #endregion

        #region protected int IncrementStatementDepth()

        /// <summary>
        /// Increments the current inset and  statement depth 
        /// returning the new statement depth
        /// </summary>
        /// <returns></returns>
        protected int IncrementStatementDepth()
        {
            StatementInset++;
            Depth++;
            return Depth;
        }

        #endregion

        #region  protected int IncrementStatementBlock()
        /// <summary>
        /// Increments and returns the current inset
        /// </summary>
        /// <returns></returns>
        protected int IncrementStatementBlock()
        {
            StatementInset++;
            return StatementInset;
        }

        #endregion

        #region protected void DecrementStatementDepth()

        /// <summary>
        /// Decreases the Statement depth and inset by one
        /// </summary>
        protected void DecrementStatementDepth()
        {
            if (Depth > 0)
                Depth--;
            if (StatementInset > 0)
                StatementInset--;
        }

        #endregion

        #region protected void DecrementStatementBlock()

        /// <summary>
        /// Decreases the inset by one
        /// </summary>
        protected void DecrementStatementBlock()
        {
            if (StatementInset > 0)
                StatementInset--;
        }

        #endregion

        #region protected void BeginNewLine()

        /// <summary>
        /// Outputs the new line and inset onto the current writer
        /// </summary>
        public virtual void BeginNewLine()
        {
            string line = Skw.NL;
            if (StatementInset > 0)
                line = line.PadRight(line.Length + StatementInset, Skw.TAB );
            Writer.Write(line);
        }

        #endregion

        #region protected virtual string GetAllFieldIdentifier()

        protected virtual string GetAllFieldIdentifier()
        {
            return Skw.All;
        }

        #endregion

        #region protected virtual string EscapeString(string value)

        protected virtual string EscapeString(string value)
        {
            return value.Replace("'", "''");
        }

        #endregion


        //
        // builder begin end section methods - public virtual
        //


        #region public virtual void BeginFunctionParameterList() + EndFunctionParameterList() + AppendReferenceSeparator()

        /// <summary>
        /// Appends the start of a new function parameter list to the statement '( '
        /// </summary>
        public virtual void BeginFunctionParameterList()
        {
            Writer.Write("(");
        }
        /// <summary>
        /// Appends the end of a function parameter list to the statement ') '
        /// </summary>
        public virtual void EndFunctionParameterList()
        {
           Writer.Write(") ");
        }

        /// <summary>
        /// Appends an individual separator to the statement ', '
        /// </summary>
        public virtual void AppendReferenceSeparator()
        {
           Writer.Write(", ");
        }

        #endregion

        #region public virtual void BeginSubSelect() + EndSubSelect()

        public virtual void BeginSubStatement()
        {
           Writer.Write(" (");
        }

        public virtual void EndSubStatement()
        {
            Writer.Write(") ");
            this.BeginNewLine();
        }

        #endregion

        #region public virtual void BeginIdentifier() + EndIdentifier() + AppendIdSeparator()

        public virtual void BeginIdentifier()
        {
            Writer.Write("[");
        }

        public virtual void EndIdentifier()
        {
            Writer.Write("]");
        }

        public virtual void AppendIdSeparator()
        {
           Writer.Write(".");
        }

        #endregion

        #region public virtual void BeginAlias() + EndAlias()

        public virtual void BeginAlias()
        {
           Writer.Write(Skw.AS);
        }

        public virtual void EndAlias()
        {

        }

        #endregion

        #region public virtual void BeginSelectStatement() + EndSelectStatement()

        public virtual void BeginSelectStatement()
        {
            this.BeginNewLine();
            if ( Depth > 0)
                this.BeginSubStatement();

            Writer.Write(Skw.SELECT);
            IncrementStatementDepth();

        }

        public virtual void EndSelectStatement()
        {
            if (Depth > 1)
                this.EndSubStatement();
            else
                this.WriteStatementTerminator();

            this.DecrementStatementDepth();

        }

        public virtual void WriteStatementTerminator()
        {
            this.WriteRaw(";");
        }

        #endregion

        #region public virtual void BeginFromList() + EndFromList()

        public virtual void BeginFromList()
        {
            this.BeginNewLine();
            Writer.Write(Skw.FROM);
            this.IncrementStatementBlock();
        }

        public virtual void EndFromList()
        {
            this.DecrementStatementBlock();
        }

        #endregion

        #region public virtual void BeginBlock() + EndBlock()

        public virtual void BeginBlock()
        {
            Writer.Write(" (");
        }

        public virtual void EndBlock()
        {
            Writer.Write(") ");
        }

        #endregion

        #region public virtual void BeginWhereStatement() + EndWhereStatement()

        public virtual void BeginWhereStatement()
        {
            this.BeginNewLine();
            Writer.Write(Skw.WHERE);
            this.IncrementStatementBlock();

        }

        public virtual void EndWhereStatement()
        {
            this.DecrementStatementBlock();
        }

        #endregion

        #region  public virtual void BeginOrderStatement() + EndOrderStatement()

        public virtual void BeginOrderStatement()
        {
            this.BeginNewLine();
            Writer.Write(Skw.ORDER_BY);
            this.IncrementStatementBlock();
        }

        public virtual void EndOrderStatement()
        {
            this.DecrementStatementBlock();
        }

        #endregion 

        #region public void BeginOrderClause(Order order) + EndOrderClause(Order order)

        public void BeginOrderClause(Order order)
        {

        }

        public virtual void EndOrderClause(Order order)
        {
            string o;
            switch (order)
            {
                case Order.Ascending:
                    o =Skw.ASC ;
                    break;
                case Order.Descending:
                    o = Skw.DESC;
                    break;
                case Order.Default:
                    o = "";
                    break;
                default:
                    throw new ArgumentOutOfRangeException("The value of the order by enumeration '" + order.ToString() + "' could not be recognised");
            }
             Writer.Write(o);
        }

        #endregion

        #region public virtual void BeginJoin(JoinType joinType) + EndJoin(JoinType joinType)

        public virtual void BeginJoin(JoinType joinType)
        {
            string j;
            switch (joinType)
            {
                case JoinType.InnerJoin:
                    j = Skw.INNER_JOIN;
                    break;
                case JoinType.LeftOuter:
                    j = Skw.LEFT_OUTER_JOIN;
                    break;
                case JoinType.CrossProduct:
                case JoinType.Join:
                    j = Skw.JOIN;
                    break;
                case JoinType.RightOuter:
                    j = Skw.RIGHT_OUTER_JOIN;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("The join type '" + joinType.ToString() + "' is not recognised");
            }
            Writer.Write(j);
            this.IncrementStatementBlock();

        }

        public virtual void EndJoin(JoinType joinType)
        {
            this.DecrementStatementBlock();
        }

        #endregion

        #region public virtual void BeginJoinOnList() + EndJoinOnList()

        public virtual void BeginJoinOnList()
        {
            this.BeginNewLine();
            Writer.Write(Skw.ON);
            this.IncrementStatementBlock();
        }

        public virtual void EndJoinOnList()
        {
            this.DecrementStatementBlock();
        }

        #endregion

        #region public virtual void BeginDateLiteral() + EndDateLiteral()

        public virtual void BeginDateLiteral()
        {
            Writer.Write("'");
        }

        public virtual void EndDateLiteral()
        {
            Writer.Write("'");
        }

        #endregion

        #region public virtual void BeingStringLiteral() + EndStringLiteral()

        public virtual void BeingStringLiteral()
        {
            Writer.Write("'");
        }

        public virtual void EndStringLiteral()
        {
            Writer.Write("'");
        }

        #endregion

        #region public virtual void BeginGroupByStatement() + EndGroupByStatement()

        public virtual void BeginGroupByStatement()
        {
            this.BeginNewLine();
            Writer.Write(Skw.GROUP_BY);
            this.IncrementStatementBlock();
        }

        public virtual void EndGroupByStatement()
        {
            this.DecrementStatementBlock();
        }

        #endregion

        #region public virtual void BeginHavingStatement() + EndHavingStatement()

        public virtual void BeginHavingStatement()
        {
            Writer.WriteLine();
            Writer.Write(Skw.HAVING);
        }

        public virtual void EndHavingStatement()
        {

        }

        #endregion

        #region public void BeginUpdateStatement() + EndUpdateStatement()

        public virtual void BeginUpdateStatement()
        {
            if ( Depth > 0)
                this.BeginSubStatement();
            this.IncrementStatementDepth();

            Writer.Write(Skw.UPDATE );
        }

        public virtual void EndUpdateStatement()
        {
            if (Depth > 1)
                this.EndSubStatement();
            else
                this.WriteStatementTerminator();


            this.DecrementStatementDepth();

            Writer.WriteLine();
        }

        #endregion

        #region public virtual void BeginSetValueList() + EndSetValueList()

        public virtual void BeginSetValueList()
        {
            Writer.WriteLine();
            Writer.Write(Skw.SET );
        }

        public virtual void EndSetValueList()
        {
        }

        #endregion

        #region public virtual void BeginAssignValue() + EndAssignValue()

        public virtual void BeginAssignValue()
        {

        }

        public virtual void EndAssignValue()
        {

        }

        #endregion

        #region public virtual void BeginInsertStatement() + EndInsertStatement()

        public virtual void BeginInsertStatement()
        {
            if ( Depth > 0)
                this.BeginSubStatement();

            this.IncrementStatementDepth();

            Writer.Write(Skw.INSERT_INTO);
        }

        public virtual void EndInsertStatement()
        {
            if (Depth > 1)
                this.EndSubStatement();
            else
                this.WriteStatementTerminator();

            this.DecrementStatementDepth();

            Writer.WriteLine();
        }

        #endregion

        #region public virtual void BeginInsertFieldList() + EndInsertFieldList()

        public virtual void BeginInsertFieldList()
        {
            this.BeginNewLine();
            Writer.Write("(");
        }

        public virtual void EndInsertFieldList()
        {
            Writer.Write(")");
        }

        #endregion

        #region public virtual void BeginInsertValueList() +  EndInsertValueList()

        public virtual void BeginInsertValueList()
        {
            this.BeginNewLine();
            Writer.Write(Skw.VALUES_OPEN);
            this.IncrementStatementBlock();
        }

        public virtual void EndInsertValueList()
        {
            Writer.Write(")");
            this.DecrementStatementBlock();
        }

        #endregion

        #region public virtual void BeginDeleteStatement() + EndDeleteStatement()

        public virtual void BeginDeleteStatement()
        {
            if ( Depth > 0)
                this.BeginSubStatement();
            this.IncrementStatementDepth();

            Writer.Write(Skw.DELETE_FROM);
        }

        public virtual void EndDeleteStatement()
        {
            if ( Depth > 1)
                this.EndSubStatement();
            else
                this.WriteStatementTerminator();

            this.DecrementStatementDepth();

            Writer.WriteLine();
        }

        #endregion

        #region public virtual void BeginFunction(Function function, string name) + EndFunction(Function function, string name)

        public virtual void BeginFunction(Function function, string name)
        {
            switch (function)
            {
                case Function.GetDate:
                    name = Skw.GETDATE ;
                    break;
                case Function.LastID:
                    name =Skw.SCOPE_IDENTITY;
                    break;
                case Function.IsNull:
                    name = Skw.ISNULL;
                    break;
                case Function.Unknown:
                default:
                    if (null == name)
                        throw new ArgumentNullException("name");

                    name = name.ToUpper();
                    break;
            }
            Writer.Write(name);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statement"></param>
        /// <param name="OrderFieldName"></param>
        public virtual void BeginCustomStatement(StatementsEn statement, DClause startClause)
        {
            string statementBeginString = "";
            switch (statement)
            {
                case StatementsEn.RowNumber_Statement:
                      // " row_number() OVER (ORDER BY "
                    statementBeginString = Skw.Row_NumberF+ Skw.OVER +"(" + Skw.ORDER_BY ; //
                    this.WriteStringClear(statementBeginString);
                    startClause.BuildStatement(this);
                    statementBeginString = "";
                    break;

                case StatementsEn.NotDefined:
                default:
                    if (null == statementBeginString)
                        throw new ArgumentNullException("statement");

                    statementBeginString = statement.ToString().ToUpper();
                    break;
            }

            if (statementBeginString != "")
                Writer.Write(statementBeginString);
        }


        public virtual void MiddleCustomStatement(StatementsEn statement, DClause middleClause)
        {
            string statementMiddleString = "";
            switch (statement)
            {
                case StatementsEn.RowNumber_Statement:
                    {
                        statementMiddleString = " ASC) AS ";
                        this.WriteStringClear(statementMiddleString);
                        middleClause.BuildStatement(this);
                        statementMiddleString = "";

                        break;
                    }

                case StatementsEn.NotDefined:
                default:
                    if (null == statementMiddleString)
                        throw new ArgumentNullException("statement");

                    statementMiddleString = statement.ToString().ToUpper();
                    break;
            }

            if (statementMiddleString != "")
                Writer.Write(statementMiddleString);
        }



        /// <summary>
        /// Завершение построения  пользовательского выражения
        /// </summary>
        /// <param name="statement"></param>
        public virtual void EndCustomStatement(StatementsEn statement, DClause endClause)
        {
            string statementEndString = "";
            switch (statement)
            {
                case StatementsEn.RowNumber_Statement:
                    //this.Writer.Write(endClause.BuildStatement(this));
                    //statementEndString = ;
                    break;

                case StatementsEn.NotDefined:
                default:
                    if (null == statementEndString)
                        throw new ArgumentNullException("statement");

                    statementEndString = statement.ToString().ToUpper();
                    break;
            }
            Writer.Write(statementEndString);
        }




        public virtual void EndFunction(Function function, string name)
        {

        }

        #endregion

        #region public virtual void BeginAggregateFunction(AggregateFunction function, string name) + EndAggregateFunction(AggregateFunction function, string name)

        public virtual void BeginAggregateFunction(AggregateFunction function, string name)
        {
            switch (function)
            {
                default:
                    if (null == name)
                        throw new ArgumentNullException("name");

                    name = name.ToUpper();
                    break;
            }
            Writer.Write(name);
        }

        public virtual void EndAggregateFunction(AggregateFunction function, string name)
        {
        }

        #endregion

        #region public virtual void BeginScript() + EndScript()

        public virtual void BeginScript()
        {
            Writer.Write( Skw.BEGIN  );
            this.IncrementStatementBlock();
            this.BeginNewLine();
        }

        public virtual void EndScript()
        {
            this.DecrementStatementBlock();
            this.BeginNewLine();
            Writer.WriteLine(Skw.END );
        }

        #endregion

        #region public virtual void BeginTertiaryOperator(Operator p) + ContinueTertiaryOperator(Operator p) + EndTertiaryOperator(Operator p)

        public virtual void BeginTertiaryOperator(Operator p)
        {
            switch (p)
            {
                case Operator.Between:
                    Writer.Write(Skw.BETWEEN);
                    break;
                default:
                    throw new ArgumentException("The operator '" + p.ToString() + "' is not a valid or known Tertiary operator");

            }
        }

        public virtual void ContinueTertiaryOperator(Operator p)
        {
            switch (p)
            {
                case Operator.Between:
                    Writer.Write(Skw.AND);
                    break;
                default:
                    throw new ArgumentException("The operator '" + p.ToString() + "' is not a valid or known Tertiary operator");

            }
        }

        internal void EndTertiaryOperator(Operator p)
        {

        }

        #endregion

        #region public virtual void BeginExecuteStatement() + EndExecuteStatement()

        public virtual void BeginExecuteStatement()
        {
            if (! DBProps.CheckSupports(DBSchemaTypes.StoredProcedure))
#if SERVER
                throw new System.Data.DataException("Current database does not support stored procedures");
#else   
                throw new Exception("Current database does not support stored procedures");
#endif

            Writer.Write(Skw.EXEC );
        }

        public virtual void EndExecuteStatement()
        {
            Writer.Write(Skw.NL);
        }

        #endregion

        #region public virtual void BeginExecuteParameters() + EndExecuteParameters()

        public virtual void BeginExecuteParameters()
        {
            Writer.Write(" ");
        }

        public virtual void EndExecuteParameters()
        {
        }

        #endregion

        #region public virtual void BeginExecuteAParameter() + EndExecuteAParameter()

        public virtual void BeginExecuteAParameter()
        {
        }

        public virtual void EndExecuteAParameter()
        {
            Writer.Write(" ");
        }

        #endregion

        #region protected virtual void BeginDrop(DBSchemaTypes type) + EndDrop(type)

        protected virtual void BeginDrop(DBSchemaTypes type)
        {
            this.WriteRaw(Skw.DROP );
            switch (type)
            {
                case DBSchemaTypes.Table:
                    this.WriteRaw(Skw.TABLE );
                    break;
                case DBSchemaTypes.View:
                    this.WriteRaw(Skw.VIEW );
                    break;
                case DBSchemaTypes.StoredProcedure:
                    this.WriteRaw(Skw.PROCEDURE );
                    break;
                case DBSchemaTypes.Function:
                    this.WriteRaw(Skw.FUNCTION );
                    break;
                case DBSchemaTypes.Index:
                    this.WriteRaw(Skw.INDEX);
                    break;
                case (DBSchemaTypes)0:
                default:
                    throw new ArgumentOutOfRangeException("type");

            }
        }

        protected virtual void EndDrop(DBSchemaTypes type)
        {
        }

        #endregion

        #region protected virtual void BeginCreate(DBSchemaTypes type) + EndCreate(type)

        protected virtual void BeginCreate(DBSchemaTypes type, string options)
        {
            this.WriteRaw(Skw.CREATE);
            if (string.IsNullOrEmpty(options) == false)
            {
                this.WriteRaw(options);
                if (!options.EndsWith(" "))
                    this.WriteRaw(" ");
            }

            switch (type)
            {
                case DBSchemaTypes.Table:
                    this.WriteRaw( Skw.TABLE);
                    break;
                case DBSchemaTypes.View:
                    this.WriteRaw(Skw.VIEW );
                    break;
                case DBSchemaTypes.StoredProcedure:
                    this.WriteRaw(Skw.PROCEDURE );
                    break;
                case DBSchemaTypes.Function:
                    this.WriteRaw(Skw.FUNCTION);
                    break;
                case DBSchemaTypes.Index:
                    this.WriteRaw(Skw.INDEX);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        protected virtual void EndCreate(DBSchemaTypes type)
        {
        }

        #endregion


        //
        // parameter addition, conversion and naming
        //

        #region public virtual string RegisterParameter(IDBValueSource source)

        /// <summary>
        /// Registers an IDBValueSource as a parameter in the statement and returns a name for this parameter.
        /// Note that this name is not the Database native parameter name 
        /// </summary>
        /// <param name="source">The source that identifies the parameter</param>
        /// <returns>The distinct name of the parameter</returns>
        public virtual string RegisterParameter(IDValueSource source)
        {
            bool declared = true;
            string paramName = source.Name;

            if (string.IsNullOrEmpty(paramName))
            {
                paramName = this.GetUniqueParameterName(ParameterNamePrefix);
                declared = false;
            }
            else if (Parameters.Contains(paramName))
            {
                return paramName;
                //if (this.DatabaseProperties.ParameterLayout == DParameterLayout.Named)
                //    //already registered
                //    return paramName;
                //else
                //    paramName = this.GetUniqueParameterName(ParameterNamePrefix);
            }
            else
                declared = true;

            StatementParameter sp = new StatementParameter();
            sp.ParameterName = paramName;
            sp.ValueSource = source;
            sp.ParameterIndex = Parameters.Count;
            if (!declared)
                sp.ValueSource.Name = paramName;

            Parameters.Add(sp);

            return paramName;
        }

        #endregion

        #region public virtual void WriteParameterReference(string paramname)

        /// <summary>
        /// Appends a parameter reference to the statement in database native format
        /// </summary>
        /// <param name="paramname">The coded parameter name</param>
        /// <exception cref="ArgumentNullException" >Thown if the paramname value is null or empty</exception>
        public virtual void WriteParameterReference(string paramname)
        {
            if (string.IsNullOrEmpty(paramname))
                throw new ArgumentNullException("Cannot append a null or empty parameter reference - implementors: Use the GetUniqueID to support unnamed parameters");

           Writer.Write(this.GetNativeParameterName(paramname));
        }

        #endregion

        #region public virtual string GetUniqueParameterName(string prefix)

        public virtual string GetUniqueParameterName(string prefix)
        {
            string name = "_param" + this.GetNextID();
            return name;
        }

        #endregion

#if SERVER

        #region public virtual DbParameter CreateCommandParameter(DbCommand cmd, StatementParameter sparam)

        public virtual DParameter CreateCommandParameter(DCommand cmd, StatementParameter sparam)
        {
            DParameter p = cmd.CreateParameter();
            PopulateParameter(p, sparam);

            return p;

        }

        internal void PopulateParameter(DbParameter p, StatementParameter sparam)
        {
            string name = sparam.ParameterName;
            //if (string.IsNullOrEmpty(name))
            //    p.SourceColumn = this.GetUniqueParameterName(DBParam.ParameterNamePrefix);
            //else
            //    p.SourceColumn = name;

            p.ParameterName = this.GetNativeParameterName(name);

            object value = sparam.ValueSource.Value;


            if (sparam.ValueSource.HasType)
                p.DbType = sparam.ValueSource.DbType;
            else
                p.DbType = this.GetDbType(value);

            if (sparam.ValueSource.Size > 0)
                p.Size = sparam.ValueSource.Size;
            else
                p.Size = 0;

            value = ConvertParamValueToNativeValue(p.DbType, value);
            p.Direction = sparam.ValueSource.Direction;
            p.Value = value;
        }

        #endregion

#endif

        #region protected virtual System.Data.DbType GetDbType(object val)


        protected virtual System.Data.DbType GetDbType(object val)
        {
            return DBTypeHelper.GetDBTypeForObject(val);
        }

        #endregion

        #region protected virtual object ConvertParamValueToNativeValue(DbType type, object value)

        protected virtual object ConvertParamValueToNativeValue(DbType type, object value)
        {
            if (null == value)
                value = DBNull.Value;
            return value;
        }

        #endregion

        #region public virtual string GetNativeParameterName(string paramName)

        public virtual string GetNativeParameterName(string paramName)
        {
            return string.Format(DBProps.Get<string>(DBProperties.Ks.ParameterFormat), paramName);
        }

        #endregion

        //
        // write methods
        //

        #region public virtual void WriteTop(double value, double offset, TopType topType)

        public virtual void WriteTop(double count, double offset, TopType topType)
        {
            if(DBProps.CheckSupportsTopType(topType) == false)
               throw new NotSupportedException("The top type '" + topType.ToString() + "' is not supported by this database");
 
            Writer.Write(Skw.TOP );
            if (topType == TopType.Percent)
            {
                Writer.Write(count);
                Writer.Write(Skw.PERCENT );
            }
            else if (topType == TopType.Count)
            {
                Writer.Write((int)count);
                Writer.Write(" ");
            }
            else if (topType == TopType.Range)
            {
                Writer.Write((int)count);
                Writer.Write(", ");
                Writer.Write((int)offset);
            }

        }

        #endregion

        #region public virtual void WriteDistinct()

        public virtual void WriteDistinct()
        {
            Writer.Write(Skw.DISTINCT);
        }

        #endregion

        #region public virtual void WriteRaw(string str)

        public virtual void WriteRaw(string str)
        {
            Writer.Write(str);
        }

        #endregion

        #region public virtual void Write(int val) + 8 overloads

        public virtual void Write(int val)
        {
            this.WriteLiteral(DbType.Int32, val);
        }

        public virtual void Write(bool val)
        {
            this.WriteLiteral(DbType.Boolean, val);
        }

        public virtual void Write(decimal d)
        {
            this.WriteLiteral(DbType.Decimal, d);
        }

        public virtual void Write(double val)
        {
            this.WriteLiteral(DbType.Double, val);
        }

        public virtual void Write(long val)
        {
            this.WriteLiteral(DbType.Int64, val);
        }

        public virtual void Write(float val)
        {
            this.WriteLiteral(DbType.Single, val);
        }

        public virtual void Write(char val)
        {
            this.WriteLiteral(DbType.StringFixedLength, val);
        }

        public virtual void Write(Guid id)
        {
            this.WriteLiteral(DbType.Guid, id.ToString());
        }

        public virtual void Write(DateTime dt)
        {
            this.WriteLiteral(DbType.DateTime, dt);
        }

        public virtual void Write(string value)
        {
            this.WriteLiteral(DbType.String, value);
        }

        public virtual void WriteStringClear(string value)
        {
            this.WriteStringOnly(DbType.String, value);
        }


        #endregion

        #region public virtual void WriteFormat(string format, object arg) + 2 overloads

        public virtual void WriteFormat(string format, object arg)
        {
            Writer.Write(format, arg);
        }

        public virtual void WriteFormat(string format, object arg1, object arg2)
        {
            Writer.Write(format, arg1, arg2);
        }

        public virtual void WriteFormat(string format, params object[] args)
        {
            Writer.Write(format, args);
        }

        #endregion

        #region public virtual void WriteOperator(Operator operation)

        public virtual void WriteOperator(Operator operation)
        {
            string ops;

            switch (operation)
            {
                case Operator.Equals:
                    ops = " = ";
                    break;
                case Operator.LessThan:
                    ops = " < ";
                    break;
                case Operator.GreaterThan:
                    ops = " > ";
                    break;
                case Operator.LessThanEqual:
                    ops = " <= ";
                    break;
                case Operator.GreaterThanEqual:
                    ops = " >= ";
                    break;
                case Operator.In:
                    ops = Skw.IN ;
                    break;
                case Operator.NotIn:
                    ops =Skw.NOT_IN;
                    break;
                case Operator.NotEqual:
                    ops = " <> ";
                    break;
                case Operator.Like:
                    ops = Skw.LIKE ;
                    break;
                case Operator.Is:
                    ops = Skw.IS;
                    break;
                case Operator.Add:
                    ops = " + ";
                    break;
                case Operator.Subtract:
                    ops = " - ";
                    break;
                case Operator.Multiply:
                    ops = " * ";
                    break;
                case Operator.Divide:
                    ops = " / ";
                    break;
                case Operator.BitShiftLeft:
                    ops = " << ";
                    break;
                case Operator.BitShiftRight:
                    ops = " >> ";
                    break;
                case Operator.Modulo:
                    ops = " % ";
                    break;
                case Operator.Not:
                    ops = Skw.NOT ;
                    break;
                case Operator.Exists:
                    ops = Skw.EXISTS;
                    break;
                case Operator.Between:
                    ops =Skw.BETWEEN ;
                    break;
                case Operator.And:
                    ops = Skw.AND  ;
                    break;
                case Operator.Or:
                    ops = Skw.OR ;
                    break;
                case Operator.XOr:
                    ops = Skw.XOR;
                    break;
                case Operator.BitwiseAnd:
                    ops = " & ";
                    break;
                case Operator.BitwiseOr:
                    ops = " | ";
                    break;
                case Operator.Concat:
                    ops = " & ";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(" The operation '" + operation + "' is not a known operator");
            }
            Writer.Write(ops);
        }

        #endregion

        #region public virtual void WriteLiteral(DbType dbType, object value)

        public virtual void WriteLiteral(DbType dbType, object value)
        {
            if (null == value)
                this.WriteNull();
            else
            {
                switch (dbType)
                {
                    case DbType.Binary:
                        Writer.Write("0x" + String.Join(String.Empty, ((byte[])value).Select(b => b.ToString("X2"))));
                        break;

                    case DbType.Date:
                    case DbType.Time:
                    case DbType.DateTime:
                    case DbType.DateTime2:
                    case DbType.DateTimeOffset:
                        this.BeginDateLiteral();
                        string date = value.ToString();
                        Writer.Write(((DateTime)value)
                            .ToCultureTimeODBCStringFormat());// ToString()); //this.DateFormatString
                        this.EndDateLiteral();
                        break;


                    //{
                    //    this.BeingStringLiteral();
                    //    this.Writer.Write(this.EscapeString((string)value));
                    //    this.EndStringLiteral();
                    //    break;
                    //}
                    //break; При Гуиде тоже тсоит ставить знчение параметра в ''
                    case DbType.Guid:
                        this.BeingStringLiteral();
                        Writer.Write(this.EscapeString(value.ToString()));
                        this.EndStringLiteral();
                        break;
                    case DbType.Double:
                        Writer.Write((value.ToString()).Replace(",", "."));
                        break;
                    case DbType.Xml:
                    case DbType.AnsiString:
                    case DbType.AnsiStringFixedLength:
                    case DbType.String:
                    case DbType.StringFixedLength:
                        this.BeingStringLiteral();
                        Writer.Write(this.EscapeString((string)value));
                        this.EndStringLiteral();
                        break;
                    default:
                        if (value is DBNull)
                            Writer.Write(Skw.NULL);
                        else
                            Writer.Write(value);
                        break;
                }
            }
        }

        public virtual void WriteStringOnly(DbType dbType, object value)
        {
            if (null == value)
                this.WriteNull();
            else
            {
                switch (dbType)
                {
                    case DbType.Binary:
                        throw new NotSupportedException("Cannot write a binary literal");

                    case DbType.Date:
                    case DbType.Time:
                    case DbType.DateTime:
                    case DbType.DateTime2:
                    case DbType.DateTimeOffset:
                        this.BeginDateLiteral();
                        string date = value.ToString();
                        Writer.Write(((DateTime)value)
                            .ToCultureTimeODBCStringFormat()); // ToString()); //this.DateFormatString
                        this.EndDateLiteral();
                        break;
                    case DbType.Guid:
                        break;
                    case DbType.Xml:
                    case DbType.AnsiString:
                    case DbType.AnsiStringFixedLength:
                    case DbType.String:
                    case DbType.StringFixedLength:
                        //this.BeingStringLiteral();
                        Writer.Write(this.EscapeString((string)value));
                        //this.EndStringLiteral();
                        break;
                    default:
                        if (value is DBNull)
                           Writer.Write(Skw.NULL);
                        else
                           Writer.Write(value);
                        break;
                }
            }
        }

        #endregion

        #region public virtual void WriteNull()

        public virtual void WriteNull()
        {
            Writer.Write(Skw.NULL);
        }

        #endregion

        #region public virtual void WriteAllFieldIdentifier(string schemaOwner, string sourceTable)

        public virtual void WriteAllFieldIdentifier(string schemaOwner, string sourceTable)
        {
            if (string.IsNullOrEmpty(sourceTable) && !string.IsNullOrEmpty(schemaOwner))
                throw new ArgumentNullException("sourceTable", "Cannot Specify Schema Owner And Not Table");

            if (string.IsNullOrEmpty(schemaOwner) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(schemaOwner);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            if (string.IsNullOrEmpty(sourceTable) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(sourceTable);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            Writer.Write(this.GetAllFieldIdentifier());
        }

        #endregion

        #region public virtual void WriteSourceField(string schemaOwner, string sourceTable, string columnName, string alias)

        /// <summary>
        /// Appends the owner (optional), table (optional unless owner is specified) and column (required) to the statement and
        /// sets the alias name (optional). All the identifers are wrapped within the identifier delimiter characters
        /// </summary>
        /// <param name="schemaOwner"></param>
        /// <param name="sourceTable"></param>
        /// <param name="columnName"></param>
        /// <param name="alias"></param>
        public virtual void WriteSourceField(string schemaOwner, string sourceTable, string columnName, string alias)
        {
            if (string.IsNullOrEmpty(columnName))
                throw new ArgumentNullException("columnName", "No Column Name Specified For A Field");

            if (string.IsNullOrEmpty(sourceTable) && !string.IsNullOrEmpty(schemaOwner))
                throw new ArgumentNullException("sourceTable", "Cannot Specify Schema Owner And Not Table");

            if (string.IsNullOrEmpty(schemaOwner) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(schemaOwner);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            if (string.IsNullOrEmpty(sourceTable) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(sourceTable);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            this.BeginIdentifier();
            this.WriteRaw(columnName);
            this.EndIdentifier();

            if (string.IsNullOrEmpty(alias) == false)
            {
                this.WriteAlias(alias);
            }
        }

        #endregion

        #region public virtual void WriteSourceTable(string schemaOwner, string sourceTable, string alias)

        /// <summary>
        /// Appends the owner (optional) and table (required) to the statement and sets the alias name (optional).
        /// All the identifiers are wrapped with the delimiter characters
        /// </summary>
        /// <param name="schemaOwner"></param>
        /// <param name="sourceTable"></param>
        /// <param name="alias"></param>
        public virtual void WriteSourceTable(string schemaOwner, string sourceTable, string alias)
        {

            if (string.IsNullOrEmpty(sourceTable))
                throw new ArgumentNullException("sourceTable", "No Table Name Specified For A Table");

            if (string.IsNullOrEmpty(schemaOwner) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(schemaOwner);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            this.BeginIdentifier();
            this.WriteRaw(sourceTable);
            this.EndIdentifier();

            if (string.IsNullOrEmpty(alias) == false)
            {
                WriteAlias(alias);
            }
        }

        #endregion

        #region public virtual void WriteSourceTable(string schemaOwner, string sourceTable, string alias)

        /// <summary>
        /// Appends the owner (optional) and table (required) to the statement and sets the alias name (optional).
        /// All the identifiers are wrapped with the delimiter characters
        /// </summary>
        /// <param name="schemaOwner"></param>
        /// <param name="sourceTable"></param>
        /// <param name="alias"></param>
        public virtual void WriteSourceTable(string catalog, string schemaOwner, string sourceTable, string alias)
        {

            if (string.IsNullOrEmpty(sourceTable))
                throw new ArgumentNullException("sourceTable", "No Table Name Specified For A Table");

            if (string.IsNullOrEmpty(catalog) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            if (string.IsNullOrEmpty(schemaOwner) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(schemaOwner);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            this.BeginIdentifier();
            this.WriteRaw(sourceTable);
            this.EndIdentifier();

            if (string.IsNullOrEmpty(alias) == false)
            {
                WriteAlias(alias);
            }
        }

        #endregion

        #region public virtual void WriteSourceTable(string schemaOwner, string sourceTable)

        /// <summary>
        /// Appends the catalog (optional),  owner (optional) and source (required) to the statement.
        /// All the identifiers are wrapped with the delimiter characters
        /// </summary>
        /// <param name="schemaOwner"></param>
        /// <param name="sourceTable"></param>
        /// <param name="alias"></param>
        public virtual void WriteSource(string catalog, string schemaOwner, string source)
        {

            if (string.IsNullOrEmpty(source))
                throw new ArgumentNullException("source", "No Table Name Specified For A Table");

            if (string.IsNullOrEmpty(catalog) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            if (string.IsNullOrEmpty(schemaOwner) == false)
            {
                this.BeginIdentifier();
                this.WriteRaw(schemaOwner);
                this.EndIdentifier();
                this.AppendIdSeparator();
            }

            this.BeginIdentifier();
            this.WriteRaw(source);
            this.EndIdentifier();

        }

        #endregion

        #region public virtual void WriteAlias(string alias)

        public virtual void WriteAlias(string alias)
        {
            this.BeginAlias();
            this.BeginIdentifier();
            this.WriteRaw(alias);
            this.EndIdentifier();
            this.EndAlias();
        }

        #endregion


        //
        // dispose and finalize
        //

        #region protected virtual void Dispose(bool disposing)

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && OwnsWriter)
                Writer.Dispose();


        }
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region public void Dispose()



        #endregion

        #region ~DBStatementBuilder()

        ~DStatementBuilder()
        {
            this.Dispose(false);
        }

        #endregion

        //
        // object overrides
        //

        #region public override string ToString()

        public override string ToString()
        {
            return Writer.ToString();
        }

        #endregion




        public virtual void GenerateCreateTableScript(DBSchemaTable schemaTable)
        {
            throw new NotSupportedException(Skw.CREATE_TABLE );
        }

        public virtual void GenerateCreateViewScript(DBSchemaView schemaView, DQuery script)
        {
            throw new NotSupportedException(Skw.CREATE_VIEW);
        }

        public virtual void GenerateCreateProcedureScript(DBSchemaSproc schemaSproc, DScript script)
        {
            throw new NotSupportedException(Skw.CREATE_PROCEDURE);
        }

        public virtual void GenerateCreateFunctionScript(DBSchemaFunction schemaSproc, DScript script)
        {
            throw new NotSupportedException(Skw.CREATE_FUNCTION);
        }

        public virtual void GenerateCreateIndexScript(DBSchemaIndex schemaIndex)
        {
            throw new NotSupportedException(Skw.CREATE_INDEX);
        }


        //
        // GenerateDropXXX methods
        //

        #region public virtual void GenerateDropTableScript(DBSchemaItemRef itemRef)

        public virtual void GenerateDropTableScript(DBSchemaItemRef itemRef)
        {
            if (null == itemRef)
                throw new ArgumentNullException("itemRef");

            if (string.IsNullOrEmpty(itemRef.Name))
                throw new ArgumentNullException("itemRef.Name");

            if (itemRef.Type != DBSchemaTypes.Table)
                throw new ArgumentOutOfRangeException("itemRef.Type");


            this.BeginDrop(itemRef.Type);

            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
            this.EndDrop(itemRef.Type);
        }

        #endregion

        #region public virtual void GenerateDropIndexScript(DBSchemaItemRef itemRef)

        public virtual void GenerateDropIndexScript(DBSchemaItemRef itemRef)
        {
            //DROP INDEX 'indexname' ON 'table name'

            if (null == itemRef)
                throw new ArgumentNullException("itemRef");

            if (string.IsNullOrEmpty(itemRef.Name))
                throw new ArgumentNullException("itemRef.Name");

            if (itemRef.Type != DBSchemaTypes.Index)
                throw new ArgumentOutOfRangeException("itemRef.Type");

            if (itemRef.Container == null || string.IsNullOrEmpty(itemRef.Container.Name) || itemRef.Container.Type != DBSchemaTypes.Table)
                throw new ArgumentNullException("itemRef.Container", "Container Not Set On Index Reference");

            this.BeginDrop(DBSchemaTypes.Index);
            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
            this.WriteRaw(" ON ");

            //write the table reference
            itemRef = itemRef.Container;
            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
        }


        #endregion

        #region public virtual void GenerateDropViewScript(DBSchemaItemRef itemRef)

        public virtual void GenerateDropViewScript(DBSchemaItemRef itemRef)
        {
            if (null == itemRef)
                throw new ArgumentNullException("itemRef");

            if (string.IsNullOrEmpty(itemRef.Name))
                throw new ArgumentNullException("itemRef.Name");

            if (itemRef.Type != DBSchemaTypes.View)
                throw new ArgumentOutOfRangeException("itemRef.Type");


            this.BeginDrop(itemRef.Type);

            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
            this.EndDrop(itemRef.Type);
        }

        #endregion

        #region public virtual void GenerateDropProcedureScript(DBSchemaItemRef itemRef)

        public virtual void GenerateDropProcedureScript(DBSchemaItemRef itemRef)
        {
            if (null == itemRef)
                throw new ArgumentNullException("itemRef");

            if (string.IsNullOrEmpty(itemRef.Name))
                throw new ArgumentNullException("itemRef.Name");

            if (itemRef.Type != DBSchemaTypes.StoredProcedure)
                throw new ArgumentOutOfRangeException("itemRef.Type");


            this.BeginDrop(itemRef.Type);

            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
            this.EndDrop(itemRef.Type);
        }

        #endregion

        #region public virtual void GenerateDropFunctionScript(DBSchemaItemRef itemRef)

        public virtual void GenerateDropFunctionScript(DBSchemaItemRef itemRef)
        {
            if (null == itemRef)
                throw new ArgumentNullException("itemRef");

            if (string.IsNullOrEmpty(itemRef.Name))
                throw new ArgumentNullException("itemRef.Name");

            if (itemRef.Type != DBSchemaTypes.Function)
                throw new ArgumentOutOfRangeException("itemRef.Type");


            this.BeginDrop(itemRef.Type);

            this.BeginIdentifier();
            if (string.IsNullOrEmpty(itemRef.Catalog) == false)
            {
                this.WriteRaw(itemRef.Catalog);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            if (string.IsNullOrEmpty(itemRef.Schema) == false)
            {
                this.WriteRaw(itemRef.Schema);
                this.EndIdentifier();
                this.AppendIdSeparator();
                this.BeginIdentifier();
            }

            this.WriteRaw(itemRef.Name);
            this.EndIdentifier();
            this.EndDrop(itemRef.Type);
        }

        #endregion

        #region protected virtual List<DBSchemaColumn> SortColumnsByOrdinal(IEnumerable<DBSchemaColumn> columns)

        protected virtual List<DBSchemaColumn> SortColumnsByOrdinal(IEnumerable<DBSchemaColumn> columns)
        {
            List<DBSchemaColumn> remain = new List<DBSchemaColumn>(columns);
            List<DBSchemaColumn> sorted = new List<DBSchemaColumn>(remain.Count);
            int maxOrd = -1;

            foreach (DBSchemaColumn col in remain)
            {
                if (col.OrdinalPosition > -1)
                    maxOrd = System.Math.Max(maxOrd, col.OrdinalPosition);
            }

            if (maxOrd < 0)// there are no ordinals set so just return the default order
                return remain;

            int currOrd = 0;
            while (currOrd <= maxOrd)
            {
                int lastcount = sorted.Count;

                foreach (DBSchemaColumn col in remain)
                {
                    //if the columns ordinal position matches our current ordinal - add it
                    if (col.OrdinalPosition == currOrd)
                        sorted.Add(col);
                }

                //remove the colums that were added to sorted from the remain collection
                while (lastcount < sorted.Count)
                {
                    remain.Remove(sorted[lastcount]);
                    lastcount++;
                    if (remain.Count < 1)
                        break;
                }

                currOrd++;
            }

            if (remain.Count > 0)
                //we have left over without ordinals set to add them 
                //to the end in the order that they were inserted into the collection
                sorted.AddRange(remain);

            return sorted;


        }


        #endregion

        #region protected virtual List<DBSchemaParameter> SortColumnsByOrdinal(IEnumerable<DBSchemaParameter> columns)

        protected virtual List<DBSchemaParameter> SortColumnsByOrdinal(IEnumerable<DBSchemaParameter> parameters)
        {
            List<DBSchemaParameter> remain = new List<DBSchemaParameter>(parameters);
            List<DBSchemaParameter> sorted = new List<DBSchemaParameter>(remain.Count);
            int maxOrd = -1;

            foreach (DBSchemaParameter col in remain)
            {
                if (col.ParameterIndex > -1)
                    maxOrd = System.Math.Max(maxOrd, col.ParameterIndex);
            }

            if (maxOrd < 0)// there are no ordinals set so just return the default order
                return remain;

            int currOrd = 0;
            while (currOrd <= maxOrd)
            {
                int lastcount = sorted.Count;

                foreach (DBSchemaParameter col in remain)
                {
                    //if the columns ordinal position matches our current ordinal - add it
                    if (col.ParameterIndex == currOrd)
                        sorted.Add(col);
                }

                //remove the colums that were added to sorted from the remain collection
                while (lastcount < sorted.Count)
                {
                    remain.Remove(sorted[lastcount]);
                    lastcount++;
                    if (remain.Count < 1)
                        break;
                }

                currOrd++;
            }

            if (remain.Count > 0)
                //we have left over without ordinals set to add them 
                //to the end in the order that they were inserted into the collection
                sorted.AddRange(remain);

            return sorted;

        }


        #endregion


        #region protected virtual string GetNativeTypeForDbType(System.Data.DbType dbType, int setSize, out string options)
        protected abstract string GetNativeTypeForDbType(DbType dbType, int setSize, out string options);
     
        #endregion



        #region public virtual void Begin/End DeclareStatement(DBParam param)

        public virtual void BeginDeclareStatement(DParam param)
        {
            this.BeginNewLine();

            this.WriteRaw("DECLARE ");
            if (!param.HasName)
                param.Name = this.GetUniqueParameterName(ParameterNamePrefix);

            this.WriteParameterReference(param.Name);
            this.WriteRaw(" ");
            string options;
            string type = this.GetNativeTypeForDbType(param.DbType, param.Size, out options);

            this.WriteRaw(type);
            if (!string.IsNullOrEmpty(options))
            {
                this.WriteRaw(options);
                if (!options.EndsWith(" "))
                    this.WriteRaw(" ");
            }

        }

        public virtual void EndDeclareStatement()
        {
            if (Depth < 1)
                this.WriteRaw(";");
            this.BeginNewLine();
        }

        #endregion

        #region public virtual void Begin/End SetStatement()

        public virtual void BeginSetStatement()
        {
            this.BeginNewLine();

            this.WriteRaw("SET ");
        }

        public virtual void EndSetStatement()
        {
            if (Depth < 1)
                this.WriteRaw(";");
            this.BeginNewLine();
        }

        #endregion

        #region public virtual void Begin/End Returns Statement

        public virtual void BeginReturnsStatement()
        {
            this.BeginNewLine();

            this.WriteRaw(Skw.RETURN);
        }

        public virtual void EndReturnsStatement()
        {
            if (Depth < 1)
                this.WriteRaw(";");
            this.BeginNewLine();
        }

        #endregion

        public virtual void WithChangeTrackingContext(Guid ChangeTrackingContext)
        {
            if (ChangeTrackingContext != Guid.Empty)
            {
                WriteRaw(String.Format("WITH CHANGE_TRACKING_CONTEXT (0x{0})", ChangeTrackingContext.ToString("N")));
                BeginNewLine();
            }
        }
    }

}
