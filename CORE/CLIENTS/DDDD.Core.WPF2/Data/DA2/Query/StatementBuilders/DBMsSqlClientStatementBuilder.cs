
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace DDDD.Core.Data.DA2.Query
{
    public class DBMsSqlClientStatementBuilder : DStatementBuilder
    {
        public DBMsSqlClientStatementBuilder()
            : base()
        {
            Initialize();
        }

        private void Initialize()
        {
            var provider =  SqlDataProvider.Get(typeof(MsSqlDataProvider));
            InitOnThread(provider.GetDefaultProperties());
               
        }


        #region protected virtual string GetNativeTypeForDbType(System.Data.DbType dbType, int setSize, out string options)

        protected override string GetNativeTypeForDbType(DbType dbType, int setSize, out string options)
        {
            string type;
            options = string.Empty;

            switch (dbType)
            {
                case System.Data.DbType.AnsiStringFixedLength:
                case System.Data.DbType.StringFixedLength:
                    if (setSize < 1)
                        setSize = 255;
                    else if (setSize > 255)
                        throw new ArgumentOutOfRangeException("CHAR size", "The maximum supported fixed length charater string is 255");

                    type = "CHAR";
                    options = "(" + setSize + ")";
                    break;

                case System.Data.DbType.String:
                case System.Data.DbType.AnsiString:
                    if (setSize < 0)
                        setSize = 255;
                    if (setSize < 256)
                    {
                        type = "VARCHAR";
                        options = "(" + setSize.ToString() + ")";
                    }
                    else if (setSize < 65536)
                        type = "TEXT";
                    else if (setSize < 16777215)
                        type = "MEDIUMTEXT";
                    else
                        type = "LONGTEXT";
                    break;


                case System.Data.DbType.Binary:
                case System.Data.DbType.Object:

                    if (setSize > 0)
                    {
                        if (setSize < 256)
                            type = "TINYBLOB";
                        else if (setSize < 65536)
                            type = "BLOB";
                        else if (setSize < 16777216)
                            type = "MEDIUMBLOB";
                        else
                            type = "LONGBLOB";
                    }
                    else
                        type = "MEDIUMBLOB";
                    break;


                case System.Data.DbType.Boolean:
                    type = "BIT";
                    break;

                case System.Data.DbType.Byte:
                    type = "TINYINT";
                    options = " UNSIGNED";
                    break;

                case System.Data.DbType.Date:
                    type = "DATE";
                    break;

                case System.Data.DbType.DateTime:
                    type = "DATETIME";
                    break;

                case System.Data.DbType.DateTime2:
                    type = "DATETIME";
                    break;

                case System.Data.DbType.DateTimeOffset:
                    type = "TIMESTAMP";
                    break;

                case System.Data.DbType.Currency:
                case System.Data.DbType.Decimal:
                    type = "DECIMAL";
                    break;

                case System.Data.DbType.Double:
                    type = "DOUBLE";
                    break;

                case System.Data.DbType.Guid:
                    type = "BINARY";
                    options = "(16)";
                    break;

                case System.Data.DbType.Int16:
                    type = "SMALLINT";
                    break;

                case System.Data.DbType.Int32:
                    type = "INT";
                    break;

                case System.Data.DbType.Int64:
                    type = "BIGINT";
                    break;

                case System.Data.DbType.SByte:
                    type = "TINYINT";
                    break;

                case System.Data.DbType.Single:
                    type = "FLOAT";
                    break;

                case System.Data.DbType.Time:
                    type = "TIME";
                    break;

                case System.Data.DbType.UInt16:
                    type = "SMALLINT";
                    options = " UNSIGNED";
                    break;

                case System.Data.DbType.UInt32:
                    type = "INT";
                    options = " UNSIGNED";
                    break;
                case System.Data.DbType.UInt64:
                    type = "BIGINT";
                    options = " UNSIGNED";
                    break;
                case System.Data.DbType.VarNumeric:
                    type = "DECIMAL";
                    break;
                case System.Data.DbType.Xml:
                    type = "LONGTEXT";
                    break;
                default:
                    throw new NotSupportedException("The DbType '" + dbType.ToString() + "'is not supported");

            }

            return type;
        }

        #endregion

    }
}
