﻿
using System;
using System.Data;

namespace DDDD.Core.Data.DA2.Query
{
    public class DBSQLiteStatementBuilder : DStatementBuilder
    {
        #region ivars

        /// <summary>
        /// Defines the value for the TopN function that is later output on the sql string as Limits
        /// </summary>
        private int _limits = -1;

        /// <summary>
        /// defines the value for the offset at which to start outputting the rows and appended to the LIMIT value
        /// </summary>
        private int _offset = -1;


        #endregion

        public DBSQLiteStatementBuilder()
            : base()
        {
        }
 

        public override void BeginFunction(Function function, string name)
        {
            if (function == Function.LastID)
                this.WriteRaw("last_insert_rowid");
            else
                base.BeginFunction(function, name);
        }

        public override void BeginScript()
        {
            //this.Writer.Write("BEGIN");
            this.IncrementStatementBlock();
            //this.BeginNewLine();
        }

        public override void EndScript()
        {
            this.DecrementStatementBlock();
            //this.BeginNewLine();
            //this.Writer.WriteLine("END");
        }

        public override void WriteTop(double limit, double offset, TopType topType)
        {
            

            if (Depth == 1)
            {
                if (topType == TopType.Percent)
                    throw new NotSupportedException("This provider does not support the top percent syntax");

                this._limits = (int)limit;
                this._offset = (int)offset;
            }
        }

        public override void EndSelectStatement()
        {
            if (Depth == 1 && this._limits > 0)
            {
                this.WriteRaw(" LIMIT ");
                this.WriteRaw(_limits.ToString());
                if (this._offset > 0)
                {
                    this.WriteRaw(" OFFSET ");
                    this.WriteRaw(_offset.ToString());
                }
            }
            base.EndSelectStatement();
        }

        protected override string GetNativeTypeForDbType(DbType dbType, int setSize, out string options)
        {
            throw new NotImplementedException();
        }
    }
}
