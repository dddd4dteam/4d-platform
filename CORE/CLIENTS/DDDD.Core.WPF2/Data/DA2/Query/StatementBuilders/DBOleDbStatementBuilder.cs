
using System;
using System.Data;

namespace DDDD.Core.Data.DA2.Query
{
    internal class DBOleDbStatementBuilder : DStatementBuilder
    {
        public DBOleDbStatementBuilder()
            : base()
        {
        }

     

        public override void BeginDateLiteral()
        {
            this.WriteRaw("#");
            //base.BeginDateLiteral();
        }

        public override void EndDateLiteral()
        {
            this.WriteRaw("#");
            //base.EndDateLiteral();
        }

        public override void BeginScript()
        {
            //this.Writer.Write("BEGIN");
            this.IncrementStatementBlock();
            //this.BeginNewLine();
        }

        public override void EndScript()
        {
            this.DecrementStatementBlock();
            //this.BeginNewLine();
            //this.Writer.WriteLine("END");
        }

        public override void WriteStatementTerminator()
        {
            base.WriteStatementTerminator();
        }

        public override void BeginFunction(Function function, string name)
        {
            if (function == Function.LastID)
                this.WriteRaw("@@Identity");//don't like this, but it looks like the only way
                                            //any other options then I'd go with them happily
            else
                base.BeginFunction(function, name);
        }
        protected override object ConvertParamValueToNativeValue(System.Data.DbType type, object value)
        {
            object converted;
            if (value is DateTime)
            {
                DateTime dtvalue = (DateTime)value;

                if (type == System.Data.DbType.DateTime)
                {
                    converted = new DateTime(dtvalue.Year, dtvalue.Month, dtvalue.Day, dtvalue.Hour, dtvalue.Minute, dtvalue.Second);
                }
                else if (type == System.Data.DbType.Date)
                {
                    converted = new DateTime(dtvalue.Year, dtvalue.Month, dtvalue.Day);
                }
                else if (type == System.Data.DbType.DateTime2)
                {
                    converted = new DateTime(dtvalue.Year, dtvalue.Month, dtvalue.Day, dtvalue.Hour, dtvalue.Minute, dtvalue.Second);
                }
                else
                    converted = base.ConvertParamValueToNativeValue(type, value);
            }
            else
                converted = base.ConvertParamValueToNativeValue(type, value);

            return converted;
        }

        protected override string GetNativeTypeForDbType(DbType dbType, int setSize, out string options)
        {
            throw new NotImplementedException();
        }
    }
}
