
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Abstract class that extends DBExpressionSet to support calculations
    /// </summary>
    [DataContract]
    public partial class DCalculableExpressionSet : DExpressionSet, IDCalculable //abstract
    {

        public DCalculableExpressionSet()
        {   }


        /// <summary>
        /// Supports the calculation of the last expression added to this set and the provided clause using the binary operation
        /// </summary>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        protected virtual DCalculableExpressionSet Calculate(BinaryOp op, DClause clause) //     //abstract
        {
            try
            {    return null; 
                //Empty Realisation
            }
            catch (Exception exc )
            { throw exc;   } 
        }
        
        //
        // interface implementations
        //

        #region DBClause IDBCalculable.Calculate(BinaryOp op, DBClause dbref)
        /// <summary>
        /// Explicit implementation of the IDBCalculable.Calculate method
        /// </summary>
        /// <param name="op"></param>
        /// <param name="dbref"></param>
        /// <returns></returns>
        DClause IDCalculable.Calculate(BinaryOp op, DClause dbref)
        {
            return this.Calculate(op, dbref);
        }

        #endregion

        //
        // operator overrides
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DCalculableExpressionSet()
            {   Last = (this.Last != null) ? this.Last.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


        #region public static DBCalculableExpressionSet operator +(DBCalculableExpressionSet left, DBClause right)

        /// <summary>
        /// Adds the last operation in the passed set with the right clause 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableExpressionSet operator +(DCalculableExpressionSet left, DClause right)
        {
            return left.Calculate(BinaryOp.Add, right);
        }

        #endregion

        #region public static DBCalculableExpressionSet operator -(DBCalculableExpressionSet left, DBClause right)
        /// <summary>
        /// Subtracts the last operation in the passed set with the right clause 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableExpressionSet operator -(DCalculableExpressionSet left, DClause right)
        {
            return left.Calculate(BinaryOp.Subtract, right);
        }

        #endregion

        #region public static DBCalculableExpressionSet operator *(DBCalculableExpressionSet left, DBClause right)
        /// <summary>
        /// Multiplies the last operation in the passed set with the right clause 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableExpressionSet operator *(DCalculableExpressionSet left, DClause right)
        {
            return left.Calculate(BinaryOp.Multiply, right);
        }

        #endregion

        #region public static DBCalculableExpressionSet operator /(DBCalculableExpressionSet left, DBClause right)
        /// <summary>
        /// Divides the last operation in the passed set with the right clause 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableExpressionSet operator /(DCalculableExpressionSet left, DClause right)
        {
            return left.Calculate(BinaryOp.Divide, right);
        }

        #endregion

        #region public static DBCalculableExpressionSet operator %(DBCalculableExpressionSet left, DBClause right)
        /// <summary>
        /// Modulo's the last operation in the passed set with the right clause 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableExpressionSet operator %(DCalculableExpressionSet left, DClause right)
        {
            return left.Calculate(BinaryOp.Modulo, right);
        }

        #endregion

    }
}
