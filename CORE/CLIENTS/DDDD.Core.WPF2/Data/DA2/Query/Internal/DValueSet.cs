
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DValueSet : DCalculableExpressionSet, IDBoolean //internal
    {
        private DClauseList _clauses;

        //
        // properties
        //

        #region public DBClauseList Clauses {get;}
        
        [DataMember]
        public DClauseList Clauses
        {
            get 
            {
                if (_clauses == null)
                    _clauses = new DClauseList();
                return _clauses;
            }
            set { _clauses = value; }
        }

        #endregion

        #region internal bool HasClauses {get;}

        internal  bool HasClauses //protected
        {
            get { return this._clauses != null && this._clauses.Count > 0; }
        }

        #endregion


        //
        // SQL Statement builder methods
        //

        
        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DValueSet()
            {
                Clauses  = (this.Clauses != null) ? this.Clauses.Clone() as DClauseList : null,                     
                Last = (this.Last != null) ? this.Last.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        




        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._clauses != null && this._clauses.Count > 0)
                return this.Clauses.BuildStatement(builder);
            else
                return false;
        }


        #endregion

        //
        // XML serialization methods
        //

        //#region protected override string XmlElementName

        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Values;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasClauses)
        //        this.Clauses.WriteXml(writer, context);

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    return this.Clauses.ReadXml(reader.LocalName, reader, context);
        //}

        //#endregion

        //
        // Interface implmentations
        //

        #region IDBCalculable - protected override DBCalculableExpressionSet Calculate(BinaryOp op, DBClause dbref)

        protected override DCalculableExpressionSet Calculate(BinaryOp op, DClause dbref)
        {
            if (this.Last == null)
                throw new InvalidOperationException("There is no previous value to calculate with");

            int index = this.Clauses.IndexOf(this.Last);
            DCalc calc = DCalc.Calculate(this.Last, op, dbref);
            this.Clauses[index] = calc;
            this.Last = calc;
            return this;
        }

        #endregion

        #region public DBClause And(DBClause clause)

        public DClause And(DClause clause)
        {
            this.Clauses.Add(clause);
            this.Last = clause;
            return this;
        }


        #endregion

        #region DBClause IDBBoolean.And(DBClause reference)

        DClause IDBoolean.And(DClause reference)
        {
            if (this.Last is IDBoolean)
                ((IDBoolean)this.Last).And(reference);
            else
                this.And(reference);

            return this;
        }

        #endregion

        #region DBClause IDBBoolean.Or(DBClause reference)

        DClause IDBoolean.Or(DClause reference)
        {
            if (this.Last is IDBoolean)
                ((IDBoolean)this.Last).Or(reference);
            else
                throw new NotSupportedException("The OR method is not directly supported on the ValueSet.");

            return this;
        }

        #endregion

        //
        // static statement factory methods
        //

        public static DValueSet Values()
        {
            DValueSet vs = new DValueSet();
            return vs;
        }
    }
}
