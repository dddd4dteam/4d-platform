﻿namespace DDDD.Core.Data.DA2.Query
{

    public interface IDClonableClause
    {
        /// <summary>
        /// Клонирование элементов запроса
        /// эсли у элемента нет реализации то вернуть null
        /// </summary>
        DClause CloneClause();
    }
}
