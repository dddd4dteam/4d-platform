
using DDDD.Core.Data.DA2.Query;
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DFilterSet : DExpressionSet, IDBoolean //internal
    {

        public DFilterSet()
        { }

        private DClause _filters;

        //
        // properties
        //

        #region public DBClause SelectQuery {get;set;}

        [DataMember]
        public DClause Filters
        {
            get { return _filters; }
            set { _filters = value; }
        }

        #endregion

        #region internal bool HasFilters {get;} //protected
                
        public bool HasFilters //protected
        {
            get { return this._filters != null;  }            
        }

        #endregion


        //
        // SQL Statement builder methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DFilterSet()
            {    
                Filters = (this.Filters != null) ? this.Filters.CloneClause() : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null                 
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------


        #region  ------------------------ Compare Two  DBFilterSet Objects ---------------------------

        /// <summary> 
        /// False- ��� ��������� �� �����
        /// True- �������� ��� ��������� ���������
        /// </summary>
        /// <param name="AnotherObject"></param>
        /// <returns></returns>
        public override bool CompareWith(DClause AnotherObject)
        {

            bool compareResult = true;//������� �� ������� ��� ������� ���������
            if ((AnotherObject as DFilterSet) == null) { compareResult = false; return compareResult; }// ������ ���� ������ ������������ ������� ����� null � ������ �� ����� �� null  �� ���� ����� ��� �����������
            
            DFilterSet anFilterSet = AnotherObject as DFilterSet;


            if ( 
                 (this.HasFilters == true && anFilterSet.HasFilters == false) 
                  ||
                 (this.HasFilters == false && anFilterSet.HasFilters == true)
               )   return compareResult; //�.�. � ��� �������- �.�. ���� � ����-�� �� ���� ����� �� ���������� � ������� �������� - ������ False    
           
            
            //������ ��� ������ ���� � ����� ������ �������� �� �����
            if (Filters.CompareWith(anFilterSet.Filters ) == false) compareResult = false;
            
            return compareResult;
        }


        #endregion ------------------------ Compare Two DBFilterSet Objects ---------------------------



        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (Filters == null)
                return false;
            else
            {
                //builder.BeginBlock();
                this.Filters.BuildStatement(builder);
                //builder.EndBlock();

                return true;
            }
        }

        #endregion
        //
        // Xml Serialization methods
        //
        //


        //
        // Statement factory methods
        //

        #region public DBFilterSet And(DBClause combine) + 4 overloads

        public DFilterSet And(string field, Compare op, DClause clause)
        {
            return And(DField.Field(field), op, clause);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DFilterSet And(Type table, string field, Compare op, DClause clause)
        {
            return And(DField.Field(table, field), op, clause);
        }


        public DFilterSet And<TTable>( string field, Compare op, DClause clause)
        {
            return And(DField.Field<TTable>(field), op, clause);
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DFilterSet And(string owner, Type table, string field, Compare op, DClause right)
        {
            return And(DField.Field(owner, table, field), op, right);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DFilterSet And<TTable>(string owner, string field, Compare op, DClause right)
        {
            return And(DField.Field<TTable>(owner, field), op, right);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DFilterSet And(DClause left, Compare op, DClause clause)
        {
            return And(DComparison.Compare(left, op, clause));
        }



        public DFilterSet And(DClause combine)
        {
            if (this.Filters == null)
                throw new InvalidOperationException("There is no current comparison to combine with");
            DBooleanOp bin = DBooleanOp.Compare(this.Filters, BooleanOp.And, combine);
            this.Last = bin;
            this.Filters = bin;
            return this;
        }

        #endregion

        #region public DBFilterSet Or(DBClause combine) + 4 overloads

        public DFilterSet Or(string field, Compare op, DClause clause)
        {
            return Or(DField.Field(field), op, clause);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DFilterSet Or(Type table, string field, Compare op, DClause clause)
        {
            return Or(DField.Field(table, field), op, clause);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DFilterSet Or<TTable>( string field, Compare op, DClause clause)
        {
            return Or(DField.Field<TTable>( field), op, clause);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DFilterSet Or(string owner, Type table, string field, Compare op, DClause right)
        {
            return Or(DField.Field(owner, table, field), op, right);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DFilterSet Or<TTable>(string owner,string field, Compare op, DClause right)
        {
            return Or(DField.Field<TTable>(owner, field), op, right);
        }


        public DFilterSet Or(DClause left, Compare op, DClause clause)
        {
            return Or(DComparison.Compare(left, op, clause));
        }

        public DFilterSet Or(DClause combine)
        {
            if (this.Filters == null)
                throw new InvalidOperationException("There is no current comparison to combine with");
            DBooleanOp bin = DBooleanOp.Compare(this.Filters, BooleanOp.Or, combine);
            this.Last = bin;
            this.Filters = bin;

            return this;
        }

        #endregion

        //
        // static statement factory methods

        #region public static DBFilterSet Where(DBComparison compare) + 2 overloads

        public static DFilterSet Where(DClause left, Compare op, DClause right)
        {
            if (null == left)
                throw new ArgumentNullException("left");
            else if (null == right)
                throw new ArgumentNullException("right");

            DComparison comp = DComparison.Compare(left, op, right);
            return Where(comp);
        }

        public static DFilterSet Where()
        {
            DFilterSet fs = new DFilterSet();
            return fs;
        }

        public static DFilterSet Where(DComparison compare)
        {
            if (null == compare)
                throw new ArgumentNullException("compare");
            DFilterSet fs = new DFilterSet();
            fs.Filters = compare;

            return fs;
        }

        #endregion

        //
        // Interface Implementations
        //

        #region IDBBoolean Members

        DClause IDBoolean.And(DClause reference)
        {
            return this.And(reference);
        }

        DClause IDBoolean.Or(DClause reference)
        {
            return this.Or(reference);
        }

        #endregion
    }
}
