
using DDDD.Core.Data.DA2.Query;
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DGroupBySet : DExpressionSet, IDBoolean, IDAggregate //internal
    {
        public DGroupBySet()
        { }

        private DClauseList _grps;

        //
        // properties
        //

        #region protected DBClauseList Groupings {get;}

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        protected DClauseList Groupings
        {
            get
            {
                if (_grps == null)
                    _grps = new DClauseList();
                return _grps;
            }
            set { _grps = value; }
        }



        #endregion

        #region internal bool HasGroupings {get;}

        internal bool HasGroupings //protected
        {
            get
            {  return (this._grps != null && this._grps.Count > 0); }
        }

        #endregion

        //
        // SQL Statement builder methods
        //



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DGroupBySet()
            {
                Groupings = (this.Groupings != null) ? this.Groupings.Clone() as DClauseList : null,
                Last = (this.Last != null) ? this.Last.CloneClause(): null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        




        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._grps == null || this._grps.Count == 0)
                return false;
            else
                return this._grps.BuildStatement(builder);
        }

        #endregion

        //
        // Xml serialization methods
        //

        //#region protected override string XmlElementName {get;}

        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Group;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasGroupings)
        //        this.Groupings.WriteXml(writer, context);

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //    if (c != null)
        //    {
        //        this.Groupings.Add(c);
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        //#endregion

        //
        // Statement Factory methods
        //

        #region internal DBClause Aggregate(AggregateFunction func, DBClause dbref)

        internal DClause Aggregate(AggregateFunction func, DClause dbref)
        {
            DAggregate agg = DAggregate.Aggregate(func, dbref);
            this.Last = agg;
            this.Groupings.Add(agg);
            return this;
        }

        #endregion

        #region public DBSelectSet And(DBReference dbref)

        internal DGroupBySet And(DClause dbref)
        {
            this.Groupings.Add(dbref);
            this.Last = dbref;
            return this;
        }

        #endregion

        //
        // static factory methods
        //

        #region internal static DBGroupBySet GroupBy() + 4 overloads

        internal static DGroupBySet GroupBy()
        {
            DGroupBySet grp = new DGroupBySet();
            return grp;
        }

        internal static DGroupBySet GroupBy(string field)
        {
            DField fld = DField.Field(field);
            return GroupBy(fld);
        }

        internal static DGroupBySet GroupBy(Type table, string field)
        {
            DField fld = DField.Field(table, field);
            return GroupBy(fld);
        }

        internal static DGroupBySet GroupBy<TTable>( string field)
        {
            DField fld = DField.Field<TTable>( field);
            return GroupBy(fld);
        }



        internal static DGroupBySet GroupBy(string owner, Type table, string field)
        {
            DField fld = DField.Field(owner, table, field);
            return GroupBy(fld);
        }


        internal static DGroupBySet GroupBy<TTable>(string owner,  string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return GroupBy(fld);
        }



        internal static DGroupBySet GroupBy(DClause clause)
        {
            DGroupBySet grp = GroupBy();
            grp.Groupings.Add(clause);
            grp.Last = clause;

            return grp;
        }

        #endregion

        //
        // interface implementations
        //

        #region DBReference IDBBoolean.And(DBReference dbref)

        DClause IDBoolean.And(DClause dbref)
        {
            return this.And(dbref);
        }

        #endregion

        #region DBReference IDBBoolean.Or(DBReference dbref) (NOT SUPPORTED)

        DClause IDBoolean.Or(DClause dbref)
        {
            throw new NotSupportedException("The OR operator cannot be applied to a select set");
        }

        #endregion

        #region DBClause IDBArregate.Aggregate(AggregateFunction func, DBClause dbref)

        DClause IDAggregate.Aggregate(AggregateFunction func, DClause dbref)
        {
            return this.Aggregate(func, dbref);
        }

        #endregion
    }
}
