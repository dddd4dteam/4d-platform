
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Reflection;

namespace DDDD.Core.Data.DA2.Query
{
    //[KnownType("GetIJoinableKnowTypes")]
    [DataContract]
    public partial class DTableSet : DExpressionSet, IDAlias, IDBoolean //internal //IDBJoinable,
    {
        
        //
        // properties
        //
        public static IEnumerable<Type> GetIJoinableKnowTypes(ICustomAttributeProvider provider)
        {
            try
            {                
                List<Type> knownTypes = new List<Type>();
                knownTypes.Add(typeof(DJoin));
                knownTypes.Add(typeof(DSubQuery));
                knownTypes.Add(typeof(DTable));
                knownTypes.Add(typeof(DTableSet));                
                return knownTypes;
            }
            catch (System.Exception ex)
            {
            	throw ex;    
            }
        }


        #region protected DBTable Root {get;set;}

        private DJoinable _root = null;
        
        [DataMember]
        public DJoinable Root
        {
            get { return _root; }
            set { _root = value; }
        }

        #endregion

        #region internal bool HasRoot {get;} //protected


        /// <summary>
        /// 
        /// </summary>        
        internal bool HasRoot 
        {
            get
            {
                return (this._root != null);
            }
        }

        #endregion

        //
        // SQL Statement builder methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DTableSet()
            {
                Root =  (this.Root != null) ? this.Root.CloneClause() as DJoinable : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null                 
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


        
        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._root != null)
                return ((DClause)this.Root).BuildStatement(builder);
            else
                return false;
        }

        #endregion

        //
        // Xml serialization methods
        //

        //#region protected override string XmlElementName

        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.From;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasRoot)
        //        ((DBClause)this.Root).WriteXml(writer, context);
        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    this.Root = (DBTable)context.Factory.Read(reader.Name, reader, context);
        //    return true;
        //}

        //#endregion


        //
        // Factory methods
        //

        #region public static DBTableSet From() + 3 overloads

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <returns></returns>
        public static DTableSet From()
        {
            DTableSet ts = new DTableSet();
            return ts;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roottable"></param>
        /// <returns></returns>
        public static DTableSet From(Type roottable)
        {
            DTable tref = DTable.Table(roottable);
            return From(tref);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public static DTableSet From<TTable>()
        {
            DTable tref = DTable.Table<TTable>();
            return From(tref);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="roottable"></param>
        /// <returns></returns>
        public static DTableSet From(string owner, Type roottable)
        {
            DTable tref = DTable.Table(owner, roottable);
            return From(tref);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static DTableSet From<TTable>(string owner)
        {
            DTable tref = DTable.Table<TTable>(owner);
            return From(tref);
        }



        public static DTableSet From(DTable roottable)
        {
            DTableSet set = new DTableSet();
            set.Root = roottable;
            set.Last = roottable;
            return set;
        }

        public static DTableSet From(DSubQuery subquery)
        {
            DTableSet set = new DTableSet();
            set.Root = subquery;
            set.Last = subquery;
            return set;
        }

        #endregion

        #region public DBTableSet InnerJoin(DBClause table) + 4 overloads

        //public DBTableSet InnerJoin(string table, string parentfield, string childfield)
        //{
        //    return this.InnerJoin(table, parentfield, Compare.Equals, childfield);
        //}


        public DTableSet InnerJoin(Type table, string parentfield, string childfield)
        {
            return this.InnerJoin(table, parentfield, Compare.Equals, childfield);
        }

        public DTableSet InnerJoin<TTable>( string parentfield, string childfield)
        {
            return this.InnerJoin<TTable>( parentfield, Compare.Equals, childfield);
        }




        public DTableSet InnerJoin(Type table, string parentfield, Compare op, string childfield)
        {
            DTable tref = DTable.Table(table);
            
            return this.InnerJoin(tref, DField.Field(parentfield), op, DField.Field(childfield));

        }

        public DTableSet InnerJoin<TTable>( string parentfield, Compare op, string childfield)
        {
            DTable tref = DTable.Table<TTable>();

            return this.InnerJoin(tref, DField.Field(parentfield), op, DField.Field(childfield));

        }




        public DTableSet InnerJoin(DTable table, DClause parent, Compare op, DClause child)
        {
            DComparison comp = DComparison.Compare(parent, op, child);

            if (this.Root is DTable)
                ((DTable)this.Root).InnerJoin(table, comp);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).InnerJoin(table, comp);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = table;

            return this;
        }

        public DTableSet InnerJoin(DTable table, DComparison compare)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).InnerJoin(table, compare);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).InnerJoin(table, compare);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = table;

            return this;
        }

        

        public DTableSet InnerJoin(DClause table)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).InnerJoin(table, null);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).InnerJoin(table, null);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = table;

            return this;
        }

        #endregion

        public DTableSet LeftJoin(DTable table)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).LeftJoin(table, null);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).LeftJoin(table, null);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = table;

            return this;
        }

        public DTableSet RightJoin(DTable table)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).RightJoin(table, null);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).RightJoin(table, null);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = table;

            return this;
        }

        public DTableSet LeftJoin(DSubQuery sub)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).LeftJoin(sub, null);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).LeftJoin(sub, null);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = sub;

            return this;
        }

        public DTableSet RightJoin(DSubQuery sub)
        {
            if (this.Root is DTable)
                ((DTable)this.Root).RightJoin(sub, null);
            else if (this.Root is DSubQuery)
                ((DSubQuery)this.Root).RightJoin(sub, null);
            else
                throw new InvalidOperationException("Current root does not support joins");

            this.Last = sub;

            return this;
        }

        #region public DBTableSet On(DBComparison comp) + 1 overload

        
        public DTableSet On(DClause parent, Compare comp, DClause child)
        {
            DComparison compare = DComparison.Compare(parent, comp, child);
            this.Last = this.Root.On(compare);
            return this;
        }

        //
        //DBTableSet On(DBComparison comp)
        //{
        //    this.Last = this.Root.On(comp);
        //    return this;
        //}

        #endregion

        //
        // Interface Implementations
        //

        #region IDBJoinable Members

        public override DClause On(DComparison comp)//IDBJoinable.
        {
            this.Last = this.Root.On(comp);
            return this;
            //return this.On(comp);
        }

        #endregion

        #region IDBAlias Members

        public void As(string aliasName)
        {
            if (this.Last is IDAlias)
                ((IDAlias)this.Last).As(aliasName);
            else
                throw new ArgumentException("The last statement does not support Alias names");
        }

        #endregion

        #region IDBBoolean Members

        public DClause And(DClause reference)
        {
            if (this.Last == null || !(this.Last is IDBoolean))
                throw new ArgumentException("The last statement does not support 'And' operations");

            this.Last = ((IDBoolean)this.Last).And(reference);
            return this;
        }

        public DClause Or(DClause reference)
        {
            if (this.Last == null || !(this.Last is IDBoolean))
                throw new ArgumentException("The last statement does not support 'Or' operations");

            this.Last = ((IDBoolean)this.Last).Or(reference);
            return this;
        }

        #endregion
    }
}
