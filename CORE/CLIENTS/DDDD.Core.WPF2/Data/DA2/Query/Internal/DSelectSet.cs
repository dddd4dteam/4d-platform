
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DSelectSet : DCalculableExpressionSet, IDAlias, IDBoolean, IDAggregate, ICloneable //internal
    {


        public DSelectSet()  {  }


        private DClauseList _results;

        //
        // properties
        //

        #region public DBClauseList Results {get;}

        [DataMember]
        public DClauseList Results
        {
            get 
            {
                if (_results == null)
                    _results = new DClauseList();
                return _results;
            }
            set { _results = value; }
        }

        #endregion

        #region protected bool HasResults
        
        /// <summary>
        /// 
        /// </summary>
        protected bool HasResults
        {
            get { return this._results != null && this._results.Count > 0; }
        }

        #endregion

        //
        // SQL Statement builder methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DSelectSet()
            {
                Results = (this.Results != null) ? this.Results.Clone() as DClauseList : null,                
                Last = (this.Last != null) ? this.Last.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        




        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._results == null || this._results.Count == 0)
                return false;
            else
                return this._results.BuildStatement(builder, true, true);
        }


        #endregion

        //
        // XML serialization methods
        //

        //#region protected override string XmlElementName {get;}

        ////protected override string XmlElementName
        ////{
        ////    get
        ////    {
        ////        return XmlHelper.Fields;
        ////    }
        ////}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasResults)
        //        this.Results.WriteXml(writer, context);

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    return this.Results.ReadXml(this.XmlElementName, reader, context);
        //}

        //#endregion

        ////
        // interface implementations
        //

        #region IDBAlias - public void As(string alias)

        public void As(string alias)
        {
            if (this.Last is IDAlias)
                ((IDAlias)this.Last).As(alias);
            else if (null == this.Last)
                throw new NullReferenceException("Cannot set an alias because there was no last item added in the query");
            else
                throw new NotSupportedException("Cannot alias a '" + this.Last.GetType().ToString() + "' item in the query");

        }

        #endregion

        #region IDBCalculable - protected override DBCalculableExpressionSet Calculate(BinaryOp op, DBClause dbref)

        protected override DCalculableExpressionSet Calculate(BinaryOp op, DClause dbref)
        {
            if (this.Last is IDCalculable)
            {
                this.Results.Remove(this.Last);
                this.Last = (DClause)((IDCalculable)this.Last).Calculate(op, dbref);
                this.Results.Add(this.Last);
                return this;
            }
            else
                throw new InvalidOperationException("The current clause does not support calculations");
        }

        #endregion

        #region IDBAggregate - public DBClause Aggregate(AggregateFunction func, DBClause dbref)

        public DClause Aggregate(AggregateFunction func, DClause dbref)
        {
            DAggregate agg = DAggregate.Aggregate(func, dbref);
            this.Last = agg;
            this.Results.Add(agg);
            return this;
        }



        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new DSelectSet()
            {
                 Last = this.Last.CloneClause() as DClause,
                 Results = this.Results.Clone() as DClauseList                
            };
        }


        #endregion

        #region public DBSelectSet And(string _ModuleInitializer)

        public DSelectSet And(string field)
        {
            DField fld = DField.Field(field);
            return this.And(fld);
        }

        #endregion

        #region public DBSelectSet And(DBClause dbref)

        public DSelectSet And(DClause dbref)
        {
            this.Results.Add(dbref);
            this.Last = dbref;
            return this;
        }

        #endregion

        #region DBClause IDBBoolean.And(DBClause dbref)

        DClause IDBoolean.And(DClause dbref)
        {
            return this.And(dbref);
        }

        #endregion

        #region DBClause IDBBoolean.Or(DBClause dbref) (NOT SUPPORTED)

        DClause IDBoolean.Or(DClause dbref)
        {
            throw new NotSupportedException("The OR operator cannot be applied to a select set");
        }

        #endregion

        //
        // static methods
        //

        #region  --------------------- Replacing METHODS -----------------------
        // Replacing Methods  - ���
        // ������ ��������� �������- �.�. �����, �� ������� ������ ��������- ���������� ������������ �������

        /// <summary>
        /// �������� ����������� ����� ������� [*] ��� ������ ������ ����� -  �� ����������� Count(*)- ������� ����� �������       
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public void ReplaceByCount()
        {            
            this.Results.Clear(); 

            DSelectSet newSelectSet = DSelectSet.SelectCount();

            this.Last = newSelectSet.Last;
            this.Results = newSelectSet.Results;
        }


        /// <summary>
        /// ������ ��������� ������� �� ��������� ������������� �������(����� count)  ��� ������-�� �������
        /// </summary>
        /// <param name="Column"></param>
        public void ReplaceByAggregate(AggregateFunction function, String Column)
        {
            if (Column == null) return;
            this.Results.Clear();

            DSelectSet newSelectSet = DSelectSet.SelectAggregate(function,Column);

            this.Last = newSelectSet.Last;
            this.Results = newSelectSet.Results;
        }



        #endregion --------------------- Replacing METHODS -----------------------






        #region public static DBSelectSet SelectCount() + 4 overloads

        public static DSelectSet SelectCount()
        {
            return SelectAggregate(AggregateFunction.Count, DField.AllFields());
        }

        public static DSelectSet SelectCount(string field)
        {
            return SelectAggregate(AggregateFunction.Count, field);
        }

        public static DSelectSet SelectCount(Type table, string field)
        {
            return SelectAggregate(AggregateFunction.Count, table, field);
        }

        public static DSelectSet SelectCount<TTable>( string field)
        {
            return SelectAggregate<TTable>(AggregateFunction.Count,  field);
        }


        public static DSelectSet SelectCount(string owner, Type table, string field)
        {
            return SelectAggregate(AggregateFunction.Count, owner, table, field);
        }

        public static DSelectSet SelectCount<TTable>(string owner, string field)
        {
            return SelectAggregate<TTable>(AggregateFunction.Count, owner,  field);
        }


        public static DSelectSet SelectCount(DClause fref)
        {
            return SelectAggregate(AggregateFunction.Count, fref);
        }

        #endregion

        #region public static DBSelectSet SelectAggregate(AggregateFunction funct, string _ModuleInitializer) + 3 overloads

        public static DSelectSet SelectAggregate(AggregateFunction funct, string field)
        {
            DField fref = DField.Field(field);

            return SelectAggregate(funct, fref);
        }

        public static DSelectSet SelectAggregate(AggregateFunction funct, string owner, Type table, string field)
        {
            DField fref = DField.Field(owner, table, field);
            return SelectAggregate(funct, fref);
        }

        public static DSelectSet SelectAggregate<TTable>(AggregateFunction funct, string owner, string field)
        {
            DField fref = DField.Field<TTable>(owner,  field);
            return SelectAggregate(funct, fref);
        }



        public static DSelectSet SelectAggregate(AggregateFunction funct, Type table, string field)
        {
            DField fref = DField.Field(table, field);
            return SelectAggregate(funct, fref);
        }

        public static DSelectSet SelectAggregate<TTable>(AggregateFunction funct,  string field)
        {
            DField fref = DField.Field<TTable>( field);
            return SelectAggregate(funct, fref);
        }


        public static DSelectSet SelectAggregate(AggregateFunction funct, DClause field)
        {
            DAggregate agg = DAggregate.Aggregate(funct, field);
            DSelectSet sel = DSelectSet.Select(agg);
            sel.Last = agg;
            return sel;
        }

        #endregion

        #region public static DBSelectSet SelectFields(params string[] fields)

        public static DSelectSet SelectFields(params string[] fields)
        {
            DSelectSet set = new DSelectSet();
            if (fields != null)
            {
                foreach (string fld in fields)
                {
                    DField fref = DField.Field(fld);
                    set.Results.Add(fref);
                    set.Last = fref;
                }
            }
            return set;
        }

        #endregion

        #region public static DBSelectSet SelectAll() + 2 overloads


        public static DSelectSet SelectAll()
        {
            DSelectSet set = new DSelectSet();
            DFieldAll fref = new DFieldAll();

            set.Results.Add(fref);
            set.Last = fref;

            return set;
        }


        public static DSelectSet SelectAll(string table)
        {
            DSelectSet set = new DSelectSet();
            DFieldAll fref = new DFieldAll();
            fref.Table = table;
            set.Results.Add(fref);
            set.Last = fref;

            return set;
        }
        

        public static DSelectSet SelectAll(string owner, Type table)
        {
            DField allRef = DField.AllFields(owner, table);
            return Select(allRef);
        }


        public static DSelectSet SelectAll<TTable>(string owner)
        {
            DField allRef = DField.AllFields<TTable>(owner);
            return Select(allRef);
        }
        
        #endregion

        #region public static DBSelectSet Select(DBClause fref) + 3 overloads

        public static DSelectSet Select()
        {
            DSelectSet set = new DSelectSet();
            return set;
        }

        public static DSelectSet Select(DClause fref)
        {
            DSelectSet set = Select();
            set.Results.Add(fref);
            set.Last = fref;
            return set;

        }

        public static DSelectSet Select(string field)
        {
            DField fref = DField.Field(field);
            return Select(fref);
        }

        public static DSelectSet Select(Type table, string field)
        {
            DField fref = DField.Field(table, field);
            return Select(fref);
        }

        public static DSelectSet Select<TTable>( string field)
        {
            DField fref = DField.Field<TTable>( field);
            return Select(fref);
        }


        public static DSelectSet Select(string owner, Type table, string field)
        {
            DField fref = DField.Field(owner, table, field);
            return Select(fref);
        }


        public static DSelectSet Select<TTable>(string owner, string field)
        {
            DField fref = DField.Field<TTable>(owner, field);
            return Select(fref);
        }

        #endregion

    }
}
