using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Abstract class for containing specific lists of 
    /// expressions such as a select list or order list
    /// </summary>
    [DataContract]
    public partial class DExpressionSet : DJoinable // abstract
    {
        public DExpressionSet()
        { }

        private DClause _last;

        #region public DBClause Last {get;set;}

        /// <summary>
        /// Gets or sets the last Clause used whilds contructing a statement
        /// </summary>
        [DataMember]
        public DClause Last
        {
            get { return _last; }
            set { _last = value; }
        }

        #endregion



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DExpressionSet()
            { 
              Last = (this.Last!= null) ? this.Last.CloneClause(): null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


        //#region protected override string XmlElementName

        ///// <summary>
        ///// overrides the base implemntation to return an empty element name
        ///// </summary>        
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.EmptyName; }
        //}

        //#endregion

    }

   
}
