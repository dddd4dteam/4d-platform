
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DOrderSet : DExpressionSet, IDBoolean //internal
    {

        public DOrderSet()
        {
           
        }


        private DOrderList _orders;

        //
        // properties
        //

        #region public DBOrderList OrderList {get;}

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DOrderList OrderList
        {
            get {
                if (_orders == null)
                    _orders = new DOrderList();
                return _orders; 
                }
            set { _orders = value; }
        }

        #endregion

        #region internal bool HasOrderBy {get;}


        /// <summary>
        /// 
        /// </summary>        
        internal bool HasOrderBy
        {
            get
            {                
                return (this._orders != null && this._orders.Count > 0);
            }            
        }

        #endregion

        //
        // SQL Statement builder
        //

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DOrderSet()
            {                 
                OrderList = (this.OrderList != null) ? this.OrderList.Clone() as DOrderList : null,                
                Last = (this.Last != null) ? this.Last.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


        #region public override bool BuildStatement(DBStatementBuilder builder)
        

        public override bool BuildStatement(DStatementBuilder builder)
        {

            if (this._orders == null || this._orders.Count == 0)
                return false;
            else
            {
                this.OrderList.BuildStatement(builder);
                return true;
            }
        }


        #endregion

        //
        // xml serialization
        //

        //#region protected override string XmlElementName

        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Order;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasOrderBy)
        //        this.OrderList.WriteXml(writer, context);

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    this.OrderList.ReadXml(reader.LocalName, reader, context);
        //    return this.OrderList.Count > 0;
        //}

        //#endregion

        ////
        // static factory methods
        //

        #region internal static DBOrderSet OrderBy() + 3 overloads

        internal static DOrderSet OrderBy()
        {
            return new DOrderSet();
        }

        internal static DOrderSet OrderBy(string field)
        {
            return OrderBy(field, Order.Default);
        }

        internal static DOrderSet OrderBy(DClause value)
        {
            return OrderBy(value, Order.Default);
        }

        internal static DOrderSet OrderBy(string field, Order order)
        {
            DField fld = DField.Field(field);
            return OrderBy(fld, order);
        }

        internal static DOrderSet OrderBy(DClause value, Order order)
        {
            DOrder oref = DOrder.OrderBy(order, value);
            
            DOrderSet set = new DOrderSet();
            set.OrderList.Add(oref);
            set.Last = oref;

            return set;
        }

        #endregion

        //
        // interface implementations
        // 

        #region IDBBoolean Members (OR not supported)

        
        public DClause And(DClause reference)
        {

            DOrder order;
            if(reference is DOrder)
                order = (DOrder)reference;
            else
                order = DOrder.OrderBy(Order.Default, reference);
            this.OrderList.Add(order);
            this.Last = order;
            return order;
        }

        DClause IDBoolean.Or(DClause reference)
        {
            throw new InvalidOperationException("Cannot operate the OR operation on an order set.");
        }

        #endregion
    }

}
