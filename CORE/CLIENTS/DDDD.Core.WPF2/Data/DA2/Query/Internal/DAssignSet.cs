using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DAssignSet : DExpressionSet //internal
    {

         
        //
        // .ctor(s)
        //
        public DAssignSet() { } // protected



        private DClauseList _assignments;

        //
        // properties
        //

        #region protected DBClauseList<DBAssignSet> Assignments {get;}

        /// <summary>
        /// Gets the list of Assignments for this assign set.
        /// </summary>
        [DataMember]
        public DClauseList Assignments
        {
            get
            {
                if (_assignments == null)
                    _assignments = new DClauseList();
                return _assignments;
            }
            set { _assignments = value; }
        }

        #endregion

        #region internal bool HasAssignments
                
        internal bool HasAssignments
        {
            get
            {   return (this._assignments != null && this._assignments.Count > 0); }            
        }

        #endregion

       
        //
        // statement construction methods
        //

        #region public DBAssignSet AndSet(DBClause item, DBClause toValue)

        public DAssignSet AndSet(DClause item, DClause toValue)
        {
            DAssign assign = DAssign.Set(item, toValue);
            return this.AndSet(assign);
        }

        #endregion

        #region internal DBAssignSet AndSet(DBClause assignment)

        internal DAssignSet AndSet(DClause assignment)
        {
            this.Last = assignment;
            this.Assignments.Add(assignment);
            return this;
        }

        #endregion

        //
        // SQL Statement Builder methods
        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._assignments == null || this._assignments.Count == 0)
                return false;
            else
            {
                builder.BeginSetValueList();
                this.Assignments.BuildStatement(builder);
                builder.EndSetValueList();
                return true;
            }
        }

        #endregion
        //
        // Xml Serialization methods
        //

        //#region protected override string XmlElementName

        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Assignments;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasAssignments)
        //        this.Assignments.WriteXml(writer, context);

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    return this.Assignments.ReadXml(reader.LocalName, reader, context);
        //}

        //#endregion

        //
        // static factory methods
        //

        #region internal static DBAssignSet Set(DBClause assignment) + 1 overload

        internal static DAssignSet Set(DClause item, DClause toValue)
        {
            DAssign assign = DAssign.Set(item, toValue);
            return Set(assign);
        }

        internal static DAssignSet Set(DClause assignment)
        {
            DAssignSet set = new DAssignSet();
            set.Last = assignment;
            set.Assignments.Add(assignment);
            return set;
        }

        #endregion

        #region internal static DBAssignSet Assign()

        internal static DAssignSet Assign()
        {
            DAssignSet set = new DAssignSet();
            return set;
        }

        #endregion

    }
}
