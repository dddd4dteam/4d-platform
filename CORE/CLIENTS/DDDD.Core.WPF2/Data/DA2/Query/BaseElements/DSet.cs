﻿ 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a SET x = [value] statement
    /// </summary>
    [DataContract]
    public partial class DSet : DStatement  //abstract
    {
        public DSet() { }
        
        private DAssign _assign;

        /// <summary>
        /// The Assignment that is set
        /// </summary>
        [DataMember]
        public DAssign Assignment
        {
            get { return this._assign; }
            set { this._assign = value; } //protected
        }

        /// <summary>
        /// Creates and returns a new DBSet assignment
        /// </summary>
        /// <param name="assign"></param>
        /// <returns></returns>
        public static DSet Set(DAssign assign)
        {
            DSet set = new DSet();
            set.Assignment = assign;

            return set;
        }



        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginSetStatement();
            this.Assignment.BuildStatement(builder);
            builder.EndSetStatement();
            return true;
        }
        



    }

    //[DataContract]
    //public partial class DBSetRef : DBSet
    //{
    //    public DBSetRef() { }
        
       

    //    //protected override string XmlElementName
    //    //{
    //    //    get
    //    //    {
    //    //        return XmlHelper.Set;
    //    //    }
    //    //}

    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.Assignment.WriteXml(writer, context);
    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{

    //    //    if (this.IsElementMatch(XmlHelper.Assign, reader, context))
    //    //    {
    //    //        this.Assignment = context.Factory.Read(XmlHelper.Assign, reader, context) as DBAssign;
    //    //        return this.Assignment != null;
    //    //    }
    //    //    else
    //    //        return base.ReadAnInnerElement(reader, context);
    //    //}

    //}
}
