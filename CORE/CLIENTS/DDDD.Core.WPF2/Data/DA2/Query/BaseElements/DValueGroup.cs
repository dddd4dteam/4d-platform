 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DValueGroup : DClause  //abstract
    {
        //
        // .ctors
        //
        #region public DBValueGroup()

        public DValueGroup() { }

        #endregion



        private DClauseList _items;

        //
        // properties
        //

        #region public DBClauseList GroupItems {get;set;}
        [DataMember]
        public DClauseList GroupItems
        {
            get 
            {
                return _items; 
            }
            set { _items = value; }
        }

        #endregion


        
        //
        // methods
        //

        #region public DBValueGroup Add(DBClause item)

        public DValueGroup Add(DClause item)
        {
            if (this.GroupItems == null)
                this.GroupItems = new DClauseList();

            this.GroupItems.Add(item);

            return this;
        }

        #endregion

        #region public DBValueGroup AddRange(IEnumerable<DBClause> items)

        public DValueGroup AddRange(IEnumerable<DClause> items)
        {
            if (this.GroupItems == null)
                this.GroupItems = new DClauseList();
            if (null != items)
            {
                foreach (DClause c in items)
                {
                    this.GroupItems.Add(c);
                }
            }

            return this;
        }

        #endregion

        //
        // static methods
        //




        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DValueGroup()
            { GroupItems = (GroupItems != null)? this.GroupItems.Clone() as DClauseList : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        #region public override bool BuildStatement(DBStatementBuilder builder)


        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginBlock();
            this.GroupItems.BuildStatement(builder);
            builder.EndBlock();
            return true;
        }

        #endregion




        #region public static DBValueGroup Empty()

        public static DValueGroup Empty()
        {
            DClauseList list = new DClauseList();
            
            DValueGroup values = new DValueGroup();
            values.GroupItems = null;

            return values;
        }

        #endregion

        #region public static DBValueGroup All(params DBClause[] items) + 3 overloads

        public static DValueGroup All(params DClause[] items)
        {
            DClauseList list = new DClauseList();
            list.AddRange(items);
            DValueGroup values = new DValueGroup();
            values.GroupItems = list;

            return values;
        }

        public static DValueGroup All(params int[] items)
        {
            DClause[] all = ConvertToClause(items);
            
            return All(all);
        }

        public static DValueGroup All(params string[] items)
        {
            DClause[] all = ConvertToClause(items);

            return All(all);
        }

        public static DValueGroup All(params Guid[] items)
        {
            DClause[] all = ConvertToClause(items);

            return All(all);
        }

        public static DValueGroup All(params double[] items)
        {
            DClause[] all = ConvertToClause(items);

            return All(all);
        }

        private static DClause[] ConvertToClause(Array items)
        {
            List<DClause> converted = new List<DClause>();
            foreach (object item in items)
            {
                converted.Add(DConst.Const(item));
            }
            return converted.ToArray();
        }

        #endregion
    }




    //[DataContract]
    //public partial class DBValueGroupRef : DBValueGroup
    //{
    //    public DBValueGroupRef() {}

    //    //
    //    // SQL Statement builder
    //    //


    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBValueGroupRef()
    //        {
    //            GroupItems = (GroupItems != null)? this.GroupItems.Clone() as DBClauseList : null
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
        

    //    //
    //    // XML Serializer
    //    //
    //    //#region protected override string XmlElementName {get;}

    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.ValueGroup; }
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.GroupItems != null)
    //    //    {
    //    //        this.GroupItems.WriteXml(writer, context);
    //    //    }
    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.GroupItems == null)
    //    //        this.GroupItems = new DBClauseList();

    //    //    DBClause c = context.Factory.Read(reader.Name, reader, context);

    //    //    if (null != c)
    //    //    {
    //    //        this.GroupItems.Add(c);
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = false;

    //    //    return b;
    //    //}

    //    //#endregion

    //}
}
