﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDD.Core.Data.DA2.Query
{
    public class DTokenListOptions
    {
        /// <summary>
        /// Gets the characters that should be appended to the list after being built
        /// </summary>
        public static  string ListEnd { get { return ""; } }

        /// <summary>
        /// Gets the characters that should prepend the list when built
        /// </summary>
        public static string ListStart { get { return ""; } }

        /// <summary>
        /// Gets the separator for each token when the statements are built
        /// </summary>
        public static  string TokenSeparator { get { return ", "; } }

        /// <summary>
        /// Gets the flag that identifies if this list should use the standard builder ReferenceSeparator
        /// </summary>
        public static  bool UseBuilderSeparator { get { return true; } }
    }
}
