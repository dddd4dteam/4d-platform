
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a join from one clause to another along with the comparison.
    /// e.g INNER JOIN Table2 ON Table1.column1 = Table2.column1
    /// </summary>
    [DataContract]
    //[InfrastructureServiceModel(InfrastructureService_RegisterEn.Query)]
    public partial class DJoin : DJoinable, IDAlias, IDBoolean//, IDBJoinable //abstract
    {
        /// DBClauseList< T : DBClause

        public DJoin() { }

        private DClause _fromcol;
        private DClause _comp;
        private JoinType _jtype;

        //
        // public properties
        //

        #region public JoinType JoinType {get; set;} //protected 

        /// <summary>
        /// Gets the JoinType - Inheritors can set this value
        /// </summary>
        [DataMember]
        public JoinType JoinType
        {
            get { return _jtype; }
            set { _jtype = value; } //protected 
        }

        #endregion

        #region public DBClause JoinedTo {get; protected set;}

        /// <summary>
        /// Gets or sets the clause this Join refers to
        /// </summary>
        [DataMember]
        public DClause JoinedTo
        {
            get { return _fromcol; }
            set { _fromcol = value; } //protected
        }

        #endregion

        #region public DBComparison Comparison {get; protected set;}

        /// <summary>
        /// Gets or sets the Comparison used to match the joined clause with the owner
        /// </summary>
        [DataMember]
        public DClause Comparison
        {
            get { return _comp; }
            set { _comp = value; }   //protected 
        }

        #endregion

        //
        // Interface Implementations
        //


        #region IDBAlias - public void As(string aliasName)
        /// <summary>
        /// Assigns an alias to this joins table
        /// </summary>
        /// <param name="aliasName"></param>
        public void As(string aliasName)
        {
            ((IDAlias)this.JoinedTo).As(aliasName);
        }

        #endregion

        #region IDBBoolean - public DBClause And(DBClause reference)
        /// <summary>
        /// Appends an AND condition to the comparison
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause And(DClause reference)
        {
            if (this.Comparison == null)
                throw new ArgumentNullException("No existing join comparision to 'AND' with");
            else
                this.Comparison = DBooleanOp.Compare(this.Comparison, BooleanOp.And, reference);
            return this;
        }

        #endregion

        #region IDBBoolean - public DBClause Or(DBClause reference)

        /// <summary>
        /// Appends an OR condition to the joins comparison
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause Or(DClause reference)
        {
            if (this.Comparison == null)
                throw new ArgumentNullException("No existing join comparision to 'AND' with");
            else
                this.Comparison = DBooleanOp.Compare(this.Comparison, BooleanOp.Or, reference);
            return this;
        }

        #endregion

        #region IDBJoinable - public DBClause On(DBComparison compare)
        /// <summary>
        /// Appends or sets the condition to this joins comparison
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public override DClause On(DComparison compare)
        {
            if (this.Comparison != null)
                return this.And(compare);
            else
                this._comp = compare;
            return this;
        } //internal

        #endregion

        

        //
        // static methods
        //
        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DJoin()
            { 
               Comparison = this.Comparison,
               JoinedTo = (this.JoinedTo != null) ? this.JoinedTo.CloneClause() : null,
               JoinType = this.JoinType
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        



        #region public static DBJoin InnerJoin(DBReference jointo, DBFieldRef from, DBFieldRef to, ComparisonOperator comparison)
        /// <summary>
        /// Creates a new INNER JOIN with the ON set to the comparison of from and to
        /// </summary>
        /// <param name="jointo"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="comparison"></param>
        /// <returns></returns>
        public static DJoin InnerJoin(DClause jointo, DClause from, DClause to, Compare comparison)
        {
            DComparison compref = DComparison.Compare(from, comparison, to);

            return InnerJoin(jointo, compref);
        }

        #endregion

        #region public static DBJoin InnerJoin(DBReference jointo, DBComparison comp)
        /// <summary>
        /// Creates a new INNER JOIN with the ON set to the comparison
        /// </summary>
        /// <param name="jointo"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static DJoin InnerJoin(DClause jointo, DComparison comp)
        {
            return Join(jointo, JoinType.InnerJoin, comp);
        }

        #endregion


        #region public static DBJoin Join(DBReference jointo, JoinType joinType, DBComparison comp)

        /// <summary>
        /// Creates a new join of the specified type and the ON set to the specified comparison
        /// </summary>
        /// <param name="jointo"></param>
        /// <param name="joinType"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static DJoin Join(DClause jointo, JoinType joinType, DComparison comp)
        {
            DJoin jref = new DJoin();
            jref.JoinType = joinType;
            jref.JoinedTo = jointo;
            jref.Comparison = comp;

            return jref;
        }

        #endregion

        #region public static DBJoin Join(DBClause jointo, JoinType joinType)

        /// <summary>
        /// Creates a new JOIN of the specified type with no comparison
        /// </summary>
        /// <param name="jointo"></param>
        /// <param name="joinType"></param>
        /// <returns></returns>
        public static DJoin Join(DClause jointo, JoinType joinType)
        {
            DJoin join = new DJoin();
            join.JoinedTo = jointo;
            join.JoinType = joinType;
            return join;
        }

        #endregion

        #region internal static DBJoin Join()
        /// <summary>
        /// Creates a new empty join
        /// </summary>
        /// <returns></returns>
        internal static DJoin Join()
        {
            return new DJoin();
        }

        #endregion





        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (null == this.JoinedTo)
                throw new NullReferenceException("No Join Right Reference");
            if (null == this.Comparison)
                throw new NullReferenceException("No Join Limits");

            builder.BeginJoin(this.JoinType);
            this.JoinedTo.BuildStatement(builder);
            builder.BeginJoinOnList();
            this.Comparison.BuildStatement(builder);
            builder.EndJoinOnList();
            builder.EndJoin(this.JoinType);
            return true;
        }

        #endregion
        
    }




    //[DataContract]
    ////[InfrastructureServiceModel(InfrastructureService_RegisterEn.Query)]
    //public partial class DBJoinRef : DBJoin
    //{
    //    public DBJoinRef() { }
    //    //
    //    // properties
    //    //

    //    //
    //    // SQL Statement build methods
    //    //

    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBJoinRef()
    //        { 
    //            Comparison = this.Comparison,
    //            JoinedTo = (this.JoinedTo != null) ? this.JoinedTo.CloneClause() : null,
    //            JoinType = this.JoinType
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


    //    //
    //    // Xml serialization
    //    //

    //    //#region protected override string XmlElementName {get;}

    //    ///// <summary>
    //    ///// Overrides the abstract base method to return the Join element name
    //    ///// </summary>
    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Join; }
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.WriteAttribute(writer, XmlHelper.JoinType, this.JoinType.ToString(), context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.JoinedTo != null)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.JoinTo, writer, context);
    //    //        this.JoinedTo.WriteXml(writer, context);
    //    //        this.WriteEndElement(XmlHelper.JoinTo, writer, context);
    //    //    }

    //    //    if (this.Comparison != null)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.JoinOn, writer, context);
    //    //        this.Comparison.WriteXml(writer, context);
    //    //        this.WriteEndElement(XmlHelper.JoinOn, writer, context);
    //    //    }

    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    if (IsAttributeMatch(XmlHelper.JoinType, reader, context))
    //    //    {
    //    //        string value = reader.Value;
    //    //        if (string.IsNullOrEmpty(value) == false && Array.IndexOf<string>(Enum.GetNames(typeof(JoinType)),value) > -1)
    //    //        {
    //    //            this.JoinType = (JoinType)Enum.Parse(typeof(JoinType), value);
    //    //            return true;
    //    //        }
    //    //    }
    //    //    return base.ReadAnAttribute(reader, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (IsElementMatch(XmlHelper.JoinTo, reader, context) && reader.IsEmptyElement == false
    //    //        && reader.Read())
    //    //    {
    //    //        this.JoinedTo = this.ReadNextInnerClause(XmlHelper.JoinTo, reader, context);
    //    //        b = true;
    //    //    }
    //    //    else if (IsElementMatch(XmlHelper.JoinOn, reader, context) && reader.IsEmptyElement == false && reader.Read())
    //    //    {
    //    //        this.Comparison = this.ReadNextInnerClause(XmlHelper.JoinOn, reader, context);
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = base.ReadAnInnerElement(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion


    //}






    [CollectionDataContract]
    //[InfrastructureServiceModel(InfrastructureService_RegisterEn.Query)]
    public partial class DJoinList : List<DJoin>, ICloneable //internal
    {
        public DJoinList() { }


        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DJoinList newJoinList = new DJoinList();

            foreach (var item in this)
            { newJoinList.Add(item.CloneClause() as DJoin); }

            return newJoinList;
        }



        //public bool BuildStatement(DBStatementBuilder builder) // override
        //{
        //    if (this.Count > 0)
        //    {
                
        //        for (int i = 0; i < this.Count; i++)
        //        {
        //            this[i].BuildStatement(builder); //, false, false
        //        }

        //        return true;
        //    }
        //    else
        //        return false;
        //}
        
        /// <summary>
        /// Builds this list of tokens onto the statement
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        internal virtual bool BuildStatement(DStatementBuilder builder)
        {
            return this.BuildStatement(builder, false, false);
        }

        /// <summary>
        /// Builds the list of tokens 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="eachOnNewLine"></param>
        /// <param name="indent"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder, bool eachOnNewLine, bool indent)
        {
            int count = 0;

            bool outputSeparator = false; //initially false

            string start = DTokenListOptions.ListStart;
            if (!string.IsNullOrEmpty(start))
                builder.WriteRaw(DTokenListOptions.ListStart);

            for (int i = 0; i < this.Count; i++)
            {
                DToken clause = this[i];

                if (outputSeparator)
                {
                    if (DTokenListOptions.UseBuilderSeparator)
                        builder.AppendReferenceSeparator();

                    else if (!string.IsNullOrEmpty(DTokenListOptions.TokenSeparator))
                        builder.WriteRaw(DTokenListOptions.TokenSeparator);

                    if (eachOnNewLine)
                        builder.BeginNewLine();
                }

                //if the clause outputs then it should return true - so the next item has a separator written
                outputSeparator = clause.BuildStatement(builder);

                if (outputSeparator) //if we were output then increment the count
                    count++;
            }

            string end = DTokenListOptions.ListEnd;
            if (!string.IsNullOrEmpty(end))
                builder.WriteRaw(end);

            //did we write anything
            if (count > 0 || !string.IsNullOrEmpty(start) || !string.IsNullOrEmpty(end))
                return true;
            else
                return false;

        }


      

      
        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);

        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {


        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add((DBJoin)c); // added DBJoin 

        //            if (isEmpty)
        //                return true;
        //        }

        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());

        //    return true;

        //}

        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //} 




    }
}
