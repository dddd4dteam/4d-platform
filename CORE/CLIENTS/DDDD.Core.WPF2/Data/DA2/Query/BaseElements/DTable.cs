
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a table reference in a DBQuery
    /// </summary>
    [DataContract]
    public partial class DTable : DJoinable, IDAlias, IDBoolean //abstract//IDBJoinable,
    {
        //
        // .ctor(s)
        //
        #region public   DBTable()  //protected

        public DTable() { }

        #endregion

        //
        // public properties
        //
        #region ------------- public Table -----------------

        Type _TableType = null;

        /// <summary>
        ///��� �������/������ �� ������� ����� �������� ������
        /// ����������� ��� 
        /// </summary>
        public Type TableType
        {
            get { return _TableType; }
            protected set
            {
                _TableType = value;
                TableTypeName = (value != null) ? value.FullName : null;
                Name = (value != null) ? value.Name : null;
            }
        }

        /// <summary>
        /// �������� ���� ��������� �������/��� ��������� - � ��������� ����������, �� ��� ������
        /// ������ ���������� �� ���� �������
        /// </summary>
        [DataMember]
        public String TableTypeName { get; set; }


        /// <summary>
        /// Gets or sets the optional name of the table / view this _ModuleInitializer is a column on
        /// </summary>
        [DataMember]
        public String Name { get; set; }


        #endregion  -------------------  public Table  -----------------

        #region public string Owner {get;set;}

        private string _owner;

        /// <summary>
        /// Gets or sets the schema owner of the table
        /// </summary>
        [DataMember]
        public string Owner
        {
            get { return _owner; }
            set { _owner = value; } //protected 
        }

        #endregion

        #region public DBJoinList Joins {get;}

        private DJoinList _joins;
        /// <summary>
        /// Gets the list of JOINS on this table
        /// </summary>
        [DataMember]
        public DJoinList Joins //internal
        {
            get
            {
                if (this._joins == null)
                    this._joins = new DJoinList();
                return _joins;
            }
            set { _joins = value; }
        }

        #endregion

        #region public string Alias {get; set;}

        private string _alias;

        /// <summary>
        /// Gets or sets the Alias name of this table
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        #endregion

        #region internal bool HasJoins

        /// <summary>
        /// Returns true if this table reference has joins
        /// </summary>        
        internal bool HasJoins
        {
            get
            {
                return (this._joins != null && this._joins.Count > 0);
            }
        }

        #endregion


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DTable()
            {
                Joins = (this.Joins != null) ? this.Joins.Clone() as DJoinList : null,
                Alias = this.Alias,
                Owner = this.Owner,
                TableType = this.TableType,
                TableTypeName = this.TableTypeName,
                Name = this.Name,
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------





        //
        // instance methods
        //
        #region public DBJoin InnerJoin(string table, string parentfield, string childfield) + 2 overloads

        /// <summary>
        /// Appends a new INNER JOIN between this table an the specified table matching between this tables parentfield and the other tables child _ModuleInitializer
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentfield"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        //public DBJoin InnerJoin(string table, string parentfield, string childfield)
        //{
        //    DBTable tbl = Table(table);
        //    DBField parent = DBField.Field(this.Owner, this.Name, parentfield);
        //    DBField child = DBField.Field(table, parentfield);

        //    return InnerJoin(tbl, parent, child);
        //}

        /// <summary>
        /// Appends a new INNER JOIN between this table an the specified table matching between this tables parentfield and the other tables child _ModuleInitializer
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="parentfield"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DJoin InnerJoin(Type TTable, string parentfield, string childfield)
        {
            DTable tbl = Table(TTable);
            DField parent = DField.Field(this.Owner, this.TableType, parentfield);
            DField child = DField.Field(TTable, parentfield);

            return InnerJoin(tbl, parent, child);
        }

        /// <summary>
        ///  Appends a new INNER JOIN between this table an the specified table matching between this tables parentfield and the other tables child _ModuleInitializer
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="parentfield"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DJoin InnerJoin<TTable>(string parentfield, string childfield)
        {
            DTable tbl = Table(typeof(TTable));
            DField parent = DField.Field(this.Owner, this.TableType, parentfield);
            DField child = DField.Field(typeof(TTable), parentfield);

            return InnerJoin(tbl, parent, child);
        }




        /// <summary>
        /// Appends a new INNER JOIN between this table an the specified table matching between this tables parentfield and the other tables child _ModuleInitializer
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentField"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DJoin InnerJoin(DTable table, DClause parentField, DClause childField)
        {
            DJoin join = DJoin.InnerJoin(table, parentField, childField, Compare.Equals);
            this.Joins.Add(join);
            return join;
        }

        /// <summary>
        /// Appends a new INNER JOIN between this table an the specified clause matching with the specified comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin InnerJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.InnerJoin(foreign, compare);
            this.Joins.Add(join);
            return join;
        }

        #endregion


        /// <summary>
        /// Appends a new LEFT OUTER JOIN between this table and the specified table matching between this tables parentfield and the other tables child _ModuleInitializer
        /// </summary>
        /// <param name="tbl"></param>
        /// <param name="parent"></param>
        /// <param name="comp"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public DJoin LeftJoin(DTable tbl, DClause parent, Compare comp, DClause child)
        {
            DComparison c = DComparison.Compare(parent, comp, child);
            return LeftJoin(tbl, c);
        }


        /// <summary>
        /// Appends a new LEFT OUTER JOIN between this table an the specified clause matching on the comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin LeftJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.Join(foreign, JoinType.LeftOuter, compare);
            this.Joins.Add(join);
            return join;
        }


        /// <summary>
        /// Appends a new RIGHT OUTER JOIN between this table an the specified clause matching on the comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin RightJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.Join(foreign, JoinType.RightOuter, compare);
            this.Joins.Add(join);
            return join;
        }


        #region public DBJoin Join(DBClause table, JoinType type, DBComparison comp)

        /// <summary>
        /// Appends a new [type] JOIN between this table an the specified clause matching on the comparison
        /// </summary>
        /// <param name="table"></param>
        /// <param name="type"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public DJoin Join(DClause table, JoinType type, DComparison comp)
        {
            DJoin join = DJoin.Join(table, type, comp);
            this.Joins.Add(join);

            return join;
        }

        #endregion

        //
        // static factory methods
        //
        #region public static DBTable Table(string name)

        public static DTable Table()
        {
            DTable tref = new DTable();
            return tref;
        }

        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        //public static DBTable Table(string name)
        //{
        //    if (string.IsNullOrEmpty(name))
        //        throw new ArgumentNullException("name");

        //    DBTableRef tref = new DBTableRef();
        //    tref.Name = name;
        //    return tref;
        //}

        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed name
        /// </summary>
        /// <param name="Tname"></param>
        /// <returns></returns>
        public static DTable Table(Type Tname)
        {
            if (string.IsNullOrEmpty(Tname.Name))
                throw new ArgumentNullException("Tname");


            DTable tref = new DTable();
            tref.TableType = Tname;
            //tref.Name = Tname.Name;
            return tref;
        }


        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed name
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="Tname"></param>
        /// <returns></returns>
        public static DTable Table<TTable>()
        {
            if (string.IsNullOrEmpty(typeof(TTable).Name))
                throw new ArgumentNullException("Tname");

            DTable tref = new DTable();
            tref.TableType = typeof(TTable);
            //tref.Name = typeof(TTable).Name;
            return tref;
        }


        #endregion



        #region public static DBTable Table(string owner, string name)

        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed schema owner name
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        //public static DBTable Table(string owner, string name)
        //{
        //    if (string.IsNullOrEmpty(name))
        //        throw new ArgumentNullException("name");

        //    DBTableRef tref = new DBTableRef();
        //    tref.Name = name;
        //    tref.Owner = owner;
        //    return tref;
        //}


        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed schema owner name
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="Tname"></param>
        /// <returns></returns>
        public static DTable Table(string owner, Type Tname)
        {
            if (string.IsNullOrEmpty(Tname.Name))
                throw new ArgumentNullException("Tname");

            DTable tref = new DTable();
            //tref.Name = Tname.Name;
            tref.Owner = owner;
            tref.TableType = Tname;
            return tref;
        }

        /// <summary>
        /// Creates and returns a new DBTable reference with the specifed schema owner name
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="Tname"></param>
        /// <returns></returns>
        public static DTable Table<TTable>(string owner)
        {
            if (string.IsNullOrEmpty(typeof(TTable).Name))
                throw new ArgumentNullException("Tname");

            DTable tref = new DTable();
            //tref.Name = typeof(TTable).Name;
            tref.Owner = owner;
            tref.TableType = typeof(TTable);
            return tref;
        }

        #endregion



        //#region public static DBTable Table()
        ///// <summary>
        ///// Creates and returns a new DBTable reference with the specifed name
        ///// </summary>
        ///// <returns></returns>
        //public static DBTable Table()
        //{
        //    DBTableRef tref = new DBTableRef();
        //    tref.Name = string.Empty;
        //    return tref;
        //}

        //#endregion

        //
        // Interface implementations
        //

        #region IDBAlias Members

        /// <summary>
        /// Specifies the alias name for this table
        /// </summary>
        /// <param name="aliasName"></param>
        public void As(string aliasName)
        {
            this._alias = aliasName;
        }

        #endregion

        #region IDBJoinable Members

        /// <summary>
        /// Adds a comparison to the last join for this table
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public override DClause On(DComparison compare)
        {
            if (this.HasJoins == false)
                throw new InvalidOperationException("No joined tables or sub queries to join to");

            DJoinable join = (DJoinable)this.Joins[this.Joins.Count - 1];
            join.On(compare);
            return (DClause)join;
        }

        #endregion

        #region IDBBoolean Members

        /// <summary>
        /// Adds an AND comparison to the last join
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause And(DClause reference)
        {
            if (this.HasJoins == false)
                throw new InvalidOperationException("No joined tables or sub queries to join to");

            IDBoolean join = (IDBoolean)this.Joins[this.Joins.Count - 1];
            join.And(reference);
            return (DClause)join;
        }

        /// <summary>
        /// Adds an OR comparison to the last join
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause Or(DClause reference)
        {
            if (this.HasJoins == false)
                throw new InvalidOperationException("No joined tables or sub queries to join to");

            IDBoolean join = (IDBoolean)this.Joins[this.Joins.Count - 1];
            join.Or(reference);
            return (DClause)join;
        }

        #endregion




        #region public override bool BuildStatement(DBStatementBuilder builder)


        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (string.IsNullOrEmpty(this.Name))
                return false;

            builder.WriteSourceTable(this.Owner, this.Name, this.Alias);

            if (this.HasJoins)
                this.Joins.BuildStatement(builder);

            return true;

        }

        #endregion



    }

    //[DataContract]
    //public partial class DBTableRef : DBTable
    //{
    //    public DBTableRef() { }

    //    //
    //    // SQL Statement
    //    //


    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBTableRef()
    //        {
    //            Joins = (this.Joins != null) ? this.Joins.Clone() as DBJoinList : null,
    //            Alias = this.Alias,
    //            Owner = this.Owner,
    //            TableType = this.TableType,
    //            TableTypeName = this.TableTypeName,
    //            Name = this.Name,
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------




    //    //
    //    // XML Serialization
    //    //
    //    //#region protected override string XmlElementName {get;}

    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Table; }
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsAttributeMatch(XmlHelper.Owner, reader, context))
    //    //    {
    //    //        this.Owner = reader.Value;
    //    //        b = true;
    //    //    }
    //    //    else if (this.IsAttributeMatch(XmlHelper.Name, reader, context))
    //    //    {
    //    //        this.Name = reader.Value;
    //    //        b = true;
    //    //    }
    //    //    else if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
    //    //    {
    //    //        this.Alias = reader.Value;
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsElementMatch(XmlHelper.JoinList, reader, context) && !reader.IsEmptyElement && reader.Read())
    //    //    {
    //    //        this.Joins.ReadXml(XmlHelper.JoinList, reader, context);
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = base.ReadAnInnerElement(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (string.IsNullOrEmpty(this.Owner) == false)
    //    //        this.WriteAttribute(writer, XmlHelper.Owner, this.Owner, context);
    //    //    if (string.IsNullOrEmpty(this.Name) == false)
    //    //        this.WriteAttribute(writer, XmlHelper.Name, this.Name, context);

    //    //    if (string.IsNullOrEmpty(this.Alias) == false)
    //    //        this.WriteAlias(writer, this.Alias, context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.Joins != null && this.Joins.Count > 0)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.JoinList, writer, context);
    //    //        this.Joins.WriteXml(writer, context);
    //    //        this.WriteEndElement(XmlHelper.JoinList, writer, context);
    //    //    }
    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //}
}
