﻿using System.Runtime.Serialization;


namespace DDDD.Core.Data.DA2.Query
{

    /// <summary>
    /// Abstract base class for statements - exist independently as an executable block
    /// </summary>
    [DataContract]
    public partial class DCustomStatement : DClause  //abstract
    {

        public DCustomStatement() { }

        #region  --------------BEGIN  FIELDS ------------------- 

        protected StatementsEn _statement;

        protected DClause _startParam;

        protected DClause _middleParam;

        protected DClause _endParam;

        #endregion  --------------END  FIELDS -------------------

        #region ------------ public DBClauseList Parameters {get;} + bool HasParameters {get;}

        

        /// <summary>
        /// Ключ самого выражения
        /// </summary>         
        [DataMember]
        public StatementsEn StatementKey
        {
            get { return _statement; }
            set { _statement = value; }
        }

        /// <summary>
        /// Параметр в начале выражения - условное обозначение
        /// </summary>
        [DataMember]
        public DClause StartParameter
        {
            get
            { return _startParam;  }
            set { _startParam = value; }
        }
        
        /// <summary>
        /// Параметр посередине выражения - -условное название
        /// </summary>
        [DataMember]
        public DClause MiddleParameter
        {
            get
            { return _middleParam; }
            set { _middleParam = value; }
        }

        /// <summary>
        /// Параметр Завершения-условное название
        /// </summary>
        [DataMember]
        public DClause EndParameter
        {
            get
            { return _endParam; }
            set { _endParam = value; }
        }

        /// <summary>
        /// Returns true if this function has one or more parameters
        /// </summary>
        internal bool HasParameters
        {
            get { return (this._startParam != null || this._middleParam != null || this._endParam != null); }
        }





   

        #endregion  ------------ public DBClauseList Parameters {get;} + bool HasParameters {get;}


        /// <summary>
        /// Statement can be Builded from one/two /three DBClauses; if you need only one Clause(it will be first clause param ) then others DBClauses will be null; custom Statement Have to process needed parameters, and if there is no needed params count throw Exception message    
        /// </summary>
        /// <param name="statementKey"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DCustomStatement Statement(StatementsEn statementKey, DClause StartParameter, DClause MiddleParameter, DClause EndParameter)
        {
            DCustomStatement st = new DCustomStatement();

            st._statement = statementKey;
            
            if ( StartParameter != null) { st._startParam = StartParameter; }
            if ( MiddleParameter != null) { st._middleParam = MiddleParameter; }
            if ( EndParameter != null) { st._endParam = EndParameter; }
            
            return st;
        }



        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginCustomStatement(this._statement, StartParameter);

            builder.MiddleCustomStatement(this._statement, MiddleParameter);

            builder.EndCustomStatement(_statement, EndParameter);

            return true;
        }

        #endregion


        

    }
    


    ///// <summary>
    ///// 
    ///// </summary>
    //[DataContract]
    //public partial class DBCustomStatementRef : DBCustomStatement
    //{
    //    public DBCustomStatementRef() { }

    //    //
    //    // properties
    //    //

    //    //
    //    // SQL Statement builder
    //    // 

    //    //
    //    // XML serialization
    //    //
    //    //#region protected override string XmlElementName {get;}

    //    ///// <summary>
    //    ///// Gets the name of the xml element for this Function
    //    ///// </summary>
    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Function; }
    //    //}

    //    //#endregion


    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    ////protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    ////{
    //    ////    if (this.KnownFunction != Core.Data.Function.Unknown)
    //    ////        this.WriteAttribute(writer, XmlHelper.KnownFunction, this.KnownFunction.ToString(), context);
    //    ////    else if (string.IsNullOrEmpty(this.FunctionName) == false)
    //    ////        this.WriteAttribute(writer, XmlHelper.FunctionName, this.FunctionName, context);

    //    ////    if (string.IsNullOrEmpty(this.Alias) == false)
    //    ////        this.WriteAlias(writer, this.Alias, context);

    //    ////    return base.WriteAllAttributes(writer, context);
    //    ////}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    ////protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    ////{
    //    ////    if (this.HasParameters)
    //    ////    {
    //    ////        this.WriteStartElement(XmlHelper.FunctionParameter, writer, context);

    //    ////        foreach (DBClause c in this.Parameters)
    //    ////        {
    //    ////            c.WriteXml(writer, context);
    //    ////        }

    //    ////        this.WriteEndElement(XmlHelper.FunctionParameter, writer, context);
    //    ////    }

    //    ////    return base.WriteInnerElements(writer, context);
    //    ////}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    ////protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    ////{
    //    ////    bool b = true;
    //    ////    if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
    //    ////        this.Alias = reader.Value;
    //    ////    else if (this.IsAttributeMatch(XmlHelper.KnownFunction, reader, context))
    //    ////        this.KnownFunction = (Function)Enum.Parse(typeof(Function), reader.Value);
    //    ////    else if (this.IsAttributeMatch(XmlHelper.FunctionName, reader, context))
    //    ////        this.FunctionName = reader.Value;
    //    ////    else
    //    ////        b = base.ReadAnAttribute(reader, context);

    //    ////    return b;
    //    ////}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsElementMatch(XmlHelper.FunctionParameter, reader, context) && !reader.IsEmptyElement && reader.Read())
    //    //    {
    //    //        b = this.Parameters.ReadXml(XmlHelper.FunctionParameter, reader, context);
    //    //    }
    //    //    else
    //    //        b = base.ReadAnInnerElement(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //}

}
