﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Represents a DECLARE statement in a script
    /// </summary>
    [DataContract]
    public partial class DDeclaration : DStatement //abstract 
    {
        public DDeclaration() { }

        private DParam _param;

        /// <summary>
        /// Gets the Parameter reference this instance is declaring
        /// </summary>
        [DataMember]
        public DParam Parameter 
        { 
            get { return _param; }
            set { _param = value; } //private
        }

        /// <summary>
        /// Creates and returns a new declaration of the parameter
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static DDeclaration Declare(DParam param)
        {
            DDelcarationRef dref = new DDelcarationRef();
            dref.Parameter = param;
            return dref;
        }
    }

    [DataContract]
    public partial class DDelcarationRef : DDeclaration
    {
        
        public DDelcarationRef() { } 

        

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginDeclareStatement(this.Parameter);
            builder.EndDeclareStatement();
            return true;
        }
        
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Declare; }
        //}

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    bool b = base.WriteInnerElements(writer, context);
        //    this.Parameter.WriteXml(writer, context);
        //    return b;
        //}

    }


}
