﻿ 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Abstract base class for statements - exist independently as an executable block
    /// </summary>
    [DataContract]
    public partial class DStatement : DClause //abstract
    {
        public DStatement() { }
        

        #region  ------------------ IClonableClause.CloneClause -----------------------
        
        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DStatement();
        }
        
        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


    }
}
