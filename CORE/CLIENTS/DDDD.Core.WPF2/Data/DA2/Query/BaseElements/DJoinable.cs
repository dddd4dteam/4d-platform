﻿using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    //[InfrastructureServiceModel(InfrastructureService_RegisterEn.Query)]
    public partial class DJoinable : DClause
    {
        /// <summary>
        /// Sets the join on filter for this instance based on the passed comparison.
        /// </summary>
        public  virtual DClause On(DComparison compare)
        {
            return null;
        }




        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DJoinable();
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        
    }
}
