
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines an aggregate function to be generated for a clause
    /// </summary>
    [DataContract]
    public partial class DAggregate : DCalculableClause, IDAlias   //abstract
    {

        public DAggregate() { }

        //
        // public properties
        //
        #region public AggregateFunction Function {get;set;}

        private AggregateFunction _func;
        /// <summary>
        /// Gets or sets the aggregate function for this instance
        /// </summary>
        [DataMember]
        public AggregateFunction Function
        {
            get { return _func; }
            set { _func = value; }
        }

        #endregion

        #region public DBClause InnerReference {get;set;}

        private DClause _innerref;
        /// <summary>
        /// Gets or sets the inner clause for the aggregation
        /// </summary>
        [DataMember]
        public DClause InnerReference
        {
            get { return _innerref; }
            set { _innerref = value; }
        }

        #endregion

        #region public string Alias {get;set;}

        private string _alias;

        /// <summary>
        /// Gets the alias name for the resultant clause
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; } // protected
        }

        #endregion


        //
        // static factory methods
        //

        #region public static DBAggregate Count(DBClause reference)
        /// <summary>
        /// Creates a new Count aggregation for this passed clause.
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static DAggregate Count(DClause reference)
        {
            return Aggregate(AggregateFunction.Count, reference);
        }

        #endregion

        #region public static DBAggregate CountAll()

        /// <summary>
        /// Creates a new Count(*) aggregation
        /// </summary>
        /// <returns></returns>
        public static DAggregate CountAll()
        {
            
            return Aggregate(AggregateFunction.Count, DField.AllFields());
        }

        #endregion

        #region public static DBAggregate Count(string _ModuleInitializer)
        /// <summary>
        /// Creates a new Count('_ModuleInitializer') aggregation
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public static DAggregate Count(string field)
        {
            return Aggregate(AggregateFunction.Count, DField.Field(field));
        }

        #endregion



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DAggregate()
            { 
                Alias = this.Alias,
                Function = this.Function,
                InnerReference = (this.InnerReference != null)? InnerReference.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
                


        #region public static DBAggregate Aggregate(AggregateFunction func, DBClause reference)
        /// <summary>
        /// Creates a new Aggregation for the provided clause.
        /// </summary>
        /// <param name="func"></param>
        /// <param name="reference"></param>
        /// <returns></returns>
        public static DAggregate Aggregate(AggregateFunction func, DClause reference)
        {
            if (null == reference)
                throw new ArgumentNullException("reference");

            DAggregate cref = new DAggregate();
            cref.Function = func;
            cref.InnerReference = reference;
            return cref;
        }

        #endregion

        #region internal static DBAggregate Aggregate()

        internal static DAggregate Aggregate()
        {
            return new DAggregate();
        }

        #endregion


        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginAggregateFunction(this.Function, this.Function.ToString());
            builder.BeginFunctionParameterList();

            if (this.InnerReference != null)
                this.InnerReference.BuildStatement(builder);

            builder.EndFunctionParameterList();
            builder.EndAggregateFunction(this.Function, this.Function.ToString());

            if (string.IsNullOrEmpty(this.Alias) == false)
            {
                builder.WriteAlias(this.Alias);
            }
            return true;
        }

        #endregion



        public void As(string aliasName)
        {
            Alias = aliasName;
        }
    }



    //[DataContract]
    //public partial class DBAggregateRef : DBAggregate
    //{
    //    public DBAggregateRef() { }



    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBAggregate()
    //        {
    //            Alias = this.Alias,
    //            Function = this.Function,
    //            InnerReference = (this.InnerReference != null) ? InnerReference.CloneClause() : null
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
                

    //    //
    //    // SQL Statement building
    //    //

    //    //
    //    // XML Serialization
    //    //

    //    //#region protected override string XmlElementName {get;}

    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Aggregate; }
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.WriteAttribute(writer, XmlHelper.FunctionName, this.Function.ToString(), context);

    //    //    if (string.IsNullOrEmpty(this.Alias) == false)
    //    //        this.WriteAttribute(writer, XmlHelper.Alias, this.Alias, context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.InnerReference != null)
    //    //        this.InnerReference.WriteXml(writer, context);
    //    //    return true;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsAttributeMatch(XmlHelper.FunctionName, reader, context))
    //    //    {
    //    //        this.Function = (AggregateFunction)Enum.Parse(typeof(AggregateFunction), reader.Value);
    //    //        b = true;
    //    //    }
    //    //    else if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
    //    //    {
    //    //        this.Alias = reader.Value;
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    this.InnerReference = this.ReadNextInnerClause(reader.Name, reader, context);
    //    //    return true;
    //    //}

    //    // #endregion

    //    //
    //    // Interface Implementations
    //    //


    //}
        
}
