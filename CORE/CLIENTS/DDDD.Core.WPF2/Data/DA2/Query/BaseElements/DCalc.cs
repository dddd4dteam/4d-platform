
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Creates a new binary calculation
    /// </summary>
    [DataContract]
    public partial class DCalc : DCalculableClause, IDAlias // abstract
    {
        /// <summary>
        /// 
        /// </summary>
        public DCalc() {  }

        //
        // public properties
        //

        #region public BinaryOp BinaryOp {get;set;}

        private BinaryOp _op;
        /// <summary>
        /// Gets or sets the binary operation type
        /// </summary>
        [DataMember]
        public BinaryOp BinaryOp
        {
            get { return _op; }
            set { _op = value; }
        }

        #endregion

        #region public DBClause Left {get;set;}

        private DClause _left;
        /// <summary>
        /// Gets or sets the left argument
        /// </summary>
        [DataMember]
        public DClause Left
        {
            get { return _left; }
            set { _left = value; }
        }

        #endregion

        #region public DBClause Right {get;set;}

        private DClause _right;
        /// <summary>
        /// Gets or sets the right argument
        /// </summary>
        [DataMember]
        public DClause Right
        {
            get { return _right; }
            set { _right = value; }
        }

        #endregion

        #region public string Alias {get;set;}

        private string _as;

        /// <summary>
        /// Gets or sets the alias name
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _as; }
            set { _as = value; }
        }

        #endregion

        //
        // static factory methods
        //

        #region public static DBCalc Calculate(DBClause left, BinaryOp op, DBClause right)
        
        /// <summary>
        /// Creates a new binary calculation with the left, op and right arguments
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Calculate(DClause left, BinaryOp op, DClause right)
        {
            if (null == left)
                throw new ArgumentNullException("left");
            if (null == right)
                throw new ArgumentNullException("right");

            DCalc cref = new DCalc();
            cref.BinaryOp = op;
            cref.Left = left;
            cref.Right = right;

            return cref;
        }

        #endregion

        #region public static DBCalc Plus(DBClause left, DBClause right)
        /// <summary>
        /// Creates a new Addition operation
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Plus(DClause left, DClause right)
        {
            return Calculate(left, BinaryOp.Add, right);
        }

        #endregion

        #region public static DBCalc Minus(DBClause left, DBClause right)
        /// <summary>
        /// Creates a new Subtract operation
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Minus(DClause left, DClause right)
        {
            return Calculate(left, BinaryOp.Subtract, right);
        }

        #endregion

        #region public static DBCalc Times(DBClause left, DBClause right)
        /// <summary>
        /// Creates a new  multiply operation
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Times(DClause left, DClause right)
        {
            return Calculate(left, BinaryOp.Multiply, right);
        }

        #endregion

        #region public static DBCalc Divide(DBClause left, DBClause right)

        /// <summary>
        /// Creates a new division operation
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Divide(DClause left, DClause right)
        {
            return Calculate(left, BinaryOp.Divide, right);
        }

        #endregion

        #region public static DBCalc Modulo(DBClause left, DBClause right)
        /// <summary>
        /// Creates a new Modulo operation
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalc Modulo(DClause left, DClause right)
        {
            return Calculate(left, BinaryOp.Modulo, right);
        }

        #endregion

        #region internal static DBCalc Calculate()

        internal static DCalc Calculate()
        {
            DCalc cref = new DCalc();
            return cref;
        }

        #endregion
        


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DCalc()
            { 
                Alias = this.Alias,
                BinaryOp = this.BinaryOp,
                Left = (this.Left != null) ? this.Left.CloneClause() : null,
                Right = (this.Right != null) ? this.Left.CloneClause() : null                
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------



        //
        // SQL Statement builder
        //
        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginBlock();
            this.Left.BuildStatement(builder);
            builder.WriteOperator((Operator)Enum.Parse(typeof(Operator), this.BinaryOp.ToString(), false));
            this.Right.BuildStatement(builder);
            builder.EndBlock();
            if (string.IsNullOrEmpty(this.Alias) == false)
            {
                builder.WriteAlias(this.Alias);
            }
            return true;
        }

        #endregion





        public void As(string aliasName)
        {
            Alias = aliasName;            
        }



    }

    //[DataContract]
    //public partial class DBCalcRef : DBCalc
    //{
    //    public DBCalcRef() { }


    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBCalcRef()
    //        {
    //            Alias = this.Alias,
    //            BinaryOp = this.BinaryOp,
    //            Left = (this.Left != null) ? this.Left.CloneClause() : null,
    //            Right = (this.Right != null) ? this.Left.CloneClause() : null
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------





    //    //
    //    // XML Serialization
    //    //
        
    //    //#region protected override string XmlElementName
        
    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Calculation; }
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsAttributeMatch(XmlHelper.Operator, reader, context))
    //    //    {
    //    //        this.BinaryOp = (BinaryOp)Enum.Parse(typeof(BinaryOp), reader.Value, true);
    //    //        b = true;
    //    //    }
    //    //    else if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
    //    //    {
    //    //        this.Alias = reader.Value;
    //    //        b = true;
    //    //    }
    //    //    else
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsElementMatch(XmlHelper.LeftOperand, reader, context)
    //    //        && !reader.IsEmptyElement && reader.Read())
    //    //    {
    //    //        this.Left = this.ReadNextInnerClause(XmlHelper.LeftOperand, reader, context);
    //    //        b = reader.Read();
    //    //    }
    //    //    else if (this.IsElementMatch(XmlHelper.RightOperand, reader, context)
    //    //        && !reader.IsEmptyElement && reader.Read())
    //    //    {
    //    //        this.Right = this.ReadNextInnerClause(XmlHelper.RightOperand, reader, context);
    //    //        b = reader.Read();
    //    //    }
    //    //    else
    //    //        b = base.ReadAnInnerElement(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.WriteAttribute(writer, XmlHelper.Operator, this.BinaryOp.ToString(), context);

    //    //    if (string.IsNullOrEmpty(this.Alias) == false)
    //    //        this.WriteAlias(writer, this.Alias, context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.Left != null)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.LeftOperand, writer, context);
    //    //        this.Left.WriteXml(writer, context);
    //    //        this.WriteEndElement(XmlHelper.LeftOperand, writer, context);
    //    //    }

    //    //    if (this.Right != null)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.RightOperand, writer, context);
    //    //        this.Right.WriteXml(writer, context);
    //    //        this.WriteEndElement(XmlHelper.RightOperand, writer, context);
    //    //    }

    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //    //
    //    // Interface Implementations
    //    //

    //}
}
