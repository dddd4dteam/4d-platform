﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// A list of DBTokens along with methods to support statement building
    /// </summary>
    //[KnownType(typeof(DBToken))]
    //[CollectionDataContract]
   // public class DBTokenList : List<DBToken> //where T : DBToken

    public class DTokenList <T> : List<T> where T : DToken
    {
        /// <summary>
        /// .ctor
        /// </summary>
        public DTokenList() { } //set { ;}   // set { ;} //set { ;} //set { ;}

        /// <summary>
        /// Builds this list of tokens onto the statement
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder)
        {
            return this.BuildStatement(builder, false, false);
        }

        /// <summary>
        /// Builds the list of tokens 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="eachOnNewLine"></param>
        /// <param name="indent"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder, bool eachOnNewLine, bool indent)
        {
            int count = 0;

            bool outputSeparator = false; //initially false

            string start = DTokenListOptions.ListStart;
            if (!string.IsNullOrEmpty(start))
                builder.WriteRaw(DTokenListOptions.ListStart);

            for (int i = 0; i < this.Count; i++)
            {
                DToken clause = this[i];

                if (outputSeparator)
                {
                    if (DTokenListOptions.UseBuilderSeparator)
                        builder.AppendReferenceSeparator();

                    else if (!string.IsNullOrEmpty(DTokenListOptions.TokenSeparator))
                        builder.WriteRaw(DTokenListOptions.TokenSeparator);

                    if (eachOnNewLine)
                        builder.BeginNewLine();
                }

                //if the clause outputs then it should return true - so the next item has a separator written
                outputSeparator = clause.BuildStatement(builder);

                if (outputSeparator) //if we were output then increment the count
                    count++;
            }

            string end = DTokenListOptions.ListEnd;
            if (!string.IsNullOrEmpty(end))
                builder.WriteRaw(end);

            //did we write anything
            if (count > 0 || !string.IsNullOrEmpty(start) || !string.IsNullOrEmpty(end))
                return true;
            else
                return false;

        }

    }
}
