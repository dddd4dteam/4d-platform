
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using System;
using System.Linq;
using System.Runtime.Serialization;


namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a new INSERT query. 
    /// Use DBQuery.Insert or one of its overloads to create intances of this method
    /// </summary>
    [DataContract]
    public partial class DInsertQuery : DQuery
    {
        public DInsertQuery() { }


        private DTable _into;

        /// <summary>
        /// 
        /// </summary>
        public DTable IntoTable
        {
            get
            {
                return _into;
            }
        }

        private DSelectSet _fields;
        private DValueSet _values;
        private DSelectQuery _innerselect;
        private DClause _last;


        //
        // properties
        //


        #region public DBTableSet IntoSet {get;set;}

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DTable Into //internal
        {
            get { return this._into; }
            set { this._into = value; }
        }

        #endregion

        #region public DBSelectSet FieldsSet {get;set;}

        [DataMember]
        public DSelectSet FieldsSet //internal
        {
            get { return this._fields; }
            set { this._fields = value; }
        }

        #endregion

        #region public DBValueSet ValueSet {get;set;}

        [DataMember]
        public DValueSet ValueSet //internal
        {
            get { return this._values; }
            set { this._values = value; }
        }

        #endregion

        #region public DBSelectQuery InnerSelect {get;set;}

        [DataMember]
        public DSelectQuery InnerSelect //internal
        {
            get { return this._innerselect; }
            set { this._innerselect = value; }
        }

        #endregion

        #region public  DBClause Last {get;set;}

        /// <summary>
        /// Gets or sets the last clause acted on so that statements can be chained.
        /// </summary>
        [DataMember]
        public DClause Last   //protected internal
        {
            get { return this._last; }
            set { this._last = value; }
        }

        #endregion

        //
        // public methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {

            return new DInsertQuery()
            {
                FieldsSet = (this.FieldsSet != null) ? this.FieldsSet.CloneClause() as DSelectSet : null,
                InnerSelect = (this.InnerSelect != null) ? this.InnerSelect.CloneClause() as DSelectQuery : null,
                Into = (this.Into != null) ? this.Into.CloneClause() as DTable : null,
                IsInnerQuery = this.IsInnerQuery,
                ValueSet = (this.ValueSet != null) ? this.ValueSet.CloneClause() as DValueSet : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null,
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------



        #region public DBInsertQuery Field(string _ModuleInitializer) + 4 overloads
        /// <summary>
        /// Specify explictly one of the schema.table.columns to be set when performing this insert
        /// </summary>
        /// <param name="owner">The table schema owner</param>
        /// <param name="table">The name of the table</param>
        /// <param name="_ModuleInitializer">The name of the column</param>
        /// <returns>Iteslf so statements can be chained</returns>
        public DInsertQuery Field(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Field(fld);
        }


        public DInsertQuery Field<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Field(fld);
        }



        /// <summary>
        /// Specify explicity one of the table.columns to be set when performing this insert
        /// </summary>
        /// <param name="table">The name of the table</param>
        /// <param name="_ModuleInitializer">The name of the column</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Field(Type table, string field)
        {
            DField fld = DField.Field(table, field);
            return Field(fld);
        }


        public DInsertQuery Field<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Field(fld);
        }



        /// <summary>
        /// Specify the name of one of the columns to be set when performing this insert
        /// </summary>
        /// <param name="_ModuleInitializer">The name of the column</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Field(string field)
        {
            DField fld = DField.Field(field);
            return Field(fld);
        }

        /// <summary>
        /// Specify one of the columns to be set when performing this insert
        /// </summary>
        /// <param name="_ModuleInitializer">Any clause</param>
        /// <returns>Itself so that statements can be chained</returns>
        public DInsertQuery Field(DClause field)
        {
            if (_fields == null)
                _last = _fields = DSelectSet.Select(field);
            else
                _last = _fields.And(field);
            return this;
        }

        /// <summary>
        /// Specify a range of columns (as an array or comma separated list) to be set when performing this insert
        /// </summary>
        /// <param name="fields">The fields to set</param>
        /// <returns>Itself so that statements can be chained</returns>
        public DInsertQuery Fields(params string[] fields)
        {
            if (_fields == null)
                _last = _fields = DSelectSet.SelectFields(fields);
            else
            {
                foreach (string fld in fields)
                {
                    _last = _fields.And(fld);

                }
            }
            return this;
        }

        #endregion

        #region public DBInsertQuery Value(ParamValue valueProvider) + 2 overloads

        /// <summary>
        /// Specify a value for the correspoding column via a delegate method
        /// </summary>
        /// <param name="valueProvider">A (anonymous) delegate that returns an object when invoked</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Value(ParamValue valueProvider)
        {
            DParam p = DParam.ParamWithDelegate(valueProvider);
            return Value(p);
        }

        /// <summary>
        /// Specify a value for the corresponding column as a clause - DBConst, DBParam etc
        /// </summary>
        /// <param name="clause">The clause to return the value</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Value(DClause clause)
        {
            if (_innerselect != null)
                throw new ArgumentException("Cannot assign from values if the insert statement has an existing select statement");

            if (_values == null)
                _values = DValueSet.Values();
            _last = _values.And(clause);
            return this;
        }

        /// <summary>
        /// Specify a number of values for the corresponding columns as clauses - DBConst, DBParam etc.
        /// </summary>
        /// <param name="values">The values to use as an array or comma separated list</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Values(params DClause[] values)
        {
            if (_innerselect != null)
                throw new ArgumentException("Cannot assign from values if the insert statement has an existing select statement");

            if (_values == null)
                _values = DValueSet.Values();

            foreach (DClause val in values)
            {
                _last = _values.And(val);

            }

            return this;
        }

        #endregion

        #region public DBInsertQuery Select(DBSelectQuery select)

        /// <summary>
        /// Appends a select statement to the Insert query which will be evaluated
        /// at runtime and actioned on the database.
        /// </summary>
        /// <param name="select">The select query which returns the results to be inserted.</param>
        /// <returns>Itself so statements can be chained</returns>
        public DInsertQuery Select(DSelectQuery select)
        {
            if (_values != null)
                throw new ArgumentException("Cannot assign from values if the insert statement has an existing select statement");

            _innerselect = select;
            _innerselect.IsInnerQuery = true;
            _last = select;
            return this;
        }

        #endregion



        #region public DBInsertQuery BuildInsertIntoFields(object tableObject, List<String> BesidesFields)

        /// <summary>
        /// Building InsertInto Fields Statements 
        /// </summary>
        /// <param name="tableObject">Table Object</param>
        /// <param name="BesidesFields">Excepting fileds - IdentifyFields.. </param>
        /// <returns></returns>
        public DInsertQuery BuildInsertIntoFields(object tableObject)
        {
            DInsertQuery insertQuery = this; // DBInsertQuery.InsertInto(tableObject.GetType().Name);

            if (tableObject == null) return this; // ����� ��� ������ � �� ��������

            foreach (var item in tableObject.GetType().GetProperties().Where(p => p.GetPropertyAttributes<SQLColumnAttribute>().Any()))
            {
                object insertFieldValue = tableObject.GetType().GetProperty(item.Name).GetValue(tableObject, null);
                insertQuery.Field(item.Name).Value(DConst.GetValueClause(insertFieldValue, item.IsDefined(typeof(NullableAttribute), false)));
            }

            return this;
        }

        #endregion public DBInsertQuery BuildInsertIntoFields(object tableObject, List<String> BesidesFields)





        public DInsertQuery WithChangeTrackingContext(Guid? ChangeTrackingContext)
        {
            this.ChangeTrackingContext = ChangeTrackingContext;
            return this;
        }

        //
        // build method(s)
        //
        #region public override bool BuildStatement(DBStatementBuilder builder)


        /// <summary>
        /// Generates the INSERT statement using the provider specific statement builder
        /// </summary>
        /// <param name="builder">The builder that outputs provider specific statements</param>
        /// <returns>true</returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (ChangeTrackingContext != null)
                builder.WithChangeTrackingContext(ChangeTrackingContext.Value);

            builder.BeginInsertStatement();
            if (_into == null)
                throw new NullReferenceException("No table defined to insert into");

            _into.BuildStatement(builder);
            if (this._fields != null && this._fields.Results.Count > 0)
            {
                builder.BeginInsertFieldList();
                _fields.BuildStatement(builder);
                builder.EndInsertFieldList();
            }
            if (_values != null && _values.HasClauses)
            {
                builder.BeginInsertValueList();
                _values.BuildStatement(builder);
                builder.EndInsertValueList();
            }
            else if (_innerselect != null)
            {
                builder.BeginSubStatement();
                _innerselect.BuildStatement(builder);
                builder.EndSubStatement();
            }
            builder.EndInsertStatement();

            return true;

        }

        #endregion



        /// <summary>
        /// Get InsertQuery for IBObject item with its providerKey.
        /// </summary>
        /// <param name="insertObject"></param>
        /// <param name="providerKey"></param>
        /// <param name="ChangeTrackingContext"></param>
        /// <returns></returns>
        public static string GetQuery(IBObject insertObject
                                    , string providerKey
                                    , Guid? ChangeTrackingContext = null)
        {


            //if (insertObject is IPersistableBO == false) return result;
            //throw new Exceptionb("");

            //if ((insertObject as BObjectBase).IsBO_VObject())
            //    tableObject = (IPersistableBO)((IVobject)insertObject).CloneToTarget(); // ������� ������ ������� ��� ��������
            //else tableObject = insertObject;

            if (ChangeTrackingContext.HasValue)
            {
                // ���������� �������
                return DInsertQuery.InsertInto(insertObject)
                                    .WithChangeTrackingContext(ChangeTrackingContext.Value)
                                    .ToSQLString(providerKey);
            }
            else
            {
                // ���������� �������
                return DInsertQuery.InsertInto(insertObject)
                                    .ToSQLString(providerKey);
            }
        }


    }
}
