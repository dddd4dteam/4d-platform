 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Represents a batch of DBStatement statements that can be passed to the 
    /// database engine in one command and executed
    /// </summary>
    [DataContract]
    public partial class DScript : DQuery
    {
        //
        // ctor(s)
        //

        #region internal DBScript()
        /// <summary>
        /// Creates a new DBScript
        /// </summary>
        public DScript() //internal
            : base()
        {
        }

        #endregion



        #region internal List<DBQuery> Inner {get;}

        private DStatementList _inner;
       /// <summary>
       /// Gets the list of statements in this script
       /// </summary>
        [DataMember]
        public DStatementList Inner //internal
        {
            get 
            {
                if (_inner == null)
                    _inner = new DStatementList();
                return _inner;
            }
            set { _inner = value; }
        }

        #endregion

        #region internal bool HasInnerStatements {get;}
        

        /// <summary>
        /// Returns true if this script has one or more inner statements
        /// </summary>        
        internal bool HasInnerStatements
        {
            get
            {                
                return (null != this._inner && this._inner.Count > 0);
            }            
        }

        #endregion

     
        //
        // factory methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DScript()
            { 
                Inner=(this.Inner != null) ? this.Inner.Clone() as DStatementList : null            
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        




        #region public DBScript Append(DBStatement query)
        /// <summary>
        /// Appends the statement onto this script
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public DScript Append(DStatement query)
        {
            return this.Then(query);
        }

        #endregion

        #region public DBScript Then(DBStatement query)
        /// <summary>
        /// Appends the statement onto this script
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public DScript Then(DStatement query)
        {
            this.Inner.Add(query);
            return this;
        }

        #endregion

        #region public DBScript End()
        /// <summary>
        /// Closes the script
        /// </summary>
        /// <returns></returns>
        public DScript End()
        {
            return this;
        }

        #endregion


        /// <summary>
        /// creates a parameter Declaration in this script
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DScript Declare(DParam param)
        {
            DDeclaration dec = DDeclaration.Declare(param);
            this.Then(dec);
            return this;
        }


        /// <summary>
        /// creates a parameter assignment in this script
        /// </summary>
        /// <param name="assignment"></param>
        /// <returns></returns>
        public DScript Set(DAssign assignment)
        {
            DSet set = DSet.Set(assignment);
            this.Then(set);
            return this;
        }



        /// <summary>
        /// creates a parameter assignment statement in this script
        /// </summary>
        /// <param name="param"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DScript Set(DParam param, DClause clause)
        {
            DAssign assign = DAssign.Set(param, clause);
            return this.Set(assign);
        }


        /// <summary>
        /// Creates a return statement in this script
        /// </summary>
        /// <returns></returns>
        public DScript Return()
        {
            DReturn ret = DReturn.Return();
            this.Then(ret);
            return this;
        }


        /// <summary>
        /// Creates a return statement with value in this script
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DStatement Return(DClause clause)
        {
            DReturn ret = DReturn.Return(clause);
            this.Then(ret);
            return ret;
        }


        /// <summary>
        /// Creates a return statement with the parameter in this script
        /// </summary>
        /// <param name="paramName"></param>
        /// <returns></returns>
        public DStatement Return(string paramName)
        {
            DParam p = DParam.Param(paramName);
            return Return(p);
        }


        /// <summary>
        /// Creates a return statement with the parameter in this script
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public DStatement Return(DParam param)
        {
            DReturn ret = DReturn.Return(param);
            return ret;
        }


        //
        // SQL Statement builder
        //

        #region public override bool BuildStatement(DBStatementBuilder builder)


        /// <summary>
        /// Builds the script with the specified statement builder
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this._inner != null && this._inner.Count > 0)
            {
                //confirm that this data provider can support multiple 
                //SQL statements
                if (_inner.Count > 1)
                {
                    if (builder.SupportsMultipleStatements == false)
                        throw new InvalidOperationException("The current database does not support multiple statements within a single command");
                }
                builder.BeginScript();
                foreach (DStatement q in this.Inner)
                {
                    q.BuildStatement(builder);
                }
                builder.EndScript();
                return true;
            }
            else
                return false;
        }

        #endregion


        //
        // xml serialization
        //

        //#region protected override string XmlElementName {get;}
        ///// <summary>
        ///// Gets the xml element name for this script
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Script; }
        //}

        //#endregion


        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        ///// <summary>
        ///// reads the inner elements
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b;
        //    DBClause c = this.ReadNextInnerClause(reader.Name, reader, context);
        //    if (c != null)
        //    {
        //        this.Inner.Add((DBStatement)c);
        //        b = true;
        //    }
        //    else
        //        b = false;

        //    return b;
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        ///// <summary>
        ///// writes all the inner elements in this script
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasInnerStatements)
        //    {
        //        this.Inner.WriteXml(writer, context);
        //    }
        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

    }
    



    [CollectionDataContract]
    public partial class DQueryList : List<DQuery>, ICloneable        // DBClauseList<DBQuery> //internal
    {
        public DQueryList() { }
        
        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DQueryList newJoinList = new DQueryList();

            foreach (var item in this)
            { newJoinList.Add(item.CloneClause() as DQuery); }

            return newJoinList;
        }



#if SERVER

#endif
        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);

        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {


        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add((DBQuery)c);

        //            if (isEmpty)
        //                return true;
        //        }

        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());

        //    return true;

        //}

    }




    [CollectionDataContract]
    public partial class DStatementList : List<DStatement> , ICloneable  // DBClauseList<DBStatement>//internal
    {
        

        public DStatementList() { }
        

        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DStatementList newJoinList = new DStatementList();

            foreach (var item in this)
            { newJoinList.Add(item.CloneClause() as DStatement); }

            return newJoinList;
        }



#if SERVER

#endif
        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);
        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {
        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add((DBStatement)c);
        //            if (isEmpty)
        //                return true;
        //        }
        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());
        //    return true;
        //}


    }

}
