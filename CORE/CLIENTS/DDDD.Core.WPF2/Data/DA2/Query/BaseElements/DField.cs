

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Definition of a table column in the SQL database schema. 
    /// This class is abstract - to create an instance use the static Field or AllField methods
    /// </summary>
    [DataContract]
    public partial class DField : DCalculableClause, IDAlias //abstract
    {
        //
        // ctor(s)   //protected
        //
        public DField() { }

        
        private string _name;       
        private string _owner;
        

        //
        // properties
        //

        #region public virtual string Name {get;set;}
        
        /// <summary>
        /// Gets or Sets the name of the Field
        /// </summary>
        [DataMember]
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion


        //
        #region ------------public string Alias {get;set;} ------------------

        private string _alias;

        /// <summary>
        /// Gets or Sets the Alias name for this _ModuleInitializer
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        #endregion ------------public string Alias {get;set;} ------------------



        #region ------------- public Table -----------------

        Type _TableType = null;

        /// <summary>
        ///��� �������/������ �� ������� ����� �������� ������
        /// ����������� ��� 
        /// </summary>
        public Type TableType
        {
            get { return _TableType; }
            protected set
            {
                _TableType = value;
                TableTypeName = (value != null) ? value.FullName : null;
                Table = (value != null) ? value.Name : null;
            }
        }

        /// <summary>
        /// �������� ���� ��������� �������/��� ��������� - � ��������� ����������, �� ��� ������
        /// ������ ���������� �� ���� �������
        /// </summary>
        [DataMember]
        public String TableTypeName { get; set; }

        
        /// <summary>
        /// Gets or sets the optional name of the table / view this _ModuleInitializer is a column on
        /// </summary>
        [DataMember]
        public String Table {get; set;}


        #endregion  -------------------  public Table  -----------------


        #region public string Owner {get;set;}

        /// <summary>
        /// Gets or sets the optional schema owner for this _ModuleInitializer
        /// </summary>
        [DataMember]
        public string Owner
        {
            get { return _owner; }
            set { _owner = value; }
        }

        #endregion

                

        //
        // static factory methods
        //
        #region public static DBField Field()
        /// <summary>
        /// Creates a new empty DBField reference
        /// </summary>
        /// <returns></returns>
        public static DField Field()
        {
            DField fref = new DField();
            return fref;
        }

        #endregion

        #region public static DBField Field(string FieldName)

        /// <summary>
        /// Creates a new DBField reference with the specified name - do not enclose in delimiters
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public static DField Field(string field)
        {
            if (string.IsNullOrEmpty(field))
                throw new ArgumentNullException("FieldName");
            DField fref = new DField();
            fref.Name = field;

            return fref;
        }


       


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DField()
            {   Name = this.Name,
                Alias = this.Alias,
                Owner = this.Owner,
                TableType = this.TableType,
                Table = this.Table,                 
                TableTypeName = this.TableTypeName                   
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        #endregion

        #region public static DBField Field(string table, string _ModuleInitializer)

        /// <summary>
        /// Creates a new DBField reference with the specified table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <param name="table">The table (or table alias) this _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The name of the _ModuleInitializer</param>
        /// <returns></returns>
        public static DField AliasField(string Alias, string field)
        {
            if (string.IsNullOrEmpty(field))
                throw new ArgumentNullException("_ModuleInitializer");
            //we can get away with table being empty or null.

            DField fref = new DField();
            fref.Name = field;
            fref.Table = Alias;

            return fref;
        }


        /// <summary>
        /// Creates a new DBField reference with the specified table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static DField Field(Type TTable, string field)
        {
            if (string.IsNullOrEmpty(field))
                throw new ArgumentNullException("_ModuleInitializer");
            //we can get away with table being empty or null.

            DField fref = new DField();
            fref.Name = field;
            fref.TableType = TTable;
            //fref.Table = TTable.Name;

            return fref;
        }

        /// <summary>
        /// Creates a new DBField reference with the specified table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static DField Field<TTable>(string field)
        {
            if (string.IsNullOrEmpty(field))
                throw new ArgumentNullException("_ModuleInitializer");
            //we can get away with table being empty or null.

            DField fref = new DField();
            fref.Name = field;
            fref.TableType = typeof(TTable);
            //fref.Table = typeof(TTable).Name;

            return fref;
        }




        #region  ------------------------ Compare Two  DBField Objects ---------------------------

        /// <summary>
        /// False- ��� ��������� �� �����
        /// True- �������� ��� ��������� ���������
        /// </summary>
        /// <param name="AnotherObject"></param>
        /// <returns></returns>
        public override bool CompareWith(DClause AnotherObject)
        {
            bool compareResult = true;
            if ((AnotherObject as DField) == null) { compareResult = false; return compareResult; } // ������ ���� ������ ������������ ������� ����� null � ������ �� ����� �� null  �� ���� ����� ��� �����������

            DField anField = AnotherObject as DField;

            //���� ��� �������� - Filed  �  FieldAll  ������� ������� ����� ������� ����� ������� ��������
            if (GetType().Name != AnotherObject.GetType().Name) { compareResult = false; return compareResult; }

            //������ ��� ������ ���� � ����� ������ �������� �� �����
            if (Alias != anField.Alias) { compareResult = false; return compareResult; }
            else if (Name != anField.Name) { compareResult = false; return compareResult; }
            else if (Table != anField.Table) { compareResult = false; return compareResult; }
            else if (Owner != anField.Owner) { compareResult = false; return compareResult; }
            

            return compareResult;
        }

        #endregion  ------------------------ Compare Two  DBField Objects ---------------------------



        #endregion

        #region public static DBField Field(string owner, string table, string _ModuleInitializer)
        /// <summary>
        /// Creates a new DBField reference with the specified owner.table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <param name="table">The table (or table alias) this _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The name of the _ModuleInitializer</param>
        /// <param name="owner">The schema owner</param>
        /// <returns></returns>
        //public static DBField Field(string owner, string table, string field)
        //{
        //    DBFieldRef fref = new DBFieldRef();
        //    fref.Name = field;
        //    fref.Table = table;
        //    fref.Owner = owner;

        //    return fref;
        //}

        /// <summary>
        /// Creates a new DBField reference with the specified owner.table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static DField Field(string owner, Type TTable, string field)
        {
            DField fref = new DField();
            fref.Name = field;
            //fref.Table = TTable.Name;
            fref.TableType = TTable;
            fref.Owner = owner;

            return fref;
        }


        /// <summary>
        /// Creates a new DBField reference with the specified owner.table._ModuleInitializer name - do not enclose in delimiters
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static DField Field<TTable>(string owner, string field)
        {
            if (string.IsNullOrEmpty(field))
                throw new ArgumentNullException("_ModuleInitializer");
            //we can get away with table being empty or null.

            DField fref = new DField();
            fref.Name = field;            
            fref.TableType = typeof(TTable);
            fref.Owner = owner;
            //fref.Table = typeof(TTable).Name;

            return fref;
        }
        
        #endregion


        #region public static DBField AllFields()

        /// <summary>
        /// Creates a new DBField reference to all fields
        /// </summary>
        /// <returns></returns>
        public static DField AllFields()
        {
            DFieldAll all = new DFieldAll();

            return all;
        }

        #endregion

        #region public static DBField AllFields(string table)

        /// <summary>
        /// Creates a new DBField reference to all fields (*) in specified table - do not enclose in delimiters
        /// </summary>
        /// <param name="table">The table (or table alias) the fields belong to</param>
        /// <returns></returns>
        //public static DBField AllFields(string table)
        //{
        //    DBFieldAllRef all = new DBFieldAllRef();
        //    all.Table = table;
        //    return all;
        //}

        /// <summary>
        /// Creates a new DBField reference to all fields (*) in specified table - do not enclose in delimiters
        /// </summary>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public static DField AllFields(Type TTable)
        {
            DFieldAll all = new DFieldAll();
            all.TableType  = TTable;
            return all;
        }


        /// <summary>
        /// Creates a new DBField reference to all fields (*) in specified table - do not enclose in delimiters
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public static DField AllFields<TTable>()
        {
            DFieldAll all = new DFieldAll();
            all.TableType = typeof(TTable);
            return all;
        }


        #endregion

        #region public static DBField AllFields(string owner, string table)

        /// <summary>
        /// Creates a new DBField reference to all fields (*) with the specified owner.table - do not enclose in delimiters
        /// </summary>
        /// <param name="table">The table (or table alias) the fields belong to</param>
        /// <param name="owner">The schema owner</param>
        /// <returns></returns>
        //public static DBField AllFields(string owner, string table)
        //{
        //    DBFieldAllRef all = new DBFieldAllRef();
        //    all.Table = table;
        //    all.Owner = owner;
        //    return all;
        //}


        /// <summary>
        /// Creates a new DBField reference to all fields (*) with the specified owner.table - do not enclose in delimiters
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public static DField AllFields(string owner, Type TTable)
        {
            DFieldAll all = new DFieldAll();
            all.TableType = TTable;
            all.Owner = owner;
            return all;
        }


        /// <summary>
        /// Creates a new DBField reference to all fields (*) with the specified owner.table - do not enclose in delimiters
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static DField AllFields<TTable>(string owner)
        {
            DFieldAll all = new DFieldAll();
            all.TableType = typeof(TTable);
            all.Owner = owner;
            return all;
        }
        
        #endregion






        #region public override bool BuildStatement(DBStatementBuilder builder)


        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (string.IsNullOrEmpty(this.Name))
                return false;

            builder.WriteSourceField(this.Owner, this.Table, this.Name, this.Alias);

            return true;
        }

        #endregion
   
        
        


        /// <summary>
        /// Sets the alias for this _ModuleInitializer reference - Inheritors must override
        /// </summary>
        /// <param name="alias"></param>
        /// <returns></returns>
        public void As(string alias)
        {
            try
            {
                Alias = alias;               
                //Need to be overriden
            }
            catch (Exception exc)
            {  throw exc;  }
        }

    }





    //
    // sub classes
    //
//    #region  --------------- public class DBFieldRef : DBField, IDBAlias ---------------------

//    [DataContract]
//    public partial class DBFieldRef : DBField
//    {

//        #region  ------------------ CTOR ---------------------

//                public DBFieldRef() { }

//        #endregion ------------------ CTOR ---------------------
        
        

//        //
//        // properties
     

//        //
//        // SQL statement build methods
//        //




//      #region  ------------------ IClonableClause.CloneClause -----------------------

//          /// <summary>
//          /// Cloning Clause
//          /// </summary>
//          /// <returns></returns>
//          public override DBClause CloneClause()
//          {
//                        return new DBFieldRef()
//                        {   Name = this.Name, 
//                            Alias = this.Alias,
//                            Owner = this.Owner,
//                            TableType = this.TableType,
//                            Table = this.Table,
//                            TableTypeName = this.TableTypeName
//                        };
//          }

//      #endregion  ------------------ IClonableClause.CloneClause -----------------------




//        //
//        // XML Serialization
//        //
//        //#region protected override string XmlElementName {get;}

//        //protected override string XmlElementName
//        //{
//        //    get { return XmlHelper.AField; }
//        //}

//        //#endregion
//        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

//        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
//        //{
//        //    bool b;

//        //    if (this.IsAttributeMatch(XmlHelper.FieldOwner, reader, context))
//        //    {
//        //        this.Owner = reader.Value;
//        //        b = true;
//        //    }
//        //    else if (this.IsAttributeMatch(XmlHelper.Table, reader, context))
//        //    {
//        //        this.Table = reader.Value;
//        //        b = true;
//        //    }
//        //    else if (this.IsAttributeMatch(XmlHelper.Name, reader, context))
//        //    {
//        //        this.Name = reader.Value;
//        //        b = true;
//        //    }
//        //    else if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
//        //    {
//        //        this.Alias = reader.Value;
//        //        b = true;
//        //    }
//        //    else
//        //        b = base.ReadAnAttribute(reader, context);

//        //    return b;
//        //}

//        //#endregion

//        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

//        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
//        //{
//        //    if (string.IsNullOrEmpty(this.Owner) == false)
//        //        this.WriteAttribute(writer, XmlHelper.Owner, this.Owner, context);
            
//        //    if (string.IsNullOrEmpty(this.Table) == false)
//        //        this.WriteAttribute(writer, XmlHelper.Table, this.Table, context);
            
//        //    if (string.IsNullOrEmpty(this.Name) == false)
//        //        this.WriteAttribute(writer, XmlHelper.Name, this.Name, context);

//        //    if (string.IsNullOrEmpty(this.Alias) == false)
//        //        this.WriteAttribute(writer, XmlHelper.Alias, this.Alias, context);

//        //    return base.WriteAllAttributes(writer, context);
//        //}

//        //#endregion

//        //
//        // Interface implementation
//        //

//    }

//    #endregion --------------- public class DBFieldRef : DBField, IDBAlias ---------------------


    #region ----------------- public class DBFieldAll : DBField ------------------------------

    /// <summary>
    /// Defines a reference to all fields e.g. SELECT * FROM ...
    /// </summary>
    [DataContract]
    public partial class DFieldAll : DField
    {
        public DFieldAll() { }

        #region public override string Name {get;set;}

        /// <summary>
        /// Overrides the default implementation to return '*' - setter does nothing
        /// </summary>
        public override string Name
        {
            get
            {
                return "*";
            }
            set
            {
                ;
            }
        }

        #endregion


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DFieldAll()
            {   Name = this.Name,
                Alias = this.Alias,
                Owner = this.Owner,
                TableType = this.TableType,
                Table = this.Table,
                TableTypeName = this.TableTypeName
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------

      //
      // SQL Statement build methods
      //

        #region public override bool BuildStatement(DBStatementBuilder builder)

        /// <summary>
        /// Outputs the AllFields reference onto the statement builder
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.WriteAllFieldIdentifier(this.Owner, this.Table);

            return true;
        }

        #endregion

        //
        // Xml Serilaization
        //
        //#region protected override string XmlElementName {get;}
        ///// <summary>
        ///// Gets the element name for this Field
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.AllFields; }
        //}

        //#endregion

        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Overrides  the default implementation to read specfic attributes for the all fields element
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b;

        //    if (this.IsAttributeMatch(XmlHelper.Owner, reader, context))
        //    {
        //        this.Owner = reader.Value;
        //        b = true;
        //    }
        //    else if (this.IsAttributeMatch(XmlHelper.Table, reader, context))
        //    {
        //        this.Table = reader.Value;
        //        b = true;
        //    }
        //    else
        //        b = base.ReadAnAttribute(reader, context);

        //    return b;
        //}

        //#endregion

        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        ///// <summary>
        ///// Overrides the default behavior to write the attributes for the All fields element.
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (string.IsNullOrEmpty(this.Owner) == false)
        //        this.WriteAttribute(writer, XmlHelper.Owner, this.Owner, context);

        //    if (string.IsNullOrEmpty(this.Table) == false)
        //        this.WriteAttribute(writer, XmlHelper.Table, this.Table, context);

        //    return base.WriteAllAttributes(writer, context);
        //}

        //#endregion

    }

    #endregion ----------------- public class DBFieldAllRef : DBField ------------------------------

    //
    // collection classes
    //

    #region  -------------------public class DBFieldList : DBClauseList<DBField> ----------------------

    /// <summary>
    /// Defines a list of DBFields
    /// </summary>
    [CollectionDataContract]
    public partial class DFieldList : List<DField>, ICloneable //DBClause
    {
        public DFieldList() { }


        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public  object Clone()
        {
            DFieldList newClauseList = new DFieldList();
            
            foreach (var item in this)
            { newClauseList.Add(item.CloneClause() as DField);       }

            return newClauseList;
        }



        /// <summary>
        /// Builds this list of tokens onto the statement
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder)
        {
            return this.BuildStatement(builder, false, false);
        }

        /// <summary>
        /// Builds the list of tokens 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="eachOnNewLine"></param>
        /// <param name="indent"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder, bool eachOnNewLine, bool indent)
        {
            int count = 0;

            bool outputSeparator = false; //initially false

            string start = DTokenListOptions.ListStart;
            if (!string.IsNullOrEmpty(start))
                builder.WriteRaw(DTokenListOptions.ListStart);

            for (int i = 0; i < this.Count; i++)
            {
                DToken clause = this[i];

                if (outputSeparator)
                {
                    if ( DTokenListOptions.UseBuilderSeparator)
                        builder.AppendReferenceSeparator();

                    else if (!string.IsNullOrEmpty(DTokenListOptions.TokenSeparator))
                        builder.WriteRaw(DTokenListOptions.TokenSeparator);

                    if (eachOnNewLine)
                        builder.BeginNewLine();
                }

                //if the clause outputs then it should return true - so the next item has a separator written
                outputSeparator = clause.BuildStatement(builder);

                if (outputSeparator) //if we were output then increment the count
                    count++;
            }

            string end = DTokenListOptions.ListEnd;
            if (!string.IsNullOrEmpty(end))
                builder.WriteRaw(end);

            //did we write anything
            if (count > 0 || !string.IsNullOrEmpty(start) || !string.IsNullOrEmpty(end))
                return true;
            else
                return false;

        }


        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);

        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {


        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add((DBField)c); //���������� ����

        //            if (isEmpty)
        //                return true;
        //        }

        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());

        //    return true;

        //}

        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //}


 

    }

    #endregion -------------------public class DBFieldList : DBClauseList<DBField> ----------------------
}
