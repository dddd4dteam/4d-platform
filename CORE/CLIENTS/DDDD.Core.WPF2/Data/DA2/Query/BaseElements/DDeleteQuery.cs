
using DDDD.Core.Diagnostics;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Encapsulates a DELETE sql query statement
    /// </summary>
    [DataContract]
    public partial class DDeleteQuery : DQuery
    {
        //
        // ctor(s)
        //
        public DDeleteQuery() //internal
          : base() { }

        private DFilterSet _where;




        private DClause _last;

        //
        // properties
        //

        #region public DBFilterSet WhereSet {get;set;}

        /// <summary>
        /// Gets or sets the WHERE filter for the delete statement
        /// </summary>
        [DataMember]
        public DFilterSet WhereSet //internal
        {
            get { return _where; }
            set { _where = value; }
        }

        #endregion

        #region public DBTable FromTable {get;set;}

        private DTable _from;


        /// <summary>
        /// Gets or sets the DBTable this delete statement refers to
        /// </summary>
        [DataMember]
        public DTable FromTable //internal
        {
            get { return _from; }
            set { _from = value; }
        }

        #endregion

        #region public DBClause Last {get;set;}

        /// <summary>
        /// Gets or sets the last clause that was modified - for chaining statements
        /// </summary>
        [DataMember]
        public DClause Last //internal
        {
            get { return this._last; }
            set { this._last = value; }
        }

        #endregion



        //#region protected override string XmlElementName {get;}

        //#endregion

        //
        // statement builder
        //

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DDeleteQuery()
            {
                FromTable = (this.FromTable != null) ? this.FromTable.CloneClause() as DTable : null,
                WhereSet = (this.WhereSet != null) ? this.WhereSet.CloneClause() as DFilterSet : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null,
                IsInnerQuery = this.IsInnerQuery
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------

        public DDeleteQuery WithChangeTrackingContext(Guid? ChangeTrackingContext)
        {
            this.ChangeTrackingContext = ChangeTrackingContext;
            return this;
        }



        #region public override bool BuildStatement(DBStatementBuilder builder)


        /// <summary>
        /// Generates the SQL statement using the provided builder for this DELETE query
        /// </summary>
        /// <param name="builder">The provider specific builder</param>
        /// <returns>true if the statement was built</returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (ChangeTrackingContext != null)
                builder.WithChangeTrackingContext(ChangeTrackingContext.Value);

            builder.BeginDeleteStatement();
            if (_from != null)
            {
                _from.BuildStatement(builder);
            }
            if (_where != null)
            {
                builder.BeginWhereStatement();
                _where.BuildStatement(builder);
                builder.EndWhereStatement();
            }
            builder.EndDeleteStatement();
            return true;

        }



        #endregion
        ////
        //// xml serialization
        ////

        ///// <summary>
        ///// Gets the XmlElement name for this DBDeleteQuery
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Delete; }
        //}

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Overrides the default implementation to write the From statement and Where elements
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (_from != null)
        //    {
        //        this.WriteStartElement(XmlHelper.From, writer, context);
        //        _from.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.From, writer, context);
        //    }
        //    if (_where != null)
        //    {
        //        _where.WriteXml(writer, context);
        //    }
        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Overrides the base implementation to read the From and Where elements
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b;

        //    if (this.IsElementMatch(XmlHelper.From, reader, context) && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this._from = (DBTable)this.ReadNextInnerClause(XmlHelper.From, reader, context);
        //        reader.Read();
        //        b = true;
        //    }
        //    else if (this.IsElementMatch(XmlHelper.Where, reader, context))
        //    {
        //        this._where = (DBFilterSet)context.Factory.Read(XmlHelper.Where, reader, context);
        //        b = true;
        //    }
        //    else
        //        b = base.ReadAnInnerElement(reader, context);

        //    return b;
        //}

        //#endregion

        //
        // where methods
        //

        #region public DBDeleteQuery Where(DBClause clause, ComparisonOperator compare, DBClause right) + 1 overload


        /// <summary>
        /// Creates the first where clause in this DBDeleteQuery using the left and right clauses as operands in the comparison
        /// </summary>
        /// <param name="left">The left operand</param>
        /// <param name="compare">The comparison type - Equals etc...</param>
        /// <param name="right">The right operand</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery Where(DClause left, Compare compare, DClause right)
        {
            DFilterSet fs = DFilterSet.Where(left, compare, right);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Creates the first where clause in this DBDeleteQuery using the DBComparison as the filter.
        /// </summary>
        /// <param name="compare">The DBComparison to use</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery Where(DComparison compare)
        {
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        //public DBDeleteQuery Where(DBComparison[] ItemPKFieldsValues)
        //{
        //    //DBFilterSet fs = DBFilterSet.Where(compare);

        //    DBFilterSet fs;

        //    for (Int32 i = 0; i < ItemPKFieldsValues.Length; i++)
        //    {


        //    }
        //        this._where = fs;
        //    this._last = fs;

        //    return this;
        //}


        #endregion

        #region public DBDeleteQuery WhereFieldEquals(string _ModuleInitializer, DBClause value) + 1 overload

        /// <summary>
        /// Creates the first where clause by using an equals comparison between the specified _ModuleInitializer and the clause
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery WhereFieldEquals(string field, DClause value)
        {
            return WhereField(field, Compare.Equals, value);
        }

        /// <summary>
        /// Creates the first where clause by using an equals comparison between the specified table._ModuleInitializer and the clause
        /// </summary>
        /// <param name="fieldTable">The name of the table the _ModuleInitializer belongs to</param>
        /// <param name="fieldName">The name of the _ModuleInitializer to comapre against</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery WhereFieldEquals(Type fieldTable, string fieldName, DClause value)
        {
            return WhereField(fieldTable, fieldName, Compare.Equals, value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DDeleteQuery WhereFieldEquals<TTable>(string fieldName, DClause value)
        {
            return WhereField<TTable>(fieldName, Compare.Equals, value);
        }

        #endregion ----------------------------------------------

        #region public DBDeleteQuery WhereField(string _ModuleInitializer, ComparisonOperator op, DBClause value) + 2 overload

        /// <summary>
        /// Creates the first where clause by using the specified comparison between the specified _ModuleInitializer and the clause
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery WhereField(string field, Compare op, DClause value)
        {
            DField fld = DField.Field(field);
            return Where(fld, op, value);
        }

        /// <summary>
        /// Creates the first where clause by using the specified comparison between the specified table._ModuleInitializer and the clause
        /// </summary>
        /// <param name="fieldTable">The table the _ModuleInitializer belongs to</param>
        /// <param name="fieldName">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery WhereField(Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldTable, fieldName);
            return Where(fld, op, value);
        }


        public DDeleteQuery WhereField<TTable>(string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldName);
            return Where(fld, op, value);
        }


        /// <summary>
        /// Creates the first where clause by using the specified comparison between the specified owner.table._ModuleInitializer and the clause
        /// </summary>
        /// <param name="fieldOwner">The schema owner of the table</param>
        /// <param name="fieldTable">The table the _ModuleInitializer belongs to</param>
        /// <param name="fieldName">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery WhereField(string fieldOwner, Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldOwner, fieldTable, fieldName);
            return Where(fld, op, value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DDeleteQuery WhereField<TTable>(string fieldOwner, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldOwner, fieldName);
            return Where(fld, op, value);
        }

        #endregion

        #region public DBDeleteQuery AndWhere(DBClause left, ComparisonOperator op, DBClause right) + 4 overloads

        /// <summary>
        /// Creates a futher where clause in this DBDeleteQuery using the left and right 
        /// clauses as operands in the comparison. Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="left">The left operand</param>
        /// <param name="op">The comparison type - Equals etc...</param>
        /// <param name="right">The right operand</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery AndWhere(DClause left, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And(left, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery using the specified comparison.
        /// Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="comparison">The comparison filter</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery AndWhere(DComparison comparison)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And(comparison);
            _last = _where;

            return this;
        }

        #endregion

        #region public DBDeleteQuery AndWhere(DBClause left, ComparisonOperator op, DBClause right) + 4 overloads

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified _ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="right">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery AndWhereField(string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And(field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified table._ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="table">The table the _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery AndWhereField(Type table, string field, Compare op, DClause value)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And(table, field, op, value);
            _last = _where;

            return this;
        }


        public DDeleteQuery AndWhereField<TTable>(string field, Compare op, DClause value)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And<TTable>(field, op, value);
            _last = _where;

            return this;
        }



        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified owner.table._ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="owner">The schema owner of the table</param>
        /// <param name="table">The table the _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="value">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery AndWhereField(string owner, Type table, string field, Compare op, DClause value)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And(owner, table, field, op, value);
            _last = _where;

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DDeleteQuery AndWhereField<TTable>(string owner, string field, Compare op, DClause value)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.And<TTable>(owner, field, op, value);
            _last = _where;

            return this;
        }



        #endregion

        #region public DBDeleteQuery OrWhere(DBClause left, ComparisonOperator op, DBClause right) + 1 overloads

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery using the specified comparison.
        /// Then combines with the previous filter in a boolean OR operation
        /// </summary>
        /// <param name="comparison">The comparison filter</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery OrWhere(DClause comparison)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or(comparison);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Creates a futher where clause in this DBDeleteQuery using the left and right 
        /// clauses as operands in the comparison. Then combines with the previous filter in a boolean AND operation
        /// </summary>
        /// <param name="left">The left operand</param>
        /// <param name="op">The comparison type - Equals etc...</param>
        /// <param name="right">The right operand</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery OrWhere(DClause left, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or(left, op, right);
            _last = _where;

            return this;
        }

        #endregion

        #region public DBDeleteQuery OrWhereField(string _ModuleInitializer, Compare op, DBClause right)

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified _ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean OR operation
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="right">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery OrWhereField(string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or(field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified table._ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean OR operation
        /// </summary>
        /// <param name="table">The table the _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="right">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery OrWhereField(Type table, string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or(table, field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DDeleteQuery OrWhereField<TTable>(string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or<TTable>(field, op, right);
            _last = _where;

            return this;
        }



        /// <summary>
        /// Creates a further where clause in this DBDeleteQuery between the specified owner.table._ModuleInitializer and the clause.
        /// Then combines with the previous filter in a boolean OR operation
        /// </summary>
        /// <param name="owner">The schema owner of the table</param>
        /// <param name="table">The table the _ModuleInitializer belongs to</param>
        /// <param name="_ModuleInitializer">The _ModuleInitializer to compare against</param>
        /// <param name="op">The comparison operator</param>
        /// <param name="right">The value clause</param>
        /// <returns>Itself so further statements can be combined</returns>
        public DDeleteQuery OrWhereField(string owner, Type table, string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or(owner, table, field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DDeleteQuery OrWhereField<TTable>(string owner, string field, Compare op, DClause right)
        {
            if (null == _where)
                throw new NullReferenceException("Cannot Append Where Clause Without Initial");

            _where = _where.Or<TTable>(owner, field, op, right);
            _last = _where;

            return this;
        }


        #endregion

        #region public DBSelectQuery WhereIn(string _ModuleInitializer, params object[] values) + 3 overloads

        /// <summary>
        /// Creates the first where clause in this DBDeleteQuery using the fields and a series of constant values
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer which will be compared to the psecified values</param>
        /// <param name="values">The set of values to limit the deletion to</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery WhereIn(string field, params object[] values)
        {
            DField fld = DField.Field(field);
            List<DClause> items = new List<DClause>();
            if (values != null && values.Length > 0)
            {
                foreach (object val in values)
                {
                    items.Add(DConst.Const(val));
                }
            }
            DComparison compare = DComparison.In(fld, items.ToArray());
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Creates the first where clause in this DBDeleteQuery using the fields and a series of DBClauses that are evaluated at execution time
        /// </summary>
        /// <param name="fld">The _ModuleInitializer which will be compared to the specified values</param>
        /// <param name="values">The set of DBClauses to be evaluated and compared</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery WhereIn(DField fld, params DClause[] values)
        {
            DComparison compare = DComparison.In(fld, values);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Creates the first where clause in this DBDeleteQuery using the fields and a sub select that is evaluated at execution time
        /// </summary>
        /// <param name="_ModuleInitializer">The _ModuleInitializer which will be compared to the specified values</param>
        /// <param name="select">The sub select to be evaluated and compared</param>
        /// <returns>Itself so further statements can be chained</returns>
        public DDeleteQuery WhereIn(string field, DSelectQuery select)
        {
            DField fld = DField.Field(field);
            DSubQuery subsel = DSubQuery.SubSelect(select);
            DComparison compare = DComparison.In(fld, subsel);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        #endregion



        //
        // static factory
        //

        #region internal static DBDeleteQuery Delete()

        /// <summary>
        /// internal factory method for the DBDeleteQuery
        /// </summary>
        /// <returns></returns>
        internal static DDeleteQuery Delete()
        {
            DDeleteQuery q = new DDeleteQuery();
            return q;
        }

        #endregion

        /// <summary>
        /// Get DeleteQuery
        /// </summary>
        /// <param name="deleteObject"></param>
        /// <param name="providerKey"></param>
        /// <param name="ChangeTrackingContext"></param>
        /// <returns></returns>
        public static string GetQuery(IBObject deleteObject
                                  , string providerKey
                                  , Guid? ChangeTrackingContext)
        {

            if ((deleteObject is IVobject))
                deleteObject = (IBObject)((IVobject)deleteObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (deleteObject is IPersistableBO), "Delete -target boItem is not IPersistableBO");//false

            DDeleteQuery DeleteQuery = DDeleteQuery.DeleteFrom(deleteObject.Contract)
                    .WithChangeTrackingContext(ChangeTrackingContext);

            DeleteQuery.WhereField(
                 (deleteObject as IPersistableBO).PrimaryKey[0]
                , Compare.Equals
                , DConst.Const(deleteObject.GetPropertyValue(
                      (deleteObject as IPersistableBO).PrimaryKey[0]))
                );

            if ((deleteObject as IPersistableBO).PrimaryKey.Count > 1)

                for (int i = 1; i < (deleteObject as IPersistableBO).PrimaryKey.Count; i++)
                {
                    DeleteQuery.AndWhereField(
                        (deleteObject as IPersistableBO).PrimaryKey[i]
                        , Compare.Equals
                        , DConst.Const(deleteObject.GetPropertyValue(
                            (deleteObject as IPersistableBO).PrimaryKey[i]))
                        );
                }

            return DeleteQuery.ToSQLString(providerKey);
        }



    }

}
