
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// DBComparison is an abstract class with static 
    /// factory methods to create concrete implementations of comparison operations
    /// </summary>
    [DataContract]
    public partial class DComparison : DClause //abstract
    {
        
        public DComparison() { }
        
        //
        // static factory methods
        //

        #region public static DBComparison Equal(DBClause from, DBClause to)
        /// <summary>
        /// Creates an equal comparison for the left and right clauses
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static DComparison Equal(DClause from, DClause to)
        {
            DBinaryComparison compref = new DBinaryComparison();
            compref.Left = from;
            compref.Right = to;
            compref.CompareOperator = DDDD.Core.Data.DA2.Query.Compare.Equals;

            return compref;
        }

        #endregion

        #region public static DBComparison Compare(DBClause from, Compare op, DBClause to)

        /// <summary>
        /// Creates a comparison operation between the left and right clauses
        /// </summary>
        /// <param name="from"></param>
        /// <param name="op"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public static DComparison Compare(DClause from, Compare op, DClause to)
        {
            if (null == from)
                throw new ArgumentNullException("from");
            if (null == to)
                throw new ArgumentNullException("to");

            DBinaryComparison compref = new DBinaryComparison();
            compref.Left = from;
            compref.Right = to;
            compref.CompareOperator = op;

            return compref;
        }

        #endregion

        #region public static DBComparison Between(DBClause value, DBClause min, DBClause max)

        /// <summary>
        /// Creates a teriary between operation for the value and the min and max clauses
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static DComparison Between(DClause value, DClause min, DClause max)
        {
            DTertiaryComparison tri = new DTertiaryComparison();
            tri.MinValue = min;
            tri.MaxValue = max;
            tri.TertiaryOperator = TertiaryOp.Between;
            tri.CompareTo = value;

            return tri;
        }

        #endregion

        #region public static DBComparison In(DBClause fld, params DBClause[] values)

        /// <summary>
        /// Creates a included in comparison using the parameter values
        /// </summary>
        /// <param name="fld"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static DComparison In(DClause fld, params DClause[] values)
        {
            DValueGroup grp = DValueGroup.All(values);

            return Compare(fld, DDDD.Core.Data.DA2.Query.Compare.In, grp);
        }

        #endregion

        #region public static DBComparison In(DBClause fld, DBClause clause)
        /// <summary>
        /// Creates an included in clause
        /// </summary>
        /// <param name="fld"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public static DComparison In(DClause fld, DClause clause)
        {
            return Compare(fld, DDDD.Core.Data.DA2.Query.Compare.In, clause);
        }

        #endregion


        #region public static DBComparison Like(DBClause fld, DBClause clause)
        /// <summary>
        /// Creates an included in clause
        /// </summary>
        /// <param name="fld"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public static DComparison Like(DClause fld, DClause clause)
        {
            return Compare(fld, DDDD.Core.Data.DA2.Query.Compare.Like, clause);
        }

        #endregion



        #region public static DBComparison Exists(DBClause clause)

        /// <summary>
        /// Creates an EXISTS comparison
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public static DComparison Exists(DClause clause)
        {
            DUnaryComparison un = new DUnaryComparison();
            un.UnaryOperator = DDDD.Core.Data.DA2.Query.UnaryOp.Exists;
            un.ToOperateOn = clause;

            return un;
        }

        #endregion

        #region public static DBComparison Not(DBClause clause)

        /// <summary>
        /// Creates an unary NOT comparison
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public static DComparison Not(DClause clause)
        {
            DUnaryComparison un = new DUnaryComparison();
            un.UnaryOperator = UnaryOp.Not;
            un.ToOperateOn = clause;
            return un;
        }

        #endregion
        
        //
        // internal parameterless factory methods
        //
        #region internal static DBComparison Not()

        internal static DComparison Not()
        {
            DUnaryComparison un = new DUnaryComparison();
            un.UnaryOperator = UnaryOp.Not;
            un.ToOperateOn = null;
            return un;
        }

        #endregion

        #region internal static DBComparison Between()

        internal static DComparison Between()
        {
            DTertiaryComparison comp = new DTertiaryComparison();
            comp.MinValue = null;
            comp.MaxValue = null;
            comp.CompareTo = null;
            comp.TertiaryOperator = TertiaryOp.Between;

            return comp;
        }

        #endregion

        #region internal static DBComparison Compare()

        internal static DComparison Compare()
        {
            DBinaryComparison comp = new DBinaryComparison();
            comp.Left = null;
            comp.Right = null;
            comp.CompareOperator = DDDD.Core.Data.DA2.Query.Compare.Equals;

            return comp;
        }

        #endregion




        #region  ------------------------ Compare Two  DBCompare Objects ---------------------------

        /// <summary>
        /// False- ��� ��������� �� �����
        /// True- �������� ��� ��������� ���������
        /// </summary>
        /// <param name="AnotherObject"></param>
        /// <returns></returns>
        public override bool CompareWith(DClause AnotherObject)
        {            
            bool compareResult  = true;//������� �� ������� ��� ������� ���������

            if ((AnotherObject as DComparison) == null) { compareResult = false; return compareResult; } // ������ ���� ������ ������������ ������� ����� null � ������ �� ����� �� null  �� ���� ����� ��� �����������
            DComparison anComparison = AnotherObject as DComparison;
            
            //�.�. ���� ������� ��� ������� �������� �� �� ������ �������� �� �������� ������� ���� ������
            if (GetType().Name != anComparison.GetType().Name) { compareResult = false; return compareResult; } //����� ������ ��������� ��� ����� ����� � ����� �������� ������
            
            
            //������ ��� ������ ���� � ����� ������ �������� �� �����
            if (this is DBinaryComparison )
            {
                if ((this as DBinaryComparison).Left.CompareWith( (anComparison as DBinaryComparison).Left) == false) { compareResult = false; return compareResult; }
                else if ((this as DBinaryComparison).Right.CompareWith((anComparison as DBinaryComparison).Right) == false) { compareResult = false; return compareResult; }
                else if ((this as DBinaryComparison).CompareOperator != (anComparison as DBinaryComparison).CompareOperator) { compareResult = false; return compareResult; }
            }

            else if (this is DTertiaryComparison)
            {
                if ((this as DTertiaryComparison).MinValue.CompareWith((anComparison as DTertiaryComparison).MinValue) == false) { compareResult = false; return compareResult; }
                else if ((this as DTertiaryComparison).MaxValue.CompareWith((anComparison as DTertiaryComparison).MaxValue) == false) { compareResult = false; return compareResult; }
                else if ((this as DTertiaryComparison).TertiaryOperator != (anComparison as DTertiaryComparison).TertiaryOperator) { compareResult = false; return compareResult; }
            }

            else if (this is DUnaryComparison)
            {
                if ((this as DUnaryComparison).UnaryOperator != (anComparison as DUnaryComparison).UnaryOperator) { compareResult = false; return compareResult; }
                else if ((this as DUnaryComparison).ToOperateOn.CompareWith((anComparison as DUnaryComparison).ToOperateOn) == false) { compareResult = false; return compareResult; }                
            }


            return compareResult;
        }


        #endregion ------------------------ Compare Two  DBCompare Objects ---------------------------
        




        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DComparison();
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        



    }

    /// <summary>
    /// internal implementation of an Unary comparison (one operand).
    /// </summary>
    [DataContract]
    public partial class DUnaryComparison : DComparison
    {
        public DUnaryComparison() { }

        #region public UnaryOp UnaryOperator {get;set;}

        private UnaryOp _unaryop;
        /// <summary>
        /// The Unary operator this comparison uses
        /// </summary>
        [DataMember]
        public UnaryOp UnaryOperator
        {
            get { return _unaryop; }
            set { _unaryop = value; }
        }

        #endregion

        #region public DBClause ToOperateOn {get;set;}

        private DClause _toop;

        /// <summary>
        /// The clause that will have the operation applied to
        /// </summary>
        [DataMember]
        public DClause ToOperateOn
        {
            get { return _toop; }
            set { _toop = value; }
        }

        #endregion
        

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DUnaryComparison()
            {
              ToOperateOn = (this.ToOperateOn != null) ? this.ToOperateOn.CloneClause() : null,
              UnaryOperator = this.UnaryOperator                
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
               

        //
        // SQL Statement builder
        //
        
        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this.ToOperateOn != null)
            {
                builder.BeginBlock();
                builder.WriteOperator((Operator)Enum.Parse(typeof(Operator), this.UnaryOperator.ToString(), false));
                this.ToOperateOn.BuildStatement(builder);
                builder.EndBlock();
                return true;
            }
            else
                return false;
        }

        #endregion
        
        //
        // XML Serialization
        //
 
        //#region protected override string XmlElementName {get;}

        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.UnaryOp; }
        //}

        //#endregion
              

        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    this.WriteAttribute(writer, XmlHelper.Operator, this.UnaryOperator.ToString(), context);

        //    return base.WriteAllAttributes(writer, context);
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    this.ToOperateOn.WriteXml(writer, context);
        //    return true;
        //}

        //#endregion

        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b = true;

        //    if (this.IsAttributeMatch(XmlHelper.Operator, reader, context))
        //        this.UnaryOperator = (UnaryOp)Enum.Parse(typeof(UnaryOp), reader.Value, true);
        //    else
        //        b = base.ReadAnAttribute(reader, context);

        //    return b;
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    this.ToOperateOn = this.ReadNextInnerClause(this.XmlElementName, reader, context);
        //    return true;
        //}

        //#endregion

    }

    /// <summary>
    /// internal implementation of a binary comparison (2 operands)
    /// </summary>
    [DataContract]
    public partial class DBinaryComparison : DComparison
    {

        public DBinaryComparison() { }

        #region public DBClause Left {get; set;}

        private DClause _left;
        
        [DataMember]
        public DClause Left
        {
            get { return _left; }
            set { _left = value; }
        }

        #endregion

        #region public DBClause Right {get; set;}

        private DClause _right;
        
        [DataMember]
        public DClause Right
        {
            get { return _right; }
            set { _right = value; } //internal
        }

        #endregion

        #region public Compare CompareOperator {get; set;}

        private Compare _op;        
        
        [DataMember]
        public Compare CompareOperator
        {
            get { return _op; }
            set { _op = value; } //internal
        }

        #endregion

        //
        // SQL Statement builder
        //



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DBinaryComparison()
            { 
                 CompareOperator = this.CompareOperator,
                 Left = (this.Left != null) ? this.Left.CloneClause() : null,
                 Right= (this.Right != null) ? this.Right.CloneClause() : null               
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        




        
        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginBlock();
            this.Left.BuildStatement(builder);
            builder.WriteOperator((Operator)Enum.Parse(typeof(Operator), this.CompareOperator.ToString(), false));

            if ((Operator)Enum.Parse(typeof(Operator), this.CompareOperator.ToString(), false) == Operator.Like && (this.Right as DConst) != null )
            { (this.Right as DConst).BuildLikeRightStatement(builder); } // for LIKE Operator

            else this.Right.BuildStatement(builder);
            builder.EndBlock();

            return true;
        }

        #endregion
        

        //
        // XML Serialization
        //
        //#region protected override string XmlElementName {get;}

        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Compare; }
        //}

        //#endregion

        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        
        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    this.WriteAttribute(writer, XmlHelper.Operator, this.CompareOperator.ToString(), context);

        //    return base.WriteAllAttributes(writer, context);
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.Left != null)
        //    {
        //        this.WriteStartElement(XmlHelper.LeftOperand, writer, context);
        //        this.Left.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.LeftOperand, writer, context);
        //    }
        //    if (this.Right != null)
        //    {
        //        this.WriteStartElement(XmlHelper.RightOperand, writer, context);
        //        this.Right.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.RightOperand, writer, context);
        //    }
        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b = true;
        //    if (this.IsAttributeMatch(XmlHelper.Operator, reader, context))
        //        this.CompareOperator = (Compare)Enum.Parse(typeof(Compare), reader.Value, true);
        //    else
        //        b = base.ReadAnAttribute(reader, context);

        //    return b;
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b = false;
        //    if (this.IsElementMatch(XmlHelper.LeftOperand, reader, context)
        //         && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this.Left = ReadNextInnerClause(XmlHelper.LeftOperand, reader, context);
        //    }
        //    else if (this.IsElementMatch(XmlHelper.RightOperand, reader, context)
        //        && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this.Right = this.ReadNextInnerClause(XmlHelper.RightOperand, reader, context);
        //    }
        //    else
        //        b = base.ReadAnInnerElement(reader, context);

        //    return b;
        //}

        
        //#endregion

    }

    /// <summary>
    /// internal implementation of a tertiary comparison (3 operands)
    /// </summary>
    [DataContract]
    public partial class DTertiaryComparison : DComparison
    {
      
        public DTertiaryComparison(){}

        //
        // public properties
        //

        #region public DBClause CompareTo {get;set;}

        private DClause _comp;
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DClause CompareTo
        {
            get { return _comp; }
            set { _comp = value; }
        }

        #endregion

        #region public DBClause MaxValue {get;set}

        private DClause _max;
        
        [DataMember]
        public DClause MaxValue
        {
            get { return _max; }
            set { _max = value; }
        }

        #endregion

        #region public DBClause MinValue {get;set;}

        private DClause _min;
        
        [DataMember]
        public DClause MinValue
        {
            get { return this._min; }
            set { this._min = value; }
        }

        #endregion

        #region public TertiaryOp TertiaryOperator

        private TertiaryOp _op;

        [DataMember]
        public TertiaryOp TertiaryOperator
        {
            get { return _op; }
            internal set { _op = value; }
        }

        #endregion
        

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DTertiaryComparison()
                {
                     CompareTo = this.CompareTo,
                     MaxValue = (this.MaxValue != null) ? this.MaxValue.CloneClause() : null,
                     MinValue = (this.MinValue != null) ? this.MaxValue.CloneClause() : null,
                      TertiaryOperator = this.TertiaryOperator
                };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
               
        

        //
        // SQL Statement builder
        //
        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginBlock();
            this.CompareTo.BuildStatement(builder);
            builder.BeginTertiaryOperator((Operator)Enum.Parse(typeof(Operator), this.TertiaryOperator.ToString(), false));
            this.MinValue.BuildStatement(builder);
            builder.ContinueTertiaryOperator((Operator)Enum.Parse(typeof(Operator), this.TertiaryOperator.ToString(), false));
            this.MaxValue.BuildStatement(builder);
            builder.EndTertiaryOperator((Operator)Enum.Parse(typeof(Operator), this.TertiaryOperator.ToString(), false));
            builder.EndBlock();

            return true;
        }

        #endregion


        ////
        //// XML Serialization
        ////
        //#region protected override string XmlElementName {get;}

        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Between; }
        //}

        //#endregion


        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    this.WriteAttribute(writer, XmlHelper.Operator, this.TertiaryOperator.ToString(), context);

        //    return base.WriteAllAttributes(writer, context);
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.CompareTo != null)
        //    {
        //        this.WriteStartElement(XmlHelper.Compare, writer, context);
        //        this.CompareTo.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.Compare, writer, context);
        //    }
        //    if (this.MinValue != null)
        //    {
        //        this.WriteStartElement(XmlHelper.MinValue, writer, context);
        //        this.MinValue.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.MinValue, writer, context);
        //    }
        //    if (this.MaxValue != null)
        //    {
        //        this.WriteStartElement(XmlHelper.MaxValue, writer, context);
        //        this.MaxValue.WriteXml(writer, context);
        //        this.WriteEndElement(XmlHelper.MaxValue, writer, context);
        //    }
        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b = false;
        //    if (this.IsElementMatch(XmlHelper.Compare, reader, context)
        //         && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this.CompareTo = this.ReadNextInnerClause(XmlHelper.Compare, reader, context);
        //        b = (this.CompareTo != null);
        //    }
        //    else if (this.IsElementMatch(XmlHelper.MinValue, reader, context)
        //             && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this.MinValue = this.ReadNextInnerClause(XmlHelper.MinValue, reader, context);
        //        b = (this.MinValue != null);
        //    }
        //    else if (this.IsElementMatch(XmlHelper.MaxValue, reader, context)
        //                && !reader.IsEmptyElement && reader.Read())
        //    {
        //        this.MaxValue = this.ReadNextInnerClause(XmlHelper.MaxValue, reader, context);
        //        b = (this.MaxValue != null);
        //    }
        //    else
        //        b = base.ReadAnInnerElement(reader, context);

        //    return b;
        //}

        //#endregion

    }
}
