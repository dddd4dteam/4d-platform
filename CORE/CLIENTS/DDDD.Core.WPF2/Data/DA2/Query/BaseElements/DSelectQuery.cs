
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// The DBSelectQuery encapsulates a select query  SQL Statement.
    /// </summary>
    [DataContract]
    public partial class DSelectQuery : DQuery
    {

        //
        // .ctors
        //
        #region public  DSelectQuery() //internal
        /// <summary>
        ///  constructor - use the DBQuery.Select... methods to create a new instance //internal
        /// </summary>
        public DSelectQuery()
        { }

        #endregion


        //
        // ivars
        //

        private DTableSet _root;

        private DSelectSet _select;
        private DClause _top;


        private DFilterSet _where;
        private DFilterSet _having;
        private DOrderSet _order;
        private DGroupBySet _grpby;
        private DClause _last;
        private bool _distinct = false;


        //
        // properties
        //




        #region ------------- public Table -----------------

        /// <summary>
        /// ��� ��������� �������/������. ���� null ������ ��������� ��� ��������� ������
        /// ������������ ��� �����������- ��������� ������ ��� �� ���������/ ��� �� ��� ������ � ��������� � �������
        /// </summary>
        public DTable LastTable
        {
            get
            {
                if (_root.HasRoot && _root.Root is DTable) return ((DTable)_root.Root);
                return null;
            }
        }

        /// <summary>
        /// ��� �������/������ �� ������� ����� �������� ������ -  
        /// ����������� ��� ������ From. ���������� ����� ������� ������ � ������ ���� ��������� ���������� DataSet, ���������� �� ���������� ����������� �������� �� ��������. 
        /// </summary>
        [DataMember]
        public DTable SelectionTable
        {
            get;
            set;
        }


        #endregion  -------------------  public Table  -----------------





        #region public DBSelectSet SelectSet {get;set;}

        /// <summary>
        /// Gets or Sets the DBSelectSet for this DBSelectQuery
        /// </summary>
        [DataMember]
        public DSelectSet SelectSet //internal
        {
            get { return this._select; }
            set { this._select = value; }
        }

        #endregion


        #region public DBTableSet RootSet {get;set;} //internal

        /// <summary>
        /// Gets or Sets the DBTableSet for this DBSelectQuery
        /// </summary>
        [DataMember]
        public DTableSet RootSet //internal
        {
            get { return this._root; }
            set { this._root = value; }
        }

        #endregion


        #region public DBFilterSet WhereSet {get;set;}//internal

        /// <summary>
        /// Gets or Sets the DBFilterSet for this DBSelectQuery
        /// </summary>
        [DataMember]
        public DFilterSet WhereSet //internal
        {
            get { return this._where; }
            set { this._where = value; }
        }

        #endregion


        #region public DBOrderSet OrderSet {get;set;}// internal

        /// <summary>
        /// Gets or sets the DBOrderBySet for this DBSelectQuery
        /// </summary>
        [DataMember]
        public DOrderSet OrderSet //internal
        {
            get { return this._order; }
            set { this._order = value; }
        }

        #endregion


        #region public DBGroupBySet GroupBySet {get;set;}//internal 

        /// <summary>
        /// Gets or Sets the DBGroupbySet for this DBSelectQuery
        /// </summary>
        [DataMember]
        public DGroupBySet GroupBySet //internal
        {
            get { return this._grpby; }
            set { this._grpby = value; }
        }

        #endregion


        #region public  DBClause Last {get;set;} //internal

        /// <summary>
        /// Gets or sets the last DBClause used in statement construction
        /// </summary>
        [DataMember]
        public DClause Last //internal
        {
            get { return this._last; }
            set { this._last = value; }
        }

        #endregion


        #region public DBClause Top {get; set;} //internal

        /// <summary>
        /// Gets or Sets the DBClause for the Top statement
        /// </summary>
        [DataMember]
        public DClause Top  //internal
        {
            get { return this._top; }
            set { this._top = value; }
        }

        #endregion


        #region internal bool IsDistinct {get;set;}

        /// <summary>
        /// Gets or sets the flag to identify if this is a distinct query
        /// </summary>        
        [DataMember]
        public bool IsDistinct //internal
        {
            get { return this._distinct; }
            set { this._distinct = value; }
        }

        #endregion



        //
        // build statement methods
        //



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DSelectQuery()
            {
                GroupBySet = (this.GroupBySet != null) ? this.GroupBySet.CloneClause() as DGroupBySet : null,
                IsDistinct = this.IsDistinct,
                OrderSet = (this.OrderSet != null) ? this.OrderSet.CloneClause() as DOrderSet : null,
                RootSet = (this.RootSet != null) ? this.RootSet.CloneClause() as DTableSet : null,
                SelectionTable = (this.SelectionTable != null) ? this.SelectionTable.CloneClause() as DTable : null,
                Top = (this.Top != null) ? this.Top.CloneClause() : null,
                SelectSet = (this.SelectSet != null) ? this.SelectSet.CloneClause() as DSelectSet : null,
                IsInnerQuery = this.IsInnerQuery,
                WhereSet = (this.WhereSet != null) ? this.WhereSet.CloneClause() as DFilterSet : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null,

                //LastTable = (this.LastTable != null) ? this.LastTable.CloneClause() as DBTable : null,

            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------





        #region public override bool BuildStatement(DBStatementBuilder builder)


        /// <summary>
        /// Overrides the abstract base declaration to create a new Select query
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            this.ValidateStatement();

            builder.BeginSelectStatement();

            if (this.IsDistinct)
                builder.WriteDistinct();

            if (this._top != null)
                this._top.BuildStatement(builder);

            this._select.BuildStatement(builder);

            if (_root != null)
            {
                builder.BeginFromList();
                this._root.BuildStatement(builder);
                builder.EndFromList();
            }
            if (_where != null)
            {
                builder.BeginWhereStatement();
                this._where.BuildStatement(builder);
                builder.EndWhereStatement();
            }

            if (_grpby != null)
            {
                builder.BeginGroupByStatement();
                this._grpby.BuildStatement(builder);
                builder.EndGroupByStatement();
            }

            if (_having != null)
            {
                builder.BeginHavingStatement();
                this._having.BuildStatement(builder);
                builder.EndHavingStatement();
            }

            if (_order != null)
            {
                builder.BeginOrderStatement();
                this._order.BuildStatement(builder);
                builder.EndOrderStatement();
            }

            builder.EndSelectStatement();

            return true;
        }

        #endregion

        #region protected virtual void ValidateStatement()
        /// <summary>
        /// validates the statement as it is - minimum requirements are at least one return column
        /// </summary>
        public virtual void ValidateStatement()
        {
            if (this._select == null || this._select.Results.Count == 0)
                throw new Exception("No Fields were specified to be selected");
        }

        #endregion

        //
        // xml serialization methods
        //

        //#region protected override string XmlElementName {get;}

        ///// <summary>
        ///// Gets the name of this instance
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Select;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Overrides the default implmentation to write the Distinct value attribute
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.IsDistinct)
        //        this.WriteAttribute(writer, XmlHelper.Distinct, bool.TrueString, context);

        //    return base.WriteAllAttributes(writer, context);
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Overrides the base implementation to write the inner elements 
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.Top != null)
        //        this.Top.WriteXml(writer, context);

        //    if (this.SelectSet != null)
        //    {
        //        this.SelectSet.WriteXml(writer, context);
        //    }
        //    if (this.RootSet != null)
        //    {
        //        this.RootSet.WriteXml(writer, context);
        //    }
        //    if (this.WhereSet != null)
        //    {
        //        this.WhereSet.WriteXml(writer, context);
        //    }
        //    if (this.GroupBySet != null)
        //    {
        //        this.GroupBySet.WriteXml(writer, context);
        //    }
        //    if (this.OrderSet != null)
        //    {
        //        this.OrderSet.WriteXml(writer, context);
        //    }

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        ////
        //// xml de-seriaization methods
        ////

        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Overrides the base implementation to read and assign the DBSelectQuery attribute(s)
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool distinct;
        //    if (this.IsAttributeMatch(XmlHelper.Distinct, reader, context) && bool.TryParse(reader.Value, out distinct))
        //    {
        //        this.IsDistinct = distinct;
        //        return true;
        //    }
        //    else
        //        return base.ReadAnAttribute(reader, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Reads one of the inner elements from the xml being de-serialized 
        ///// and if recognized - deserializes and assigns it to the correct property in this DBSelectQuery
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b;
        //    switch (reader.LocalName)
        //    {
        //        case (XmlHelper.Top):
        //            this.Top = context.Factory.Read(XmlHelper.Top, reader, context);
        //            b = true;
        //            break;
        //        case (XmlHelper.Fields):
        //            this.SelectSet = context.Factory.Read(XmlHelper.Fields, reader, context) as DBSelectSet;
        //            b = true;
        //            break;
        //        case (XmlHelper.From):
        //            this.RootSet = context.Factory.Read(XmlHelper.From, reader, context) as DBTableSet;
        //            b = true;
        //            break;
        //        case (XmlHelper.Where):
        //            this.WhereSet = context.Factory.Read(XmlHelper.Where, reader, context) as DBFilterSet;
        //            b = true;
        //            break;
        //        case (XmlHelper.Group):
        //            this.GroupBySet = context.Factory.Read(XmlHelper.Group, reader, context) as DBGroupBySet;
        //            b = true;
        //            break;
        //        case (XmlHelper.Order):
        //            this.OrderSet = context.Factory.Read(XmlHelper.Order, reader, context) as DBOrderSet;
        //            b = true;
        //            break;
        //        default:
        //            b = base.ReadAnInnerElement(reader, context);
        //            break;
        //    }
        //    return b;
        //}

        //#endregion



        //
        // Statement Construction methods
        //




        //
        // Top, Distinct
        //

        #region public DBSelectQuery TopN(int count)
        /// <summary>
        /// Restricts the return results to the specified count
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public DSelectQuery TopN(int count)
        {
            DTop top = DTop.Number(count);
            this.Top = top;

            return this;
        }

        #endregion

        #region public DBSelectQuery TopPercent(double count)
        /// <summary>
        /// Restricts the return results to the specified percentage of the total results
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        public DSelectQuery TopPercent(double count)
        {
            DTop top = DTop.Percent(count);
            this.Top = top;

            return this;
        }

        #endregion

        #region public DBSelectQuery Distinct()
        /// <summary>
        /// Restricts the return results to only unique results
        /// </summary>
        /// <returns></returns>
        public DSelectQuery Distinct()
        {
            this.IsDistinct = true;
            return this;
        }

        #endregion

        //
        // aggregate
        //

        #region public DBSelectQuery Sum(string _ModuleInitializer) + 3 overloads

        /// <summary>
        /// Adds an aggregate SUM([_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Sum(string field)
        {
            DField fld = DField.Field(field);
            return Sum(fld);
        }

        /// <summary>
        /// Adds an aggregate SUM([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Sum(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return Sum(fld);
        //}

        public DSelectQuery Sum(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return Sum(fld);
        }


        public DSelectQuery Sum<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Sum(fld);
        }




        /// <summary>
        /// Adds an aggregate SUM([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Sum(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return Sum(fld);
        //}

        public DSelectQuery Sum(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Sum(fld);
        }


        public DSelectQuery Sum<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Sum(fld);
        }




        /// <summary>
        /// Adds an aggregate SUM(clause) to the select columns
        /// </summary>
        /// <param name="clause">Any individual or combination of sql clauses</param>
        /// <returns></returns>
        public DSelectQuery Sum(DClause clause)
        {
            return Aggregate(AggregateFunction.Sum, clause);
        }

        #endregion

        #region public DBSelectQuery Avg(string _ModuleInitializer) + 3 overloads

        /// <summary>
        /// Adds an aggregate AVG([_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Avg(string field)
        {
            DField fld = DField.Field(field);
            return Avg(fld);
        }

        /// <summary>
        /// Adds an aggregate AVG([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Avg(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return Avg(fld);
        //}


        /// <summary>
        /// Adds an aggregate AVG([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Avg(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return Avg(fld);
        }


        /// <summary>
        /// Adds an aggregate AVG([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Avg<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Avg(fld);
        }



        /// <summary>
        /// Adds an aggregate AVG([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Avg(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field( owner , table , field );
        //    return Avg(fld);
        //}

        /// <summary>
        /// Adds an aggregate AVG([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Avg(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Avg(fld);
        }

        /// <summary>
        /// Adds an aggregate AVG([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Avg<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Avg(fld);
        }



        /// <summary>
        /// Adds an aggregate AVG([clause]) to the select columns
        /// </summary>
        /// <param name="clause">Any individual or combination of clauses</param>
        /// <returns></returns>
        public DSelectQuery Avg(DClause clause)
        {
            return Aggregate(AggregateFunction.Avg, clause);
        }

        #endregion

        #region public DBSelectQuery Min(string _ModuleInitializer) + 3 overloads
        /// <summary>
        ///  Adds an aggregate MIN([_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Min(string field)
        {
            DField fld = DField.Field(field);
            return Min(fld);
        }

        /// <summary>
        ///  Adds an aggregate MIN([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        //public DBSelectQuery Min(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return Min(fld);
        //}

        /// <summary>
        /// Adds an aggregate MIN([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Min(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return Min(fld);
        }

        /// <summary>
        /// Adds an aggregate MIN([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Min<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Min(fld);
        }





        /// <summary>
        ///  Adds an aggregate MIN([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Min(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return Min(fld);
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Min(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Min(fld);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Min<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Min(fld);
        }


        /// <summary>
        /// Adds an aggregate MIN([clause]) to the select columns
        /// </summary>
        /// <param name="clause">Any individual or combination of clauses</param>
        /// <returns></returns>
        public DSelectQuery Min(DClause clause)
        {
            return Aggregate(AggregateFunction.Min, clause);
        }

        #endregion

        #region public DBSelectQuery Max(string _ModuleInitializer) + 3 overloads

        /// <summary>
        /// Adds an aggregate MAX([_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Max(string field)
        {
            DField fld = DField.Field(field);
            return Max(fld);
        }

        /// <summary>
        /// Adds an aggregate MAX([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Max(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return Max(fld);
        //}

        /// <summary>
        /// Adds an aggregate MAX([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Max(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return Max(fld);
        }

        /// <summary>
        /// Adds an aggregate MAX([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Max<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Max(fld);
        }




        /// <summary>
        /// Adds an aggregate MAX([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Max(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return Max(fld);
        //}

        /// <summary>
        /// Adds an aggregate MAX([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Max(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Max(fld);
        }

        /// <summary>
        /// Adds an aggregate MAX([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Max<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Max(fld);
        }



        /// <summary>
        /// Adds an aggregate MAX([clause]) to the select columns
        /// </summary>
        /// <param name="clause">Any individual or combination of clauses</param>
        /// <returns></returns>
        public DSelectQuery Max(DClause clause)
        {
            return Aggregate(AggregateFunction.Max, clause);
        }

        #endregion


        #region public DBSelectQuery Count() + 4 overloads

        /// <summary>
        /// Adds an aggregate COUNT(*) to the select columns
        /// </summary>
        /// <returns></returns>
        public DSelectQuery Count()
        {
            return Count(DField.AllFields());
        }

        /// <summary>
        /// Adds an aggregate COUNT([_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Count(string field)
        {
            DField fld = DField.Field(field);
            return Count(fld);
        }

        /// <summary>
        /// Adds an aggregate COUNT([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Count(string table, string field)
        //{
        //    DBField fld = DBField.Field(table,field);
        //    return Count(fld);
        //}

        /// <summary>
        /// Adds an aggregate COUNT([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Count(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return Count(fld);
        }

        /// <summary>
        /// Adds an aggregate COUNT([table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Count<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return Count(fld);
        }


        /// <summary>
        /// Adds an aggregate COUNT([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Count(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return Count(fld);
        //}

        /// <summary>
        /// Adds an aggregate COUNT([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Count(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return Count(fld);
        }


        /// <summary>
        /// Adds an aggregate COUNT([owner].[table].[_ModuleInitializer]) to the select columns
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Count<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return Count(fld);
        }



        /// <summary>
        /// Adds an aggregate COUNT([clause]) to the select columns
        /// </summary>
        /// <param name="clause">Any individual or combination of clauses</param>
        /// <returns></returns>
        public DSelectQuery Count(DClause clause)
        {
            return Aggregate(AggregateFunction.Count, clause);
        }

        #endregion




        #region  ------------------------------ PAGEABLE  QUERY SQL Server -----------------------


        #region  --------------------- PageableQuery(DBSelectQuery query, Int32 PageSize, Int32 SkipPages, string OrderField) --------------------

        /// <summary>
        /// ������� ������ ���������� ���������� ��������. OrderField - ����������� ������� ��� ������� ���������
        /// </summary>
        /// <param name="query"></param>
        /// <param name="pageSize"></param>
        /// <param name="SkipPages"></param>
        private static void PageableQuery(ref DSelectQuery query, Int32 PageSize, Int32 SkipPages, string OrderField)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";

            Validator.ATNullReferenceArgDbg<DTable>(query.SelectionTable
                , nameof(PageableQuery), "SelectionTable  in PagableQuery");

            DTable SelectionTable = query.SelectionTable.CloneClause() as DTable;

            query = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(query).As("PageQuery");
            //query 

            //Pageable query 
            query = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(query).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            query.SelectionTable = SelectionTable;

        }



        /// <summary> 
        /// ��� ������ ��������� �������� ��������� - ����� ��� ���������� �������� ������ ������� ������� �������� �����������         ///
        /// </summary>
        /// <param name="query"></param>
        /// <param name="pageSize"></param>
        /// <param name="SkipPages"></param>
        private static DSelectQuery PageableQuery(DSelectQuery query, Int32 PageSize, Int32 SkipPages, string OrderField)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";
            DSelectQuery queryClone = query.CloneClause() as DSelectQuery;

            Validator.ATNullReferenceArgDbg<DTable>(queryClone.SelectionTable
               , nameof(PageableQuery), "SelectionTable");

            DTable SelectionTable = query.SelectionTable.CloneClause() as DTable;


            queryClone = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(queryClone).As("PageQuery");
            //query 

            //Pageable query 
            queryClone = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(queryClone).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            queryClone.SelectionTable = SelectionTable;
            return queryClone;
        }

        #endregion  --------------------- PageableQuery(DBSelectQuery query, Int32 PageSize, Int32 SkipPages, string OrderField) --------------------



        #region  --------------------- PageableQuery( DBSelectQuery query, Int32 PageSize, Int32 SkipPages, Type Contract) --------------------


        /// <summary>
        /// ������� ������ ���������� ���������� ��������. OrderField - ���������� �� ���������
        /// </summary>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <param name="ContractName"></param>
        private static void PageableQuery(ref DSelectQuery query, Int32 PageSize, Int32 SkipPages, Type Contract)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";
            string OrderField = Contract.GetProperties()[0].Name;

            Validator.ATNullReferenceArgDbg<DTable>(query.SelectionTable, nameof(PageableQuery), "SelectionTable");


            DTable SelectionTable = query.SelectionTable.CloneClause() as DTable;

            query = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(query).As("PageQuery");

            //Pageable query 
            query = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(query).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            query.SelectionTable = SelectionTable;
            //return query;
        }


        /// <summary>
        /// ��� ������ ��������� �������� ��������� - ����� ��� ���������� �������� ������ ������� ������� �������� �����������         ///  
        /// </summary>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <param name="ContractName"></param>
        private static DSelectQuery PageableQuery(DSelectQuery query, Int32 PageSize, Int32 SkipPages, Type Contract)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";
            string OrderField = Contract.GetProperties()[0].Name;

            DSelectQuery queryClone = query.CloneClause() as DSelectQuery;

            Validator.ATNullReferenceArgDbg<DTable>(queryClone.SelectionTable
                , nameof(PageableQuery), "SelectionTable");

            DTable SelectionTable = queryClone.SelectionTable.CloneClause() as DTable;

            queryClone = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(queryClone).As("PageQuery");

            //Pageable query 
            queryClone = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(queryClone).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            queryClone.SelectionTable = SelectionTable;
            return queryClone;
        }

        #endregion  --------------------- PageableQuery( DBSelectQuery query, Int32 PageSize, Int32 SkipPages, Type Contract) --------------------



        #region  --------------------- PageableQuery (DBSelectQuery query, Int32 PageSize, Int32 SkipPages) --------------------


        /// <summary>
        /// ������� ������ ���������� ���������� ��������. OrderField - ���������� �� ���������
        /// </summary>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <returns></returns>
        public static void PageableQuery(ref DSelectQuery query, Int32 PageSize, Int32 SkipPages)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";

            Validator.ATNullReferenceArgDbg<DTable>(query.SelectionTable
                , nameof(PageableQuery), "SelectionTable");

            string OrderField = query.SelectionTable.TableType.GetProperties()[0].Name;
            DTable SelectionTable = query.SelectionTable.CloneClause() as DTable;

            query = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(query).As("PageQuery");

            //Pageable query 
            query = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(query).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            query.SelectionTable = SelectionTable;
            //return query;
        }


        /// <summary>
        /// ��� ������ ��������� �������� ��������� - ����� ��� ���������� �������� ������ ������� ������� �������� �����������
        /// </summary>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <returns></returns>
        public static DSelectQuery PageableQuery(DSelectQuery query, Int32 PageSize, Int32 SkipPages)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";

            DSelectQuery queryClone = query.CloneClause() as DSelectQuery;

            Validator.ATNullReferenceArgDbg<DTable>(queryClone.SelectionTable
                , nameof(PageableQuery), "SelectionTable");

            string OrderField = queryClone.SelectionTable.TableType.GetProperties()[0].Name;
            DTable SelectionTable = queryClone.SelectionTable.CloneClause() as DTable;

            queryClone = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(queryClone).As("PageQuery");

            //Pageable query 
            queryClone = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(queryClone).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            queryClone.SelectionTable = SelectionTable;
            return queryClone;
        }


        #endregion --------------------- PageableQuery (DBSelectQuery query, Int32 PageSize, Int32 SkipPages) --------------------



        #region  --------------------- PageableQuery<TContract>( DBSelectQuery query, Int32 PageSize, Int32 SkipPages) --------------------



        /// <summary>
        /// ������� ������ ���������� ���������� ��������. OrderField - ���������� �� ��������� 
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <returns></returns>
        private static void PageableQuery<TContract>(ref DSelectQuery query, Int32 PageSize, Int32 SkipPages)
        {
            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";
            string OrderField = typeof(TContract).GetProperties()[0].Name;

            Validator.ATNullReferenceArgDbg<DTable>(query.SelectionTable
                , nameof(PageableQuery), "SelectionTable");

            DTable SelectionTable = query.SelectionTable.CloneClause() as DTable;


            query = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(query).As("PageQuery");


            //Pageable query 
            query = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(query).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            query.SelectionTable = SelectionTable;

        }

        /// <summary>
        /// ��� ������ ��������� �������� ��������� - ����� ��� ���������� �������� ������ ������� ������� �������� �����������
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <param name="query"></param>
        /// <param name="PageSize"></param>
        /// <param name="SkipPages"></param>
        /// <returns></returns>
        private static DSelectQuery PageableQuery<TContract>(DSelectQuery query, Int32 PageSize, Int32 SkipPages)
        {
            DSelectQuery queryClone = query.CloneClause() as DSelectQuery;

            Int32 SkipPagesItems = SkipPages * PageSize;
            string rownumberAlias = "rownumber";
            string OrderField = typeof(TContract).GetProperties()[0].Name;

            Validator.ATNullReferenceArgDbg<DTable>(queryClone.SelectionTable
                , nameof(PageableQuery), "SelectionTable");

            DTable SelectionTable = queryClone.SelectionTable.CloneClause() as DTable;


            queryClone = DSelectQuery.SelectAll().SelectRowNumber(OrderField, rownumberAlias).From(queryClone).As("PageQuery");


            //Pageable query 
            queryClone = DSelectQuery.SelectAll().TopN(PageSize).  //DBSelectQuery queryPageable
                 From(queryClone).As("Query").
                 Where(rownumberAlias, Compare.GreaterThan, DConst.Int32(SkipPagesItems));

            queryClone.SelectionTable = SelectionTable;

            return queryClone;
        }

        #endregion  --------------------- PageableQuery<TContract>( DBSelectQuery query, Int32 PageSize, Int32 SkipPages) --------------------



        #endregion ------------------------------ PAGEABLE  QUERY [SQL Server] -----------------------





        #region public DBSelectQuery Aggregate(AggregateFunction func, DBClause dbref)

        /// <summary>
        /// Adds an aggregate function to the select list (or the last clause).
        /// </summary>
        /// <param name="func">The aggregate function</param>
        /// <param name="dbref">The clause to aggregate</param>
        /// <returns>Itself to support chaining of statements</returns>
        public DSelectQuery Aggregate(AggregateFunction func, DClause dbref)
        {
            if (_last == null)
            {
                if (_select == null)
                    _select = DSelectSet.SelectAggregate(func, dbref);
                else
                    _select = _select.And(dbref);
                _last = _select;
            }
            else if (_last is IDAggregate)
                _last = ((IDAggregate)_last).Aggregate(func, dbref);
            else
                throw new InvalidOperationException("The current clause does not support aggregate operations");

            return this;
        }

        #endregion

        //
        // _ModuleInitializer
        //

        #region public DBSelectQuery Field(DBReference _ModuleInitializer) + 3 Overloads

        /// <summary>
        /// Adds a clause to the select list (or the last clause in the statement)
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Field(DClause clause)
        {
            if (this._last == null)
            {
                _select = DSelectSet.Select(clause);
                _last = _select;
            }
            else if (_last is IDBoolean)
                _last = ((IDBoolean)_last).And(clause);
            return this;
        }

        /// <summary>
        /// Adds a [_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Field(string field)
        {
            DField fld = DField.Field(field);
            return this.Field(fld);
        }

        /// <summary>
        /// Adds a [table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Field(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return this.Field(fld);
        //}

        /// <summary>
        /// Adds a [table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Field(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return this.Field(fld);
        }

        /// <summary>
        /// Adds a [table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Field<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return this.Field(fld);
        }




        /// <summary>
        /// Adds a [owner].[table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Field(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return this.Field(fld);
        //}

        /// <summary>
        /// Adds a [owner].[table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Field(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return this.Field(fld);
        }

        /// <summary>
        /// Adds a [owner].[table].[_ModuleInitializer] clause to the select list, or the last clause in the statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Field<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return this.Field(fld);
        }


        #endregion

        #region public DBSelectQuery Fields(params string[] fieldnames)

        /// <summary>
        /// Adds a set of fields to the select list, or the last clause in the statement
        /// </summary>
        /// <param name="fieldnames">The names of the fields to the list</param>
        /// <returns></returns>
        public DSelectQuery Fields(params string[] fieldnames)
        {
            DSelectQuery last = this;

            foreach (string name in fieldnames)
            {
                DField fld = DField.Field(name);
                last = last.Field(fld);
            }

            return last;
        }

        #endregion

        //
        // From, Join, On
        //
        #region public DBQuery From(string table) + 2 overloads

        /// <summary>
        /// Specifies the root table or view to select data from in this statement
        /// </summary>
        /// <param name="table">The name of the table</param>
        /// <returns></returns>
        //public DBSelectQuery From(string table)
        //{
        //    DBTable tr = DBTable.Table(table);
        //    return From(tr);
        //}

        /// <summary>
        /// Specifies the root table or view to select data from in this statement
        /// </summary>
        /// <param name="table">The name of the table</param>
        /// <returns></returns>
        public DSelectQuery From(Type TTable)
        {
            DTable tr = DTable.Table(TTable);
            SelectionTable = tr.CloneClause() as DTable;
            return From(tr);
        }

        /// <summary>
        /// Specifies the root table or view to select data from in this statement
        /// </summary>
        /// <typeparam name="TType"></typeparam>
        /// <returns></returns>
        public DSelectQuery From<TTable>()
        {
            DTable tr = DTable.Table<TTable>();
            SelectionTable = tr.CloneClause() as DTable;
            return From(tr);
        }


        /// <summary>
        /// Specifies the root owner.table or owner.view to select data from in this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        //public DBSelectQuery From(string owner, string table)
        //{
        //    DBTable tr = DBTable.Table(owner, table);
        //    return From(tr);
        //}

        /// <summary>
        ///  Specifies the root owner.table or owner.view to select data from in this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery From(string owner, Type TTable)
        {
            DTable tr = DTable.Table(owner, TTable);
            SelectionTable = tr.CloneClause() as DTable;
            return From(tr);
        }

        /// <summary>
        /// Specifies the root owner.table or owner.view to select data from in this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public DSelectQuery From<TTable>(string owner)
        {
            DTable tr = DTable.Table<TTable>(owner);
            SelectionTable = tr.CloneClause() as DTable;
            return From(tr);
        }



        /// <summary>
        /// Specifies the root table or view to select data from in this statement
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        public DSelectQuery From(DTable root)
        {
            DTableSet ts = DTableSet.From(root);
            SelectionTable = root.CloneClause() as DTable;
            this._root = ts;
            this._last = root;

            return this;
        }

        /// <summary>
        /// Specifies the root sub select statement or view to select data from in this statement
        /// </summary>
        /// <param name="subselect"></param>
        /// <returns></returns>
        public DSelectQuery From(DSelectQuery subselect)
        {
            DSubQuery sub = DSubQuery.SubSelect(subselect);
            DTableSet ts = DTableSet.From(sub);
            this._root = ts;
            this._last = sub;

            return this;
        }

        #endregion


        #region public DBQuery InnerJoin(string table, string parentfield, string childfield) + 4 overloads
        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [parentfield] = [childfield]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentfield"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        //public DBSelectQuery InnerJoin(string table, string parentfield, string childfield)
        //{
        //    return this.InnerJoin(table, parentfield, Compare.Equals, childfield);
        //}

        public DSelectQuery InnerJoin(Type TTable, string parentfield, string childfield)
        {
            return this.InnerJoin(TTable, parentfield, Compare.Equals, childfield);
        }

        public DSelectQuery InnerJoin<TTable>(string parentfield, string childfield)
        {
            return this.InnerJoin<TTable>(parentfield, Compare.Equals, childfield);
        }



        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [parentfield] [op] [childfield]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentfield"></param>
        /// <param name="op"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        //public DBSelectQuery InnerJoin(string table, string parentfield, Compare op, string childfield)
        //{
        //    DBTable tbl = DBTable.Table(table);
        //    DBField par = DBField.Field(parentfield);
        //    DBField child = DBField.Field(childfield);

        //    return this.InnerJoin(tbl, par, op, child);
        //}

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [parentfield] [op] [childfield]
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="parentfield"></param>
        /// <param name="op"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(Type TTable, string parentfield, Compare op, string childfield)
        {
            DTable tbl = DTable.Table(TTable);
            DField par = DField.Field(parentfield);
            DField child = DField.Field(childfield);

            return this.InnerJoin(tbl, par, op, child);
        }

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [parentfield] [op] [childfield]
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="parentfield"></param>
        /// <param name="op"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin<TTable>(string parentfield, Compare op, string childfield)
        {
            DTable tbl = DTable.Table<TTable>();
            DField par = DField.Field(parentfield);
            DField child = DField.Field(childfield);

            return this.InnerJoin(tbl, par, op, child);
        }



        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [parentfield] [op] [childfield]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentfield"></param>
        /// <param name="op"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(DTable table, DField parentfield, Compare op, DField childfield)
        {
            this._last = this._root.InnerJoin(table, parentfield, op, childfield);
            return this;
        }


        /// <summary>
        ///  Specifies an INNER JOIN on the specified [table] ON [left] [op] [right]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery InnerJoin(string table, DBClause left, Compare op, DBClause right)
        //{
        //    DBTable tbl = DBTable.Table(table);
        //    this._last = this._root.InnerJoin(tbl, left, op, right);
        //    return this;
        //}


        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [left] [op] [right]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(Type TTable, DClause left, Compare op, DClause right)
        {
            DTable tbl = DTable.Table(TTable);
            this._last = this._root.InnerJoin(tbl, left, op, right);
            return this;
        }


        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [left] [op] [right]
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin<TTable>(DClause left, Compare op, DClause right)
        {
            DTable tbl = DTable.Table<TTable>();
            this._last = this._root.InnerJoin(tbl, left, op, right);
            return this;
        }




        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [left] [op] [right]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(DTable table, DClause left, Compare op, DClause right)
        {
            this._last = this._root.InnerJoin(table, left, op, right);
            return this;
        }

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] ON [comparison]
        /// </summary>
        /// <param name="table"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(DTable table, DComparison compare)
        {
            this._last = this._root.InnerJoin(table, compare);
            return this;
        }

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        //public DBSelectQuery InnerJoin(string table)
        //{
        //    DBTable tbl = DBTable.Table(table);
        //    return this.InnerJoin(tbl);
        //}

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(Type TTable)
        {
            DTable tbl = DTable.Table(TTable);
            return this.InnerJoin(tbl);
        }

        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] - follow with an .On(...) statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public DSelectQuery InnerJoin<TTable>()
        {
            DTable tbl = DTable.Table<TTable>();
            return this.InnerJoin(tbl);
        }



        /// <summary>
        /// Specifies an INNER JOIN on the specified [owner].[table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        //public DBSelectQuery InnerJoin(string owner, string table)
        //{
        //    DBTable tbl = DBTable.Table(owner, table);
        //    return this.InnerJoin(tbl) ;
        //}


        /// <summary>
        /// Specifies an INNER JOIN on the specified [owner].[table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(string owner, Type TTable)
        {
            DTable tbl = DTable.Table(owner, TTable);
            return this.InnerJoin(tbl);
        }

        /// <summary>
        /// Specifies an INNER JOIN on the specified [owner].[table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin<TTable>(string owner)
        {
            DTable tbl = DTable.Table<TTable>(owner);
            return this.InnerJoin(tbl);
        }



        /// <summary>
        /// Specifies an INNER JOIN on the specified [table] - follow with an .On(...) statement
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(DTable table)
        {
            this._last = this._root.InnerJoin(table);
            return this;
        }


        /// <summary>
        /// Specifies an INNER JOIN on the specified sub select statement - follow with an .On(...) statement
        /// </summary>
        /// <param name="inner">An inner select query</param>
        /// <returns></returns>
        public DSelectQuery InnerJoin(DSelectQuery inner)
        {
            if (inner == this)
                throw new ArgumentException("Circular reference");

            DSubQuery subselect = DSubQuery.SubSelect(inner);
            this._last = this._root.InnerJoin(subselect);
            return this;
        }

        #endregion


        #region public DBSelectQuery LeftJoin(string table) + 3 overloads

        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        //public DBSelectQuery LeftJoin(string table)
        //{
        //    DBTable tbl = DBTable.Table(table);
        //    return LeftJoin(tbl);
        //}

        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public DSelectQuery LeftJoin(Type TTable)
        {
            DTable tbl = DTable.Table(TTable);
            return LeftJoin(tbl);
        }

        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public DSelectQuery LeftJoin<TTable>()
        {
            DTable tbl = DTable.Table<TTable>();
            return LeftJoin(tbl);
        }


        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name and schema/owner.
        /// </summary>
        /// <param name="owner">The name of the table schema/owner</param>
        /// <param name="table">The name of the table</param>
        /// <returns></returns>
        //public DBSelectQuery LeftJoin(string owner, string table)
        //{
        //    DBTable tbl = DBTable.Table(owner, table);
        //    return LeftJoin(tbl);
        //}

        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name and schema/owner.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery LeftJoin(string owner, Type TTable)
        {
            DTable tbl = DTable.Table(owner, TTable);
            return LeftJoin(tbl);
        }

        /// <summary>
        /// Performs a Left Join from the previous table onto the table with the specified name and schema/owner.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery LeftJoin<TTable>(string owner)
        {
            DTable tbl = DTable.Table<TTable>(owner);
            return LeftJoin(tbl);
        }


        /// <summary>
        /// Performs a left join from the previous table onto the specified table
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public DSelectQuery LeftJoin(DTable table)
        {
            this._last = this._root.LeftJoin(table);
            return this;
        }

        /// <summary>
        /// Performs a Left Join from the previous table to the specified Select sub query
        /// </summary>
        /// <param name="select"></param>
        /// <returns></returns>
        public DSelectQuery LeftJoin(DSelectQuery select)
        {
            if (select == this)
                throw new ArgumentException("Circular Reference");

            DSubQuery subselect = DSubQuery.SubSelect(select);
            this._last = this._root.LeftJoin(subselect);
            return this;
        }

        #endregion


        #region public DBSelectQuery RightJoin(string table) + 3 overloads

        /// <summary>
        /// Performs a Right Join from the previous table to the table with the specified name.
        /// </summary>
        /// <param name="table">The name of the table to join to.</param>
        /// <returns></returns>
        //public DBSelectQuery RightJoin(string table)
        //{
        //    DBTable tble = DBTable.Table(table);
        //    return RightJoin(tble);
        //}

        /// <summary>
        /// Performs a Right Join from the previous table to the table with the specified name.
        /// </summary>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery RightJoin(Type TTable)
        {
            DTable tble = DTable.Table(TTable);
            return RightJoin(tble);
        }

        /// <summary>
        /// Performs a Right Join from the previous table to the table with the specified name.
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public DSelectQuery RightJoin<TTable>()
        {
            DTable tble = DTable.Table<TTable>();
            return RightJoin(tble);
        }



        /// <summary>
        /// Performs a Right Join from the previous table to the table with the specified name and owner.
        /// </summary>
        /// <param name="owner">The table schema/owner</param>
        /// <param name="table">The name of the table to join to</param>
        /// <returns></returns>
        //public DBSelectQuery RightJoin(string owner, string table)
        //{
        //    DBTable tbl = DBTable.Table(table);
        //    return RightJoin(tbl);
        //}


        /// <summary>
        ///  Performs a Right Join from the previous table to the table with the specified name and owner.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery RightJoin(string owner, Type TTable)
        {
            DTable tbl = DTable.Table(owner, TTable);
            return RightJoin(tbl);
        }

        /// <summary>
        ///  Performs a Right Join from the previous table to the table with the specified name and owner.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <returns></returns>
        public DSelectQuery RightJoin<TTable>(string owner)
        {
            DTable tbl = DTable.Table<TTable>(owner);
            return RightJoin(tbl);
        }



        /// <summary>
        /// Preforms a Right Join from the previous table to the specified table
        /// </summary>
        /// <param name="tbl"></param>
        /// <returns></returns>
        public DSelectQuery RightJoin(DTable tbl)
        {
            this._last = this._root.RightJoin(tbl);
            return this;
        }

        /// <summary>
        /// Performs a Right Join from the previous table to the specified Select sub query
        /// </summary>
        /// <param name="select"></param>
        /// <returns></returns>
        public DSelectQuery RightJoin(DSelectQuery select)
        {
            DSubQuery subselect = DSubQuery.SubSelect(select);
            this._last = this._root.RightJoin(subselect);
            return this;
        }

        #endregion


        #region public DBSelectQuery On(string parentfield, Compare comp, string childfield) + 2 overloads
        /// <summary>
        /// Specifies an ON restriction on the previously joined tables (views, or sub selects)
        /// </summary>
        /// <param name="parentfield"></param>
        /// <param name="comp"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery On(string parentfield, Compare comp, string childfield)
        {
            DField parent = DField.Field(parentfield);
            DField child = DField.Field(childfield);

            this._last = this._root.On(parent, comp, child);
            return this;
        }

        /// <summary>
        /// Specifies an ON restriction on the specified joined tables (views, or sub selects)
        /// </summary>
        /// <param name="parenttable"></param>
        /// <param name="parentfield"></param>
        /// <param name="comp"></param>
        /// <param name="childtable"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        //public DBSelectQuery On(string parenttable, string parentfield, Compare comp, string childtable, string childfield)
        //{
        //    DBField parent = DBField.Field(parenttable, parentfield);
        //    DBField child = DBField.Field(childtable, childfield);
        //    this._last = this._root.On(parent, comp, child);
        //    return this;
        //}

        /// <summary>
        /// Specifies an ON restriction on the specified joined tables (views, or sub selects)
        /// </summary>
        /// <param name="parenttable"></param>
        /// <param name="parentfield"></param>
        /// <param name="comp"></param>
        /// <param name="childtable"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery On(Type parentTTable, string parentfield, Compare comp, Type childTTable, string childfield)
        {
            DField parent = DField.Field(parentTTable, parentfield);
            DField child = DField.Field(childTTable, childfield);
            this._last = this._root.On(parent, comp, child);
            return this;
        }

        /// <summary>
        /// Specifies an ON restriction on the specified joined tables (views, or sub selects)
        /// </summary>
        /// <typeparam name="parentTTable"></typeparam>
        /// <typeparam name="childTTable"></typeparam>
        /// <param name="parentfield"></param>
        /// <param name="comp"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DSelectQuery On<TParentTable, TChildTable>(string parentfield, Compare comp, string childfield)
        {
            DField parent = DField.Field<TParentTable>(parentfield);
            DField child = DField.Field<TChildTable>(childfield);
            this._last = this._root.On(parent, comp, child);
            return this;
        }




        /// <summary>
        /// Specifies an ON restriction on the previously joined tables (views, or sub selects)
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="comp"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public DSelectQuery On(DClause parent, Compare comp, DClause child)
        {
            this._last = this._root.On(parent, comp, child);
            return this;
        }

        #endregion

        //
        // As
        //

        #region public DBQuery As(string alias)

        /// <summary>
        /// Assigns the alias name to the last _ModuleInitializer or table in the select query. 
        /// It is an error to set the alias to the last item if it does not support alias names
        /// </summary>
        /// <param name="alias">The name to use as an alias</param>
        /// <returns>Itself</returns>
        public DSelectQuery As(string alias)
        {
            if (this._last is IDAlias)
                ((IDAlias)this._last).As(alias);
            else if (null == this._last)
                throw new NullReferenceException("Cannot set an alias because there was no last item added in the query");
            else
                throw new NotSupportedException("Cannot alias a '" + this._last.GetType().ToString() + "' item in the query");

            return this;
        }

        #endregion

        //
        // Boolean Ops
        //

        #region public DBSelectQuery And(string _ModuleInitializer)

        /// <summary>
        /// Appends a _ModuleInitializer to this statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery And(string field)
        {
            DField fld = DField.Field(field);
            return And(fld);
        }

        /// <summary>
        /// Appends the [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery And(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return And(fld);
        //}

        /// <summary>
        /// Appends the [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery And(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return And(fld);
        }

        /// <summary>
        /// Appends the [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery And<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return And(fld);
        }



        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery And(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return And(fld);
        //}

        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>        
        public DSelectQuery And(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return And(fld);
        }

        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery And<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return And(fld);
        }



        /// <summary>
        /// Appends the clause to this statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery And(DClause clause)
        {
            if (_last == null)
                return this.Select(clause);
            else if (_last is IDBoolean)
            {
                _last = ((IDBoolean)_last).And(clause);
                return this;
            }
            else
                throw new ArgumentException("The last clause in the statement does not support 'and' operations");

        }

        #endregion


        #region public DBSelectQuery And(string parent, Compare comp, string child) + 3 overloads

        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="left"></param>
        /// <param name="comp"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery And(string left, Compare comp, string right)
        {
            DField par = DField.Field(left);
            DField chi = DField.Field(right);
            return And(par, comp, chi);
        }

        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="parentTable"></param>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childTable"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        //public DBSelectQuery And(string parentTable, string parentField, Compare comp, string childTable, string childField)
        //{
        //    DBField par = DBField.Field(parentTable,parentField);
        //    DBField chi = DBField.Field(childTable,childField);
        //    return And(par, comp, chi);
        //}

        /// <summary>
        ///  Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="parentTable"></param>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childTable"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DSelectQuery And(Type parentTTable, string parentField, Compare comp, Type childTTable, string childField)
        {
            DField par = DField.Field(parentTTable, parentField);
            DField chi = DField.Field(childTTable, childField);
            return And(par, comp, chi);
        }

        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <typeparam name="TParentTable"></typeparam>
        /// <typeparam name="TChildTable"></typeparam>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DSelectQuery And<TParentTable, TChildTable>(string parentField, Compare comp, string childField)
        {
            DField par = DField.Field<TParentTable>(parentField);
            DField chi = DField.Field<TChildTable>(childField);
            return And(par, comp, chi);
        }



        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="parentOwner"></param>
        /// <param name="parentTable"></param>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childOwner"></param>
        /// <param name="childTable"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        //public DBSelectQuery And(string parentOwner, string parentTable, string parentField, Compare comp, string childOwner, string childTable, string childField)
        //{
        //    DBField par = DBField.Field(parentOwner, parentTable, parentField);
        //    DBField chi = DBField.Field(childOwner, childTable, childField);
        //    return And(par, comp, chi);
        //}


        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="parentOwner"></param>
        /// <param name="parentTable"></param>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childOwner"></param>
        /// <param name="childTable"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DSelectQuery And(string parentOwner, Type parentTTable, string parentField, Compare comp, string childOwner, Type childTTable, string childField)
        {
            DField par = DField.Field(parentOwner, parentTTable, parentField);
            DField chi = DField.Field(childOwner, childTTable, childField);
            return And(par, comp, chi);
        }


        /// <summary>
        /// Appends a _ModuleInitializer to _ModuleInitializer comparison to the last statement in this select query
        /// </summary>
        /// <param name="parentOwner"></param>
        /// <param name="parentTTable"></param>
        /// <param name="parentField"></param>
        /// <param name="comp"></param>
        /// <param name="childOwner"></param>
        /// <param name="childTTable"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DSelectQuery And<TParentTable, TChildTable>(string parentOwner, string parentField, Compare comp, string childOwner, string childField)
        {
            DField par = DField.Field<TParentTable>(parentOwner, parentField);
            DField chi = DField.Field<TChildTable>(childOwner, childField);
            return And(par, comp, chi);
        }

        /// <summary>
        /// Appends a clause to clause comparison to the last statement in this select query
        /// </summary>
        /// <param name="left"></param>
        /// <param name="comp"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery And(DClause left, Compare comp, DClause right)
        {
            DComparison comarison = DComparison.Compare(left, comp, right);
            return And(comarison);
        }

        #endregion

        //
        // ordering
        //


        #region public DBSelectQuery OrderBy(string _ModuleInitializer) + 6 overloads

        /// <summary>
        /// Appends an ORDER BY [_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(string field)
        {
            return OrderBy(field, Order.Default);
        }

        /// <summary>
        /// Appends an ORDER BY [table][_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery OrderBy(string table, string field)
        //{
        //    return OrderBy(table, field, Order.Default);
        //}

        /// <summary>
        /// Appends an ORDER BY [table][_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(Type TTable, string field)
        {
            return OrderBy(TTable, field, Order.Default);
        }


        /// <summary>
        /// Appends an ORDER BY [table][_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy<TTable>(string field)
        {
            return OrderBy<TTable>(field, Order.Default);
        }


        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery OrderBy(string owner, string table, string field)
        //{
        //    return OrderBy(owner, table, field, Order.Default);
        //}

        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(string owner, Type TTable, string field)
        {
            return OrderBy(owner, TTable, field, Order.Default);
        }

        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy<TTable>(string owner, string field)
        {
            return OrderBy<TTable>(owner, field, Order.Default);
        }



        /// <summary>
        /// Appends an ORDER BY [_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(string field, Order order)
        {
            DClause clause = DField.Field(field);
            return OrderBy(clause, order);
        }

        /// <summary>
        /// Appends an ORDER BY [table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        //public DBSelectQuery OrderBy(string table, string field, Order order)
        //{
        //    DBClause clause = DBField.Field(table, field);
        //    return OrderBy(clause, order);
        //}

        /// <summary>
        /// Appends an ORDER BY [table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(Type TTable, string field, Order order)
        {
            DClause clause = DField.Field(TTable, field);
            return OrderBy(clause, order);
        }

        /// <summary>
        ///  Appends an ORDER BY [table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy<TTable>(string field, Order order)
        {
            DClause clause = DField.Field<TTable>(field);
            return OrderBy(clause, order);
        }



        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        //public DBSelectQuery OrderBy(string owner, string table, string field, Order order)
        //{
        //    DBClause clause = DBField.Field(owner, table, field);
        //    return OrderBy(clause, order);
        //}


        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(string owner, Type TTable, string field, Order order)
        {
            DClause clause = DField.Field(owner, TTable, field);
            return OrderBy(clause, order);
        }

        /// <summary>
        /// Appends an ORDER BY [owner].[table].[_ModuleInitializer] ASC|DESC to this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy<TTable>(string owner, string field, Order order)
        {
            DClause clause = DField.Field<TTable>(owner, field);
            return OrderBy(clause, order);
        }




        /// <summary>
        /// Appends an ORDER BY [any clause] ASC|DESC to this statement
        /// </summary>
        /// <param name="clause"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public DSelectQuery OrderBy(DClause clause, Order order)
        {
            DOrder oc = DOrder.OrderBy(order, clause);

            if (this._order == null)
            {
                this._order = DOrderSet.OrderBy(clause, order);
                this._last = this._order;
            }
            else
            {
                this._last = this._order.And(oc);
            }

            return this;
        }



        #endregion

        //
        // grouping
        //

        #region public static DBSelectQuery GroupBy(string _ModuleInitializer) + 3 overloads

        /// <summary>
        /// Appends a GROUP BY [_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy(string field)
        {
            DField fld = DField.Field(field);
            return GroupBy(fld);
        }

        /// <summary>
        /// Appends a GROUP BY [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery GroupBy(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return GroupBy(fld);
        //}

        /// <summary>
        /// Appends a GROUP BY [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return GroupBy(fld);
        }


        /// <summary>
        /// Appends a GROUP BY [table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return GroupBy(fld);
        }


        /// <summary>
        /// Appends a GROUP BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery GroupBy(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return GroupBy(fld);
        //}


        /// <summary>
        /// Appends a GROUP BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return GroupBy(fld);
        }

        /// <summary>
        /// Appends a GROUP BY [owner].[table].[_ModuleInitializer] to this statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return GroupBy(fld);
        }


        /// <summary>
        /// Appends a GROUP BY [any clause] to this statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery GroupBy(DClause clause)
        {
            if (this._grpby == null)
                this._grpby = DGroupBySet.GroupBy(clause);
            else
                this._grpby.And(clause);

            this._last = this._grpby;

            return this;
        }

        #endregion

        //
        // select
        //

        #region ---------------public DBSelectQuery Select(DBClause reference) + 3 overloads

        /// <summary>
        /// Appends the clause onto the select set
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public new DSelectQuery Select(DClause clause)
        {
            if (this._select == null)
                this._select = DSelectSet.Select(clause);
            else
                this._select = (DSelectSet)this._select.And(clause);
            _last = this._select;

            return this;
        }

        /// <summary>
        /// Appends the [_ModuleInitializer] onto the select set.
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        public DSelectQuery Select(string field)
        {
            DField fld = DField.Field(field);
            return this.Select(fld);
        }


        /// <summary>
        /// ������ � ROW COUNT ����������
        /// </summary>
        /// <param name="OrderField"></param>
        /// <param name="RowNumAlias"></param>
        /// <returns></returns>
        public DSelectQuery SelectRowNumber(string OrderField, string RowNumAlias)
        {
            DField orderField = DField.Field(OrderField);
            DField RowNumberAliasField = DField.Field(RowNumAlias);
            return this.Select(DCustomStatement.Statement(StatementsEn.RowNumber_Statement, orderField, RowNumberAliasField, null));

        }






        /// <summary>
        /// Appends the [table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Select(string table, string field)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    return this.Select(fld);
        //}


        /// <summary>
        /// Appends the [table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Select(Type TTable, string field)
        {
            DField fld = DField.Field(TTable, field);
            return this.Select(fld);
        }

        /// <summary>
        /// Appends the [table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Select<TTable>(string field)
        {
            DField fld = DField.Field<TTable>(field);
            return this.Select(fld);
        }


        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <returns></returns>
        //public DBSelectQuery Select(string owner, string table, string field)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    return this.Select(fld);
        //}

        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Select(string owner, Type TTable, string field)
        {
            DField fld = DField.Field(owner, TTable, field);
            return this.Select(fld);
        }

        /// <summary>
        /// Appends the [owner].[table].[_ModuleInitializer] onto the select set
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public DSelectQuery Select<TTable>(string owner, string field)
        {
            DField fld = DField.Field<TTable>(owner, field);
            return this.Select(fld);
        }


        #endregion

        //
        // where
        //

        #region public DBSelectQuery Where(DBReference left, ComparisonOperator compare, DBClause right) + 1 overload

        /// <summary>
        /// Appends the WHERE comparison clause onto the select set
        /// </summary>
        /// <param name="left"></param>
        /// <param name="compare"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery Where(DClause left, Compare compare, DClause right)
        {
            DFilterSet fs = DFilterSet.Where(left, compare, right);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Appends the WHERE _ModuleInitializer to _ModuleInitializer comparison clause onto the select set
        /// </summary>
        /// <param name="left"></param>
        /// <param name="compare"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery Where(string left, Compare compare, string right)
        {
            DField fls = DField.Field(left);
            DField frs = DField.Field(right);

            return Where(fls, compare, frs);
        }

        /// <summary>
        /// Appends the WHERE _ModuleInitializer to clause comparison clause onto the select set
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="compare"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Where(string field, Compare compare, DClause clause)
        {
            DField left = DField.Field(field);

            return Where(left, compare, clause);
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] to [table].[_ModuleInitializer] comparison clause onto the select set
        /// </summary>
        /// <param name="lefttable"></param>
        /// <param name="leftfield"></param>
        /// <param name="compare"></param>
        /// <param name="righttable"></param>
        /// <param name="rightfield"></param>
        /// <returns></returns>
        //public DBSelectQuery Where(string lefttable, string leftfield, Compare compare, string righttable, string rightfield)
        //{
        //    DBField fls = DBField.Field(lefttable, leftfield);
        //    DBField frs = DBField.Field(righttable, rightfield);

        //    return Where(fls, compare, frs);
        //}

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] to [table].[_ModuleInitializer] comparison clause onto the select set
        /// </summary>
        /// <param name="lefttable"></param>
        /// <param name="leftfield"></param>
        /// <param name="compare"></param>
        /// <param name="righttable"></param>
        /// <param name="rightfield"></param>
        /// <returns></returns>
        public DSelectQuery Where(Type leftTTable, string leftfield, Compare compare, Type rightTTable, string rightfield)
        {
            DField fls = DField.Field(leftTTable, leftfield);
            DField frs = DField.Field(rightTTable, rightfield);

            return Where(fls, compare, frs);
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] to [table].[_ModuleInitializer] comparison clause onto the select set
        /// </summary>
        /// <typeparam name="TLeftTable"></typeparam>
        /// <typeparam name="TRightTable"></typeparam>
        /// <param name="leftfield"></param>
        /// <param name="compare"></param>
        /// <param name="rightfield"></param>
        /// <returns></returns>
        public DSelectQuery Where<TLeftTable, TRightTable>(string leftfield, Compare compare, string rightfield)
        {
            DField fls = DField.Field<TLeftTable>(leftfield);
            DField frs = DField.Field<TRightTable>(rightfield);

            return Where(fls, compare, frs);
        }


        /// <summary>
        /// Appends the WHERE comparison clause onto the select set
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DSelectQuery Where(DComparison compare)
        {
            DFilterSet fs;
            if (null == this._where)
                fs = DFilterSet.Where(compare);
            else
                fs = this._where.And(compare);

            this._where = fs;
            this._last = fs;

            return this;
        }

        #endregion

        #region public DBSelectQuery WhereFieldEquals(string _ModuleInitializer, DBClause value) + 1 overload

        /// <summary>
        /// Appends the WHERE [_ModuleInitializer] = .... to this select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereFieldEquals(string field, DClause value)
        {
            return WhereField(field, Compare.Equals, value);
        }

        /// <summary>
        ///  Appends the WHERE [table].[_ModuleInitializer] = .... to this select statement
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery WhereFieldEquals(string fieldTable, string fieldName, DBClause value)
        //{
        //    return WhereField(fieldTable, fieldName, Compare.Equals, value);
        //}

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] = .... to this select statement
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereFieldEquals(Type fieldTTable, string fieldName, DClause value)
        {
            return WhereField(fieldTTable, fieldName, Compare.Equals, value);
        }

        /// <summary>
        ///  Appends the WHERE [table].[_ModuleInitializer] = .... to this select statement
        /// </summary>
        /// <param name="fieldTTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereFieldEquals<TTable>(string fieldName, DClause value)
        {
            return WhereField<TTable>(fieldName, Compare.Equals, value);
        }


        #endregion

        #region public DBSelectQuery WhereField(string _ModuleInitializer, ComparisonOperator op, DBClause value) + 2 overload

        /// <summary>
        ///  Appends the WHERE [_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereField(string field, Compare op, DClause value)
        {
            DField fld = DField.Field(field);
            return Where(fld, op, value);
        }

        /// <summary>
        ///  Appends the WHERE [table].[_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery WhereField(string fieldTable, string fieldName, Compare op, DBClause value)
        //{
        //    DBField fld = DBField.Field(fieldTable, fieldName);
        //    return Where(fld, op, value);
        //}

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereField(Type fieldTTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldTTable, fieldName);
            return Where(fld, op, value);
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereField<TTable>(string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldName);
            return Where(fld, op, value);
        }

        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldTTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereField(string fieldOwner, Type fieldTTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldOwner, fieldTTable, fieldName);
            return Where(fld, op, value);
        }

        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] op .... to this select statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery WhereField<TTable>(string fieldOwner, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldOwner, fieldName);
            return Where(fld, op, value);
        }


        #endregion

        #region public DBSelectQuery AndWhere(DBClause left, ComparisonOperator op, DBClause right) + 3 overloads
        /// <summary>
        /// Appends the AND ( [compare] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere(DComparison compare)
        {
            _where = _where.And(compare);
            _last = _where;
            return this;
        }

        /// <summary>
        ///  Appends the AND ( [left] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere(DClause left, Compare op, DClause right)
        {
            _where = _where.And(left, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        ///  Appends the AND ( [_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere(string field, Compare op, DClause right)
        {
            _where = _where.And(field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        ///  Appends the AND ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery AndWhere(string table, string field, Compare op, DBClause right)
        //{
        //    _where = _where.And(table, field, op, right);
        //    _last = _where;

        //    return this;
        //}

        /// <summary>
        /// Appends the AND ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere(Type TTable, string field, Compare op, DClause right)
        {
            _where = _where.And(TTable, field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Appends the AND ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere<TTable>(string field, Compare op, DClause right)
        {
            _where = _where.And<TTable>(field, op, right);
            _last = _where;

            return this;
        }



        /// <summary>
        /// Appends the AND ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery AndWhere(string owner, string table, string field, Compare op, DBClause right)
        //{
        //    _where = _where.And(owner, table, field, op, right);
        //    _last = _where;

        //    return this;
        //}

        /// <summary>
        /// Appends the AND ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere(string owner, Type TTable, string field, Compare op, DClause right)
        {
            _where = _where.And(owner, TTable, field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        /// Appends the AND ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndWhere<TTable>(string owner, string field, Compare op, DClause right)
        {
            _where = _where.And<TTable>(owner, field, op, right);
            _last = _where;

            return this;
        }


        #endregion

        #region public DBSelectQuery OrWhere(DBClause left, ComparisonOperator op, DBClause right) + 4 overloads

        /// <summary>
        ///  Appends the OR ( [left] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere(DComparison comp)
        {
            _where = _where.Or(comp);
            _last = _where;

            return this;
        }

        /// <summary>
        ///  Appends the OR ( [left] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere(DClause left, Compare op, DClause right)
        {
            _where = _where.Or(left, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        ///  Appends the OR ( [_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere(string field, Compare op, DClause right)
        {
            _where = _where.Or(field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        ///  Appends the OR ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery OrWhere(string table, string field, Compare op, DBClause right)
        //{
        //    _where = _where.Or(table, field, op, right);
        //    _last = _where;

        //    return this;
        //}

        /// <summary>
        ///  Appends the OR ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere(Type TTable, string field, Compare op, DClause right)
        {
            _where = _where.Or(TTable, field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        ///  Appends the OR ( [table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere<TTable>(string field, Compare op, DClause right)
        {
            _where = _where.Or<TTable>(field, op, right);
            _last = _where;

            return this;
        }



        /// <summary>
        ///  Appends the OR ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery OrWhere(string owner, string table, string field, Compare op, DBClause right)
        //{
        //    _where = _where.Or(owner, table, field, op, right);
        //    _last = _where;

        //    return this;
        //}

        /// <summary>
        /// Appends the OR ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere(string owner, Type TTable, string field, Compare op, DClause right)
        {
            _where = _where.Or(owner, TTable, field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// Appends the OR ( [owner].[table].[_ModuleInitializer] op [right] ) to the WHERE clause in the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrWhere<TTable>(string owner, string field, Compare op, DClause right)
        {
            _where = _where.Or<TTable>(owner, field, op, right);
            _last = _where;

            return this;
        }

        #endregion

        #region public DBSelectQuery WhereIn(string _ModuleInitializer, params object[] values) + 3 overloads

        /// <summary>
        ///  Appends the WHERE [_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn(string field, params object[] values)
        {
            DField fld = DField.Field(field);
            List<DClause> items = new List<DClause>();
            if (values != null && values.Length > 0)
            {
                foreach (object val in values)
                {
                    items.Add(DConst.Const(val));
                }
            }
            DComparison compare = DComparison.In(fld, items.ToArray());

            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        //public DBSelectQuery WhereIn(string table, string field, params DBClause[] values)
        //{
        //    DBField fld = DBField.Field(table, field);
        //    DBComparison compare = DBComparison.In(fld, values);
        //    DBFilterSet fs = DBFilterSet.Where(compare);
        //    this._where = fs;
        //    this._last = fs;

        //    return this;
        //}

        /// <summary>
        ///  Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn(Type TTable, string field, params DClause[] values)
        {
            DField fld = DField.Field(TTable, field);
            DComparison compare = DComparison.In(fld, values);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn<TTable>(string field, params DClause[] values)
        {
            DField fld = DField.Field<TTable>(field);
            DComparison compare = DComparison.In(fld, values);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }


        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        //public DBSelectQuery WhereIn(string owner, string table, string field, params DBClause[] values)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    DBComparison compare = DBComparison.In(fld, values);
        //    DBFilterSet fs = DBFilterSet.Where(compare);
        //    this._where = fs;
        //    this._last = fs;

        //    return this;
        //}


        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn(string owner, Type TTable, string field, params DClause[] values)
        {
            DField fld = DField.Field(owner, TTable, field);
            DComparison compare = DComparison.In(fld, values);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }


        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn<TTable>(string owner, string field, params DClause[] values)
        {
            DField fld = DField.Field<TTable>(owner, field);
            DComparison compare = DComparison.In(fld, values);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }



        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN (SELECT ... ) to the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="select"></param>
        /// <returns></returns>
        public DSelectQuery WhereIn(string field, DSelectQuery select)
        {
            DField fld = DField.Field(field);
            DSubQuery subsel = DSubQuery.SubSelect(select);
            DComparison compare = DComparison.In(fld, subsel);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        #endregion

        #region public DBSelectQuery AndWhereIn(string _ModuleInitializer, params object[] values) + 3 overloads

        /// <summary>
        ///  Appends the WHERE [_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn(string field, params object[] values)
        {
            DField fld = DField.Field(field);
            List<DClause> items = new List<DClause>();
            if (values != null && values.Length > 0)
            {
                foreach (object val in values)
                {
                    items.Add(DConst.Const(val));
                }
            }
            DComparison compare = DComparison.In(fld, items.ToArray());

            return this.AndWhere(compare);

        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        //public DBSelectQuery AndWhereIn(string table, string field, params DBClause[] values)
        //{
        //    DBField fld = DBField.Field(table, field);

        //    DBComparison compare = DBComparison.In(fld, values);

        //    return this.AndWhere(compare);
        //}


        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn(Type TTable, string field, params DClause[] values)
        {
            DField fld = DField.Field(TTable, field);

            DComparison compare = DComparison.In(fld, values);

            return this.AndWhere(compare);
        }

        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn<TTable>(string field, params DClause[] values)
        {
            DField fld = DField.Field<TTable>(field);

            DComparison compare = DComparison.In(fld, values);

            return this.AndWhere(compare);
        }



        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        //public DBSelectQuery AndWhereIn(string owner, string table, string field, params DBClause[] values)
        //{
        //    DBField fld = DBField.Field(owner, table, field);
        //    DBComparison compare = DBComparison.In(fld, values);

        //    return this.AndWhere(compare);
        //}


        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn(string owner, Type TTable, string field, params DClause[] values)
        {
            DField fld = DField.Field(owner, TTable, field);
            DComparison compare = DComparison.In(fld, values);

            return this.AndWhere(compare);
        }


        /// <summary>
        /// Appends the WHERE [owner].[table].[_ModuleInitializer] IN ( [value1], .... ) to the select statement
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn<TTable>(string owner, string field, params DClause[] values)
        {
            DField fld = DField.Field<TTable>(owner, field);
            DComparison compare = DComparison.In(fld, values);

            return this.AndWhere(compare);
        }



        /// <summary>
        /// Appends the WHERE [table].[_ModuleInitializer] IN (SELECT ... ) to the select statement
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="select"></param>
        /// <returns></returns>
        public DSelectQuery AndWhereIn(string field, DSelectQuery select)
        {
            DField fld = DField.Field(field);
            DSubQuery subsel = DSubQuery.SubSelect(select);
            DComparison compare = DComparison.In(fld, subsel);

            return this.AndWhere(compare);
        }

        #endregion

        #region public DBSelectQuery WhereExists(DBSelectQuery select)

        /// <summary>
        /// Appends the WHERE EXISTS (SELECT ... ) to the select statement
        /// </summary>
        /// <param name="select"></param>
        /// <returns></returns>
        public DSelectQuery WhereExists(DSelectQuery select)
        {
            DSubQuery subsel = DSubQuery.SubSelect(select);
            DComparison compare = DComparison.Exists(subsel);
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        #endregion

        //
        // Const()
        //

        #region public DBSelectQuery Const(int value) + 5 overloads
        /// <summary>
        /// Appends  the constant integer value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(int value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant boolean value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(bool value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant date time value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(DateTime value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant string value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(string value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant double value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(double value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant Guid value to the last clause in the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(Guid value)
        {
            DConst con = DConst.Const(value);
            return this.And(con);
        }

        /// <summary>
        /// Appends  the constant value of the specified type to the last clause in the statement
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery Const(System.Data.DbType type, object value)
        {
            DConst con = DConst.Const(type, value);
            return this.And(con);
        }

        #endregion

        //
        // having
        //

        #region public DBSelectQuery Having(DBReference left, ComparisonOperator compare, DBClause right) + 1 overload
        /// <summary>
        /// Appends the first HAVING [left] op [right] clause to this SQL statement 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery Having(DClause left, Compare op, DClause right)
        {
            DFilterSet fs = DFilterSet.Where(left, op, right);
            this._having = fs;
            this._last = fs;

            return this;
        }

        /// <summary>
        /// Appends the first HAVING [compare] clause to this SQL statement 
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DSelectQuery Having(DComparison compare)
        {
            DFilterSet fs = DFilterSet.Where(compare);
            this._having = fs;
            this._last = fs;

            return this;
        }

        #endregion

        #region public DBSelectQuery HavingFieldEquals(string _ModuleInitializer, DBClause value) + 1 overload

        /// <summary>
        /// Appends the first HAVING [_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingFieldEquals(string field, DClause value)
        {
            return HavingField(field, Compare.Equals, value);
        }

        /// <summary>
        /// Appends the first HAVING [table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery HavingFieldEquals(string fieldTable, string fieldName, DBClause value)
        //{
        //    return HavingField(fieldTable, fieldName, Compare.Equals, value);
        //}


        /// <summary>
        /// Appends the first HAVING [table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldTTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingFieldEquals(Type fieldTTable, string fieldName, DClause value)
        {
            return HavingField(fieldTTable, fieldName, Compare.Equals, value);
        }

        /// <summary>
        /// Appends the first HAVING [table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingFieldEquals<TTable>(string fieldName, DClause value)
        {
            return HavingField<TTable>(fieldName, Compare.Equals, value);
        }



        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="owner"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery HavingFieldEquals(string owner, string table, string field, DBClause value)
        //{
        //    return HavingField(owner, table, field, Compare.Equals, value);
        //}


        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingFieldEquals(string owner, Type TTable, string field, DClause value)
        {
            return HavingField(owner, TTable, field, Compare.Equals, value);
        }

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] = [value] clause to this SQL statement 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingFieldEquals<TTable>(string owner, string field, DClause value)
        {
            return HavingField<TTable>(owner, field, Compare.Equals, value);
        }


        #endregion

        #region public DBSelectQuery HavingField(string _ModuleInitializer, ComparisonOperator op, DBClause value) + 2 overload
        /// <summary>
        /// Appends the first HAVING [_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingField(string field, Compare op, DClause value)
        {
            DField fld = DField.Field(field);
            return Having(fld, op, value);
        }

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery HavingField(string fieldTable, string fieldName, Compare op, DBClause value)
        //{
        //    DBField fld = DBField.Field(fieldTable, fieldName);
        //    return Having(fld, op, value);
        //}

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingField(Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldTable, fieldName);
            return Having(fld, op, value);
        }

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingField<TTable>(string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldName);
            return Having(fld, op, value);
        }


        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        //public DBSelectQuery HavingField(string fieldOwner, string fieldTable, string fieldName, Compare op, DBClause value)
        //{
        //    DBField fld = DBField.Field(fieldOwner, fieldTable, fieldName);
        //    return Having(fld, op, value);
        //}

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldTable"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingField(string fieldOwner, Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldOwner, fieldTable, fieldName);
            return Having(fld, op, value);
        }

        /// <summary>
        /// Appends the first HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="fieldOwner"></param>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DSelectQuery HavingField<TTable>(string fieldOwner, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldOwner, fieldName);
            return Having(fld, op, value);
        }


        #endregion

        #region public DBSelectQuery AndHaving(DBClause left, ComparisonOperator op, DBClause right) + 3 overloads
        /// <summary>
        /// Appends another AND HAVING [left] op [right] clause to this SQL statement 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving(DClause left, Compare op, DClause right)
        {
            _having = _having.And(left, op, right);
            _last = _having;

            return this;
        }

        /// <summary>
        /// Appends another AND HAVING [_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving(string field, Compare op, DClause right)
        {
            _having = _having.And(field, op, right);
            _last = _having;

            return this;
        }

        /// <summary>
        /// Appends another AND HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery AndHaving(string table, string field, Compare op, DBClause right)
        //{
        //    _having = _having.And(table, field, op, right);
        //    _last = _having;

        //    return this;
        //}


        /// <summary>
        /// Appends another AND HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving(Type TTable, string field, Compare op, DClause right)
        {
            _having = _having.And(TTable, field, op, right);
            _last = _having;

            return this;
        }


        /// <summary>
        /// Appends another AND HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving<TTable>(string field, Compare op, DClause right)
        {
            _having = _having.And<TTable>(field, op, right);
            _last = _having;

            return this;
        }




        /// <summary>
        /// Appends another AND HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery AndHaving(string owner, string table, string field, Compare op, DBClause right)
        //{
        //    _having = _having.And(owner, table, field, op, right);
        //    _last = _having;

        //    return this;
        //}

        /// <summary>
        /// Appends another AND HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving(string owner, Type TTable, string field, Compare op, DClause right)
        {
            _having = _having.And(owner, TTable, field, op, right);
            _last = _having;
            return this;
        }

        /// <summary>
        /// Appends another AND HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving<TTable>(string owner, string field, Compare op, DClause right)
        {
            _having = _having.And<TTable>(owner, field, op, right);
            _last = _having;
            return this;
        }



        /// <summary>
        /// Appends another AND HAVING [comparison] clause to this SQL statement 
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public DSelectQuery AndHaving(DComparison comp)
        {
            _having = _having.And(comp);
            _last = _having;

            return this;
        }

        #endregion

        #region public DBSelectQuery OrHaving(DBClause left, ComparisonOperator op, DBClause right) + 3 overloads

        /// <summary>
        ///  Appends another OR HAVING [left] op [right] clause to this SQL statement 
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving(DClause left, Compare op, DClause right)
        {
            _having = _having.Or(left, op, right);
            _last = _having;

            return this;
        }

        /// <summary>
        /// Appends another OR HAVING [_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving(string field, Compare op, DClause right)
        {
            _having = _having.Or(field, op, right);
            _last = _having;

            return this;
        }

        /// <summary>
        /// Appends another OR HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery OrHaving(string table, string field, Compare op, DBClause right)
        //{
        //    _having = _having.Or(table, field, op, right);
        //    _last = _having;

        //    return this;
        //}

        /// <summary>
        /// Appends another OR HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving(Type TTable, string field, Compare op, DClause right)
        {
            _having = _having.Or(TTable, field, op, right);
            _last = _having;

            return this;
        }

        /// <summary>
        /// Appends another OR HAVING [table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving<TTable>(string field, Compare op, DClause right)
        {
            _having = _having.Or<TTable>(field, op, right);
            _last = _having;

            return this;
        }


        /// <summary>
        /// Appends another OR HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        //public DBSelectQuery OrHaving(string owner, string table, string field, Compare op, DBClause right)
        //{
        //    _having = _having.Or(owner, table, field, op, right);
        //    _last = _having;

        //    return this;
        //}

        /// <summary>
        /// Appends another OR HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving(string owner, Type TTable, string field, Compare op, DClause right)
        {
            _having = _having.Or(owner, TTable, field, op, right);
            _last = _having;

            return this;
        }


        /// <summary>
        /// Appends another OR HAVING [owner].[table].[_ModuleInitializer] op [value] clause to this SQL statement 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="TTable"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving<TTable>(string owner, string field, Compare op, DClause right)
        {
            _having = _having.Or<TTable>(owner, field, op, right);
            _last = _having;

            return this;
        }



        /// <summary>
        /// Appends another OR HAVING [comparison] clause to this SQL statement 
        /// </summary>
        /// <param name="comp"></param>
        /// <returns></returns>
        public DSelectQuery OrHaving(DComparison comp)
        {
            _having = _having.Or(comp);
            _last = _having;

            return this;
        }

        #endregion

        //
        // calculation
        //

        #region IDBCalculable Implementation

        /// <summary>
        /// Appends a '+' [clause] statement to this Select statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Plus(DClause clause)
        {
            if (this._last is IDCalculable)
                this._last = ((IDCalculable)this._last).Calculate(BinaryOp.Add, clause);
            else
                throw new InvalidOperationException("Cannot support calculations on the this query set");
            return this;
        }

        /// <summary>
        /// Appends a '-' [clause] statement to this Select statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Minus(DClause clause)
        {
            if (this._last is IDCalculable)
                this._last = ((IDCalculable)this._last).Calculate(BinaryOp.Subtract, clause);
            else
                throw new InvalidOperationException("Cannot support calculations on the this query set");
            return this;
        }

        /// <summary>
        /// Appends a '*' [clause] statement to this Select statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Times(DClause clause)
        {
            if (this._last is IDCalculable)
                this._last = ((IDCalculable)this._last).Calculate(BinaryOp.Multiply, clause);
            else
                throw new InvalidOperationException("Cannot support calculations on the this query set");
            return this;
        }

        /// <summary>
        /// Appends a '/' [clause] statement to this Select statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Divide(DClause clause)
        {
            if (this._last is IDCalculable)
                this._last = ((IDCalculable)this._last).Calculate(BinaryOp.Divide, clause);
            else
                throw new InvalidOperationException("Cannot support calculations on the this query set");
            return this;
        }

        /// <summary>
        /// Appends a '%' [clause] statement to this Select statement
        /// </summary>
        /// <param name="clause"></param>
        /// <returns></returns>
        public DSelectQuery Modulo(DClause clause)
        {
            if (this._last is IDCalculable)
                this._last = ((IDCalculable)this._last).Calculate(BinaryOp.Modulo, clause);
            else
                throw new InvalidOperationException("Cannot support calculations on the this query set");
            return this;
        }

        #endregion

    }
}
