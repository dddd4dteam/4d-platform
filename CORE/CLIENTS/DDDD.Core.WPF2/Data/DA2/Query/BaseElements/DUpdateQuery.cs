
using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Data.DA2.Query
{
    [DataContract]
    public partial class DUpdateQuery : DQuery, IDBoolean
    {
        //
        // ctor(s)
        //
        #region public DBUpdateQuery()

        /// <summary>
        /// internal constructor - use the DBQuery.Update(...) methods to create a new DBUpdateQuery
        /// </summary>
        public DUpdateQuery() //internal
        {
        }

        #endregion


        //
        // properties
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DUpdateQuery()
            {
                AssignSet = (this.AssignSet != null) ? this.AssignSet.CloneClause() as DAssignSet : null,
                IsInnerQuery = this.IsInnerQuery,
                TableSet = (this.TableSet != null) ? this.TableSet.CloneClause() as DTableSet : null,
                WhereSet = (this.WhereSet != null) ? this.WhereSet.CloneClause() as DFilterSet : null,
                Last = (this.Last != null) ? this.Last.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        #region public  DBTableSet TableSet {get;set;}//internal

        private DTableSet _table;

        /// <summary>
        /// Gets or sets the DBTableSet in this update query
        /// </summary>
        [DataMember]
        public DTableSet TableSet //internal
        {
            get { return this._table; }
            set { this._table = value; }
        }


        public DTable UpdateTable
        {
            get
            {
                if (_table.HasRoot && _table.Root is DTable)
                    return (_table.Root as DTable);
                else return null;
            }
        }

        #endregion

        #region public DBAssignSet AssignSet {get;set;} //internal

        private DAssignSet _assigns;

        /// <summary>
        /// Gets or sets the DBAssignSet in this update query
        /// </summary>
        [DataMember]
        public DAssignSet AssignSet //internal
        {
            get { return this._assigns; }
            set { this._assigns = value; }
        }

        #endregion

        #region public DBFilterSet WhereSet {get; set;} //internal

        private DFilterSet _where;

        /// <summary>
        /// Gets and sets the DBFilterSet for this update query
        /// </summary>
        [DataMember]
        public DFilterSet WhereSet   //internal
        {
            get { return this._where; }
            set { this._where = value; }
        }

        #endregion

        #region public DBClause Last {get;set;} //internal

        private DClause _last;

        /// <summary>
        /// Gets or Sets the last clause used in the statement construction
        /// </summary>
        [DataMember]
        public DClause Last //internal
        {
            get { return this._last; }
            set { this._last = value; }
        }

        #endregion




        //
        // statement construction methods
        //

        #region public DBUpdateQuery Set(string _ModuleInitializer, DBClause toValue) + 2 overloads

        public DUpdateQuery Set(string field, DClause toValue)
        {
            DField fld = DField.Field(field);
            return Set(fld, toValue);
        }

        public DUpdateQuery Set(DClause item, DClause toValue)
        {
            if (_assigns == null)
                _last = _assigns = DAssignSet.Set(item, toValue);
            else
                _last = _assigns.AndSet(item, toValue);
            return this;
        }

        public DUpdateQuery Set(DClause assignment)
        {
            if (_assigns == null)
                _last = _assigns = DAssignSet.Set(assignment);
            else
                _last = _assigns.AndSet(assignment);

            return this;
        }

        #endregion


        //
        // Where... methods
        //

        #region public DBUpdateQuery Where(DBClause left, Compare compare, DBClause right) + 1 overload

        public DUpdateQuery Where(DClause left, Compare compare, DClause right)
        {
            DFilterSet fs = DFilterSet.Where(left, compare, right);
            this._where = fs;
            this._last = fs;

            return this;
        }

        public DUpdateQuery Where(DComparison compare)
        {
            DFilterSet fs = DFilterSet.Where(compare);
            this._where = fs;
            this._last = fs;

            return this;
        }

        #endregion

        #region public DBUpdateQuery WhereFieldEquals(string _ModuleInitializer, DBClause value) + 1 overload

        public DUpdateQuery WhereFieldEquals(string field, DClause value)
        {
            return WhereField(field, Compare.Equals, value);
        }

        public DUpdateQuery WhereFieldEquals(Type fieldTable, string fieldName, DClause value)
        {
            return WhereField(fieldTable, fieldName, Compare.Equals, value);
        }

        public DUpdateQuery WhereFieldEquals<TTable>(string fieldName, DClause value)
        {
            return WhereField<TTable>(fieldName, Compare.Equals, value);
        }


        #endregion

        public DUpdateQuery SetWhereFilter(DFilterSet filter)
        {
            if (filter == null) return this;
            WhereSet = filter;
            return this;
        }


        #region public DBUpdateQuery WhereField(string _ModuleInitializer, ComparisonOperator op, DBClause value) + 2 overload

        public DUpdateQuery WhereField(string field, Compare op, DClause value)
        {
            DField fld = DField.Field(field);
            return Where(fld, op, value);
        }

        public DUpdateQuery WhereField(Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldTable, fieldName);
            return Where(fld, op, value);
        }


        public DUpdateQuery WhereField<TTable>(string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldName);
            return Where(fld, op, value);
        }


        public DUpdateQuery WhereField(string fieldOwner, Type fieldTable, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field(fieldOwner, fieldTable, fieldName);
            return Where(fld, op, value);
        }


        public DUpdateQuery WhereField<TTable>(string fieldOwner, string fieldName, Compare op, DClause value)
        {
            DField fld = DField.Field<TTable>(fieldOwner, fieldName);
            return Where(fld, op, value);
        }


        #endregion

        #region public DBUpdateQuery AndWhere(DBClause left, ComparisonOperator op, DBClause right) + 3 overloads

        public DUpdateQuery AndWhere(DClause left, Compare op, DClause right)
        {
            _where = _where.And(left, op, right);
            _last = _where;

            return this;
        }

        public DUpdateQuery AndWhere(string field, Compare op, DClause right)
        {
            _where = _where.And(field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DUpdateQuery AndWhere(Type table, string field, Compare op, DClause right)
        {
            _where = _where.And(table, field, op, right);
            _last = _where;

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DUpdateQuery AndWhere<TTable>(string field, Compare op, DClause right)
        {
            _where = _where.And<TTable>(field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DUpdateQuery AndWhere(string owner, Type table, string field, Compare op, DClause right)
        {
            _where = _where.And(owner, table, field, op, right);
            _last = _where;

            return this;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public DUpdateQuery AndWhere<TTable>(string owner, string field, Compare op, DClause right)
        {
            _where = _where.And<TTable>(owner, field, op, right);
            _last = _where;

            return this;
        }


        #endregion

        #region public DBUpdateQuery OrWhere(DBClause left, ComparisonOperator op, DBClause right) + 3 overloads

        public DUpdateQuery OrWhere(DClause left, Compare op, DClause right)
        {
            _where = _where.Or(left, op, right);
            _last = _where;

            return this;
        }

        public DUpdateQuery OrWhere(string field, Compare op, DClause right)
        {
            _where = _where.Or(field, op, right);
            _last = _where;

            return this;
        }

        public DUpdateQuery OrWhere<TTable>(string field, Compare op, DClause right)
        {
            _where = _where.Or<TTable>(field, op, right);
            _last = _where;

            return this;
        }

        public DUpdateQuery OrWhere(Type table, string field, Compare op, DClause right)
        {
            _where = _where.Or(table, field, op, right);
            _last = _where;

            return this;
        }




        public DUpdateQuery OrWhere(string owner, Type table, string field, Compare op, DClause right)
        {
            _where = _where.Or(owner, table, field, op, right);
            _last = _where;

            return this;
        }


        public DUpdateQuery OrWhere<TTable>(string owner, string field, Compare op, DClause right)
        {
            _where = _where.Or<TTable>(owner, field, op, right);
            _last = _where;

            return this;
        }


        #endregion


        #region DBUpdateQuery BuildUpdateSetFields(object tableObject, List<String> UpdateOnlyFields)

        ///// <summary>
        ///// Building InsertInto Fields Statements 
        ///// </summary>
        ///// <param name="tableObject">Table Object</param>
        ///// <param name="BesidesFields">Excepting fileds - IdentifyFields.. </param>
        ///// <returns></returns>
        //public DBUpdateQuery BuildUpdateSetFields(object tableObject, List<String> UpdateOnlyFields)
        //{
        //    DBUpdateQuery updateQuery = this; 

        //    if (tableObject == null) return this; // ����� ��� ������ � �� ��������


        //    List<PropertyInfo> TableProperties = tableObject.GetType().GetProperties().ToList<PropertyInfo>();

        //    foreach (var item in UpdateOnlyFields)
        //    {
        //        if ( TableProperties.Where(pr => pr.Name == item).FirstOrDefault() != null)
        //        { // ���  ���� ����� ������� ������� � UpdateOnlyFields

        //            object updateFieldValue = tableObject.GetType().GetProperty(item).GetValue(tableObject, null);
        //            //if (updateFieldValue == null) continue;

        //            updateQuery.Set(item, DBConst.GetValueClause(updateFieldValue));
        //        }
        //        else continue;
        //    }

        //    return this;

        //}

        ///// <summary>
        ///// Building InsertInto Fields Statements 
        ///// </summary>
        ///// <param name="tableObject"></param>
        ///// <param name="UpdateOnlyFields"></param>
        ///// <returns></returns>
        //public DUpdateQuery BuildUpdateSetFields(object tableObject, String[] UpdateOnlyFields)
        //{
        //    DUpdateQuery updateQuery = this;

        //    if (tableObject == null) return this; // ����� ��� ������ � �� ��������

        //    PropertyInfo[] TableProperties = tableObject.GetType().GetProperties();

        //    foreach (var UpdField in UpdateOnlyFields)
        //    {
        //        if (TableProperties.Where(pr => pr.Name == UpdField).FirstOrDefault() != null)
        //        { // ���  ���� ����� ������� ������� � UpdateOnlyFields

        //            object updateFieldValue = tableObject.GetValue(UpdField); //.n GetType().GetProperty(item).GetValue(tableObject, null)
        //            //if (updateFieldValue == null) continue;       //  � ���� ��� ����� ����������� ����

        //            updateQuery.Set(UpdField, DConst.GetValueClause(updateFieldValue, tableObject.GetType().GetProperty(UpdField).IsDefined(typeof(NullableAttribute), false)));
        //        }
        //        else continue;
        //    }

        //    return this;

        //}

        //public DUpdateQuery BuildUpdateSetFields(object tableObject, List<String> UpdateOnlyFields)
        //{
        //    DUpdateQuery updateQuery = this;

        //    if (tableObject == null) return this; // ����� ��� ������ � �� ��������

        //    PropertyInfo[] TableProperties = tableObject.GetType().GetProperties();

        //    foreach (var UpdField in UpdateOnlyFields)
        //    {
        //        if (TableProperties.Where(pr => pr.Name == UpdField).FirstOrDefault() != null)
        //        { // ���  ���� ����� ������� ������� � UpdateOnlyFields

        //            object updateFieldValue = tableObject.GetValue(UpdField); //.n GetType().GetProperty(item).GetValue(tableObject, null)
        //            //if (updateFieldValue == null) continue;       //  � ���� ��� ����� ����������� ����

        //            updateQuery.Set(  UpdField
        //                            , DConst.GetValueClause(updateFieldValue
        //                                        , tableObject.GetType()
        //                                        .GetProperty(UpdField)
        //                                        .IsDefined(typeof(NullableAttribute), false)));

        //        }
        //        else continue;
        //    }

        //    return this;
        //}        //public DUpdateQuery BuildUpdateSetFields(object tableObject, List<String> UpdateOnlyFields)
        //{
        //    DUpdateQuery updateQuery = this;

        //    if (tableObject == null) return this; // ����� ��� ������ � �� ��������

        //    PropertyInfo[] TableProperties = tableObject.GetType().GetProperties();

        //    foreach (var UpdField in UpdateOnlyFields)
        //    {
        //        if (TableProperties.Where(pr => pr.Name == UpdField).FirstOrDefault() != null)
        //        { // ���  ���� ����� ������� ������� � UpdateOnlyFields

        //            object updateFieldValue = tableObject.GetValue(UpdField); //.n GetType().GetProperty(item).GetValue(tableObject, null)
        //            //if (updateFieldValue == null) continue;       //  � ���� ��� ����� ����������� ����

        //            updateQuery.Set(  UpdField
        //                            , DConst.GetValueClause(updateFieldValue
        //                                        , tableObject.GetType()
        //                                        .GetProperty(UpdField)
        //                                        .IsDefined(typeof(NullableAttribute), false)));

        //        }
        //        else continue;
        //    }

        //    return this;
        //}





        /// <summary>
        /// Build Update Set Fields: get values of [updateOnlyFields] from [boItem]
        /// </summary>
        /// <param name="boItem"></param>
        /// <param name="updateOnlyFields"></param>
        /// <returns></returns>
        public DUpdateQuery BuildUpdateSetFields(IBObject boItem, List<String> updateOnlyFields)
        {
            DUpdateQuery updateQuery = this;

            //check is it the same Update TargetTable in boItem            
            Validator.ATInvalidOperationDbg<DUpdateQuery>(UpdateTable.TableType != boItem.Contract
                , nameof(BuildUpdateSetFields)
                , "boItem Contract is Not the same as in this update quwry object");

            if (boItem == null) return this; // ����� ��� ������ � �� ��������

            var TableProperties = boItem.GetDomainPropertyNames();

            int i = -1;
            foreach (var UpdField in updateOnlyFields)
            { // ���  ���� ����� ������� ������� � UpdateOnlyFields
                i++;
                //���� ���� ����������- �������� ��� ����������
                if (TableProperties.NotContains(UpdField)) continue;

                object updateFieldValue = boItem.GetPropertyValue(UpdField);
                //  � ���� ��� ����� ����������� ����                                                                                 

                if (i == 0)
                {
                    updateQuery.Set(UpdField, DConst.GetValueClause(updateFieldValue,
                                                                    boItem.IsPropertyNullable(UpdField)));

                    continue;
                }

                updateQuery.AndSet(UpdField, DConst.GetValueClause(updateFieldValue,
                                                boItem.IsPropertyNullable(UpdField)));

            }

            return this;
        }


        /// <summary>
        /// Build Update Set Fields: set fields by keys and values from [propValuesToUpdate] 
        /// </summary>
        /// <param name="boContract"></param>
        /// <param name="propValuesToUpdate"></param>
        /// <returns></returns>
        public DUpdateQuery BuildUpdateSetFields(Dictionary<string, object> propValuesToUpdate)
        {

            Validator.ATNullReferenceArgDbg<DUpdateQuery>(propValuesToUpdate
                , nameof(BuildUpdateSetFields), nameof(propValuesToUpdate));


            DUpdateQuery updateQuery = this;
            var boInstance = TypeActivator.CreateInstanceTBase<IBObject>(UpdateTable.TableType);
            var TableProperties = boInstance.GetDomainPropertyNames();

            int i = -1;
            foreach (var UpdField in propValuesToUpdate)
            { // ���  ���� ����� ������� ������� � UpdateOnlyFields
                i++;
                //���� ���� ����������- �������� ��� ����������
                if (TableProperties.NotContains(UpdField.Key)) continue;

                //  � ���� ��� ����� ����������� ����                                                                                 

                if (i == 0)
                {
                    updateQuery.Set(UpdField.Key
                        , DConst.GetValueClause(UpdField.Value
                                                , UpdField.Value.GetTypeInfoEx().IsNullableType));

                    continue;
                }

                updateQuery.AndSet(UpdField.Key
                        , DConst.GetValueClause(UpdField.Value
                                                , UpdField.Value.GetTypeInfoEx().IsNullableType));

            }

            return this;
        }




        #endregion public DBUpdateQuery BuildUpdateSetFields(object tableObject, List<String> UpdateOnlyFields)


        #region DBUpdateQuery BuildUpdateWhereFieldsOnAnd(object tableObject, List<String> WhereFields)

        ///// <summary>
        ///// Building InsertInto Fields Statements 
        ///// </summary>
        ///// <param name="tableObject">Table Object</param>
        ///// <param name="BesidesFields">Excepting fileds - IdentifyFields.. </param>
        ///// <returns></returns>
        //public DUpdateQuery BuildUpdateWhereFieldsOnAnd(object tableObject, List<String> WhereFields)
        //{
        //    DUpdateQuery updateQuery = this;

        //    if (tableObject == null) return this; // ����� ��� ������ � �� ��������

        //    List<PropertyInfo> TableProperties = tableObject.GetType().GetProperties().ToList<PropertyInfo>();

        //    for(Int32 i=0;  i< WhereFields.Count ;i++)
        //    {
        //        if (TableProperties.Where(pr => pr.Name == WhereFields[i]).FirstOrDefault() != null)
        //        { // ���  ���� ����� ������� ������� � UpdateOnlyFields

        //            object whereFieldValue = tableObject.GetType().GetProperty(WhereFields[i]).GetValue(tableObject, null);
        //            if (whereFieldValue == null) continue;

        //            //��� ������� ������� ����� WhereFieldEquals
        //            if (i == 0)
        //            {
        //                updateQuery.WhereFieldEquals(WhereFields[i], DConst.GetValueClause(whereFieldValue)); 
        //                continue;
        //            }

        //            // ���������� ��������� �������� Where ����������
        //            updateQuery.AndWhere(WhereFields[i], Compare.Equals, DConst.GetValueClause(whereFieldValue));

        //        }
        //        else continue;
        //    }

        //    return this;

        //}



        /// <summary>
        /// Building InsertInto Fields Statements
        /// </summary>
        /// <param name="tableObject"></param>
        /// <param name="whereFields"></param>
        /// <returns></returns>
        public DUpdateQuery BuildUpdateWhereFieldsOnAnd(IBObject tableObject, List<String> whereFields)
        {
            DUpdateQuery updateQuery = this;

            if (tableObject == null) return this; // ����� ��� ������ � �� ��������

            List<PropertyInfo> TableProperties = tableObject.GetDomainProperties();

            for (Int32 i = 0; i < whereFields.Count; i++)
            {
                if (TableProperties.Where(pr => pr.Name == whereFields[i]).FirstOrDefault() != null)
                { // ���  ���� ����� ������� ������� � UpdateOnlyFields

                    object whereFieldValue = tableObject.GetPropertyValue(whereFields[i]); //.GetValue(tableObject, null);
                    if (whereFieldValue == null) continue;

                    //��� ������� ������� ������ WhereFieldEquals
                    if (i == 0)
                    {
                        updateQuery.WhereFieldEquals(whereFields[i], DConst.GetValueClause(whereFieldValue));
                        continue;
                    }

                    // ���������� ��������� �������� Where ����������
                    updateQuery.AndWhere(whereFields[i], Compare.Equals, DConst.GetValueClause(whereFieldValue));

                }
                else continue;
            }

            return this;

        }



        #endregion DBUpdateQuery BuildUpdateWhereFieldsOnAnd(object tableObject, List<String> WhereFields)


        public DUpdateQuery WithChangeTrackingContext(Guid? ChangeTrackingContext)
        {
            this.ChangeTrackingContext = ChangeTrackingContext;
            return this;
        }


        //
        // statement build method
        //
        #region public override bool BuildStatement(DBStatementBuilder builder)


        /// <summary>
        /// Overriden BuildStatement method to construct a valid SQL Update statement using the DBStatementBuilder parameter
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (ChangeTrackingContext != null)
                builder.WithChangeTrackingContext(ChangeTrackingContext.Value);

            builder.BeginUpdateStatement();

            if (this._table == null)
                throw new InvalidOperationException("No Source was set on the update statement");

            this._table.BuildStatement(builder);

            if (_assigns == null)
                throw new InvalidOperationException("No Source was set on the update statement");
            this._assigns.BuildStatement(builder);

            if (this._where != null)
            {
                builder.BeginWhereStatement();
                this._where.BuildStatement(builder);
                builder.EndWhereStatement();
            }
            return true;
        }

        #endregion

        //
        // xml serialization methods
        //
        //#region protected override string XmlElementName {get;}

        ///// <summary>
        ///// Gets the Xml Element name for this DBUpdateQuery
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get
        //    {
        //        return XmlHelper.Update;
        //    }
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Overrides the default implementation to write all the inner elements to the XmlWriter
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this._table != null)
        //    {
        //        this._table.WriteXml(writer, context);
        //    }

        //    if (this._assigns != null)
        //    {
        //        this.AssignSet.WriteXml(writer, context);
        //    }

        //    if (this._where != null)
        //    {
        //        this.WhereSet.WriteXml(writer, context);
        //    }

        //    return base.WriteInnerElements(writer, context);
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Overrides the default implementation to read the table, assignments and where elements
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b;
        //    if (this.IsElementMatch(XmlHelper.From, reader, context))
        //    {
        //        this.TableSet = (DBTableSet)context.Factory.Read(XmlHelper.From, reader, context);
        //        b = true;
        //    }
        //    else if (this.IsElementMatch(XmlHelper.Assignments, reader, context))
        //    {
        //        this.AssignSet = (DBAssignSet)context.Factory.Read(XmlHelper.Assignments, reader, context);
        //        b = true;
        //    }
        //    else if (this.IsElementMatch(XmlHelper.Where, reader, context))
        //    {
        //        this.WhereSet = (DBFilterSet)context.Factory.Read(XmlHelper.Where, reader, context);
        //        b = true;
        //    }
        //    else
        //        b = base.ReadAnInnerElement(reader, context);


        //    return b;
        //}

        //#endregion



        #region IDBoolean Members

        public DUpdateQuery AndSet(DClause reference)
        {
            return this.Set(reference);
        }

        public DUpdateQuery AndSet(DClause item, DClause toValue)
        {
            return this.Set(item, toValue);
        }

        public DUpdateQuery AndSet(string field, DClause toValue)
        {
            DField fld = DField.Field(field);
            return this.AndSet(fld, toValue);
        }




        DClause IDBoolean.And(DClause reference)
        {
            return this.AndSet(reference);
        }

        DClause IDBoolean.Or(DClause reference)
        {
            throw new Exception("The Or operation is not supported on a DBUpdateQuery.");
        }


        #endregion


        /// <summary>
        /// Get InsertQuery for IBObject item with its providerKey and
        /// build Query text.
        /// </summary>
        /// <param name="insertObject"></param>
        /// <param name="providerKey"></param>
        /// <param name="ChangeTrackingContext"></param>
        /// <returns></returns>
        public static string GetQuery(IBObject updateObject
                                    , string providerKey
                                    , Guid? ChangeTrackingContext = null)
        {
            if ((updateObject is IVobject))
                updateObject = (IBObject)((IVobject)updateObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (updateObject is IPersistableBO), "Delete -target boItem is not IPersistableBO");//false

            var updateProps = updateObject.GetDomainPropertyNamesNoPK();

            // ���������� �������
            return DUpdateQuery.Update(updateObject)
                                .BuildUpdateSetFields(updateObject, updateProps)
                                .BuildUpdateWhereFieldsOnAnd(updateObject, (updateObject as IPersistableBO).PrimaryKey)
                                .WithChangeTrackingContext(ChangeTrackingContext.Value)
                                .ToSQLString(providerKey);
        }


        public static string GetQuery(IBObject updateObject
                                    , List<string> propNames
                                    , string providerKey
                                    , Guid? ChangeTrackingContext = null)
        {
            if ((updateObject is IVobject))
                updateObject = (IBObject)((IVobject)updateObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (updateObject is IPersistableBO), "Delete -target boItem is not IPersistableBO");//false

            // ���������� �������
            return DUpdateQuery.Update(updateObject)
                                .BuildUpdateSetFields(updateObject, propNames)
                                .BuildUpdateWhereFieldsOnAnd(updateObject, (updateObject as IPersistableBO).PrimaryKey)
                                .WithChangeTrackingContext(ChangeTrackingContext.Value)
                                .ToSQLString(providerKey);
        }




        public static string GetQuery(IBObject updateObject
                                        , List<string> propNames
                                        , DFilterSet filter
                                        , string providerKey
                                        , Guid? ChangeTrackingContext = null)
        {
            if ((updateObject is IVobject))
                updateObject = (IBObject)((IVobject)updateObject).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (updateObject is IPersistableBO), "Delete -target boItem is not IPersistableBO");//false

            // ���������� �������
            return DUpdateQuery.Update(updateObject)
                                .BuildUpdateSetFields(updateObject, propNames)
                                .SetWhereFilter(filter)
                                .WithChangeTrackingContext(ChangeTrackingContext.Value)
                                .ToSQLString(providerKey);
        }

        public static string GetQuery(Type updateObjectContract
                                        , Dictionary<string, object> propValues
                                        , DFilterSet filter
                                        , string providerKey
                                        , Guid? ChangeTrackingContext = null)
        {

            Validator.ATInvalidOperationDbg<DUpdateQuery>(
                     updateObjectContract.IsImplementInterface<IPersistableBO>() == false
                , nameof(GetQuery), " Only types thas implement IPersistableBO can be used here - as table Contract");


            // ���������� �������
            return DUpdateQuery.Update(updateObjectContract)
                                .BuildUpdateSetFields(propValues)
                                .SetWhereFilter(filter)
                                .WithChangeTrackingContext(ChangeTrackingContext.Value)
                                .ToSQLString(providerKey);
        }







    }
}
