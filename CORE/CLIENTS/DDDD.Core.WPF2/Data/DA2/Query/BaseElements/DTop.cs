 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a TOP/LIMITS ... restriction for a select query
    /// </summary>
    [DataContract]
    public partial class DTop : DClause  //abstract
    {
        //.ctor
        public DTop() { }
        
        //
        // properties
        //
        #region public double TopValue {get;set;}

        private double _val;

        /// <summary>
        /// Gets or sets the N value to return for the TOP [N] clause
        /// </summary>
        [DataMember]
        public double TopValue
        {
            get { return _val; }
            set { _val = value; }
        }

        #endregion

        #region public double Startoffset {get; set;}

        private double _startoffset = -1.0;

        /// <summary>
        /// Gets or sets the optional start offset to begin returning rows
        /// </summary>
        [DataMember]
        public double StartOffset
        {
            get { return _startoffset; }
            set { _startoffset = value; }
        }

        #endregion

        #region public TopType Type {get;set;}

        private TopType _type;

        /// <summary>
        /// Gets or sets the type of TOP required (count or percent)
        /// </summary>
        [DataMember]
        public TopType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        #endregion

        //
        // static factory methods
        //

        #region public static DBTop Number(int count)
        /// <summary>
        /// Creates and returns a new TOP [count] statement
        /// </summary>
        /// <param name="count">The number of rows to return</param>
        /// <returns></returns>
        public static DTop Number(int count)
        {
            DTop top = new DTop();
            top.TopValue = count;
            top.Type = TopType.Count;
            return top;
        }

        #endregion

        #region public static DBTop Percent(double percent)

        /// <summary>
        /// Creates and returns a new TOP [percent] PERCENT statement
        /// </summary>
        /// <param name="percent"></param>
        /// <returns></returns>
        public static DTop Percent(double percent)
        {
            DTop top = new DTop();
            top.TopValue = percent;
            top.Type = TopType.Percent;
            return top;
        }

        #endregion

        #region public static DBTop Top(double value, TopType type)

        /// <summary>
        /// Creates a returns a new TOP (LIMITS) statment with the specified restrictions
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DTop Top(double value, TopType type)
        {
            DTop top = new DTop();
            top.Type = type;
            top.TopValue = value;

            return top;
        }

        #endregion




        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DTop()
            { StartOffset = this.StartOffset,
                 TopValue = this.TopValue, 
                  Type = this.Type                
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
               


        /// <summary>
        /// Create a new LIMITS [range...] statement
        /// </summary>
        /// <param name="count"></param>
        /// <param name="startOffset"></param>
        /// <returns></returns>
        public static DTop Range(int count, int startOffset)
        {
            DTop top = new DTop();
            top.Type = TopType.Range;
            top.TopValue = count;
            top.StartOffset = startOffset;

            return top;
        }

        #region internal static DBClause Top()

        internal static DClause Top()
        {
            DTop top = new DTop();
            return top;
        }

        #endregion


        #region public override bool BuildStatement(DBStatementBuilder builder)


        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (this.TopValue > 0.0)
            {
                builder.WriteTop(this.TopValue, this.StartOffset, this.Type);
                return true;
            }
            else
                return false;
        }


        #endregion



    }

    //[DataContract]
    //public partial class DBTopRef : DBTop
    //{

    //    public DBTopRef() { }




    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBTopRef()
    //        {
    //            StartOffset = this.StartOffset,
    //            TopValue = this.TopValue,
    //            Type = this.Type
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
               



    //    //
    //    // SQL Statement builder methods
    //    //


    //    //
    //    // XML Serialization
    //    //
    //    //#region protected override string XmlElementName {get;}

    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Top; }
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b = true;
    //    //    if (this.IsAttributeMatch(XmlHelper.TopValue, reader, context))
    //    //        this.TopValue = int.Parse(reader.Value);
    //    //    else if (this.IsAttributeMatch(XmlHelper.TopType, reader, context))
    //    //        this.Type = (TopType)Enum.Parse(typeof(TopType), reader.Value);
    //    //    else
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.WriteAttribute(writer, XmlHelper.TopValue, this.TopValue.ToString(), context);
    //    //    this.WriteAttribute(writer, XmlHelper.TopType, this.Type.ToString(), context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion
    //}
}
