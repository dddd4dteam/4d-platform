using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// DBClaculable clause extends the DClause class to support calculation operations such as plus, minus etc.
    /// It also implements the operator overloads for the standard operations (+,-,/, etc...)
    /// </summary>
    /// <remarks>If you want your classes to support inclusion in calculations then inherit from this class</remarks>
    [DataContract]
    public partial class DCalculableClause : DClause, IDCalculable //abstract
    {


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DCalculableClause();
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
                


        /// <summary>
        /// 
        /// </summary>
        public DCalculableClause() { }

        #region public virtual DBCalc Calculate(BinaryOp op, DBClause clause)

        /// <summary>
        /// Add a calculation operation to the statement using this instance as the left side, 
        /// and the clause as the right side, with the specified operation
        /// </summary>
        /// <param name="op"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public virtual DCalc Calculate(BinaryOp op, DClause clause)
        {
            DCalc calc = DCalc.Calculate(this, op, clause);
            return calc;
        }

        #endregion

        #region public DBCalc Plus(int value) + 2 overloads

        /// <summary>
        /// Add an addition operation to this clause
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Plus(int value)
        {
            return Plus(DConst.Const(value));
        }

        /// <summary>
        /// Add an addition operation to this clause
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Plus(double value)
        {
            return Plus(DConst.Const(value));
        }

        /// <summary>
        /// Add an addition operation to this clause
        /// </summary>
        /// <param name="dbref"></param>
        /// <returns></returns>
        public virtual DCalc Plus(DClause dbref)
        {
            DCalc calc = DCalc.Plus(this, dbref);
            return calc;
        }

        #endregion

        #region public DBCalc Minus(int value) + 2 overloads

        /// <summary>
        /// Add a subtraction to this operation
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Minus(int value)
        {
            return Minus(DConst.Const(value));
        }

        /// <summary>
        /// Add a subtraction operation to this clause
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Minus(double value)
        {
            return Minus(DConst.Const(value));
        }

        /// <summary>
        /// Add a subtraction operation to this clause
        /// </summary>
        /// <param name="dbref"></param>
        /// <returns></returns>
        public virtual DCalc Minus(DClause dbref)
        {
            DCalc calc = DCalc.Minus(this, dbref);
            return calc;
        }

        #endregion

        #region public DBCalc Times(int value) + 2 overloads

        /// <summary>
        /// Add a multipy operation to this clause
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Times(int value)
        {
            return Times(DConst.Const(value));
        }
        
        /// <summary>
        /// Add a multiply operation to this clause
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Times(double value)
        {
            return Times(DConst.Const(value));
        }

        /// <summary>
        /// Add a multiply operation to this clause
        /// </summary>
        /// <param name="dbref"></param>
        /// <returns></returns>
        public virtual DCalc Times(DClause dbref)
        {
            DCalc calc = DCalc.Times(this, dbref);
            return calc;
        }

        #endregion

        #region public DBCalc Divide(int value)

        /// <summary>
        /// Adds a division operation to the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Divide(int value)
        {
            return Divide(DConst.Const(value));
        }

        /// <summary>
        ///  Adds a division operation to the statement
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public DCalc Divide(double value)
        {
            return Divide(DConst.Const(value));
        }

        /// <summary>
        ///  Adds a division operation to the statement
        /// </summary>
        /// <param name="dbref"></param>
        /// <returns></returns>
        public virtual DCalc Divide(DClause dbref)
        {
            DCalc calc = DCalc.Divide(this, dbref);
            return calc;
        } 
        
        #endregion

        #region public virtual DBCalc Modulo(DBClause dbref)

        /// <summary>
        ///  Adds a modulo (remainder) operation to the statement
        /// </summary>
        /// <param name="dbref"></param>
        /// <returns></returns>
        public virtual DCalc Modulo(DClause dbref)
        {
            DCalc calc = DCalc.Modulo(this, dbref);
            return calc;
        }

        #endregion


        #region IDBCalculable Members


        DClause IDCalculable.Calculate(BinaryOp op, DClause dbref)
        {
            return this.Calculate(op, dbref);
        }

        #endregion

        //
        // operator overrides
        //

        /// <summary>
        /// Add left and right clauses
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableClause operator +(DCalculableClause left, DClause right)
        {
            return left.Plus(right);
        }

        /// <summary>
        /// Subtract left and right clauses
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableClause operator -(DCalculableClause left, DClause right)
        {
            return left.Minus(right);
        }

        /// <summary>
        /// Multiply left and right clauses
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableClause operator *(DCalculableClause left, DClause right)
        {
            return left.Times(right);
        }

        /// <summary>
        /// Divide left and right clauses
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableClause operator /(DCalculableClause left, DClause right)
        {
            return left.Divide(right);
        }


        /// <summary>
        /// Get the modulo of the left and right clauses
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DCalculableClause operator %(DCalculableClause left, DClause right)
        {
            return left.Modulo(right);
        }
    }
}
