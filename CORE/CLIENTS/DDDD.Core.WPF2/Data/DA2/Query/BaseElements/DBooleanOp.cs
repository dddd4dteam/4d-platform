 
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a boolean operation - AND, OR, XOR
    /// </summary>
    [DataContract]
    public partial class DBooleanOp : DCalculableClause  //abstract
    {
        public DBooleanOp() { }


        #region public DBClause Left {get;set;}

        private DClause _left;

        /// <summary>
        /// Gets or sets the left argument of the operation
        /// </summary>
        [DataMember]
        public DClause Left 
        {
            get { return _left; }
            set { _left = value; }
        }

        #endregion

        #region public BooleanOp Operator {get;set;}

        private BooleanOp _op;
        /// <summary>
        /// Gets or sets the boolean operator.
        /// </summary>
        [DataMember]
        public BooleanOp Operator
        {
            get { return _op; }
            set { _op = value; }
        }

        #endregion

        #region public DBClause Right {get;set;}

        private DClause _right;
        /// <summary>
        /// Gets or sets the right argument of the operation.
        /// </summary>
        [DataMember]
        public DClause Right
        {
            get { return _right; }
            set { _right = value; }
        }

        #endregion
        

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DBooleanOp()
            { 
                Left = (this.Left != null) ? this.Left.CloneClause() : null,
                Operator = this.Operator,
                Right = (this.Right != null) ? this.Left.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        #region  ------------------------ Compare Two DBBooleanOp Objects ---------------------------

        /// <summary>
        /// False- ��� ��������� �� �����
        /// True- �������� ��� ��������� ���������
        /// </summary>
        /// <param name="AnotherObject"></param>
        /// <returns></returns>
        public override bool CompareWith(DClause AnotherObject)
        {
            bool compareResult = true; //������� �� ������� ��� ������� ���������

            if ((AnotherObject as DBooleanOp ) == null) { compareResult = false;  return compareResult; }// ������ ���� ������ ������������ ������� ����� null � ������ �� ����� �� null  �� ���� ����� ��� �����������
            DBooleanOp anBooleanOp = AnotherObject as DBooleanOp;


            //������ ��� ������ ���� � ����� ������ �������� �� �����
            if ( Left.CompareWith( anBooleanOp.Left) == false) { compareResult = false;  return compareResult; }
                else if ( Right.CompareWith(anBooleanOp.Right) == false ) { compareResult = false;  return compareResult; }
                else if ( Operator != anBooleanOp.Operator ) { compareResult = false;  return compareResult; }
            
            
            return compareResult;
        }

        #endregion ------------------------ Compare Two  DBBooleanOp Objects ---------------------------





        //
        // static factory method(s)
        //

        #region public static DBBooleanOp Compare(DBClause left, BooleanOp op, DBClause right)

        /// <summary>
        /// Creates a new DBBooleanOp to compare the left and right clauses using the specified boolean operator
        /// </summary>
        /// <param name="left"></param>
        /// <param name="op"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static DBooleanOp Compare(DClause left, BooleanOp op, DClause right)
        {
            DBooleanOp oref = new DBooleanOp();
            oref.Left = left;
            oref.Right = right;
            oref.Operator = op;
            return oref;
        }

        #endregion

        #region internal static DBBooleanOp Compare()

        internal static DBooleanOp Compare()
        {
            return new DBooleanOp();
        }

        #endregion



       
       #region public override bool BuildStatement(DBStatementBuilder builder)
       
       public override bool BuildStatement(DStatementBuilder builder)
       {
           builder.BeginBlock();
           Left.BuildStatement(builder);
           builder.WriteOperator((Operator)Enum.Parse(typeof(Operator), this.Operator.ToString(), false));
           Right.BuildStatement(builder);
           builder.EndBlock();
           return true;
       }
       
       #endregion




    }




//   [DataContract]
//    public partial class DBBooleanOpRef : DBBooleanOp
//    {
//       public DBBooleanOpRef() { }


//       #region  ------------------ IClonableClause.CloneClause -----------------------

//       /// <summary>
//       /// Cloning Clause
//       /// </summary>
//       /// <returns></returns>
//       public override DBClause CloneClause()
//       {
//           return new DBBooleanOpRef()
//           {
//               Left = (this.Left != null) ? this.Left.CloneClause() : null,
//               Operator = this.Operator,
//               Right = (this.Right != null) ? this.Left.CloneClause() : null
//           };
//       }

//       #endregion  ------------------ IClonableClause.CloneClause -----------------------


        
//        ////
//        //// XML Serialization
//        ////
//        //protected override string XmlElementName
//        //{
//        //    get { return XmlHelper.BooleanOperator; }
//        //}

//        //
//        // SQL Statement builder
//        //
//        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
//        //{
//        //    this.WriteAttribute(writer, XmlHelper.Operator, this.Operator.ToString(), context);

//        //    return base.WriteAllAttributes(writer, context);
//        //}

//        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
//        //{
//        //    if (this.Left != null)
//        //    {
//        //        this.WriteStartElement(XmlHelper.LeftOperand, writer, context);
//        //        this.Left.WriteXml(writer, context);
//        //        this.WriteEndElement(XmlHelper.LeftOperand, writer, context);
//        //    }

//        //    if (this.Right != null)
//        //    {
//        //        this.WriteStartElement(XmlHelper.RightOperand, writer, context);
//        //        this.Right.WriteXml(writer, context);
//        //        this.WriteEndElement(XmlHelper.RightOperand, writer, context);
//        //    }

//        //    return base.WriteInnerElements(writer, context);
//        //}

//        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
//        //{
//        //    bool b;
//        //    if (this.IsAttributeMatch(XmlHelper.Operator, reader, context))
//        //    {
//        //        this.Operator = (BooleanOp)Enum.Parse(typeof(BooleanOp), reader.Value, true);
//        //        b = true;
//        //    }
//        //    else
//        //        b = base.ReadAnAttribute(reader, context);

//        //    return b;
//        //}

//        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
//        //{
//        //    bool b;

//        //    if (this.IsElementMatch(XmlHelper.LeftOperand, reader, context) && !reader.IsEmptyElement && reader.Read())
//        //    {
//        //        this.Left = this.ReadNextInnerClause(XmlHelper.LeftOperand, reader, context);
//        //        b = true;
//        //    }
//        //    else if (this.IsElementMatch(XmlHelper.RightOperand, reader, context) && !reader.IsEmptyElement && reader.Read())
//        //    {
//        //        this.Right = this.ReadNextInnerClause(XmlHelper.RightOperand, reader, context);
//        //        b = true;
//        //    }
//        //    else
//        //        b = base.ReadAnInnerElement(reader, context);
//        //    return b;
//        //}
        
        
//    }
}
