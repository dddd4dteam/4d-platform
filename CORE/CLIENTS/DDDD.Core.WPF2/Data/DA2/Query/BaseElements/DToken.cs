
using System;
using System.Runtime.Serialization;


namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// The DBToken class is the base class for all the 
    /// SQL statement elements in the Query library
    /// </summary>
    [DataContract]
    public partial class DToken //abstract
    {
        public DToken() { }

        //
        // abstract BuildStatement method
        //
        /// <summary>
        /// Abstract method that inheriting classes must override to build statements
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder)
        {
            try 
	        {	        
		        return false;
             //Need To be overriden 
	        }
	        catch (Exception exc) 
	        {		
		        throw exc;
	        }
        }
        //abstract
    }
}
