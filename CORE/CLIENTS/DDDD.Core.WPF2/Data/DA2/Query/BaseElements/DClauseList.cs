 
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// A list of clauses that supports statment building and xml generation
    /// </summary>
    [CollectionDataContract]
    //[InfrastructureServiceModel(Meta.Systems.InfrastructureService_RegisterEn.Query)]
    public partial class DClauseList : List<DClause>, ICloneable // DBTokenList<DBClause>
    {
        /// <summary>
        /// 
        /// </summary>
        public DClauseList() { } //set { ;}   // set { ;} //set { ;} //set { ;}


        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public  object Clone()
        {
            DClauseList newClauseList = new DClauseList();
            foreach (var item in  this)
            {   newClauseList.Add(item.CloneClause());  }

            return newClauseList;
        }



        

        /// <summary>
        /// Builds this list of tokens onto the statement
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder)
        {
            return this.BuildStatement(builder, false, false);
        }


        /// <summary>
        /// Builds the list of tokens 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="eachOnNewLine"></param>
        /// <param name="indent"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder, bool eachOnNewLine, bool indent)
        {
            int count = 0;

            bool outputSeparator = false; //initially false

            string start = DTokenListOptions.ListStart;
            if (!string.IsNullOrEmpty(start))
                builder.WriteRaw(DTokenListOptions.ListStart);

            for (int i = 0; i < this.Count; i++)
            {
                DToken clause = this[i];

                if (outputSeparator)
                {
                    if (DTokenListOptions.UseBuilderSeparator)
                        builder.AppendReferenceSeparator();

                    else if (!string.IsNullOrEmpty(DTokenListOptions.TokenSeparator))
                        builder.WriteRaw(DTokenListOptions.TokenSeparator);

                    if (eachOnNewLine)
                        builder.BeginNewLine();
                }

                //if the clause outputs then it should return true - so the next item has a separator written
                outputSeparator = clause.BuildStatement(builder);

                if (outputSeparator) //if we were output then increment the count
                    count++;
            }

            string end = DTokenListOptions.ListEnd;
            if (!string.IsNullOrEmpty(end))
                builder.WriteRaw(end);

            //did we write anything
            if (count > 0 || !string.IsNullOrEmpty(start) || !string.IsNullOrEmpty(end))
                return true;
            else
                return false;

        }


        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //}

        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);

        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {
                    

        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add(c);

        //            if (isEmpty)
        //                return true;
        //        }

        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());

        //    return true;
            
        //}







    }



    /// <summary>
    /// Generic version of the DBClauseList
    /// </summary>
    /// <typeparam name="T"></typeparam>
    //[CollectionDataContract] - not defined Generic<T>  cannot be serialized by WCF
    public class DClauseList<T> : DClauseList ,ICloneable
        where T : DClause
    {
        public DClauseList() { }


        /// <summary>
        /// ��������������� ������ ������������
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DClauseList<T> newClauseList = new DClauseList<T>();
            foreach (var item in this)
            { newClauseList.Add(item.CloneClause()); }

            return newClauseList;
        }



    }
}
