
using DDDD.Core.Data.DA2.Mapping;
using System;
using System.Data;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a constant value that will be generated explicitly on the statement.
    /// </summary>
    /// <remarks>Most system value types can also be cast as DBConst 
    /// values through opertator overloading.
    /// As it inherits from DBCalculableClause instance can also be 
    /// included in calculation expressions</remarks>
    [DataContract]
    public partial class DConst : DCalculableClause, IDAlias //abstract
    {
        /// <summary>
        /// 
        /// </summary>
        public DConst() { }

        //
        // propterties
        //

        #region public object Value

        private object _val;
        /// <summary>
        /// Gets the value of this constant
        /// </summary>
        [DataMember]
        public object Value
        {
            get { return _val; }
            set { _val = value; } //protected
        }

        #endregion

        #region public DbType Type

        private DbType _type = DbType.Object;
        /// <summary>
        /// Gets the DbType of this constant.
        /// </summary>
        [DataMember]
        public DbType Type
        {
            get { return _type; }
            set { _type = value; } //protected
        }

        #endregion

        #region public string Alias
        
        private string _alias;

        /// <summary>
        /// Gets the alias name of this constant if set
        /// </summary>
        [DataMember] 
        public string Alias 
        {
            get { return this._alias; }
            set { this._alias = value; } //protected
        }

        #endregion

        //
        // static methods
        //

        #region public static DBConst Const(DbType type, object value)
        /// <summary>
        /// Creates a new constant of the specified type and with the specified value
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Const(DbType type, object value)
        {
            DConst c = new DConst();
            c.Type = type;
            c.Value = value;

            return c;
        }

        #endregion

        #region public static DBConst Const(object value)

        /// <summary>
        /// Creates a new constant with the specified value, and a DbType extracted from the value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Const(object value)
        {
            //if (value is string)
            //    return DConst.String((string)value);
            //else if (value is DateTime)
            //    return DConst.DateTime((DateTime)value);
            //else if (value is double)
            //    return DConst.Double((double)value);
            //else if (value is Guid)
            //    return DConst.Guid((Guid)value);
            //else if (value is int)
            //    return DConst.Int32((int)value);
            //else if (value is bool)
            //    return (bool)value
            //                    ? DConst.Const(1)
            //                    : DConst.Const(0);

            return Const(GetDbType(value), value);
        }

        #endregion

        #region protected static System.Data.DbType GetDbType(object val)
        /// <summary>
        /// Gets the DbType of a value
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        protected static System.Data.DbType GetDbType(object val)
        {
            System.Data.DbType type = DBTypeHelper.GetDBTypeForObject(val);
            return type;
        }

        
        #endregion

        #region public static DBConst Int32(int value)
        /// <summary>
        /// Creates a new Int32 typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Int32(int value)
        {
            return Const(DbType.Int32, value);
        }

        #endregion


        #region  ------------------------ Compare Two  DBConst Objects ---------------------------
        
        /// <summary>
        /// False- ��� ��������� �� �����
        /// True- �������� ��� ��������� ���������
        /// </summary>
        /// <param name="AnotherObject"></param>
        /// <returns></returns>
        public override bool CompareWith(DClause AnotherObject)
        {
            bool compareResult = true;
            if (AnotherObject as DConst == null) { compareResult = false; return compareResult; } //�.�. ������ �� ����������
            DConst anConst = AnotherObject as DConst;

            //������ ��� ������ ���� � ����� ������ �������� �� �����                        
            if (Alias != anConst.Alias) { compareResult = false; return compareResult; }
            else if (Type != anConst.Type) { compareResult = false; return compareResult; }
            else if ((Value == null && anConst.Value != null) || (Value != null && anConst.Value == null))
            { compareResult = false; return compareResult; }
            else if (Value == null && anConst.Value == null)
            { compareResult = true; return compareResult; }
            else if (Value.ToString() != anConst.Value.ToString()) { compareResult = false; return compareResult; }

            return compareResult;
        }


        #endregion ------------------------ Compare Two  DBConst Objects ---------------------------



        #region public static DBConst String(string value)
        /// <summary>
        /// Creates a new String typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst String(string value)
        {
            return Const(DbType.String, value);
        }

        #endregion

        #region public static DBConst DateTime(DateTime value)
        /// <summary>
        /// Creates a new DateTime typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst DateTime(DateTime value)
        {
            return Const(DbType.DateTime, value);
        }

        /// <summary>
        /// Creates a new Date typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Date(DateTime value)
        {
            return Const(DbType.Date, value);
        }

        #endregion

        #region public static DBConst Guid(Guid value)
        /// <summary>
        /// Creates a new Guid typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Guid(Guid value)
        {
            return Const(DbType.Guid, value);
        }

        #endregion

        #region public static DBConst Double(double value)
        /// <summary>
        /// Creates a new Double typed constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static DConst Double(double value)
        {
            return Const(DbType.Double, value);
        }

        #endregion

        #region public static DBConst Null()
        /// <summary>
        /// Creates a new NULL constant. The DbType will always be object
        /// </summary>
        /// <returns></returns>
        public static DConst Null()
        {
            return Const(DbType.Object, null); // DBNull.Value
        }

        /// <summary>
        /// Creates a new NULL constant - but with an explicit type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static DConst Null(DbType type)
        {
            return Const(type, null );// DBNull.Value
        }

        #endregion

        public static DConst Binary(byte[] value)
        {
            return Const(DbType.Binary, value);
        }


        #region public DBConst GetValueClause(object FieldValue)

        ///  <summary>
        /// �������� ��������� DB Const  ��������������� ���� ������ �������� Value
        ///  </summary>
        ///  <param name="FieldValue"></param>
        /// <param name="isNullable">������� ����� ���� NULL</param>
        /// <returns></returns>
        public static DConst GetValueClause(object FieldValue, bool isNullable = true)
        {
            try
            {               
                if (FieldValue == null) return DConst.Null();
                
                Type itmType = FieldValue.GetType();

                if (itmType == typeof(int?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Int32);
                }
                else if (itmType == typeof(int) || itmType == typeof(int?))
                {
                    return DConst.Int32((Int32)FieldValue);
                }



                if ((itmType == typeof(string) && FieldValue == null)
                    ||
                     (itmType == typeof(string) && (FieldValue as string).Length == 0 && isNullable)
                   )
                {
                    return DConst.Null(DbType.String);
                }
                else if (itmType == typeof(string))
                {
                    return DConst.String((string)FieldValue);
                }

                if (itmType == typeof (char))
                    return DConst.String(((char)FieldValue).ToString());

                if (itmType == typeof(decimal?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Decimal);
                }
                else if (itmType == typeof(decimal) || itmType == typeof(decimal?))
                {
                    return DConst.Double((double)(decimal)FieldValue);
                }


                if (itmType == typeof(double?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Double);
                }
                else if (itmType == typeof(double) || itmType == typeof(double?))
                {
                    return DConst.Double((double)FieldValue);
                }


                if (  (itmType == typeof(DateTime) || itmType == typeof(DateTime?))
                      && FieldValue == null
                    )
                {
                    return DConst.Null(DbType.DateTime);
                }
                else if (itmType == typeof(DateTime) || itmType == typeof(DateTime?))
                {
                    //�������� �� ������� ���� ���  1�� ������ ���� 2 ��� ������� � ����  - ��� ����� ����� �������
                    System.DateTime outDtTime = GetCorrectDateTime((DateTime)FieldValue);

                    return DConst.DateTime(outDtTime);
                }


                if (itmType == typeof(float?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Double);
                }
                if (itmType == typeof(float) || itmType == typeof(float?))
                {
                    return DConst.Double((double)FieldValue);
                }


                if (itmType == typeof(bool?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Boolean);
                }
                else if (itmType == typeof(bool) || itmType == typeof(bool?))
                {
                    if (((bool?)FieldValue) == true)
                    {
                        return DConst.Int32(1);
                    }
                    else if (((bool?)FieldValue) == false)
                    {
                        return DConst.Int32(0);
                    }
                }


                if (itmType == typeof(long?) && FieldValue == null)
                {
                    return DConst.Null(DbType.Int64);
                }
                else if (itmType == typeof(long) || itmType == typeof(long?))
                {
                    return DConst.Int32((int)FieldValue);
                }


                if (itmType == typeof(Guid?) &&  FieldValue == null)            //????
                {
                    return DConst.Null(DbType.Guid);
                }
                else if (itmType == typeof(Guid) || itmType == typeof(Guid?))
                {
                    return DConst.Guid((Guid)FieldValue);
                }

                if (itmType == typeof(byte[]) && FieldValue == null)
                    return DConst.Null(DbType.Binary);
                else if (itmType == typeof(byte[]))
                    return DConst.Binary((byte[])FieldValue);

                    return DConst.Null();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion public DBConst GetValueClause(object FieldValue)

        /// <summary>
        /// �������� ����-����� ����� ������ ���� ��� ������� � ���� ����, ���� �������� �� ������� �������
        /// </summary>
        /// <param name="dateTmValue"></param>
        /// <returns></returns>
        private static DateTime GetCorrectDateTime(DateTime dateTmValue)
        {
            if (((DateTime)dateTmValue) == System.DateTime.MinValue)
            {
                return System.DateTime.Now;
            }
            /*else if (  ((DateTime)dateTmValue) != System.DateTime.MinValue
                         && ((DateTime)dateTmValue).Hour == 0 && ((DateTime)dateTmValue).Minute == 0

                        )
            {
                System.DateTime dtTimeNow = System.DateTime.Now;
                return new DateTime(year: ((DateTime)dateTmValue).Year,
                                          month: ((DateTime)dateTmValue).Month,
                                          day: ((DateTime)dateTmValue).Day,
                                            hour: dtTimeNow.Hour,
                                            minute: dtTimeNow.Minute,
                                            second: dtTimeNow.Second);
            }*/
            
            return dateTmValue;
        }





        //
        // operator overloads
        //

        #region public static explicit operator DBConst(int value)
        /// <summary>
        /// Generates a new Int32 constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(int value)
        {
            return Const(value);
        }

        #endregion

        #region public static explicit operator DBConst(string value)
        /// <summary>
        /// Generates an new string constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(string value)
        {
            return Const(value);
        }

        #endregion

        #region public static explicit operator DBConst(DateTime value)
        /// <summary>
        /// Generates a new DateTime constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(DateTime value)
        {
            return Const(value);
        }

        #endregion

        #region public static explicit operator DBConst(Guid value)
        /// <summary>
        /// Generates a new Guid constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(Guid value)
        {
            return Const(value);
        }

        #endregion

        #region public static explicit operator DBConst(double value)
        /// <summary>
        /// Generates a new double constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(double value)
        {
            return Const(value);
        }

        #endregion

        #region public static explicit operator DBConst(DBNull value)
        /// <summary>
        /// Generates a new DBNull constant
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static explicit operator DConst(DBNull value)
        {
            return DConst.Null();
        }

        #endregion


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DConst()
            { 
               Alias = this.Alias,
              Type = this.Type,
              Value = (this.Value != null && this.Value is ICloneable) ? (this.Value as ICloneable).Clone() : null                
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        



        #region public override bool BuildStatement(DBStatementBuilder builder)

        /// <summary>
        /// ��������� ��������� 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (null == this.Value || this.Value is DBNull)
                builder.WriteNull();
            else
                builder.WriteLiteral(this.Type, this.Value);
            if (string.IsNullOrEmpty(this.Alias) == false)
                builder.WriteAlias(this.Alias);

            return true;
        }


        /// <summary>
        /// ��������� ������ �������� Like
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns> 
        public bool BuildLikeRightStatement(DStatementBuilder builder) //override
        {
            if (null == this.Value || this.Value is DBNull)
                builder.WriteNull();
            else
                builder.WriteFormat(@"'%{0}%'" , Value);
            

            return true;
        }

        #endregion



        /// <summary>
        /// IDBAlias ���������
        /// </summary>
        /// <param name="aliasName"></param>
        public void As(string aliasName)
        {
            Alias = aliasName;            
        }

    }



//    [DataContract]
//    public partial class DBConstRef : DBConst
//    {

//        #region   public DBConstRef()

//        public DBConstRef() { }

//        #endregion


//        //
//        // ctor
//        //

//        //
//        // SQL Statement builder methods
//        //


//        #region  ------------------ IClonableClause.CloneClause -----------------------

//        /// <summary>
//        /// Cloning Clause
//        /// </summary>
//        /// <returns></returns>
//        public override DBClause CloneClause()
//        {
//            return new DBConstRef()
//            {
//                Alias = this.Alias,
//                Type = this.Type,
//                Value = (this.Value != null && this.Value is ICloneable) ? (this.Value as ICloneable).Clone() : null
//            };
//        }

//        #endregion  ------------------ IClonableClause.CloneClause -----------------------



//
//#endif
//        //
//        // XML Serialization Methods
//        //

//        //#region protected override string XmlElementName {get;}

//        //protected override string XmlElementName
//        //{
//        //    get { return XmlHelper.Constant; }
//        //}

//        //#endregion

//        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        
//        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
//        //{
//        //    this.WriteAttribute(writer, XmlHelper.DbType, this.Type.ToString(), context);

//        //    if(string.IsNullOrEmpty(this.Alias) == false)
//        //        this.WriteAlias(writer, this.Alias, context);

//        //    bool isNull = (null == this.Value || this.Value is DBNull);

//        //    this.WriteAttribute(writer, XmlHelper.IsNull, isNull.ToString(), context);
            
//        //    return base.WriteAllAttributes(writer, context);
//        //}

//        //#endregion

//        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
//        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
//        //{
//        //    XmlHelper.WriteNativeValue(this.Value, writer, context);
//        //    return true;
//        //}

//        //#endregion

       
//        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
//        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
//        //{
//        //    bool b = true;

//        //    if (this.IsAttributeMatch(XmlHelper.DbType, reader, context))
//        //        this.Type = (DbType)Enum.Parse(typeof(DbType), reader.Value, true);
//        //    else if (this.IsAttributeMatch(XmlHelper.IsNull, reader, context) && bool.TrueString.Equals(reader.Value))
//        //        this.Value = DBNull.Value;
//        //    else if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
//        //    {
//        //        this.Alias = reader.Value;
//        //        b = true;
//        //    }
//        //    else
//        //        b = base.ReadAnAttribute(reader, context);
//        //    return b;
//        //}

//        //#endregion

//        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
//        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
//        //{
//        //    if (reader.NodeType == System.Xml.XmlNodeType.Text && reader.Value == XmlHelper.NullString)
//        //        this.Value = DBNull.Value;
//        //    else
//        //    {
//        //        if (reader.IsEmptyElement == false)
//        //        {
//        //            string end = reader.LocalName;

//        //            do
//        //            {
//        //                if (reader.NodeType == System.Xml.XmlNodeType.Element && this.IsElementMatch(XmlHelper.ParameterValue, reader, context))
//        //                {
//        //                    this.Value = XmlHelper.ReadNativeValue(reader, context);
//        //                }
//        //                if (reader.NodeType == System.Xml.XmlNodeType.EndElement && this.IsElementMatch(end, reader, context))
//        //                    break;
//        //            }
//        //            while (reader.Read());
//        //        }
//        //    }
//        //    return base.ReadAnInnerElement(reader, context);
//        //}

//        //#endregion

//    }


}
