 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using DDDD.Core.Data.DA2.Query;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// A database engine function
    /// </summary>
    [DataContract]
    public partial class DFunction : DClause, IDAlias //abstract
    {

        public DFunction() { }
         

        private Function _func;
        private string _funcname;
        private DClauseList _params;
        private string _alias;

        //
        // properties
        //


        #region public Function KnownFunction {get;set;}

        /// <summary>
        /// Gets or Sets the function to execute in the statement
        /// </summary>
        [DataMember]
        public Function KnownFunction
        {
            get { return _func; }
            set { _func = value; this._funcname = null; }
        }

        #endregion

        
        #region --------- public string FunctionName {get;set;} 
        
        /// <summary>
        /// Gets or Sets the name of the Function to Execute in the statement
        /// </summary>
        [DataMember]
        public string FunctionName
        {
            get 
            {
                if (null == _funcname)
                {
                    if (this._func != DDDD.Core.Data.DA2.Query.Function.Unknown)
                        _funcname = this._func.ToString();
                    else
                        _funcname = string.Empty;
                }
                return _funcname;
            }
            set 
            {
                _funcname = value;
                if (string.IsNullOrEmpty(_funcname))
                    this._func = DDDD.Core.Data.DA2.Query.Function.Unknown;
                else
                {
                   
                    #if !SILVERLIGHT
                   
                    //Data.Function func = Data.Function.                                         
                    if (Array.IndexOf<string>(Enum.GetNames(typeof(Function)), value) > -1)
                    {
                        this._func = (Function)Enum.Parse(typeof(Function), value, true);
                    }


                    #endif

                    #if SILVERLIGHT
                       this._func = (Function)Enum.Parse(typeof(Function), value, true);
                    #endif
                }

            }
        }

        #endregion

        #region public DBClauseList Parameters {get;} + bool HasParameters {get;}

        /// <summary>
        /// Gets the parameter list for this function
        /// </summary>
        [DataMember]
        public DClauseList Parameters
        {
            get 
            {
                if (this._params == null)
                    this._params = new DClauseList();
                return _params;
            }
            set { _params = value; }
        }


        
        /// <summary>
        /// Returns true if this function has one or more parameters
        /// </summary>        
        internal bool HasParameters
        {
            get {      return _params != null && this._params.Count > 0;
            }            
        }

        #endregion

        #region public string Alias {get;set;}

        /// <summary>
        /// Gets or sets the optional alias name for the output of this Function
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _alias; }
            set { _alias = value; }
        }

        #endregion

        //
        // static factory methods
        //


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DFunction()
            {  
              Alias = this.Alias,
              FunctionName = this.FunctionName, 
              KnownFunction = this.KnownFunction,
              Parameters = (this.Parameters != null) ? this.Parameters.Clone() as DClauseList : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        



        #region public static DBFunction IsNull(string _ModuleInitializer, DBClause otherwise) + 3 overloads

        /// <summary>
        /// Creates a new IsNull function reference to be executed on the database server - ISNULL(_ModuleInitializer, otherwise)
        /// </summary>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull(string field, DClause otherwise)
        {
            return IsNull(DField.Field(field), otherwise);
        }

        /// <summary>
        /// Creates a new IsNull function reference to be executed on the database server - ISNULL(table._ModuleInitializer, otherwise)
        /// </summary>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull(Type table, string field, DClause otherwise)
        {
            return IsNull(DField.Field(table, field), otherwise);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="field"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull<TTable>( string field, DClause otherwise)
        {
            return IsNull(DField.Field<TTable>(field), otherwise);
        }


        /// <summary>
        /// Creates a new IsNull function reference to be executed on the database server - ISNULL(schema.table._ModuleInitializer, otherwise)
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="table"></param>
        /// <param name="_ModuleInitializer"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull(string owner, Type table, string field, DClause otherwise)
        {
            return IsNull(DField.Field(owner, table, field), otherwise);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull<TTable>(string owner,  string field, DClause otherwise)
        {
            return IsNull(DField.Field<TTable>(owner,  field), otherwise);
        }



        /// <summary>
        /// Creates a new IsNull function reference to be executed on the database server - ISNULL(match, otherwise)
        /// </summary>
        /// <param name="match"></param>
        /// <param name="otherwise"></param>
        /// <returns></returns>
        public static DFunction IsNull(DClause match, DClause otherwise)
        {
            DFunction fref = new DFunction();
            fref.KnownFunction = DDDD.Core.Data.DA2.Query.Function.IsNull;
            fref.Parameters.Add(match);
            fref.Parameters.Add(otherwise);

            return fref;
        }

        #endregion

        #region public static DBFunction GetDate()

        /// <summary>
        /// Creates a new GetDate() function to be executed on the database server
        /// </summary>
        /// <returns></returns>
        public static DFunction GetDate()
        {
            DFunction func = new DFunction();
            func.KnownFunction = DDDD.Core.Data.DA2.Query.Function.GetDate;

            return func;
        }

        #endregion

        #region public static DBFunction LastID()

        /// <summary>
        /// Creates a new LastID function to be executed on the database server
        /// </summary>
        /// <returns></returns>
        public static DFunction LastID()
        {
            DFunction func = new DFunction();
            func.KnownFunction = DDDD.Core.Data.DA2.Query.Function.LastID;

            return func;
        }

        #endregion


        #region  public static DBFunction Function(Function func, params DBClause[] parameters) + 3 overloads

        /// <summary>
        /// Creates a new empty DBFunction reference
        /// </summary>
        /// <returns></returns>
        public static DFunction Function()
        {
            DFunction func = new DFunction();
            return func;
        }

        /// <summary>
        /// Creates a new DBFunction reference for the known function
        /// </summary>
        /// <param name="func"></param>
        /// <returns></returns>
        public static DFunction Function(Function func)
        {
            DFunction f = DFunction.Function();
            f.KnownFunction = func;
            return f;
        }

        /// <summary>
        /// Creates a new DBFunction reference for the known function and appends the provided parameters
        /// </summary>
        /// <param name="func"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DFunction Function(Function func, params DClause[] parameters)
        {
            DFunction f = new DFunction();
            f.KnownFunction = func;
            if (null != parameters && parameters.Length > 0)
            {
                foreach (DClause clause in parameters)
                {
                    f.Parameters.Add(clause);
                }
            }
            return f;
        }



        /// <summary>
        /// Creates a new DBFunction reference for the <i>custom</i> function and appends the provided parameters.
        /// A custom function cannot be garanteed to be supported across all database engines
        /// </summary>
        /// <param name="func"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static DFunction Function(string func, params DClause[] parameters)
        {
            DFunction f = new DFunction();
            f.FunctionName = func;
            if (null != parameters && parameters.Length > 0)
            {
                foreach (DClause clause in parameters)
                {
                    f.Parameters.Add(clause);
                }
            }
            return f;
        }

        #endregion

        //
        // Interface Implementations
        //




        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (string.IsNullOrEmpty(this.FunctionName))
                return false;

            builder.BeginFunction(this.KnownFunction, this.FunctionName);
            builder.BeginFunctionParameterList();

            if (this.HasParameters)
            {
                this.Parameters.BuildStatement(builder);
            }
            builder.EndFunctionParameterList();

            builder.EndFunction(this.KnownFunction, this.FunctionName);

            if (string.IsNullOrEmpty(this.Alias) == false)
                builder.WriteAlias(this.Alias);

            return true;
        }


        #endregion



        #region IDBAlias Members

        void IDAlias.As(string aliasName)
        {
            this.Alias = aliasName;
        }

        #endregion
    }

    //[DataContract]
    //public partial class DBFunctionRef : DBFunction
    //{
    //    public DBFunctionRef() { }

    //    //
    //    // properties
    //    //

    //    //
    //    // SQL Statement builder
    //    // 



    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBFunctionRef()
    //        { 
    //            Alias = this.Alias,
    //            FunctionName = this.FunctionName,
    //            KnownFunction = this.KnownFunction,
    //            Parameters = (this.Parameters != null) ? this.Parameters.Clone() as DBClauseList : null
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


    //    //
    //    // XML serialization
    //    //

    //    //#region protected override string XmlElementName {get;}

    //    ///// <summary>
    //    ///// Gets the name of the xml element for this Function
    //    ///// </summary>
    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.Function; }
    //    //}

    //    //#endregion
        
    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        
    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.KnownFunction != Core.Data.Function.Unknown)
    //    //        this.WriteAttribute(writer, XmlHelper.KnownFunction, this.KnownFunction.ToString(), context);
    //    //    else if (string.IsNullOrEmpty(this.FunctionName) == false)
    //    //        this.WriteAttribute(writer, XmlHelper.FunctionName, this.FunctionName, context);

    //    //    if (string.IsNullOrEmpty(this.Alias) == false)
    //    //        this.WriteAlias(writer, this.Alias, context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    if (this.HasParameters)
    //    //    {
    //    //        this.WriteStartElement(XmlHelper.FunctionParameter, writer, context);
                
    //    //        foreach (DBClause c in this.Parameters)
    //    //        {
    //    //            c.WriteXml(writer, context);
    //    //        }

    //    //        this.WriteEndElement(XmlHelper.FunctionParameter, writer, context);
    //    //    }

    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b = true;
    //    //    if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
    //    //        this.Alias = reader.Value;
    //    //    else if (this.IsAttributeMatch(XmlHelper.KnownFunction, reader, context))
    //    //        this.KnownFunction = (Function)Enum.Parse(typeof(Function), reader.Value);
    //    //    else if (this.IsAttributeMatch(XmlHelper.FunctionName, reader, context))
    //    //        this.FunctionName = reader.Value;
    //    //    else
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;
    //    //    if (this.IsElementMatch(XmlHelper.FunctionParameter, reader, context) && !reader.IsEmptyElement && reader.Read())
    //    //    {
    //    //        b = this.Parameters.ReadXml(XmlHelper.FunctionParameter, reader, context);
    //    //    }
    //    //    else
    //    //        b = base.ReadAnInnerElement(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion


    //}
}
