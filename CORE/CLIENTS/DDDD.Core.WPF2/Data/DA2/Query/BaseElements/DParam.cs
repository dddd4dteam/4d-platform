 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a parameter for a database query. 
    /// Use the static (shared) methods to create a new instance
    /// </summary>
    /// <remarks>
    /// A DBParam can either be a specific value, or a delegate method evaluated at execution time. 
    /// If a name is not provided then it be dynamically set when the SQL statement is built.<br/>
    /// The delegate values are not carried across a serialized Xml query. 
    /// The parameter will be evaulated and used as a constant when deserialized.</remarks>
    [DataContract]
    public partial class DParam : DClause, IDValueSource //abstract
    {

        // .ctors
        /// <summary>
        /// protected parsmeterless ctor
        /// </summary>
        public DParam() { }

        /// <summary>
        /// Event that is raised when this parameters name is changed
        /// </summary>
        public event EventHandler NameChanged;

        
        internal const string ParameterNamePrefix = "_param";


        #region public string Name {get; set;}

        private string _name;

        /// <summary>
        /// Gets or sets the name of the parameter - raises the name changed event when set.
        /// </summary>
        [DataMember]
        public string Name
        {
            get { return _name; }
            set 
            {
                string orig = _name;
                _name = value;

                if (string.Equals(orig, this._name) == false)
                {
                    this.OnNameChanged(EventArgs.Empty);
                }
            }
        }

        #endregion

        #region internal bool HasName {get;}
        
        /// <summary>
        /// Returns true if this parameter has a non-null name
        /// </summary>
        internal bool HasName
        {
            get
            {   return (string.IsNullOrEmpty(this.Name) == false); }            
        }

        #endregion


        #region internal virtual bool HasValue {get;}
              
        /// <summary>
        /// Returns true if this parameter has an explicit value - inheritors can override
        /// </summary>        
        internal virtual bool HasValue
        {
            get { return (_val != null);  }
        }

        #endregion

        #region public virtual object Value {get;set;}

        private object _val;

        /// <summary>
        /// Gets the value of the parameter 
        /// (inheritors can override this property - but should also override the HasValue property)
        /// </summary>
        [DataMember]
        public virtual object Value
        {
            get { return _val; }
            set 
            {               
                _val = value;
            }
        }

        #endregion


        #region public System.Data.DbType DbType

        private System.Data.DbType _type;

        /// <summary>
        /// Gets or sets the DbType of this parameter
        /// </summary>
        [DataMember]
        public System.Data.DbType DbType
        {
            get
            {
                return _type;
            }
            set { _type = value; _hasType = true; }
        }

        #endregion

        #region public bool HasType {get;}// internal

        private bool _hasType = false;
        /// <summary>
        /// Returns true if this parameter has an explicit DbType
        /// </summary>           
        public bool HasType
        {
            get { return this._hasType; }            
        }

        #endregion
        
        #region public int Size {get;set;}

        private int _size = 0;
        /// <summary>
        /// Gets or sets the explicit size of this parameter
        /// </summary>
        [DataMember]
        public int Size
        {
            get { return _size; }
            set { _size = value; }
        }

        #endregion

        #region internal bool HasSize {get;}

        
        /// <summary>
        /// Returns true if this parameter has an explicit size
        /// </summary>        
        internal bool HasSize
        {
            get
            {            
                return (this._size > 0); }
        }

        #endregion

        #region public System.Data.ParameterDirection Direction {get;set;}


        private System.Data.ParameterDirection _direction = System.Data.ParameterDirection.Input;

        /// <summary>
        /// Gets or sets the direction of this parameter. 
        /// To access Output parameters they must be explicitly declared in your code
        /// </summary>
        [DataMember]
        public System.Data.ParameterDirection Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

        #endregion


        //
        // methods
        //
        #region protected virtual void OnNameChanged(EventArgs args)

        /// <summary>
        /// Raises the NameChanged event
        /// </summary>
        /// <param name="args"></param>
        protected virtual void OnNameChanged(EventArgs args)
        {
            if (null != this.NameChanged)
                this.NameChanged(this, args);
        }

        #endregion

        #region protected virtual object GetDbValue(object value)

        /// <summary>
        /// Gets the Database appropriate value - base implementation returns DBNull for null (Nothing).
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected virtual object GetDbValue(object value)
        {
            if (null == value)
                return DBNull.Value;
            else
                return value;
        }

        #endregion





        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DParam()
            {
                DbType = this.DbType,
                Direction = this.Direction,
                Name = this.Name,
                Size = this.Size,
                Value = this.Value
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------



        //
        // Xml Serialization
        //        
        //#region protected override string XmlElementName {get;}

        ///// <summary>
        ///// Returns the name for this XmlElement
        ///// </summary>
        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Parameter; }
        //}

        //#endregion

        //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        ///// <summary>
        ///// Writes all the Xml Attributes for this parameter
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (this.HasName)
        //        this.WriteAttribute(writer, XmlHelper.Name, this.Name, context);

        //    if (context.Parameters.Contains(this) == false)
        //        context.Parameters.Add(this);

        //    return true;

        //}

        //#endregion

        //#region public bool WriteFullParameterXml(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Writes all the values for this DBParam instance
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteFullParameterXml(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    writer.WriteStartElement(this.XmlElementName);

        //    if (this.HasName)
        //        this.WriteAttribute(writer, XmlHelper.Name, this.Name, context);
        //    if (this.HasType)
        //        this.WriteAttribute(writer, XmlHelper.DbType, this.DbType.ToString(), context);
        //    if (this.HasSize)
        //        this.WriteAttribute(writer, XmlHelper.ParameterSize, this.Size.ToString(), context);

        //    this.WriteAttribute(writer, XmlHelper.ParameterDirection, this.Direction.ToString(), context);

        //    base.WriteAllAttributes(writer, context);

        //    XmlHelper.WriteNativeValue(this.Value, writer, context);

        //    writer.WriteEndElement();

        //    return true;
        //}

        //#endregion

        //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

        ///// <summary>
        ///// Overrides the base implementation an returns true
        ///// </summary>
        ///// <param name="writer"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    return true;
        //}

        //#endregion

        //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Reads the attributes of this parameter and returns true if it was a known attribute. 
        ///// Otherwise returns the base method output
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool b = true;
        //    if (this.IsAttributeMatch(XmlHelper.ParameterDirection, reader, context) && !string.IsNullOrEmpty(reader.Value))
        //    {
        //        this.Direction = (System.Data.ParameterDirection)Enum.Parse(typeof(System.Data.ParameterDirection), reader.Value, true);
        //    }
        //    else if (this.IsAttributeMatch(XmlHelper.ParameterSize, reader, context) && !string.IsNullOrEmpty(reader.Value))
        //    {
        //        this.Size = int.Parse(reader.Value);
        //    }
        //    else if (this.IsAttributeMatch(XmlHelper.Name, reader, context))
        //    {
        //        this.Name = reader.Value;
        //    }
        //    else if (this.IsAttributeMatch(XmlHelper.DbType, reader, context) && !string.IsNullOrEmpty(reader.Value))
        //    {
        //        this.DbType = (System.Data.DbType)Enum.Parse(typeof(System.Data.DbType), reader.Value);
        //    }
        //    else
        //        b = base.ReadAnAttribute(reader, context);

        //    return b;
        //}

        //#endregion

        //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

        ///// <summary>
        ///// Reads any known iner elements
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    if (reader.NodeType == System.Xml.XmlNodeType.Text && reader.Value == XmlHelper.NullString)
        //        this.Value = DBNull.Value;
        //    else
        //    {
        //        if (reader.IsEmptyElement == false)
        //        {
        //            string end = reader.LocalName;

        //            do
        //            {
        //                if (reader.NodeType == System.Xml.XmlNodeType.EndElement && this.IsElementMatch(end, reader, context))
        //                    break;
        //                else if (reader.NodeType == System.Xml.XmlNodeType.Element && this.IsElementMatch(XmlHelper.ParameterValue, reader, context))
        //                {
        //                    this.Value = XmlHelper.ReadNativeValue(reader, context);
        //                }
        //            }
        //            while (reader.Read());
        //        }
        //    }
        //    return base.ReadAnInnerElement(reader, context);
        //}

        //#endregion



        //
        // static factory methods
        //

        #region public static DParam Param(string name) + 3 overloads

        /// <summary>
        /// Creates a new DBParam clause with the specified generic name. Cannot support delegate methods as values
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <returns></returns>
        public static DParam Param(string genericName)
        {
            DParam pref = new DParam();
            pref.Name = genericName;

            return pref;
        }

        /// <summary>
        /// Creates a new DParam clause with the specified name and DBType. Cannot support delegate methods as values
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The DbType of the parameter</param>
        /// <returns></returns>
        public static DParam Param(string genericName, System.Data.DbType type)
        {
            DParam pref = new DParam();
            pref.Name = genericName;
            pref.DbType = type;

            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the specified name, DBtype and size. Cannot support delegate methods as values
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The type of the parameter</param>
        /// <param name="size">The (maximum) size of the parameter</param>
        /// <returns></returns>
        public static DParam Param(string genericName, System.Data.DbType type, int size)
        {
            DParam pref = new DParam();
            pref.Name = genericName;
            pref.DbType = type;
            pref.Size = size;
            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the specified name, DBType and direction. Cannot support delegate methods as values
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The DbType of the parameter</param>
        /// <param name="direction">The parameters direction</param>
        /// <returns></returns>
        public static DParam Param(string genericName, System.Data.DbType type, System.Data.ParameterDirection direction)
        {
            DParam pref = new DParam();
            pref.Name = genericName;
            pref.DbType = type;
            pref.Direction = direction;
            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the specified name, DBType, size and direction. Cannot support delegate methods as values
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The DbType of the parameter</param>
        /// <param name="size">The (maximum) size of the parameter value</param>
        /// <param name="direction">The parameters direction</param>
        /// <returns></returns>
        public static DParam Param(string genericName, System.Data.DbType type, int size, System.Data.ParameterDirection direction)
        {
            DParam pref = new DParam();
            pref.Name = genericName;
            pref.DbType = type;
            pref.Size = size;
            pref.Direction = direction;
            return pref;
        }
               
        #endregion

        #region public static DParam ParamWithValue(object paramValue) + 3 overloads

        /// <summary>
        /// Creates a new DParam with an constant value. 
        /// NOTE: The parameter type cannot be determined if null or DBUnll is passed as the value
        /// </summary>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public static DParam ParamWithValue(object paramValue)
        {
            DParam pref = new DParam();
            pref.Value = paramValue;
            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the DbType and constant value.
        /// </summary>
        /// <param name="type">The DbType of the parameter</param>
        /// <param name="paramValue">The value of the parameter</param>
        /// <returns></returns>
        public static DParam ParamWithValue(System.Data.DbType type, object paramValue)
        {
            DParam pref = new DParam();
            pref.Value = paramValue;
            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the name and constant value.
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="value">The value of the parameter</param>
        /// <returns></returns>
        public static DParam ParamWithValue(string genericName, object value)
        {
            DParam pref = new DParam();
            pref.Value = value;
            pref.Name = genericName;

            return pref;
        }

        /// <summary>
        /// Creates a new DParam with the specified name, type, and constant value
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The type of the parameter</param>
        /// <param name="value">The value of the parameter</param>
        /// <returns></returns>
        public static DParam ParamWithValue(string genericName, System.Data.DbType type, object value)
        {
            DParam pref = new DParam();
            pref.Name = genericName;
            pref.DbType = type;
            pref.Value = value;
            return pref;
        }


        #endregion

        #region public static DParam ParamWithDelegate(ParamValue valueprovider) + 3 overloads

        /// <summary>
        /// Creates a new DBParam with a delegate method as the value provider (no parameters, returns object).
        /// NOTE: The parameter type cannot be determined if null or DBUnll is passed as the value
        /// </summary>
        /// <param name="valueprovider">The delegate method (cannot be null)</param>
        /// <returns></returns>
        /// <remarks>
        ///  The method will be executed when any queries using this parameter are converted to their SQL statements
        /// </remarks>
        public static DParam ParamWithDelegate(ParamValue valueprovider)
        {
            if (null == valueprovider)
                throw new ArgumentNullException("valueProvider");

            DDelegateParam del = new DDelegateParam(valueprovider);
            
            return del;
        }

        /// <summary>
        /// Creates a new DParam with a delegate method as the value provider (no parameters, returns object). 
        /// </summary>
        /// <param name="type">The DbType of the parameter </param>
        /// <param name="valueProvider">The delegate method (cannot be null)</param>
        /// <returns></returns>
        /// <remarks>
        ///  The valueProvider method will be executed when any queries using this parameter are converted to their SQL statements
        /// </remarks>
        public static DParam ParamWithDelegate(System.Data.DbType type, ParamValue valueProvider)
        {
            if (null == valueProvider)
                throw new ArgumentNullException("valueProvider");

            DDelegateParam del = new DDelegateParam(valueProvider);
            del.DbType = type;
            return del;
        }

        /// <summary>
        /// Creates a new DParam with the specified name and a delegate method as the value provider (no parameters, returns object).
        /// NOTE: The parameter type cannot be determined if null or DBUnll is passed as the value
        /// </summary>
        /// <param name="genericName">The name of the parameter</param>
        /// <param name="valueprovider">The delegate method (cannot be null)</param>
        /// <returns></returns>
        /// <remarks>
        ///  The method will be executed when any queries using this parameter are converted to their SQL statements
        /// </remarks>
        public static DParam ParamWithDelegate(string genericName, ParamValue valueprovider)
        {
            if (null == valueprovider)
                throw new ArgumentNullException("valueProvider");

            DDelegateParam del = new DDelegateParam(valueprovider);
            del.Name = genericName;
            return del;
        }


        /// <summary>
        /// Creates a new DParam with the specified name, type and a delegate method as the value provider (no parameters, returns object).
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The DbType of the parameter</param>
        /// <param name="valueprovider">The delegate method (cannot be null)</param>
        /// <returns></returns>
        /// <remarks>
        ///  The method will be executed when any queries using this parameter are converted to their SQL statements
        /// </remarks>
        public static DParam ParamWithDelegate(string genericName, System.Data.DbType type, ParamValue valueprovider)
        {
            if (null == valueprovider)
                throw new ArgumentNullException("valueProvider");

            DDelegateParam del = new DDelegateParam(valueprovider);
            del.DbType = type;
            del.Name = genericName;
            return del;
        }

        /// <summary>
        /// Creates a new DParam with the specified name, type, size and a delegate method as the value provider (no parameters, returns object).
        /// </summary>
        /// <param name="genericName">The generic name of the parameter (e.g. param1 rather than @param1)</param>
        /// <param name="type">The DbType of the parameter</param>
        /// <param name="size">The maximum size of the parameter value</param>
        /// <param name="valueprovider">The delegate method (cannot be null)</param>
        /// <returns></returns>
        /// <remarks>
        ///  The method will be executed when any queries using this parameter are converted to their SQL statements
        /// </remarks>
        public static DParam ParamWithDelegate(string genericName, System.Data.DbType type, int size, ParamValue valueprovider)
        {
            if (null == valueprovider)
                throw new ArgumentNullException("valueProvider");

            DDelegateParam del = new DDelegateParam(valueprovider);
            del.DbType = type;
            del.Name = genericName;
            return del;
        }

        #endregion

        #region internal static DBClause Param()

        /// <summary>
        /// Internal method to create an empty parameter
        /// </summary>
        /// <returns></returns>
        internal static DParam Param()
        {
            return new DParam();
        }

        #endregion
        


        /// <summary>
        /// Overrides the default behaviour to write the parameter
        /// </summary>
        /// <param name="builder">The SQL statement builder</param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            this.Name = builder.RegisterParameter(this);

            builder.WriteParameterReference(this.Name);

            return true;
        }

    }

    //
    // subclasses
    //
    #region internal class DBParamRef : DBParam, IDBValueSource

    /// <summary>
    /// Implementation of a value parameter
    /// </summary>
    //[DataContract]
    //public partial class DBParamRef : DBParam
    //{
    //    public DBParamRef() { }


    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBParamRef()
    //        {
    //            DbType = this.DbType,
    //            Direction = this.Direction,
    //            Name = this.Name,
    //            Size = this.Size,
    //            Value = (this.Value != null && this.Value is ICloneable) ? (this.Value as ICloneable).Clone() : null                
    //        };
    //    }
    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
    //}
    

    #endregion


    #region internal class DBDelegateParam : DBParam, IDBValueSource


    /// <summary>
    /// Implementation of a DBParam parameter that gets its value from a delegate method
    /// </summary>   
    public class DDelegateParam : DParam, IDValueSource
    {
        public DDelegateParam() { }

        private ParamValue _val;

        /// <summary>
        /// Gets or sets the delegate
        /// </summary>        
        public ParamValue ValueDelegate
        {
            get { return this._val; }
            set { this._val = value; }
        }



        //bool _IsHasValue = false;
        /// <summary>
        /// Overrides to return true if there is a delegate set
        /// </summary>        
        internal override bool HasValue
        {
            get
            {               
                return (this._val != null);
            }            
        }

        /// <summary>
        /// Overrides to return the value from the executed delegate
        /// </summary>
        public override object Value
        {
            get 
            { 
                return this.ValueDelegate();
            }
            set { throw new InvalidOperationException("Cannot set the value on a delegated parameter"); }
        }

        /// <summary>
        /// Creates a new DBDelegateParam
        /// </summary>
        /// <param name="value"></param>
        public DDelegateParam(ParamValue value)
            : base()
        {
            this._val = value;
        }



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DDelegateParam()
            {
                DbType = this.DbType,
                Direction = this.Direction,
                Name = this.Name,
                Size = this.Size,
                Value = ( this.Value != null && this.Value is ICloneable) ?  (this.Value as ICloneable).Clone() : null,
#if SERVER                
                ValueDelegate  = this.ValueDelegate.Clone() as ParamValue
#endif
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        /// <summary>
        /// Overrides the default behaviour to register the parameter
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            this.Name = builder.RegisterParameter(this);

            builder.WriteParameterReference(this.Name);

            return true;

        }
                     

    }

    #endregion

    //
    // collection classes
    //

    #region public class DBParamList : System.Collections.ObjectModel.KeyedCollection<string, DBParam>

    /// <summary>
    /// Defines a collection of DBParam's that can be accessed by index or generic Name
    /// </summary>
    [CollectionDataContract]
    public partial class DBParamList : KeyedCollection<string, DParam> , ICloneable
    {
        public DBParamList() { }

        /// <summary>
        /// Overriden to return the name of the parameter
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        protected override string GetKeyForItem(DParam item)
        {
            return item.Name;
        }

        /// <summary>
        /// overriden to attach to the NameChanged event so the key for this item can be updated
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void InsertItem(int index, DParam item)
        {
            if(null != item)
                item.NameChanged += new EventHandler(item_NameChanged);
            base.InsertItem(index, item);
        }

        /// <summary>
        /// Overriden to detach from the NameChanged event handler
        /// </summary>
        /// <param name="index"></param>
        protected override void RemoveItem(int index)
        {
            DParam item = this[index];
            if (null != item)
                item.NameChanged -= new EventHandler(this.item_NameChanged);
            base.RemoveItem(index);
        }

        /// <summary>
        /// Overriden to detach all contained parameters from their NameChanged event
        /// </summary>
        protected override void ClearItems()
        {
            foreach (DParam item in this)
            {
                item.NameChanged -= new EventHandler(this.item_NameChanged);
            }
            base.ClearItems();
        }
        
        /// <summary>
        /// Overriden to detach the current and attach the new name changed event handlers
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void SetItem(int index, DParam item)
        {
            DParam orig = this[index];
            if (null != orig)
                orig.NameChanged -= new EventHandler(this.item_NameChanged);
            if(null != item)
                item.NameChanged += new EventHandler(this.item_NameChanged);

            base.SetItem(index, item);
        }

        /// <summary>
        /// Handles an items name changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void item_NameChanged(object sender, EventArgs e)
        {
            DParam param = (DParam)sender;
            this.ChangeItemKey(param, param.Name);
        }

        /// <summary>
        /// Attempts to retrieve the parameter with the specified name, 
        /// and returns true if one was found, otherwise false
        /// </summary>
        /// <param name="name"></param>
        /// <param name="aparam"></param>
        /// <returns></returns>
        internal bool TryGetParameter(string name, out DParam aparam)
        {
            if (this.Count == 0)
            {
                aparam = null;
                return false;
            }
            else
                return this.Dictionary.TryGetValue(name, out aparam);
        }



        public object Clone()
        {
            DBParamList newParamList = new DBParamList();
            
            foreach (var item in this)
            { newParamList.Add(item.CloneClause() as DParam );
            }

            return newParamList;
        }

    }

    #endregion
}
