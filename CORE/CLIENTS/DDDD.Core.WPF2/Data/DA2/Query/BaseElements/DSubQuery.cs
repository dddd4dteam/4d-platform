 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a query included within an outer query. 
    /// For example an inner select on a SELECT statement, or a SELECT for an insert
    /// </summary>
    [DataContract]
    public partial class DSubQuery : DJoinable, IDAlias, IDBoolean  //abstract //IDBJoinable,
    {

        public DSubQuery() { }

        #region public string Alias

        private string _name;

        /// <summary>
        /// Gets or sets the alias name of this subquery
        /// </summary>
        [DataMember]
        public string Alias
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        #region public DBQuery InnerQuery

        private DQuery _innerq;

        /// <summary>
        /// Gets or sets the inner query in this sub query
        /// </summary>
        [DataMember]
        public DQuery InnerQuery
        {
            get { return _innerq; }
            set
            {
                _innerq = value;
                if (null != _innerq)
                    _innerq.IsInnerQuery = true;
            }
        }

        #endregion

        #region public DBJoinList Joins {get;}

        private DJoinList _joins;
        /// <summary>
        /// Gets or sets the list of joins between the outer query and this inner query (if any). 
        /// </summary>
        [DataMember]
        public DJoinList Joins
        {
            get
            {
                if (this._joins == null)
                    this._joins = new DJoinList();
                return _joins;
            }
            set { _joins = value; }
        }

        #endregion

        #region internal bool HasJoins
                
        /// <summary>
        /// Returns true if there are joins defined between the inner and outer queries
        /// </summary>                
        internal bool HasJoins
        {
            get { return (this._joins != null && this._joins.Count > 0);  }
        }

        #endregion

        #region IDBAlias Members

        /// <summary>
        /// Applies an alias name to this inner query
        /// </summary>
        /// <param name="aliasName"></param>
        public void As(string aliasName)
        {
            this._name = aliasName;
        }

        #endregion
        

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DSubQuery()
            {   InnerQuery = (this.InnerQuery != null) ? this.InnerQuery.CloneClause() as DQuery : null,
                Joins = (this.Joins != null) ? this.Joins.Clone() as DJoinList : null,
                Alias = this.Alias 
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        

        #region public DBJoin InnerJoin(DBClause table) + 4 overloads

        /// <summary>
        /// Adds an INNER JOIN from the results of this sub query to a second table 
        /// where this tables parent _ModuleInitializer matched the child _ModuleInitializer
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentfield"></param>
        /// <param name="childfield"></param>
        /// <returns></returns>
        public DJoin InnerJoin(Type table, string parentfield, string childfield)
        {
            DTable tbl = DTable.Table(table);
            DField parent = DField.AliasField(this.Alias, parentfield);
            DField child = DField.Field(table, parentfield);

            return InnerJoin(tbl, parent, child);
        }

        /// <summary>
        /// Adds an INNER JOIN from the results of this sub query to a second table 
        /// where this tables parent _ModuleInitializer matched the child _ModuleInitializer
        /// </summary>
        /// <param name="table"></param>
        /// <param name="parentField"></param>
        /// <param name="childField"></param>
        /// <returns></returns>
        public DJoin InnerJoin(DTable table, DClause parentField, DClause childField)
        {
            DJoin join = DJoin.InnerJoin(table, parentField, childField, Compare.Equals);
            this.Joins.Add(join);
            return join;
        }

        /// <summary>
        /// Adds an INNER JOIN from the results of this sub query to a second foreign clause 
        /// with the comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin InnerJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.InnerJoin(foreign, compare);
            this.Joins.Add(join);
            return join;
        }

        #endregion

        //
        // other joins
        //

        /// <summary>
        /// Adds a LEFT OUTER JOIN from the results of this sub query to a second foreign clause 
        /// with the comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin LeftJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.Join(foreign, JoinType.LeftOuter, compare);
            this.Joins.Add(join);
            return join;
        }

        /// <summary>
        /// Adds a RIGHT OUTER JOIN from the results of this sub query to a second foreign clause 
        /// with the comparison
        /// </summary>
        /// <param name="foreign"></param>
        /// <param name="compare"></param>
        /// <returns></returns>
        public DJoin RightJoin(DClause foreign, DComparison compare)
        {
            DJoin join = DJoin.Join(foreign, JoinType.RightOuter, compare);
            this.Joins.Add(join);
            return join;
        }

        /// <summary>
        /// Adds a JOIN from the results of this sub query to a second foreign clause 
        /// with the comparison
        /// </summary>
        /// <param name="table"></param>
        /// <param name="type"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public DJoin Join(DClause table, JoinType type, DComparison comp)
        {
            DJoin join = DJoin.Join(table, type, comp);
            this.Joins.Add(join);

            return join;
        }

        /// <summary>
        /// Adds an INNER JOIN from the results of this sub query to a second foreign clause. 
        /// Use the ON method to add restrictions
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public DJoin InnerJoin(DClause table)
        {
            DJoin join = DJoin.Join(table, JoinType.InnerJoin);
            this.Joins.Add(join);

            return join;
        }
        
        #region IDBJoinable Members

        /// <summary>
        /// Appends restrictions to the last joined clauses and this sub query
        /// </summary>
        /// <param name="compare"></param>
        /// <returns></returns>
        public override DClause On(DComparison compare)
        {
            DJoinable join = (DJoinable)this.Joins[this.Joins.Count - 1];
            join.On(compare);
            return (DClause)join;
        }

        #endregion

        #region IDBBoolean Members

        /// <summary>
        /// Appends an AND restriction to this Sub selects last JOIN statements
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause And(DClause reference)
        {
            IDBoolean join = (IDBoolean)this.Joins[this.Joins.Count - 1];
            join.And(reference);
            return (DClause)join;
        }

        /// <summary>
        /// Appends an OR restriction to this sub selectes last JOIN statements
        /// </summary>
        /// <param name="reference"></param>
        /// <returns></returns>
        public DClause Or(DClause reference)
        {
            IDBoolean join = (IDBoolean)this.Joins[this.Joins.Count - 1];
            join.Or(reference);
            return (DClause)join;
        }

        #endregion

        //
        // static factory methods
        //

        #region internal static DBSubQuery SubSelect(DBQuery inner)

        /// <summary>
        /// Defines a new SubSelect with an inner query
        /// </summary>
        /// <param name="inner"></param>
        /// <returns></returns>
        internal static DSubQuery SubSelect(DQuery inner)
        {
            DSubQuery sub = new DSubQuery();
            sub.InnerQuery = inner;
            inner.IsInnerQuery = true;
            return sub;
        }

        #endregion

        #region internal static DBSubQuery SubSelect()

        /// <summary>
        /// Defines a new empty sub select
        /// </summary>
        /// <returns></returns>
        internal static DSubQuery SubSelect()
        {
            DSubQuery subref = new DSubQuery();
            return subref;
        }

        #endregion




        #region public override bool BuildStatement(DBStatementBuilder builder)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public override bool BuildStatement(DStatementBuilder builder)
        {
            InnerQuery.BuildStatement(builder);

            if (string.IsNullOrEmpty(this.Alias) == false)
            {
                builder.BeginAlias();
                builder.BeginIdentifier();
                builder.WriteRaw(this.Alias);
                builder.EndIdentifier();
                builder.EndAlias();
            }

            if (this.HasJoins)
                this.Joins.BuildStatement(builder);

            return true;
        }


        #endregion


    }

   //[DataContract]
   // public partial class DBSubQueryRef : DBSubQuery
   // {
   //    public DBSubQueryRef() { }


   //     //
   //     // SQL Statement
   //     //

   //     ////
   //     //// XML Serialization
   //     ////
   //     //#region protected override string XmlElementName {get;}

   //     //protected override string XmlElementName
   //     //{
   //     //    get { return XmlHelper.InnerSelect; }
   //     //}

   //     //#endregion

   //     //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
        
   //     //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
   //     //{
   //     //    if (string.IsNullOrEmpty(this.Alias) == false)
   //     //        this.WriteAlias(writer, this.Alias, context);

   //     //    return base.WriteAllAttributes(writer, context);
   //     //}

   //     //#endregion

   //     //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)

   //     //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
   //     //{
   //     //    if (this.InnerQuery != null)
   //     //        this.InnerQuery.WriteXml(writer, context);

   //     //    return base.WriteInnerElements(writer, context);
   //     //}

   //     //#endregion

   //     //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
   //     //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
   //     //{
   //     //    bool b = true;

   //     //    if (this.IsAttributeMatch(XmlHelper.Alias, reader, context))
   //     //        this.Alias = reader.Value;
   //     //    else
   //     //        b = base.ReadAnAttribute(reader, context);
   //     //    return b;
   //     //}

   //     //#endregion

   //     //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
   //     //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
   //     //{
   //     //    DBClause c = context.Factory.Read(reader.Name, reader, context);

   //     //    if (c is DBQuery)
   //     //    {
   //     //        this.InnerQuery = (DBQuery)c;
   //     //        return true;
   //     //    }
   //     //    else
   //     //    {
   //     //        throw new InvalidCastException(Errors.CanOnlyUseQueriesInInnerSelects);
   //     //    }
   //     //}

   //     //#endregion


   // }
}
