
using System;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Creates a new Assignment (X = Y) clause. 
    /// Used specifically in UPDATE operations (SET field1 = param1, field2 = param2 etc.)
    /// </summary>
    [DataContract]
    public partial class DAssign : DClause //abstract
    {
        //
        // .ctor
        //
        #region protected DBAssign()

        /// <summary>
        /// To support inherited constructors
        /// </summary>
        public DAssign() //protected
        { }

        #endregion



        #region public DBClause Item {get;set;}

        private DClause _item;
        /// <summary>
        /// Gets or sets the receiver of the value
        /// </summary>
        [DataMember]
        public DClause Item
        {
            get { return _item; }
            set { this._item = value; }
        }

        #endregion

        #region public DBClause ToValue {get; set;}

        private DClause _toval;
        /// <summary>
        /// Gets or sets the value to be assigned
        /// </summary>
        [DataMember]
        public DClause ToValue
        {
            get { return _toval; }
            set { this._toval = value; }
        }

        #endregion


        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DAssign()
            {
                ToValue = (this.ToValue != null) ? this.ToValue.CloneClause() : null,
                Item = (this.Item != null) ? this.Item.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------
               



        
        //
        // static factory methods
        //
        
        #region public static DBAssign Set(DBClause item, DBClause toValue)

        /// <summary>
        /// Creates a new Assignment clause
        /// </summary>
        /// <param name="item"></param>
        /// <param name="toValue"></param>
        /// <returns></returns>
        public static DAssign Set(DClause item, DClause toValue)
        {
            DAssign aref = new DAssign();
            aref._item = item;
            aref._toval = toValue;

            return aref;
        }

        #endregion

        
        #region internal static DBAssign Assign()

            /// <summary>
            /// Creates a new Empty assignment
            /// </summary>
            /// <returns></returns>
            internal static DAssign Assign()
            {
                DAssign aref = new DAssign();
                return aref;
            }

        #endregion




        #region public override bool BuildStatement(DBStatementBuilder builder)

        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginAssignValue();
            this.Item.BuildStatement(builder);
            builder.WriteOperator(Operator.Equals);
            this.ToValue.BuildStatement(builder);
            builder.EndAssignValue();

            return true;
        }

        #endregion



    }




//    [DataContract]
//    public partial class DBAssignRef : DBAssign
//    {
//        //
//        // .ctor(s)
//        //
//        #region public DBAssignRef()

//        public DBAssignRef() {}

//        #endregion



//        #region  ------------------ IClonableClause.CloneClause -----------------------

//        /// <summary>
//        /// Cloning Clause
//        /// </summary>
//        /// <returns></returns>
//        public override DBClause CloneClause()
//        {
//            return new DBAssignRef()
//            { 
//                ToValue = (this.ToValue != null) ? this.ToValue.CloneClause() : null,
//                Item = (this.Item != null) ? this.Item.CloneClause() : null
//            };
//        }

//        #endregion  ------------------ IClonableClause.CloneClause -----------------------
        
       

//        ////
//        // SQL Statement 
//        //

        
//        //#region protected override string XmlElementName {get;}

//        //protected override string XmlElementName
//        //{
//        //    get { return XmlHelper.Assign; }
//        //}

//        //#endregion
//        //        #region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
////        protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
////        {
////            if (this.Item != null)
////            {
////                this.WriteStartElement(XmlHelper.Item, writer, context);
////                this.Item.WriteXml(writer, context);
////                this.WriteEndElement(XmlHelper.Item, writer, context);
////            }

////            if (this.ToValue != null)
////            {
////                this.WriteStartElement(XmlHelper.Value, writer, context);
////                this.ToValue.WriteXml(writer,context);
////                this.WriteEndElement(XmlHelper.Value, writer, context);
////            }

////            return base.WriteInnerElements(writer, context);
////        }

////        #endregion

////        #region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)

////        protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
////        {
////            bool b;
////            if (this.IsElementMatch(XmlHelper.Item, reader, context) && !reader.IsEmptyElement && reader.Read())
////            {
////                this.Item = this.ReadNextInnerClause(XmlHelper.Item, reader, context);
////                b = true;
////            }
////            else if (this.IsElementMatch(XmlHelper.Value, reader, context) && !reader.IsEmptyElement && reader.Read())
////            {
////                this.ToValue = this.ReadNextInnerClause(XmlHelper.Value, reader, context);
////                b = true;
////            }
////            else
////                b = base.ReadAnInnerElement(reader, context);

////            return b;
////        }

////        #endregion


//    }
}
