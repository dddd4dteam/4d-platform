﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a Return ... statement
    /// </summary>
    [DataContract]
    public partial class DReturn : DStatement //abstract
    {
        public DReturn() { }
        

        private DClause _toreturn;
        /// <summary>
        /// Get the clause that will be returned by the database engine when this statement is executed. 
        /// Inheritors can set the value
        /// </summary>
        [DataMember]
        public DClause ToReturn
        {
            get { return _toreturn; }
            set { _toreturn = value; } //protected
        }

        /// <summary>
        /// Creates and returns a new RETURN empty statement
        /// </summary>
        /// <returns></returns>
        public static DReturn Return()
        {
            return new DReturnRef();
        }

        /// <summary>
        /// Creates and returns a new RETURN xxx statement
        /// </summary>
        /// <param name="toreturn">The clause that will be executed by the database and its resultant value returned</param>
        /// <returns></returns>
        public static DReturn Return(DClause toreturn)
        {
            DReturnRef ret = new DReturnRef();
            ret.ToReturn = toreturn;
            return ret;
        }
    }

    [DataContract]
    public partial class DReturnRef : DReturn
    {
        public DReturnRef() { }

       



        public override bool BuildStatement(DStatementBuilder builder)
        {
            builder.BeginReturnsStatement();
            if (null != this.ToReturn)
                this.ToReturn.BuildStatement(builder);
            builder.EndReturnsStatement();
            return true;
        }


        //protected override string XmlElementName
        //{
        //    get { return XmlHelper.Returns; }
        //}

        //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        //{
        //    if (null != this.ToReturn)
        //    {
        //        this.WriteStartElement("to-return", writer, context);
        //        this.ToReturn.WriteXml(writer, context);
        //        this.WriteEndElement("to-return", writer, context);
        //    }
        //    return base.WriteInnerElements(writer, context);
        //}

        //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    if (this.IsElementMatch("to-return", reader, context))
        //    {
        //        DBClause ret = this.ReadNextInnerClause("to-return", reader, context);
        //        this.ToReturn = ret;
        //        return true;
        //    }
        //    else
        //        return base.ReadAnInnerElement(reader, context);
        //}
    }
}
