
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;
using DDDD.Core.Data.DA2;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// DBQuery.Select, Update, Delete, Insert, Exec and Begin all return specific DBQuery 
    /// instances that enable the building of SQL Statements to be executed on a Relational Database.
    /// </summary>
    [DataContract]
    public partial class DQuery : DStatement //abstract
    {


        /// <summary>
        /// We can prebuild query on clinet side,  so we save time for server.
        /// </summary>
        public static bool UseQueryPrebuildOnClient
        { get; set; } = false;

        /// <summary>
        /// Prebuilded query on clinet side,  so we save time for server.
        /// </summary>
        public string PrebuildQuery
        { get; protected set; }



        public DQuery() { }

        #region  public bool IsInnerQuery {get;set;} //internal

        private bool _innerq = false;

        [DataMember]
        public bool IsInnerQuery //internal
        {
            get { return this._innerq; }
            set { this._innerq = value; }
        }

        #endregion

        [DataMember]
        public Guid? ChangeTrackingContext { get; set; }

        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DQuery()
            {
                IsInnerQuery = this.IsInnerQuery
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------




        //
        // DBSelectQuery factory methods
        //
        #region public static DBQuery SelectCount() + 3 overloads

        /// <summary>
        /// Begins a new SELECT statement with a COUNT(*) as the first result
        /// </summary>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectCount()
        {
            DSelectSet sel = DSelectSet.SelectCount();
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            q.Last = sel;
            return q;
        }

        /// <summary>
        /// Begins a new SELECT statement with a COUNT([_ModuleInitializer]) as the first result
        /// </summary>
        /// <param name="_ModuleInitializer">The name of the _ModuleInitializer to count</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectCount(string field)
        {
            DSelectSet sel = DSelectSet.SelectCount(field);
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            return q;
        }


        /// <summary>
        /// Begins a new SELECT statement with a COUNT([table].[_ModuleInitializer]) as the first result
        /// </summary>
        /// <param name="_ModuleInitializer">The name of the _ModuleInitializer to count</param>
        /// <param name="table">The name of the table (or alias) containing the _ModuleInitializer to count</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectCount(Type table, string field)
        {
            DSelectSet sel = DSelectSet.SelectCount(table, field);
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            return q;
        }


        public static DSelectQuery SelectCount<TTable>(string field)
        {
            DSelectSet sel = DSelectSet.SelectCount<TTable>(field);
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            return q;
        }


        /// <summary>
        /// Begins a new SELECT statement with a COUNT([owner].[table].[_ModuleInitializer]) as the first result
        /// </summary>
        /// <param name="_ModuleInitializer">The name of the _ModuleInitializer to count</param>
        /// <param name="table">The name of the table (or alias) containing the _ModuleInitializer to count</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectCount(string owner, Type table, string field)
        {
            DSelectSet sel = DSelectSet.SelectCount(owner, table, field);
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            return q;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static DSelectQuery SelectCount<TTable>(string owner, string field)
        {
            DSelectSet sel = DSelectSet.SelectCount<TTable>(owner, field);
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = sel;
            return q;
        }


        #endregion

        #region public static DBQuery SelectFields(params string[] fields)

        /// <summary>
        /// Begins a new SELECT statement with a set of fields as the result
        /// </summary>
        /// <param name="fields">The name of the fields to select</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectFields(params string[] fields)
        {
            DSelectSet sel = DSelectSet.SelectFields(fields);
            DSelectQuery query = new DSelectQuery();
            query.SelectSet = sel;
            query.Last = sel;

            return query;
        }

        #endregion

        #region public static DBSelectQuery Select()
        /// <summary>
        /// Begins a new SELECT statement. Add fields and tables by using the methods on the instance returned
        /// </summary>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery Select()
        {
            DSelectQuery q = new DSelectQuery();
            return q;
        }

        #endregion

        #region public static DBSelectQuery SelectTopN(int count)
        /// <summary>
        /// Begins a new SELECT statement with a limit of [count] items returned
        /// </summary>
        /// <param name="count">The total number of items to return</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectTopN(int count)
        {
            DSelectQuery q = DQuery.Select();
            return q.TopN(count);
        }

        #endregion

        #region public static DBSelectQuery SelectDistinct()

        /// <summary>
        /// Begins a new SELECT DISTINCT statement. Add fields and tables by using the methods on the instance returned
        /// </summary>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectDistinct()
        {
            DSelectQuery q = DQuery.Select();
            return q.Distinct();
        }

        #endregion

        #region public static DBSelectQuery Select(DBClause _ModuleInitializer)
        /// <summary>
        /// Begins a new SELECT statement with the clause as the first value returned.
        /// </summary>
        /// <param name="clause">Any valid SQL clause (DBConst, DBParam, etc.) </param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery Select(DClause clause)
        {
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = DSelectSet.Select(clause);
            q.Last = q.SelectSet;
            return q;
        }

        #endregion

        #region public static DBSelectQuery SelectAll()
        /// <summary>
        /// Begins a new SELECT * statement.
        /// </summary>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectAll()
        {
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = DSelectSet.SelectAll();
            q.Last = q.SelectSet;
            return q;
        }

        #endregion

        #region public static DBSelectQuery SelectAll(string table)
        /// <summary>
        /// Begins a new SELECT [table].* statment
        /// </summary>
        /// <param name="table">The name (or alias) of the table</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectAll(string table)
        {
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = DSelectSet.SelectAll(table);
            q.Last = q.SelectSet;
            return q;
        }

        #endregion

        #region public static DBSelectQuery SelectAll(string owner, string table)

        /// <summary>
        /// Begins a new SELECT [owner].[table].* statment
        /// </summary>
        /// <param name="table">The name (or alias) of the table</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBSelectQuery to support statement chaining</returns>
        public static DSelectQuery SelectAll(string owner, Type table)
        {
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = DSelectSet.SelectAll(owner, table);
            q.Last = q.SelectSet;
            return q;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static DSelectQuery SelectAll<TTable>(string owner)
        {
            DSelectQuery q = new DSelectQuery();
            q.SelectSet = DSelectSet.SelectAll<TTable>(owner);
            q.Last = q.SelectSet;
            return q;
        }






        #endregion


        //
        // DBExecQuery factory methods
        //
        #region public static DBExecQuery Exec(string name)

        /// <summary>
        /// Begins a new 'EXEC [procedure name]' statement
        /// </summary>
        /// <param name="name">The name of the procedure</param>
        /// <returns>A new DBExecQuery to support statement chaining</returns>
        public static DExecQuery Exec(string name)
        {
            DExecQuery exec = new DExecQuery();
            exec.SprocName = name;
            return exec;
        }

        #endregion


        //
        // DBUpdateQuery factory methods
        //
        #region public static DBUpdateQuery Update() + 3 overloads

        /// <summary>
        /// Begins a new UPDATE statement
        /// </summary>
        /// <returns>A new DBUpdateQuery to support statement chaining</returns>
        public static DUpdateQuery Update()
        {
            return new DUpdateQuery();
        }

        /// <summary>
        /// Begins a new UPDATE [table] statement.
        /// </summary>
        /// <param name="table">The name of the table to update the rows on</param>
        /// <returns>A new DBUpdateQuery to support statement chaining</returns>
        public static DUpdateQuery Update(Type table)
        {
            DUpdateQuery upd = Update();
            upd.TableSet = DTableSet.From(table);
            upd.Last = upd.TableSet;
            return upd;
        }


        /// <summary>
        /// Create Update Query for boItem. 
        /// If  boItem is IVobject then it'll be converted into TargetTable instance
        /// and Update query will be create for end BO Instance value.
        /// </summary>
        /// <param name="boItem"></param>
        /// <returns></returns>
        public static DUpdateQuery Update(IBObject boItem, bool checkVObject = true)
        {
            if (checkVObject && (boItem is IVobject))
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "Update -target boItem is not IPersistableBO");//false

            return Update(boItem.Contract);

        }



        /// <summary>
        /// Create Update Query for TTable  Type.
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public static DUpdateQuery Update<TTable>()
        {
            DUpdateQuery upd = Update();
            upd.TableSet = DTableSet.From<TTable>();
            upd.Last = upd.TableSet;
            return upd;
        }




        /// <summary>
        /// Begins a new   UPDATE [table] statement
        /// </summary>
        /// <param name="table">The name of the table to update the rows on</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBUpdateQuery to support statement chaining</returns>
        public static DUpdateQuery Update(string owner, Type table)
        {
            DUpdateQuery upd = Update();
            upd.TableSet = DTableSet.From(owner, table);
            upd.Last = upd.TableSet;
            return upd;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static DUpdateQuery Update<TTable>(string owner)
        {
            DUpdateQuery upd = Update();
            upd.TableSet = DTableSet.From<TTable>(owner);
            upd.Last = upd.TableSet;
            return upd;
        }


        /// <summary>
        /// Begins a new UPDATE [source] statement
        /// </summary>
        /// <param name="source">The table to update the rows on</param>
        /// <returns>A new DBUpdateQuery to support statement chaining</returns>
        public static DUpdateQuery Update(DTable source)
        {
            DUpdateQuery upd = DUpdateQuery.Update();
            upd.TableSet = DTableSet.From(source);
            upd.Last = upd.TableSet;
            return upd;
        }

        #endregion


        //
        // DBInsertQuery factory methods
        //
        #region public static DBInsertQuery InsertInto(string intoTable) + 3 overloads


        /// <summary>
        /// Create Insert Item Query.
        /// If  boItem is IVobject then it'll be converted into TargetTable instance
        /// and Insert query will be create for end BO Instance value.
        /// </summary>
        /// <param name="insertBO"></param>
        /// <returns></returns>
        public static DInsertQuery InsertInto(IBObject insertBO, bool checkVObject = true)
        {
            if (checkVObject && (insertBO is IVobject))
                insertBO = (IBObject)((IVobject)insertBO).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (insertBO is IPersistableBO), "Insert -target boItem is not IPersistableBO");//false

            return
                 InsertInto(insertBO.Contract)
                .BuildInsertIntoFields(insertBO);

        }


        /// <summary>
        /// Begins a new INSERT INTO [table] statement
        /// </summary>
        /// <param name="intoTable">The table to insert the rows on</param>
        /// <returns>A new DBInsertQuery to support statement chaining</returns>
        public static DInsertQuery InsertInto(Type intoTable)
        {

            DTable ts = DTable.Table(intoTable);
            DInsertQuery q = new DInsertQuery();
            q.Into = ts;
            q.Last = ts;
            return q;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="intoTable"></param>
        /// <returns></returns>
        public static DInsertQuery InsertInto<TTable>()
        {
            DTable ts = DTable.Table<TTable>();
            DInsertQuery q = new DInsertQuery();
            q.Into = ts;
            q.Last = ts;
            return q;
        }





        /// <summary>
        /// Begins a new INSERT INTO [owner].[table] statement
        /// </summary>
        /// <param name="table">The table to insert the rows on</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBInsertQuery to support statement chaining</returns>
        public static DInsertQuery InsertInto(string owner, Type table)
        {
            DTable ts = DTable.Table(owner, table);
            DInsertQuery q = new DInsertQuery();
            q.Into = ts;
            q.Last = ts;
            return q;
        }


        /// <summary>
        /// Begins a new INSERT INTO [owner].[table] statement
        /// </summary>
        /// <param name="table">The table to insert the rows on</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBInsertQuery to support statement chaining</returns>
        public static DInsertQuery InsertInto<TTable>(string owner)
        {
            DTable ts = DTable.Table<TTable>(owner);
            DInsertQuery q = new DInsertQuery();
            q.Into = ts;
            q.Last = ts;
            return q;
        }


        /// <summary>
        /// Begins a new INSERT INTO [table] statement
        /// </summary>
        /// <param name="tbl">The table to insert the rows on</param>
        /// <returns>A new DBInsertQuery to support statement chaining</returns>
        public static DInsertQuery InsertInto(DTable tbl)
        {
            DInsertQuery q = new DInsertQuery();
            q.Into = tbl;
            q.Last = tbl;
            return q;
        }

        /// <summary>
        /// Begins a new INSERT INTO statement
        /// </summary>
        /// <returns>A new DBInsertQuery to support statement chaining</returns>
        internal static DInsertQuery InsertInto()
        {
            DInsertQuery q = new DInsertQuery();
            return q;
        }

        #endregion


        //
        // DBDeleteQuery factory methods
        //

        #region public static DBDeleteQuery DeleteFrom(string intoTable) + 2 overloads


        public static DDeleteQuery DeleteFrom(IBObject boItem, bool checkVObject = true)
        {
            if (checkVObject && (boItem is IVobject))
                boItem = (IBObject)((IVobject)boItem).CloneToTarget();

            Validator.AssertFalseDbg<InvalidOperationException>(
                (boItem is IPersistableBO), "Update -target boItem is not IPersistableBO");//false

            return DeleteFrom(boItem.Contract);

        }

        /// <summary>
        /// begins a new DELETE FROM [table] statement
        /// </summary>
        /// <param name="table">The name of the table to delete from</param>
        /// <returns>A new DBDeleteQuery to support statement chaining</returns>
        public static DDeleteQuery DeleteFrom(Type table)
        {
            DDeleteQuery q = new DDeleteQuery();
            DTable ts = DTable.Table(table);
            q.FromTable = ts;
            q.Last = ts;
            return q;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <returns></returns>
        public static DDeleteQuery DeleteFrom<TTable>()
        {
            DTable ts = DTable.Table<TTable>();
            DDeleteQuery q = new DDeleteQuery();
            q.FromTable = ts;
            q.Last = ts;
            return q;
        }



        /// <summary>
        /// begins a new DELETE FROM [owner].[table] statement
        /// </summary>
        /// <param name="table">The name of the table to delete from</param>
        /// <param name="owner">The schema owner of the table</param>
        /// <returns>A new DBDeleteQuery to support statement chaining</returns>
        public static DDeleteQuery DeleteFrom(string owner, Type table)
        {
            DTable ts = DTable.Table(owner, table);
            DDeleteQuery q = new DDeleteQuery();
            q.FromTable = ts;
            q.Last = ts;
            return q;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TTable"></typeparam>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static DDeleteQuery DeleteFrom<TTable>(string owner)
        {
            DTable ts = DTable.Table<TTable>(owner);
            DDeleteQuery q = new DDeleteQuery();
            q.FromTable = ts;
            q.Last = ts;
            return q;
        }




        /// <summary>
        /// begins a new DELETE FROM [table] statement
        /// </summary>
        /// <param name="table">The table to delete from</param>
        /// <returns>A new DBDeleteQuery to support statement chaining</returns>
        public static DDeleteQuery DeleteFrom(DTable table)
        {
            DDeleteQuery q = new DDeleteQuery();
            q.FromTable = table;
            q.Last = table;
            return q;
        }

        #endregion

        //
        // DBScriptQuery factory methods
        //

        #region public static DBScript Begin() + 2 overloads

        /// <summary>
        /// Starts a new script where multiple statements can be executed within the same command
        /// </summary>
        /// <returns>A new DBScript for statement chaining</returns>
        public static DScript Begin()
        {
            DScript s = new DScript();
            return s;
        }

        /// <summary>
        /// Starts a new script where multiple statements can be executed within the same command
        /// and sets the first statement to the specified query
        /// </summary>
        /// <param name="query">The first statement in the script</param>
        /// <returns>A new DBScript for statement chaining</returns>
        public static DScript Begin(DQuery query)
        {
            DScript s = Begin();
            s.Then(query);
            return s;
        }

        /// <summary>
        /// Starts a new script where the provided multiple statements can be executed within the same command
        /// </summary>
        /// <param name="querys">The statements in the script</param>
        /// <returns>A new DBScript for statement chaining</returns>
        public static DScript Begin(params DQuery[] querys)
        {
            DScript s = Begin();

            if (null != querys && querys.Length > 0)
            {
                foreach (DQuery q in querys)
                {
                    s.Then(q);
                }
            }

            return s;
        }

        #endregion

        #region public static DBScript Script(params DBQuery[] statements)

        /// <summary>
        /// Starts a new script where the provided multiple statements can be executed within the same command
        /// </summary>
        /// <param name="statements">The statements in the script</param>
        /// <returns>A new DBScript for statement chaining</returns>
        public static DScript Script(params DStatement[] statements)
        {
            DScript s = Begin();

            if (statements != null && statements.Length > 0)
                s.Inner.AddRange(statements);

            return s;
        }

        #endregion


        //
        // Build statement methods
        //

        #region protected virtual System.Data.CommandType GetCommandType()
        /// <summary>
        /// Gets the command type for this query (default is Text).
        /// Inheritors can override to return their own type of command
        /// </summary>
        /// <returns></returns>
        internal virtual System.Data.CommandType GetCommandType() //internal
        {
            return System.Data.CommandType.Text;
        }

        #endregion

        //
        // SQL String methods
        //


        #region public string ToSQLString(string provider)

        /// <summary>
        /// Generates the implementation specific sql statement for this query. 
        /// </summary>
        /// <param name="fordatabase">The DBDatabase to use for generation of the SQL statement</param>
        /// <returns>This SQL statement as a string</returns>
        public string ToSQLString(string providerKey)
        {
            return GetCommandText(this, providerKey);
        }

        protected string GetCommandText(DQuery query, string providerKey)
        {
            if (UseQueryPrebuildOnClient && PrebuildQuery != null)
            { return PrebuildQuery; }

            if (null == query)
                throw new ArgumentNullException("query");

            using (var sb = DStatementBuilder.Get(providerKey))
            {
                query.BuildStatement(sb);
                string text = sb.ToString().Trim();

                if (UseQueryPrebuildOnClient) // On CLient
                { PrebuildQuery = text; }

                return text;
            }
        }




        #endregion
        //
        // writeXml methods
        //
        //#region public void WriteXml(System.Xml.XmlWriter writer)
        ///// <summary>
        ///// Outputs this query as an xml stream onto the specifed writer
        ///// </summary>
        ///// <param name="writer"></param>
        //public void WriteXml(System.Xml.XmlWriter writer)
        //{
        //    this.WriteXml(writer, "http://schemas.perceiveit.co.uk/Query", "");
        //}

        //#endregion

        //#region public void WriteXml(System.Xml.XmlWriter writer, string ns, string prefix)

        ///// <summary>
        ///// Outputs this query as an xml stream onto the specified writer with the specified namespace and prefix
        ///// </summary>
        ///// <param name="writer">The XmlWriter to output to</param>
        ///// <param name="ns">The xml namespace to use</param>
        ///// <param name="prefix">The specifed prefix</param>
        //public void WriteXml(System.Xml.XmlWriter writer, string ns, string prefix)
        //{
        //    XmlWriterContext context = new XmlWriterContext(ns, prefix);
        //    writer.WriteStartElement(XmlHelper.Statement, ns);
        //    this.WriteXml(writer, context);
        //    if (context.Parameters.Count > 0)
        //    {
        //        writer.WriteStartElement(XmlHelper.Parameters);
        //        foreach (DBParam p in context.Parameters)
        //        {
        //            p.WriteFullParameterXml(writer, context);
        //        }
        //        writer.WriteEndElement();
        //    }
        //    writer.WriteEndElement();//statement
        //}

        //#endregion

        ////
        //// readXml methods
        ////
        //#region public static DBQuery ReadXml(System.Xml.XmlReader reader)
        ///// <summary>
        ///// Inputs the XmlReader and attempts to parse as a valid DbQuery
        ///// </summary>
        ///// <param name="reader">The reader to load the DbQuery from</param>
        ///// <returns>The parsed DbQuery</returns>
        //public static DBQuery ReadXml(System.Xml.XmlReader reader)
        //{
        //    return DBQuery.ReadXml(reader, "http://schemas.perceiveit.co.uk/Query", "");
        //}

        //#endregion

        //#region public static DBQuery ReadXml(System.Xml.XmlReader reader, string ns, string pref)

        ///// <summary>
        ///// Inputs the XmlReader and attempts to parse as a valid DbQuery with the specified element namespace and prefix
        ///// </summary>
        ///// <param name="reader">The reader to load the DbQuery from</param>
        ///// <param name="ns">The namespace of the DBQuery elements</param>
        ///// <param name="prefix">The prefix</param>
        ///// <returns>The parsed DbQuery</returns>
        //public static DBQuery ReadXml(System.Xml.XmlReader reader, string ns, string prefix)
        //{
        //    XmlReaderContext context = new XmlReaderContext(ns, prefix);
        //    return DBQuery.sReadXml(reader, context);
        //}

        //#endregion

        //#region public static DBQuery ReadXml(System.Xml.XmlReader reader, string ns, string pref, XmlFactory factory)

        ///// <summary>
        ///// Inputs the XmlReader and attempts to parse as a valid DbQuery with the 
        ///// specified element namespace and prefix
        ///// </summary>
        ///// <param name="reader">The reader to load the DbQuery from</param>
        ///// <param name="ns">The namespace of the DBQuery elements</param>
        ///// <param name="prefix">The prefix</param>
        ///// <param name="factory">An XmlFactory that creates new DBClause instances from their known names</param>
        ///// <returns>The parsed DbQuery</returns>
        //public static DBQuery ReadXml(System.Xml.XmlReader reader, string ns, string prefix, XmlFactory factory)
        //{
        //    XmlReaderContext context = new XmlReaderContext(ns, prefix, factory);
        //    return DBQuery.sReadXml(reader, context);
        //}

        //#endregion

        //#region private static DBQuery sReadXml(System.Xml.XmlReader reader, XmlReaderContext context)
        ///// <summary>
        ///// Read implementation
        ///// </summary>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //private static DBQuery sReadXml(System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    DBQuery q = null;
        //    DBClause c = null;

        //    while (reader.EOF == false && reader.NodeType != System.Xml.XmlNodeType.Element)
        //    {
        //        if (!reader.Read())
        //            break;
        //    }

        //    if (reader.NodeType == System.Xml.XmlNodeType.Element && XmlHelper.IsElementMatch(XmlHelper.Statement, reader, context))
        //    {
        //        while(reader.Read())
        //            if(reader.NodeType == System.Xml.XmlNodeType.Element)
        //                break;

        //        if (!reader.EOF)
        //        {
        //            c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c is DBQuery)
        //                q = (DBQuery)c;
        //            else
        //                throw XmlHelper.CreateException("The Xml stream did not begin with a valid DBQuery node, and cannot be deserialized", reader, null);
        //        }
        //        else
        //            throw XmlHelper.CreateException("The Xml stream did not begin with a valid DBQuery node, and cannot be deserialized", reader, null);
        //    }
        //    if (!reader.EOF)
        //    {
        //        while (reader.Read())
        //        {
        //            if (reader.NodeType == System.Xml.XmlNodeType.Element && reader.Name == XmlHelper.Parameters)
        //            {
        //                while (reader.Read())
        //                {
        //                    if (reader.NodeType == System.Xml.XmlNodeType.Element && XmlHelper.IsElementMatch(XmlHelper.Parameter, reader, context))
        //                    {
        //                        string name = reader.GetAttribute(XmlHelper.Name);
        //                        DBParam param;
        //                        if (context.Parameters.TryGetParameter(name, out param))
        //                            param.ReadXml(reader, context);
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    return q;
        //}

        //#endregion

        //#region IXmlSerializable Members

        ////System.Xml.Schema.XmlSchema System.Xml.Serialization.IXmlSerializable.GetSchema()
        ////{
        ////    throw new NotSupportedException("IXmlSerializable.GetSchema");
        ////}

        ////void System.Xml.Serialization.IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
        ////{
        ////     XmlReaderContext context = new XmlReaderContext("http://schemas.perceiveit.co.uk/Query", string.Empty);
        ////     this.ReadXml(reader, context);
        ////}

        //#endregion



    }
}
