 
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DDDD.Core.Data.DA2.Query
{
    /// <summary>
    /// Defines a single order by clause
    /// </summary>
    [DataContract]
    public partial class DOrder : DClause  //abstract
    {

        public DOrder() { }

        //
        // public properties
        //

        #region public DBClause Clause {get;set;}

        private DClause _clases;
        /// <summary>
        /// Gets or sets the order by XXX clause
        /// </summary>
        [DataMember]
        public DClause Clause
        {
            get { return _clases; }
            set { _clases = value; }
        }

        #endregion

        #region public Order Order {get;set;}

        private Order _order;
        /// <summary>
        /// Gets or sets the direction of sort
        /// </summary>
        [DataMember]
        public Order Order
        {
            get { return _order; }
            set { _order = value; }
        }

        #endregion



        #region  ------------------ IClonableClause.CloneClause -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public override DClause CloneClause()
        {
            return new DOrder()
            {   Order = this.Order,
                Clause = (this.Clause != null) ? this.Clause.CloneClause() : null
            };
        }

        #endregion  ------------------ IClonableClause.CloneClause -----------------------


        #region public override bool BuildStatement(DBStatementBuilder builder)



        public override bool BuildStatement(DStatementBuilder builder)
        {
            if (Clause == null)
                return false;

            builder.BeginOrderClause(this.Order);
            this.Clause.BuildStatement(builder);
            builder.EndOrderClause(this.Order);

            return true;
        }

        #endregion


        //
        // static factory methods
        //

        #region public static DBOrder OrderBy(Order order, DBClause clause)
        /// <summary>
        /// Creates a new OrderBy clause
        /// </summary>
        /// <param name="order"></param>
        /// <param name="clause"></param>
        /// <returns></returns>
        public static DOrder OrderBy(Order order, DClause clause)
        {
            DOrder orderC = new DOrder();
            orderC.Order = order;
            orderC.Clause = clause;
            return orderC;
        }

        #endregion

        #region internal static DBClause OrderBy()
        /// <summary>
        /// Creates a new empty order by clause
        /// </summary>
        /// <returns></returns>
        internal static DClause OrderBy()
        {
            return OrderBy(Order.Default, null);
        }

        #endregion
    }

    //[DataContract]
    //public partial class DBOrderRef : DBOrder
    //{
    //    public DBOrderRef() { }

    //    //
    //    // SQL Statement builder
    //    //


    //    #region  ------------------ IClonableClause.CloneClause -----------------------

    //    /// <summary>
    //    /// Cloning Clause
    //    /// </summary>
    //    /// <returns></returns>
    //    public override DBClause CloneClause()
    //    {
    //        return new DBOrderRef()
    //        {                
    //            Order = this.Order,
    //            Clause = (this.Clause != null) ? this.Clause.CloneClause() : null
    //        };
    //    }

    //    #endregion  ------------------ IClonableClause.CloneClause -----------------------
        


    //    //
    //    // XML Serialization
    //    //

    //    //#region protected override string XmlElementName {get;}
        
    //    //protected override string XmlElementName
    //    //{
    //    //    get { return XmlHelper.OrderBy; }
    //    //}

    //    //#endregion
                
    //    //#region protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)

    //    //protected override bool WriteAllAttributes(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{
    //    //    this.WriteAttribute(writer, XmlHelper.SortBy, this.Order.ToString(), context);

    //    //    return base.WriteAllAttributes(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
        
    //    //protected override bool WriteInnerElements(System.Xml.XmlWriter writer, XmlWriterContext context)
    //    //{

    //    //    if (this.Clause != null)
    //    //    {
    //    //        this.Clause.WriteXml(writer, context);
    //    //    }

    //    //    return base.WriteInnerElements(writer, context);
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
        
    //    //protected override bool ReadAnAttribute(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    bool b;

    //    //    if (this.IsAttributeMatch(XmlHelper.SortBy, reader, context))
    //    //    {
    //    //        this.Order = (Order)Enum.Parse(typeof(Order), reader.Value, true);
    //    //        b = true;
    //    //    }
    //    //    else 
    //    //        b = base.ReadAnAttribute(reader, context);

    //    //    return b;
    //    //}

    //    //#endregion

    //    //#region protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
        
    //    //protected override bool ReadAnInnerElement(System.Xml.XmlReader reader, XmlReaderContext context)
    //    //{
    //    //    this.Clause = context.Factory.Read(reader.Name, reader, context);
    //    //    return true;
    //    //}

    //    //#endregion

    //}




    [CollectionDataContract]
    public partial class DOrderList : List<DOrder>, ICloneable  //  DBClauseList<DBOrder> //internal
    {
        public DOrderList() { }

        /// <summary>
        /// Gets the separator for each token when the statements are built
        /// </summary>
       // [DataMember]
        public virtual string TokenSeparator { get { return ", "; } }

        /// <summary>
        /// Gets the flag that identifies if this list should use the standard builder ReferenceSeparator
        /// </summary>
       // [DataMember]
        public virtual bool UseBuilderSeparator { get { return true; } }


        #region  ------------------ IClonable.Clone -----------------------

        /// <summary>
        /// Cloning Clause
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            DOrderList newOrderList = new DOrderList();
            foreach (var item in this)
            { newOrderList.Add(item.CloneClause() as DOrder); }

            return newOrderList;           
        }

        #endregion  ------------------ IClonable.Clone -----------------------
        




        /// <summary>
        /// Builds this list of tokens onto the statement
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder)
        {
            return this.BuildStatement(builder, false, false);
        }

        /// <summary>
        /// Builds the list of tokens 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="eachOnNewLine"></param>
        /// <param name="indent"></param>
        /// <returns></returns>
        public virtual bool BuildStatement(DStatementBuilder builder, bool eachOnNewLine, bool indent)
        {
            int count = 0;

            bool outputSeparator = false; //initially false

            string start = DTokenListOptions.ListStart;
            if (!string.IsNullOrEmpty(start))
                builder.WriteRaw(DTokenListOptions.ListStart);

            for (int i = 0; i < this.Count; i++)
            {
                DToken clause = this[i];

                if (outputSeparator)
                {
                    if (DTokenListOptions.UseBuilderSeparator)
                        builder.AppendReferenceSeparator();

                    else if (!string.IsNullOrEmpty(DTokenListOptions.TokenSeparator))
                        builder.WriteRaw(DTokenListOptions.TokenSeparator);

                    if (eachOnNewLine)
                        builder.BeginNewLine();
                }

                //if the clause outputs then it should return true - so the next item has a separator written
                outputSeparator = clause.BuildStatement(builder);

                if (outputSeparator) //if we were output then increment the count
                    count++;
            }

            string end = DTokenListOptions.ListEnd;
            if (!string.IsNullOrEmpty(end))
                builder.WriteRaw(end);

            //did we write anything
            if (count > 0 || !string.IsNullOrEmpty(start) || !string.IsNullOrEmpty(end))
                return true;
            else
                return false;

        }

        ///// <summary>
        ///// Parses the XML data and generates the list of DBClauses from this.
        ///// </summary>
        ///// <param name="endElement"></param>
        ///// <param name="reader"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool ReadXml(string endElement, System.Xml.XmlReader reader, XmlReaderContext context)
        //{
        //    bool isEmpty = reader.IsEmptyElement && XmlHelper.IsElementMatch(endElement, reader, context);

        //    do
        //    {
        //        if (reader.NodeType == System.Xml.XmlNodeType.Element)
        //        {


        //            DBClause c = context.Factory.Read(reader.LocalName, reader, context);
        //            if (c != null)
        //                this.Add((DBOrder)c); // added DBOrder

        //            if (isEmpty)
        //                return true;
        //        }

        //        if (reader.NodeType == System.Xml.XmlNodeType.EndElement && XmlHelper.IsElementMatch(endElement, reader, context))
        //            break;
        //    }
        //    while (reader.Read());

        //    return true;

        //}

        ///// <summary>
        ///// Outputs the XML for each element in the list
        ///// </summary>
        ///// <param name="xmlWriter"></param>
        ///// <param name="context"></param>
        ///// <returns></returns>
        //public bool WriteXml(System.Xml.XmlWriter xmlWriter, XmlWriterContext context)
        //{
        //    if (this.Count > 0)
        //    {
        //        foreach (DBClause tkn in this)
        //        {
        //            tkn.WriteXml(xmlWriter, context);
        //        }
        //        return true;
        //    }
        //    else
        //        return false;
        //} 



    }
}
