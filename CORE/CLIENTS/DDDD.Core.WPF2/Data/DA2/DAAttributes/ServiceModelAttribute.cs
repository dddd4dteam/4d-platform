﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;
using System.ComponentModel.Composition;

 
namespace DDDD.Core.Data.DA2.Mapping
{

    /// <summary>    
    /// Service Model for which this BO belongs
    /// </summary>        
    [AttributeUsage(AttributeTargets.Class 
        | AttributeTargets.Struct, AllowMultiple = false)]
    public sealed class ServiceModelAttribute : Attribute
    {

        /// <summary>
        /// Map Service Model to BO contract: ServieModel Key and Service Model Manager Type
        /// </summary>
        /// <param name="serviceModel">Here Service model  Name like [Sx_Name] wher x is number</param>
        /// <param name="serviceModelManager"></param>
        public ServiceModelAttribute(string serviceModel, Type serviceModelType)
        {
            ServiceModel = serviceModel;
            ServiceIndex = NamingConvention.GetServiceModelIndex(serviceModel);
            ServiceModelType = serviceModelType;
        }

        /// <summary>
        /// Map Service Model to BO contract: ServieModel Key and Service Model Manager Type
        /// </summary>
        /// <param name="serviceModelType"></param>
        public ServiceModelAttribute(Type serviceModelType)
        {
            ServiceModelType = serviceModelType;
            ServiceModel = serviceModelType.Name;
            ServiceIndex = NamingConvention.GetServiceModelIndex(ServiceModel);           
        }



        /// <summary>        
        /// Service Model Key
        /// </summary>
        public string ServiceModel
        { get; private set; }

        /// <summary>
        /// Service Model Index  : if [S123] - 123
        /// </summary>
        public int ServiceIndex
        { get; private set; }


        /// <summary>
        /// Service Model  Type
        /// </summary>
        public Type ServiceModelType
        { get; private set; }

    }
}
