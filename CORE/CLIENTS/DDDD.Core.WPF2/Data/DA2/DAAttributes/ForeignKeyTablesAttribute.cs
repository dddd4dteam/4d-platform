﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2.Mapping
{
    /// <summary>
    /// Indicates that table depends on other tables. Each (distinct) table type is stored in TableTypes array
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class ForeignKeyTablesAttribute : Attribute
    {
        public ForeignKeyTablesAttribute(params Type[] TableTypes)
        {
            this.TableTypes = TableTypes;
        }

        public Type[] TableTypes { get; set; }
    }

}

