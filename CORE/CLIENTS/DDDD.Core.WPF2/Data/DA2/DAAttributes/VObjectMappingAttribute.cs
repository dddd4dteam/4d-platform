﻿using System;


namespace DDDD.Core.Data.DA2.Mapping
{

    
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public sealed class VObjectMapAttribute : Attribute
    {
        public VObjectMapAttribute(Type TableEntity)
        {
            DALTableEntity = TableEntity;            
        }
        
        
        /// <summary>
        ///Type of DAL Table Entity 
        /// </summary>
        public Type DALTableEntity
        {  get; private set; }
                 

    }
}
