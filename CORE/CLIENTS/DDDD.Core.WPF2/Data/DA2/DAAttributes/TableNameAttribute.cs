﻿using System;

namespace   DDDD.Core.Data.DA2.Mapping
{

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface)]
	public class TableNameAttribute : Attribute
	{
		

		public TableNameAttribute(string name)
		{
			Name = name;
		}

		public TableNameAttribute(string database, string name)
		{
			Database = database;
			Name = name;
		}

		public TableNameAttribute(string database, string owner, string name)
		{			
			Database = database;
			Owner = owner;
			Name = name;
		}


		public virtual string Name
		{ get; private set;}
			    
		public virtual string Database
		{ get; private set; }
				
		public virtual string Owner
		{ get; private set; }

		
		
	}

}