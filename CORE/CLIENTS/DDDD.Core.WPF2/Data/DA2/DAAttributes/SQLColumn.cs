﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 
namespace DDDD.Core.Data.DA2.Mapping
{

    /// <summary>
    /// Used to indicate that Field or Property is directly mapped to SQL Column
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class SQLColumnAttribute : Attribute
    {
    }

}
