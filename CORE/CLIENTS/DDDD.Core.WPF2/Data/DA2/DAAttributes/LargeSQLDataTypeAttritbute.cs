﻿using System;

namespace DDDD.Core.Data.DA2.Mapping
{
    /// <summary>
    /// Used to indicate fields mapped to large SQL DataTypes (text, varchar(MAX), nvarchar(MAX), varbinary(MAX))
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public sealed class LargeSQLDataTypeAttribute : Attribute
    {
    }
}
