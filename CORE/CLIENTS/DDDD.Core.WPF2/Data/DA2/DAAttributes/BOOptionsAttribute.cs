﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2.Mapping
{
    /// <summary>
    /// Атрибут роли БО- Опции - навигация по джанным / редактируемость для уровня DAL  и модели данных  
    /// BO(Business Object) Options attribute. Options : like Navigation, editable in DAL...
    /// <para/> Here we use BOOptionsEn flagged enum value. Only one attrbute enabled.
    /// </summary>   
    [AttributeUsage(AttributeTargets.Class 
     | AttributeTargets.Struct, AllowMultiple = false)]
    public sealed class BOOptionsAttribute : Attribute
    {

        #region -------------- CTOR ---------------

        public BOOptionsAttribute()
        {   ///By Default it s only Navigable Option
            Options = BOOptionsEn.Navigable;
        }
        public BOOptionsAttribute(BOOptionsEn objectOptions)
        {
            Options = objectOptions;
        }

        #endregion -------------- CTOR ---------------
        

        public BOOptionsEn Options
        { get; private set; }


    }
}
