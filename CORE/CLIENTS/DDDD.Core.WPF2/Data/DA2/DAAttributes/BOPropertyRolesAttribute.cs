﻿using System;


namespace DDDD.Core.Data.DA2.Mapping
{

    /// <summary>
    /// BO(Business Object) Property roles attribute.     
    /// <para/> Here we use  PropertyRoleEn flagged enum value. Only one attrbute enabled.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property 
        | AttributeTargets.Field,AllowMultiple=false)]
    public sealed class BOPropertyRolesAttribute : Attribute
    {
        public BOPropertyRolesAttribute()
        {
            PropertyRole = PropertyRoleEn.DataField;// По умолчанию
        }

        public BOPropertyRolesAttribute(PropertyRoleEn propertyRole, int pkIndex, int pkIdentityStep =-1)
        {
            PropertyRole = propertyRole;
            PropertyRole = propertyRole;
            PKIndex = pkIndex;
            PKIdentityStep = pkIdentityStep;
        }

        public BOPropertyRolesAttribute(PropertyRoleEn propertyRole)
        {
            PropertyRole = propertyRole;
        }

        /// <summary>
        /// Ctor with additional Parameter
        /// </summary>
        /// <param name="propertyRole"></param>
        /// <param name="AdditionalParam"></param>
        public BOPropertyRolesAttribute(PropertyRoleEn propertyRole, String crAdditionalParam)
        {
            PropertyRole = propertyRole;
            AdditionalParam = crAdditionalParam;
        }
        
        
        /// <summary>
        /// Role of Property in BO
        /// </summary>       
        public PropertyRoleEn PropertyRole
        {   get; private set;   }

        /// <summary>
        ///  PK(Primary Key) Index. By Default (-1)
        /// </summary>
        public int PKIndex 
        { get; private set; } = -1;


        /// <summary>
        /// When Primary Key is Identity, then Identity logic can contains increment-Step value different from 1- so here we can save this value.
        /// Default Value -1 - means value not settted
        /// </summary>
        public int PKIdentityStep
        { get; private set; } = -1;


        /// <summary>
        /// Additional parameter: 
        ///<para/> - when PropertyRole is VoFkey then AdditionalParam means Parent contract Name for this ForegnKeyProperty property
        ///<para/> - when PropertyRole is Fkey then AdditionalParam means Parent contract Name for this ForegnKeyProperty property
        /// </summary>
        public String AdditionalParam
        { get; private set; }

    }



}
