﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Runtime.Serialization;


namespace DDDD.Core.Data.DA2.Mapping
{
    /// <summary>    
    /// BO(Business Object)  roles attribute.     
    /// <para/> Here we use BORoleEn flagged enum value. Only one attrbute enabled.
    /// </summary>   
    [AttributeUsage(AttributeTargets.Class
        | AttributeTargets.Struct , AllowMultiple=false) ]
    public sealed class BORolesAttribute : Attribute
    {
        public BORolesAttribute()
        {
           //По умолчанию  - это роль несвязанной  ни с какими обработчиками тип Structure

            //По умолчанию - это объект структуры - т.е. у него нет каких то связей с DAL напрямую в менеджере модели
            _objectRole = BORoleEn.Structure;                        
        }


        public BORolesAttribute(BORoleEn ObjectRole)
        {
            _objectRole = ObjectRole;
            
        }


        private BORoleEn _objectRole;
        /// <summary>
        /// ROle of  Business Object
        /// </summary>
        [DataMember]
        public BORoleEn ObjectRole
        {
            get { return _objectRole; }
            set { _objectRole = value; }
        }
    }
}
