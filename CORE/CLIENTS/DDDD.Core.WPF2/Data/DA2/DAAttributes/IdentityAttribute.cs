﻿using System;

namespace DDDD.Core.Data.DA2.Mapping
{
	[Serializable]
	[AttributeUsage(
	AttributeTargets.Field | AttributeTargets.Property |
	AttributeTargets.Class | AttributeTargets.Interface,
	AllowMultiple = true)]
	public class IdentityAttribute   : Attribute
	{
		public IdentityAttribute()  
		{
		}
	}
}