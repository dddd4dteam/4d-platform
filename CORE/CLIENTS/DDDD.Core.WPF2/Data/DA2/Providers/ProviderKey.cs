﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2
{


	/// <summary>
	/// ///Data Provider Keys
	/// </summary>
	public static class ProviderKey
	{
		/// <summary>
		/// Default Provider Key . Can be changed . Dedault value is MsSql.
		/// </summary>
		public static string Default
		{ get; set; } = MsSql;

		/// <summary>
		///  Ms Access data provider key
		/// </summary>
		public const string Access = "Access";
		/// <summary>
		///  IBM DB2  data provider key
		/// </summary>
		public const string DB2 = "DB2";
		/// <summary>
		/// Firebirs data provider
		/// </summary>
		public const string Firebird = "Fdp";
		/// <summary>
		/// Informix data provider
		/// </summary>
		public const string Informix = "Informix";

		/// <summary>
		/// Ms SqlServer data provider 
		/// </summary>
		public const string MsSql = "Sql";
		/// <summary>
		/// Ms SqlServer 2000 data provider 
		/// </summary>
		public const string MsSql2000 = "MsSql2000";

		/// <summary>
		/// Ms SqlServer 2005 data provider 
		/// </summary>
		public const string MsSql2005 = "MsSql2005";

		/// <summary>
		/// Ms SqlServer 2008 data provider 
		/// </summary>
		public const string MsSql2008 = "MsSql2008";

		/// <summary>
		/// Ms SqlServer 2012 data provider 
		/// </summary>
		public const string MsSql2012 = "MsSql2012";
		/// <summary>
		/// Ms SqlServer 2014 data provider 
		/// </summary>
		public const string MsSql2014 = "MsSql2014";
		/// <summary>
		/// Ms SqlServer 2017 data provider 
		/// </summary>
		public const string MsSql2017 = "MsSql2017";
		/// <summary>
		/// MySQl  data provider 
		/// </summary>
		public const string MySql = "MySql";
		/// <summary>
		/// ODBC  some data provider 
		/// </summary>
		public const string Odbc = "Odbc";
		/// <summary>
		/// OleDB  some data provider 
		/// </summary>
		public const string OleDb = "OleDb";
		/// <summary>
		/// Oracle  data provider 
		/// </summary>
		public const string Oracle = "Odp";
		/// <summary>
		/// Oracle Managed data provider 
		/// </summary>
		public const string OracleManaged = "OdpManaged";
		/// <summary>
		/// PostgreSQL  data provider 
		/// </summary>
		public const string PostgreSQL = "PostgreSQL";
		/// <summary>
		/// Sql Server compact edition  data provider
		/// </summary>
		public const string SqlCe = "SqlCe";
		/// <summary>
		/// Sqlite data provider
		/// </summary>
		public const string SQLite = "SQLite";
		/// <summary>
		/// Sybase data provider
		/// </summary>
		public const string Sybase = "Sybase";


		/// <summary>
		/// MongoDB NoSQL Store 
		/// </summary>
		public const string Mongo = "MongoDB";

		/// <summary>
		/// Redis NoSQL Store 
		/// </summary>
		public const string Redis = "Redis";


	}


	public static class ProviderNamespace
	{
		public const string DB2 = "IBM.Data.DB2";
		public const string Firebird = "FirebirdSql.Data.FirebirdClient";
		public const string Informix = "IBM.Data.Informix.Client";
		public const string MsSql = "System.Data.SqlClient";
		public const string MySql = "MySql.Data.MySqlClient";
		public const string Odbc = "System.Data.Odbc";
		public const string OleDb = "System.Data.OleDb";
		public const string OracleNet = "System.Data.OracleClient";
		public const string Oracle = "Oracle.DataAccess.Client";
		public const string PostgreSQL = "Npgsql";
		public const string SqlCe = "System.Data.SqlServerCe";
		public const string SQLite = "System.Data.SQLite";
		public const string Sybase = "Sybase.Data.AseClient";
	}
}
