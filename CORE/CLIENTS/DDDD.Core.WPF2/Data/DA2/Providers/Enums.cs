﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.DA2 
{

    /// <summary>
    /// Defines the type of parameter referencing used by the database engine.
    /// </summary>
    public enum DBParameterLayout
    {
        /// <summary>
        /// The order the parameters are declated in does not matter. The names of the parameters are used as identifiers.
        /// </summary>        
        Named
        
        /// <summary>
        /// The names of the parameters are ignored and the position (order of referencing and declaration) is the way values are determined
        /// </summary>        
        ,Positional
    }


    /// <summary>
    /// Defines the schema types in a database
    /// </summary>
    [Flags()]
    public enum DBSchemaTypes
    {
        /// <summary>
        /// A unknown 
        /// </summary>        
        UnKnown = 1, 

        /// <summary>
        /// A database table
        /// </summary>        
        Table = 2, 

        /// <summary>
        /// A prefined view on one or more tables
        /// </summary>        
        View = 4, 

        /// <summary>
        /// A predefined executable procedure 
        /// </summary>        
        StoredProcedure = 8, 

        /// <summary>
        /// A predfined function that can be caled inline.
        /// </summary>        
        Function = 16, 

        /// <summary>
        ///  Triggers on Update/ Delete operations
        /// </summary>
        Trigger = 32,

        /// <summary>
        /// A fast access lookup
        /// </summary>        
        Index = 64,   

        /// <summary>
        /// A set of statements
        /// </summary>        
        CommandScripts = 128, 

        /// <summary>
        /// An indexed link between 2 tables
        /// </summary>        
        ForeignKey = 256   
    }


    /// <summary>
    /// All the different returned range options - limits
    /// </summary>    
    public enum TopType
    {
        /// <summary>
        /// Explicit number to return
        /// </summary>        
        Count,
        /// <summary>
        /// The maximum percentage to return
        /// </summary>        
        Percent,

        /// <summary>
        /// Within a specific range - not curently implemented as not fully suported in all db engines.
        /// </summary>        
        Range           
      
    }



}
