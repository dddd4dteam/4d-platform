﻿
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace DDDD.Core.Data.DA2
{

    /// <summary>
    /// Data Provider for Ms SQl Server Databases
    /// </summary>
    public class MsSqlDataProvider : SqlDataProvider
    {
        static readonly Type CT = typeof(MsSqlDataProvider);
        static readonly string CN = nameof(MsSqlDataProvider);
        
        public override Type ProviderType
        { get; } = CT;

        public override string Name
        { get; } = CN;
        
        public override string Key
        { get; } = ProviderKey.MsSql;

        public override string FullName
        { get; } = DA2.ProviderNamespace.MsSql;

        public override Type ConnectionType
        { get; } = typeof(SqlConnection);

        public override IDbConnection CreateConnection(string connectString)
        {
            return new SqlConnection(connectString);
        }

        public override IDbCommand CreateCommand(IDbConnection connection)
        {
            return (connection as SqlConnection).CreateCommand();
        }

        protected internal override Type TransactionType
        { get; } = typeof(SqlTransaction);


        public override DbDataAdapter CreateDataAdapterObject()
        {
            return new SqlDataAdapter();            
        }

        public override bool DeriveParameters(IDbCommand command)
        {
            throw new NotImplementedException();
        }

        public override DBSchemaTypes SchemaOptions
        {   get;  }= DBSchemaTypes.Function
                        | DBSchemaTypes.Trigger
                        | DBSchemaTypes.StoredProcedure
                        | DBSchemaTypes.Table
                        | DBSchemaTypes.Index
                        | DBSchemaTypes.View
                        | DBSchemaTypes.CommandScripts;

        public override DbType[] SUPPORTED_TYPES
        { get; }= new DbType[] {DbType.Int32, DbType.Int64, DbType.AnsiString, DbType.AnsiStringFixedLength
                                ,DbType.Boolean, DbType.Byte, DbType.Currency, DbType.DateTime
                                ,DbType.Decimal, DbType.Double, DbType.Guid, DbType.Single
                                ,DbType.String, DbType.StringFixedLength, DbType.Binary};

        protected internal override DBProperties GetPropertiesFromDb(DBProxy  proxy) //DBDatabase forDatabase
        {
            var statement = "SELECT  SERVERPROPERTY('productversion') AS [version]"
                            + ", SERVERPROPERTY ('productlevel') AS [level]" 
                            +", SERVERPROPERTY ('edition') AS [edition]";

            DBProperties props;
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(proxy.ConnectionString);
            string dbname = builder.InitialCatalog;
            if (string.IsNullOrEmpty(dbname))
                dbname = builder.DataSource;


            props =  proxy.SelectItem<DBProperties>(statement,  (reader)=>
                {
                 
                    if (reader.Read())
                    {
                        return new DBProperties(dbname
                                , "SQL Server"
                                , reader["edition"].ToString()
                                , reader["level"].ToString()
                                , "@{0}"
                                , new Version(reader["version"].ToString())
                                , SchemaOptions
                                , false
                                , DBParameterLayout.Named
                                , SUPPORTED_TYPES
                                , new TopType[] { TopType.Count, TopType.Percent });
                    }
                    else
                    {
                        return DBProperties.Unknown;

                    }
                });

            return props;

        }

        protected internal override DBProperties GetDefaultProperties() //DBDatabase forDatabase
        {
            var defaultEdition = "MS Sql Server 2012 Standart";
            var defaultLevel = "1";
            var defaultVersion = "12.0.1000";
            var defaultDBName = "tempdb";

            DBProperties props = new DBProperties(defaultDBName
                            , "SQL Server"
                            , defaultEdition // reader["edition"].ToString()
                            , defaultLevel  //reader["level"].ToString()
                            , "@{0}"
                            , new Version(defaultVersion)// reader["version"].ToString()
                            , SchemaOptions
                            , false
                            , DBParameterLayout.Named
                            , SUPPORTED_TYPES
                            , new TopType[] { TopType.Count, TopType.Percent });

            return props;
        }

        protected override List<Type> SpecificTypes
        { get; } = new List<Type>() { 
             typeof(SqlBinary) ,typeof(SqlBoolean), typeof(SqlByte)
            ,typeof(SqlBytes), typeof(SqlChars)
            ,typeof(SqlDateTime), typeof(SqlDecimal)
            ,typeof(SqlDouble), typeof(SqlGuid)
            ,typeof(SqlInt16), typeof(SqlInt32)
            ,typeof(SqlInt64), typeof(SqlMoney)
            ,typeof( SqlSingle),  typeof(SqlString)
            ,typeof( SqlXml)
        };
               

        protected internal override void AddSetMemberFunc(Dictionary<string, DASetMemberValueDelegate>  map, string property, Type proptyType)
        {
            // SqlBinary   | SqlBoolean | SqlByte
            // SqlBytes    | SqlChars   | SqlDateTime
            // SqlDecimal  | SqlDouble  | SqlGuid
            // SqlInt16    |  SqlInt32  | SqlInt64
            // SqlMoney    |  SqlSingle | SqlString
            // SqlXml

            // member for provider mapped key : [provider.member] 
            var memberKey = Key + "." + property;


            if (proptyType == typeof(SqlBinary))
            { 
                map.Add(memberKey, SetSqlBinary);
            }
            if (proptyType == typeof(SqlBoolean))
            {
                map.Add(memberKey, SetSqlBoolean);
            }
            if (proptyType == typeof(SqlByte))
            {
                map.Add(memberKey, SetSqlByte);
            }
            if (proptyType == typeof(SqlBytes))
            {
                map.Add(memberKey, SetSqlBytes);
            }
            if (proptyType == typeof(SqlChars))
            {
                map.Add(memberKey, SetSqlChars);
            }
            if (proptyType == typeof(SqlDateTime))
            {
                map.Add(memberKey, SetSqlDateTime);
            }
            if (proptyType == typeof(SqlDecimal))
            {
                map.Add(memberKey, SetSqlDecimal);
            }
            if (proptyType == typeof(SqlDouble))
            {
                map.Add(memberKey, SetSqlDouble);
            }
            if (proptyType == typeof(SqlGuid))
            {
                map.Add(memberKey, SetSqlGuid);
            }
            if (proptyType == typeof(SqlInt16))
            {
                map.Add(memberKey, SetSqlInt16);
            }
            if (proptyType == typeof(SqlInt32))
            {
                map.Add(memberKey, SetSqlInt32);
            }
            if (proptyType == typeof(SqlInt64))
            {
                map.Add(memberKey, SetSqlInt64);
            }
            if (proptyType == typeof(SqlMoney))
            {
                map.Add(memberKey, SetSqlMoney);
            }
            if (proptyType == typeof(SqlSingle))
            {
                map.Add(memberKey, SetSqlSingle);
            }
            if (proptyType == typeof(SqlString))
            {
                map.Add(memberKey, SetSqlString);
            }
            if (proptyType == typeof(SqlXml))
            {
                map.Add(memberKey, SetSqlXml);
            }
        }


        #region ----------  SETTER METHODS FOR  SPECIFIC PROVIDER  TYPES  WITH ITypeAccessor -------------
        // SqlBinary   | SqlBoolean | SqlByte
        // SqlBytes    | SqlChars   | SqlDateTime
        // SqlDecimal  | SqlDouble  | SqlGuid
        // SqlInt16    |  SqlInt32  | SqlInt64
        // SqlMoney    |  SqlSingle | SqlString
        // SqlXml

        static void SetSqlBinary(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlBinary(ordinal);
            accessor.SetMember<SqlBinary>(member, ref targetItem, value);
        }

        static void SetSqlBoolean(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlBoolean(ordinal);
            accessor.SetMember<SqlBoolean>(member, ref targetItem, value);
        }


        static void SetSqlByte(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlByte(ordinal);
            accessor.SetMember<SqlByte>(member, ref targetItem, value);
        }

        static void SetSqlBytes(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlBytes(ordinal);
            accessor.SetMember<SqlBytes>(member, ref targetItem, value);
        }



        static void SetSqlChars(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlChars(ordinal);
            accessor.SetMember<SqlChars>(member, ref targetItem, value);
        }



        static void SetSqlDateTime(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlDateTime(ordinal);
            accessor.SetMember<SqlDateTime>(member, ref targetItem, value);
        }


        static void SetSqlDecimal(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlDecimal(ordinal);
            accessor.SetMember<SqlDecimal > (member, ref targetItem, value);
        }

        static void SetSqlDouble(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlDouble(ordinal);
            accessor.SetMember<SqlDouble>(member, ref targetItem, value);
        }

        static void SetSqlGuid(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlGuid(ordinal);
            accessor.SetMember<SqlGuid>(member, ref targetItem, value);
        }

        static void SetSqlInt16(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlInt16(ordinal);
            accessor.SetMember<SqlInt16>(member, ref targetItem, value);
        }

        static void SetSqlInt32(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlInt32(ordinal);
            accessor.SetMember<SqlInt32>(member, ref targetItem, value);
        }

        static void SetSqlInt64(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlInt64(ordinal);
            accessor.SetMember<SqlInt64>(member, ref targetItem, value);
        }
        

        static void SetSqlMoney(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlMoney(ordinal);
            accessor.SetMember<SqlMoney>(member, ref targetItem, value);
        }

        static void SetSqlSingle(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlSingle(ordinal);
            accessor.SetMember<SqlSingle>(member, ref targetItem, value);
        }

        static void SetSqlString(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlString(ordinal);
            accessor.SetMember<SqlString>(member, ref targetItem, value);
        }
        static void SetSqlXml(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var specReader = reader as SqlDataReader;
            var value = specReader.GetSqlXml(ordinal);
            accessor.SetMember<SqlXml>(member, ref targetItem, value);
        }


        #endregion  ----------  SETTER METHODS FOR  SPECIFIC PROVIDER  TYPES WITH ITypeAccessor -------------


    }
}
