﻿using System;
using System.Collections.Generic;

using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

using DDDD.Core.Collections.Concurrent;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Reflection;



namespace DDDD.Core.Data.DA2
{

   

    /// <summary>
    /// Base abstract Sql Provider. 
    /// <para/>Mapps Standart IDataReader Types  
    /// </summary>
    public abstract class SqlDataProvider      
    {

        #region --------------- CACHE PROVIDERS -----------------
        
        private static readonly object syncRoot = new object();

        internal static SqlDataProvider Get(Type providerType)            
        {
            var targetType = providerType;
            var key = Tps.GetTypeID(targetType);
            SqlDataProvider provdr = null;
            if (providers.TryGetValue(key, out provdr) == false)
            {
                lock (syncRoot)
                {
                    if (providers.ContainsKey(key) == false)
                    {
                        //create and add to cache
                        var newProvider = TypeActivator
                            .CreateInstanceTBase<SqlDataProvider>(targetType);
                       
                        providers.Add(key, newProvider);
                    }
                }
                return providers[key];
            }
            else
            { return provdr; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="providerKey"></param>
        /// <returns></returns>
        internal static SqlDataProvider GetByKey(string providerKey)
        {
            return providers.GetValue(dbprov => dbprov.Key == providerKey); 
        }


        /// <summary>
        /// Get Data provider of    T Type.Where T is DataProviderBase
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static SqlDataProvider Get<T>() where T : SqlDataProvider
        {
            return Get(typeof(T));
        }


        static readonly SafeDictionary2<int, SqlDataProvider> providers =
            new SafeDictionary2<int, SqlDataProvider>();



        #endregion --------------- CACHE PROVIDERS -----------------


        #region ---------------- PROPERTIES ---------------

        /// <summary>
        /// Key of Data Provider . Like [MsSql], [MsSql2014], [Oracle]...
        /// </summary>
        public abstract string Key
        { get; }

        /// <summary>
        /// Current Data Provider Type
        /// </summary>
        public abstract Type ProviderType
        { get; } 

        /// <summary>
        /// Data Provider Class Name.
        /// </summary>
        public abstract string Name
        { get; }


        /// <summary>
        /// Data Provider Class FullName. -Namespace name to Connection type.
        /// </summary>
        public abstract string FullName
        { get; }




        private string _uniqueName;
        /// <summary>
		/// Same as <see cref="Name"/>, but may be overridden to add two or more providers of same type.
		/// </summary>
        public string UniqueName
        {
            get { return _uniqueName ?? Name; }
            set { _uniqueName = value; }
        }
        public virtual int MaxParameters 
        { get { return 100; } }
        public virtual int MaxBatchSize 
        { get { return 65536; } }
        public virtual string EndOfSql 
        { get { return ";"; } }



        /// <summary>
        /// Supported Schema Options - 
        /// Table/ View/Functions /Triggers
        /// /Stored rocedures / Index /Scripts
        /// /ForeignKey
        /// </summary>
        public abstract DBSchemaTypes SchemaOptions
        { get; }

        /// <summary>
        /// Supported  Types
        /// </summary>
        public abstract DbType[] SUPPORTED_TYPES
        { get; }

        #endregion ---------------- PROPERTIES ---------------


        #region ------------------- LOAD DbProperties -------------------

        /// <summary>
        /// Load DB Properties
        /// </summary>
        /// <param name="proxy"></param>
        /// <returns></returns>
        internal protected abstract DBProperties GetPropertiesFromDb(DBProxy proxy); //DBDatabase forDatabase


        /// <summary>
        /// Load DB Properties as stub DB to build Query independentlys
        /// </summary>
        /// <returns></returns>
        internal protected abstract DBProperties GetDefaultProperties();  


        #endregion------------------- LOAD DbProperties -------------------


        #region---------------- DbDataAdapter ------------------

        /// <summary>
        /// Creates an instance of the <see cref="DbDataAdapter"/>.
        /// </summary>
        /// <remarks>
        /// See the <see cref="DbManager.AddDataProvider(SqlDataProvider)"/> method to find an example.
        /// </remarks>
        /// <seealso cref="DbManager.AddDataProvider(SqlDataProvider)">AddDataManager Method</seealso>
        /// <returns>The <see cref="DbDataAdapter"/> object.</returns>
        public abstract DbDataAdapter CreateDataAdapterObject();

        #endregion---------------- DbDataAdapter ------------------


        #region ---------------- IDbConnection ----------------

        /// <summary>
        /// Returns an actual type of the connection object used by this instance of the <see cref="DBProxy"/>.
        /// </summary>
        public abstract Type ConnectionType
        { get; }

        /// <summary>
        /// Data Provider namespace - Connection class Namespace
        /// </summary>
        public virtual string ProviderNamespace
        { get { return ConnectionType.Namespace; } }


        /// <summary>
        /// Creates a new instance of the <see cref="IDbConnection"object
        /// </summary>
        /// <returns></returns>
        public abstract IDbConnection CreateConnection(string connectString);


        #endregion ---------------- IDbConnection ----------------


        #region ---------------- IDbCommand -------------------

        /// <summary>
        /// Create Command Object
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public virtual IDbCommand CreateCommand(IDbConnection connection)
        {
            return connection.CreateCommand();
        }



        /// <summary>
        /// Attach Parameter to command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="parameter"></param>
        public virtual void AttachParameter(
           IDbCommand command,
           IDbDataParameter parameter)
        {
            command.Parameters.Add(parameter);
        }



        /// <summary>
        /// Prepare command with command parameters
        /// </summary>
        /// <param name="commandType"></param>
        /// <param name="commandText"></param>
        /// <param name="commandParameters"></param>
        public virtual void PrepareCommand(ref CommandType commandType
                                            , ref string commandText
                                            , ref IDbDataParameter[] commandParameters)
        {

            if (commandParameters != null)
                foreach (var p in commandParameters)
                {
                    if (p.Value is System.Data.Linq.Binary)
                    {
                        var arr = ((System.Data.Linq.Binary)p.Value).ToArray();

                        p.Value = arr;
                        p.DbType = DbType.Binary;
                        p.Size = arr.Length;
                    }
                }
        }


        /// <summary>
        ///   User Defined Types . By default-abstract - such case is not supported
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="typeName"></param>
        public virtual void SetUserDefinedType(IDbDataParameter parameter, string typeName)
        {
            throw new NotSupportedException(Name + " data provider does not support UDT.");
        }


        /// <summary>
        /// If we can reause command - by default  true.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="newCommandType"></param>
        /// <returns></returns>
        public virtual bool CanReuseCommand(IDbCommand command, CommandType newCommandType)
        {
            return true;
        }

        /// <summary>
        /// Derive command Parameters
        /// </summary>
        /// <remarks>
        /// See the <see cref="DbManager.AddDataProvider(SqlDataProvider)"/> method to find an example.
        /// </remarks>
        /// <seealso cref="DbManager.AddDataProvider(SqlDataProvider)">AddDataManager Method</seealso>
        /// <param name="command">The <see cref="IDbCommand"/> referencing the stored procedure 
        /// for which the parameter information is to be derived.
        /// The derived parameters will be populated into the Parameters of this command.</param>
        /// <returns>true - parameters can be derive.</returns>
        public abstract bool DeriveParameters(IDbCommand command);


        #endregion ---------------- IDbCommand -------------------

        #region ----------- Transaction------------------


        /// <summary>
        /// Transaction Type
        /// </summary>
        protected internal abstract Type TransactionType
        { get; } 

        #endregion ----------- Transaction------------------


        #region ----------------  IDataReader ------------------
        public virtual IDataReader GetDataReader(IDbCommand command)
        {
            return command.ExecuteReader();
        }

        public virtual IDataReader GetDataReader(IDbCommand command, CommandBehavior commandBehavior)
        {
            return command.ExecuteReader(commandBehavior);
        }

        // protected internal abstract DStatementBuilder CreateStatementBuilder();



        #endregion ----------------  IDataReader ------------------


        // ------------------------------
        //
        //   IDataReader DEFAULT VALUE SETTERS : 
        //        static methods - to set TargetType property value  
        //        with ITypeAccessor
        // 
        //-------------------------------

        //Boolean  | Byte    |  Char |  DateTime  
        //Decimal  | Double  | Float |  Guid 
        //Int16    | Int32   | Int64  

        //Bytes-byte[] | chars -char[] | string

        //Boolean?  | Byte?   |  Char? |  DateTime?   
        //Decimal?  | Double? | Float? | Guid? 
        //Int16?    | Int32?  | Int64? 
        internal static void AddMemberSetFunc(TypeDAMap mapper, KeyValuePair<string, TypeMember> property, string providerKey)
        {
            // Boolean  | Byte   |  Char | DateTime
            // Decimal  | Double | Float | Guid 
            // Int16    |  Int32 | Int64 

            // member for provider mapped key : [provider.member] 
            var memberKey = providerKey + "." + property.Key;
            var memberType = property.Value.TypeInf.OriginalType;

            if (memberType == Tps.T_bool)
            { mapper.MemberSetters.Add(memberKey, SetBoolean); return; }
            else if (memberType == Tps.T_byte)
            { mapper.MemberSetters.Add(memberKey, SetByte); return; }
            else if (memberType == Tps.T_char)
            { mapper.MemberSetters.Add(memberKey, SetChar); return; }
            else if (memberType == Tps.T_DateTime)
            { mapper.MemberSetters.Add(memberKey, SetDateTime); return; }
            else if (memberType == Tps.T_decimal)
            { mapper.MemberSetters.Add(memberKey, SetDecimal); return; }
            else if (memberType == Tps.T_Double)
            { mapper.MemberSetters.Add(memberKey, SetDouble); return; }
            else if (memberType == Tps.T_float)
            { mapper.MemberSetters.Add(memberKey, SetFloat); return; }
            else if (memberType == Tps.T_Guid)
            { mapper.MemberSetters.Add(memberKey, SetGuid); return; }
            else if (memberType == Tps.T_Int16)
            { mapper.MemberSetters.Add(memberKey, SetInt16); return; }
            else if (memberType == Tps.T_Int32)
            { mapper.MemberSetters.Add(memberKey, SetInt32); return; }
            else if (memberType == Tps.T_Int64)
            { mapper.MemberSetters.Add(memberKey, SetInt64); return; }

            // - always NULLABLE ARRAYS: Bytes | Chars | String
            else if (memberType == Tps.T_charArray)
            { mapper.MemberSetters.Add(memberKey, SetChars); return; }
            else if (memberType == Tps.T_byteArray)
            { mapper.MemberSetters.Add(memberKey, SetBytes); return; }
            else if (memberType == Tps.T_string)
            { mapper.MemberSetters.Add(memberKey, SetString); return; }


            // ------ NULLABLE SETTERS
            //Boolean?  | Byte?   |  Char? |  DateTime?   
            //Decimal?  | Double? | Float? | Guid? 
            //Int16?    | Int32?  | Int64? 
            if (memberType == Tps.T_boolNul)
            { mapper.MemberSetters.Add(memberKey, SetBoolean_Null); return; }
            else if (memberType == Tps.T_byteNul)
            { mapper.MemberSetters.Add(memberKey, SetByte_Null); return; }
            else if (memberType == Tps.T_charNul)
            { mapper.MemberSetters.Add(memberKey, SetChar_Null); return; }
            else if (memberType == Tps.T_DateTimeNul)
            { mapper.MemberSetters.Add(memberKey, SetDateTime_Null); return; }
            else if (memberType == Tps.T_decimalNul)
            { mapper.MemberSetters.Add(memberKey, SetDecimal_Null); return; }
            else if (memberType == Tps.T_DoubleNul)
            { mapper.MemberSetters.Add(memberKey, SetDouble_Null); return; }
            else if (memberType == Tps.T_floatNul)
            { mapper.MemberSetters.Add(memberKey, SetFloat_Null); return; }
            else if (memberType == Tps.T_GuidNul)
            { mapper.MemberSetters.Add(memberKey, SetGuid_Null); return; }
            else if (memberType == Tps.T_Int16Nul)
            { mapper.MemberSetters.Add(memberKey, SetInt16_Null); return; }
            else if (memberType == Tps.T_Int32Nul)
            { mapper.MemberSetters.Add(memberKey, SetInt32_Null); return; }
            else if (memberType == Tps.T_Int64Nul)
            { mapper.MemberSetters.Add(memberKey, SetInt64_Null); return; }

        }

        // ------------------------------------------------------
        // ----------- Standart IdataReader Types Mapping -SET FUNCS ----------------------
        // ------------------------------------------------------

        #region -------------  IDataReader VALUE SETTERS with  ITypeAccessor --------------------

        //Boolean  | Byte    |  Char |  DateTime  
        //Decimal  | Double  | Float |  Guid 
        //Int16    | Int32   | Int64  

        internal static void SetBoolean(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Boolean>(member, ref targetItem, reader.GetBoolean(ordinal));
        }
        internal static void SetByte(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<byte>(member, ref targetItem, reader.GetByte(ordinal));
        }
        internal static void SetChar(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<char>(member, ref targetItem, reader.GetChar(ordinal));
        }
        internal static void SetDateTime(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<DateTime>(member, ref targetItem, reader.GetDateTime(ordinal));
        }
        internal static void SetDecimal(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Decimal>(member, ref targetItem, reader.GetDecimal(ordinal));
        }
        internal static void SetDouble(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Double>(member, ref targetItem, reader.GetDouble(ordinal));
        }
        internal static void SetFloat(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<float>(member, ref targetItem, reader.GetFloat(ordinal));
        }
        internal static void SetGuid(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Guid>(member, ref targetItem, reader.GetGuid(ordinal));
        }
        internal static void SetInt16(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Int16>(member, ref targetItem, reader.GetInt16(ordinal));
        }
        internal static void SetInt32(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Int32>(member, ref targetItem, reader.GetInt32(ordinal));
        }
        internal static void SetInt64(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            accessor.SetMember<Int64>(member, ref targetItem, reader.GetInt64(ordinal));
        }


        #endregion -------------  IDataReader VALUE SETTERS with ITypeAccessor--------------------

        #region -------- IDataReader  VALUE SETTERS for  Char/Byte Arrays/string -always nullable  with  ITypeAccessor-----------

        internal static void SetBytes(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            if (reader.IsDBNull(ordinal) == false)
            {
                long size = reader.GetBytes(ordinal, 0, null, 0, 0);  //get the length of data
                byte[] barrayValue = new byte[size];

                int bufferSize = 1024;
                long bytesRead = 0;
                int curPos = 0;

                while (bytesRead < size)
                {
                    bytesRead += reader.GetBytes(ordinal, curPos, barrayValue, curPos, bufferSize);
                    curPos += bufferSize;
                }

                accessor.SetMember<byte[]>(member, ref targetItem, barrayValue);
            }
        }
        internal static void SetChars(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {   //accessor.SetMember<char[]>(member, ref targetItem, reader.GetChars(ordinal));

        }
        internal static void SetString(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {            
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (string)value;
            accessor.SetMember<string>(member, ref targetItem, nullVal);
        }

        #endregion  -------- IDataReader  VALUE SETTERS for  Char/Byte Arrays/string -always nullable  with  ITypeAccessor -----------

        #region -------------IDataReader  Nullalble  VALUE SETTERS with ITypeAccessor --------------------
        //Boolean?  | Byte?   |  Char? |  DateTime?   
        //Decimal?  | Double? | Float? | Guid? 
        //Int16?    | Int32?  | Int64? 

        internal static void SetBoolean_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Boolean?)value;
            accessor.SetMember<Boolean?>(member, ref targetItem, nullVal);
        }

        internal static void SetByte_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Byte?)value;
            accessor.SetMember<Byte?>(member, ref targetItem, nullVal);
        }

        internal static void SetChar_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (char?)value;
            accessor.SetMember<char?>(member, ref targetItem, nullVal);
        }

        internal static void SetDateTime_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (DateTime?)value;
            accessor.SetMember<DateTime?>(member, ref targetItem, nullVal);
        }

        internal static void SetDecimal_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Decimal?)value;
            accessor.SetMember<Decimal?>(member, ref targetItem, nullVal);
        }
        internal static void SetDouble_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Double?)value;
            accessor.SetMember<Double?>(member, ref targetItem, nullVal);
        }
        internal static void SetFloat_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (float?)value;
            accessor.SetMember<float?>(member, ref targetItem, nullVal);
        }
        internal static void SetGuid_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Guid?)value;
            accessor.SetMember<Guid?>(member, ref targetItem, nullVal);
        }

        internal static void SetInt16_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int16?)value;
            accessor.SetMember<Int16?>(member, ref targetItem, nullVal);
        }
        internal static void SetInt32_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int32?)value;
            accessor.SetMember<Int32?>(member, ref targetItem, nullVal);
        }
        internal static void SetInt64_Null(ITypeAccessor accessor, object targetItem, string member, int ordinal, IDataReader reader)
        {
            var value = reader.GetValue(ordinal);
            var nullVal = value is DBNull ? null : (Int64?)value;
            accessor.SetMember<Int64?>(member, ref targetItem, nullVal);
        }

        #endregion   -------------IDataReader  Nullalble  VALUE SETTERS with ITypeAccessor --------------------






        // ------------------------------------------------------
        // ----------- Specific Provider Types Mapping ----------------------
        // ------------------------------------------------------
        #region ---------------- SPECIFIC Provider Type MAPPING ---------------


        /// <summary>
        /// Specific Data Provider Types 
        /// </summary>
        protected abstract List<Type> SpecificTypes
        { get; }

        /// <summary>
        /// Check if some Member Type (checkingType) is specific DataProvider Type
        /// </summary>
        /// <param name="checkingType"></param>
        /// <returns></returns>
        protected internal bool IsSpecificType(Type checkingType)
        {
            return SpecificTypes.Contains(checkingType);
        }

        /// <summary>
        /// Get  Set Func from DataPropvider for its specific Type
        /// </summary>
        /// <param name="property"></param>
        /// <param name="proptyType"></param>
        /// <returns></returns>
        protected internal abstract void AddSetMemberFunc(Dictionary<string, DASetMemberValueDelegate> map, string property, Type proptyType);



        #endregion ---------------- SPECIFIC Provider Type MAPPING ---------------







    }
}