using System;
using System.Runtime.Serialization;

namespace SevenZSharp.Exceptions
{
   /// <summary>
   /// 
   /// </summary>
   [Serializable]
   public class CompressionException : Exception
   {
      /// <summary>
      /// 
      /// </summary>
      public CompressionException()
      {
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="message"></param>
      public CompressionException(string message) : base(message)
      {
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="message"></param>
      /// <param name="inner"></param>
      public CompressionException(string message, Exception inner) : base(message, inner)
      {
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="info"></param>
      /// <param name="context"></param>
      protected CompressionException(
         SerializationInfo info,
         StreamingContext context)
         : base(info, context)
      {
      }
   }
}