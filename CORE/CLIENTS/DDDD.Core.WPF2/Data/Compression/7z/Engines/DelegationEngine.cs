using System.Collections.Generic;
using SevenZSharp.Decoders;
using SevenZSharp.Encoders;

namespace SevenZSharp.Engines
{
   /// <summary>
   /// 
   /// </summary>
   public class DelegationEngine : CompressionEngine
   {
      /// <summary>
      /// 
      /// </summary>
      public DelegationEngine()
      {
         Initialize();
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="engines"></param>
      public DelegationEngine(Dictionary<CompressionFormat, ICompressionEngine> engines)
      {
         Initialize(engines);
      }

      /// <summary>
      /// 
      /// </summary>
      protected override void Initialize()
      {
         Dictionary<CompressionFormat, ICompressionEngine> engines =
            new Dictionary<CompressionFormat, ICompressionEngine>();

         engines[CompressionFormat.Unknown] = new ShellEngine();
         engines[CompressionFormat.SevenZ] = new LzmaEngine();

         Initialize(engines);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="engines"></param>
      protected virtual void Initialize(Dictionary<CompressionFormat, ICompressionEngine> engines)
      {
         Encoder = new DelegationEncoder(engines);
         Decoder = new DelegationDecoder(engines);
      }
   }
}