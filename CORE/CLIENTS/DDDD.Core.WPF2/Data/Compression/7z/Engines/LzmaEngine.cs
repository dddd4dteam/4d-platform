using System;
using SevenZip;
using SevenZSharp.Decoders;
using SevenZSharp.Encoders;

namespace SevenZSharp.Engines
{
   /// <summary>
   /// 
   /// </summary>
   public class LzmaEngine : ShellEngine
   {
      /// <summary>
      /// 
      /// </summary>
      protected override void Initialize()
      {
         base.Initialize();
         Encoder = new LzmaEncoder(this);
         Decoder = new LzmaDecoder(this);
      }

      /// <summary>
      /// Prepares the encoder.
      /// </summary>
      /// <param name="propIDs">The prop I ds.</param>
      /// <param name="properties">The properties.</param>
      public static void PrepareEncoder(out CoderPropID[] propIDs, out object[] properties)
      {
         bool eos = false;
         Int32 dictionary = 1 << 21;
         Int32 posStateBits = 2;
         Int32 litContextBits = 3; // for normal files
         // UInt32 litContextBits = 0; // for 32-bit data
         Int32 litPosBits = 0;
         // UInt32 litPosBits = 2; // for 32-bit data
         Int32 algorithm = 2;
         Int32 numFastBytes = 128;
         string mf = "bt4";

         propIDs = new CoderPropID[]
            {
               CoderPropID.DictionarySize,
               CoderPropID.PosStateBits,
               CoderPropID.LitContextBits,
               CoderPropID.LitPosBits,
               CoderPropID.Algorithm,
               CoderPropID.NumFastBytes,
               CoderPropID.MatchFinder,
               CoderPropID.EndMarker
            };
         properties = new object[]
            {
               dictionary,
               posStateBits,
               litContextBits,
               litPosBits,
               algorithm,
               numFastBytes,
               mf,
               eos
            };
      }
   }
}