﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SevenZSharp.CommandLine
{
    public class ArchiveTypes
    {
        internal static string GetArchiveType(CompressionFormat format)
        {

            switch (format)
            {
                case CompressionFormat.Unknown:
                    break;
                case CompressionFormat.SevenZ:
                    break;
                case CompressionFormat.Zip:
                    break;
                case CompressionFormat.Cab:
                    break;
                case CompressionFormat.Rar:
                    break;
                case CompressionFormat.Arj:
                    break;
                case CompressionFormat.Lzh:
                    break;
                case CompressionFormat.Chm:
                    break;
                case CompressionFormat.Gzip:
                    break;
                case CompressionFormat.Bzip2:
                    break;
                case CompressionFormat.Z:
                    break;
                case CompressionFormat.Tar:
                    break;
                case CompressionFormat.Cpio:
                    break;
                case CompressionFormat.Rpm:
                    break;
                case CompressionFormat.Deb:
                    break;
                case CompressionFormat.Split:
                    break;
                case CompressionFormat.Iso:
                    break;
                case CompressionFormat.Nsis:
                    break;
                default:
                    break;
            }
            throw new NotImplementedException();
        }
    }
}
