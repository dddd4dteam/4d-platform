Based on the 7z LZMA SDK at http://www.7-zip.org/sdk.html

Revision History:

Version 1.0.3:
 * Added LzmaEngine to DelegationEngine
 * Made sure source code installed with 7zsharp.dll
Version 1.0.2:
 * BUG http://www.codeplex.com/7zsharp/WorkItem/View.aspx?WorkItemId=1008
Version 1.0.0:
 * Based on SDK version 4.43 (2006-06-04)
