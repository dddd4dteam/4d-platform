﻿using DDDD.Core.Stores.Replication;
using System.Collections.Generic;

namespace DDDD.Core.Stores.Model
{
    public interface IContentElement : IHierarchyElement
    {
        string Name { get; set; }

        string Description { get; set; }

        bool IsCategory { get; set; }

        Dictionary<int, NodeReplicaInfo> NodeReplics { get; }

        bool IsContainsReplicaWithAddress(int replicaIndex, AccessMethodEn accessMethod);

        string GetReplicaAdress(int replicaIndex, AccessMethodEn accessmethod);

        
    }
}