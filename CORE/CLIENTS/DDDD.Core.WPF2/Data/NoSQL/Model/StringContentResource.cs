﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Stores
{
    /// <summary>
    /// String Content Resource.
    /// </summary>
    public class StringContentResource
    {
        // Here we use  List<KeyValuePair<string, string>> - and not use Dictionary<string, string> to add support of browsers serialization - [JSON.parse()].

        public StringContentResource()
        {
            CultureValues = new List<KeyValuePair<string, string>>(); 
        }


        /// <summary>
        /// Key of some string value
        /// </summary>
        public string Key
        { get; set; }


        /// <summary>
        /// Culture-Value collection.
        /// </summary>
        public List<KeyValuePair<string, string>> CultureValues
        { get; set; } 

    }


}
