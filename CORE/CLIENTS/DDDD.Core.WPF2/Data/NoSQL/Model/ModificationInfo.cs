﻿using System;

namespace DDDD.Core.Stores
{
    /// <summary>
    ///  Node modification simple info.
    /// </summary>
    public class ModificationInfo
    {
        /// <summary>
        /// User Name - Author whom create this category
        /// </summary>
        public string Author
        { get; set; }

        /// <summary>
        /// Created Date Time
        /// </summary>
        public DateTime CreatedDateTime
        { get; set; }

        /// <summary>
        /// Last modification Date Time
        /// </summary>
        public DateTime LastUpdatedDateTime
        { get; set; }

        /// <summary>
        /// User Name -  whom create last modification
        /// </summary>
        public string LastModifier
        { get; set; }
    }

}
