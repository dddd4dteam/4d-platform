﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Extensions;


namespace DDDD.Core.Stores.Replication
{

   



    public class NodeReplicaInfo  
    {
         

        public string HostUser
        { get; set; }


        public string NodePC
        { get; set; }

          
        const string winDelim = @"\";
         
         

        #region ---------------------------- LOCAL ADDRESS ------------------------


        public string NodeStoreLocalAddress
        { get; set; }


        public bool ContainsLocalAddress
        {
            get
            {
                return NodeStoreLocalAddress.IsNotNullOrEmpty();
            }
        }


        #endregion  ---------------------------- LOCAL ADDRESS ------------------------




        #region ---------------------------- NETWORK ACCESS ------------------------


        public string NodeStoreNetworkAddress
        { get; set; }


        public bool ContainsNetworkAddress
        {
            get
            {
                return NodeStoreNetworkAddress.IsNotNullOrEmpty();
            }
        }


        #endregion ---------------------------- NETWORK ACCESS ------------------------

        #region ---------------------------- HTTP ADDRESS ------------------------


        public string NodeStoreHttpAddress
        { get; set; }


        public bool ContainsHttpAddress
        {
            get
            {
                return NodeStoreHttpAddress.IsNotNullOrEmpty();
            }
        }


        #endregion  ---------------------------- HTTP ADDRESS ------------------------





    }


}
