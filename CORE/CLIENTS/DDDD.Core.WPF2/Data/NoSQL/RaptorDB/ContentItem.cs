﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using ServiceStack.Text;

using DDDD.Core.Collections;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Threading;
using DDDD.Core.Data.MIME;
using DDDD.Core.Configuration;

namespace DDDD.Core.Stores.Model
{





    /// <summary>
    /// ContentItem - is Container for some hostEntity and also it works with Files storing.
    /// </summary>
    public class ContentItem //: NodeM<long>, IEntityAssociated<TEntity>
    {


        #region ---------------------------- CONSTS ------------------------------------


        /// <summary>
        /// Category  prefix in folder name
        /// </summary>
        public const string CategoryPref = "CG";

        /// <summary>
        /// Content Item prefix in folder name
        /// </summary>
        public const string ContentPref = "CNT";


        public const string CategoryDefJson = "category";
        public const string ContentDefJson = "content";
        

        public const string NodeIconImage = "icon.png";


        public const string Pages = nameof(Pages);

        #endregion ---------------------------- CONSTS ------------------------------------


        #region --------------------------- CTOR / INIT ----------------------------------

        public ContentItem( IContentElement hostEntity )
        {
            //Validator.ATNullOrEmptyArgDbg(hostNodePC, nameof(ContentItem), nameof(ContentItem), nameof(hostNodePC));
            //Validator.ATNullOrEmptyArgDbg(hostUser, nameof(ContentItem), nameof(ContentItem), nameof(hostUser));
            //Validator.ATNullOrEmptyArgDbg(rootRepositoryPath, nameof(ContentItem), nameof(ContentItem), nameof(rootRepositoryPath));
            //Validator.ATNullOrEmptyArgDbg(nodeRelativePath, nameof(ContentItem), nameof(ContentItem), nameof(nodeRelativePath));
              
            HostElement = hostEntity;
            //HostNodePC = hostNodePC;
            //HostUser = hostUser;

            //RootRepositoryPath = rootRepositoryPath;
            //NodeRelativePath = nodeRelativePath;
            NodePhysicalPath = RootRepositoryPath + NodeRelativePath;

        }




        /// <summary>
        /// Deferred from Create Initing function. 
        /// </summary>
        public   void Init()
        {
            
            var folderName =  Path.GetDirectoryName(NodePhysicalPath);// segments[segments.Count - 1];

         
            // Folder Name
             FolderName = folderName; //if (!IsRootNode)

            //FolderNameIndex
            if ( FolderName.IsNotNull() )
            {
                if (HostElement.IsCategory)
                {
                    FolderNameIndex = int.Parse(FolderName.Replace(CategoryPref, ""));
                }
                else if (HostElement.IsCategory == false) //Content Node
                {
                    FolderNameIndex = int.Parse(FolderName.Replace( ContentPref, "") );
                }                 
            } 
            
            //NodeRelativePath            
            //NodeRelativePath = PhysicalPath.Replace((HostTree.Root as ContentItem).PhysicalPath, "");
            //if (!IsRootNode)     { }

            ReloadMetaFiles();

            //LA_PagesCount = LazyAct<int>.Create(LoadPagesCount, null, new object[] { this } //this node as arg 
            // , false, false, true);

            //LA_SubNodesCount = LazyAct<int>.Create(LoadSubNodesCount, null, new object[] { this } //this node as arg 
            //  , false, false, true);

           
            LA_NodeDataFiles = LazyAct<List<FileInfo2>>.Create(LoadNodeDataFiles, null, new object[] { this } //this node as arg 
            , false, false, true); 

        }


        #endregion --------------------------- CTOR / INIT ----------------------------------
        

        #region ------------------------ HOST ENTITY -----------------------------

        /// <summary>
        /// Host Entity of this Content Node. It means that this Entity is primarily, and we'll craete this Node only for it.
        /// </summary>
        public IContentElement HostElement { get; set; }

        #endregion ------------------------ HOST ENTITY -----------------------------


        #region ------------------CACHING - Item LOAD GENERATION ---------------------------

        /// <summary>
        /// Loaded into Tree generation for this Node. iT can be used to clear cached/loaded into memory nodes.
        /// </summary>
        public int NodeLoadGeneration
        {
            get; internal set;
        }


        #endregion ------------------CACHING - Item LOAD GENERATION ---------------------------
         


        #region ----------------------------- FILE SYSTEM PRIMARY INFO STORING MODEL ----------------------------------
        /// <summary>
        ///  PC Host Key of this Node 
        /// </summary>
        public string HostNodePC
        { get; internal set; }


        /// <summary>
        /// Host User  who seems like administrator of  this node.
        /// </summary>
        public string HostUser
        { get; internal set; }


        /// <summary>
        /// 
        /// </summary>
        public string RootRepositoryPath
        { get; internal set; }

        /// <summary>
        /// Node Full Physical Path  - [HosterRoot]\[TreePath]
        /// </summary>
        public string NodePhysicalPath
        { get; internal set; }

        /// <summary>
        /// NodeRelativePath - relative to Root Path - [TreePath] only
        /// </summary>
        public string NodeRelativePath
        { get; internal set; }


        ///// <summary>
        ///// Node Type - Category or Content Node node
        ///// </summary>
        //public NodeTypeEn NodeType
        //{ get; private set; }



        /// <summary>
        /// Folder Name
        /// </summary>
        public string FolderName
        { get; private set; }


        /// <summary>
        /// Index in Folder Name - CG_[index], or CNT_[index]. Root Node  has only  -1 value.
        /// </summary>
        public int FolderNameIndex
        { get; private set; }



        #endregion ----------------------------- FILE SYSTEM PRIMARY INFO STORING MODEL ----------------------------------



        #region ---------------------- PAGES STORING INFO ---------------------------



        /// <summary>
        /// Physical Parent Page Index. Each Node also storing by pages subdirs of parentNode. 
        /// In Parent Node we create P[Index-this Property value] and only inside this folder creating folder of new Node.
        /// </summary>
        public int ParentPageIndex
        { get; private set; }


        /// <summary>
        /// How many Page dirs with subnodes this Node contains.
        /// </summary>
        public Dictionary<string, int> PageCountRegister
        { get; private set; } = new Dictionary<string, int>();
          


        private static Dictionary<string, int> UpdatePagesForSavingItem(ContentItem  targetNode, int expectingItemsCount )
        {
         
            var pageDirs = Directory.GetDirectories(targetNode.NodePhysicalPath);
            pageDirs = pageDirs.Where(dir => dir.ToLower().StartsWith("p")).ToArray();

            //1 - do not contains pages
            //2 - contains pages and Contain Place To Saveitem
            //3 - contains pages but not Contain Place in all of them? then create new page  to SaveItem
            
            if (pageDirs.IsNull() || pageDirs.Length == 0 )
            {
                //create Page(s) for expectingItemsConnt
                CreatePagesForItemsInCout(targetNode, expectingItemsCount);
                 
                for (int i = 0; i < pageDirs.Length;i++)
                {                    
                    var pageNodesDirs = Directory.GetDirectories(targetNode.NodePhysicalPath + pageDirs[i]);
                    targetNode.PageCountRegister.Add(pageDirs[i], pageNodesDirs.Length);
                }
            }

            return targetNode.PageCountRegister;
        }

        private static void CreatePagesForItemsInCout(ContentItem targetNode, int expectingItemsCount)
        {
           // var newPagesCount = Math. expectingItemsCount
           
        }

        internal object GetNewChildNodePhysicalPath()
        {
            throw new NotImplementedException();
        }

        public void ReloadPagesCount()
        {
            //var tempVal = LA_PagesCount.Value;
        }


        #endregion  ---------------------- PAGES STORING INFO ---------------------------
         


        #region -------------------------  META FILES  ---------------------------

        /// <summary>
        /// Json content and other settings of current ContentItem. This json files will never be showned to user.
        /// </summary>
        internal List<FileInfo2> MetaFiles
        { get; private set; } = new List<FileInfo2>();



        /// <summary>
        /// Load all json files and their json code for current Node.
        /// </summary>
        public void ReloadMetaFiles()
        {
            MetaFiles.Clear();
            var tatgetDir = Path.Combine(NodePhysicalPath, "text");
            if (!Directory.Exists( tatgetDir )  )
                throw new DirectoryNotFoundException(@" [ContentTree.Root.Data.PhysicalPath\json] directory not found : [{0}]".Fmt(tatgetDir));

            var jsonfileNames = Directory.GetFiles(tatgetDir);
            foreach (var jsonFile in jsonfileNames)
            {
                if (jsonFile.EndsWith(".json"))
                {
                    var jsonFile2 = new FileInfo2(jsonFile);
                    MetaFiles.Add(jsonFile2);
                }
            }
        }




        #endregion -------------------------  META FILES  --------------------------- 
        
         

        #region ------------------------  DataFiles --------------------------------

        internal List<FileInfo2>  DataFiles
        { get; private set; } = new List<FileInfo2>();


        /// <summary>
        /// Node Content Files -  images/audio/video/documents   ...articles/screenshots...
        /// </summary>
        internal LazyAct<List<FileInfo2>> LA_NodeDataFiles
        { get; private set; }


        /// <summary>
        /// Load Node DataFiles from existed dataDirectories- images/audio/video/document
        /// </summary>
        static List<FileInfo2> LoadNodeDataFiles(params object[] args)
        {
            var targetNode = args[0] as ContentItem;
            var dataDirs = MimeTypeMap.MimeTypeCategories; 
           
            targetNode.DataFiles.Clear();

            foreach (var fileDataTypeName in dataDirs)
            {
                if (Directory.Exists(targetNode.NodePhysicalPath + fileDataTypeName))
                {
                    var dataFiles = Directory.GetFiles(targetNode.NodePhysicalPath + fileDataTypeName);
                    foreach (var dataFile in dataFiles)
                    {
                        targetNode.DataFiles.Add(new FileInfo2(dataFile));
                    }
                }
            }
            return targetNode.DataFiles;
        }

        public void ReloadNodeDataFiles()
        {
            var tempVal = LA_NodeDataFiles.Value;
        }



        /// <summary>
        /// Get Node Files by  MimeCategory.
        /// </summary>
        /// <param name="fileDataType"></param>
        /// <param name="reloadfilesBefore"></param>
        /// <returns></returns>
        public List<FileInfo2> GetFilesByMimeCategory(string mimeTypeCategory, bool reloadfilesBefore = false)
        {
            var reslt = new List<FileInfo2>();
            if (reloadfilesBefore) ReloadNodeDataFiles();
            
            reslt =  DataFiles.Where(fl => fl.MimeTypeCategory == mimeTypeCategory).ToList();
            return reslt;
        }

        #endregion ------------------------ NodeDataFiles --------------------------------


    }

}



#region ----------------------------- GARBAGE ------------------------------------



///// <summary>
///// Entity is primarily instance, and we'll associate  some  other instance that implements this interface with it(entity instance).
///// </summary>
///// <typeparam name="TEntity"></typeparam>
//internal interface IEntityAssociated<TEntity>
//{
//    /// <summary>
//    /// Host Entity  means that this Entity is primarily instance,  and we'll associate  some  other instance that implements this interface with it(entity instance).
//    /// </summary>
//    TEntity HostEntity { get; set; }
//}


#region ----------------------- NODE DEFINITION -------------------------

///// <summary>
///// Content Node Definition. Json transmittable definition.
///// </summary>
//public ContentNodeDefinition Definition
//{ get; private set; }


///// <summary>
///// Loading Node Definition - from category.json or content.json and Update Node ID.
///// </summary>
//private void ReloadNodeDefinition()
//{
//    //for category root
//    if (NodeType == NodeTypeEn.Category)
//    {
//        //check definition exist then read it/else create new and write as category.json
//        var contDefJsonFile = MetaFiles.FirstOrDefault(fl => fl.Name == CategoryDefJson);
//        if (contDefJsonFile.IsNotNull())
//        {
//            try
//            {
//                // load definition from file
//                Definition = JsonSerializer.DeserializeFromString<ContentNodeDefinition>(contDefJsonFile.LoadJsonText());
//            }
//            catch (Exception serEx)
//            {
//                //create new and write new node Definition. 
//                Definition = CreateNodeCategoryDefinition();
//            }
//        }
//        else
//        {
//            //create new and write new node Definition. 
//            Definition = CreateNodeCategoryDefinition();
//        }
//    }
//    else if (NodeType == NodeTypeEn.Content)
//    {
//        //check definition exist then read it/else create new and write as content.json
//        var contDefJsonFile = MetaFiles.FirstOrDefault(fl => fl.Name == ContentDefJson);
//        if (contDefJsonFile.IsNotNull())
//        {
//            try
//            {
//                // load definition from file
//                Definition = JsonSerializer.DeserializeFromString<ContentNodeDefinition>(contDefJsonFile.LoadJsonText());
//            }
//            catch (Exception serEx)
//            {
//                //create new and write new node Definition. 
//                Definition = CreateNodeContentDefinition();
//            }
//        }
//        else
//        {
//            //create new and write new node Definition. 
//            Definition = CreateNodeContentDefinition();
//        }
//    }
//    //updating ID and IDCounter from Definition readed/created info
//    ID = Definition.ID_ContentNode; HostTree.IDCounter.UpdateLastID(ID);
//}



//ContentNodeDefinition CreateNodeCategoryDefinition()
//{
//    var newCategoryDefinition = new ContentNodeDefinition();

//    newCategoryDefinition.ID_ContentNode =  HostTree.IDCounter.GetNext();
//    newCategoryDefinition.NodeIconImage = NodeIconImage;// "icon.png";

//    if (Parent.IsNotNull())
//    {
//        newCategoryDefinition.ID_ParentContentNode =  Parent.ID;
//    }
//    else newCategoryDefinition.ID_ParentContentNode = -1;

//    //Title        //Description     //DisplayName
//    newCategoryDefinition.AddUpdateContentString("Title", "Ru", "Value");
//    newCategoryDefinition.AddUpdateContentString("Description", "Ru", "Value");
//    newCategoryDefinition.AddUpdateContentString("DisplayName", "Ru", "Value");

//    //save to node dir
//    File.WriteAllText(PhysicalPath + CategoryDefJson + ".json", JsonSerializer.SerializeToString(newCategoryDefinition));

//    return newCategoryDefinition;
//}

//ContentNodeDefinition CreateNodeContentDefinition()
//{
//    var newContentDefinition = new ContentNodeDefinition();

//    newContentDefinition.ID_ContentNode =HostTree.IDCounter.GetNext();
//    newContentDefinition.NodeIconImage = NodeIconImage;// "icon.png";

//    if (Parent.IsNotNull())
//    {
//        newContentDefinition.ID_ParentContentNode = Parent.ID;
//    }
//    else newContentDefinition.ID_ParentContentNode = -1;

//    //Title        //Description     //DisplayName
//    newContentDefinition.AddUpdateContentString("Title", "Ru", "Value");
//    newContentDefinition.AddUpdateContentString("Description", "Ru", "Value");
//    newContentDefinition.AddUpdateContentString("DisplayName", "Ru", "Value");

//    //save to node dir
//    File.WriteAllText(PhysicalPath + ContentDefJson + ".json", JsonSerializer.SerializeToString(newContentDefinition));

//    return newContentDefinition;
//}

///// <summary>
///// Reload ContentNodeDefinition from  js file.
///// </summary>
//public void ReloadContentNodeDefinition()
//{
//    try
//    {
//        if (NodeType == NodeTypeEn.Category)
//        {
//            var categDefJsonFile = MetaFiles.FirstOrDefault(fl => fl.Name == CategoryDefJson);
//            // load definition from file
//            Definition = JsonSerializer.DeserializeFromString<ContentNodeDefinition>(categDefJsonFile.LoadJsonText());                  
//        }
//        else if (NodeType == NodeTypeEn.Content)
//        {
//            var contDefJsonFile = MetaFiles.FirstOrDefault(fl => fl.Name == ContentDefJson); 
//                // load definition from file
//            Definition = JsonSerializer.DeserializeFromString<ContentNodeDefinition>(contDefJsonFile.LoadJsonText());                   
//        }
//    }
//    catch (Exception serEx)
//    {
//        //create new and write new node Definition. 
//        Definition = default(ContentNodeDefinition);
//    }

//}
#endregion ----------------------------- NODE DEFINITION -------------------------------------





//static Dictionary<string, int>  LoadPagesCount(params object[] args)
//{
//    var reslt = new Dictionary<string, int>();
//    var targetNode = args[0] as ContentNode<TEntity>;
//    if (targetNode.NodeType == NodeTypeEn.Content) return reslt;

//    //reslt = UpdatePagesForSavingItems(targetNode );             

//    return reslt;
//}



//#region ------------------------ SUB NODES STORING INFO -----------------------------

///// <summary>
///// Count of subnodes that this node contains.
///// </summary>
//public LazyAct<int> LA_SubNodesCount
//{ get; private set; }


//static int LoadSubNodesCount(params object[] args)
//{
//    var targetNode = args[0] as ContentNode<TEntity>;
//    if (targetNode.NodeType == NodeTypeEn.Content) return 0;



//    var pageDirs = targetNode.LA_PagesCount.Value;  //Directory.GetDirectories(targetNode.PhysicalPath);



//    pageDirs = pageDirs.Where(dir => dir.ToLower().StartsWith("p")).ToArray();

//    return pageDirs.Length;

//    return 0;
//}

//public void ReloadSubNodesCount()
//{
//    var tempVal = LA_SubNodesCount.Value;
//}

//#endregion ------------------------ SUB NODES STORING INFO -----------------------------




//var pagesJson = targetNode.JsonFiles.FirstOrDefault(fl => fl.Name == Pages);
//            if (pagesJson.IsNotNull())
//            {                
//                try
//                {
//                    // load definition from file
//                    reslt = JsonSerializer.DeserializeFromString<Dictionary<string, int>>(pagesJson.LoadJsonText());
//                }
//                catch (Exception serEx)
//                {
//                    //create new and write new node Definition. 
//                    //Definition = CreateNodeCategoryDefinition();
//                }


//            }


#region ---------------------------------- SUBNODESDIRS -------------------------------------
// get first time safety, and safe value to SubNodeDirs - by simply calling LA_SubNodesDirs.Value,

//internal List<DirectoryInfo2> SubNodeDirs
//{ get; set; } = new List<DirectoryInfo2>();

//LA_SubNodesDirs = LazyAct<List<DirectoryInfo2>>.Create( LoadSubNodesDirs, null, new object[] { this } //this node as arg 
// , false, false, true );

///// <summary>
///// Content semantic Directories - 1-CG_Dirs/, 2 CNT_Dirs/ , 3 DataFileDirs-  images/audio/video/documents/articles/screenshots.
///// </summary>
//internal LazyAct<List<DirectoryInfo2>> LA_SubNodesDirs
//{ get; private set; }


///// <summary>
///// Load/reload all sub dirs for current Node- sub nodes dirs
///// </summary>
///// <param name="args"></param>
///// <returns></returns>
//static List<DirectoryInfo2> LoadSubNodesDirs(object[] args)
//{
//    var targetNode = args[0] as ContentNode;
//    var alldirs = Directory.GetDirectories(targetNode.PhysicalPath);

//    targetNode.SubNodeDirs.Clear();

//    for (int i = 0; i < alldirs.Length; i++)
//    {
//        var newDir = new DirectoryInfo2(alldirs[i], (targetNode.HostTree.Root as ContentNode).PhysicalPath);
//        targetNode.SubNodeDirs.Add(newDir);
//    }
//    return targetNode.SubNodeDirs;
//}


//public void ReloadSubNOdeDirs()
//{
//    var tempVal = LA_SubNodesDirs.Value;            
//}
#endregion -------------------------------- SUBNODESDIRS -------------------------------------


#endregion ----------------------------- GARBAGE ------------------------------------