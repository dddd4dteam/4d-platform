﻿using DDDD.Core.Stores.Model;
using RaptorDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Stores.RaptorDB
{
    public interface IRaptorDBRepository
    {

        void Insert<TEntity>(TEntity entity) where TEntity : IDocumentEntity;


        void Delete<TEntity>(TEntity entity) where TEntity : IDocumentEntity;


        Result<TViewRowSchema> GetById<TViewRowSchema>(Guid id) where TViewRowSchema : RDBSchema, IDocumentEntity;


        //Result<TViewRowSchema> GetAll<TViewRowSchema>() where TViewRowSchema : IDocumentEntity;
        Result<TViewRowSchema> GetItems<TViewRowSchema>(Expression<Predicate<TViewRowSchema>> predicate,int page, int pageSize);
        
        Result<TViewRowSchema> SearchFor<TViewRowSchema>(Expression<Predicate<TViewRowSchema>> predicate) where TViewRowSchema : RDBSchema, IDocumentEntity;



    }
}
