﻿#if SERVER 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;

using DDDD.Core.Extensions;
using DDDD.Core.Stores.Replication;
using DDDD.Core.Patterns;
using DDDD.Core.Stores.Model;
using DDDD.Core.Data.MIME;
using DDDD.Core.Diagnostics;
using DDDD.Core.Environment;

using ServiceStack.Text;

namespace DDDD.Core.Stores.Files
{
    /// <summary>  
    /// File orderly saving service. 
    /// Based on IContentElement service generate addresses for target files storing, and load files there. Also this files can be easily getted from client.
    /// </summary>
    public class FileService : Singleton<FileService>, IFileService
    {


        #region ----------------------- CTOR ---------------------------

        FileService()
        {
            
        }


        #endregion ----------------------- CTOR ---------------------------



        #region ----------------------  CONFIGURATION ---------------------



        public class Configuration
        {
            /// <summary>
            /// Maximum Elements- Node directories in one Physical Page of some Parent Node.
            /// <para/> For Example -100 means - 100 items - (Sum of CTx and CNx element nodes) only can exist inside one Physical Page Directory. 
            /// </summary>
            public static int MaxItemsCountInPage { get; set; } = 200;
                        
        }

        #endregion ----------------------  CONFIGURATION ---------------------
        


        #region ------------------ SINGLETON ------------------------

        /// <summary>
        /// Singleton instance
        /// </summary>
        public static FileService Current
        { get { return LA_Current.Value; } }

        #endregion ------------------ SINGLETON ------------------------



        /// <summary>
        /// Username of logged in windows user by default.   Windows Logged in User.
        /// </summary>
        public static string AuthenticatedUser
        { get; private set; } = Environment2.UserName;


        /// <summary>
        /// Replication Targets 
        /// </summary>
        public List<ReplicaTarget> ReplicationTargets
        { get; private set; } = new List<ReplicaTarget>();


        public ReplicaTarget GetBestReplica()
        {
            return ReplicationTargets[1];
        }


        internal static string GetAppRootData()
        {
            string execAssemblyDir = Assembly.GetExecutingAssembly().Location;
            var lastDirectoryName = Path.GetDirectoryName(execAssemblyDir).ToLower();
            //if lastname is bin -then go Upper
            if (lastDirectoryName == "bin")
            {
                var targetAppRoot = execAssemblyDir.Replace(@"\bin\", "");
                targetAppRoot = execAssemblyDir.Replace(@"\bin", @"\");
                return targetAppRoot;
            }
            return execAssemblyDir;
        }
        


        #region -------------------- INIT WORK WITH HOST STORE ------------------------

       
        public  IFilesMetaStore HostStoreService
        { get; private set; }

        public  void Init(IFilesMetaStore hostStoreService )
        {
            ReplicationTargets.Add(ReplicaTarget.CreateWithLocalPriority());
            HostStoreService = hostStoreService; 
        }

        #endregion -------------------- INIT WORK WITH HOST STORE ------------------------
         

        #region ------------------ CONTENTELEMENT DIRECTORY BUILDING---------------------------

        /// Node Paths:
        /// NodeLogicalPath   - [NodePC]\[appRootDir]\content\[hostUser]\[nodeRelativePath]  - GetNodeLogicalPath
        /// NodeLocalAddress     - [appRootDir]\content\[hostUser]\[nodeRelativePath]           - GetNodeLocalPath
        /// NodeNetworkAdress - [userStoreNetworkAddress]\[nodeRelativePath]                 - ContainsNetworkAddress GetNodeNetworkPath 






        static  void CreateNewNodeDir(IContentElement targetElement,  int replicaIndex = 1, AccessMethodEn accessMethod = AccessMethodEn.LocalFileSystem)
        { 
            if ( targetElement.IDParent.IsNull())
            {
                //rootdir  
                // determining page dir and create if not exists
                var page = GetTargetPageFolder(null);
                var targetnodeDir = Path.Combine(Current.ReplicationTargets[0].NodeStoreLocalAddress, page);
                if (!Directory.Exists(targetnodeDir)) Directory.CreateDirectory(targetnodeDir);

                // determining  [CT/CN+'Index'] and create dir if not exists
                var newNodeFolder = GetNewNodeFolder(targetElement.IsCategory, targetnodeDir); //
                targetnodeDir = Path.Combine(targetnodeDir, newNodeFolder);
                if (!Directory.Exists(targetnodeDir)) Directory.CreateDirectory(targetnodeDir);

                //updating NodeReplicaInfo   --- !!!
                targetElement.NodeReplics[replicaIndex].NodeStoreLocalAddress = targetnodeDir;
                //UpdateElementAct(targetElement);
                Current.HostStoreService.UpdateElement(targetElement);//.Update();

            }
            else 
            {
                IContentElement parentElement = Current.HostStoreService.GetElement(targetElement.IDParent.Value);
                //parentElement not contains its own DIR - then create it for him 
                if (parentElement.IsContainsReplicaWithAddress(replicaIndex, accessMethod) == false)                     
                {
                    CreateNewNodeDir(parentElement, replicaIndex, accessMethod);
                }

                //updated parentElement   contains its own DIR
                var parentNodeDir = GetParentNodeDir(parentElement, replicaIndex, accessMethod);//updated ParentElement
                                                                                                // determining page dir and create if not exists
                var page = GetTargetPageFolder(parentNodeDir);
                var targetnodeDir = Path.Combine(parentNodeDir, page);
                if (!Directory.Exists(targetnodeDir)) Directory.CreateDirectory(targetnodeDir);

                // determining  [CT/CN+'Index'] and create dir if not exists
                var newNodeFolder = GetNewNodeFolder(targetElement.IsCategory, targetnodeDir); //
                targetnodeDir = Path.Combine(targetnodeDir, newNodeFolder);
                if (!Directory.Exists(targetnodeDir)) Directory.CreateDirectory(targetnodeDir);

                //updating NodeReplicaInfo   --- !!!
                targetElement.NodeReplics[replicaIndex].NodeStoreLocalAddress = targetnodeDir;
                Current.HostStoreService.UpdateElement(targetElement);//
                //UpdateElementAct(targetElement);  

            } 
              
        }
         

        static string GetParentNodeDir(IContentElement parentElement, int replicaIndex = 1, AccessMethodEn accessMethod = AccessMethodEn.LocalFileSystem )
        { 
            if (parentElement.IsNull()) return ""; //no parent dir             
            else 
            {
                // CREATE NODE DIR IF PARENT NOT CONTAINS !!!
                //check replica index
                Validator.ATInvalidOperationDbg(parentElement.NodeReplics.NotContainsKey(replicaIndex),
                    nameof(FileService), nameof(GetParentNodeDir), "Parent Element doesn't contain Replication info with index [{0}] ".Fmt(replicaIndex.S()) );

                //get target replica info
                var replicaInfo = parentElement.NodeReplics[replicaIndex];

                //case  accessMethod - LocalAccess
                switch (accessMethod)
                {
                    case AccessMethodEn.LocalFileSystem:
                        return replicaInfo.NodeStoreLocalAddress; 
                    case AccessMethodEn.NetworkAccess:
                        return replicaInfo.NodeStoreNetworkAddress; 
                    case AccessMethodEn.HttpAccess:
                        return replicaInfo.NodeStoreHttpAddress; 
                    default:
                        return replicaInfo.NodeStoreLocalAddress; 
                }  

            }
            
        }


        static string GetTargetPageFolder(string parentNodeDir)
        {
            //analyze pages :
            //pages not exist-   return first page
            Validator.ATInvalidOperationDbg(!Directory.Exists(parentNodeDir), nameof(FileService), nameof(GetTargetPageFolder), "Parent Node Dir should exists.");

            var existedPages = Directory.GetDirectories(parentNodeDir);//all pages    
             
            if (existedPages.Length == 0 )
            {
                return "P1"; 
            }
            //pages one or more
            else
            {
                //get first not fullfill or craete new
                var pageIndex = 1;
                for (int i = 0; i < existedPages.Length; i++)
                {
                    var pageDir = Path.Combine(parentNodeDir, existedPages[i]);

                    if (Directory.GetDirectories(pageDir).Length < Configuration.MaxItemsCountInPage)
                    {
                        pageIndex = i + 1;
                        return ("P" + pageIndex.S());
                    } 
                }

                //all pages fullfill- create new
                pageIndex = existedPages.Length + 1;
                return ("P" + pageIndex.S());
                 
            } 
             
        }


        static string GetNewNodeFolder(bool isCategoryTarget, string targetPageDir)
        {
            Validator.ATInvalidOperationDbg(!Directory.Exists(targetPageDir), nameof(FileService), nameof(GetNewNodeFolder), "Target Node Page Dir should exists.");


            var existedElements = Directory.GetDirectories(targetPageDir);
                        

            if (isCategoryTarget) //categoryelement - [CT+'Index']
            {
                var existedCategoryNodes = existedElements.Where(dir => dir.StartsWith("CT"));
                if (existedCategoryNodes.Count() == 0) return "CT1";
                else
                {
                    var names = existedCategoryNodes.ToList();
                    names.ForEach(nm => nm.Replace("CT", ""));
                    var namesInt = new List<int>();
                    names.ForEach(nm => namesInt.Add( int.Parse(nm) )  );
                    var maxIndex = namesInt.Max();
                    return "CT" + (maxIndex + 1).S();
                }

            }
            else // contentElement - [CN'Index']
            {
                var existedContentNodes = existedElements.Where(dir => dir.StartsWith("CN"));
                if (existedContentNodes.Count() == 0) return "CN1";
                else
                {
                    var names = existedContentNodes.ToList();
                    names.ForEach(nm => nm.Replace("CN", ""));
                    var namesInt = new List<int>();
                    names.ForEach(nm => namesInt.Add(int.Parse(nm)));
                    var maxIndex = namesInt.Max();
                    return "CN" + (maxIndex + 1).S();
                }
            } 
        }
        

        public static string GetFileFolder(string fileName)
        {
            return  MimeTypeMap.GetMimeTypeCategory(fileName.GetFileExtension());
        }

        #endregion ------------------ CONTENTELEMENT DIRECTORY BUILDING---------------------------



        #region --------------------------- ADD FILE TO ASSOCIATED FILE STORAGE -------------------------------

        /// <summary>
        /// Add File to Associated File Storage.
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="filePath"></param>
        /// <param name="replicaIndex"></param>
        /// <param name="accessMethod"></param>
        public void AddFile(IContentElement targetElement, string filePath, int replicaIndex = 1, AccessMethodEn accessMethod = AccessMethodEn.LocalFileSystem)
        {

            Validator.ATInvalidOperationDbg(!File.Exists(filePath), nameof(FileService), nameof(AddFile), "File - [{0}] should exist before move it into internal Node Dir".Fmt(filePath));

            if (targetElement.IsContainsReplicaWithAddress(replicaIndex, accessMethod) == false) //create new node dir
            {
                //add default replication info
                var selectedNodeReplica = Current.GetBestReplica().ToReplicaInfo();
                targetElement.NodeReplics.Add(1, selectedNodeReplica);

                //update for real dir of current node 
                CreateNewNodeDir(targetElement, replicaIndex, accessMethod);

            }

            //use existed node dir
            var filename = Path.GetFileName(filePath);
            var targetFilePath = Path.Combine(targetElement.GetReplicaAdress(replicaIndex, accessMethod), filename);
              
            try
            {
                File.Move(filePath, targetFilePath);
            }
            catch (System.Exception ex)
            {
                throw new InvalidOperationException();
            }
        }

        #endregion --------------------------- ADD FILE TO ASSOCIATED FILE STORAGE -------------------------------



        #region --------------------------- CLIENT QUERIES FOR FILES ------------------------------

        //someFile... - {filename| thumb/iconUrl | originalUrl }


        //GetFileInfosOfNode(NodeID, MimeCategory)  - get file Name and Url of Node of MimeCategory
        

        //GetFilePathByUrl - IContentElement , fileName  - local node address + file extension - get MimeCategory + filename
        //GetUrlForFilePath -  IContentElement, filePath -    baseurl/NodeID/files/fileName



        #endregion --------------------------- CLIENT QUERIES FOR FILES ------------------------------




        #region ------------------------- IMPORT/ SYNCHRONOZE WITH SOME SOURCE DIR-----------------------




        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceRootPath"></param>
        public void ImportSynchronize(string sourceRootPath)
        {
            Validator.ATInvalidOperationDbg(!Directory.Exists(sourceRootPath), nameof(FileService), nameof(ImportSynchronize), " sourceRootPath Dir should exists.");

            
            ImportCategory(sourceRootPath, sourceRootPath, null);
  
        }


        
        static void ImportCategory(string sourceRootPath, string categoryPath, Guid? parentID)
        {

            // foreach category -- get description and add if not exist
            //                  -- check save MimeFiles of Category
            //                  --get pages- -foreach page: 
            //                  -- 1 get content nodes description and add if not exist -- check save MimeFiles of Category; 
            //                  -- 2 get categories -recursion for each category                 


            // get description and add element to store if not exist
            var categoryDescriptionFilePath = Path.Combine(categoryPath, "text", "category.json");
            Validator.ATInvalidOperationDbg(File.Exists(categoryDescriptionFilePath) == false, nameof(FileService), nameof(ImportCategory)
                                                  , "Can't find category definitino file -[{0}]".Fmt(categoryDescriptionFilePath));
                          
            var categoryDescriptionText  = File.ReadAllText(categoryDescriptionFilePath);
            Validator.ATInvalidOperationDbg(categoryDescriptionText.IsNullOrEmpty(), nameof(FileService), nameof(ImportCategory)
                                                  , "CategoryDefintiionText can't be null or empty -[{0}]".Fmt(categoryDescriptionText));

            var categoryDescription = JsonSerializer.DeserializeFromString<NodeDescription>(categoryDescriptionText);
            Validator.ATInvalidOperationDbg(categoryDescription.Name.IsNullOrEmpty(), nameof(FileService), nameof(ImportCategory)
                                                 ,"Category description  Name value can't be null or empty, in file [{0}]".Fmt(categoryDescriptionFilePath));
             

            // add element to store if not exist
            if (Current.HostStoreService.ContainsElement(categoryDescription.Name, parentID) == false)
            {
                Current.HostStoreService.AddElement(categoryDescription.Name, categoryDescription.Description, parentID, true); //add
            }
            //check save MimeFiles of Category
            CopySourceNodeFilesToDestinationNode(sourceRootPath, categoryPath);
            //element now exists 
            IContentElement element = Current.HostStoreService.GetElement(categoryDescription.Name, parentID);
              

            //--get pages - -foreach page --1 get content nodes -
            var pages = Directory.GetDirectories(Path.Combine(categoryPath),"P*");
            if (pages.IsWorkable() ) 
            {
                foreach (var pgItem in pages)
                {
                    // 1 get content nodes descriptions and add if not exist
                    var contentItems  = Directory.GetDirectories(Path.Combine(categoryPath, pgItem), "CN*");
                    if (contentItems.IsWorkable())
                    {
                        foreach (var contentItemFldr in contentItems)
                        {
                            var contentItemDir = Path.Combine(categoryPath, pgItem, contentItemFldr);
                            var contentItemDescriptionFilePath = Path.Combine(contentItemDir, "text", "content.json" );
                            Validator.ATInvalidOperationDbg( File.Exists(contentItemDescriptionFilePath) == false, nameof(FileService), nameof(ImportCategory)
                                                    , "Can't find content definitino file -[{0}]".Fmt(contentItemDescriptionFilePath));
                            var contentDescriptionText = File.ReadAllText(contentItemDescriptionFilePath);
                            Validator.ATInvalidOperationDbg( contentDescriptionText.IsNullOrEmpty() , nameof(FileService), nameof(ImportCategory)
                                                    , "ContentDescription Text can't be null or empty -[{0}]".Fmt(contentItemDescriptionFilePath));
                            var contentDescription = JsonSerializer.DeserializeFromString<NodeDescription>(contentItemDescriptionFilePath);
                            Validator.ATInvalidOperationDbg(contentDescription.Name.IsNullOrEmpty(), nameof(FileService), nameof(ImportCategory)
                                               , "Content description [Name] property value can't be null or empty, in file [{0}]".Fmt(contentItemDescriptionFilePath));
 
                            //Add contentDescription  if not exists 
                            if (Current.HostStoreService.ContainsElement(contentDescription.Name, element.ID ) == false)
                            {   Current.HostStoreService.AddElement(contentDescription.Name, contentDescription.Description, element.ID, false); //add
                            }

                            //get/copy MimeCategories files -  foreach MimeTypeCategory -- if source dir exist - create category folder -- copy all files  
                            CopySourceNodeFilesToDestinationNode(sourceRootPath, contentItemDir); 
                             
                        }  
                    }
                     
                    //2get sub Categories - recursion run Import Again
                    var subCategories  = Directory.GetDirectories(Path.Combine(categoryPath, pgItem), "CG*");
                    if (subCategories.IsWorkable())
                    {
                        foreach (var subCategoryFldr in subCategories)
                        {
                            var subCategoryDir = Path.Combine(categoryPath, pgItem, subCategoryFldr);
                            ImportCategory(sourceRootPath, subCategoryDir, element.ID);
                        }
                    }

                } 

            }



        }


        static void CopySourceNodeFilesToDestinationNode(string  sourceRootPath, string sourceNodeDir)
        {
            //get/copy MimeCategories files -  foreach MimeTypeCategory -- if source dir exist - create category folder -- copy all files  
            var mimeCategories = MimeTypeMap.MimeTypeCategories;
            foreach (var mimeCategory in mimeCategories)
            {
                var checkSourceDirPath = Path.Combine(sourceNodeDir, mimeCategory);
                if (!Directory.Exists(checkSourceDirPath)) continue;

                var existedFilesInMimeDir = Directory.GetFiles(checkSourceDirPath);
                if (existedFilesInMimeDir.IsWorkable() == false) continue;

                foreach (var foundedFile in existedFilesInMimeDir)
                {
                    var foundedFilePath = Path.Combine(checkSourceDirPath, foundedFile);

                    var relativeDirPath = checkSourceDirPath.Replace(sourceRootPath, "");
                    var destinationDirPath = Path.Combine( Current.GetBestReplica().NodeStoreLocalAddress, relativeDirPath);
                    if (!Directory.Exists(destinationDirPath)) Directory.CreateDirectory(destinationDirPath);
                    var destinationFilePath = Path.Combine(destinationDirPath, foundedFile);
                    File.Copy(foundedFilePath, destinationFilePath);
                }
            }
        }


        #endregion ------------------------- IMPORT/ SYNCHRONOZE WITH SOME SOURCE DIR-----------------------




        public void Backup(string User)
        {
            throw new NotImplementedException();
        }

        public void Restore(string userFilesBackup, string userFilesMetaDBBackup)
        {
            throw new NotImplementedException();
        }





    }



}



#endif // SERVER ONLY TARGETS
