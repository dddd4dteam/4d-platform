﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Data.Hash
{
     public class Hashes
    {

        /*
        JS Hash Function

        A bitwise hash function written by Justin Sobel
      */




        #region ---------- HASH 4 STRING : 1 -------------

        /// <summary>
        /// Copy of Hash func for string, like in .NET 2.x
        /// Refactored function from NET REFERENCIES sources.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int StringHashCode1(string value) // NET2.0
        {
            int num = 352654597;
            int num2 = num;

            for (int i = 0; i < value.Length; i += 4)
            {
                int ptr0 = value[i] << 16;
                if (i + 1 < value.Length)
                    ptr0 |= value[i + 1];

                num = (num << 5) + num + (num >> 27) ^ ptr0;

                if (i + 2 < value.Length)
                {
                    int ptr1 = value[i + 2] << 16;
                    if (i + 3 < value.Length)
                        ptr1 |= value[i + 3];
                    num2 = (num2 << 5) + num2 + (num2 >> 27) ^ ptr1;
                }
            }

            return num + num2 * 1566083941;
        }


        /*  TS realization StringHashCode1
         *  
          
    public static  StringHashCode1(value: string): number
    {
        let num: number = 352654597;
        let num2: number  = num;         

        for (var i = 0; i < value.length; i += 4)
        {
            let ptr0: number = value.charCodeAt(i) << 16;
            if (i + 1 < value.length)
                ptr0 |= value.charCodeAt[i + 1];

            num = (num << 5) + num + (num >> 27) ^ ptr0;

            if (i + 2 < value.length) {
                let ptr1: number = value.charCodeAt[i + 2] << 16;
                if (i + 3 < value.length)
                    ptr1 |= value.charCodeAt[i + 3];
                num2 = (num2 << 5) + num2 + (num2 >> 27) ^ ptr1;
            }
        }

        return num + num2 * 1566083941;
    }
         */


        #endregion ---------- HASH 4 STRING : 1  -------------


        #region -------------- HASH 4 STRING : 2  -------------

        /// <summary>
        /// Copy of Hash func for string, like in .NET 4.x
        /// Refactored function from NET REFERENCIES sources.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int StringHashCode2(string value)// NET4.0
        {
            int num = 5381;
            int num2 = num;
            for (int i = 0; i < value.Length; i += 2)
            {
                num = (((num << 5) + num) ^ value[i]);

                if (i + 1 < value.Length)
                    num2 = (((num2 << 5) + num2) ^ value[i + 1]);
            }
            return num + num2 * 1566083941;
        }



        /*  TS realization StringHashCode2
         *  
          

         */



        #endregion -------------- HASH 4 STRING : 2  -------------


        /// <summary>
        /// JS Hash Function
        /// <para/> A bitwise hash function written by Justin Sobel
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int JSHash(string value)
        {
            int hash = 1315423911;

            for (int i = 0; i < value.Length; i++)
            {
                hash ^= ((hash << 5) + (value[i]) + (hash >> 2));
            }
            return hash;
        }

         
        /// <summary>
        /// ELF Hash Function
        /// <para/> Similar to the PJW Hash function, but tweaked for 32-bit processors. Its the hash function widely used on most UNIX systems.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public UInt64 ELFHash(string value)
        {
            UInt32 hash = 0;
            UInt32 x = 0;
            //UInt32 i = 0;
            UInt32 tElf =  UInt32.Parse("0xF0000000L", NumberStyles.HexNumber);

            for (int i = 0; i < value.Length;  i++) //value++,
            {
                hash = (hash << 4) + value[i]; //(unsigned char)(*value);
                if ((x = hash & tElf) != 0)
                {
                    hash ^= (x >> 24);
                    hash &= ~x;
                }
            }
            return hash;
        }



        static ulong SimpleHash(string read, bool lowTolerance)
        {
            ulong  hashedValue = 0;
            int i = 0;
            ulong multiplier = 1;
            while (i < read.Length)
            {
                hashedValue += read[i] * multiplier;
                multiplier *= 37;
                if (lowTolerance) i += 2;
                else i++;
            }
            return hashedValue;
        }





    }


}
