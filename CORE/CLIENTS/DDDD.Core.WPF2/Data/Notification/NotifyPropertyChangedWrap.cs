﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Linq.Expressions;
using System.Diagnostics;
using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace DDDD.Core.Data.Notification
{

	/// <summary>
	/// Base class that raises the property changed
	/// </summary>
	public class NotifyPropertyChangedWrap: INotifyPropertyChanged
	{
	
		#region  ------------------------------- FIELDS -----------------------------------------

	 	private   readonly object _lockObject = new object();

		#endregion ------------------------------- FIELDS ---------------------------------------



		#region ---------------------------- INotifyPropertyChanged-------------------------------

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion ---------------------------- INotifyPropertyChanged-------------------------------



		[Conditional("DEBUG")]
		private void VerifyPropertyDbg(string propertyName)
		{
			Type type = this.GetType();
			
			// Look for a public property with the specified name.
			PropertyInfo propertyInfo = type.GetProperty(propertyName);
			
			// The property couldn't be found, so get upset.
			Debug.Assert(propertyInfo != null, String.Format("Unable to verify that type {0} has property {1} !", type.FullName, propertyName));
		}


		/// <summary>
		/// Subclasses can override this method to do stuff after a property value is set. The base implementation does nothing.
		/// </summary>
		/// <param name="propertyName">The name of the property that has changed.</param>
		protected virtual void AfterPropertyChanged(string propertyName)
		{
		}

 
		

		/// <summary>
		/// Raise Property changed event handler.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="property"></param>
		public virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
		{
			var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
			if (propertyInfo == null)
			{
				throw new ArgumentException("The lambda expression 'property' should point to a valid property");
			}

			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
		}


        /// <summary>
        ///Raise Property changed event handler.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
		{
			//var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
			Contract.Requires(  String.IsNullOrEmpty(propertyName) , @"  OnPropertyChanged(): The  propertyName should point to a valid Subject");
			
			VerifyPropertyDbg(propertyName);

			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));			  

			AfterPropertyChanged(propertyName);
		}

        /// <summary>
        /// Raise Property changed event handler.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="R"></typeparam>
        /// <param name="property"></param>
        public void OnPropertyChanged<T, R>(Expression<Func<T, R>> property)
		{
			var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
			
			Contract.Assert( propertyInfo != null , @"   OnPropertyChanged<T,R>(): The lambda expression 'property' should point to a valid Subject");

			VerifyPropertyDbg(propertyInfo.Name);
			
			if (PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
			
			AfterPropertyChanged(propertyInfo.Name);
		}

	}


}
