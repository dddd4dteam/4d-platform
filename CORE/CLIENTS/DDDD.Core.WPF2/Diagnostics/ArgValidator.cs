﻿using DDDD.Core.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Diagnostics
{
    /// <summary>
    ///  Validator with simple Assertions:
    ///  - Invalid Operation Exception,
    ///  - Null Reference Exceptopm
    /// <para/> Cacn be created l;ocally in class as local static instance
    /// </summary>
    public class ArgValidator
    {

        public ArgValidator(string className)
        {
            ClassName = className;
        }



        /// <summary>
        /// Class name where ArgValidation uses.
        /// </summary>
        public string ClassName
        { get; private set;}

        /// To get Message we use template from Default ResourceManager by Validator class.
        /// Message template Key: [T-Validator] + [Operation Name - Operation].  

        /// <summary>
        ///  AT(AssertTrue) If  nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException.  Works only in DEBUG mode.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// </summary>
        /// <param name="nullChekingItem"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        [Conditional("DEBUG")]
        public  void ATNullReferenceArgDbg(object nullChekingItem, string methodName, string parameterName)
        {
            if (nullChekingItem == null)
            {
                throw new NullReferenceException(string.Format(
                    RCX.ERR_NullReferenceValue
                    , ClassName, methodName, parameterName ));
            }
        }


        /// <summary>
        ///  AT(AssertFalse) If  nullChekingItem IS NOT NULL - (FALSE that nullChekingItem.IsNull()- .IsNotNull()) then throw NullReferenceException.  Works only in DEBUG mode.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// </summary>
        /// <param name="nullChekingItem"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        [Conditional("DEBUG")]
        public void AF_NullReferenceArgDbg(object nullChekingItem,   string methodName, string parameterName)
        {
            if (nullChekingItem != null)
            {
                throw new NullReferenceException(
                   string .Format( RCX.ERR_NullReferenceValue,
                             ClassName, methodName, parameterName));
            }
        }




        /// <summary>
        /// If condition is TRUE - then throw InvalidOperationException. Works only in DEBUG mode.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].".
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="methodName"></param>
        /// <param name="userMessage"></param>
        [Conditional("DEBUG")]
        public  void ATInvalidOperationDbg(bool condition, string methodName, string userMessage)
        {
            if (condition)
            {
                throw new InvalidOperationException(
                  string.Format( RCX.ERR_InvalidOperation
                        , ClassName, methodName, userMessage));
            }
        }



        /// <summary>
        /// If condition is FALSE - then throw InvalidOperationException. Works only in DEBUG mode.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].". 
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="methodName"></param>
        /// <param name="userMessage"></param>
        [Conditional("DEBUG")]
        public  void AFInvalidOperationDbg(bool condition,  string methodName, string userMessage)
        {
            if (!condition)
            {
                throw new InvalidOperationException(
                   string.Format(RCX.ERR_InvalidOperation
                   , ClassName , methodName, userMessage));
            }
        }




    }
}
