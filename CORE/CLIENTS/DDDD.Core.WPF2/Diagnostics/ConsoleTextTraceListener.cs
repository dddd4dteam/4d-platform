﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.BCL.Diagnostics
{
    /// <summary>
    /// Custom Text Trace Listener based on the System.Diagnostics.TextWriterTraceListener,
    ///  and also it can write text output to the Console.
    /// </summary>
    public class ConsoleTextTraceListener : TextWriterTraceListener
    {

        #region -------------------- CTORS -------------------------
        
        
        /// <summary>
        ///  Initializes a new instance of the TextTraceListener
        ///     class with System.IO.TextWriter as the output recipient.
        /// </summary>
        public ConsoleTextTraceListener()
            : base()
        {
            UseConsoleOutput = true;
        }


        /// <summary>
        /// Initializes a new instance of the TextTraceListener
        ///     class, using the stream as the recipient of the debugging and tracing output.
        ///
        /// </summary>
        /// <param name="stream"></param>
        public ConsoleTextTraceListener(Stream stream)
            : base(stream)
        {
            UseConsoleOutput = false;
        }

       
        /// <summary>
        ///  Initializes a new instance of the TextTraceListener
        ///     class using the specified writer as recipient of the tracing or debugging
        ///     output.
        /// </summary>
        /// <param name="writer"></param>
        public ConsoleTextTraceListener(TextWriter writer)
            : base(writer)
        {
            UseConsoleOutput = false;
        }

        /// <summary>
        /// Initializes a new instance of the TextTraceListener
        ///     class with the specified name, using the specified writer as recipient of
        ///     the tracing or debugging output.
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="name"></param>
        public ConsoleTextTraceListener(TextWriter writer, string name)
            : base(writer, name)
        {
            UseConsoleOutput = false;
        }


        /// <summary>
        ///  Initializes a new instance of the TextTraceListener
        ///     class with the specified name, using the stream as the recipient of the debugging
        ///     and tracing output.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="name"></param>
        public ConsoleTextTraceListener(Stream stream, string name)
            : base(stream, name)
        {
            UseConsoleOutput = false;
        }

      

        /// <summary>
        /// Initializes a new instance of the TextTraceListener
        ///     class, using the file as the recipient of the debugging and tracing output.
        /// </summary>
        /// <param name="path"></param>
        public ConsoleTextTraceListener(string path)
            : base(path)
        {
            UseConsoleOutput = false;
        }

        /// <summary>
        ///  Initializes a new instance of the TextTraceListener
        ///     class with the specified name, using the file as the recipient of the debugging
        ///     and tracing output.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="name"></param>
        public ConsoleTextTraceListener(string path, string name)
            : base(path, name)
        {
            UseConsoleOutput = false;
        }

        #endregion -------------------- CTORS -------------------------


        static readonly object _locker = new object();


        /// <summary>
        /// Use Console for text output on WriteLine call
        /// </summary>
        public bool UseConsoleOutput
        {
            get;
            internal set;
        }


        /// <summary>
        /// Write Message as Line string value into the Console(if UseConsoleOutput is true) 
        /// or other enabled for System.TextWriterTraceListener output targets.
        /// </summary>
        /// <param name="message"></param>
        public override void WriteLine(string message)
        {
            lock (_locker)
            {
                if (UseConsoleOutput)
                {

                    WriteHeader();
                    Console.WriteLine(message);
                    WriteFooter();
                }
                else
                {
                    base.WriteLine(message);
                }
            } 
        }


        internal bool IsEnabled(TraceOptions opts)
        {
            return (opts & TraceOutputOptions) != 0;
        }


        private void WriteHeader(String source, TraceEventType eventType, int id)
        {
            Write(String.Format(CultureInfo.InvariantCulture, "{0} {1}: {2} : ", source, eventType.ToString(), id.ToString(CultureInfo.InvariantCulture)));
        }

        

        private void WriteFooter()
        {
            //if (eventCache == null)
                return;

            //indentLevel++;
            if (IsEnabled(TraceOptions.ProcessId))
            //    WriteLine("ProcessId=" + eventCache.ProcessId);

            if (IsEnabled(TraceOptions.LogicalOperationStack))
            {
                Write("LogicalOperationStack=");
                Stack operationStack = eventCache.LogicalOperationStack;
                bool first = true;
                foreach (Object obj in operationStack)
                {
                    if (!first)
                        Write(", ");
                    else
                        first = false;

                    Write(obj.ToString());
                }
                WriteLine(String.Empty);
            }

            if (IsEnabled(TraceOptions.ThreadId))
                WriteLine("ThreadId=" + Thread.CurrentThread.ManagedThreadId);

            if (IsEnabled(TraceOptions.DateTime))
                WriteLine("DateTime=" + DateTime.Now.ToString("o", CultureInfo.InvariantCulture));

            
            //if (IsEnabled(TraceOptions.Callstack))
            //    WriteLine("Callstack=" + eventCache.Callstack);
            
            //indentLevel--;
        }

    }


}
