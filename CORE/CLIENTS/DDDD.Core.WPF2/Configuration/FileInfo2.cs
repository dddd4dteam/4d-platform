﻿using System;
using System.IO;
using System.Collections.Generic;

using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Data.MIME;

namespace DDDD.Core.Configuration
{
    /// <summary>
    /// FileInfo  2 - simple info about File, about it's relation inside Directory Node;  never lock any target file. 
    /// Json content always will be loaded on FileInfo2 creation. 
    /// </summary>
    public class FileInfo2
    {

        #region ------------------------ CTOR -----------------------------

        /// <summary>
        /// FileInfo 2 - simple info about File, about it's relation inside Directory Node;  never lock any target file. 
        /// Json content always will be loaded on FileInfo2 creation. 
        /// </summary>
        /// <param name="fullFilePath"></param>
        public FileInfo2(string fullFilePath)
        {
            Validator.ATNullOrEmptyArgDbg(fullFilePath, nameof(FileInfo2), nameof(FileInfo2), nameof(fullFilePath));
            Validator.ATInvalidOperationDbg(!File.Exists(fullFilePath), nameof(FileInfo2), nameof(FileInfo2), "File [{0}] do not exist.".Fmt(fullFilePath) );

            FilePath = fullFilePath;
            var segments = FilePath.SplitNoEntries(@"\");
            //Validator.ATInvalidOperationDbg(segments.Count == 0, nameof(FileInfo2), nameof(FileInfo2), "Invalid FilePath value. Segments count is 0");

            var lastSegment = segments[segments.Count - 1];
            var nameExtension = lastSegment.SplitNoEntries(".");
            //Validator.ATInvalidOperationDbg(nameExtension.Count == 0, nameof(FileInfo2), nameof(FileInfo2), " File [Name + Extension] value. Segments count  of nameExtension is 0");
            
            Extension = "."+ nameExtension[nameExtension.Count - 1];
            Name = lastSegment.Replace(Extension, "");

            MimeType = MimeTypeMap.GetMimeType(Extension);
            MimeTypeCategory = MimeTypeMap.GetMimeTypeCategory(Extension);


            //if (Extension == ".json")
            //{   FileMimeCategory = string.json;
                
            //    //load json Content to Info Parameters
            //    //InfoParameters.Add(new KeyValuePair<string, string>("json", File.ReadAllText(FilePath)));
            //}  
        }
         

        #endregion ------------------------ CTOR -----------------------------



        #region ---------------------------- PROPERTIES -------------------------------
        
      
        /// <summary>
        /// File Name without extension
        /// </summary>
        public string Name
        { get; set; }


        /// <summary>
        /// Extension of file
        /// </summary>
        public string Extension
        { get; set; }


        /// <summary>
        /// File MIME-type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// File  MIME-Type category -  audio/video/image/... based on its extension.
        /// </summary>
        public string MimeTypeCategory { get; set; }



        /// <summary>
        /// Full File path
        /// </summary>
        public string FilePath
        { get; set; }

        /// <summary>
        /// Relative Path from RootDir. Remember: RootDir always finished with delimeter symbol '\'.  
        /// </summary>
        public string RelativePath { get; set; }


        #endregion ---------------------------- PROPERTIES -------------------------------


        public string LoadText()
        {
           Validator.ATInvalidOperationDbg(MimeTypeCategory != "text" , nameof(FileInfo2), nameof(LoadText), "Only text files should be readed as text.");

           return File.ReadAllText(FilePath);                        
        }


        /// <summary>
        /// Update File Relations ad FileDataType. 
        /// </summary>
        /// <param name="rootPhysicalpath"></param>
        /// <param name="extensionBasedDataTypeDetermineFunc"></param>
        public void UpdateRelativePath(string rootPhysicalpath )
        {
            RelativePath = FilePath.Replace(rootPhysicalpath,"");
        }
 


    }
}
