﻿
using System;
using DDDD.Core.Reflection;
namespace DDDD.Core.Configuration.ECE 
{
    /// <summary>
    /// Configuration Parameter from Enum.Field . 
    /// Use ctor of NamedParameters  version and describe EnumParameter. 
    /// 1 Parameter should have  [ParamType] and [Kind]  property values 
    /// 2 Then you may/or not set other properties. 
    /// 3 Default content of simple struct types( as bool, int... ) can be set to content as String, and then values will be parsed for real [Content] value.                    
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field, AllowMultiple = false)]
    public class EnumParamAttribute : Attribute
    {

        //////////////////// CTOR - Use ctor of NamedParameters  version and describe EnumParameter.  //////////////////////




        #region ---------------------------- PROPERTIES ---------------------------

        /// <summary>
        /// Param Value: DefaultValue/ CustomValue
        /// </summary>
        public object Content
        {
            get;
            set;
        }


        /// <summary>
        /// Tag  -additional values like Delegate/MethodName value to get end Content.
        /// Tag value Can be of course as String or some Type
        /// </summary>
        public object Tag
        {
            get;
            set;
        }



        #region  --------------------------------- TYPOLOGY -------------------------------------

        /// <summary>
        /// Parameters Classification -Parameter Kind. Kind is not the same as Type(ParamType property).        
        /// 
        /// </summary>
        public ParamKindEn Kind
        {
            get;
            set;
        }

        /// <summary>
        /// Тип конечного значения параметра. Если ValidationLevel == 0 то CurrentType может отличаться от ParamType-а.  
        /// </summary>
        public Type ParamType
        {
            get;
            set;
        }


        /// <summary>
        /// Название имени Типа Параметра
        /// </summary>
        public String ParamTypeName
        {
            get { return ParamType.Name; }

        }


        #endregion --------------------------------- TYPOLOGY -------------------------------------


        #region --------------------------------  SEARCH Filters ---------------------------------

        /// <summary>
        ///  Группировка парамтера- Группа.  Фильтр для дальнейшего поиска параметра в общей таблице
        /// </summary>
        public String GroupFilter
        {
            get;
            set;
        }


        /// <summary>
        /// Группировка парамтера- ПодГруппа.  Фильтр для дальнейшего поиска параметра в общей таблице
        /// </summary>
        public String SubGroupFilter
        {
            get;
            set;
        }


        #endregion -------------------------------- SEARCH Filters --------------------------------



        /// <summary>
        ///  Validation Level - validation ( Content only value) behavior.   Can be setted  only as 0 or 1 value
        ///  If Validation Level is 0 - it means exception won't throw - it's, the Parameter means nothing
        ///  If Validation Level is 1 - exception will throw- the Parameter is important  - for your Object Model
        /// </summary>
        public Int32 ValidationLevel
        {
            get;
            internal set;
        }


        #region  ----------------------------------Lazy/ Loading Value ----------------------------------
        /// <summary>
        /// Path  to load  value - XmlfileContent, Create Type ...
        /// </summary>
        public String LoadPath
        {
            get;
            set;
        }


        /// <summary>
        /// Target Content of this Parameter can be loaded some later by LoadPath/XmlRefType.
        /// </summary>
        public bool IsLazyLoad
        {
            get;
            set;
        }



        #endregion ----------------------------------Lazy/ Loading Value ----------------------------------


        #region ----------------------------  Persistance (SterlingDB Store Engine ) -----------------------------------

        /// <summary>
        /// if we need to store this configuration param in the EnumConfigurationEngine.PersistanceStore
        /// </summary>
        public bool IsPersistable
        {
            get;
            set;
        }


        #endregion ----------------------------  Persistance (SterlingDB Store Engine ) -----------------------------------


        #region ----------------------------------- LOCALIZATION SUPPORT ---------------------------

        /// <summary>
        /// Localization with *.Resx of Param Name information
        /// </summary>
        public String LocalNameKey
        {
            get;
            set;
        }

        /// <summary>
        /// Localization with *.Resx of Param Description information
        /// </summary>
        public String LocalDescriptionKey
        {   //, – ru, RU-ru, En-us… 
            get;
            set;
        }

        #endregion ----------------------------------- LOCALIZATION SUPPORT ---------------------------


        #endregion ---------------------------- PROPERTIES ---------------------------



        #region   --------------------------------------- METHODS ------------------------------------





        #region  ---------------------------------- PARSING PARAM  -----------------------------------------




        #endregion ---------------------------------- PARSING PARAM  -----------------------------------------



        internal void InitContent()
        {
            if (Kind == ParamKindEn.ComplexType || Kind == ParamKindEn.ValueType || Kind == ParamKindEn.Enum)
            {
                this.Content = TypeActivator.CreateInstanceBoxedLazy(ParamType); //Activator.CreateInstance(ParamType);
            } 
        }



        #endregion --------------------------------------- METHODS ------------------------------------

    }
}
