﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DDDD.Core.Extensions;


namespace DDDD.Core.Configuration
{


    /// <summary>
    /// Configuration Parameter Information 
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum|AttributeTargets.Field, AllowMultiple=false)]    
    public class ConfigurationParameterInfoAttribute : Attribute
    {

        // The constructor is called when the attribute is set.

        #region -------------------------CTOR ---------------------------
        




        public ParamAttribute(Type pType, ParamKindEn kind = ParamKindEn.Struct, 
                              object defaultValue = null,
                              object supportValue = null, 
                              bool pIsLazyLoad = false,
                              String pLoadPath = "",
                              Int32 pLifeTimeMin = 0  )
        {
            
            ParamTypeName = pType.FullName;
            FinalType = pType; //can get Type / or  will throw exception
            //ParamTypeCode = TypeCode.Empty;  // not use this Type reflection way
            
            Kind = kind;
            Value = defaultValue;
            SupportValue = supportValue;
            LifeTimeMin = pLifeTimeMin;

        }


      

                
        #endregion -------------------------CTOR --------------------------
                


        #region ---------------------------- PROPERTIES --------------------------- 
        
        /// <summary>
        /// Param Value: DefaultValue/ CustomValue
        /// </summary>
        public object Value
        {
            get; 
            private set;
        }

        /// <summary>
        /// MethodName to get Delegate value as Value
        /// </summary>
        public object SupportValue
        {
            get;
            private set;
        }





        #region  ---------------------------- identification ---------------------------------

        /// <summary>
        ///  Global Configuration Service Dictionary Key 
        /// </summary>
        public Int32 UniqueHashKey
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public String GlobalKeyMask
        {
            get;
            private set;
        }


        /// <summary>
        /// Global String ParamItemKey that build with the help of ConfigEnum.ItemKeyMask 
        /// </summary>
        public String GlobalKey
        {
            get;
            private set;
        }

        /// <summary>
        /// Тип родительского Перечисления конфигурации 
        /// </summary>
        public Type ParentEnumType
        {
            get;
            private set;
        }

        /// <summary>
        /// Родительское перечисление. поле параметра  :  "CustomEnum.FiledValue"
        /// </summary>
        public String EnumValueKey
        {
            get;
            internal set;
        }

        #endregion ---------------------------- Identification ---------------------------------


        #region  --------------------------------- TYPOLOGY -------------------------------------

        /// <summary>
        /// 
        /// </summary>
        public ParamKindEn Kind
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public String ParamTypeName
        {
            get;
            private set;
        }

        /// <summary>
        /// Тип параметра
        /// </summary>
        public Type FinalType
        {
            get;
            private set;
        }

        /// <summary>
        /// That has value only after ConverToValue<T>  - T will be set to ParamAttribute here as ConvertedType. 
        /// Before use ConverToValue<T> ConvertedType value is null. 
        /// </summary>
        public Type ConvertedType
        {
            get;
            private set;
        }

        /// <summary>
        /// Ключевые мсемантические флаги параметра
        /// </summary>
        public List<string> SemanticFlags 
        {
            get;
            private set;
        }


        #endregion --------------------------------- TYPOLOGY -------------------------------------
        

        #region -------------------------------- VOLATILE SEARCH ---------------------------------

        /// <summary>
        /// 
        /// </summary>
        public String[] KeyWords
        {
            get;
            private set;
        }

        #endregion -------------------------------- VOLATILE SEARCH --------------------------------



        /// <summary>
        ///  Importance Level of current Parameter - what will happen on Get; Param Value. 
        ///  If importance level is 0 - it means exception won't throw - it's, the Parameter means nothing
        ///  If importance level is 1 - exception will throw- the Parameter is important 
        /// </summary>
        public Int32 ImportaceLevel
        {
            get;
            private set;
        }
        

        #region  ----------------------------------Lazy/ Loading Value ----------------------------------
        /// <summary>
        /// 
        /// </summary>
        public String LoadPath
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool IsLazyLoad
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool IsLoaded
        {
            get;
            set;
        }

        #endregion ----------------------------------Lazy/ Loading Value ----------------------------------
        
        
        #region ----------------------------------- LOCALIZATION SUPPORT ---------------------------

        /// <summary>
        /// 
        /// </summary>
        public String LocalsKey
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String LocalsSupport
        {   //, – ru, RU-ru, En-us… 
            get;
            set;
        }

        #endregion ----------------------------------- LOCALIZATION SUPPORT ---------------------------


#region -------------------------------- LifeTime Param Instance Object --------------------------------------
        
        /// <summary>
        /// Последняя дата получения 
        /// </summary>
        public DateTime LastGetUseTime
        {
            get;
            private set;
        }
        

        /// <summary>
        /// назначенное время жизни Параметра после создания или последнего использования
        /// </summary>
        private Int32 LifeTimeMin
        {
            get; 
            set;
        }


        /// <summary>
        /// Дата/время создания и последнего присвоения 
        /// </summary>
        public DateTime LastCreationOrSetTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Пришло ли время умирать 
        /// </summary>
        /// <returns></returns>
        public bool IsTimeToDie()
        {
            TimeSpan deltaTimeFromCreation = (DateTime.Now - LastCreationOrSetTime);
            TimeSpan deltaTimeFromLastGetting = (DateTime.Now - LastCreationOrSetTime);
            
            if (
                 deltaTimeFromCreation    > TimeSpan.FromMinutes(LifeTimeMin)   &&
                 deltaTimeFromLastGetting > TimeSpan.FromMinutes(LifeTimeMin)      
               )
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Обновить время GetUse 
        /// </summary>
        public void UpdateGetTime()
        {
            LastGetUseTime =  DateTime.Now;
        }
        
        /// <summary>
        /// Обновить время Create 
        /// </summary>
        public void UpdateCreateTime()
        {
            LastCreationOrSetTime = DateTime.Now;
        }


#endregion -------------------------------- LifeTime Param Instance Object --------------------------------------


        #endregion ---------------------------- PROPERTIES ---------------------------



        #region   --------------------------------------- METHODS ------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="NewValue"></param>
        public void ResetValue(Int32 GlobalHashCode, Type ConfigEnumType, String pGlobalKey, String pGlobalKeyMask, String SimpleEnumValueKey, string[] KnownKeys)
        {   
            UniqueHashKey = GlobalHashCode;                        

            GlobalKey = pGlobalKey; 
            // Parse into flags

            GlobalKeyMask = pGlobalKeyMask;

            SemanticFlags = CollectFlags(pGlobalKeyMask,  Configuration.ParamKeyPattern, KnownKeys);

            ParentEnumType = ConfigEnumType;

            EnumValueKey = SimpleEnumValueKey;
        }



        public void UpdateValueOnly(object newValue)
        {
            Value = newValue;
        }

        public void UpdateConvertedTypeOnly(Type newConvertedType)
        {
            ConvertedType = newConvertedType;            
        }





        #region  ---------------------------------- PARSING PARAM  -----------------------------------------

        static List<String> CollectFlags(String inputData, String Pattern, string[] KnownKeys)
        {
            // Collecting flags
            //string pattern = @"\[([A-Za-z0-9]+)\]|\{([A-Za-z0-9]+)\}";
            Regex regex = new Regex(Pattern);
            string[] splitArray = regex.Split(inputData);
            List<string> FlagArray = new List<string>();
            
            foreach (var item in splitArray)
            {
                if (String.IsNullOrEmpty(item)) continue;

                string clearItem = item.TrimSymbols(new string[] { "[", "]", "{", "}", ".", ",", ":", ";" }, "");
                
                if (String.IsNullOrEmpty(clearItem)) continue;

                if (KnownKeys.Contains(clearItem) == false)
                {
                    FlagArray.Add(item);
                }
            }


            return FlagArray;
        }

        #endregion ---------------------------------- PARSING PARAM  -----------------------------------------



#region  --------------------------------------- LAZY LOADING PARAMS -----------------------------------------


        /// <summary>
        /// 
        /// </summary>
        private void LoadLazyXmlRefValue(String ResourceKey, bool NeedDeserialize, bool NeedUnpack)
        {
            String XmlStringRes = null;
            if (Kind == ParamKindEn.XmlRef && FinalType != null)
            {   //check Assembly param value
            }

            if (NeedUnpack)
            {
            }

            if (NeedDeserialize)
            {
                //RuntimeTypeModel.Default.Default[typeof(Message)] .AddSubType(1, typeof(LogonRequest));
                //ProtoBuf.Serializer.NonGeneric.
                //ProtoBuf.Serializer.DeserializeWithLengthPrefix<
                //ProtoBuf.Serializer.GlobalOptions.
                //ProtoBuf.Serializer.Deserialize(ResourceLoader.LoadStringStream(ResourceKey) , ParamType);
            }

            XmlStringRes = ResourceLoader.LoadStringRes(ResourceKey);

        }

#endregion --------------------------------------- LAZY LOADING PARAMS -----------------------------------------



        


        
#endregion --------------------------------------- METHODS ------------------------------------
        
    }

}
