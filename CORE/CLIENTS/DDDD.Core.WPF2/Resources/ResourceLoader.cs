﻿using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Resources;

namespace DDDD.Core.Resources
{


    /// <summary>
    /// 
    /// </summary>
    public class ResourceLoader
    {

        #region  ----------------------- LOADING BINARY RESOURCES -------------------------

        // LoadStringRes
        // LoadStringRes


        /// <summary>
        /// Load byte[] resource by Uri. 
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static byte[] LoadBinaryRes(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            byte[] resBody = null;
            using (var reader = new BinaryReader(resStreamInfo.Stream))
            {
                resBody = reader.ReadBytes((Int32)reader.BaseStream.Length);
            }

            return resBody;
        }


        /// <summary>
        /// Load byte[] resource by String Uri. 
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static byte[] LoadBinaryRes(string resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadBinaryRes(resInfo);
        }


        #endregion ----------------------- LOADING BINARY RESOURCES -------------------------


        #region  ----------------------- LOADING STRING RESOURCES -------------------------

        // LoadStringRes
        // LoadStringRes


        /// <summary>
        /// Load string resource by Uri. Returns String result.
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static String LoadStringRes(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            string resBody = null;
            using (var reader = new StreamReader(resStreamInfo.Stream))
            {
                resBody = reader.ReadToEnd();
            }

            return resBody;
        }

        /// <summary>
        /// Load String Stream  by Uri. Returns Stream result.
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static Stream LoadStringStream(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            return resStreamInfo.Stream;
        }


        /// <summary>
        /// Load String Stream  by String of Uri. Returns Stream result.
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static Stream LoadStringStream(String resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadStringStream(resInfo);
        }


        /// <summary>
        /// Load String Stream  by String of Uri. Returns String result.
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static String LoadStringRes(string resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadStringRes(resInfo);
        }


        #endregion ----------------------- LOADING STRING RESOURCES -------------------------


        #region  ------------------------ LOADING ASSEMBLY ------------------------------





#if CLIENT && SL5




        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <returns></returns>
        public static Assembly LoadAssemmbly<TContract>()
        {
            return LoadAssemmbly(typeof(TContract));
        }


        public static Assembly LoadAssemmbly(Type contract)
        {
            //ArgumentValidator.AssertNotNullOrEmpty(AssemblyUri, "LoadAssembly:  Uri String cannot be null");
            String AssemblyUri = GetTypeModule(contract);
            Uri assemblyUri = new Uri(AssemblyUri, UriKind.Relative);
            return LoadAssemmbly(assemblyUri);

        }


        private static String GetTypeModule(Type contract)
        {
            return contract.Assembly.ManifestModule.Name; // FullName.Split(',')[0] + ".dll";
        }


        /// <summary>
        /// Подгрузка сборки
        /// </summary>
        /// <param name="AssemblyUri"></param>
        /// <returns></returns>         
        public static Assembly LoadAssemmbly(String AssemblyUri)
        {
            //ArgumentValidator.AssertNotNullOrEmpty(AssemblyUri, "LoadAssembly:  Uri String cannot be null");

            Uri assemblyUri = new Uri(AssemblyUri, UriKind.Relative);
            return LoadAssemmbly(assemblyUri);

        }




        /// <summary>
        /// Подгрузка сборки
        /// </summary>
        /// <param name="AssemblyUri"></param>
        /// <returns></returns> 
        public static Assembly LoadAssemmbly(Uri AssemblyUri)
        {
            Uri assemblyUri = AssemblyUri;

            //ArgumentValidator.AssertNotNull(AssemblyUri, "LoadAssembly:  Uri cannot be null");
            // Assembly pluginAssembly = Assembly.Load(typeof(ConfigurationEngine).Assembly.FullName);

            // Type[] types = pluginAssembly.GetTypes();



            StreamResourceInfo assInfo = Application.GetResourceStream(assemblyUri);
            return new AssemblyPart().Load(assInfo.Stream); // Current.ApplicationLifetimeObjects new AssemblyPart()
        }

#elif SERVER



#endif

        #endregion ------------------------ LOADING ASSEMBLY ------------------------------
        


        #region  ----------------------- LOADING RESOURCES DICTIONARY from STRING --------------------------------------

        /// <summary>
        /// Подгрузить словарь ресурсов
        /// </summary>
        /// <param name="resourceDictionaryUri"></param>
        /// <returns></returns>
        public static ResourceDictionary LoadResourceDictionary(string resourceDictionaryUri)
        {
            Uri ResDictionaryUri = new Uri(resourceDictionaryUri, UriKind.RelativeOrAbsolute);
            return LoadResourceDictionary(ResDictionaryUri);
        }



        /// <summary>
        /// Подгрузить словарь ресурсов
        /// </summary>
        /// <param name="resourceDictionaryUri"></param>
        /// <returns></returns>
        public static ResourceDictionary LoadResourceDictionary(Uri resourceDictionaryUri)
        {
            try
            {
                ResourceDictionary resDitionary = new ResourceDictionary();

                resDitionary.Source = resourceDictionaryUri;

                return resDitionary;

                //return XamlReader.Load(xamlStringBody) as ResourceDictionary;                   
            }
            catch (Exception xamlEx)
            {
                throw new Exception("LoadResourceDictionary Error :  " + xamlEx.Message);
            }
        }





        #endregion --------------------------  LOADING RESOURCES DICTIONARY from STRING--------------------------------------



        #region -------------------------- LOADING RESOURCES ----------------------------
        





        #region ----------------------------- LOADING IMAGE -------------------------------

        /// <summary>
        /// Источник - Запустить загрузку изображения из сборки которая 
        /// </summary>
        /// <param name="StrSource"></param>
        public static BitmapImage LoadImageRes(String ImageStrUri)
        {
            Contract.Requires(ImageStrUri != null, "LoadImageRes: Uri String cannot be null");

            Uri imageUri = new Uri(ImageStrUri, UriKind.RelativeOrAbsolute);
            return LoadImageRes(ImageStrUri);
        }


        /// <summary>
        /// Источник - Запустить загрузку изображения из сборки которая 
        /// </summary>
        /// <param name="StrSource"></param>
        public static BitmapImage LoadImageRes(Uri ImageUri)
        {
            Contract.Requires(ImageUri != null, "LoadImageRes: Uri cannot be null");

            try
            {

                return new BitmapImage(ImageUri);
            }
            catch (Exception ex)
            {
                // DEBUG               
                throw new Exception("LoadImageRes error:" + ex.Message);
            }
        }


        #endregion ----------------------------- LOADING IMAGE ------------------------------


        // Local components from xap:
        // StreamResource
        // assembly from xap 
        // Image file from assembly - // BY LEEDTOOLS from directory
        // xml file from assembly
        // config object from xml


        // DAL Assemblies  DomDalSL4Assembly as String constant 
        // string DalFileName = BootSettingsBuilder.GetComposedSt(CompositionSettingsEn.DomDalSL4Assembly);
        // StreamResourceInfo info = Application.GetResourceStream(new Uri(DalFileName, UriKind.Relative));
        // DomainAssemblies.Add(DefaultDomainAssemblyKey, new AssemblyPart().Load(info.Stream));



        #endregion -------------------------- LOADING RESOURCES ----------------------------


    }
}
