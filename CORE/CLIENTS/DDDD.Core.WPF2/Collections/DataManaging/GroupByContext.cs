﻿
using System;
using System.Collections;
using System.Linq;
using System.Windows.Data;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that encapsulates the GroupBy functionality used by the <see cref="T: DataManagerBase" />
    /// </summary>
    public abstract class GroupByContext
    {
        /// <summary>
        /// Gets the name of the property that data should be grouped by.
        /// </summary>
        public string PropertyName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets a list of the summaries that should be applied to Children.
        /// </summary>
        protected internal SummaryDefinitionCollection Summaries
        {
            get;
            set;
        }

        protected GroupByContext()
        {
        }

        /// <summary>
        /// Creates a Generic instance of the <see cref="T: GroupByContext" />.
        /// </summary>
        /// <param name="cti">The type of data to create the GroupByContext from.</param>
        /// <param name="propertyName">The name of the property, that the data should be grouped by.</param>
        /// <param name="comparer">The IEqualityComparer that will be used to perform the grouping</param>
        /// <returns></returns>
        public static GroupByContext CreateGenericCustomGroup(CachedTypedInfo cti, string propertyName, object comparer)
        {
            Type cachedType = cti.CachedType;
            Type type = typeof(GroupByContext<>);
            Type[] typeArray = new Type[] { DataManagerBase.ResolvePropertyTypeFromPropertyName(propertyName, cti) };
            Type type1 = type.MakeGenericType(typeArray);
            object[] objArray = new object[] { propertyName, comparer, cti };
            return TypeActivator.CreateInstanceTBaseLazy<GroupByContext>(type1,TypeActivator.DefaultCtorSearchBinding, objArray);  //(GroupByContext)Activator.CreateInstance(type1, objArray);
        }

        /// <summary>
        /// Creates a Generic instance of the <see cref="T: GroupByContext" />.
        /// </summary>
        /// <param name="cti">The type of data to create the GroupByContext from.</param>
        /// <param name="comparer"></param>
        /// <param name="converter"></param>
        /// <param name="converterParam"></param>
        /// <returns></returns>
        public static GroupByContext CreateGenericCustomGroup(CachedTypedInfo cti, object comparer, IValueConverter converter, object converterParam)
        {
            Type cachedType = cti.CachedType;
            Type type = typeof(GroupByContext<>).MakeGenericType(new Type[] { cachedType });
            object[] objArray = new object[] { comparer, converter, converterParam, cti };
            return TypeActivator.CreateInstanceTBaseLazy<GroupByContext>(type, TypeActivator.DefaultCtorSearchBinding, objArray);   //(GroupByContext)Activator.CreateInstance(type, objArray);
        }

        /// <summary>
        /// Groups the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
        /// </summary>
        /// <typeparam name="T">The typeof data that needs to be grouped.</typeparam>
        /// <param name="query">A colleciton of data to group by.</param>
        /// <returns>Collection of <see cref="T: GroupByDataContext" /> objects.</returns>
        public abstract IList Group<T>(IQueryable<T> query);
    }


}
