﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using DDDD.Core.Collections.Format.IG;
using DDDD.Core.Collections.Summary.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Wraps an IEnumerable to get items while using IList or IQueryable to improve performance if available.
    /// </summary>
    /// <typeparam name="T">The type of the IEnumerable<![CDATA[<T>]]> that this data manager manages.</typeparam>
    public class DataManager<T> : DataManagerBase
    {
        private IEnumerable<T> _dataSource;

        private List<T> _cachedFilterSortedList;

        private bool _needSort;

        private bool _needGroupBy;

        private bool _needPaging;

        private bool _needFiltering;

        private bool _needSummary;

        private bool _needConditionalFormat;

        private bool _needMerge;

        private MultiSortComparer<T> _multiSortComparer;

        private bool _sortDescriptionsClearing;

        internal override IList SortedFilteredList
        {
            get
            {
                if (this._cachedFilterSortedList != null)
                {
                    return this._cachedFilterSortedList;
                }
                return base.SortedFilteredList;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataManagerBase" /> class.
        /// </summary>
        public DataManager()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataManagerBase" /> class and sets the <see cref="P: DataManagerBase.DataSource" />.
        /// </summary>
        /// <param name="dataSource">The datasource that this datamanager should manage.</param>
        public DataManager(IEnumerable<T> dataSource) : this()
        {
            this.SetDataSource(dataSource);
        }

        /// <summary>
        /// Applies all Clientside related data manipulations, such as sorting, paging, groupBy and filtering.
        /// </summary>
        protected bool ApplyClientDataManipulations()
        {
            return this.ApplyClientDataManipulations(true);
        }

        /// <summary>
        /// Applies all Clientside related data manipulations, such as sorting, paging, groupBy and filtering.
        /// </summary>
        /// <param name="setSortedFilteredDataSource">True if the internal filtered datasource should be set.</param>
        protected bool ApplyClientDataManipulations(bool setSortedFilteredDataSource)
        {
            bool flag;
            if (base.ICollectionViewData == null)
            {
                flag = true;
            }
            else
            {
                flag = (base.ICollectionViewData.Groups == null ? true : base.MergeDataContexts.Count > 0);
            }
            if (!flag || base.SuspendInvalidateDataSource || !this._needSort && !this._needMerge && !this._needFiltering && !this._needGroupBy && !this._needPaging && !this._needSummary && !this._needConditionalFormat)
            {
                return false;
            }
            if (base.SummaryResultCollection != null)
            {
                base.SummaryResultCollection.Clear();
            }
            IList list = null;
            IQueryable<T> ts =
                from item in this._dataSource.AsQueryable<T>()
                select item;
            this.ApplySummary(ts,  SummaryExecution.PriorToFilteringAndPaging);
            this.GatherDataForConditionalFormatting(ts, EvaluationStage.PriorToFilteringAndPaging);
            IQueryable<T> ts1 = ts;
            IQueryable<GroupByDataContext> groupByDataContexts = null;
            if (this._needFiltering)
            {
                ts1 = this.ApplyFilter(ts1);
            }
            base.IsSorted = false;
            if (this._needSort)
            {
                ts1 = this.ApplySort(ts1);
                base.IsSorted = true;
            }
            if (this._needGroupBy)
            {
                base.GroupByObject.Summaries = base.Summaries;
                list = base.GroupByObject.Group<T>(ts1);
                ts1 = null;
            }
            if (this._needMerge && base.MergeDataContexts.Count > 0)
            {
                list = base.MergeDataContexts[0].Merge(ts1, base.MergeDataContexts, base.Summaries);
                ts1 = null;
            }
            if (ts1 != null)
            {
                this.ApplySummary(ts1,  SummaryExecution.AfterFilteringBeforePaging);
                this.GatherDataForConditionalFormatting(ts1, EvaluationStage.AfterFilteringBeforePaging);
            }
            if (this._needPaging && base.IPagedCollectionViewData == null)
            {
                int totalRecordCount = this.TotalRecordCount;
                if (!this._needGroupBy && !this._needMerge)
                {
                    if ((this._needSort || this._needFiltering) && this._cachedFilterSortedList == null)
                    {
                        this._cachedFilterSortedList = ts1.ToList<T>();
                    }
                    if (this._needFiltering)
                    {
                        totalRecordCount = this._cachedFilterSortedList.Count;
                    }
                }
                base.PageCount = (int)Math.Ceiling((double)totalRecordCount / (double)base.PageSize);
                int currentPage = base.CurrentPage;
                if (this._cachedFilterSortedList != null)
                {
                    ts1 = this._cachedFilterSortedList.Skip<T>(base.PageSize * (currentPage - 1)).Take<T>(base.PageSize).AsQueryable<T>();
                }
                else if (this._needGroupBy)
                {
                    List<GroupByDataContext> groupByDataContexts1 = list as List<GroupByDataContext>;
                    if (groupByDataContexts1 != null)
                    {
                        base.PageCount = (int)Math.Ceiling((double)list.Count / (double)base.PageSize);
                        currentPage = base.CurrentPage;
                        groupByDataContexts = groupByDataContexts1.AsQueryable<GroupByDataContext>();
                        groupByDataContexts = groupByDataContexts.Skip<GroupByDataContext>(base.PageSize * (currentPage - 1)).Take<GroupByDataContext>(base.PageSize);
                    }
                }
                else if (!this._needMerge)
                {
                    ts1 = ts1.Skip<T>(base.PageSize * (currentPage - 1)).Take<T>(base.PageSize);
                }
                else
                {
                    List<MergedRowInfo> mergedRowInfos = list as List<MergedRowInfo>;
                    if (mergedRowInfos != null)
                    {
                        base.PageCount = (int)Math.Ceiling((double)list.Count / (double)base.PageSize);
                        currentPage = base.CurrentPage;
                        IQueryable<MergedRowInfo> mergedRowInfos1 = mergedRowInfos.AsQueryable<MergedRowInfo>();
                        mergedRowInfos1 = mergedRowInfos1.Skip<MergedRowInfo>(base.PageSize * (currentPage - 1)).Take<MergedRowInfo>(base.PageSize);
                        list = mergedRowInfos1.ToList<MergedRowInfo>();
                    }
                }
            }
            if (groupByDataContexts != null)
            {
                list = groupByDataContexts.ToList<GroupByDataContext>();
            }
            else if (ts1 != null)
            {
                this.ApplySummary(ts1,  SummaryExecution.AfterFilteringAndPaging);
                this.GatherDataForConditionalFormatting(ts1, EvaluationStage.AfterFilteringAndPaging);
                list = ts1.ToList<T>();
            }
            if (setSortedFilteredDataSource)
            {
                this.SetSortedFilteredDataSource(list);
                this.OnDataUpdated();
            }
            int num = 0;
            bool flag1 = false; ///(bool)num;
            this._needMerge = false;//(bool)num;
            bool flag2 = flag1;
            bool flag3 = flag2;
            this._needPaging = flag2;
            bool flag4 = flag3;
            bool flag5 = flag4;
            this._needGroupBy = flag4;
            bool flag6 = flag5;
            bool flag7 = flag6;
            this._needFiltering = flag6;
            bool flag8 = flag7;
            bool flag9 = flag8;
            this._needSort = flag8;
            this._needSummary = flag9;
            return true;
        }

        private IQueryable<T> ApplyFilter(IQueryable<T> query)
        {
            if (base.Filters != null && base.Filters.Count > 0)
            {
                FilterContext filterContext = FilterContext.CreateGenericFilter(base.CachedTypedInfo, null, false, false);
                Expression currentExpression = ((IExpressConditions)base.Filters).GetCurrentExpression(filterContext);
                if (currentExpression != null)
                {
                    query = query.Where<T>((Expression<Func<T, bool>>)currentExpression);
                }
            }
            return query;
        }

        private void ApplyServerGrouping()
        {
            if (base.ICollectionViewData.Groups != null)
            {
                if (base.MergeDataContexts.Count > 0)
                {
                    MergedDataContext item = base.MergeDataContexts[0];
                    List<MergedRowInfo> mergedRowInfos = new List<MergedRowInfo>();
                    this.BuildMergedGroupings(mergedRowInfos, base.ICollectionViewData.Groups, new List<MergedColumnInfo>(), item, new Dictionary<MergedColumnInfo, bool>());
                    this.SetSortedFilteredDataSource(mergedRowInfos);
                    return;
                }
                List<GroupByDataContext> groupByDataContexts = new List<GroupByDataContext>();
                foreach (object group in base.ICollectionViewData.Groups)
                {
                    CollectionViewGroup collectionViewGroup = group as CollectionViewGroup;
                    if (collectionViewGroup == null)
                    {
                        continue;
                    }
                    GroupByDataContext groupByDataContext = new GroupByDataContext();
                    while (!collectionViewGroup.IsBottomLevel)
                    {
                        collectionViewGroup = collectionViewGroup.Items[0] as CollectionViewGroup;
                    }
                    groupByDataContext.Records = collectionViewGroup.Items;
                    groupByDataContext.Value = collectionViewGroup.Name;
                    groupByDataContexts.Add(groupByDataContext);
                }
                this.SetSortedFilteredDataSource(groupByDataContexts);
            }
        }

        private void ApplyServerPaging()
        {
            if (!base.IPagedCollectionViewData.IsPageChanging && base.IPagedCollectionViewData.TotalItemCount != -1)
            {
                int currentPage = base.CurrentPage - 1;
                if (currentPage != base.IPagedCollectionViewData.PageIndex && base.IPagedCollectionViewData.CanChangePage)
                {
                    base.IPagedCollectionViewData.MoveToPage(currentPage);
                }
            }
        }

        private IQueryable<T> ApplySort(IQueryable<T> query)
        {
            int count = base.Sort.Count;
            if (count == 0 && base.GroupBySortContext == null && base.MergeDataContexts.Count == 0)
            {
                return query;
            }
            IOrderedQueryable<T> ts = null;
            List<SortContext> sortContexts = new List<SortContext>();
            if (base.GroupBySortContext != null)
            {
                ts = base.GroupBySortContext.Sort<T>(query);
                sortContexts.Add(base.GroupBySortContext);
                if (count > 0)
                {
                    ts = base.Sort[0].AppendSort<T>(ts);
                    sortContexts.Add(base.Sort[0]);
                }
            }
            else if (base.MergeDataContexts.Count <= 0)
            {
                ts = base.Sort[0].Sort<T>(query);
                sortContexts.Add(base.Sort[0]);
            }
            else
            {
                int num = 0;
                foreach (MergedDataContext mergeDataContext in base.MergeDataContexts)
                {
                    ts = (num != 0 ? mergeDataContext.SortContext.AppendSort<T>(ts) : mergeDataContext.SortContext.Sort<T>(query));
                    num++;
                    sortContexts.Add(mergeDataContext.SortContext);
                }
                if (count > 0)
                {
                    ts = base.Sort[0].AppendSort<T>(ts);
                    sortContexts.Add(base.Sort[0]);
                }
            }
            for (int i = 1; i < count; i++)
            {
                ts = base.Sort[i].AppendSort<T>(ts);
                sortContexts.Add(base.Sort[i]);
            }
            this._multiSortComparer = new MultiSortComparer<T>(sortContexts, base.CachedTypedInfo);
            return ts;
        }

        private void ApplySummary(IQueryable<T> query,  SummaryExecution summaryExecution)
        {
            if (base.Summaries == null || base.SummaryResultCollection == null)
            {
                return;
            }
            foreach (SummaryDefinition definitionsBySummaryExecution in base.Summaries.GetDefinitionsBySummaryExecution(summaryExecution, summaryExecution == base.SummaryExecution))
            {
                ISupportLinqSummaries summaryCalculator = definitionsBySummaryExecution.SummaryOperand.SummaryCalculator as ISupportLinqSummaries;
                if (summaryCalculator == null)
                {
                    SynchronousSummaryCalculator synchronousSummaryCalculator = definitionsBySummaryExecution.SummaryOperand.SummaryCalculator as SynchronousSummaryCalculator;
                    if (synchronousSummaryCalculator == null)
                    {
                        continue;
                    }
                    base.SummaryResultCollection.Add(new SummaryResult(definitionsBySummaryExecution, synchronousSummaryCalculator.Summarize(query, definitionsBySummaryExecution.ColumnKey)));
                }
                else
                {
                    SummaryContext summaryContext = SummaryContext.CreateGenericSummary(base.CachedTypedInfo, definitionsBySummaryExecution.ColumnKey, summaryCalculator.SummaryType);
                    summaryCalculator.SummaryContext = summaryContext;
                    base.SummaryResultCollection.Add(new SummaryResult(definitionsBySummaryExecution, summaryContext.Execute(query)));
                }
            }
        }

        /// <summary>
        /// Goes through each grouping returned by an ICollectionView, and flattens it out for merged information.
        /// </summary>
        /// <param name="mris"></param>
        /// <param name="groups"></param>
        /// <param name="mcis"></param>
        /// <param name="mdc"></param>
        /// <param name="lastMciLookup"></param>
        /// <returns></returns>
        private List<object> BuildMergedGroupings(List<MergedRowInfo> mris, ReadOnlyObservableCollection<object> groups, List<MergedColumnInfo> mcis, MergedDataContext mdc, Dictionary<MergedColumnInfo, bool> lastMciLookup)
        {
            List<object> objs = new List<object>();
            int num = 0;
            int count = groups.Count;
            foreach (object group in groups)
            {
                bool flag = num == count - 1;
                CollectionViewGroup collectionViewGroup = group as CollectionViewGroup;
                if (collectionViewGroup == null)
                {
                    MergedRowInfo mergedRowInfo = new MergedRowInfo()
                    {
                        Data = group
                    };
                    foreach (MergedColumnInfo mci in mcis)
                    {
                        if (!flag)
                        {
                            mergedRowInfo.IsLastRowInGroup.Add(mci.MergingObject, false);
                        }
                        else if (!lastMciLookup.ContainsKey(mci))
                        {
                            mergedRowInfo.IsLastRowInGroup.Add(mci.MergingObject, true);
                        }
                        else
                        {
                            mergedRowInfo.IsLastRowInGroup.Add(mci.MergingObject, lastMciLookup[mci]);
                        }
                    }
                    mergedRowInfo.MergedGroups = mcis;
                    mris.Add(mergedRowInfo);
                    objs.Add(group);
                }
                else
                {
                    MergedColumnInfo mergedColumnInfo = new MergedColumnInfo()
                    {
                        Key = collectionViewGroup.Name,
                        Summaries = base.Summaries,
                        TypedInfo = base.CachedTypedInfo
                    };
                    MergedColumnInfo item = mergedColumnInfo;
                    if (mcis.Count > 0)
                    {
                        item.ParentMergedColumnInfo = mcis[mcis.Count - 1];
                    }
                    item.DataType = base.CachedType;
                    List<MergedColumnInfo> mergedColumnInfos = new List<MergedColumnInfo>(mcis)
                    {
                        item
                    };
                    item.MergingObject = mdc.MergedObject;
                    MergedDataContext mergedDataContext = null;
                    int num1 = base.MergeDataContexts.IndexOf(mdc);
                    if (num1 != base.MergeDataContexts.Count - 1)
                    {
                        mergedDataContext = base.MergeDataContexts[num1 + 1];
                    }
                    if (item.ParentMergedColumnInfo != null)
                    {
                        if (!lastMciLookup.ContainsKey(item.ParentMergedColumnInfo))
                        {
                            lastMciLookup.Add(item.ParentMergedColumnInfo, flag);
                        }
                        else
                        {
                            lastMciLookup[item.ParentMergedColumnInfo] = flag;
                        }
                    }
                    List<object> objs1 = this.BuildMergedGroupings(mris, collectionViewGroup.Items, mergedColumnInfos, mergedDataContext, lastMciLookup);
                    objs.AddRange(objs1);
                    item.Children = objs1;
                }
                num++;
            }
            return objs;
        }

        /// <summary>
        /// Clears any cached information that the manager keeps.
        /// </summary>
        protected override void ClearCachedDataSource(bool invalidateTotalRowCount)
        {
            this._cachedFilterSortedList = null;
            IList sortedFilteredDataSource = base.SortedFilteredDataSource;
            base.ClearCachedDataSource(invalidateTotalRowCount);
            if (sortedFilteredDataSource != null && base.SortedFilteredDataSource == null && !base.Defer)
            {
                this.OnDataUpdated();
            }
        }

        /// <summary>
        /// Filters a list of items using the filter operands applied on the <see cref="T: DataManagerBase" />.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns>Filtered list or null if filtering cannot be applied.</returns>
        /// <remarks>
        /// This method is added solely for the purpose of fast adding of an item in sorted/filtered list.
        /// </remarks>
        internal override IList FilterItems(IList items)
        {
            IQueryable<T> ts = items.OfType<T>().AsQueryable<T>();
            return this.ApplyFilter(ts).ToList<T>();
        }

        /// <summary>
        /// FIlters an IEnumerable using provided filter items.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <param name="filtersToUse">Filters to use during fitlering.</param>
        /// <returns>Filtered list or null if filtering cannot be applied.</returns>
        internal override IList FilterItems(IEnumerable items, RecordFilterCollection filtersToUse)
        {
            IQueryable<T> ts = items.OfType<T>().AsQueryable<T>();
            if (filtersToUse != null && filtersToUse.Count > 0)
            {
                FilterContext filterContext = FilterContext.CreateGenericFilter(base.CachedTypedInfo, null, false, false);
                Expression currentExpression = ((IExpressConditions)filtersToUse).GetCurrentExpression(filterContext);
                if (currentExpression != null)
                {
                    ts = ts.Where<T>((Expression<Func<T, bool>>)currentExpression);
                }
            }
            return ts.ToList<T>();
        }

        private void GatherDataForConditionalFormatting(IQueryable<T> query, EvaluationStage stage)
        {
            if (base.ConditionalFormattingRules == null || base.ConditionalFormattingRules.Count == 0)
            {
                return;
            }
            ReadOnlyCollection<IRule> rulesForStage = base.ConditionalFormattingRules.GetRulesForStage(stage);
            if (rulesForStage.Count == 0)
            {
                return;
            }
            foreach (IRule rule in rulesForStage)
            {
                rule.GatherData(query);
            }
        }

        /// <summary>
        /// Gets the <see cref="P: DataManagerBase.DataSource" /> associated with this <see cref="T: DataManagerBase" />.
        /// </summary>
        protected override IEnumerable GetDataSource()
        {
            return this._dataSource;
        }

        /// <summary>
        /// Triggered when the underlying data source's data is changed.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnDataSourceCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this._sortDescriptionsClearing)
            {
                return;
            }
            if (base.ICollectionViewData == null)
            {
                base.OnDataSourceCollectionChanged(e);
                return;
            }
            this.TotalRecordCount = -1;
            this.RecordCount = -1;
            this.OnCollectionChanged(e);
            if (base.ICollectionViewData.Groups != null)
            {
                this.ApplyServerGrouping();
            }
            if (base.Defer || base.ICollectionViewData.Groups != null)
            {
                if (base.IPagedCollectionViewData != null)
                {
                    this.ApplyServerPaging();
                }
                this.ApplyClientDataManipulations();
                this.OnDataUpdated();
            }
            else
            {
                base.OnDataSourceCollectionChanged(e);
            }
            base.Defer = false;
        }

        /// <summary>
        /// Raises the ResolvingData event.
        /// </summary>
        /// <param name="args"></param>
        protected internal override void OnResolvingData(DataAcquisitionEventArgs args)
        {
            base.OnResolvingData(args);
            if (args != null)
            {
                args.Handled = args.DataSource != null;
            }
        }

        /// <summary>
        /// Reevaluates the summaries for the ItemsSource bound to this <see cref="T: DataManagerBase" />.
        /// </summary>
        public override void RefreshSummaries()
        {
            this._needSummary = true;
            this._needSort = true;
            this._needPaging = (!base.EnablePaging ? false : base.PageSize > 0);
            this._needFiltering = true;
            this._needGroupBy = base.GroupByObject != null;
            base.SummaryResultCollection.Clear();
            this.ApplyClientDataManipulations(false);
        }

        /// <summary>
        /// Determines the size of the collection by walking through the DataSource.
        /// </summary>
        /// <returns></returns>
        protected override int ResolveCount()
        {
            if (this.IListData != null)
                return this.IListData.Count;
            return this._dataSource.Select<T, T>((Func<T, T>)(a => a)).Count<T>();


   //         if (base.IListData != null)
            //{
            //	return base.IListData.Count;
            //}
            //return (
            //	from s in this._dataSource

            //	select a).Count<T>();
        }

        /// <summary>
        /// Uses the existing paging, sorting, and filtering information to build a cached object for the
        /// DataManagerBase to use.
        /// </summary>
        protected override void ResolveFilteredSortedPagedDataSource()
        {
            bool canSort = false;
            bool canFilter = false;
            bool flag = false;
            bool flag1 = false;
            bool flag2 = false;
            bool flag3 = false;
            bool groupByObject = false;
            bool flag4 = false;
            bool flag5 = false;
            bool flag6 = false;
            if (base.SummaryResultCollection != null)
            {
                base.SummaryResultCollection.Clear();
            }
            if (base.ICollectionViewData != null && base.AllowCollectionViewOverrides)
            {
                canSort = base.ICollectionViewData.CanSort;
                bool canGroup = base.ICollectionViewData.CanGroup;
                flag1 = canGroup;
                flag2 = canGroup;
                if (canSort)
                {
                    SortDescriptionCollection sortDescriptions = base.ICollectionViewData.SortDescriptions;
                    int count = base.Sort.Count + (base.GroupBySortContext != null ? 1 : 0);
                    count = count + base.MergeDataContexts.Count;
                    if (sortDescriptions.Count == count)
                    {
                        int num = 0;
                        if (base.GroupBySortContext != null)
                        {
                            SortDescription item = sortDescriptions[0];
                            if (item.Direction == ListSortDirection.Ascending != base.GroupBySortContext.SortAscending || item.PropertyName != base.GroupBySortContext.SortPropertyName)
                            {
                                flag3 = true;
                            }
                            num = 1;
                        }
                        foreach (MergedDataContext mergeDataContext in base.MergeDataContexts)
                        {
                            SortDescription sortDescription = sortDescriptions[num];
                            if (sortDescription.Direction == ListSortDirection.Ascending != mergeDataContext.SortContext.SortAscending || sortDescription.PropertyName != mergeDataContext.SortContext.SortPropertyName)
                            {
                                flag3 = true;
                            }
                            num++;
                        }
                        int count1 = base.Sort.Count;
                        int num1 = 0;
                        while (num1 < count1)
                        {
                            SortContext sortContext = base.Sort[num1];
                            SortDescription item1 = sortDescriptions[num1 + num];
                            if (sortContext.SortAscending != (item1.Direction == ListSortDirection.Ascending) || item1.PropertyName != sortContext.SortPropertyName)
                            {
                                flag3 = true;
                                break;
                            }
                            else
                            {
                                num1++;
                            }
                        }
                    }
                    else
                    {
                        flag3 = true;
                    }
                }
                if (flag1)
                {
                    if (base.GroupByObject != null)
                    {
                        if (base.ICollectionViewData.GroupDescriptions.Count == 0 || ((PropertyGroupDescription)base.ICollectionViewData.GroupDescriptions[0]).PropertyName != base.GroupByObject.PropertyName)
                        {
                            groupByObject = true;
                        }
                    }
                    else if (base.ICollectionViewData.GroupDescriptions != null && base.ICollectionViewData.GroupDescriptions.Count > 0 && base.MergeDataContexts.Count == 0)
                    {
                        groupByObject = true;
                    }
                }
                if (flag2 && base.GroupByObject == null)
                {
                    if (base.SummariesDirty && base.MergeDataContexts.Count > 0)
                    {
                        flag6 = true;
                    }
                    else if (base.MergeDataContexts.Count == base.ICollectionViewData.GroupDescriptions.Count)
                    {
                        int num2 = 0;
                        foreach (MergedDataContext mergedDataContext in base.MergeDataContexts)
                        {
                            PropertyGroupDescription propertyGroupDescription = base.ICollectionViewData.GroupDescriptions[num2] as PropertyGroupDescription;
                            if (propertyGroupDescription == null || mergedDataContext.PropertyName != propertyGroupDescription.PropertyName)
                            {
                                flag6 = true;
                                break;
                            }
                            else
                            {
                                num2++;
                            }
                        }
                    }
                    else
                    {
                        flag6 = true;
                    }
                }
            }
            if (base.IFilteredCollectionViewData != null && base.Filters != null)
            {
                canFilter = base.IFilteredCollectionViewData.CanFilter;
                if (canFilter && base.Filters.Count == 0 && base.IFilteredCollectionViewData.FilterConditions != null && base.IFilteredCollectionViewData.FilterConditions.Count > 0)
                {
                    flag4 = true;
                }
            }
            if (base.IPagedCollectionViewData != null)
            {
                flag = true;
            }
            if (flag3 || groupByObject || flag4 || flag6)
            {
                if (!flag3 || groupByObject || flag4 || base.Sort.Count != 0 || base.GroupBySortContext != null || base.MergeDataContexts.Count != 0)
                {
                    using (IDisposable disposable = base.ICollectionViewData.DeferRefresh())
                    {
                        if (flag3)
                        {
                            base.ICollectionViewData.SortDescriptions.Clear();
                            int count2 = base.Sort.Count;
                            if (count2 != 0 || base.GroupBySortContext != null || base.MergeDataContexts.Count > 0)
                            {
                                int num3 = 0;
                                if (base.GroupBySortContext != null)
                                {
                                    base.ICollectionViewData.SortDescriptions.Add(new SortDescription(base.GroupBySortContext.SortPropertyName, (base.GroupBySortContext.SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending)));
                                }
                                else if (base.MergeDataContexts.Count <= 0)
                                {
                                    base.ICollectionViewData.SortDescriptions.Add(new SortDescription(base.Sort[0].SortPropertyName, (base.Sort[0].SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending)));
                                    num3 = 1;
                                }
                                else
                                {
                                    foreach (MergedDataContext mergeDataContext1 in base.MergeDataContexts)
                                    {
                                        base.ICollectionViewData.SortDescriptions.Add(new SortDescription(mergeDataContext1.SortContext.SortPropertyName, (mergeDataContext1.SortContext.SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending)));
                                    }
                                }
                                for (int i = num3; i < count2; i++)
                                {
                                    base.ICollectionViewData.SortDescriptions.Add(new SortDescription(base.Sort[i].SortPropertyName, (base.Sort[i].SortAscending ? ListSortDirection.Ascending : ListSortDirection.Descending)));
                                }
                            }
                        }
                        this._needFiltering = false;
                        if (groupByObject)
                        {
                            if (base.GroupByObject == null)
                            {
                                base.ICollectionViewData.GroupDescriptions.Clear();
                            }
                            else
                            {
                                base.ICollectionViewData.GroupDescriptions.Clear();
                                base.ICollectionViewData.GroupDescriptions.Add(new PropertyGroupDescription(base.GroupByObject.PropertyName));
                            }
                        }
                        if (flag6)
                        {
                            base.ICollectionViewData.GroupDescriptions.Clear();
                            foreach (MergedDataContext mergedDataContext1 in base.MergeDataContexts)
                            {
                                base.ICollectionViewData.GroupDescriptions.Add(new PropertyGroupDescription(mergedDataContext1.PropertyName));
                            }
                        }
                        base.Defer = true;
                    }
                }
                else
                {
                    this._sortDescriptionsClearing = true;
                    base.Defer = true;
                    base.ICollectionViewData.SortDescriptions.Clear();
                    this._sortDescriptionsClearing = false;
                }
                int num4 = 0;
                groupByObject = false;//(bool)num4;
                flag4 = false;// (bool)num4;
                flag3 = false; // (bool)num4;
            }
            bool flag7 = false;
            if (flag && !base.Defer)
            {
                this.ApplyServerPaging();
                bool isPageChanging = base.IPagedCollectionViewData.IsPageChanging;
                flag5 = isPageChanging;
                base.Defer = isPageChanging;
            }
            if (!base.SupportsDataManipulations)
            {
                flag3 = false;
            }
            else if (base.IPagedCollectionViewData == null && base.IFilteredCollectionViewData == null && base.ICollectionViewData == null)
            {
                DataAcquisitionEventArgs dataAcquisitionEventArg = new DataAcquisitionEventArgs()
                {
                    CurrentPage = base.CurrentPage,
                    EnablePaging = base.EnablePaging,
                    PageSize = base.PageSize,
                    CurrentSort = new ObservableCollection<SortContext>(),
                    GroupByContext = base.GroupByObject,
                    Filters = base.Filters
                };
                DataAcquisitionEventArgs dataAcquisitionEventArg1 = dataAcquisitionEventArg;
                foreach (SortContext sort in base.Sort)
                {
                    dataAcquisitionEventArg1.CurrentSort.Add(sort);
                }
                this.OnResolvingData(dataAcquisitionEventArg1);
                if (dataAcquisitionEventArg1.Handled)
                {
                    this.SetSortedFilteredDataSource(dataAcquisitionEventArg1.DataSource);
                    this.OnDataUpdated();
                    return;
                }
            }
            if (!canSort)
            {
                flag3 = (base.Sort.Count > 0 || base.GroupByObject != null ? true : base.MergeDataContexts.Count > 0);
            }
            if (!canFilter && (!flag1 || flag1 && base.GroupByObject == null))
            {
                flag4 = (base.Filters == null ? false : base.Filters.Count > 0);
            }
            if (!flag1)
            {
                groupByObject = base.GroupByObject != null;
            }
            if (!flag)
            {
                flag5 = (!base.EnablePaging ? false : base.PageSize > 0);
            }
            this._needSummary = (base.Summaries == null ? false : base.Summaries.Count > 0);
            this._needConditionalFormat = (base.ConditionalFormattingRules == null ? false : base.ConditionalFormattingRules.Count > 0);
            if (!base.SupportsDataManipulations)
            {
                flag3 = false;
            }
            this._needFiltering = flag4;
            this._needGroupBy = groupByObject;
            this._needSort = flag3;
            this._needPaging = flag5;
            this._needMerge = flag6;
            if (base.MergeDataContexts.Count > 0 && !flag2)
            {
                this._needMerge = true;
                this._needSort = true;
            }
            if (!base.Defer)
            {
                bool updateDataSource = base.UpdateDataSource;
                if (flag1 && (base.MergeDataContexts.Count > 0 || base.ICollectionViewData.Groups != null && base.ICollectionViewData.Groups.Count > 0))
                {
                    this.ApplyServerGrouping();
                    updateDataSource = false;
                }
                if (!this.ApplyClientDataManipulations(updateDataSource) && flag7)
                {
                    this.OnDataUpdated();
                }
            }
            base.SummariesDirty = false;
        }

        /// <summary>
        /// Looks through the filtered DataSource for the index of the item specified. 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isAdding"></param>
        /// <returns></returns>
        internal override int? ResolveIndexForInsertOrDelete(object data, bool isAdding)
        {
            if (base.IsSortedFilteredDataSourceCalculated && base.IsSorted && this._multiSortComparer != null)
            {
                List<T> sortedFilteredDataSource = base.SortedFilteredDataSource as List<T>;
                if (sortedFilteredDataSource != null && data is T)
                {
                    return this.ResolveIndexForInsertOrDelete(sortedFilteredDataSource, (T)data, isAdding);
                }
            }
            return null;
        }

        private int? ResolveIndexForInsertOrDelete(List<T> list, T data, bool isAdding)
        {
            int? nullable = null;
            if (base.IsSortedFilteredDataSourceCalculated && base.IsSorted && this._multiSortComparer != null)
            {
                int num = list.BinarySearch(data, this._multiSortComparer);
                if (!isAdding)
                {
                    bool flag = false;
                    if (num >= 0)
                    {
                        flag = true;
                    }
                    else
                    {
                        int count = list.Count;
                        int num1 = ~num;
                        int num2 = num1 - 1;
                        int num3 = num1 + 1;
                        bool flag1 = false;
                        bool flag2 = false;
                        while (!flag1 || !flag2)
                        {
                            if (num2 < 0)
                            {
                                flag1 = true;
                            }
                            else if ((object)list[num2] != (object)data)
                            {
                                num2--;
                            }
                            else
                            {
                                num = num2;
                                flag = true;
                                goto Label0;
                            }
                            if (num3 >= count)
                            {
                                flag2 = true;
                            }
                            else if ((object)list[num3] != (object)data)
                            {
                                num3++;
                            }
                            else
                            {
                                num = num3;
                                flag = true;
                                goto Label0;
                            }
                        }
                    }
                    Label0:
                    if (flag)
                    {
                        nullable = new int?(num);
                    }
                }
                else if (num < 0)
                {
                    nullable = new int?(~num);
                }
            }
            return nullable;
        }

        /// <summary>
        /// Resolve the specified record at a given index. 
        /// </summary>
        /// <param name="recordIndex"></param>
        /// <returns></returns>
        /// <remarks> This method only gets called, if the data source isn't of type IList.</remarks>
        protected override object ResolveRecord(int recordIndex)
        {
            IQueryable<T> ts = this._dataSource.AsQueryable<T>();
            return ts.Skip<T>(recordIndex).Take<T>(1).ToList<T>()[0];
        }

        /// <summary>
        /// Sets the <see cref="P: DataManagerBase.DataSource" /> while registering for change notification.
        /// </summary>
        /// <param name="source">The IEnumerable to set the DataSource to.</param>
        protected override void SetDataSource(IEnumerable source)
        {
            if (source == null)
            {
                this._dataSource = null;
                base.OriginalDataSource = null;
            }
            else
            {
                IEnumerable<T> ts = source as IEnumerable<T>;
                if (ts != null)
                {
                    this._dataSource = ts;
                }
                else
                {
                    this._dataSource = source.Cast<T>();
                }
                base.OriginalDataSource = source;
            }
            base.SetDataSource(this._dataSource);
        }

        /// <summary>
        /// Updates the cached data manipulations.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="isAdding">if set to <c>true</c> the item will be added to the cached lists, otherwise removed.</param>
        /// <returns><c>true</c> if the cache was updated, otherwise <c>false</c>.</returns>
        internal override bool UpdateCachedDataManipulations(object item, bool isAdding)
        {
            bool flag = base.UpdateCachedDataManipulations(item, isAdding);
            if (this._cachedFilterSortedList != null && this._multiSortComparer != null && item is T)
            {
                bool flag1 = false;
                T t = (T)item;
                int? nullable = this.ResolveIndexForInsertOrDelete(this._cachedFilterSortedList, t, isAdding);
                if (nullable.HasValue)
                {
                    if (!isAdding)
                    {
                        this._cachedFilterSortedList.RemoveAt(nullable.Value);
                    }
                    else
                    {
                        this._cachedFilterSortedList.Insert(nullable.Value, t);
                    }
                    flag1 = true;
                    flag = true;
                }
                if (flag1)
                {
                    this._needSort = true;
                    this._needPaging = true;
                    this._needFiltering = true;
                    this.ApplyClientDataManipulations(true);
                }
            }
            return flag;
        }
    }
}
