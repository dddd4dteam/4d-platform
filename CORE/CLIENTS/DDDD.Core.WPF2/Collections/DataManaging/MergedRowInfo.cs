﻿using System.Collections.Generic;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that stores the merge information for a particualr data row from an items source.
    /// </summary>
    public class MergedRowInfo
    {
        private Dictionary<object, bool> _lastRowInGroup = new Dictionary<object, bool>();

        private Dictionary<object, bool> _firstRowInGroup = new Dictionary<object, bool>();

        /// <summary>
        /// The underlying data object that this row object represents
        /// </summary>
        public object Data
        {
            get;
            set;
        }

        /// <summary>
        /// A lookup table for whether a specific key of a MergedColumnInfo, that this row is the first one in its grouping.
        /// </summary>
        public Dictionary<object, bool> IsFirstRowInGroup
        {
            get
            {
                return this._firstRowInGroup;
            }
        }

        /// <summary>
        /// A lookup table for whether a specific key of a MergedColumnInfo, that this row is the last one in its grouping.
        /// </summary>
        public Dictionary<object, bool> IsLastRowInGroup
        {
            get
            {
                return this._lastRowInGroup;
            }
        }

        /// <summary>
        /// A collection of MergeColumnInfo that this row has been merged by.
        /// </summary>
        public List<MergedColumnInfo> MergedGroups
        {
            get;
            set;
        }

        public MergedRowInfo()
        {
        }

        /// <summary>
        /// Does an equals comparison on the underlying data, not the MergedRowInfo object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            MergedRowInfo mergedRowInfo = obj as MergedRowInfo;
            if (this.Data == null || mergedRowInfo == null)
            {
                return this.Equals(obj);
            }
            return this.Data.Equals(mergedRowInfo.Data);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (this.Data == null)
            {
                return this.GetHashCode();
            }
            return this.Data.GetHashCode();
        }
    }
}
