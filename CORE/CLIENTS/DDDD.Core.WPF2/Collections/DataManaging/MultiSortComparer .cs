﻿
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using DDDD.Core.Caching.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Compares items based on list of <see cref="T: SortContext" />s. Read remarks.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <remarks>
    /// This comparer is used for binary searching. It doesn't always return a preciese result.
    /// First the items will be compared based on SortContext.(SortProperty and SortAscending).
    /// Then if the items have the same properties they will be compared for equality,
    /// and if they are not equal -1 will be returned.
    /// Intended for use with <see cref="M:System.Collections.Generic.List`1.BinarySearch(`0,System.Collections.Generic.IComparer{`0})" />.
    /// </remarks>
    internal class MultiSortComparer<T> : IComparer<T>
	{
		private readonly IList<SortContext> _sortContexts;

		private Func<T, T, int> _compare;

		private bool _isCompiled;

		private readonly CachedTypedInfo _cachedTypedInfo;

		/// <summary>
		/// Initializes a new instance of the <see cref="T: MultiSortComparer`1" /> class.
		/// </summary>
		/// <param name="sortContexts">The sort contexts.</param>
		public MultiSortComparer(IList<SortContext> sortContexts, CachedTypedInfo cachedTypedInfo)
		{
			this._sortContexts = sortContexts;
			this._cachedTypedInfo = cachedTypedInfo;
		}

		private Expression BuildCompareCondition(SortContext sortContext, ParameterExpression param1, ParameterExpression param2)
		{
			Expression expression;
			MemberExpression memberExpression = Expression.Property(Expression.Constant(sortContext), "Comparer");
			Type propertyType = sortContext.PropertyType;
			Type type = typeof(IComparer<>).MakeGenericType(new Type[] { propertyType });
			MethodInfo method = type.GetMethod("Compare");
			if (sortContext.SortPropertyName != null)
			{
				Expression expression1 = DataManagerBase.BuildPropertyExpressionFromPropertyName(sortContext.SortPropertyName, param1, this._cachedTypedInfo, propertyType, null);
				Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(sortContext.SortPropertyName, param2, this._cachedTypedInfo, propertyType, null);
				Type type1 = typeof(Comparer<>).MakeGenericType(new Type[] { propertyType });
				Type[] typeArray = new Type[] { propertyType, propertyType };
				MethodInfo methodInfo = type1.GetMethod("Compare", typeArray);
				MemberExpression memberExpression1 = Expression.Property(null, type1, "Default");
				Expression expression3 = Expression.Call(memberExpression1, methodInfo, expression1, expression2);
				Expression expression4 = Expression.Call(memberExpression, method, expression1, expression2);
				Expression expression5 = Expression.Equal(memberExpression, Expression.Constant(null, type));
				expression = Expression.Condition(expression5, expression3, expression4);
			}
			else
			{
				expression = Expression.Call(memberExpression, method, param1, param2);
			}
			if (!sortContext.SortAscending)
			{
				expression = Expression.Multiply(expression, Expression.Constant(-1, typeof(int)));
			}
			return expression;
		}

		/// <summary>
		/// Builds compare expression.
		/// </summary>
		/// <param name="sortContexts">The sort contexts.</param>
		/// <param name="resultVar">The result var.</param>
		/// <param name="p1">The p1.</param>
		/// <param name="p2">The p2.</param>
		/// <returns></returns>
		/// <remarks>
		/// resultVar will contain the result of the comparsion.
		/// -1 if the position of p1 is before p2 in the sorted list.
		/// 0 if p1 and p2 are equal
		/// +1 if the position of p1 is after p2 in the sorted list.
		/// </remarks>
		private Expression BuildCompareExpression(IList<SortContext> sortContexts, Expression resultVar, ParameterExpression p1, ParameterExpression p2)
		{
			int count = sortContexts.Count - 1;
			Expression expression = null;
			for (int i = count; i >= 0; i--)
			{
				SortContext item = sortContexts[i];
				if (count != i)
				{
					BinaryExpression binaryExpression = Expression.Equal(resultVar, Expression.Constant(0, typeof(int)));
					expression = Expression.Block(Expression.Assign(resultVar, this.BuildCompareCondition(item, p1, p2)), Expression.IfThen(binaryExpression, expression));
				}
				else
				{
					expression = Expression.Assign(resultVar, this.BuildCompareCondition(item, p1, p2));
				}
			}
			return expression;
		}

		public int Compare(T x, T y)
		{
			object obj = x;
			object obj1 = y;
			if (obj == null && obj1 == null)
			{
				return 0;
			}
			if (obj == null)
			{
				return -1;
			}
			if (obj1 == null)
			{
				return 1;
			}
			if (!this._isCompiled)
			{
				this.Compile();
			}
			int num = this._compare(x, y);
			if (num != 0)
			{
				return num;
			}
			if (object.ReferenceEquals(x, y))
			{
				return 0;
			}
			return -1;
		}

		private void Compile()
		{
			ParameterExpression p1 = Expression.Parameter(typeof(T), "p1");
			ParameterExpression p2 = Expression.Parameter(typeof(T), "p2");
			ParameterExpression parameterExpression = Expression.Variable(typeof(int), "result");

			//TO DO   [12/14/2016 A1]

			//this._compare = (
			//    (Expression<Func<T, T, int>>)( 
			//        (parameterExpression1, parameterExpression2) => 
			//        Expression.Block(
			//           (IEnumerable<ParameterExpression>)new ParameterExpression[1]
			//           {
			//                parameterExpression
			//           }
			//           , new Expression[2]
			//          {
			//            this.BuildCompareExpression(this._sortContexts, (Expression) parameterExpression, p1, p2)
			//          , (Expression) parameterExpression
			//          })
			//       )
			//       ).Compile();
			this._isCompiled = true;
		}
	}
}
