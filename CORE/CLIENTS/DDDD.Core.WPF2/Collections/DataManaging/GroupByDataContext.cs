﻿
using System.Linq;
using System.Collections;
using System.Collections.Generic;


using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that contains the informaton of data that has been grouped by the <see cref="T: GroupByContext" />
    /// </summary>
    public class GroupByDataContext
    {
        private SummaryResultCollection _summaryResultCollection;

        private SummaryResultCollection _groupBySummaryResultCollection;

        private Dictionary<string, SummaryResultCollection> _groupBySummariesLookup;

        private Dictionary<string, SummaryResultCollection> _summaryLookup;

        /// <summary>
        /// Gets the total amount of records in this particular grouping.
        /// </summary>
        public int Count
        {
            get
            {
                if (this.Records == null)
                {
                    return 0;
                }
                return this.Records.Count;
            }
        }

        /// <summary>
        /// Gets the string representation of the value, with the Count appended to it. 
        /// </summary>
        public string DisplayValue
        {
            get
            {
                string displayValueStringFormat = this.DisplayValueStringFormat;
                object value = this.Value;
                if (value == null)
                {
                    value = string.Empty;
                }
                return string.Format(displayValueStringFormat, value, this.Count);
            }
        }

        internal string DisplayValueStringFormat
        {
            get
            {
                return "{0}: ({1})";
            }
        }

        /// <summary>
        /// Gets a list of the summaries that should be applied specifically for GroupBy.
        /// </summary>
        protected internal SummaryDefinitionCollection GroupBySummaries
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a lookup table of the GroupBySummaryResults based on a column key.
        /// </summary>
        public Dictionary<string, SummaryResultCollection> GroupBySummaryLookupResults
        {
            get
            {
                if (this.GroupBySummaryResults == null)
                {
                    return new Dictionary<string, SummaryResultCollection>();
                }
                return this._groupBySummariesLookup;
            }
        }

        /// <summary>
        /// Summary Results specific to the particular field that this GroupByContext represents.
        /// </summary>
        public SummaryResultCollection GroupBySummaryResults
        {
            get
            {
                if (this._groupBySummaryResultCollection == null)
                {
                    this._groupBySummariesLookup = new Dictionary<string, SummaryResultCollection>();
                    if (this.GroupBySummaries.Count <= 0)
                    {
                        return new SummaryResultCollection();
                    }
                    this._groupBySummaryResultCollection = new SummaryResultCollection();
                    IQueryable queryables = this.Records.AsQueryable();
                    foreach (SummaryDefinition groupBySummary in this.GroupBySummaries)
                    {
                        if (!this._groupBySummariesLookup.ContainsKey(groupBySummary.ColumnKey))
                        {
                            this._groupBySummariesLookup.Add(groupBySummary.ColumnKey, new SummaryResultCollection());
                        }
                        SummaryResultCollection item = this._groupBySummariesLookup[groupBySummary.ColumnKey];
                        SummaryResult summaryResult = null;
                        ISupportLinqSummaries summaryCalculator = groupBySummary.SummaryOperand.SummaryCalculator as ISupportLinqSummaries;
                        if (summaryCalculator == null)
                        {
                            SynchronousSummaryCalculator synchronousSummaryCalculator = groupBySummary.SummaryOperand.SummaryCalculator as SynchronousSummaryCalculator;
                            if (synchronousSummaryCalculator != null)
                            {
                                summaryResult = new SummaryResult(groupBySummary, synchronousSummaryCalculator.Summarize(queryables, groupBySummary.ColumnKey));
                            }
                        }
                        else
                        {
                            SummaryContext summaryContext = SummaryContext.CreateGenericSummary(this.TypedInfo, groupBySummary.ColumnKey, summaryCalculator.SummaryType);
                            summaryCalculator.SummaryContext = summaryContext;
                            summaryResult = new SummaryResult(groupBySummary, summaryContext.Execute(queryables));
                        }
                        if (summaryResult == null)
                        {
                            continue;
                        }
                        this._groupBySummaryResultCollection.Add(summaryResult);
                        item.Add(summaryResult);
                    }
                }
                return this._groupBySummaryResultCollection;
            }
        }

        /// <summary>
        /// Gets a collection of data that belongs to this particular grouping.
        /// </summary>
        public IList Records
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a list of the summaries that should be applied to Children.
        /// </summary>
        protected internal SummaryDefinitionCollection Summaries
        {
            get;
            set;
        }

        /// <summary>
        /// Gets a lookup table of the SummaryResults based on a column key.
        /// </summary>
        public Dictionary<string, SummaryResultCollection> SummaryLookupResults
        {
            get
            {
                if (this.SummaryResults == null)
                {
                    return new Dictionary<string, SummaryResultCollection>();
                }
                return this._summaryLookup;
            }
        }

        /// <summary>
        /// Summary results for that were specified for all fields.
        /// </summary>
        public SummaryResultCollection SummaryResults
        {
            get
            {
                if (this._summaryResultCollection == null)
                {
                    if (this.Summaries.Count <= 0)
                    {
                        return new SummaryResultCollection();
                    }
                    this._summaryResultCollection = new SummaryResultCollection();
                    this._summaryLookup = new Dictionary<string, SummaryResultCollection>();
                    IQueryable queryables = this.Records.AsQueryable();
                    foreach (SummaryDefinition summary in this.Summaries)
                    {
                        if (!this._summaryLookup.ContainsKey(summary.ColumnKey))
                        {
                            this._summaryLookup.Add(summary.ColumnKey, new SummaryResultCollection());
                        }
                        SummaryResultCollection item = this._summaryLookup[summary.ColumnKey];
                        SummaryResult summaryResult = null;
                        ISupportLinqSummaries summaryCalculator = summary.SummaryOperand.SummaryCalculator as ISupportLinqSummaries;
                        if (summaryCalculator == null)
                        {
                            SynchronousSummaryCalculator synchronousSummaryCalculator = summary.SummaryOperand.SummaryCalculator as SynchronousSummaryCalculator;
                            if (synchronousSummaryCalculator != null)
                            {
                                summaryResult = new SummaryResult(summary, synchronousSummaryCalculator.Summarize(queryables, summary.ColumnKey));
                            }
                        }
                        else
                        {
                            SummaryContext summaryContext = SummaryContext.CreateGenericSummary(this.TypedInfo, summary.ColumnKey, summaryCalculator.SummaryType);
                            summaryCalculator.SummaryContext = summaryContext;
                            summaryResult = new SummaryResult(summary, summaryContext.Execute(queryables));
                        }
                        if (summaryResult == null)
                        {
                            continue;
                        }
                        this._summaryResultCollection.Add(summaryResult);
                        item.Add(summaryResult);
                    }
                }
                return this._summaryResultCollection;
            }
        }

        /// <summary>
        /// Gets/Sets the TypedInfo for the object.
        /// </summary>
        protected internal CachedTypedInfo TypedInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the value of the that the data has been grouped by.
        /// </summary>
        public object Value
        {
            get;
            internal set;
        }

        public GroupByDataContext()
        {
        }

        internal void Reload()
        {
            this._summaryResultCollection = null;
            this._groupBySummaryResultCollection = null;
        }
    }
}
