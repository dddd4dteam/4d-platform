﻿
using System;
using System.Linq.Expressions;

using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Condition.IG;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{

    /// <summary>
    /// A nongeneric abstract class representing a filter on an object.
    /// Cast up to CustomFilter{T} to get an expression representing the filter.
    /// </summary>
    public abstract class FilterContext
    {

        #region ------------------------ CTOR --------------------------

        protected FilterContext()
        {
        }

        #endregion ------------------------ CTOR --------------------------

        /// <summary>
        /// The CachedTypedInfo for the opeartion
        /// </summary>
        protected CachedTypedInfo CachedTypedInfo
        { get; set; }
       
        /// <summary>
        /// Gets if the filters being built will be case sensitive or not.
        /// </summary>
        public bool CaseSensitive
        {
            get;
            protected set;
        }

        /// <summary>
        /// The <see cref="T:System.Type" /> of the field being filtered on.
        /// </summary>
        public Type FieldDataType
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets if the filter being build is for the Date Column.  If so then we will use some ranging logic when
        /// building filters since the Date column does not support time.
        /// </summary>
        protected bool FromDateColumn
        { get; set; }


        internal abstract Expression Or(Expression left, Expression right);
        internal abstract Expression And(Expression left, Expression right);

        /// <summary>
        /// Creates a new <see cref="T:System.Linq.Expressions.Expression" />
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public virtual Expression CreateExpression(string fieldName, ComparisonOperator op, object value, bool caseSensitive)
        {
            return null;
        }

        /// <summary>
        /// Creates a new <see cref="T:System.Linq.Expressions.Expression" />
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public virtual Expression CreateExpression(string fieldName, ComparisonOperator op, object value)
        {
            return null;
        }

        /// <summary>
        /// Creates a new <see cref="T:System.Linq.Expressions.Expression" />
        /// </summary>
        /// <param name="conditionGroup"></param>
        /// <returns></returns>
        public virtual Expression CreateExpression(ConditionCollection conditionGroup)
        {
            return null;
        }

        /// <summary>
        /// Creates a new <see cref="T:System.Linq.Expressions.Expression" />
        /// </summary>
        /// <param name="conditionGroup"></param>
        /// <returns></returns>
        public virtual Expression CreateExpression(RecordFilterCollection conditionGroup)
        {
            return null;
        }

        /// <summary>
        /// Creates a FilterContext instanced typed to the object type of the data being processed.
        /// </summary>
        /// <param name="cachedTypeInfo">The data object type that will be processed over.</param>
        /// <param name="fieldDataType">The field data type that will be processed on.</param>
        /// <param name="caseSensitive">True if case sensitivity should be applied.  Only used for string fieldDataTypes.</param>
        /// <param name="fromDateColumn">True if this filter was created by a DateColumn and will perform some extra actions to filter out time aspect.</param>
        /// <returns></returns>
        public static FilterContext CreateGenericFilter( CachedTypedInfo cachedTypeInfo, Type fieldDataType, bool caseSensitive, bool fromDateColumn)
        {
            Type cachedType = cachedTypeInfo.CachedType;
            Type type = typeof(FilterContext<>).MakeGenericType( cachedType );
            object[] objArray = new object[] { fieldDataType, caseSensitive, fromDateColumn, cachedTypeInfo };
            return TypeActivator.CreateInstanceTBaseLazy<FilterContext>(type , TypeActivator.DefaultCtorSearchBinding, objArray);// Activator.CreateInstance(type, objArray);
        }

        
    }
}
