﻿
using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Linq.Expressions;
using System.Collections.Specialized;

using DDDD.Core.Collections.IG;
using DDDD.Core.Collections.Condition.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// A collection of <see cref="T: IFilterCondition" /> objects which represent a group of conditions bound by a <see cref="P: ConditionCollection.LogicalOperator" />
    /// </summary>
    public class ConditionCollection : CollectionBase<IFilterCondition>, IFilterCondition, INotifyPropertyChanged, IGroupFilterConditions, IExpressConditions, IProvidePersistenceLookupKeys
    {
        private IRecordFilter _parent;

        private  LogicalOperator _logicalOperator;



         LogicalOperator  IGroupFilterConditions.LogicalOperator
        {
            get
            {
                return this.LogicalOperator;
            }
        }

        /// <summary>
        /// Gets / sets the <see cref="P: ConditionCollection.LogicalOperator" /> that will be used to combine all the <see cref="T: IFilterCondition" /> objects in this collection.
        /// </summary>
        public  LogicalOperator LogicalOperator
        {
            get
            {
                return this._logicalOperator;
            }
            set
            {
                if (this._logicalOperator != value)
                {
                    this._logicalOperator = value;
                    this.OnPropertyChanged("LogicalOperator");
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="T: IRecordFilter" /> object which contains this object.
        /// </summary>
        public IRecordFilter Parent
        {
            get
            {
                return this._parent;
            }
            set
            {
                if (this._parent != value)
                {
                    this._parent = value;
                    foreach (IFilterCondition item in this.Items)
                    {
                        item.Parent = value;
                    }
                }
            }
        }



        //IRecordFilter  Parent
        //{
        //    get
        //    {
        //        return this.Parent;
        //    }
        //    set
        //    {
        //        this.Parent = value;
        //    }
        //}

        public ConditionCollection()
        {
        }

        /// <summary>
        /// Adds the item at the specified index. 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void AddItem(int index, IFilterCondition item)
        {
            base.AddItemSilently(index, item);
            item.Parent = this.Parent;
            item.PropertyChanged += new PropertyChangedEventHandler(this.Item_PropertyChanged);
            this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        /// <summary>
        /// Adds an element to the collection without raising any events.
        /// </summary>
        /// <param name="item"></param>
        protected internal void AddItemSilently(IFilterCondition item)
        {
            base.AddItemSilently(this.Items.Count, item);
            item.Parent = this.Parent;
            item.PropertyChanged += new PropertyChangedEventHandler(this.Item_PropertyChanged);
        }

        /// <summary>
        /// Looks through the keys, and determines that all the keys are in the collection, and that the same about of objects are in the collection.
        /// If this isn't the case, false is returned, and the Control Persistence Framework, will not try to reuse the object that are already in the collection.
        /// </summary>
        /// <param name="lookupKeys"></param>
        /// <returns></returns>
        protected virtual bool CanRehydrate(Collection<string> lookupKeys)
        {
            if (lookupKeys.Count > 0)
            {
                this.LogicalOperator = ( LogicalOperator)Enum.Parse(typeof( LogicalOperator), lookupKeys[0], true);
            }
            return false;
        }

        /// <summary>
        /// Removes all the elements of the collection without raising any events.
        /// </summary>
        protected internal void ClearSilently()
        {
            this.ResetItemsSilently();
        }

        /// <summary>
        /// Generates the current expression for this <see cref="T: ConditionCollection" />.
        /// </summary>
        /// <returns></returns>
        protected virtual Expression GetCurrentExpression()
        {
            if (this.Parent == null)
            {
                return null;
            }
            FilterContext filterContext = FilterContext.CreateGenericFilter(this.Parent.ObjectTypedInfo, this.Parent.FieldType, false, false);
            return this.GetCurrentExpression(filterContext);
        }

        /// <summary>
        /// Generates the current expression for this <see cref="T: ConditionCollection" /> using the inputted context.
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        protected virtual Expression GetCurrentExpression(FilterContext context)
        {
            return context.CreateExpression(this);
        }

        /// <summary>
        /// Gets a list of keys that each object in the collection has. 
        /// </summary>
        /// <returns></returns>
        protected virtual Collection<string> GetLookupKeys()
        {
            return new Collection<string>()
            {
                this.LogicalOperator.ToString()
            };
        }

        Expression  IExpressConditions.GetCurrentExpression(FilterContext context)
        {
            return this.GetCurrentExpression(context);
        }

        Expression  IExpressConditions.GetCurrentExpression()
        {
            return this.GetCurrentExpression();
        }

        bool  IProvidePersistenceLookupKeys.CanRehydrate(Collection<string> lookupKeys)
        {
            return this.CanRehydrate(lookupKeys);
        }

        Collection<string>  IProvidePersistenceLookupKeys.GetLookupKeys()
        {
            return this.GetLookupKeys();
        }

        /// <summary>
        /// Adds an item to the collection at a given index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void InsertItem(int index, IFilterCondition item)
        {
            this.AddItem(index, item);
        }

        private void Item_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnCollectionItemChanged();
        }

        /// <summary>
        /// Raises the CollectionItemChanged event.
        /// </summary>
        protected virtual void OnCollectionItemChanged()
        {
            if (this.CollectionItemChanged != null)
            {
                this.CollectionItemChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the <see cref="E: ConditionCollection.PropertyChanged" /> event.
        /// </summary>
        /// <param name="name"></param>
        protected virtual new void OnPropertyChanged(string name)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override bool RemoveItem(int index)
        {
            this.Items[index].Parent = null;
            this.Items[index].PropertyChanged -= new PropertyChangedEventHandler(this.Item_PropertyChanged);
            this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, this.Items[index], index));
            return base.RemoveItem(index);
        }

        /// <summary>
        /// Removes the item at the specified index without raising any events.
        /// </summary>
        /// <param name="item"></param>
        protected internal bool RemoveItemSilently(IFilterCondition item)
        {
            int num = this.Items.IndexOf(item);
            if (num != -1)
            {
                item.Parent = null;
                item.PropertyChanged -= new PropertyChangedEventHandler(this.Item_PropertyChanged);
                base.RemoveItemSilently(num);
            }
            return false;
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ResetItems()
        {
            foreach (IFilterCondition item in this.Items)
            {
                item.PropertyChanged -= new PropertyChangedEventHandler(this.Item_PropertyChanged);
            }
            this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            base.ResetItems();
        }

        /// <summary>
        /// Removes all items from the collection without firing any events.
        /// </summary>
        protected override void ResetItemsSilently()
        {
            foreach (IFilterCondition item in this.Items)
            {
                item.PropertyChanged -= new PropertyChangedEventHandler(this.Item_PropertyChanged);
            }
            base.ResetItemsSilently();
        }

        /// <summary>
        /// Event raised when an Item in the Collection is changed.
        /// </summary>
        public event EventHandler<EventArgs> CollectionItemChanged;

        /// <summary>
        /// Event raised when a property on this object changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
