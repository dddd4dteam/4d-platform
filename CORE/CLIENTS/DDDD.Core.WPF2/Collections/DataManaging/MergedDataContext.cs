﻿
using System;
using System.Linq;
using System.Collections;
using System.Windows.Data;
using System.Collections.Generic;

using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;
using DDDD.Core.Collections.Merging.IG;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that encapsulates the Merged Data functionality used by the <see cref="T: DataManagerBase" />
    /// </summary>
    public abstract class MergedDataContext
    {
        /// <summary>
        /// The Object the merged operation is performed on.
        /// </summary>
        public object MergedObject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the name of the property that data should be merged by.
        /// </summary>
        public string PropertyName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets/Sets the sort direction that should be applied to the field that the underlying data has been merged by.
        /// </summary>
        public bool SortAscending
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/Sets the CurrentSort that will be applied when the data is merged by a particular field.
        /// </summary>
        public  SortContext SortContext
        {
            get;
            set;
        }

        protected MergedDataContext()
        {
        }

        /// <summary>
        /// Creates a Generic instance of the <see cref="T: MergedDataContext" />.
        /// </summary>
        /// <param name="cti">The type of data to create the MergedDataContext from.</param>
        /// <param name="propertyName">The name of the property, that the data should be merged by.</param>
        /// <param name="comparer">The IEqualityComparer that will be used to perform the mergin</param>
        /// <returns></returns>
        public static MergedDataContext CreateGenericCustomMerge(CachedTypedInfo cti, string propertyName, object comparer)
        {
            Type cachedType = cti.CachedType;
            Type type = typeof(MergedDataContext<,>);
            Type[] typeArray = new Type[] { cachedType, DataManagerBase.ResolvePropertyTypeFromPropertyName(propertyName, cti) };
            Type type1 = type.MakeGenericType(typeArray);
            object[] objArray = new object[] { propertyName, comparer, cti };
            return TypeActivator.CreateInstanceTBaseLazy<MergedDataContext>(type1, TypeActivator.DefaultCtorSearchBinding, objArray);  // (MergedDataContext)Activator.CreateInstance(type1, objArray);
        }

        /// <summary>
        /// Creates a Generic instance of the <see cref="T: MergedDataContext" />.
        /// </summary>
        /// <param name="cti">The type of data to create the MergedDataContext from.</param>
        /// <param name="comparer"></param>
        /// <param name="converter"></param>
        /// <param name="converterParam"></param>
        /// <returns></returns>
        public static MergedDataContext CreateGenericCustomMerge(CachedTypedInfo cti, object comparer, IValueConverter converter, object converterParam)
        {
            Type cachedType = cti.CachedType;
            Type type = typeof(MergedDataContext<,>);
            Type[] typeArray = new Type[] { cachedType, cachedType };
            Type type1 = type.MakeGenericType(typeArray);
            object[] objArray = new object[] { comparer, converter, converterParam, cti };
            return TypeActivator.CreateInstanceTBaseLazy<MergedDataContext>(type1, TypeActivator.DefaultCtorSearchBinding, objArray); //(MergedDataContext)Activator.CreateInstance(type1, objArray);
        }

        /// <summary>
        /// Merges the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="mdcs"></param>
        /// <param name="summaries"></param>
        /// <returns></returns>
        public abstract IList Merge(IQueryable query, List<MergedDataContext> mdcs, SummaryDefinitionCollection summaries);

        /// <summary>
        /// Merges the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
        /// </summary>
        /// <param name="q"></param>
        /// <param name="mdcs"></param>
        /// <param name="method"></param>
        /// <param name="parentMCI"></param>
        /// <returns></returns>
        protected internal abstract IList Merge(IQueryable q, List<MergedDataContext> mdcs, MergeDelegate<object, IEnumerable, MergedDataContext, object, MergedColumnInfo, object> method, MergedColumnInfo parentMCI);
    }
}
