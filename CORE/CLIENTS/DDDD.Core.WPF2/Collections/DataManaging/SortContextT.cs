﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Compare.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    public class SortContext<T, TColumnType> : SortContext
    {
        private LambdaExpression _lambda;

        protected LambdaExpression Lambda
        {
            get
            {
                return this._lambda;
            }
        }

        protected IComparer<TColumnType> Comparer { get; private set; }

        protected IValueConverter Converter { get; private set; }

        protected object ConverterParameter { get; private set; }

        public SortContext(string propertyName, bool sortAscending, bool caseSensitiveSort, object comparer, CachedTypedInfo cachedTypeInfo)
        {
            this.SortPropertyName = propertyName;
            this.SortAscending = sortAscending;
            this.CaseSensitiveSort = caseSensitiveSort;
            this.Comparer = (IComparer<TColumnType>)comparer;
            this.PropertyType = typeof(TColumnType);
            this.DataType = typeof(T);
            ParameterExpression paramExpression = Expression.Parameter(typeof(T), "param");
            Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(propertyName, paramExpression, cachedTypeInfo, this.PropertyType, (object)default(TColumnType));
            if (!caseSensitiveSort && this.PropertyType == typeof(string) && comparer == null)
                this.Comparer = (IComparer<TColumnType>)StringComparer.CurrentCultureIgnoreCase;
            if (!paramExpression.Type.IsValueType)
                expression = (Expression)Expression.Condition((Expression)Expression.Equal((Expression)paramExpression, (Expression)Expression.Constant((object)null, this.DataType)), (Expression)Expression.Constant((object)default(TColumnType), this.PropertyType), expression);
            this._lambda = Expression.Lambda(expression, new ParameterExpression[1]
            {
        paramExpression
            });
        }

        public SortContext(string propertyName, bool sortAscending, bool caseSensitiveSort, CachedTypedInfo cachedTypeInfo)
          : this(propertyName, sortAscending, caseSensitiveSort, (object)null, cachedTypeInfo)
        {
        }

        public SortContext(bool sortAscending, object comparer, IValueConverter converter, object converterParam, CachedTypedInfo cachedTypeInfo)
        {
            this.SortAscending = sortAscending;
            this.Converter = converter;
            this.ConverterParameter = converterParam;
            this.PropertyType = this.DataType = typeof(T);
            this.Comparer = comparer as IComparer<TColumnType>;
            if (comparer == null && converter != null)
                this.Comparer = (IComparer<TColumnType>)new ValObjComparer<TColumnType>(converter, converterParam);
            ParameterExpression parameterExpression = Expression.Parameter(typeof(T), "param");
            Expression expression = (Expression)parameterExpression;
            if (!parameterExpression.Type.IsValueType)
                expression = (Expression)Expression.Condition((Expression)Expression.Equal((Expression)parameterExpression, (Expression)Expression.Constant((object)null, this.DataType)), (Expression)Expression.Constant((object)default(TColumnType), this.PropertyType), expression);
            this._lambda = Expression.Lambda(expression, new ParameterExpression[1]
            {
        parameterExpression
            });
        }

        public override IOrderedQueryable<TDataType> Sort<TDataType>(IQueryable<TDataType> query)
        {
            Expression<Func<TDataType, TColumnType>> keySelector = (Expression<Func<TDataType, TColumnType>>)this._lambda;
            if (this.Comparer != null)
            {
                if (this.SortAscending)
                    return query.OrderBy<TDataType, TColumnType>(keySelector, this.Comparer);
                return query.OrderByDescending<TDataType, TColumnType>(keySelector, this.Comparer);
            }
            if (this.SortAscending)
                return query.OrderBy<TDataType, TColumnType>(keySelector);
            return query.OrderByDescending<TDataType, TColumnType>(keySelector);
        }

        public override IOrderedQueryable<TDataType> AppendSort<TDataType>(IOrderedQueryable<TDataType> query)
        {
            Expression<Func<TDataType, TColumnType>> keySelector = (Expression<Func<TDataType, TColumnType>>)this._lambda;
            if (this.Comparer != null)
            {
                if (this.SortAscending)
                    return query.ThenBy<TDataType, TColumnType>(keySelector, this.Comparer);
                return query.ThenByDescending<TDataType, TColumnType>(keySelector, this.Comparer);
            }
            if (this.SortAscending)
                return query.ThenBy<TDataType, TColumnType>(keySelector);
            return query.ThenByDescending<TDataType, TColumnType>(keySelector);
        }
    }
}
