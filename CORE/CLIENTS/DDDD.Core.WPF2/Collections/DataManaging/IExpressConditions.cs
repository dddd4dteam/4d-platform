﻿using System.Linq.Expressions;

namespace DDDD.Core.Collections.DataManage.IG
{

    /// <summary>
    /// An interface which will be used by filtering to generate the expression to be applied.
    /// </summary>
    public interface IExpressConditions
    {
        /// <summary>
        /// Creates an expression based on a given <see cref="T:Infragistics.FilterContext" />
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        Expression GetCurrentExpression(FilterContext context);

        /// <summary>
        /// Creates an expression. 
        /// </summary>
        /// <returns></returns>
        Expression GetCurrentExpression();
    }

}
