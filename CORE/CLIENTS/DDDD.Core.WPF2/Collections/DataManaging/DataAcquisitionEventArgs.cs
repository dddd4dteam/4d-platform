﻿using System.Collections;
using System.Collections.ObjectModel;

using DDDD.Core.Collections.IG;


namespace DDDD.Core.Collections.DataManage.IG
{
    public class DataAcquisitionEventArgs : HandleableEventArgs
    {
        public IList DataSource { get; set; }

        public bool EnablePaging { get; protected internal set; }

        public int PageSize { get; protected internal set; }

        public int CurrentPage { get; protected internal set; }

        public ObservableCollection<SortContext> CurrentSort { get; protected internal set; }

        public GroupByContext GroupByContext { get; protected internal set; }

        public RecordFilterCollection Filters { get; protected internal set; }
    }
}
