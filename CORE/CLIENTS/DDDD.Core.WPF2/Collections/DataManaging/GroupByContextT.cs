﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Windows.Data;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Compare.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that encapsulates the GroupBy functionality used by the <see cref="T: DataManagerBase" />
    /// </summary>
    /// <typeparam name="TColumnType">The type of data that should be grouped by.</typeparam>
    public class GroupByContext<TColumnType> : GroupByContext
	{
		private bool _unboundColumn;

		/// <summary>
		/// The CachedTypedInfo for the operation.
		/// </summary>
		protected  CachedTypedInfo CachedTypedInfo
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the Comparer that will be used to perform the grouping.
		/// </summary>
		public IEqualityComparer<TColumnType> Comparer
		{
			get;
			protected set;
		}

		/// <summary>
		/// Gets the <see cref="T:System.Windows.Data.IValueConverter" /> which will be used to evaluate the GroupBy.
		/// </summary>
		protected IValueConverter Converter
		{
			get;
			private set;
		}

		/// <summary>
		/// The parameter applied to the <see cref="P: GroupByContext`1.Converter" />.
		/// </summary>
		protected object ConverterParameter
		{
			get;
			private set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T: GroupByContext" />.
		/// </summary>
		/// <param name="propertyName"></param>
		/// <param name="comparer"></param>
		/// <param name="cti"></param>
		public GroupByContext(string propertyName, object comparer,  CachedTypedInfo cti)
		{
			base.PropertyName = propertyName;
			this.Comparer = comparer as IEqualityComparer<TColumnType>;
			this._unboundColumn = false;
			this.CachedTypedInfo = cti;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T: GroupByContext" />.
		/// </summary>
		/// <param name="comparer"></param>
		/// <param name="converter"></param>
		/// <param name="converterParam"></param>
		/// <param name="cti"></param>
		public GroupByContext(object comparer, IValueConverter converter, object converterParam,  CachedTypedInfo cti)
		{
			this._unboundColumn = true;
			this.Comparer = comparer as IEqualityComparer<TColumnType>;
			this.Converter = converter;
			this.ConverterParameter = converterParam;
			this.CachedTypedInfo = cti;
		}

		/// <summary>
		/// Groups the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
		/// </summary>
		/// <typeparam name="T">Type typeof data that needs to be grouped.</typeparam>
		/// <param name="query">A colleciton of data to group by.</param>
		/// <returns>Collection of <see cref="T: GroupByDataContext" /> objects.</returns>
		public override IList Group<T>(IQueryable<T> query)
		{
			ParameterExpression parameterExpression = System.Linq.Expressions.Expression.Parameter(typeof(T), "param");
			System.Linq.Expressions.Expression expression = null;
			if (!this._unboundColumn)
			{
				TColumnType tColumnType = default(TColumnType);
				expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(base.PropertyName, parameterExpression, this.CachedTypedInfo, typeof(TColumnType), tColumnType);
			}
			else
			{
				expression = parameterExpression;
			}
			if (!typeof(T).IsValueType)
			{
				System.Linq.Expressions.Expression expression1 = System.Linq.Expressions.Expression.Equal(parameterExpression, System.Linq.Expressions.Expression.Constant(null, typeof(T)));
				System.Linq.Expressions.Expression expression2 = System.Linq.Expressions.Expression.Constant(default(TColumnType), typeof(TColumnType));
				expression = System.Linq.Expressions.Expression.Condition(expression1, expression2, expression);
			}
			ParameterExpression[] parameterExpressionArray = new ParameterExpression[] { parameterExpression };
			Expression<Func<T, TColumnType>> expression3 = System.Linq.Expressions.Expression.Lambda(expression, parameterExpressionArray) as Expression<Func<T, TColumnType>>;
			List<GroupByDataContext> groupByDataContexts = new List<GroupByDataContext>();
			IEqualityComparer<TColumnType> comparer = this.Comparer;
			if (comparer == null && this._unboundColumn)
			{
				comparer = new ValObjComparer<TColumnType>(this.Converter, this.ConverterParameter);
			}
			IQueryable<IGrouping<TColumnType, T>> groupings = query.GroupBy<T, TColumnType>(expression3, comparer);
			if (!this._unboundColumn)
			{
				foreach (IGrouping<TColumnType, T> tColumnTypes in groupings)
				{
					GroupByDataContext groupByDataContext = new GroupByDataContext()
					{
						Records = tColumnTypes.ToList<T>(),
						Value = tColumnTypes.Key,
						TypedInfo = this.CachedTypedInfo,
						Summaries = base.Summaries
					};
					groupByDataContexts.Add(groupByDataContext);
				}
			}
			else
			{
				foreach (IGrouping<TColumnType, T> tColumnTypes1 in groupings)
				{
					ValObj valObj = new ValObj();
					Binding binding = new Binding()
					{
						Converter = this.Converter,
						ConverterParameter = this.ConverterParameter,
						Source = tColumnTypes1.Key
					};
					valObj.SetBinding(ValObj.ValueProperty, binding);
					GroupByDataContext groupByDataContext1 = new GroupByDataContext()
					{
						Records = tColumnTypes1.ToList<T>(),
						Value = valObj.Value,
						TypedInfo = this.CachedTypedInfo,
						Summaries = base.Summaries
					};
					groupByDataContexts.Add(groupByDataContext1);
				}
			}
			return groupByDataContexts;
		}
	}
}
