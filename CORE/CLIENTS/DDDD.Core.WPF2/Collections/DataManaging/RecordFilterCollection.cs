﻿
using System;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using DDDD.Core.Collections.IG;
using DDDD.Core.Collections.Condition.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// A CollectionBase of <see cref="T: IRecordFilter" /> objects which combine to give the the current filter.
    /// </summary>
    public class RecordFilterCollection : CollectionBase<IRecordFilter>, IGroupFilterConditions, IExpressConditions, INotifyPropertyChanged
    {
        private  LogicalOperator _logicalOperator;

         LogicalOperator  IGroupFilterConditions.LogicalOperator
        {
            get
            {
                return this.LogicalOperator;
            }
        }

        /// <summary>
        /// The <see cref="P: RecordFilterCollection.LogicalOperator" /> which will be used to combine all the terms in the <see cref="T: RecordFilterCollection" />.
        /// </summary>
        public  LogicalOperator LogicalOperator
        {
            get
            {
                return this._logicalOperator;
            }
            set
            {
                if (this._logicalOperator != value)
                {
                    this._logicalOperator = value;
                    this.OnPropertyChanged("LogicalOperator");
                }
            }
        }

        public RecordFilterCollection()
        {
        }

        /// <summary>
        /// Adds the item at the specified index. 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void AddItem(int index, IRecordFilter item)
        {
            base.AddItem(index, item);
            item.PropertyChanged -= new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
            item.PropertyChanged += new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
        }

        /// <summary>
        /// Adds an element to the collection without invoking any events.
        /// </summary>
        /// <param name="item"></param>
        protected internal void AddItemSilently(IRecordFilter item)
        {
            base.AddItemSilently(this.Items.Count, item);
            item.PropertyChanged -= new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
            item.PropertyChanged += new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> based on the objects current values.
        /// </summary>
        /// <param name="context">The <see cref="T: FilterContext" /> object which will be used as a basis for the Expression being built.</param>
        /// <returns></returns>
        protected virtual Expression GetCurrentExpression(FilterContext context)
        {
            return context.CreateExpression(this);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> based on the objects current values.
        /// </summary>
        /// <remarks>
        /// Not used by this object.
        /// </remarks>
        /// <exception cref="T:System.NotImplementedException">Will be raised if this method is used.</exception>
        protected virtual Expression GetCurrentExpression()
        {
            throw new NotImplementedException("");
        }

        Expression  IExpressConditions.GetCurrentExpression(FilterContext context)
        {
            return this.GetCurrentExpression(context);
        }

        Expression  IExpressConditions.GetCurrentExpression()
        {
            return this.GetCurrentExpression();
        }

        /// <summary>
        /// Adds an item to the collection at a given index.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        protected override void InsertItem(int index, IRecordFilter item)
        {
            this.AddItem(index, item);
        }

        /// <summary>
        /// Raises the <see cref="E: RecordFilterCollection.CollectionItemChanged" /> event.
        /// </summary>
        protected virtual void OnCollectionItemChanged()
        {
            if (this.CollectionItemChanged != null)
            {
                this.CollectionItemChanged(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Raises the <see cref="E: RecordFilterCollection.PropertyChanged" /> event.
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual new void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void ProvidesChanged_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            this.OnCollectionItemChanged();
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        protected override bool RemoveItem(int index)
        {
            base[index].PropertyChanged -= new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
            return base.RemoveItem(index);
        }

        /// <summary>
        /// Removes all items from the collection.
        /// </summary>
        protected override void ResetItems()
        {
            foreach (IRecordFilter item in this.Items)
            {
                item.PropertyChanged -= new PropertyChangedEventHandler(this.ProvidesChanged_PropertyChanged);
            }
            base.ResetItems();
        }

        /// <summary>
        /// Raised when an Item in collection is modified.
        /// </summary>
        public event EventHandler<EventArgs> CollectionItemChanged;

        /// <summary>
        /// Fired when a property changes on the <see cref="T: RecordFilterCollection" />.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
    }
}
