﻿using DDDD.Core.Collections.Compare.IG;
using System.Collections.Generic;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Used to sort the DataFields in the order of their Order property.
    /// </summary>
    internal class DataFieldComparer : IComparer<DataField>
    {
        public DataFieldComparer()
        {
        }

        public int Compare(DataField x, DataField y)
        {
            return x.Order.CompareTo(y.Order);
        }
    }
}
