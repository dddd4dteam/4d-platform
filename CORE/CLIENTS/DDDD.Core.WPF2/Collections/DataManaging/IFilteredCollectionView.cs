﻿namespace DDDD.Core.Collections.DataManage.IG
{
    public interface IFilteredCollectionView
    {
        bool CanFilter { get; }
        RecordFilterCollection FilterConditions { get; }
    }
}
