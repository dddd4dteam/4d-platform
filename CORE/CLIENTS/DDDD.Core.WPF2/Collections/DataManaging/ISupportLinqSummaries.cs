﻿using DDDD.Core.Collections.Summary.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An interface used to designate a summary that can use the LINQ summary structure.
    /// </summary>
    internal interface ISupportLinqSummaries
    {
        /// <summary>
        /// Gets / sets the <see cref="P: ISupportLinqSummaries.SummaryContext" /> that will be used by the summary framework to build the summary.
        /// </summary>
         SummaryContext SummaryContext
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the <see cref="T: LinqSummaryOperator" /> which designates which LINQ summary to use.
        /// </summary>
        LinqSummaryOperator SummaryType
        {
            get;
        }
    }


}
