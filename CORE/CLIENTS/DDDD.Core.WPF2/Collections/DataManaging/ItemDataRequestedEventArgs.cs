﻿ 
using System;
using System.ComponentModel;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Event arguments for the event <see cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
    /// </summary>
    /// <seealso cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
    public class ItemDataRequestedEventArgs : EventArgs
    {
        /// <summary>
        /// Gets a collection of FilterConditions that should be applied to the datasource.
        /// </summary>
        /// <seealso cref="T:RecordFilterCollection" />
        public RecordFilterCollection FilterConditions
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the count of requested items.
        /// </summary>
        public int ItemsCount
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a collection of <see cref="T:System.ComponentModel.SortDescription" /> that should be applied to the datasource.
        /// </summary>
        public SortDescriptionCollection SortDescriptions
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the start index of requested items.
        /// </summary>
        public int StartIndex
        {
            get;
            internal set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.ItemDataRequestedEventArgs" /> class.
        /// </summary>
        /// <param name="startIndex">The start index.</param>
        /// <param name="itemsCount">The items count.</param>
        public ItemDataRequestedEventArgs(int startIndex, int itemsCount)
        {
            this.ItemsCount = itemsCount;
            this.StartIndex = startIndex;
        }
    }
}
