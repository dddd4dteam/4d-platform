﻿

using System.ComponentModel;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Interface that defines an object that can participate in filtering.
    /// </summary>
    public interface IFilterCondition : IExpressConditions, INotifyPropertyChanged
	{
		/// <summary>
		/// The <see cref="T:Infragistics.IRecordFilter" /> object that ultimately is the parent of this object.
		/// </summary>
		IRecordFilter Parent
		{
			get;
			set;
		}
	}
}
