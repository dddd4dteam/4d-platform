﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;

namespace DDDD.Core.Collections.IG
{
	/// <summary>
	/// A base collection interface that provides hooks for derived classes to override base functionality.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface ICollectionBase<T> : ICollectionBase, IList, ICollection, INotifyCollectionChanged, IDisposable, IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable
	{
		/// <summary>
		/// Adds the item at the specified index, without triggering any events. 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		void AddItemSilently(int index, T item);
	}
}
