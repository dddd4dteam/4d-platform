﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Collections.IG
{


    /// <summary>
    /// An <see cref="T:System.Collections.IEnumerator" />  for the <see cref="T: Collections.CollectionBase`1" /> class.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CollectionBaseEnumerator<T> : IEnumerator<T>, IEnumerator, IDisposable
    {
        private int _index;

        private T _current;

        private CollectionBase<T> _collection;

        T System.Collections.Generic.IEnumerator<T>.Current
        {
            get
            {
                return this._current;
            }
        }

        /// <summary>
        /// Gets the current element in the collection. 
        /// </summary>
        object System.Collections.IEnumerator.Current
        {
            get
            {
                return this._current;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: Collections.CollectionBaseEnumerator`1" /> class.
        /// </summary>
        /// <param name="collection"></param>
        public CollectionBaseEnumerator(CollectionBase<T> collection)
        {
            this._index = 0;
            this._collection = collection;
            this._current = default(T);
        }

        /// <summary>
        /// Disposes the <see cref="T: Collections.CollectionBaseEnumerator`1" />
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Gets the next item in the Enumerator.
        /// </summary>
        /// <returns></returns>
        bool System.Collections.IEnumerator.MoveNext()
        {
            if (this._index >= this._collection.Count)
            {
                return false;
            }
            this._current = this._collection[this._index];
            CollectionBaseEnumerator<T> collectionBaseEnumerator = this;
            collectionBaseEnumerator._index = collectionBaseEnumerator._index + 1;
            return true;
        }

        /// <summary>
        /// Sets the enumerator to its initial position, which is before the first element in the collection.
        /// </summary>
        void System.Collections.IEnumerator.Reset()
        {
            this._index = 0;
            this._current = default(T);
        }
    }
}
