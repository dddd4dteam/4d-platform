﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace DDDD.Core.Collections.IG
{

    /// <summary>
    /// A base collection interface that provides hooks for derived classes to override base functionality.
    /// </summary>
    public interface ICollectionBase : IList, ICollection, IEnumerable, INotifyCollectionChanged, IDisposable
    {
        /// <summary>
        /// Adds the item at the specified index, without triggering any events. 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        void AddItemSilently(int index, object item);

        /// <summary>
        /// Removes the item at the specified index, without triggering any events. 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        bool RemoveItemSilently(int index);

        /// <summary>
        /// Removes all items from the collection without firing any events.
        /// </summary>
        void ResetItemsSilently();
    }
}
