﻿using System.Collections.ObjectModel;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// Some collections have specific keys that identify them. So, when saving and loading, it's important that these properties
    /// get set first, so that they can be rehydrated instead of destroyed.
    /// </summary>
    public interface IProvidePersistenceLookupKeys
	{
		/// <summary>
		/// Looks through the keys, and determines that all the keys are in the collection, and that the same about of objects are in the collection.
		/// If this isn't the case, false is returned, and the Control Persistence Framework, will not try to reuse the object that are already in the collection.
		/// </summary>
		/// <param name="lookupKeys"></param>
		/// <returns></returns>
		bool CanRehydrate(Collection<string> lookupKeys);

		/// <summary>
		/// Gets a list of keys that each object in the collection has. 
		/// </summary>
		/// <returns></returns>
		Collection<string> GetLookupKeys();
	}
}
