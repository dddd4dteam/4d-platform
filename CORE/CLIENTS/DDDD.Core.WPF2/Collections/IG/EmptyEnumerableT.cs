﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Collections.IG
{


    internal class EmptyEnumerable<T> : IEnumerable<T>, IEnumerable
    {
        public EmptyEnumerable()
        {
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new  EmptyEnumerable<T>.Enumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public class Enumerator : IEnumerator<T>, IDisposable, IEnumerator
        {
            public T Current
            {
                get
                {
                    throw new InvalidOperationException();
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    throw new InvalidOperationException();
                }
            }

            public Enumerator()
            {
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return false;
            }

            public void Reset()
            {
            }
        }
    }

}
