﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Threading;

namespace  DDDD.Core.Collections.IG
{
    /// <summary>
    /// A base collection class that provides hooks for derived classes to override base functionality.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CollectionBase<T> : ICollectionBase<T>, ICollectionBase, IList, ICollection, INotifyCollectionChanged, IDisposable, IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, INotifyPropertyChanged
	{
		private Collection<T> _items;

		private object _syncRoot;

		private Dictionary<int, T> _unIndexedItems;

		private Dictionary<T, int> _unIndexedItemsReverse;

		/// <summary>
		/// Gets the amount of items in the collection.
		/// </summary>
		public int Count
		{
			get
			{
				return this.GetCount();
			}
		}

		/// <summary>
		/// Gets whether or not the collection is read only.
		/// </summary>
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		/// <summary>
		/// Gets the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		public T this[int index]
		{
			get
			{
				return this.GetItem(index);
			}
			set
			{
				this.ReplaceItem(index, value);
			}
		}

		/// <summary>
		/// Gets the unerlying collection used to store all items.
		/// </summary>
		protected virtual Collection<T> Items
		{
			get
			{
				return this._items;
			}
		}

		bool System.Collections.ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		object System.Collections.ICollection.SyncRoot
		{
			get
			{
				if (this._syncRoot == null)
				{
					ICollection collections = this._items;
					if (collections == null)
					{
						Interlocked.CompareExchange<object>(ref this._syncRoot, new object(), null);
					}
					else
					{
						this._syncRoot = collections.SyncRoot;
					}
				}
				return this._syncRoot;
			}
		}

		bool System.Collections.IList.IsFixedSize
		{
			get
			{
				IList lists = this._items;
				if (lists == null)
				{
					return false;
				}
				return lists.IsFixedSize;
			}
		}

		object System.Collections.IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (T)value;
			}
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T: Collections.CollectionBase`1" /> class.
		/// </summary>
		public CollectionBase()
		{
			this._items = new Collection<T>();
			this._unIndexedItems = new Dictionary<int, T>();
			this._unIndexedItemsReverse = new Dictionary<T, int>();
		}

		/// <summary>
		/// Adds the item to the end of the collection.
		/// </summary>
		/// <param name="item"></param>
		public virtual void Add(T item)
		{
			this.AddItem(this.Count, item);
		}

		/// <summary>
		/// Adds the item at the specified index. 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected virtual void AddItem(int index, T item)
		{
			this.AddItemSilently(index, item);
			this.OnItemAdded(index, item);
		}

		/// <summary>
		/// Adds the item at the specified index, without triggering any events. 
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected virtual void AddItemSilently(int index, T item)
		{
			if (index <= this._items.Count)
			{
				this._items.Insert(index, item);
				return;
			}
			this._unIndexedItems.Add(index, item);
			this._unIndexedItemsReverse.Add(item, index);
		}

		/// <summary>
		/// Removes all items from the collection.
		/// </summary>
		public virtual void Clear()
		{
			this.ResetItems();
		}

		/// <summary>
		/// Determines if the collection contains the specified item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns>True if the item is in the collection.</returns>
		public bool Contains(T item)
		{
			if (item != null && this._unIndexedItemsReverse.ContainsKey(item))
			{
				return true;
			}
			return this._items.Contains(item);
		}

		/// <summary>
		/// Copies the collection to the specified array, starting at the specified index.
		/// </summary>
		/// <param name="array"></param>
		/// <param name="index"></param>
		public void CopyTo(Array array, int index)
		{
			int count = this.GetCount();
			for (int i = index; i < count; i++)
			{
				array.SetValue(this.GetItem(i), i);
			}
		}

		/// <summary>
		/// Releases the unmanaged resources used by the <see cref="T: Collections.CollectionBase`1" /> and optionally
		/// releases the managed resources.
		/// </summary>
		/// <param name="disposing">
		/// true to release both managed and unmanaged resources; 
		/// false to release only unmanaged resources.
		/// </param>
		protected virtual void Dispose(bool disposing)
		{
		}

		/// <summary>
		/// Releases the unmanaged and managed resources used by the <see cref="T: Collections.CollectionBase`1" />.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Retrieves the amount of items in the collection.
		/// </summary>
		/// <returns></returns>
		protected virtual int GetCount()
		{
			return this._items.Count;
		}

		/// <summary>
		/// Gets the <see cref="T:System.Collections.IEnumerator" /> for the collection.
		/// </summary>
		/// <returns></returns>
		public IEnumerator<T> GetEnumerator()
		{
			return new CollectionBaseEnumerator<T>(this);
		}

		/// <summary>
		/// Gets the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		protected virtual T GetItem(int index)
		{
			return this.ResolveItem(index);
		}

		/// <summary>
		/// Gets the index of the specified item.
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public virtual int IndexOf(T item)
		{
			if (item == null)
			{
				return -1;
			}
			int num = -1;
			if (this._unIndexedItemsReverse.TryGetValue(item, out num))
			{
				return num;
			}
			return this._items.IndexOf(item);
		}

		void  ICollectionBase.AddItemSilently(int index, object item)
		{
			this.AddItemSilently(index, (T)item);
		}

		bool  ICollectionBase.RemoveItemSilently(int index)
		{
			return this.RemoveItemSilently(index);
		}

		void  ICollectionBase.ResetItemsSilently()
		{
			this.ResetItemsSilently();
		}

		void  ICollectionBase<T>.AddItemSilently(int index, T item)
		{
			this.AddItemSilently(index, item);
		}

		/// <summary>
		/// Inserts the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		public virtual void Insert(int index, T item)
		{
			this.InsertItem(index, item);
		}

		/// <summary>
		/// Adds an item to the collection at a given index.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected virtual void InsertItem(int index, T item)
		{
			this.AddItemSilently(index, item);
			this.OnItemAdded(index, item);
		}

		/// <summary>
		/// Invoked when an Item is added at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected virtual void OnItemAdded(int index, T item)
		{
			this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, (object)item, index));
		}

		/// <summary>
		/// Invoked when an Item is removed from the collection.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="item"></param>
		protected virtual void OnItemRemoved(int index, T item)
		{
			this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, (object)item, index));
		}

		/// <summary>
		/// Invoked when this collection changes.
		/// </summary>
		/// <param name="args"></param>
		protected virtual void OnNotifyCollectionChanged(NotifyCollectionChangedEventArgs args)
		{
			switch (args.Action)
			{
				case NotifyCollectionChangedAction.Add:
				case NotifyCollectionChangedAction.Remove:
				case NotifyCollectionChangedAction.Reset:
					{
						this.OnPropertyChanged("Count");
						this.OnPropertyChanged("Item[]");
						if (this.CollectionChanged != null)
						{
							this.CollectionChanged(this, args);
						}
						return;
					}
				case NotifyCollectionChangedAction.Replace:
				case NotifyCollectionChangedAction.Remove | NotifyCollectionChangedAction.Replace:
					{
						this.OnPropertyChanged("Item[]");
						if (this.CollectionChanged != null)
						{
							this.CollectionChanged(this, args);
						}
						return;
					}
				default:
					{
						this.OnPropertyChanged("Item[]");
						if (this.CollectionChanged != null)
						{
							this.CollectionChanged(this, args);
						}
						return;
					}
			}
		}

		/// <summary>
		/// Occurs when a property changes.
		/// </summary>
		/// <param name="propertyName">The name of the changed property.</param>
		protected virtual void OnPropertyChanged(string propertyName)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
			}
		}

		/// <summary>
		/// Invoked when all items are cleared from a collection.
		/// </summary>
		protected virtual void OnResetItems()
		{
			this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
		}

		/// <summary>
		/// Removes the specified item. 
		/// </summary>
		/// <param name="item"></param>
		/// <returns>True if the item was removed.</returns>
		public bool Remove(T item)
		{
			return this.RemoveItem(this.IndexOf(item));
		}

		/// <summary>
		/// Removes the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		public void RemoveAt(int index)
		{
			this.RemoveItem(index);
		}

		/// <summary>
		/// Removes the item at the specified index.
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		protected virtual bool RemoveItem(int index)
		{
			T item = this[index];
			bool flag = this.RemoveItemSilently(index);
			if (flag)
			{
				this.OnItemRemoved(index, item);
			}
			return flag;
		}

		/// <summary>
		/// Removes the item at the specified index, without triggering any events. 
		/// </summary>
		/// <param name="index"></param>
		/// <returns></returns>
		protected virtual bool RemoveItemSilently(int index)
		{
			bool flag = false;
			T item = default(T);
			if (this._unIndexedItems.TryGetValue(index, out item))
			{
				flag = true;
				this._unIndexedItems.Remove(index);
				this._unIndexedItemsReverse.Remove(item);
			}
			else if (index >= 0 && index < this._items.Count)
			{
				item = this._items[index];
				flag = this._items.Remove(item);
			}
			return flag;
		}

		/// <summary>
		/// Replaces the item at the specified index with the specified item.
		/// </summary>
		/// <param name="index"></param>
		/// <param name="newItem"></param>
		protected virtual void ReplaceItem(int index, T newItem)
		{
			T t = this.ResolveItem(index);
			if (this._unIndexedItems.ContainsKey(index))
			{
				this._unIndexedItems[index] = newItem;
				this._unIndexedItemsReverse.Remove(t);
				this._unIndexedItemsReverse.Add(newItem, index);
			}
			else if (index >= this._items.Count)
			{
				this.AddItemSilently(index, newItem);
			}
			else
			{
				this._items[index] = newItem;
			}
			this.OnNotifyCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, (object)newItem, (object)t, index));
		}

		/// <summary>
		/// Removes all items from the collection.
		/// </summary>
		protected virtual void ResetItems()
		{
			this.ResetItemsSilently();
			this.OnResetItems();
		}

		/// <summary>
		/// Removes all items from the collection without firing any events.
		/// </summary>
		protected virtual void ResetItemsSilently()
		{
			this._items.Clear();
			this._unIndexedItems.Clear();
			this._unIndexedItemsReverse.Clear();
		}

		private T ResolveItem(int index)
		{
			T t;
			if (index > -1 && index < this._items.Count)
			{
				return this._items[index];
			}
			if (this._unIndexedItems.TryGetValue(index, out t))
			{
				return t;
			}
			return default(T);
		}

		void System.Collections.Generic.ICollection<T>.CopyTo(T[] array, int arrayIndex)
		{
			for (int i = arrayIndex; i < (int)array.Length; i++)
			{
				array[i] = this.GetItem(i);
			}
		}

		IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		int System.Collections.IList.Add(object value)
		{
			if (!(value is T))
			{
				return -1;
			}
			this.Add((T)value);
			return this.Count;
		}

		bool System.Collections.IList.Contains(object value)
		{
			if (!(value is T))
			{
				return false;
			}
			return this.Contains((T)value);
		}

		int System.Collections.IList.IndexOf(object value)
		{
			if (!(value is T))
			{
				return -1;
			}
			return this.IndexOf((T)value);
		}

		void System.Collections.IList.Insert(int index, object value)
		{
			if (value is T)
			{
				this.Insert(index, (T)value);
			}
		}

		void System.Collections.IList.Remove(object value)
		{
			if (value is T)
			{
				this.Remove((T)value);
			}
		}

		/// <summary>
		/// Fired when the collection changes.
		/// </summary>
		public event NotifyCollectionChangedEventHandler CollectionChanged;

		private event PropertyChangedEventHandler PropertyChanged;

		event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
		{
			add
			{
				this.PropertyChanged += value;
			}
			remove
			{
				this.PropertyChanged -= value;
			}
		}
	}
}
