﻿using System;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// Event arguments for the event <see cref="E:Collections.VirtualCollection`1.RemoveItem" />
    /// </summary>
    /// <typeparam name="T">The type of removed item.</typeparam>
    /// <seealso cref="E:Collections.VirtualCollection`1.RemoveItem" />
    public class RemoveItemEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Gets the index of the item to be removed..
        /// </summary>
        /// <value>The index.</value>
        /// <remarks>The value of Index is -1 when <see cref="M:Collections.VirtualCollection`1.RemoveAt(System.Int32)" /> raises the event</remarks>
        public int Index
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the item to be removed.
        /// </summary>
        /// <value>The edit item.</value>
        /// <remarks>The value of RemovedItem is null when <see cref="M:Collections.VirtualCollection`1.Remove(System.Object)" /> raises the event</remarks>
        public T RemovedItem
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.RemoveItemEventArgs`1" /> class.
        /// </summary>
        /// <param name="item">The item to be removed.</param>
        public RemoveItemEventArgs(T item)
        {
            this.RemovedItem = item;
            this.Index = -1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.RemoveItemEventArgs`1" /> class.
        /// </summary>
        /// <param name="index">The index.</param>
        public RemoveItemEventArgs(int index)
        {
            this.Index = index;
            this.RemovedItem = default(T);
        }
    }
}
