﻿using System;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// Event arguments for the event <see cref="E:Collections.VirtualCollection`1.CommitEditItem" />
    /// </summary>
    /// <typeparam name="T">The type of item.</typeparam>
    /// <seealso cref="E:Collections.VirtualCollection`1.CommitEditItem" />
    public class CommitEditItemEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Gets the edit item to be committed.
        /// </summary>
        /// <value>The edit item.</value>
        public T EditItem
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.CommitEditItemEventArgs`1" /> class.
        /// </summary>
        /// <param name="item">The item to be committed.</param>
        public CommitEditItemEventArgs(T item)
        {
            this.EditItem = item;
        }
    }
}
