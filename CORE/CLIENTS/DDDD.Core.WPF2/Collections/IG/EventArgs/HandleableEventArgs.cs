﻿using System;

namespace DDDD.Core.Collections.IG
{
    public class HandleableEventArgs : EventArgs
    {
        public bool Handled { get; set; }
    }
}
