﻿using System;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// Event argument used when the DataManager has a request for a new data object.
    /// </summary>
    public class HandleableObjectGenerationEventArgs : HandleableEventArgs
    {
        /// <summary>
        /// Gets the <see cref="T:System.Type" /> which is contained in the underlying collection.
        /// </summary>
        public Type CollectionType
        {
            get;
            protected internal set;
        }

        /// <summary>
        /// Gets / sets the instance of the object that will be used by the DataManager rather then attempting to create a new instance of the object using the default constructor.
        /// </summary>
        public object NewObject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the <see cref="T:System.Type" /> of object that the DataManager is handling. 
        /// </summary>
        public Type ObjectType
        {
            get;
            protected internal set;
        }

        public HandleableObjectGenerationEventArgs()
        {
        }
    }
}
