﻿using System.Windows;

namespace DDDD.Core.Collections.Compare.IG
{
    internal class ValObj : FrameworkElement
    {
        public static readonly DependencyProperty ValueProperty = DependencyProperty.Register("Value", typeof(string), typeof(ValObj), new PropertyMetadata(new PropertyChangedCallback(ValObj.ValueChanged)));

        public string Value
        {
            get
            {
                return (string)this.GetValue(ValObj.ValueProperty);
            }
            set
            {
                this.SetValue(ValObj.ValueProperty, (object)value);
            }
        }

        private static void ValueChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}
