﻿namespace DDDD.Core.Collections.Condition.IG
{
	/// <summary>
	/// And enumeration of available operators.
	/// </summary>
	public enum LogicalOperator
	{
		/// <summary>
		/// Performs a logical AND operation.
		/// </summary>
		And,
		/// <summary>
		/// Performs a logical OR operation.
		/// </summary>
		Or
	}

	/// <summary>
	/// Enum describing operators which can be used for filtering.
	/// </summary>
	public enum ComparisonOperator
	{
		/// <summary>
		/// An equality compare is executed.  With string values, a case sensitivity flag may effect the result.
		/// </summary>
		Equals,
		/// <summary>
		/// An non equality compare is executed.  With string values, a case sensitivity flag may effect the result.
		/// </summary>
		NotEquals,
		/// <summary>
		/// A GreaterThan compare is executed. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		GreaterThan,
		/// <summary>
		/// A greater than or equal compare is executed.  With string values, a case sensitivity flag may effect the result.
		/// </summary>
		GreaterThanOrEqual,
		/// <summary>
		/// A less than compare is executed.  With string values, a case sensitivity flag may effect the result.
		/// </summary>
		LessThan,
		/// <summary>
		/// A less than or equal compare is executed.  With string values, a case sensitivity flag may effect the result.
		/// </summary>
		LessThanOrEqual,
		/// <summary>
		/// Evaluates if a string begins with a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		StartsWith,
		/// <summary>
		/// Evaluates if a string does not begin with a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		DoesNotStartWith,
		/// <summary>
		/// Evaluates if a string ends with a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		EndsWith,
		/// <summary>
		/// Evaluates if a string does not end with a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		DoesNotEndWith,
		/// <summary>
		/// Evaluates if a string contains a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		Contains,
		/// <summary>
		/// Evaluates if a string does not contain a compared value. With string values, a case sensitivity flag may effect the result.
		/// </summary>
		DoesNotContain,
		/// <summary>
		/// Evaluates if a DateTime object comes after a given input.
		/// </summary>
		DateTimeAfter,
		/// <summary>
		/// Evaluates if a DateTime object comes prior to a given input.
		/// </summary>
		DateTimeBefore,
		/// <summary>
		/// Evaluates if a DateTime value is today.
		/// </summary>
		DateTimeToday,
		/// <summary>
		/// Evaluates if a DateTime value is tomorrow.
		/// </summary>
		DateTimeTomorrow,
		/// <summary>
		/// Evaluates if a DateTime value is yesterday.
		/// </summary>
		DateTimeYesterday,
		/// <summary>
		/// Evaluates if a DateTime value is in this week.
		/// </summary>
		DateTimeThisWeek,
		/// <summary>
		/// Evaluates if a DateTime value is in next week.
		/// </summary>
		DateTimeNextWeek,
		/// <summary>
		/// Evaluates if a DateTime value is in last week.
		/// </summary>
		DateTimeLastWeek,
		/// <summary>
		/// Evaluates if a DateTime value is in this month.
		/// </summary>
		DateTimeThisMonth,
		/// <summary>
		/// Evaluates if a DateTime value is in last month.
		/// </summary>
		DateTimeLastMonth,
		/// <summary>
		/// Evaluates if a DateTime value is in next month.
		/// </summary>
		DateTimeNextMonth,
		/// <summary>
		/// Evaluates if a DateTime value is in this year.
		/// </summary>
		DateTimeThisYear,
		/// <summary>
		/// Evaluates if a DateTime value is in last year.
		/// </summary>
		DateTimeLastYear,
		/// <summary>
		/// Evaluates if a DateTime value is in next year.
		/// </summary>
		DateTimeNextYear,
		/// <summary>
		/// Evaluates if a DateTime value is in the current year up to today's date.
		/// </summary>
		DateTimeYearToDate,
		/// <summary>
		/// Evaluates if a DateTime value is in the last quarter.
		/// </summary>
		DateTimeLastQuarter,
		/// <summary>
		/// Evaluates if a DateTime value is in this quarter.
		/// </summary>
		DateTimeThisQuarter,
		/// <summary>
		/// Evaluates if a DateTime value is in the next quarter.
		/// </summary>
		DateTimeNextQuarter,
		/// <summary>
		/// Evaluates that a DateTime is in January.
		/// </summary>
		DateTimeJanuary,
		/// <summary>
		/// Evaluates that a DateTime is in February.
		/// </summary>
		DateTimeFebruary,
		/// <summary>
		/// Evaluates that a DateTime is in March.
		/// </summary>
		DateTimeMarch,
		/// <summary>
		/// Evaluates that a DateTime is in April.
		/// </summary>
		DateTimeApril,
		/// <summary>
		/// Evaluates that a DateTime is in May.
		/// </summary>
		DateTimeMay,
		/// <summary>
		/// Evaluates that a DateTime is in June.
		/// </summary>
		DateTimeJune,
		/// <summary>
		/// Evaluates that a DateTime is in July.
		/// </summary>
		DateTimeJuly,
		/// <summary>
		/// Evaluates that a DateTime is in August.
		/// </summary>
		DateTimeAugust,
		/// <summary>
		/// Evaluates that a DateTime is in Sepember.
		/// </summary>
		DateTimeSeptember,
		/// <summary>
		/// Evaluates that a DateTime is in October.
		/// </summary>
		DateTimeOctober,
		/// <summary>
		/// Evaluates that a DateTime is in November.
		/// </summary>
		DateTimeNovember,
		/// <summary>
		/// Evaluates that a DateTime is in December.
		/// </summary>
		DateTimeDecember,
		/// <summary>
		/// Evaluates that a DateTime is in the first quarter.
		/// </summary>
		DateTimeQuarter1,
		/// <summary>
		/// Evaluates that a DateTime is in the second quarter.
		/// </summary>
		DateTimeQuarter2,
		/// <summary>
		/// Evaluates that a DateTime is in the third quarter.
		/// </summary>
		DateTimeQuarter3,
		/// <summary>
		/// Evaluates that a DateTime is in the forth quarter.
		/// </summary>
		DateTimeQuarter4,
		/// <summary>
		/// Evaluates that a value is in the list provided as FilterValue.
		/// </summary>
		InOperand
	}





}
