﻿using System.Linq;

namespace DDDD.Core.Collections.Format.IG
{
    /// <summary>
    /// Defines an interface which will participate the data binding so that it can gather data during the
    /// data processing.
    /// </summary>
    public interface IRule
	{
		/// <summary>
		/// Designates at what stage during the data binding the GatherData needs to be evaluated.
		/// </summary>		
		EvaluationStage RuleExecution
		{
			get;
		}

		/// <summary>
		/// Allows access to the query at the time so that values can be derived off it for the condition.
		/// </summary>
		void GatherData(IQueryable query);
	}
}
