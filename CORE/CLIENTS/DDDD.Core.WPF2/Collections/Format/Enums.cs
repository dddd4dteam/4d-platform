﻿namespace DDDD.Core.Collections.Format.IG
{ 
    /// <summary>
    /// Enumeration which lists when conditional formatting data will be gathered.
    /// </summary>
    public enum EvaluationStage
	{
		/// <summary>
		/// GatherData will not be called.
		/// </summary>
		None,
		/// <summary>
		/// GatherData will be called prior to filtering and paging.
		/// </summary>
		PriorToFilteringAndPaging,
		/// <summary>
		/// GatherData will be called after filtering and before paging.
		/// </summary>
		AfterFilteringBeforePaging,
		/// <summary>
		/// GatherData will be called after filtering and after paging.
		/// </summary>
		AfterFilteringAndPaging
	}
}
