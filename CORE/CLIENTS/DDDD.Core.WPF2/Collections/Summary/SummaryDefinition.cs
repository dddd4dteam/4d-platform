﻿

using System.Windows;

using DDDD.Core.UI.IG;

namespace DDDD.Core.Collections.Summary.IG
{
    /// <summary>
    /// A class which describes the type of summary being applied.
    /// </summary>
    public class SummaryDefinition : DependencyObjectNotifier
    {

        #region  ------------------------ CTOR -------------------------

        static SummaryDefinition()
        {
            SummaryDefinition.ColumnKeyProperty = DependencyProperty.Register("ColumnKey", typeof(string), typeof(SummaryDefinition), new PropertyMetadata(new PropertyChangedCallback(SummaryDefinition.ColumnKeyChanged)));
        }

        public SummaryDefinition()
        {
        }

        #endregion ------------------------ CTOR -------------------------



        /// <summary>
        /// Identifies the <see cref="P:SummaryDefinition.ColumnKey" /> dependency property. 
        /// </summary>
        public readonly static DependencyProperty ColumnKeyProperty;

        /// <summary>
        /// Gets / sets the key of the column which this filter applies to.
        /// </summary>
        public string ColumnKey
        {
            get
            {
                return (string)base.GetValue(SummaryDefinition.ColumnKeyProperty);
            }
            set
            {
                base.SetValue(SummaryDefinition.ColumnKeyProperty, value);
            }
        }

        /// <summary>
        /// Get / set the <see cref="T:SummaryOperandBase" /> which designates which summary should be executed.
        /// </summary>
        public SummaryOperandBase SummaryOperand
        {   get;  set;   }

        private static void ColumnKeyChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SummaryDefinition)obj).OnPropertyChanged("ColumnKey");
        }
    }
}
