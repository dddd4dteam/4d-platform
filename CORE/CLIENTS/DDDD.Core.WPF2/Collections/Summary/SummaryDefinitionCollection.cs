﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;
using DDDD.Core.Collections.IG;

namespace DDDD.Core.Collections.Summary.IG
{   /// <summary>
    /// A collection of <see cref="T:SummaryDefinition" /> objects.
    /// </summary>
    public class SummaryDefinitionCollection : CollectionBase<SummaryDefinition>
    {
        public SummaryDefinitionCollection()
        {
        }

        /// <summary>
        /// Adds an element to the collection without invoking any events.
        /// </summary>
        /// <param name="item"></param>
        protected internal void AddItemSilently(SummaryDefinition item)
        {
            base.AddItemSilently(this.Items.Count, item);
        }

        /// <summary>
        /// Returns a collection of <see cref="T:SummaryDefinition" /> which are applied to a given column based on it's key value.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public ReadOnlyCollection<SummaryDefinition> GetDefinitionsByKey(string key)
        {
            List<SummaryDefinition> summaryDefinitions = new List<SummaryDefinition>();
            foreach (SummaryDefinition item in this.Items)
            {
                if (item.ColumnKey != key)
                {
                    continue;
                }
                summaryDefinitions.Add(item);
            }
            return new ReadOnlyCollection<SummaryDefinition>(summaryDefinitions);
        }

        /// <summary>
        /// Returns a collection of <see cref="T:SummaryDefinition" />  based on when the summary is applied during databind.
        /// </summary>
        /// <param name="summaryExecution"></param>
        /// <returns></returns>
        public ReadOnlyCollection<SummaryDefinition> GetDefinitionsBySummaryExecution(SummaryExecution summaryExecution)
        {
            return this.GetDefinitionsBySummaryExecution(summaryExecution, false);
        }

        /// <summary>
        /// Returns a collection of <see cref="T:SummaryDefinition" />  based on when the summary is applied during databind.
        /// </summary>
        /// <param name="summaryExecution"></param>
        /// <param name="includeNulls"></param>
        /// <returns></returns>
        public ReadOnlyCollection<SummaryDefinition> GetDefinitionsBySummaryExecution(SummaryExecution summaryExecution, bool includeNulls)
        {
            List<SummaryDefinition> summaryDefinitions = new List<SummaryDefinition>();
            foreach (SummaryDefinition item in this.Items)
            {
                if (item.SummaryOperand != null && item.SummaryOperand.SummaryCalculator != null)
                {
                    SummaryExecution? nullable = item.SummaryOperand.SummaryCalculator.SummaryExecution;
                    if ((nullable.GetValueOrDefault() != summaryExecution ? false : nullable.HasValue))
                    {
                        goto Label0;
                    }
                }
                if (!includeNulls || item.SummaryOperand.SummaryCalculator.SummaryExecution.HasValue)
                {
                    continue;
                }
                Label0:
                summaryDefinitions.Add(item);
            }
            return new ReadOnlyCollection<SummaryDefinition>(summaryDefinitions);
        }
    }
}
