﻿
using System.Collections.Generic;
using DDDD.Core.Collections.IG;

namespace DDDD.Core.Collections.Summary.IG
{
    /// <summary>
    /// A collection of <see cref="T: SummaryResult" /> objects which are used to display summary data.
    /// </summary>
    public class SummaryResultCollection : CollectionBase<SummaryResult>
    {
        /// <summary>
        /// Gets the <see cref="T: SummaryResult" /> for a given <see cref="T: SummaryOperandBase" />.
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        public SummaryResult this[SummaryOperandBase op]
        {
            get
            {
                SummaryResult summaryResult;
                using (IEnumerator<SummaryResult> enumerator = this.Items.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        SummaryResult current = enumerator.Current;
                        if (current.SummaryDefinition.SummaryOperand != op)
                        {
                            continue;
                        }
                        summaryResult = current;
                        return summaryResult;
                    }
                    return null;
                }
                return summaryResult;
            }
        }

        public SummaryResultCollection()
        {
        }
    }
}
