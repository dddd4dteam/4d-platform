﻿namespace DDDD.Core.Collections.Summary.IG
{
    /// <summary>
    /// Enum that is used to determine when a summary should be calculated.
    /// </summary>
    public enum SummaryExecution
	{
		/// <summary>
		/// Summary is executed prior to paging and filtering.
		/// </summary>
		PriorToFilteringAndPaging,
		/// <summary>
		/// Summary is executed prior to paging but after filtering.
		/// </summary>
		AfterFilteringBeforePaging,
		/// <summary>
		/// Summary is executed after paging and filtering.
		/// </summary>
		AfterFilteringAndPaging
	}




	/// <summary>
	/// An enum used by summary to designate that a LINQ summary should be use.
	/// </summary>
	public enum LinqSummaryOperator
	{
		/// <summary>
		/// Use the LINQ Count summary
		/// </summary>
		Count,
		/// <summary>
		/// Use the LINQ Minimum summary.
		/// </summary>
		Minimum,
		/// <summary>
		/// Use the LINQ Maximum summary.
		/// </summary>
		Maximum,
		/// <summary>
		/// Use the LINQ Sum summary.
		/// </summary>
		Sum,
		/// <summary>
		/// Use the LINQ Average summary.
		/// </summary>
		Average
	}



}
