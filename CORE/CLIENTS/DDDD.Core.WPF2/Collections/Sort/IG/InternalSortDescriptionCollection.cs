﻿using System.Collections.Specialized;
using System.ComponentModel;

namespace DDDD.Core.Collections.Sort.IG
{
    /// <summary>
    /// Represents a collection of System.ComponentModel.SortDescription instances and public NotifyCollectionChangedEventHandler
    /// </summary>
    internal class InternalSortDescriptionCollection : SortDescriptionCollection
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.InternalSortDescriptionCollection" /> class.
        /// </summary>
        public InternalSortDescriptionCollection()
        {
            base.CollectionChanged += new NotifyCollectionChangedEventHandler(this.SortDescriptions_CollectionChanged);
        }

        private void SortDescriptions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (this.SortDescriptionsChanged != null)
            {
                this.SortDescriptionsChanged(this, e);
            }
        }

        public event NotifyCollectionChangedEventHandler SortDescriptionsChanged;
    }
}
