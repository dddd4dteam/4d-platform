﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Data;


using DDDD.Core.Collections.DataManage.IG;


namespace DDDD.Core.Collections.Virtual
{
    /// <summary>
    /// Represents a group created by the VirtualCollection based on its GroupDescriptions.
    /// </summary>
    internal class VirtualCollectionGroup : CollectionViewGroup
    {
        private readonly bool _isBottomLevel;

        /// <summary>
        /// Gets a value indicating whether this group has any subgroups.
        /// </summary>
        /// <returns>true if this group is at the bottom level and does not have any subgroups; otherwise, false.</returns>
        public override bool IsBottomLevel
        {
            get
            {
                return this._isBottomLevel;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.VirtualCollectionGroup" /> class.
        /// </summary>
        /// <param name="name">The name of the group.</param>
        /// <param name="isbottom">The value of IsBottom property.</param>
        public VirtualCollectionGroup(object name, bool isbottom) : base(name)
        {
            this._isBottomLevel = isbottom;
        }

        /// <summary>
        /// Adds an object to the end of the items.
        /// </summary>
        /// <param name="item">The object to be added to the end of the items.</param>
        public void AddItem(object item)
        {
            base.ProtectedItems.Add(item);
            base.ProtectedItemCount = base.ProtectedItems.Count;
        }

        /// <summary>
        /// Creates the groups using PropertyGroupDescription.
        /// </summary>
        /// <param name="dataSource">The data source.</param>
        /// <param name="propGroupDesc">The PropertyGroupDescription.</param>
        /// <param name="type">The type of elements.</param>
        /// <returns>The list of created groups</returns>
        public static List<VirtualCollectionGroup> CreateGroups(IEnumerable dataSource, PropertyGroupDescription propGroupDesc, Type type)
        {
            DataManagerBase dataManagerBase = DataManagerBase.CreateDataManager(dataSource);
            dataManagerBase.GroupByObject = GroupByContext.CreateGenericCustomGroup(dataManagerBase.CachedTypedInfo, propGroupDesc.PropertyName, null);
            List<VirtualCollectionGroup> virtualCollectionGroups = new List<VirtualCollectionGroup>();
            for (int i = 0; i < dataManagerBase.RecordCount; i++)
            {
                GroupByDataContext record = dataManagerBase.GetRecord(i) as GroupByDataContext;
                if (record != null)
                {
                    VirtualCollectionGroup virtualCollectionGroup = new VirtualCollectionGroup(record.Value, true);
                    foreach (object obj in record.Records)
                    {
                        virtualCollectionGroup.AddItem(obj);
                    }
                    virtualCollectionGroups.Add(virtualCollectionGroup);
                }
            }
            return virtualCollectionGroups;
        }

        /// <summary>
        /// Recreates the groups - replaces the items with a new VirtualCollectionGroup.
        /// </summary>
        /// <param name="groups">The groups.</param>
        /// <param name="propertyGroupDescription">The property group description.</param>
        /// <param name="type">The type of elements.</param>
        public static void ReGroup(IList<VirtualCollectionGroup> groups, PropertyGroupDescription propertyGroupDescription, Type type)
        {
            int count = groups.Count;
            for (int i = 0; i < count; i++)
            {
                VirtualCollectionGroup item = groups[i];
                groups[i] = VirtualCollectionGroup.ReGroupItems(propertyGroupDescription, type, item);
            }
        }

        private static VirtualCollectionGroup ReGroupItems(PropertyGroupDescription propertyGroupDescription, Type type, VirtualCollectionGroup group)
        {
            if (!group.IsBottomLevel)
            {
                for (int i = 0; i < group.Items.Count; i++)
                {
                    VirtualCollectionGroup item = group.Items[i] as VirtualCollectionGroup;
                    if (item != null)
                    {
                        group.ReplaceItem(i, VirtualCollectionGroup.ReGroupItems(propertyGroupDescription, type, item));
                    }
                }
                return group;
            }
            List<VirtualCollectionGroup> virtualCollectionGroups = VirtualCollectionGroup.CreateGroups(group.Items, propertyGroupDescription, type);
            VirtualCollectionGroup virtualCollectionGroup = new VirtualCollectionGroup(group.Name, false);
            int itemCount = 0;
            foreach (VirtualCollectionGroup virtualCollectionGroup1 in virtualCollectionGroups)
            {
                virtualCollectionGroup.AddItem(virtualCollectionGroup1);
                itemCount = itemCount + virtualCollectionGroup1.ItemCount;
            }
            virtualCollectionGroup.ProtectedItemCount = itemCount;
            return virtualCollectionGroup;
        }

        /// <summary>
        /// Replaces the element at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the element to replace.</param>
        /// <param name="newItem">The new item.</param>
        public void ReplaceItem(int index, object newItem)
        {
            base.ProtectedItems[index] = newItem;
        }
    }
}
