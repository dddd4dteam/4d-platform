﻿namespace System.ComponentModel
{

    public sealed class PageChangingEventArgs : CancelEventArgs
    {
        private readonly int _newpageindex;

        
        public PageChangingEventArgs(int newPageIndex)
        {
            _newpageindex = newPageIndex;
        }

        
        public int NewPageIndex
        {
            get { return _newpageindex; }
        }
    }

}
