
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows.Data;

using DDDD.Core.Environment;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.IG;
using DDDD.Core.Collections.Sort.IG;
using DDDD.Core.Collections.DataManage.IG;
using DDDD.Core.Collections.Virtual.Requests;
using DDDD.Core.Collections.Virtual.DeferredItem;



namespace DDDD.Core.Collections.Virtual
{
    /// <summary>
    /// A collection which allows a developer to simulate a collection of very large sizes without having to actually load every data element.
    /// </summary>
    /// <typeparam name="T">The type of elements in the collection</typeparam>
    public class VirtualCollection<T> : ICollectionView, INotifyCollectionChanged, IPagedCollectionView, IFilteredCollectionView, IList, ICollection, IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable, INotifyPropertyChanged, IEditableCollectionView, IDisposable
    {
        private const string MessageIndexOutOfRange = "E_IndexOutOfRange";

        private const string MessageDataIsNotLoaded = "E_DataIsNotLoadedWhenEdit";

        private const string MessageFilterNotSupported = "E_FilterNotSupported";

        private const string MessageAddNewNotSupported = "E_AddNewNotSupported";

        private const string MessageCancelEditNotSupported = "E_CancelEditNotSupported";

        private const string MessageCancelNewIsEditing = "E_CancelNewIsEditing";

        private const string MessageCommitEditWhenAdding = "E_CommitEditWhenAdding";

        private const string MessageCommitNewWhenEditing = "E_CommitNewWhenEditing";

        private const string MessageCommitNewWhenNull = "E_CommitNewWhenNull";

        private const string MessageRemoveWhenEdit = "E_RemoveWhenEdit";

        private const string MessageRefreshWhenEdit = "E_RefreshWhenEdit";

        private const string MessageIListEditing = "E_IListEdit";

        private readonly Dictionary<string, RequestedDataDescription> _requestedPages;

        private readonly Dictionary<string, RequestedDataDescription> _requestedItems;

        private readonly Dictionary<int, T> _accessedItems;

        private readonly CacheManager<T> _itemsCache;

        private readonly List<T> _pageCache;

        private readonly RecordFilterCollection _filterConditions;

        private readonly InternalSortDescriptionCollection _sortDescriptions;

        private readonly ObservableCollection<GroupDescription> _groupDescriptions;

        private ReadOnlyObservableCollection<object> _groupsReadOnly;

        private ObservableCollection<object> _groups;

        private object _syncRoot;

        private bool _canChangePage;

        private bool _isPageChanging;

        private int _pageIndex;

        private int _pageSize;

        private int _totalItemCount;

        private CultureInfo _culture;

        private object _currentItem;

        private int _currentPosition;

        private bool _isCurrentAfterLast;

        private bool _isCurrentBeforeFirst;

        private int _moveToEmptyPosition;

        private int _deferredRefreshCount;

        private bool _refreshIsWaiting;

        private object _currentAddItem;

        private object _currentEditItem;

        private int _deferredItemRequestsCount;

        private int _loadSize;

        private int _anticipatedCount;

        private bool _hasEmptyIndex;

        private int _maxAccessedIndex;

        private int _minAccessedIndex;

        private int _lastRequestedPage;

        private bool _isRequestActive;

        private object _lockObject;

        private bool _isItemDataRequestedCalling;

        /// <summary>
        /// Gets or sets the anticipated number of items in the collection.
        /// </summary>
        /// <remarks>
        /// The application must set this property with the number of actual items.
        /// This value determines the value of the <see cref="P:Collections.VirtualCollection`1.ItemCount" /> and <see cref="P:Collections.VirtualCollection`1.TotalItemCount" />.
        /// </remarks>
        /// <seealso cref="P:Collections.VirtualCollection`1.ItemCount" />
        /// <seealso cref="P:Collections.VirtualCollection`1.TotalItemCount" />
        public int AnticipatedCount
        {
            get
            {
                return this._anticipatedCount;
            }
            set
            {
                this.OnAnticipatedCountChanging(value);
            }
        }

        /// <summary>
        /// Gets a value indicating whether a new item can be added to the collection.
        /// </summary>
        /// <remarks>
        /// The CanAddNew method returns true if the specified type (T) has a default public constructor
        /// or if the application establishes an event handler for the <see cref="E:Collections.VirtualCollection`1.AddNewItem" /> event.
        /// </remarks>
        public bool CanAddNew
        {
            get
            {
                ConstructorInfo constructor = typeof(T).GetConstructor(Type.EmptyTypes);
                if (this.AddNewItem != null)
                {
                    return true;
                }
                if (constructor == null)
                {
                    return false;
                }
                return !typeof(T).IsNotPublic;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the collection view can discard pending changes and restore the original values of an edited object.
        /// </summary>
        /// <returns>true if the collection view can discard pending changes and restore the original values of an edited object; otherwise, false.</returns>
        public bool CanCancelEdit
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="P:Collections.VirtualCollection`1.PageIndex" /> value can change.
        /// </summary>
        /// <remarks>The <see cref="T:Collections.VirtualCollection`1" /> always returns true.</remarks>
        public bool CanChangePage
        {
            get
            {
                return JDG_get_CanChangePage();
            }
            set
            {
                JDG_set_CanChangePage(value);
            }
        }

        public bool JDG_get_CanChangePage()
        {
            return this._canChangePage;
        }

        private void JDG_set_CanChangePage(bool value)
        {
            if (this._canChangePage != value)
            {
                this._canChangePage = value;
                this.OnPropertyChanged("CanChangePage");
            }
        }

        /// <summary>
        /// Gets a value indicating whether filtering is supported on a particular datasource.
        /// </summary>
        /// <seealso cref="P:Collections.VirtualCollection`1.FilterConditions" />
        protected virtual bool CanFilter
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this view supports grouping by way of the <see cref="P:Collections.VirtualCollection`1.GroupDescriptions" /> property.
        /// </summary>
        public bool CanGroup
        {
            get
            {
                return this.IsPagingMode;
            }
        }

        /// <summary>
        /// Gets a value indicating whether an item can be removed from the collection.
        /// </summary>
        /// <value></value>
        /// <returns>true if an item can be removed from the collection; otherwise, false.</returns>
        public bool CanRemove
        {
            get
            {
                if (this.IsEditingItem || this.IsAddingNew)
                {
                    return false;
                }
                return !this.IsEmpty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this view supports sorting by way of the <see cref="P:Collections.VirtualCollection`1.SortDescriptions" /> property.
        /// </summary>
        public bool CanSort
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this view supports filtering by way of the <see cref="P:System.ComponentModel.ICollectionView.Filter" /> property.
        /// </summary>
        /// <returns>
        ///  <c>true</c> if the <see cref="T:Collections.VirtualCollection`1" /> can use predicate filter; otherwise, <c>false</c>.
        /// </returns>
        /// <remarks>This property returns false. Use the <see cref="P:Collections.VirtualCollection`1.FilterConditions" /> to filter items.</remarks>
        /// <seealso cref="T:System.ComponentModel.ICollectionView" />
        protected virtual bool CanUsePredicateFilter
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the count of object in the current view.
        /// </summary>
        /// <remarks>
        /// This is a number of items in a page or the count of all items in the <see cref="T:Collections.VirtualCollection`1" />.
        /// </remarks>
        /// <seealso cref="P:Collections.VirtualCollection`1.AnticipatedCount" />
        /// <seealso cref="P:Collections.VirtualCollection`1.TotalItemCount" />
        public int Count
        {
            get
            {
                return this.GetCount();
            }
        }

        /// <summary>
        /// Gets or sets the cultural information for any operations of the view that may differ by culture, such as sorting.
        /// </summary>
        public CultureInfo Culture
        {
            get
            {
                return this._culture;
            }
            set
            {
                if (this._culture != value)
                {
                    this._culture = value;
                    this.OnPropertyChanged("Culture");
                }
            }
        }

        /// <summary>
        /// Gets the item that is being added during the current add transaction.
        /// </summary>
        /// <returns>The item that is being added if <see cref="P:Collections.VirtualCollection`1.IsAddingNew" /> is true; otherwise, null.</returns>
        public object CurrentAddItem
        {
            get
            {
                return JDG_get_CurrentAddItem();
            }
            set
            {
                JDG_set_CurrentAddItem(value);
            }
        }

        public object JDG_get_CurrentAddItem()
        {
            return this._currentAddItem;
        }

        private void JDG_set_CurrentAddItem(object value)
        {
            if (this._currentAddItem != value)
            {
                this._currentAddItem = value;
                this.OnPropertyChanged("CurrentAddItem");
                this.OnPropertyChanged("IsAddingNew");
            }
        }

        /// <summary>
        /// Gets the item in the collection that is being edited.
        /// </summary>
        /// <returns>The item that is being edited if <see cref="P:Collections.VirtualCollection`1.IsEditingItem" /> is true; otherwise, null.</returns>
        public object CurrentEditItem
        {
            get
            {
                return JDG_get_CurrentEditItem();
            }
            set
            {
                JDG_set_CurrentEditItem(value);
            }
        }

        public object JDG_get_CurrentEditItem()
        {
            return this._currentEditItem;
        }

        private void JDG_set_CurrentEditItem(object value)
        {
            if (this._currentEditItem != value)
            {
                this._currentEditItem = value;
                this.OnPropertyChanged("IsEditingItem");
                this.OnPropertyChanged("CurrentEditItem");
            }
        }

        /// <summary>
        /// Gets the current item in the view.
        /// </summary>
        public object CurrentItem
        {
            get
            {
                return JDG_get_CurrentItem();
            }
            set
            {
                JDG_set_CurrentItem(value);
            }
        }

        public object JDG_get_CurrentItem()
        {
            return this._currentItem;
        }

        private void JDG_set_CurrentItem(object value)
        {
            if (this._currentItem != value)
            {
                this.OnCurrentChanging(new CurrentChangingEventArgs(false));
                this._currentItem = value;
                this.OnPropertyChanged("CurrentItem");
                this.OnCurrentChanged(EventArgs.Empty);
            }
        }

        /// <summary>
        /// Gets the ordinal position of the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> in the view.
        /// </summary>
        public int CurrentPosition
        {
            get
            {
                return JDG_get_CurrentPosition();
            }
            set
            {
                JDG_set_CurrentPosition(value);
            }
        }

        public int JDG_get_CurrentPosition()
        {
            return this._currentPosition;
        }

        private void JDG_set_CurrentPosition(int value)
        {
            if (this._currentPosition != value)
            {
                this._currentPosition = value;
                this.OnPropertyChanged("CurrentPosition");
            }
        }

        /// <summary>
        /// Gets or sets a callback that is used to determine whether an item is appropriate for inclusion in the view.
        /// </summary>
        /// <remarks>
        /// This property returns always null and throws NotSupportedException if the application sets it.
        /// </remarks>
        protected virtual Predicate<object> Filter
        {
            get
            {
                return null;
            }
            set
            {
                if (value != null)
                {
                    throw new NotSupportedException("E_FilterNotSupported"); //SR.GetString(  -  [12/15/2016 A1]
                }
            }
        }

        /// <summary>
        /// Gets a collection of FilterConditions that should be applied to the datasource.
        /// </summary>
        public RecordFilterCollection FilterConditions
        {
            get
            {
                return this._filterConditions;
            }
        }

        /// <summary>
        /// Gets a collection of <see cref="T:System.ComponentModel.GroupDescription" /> objects that describe how the items in the collection are grouped in the view.
        /// </summary>
        public ObservableCollection<GroupDescription> GroupDescriptions
        {
            get
            {
                return this._groupDescriptions;
            }
        }

        /// <summary>
        /// Gets the top-level groups.
        /// </summary>
        public ReadOnlyObservableCollection<object> Groups
        {
            get
            {
                return this._groupsReadOnly;
            }
        }

        /// <summary>
        /// Gets a value indicating whether filtering is supported on a particular datasource.
        /// </summary>
        /// <returns>true if this view supports filtering; otherwise, false.</returns>
        /// <seealso cref="P:Collections.VirtualCollection`1.FilterConditions" />
        bool IFilteredCollectionView.CanFilter
        {
            get
            {
                return this.CanFilter;
            }
        }

        /// <summary>
        /// Gets a value indicating whether an add transaction is in progress.
        /// </summary>
        public bool IsAddingNew
        {
            get
            {
                return this._currentAddItem != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> of the view is beyond the end of the collection.
        /// </summary>
        public bool IsCurrentAfterLast
        {
            get
            {
                return JDG_get_IsCurrentAfterLast();
            }
            set
            {
                JDG_set_IsCurrentAfterLast(value);
            }
        }

        public bool JDG_get_IsCurrentAfterLast()
        {
            return this._isCurrentAfterLast;
        }

        private void JDG_set_IsCurrentAfterLast(bool value)
        {
            if (this._isCurrentAfterLast != value)
            {
                this._isCurrentAfterLast = value;
                this.OnPropertyChanged("IsCurrentAfterLast");
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> of the view is beyond the start of the collection.
        /// </summary>
        public bool IsCurrentBeforeFirst
        {
            get
            {
                return JDG_get_IsCurrentBeforeFirst();
            }
            set
            {
                JDG_set_IsCurrentBeforeFirst(value);
            }
        }

        public bool JDG_get_IsCurrentBeforeFirst()
        {
            return this._isCurrentBeforeFirst;
        }

        private void JDG_set_IsCurrentBeforeFirst(bool value)
        {
            if (this._isCurrentBeforeFirst != value)
            {
                this._isCurrentBeforeFirst = value;
                this.OnPropertyChanged("IsCurrentBeforeFirst");
            }
        }

        /// <summary>
        /// Gets a value indicating whether an edit transaction is in progress.
        /// </summary>
        /// <seealso cref="P:Collections.VirtualCollection`1.CurrentEditItem" />
        public bool IsEditingItem
        {
            get
            {
                return this._currentEditItem != null;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the view is empty.
        /// </summary>
        public bool IsEmpty
        {
            get
            {
                return this.Count < 1;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T: IList" /> has a fixed size.
        /// </summary>
        /// <returns>true if the <see cref="T: IList" /> has a fixed size; otherwise, false.</returns>
        protected virtual bool IsFixedSize
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the page index is changing.
        /// </summary>
        public bool IsPageChanging
        {
            get
            {
                return JDG_get_IsPageChanging();
            }
            set
            {
                JDG_set_IsPageChanging(value);
            }
        }

        public bool JDG_get_IsPageChanging()
        {
            return this._isPageChanging;
        }

        private void JDG_set_IsPageChanging(bool value)
        {
            if (this._isPageChanging != value)
            {
                this._isPageChanging = value;
                this.OnPropertyChanged("IsPageChanging");
            }
        }

        private bool IsPagingMode
        {
            get
            {
                return this._pageSize > 0;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T: IList" /> is read-only.
        /// </summary>
        /// <returns>true if the <see cref="T: IList" /> is read-only; otherwise, false.</returns>
        protected virtual bool IsReadOnly
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T: ICollection" /> is synchronized (thread safe).
        /// </summary>
        /// <returns>true if access to the <see cref="T: ICollection" /> is synchronized (thread safe); otherwise, false.</returns>
        protected virtual bool IsSynchronized
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the item at the specified index.
        /// </summary>
        /// <param name="index">The index of an item</param>
        /// <remarks>
        /// This property provides the ability to access a specific element in the whole collection - not only from the current view.
        /// The <see cref="T:Collections.VirtualCollection`1" /> raises <see cref="E:Collections.VirtualCollection`1.ItemDataRequested" /> event if the requested item is not in internal cache.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.DeferItemRequests" />
        /// <seealso cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
        public T this[int index]
        {
            get
            {
                return this.GetVirtualItem(index);
            }
            set
            {
                this.SetItem(index, value);
            }
        }

        /// <summary>
        /// Gets the number of known items in the view before paging is applied.
        /// </summary>
        protected virtual int ItemCount
        {
            get
            {
                return this.TotalItemCount;
            }
        }

        /// <summary>
        /// Gets or sets the number of objects the collection should ask for when it encounters an empty collection index.
        /// </summary>
        /// <remarks>
        /// The VirtualCollection requests a block of data around of an empty collection index. 
        /// The number of rquested items is the larger of PageSize and LoadSize.
        /// Avoid using too small values of the LoadSize when the VirtualCollection is a source of the MS DataGrid or XamWebDataGrid.
        /// This value should depend on the number of displayed rows in the MS DataGrid or XamWebDataGrid.
        /// It is recommended that the LoadSize should be no less that one fourth from the number of visible rows.
        /// </remarks>
        /// <seealso cref="P:Collections.VirtualCollection`1.Item(System.Int32)" />
        /// <seealso cref="M:Collections.VirtualCollection`1.LoadItems(System.Int32,  IEnumerable{`0})" />
        /// <seealso cref="M:Collections.VirtualCollection`1.DeferItemRequests" />
        /// <seealso cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
        public int LoadSize
        {
            get
            {
                return this._loadSize;
            }
            set
            {
                if (this._loadSize != value)
                {
                    this._loadSize = value;
                    this.ClearCachce();
                    this.OnPropertyChanged("LoadSize");
                }
            }
        }

        /// <summary>
        /// Gets or sets the position of the new item placeholder in the collection view.
        /// </summary>
        public System.ComponentModel.NewItemPlaceholderPosition NewItemPlaceholderPosition
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the zero-based index of the current page.
        /// </summary>
        public int PageIndex
        {
            get
            {
                return JDG_get_PageIndex();
            }
            set
            {
                JDG_set_PageIndex(value);
            }
        }

         int JDG_get_PageIndex()
        {
            if (!this.IsPagingMode)
            {
                return this._pageIndex;
            }
            return Math.Max(0, this._pageIndex);
        }

        private void JDG_set_PageIndex(int value)
        {
            if (this._pageIndex != value)
            {
                this._pageIndex = value;
                this.OnPropertyChanged("PageIndex");
            }
        }



        /// <summary>
        /// Gets or sets the number of items to display on a page.
        /// </summary>
        public int PageSize
        {
            get
            {
                return this._pageSize;
            }
            set
            {
                if (this._pageSize != value)
                {
                    int count = this.Count;
                    int num = this._pageSize;
                    this._pageSize = value;
                    this.OnPropertyChanged("PageSize");
                    if (count != this.Count)
                    {
                        this.OnPropertyChanged("Count");
                    }
                    this.RefreshView();
                    if (num == 0)
                    {
                        this.OnPropertyChanged("PageIndex");
                    }
                }
            }
        }

        /// <summary>
        /// Gets a collection of <see cref="T:System.ComponentModel.SortDescription" /> instances that describe how the items in the collection are sorted in the view.
        /// </summary>
        public SortDescriptionCollection SortDescriptions
        {
            get
            {
                return this._sortDescriptions;
            }
        }

        /// <summary>
        /// Gets the source collection.
        /// </summary>
        /// <remarks>This method returns always null. </remarks>
        protected virtual IEnumerable SourceCollection
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets an object that can be used to synchronize access to the <see cref="T: ICollection" />.
        /// </summary>
        /// <returns>An object that can be used to synchronize access to the <see cref="T: ICollection" />.</returns>
        protected virtual object SyncRoot
        {
            get
            {
                if (this._syncRoot == null)
                {
                    Interlocked.CompareExchange(ref this._syncRoot, new object(), null);
                }
                return this._syncRoot;
            }
        }

        bool   ICollection<T>.IsReadOnly
        {
            get
            {
                return true;
            }
        }

        T   IList<T>.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this.SetItem(index, value);
            }
        }

        bool  ICollection.IsSynchronized
        {
            get
            {
                return this.IsSynchronized;
            }
        }

        object  ICollection.SyncRoot
        {
            get
            {
                return this.SyncRoot;
            }
        }

        bool  IList.IsFixedSize
        {
            get
            {
                return this.IsFixedSize;
            }
        }

        bool  IList.IsReadOnly
        {
            get
            {
                return this.IsReadOnly;
            }
        }

        object  IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (T)value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this view supports filtering by way of the <see cref="P:System.ComponentModel.ICollectionView.Filter" /> property.
        /// </summary>
        /// <remarks>This property returns false. Use the <see cref="P:Collections.VirtualCollection`1.FilterConditions" /> to filter items.</remarks>
        bool System.ComponentModel.ICollectionView.CanFilter
        {
            get
            {
                return this.CanUsePredicateFilter;
            }
        }

        /// <summary>
        /// Gets or sets a callback that is used to determine whether an item is appropriate for inclusion in the view.
        /// </summary>
        /// <remarks>
        /// This property returns always null and throws NotSupportedException if the application sets it.
        /// </remarks>
        Predicate<object> System.ComponentModel.ICollectionView.Filter
        {
            get
            {
                return this.Filter;
            }
            set
            {
                this.Filter = value;
            }
        }

        /// <summary>
        /// Gets the underlying collection.
        /// </summary>
        /// <remarks>The <see cref="T:Collections.VirtualCollection`1" /> returns always null.</remarks>
        IEnumerable System.ComponentModel.ICollectionView.SourceCollection
        {
            get
            {
                return this.SourceCollection;
            }
        }

        /// <summary>
        /// Gets the number of known items in the view before paging is applied.
        /// </summary>
        int System.ComponentModel.IPagedCollectionView.ItemCount
        {
            get
            {
                return this.TotalItemCount;
            }
        }

        /// <summary>
        /// Gets the total number of items in the view before paging is applied.
        /// </summary>
        /// <returns>The total number of items in the view before paging is applied, or -1 if the total number is unknown.</returns>
        public int TotalItemCount
        {
            get
            {
                return JDG_get_TotalItemCount();
            }
            set
            {
                JDG_set_TotalItemCount(value);
            }
        }

        public int JDG_get_TotalItemCount()
        {
            return this._totalItemCount;
        }

        private void JDG_set_TotalItemCount(int value)
        {
            if (this._totalItemCount != value)
            {
                this._totalItemCount = value;
                this.OnPropertyChanged("TotalItemCount");
                this.OnPropertyChanged("ItemCount");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.VirtualCollection`1" /> class.
        /// </summary>
        public VirtualCollection()
        {
            this._itemsCache = new CacheManager<T>();
            this._pageIndex = -1;
            this._groupDescriptions = new ObservableCollection<GroupDescription>();
            this._groupDescriptions.CollectionChanged += new NotifyCollectionChangedEventHandler(this.GroupDescriptions_CollectionChanged);
            this._sortDescriptions = new InternalSortDescriptionCollection();
            this._sortDescriptions.SortDescriptionsChanged += new NotifyCollectionChangedEventHandler(this.SortDescriptions_CollectionChanged);
            this._filterConditions = new RecordFilterCollection();
            this._filterConditions.CollectionChanged += new NotifyCollectionChangedEventHandler(this.FilterConditions_CollectionChanged);
            this._filterConditions.CollectionItemChanged += new EventHandler<EventArgs>(this.FilterConditions_CollectionItemChanged);
            this.CanChangePage = true;
            this._moveToEmptyPosition = -1;
            this.MoveCurrentToPosition(-1);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.VirtualCollection`1" /> class.
        /// </summary>
        /// <param name="anticipatedSize">The anticipated collection size.</param>
        public VirtualCollection(int anticipatedSize) : this()
        {
            this.AnticipatedCount = anticipatedSize;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.VirtualCollection`1" /> class.
        /// </summary>
        /// <param name="anticipatedSize">The anticipated collection size.</param>
        /// <param name="loadSize">The number of items which collection requests to load.</param>
        public VirtualCollection(int anticipatedSize, int loadSize) : this()
        {
            this.AnticipatedCount = anticipatedSize;
            this.LoadSize = loadSize;
        }

        /// <summary>
        /// Inserts a new item 
        /// </summary>
        /// <param name="item">The object to insert into the collection.</param>
        /// <returns>The current implementation throws an InvalidOperationException exception.</returns>
        /// <remarks>
        /// The current implementation throws an InvalidOperationException exception.
        /// The application must use IEditableCollectionView to modify the VirtualCollection.
        /// </remarks>
        protected virtual int Add(T item)
        {
            throw new InvalidOperationException("E_IListEdit"); //SR.GetString(  -  [12/15/2016 A1]
        }

        /// <summary>
        /// Adds a new item to the underlying collection.
        /// </summary>
        /// <returns>
        /// The new item that is added to the collection.
        /// </returns>
        public object AddNew()
        {
            this.CheckForChanges();
            if (this.IsEditingItem)
            {
                this.CommitEdit();
            }
            if (this.IsAddingNew)
            {
                this.CommitNew();
            }
            if (!this.CanAddNew)
            {
                throw new NotSupportedException("E_AddNewNotSupported"); //SR.GetString(  -  [12/15/2016 A1]
            }
            AddNewItemEventArgs<T> addNewItemEventArg = new AddNewItemEventArgs<T>();
            this.OnAddNewItem(addNewItemEventArg);
            T defaultValue = addNewItemEventArg.DefaultValue;
            this.CurrentAddItem = defaultValue;
            return defaultValue;
        }

        private void AddSortFilterGroupArgs(ItemDataRequestedEventArgs args)
        {
            args.FilterConditions = this._filterConditions;
            args.SortDescriptions = new SortDescriptionCollection();
            int i = 0;
            if (this._groupDescriptions != null && this._groupDescriptions.Count > 0)
            {
                while (i < this._groupDescriptions.Count)
                {
                    PropertyGroupDescription item = this._groupDescriptions[i] as PropertyGroupDescription;
                    if (item != null)
                    {
                        ListSortDirection direction = ListSortDirection.Ascending;
                        if (i < this._sortDescriptions.Count && this._sortDescriptions[i].PropertyName == item.PropertyName)
                        {
                            direction = this._sortDescriptions[i].Direction;
                        }
                        SortDescription sortDescription = new SortDescription(item.PropertyName, direction);
                        args.SortDescriptions.Add(sortDescription);
                    }
                    i++;
                }
            }
            for (i = 0; i < this._sortDescriptions.Count; i++)
            {
                if (!VirtualCollection<T>.SortDescriptionsContains(args.SortDescriptions, this._sortDescriptions[i].PropertyName))
                {
                    args.SortDescriptions.Add(this._sortDescriptions[i]);
                }
            }
        }

        private int CalcStartIndexToRequest(int minIndex, int maxIndex, out int size)
        {
            int num = maxIndex - minIndex + 1;
            size = Math.Max(this.LoadSize, num);
            if (size > this.TotalItemCount)
            {
                size = this.TotalItemCount;
            }
            int num1 = minIndex - (size - num) / 2;
            if (num1 < 0 || size == this.TotalItemCount)
            {
                num1 = 0;
            }
            else
            {
                int num2 = num1 + size - this.TotalItemCount;
                if (num2 > 0)
                {
                    num1 = num1 - num2;
                }
            }
            return num1;
        }

        /// <summary>
        /// Ends the edit transaction and, if possible, restores the original value of the item.
        /// </summary>
        /// <seealso cref="P:Collections.VirtualCollection`1.CanCancelEdit" />
        public void CancelEdit()
        {
            if (this.IsEditingItem)
            {
                if (!this.CanCancelEdit)
                {
                    throw new NotSupportedException("E_CancelEditNotSupported"); //SR.GetString(   -  [12/15/2016 A1]
                }
                this.CheckForChanges();
                this.CurrentEditItem = null;
            }
        }

        /// <summary>
        /// Ends the add transaction and discards the pending new item.
        /// </summary>
        public void CancelNew()
        {
            if (!this.IsAddingNew)
            {
                return;
            }
            if (this.IsEditingItem)
            {
                throw new InvalidOperationException("E_CancelNewIsEditing");  //SR.GetString( -  [12/15/2016 A1]
            }
            this.CurrentAddItem = null;
        }

        private void ChangePage(int pageIndex)
        {
            this.IsPageChanging = this.PageIndex != pageIndex;
            this.PageIndex = pageIndex;
            if (this.IsPageChanging)
            {
                this.IsPageChanging = false;
                this.OnPageChanged(EventArgs.Empty);
            }
            this.RaiseViewChanged();
        }

        private void CheckForChanges()
        {
            if (this._deferredRefreshCount > 0)
            {
                throw new InvalidOperationException("E_DataIsNotLoadedWhenEdit"); //SR.GetString(  - [12/15/2016 A1]
            }
        }

        private void CheckIndex(int index)
        {
            if (index >= this.Count || index < 0)
            {
                throw new ArgumentOutOfRangeException("index", "E_IndexOutOfRange");   //SR.GetString(  -  [12/15/2016 A1]
            }
        }

        /// <summary>
        /// Removes all items.
        /// </summary>
        /// <remarks>
        /// The current implementation throws an InvalidOperationException exception.
        /// The application must use IEditableCollectionView to modify the VirtualCollection.
        /// </remarks>
        protected virtual void Clear()
        {
            throw new InvalidOperationException( "E_IListEdit");   // SR.GetString(   -  [12/15/2016 A1]
        }

        private void ClearCachce()
        {
            this.MoveCurrentToPosition(-1);
            this.GetCount();
            this._pageCache.Clear();
            this._itemsCache.ClearCache();
            this._itemsCache.LoadSize = Math.Max(this.PageSize, this.LoadSize);
        }

        private void ClearGroups()
        {
            if (this._groups != null)
            {
                this._groups.Clear();
                this._groups = null;
                this._groupsReadOnly = null;
                this.OnPropertyChanged("Groups");
            }
        }

        /// <summary>
        /// Ends the edit transaction and saves the pending changes.
        /// </summary>
        public void CommitEdit()
        {
            if (this.IsAddingNew)
            {
                throw new InvalidOperationException( "E_CommitEditWhenAdding");    // SR.GetString( -  [12/15/2016 A1]
            }
            this.CheckForChanges();
            if (this.IsEditingItem)
            {
                T currentEditItem = (T)this.CurrentEditItem;
                this.CurrentEditItem = null;
                this.OnCommitEditItem(new CommitEditItemEventArgs<T>(currentEditItem));
            }
        }

        /// <summary>
        /// Ends the add transaction and saves the pending new item.
        /// </summary>
        public void CommitNew()
        {
            if (this.IsEditingItem)
            {
                throw new InvalidOperationException("E_CommitNewWhenEditing");     // SR.GetString(  -  [12/15/2016 A1]
            }
            this.CheckForChanges();
            if (!this.IsAddingNew)
            {
                throw new InvalidOperationException("E_CommitNewWhenNull");  // SR.GetString(   -  [12/15/2016 A1]
            }
            this.OnCommitNewItem(new CommitNewItemEventArgs<T>((T)this.CurrentAddItem));
            this.CurrentAddItem = null;
            this.OnPropertyChanged("ItemCount");
        }

        /// <summary>
        /// Indicates whether the specified item belongs to this collection view.
        /// </summary>
        /// <param name="item">The object to check.</param>
        /// <returns>
        /// true if the item belongs to this collection view; otherwise, false.
        /// </returns>
        public bool Contains(object item)
        {
            if (this.IsPageChanging)
            {
                return false;
            }
            return this.IndexOf(item) > -1;
        }

        /// <summary>
        /// Copies the elements from the current view of the <see cref="T:Collections.VirtualCollection`1" /> to an <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <param name="array">The one-dimensional <see cref="T:System.Array" /> that is the destination of the elements copied from <see cref="T:  ICollection`1" />. The <see cref="T:System.Array" /> must have zero-based indexing.</param>
        /// <param name="arrayIndex">The zero-based index in <paramref name="array" /> at which copying begins.</param>
        protected virtual void CopyTo(T[] array, int arrayIndex)
        {
            for (int i = 0; i < this.Count; i++)
            {
                array[i + arrayIndex] = (T)this.GetItemAt(i);
            }
        }

        private void CreateGroups()
        {
            if (this.GroupDescriptions.Count <= 0 || this.Count <= 0)
            {
                this.ClearGroups();
                return;
            }
            T[] tArray = new T[this.Count];
            this.CopyTo(tArray, 0);
            DataManagerBase dataManagerBase = DataManagerBase.CreateDataManager(tArray);
            if (dataManagerBase == null)
            {
                return;
            }
            this.ClearGroups();
            if (this._groups == null)
            {
                this._groups = new ObservableCollection<object>();
                this._groupsReadOnly = new ReadOnlyObservableCollection<object>(this._groups);
            }
            if (this.CanGroup)
            {
                for (int i = 0; i < dataManagerBase.RecordCount; i++)
                {
                    GroupByDataContext record = dataManagerBase.GetRecord(i) as GroupByDataContext;
                    if (record != null)
                    {
                        VirtualCollectionGroup virtualCollectionGroup = new VirtualCollectionGroup(record.Value, true);
                        foreach (object obj in record.Records)
                        {
                            virtualCollectionGroup.AddItem(obj);
                        }
                        this._groups.Add(virtualCollectionGroup);
                    }
                }
                List<VirtualCollectionGroup> virtualCollectionGroups = VirtualCollectionGroup.CreateGroups(tArray, this.GroupDescriptions[0] as PropertyGroupDescription, typeof(T));
                for (int j = 1; j < this.GroupDescriptions.Count; j++)
                {
                    VirtualCollectionGroup.ReGroup(virtualCollectionGroups, this.GroupDescriptions[j] as PropertyGroupDescription, typeof(T));
                }
                this._groups.Clear();
                foreach (VirtualCollectionGroup virtualCollectionGroup1 in virtualCollectionGroups)
                {
                    this._groups.Add(virtualCollectionGroup1);
                }
            }
            if (this._groups.Count == 0)
            {
                this.ClearGroups();
            }
            this.OnPropertyChanged("Groups");
        }

        /// <summary>
        /// Enters a defer cycle that you can use to access contiguous block of items and delay automatic loading of items.
        /// </summary>
        /// <returns>
        /// The typical usage is to create a using scope with an implementation of this method and then access a contiguous block of items.
        /// </returns>
        /// <remarks>
        /// All the methods inside the using block will no longer cause a ItemDataRequested event, they will just indicate that a loading of items is needed. 
        /// Then when the code leaves the using block the collection will request a block of items that includes all accessed items.
        /// </remarks>
        public IDisposable DeferItemRequests()
        {
            VirtualCollection<T> virtualCollection = this;
            virtualCollection._deferredItemRequestsCount = virtualCollection._deferredItemRequestsCount + 1;
            this._maxAccessedIndex = -1;
            this._minAccessedIndex = 2147483647;
            this._hasEmptyIndex = false;
            return new DeferredItemRequest<T>(this);
        }

        /// <summary>
        /// Enters a defer cycle that you can use to merge changes to the view and delay automatic refresh.
        /// </summary>
        /// <returns>
        /// The typical usage is to create a using scope with an implementation of this method and then include multiple view-changing calls within the scope. The implementation should delay automatic refresh until after the using scope exits.
        /// </returns>
        public IDisposable DeferRefresh()
        {
            VirtualCollection<T> virtualCollection = this;
            virtualCollection._deferredRefreshCount = virtualCollection._deferredRefreshCount + 1;
            return new DeferredRefresh<T>(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this._filterConditions.Dispose();
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Begins an edit transaction on the specified item.
        /// </summary>
        /// <param name="item">The item to edit.</param>
        public void EditItem(object item)
        {
            this.CheckForChanges();
            if (this.IsAddingNew)
            {
                if (object.Equals(item, this.CurrentAddItem))
                {
                    return;
                }
                this.CommitNew();
            }
            this.CommitEdit();
            this.CurrentEditItem = item;
        }

        private void FilterConditions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.RefreshView();
        }

        private void FilterConditions_CollectionItemChanged(object sender, EventArgs e)
        {
            this.RefreshView();
        }

        private int GetCount()
        {
            int pageIndex = this.PageIndex;
            if (this._lastRequestedPage != -1 && this.PageIndex != this._lastRequestedPage)
            {
                pageIndex = this._lastRequestedPage;
            }
            if (!this.IsPagingMode)
            {
                return this.ItemCount;
            }
            return this.GetPageSize(pageIndex);
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T: IEnumerator" /> object that can be used to iterate through 
        /// the current page when the <see cref="P:Collections.VirtualCollection`1.PageSize" /> is greater than 0 or it can be used to iterate through the cached items.
        /// </returns>
        public IEnumerator GetEnumerator()
        {
            return new VirtualCollectionEnumerator<T>(this);
        }

        /// <summary>
        /// Gets the item at the specified index.
        /// </summary>
        /// <param name="index">The index of an item</param>
        /// <returns>The item at the specified index</returns>
        protected virtual object GetItemAt(int index)
        {
            return this.GetVirtualItem(index);
        }

        private T GetItemFromCache(int index)
        {
            if (this.IsPagingMode)
            {
                if (index < this._pageCache.Count)
                {
                    return this._pageCache[index];
                }
                index = index + this.PageIndex * this.PageSize;
            }
            return this._itemsCache.GetItem(index);
        }

        private int GetPageCount()
        {
            if (this.PageSize <= 0)
            {
                return 0;
            }
            return Math.Max(1, (int)Math.Ceiling((double)this.ItemCount / (double)this.PageSize));
        }

        private int GetPageSize(int pageIndex)
        {
            if (pageIndex < 0)
            {
                return Math.Min(this._pageSize, this.TotalItemCount);
            }
            int totalItemCount = this.TotalItemCount - pageIndex * this._pageSize;
            if (totalItemCount > this._pageSize)
            {
                return this._pageSize;
            }
            if (pageIndex <= this.GetPageCount() && totalItemCount >= 0)
            {
                return totalItemCount;
            }
            return 0;
        }

        private RequestedDataDescription GetRequest(int index)
        {
            RequestedDataDescription requestedDataDescription;
            Dictionary<string, RequestedDataDescription>.ValueCollection.Enumerator enumerator = this._requestedItems.Values.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    RequestedDataDescription current = enumerator.Current;
                    if (index < current.StartIndex || index >= current.StartIndex + current.Size)
                    {
                        continue;
                    }
                    requestedDataDescription = current;
                    return requestedDataDescription;
                }
                return null;
            }
            finally
            {
                ((IDisposable)enumerator).Dispose();
            }
            return requestedDataDescription;
        }

        private T GetVirtualItem(int index)
        {
            int num;
            T itemFromCache;
            this.CheckIndex(index);
            if (this.IsLoaded(index))
            {
                return this.GetItemFromCache(index);
            }
            T t = default(T);
            lock (this._lockObject)
            {
                this._isRequestActive = true;
                if (this._deferredItemRequestsCount > 0)
                {
                    this._hasEmptyIndex = true;
                    if (this._minAccessedIndex > index)
                    {
                        this._minAccessedIndex = index;
                    }
                    if (this._maxAccessedIndex < index)
                    {
                        this._maxAccessedIndex = index;
                    }
                }
                else if (!this.IsPagingMode)
                {
                    int request = this.CalcStartIndexToRequest(index, index, out num);
                    this.RequestDataForIndex(index, num, request);
                }
                else if (!this.IsRequested(index))
                {
                    this.RequestPageData(this.PageIndex);
                }
                this._isRequestActive = false;
                if (!this.IsLoaded(index))
                {
                    if (this._requestedItems.Count >= this._itemsCache.BlocksCount)
                    {
                        this._itemsCache.Extend();
                    }
                    if (!this._accessedItems.ContainsKey(index))
                    {
                        this._accessedItems.Add(index, t);
                    }
                    return t;
                }
                else
                {
                    itemFromCache = this.GetItemFromCache(index);
                }
            }
            return itemFromCache;
        }

        private void GroupDescriptions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.RefreshView();
        }

        /// <summary>
        /// Searches for the specified object and returns the zero-based index of the first occurrence within the loaded items
        /// </summary>
        /// <param name="item"> The object to locate</param>
        /// <returns>The zero-based index of the first occurrence of item, if found; otherwise, –1.</returns>
        protected virtual int IndexOf(object item)
        {
            if (!(item is T))
            {
                return -1;
            }
            if (this.IsPagingMode)
            {
                return this._pageCache.IndexOf((T)item);
            }
            return this._itemsCache.IndexOf((T)item);
        }

        /// <summary>
        /// Inserts an item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index at which <paramref name="item" /> should be inserted.</param>
        /// <param name="item">The object to insert into the collection.</param>
        /// <remarks>
        /// The current implementation throws an InvalidOperationException exception.
        /// The application must use IEditableCollectionView to modify the VirtualCollection.
        /// </remarks>
        protected virtual void Insert(int index, T item)
        {
            throw new InvalidOperationException("E_IListEdit");  // SR.GetString(  -  [12/15/2016 A1]
        }

        private bool IsLoaded(int index)
        {
            if (index < 0)
            {
                return false;
            }
            if (this.IsPagingMode)
            {
                if (index < this._pageCache.Count)
                {
                    return true;
                }
                index = index + this.PageIndex * this.PageSize;
            }
            return this._itemsCache.GetCacheForItem(index) != null;
        }

        private bool IsRequested(int index)
        {
            bool flag;
            if (!this.IsPagingMode)
            {
                foreach (RequestedDataDescription value in this._requestedItems.Values)
                {
                    if (index < value.StartIndex || index >= value.StartIndex + value.Size)
                    {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
                return false;
            }
            else
            {
                foreach (RequestedDataDescription requestedDataDescription in this._requestedPages.Values)
                {
                    int pageSize = this.GetPageSize(requestedDataDescription.PageIndex);
                    if (requestedDataDescription.StartIndex < 0 || requestedDataDescription.Size < pageSize)
                    {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
                return false;
            }
            return flag;
        }

        /// <summary>
        /// Loads the list of items into the collection cache.
        /// </summary>
        /// <param name="index">The start index.</param>
        /// <param name="items">The new items.</param>
        /// <remarks>
        /// The application must add actual items to the <see cref="T:Collections.VirtualCollection`1" /> when the collection requests a block of items. 
        /// The application can load items synchronous or asynchronous (using WCF service for example).
        /// This method adds items the VirtualCollection internal cache.
        /// It is possible, but it is not recommended to use this method to load block of items that is not requested from the <see cref="T:Collections.VirtualCollection`1" />.
        /// </remarks>
        /// <seealso cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
        public void LoadItems(int index, IEnumerable<T> items)
        {
            T t;
            lock (this._lockObject)
            {
                int count = this.Count;
                int currentPosition = this.CurrentPosition;
                int num = this._itemsCache.LoadItems(index, items);
                string str = index.ToString(CultureInfo.InvariantCulture);
                if (!this.IsPagingMode)
                {
                    if (!this._requestedItems.ContainsKey(str))
                    {
                        CachedBlock<T> cacheForItem = this._itemsCache.GetCacheForItem(index);
                        for (int i = 0; i < num; i++)
                        {
                            int num1 = i + index;
                            if (cacheForItem.IsOverwritten(num1) && !this._accessedItems.ContainsKey(num1))
                            {
                                this._accessedItems.Add(num1, default(T));
                            }
                        }
                    }
                    else
                    {
                        this._requestedItems.Remove(str);
                        this.CreateGroups();
                    }
                    if (this._deferredRefreshCount < 1)
                    {
                        List<int> nums = new List<int>();
                        foreach (int key in this._accessedItems.Keys)
                        {
                            if (!this._itemsCache.TryGetItem(key, out t))
                            {
                                continue;
                            }
                            nums.Add(key);
                        }
                        nums.Sort();
                        foreach (int num2 in nums)
                        {
                            T item = this._accessedItems[num2];
                            this._accessedItems.Remove(num2);
                            this.NotifyItemChanged(item, num2);
                        }
                        if (this.IsLoaded(currentPosition))
                        {
                            T t1 = default(T);
                            if (!object.Equals(this[currentPosition], t1) && this._currentItem != null && this._moveToEmptyPosition == currentPosition)
                            {
                                this.OnCurrentChanged(EventArgs.Empty);
                            }
                        }
                        else
                        {
                            this.MoveCurrentToPosition(-1);
                        }
                    }
                }
                else
                {
                    bool flag = false;
                    int pageIndex = this.PageIndex;
                    if (this._requestedPages.ContainsKey(str))
                    {
                        RequestedDataDescription requestedDataDescription = this._requestedPages[str];
                        if (this._lastRequestedPage != this.PageIndex || this.PageIndex == requestedDataDescription.PageIndex)
                        {
                            if (num >= 1)
                            {
                                pageIndex = requestedDataDescription.PageIndex;
                                flag = this.TryLoadPageFromInternalCache(requestedDataDescription.PageIndex);
                            }
                            else
                            {
                                this._pageCache.Clear();
                            }
                        }
                        this._requestedPages.Remove(str);
                    }
                    else if (!this.IsPageChanging && this.PageIndex >= 0)
                    {
                        flag = this.TryLoadPageFromInternalCache(this.PageIndex);
                    }
                    if (flag)
                    {
                        this.OnNewPageLoaded(pageIndex);
                    }
                    else if (this.UpdateCurrentPageFromInternalCache(index, num))
                    {
                        this.RaiseViewChanged();
                    }
                }
                if (count != this.Count)
                {
                    this.OnPropertyChanged("Count");
                }
                this._itemsCache.Current.ResetChanges();
            }
        }

        /// <summary>
        /// Sets the specified item in the view as the <see cref="P:System.ComponentModel.ICollectionView.CurrentItem" />.
        /// </summary>
        /// <param name="item">The item to set as the current item.</param>
        /// <returns>
        /// true if the resulting <see cref="P:System.ComponentModel.ICollectionView.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        public bool MoveCurrentTo(object item)
        {
            int num = this.IndexOf(item);
            if (num >= 0)
            {
                return this.MoveCurrentToPosition(num);
            }
            this._moveToEmptyPosition = this._currentPosition;
            return false;
        }

        /// <summary>
        /// Sets the first item in the view as the <see cref="P:Collections.VirtualCollection`1.CurrentItem" />.
        /// </summary>
        /// <returns>
        /// true if the resulting <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        public bool MoveCurrentToFirst()
        {
            return this.MoveCurrentToPosition(0);
        }

        /// <summary>
        /// Sets the last item in the view as the <see cref="P:Collections.VirtualCollection`1.CurrentItem" />.
        /// </summary>
        /// <returns>
        /// true if the resulting <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        public bool MoveCurrentToLast()
        {
            return this.MoveCurrentToPosition(this.Count - 1);
        }

        /// <summary>
        /// Sets the item after the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> in the view as the <see cref="P:Collections.VirtualCollection`1.CurrentItem" />.
        /// </summary>
        /// <returns>
        /// true if the resulting <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        public bool MoveCurrentToNext()
        {
            return this.MoveCurrentToPosition(this._currentPosition + 1);
        }

        /// <summary>
        /// Sets the item at the specified index to be the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> in the view.
        /// </summary>
        /// <param name="position">The index to set the <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> to.</param>
        /// <returns>
        /// true if the resulting <see cref="P:Collections.VirtualCollection`1.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        /// <remarks>
        /// This method returns false if the item at specified index has not been loaded yet.
        /// </remarks>
        public bool MoveCurrentToPosition(int position)
        {
            if (position == this._currentPosition)
            {
                return false;
            }
            if (position == -1)
            {
                this.CurrentPosition = position;
                this.CurrentItem = null;
                this.IsCurrentAfterLast = false;
                this.IsCurrentBeforeFirst = true;
                return false;
            }
            bool flag = position >= this.Count;
            if (position >= 0 && this.IsLoaded(position) && !flag)
            {
                this.CurrentPosition = position;
                this.CurrentItem = this.GetItemFromCache(position);
                this.IsCurrentAfterLast = false;
                this.IsCurrentBeforeFirst = false;
                return true;
            }
            if (flag)
            {
                this.CurrentPosition = this.Count;
                this.CurrentItem = null;
                this.IsCurrentAfterLast = true;
                this.IsCurrentBeforeFirst = false;
            }
            return false;
        }

        /// <summary>
        /// Sets the item before the <see cref="P:System.ComponentModel.ICollectionView.CurrentItem" /> in the view to the <see cref="P:System.ComponentModel.ICollectionView.CurrentItem" />.
        /// </summary>
        /// <returns>
        /// true if the resulting <see cref="P:System.ComponentModel.ICollectionView.CurrentItem" /> is an item in the view; otherwise, false.
        /// </returns>
        public bool MoveCurrentToPrevious()
        {
            return this.MoveCurrentToPosition(this._currentPosition - 1);
        }

        /// <summary>
        /// Sets the first page as the current page.
        /// </summary>
        /// <returns>
        /// true if the operation was successful; otherwise, false.
        /// </returns>
        public bool MoveToFirstPage()
        {
            return this.MoveToPage(0);
        }

        /// <summary>
        /// Sets the last page as the current page.
        /// </summary>
        /// <returns>
        /// true if the operation was successful; otherwise, false.
        /// </returns>
        public bool MoveToLastPage()
        {
            if (this.PageSize <= 0 || this.TotalItemCount == -1)
            {
                return false;
            }
            return this.MoveToPage(this.GetPageCount() - 1);
        }

        /// <summary>
        /// Moves to the page after the current page.
        /// </summary>
        /// <returns>
        /// true if the operation was successful; otherwise, false.
        /// </returns>
        public bool MoveToNextPage()
        {
            if (this._lastRequestedPage != -1)
            {
                return this.MoveToPage(this._lastRequestedPage + 1);
            }
            return this.MoveToPage(this._pageIndex + 1);
        }

        /// <summary>
        /// Moves to the page at the specified index.
        /// </summary>
        /// <param name="pageIndex">The index of the page to move to.</param>
        /// <returns>
        /// true if the operation was successful; otherwise, false.
        /// </returns>
        public bool MoveToPage(int pageIndex)
        {
            if (pageIndex < -1 || pageIndex >= this.GetPageCount())
            {
                return false;
            }
            if (this._pageIndex == pageIndex)
            {
                return false;
            }
            if (!this.CanChangePage)
            {
                return false;
            }
            if (pageIndex == -1 && this.PageSize > 0)
            {
                return false;
            }
            if (pageIndex == -1)
            {
                this.ChangePage(pageIndex);
            }
            else
            {
                this.RequestPageData(pageIndex);
            }
            return this.IsPagingMode;
        }

        /// <summary>
        /// Moves to the page before the current page.
        /// </summary>
        /// <returns>
        /// true if the operation was successful; otherwise, false.
        /// </returns>
        public bool MoveToPreviousPage()
        {
            if (this._lastRequestedPage != -1)
            {
                return this.MoveToPage(this._lastRequestedPage - 1);
            }
            return this.MoveToPage(this._pageIndex - 1);
        }

        private void NotifyItemChanged(T oldItem, int collectionIndex)
        {
            T item = this._itemsCache.GetItem(collectionIndex);
            this.RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, (object)item, collectionIndex));
            NotifyCollectionChangedEventArgs notifyCollectionChangedEventArg = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, (object)oldItem, collectionIndex + 1);
            this.RaiseCollectionChanged(notifyCollectionChangedEventArg);
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.AddNewItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:Collections.AddNewItemEventArgs`1" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.AddNewItem" />
        /// <seealso cref="T:Collections.AddNewItemEventArgs`1" />
        protected virtual void OnAddNewItem(AddNewItemEventArgs<T> args)
        {
            if (this.AddNewItem != null)
            {
                this.AddNewItem(this, args);
            }
        }

        private void OnAnticipatedCountChanged(int newValue)
        {
            int count = this.Count;
            this.TotalItemCount = newValue;
            this.OnPropertyChanged("AnticipatedCount");
            this.OnPropertyChanged("ItemCount");
            if (count != this.Count)
            {
                this.OnPropertyChanged("Count");
            }
            this.RefreshView();
        }

        private void OnAnticipatedCountChanging(int newValue)
        {
            if (this._anticipatedCount == newValue)
            {
                return;
            }
            this._anticipatedCount = newValue;
            if (!this._isItemDataRequestedCalling)
            {
                this.OnAnticipatedCountChanged(newValue);
                return;
            }
 
            Environment2.DISP.BeginInvoke(  new Action( ()=>  OnAnticipatedCountChanged(newValue) )  );

            //Deployment.Current.Dispatcher.BeginInvoke(() => this.OnAnticipatedCountChanged(newValue));
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.CollectionChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T: Specialized.NotifyCollectionChangedEventArgs" /> instance containing the event data.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.CommitEditItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:Collections.CommitEditItemEventArgs`1" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.CommitEditItem" />
        /// <seealso cref="T:Collections.CommitEditItemEventArgs`1" />
        protected virtual void OnCommitEditItem(CommitEditItemEventArgs<T> args)
        {
            if (this.CommitEditItem != null)
            {
                this.CommitEditItem(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.CommitNewItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:Collections.CommitNewItemEventArgs`1" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.CommitNewItem" />
        /// <seealso cref="T:Collections.CommitNewItemEventArgs`1" />
        protected virtual void OnCommitNewItem(CommitNewItemEventArgs<T> args)
        {
            if (this.CommitNewItem != null)
            {
                this.CommitNewItem(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.CurrentChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnCurrentChanged(EventArgs args)
        {
            if (this.CurrentChanged != null)
            {
                this.CurrentChanged(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.CurrentChanging" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:System.ComponentModel.CurrentChangingEventArgs" /> instance containing the event data.</param>
        protected virtual void OnCurrentChanging(CurrentChangingEventArgs args)
        {
            if (this.CurrentChanging != null)
            {
                this.CurrentChanging(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.ItemDataRequested" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:Collections.ItemDataRequestedEventArgs" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.ItemDataRequested" />
        /// <seealso cref="T:Collections.ItemDataRequestedEventArgs" />
        protected virtual void OnItemDataRequested(ItemDataRequestedEventArgs args)
        {
            if (this.ItemDataRequested != null)
            {
                this._isItemDataRequestedCalling = true;
                this.ItemDataRequested(this, args);
                this._isItemDataRequestedCalling = false;
            }
        }

        private void OnNewPageLoaded(int pageIndex)
        {
            this.CreateGroups();
            if (!this._isRequestActive)
            {
                this.ChangePage(pageIndex);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.PageChanged" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:System.EventArgs" /> instance containing the event data.</param>
        protected virtual void OnPageChanged(EventArgs args)
        {
            if (this.PageChanged != null)
            {
                this.PageChanged(this, args);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.PageChanging" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:System.ComponentModel.PageChangingEventArgs" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.PageChanging" />
        /// <seealso cref="T:System.ComponentModel.PageChangingEventArgs" />
        protected virtual void OnPageChanging(PageChangingEventArgs args)
        {
            if (this.PageChanging != null)
            {
                this.PageChanging(this, args);
            }
        }

        /// <summary>
        /// Called when a property value changes. 
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Raises the <see cref="E:Collections.VirtualCollection`1.RemoveItem" /> event.
        /// </summary>
        /// <param name="args">The <see cref="T:Collections.RemoveItemEventArgs`1" /> instance containing the event data.</param>
        /// <seealso cref="E:Collections.VirtualCollection`1.RemoveItem" />
        /// <seealso cref="T:Collections.RemoveItemEventArgs`1" />
        protected virtual void OnRemoveItem(RemoveItemEventArgs<T> args)
        {
            if (this.RemoveItem != null)
            {
                this.RemoveItem(this, args);
            }
        }

        private void OnViewChanged()
        {
            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
            this._refreshIsWaiting = false;
        }

        private void RaiseCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (this._deferredRefreshCount >= 1)
            {
                this._refreshIsWaiting = true;
                return;
            }
            this.OnCollectionChanged(args);
            this._refreshIsWaiting = false;
        }

        private void RaiseViewChanged()
        {
            this.RaiseCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        /// <summary>
        /// Recreates the view.
        /// </summary>
        public void Refresh()
        {
            if (this.IsAddingNew || this.IsEditingItem)
            {
                throw new InvalidOperationException("E_RefreshWhenEdit");  // SR.GetString(  -  [12/15/2016 A1]
            }
            this.ReloadCurrentView();
        }

        private void RefreshView()
        {
            if (this._deferredRefreshCount < 1)
            {
                this.ReloadCurrentView();
                return;
            }
            this._refreshIsWaiting = true;
        }

        private void ReloadCurrentView()
        {
            this.ResetCacheAndDataRequests();
            this.CreateGroups();
            this.OnViewChanged();
        }

        /// <summary>
        /// Removes the specified item from the collection.
        /// </summary>
        /// <param name="item">The item to remove.</param>
        public void Remove(object item)
        {
            if (this.IsEditingItem || this.IsAddingNew)
            {
                throw new InvalidOperationException("E_RemoveWhenEdit");    // SR.GetString(  //  [12/15/2016 A1]
            }
            this.CheckForChanges();
            if (item is T)
            {
                this.OnRemoveItem(new RemoveItemEventArgs<T>((T)item));
                this.OnPropertyChanged("ItemCount");
            }
        }

        /// <summary>
        /// Removes the item at the specified position from the collection.
        /// </summary>
        /// <param name="index">Index of item to remove.</param>
        public void RemoveAt(int index)
        {
            if (this.IsEditingItem || this.IsAddingNew)
            {
                throw new InvalidOperationException("E_RemoveWhenEdit");     // SR.GetString(   //  [12/15/2016 A1]
            }
            this.CheckIndex(index);
            this.CheckForChanges();
            this.OnRemoveItem(new RemoveItemEventArgs<T>(index));
            this.OnPropertyChanged("ItemCount");
        }

        /// <summary>
        /// Removes the item at the specified index.
        /// </summary>
        /// <param name="index">The zero-based index of the item to remove.</param>
        /// <remarks>
        /// The current implementation throws an InvalidOperationException exception.
        /// The application must use IEditableCollectionView to modify the VirtualCollection.
        /// </remarks>
        /// <seealso cref="T: IList" />
        protected virtual void RemoveItemAt(int index)
        {
            throw new InvalidOperationException("E_IListEdit");   // SR.GetString(    - [12/15/2016 A1]
        }

        private void RequestAccessedItems()
        {
            int num;
            if (this._hasEmptyIndex)
            {
                int request = this.CalcStartIndexToRequest(this._minAccessedIndex, this._maxAccessedIndex, out num);
                this.RequestDataForIndex(this._minAccessedIndex, num, request);
                this._hasEmptyIndex = false;
            }
        }

        private void RequestData(int startIndex, int size)
        {
            ItemDataRequestedEventArgs itemDataRequestedEventArg = new ItemDataRequestedEventArgs(startIndex, size);
            this.AddSortFilterGroupArgs(itemDataRequestedEventArg);
            this.OnItemDataRequested(itemDataRequestedEventArg);
        }

        private void RequestDataForIndex(int index, int size, int startIndex)
        {
            if (this._deferredItemRequestsCount > 0)
            {
                this._hasEmptyIndex = true;
                return;
            }
            if (!this.IsRequested(index))
            {
                int firstCachedIndex = startIndex + size - 1;
                CachedBlock<T> cacheForItem = this._itemsCache.GetCacheForItem(startIndex);
                CachedBlock<T> cachedBlock = this._itemsCache.GetCacheForItem(firstCachedIndex);
                if (cacheForItem != null)
                {
                    startIndex = cacheForItem.LastCachedIndex + 1;
                    firstCachedIndex = Math.Min(startIndex + size - 1, this.TotalItemCount - 1);
                }
                else if (cachedBlock != null)
                {
                    firstCachedIndex = cachedBlock.FirstCachedIndex - 1;
                    startIndex = Math.Max(0, firstCachedIndex - size + 1);
                }
                if (this.IsRequested(firstCachedIndex))
                {
                    firstCachedIndex = this.GetRequest(firstCachedIndex).StartIndex - 1;
                    startIndex = Math.Max(0, firstCachedIndex - size + 1);
                }
                if (this.IsRequested(startIndex))
                {
                    RequestedDataDescription request = this.GetRequest(startIndex);
                    startIndex = request.StartIndex + request.Size;
                    firstCachedIndex = Math.Min(startIndex + size - 1, this.TotalItemCount - 1);
                }
                size = firstCachedIndex - startIndex + 1;
                RequestedDataDescription requestedDataDescription = new RequestedDataDescription(-1, startIndex, size);
                if (this.ItemDataRequested != null)
                {
                    this._requestedItems.Add(requestedDataDescription.Key, requestedDataDescription);
                }
                this.RequestData(startIndex, size);
            }
        }

        private void RequestPageData(int pageIndex)
        {
            int num;
            this.IsPageChanging = this.PageIndex != pageIndex;
            if (this.IsPageChanging)
            {
                PageChangingEventArgs pageChangingEventArg = new PageChangingEventArgs(pageIndex);
                this.OnPageChanging(pageChangingEventArg);
                if (pageChangingEventArg.Cancel)
                {
                    this.IsPageChanging = false;
                    return;
                }
            }
            int request = pageIndex * this._pageSize;
            int pageSize = request + this.GetPageSize(pageIndex) - 1;
            request = this.CalcStartIndexToRequest(request, pageSize, out num);
            RequestedDataDescription requestedDataDescription = new RequestedDataDescription(pageIndex, request, num);
            this._lastRequestedPage = pageIndex;
            if (!this.TryLoadPageFromInternalCache(pageIndex))
            {
                if (!this._requestedPages.ContainsKey(requestedDataDescription.Key))
                {
                    if (this.ItemDataRequested != null)
                    {
                        this._requestedPages.Add(requestedDataDescription.Key, requestedDataDescription);
                    }
                    this.RequestData(request, num);
                }
                return;
            }
            this.OnNewPageLoaded(pageIndex);
            if (this._requestedPages.ContainsKey(requestedDataDescription.Key))
            {
                this._requestedPages.Remove(requestedDataDescription.Key);
            }
        }

        private void ResetCacheAndDataRequests()
        {
            this._requestedItems.Clear();
            this._requestedPages.Clear();
            this._accessedItems.Clear();
            this.ClearCachce();
            if (!this.IsPagingMode)
            {
                this.MoveToPage(-1);
            }
            else
            {
                this._lastRequestedPage = -1;
                if (this._pageIndex > this.GetPageCount() - 1)
                {
                    this.MoveToLastPage();
                    return;
                }
                if (this._pageIndex < 0)
                {
                    this.MoveToFirstPage();
                    return;
                }
            }
        }

        /// <summary>
        /// Sets the item at the specified index.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="newItem">The new item.</param>
        /// <remarks>
        /// The current implementation throws an InvalidOperationException exception.
        /// The application must use IEditableCollectionView to modify the VirtualCollection.
        /// </remarks>
        protected virtual void SetItem(int index, T newItem)
        {
            throw new InvalidOperationException("E_IListEdit");   // SR.GetString(   - [12/15/2016 A1]
        }

        private void SortDescriptions_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.RefreshView();
        }

        private static bool SortDescriptionsContains(IEnumerable<SortDescription> sortDescriptionCollection, string propertyName)
        {
            return sortDescriptionCollection.Count<SortDescription>((SortDescription item) => string.Equals(item.PropertyName, propertyName, StringComparison.InvariantCultureIgnoreCase)) > 0;
        }

        internal void StopDeferItemRequests()
        {
            VirtualCollection<T> virtualCollection = this;
            virtualCollection._deferredItemRequestsCount = virtualCollection._deferredItemRequestsCount - 1;
            if (this._deferredItemRequestsCount == 0)
            {
                this.RequestAccessedItems();
            }
        }

        internal void StopDeferRefresh()
        {
            VirtualCollection<T> virtualCollection = this;
            virtualCollection._deferredRefreshCount = virtualCollection._deferredRefreshCount - 1;
            if (this._refreshIsWaiting && this._deferredRefreshCount < 1)
            {
                this.ReloadCurrentView();
            }
        }

        void   ICollection<T>.Add(T item)
        {
            this.Insert(this.TotalItemCount, item);
        }

        void   ICollection<T>.Clear()
        {
            this.Clear();
        }

        bool   ICollection<T>.Contains(T item)
        {
            return this.Contains(item);
        }

        void   ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            this.CopyTo(array, arrayIndex);
        }

        bool   ICollection<T>.Remove(T item)
        {
            throw new InvalidOperationException("E_IListEdit");     // SR.GetString(  -  [12/15/2016 A1]
        }

        IEnumerator<T>   IEnumerable<T>.GetEnumerator()
        {
            return (IEnumerator<T>)this.GetEnumerator();
        }

        int   IList<T>.IndexOf(T item)
        {
            return this.IndexOf(item);
        }

        void   IList<T>.Insert(int index, T item)
        {
            this.Insert(index, item);
        }

        void   IList<T>.RemoveAt(int index)
        {
            this.RemoveItemAt(index);
        }

        void  ICollection.CopyTo(Array array, int index)
        {
            this.CopyTo(array as T[], index);
        }

        int  IList.Add(object value)
        {
            return this.Add((T)value);
        }

        void  IList.Clear()
        {
            this.Clear();
        }

        bool  IList.Contains(object value)
        {
            return this.Contains(value);
        }

        int  IList.IndexOf(object value)
        {
            return this.IndexOf(value);
        }

        void  IList.Insert(int index, object value)
        {
            this.Insert(index, (T)value);
        }

        void  IList.Remove(object value)
        {
            this.RemoveItemAt(this.IndexOf(value));
        }

        void  IList.RemoveAt(int index)
        {
            this.RemoveItemAt(index);
        }

        private bool TryLoadPageFromInternalCache(int pageIndex)
        {
            int num = pageIndex * this.PageSize;
            int pageSize = this.GetPageSize(pageIndex);
            if (pageSize == 0)
            {
                return true;
            }
            if (this._itemsCache.TrySetCurrent(num, pageSize))
            {
                int count = this.Count;
                if (this._itemsCache.Current.CopyItemsTo(this._pageCache, num, pageSize))
                {
                    if (count != this.Count)
                    {
                        this.OnPropertyChanged("Count");
                    }
                    return true;
                }
            }
            return false;
        }

        private bool UpdateCurrentPageFromInternalCache(int index, int size)
        {
            if (this.IsPageChanging || this.PageIndex < 0)
            {
                return false;
            }
            int pageIndex = this.PageIndex * this.PageSize;
            int pageSize = this.GetPageSize(this.PageIndex);
            int num = pageIndex + pageSize - 1;
            if (pageSize == 0 || index > num || pageIndex > index + size - 1)
            {
                return false;
            }
            int num1 = 0;
            while (num1 < pageSize)
            {
                T item = this._itemsCache.GetItem(pageIndex);
                if (num1 >= this._pageCache.Count)
                {
                    this._pageCache.Add(item);
                }
                else if (!object.Equals(this._pageCache[num1], item))
                {
                    this._pageCache[num1] = item;
                }
                num1++;
                pageIndex++;
            }
            return true;
        }

        /// <summary>
        /// Occurs when the <see cref="M:Collections.VirtualCollection`1.AddNew" /> method is called.
        /// </summary>
        /// <remarks>
        /// The application is responsible to add the new item to the actual data source when the <see cref="E:Collections.VirtualCollection`1.CommitNewItem" /> event is raised.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.AddNew" />
        /// <seealso cref="T:Collections.AddNewItemEventArgs`1" />
        /// <seealso cref="E:Collections.VirtualCollection`1.CommitNewItem" />
        public event EventHandler<AddNewItemEventArgs<T>> AddNewItem;

        /// <summary>
        /// Occurs when the items list of the collection has changed, or the collection is reset.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Occurs when the <see cref="M:Collections.VirtualCollection`1.CommitEdit" /> method is called.
        /// </summary>
        /// <remarks>
        /// The application is responsible to send the changes to the actual data source.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.CommitEdit" />
        public event EventHandler<CommitEditItemEventArgs<T>> CommitEditItem;

        /// <summary>
        /// Occurs when the <see cref="M:Collections.VirtualCollection`1.CommitNew" /> method is called.
        /// </summary>
        /// <remarks>
        /// The application is responsible to add the new item to the actual data source.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.CommitNew" />
        /// <seealso cref="T:Collections.CommitNewItemEventArgs`1" />
        public event EventHandler<CommitNewItemEventArgs<T>> CommitNewItem;

        /// <summary>
        /// Occurs after the current item has been changed.
        /// </summary>
        public event EventHandler CurrentChanged;

        /// <summary>
        /// Occurs before the current item changes.
        /// </summary>
        public event CurrentChangingEventHandler CurrentChanging;

        /// <summary>
        /// Occurs when the collection encounters an empty item at specified index.
        /// </summary>
        /// <remarks>
        /// This event indicates that the <see cref="T:Collections.VirtualCollection`1" /> needs a range of items that does not exists in the cache.
        /// The application must use <see cref="M:Collections.VirtualCollection`1.LoadItems(System.Int32,  IEnumerable{`0})" /> method to put the actual data when it is ready to use.
        /// The data must be sorted and filtered according specified filter conditions and sort descriptions.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.LoadItems(System.Int32,  IEnumerable{`0})" />
        /// <seealso cref="P:Collections.VirtualCollection`1.LoadSize" />
        /// <seealso cref="T:Collections.ItemDataRequestedEventArgs" />
        public event EventHandler<ItemDataRequestedEventArgs> ItemDataRequested;

        /// <summary>
        /// Occurs after the <see cref="P:Collections.VirtualCollection`1.PageIndex" /> has changed.
        /// </summary>
        public event EventHandler<EventArgs> PageChanged;

        /// <summary>
        /// Occurs before changing the <see cref="P:System.ComponentModel.IPagedCollectionView.PageIndex" />. The event handler can cancel this event.
        /// </summary>
        public event EventHandler<PageChangingEventArgs> PageChanging;

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs when the <see cref="M:Collections.VirtualCollection`1.Remove(System.Object)" /> method is called.
        /// </summary>
        /// <remarks>
        /// The application is responsible to remove the specified item from the actual data source.
        /// </remarks>
        /// <seealso cref="M:Collections.VirtualCollection`1.Remove(System.Object)" />
        /// <seealso cref="M:Collections.VirtualCollection`1.RemoveAt(System.Int32)" />
        /// <seealso cref="T:Collections.RemoveItemEventArgs`1" />
        public event EventHandler<RemoveItemEventArgs<T>> RemoveItem;
    }
}