﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Collections.Virtual.DeferredItem
{
    /// <summary>
    /// An object that VirtualCollection DeferRefresh method returns.
    /// </summary>
    /// <typeparam name="T">The type of elements in the collection</typeparam>
    internal class DeferredRefresh<T> : IDisposable
    {
        private VirtualCollection<T> _virtualCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.DeferredRefresh`1" /> class.
        /// </summary>
        /// <param name="source">The VirtualCollection that creates this instance.</param>
        public DeferredRefresh(VirtualCollection<T> source)
        {
            this._virtualCollection = source;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            this._virtualCollection.StopDeferRefresh();
            this._virtualCollection = null;
            GC.SuppressFinalize(this);
        }
    }
}
