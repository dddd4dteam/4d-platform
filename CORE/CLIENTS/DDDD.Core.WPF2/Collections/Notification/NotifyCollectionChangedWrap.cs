﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Linq;
using System.Linq.Expressions;
using System.Diagnostics;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Collections;
using System.Collections.Specialized;

using System.Runtime.CompilerServices;
using DDDD.Core.Events;

namespace DDDD.Core.Collections.Notification
{






	//public delegate void NotifyCollectionChangedEventHandler(object sender, NotifyCollectionChangedEventArgs e);



	/// <summary>
	/// Base class that raises the property changed notification
	/// </summary>
	public class NotifyCollectionChangedWrap  :INotifyCollectionChanged //, INotifyPropertyChanged
	{
	
		#region  ------------------------------- FIELDS ----------------------------------------

		private static readonly Dictionary<string, PropertyChangedEventArgs> _eventArgCache = new Dictionary<string, PropertyChangedEventArgs>();
		private static readonly object _lockObject = new object();

		//private readonly WeakReference _weakReference;
		//private List<IWeakEventListener> _weakPropertyChangedListeners;
		//private ConditionalWeakTable<object, IWeakEventListener> _weakPropertyChangedListenersTable;

		private List<IWeakEventListener> _weakCollectionChangedListeners;

		private ConditionalWeakTable<object, IWeakEventListener> _weakCollectionChangedListenersTable;

	
		private ConditionalWeakTable<object, List<WeakReference>> _collectionItems;





		#endregion ------------------------------- FIELDS ----------------------------------------


		/// <summary>
		/// Occurs when the <see cref="PropertyChanged"/> event occurs in the collection when the target object is a collection.
		/// </summary>
		public event PropertyChangedEventHandler CollectionItemPropertyChanged;

		/// <summary>
		/// Occurs when the <see cref="CollectionChanged"/> event occurs on the target object.
		/// </summary>
		public event NotifyCollectionChangedEventHandler CollectionChanged;






		private void UnsubscribeNotifyChangedEvent(object value,   ICollection parentCollection)
		{
            if (value == null) return;
			 

			lock (_lockObject)
			{
				ConditionalWeakTable<object, IWeakEventListener> eventsTable;
				List<IWeakEventListener> eventsList;
				 
				eventsTable = _weakCollectionChangedListenersTable;
				eventsList = _weakCollectionChangedListeners;


				IWeakEventListener oldSubscription;
				if (eventsTable != null && eventsTable.TryGetValue(value, out oldSubscription))
				{
					if (oldSubscription != null)
					{
						oldSubscription. Detach();

						eventsList.Remove(oldSubscription);
					}

					eventsTable.Remove(value);
				}

				if (value is ICollection)
				{
					List<WeakReference> collectionItems;
					if (_collectionItems.TryGetValue(value, out collectionItems))
					{
						foreach (var item in collectionItems)
						{
							if (item.IsAlive)
							{
								var actualItem = item.Target;
								UnsubscribeNotifyChangedEvents(actualItem, parentCollection);
							}
						}

						collectionItems.Clear();
						_collectionItems.Remove(value);
					}
				}
			}
		}


		/// <summary>
		/// Unsubscribes from all events.
		/// </summary>
		public void UnsubscribeFromAllEvents()
		{
			lock (_lockObject)
			{  

				if (_weakCollectionChangedListeners != null)
				{
					foreach (var weakListener in _weakCollectionChangedListeners)
					{
						weakListener.Detach();
					}

					_weakCollectionChangedListeners.Clear();
				}
			}
		}

		/// <summary>
		/// Updates all the collection subscriptions.
		/// <para />
		/// This method is internally used when a notifiable collection raises the <see cref="NotifyCollectionChangedAction.Reset"/> event.
		/// </summary>
		public void UpdateCollectionSubscriptions(ICollection collection)
		{
            if (collection == null) return;
			 

			lock (_lockObject)
			{
				List<WeakReference> collectionItems;
				if (_collectionItems.TryGetValue(collection, out collectionItems))
				{
					var oldItems = collectionItems.ToArray();
					foreach (var item in oldItems)
					{
						if (item.IsAlive)
						{
							var actualItem = item.Target;
							UnsubscribeNotifyChangedEvents(actualItem, collection);
						}
					}

					collectionItems.Clear();

					var newItems = collection.Cast<object>().ToArray();
					foreach (var item in newItems)
					{
						SubscribeNotifyChangedEvents(item, collection);
					}
				}
			}
		}


		/// <summary>
		/// Subscribes to the notify changed events.
		/// </summary>
		/// <param name="value">The object to subscribe to.</param>
		/// <param name="parentCollection">If not <c>null</c>, this is a collection item which should use <see cref="OnObjectCollectionItemPropertyChanged"/>.</param>
		public void SubscribeNotifyChangedEvents(object value, ICollection parentCollection)
		{
            if (value == null) return;
			 

			lock (_lockObject)
			{
				var collectionChangedValue = value as INotifyCollectionChanged;
				if (collectionChangedValue != null)
				{
					SubscribeNotifyChangedEvent(collectionChangedValue,   parentCollection);

					var collection = value as ICollection;
					if (collection != null)
					{
						foreach (var child in collection)
						{
							SubscribeNotifyChangedEvents(child, collection);
						}
					}
				}

				var propertyChangedValue = value as INotifyPropertyChanged;
				if (propertyChangedValue != null)
				{
					// ObservableObject implements PropertyChanged as protected, make sure we accept that in non-.NET languages such
					// as Silverlight, Windows Phone and WinRT
					try
					{
						SubscribeNotifyChangedEvent(propertyChangedValue,   parentCollection);
					}
					catch (Exception ex)
					{
						//Log.Warning(ex, "Failed to subscribe to PropertyChanged event, the event is probably not public");
					}
				}
			}
		}



		private void SubscribeNotifyChangedEvent(object value,   ICollection parentCollection)
		{
            if (value == null) return;


            lock (_lockObject)
			{
				ConditionalWeakTable<object, IWeakEventListener> eventsTable;
				List<IWeakEventListener> eventsList;
				 
				   
				if (_weakCollectionChangedListenersTable == null)
				{
					_weakCollectionChangedListenersTable = new ConditionalWeakTable<object, IWeakEventListener>();
				}

				if (_weakCollectionChangedListeners == null)
				{
					_weakCollectionChangedListeners = new List<IWeakEventListener>();
				}

				if (_collectionItems == null)
				{
					_collectionItems = new ConditionalWeakTable<object, List<WeakReference>>();
				}

				eventsTable = _weakCollectionChangedListenersTable;
				eventsList = _weakCollectionChangedListeners;
					 
			   

				IWeakEventListener oldSubscription;
				if (eventsTable != null && eventsTable.TryGetValue(value, out oldSubscription))
				{
					oldSubscription.Detach();

					eventsList.Remove(oldSubscription);
					eventsTable.Remove(value);
				}

				IWeakEventListener weakListener;
				
				  
				//weakListener = this.SubscribeToWeakCollectionChangedEvent(value, OnObjectCollectionChanged, false);
				//if (weakListener == null)
				//{
				//	//Log.Debug("Failed to use weak events to subscribe to 'value.CollectionChanged', going to subscribe without weak events");

				//	((INotifyCollectionChanged)value).CollectionChanged += OnObjectCollectionChanged;
				//}
					   

				 
			}

				//if (eventsTable != null)
				//{
				//	eventsTable.Add(value, weakListener);
				//}

				//if (eventsList != null)
				//{
				//	eventsList.Add(weakListener);
				//}
		 }
		 




		/// <summary>
		/// Unsubscribes from the notify changed events.
		/// </summary>
		/// <param name="value">The object to unsubscribe from.</param>
		/// <param name="parentCollection">The parent collection.</param>
		/// <remarks>No need to check for weak events, they are unsubscribed automatically.</remarks>
		public void UnsubscribeNotifyChangedEvents(object value, ICollection parentCollection)
		{
			if (value == null)  return; 

			lock (_lockObject)
			{ 
				var collectionChangedValue = value as INotifyCollectionChanged;
				if (collectionChangedValue != null)
				{
					UnsubscribeNotifyChangedEvent(collectionChangedValue,  parentCollection);

					foreach (var child in (IEnumerable)value)
					{
						UnsubscribeNotifyChangedEvents(child, parentCollection);
					}
				}
			}
		}




		/// <summary>
		/// Called when the target object raises the <see cref="INotifyCollectionChanged.CollectionChanged"/> event.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="NotifyCollectionChangedEventArgs"/> instance containing the event data.</param>
		/// <remarks>
		/// This method is public to allow the usage of the <see cref="WeakEventListener"/>, do not call this method yourself.
		/// </remarks>
		public void OnObjectCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			var collection = sender as ICollection;

			lock (_lockObject)
			{
				if (e.OldItems != null)
				{
					foreach (var item in e.OldItems)
					{
						UnsubscribeNotifyChangedEvents(item, collection);
					}
				}

				// Reset requires our own logic
				if (e.Action == NotifyCollectionChangedAction.Reset)
				{
					if (collection != null)
					{
						UpdateCollectionSubscriptions(collection);
					}
					else
					{
						//Log.Warning("Received NotifyCollectionChangedAction.Reset for '{0}', but the type does implement ICollection", sender.GetType().GetSafeFullName());
					}
				}

				if (e.NewItems != null)
				{
					foreach (var item in e.NewItems)
					{
						SubscribeNotifyChangedEvents(item, collection);
					}
				}
			}

			CollectionChanged.Invoke(sender, e); //SafeInvoke
		}

	}


}
