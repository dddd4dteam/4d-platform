﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Collections 
{

    public sealed class Link
    {       
        public int? Parent { get; set; }
        public int? Index { get; set; }
        public int Depth { get; set; }
               
        
        /// <summary>
        /// Indexed Child IDs -  
        /// </summary>
        public List<int> Childs
        { get; } = new List<int>();

        //Up  child with ID     - UpChild
        //Down  child with ID   - DownChild
        

    }


    public sealed class TreeNode<T>
    {
        public TreeNode() 
        { }
        public TreeNode(int id) //this ctor can replace dafault ID 
        { Id = id; }
        public int Id { get; private set; } 
            = Guid.NewGuid().GetHashCode();
        public T Data { get; set; }
        public  Link Link { get; set; }

        // TO DO
        // ----  EQUALS [==] and [!=] operators ------
        // TO DO
    }





    public class Tree2<T> : IEnumerable<TreeNode<T>> //Dictionary<long, TreeNode<T>>
    {

        #region -------------- CTOR ----------
        protected Tree2()
        {   }
         
        #endregion -------------- CTOR ----------

        readonly Dictionary<int, TreeNode<T>> nodes = new Dictionary<int, TreeNode<T>>();


        /// <summary>
        /// Root ID Root Node ID. Depth = 0.Default value is null
        /// </summary>
        public int? Root
        { get; private set; }

        public TreeNode<T> this[int key]
        {
            get { return nodes[key]; }
            set { nodes[key] = value; }
        }



        /// <summary>
        /// Add or Update Node: create/add  or update  Root or any other Node.         /// 
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <param name="data"></param>
        public void AddOrUpdateNode(int id, T data, int? parentId = null)
        {
            //1 argerror - id cant be  0 
            //2 argerror - parentId not exist in data

            //parentId can be null - root craete/update case

            //1 - id exist - update Node by ID            
            //2 - root not exist - create root ( parentId should bt null)
            //    root DO NOT CONTAINS LINK object
            //3 - root exist - update root
            //4 - root exist - add node  to root
            //5 - root exist - create/add  Node with ID to some parentId Node( not Root Node)

            //1 argerror - id cant be  0 
            if (id == 0)
                throw new ArgumentException($"Id {id} can't be 0-zero");

            //2 argerror - parentId not exist in data
            if (parentId != null && !ContainsKey(parentId.Value))
                throw new ArgumentException($"parentId {parentId} not found in tree.");

            //1 - id exist - update Node by ID
            if (ContainsKey(id))
            {
                this[id].Data = data;
            }
            //2 - root not exist - create root ( parentId should bt null)
            if (Root == null && parentId == null)
            {   // create root value-             
                Root = id;
                var newNode = new TreeNode<T>()
                { Data = data };
                nodes.Add(id, newNode);
            }
            //3 - root exist - update root
            else if (Root.HasValue && parentId == null)
            {   // update root               
                nodes[Root.Value].Data = data;
            }
            //4 - root exist - add node to root
            else if (Root.HasValue && parentId.HasValue && Root == parentId)
            {
                var thisnodeLink = new Link
                {
                    Parent = Root,
                    Depth = 1
                };
                var newNode = new TreeNode<T>()
                {
                    Link = thisnodeLink
                    , Data = data };

                nodes.Add(id, newNode);

            }

            //5 - root exist - create/add  Node with ID to some parentId Node( not Root Node)
            else if (parentId.HasValue && Root != parentId
               && ContainsKey(id) == false && ContainsKey(parentId.Value))
            {
                var parentNode = this[parentId.Value];
                var thisnodeLink = new Link
                {
                    Parent = parentNode.Id,
                    Depth = parentNode.Link.Depth + 1
                };
                var newNode = new TreeNode<T>()
                {
                    Link = thisnodeLink,
                    Data = data
                };

                nodes.Add(id, newNode);
            }

        }

        /// <summary>
        /// Get Node 
        /// </summary>
        /// <param name="nodeContentSelector"></param>
        /// <returns></returns>
        public TreeNode<T> GetNode(Func<TreeNode<T>, bool> nodeContentSelector)
        {
            return nodes.Values.FirstOrDefault(nodeContentSelector);
        }

        /// <summary>
        /// Get Node
        /// </summary>
        /// <param name="nodeID"></param>
        /// <returns></returns>
        public TreeNode<T> GetNode(int nodeID)
        {
            return nodes[nodeID];            
        }



        #region --------------   IEnumerable<TreeNode<T>>---------------

    /// <summary>
    /// Tree Nodes count
    /// </summary>
    public int Count => nodes.Count;
        
        /// <summary>
        /// Clear all nodes from Tree 
        /// </summary>
        public void Clear()
        {            
            nodes.Clear();
            Root = null;
        }

        /// <summary>
        /// Check  node contains in tree by Node object.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool Contains(TreeNode<T> item)
        {
            return nodes.ContainsKey(item.Id);            
        }

        /// <summary>
        /// Check  node contains in tree by nodeID.
        /// </summary>
        /// <param name="itemKey"></param>
        /// <returns></returns>
        public bool ContainsKey(int itemKey)
        {
            return nodes.ContainsKey(itemKey);
        }

        public bool NotContainsNode(int id)
        {
            return ContainsKey(id) == false;            
        }


        /// <summary>
        /// Remove Node by Node object
        /// </summary>
        /// <param name="item"></param>
        public void Remove(TreeNode<T> item)
        {
            nodes.Remove(item.Id);
        }

        /// <summary>
        /// Remove Node by NodeID
        /// </summary>
        /// <param name="itemKey"></param>
        public void RemoveByKey(int itemKey)
        {
            nodes.Remove(itemKey);
        }



        /// <summary>
        /// Get tree Nodes enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<TreeNode<T>> GetEnumerator()
        {
          return  nodes.Values.GetEnumerator();
        }

        /// <summary>
        /// Get tree Nodes enumerator
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return nodes.GetEnumerator();
        }

        /// <summary>
        /// Get tree Nodes enumerator
        /// </summary>
        /// <returns></returns>
        public TreeNode<T>  GetElementAt(int indexOrder)
        {
            return nodes.ElementAt(indexOrder).Value;
        }

        #endregion --------------   IEnumerable<TreeNode<T>>---------------



        #region ----------- Traversal Methods ----------------
        public List<TreeNode<T>> GetAllSiblings(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public List<TreeNode<T>> GetAllSiblingsAndSelf(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }
        public List<TreeNode<T>> GetAncestors(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }
        public List<TreeNode<T>> GetAncestorsAndSelf(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public List<TreeNode<T>> GetDescendents(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public List<TreeNode<T>> GetDescendentsAndSelf(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public TreeNode<T> GetFirstLeftSibling(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public TreeNode<T> GetFirstRightSibling(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public List<TreeNode<T>> GetNodesOnLevel(int deepLevel)
        {
            throw new NotImplementedException();
        }


        public List<TreeNode<T>> GetSiblingsOnLeft(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }

        public List<TreeNode<T>> GetSiblingsOnRight(TreeNode<T> fromNode)
        {
            throw new NotImplementedException();
        }
        
        public List<TreeNode<T>> SearchFor(Func<TreeNode<T>, bool> nodeContentSelector)
        {
            throw new NotImplementedException();
        }
        
        #endregion  ----------- Traversal Methods ----------------


    }
}
