﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Concurrent;

namespace DDDD.Core.Collections.Concurrent
{



    public interface IKVGrowOnly<T, V>
    {
        int Count { get; }       
       
        IEnumerator<KeyValuePair<T, V>> GetEnumerator();
        void Add(T key, V value);
        void AddOrChange(T key, V value);
        T[] Keys();

        bool ContainsKey(T key);
        bool ContainsValue(V value);

        V GetValue(T key);
        V GetValue(Func<V, bool> selector);

        bool TryGetValue(T key, out V val);
        bool TryGetValue(Func<V, bool> selector, out V value);
        // safesortedlist only
        //V GetValue(int index);
        //T GetKey(int index);
    }

    /// <summary>
    /// Safe Grow Only Dictionary based on ConcurrentDictionary. Sync locking used onnly on Add/SetValue/Keys operations.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class SafeDictionary2<TKey, TValue> : IKVGrowOnly<TKey, TValue>
    {
        #region ------------------------- CTOR -----------------------

        static int numProcs = 4;// Environment.ProcessorCount;
       static int concurrencyLevel = numProcs * 2;
        public SafeDictionary2(int capacity)
        {
            //ConcurrencyLevel , capacity
            _Dictionary = new ConcurrentDictionary<TKey, TValue>();
        }

        public SafeDictionary2()
        {
            _Dictionary = new ConcurrentDictionary<TKey, TValue>();
        }

        #endregion ------------------------- CTOR -----------------------


        private readonly object syncRoot = new object();
        private readonly ConcurrentDictionary<TKey, TValue> _Dictionary;

        /// <summary>
        /// Key|Value indexator
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get
            {
                //lock (syncRoot)
                    return _Dictionary[key];
            }
            set
            {
                //lock (syncRoot)
                    _Dictionary[key] = value;
            }
        }

        /// <summary>
        /// Count Dictionary Items
        /// </summary>
        /// <returns></returns>
        public int Count
        {
            get { return _Dictionary.Count; }
            //lock (syncRoot)                
        }

        /// <summary>
        /// Try get value if it exists
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            //lock (syncRoot)
                return _Dictionary.TryGetValue(key, out value);
        }

     



        /// <summary>
        /// Add or change By Key - Value in Dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
            _Dictionary.TryAdd(key, value);            
        }

        /// <summary>
        ///  Add or change By Key - Value in Dictionary
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddOrChange(TKey key, TValue value)
        {
            _Dictionary.AddOrUpdate(key,value, (k,v) =>  value  );
                        
            //if (_Dictionary.TryAdd(key, value)  == false)
            //    _Dictionary[key] = value;
            
        }


        /// <summary>
        /// Keys 
        /// </summary>
        /// <returns></returns>
        public TKey[] Keys()
        {
            //lock (syncRoot)
            //{
               // TKey[] keys = new TKey[_Dictionary.Keys.Count];
             return    _Dictionary.Keys.ToArray();
              //  return keys;
           // }
        }

      


        /// <summary>
        /// Contains Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            //lock (syncRoot)
                return _Dictionary.ContainsKey(key);
        }

        /// <summary>
        /// Not Contains Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool NotContainsKey(TKey key)
        {
            return (ContainsKey(key) == false);
        }



        /// <summary>
        /// Contains Value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ContainsValue(TValue value)
        {
            //lock (syncRoot)
                return _Dictionary.Values.Contains(value);
        }



        /// <summary>
        /// Get Value by TKey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue GetValue(TKey key)
        {
            //lock (syncRoot)
            return _Dictionary[key];
        }

        /// <summary>
        /// Get Value from VALUES by custom Selector func
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        public TValue GetValue(Func<TValue,bool> selector)
        {
            return _Dictionary.Values.FirstOrDefault(selector);            
        }

        /// <summary>
        /// Get Value from VALUES by custom Selector func
        /// </summary>
        /// <param name="selector"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(Func<TValue, bool> selector, out TValue value)
        {            
            value = _Dictionary.Values.FirstOrDefault(selector);
            return value != null;            
        }



        /// <summary>
        /// Get Enumerator
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return //((ICollection<KeyValuePair<TKey, TValue>>)
                _Dictionary.GetEnumerator();
        }
       

        /// <summary>
        /// Remove item by TKey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        //public bool Remove(TKey key)
        //{
        //    if (key == null)   return true;

        //    lock (_Padlock)
        //    {
        //        return _Dictionary.Remove(key);
        //    }
        //}

        ///// <summary>
        ///// Clear Dictionary
        ///// </summary>
        //public void Clear()
        //{
        //    lock (_Padlock)
        //        _Dictionary.Clear();
        //}


    }



}
