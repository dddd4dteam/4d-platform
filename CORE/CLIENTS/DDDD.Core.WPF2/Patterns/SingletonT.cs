﻿
using DDDD.Core.Threading;
using DDDD.Core.Reflection;

namespace DDDD.Core.Patterns
{
    
    /// <summary>
    /// Singleton Pattern implementation.  
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Singleton<T>  
        where T : class//, new() 
    {

        /// <summary>
        /// Indicator -shpw if singleton  instance was Inited
        /// </summary>
        public bool IsInited
        { get; protected set; }

        /// <summary>
        /// This Initialize() method will be called after T instance will be created - not inside Ctor of T.
        /// </summary>
        protected virtual void Initialize() { }


        /// <summary>
        /// Instance of DCManager of [TManaget] Type - your custom end DCManager Type
        /// </summary>
        protected static LazyAct<T> LA_Current
        { get; } = LazyAct<T>.Create(
            (args) =>
            {
                // ctor()
                var newTargetT = TypeActivator.CreateInstanceT<T>();// new T();
                // Initializing
                (newTargetT as Singleton<T>).Initialize();
                return newTargetT;
            }
            , null, null
            );


        /// <summary>
        /// Thread Safety created T service instance
        /// </summary>
        public static T Current
        {
            get { return LA_Current.Value; }
        }

    }

}
