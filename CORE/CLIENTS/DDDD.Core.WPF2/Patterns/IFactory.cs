﻿using System;
using System.Collections.Generic;

namespace DDDD.Core.Patterns
{

    /// <summary>
    /// Factory interface
    /// </summary>
    public interface IFactory
    {
        /// <summary>
        /// Base Product Type - Product Contract.
        /// </summary>
        Type BaseProductType { get; }


        /// <summary>
        /// Factory Product Types - result Types.  
        /// </summary>
        List<Type> ProductTypes { get; }



        object CreateItem(params KeyValuePair<string, object>[] parameters);

      //  T CreateItemT<T>(params KeyValuePair<string, object>[] parameters);
    }
}