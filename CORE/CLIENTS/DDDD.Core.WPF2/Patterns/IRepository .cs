﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace DDDD.Core.Stores
{


    public interface IRepository
    {

        void Insert<TEntity>(TEntity entity);


        void Delete<TEntity>(TEntity entity);


        TEntity GetById<TEntity>(int id);


        IQueryable<TEntity> GetAll<TEntity>();


        IQueryable<TEntity> SearchFor<TEntity>(Expression<Func<TEntity, bool>> predicate);


    }

}
