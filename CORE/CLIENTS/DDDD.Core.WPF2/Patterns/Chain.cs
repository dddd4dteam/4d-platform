﻿//You can find the source code and binaries at http://VSDesignPatterns.codeplex.com

////Avoid coupling the sender of a request to its receiver by giving more than one object
//a chance to handle the request.
//
//Chain the receiving objects and pass the request along the chain until an object
//handles it.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDD.Core.Patterns
{

    internal class Request
    {
        object _data;

        public int Target
        {
            get { return (int)_data; }
        }
        internal Request(object data)
        {
            _data = data;
        }
    }

    // "Handler" 
    abstract class Handler
    {
       // private Handler successor;

        public Handler Successor
        {
            get;set;
        }
        abstract protected bool CanHandle(Request request);
        internal static void Test()
        {
            // Setup Chain of Responsibility 
            Handler h1 = new ConcreteHandlerA();
            Handler h2 = new ConcreteHandlerB();
            Handler h3 = new ConcreteHandlerC();
            h1.Successor = h2;
            h2.Successor = h3;
            //Generate  requests
            Request[] requests = { new Request(1), new Request(7), new Request(15), new Request(23), new Request(13), new Request(6), new Request(28), new Request(23) };
             //process requests
            foreach (Request request in requests)
            {
                h1.HandleRequest(request);
            }
        }

        public abstract void HandleRequest(Request request);
    }

    // "ConcreteHandlerA" 
    class ConcreteHandlerA : Handler
    {
        override protected bool CanHandle(Request request)
        {
            return request.Target >= 0 && request.Target < 10;
        }
        public override void HandleRequest(Request request)
        {
            if (CanHandle(request))
            {
                Console.WriteLine("{0} handled request {1}", this.GetType().Name, request.Target);

            }
            else if (Successor != null)
            {
                Successor.HandleRequest(request);
            }
        }
    }

    // "ConcreteHandlerB" 
    class ConcreteHandlerB : Handler
    {
        override protected bool CanHandle(Request request)
        {
            return request.Target >= 10 && request.Target < 20;
        }
        public override void HandleRequest(Request request)
        {
            if (CanHandle(request))
            {
                Console.WriteLine("{0} handled request {1}", this.GetType().Name, request.Target);

            }
            else if (Successor != null)
            {
                Successor.HandleRequest(request);
            }
        }
    }

    // "ConcreteHandlerC" 
    class ConcreteHandlerC : Handler
    {
        override protected bool CanHandle(Request request)
        {
            return request.Target >= 20 && request.Target < 30;
        }
        public override void HandleRequest(Request request)
        {
            if (CanHandle(request))
            {
                Console.WriteLine("{0} handled request {1}", this.GetType().Name, request.Target);

            }
            else if (Successor != null)
            {
                Successor.HandleRequest(request);
            }
        }
    }

/*
    class Program
    {
        static void Main()
        {
            Handler.Test();
        }
    }
*/

}
