﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Patterns 
{ 
    
    /// <summary>
    /// Provides an interface for visitors.
    /// </summary>
    /// <typeparam name="T">The type of objects to be visited.</typeparam>
    public interface IVisitor<T>
    {
        /// <summary>
        /// Gets a value indicating whether this instance is done performing it's work..
        /// </summary>
        /// <value><c>true</c> if this instance is done; otherwise, <c>false</c>.</value>
        bool HasCompleted { get; }

        /// <summary>
        /// Visits the specified object.
        /// </summary>
        /// <param name="obj">The object to visit.</param>
        void Visit(T obj);
    }


}
