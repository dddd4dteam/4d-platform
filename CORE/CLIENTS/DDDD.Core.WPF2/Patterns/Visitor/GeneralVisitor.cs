﻿using System;
using DDDD.Core.Diagnostics;


namespace DDDD.Core.Patterns
{

    /// <summary>
    /// A general visitor that executes the specified <see cref="Predicate{T}"/> delegate.
    /// </summary>
    /// <typeparam name="T">The type of item to visit.</typeparam>
    public class GeneralVisitor<T> : IVisitor<T>
    {
        #region Globals

        private bool completed;
        private readonly Predicate<T> predicate;

        #endregion

        #region Construction

        /// <param name="hasCompletedPredicate">The <see cref="Predicate{T}"/> delegate.  The return value is used to indicate whether the visitor has completed.</param>
        public GeneralVisitor(Predicate<T> hasCompletedPredicate)
        {            
            Validator.ATNullReferenceArgDbg(hasCompletedPredicate, nameof(GeneralVisitor<T>), nameof(GeneralVisitor<T>), nameof(hasCompletedPredicate)); //ERR_InvalidOperation
            
            predicate = hasCompletedPredicate;
        }

        #endregion

        #region IVisitor<T> Members

        /// <inheritdoc />
        public bool HasCompleted
        {
            get { return completed; }
            set { completed = value; }
        }

        /// <inheritdoc />
        public void Visit(T obj)
        {
            completed = predicate(obj);
        }

        #endregion
    }

}
