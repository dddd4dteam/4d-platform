﻿ 

namespace DDDD.Core.Stores
{
    ///// <summary>
    ///// Tree Structure Content Storing Node Types
    ///// </summary>
    //public enum NodeTypeEn
    //{
    //    /// <summary>
    //    /// Node is Category.Node Directory - prefix [CG]. Category Node should contains  [category.json] file in the root of it's directory, which contains this Node definition. 
    //    /// </summary>
    //    Category
    //    ,
    //    /// <summary>
    //    /// Node is end Content Item. Node Directory - prefix [CNT]. Content Item Node should contains  [content.json] file in the root of it's directory, which contains this Node definition. 
    //    /// </summary>
    //    Content
    //}

    /// <summary>
    /// File Resource Type - is   json/image/audio/video/ or document
    /// </summary>
    //public enum FileDataTypeEn
    //{
    //    /// <summary>
    //    /// data type was not determined.
    //    /// </summary>
    //    NotDetermined
    //    ,
    //    /// <summary>
    //    /// json data in file 
    //    /// </summary>
    //    json
    //    ,
    //    /// <summary>
    //    /// image data in file
    //    /// </summary>
    //    image
    //    ,
    //    /// <summary>
    //    /// audio  data in file
    //    /// </summary>
    //    audio
    //    ,
    //    /// <summary>
    //    /// video data in file
    //    /// </summary>
    //    video
    //    ,
    //    /// <summary>
    //    /// some document  
    //    /// </summary>
    //    document
    //}



}
