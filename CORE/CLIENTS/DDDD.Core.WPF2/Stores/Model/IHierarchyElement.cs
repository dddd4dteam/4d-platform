﻿using System;

namespace DDDD.Core.Stores.Model
{
    public interface IHierarchyElement : IDocumentEntity
    {        

       Guid? IDParent { get; set; }

    }
}