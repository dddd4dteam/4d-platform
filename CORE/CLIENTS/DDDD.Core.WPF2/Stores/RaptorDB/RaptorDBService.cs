﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using DDDD.Core.Extensions;
using DDDD.Core.Stores.Model;
using DDDD.Core.Patterns;
using DDDD.Core.Reflection;
using DDDD.Core.Stores.Files;

using Raptor =RaptorDB;


namespace DDDD.Core.Stores.RaptorDB
{

    /// <summary>
    /// 
    /// </summary>
    public class RaptorDBService : Singleton<RaptorDBService>, IFilesMetaStore,  IDisposable
    {

        #region --------------------- CTOR ------------------------
        /// <summary>
        /// We always should Init() Current - service instance, before to do something
        /// </summary>
        RaptorDBService() { } // nobody can create/get service instance  overvise than from Current property
                              // 
                              //public RaptorDBService(string hostUser)
                              //{
                              //    Init( hostUser);
                              //}

        //public RaptorDBService(string appRootDirectoryExplicitly,  string hostUser)
        //{
        //    Init(appRootDirectoryExplicitly, hostUser);
        //}

        #endregion --------------------- CTOR ------------------------

        const string winDelim = @"\";
        const string raptordata = nameof(raptordata);
        const string content = nameof(content);

        #region ----------------------- SINGLETON SERVICE INSTANCE ----------------------------

        /// <summary>
        /// Service singleton instance
        /// </summary>
        public static RaptorDBService Current
        { get { return LA_Current.Value; } }

        #endregion ----------------------- SINGLETON SERVICE INSTANCE ----------------------------


        #region ----------------------- INIT ------------------------

        /// App Startup Paths:
        /// [NodePC]/[appRootDir]/raptordata - metadata in raptorDB store
        /// [NodePC]/[appRootDir]/content/[hostUser]


        
        public void Init(string appRootDirectoryExplicitly, string hostUser)
        {
            AppRootDirectory = appRootDirectoryExplicitly;

            NodePC = System.Environment.MachineName; // NetBIOS
            HostUser = hostUser;          

            StorePath = Path.Combine(AppRootDirectory, raptordata);
            if (!Directory.Exists(StorePath))
                Directory.CreateDirectory(StorePath);

            FilesContentPath = Path.Combine(AppRootDirectory, content, HostUser);
            if (!Directory.Exists(FilesContentPath))
                Directory.CreateDirectory(FilesContentPath);

            //now Init RaptorDB instance and default Model - CategoryElement, ContentElement 
            Store = Raptor.RaptorDB.Open(StorePath);
            Raptor.Global.RequirePrimaryView = false;

            
            RegisterRepositories<ContentElement, MenuItemView, ContentElementFilesView>();

            
        }


        public void Init(string hostUser)
        {
            Init(GetAppRootData(), hostUser);
        }

        private string GetAppRootData()
        {
            string execAssemblyDir = Assembly.GetExecutingAssembly().Location;
            var lastDirectoryName = Path.GetDirectoryName(execAssemblyDir).ToLower();
            //if lastname is bin -then go Upper
            if (lastDirectoryName == "bin")
            {
                var targetAppRoot = execAssemblyDir.Replace(@"\bin\", "");
                targetAppRoot = execAssemblyDir.Replace(@"\bin", @"\");
                return targetAppRoot;
            }
            return execAssemblyDir;
        }

       
        #endregion ----------------------- INIT ------------------------


        #region ------------------------ IDisposable -----------------------------

        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Store.Shutdown();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion ------------------------ IDisposable -----------------------------


        #region -------------------------- PROPERTIES ---------------------------

        /// <summary>
        ///   Host User  who seems like administrator of  this store db. 
        /// </summary>
        public static string HostUser
        { get; set; }

        /// <summary>
        /// application startup directory path
        /// </summary>
        public static string AppRootDirectory { get; private set; }

        /// <summary>
        /// NetBIOS Machine name 
        /// </summary>
        public string NodePC
        { get; private set; }


        /// <summary>
        /// Opened RaptorDB data directory path
        /// </summary>
        public string StorePath
        { get; private set; }

        /// <summary>
        /// Directory where Nodes with files will be stored.
        /// </summary>
        public string FilesContentPath
        { get; private set; }
         

        /// <summary>
        /// RaptorDB - Entities Store
        /// </summary>
        public Raptor.RaptorDB Store
        { get; private set; }


        Dictionary<int, RaptorDBRepository> AllRepositories
        { get; } = new Dictionary<int, RaptorDBRepository>();


        /// <summary>
        /// Host Elements Store. 
        /// </summary>
        public string HostElementsStore
        {
            get {   return "RaptorDB";  }
        }



        #endregion -------------------------- PROPERTIES ---------------------------



        #region ----------------------------- REGISTER REPOSITORIES ----------------------------------


        /// <summary>
        /// Register new  RaptorDBRepository for  TEntityView.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        public void RegisterRepository<TEntity, TEntityView>()
            where TEntityView : Raptor.View<TEntity>
        {
            var key = typeof(TEntityView).GetHashCode();
            if (AllRepositories.NotContainsKey(key))
            {
                try
                {
                    var viewinstance = TypeActivator.CreateInstanceT<TEntityView>();
                    Store.RegisterView<TEntity>(viewinstance);
                    AllRepositories.Add(key, new RaptorDBRepository(typeof(TEntityView)));
                }
                catch (Exception regRaptViewEx)
                {
                    throw new InvalidOperationException(" RegiserRepository() Error when registering View[{0}]".Fmt(typeof(TEntityView).FullName));
                }
            }
        }



        /// <summary>
        /// Register new  RaptorDBRepository for TEntityView, TEntityView2.
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TEntityView"></typeparam>
        /// <typeparam name="TEntityView2"></typeparam>
        public void RegisterRepositories<TEntity, TEntityView, TEntityView2>()
            where TEntityView : Raptor.View<TEntity>
            where TEntityView2 : Raptor.View<TEntity>
        {
            RegisterRepository<TEntity, TEntityView>();
            RegisterRepository<TEntity, TEntityView2>();

        }


        /// <summary>
        /// Register new  RaptorDBRepository for  TEntityView, TEntityView2, TEntityView3
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TEntityView"></typeparam>
        /// <typeparam name="TEntityView2"></typeparam>
        /// <typeparam name="TEntityView3"></typeparam>
        public void RegisterRepositories<TEntity, TEntityView, TEntityView2, TEntityView3>()
           where TEntityView : Raptor.View<TEntity>
           where TEntityView2 : Raptor.View<TEntity>
           where TEntityView3 : Raptor.View<TEntity>
        {
            RegisterRepository<TEntity, TEntityView>();
            RegisterRepository<TEntity, TEntityView2>();
            RegisterRepository<TEntity, TEntityView3>();
        }


        /// <summary>
        /// Register new  RaptorDBRepository for  TEntityView, TEntityView2, TEntityView3, TEntityView4
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TEntityView"></typeparam>
        /// <typeparam name="TEntityView2"></typeparam>
        /// <typeparam name="TEntityView3"></typeparam>
        /// <typeparam name="TEntityView4"></typeparam>
        public void RegisterRepositories<TEntity, TEntityView, TEntityView2, TEntityView3, TEntityView4>()
           where TEntityView : Raptor.View<TEntity>
           where TEntityView2 : Raptor.View<TEntity>
           where TEntityView3 : Raptor.View<TEntity>
           where TEntityView4 : Raptor.View<TEntity>
        {
            RegisterRepository<TEntity, TEntityView>();
            RegisterRepository<TEntity, TEntityView2>();
            RegisterRepository<TEntity, TEntityView3>();
            RegisterRepository<TEntity, TEntityView4>();
        }
        #endregion ----------------------------- REGISTER REPOSITORIES ----------------------------------






        #region --------------------- CRUD / SEARCHFOR -----------------------

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void Delete<TEntity>(TEntity entity) where TEntity : IDocumentEntity
        {
            var key = typeof(TEntity).GetHashCode();
            AllRepositories[key].Delete(entity);
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public Raptor.Result<object> GetAll<TView>() //where TView //, IDocumentEntity
        {
            var emptyGuid =  "'" + Guid.Empty.S() + "'";
            //return Store.Query<TViewRowSchema>(  "Name != \"\" " );
            return Store.Query(typeof(TView).Name);// "Name != \"\" ");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public Raptor.Result<TViewRowSchema> GetById<TViewRowSchema>(Guid id) where TViewRowSchema : Raptor.RDBSchema, IDocumentEntity
        {
            return Store.Query<TViewRowSchema>((TViewRowSchema s) => s.ID == id); // typeof(TView).Name, "ID == {0}".Fmt(id.S()));  

        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public   void Insert<TEntity>(TEntity entity) where TEntity : IDocumentEntity
        {
            var key = typeof(TEntity).GetHashCode();
            AllRepositories[key].Insert(entity);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Raptor.Result<TViewRowSchema> SearchFor<TViewRowSchema>(Expression<Predicate<TViewRowSchema>> predicate) where TViewRowSchema : Raptor.RDBSchema, IDocumentEntity
        {
            return Store.Query<TViewRowSchema>(predicate);
        }



        #endregion --------------------- CRUD / SEARCHFOR -----------------------


        #region ---------------------- IHostStoreService --------------------------

        public Type ContentElementType
        { get; } = typeof(ContentElement);

        public List<Type> ContentElementViewTypes
        { get; } = new List<Type>() { typeof(ContentElementFilesView) };


        public IContentElement GetElement(Guid elementID)
        {
            var key = ContentElementViewTypes[0].GetHashCode();
            var reslt = Current.AllRepositories[key].SearchFor<ContentElementFilesView.RowSchema>(flv => flv.ID == elementID);
            if (reslt.Count > 1) throw new InvalidDataException("We can't have two items with the same ID ");
            if (reslt.Count == 0) return null;
            if (reslt.Count == 1) return reslt.Rows[0] as IContentElement;
            return null;           
        }



        public IContentElement GetElement(string elementName, Guid? parentID)
        {
            var key = ContentElementViewTypes[0].GetHashCode();
            var reslt = Current.AllRepositories[key].SearchFor<ContentElementFilesView.RowSchema>(flv => flv.Name == elementName && flv.IDParent == parentID);
            if (reslt.Count > 1) throw new InvalidDataException("We can't have two items with the same ID ");
            if (reslt.Count == 0) return null;
            if (reslt.Count == 1) return reslt.Rows[0] as IContentElement;
            return null;
        }


        public bool ContainsElement(string elementName, Guid? parentID)
        {
            var key = ContentElementViewTypes[0].GetHashCode();
            var reslt = Current.AllRepositories[key].SearchFor<ContentElementFilesView.RowSchema>(flv => flv.Name  == elementName && flv.IDParent == parentID);
            if (reslt.Count > 1) throw new InvalidDataException("We can't have two items with the same ID ");
            if (reslt.Count == 0) return false;
            if (reslt.Count == 1) return true;
            return false;
        }



        public Guid AddElement(string name, string description, Guid? parentID, bool isCategory)
        {
            var newElement = TypeActivator.CreateInstanceTBase<IContentElement>(ContentElementType);
            newElement.Name = name;
            newElement.Description = description;
            newElement.IDParent = parentID;
            newElement.IsCategory = isCategory;

            Store.Save(newElement.ID, newElement);

            return newElement.ID;            
        }

        public void UpdateElement(IContentElement elementToUpdate)
        {
            Store.Save(elementToUpdate.ID, elementToUpdate);          
        }

        
        #endregion ---------------------- IHostStoreService --------------------------






    }


}





#region ---------------------------- GARBAGE ------------------------------
 
/// <summary>
/// Host Entities For Category  Nodes. When Category Entity will be created, then CategoryNode-file storing part, also will be created. 
/// </summary>
//public List<Type> CategoryEntities
//{ get; private set; } = new List<Type>() { typeof(CategoryElement)};


///// <summary>
///// Host Entities For Content  Nodes. When host Entity will be created, then ContentNode-file storing part, also will be created. 
///// </summary>
//public List<Type> ContenEntities
//{ get; private set; } = new List<Type>() { typeof(ContentElement)};


//public interface IEntity
//{
//    int ID { get; }
//}

//public class Repository<T> : IRepository<T> where T : class, IEntity
//{
//    protected Table<T> DataTable;

//    public Repository(DataContext dataContext)
//    {
//        DataTable = dataContext.GetTable<T>();
//    }

//    #region IRepository<T> Members

//    public void Insert(T entity)
//    {
//        DataTable.InsertOnSubmit(entity);
//    }

//    public void Delete(T entity)
//    {
//        DataTable.DeleteOnSubmit(entity);
//    }

//    public IQueryable<T> SearchFor(Expression<Func<T, bool>> predicate)
//    {
//        return DataTable.Where(predicate);
//    }

//    public IQueryable<T> GetAll()
//    {
//        return DataTable;
//    }

//    public T GetById(int id)
//    {
//        // Sidenote: the == operator throws NotSupported Exception!
//        // 'The Mapping of Interface Member is not supported'
//        // Use .Equals() instead
//        return DataTable.Single(e => e.ID.Equals(id));
//    }

//    #endregion
//}


#endregion ---------------------------- GARBAGE ------------------------------