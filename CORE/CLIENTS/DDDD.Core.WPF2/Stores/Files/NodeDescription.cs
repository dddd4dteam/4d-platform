﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Stores.Files
{ 

     /// <summary>
     /// Contract that we use to save/import as json data -  Element-Node description.  
     /// </summary>
    public class NodeDescription
    {

        public string Name { get; set; }

        public string Description { get; set; }
    }

    

}
