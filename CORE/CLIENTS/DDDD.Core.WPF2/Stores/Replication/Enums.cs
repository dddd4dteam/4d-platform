﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Stores.Replication
{
    public enum AccessMethodEn
    {
        LocalFileSystem

        , NetworkAccess
        
        , HttpAccess
    }

    public enum PCNodeStateEn
    {
         Normal
        , NotPinged
        , SuccessfullyWorked 
        
    }

}
