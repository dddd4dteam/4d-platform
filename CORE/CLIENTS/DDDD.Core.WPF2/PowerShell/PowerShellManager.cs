﻿using DDDD.Core.Dependencies;
using DDDD.Core.Environment;
using System.IO;

namespace DDDD.Core.PowerShell
{

#if CLIENT && SL5



    /// <summary>
    /// <para/> EXEC Powershell Script - with the help of WScriptShell(Environment2.WScriptShell.Exec)  we rise powershell with command -powershell script, then listen Console- Stdout and getting results from it  
    /// </summary>
    public class PowerShellManager
    {
        static PowerShellManager()
        {
            Initialize();

        }

        private static void Initialize()
        {
            CmdPackageDependencyManager.RegisterDependecies<PowerShellManager>();

        }

        /// <summary>
        /// POWERSHELL temp Folder to write temp output results
        /// </summary>
        internal static string PSTempFolder => Path.Combine(Environment2.ClientAppRootDir, "PSTemp");

        static PSExecutionPolicyEn _ExecutionPolicy = PSExecutionPolicyEn.NotDefined;

        


    }




#elif CLIENT && WPF

    // SOME VARIANT HERE

#endif 



}
