﻿
#if SL5

using System;
using System.ComponentModel;
using System.IO;


using System.Runtime.InteropServices.Automation;
using DDDD.Core.Dependencies;
using DDDD.Core.Extensions;
using DDDD.Core.Environment;

using System.Threading.Tasks;
using System.Runtime.InteropServices;


namespace DDDD.Core.PowerShell
{

    /// <summary>
    /// <para/> EXEC Powershell Script - with the help of WScriptShell(Environment2.WScriptShell.Exec)  we rise powershell with command -powershell script, then listen Console- Stdout and getting results from it 
    /// <para/> RUN Powershell Script - with the help of WScriptShell(Environment2.WScriptShell.Run)  we rise powershell with command -powershell script parameter
    /// <para/> RUN PKG Powershell Script - in association with some cmd package we build our script and then run it by WScriptShell(Environment2.WScriptShell.Run)  - we rise powershell with command -powershell script parameter 
    /// </summary>
    public class PowerShellManager
    {
        static PowerShellManager()
        {
            Initialize();

        }

        private static void Initialize()
        {
            CmdPackageDependencyManager.RegisterDependecies<PowerShellManager>();

        }

        /// <summary>
        /// POWERSHELL temp Folder to write temp output results
        /// </summary>
        internal static string PSTempFolder => Path.Combine(Environment2.ClientAppRootDir, "PSTemp");

        static PSExecutionPolicyEn _ExecutionPolicy = PSExecutionPolicyEn.NotDefined;

        /// <summary>
        /// OS PowerShell Execution Policy
        /// </summary>
        public static PSExecutionPolicyEn ExecutionPolicy
        {
            get
            {
                if (_ExecutionPolicy == PSExecutionPolicyEn.NotDefined)
                {
                    lock (locker)
                    {
                        if (_ExecutionPolicy == PSExecutionPolicyEn.NotDefined)
                        {
                            _ExecutionPolicy = GetPSExecPolicy();
                        }
                    }
                }
                return _ExecutionPolicy;
            }

            private set { _ExecutionPolicy = value; }
        }

        private const string PowerShellExe = "powershell.exe";
        private static readonly object locker = new object();

        internal const string NL = "\r\n";


#region ----------------------------------- [POWERSHELL] PKG DEPENDENT SCRIPTS ------------------------------------


        //private const string Pkg_WinBaseTools = "WinBaseTools";
        //private const string CmdFile_EnableExecutionPolicy = "EnableExecutionPolicy.bat";
        //private const string PSFile_ChangeWindowsLUA = "ChangeWindowsLUA.ps1";

        /// <summary>
        /// Runnig *.bat command file in Package Folder by WScriptShell.Run(script, 0, true)
        /// </summary>
        /// <param name="packageType"></param>
        /// <param name="Package"></param>
        /// <param name="commandFileName"></param>
        /// <param name="outFileName"></param>
        /// <returns></returns>
        public static string RunPkgCommmandFile(PackageTypeEn packageType, String Package, string commandFileName, string outFileName = null)
        {


            string commandFilePath = Path.Combine(Environment2.ClientAppRootDir, packageType.S(), Package, commandFileName);

            string commandOutputFilePath = (outFileName.IsNullOrEmpty()) ? null : Path.Combine(Environment2.ClientAppRootDir, packageType.S(), Package, outFileName);

            string cmdToRun = commandFilePath + ((!commandOutputFilePath.IsNullOrEmpty()) ? " > " + commandOutputFilePath : "");

            string output = "";

            try
            {
                Environment2.WScriptShell.Run(commandFilePath, 0, true); // 0,  true                    

                if (File.Exists(commandOutputFilePath))
                {
                    output = File.ReadAllText(commandOutputFilePath);
                }

                return output;

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("PowerShellManager.RunPkgCommmandFile() : PackageType -[{0}] , Package -[{1}] , CommandFile -[{2}] , OutFile -[{3}].  Exception - [{4}]"
                                                .Fmt(packageType.S(), Package, commandFileName, outFileName, exc.Message));
            }
        }



        /// <summary>
        /// Runnig *.ps1  script file in Package Folder by WScriptShell.Run(script, 0, true)
        /// Returns - [-1] - Script file was not found by such path
        /// </summary>
        /// <param name="packageType"></param>
        /// <param name="Package"></param>
        /// <param name="psscriptFileName"></param>
        /// <param name="outFileName"></param>
        /// <returns></returns>       
        public static string RunPkgPSScriptFile(PackageTypeEn packageType, String Package, string psscriptFileName, string outFileName = null)
        {
            string psscriptFilePath = Path.Combine(Environment2.ClientAppRootDir, packageType.S(), Package, psscriptFileName);

            string psscriptOutputFilePath = (outFileName.IsNullOrEmpty()) ? null : Path.Combine(Environment2.ClientAppRootDir, packageType.S(), Package, outFileName);

            string cmdToRun = PowerShellExe
                              + " -NoLogo" + " -NonInteractive" + " -NoProfile" + " -WindowStyle Hidden "
                              + @psscriptFilePath //full Path
                              + ((!psscriptOutputFilePath.IsNullOrEmpty()) ? " > " + psscriptOutputFilePath : "");

            string output = "";

            try
            {

                if (!File.Exists(psscriptFilePath))
                {
                    throw new InvalidOperationException("PowerShellManager.RunPkgPSScriptFile(): File not found [{0}] ".Fmt(psscriptFilePath));
                }

                Environment2.WScriptShell.Run(cmdToRun, 0, true);
                //Environment2.WScriptShell.Run(psscriptFilePath, 0, true);

                if (File.Exists(psscriptOutputFilePath))
                {
                    output = File.ReadAllText(psscriptOutputFilePath);
                    File.Delete(psscriptOutputFilePath);
                }

                return output;

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("PowerShellManager.RunPkgPSScriptFile() : PackageType -[{0}] , Package -[{1}] , PsscriptFile -[{2}] , OutFile -[{3}].  Exception - [{4}]"
                                                    .Fmt(packageType.S(), Package, psscriptFileName, outFileName, exc.Message));
            }
        }






        /// <summary>
        /// Run PowerShell script and save result in some out file that will be readed after script running completed.
        /// <para/> When file will be readed it'll be deleted, and readed result will be returned        
        /// </summary>
        /// <param name="packageType"></param>
        /// <param name="Package"></param>
        /// <param name="psscriptFileName"></param>
        /// <param name="outFileName"></param>
        /// <returns></returns>
        public static string RunPkgPSScript(PackageTypeEn packageType, String Package, string psscript, string outFileName = null)
        {

            string psscriptOutputFilePath = (outFileName.IsNullOrEmpty()) ? null : Path.Combine(Environment2.ClientAppRootDir, packageType.S(), Package, outFileName);

            string cmdToRun = PowerShellExe
                              + " -NonInteractive " + "-NoProfile" + " -WindowStyle Hidden "
                              + psscript
                              + ((!psscriptOutputFilePath.IsNullOrEmpty()) ? " > " + psscriptOutputFilePath : "");



            try
            {
                string output = "";

                Environment2.WScriptShell.Run(cmdToRun, 0, true);

                if (File.Exists(psscriptOutputFilePath))
                {
                    output = File.ReadAllText(psscriptOutputFilePath);
                    File.Delete(psscriptOutputFilePath);
                }

                return output.Trim();

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("PowerShellManager.RunPkgPSScriptFile() : PackageType -[{0}] , Package -[{1}] ,script -[{2}] , OutFile -[{3}].  Exception - [{4}]"
                                                    .Fmt(packageType.S(), Package, psscript, outFileName, exc.Message));
            }
        }

#endregion ----------------------------------- [POWERSHELL] PKG DEPENDENT SCRIPTS ------------------------------------


#region ---------------------------------- POWERSHELL RUN SCRIPTS ---------------------------------


        /// <summary>
        /// Executing PowerShell  script by WScriptShell.Exec(script). 
        /// <para/> We can get script outputresult if returnResult is true, at that time waitOnRun will be true also.
        /// <para/> If you are going to run scrpit without output result but that still need waiting untill operation'll be finished, then you can use waitOnRun separately.
        /// <para/> We can add  results filtering with help of [fileter] and [filtersymbol], this'll be appended to the end of script line code
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        public static string RunPSScript(string script, bool returnResult, bool waitOnRun = true, string selectProperties = null, PSFormatEn format = PSFormatEn.NotDefined, string formatSymbol = null)
        {
            if (script.IsNullOrEmpty()) return null;

            if (returnResult == true) waitOnRun = true;

            var tempResultOutFile = "";
            if (returnResult == true)
            {
                tempResultOutFile = Path.Combine(PSTempFolder, $"tmprslt_{Guid.NewGuid().ToString().Replace("-", "_").Substring(0, 7)}.rslt");

                if (!selectProperties.IsNullOrEmpty()) // 
                {
                    script += $" | select {selectProperties}";
                }

                if (format == PSFormatEn.FormatList) { script += " | Fl " + formatSymbol; }
                if (format == PSFormatEn.FormatTable) { script += " | Ft " + formatSymbol; }

                //script += @" | Out-File '" + tempResultOutFile + "' unicode"; // -Force
            }

            //powershell -ExecutionPolicy unrestricted -command "& { c:\say-hello.ps1; SayHello }"            
            string finalCmd =
                  //"cmd.exe /c "
                  PowerShellExe
                + " -NonInteractive " + "-NoProfile" + " -WindowStyle Hidden " + "-Command "
                + "\"& { " + script + " }\""
                 + ((tempResultOutFile.IsNullOrEmpty()) ? tempResultOutFile : $" > {tempResultOutFile}")
                ;

            string output = "";

            dynamic result = null;

            try
            {
                if (!Directory.Exists(PSTempFolder))
                {
                    Directory.CreateDirectory(PSTempFolder);
                }

                result = Environment2.WScriptShell.Run(finalCmd, 0, waitOnRun);
                //Thread.Sleep(1000);
                //read result from out file
                if (returnResult == true && File.Exists(tempResultOutFile))
                {
                    output = File.ReadAllText(tempResultOutFile);
                    File.Delete(tempResultOutFile);
                }
            }
            catch (Exception exc)
            {

                //LOG ON DEBUG
                if (exc is FileNotFoundException)
                {
                    throw new FileNotFoundException("PowerShell is not installed on the local computer.", exc);
                }

                throw exc;
            }


            return output;
        }


        public static string ExecPSScript(string script)
        {

            // Check to see if stuff is available
            if (!AutomationFactory.IsAvailable)
                throw new InvalidOperationException("AutomationFactory is unavailable. Program must be run as out-of-browser with elevated permissions.");

            var shell = AutomationFactory.CreateObject("WScript.Shell");
            string cmd = //"cmd.exe /c "
                         //+ 
                PowerShellExe
                + " -NonInteractive" + " -NoProfile" + " -WindowStyle Hidden" + " -Command -";
            //+ "\"& { " + script + " }\"";

            string output = "";

            dynamic result = null;
            try
            {
                result = shell.Exec(cmd);
                result.StdIn.Write(script);
                result.StdIn.Close();

                while (!result.StdOut.AtEndOfStream)
                {
                    output += result.StdOut.ReadLine() + "\r\n";
                }
            }
            catch (FileNotFoundException fnfex)
            {
                throw new FileNotFoundException("PowerShell is not installed on the local computer.", fnfex);
            }

            return output;
        }




#endregion ---------------------------------- POWERSHELL RUN SCRIPTS ---------------------------------



        /// <summary>
        /// Getting  Powershell Version[Major,Minor].
        /// </summary>
        /// <returns></returns>
        public static Version GetCurrentPSVersion()
        {
            // $PSVersionTable.PSVersion | Select Major,Minor | Fl
            var psVersion = new Version(0, 0);

            throw new NotImplementedException("POWERSHELL GETCurrentVERSION now is not implemented");

            //return psVersion;
        }


#region ----------------------------------- [POWERSHELL] PS EXECUTION POLICY ------------------------------------



        public static PSExecutionPolicyEn GetPSExecPolicy()
        {
            //var script = ""; 
            var getPSExecPolicyScript = "Get-ExecutionPolicy";
            // + @" | Out-File '" + PS_CMD_RESULT_OUTFILE + "' unicode";


            var reslt = RunPSScript(getPSExecPolicyScript, returnResult: true);
            //.GetParameterValue<string>()

            if (reslt.IsNullOrEmpty()) return PSExecutionPolicyEn.NotDefined;

            PSExecutionPolicyEn resPolicy = PSExecutionPolicyEn.NotDefined;
            Enum.TryParse(reslt, out resPolicy);
            return resPolicy;

        }


        /// <summary>
        /// Set PS Execution Policy By command to POWERSHELL PROCESS
        /// </summary>
        public static void SetPSExecPolicy(PSExecutionPolicyEn psexecutionPolicy)
        {
            // Execution policy can be changed only from WScriptShell
            // @"Set-ExecutionPolicy -ExecutionPolicy AllSigned -scope CurrentUser -Force; \r\n"

            var setpsExecPolicyScript = "";
            if (ExecutionPolicy == PSExecutionPolicyEn.Restricted && psexecutionPolicy == PSExecutionPolicyEn.Unrestricted)
            {
                setpsExecPolicyScript = @"powershell -WindowStyle Hidden Set-ExecutionPolicy -ExecutionPolicy AllSigned -Scope CurrentUser -Force"
                                       + NL
                                       + @"powershell -WindowStyle Hidden Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope CurrentUser -Force";
            }
            else
            {
                setpsExecPolicyScript = $@"powershell -WindowStyle Hidden Set-ExecutionPolicy -ExecutionPolicy {psexecutionPolicy.S()} -Scope CurrentUser -Force";
            }
            try
            {
                var reslt = Environment2.WScriptShell.Run(setpsExecPolicyScript, 0, true);
            }
            catch (Exception exc)
            {
                //FormatMessage
                throw new InvalidOperationException($"{nameof(PowerShellManager)}.{nameof(SetPSExecPolicy)}():  Windows Error {Marshal.GetLastWin32Error().ToString()}");
            }

            ExecutionPolicy = GetPSExecPolicy();

        }



#endregion ----------------------------------- [POWERSHELL]  EXECUTION POLICY ------------------------------------



#region --------------------------------  MODULES - EXISTS -------------------------------

        /// <summary>
        /// Checking that PowerShell Module has been installed on PowerShell
        /// </summary>
        /// <param name="psModuleName"></param>
        /// <returns></returns>
        public static bool CheckPSModuleExist(string psModuleName)
        {
            var ExistValue = "MODULE_EXISTS";
            var NotExistValue = "MODULE_NOT_EXISTS";

            // if ( Get-Module -ListAvailable -Name SQLPS) {  Write-Host "MODULE_EXISTS" } else {  Write-Host "MODULE_NOT_EXISTS" }
            var checkPSModuleExistsScript = $"if ( Get-Module -ListAvailable -Name {psModuleName}) {{  Write-Host {ExistValue} }} else {{  Write-Host {NotExistValue} }}";
            var returnreslt = RunPSScript(checkPSModuleExistsScript, returnResult: true, waitOnRun: true);

            if (returnreslt == ExistValue) return true;

            return false;
        }




#endregion -------------------------------- INSTALL MODULE -------------------------------







        internal static string BuildTerminateCurrentProcessScript(Int32 ProcID)
        {
            var scriptTerminateProcess =
                "Get-WmiObject -Class Win32_Process -ComputerName \".\" |   Where-Object { $_.ProcessId -eq " + ProcID.ToString() + " } | ForEach-Object { $_.Terminate() }";

            return scriptTerminateProcess;
        }


    }

}

#endif