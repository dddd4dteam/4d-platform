﻿using System;
using System.Runtime.InteropServices;

namespace DDDD.Core.Environment
{


    // .NET & APP PROCESS: TargetFramework | ProcessProfileEn | ProcessHostEn |  ProcessTierEn |  ProcessLogicalRoleEn | ProcessLoadingTypeEn | ProcessWebScopeEn

    // POWERSHEL:  PSExecutionPolicyScope | PSExecutionPolicyEn 

    // PRINTERS:  PrinterEnumFlags | PrinterStatus | PrinterAttributes

    // REGISTRY PINVOKE:  PrinterEnumFlags | PrinterStatus | PrinterAttributes


    #region -------------------------  .NET & APP PROCESS -----------------------

    /// <summary>
    ///  TargetNetFramework - net framework that help us to detect .NET Framework Port for some Application Process instance. 
    ///<para/>  But this flag can be viewed in two modes: 
    ///<para/>    1 Environment2  runtime  detected .NET Framework Port(now called  TargetFramework) - value based on the the same precompiler flags as field Name.
    ///<para/>    2 To figure in some configuration - where it can show to us value that not depends on runtime process - it can by any of field.
    /// </summary>
    public enum TargetNetFrameworkEn
    {
        /// <summary>
        /// Default not defined case value
        /// </summary>
        NotDefined,

        /// <summary>
        /// .NET4.0
        /// </summary>
        NET4,

        /// <summary>
        /// .NET4.5
        /// </summary>
        NET45,

        /// <summary>
        /// .NET4.6
        /// </summary>
        NET46,

        /// <summary>
        /// Windows Store 8.1
        /// </summary>
        NETCORE451,

        /// <summary>
        /// SL5 5.x
        /// </summary>
        SL5,


        /// <summary>
        /// Windows Phone SL5 8
        /// </summary>
        WP8,

        /// <summary>
        /// Windows Phone RT 8 
        /// </summary>
        WPA8,

        /// <summary>
        /// Windows Phone SL5 8.1
        /// </summary>
        WP81,

        /// <summary>
        /// Windows Phone RT 8.1
        /// </summary>
        WPA81,

        /// <summary>
        /// Linux Mono
        /// </summary>
        MONO,

        /// <summary>
        /// Mono for Android
        /// </summary>
        MONOANDROID,

        /// <summary>
        /// Mono for  IOS touch  
        /// </summary>
        MONOTOUCH,

        /// <summary>
        /// Universal application 
        /// </summary>
        UAP
    }


    /// <summary>
    ///  Профиль процесса с которым он запускается и инициализируется для дальнейшей работы
    ///  Process Environment  Profile - is the information about PREPROCESSOR FLAGS in current library build - that consist of two items: 
    ///  [1]Tiers - CLIENT || SERVER || SERVER_MIDLETIER
    /// and 
    ///  [2]  .NET Plarform Ports  -   SL5 || WINRT || WP||  WPF || MONO || MONODROID || MONOTOUCH 
    /// </summary>
    public enum ProcessProfileEn
    {
        /// <summary>
        ///  Client tier; .NET Platform - SILVELRIGHT 5  
        /// </summary>
        CLIENT_SL5,

        /// <summary>
        ///  Client tier; .NET Platform - WINRT  
        /// </summary>
        CLIENT_WINRT,

        /// <summary>
        ///  Client tier; .NET Platform - WPF
        /// </summary>
        CLIENT_WPF,

        /// <summary>
        ///  Client tier; .NET Platform - Windows Phone  
        /// </summary>
        CLIENT_WP81,

        /// <summary>
        ///  Client tier; .NET Platform - Mono for linux(like Ubuntu/ Debian ...)  
        /// </summary>
        CLIENT_MONO,

        /// <summary>
        ///  Client tier; .NET Platform - Mono for Google Android
        /// </summary>
        CLIENT_MONODROID,

        /// <summary>
        /// Client tier; .NET Platform - Mono for Apple IOS  
        /// </summary>
        CLIENT_MONOTOUCH,

        /// <summary>
        ///   SERVER tier - Target WEB ENDPOINT ; .NET Platform -  full MS .net 4.0 or 4.5
        /// </summary>
        WEBSERVER,

        /// <summary>
        ///  SERVER tier - Target WEB ENDPOINT  ; .NET Platform -  Mono for linux(like Ubuntu/ Debian ...)
        /// </summary>
        WEBSERVER_MONO,

        /// <summary>
        /// Application  Server- Middletier; .NET Platform -  full MS .net 4.0 or 4.5 
        /// </summary>
        APPSERVER,

        /// <summary>
        /// Application  Server- Middletier; .NET Platform -  Mono for linux(like Ubuntu/ Debian ...) 
        /// </summary>
        APPSERVER_MONO

    }


    /// <summary>
    /// PROCESS Host  of Application
    /// </summary>
    public enum ProcessHostEn
    {
        /// <summary>
        /// 
        /// </summary>
        WebServerIIS,

        /// <summary>
        /// for WPF
        /// </summary>
        Desktop,

        /// <summary>
        /// For SL5 
        /// </summary>
        Browser,


        /// <summary>
        /// 
        /// </summary>
        Azure
    }


    /// <summary>
    /// PROCESS LOGICAL TIER IN ENTERPRISE SYSTEM
    /// </summary>
    public enum ProcessTierEn
    {
        /// <summary>
        /// WebEndpoint,  Router 
        /// </summary>
        SERVER,

        /// <summary>
        /// Gateway; Distributor
        /// </summary>
        APPSERVER,


        /// <summary>
        /// Client App - WP; SL; WPF; MTOUCH; MDROID ;M 
        /// </summary>
        CLIENT
    }


    /// <summary>
    /// Логическая роль Процесса
    /// </summary>
    public enum ProcessLogicalRoleEn
    {
        WEBENDPOINT,

        ROUTER,

        DISPATCHER,

        GATEWAY,

        DISTRIBUTOR,

        DALDISTRIBUTOR,

        CLIENTAPPPROCESS
    }


    /// <summary>
    /// Тип загрузки и инициализации Процесса Приложения 
    /// </summary>
    public enum ProcessLoadingTypeEn
    {
        /// <summary>
        /// Подгрузить - как локального процесса- т.е. он считывает конфигурацию которая находится в папке 
        /// Configuration относительно каталога загрузки,но не из системной папки конфигурации платформы или Хранилища Конфигураций.
        /// Ну и конфигурация сама настраивается на догрузку остальных компонентов из локальных директорий
        /// </summary>
        LoadLocally,


        /// <summary>
        /// Подгрузить - как процесса  от платформы - т.е. он считывает конфигурацию которая находится в папке 
        /// Configuration  по пути установки платформы, не локально или Хранилища Конфигураций.
        /// Ну и конфигурация сама настраивается на догрузку остальных компонентов из папок Платформы
        /// </summary>
        LoadFromPlatform,


        /// <summary>
        /// Подгрузить - как процесса  от платформы - т.е. он считывает конфигурацию которая находится в хранилище конфигураций - БД конфигураций
        ///  не локально или из папки кинфигурации.
        /// Ну и конфигурация сама настраивается на догрузку остальных компонентов из папок Платформы
        /// </summary>
        LoadFromStore
    }


    /// <summary>
    /// Область действия Веб Процесса
    /// </summary>
    public enum ProcessWebScopeEn
    {
        /// <summary>
        /// Область действия - Интранет Веб-Процесса
        /// </summary>
        IntranetWeb,

        /// <summary>
        /// Область действия - Интернет Веб-Процесса
        /// </summary>
        InternetWeb
    }



    #endregion -------------------------  .NET & APP PROCESS -----------------------


    #region -------------------------------- POWERSHELL ENUMS --------------------------------


    /// <summary>
    ///PowerShell  Execution Policy Scope
    /// </summary>
    public enum PSExecutionPolicyScope
    {
        NotDefined
      ,
        CurrentUser

      , LocalMachine

    }



    /// <summary>
    /// PowerShell  execution policy
    /// </summary>
    public enum PSExecutionPolicyEn
    {
        /// <summary>
        /// Not defined PS Execution Policy
        /// </summary>
        NotDefined
        ,
        /// <summary>
        /// No scripts can be run. Windows PowerShell can be used only in interactive mode.
        /// </summary>
        Restricted
        ,

        /// <summary>
        /// Only scripts signed by a trusted publisher can be run.
        /// </summary>
        AllSigned

        ,
        /// <summary>
        /// Downloaded scripts must be signed by a trusted publisher before they can be run.
        /// </summary>
        RemoteSigned

        ,
        /// <summary>
        /// No restrictions; all Windows PowerShell scripts can be run.
        /// </summary>
        Unrestricted

    }

    /// <summary>
    /// PowerShell  script result filter
    /// </summary>
    public enum PSFormatEn
    {
        NotDefined
       ,
        FormatList
       ,
        FormatTable
    }

    #endregion -------------------------------- POWERSHELL ENUMS --------------------------------


    #region ---------------------------------- WINDOWS OPTIONAL FEATURES ---------------------------------


    [ComVisible(true)]
    public enum Windows81OptionalFeatures
    {
        Microsoft_Hyper_V_All

     , Microsoft_Hyper_V_Tools_All

     , Microsoft_Hyper_V

     , Microsoft_Hyper_V_Management_Clients

     , Microsoft_Hyper_V_Management_PowerShell

     , Printing_Foundation_Features

     , Printing_Foundation_LPRPortMonitor

     , Printing_Foundation_LPDPrintService

     , Printing_Foundation_InternetPrinting_Client

    , FaxServicesClientPackage

    , ScanManagementConsole

    , LegacyComponents

    , DirectPlay

    , SimpleTCP

    , SNMP

    , WMISnmpProvider

    , Windows_Defender_Default_Definitions

    , Windows_Identity_Foundation

    , MicrosoftWindowsPowerShellV2Root

    , MicrosoftWindowsPowerShellV2

    , DirectoryServices_ADAM_Client

    , Internet_Explorer_Optional_amd64

    , NetFx3

    , NetFx4

    , IIS_WebServerRole

    , IIS_WebServer

    , IIS_CommonHttpFeatures

    , IIS_HttpErrors

    , IIS_HttpRedirect

    , IIS_ApplicationDevelopment

    , IIS_NetFxExtensibility

    , IIS_NetFxExtensibility45

    , IIS_HealthAndDiagnostics

    , IIS_HttpLogging

    , IIS_LoggingLibraries

    , IIS_RequestMonitor

    , IIS_HttpTracing

    , IIS_Security

    , IIS_URLAuthorization

    , IIS_RequestFiltering

    , IIS_IPSecurity

    , IIS_Performance

    , IIS_HttpCompressionDynamic

    , IIS_WebServerManagementTools

    , IIS_ManagementScriptingTools

    , IIS_IIS6ManagementCompatibility

    , IIS_Metabase

    , WAS_WindowsActivationService

    , WAS_ProcessModel

    , WAS_NetFxEnvironment

    , WAS_ConfigurationAPI

    , IIS_HostableWebCore

    , IIS_CertProvider

    , IIS_WindowsAuthentication

    , IIS_DigestAuthentication

    , IIS_ClientCertificateMappingAuthentication

    , IIS_IISCertificateMappingAuthentication

    , IIS_ODBCLogging

    , IIS_StaticContent

    , IIS_DefaultDocument

    , IIS_DirectoryBrowsing

    , IIS_WebDAV

    , IIS_WebSockets

    , IIS_ApplicationInit

    , IIS_ASPNET

    , IIS_ASPNET45

    , IIS_ASP

    , IIS_CGI

    , IIS_ISAPIExtensions

    , IIS_ISAPIFilter

    , IIS_ServerSideIncludes

    , IIS_CustomLogging

    , IIS_BasicAuthentication

    , IIS_HttpCompressionStatic

    , IIS_ManagementConsole

    , IIS_ManagementService

    , IIS_WMICompatibility

    , IIS_LegacyScripts

    , IIS_LegacySnapIn

    , IIS_FTPServer

    , IIS_FTPSvc

    , IIS_FTPExtensibility

    , MSMQ_Container

    , MSMQ_Server

    , MSMQ_Triggers

    , MSMQ_ADIntegration

    , MSMQ_HTTP

    , MSMQ_Multicast

    , MSMQ_DCOMProxy

    , WCF_Services45

    , WCF_HTTP_Activation45

    , WCF_TCP_Activation45

    , WCF_Pipe_Activation45

    , WCF_MSMQ_Activation45

    , WCF_TCP_PortSharing45

    , WCF_HTTP_Activation

    , WCF_NonHTTP_Activation

    , NetFx4_AdvSrvs

    , NetFx4Extended_ASPNET45

    , MediaPlayback

    , WindowsMediaPlayer

    , Microsoft_Windows_MobilePC_Client_Premium_Package_net

    , Microsoft_Windows_MobilePC_LocationProvider_INF

    , Printing_XPSServices_Features

    , RasCMAK

    , RasRip

    , MSRDC_Infrastructure

    , SearchEngine_Client_Package

    , TelnetClient

    , TelnetServer

    , TFTP

    , TIFFIFilter

    , Xps_Foundation_Xps_Viewer

    , WorkFolders_Client

    , SMB1Protocol

    }


    #endregion ---------------------------------- WINDOWS OPTIONAL FEATURES ---------------------------------


    #region ----------------------- REGISTRY PINVOKE -----------------------------

    public enum RegSAM
    {
        QueryValue = 0x0001,
        SetValue = 0x0002,
        CreateSubKey = 0x0004,
        EnumerateSubKeys = 0x0008,
        Notify = 0x0010,
        CreateLink = 0x0020,
        WOW64_32Key = 0x0200,
        WOW64_64Key = 0x0100,
        WOW64_Res = 0x0300,
        Read = 0x00020019,
        Write = 0x00020006,
        Execute = 0x00020019,
        AllAccess = 0x000f003f
    }


    public enum RegHive : uint
    {
        HKEY_LOCAL_MACHINE = 0x80000002u
      ,
        HKEY_CURRENT_USER = 0x80000001u
    }


    #endregion ----------------------- REGISTRY PINVOKE -----------------------------


    #region -------------------------- PRINTERS ENUMS ---------------------------------


    [Flags]
    public enum PrinterEnumFlags
    {
        Default = 0x00000001,
        Local = 0x00000002,
        Connections = 0x00000004,
        Favorite = 0x00000004,
        Name = 0x00000008,
        Remote = 0x00000010,
        Shared = 0x00000020,
        Network = 0x00000040,
        Expand = 0x00004000,
        Container = 0x00008000,
        IconMask = 0x00ff0000,
        Icon1 = 0x00010000,
        Icon2 = 0x00020000,
        Icon3 = 0x00040000,
        Icon4 = 0x00080000,
        Icon5 = 0x00100000,
        Icon6 = 0x00200000,
        Icon7 = 0x00400000,
        Icon8 = 0x00800000,
        Hide = 0x01000000
    }


    [Flags]
    public enum PrinterStatus
    {
        Busy = 0x200,
        DoorOpen = 0x400000,
        Error = 0x2,
        Initializing = 0x8000,
        IOActive = 0x100,
        ManualFeed = 0x20,
        NoToner = 0x40000,
        NotAvailable = 0x1000,
        Offline = 0x80,
        OutOfMemory = 0x200000,
        OutputBinFull = 0x800,
        PagePunt = 0x80000,
        PaperJam = 0x8,
        PaperOut = 0x10,
        PaperProblem = 0x40,
        Paused = 0x1,
        PendingDeletion = 0x4,
        Printing = 0x400,
        Processing = 0x4000,
        TonerLow = 0x20000,
        UserIntervention = 0x100000,
        Waiting = 0x2000,
        WarmingUp = 0x10000
    }


    [Flags]
    public enum PrinterAttributes
    {
        Queued = 0x1,
        Direct = 0x2,
        Default = 0x4,
        Shared = 0x8,
        Network = 0x10,
        Hidden = 0x20,
        Local = 0x40,
        WorkOffline = 0x400,
        EnableBidi = 0x800
    }


    #endregion -------------------------- PRINTERS ENUMS ---------------------------------


    #region ------------------------------- Windows SECURITY ----------------------------

    [ComVisible(true)]
    public enum TokenImpersonationLevel
    {
        None = 0,
        Anonymous = 1,
        Identification = 2,
        Impersonation = 3,
        Delegation = 4
    }


    [ComVisible(true)]
    public enum WindowsAccountType
    {
        Normal = 0,
        Guest = 1,
        System = 2,
        Anonymous = 3
    }

    // Keep in [....] with vm\comprincipal.h
    internal enum WinSecurityContext
    {
        Thread = 1, // OpenAsSelf = false
        Process = 2, // OpenAsSelf = true
        Both = 3 // OpenAsSelf = true, then OpenAsSelf = false
    }

    internal enum ImpersonationQueryResult
    {
        Impersonated = 0,    // current thread is impersonated
        NotImpersonated = 1,    // current thread is not impersonated
        Failed = 2     // failed to query 
    }

    #endregion ------------------------------- Windows SECURITY ----------------------------


    #region -----------------------------Application  Subfolder -------------------------------

    /// <summary>
    /// Possibe Subfolders to Application RootFolder
    /// </summary>
    public enum SubfolderEn
    {
          Temp
        , Distrib
        , Tools
        , Updates
        , Scripts
        , PS
        , Components
        , Modules
        , LocalDB
        , Config
        , Logs
    }

    #endregion -----------------------------Application  Subfolder -------------------------------



    /// <summary>
    /// Профиль тестирования
    /// </summary>
    /// TEST





}
