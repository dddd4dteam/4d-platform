﻿#if NOTRELEASED 

using System;
using System.IO;

#if SL5
    using System.Runtime.InteropServices.Automation;
#endif

using System.Threading.Tasks;
using System.Windows;
using DDDD.Core.Dependencies;
using DDDD.Core.Extensions;
 

namespace DDDD.Core.Environment.Devices
{

    //cmd templates
    //devconx64 status "@ROOT\WPD\0000" > a.txt
    //devconx64 restart "@ROOT\WPD\0000" > a.txt
    //devconx64 /r disable "@ROOT\WPD\0000" > a.txt



    /// <summary>
    ///  DevConService represents some functionality Upon DevConx64.
    /// <para/> DevConx64 is the Device Console tool that help us to manipulate by PC Devices like from Device Manager 
    /// </summary>
    public class DeviceManager
    {
#region --------------------------------- CTOR ------------------------------------

        DeviceManager()
        {
            Initialize();
        }

#endregion --------------------------------- CTOR ------------------------------------

        internal static DeviceManager Create()
        {
            return new DeviceManager();
        }


#region ------------------------------- CONST -------------------------------

        /// <summary>
        /// Name of the Tool [DeviceManagement]. Devconx64 is standart MS tool - exe file that should be downloded previosly.
        /// </summary>
        public const string ToolName = "Devconx64";

#endregion ------------------------------- CONST -------------------------------



#region --------------------------- PROPERTIES ----------------------------

        /// <summary>
        /// Tool(Devconx64) Exe file path    
        /// <para/> Rule - [ToolDir]\[Tool +  ".exe"]
        /// </summary>
        public string ToolFilePathClientCmd
        {
            get
            {
                return CmdPackageDependencyManager.Get_PackageFilePathClient(ToolName)
                 .Replace(CmdPackageDependencyManager.PackageExt, ".exe");
            }
        }

        /// <summary>
        /// Path to the Output stream file - for tool messaging
        /// </summary>
        public string ToolOutputFilePathClient
        {
            get
            {
                return Path.Combine(CmdPackageDependencyManager.Get_PackageDirClient(ToolName), "output.txt");
            }
        }

#endregion --------------------------- PROPERTIES ----------------------------


#region ------------------------- INITIALIZE SERVICE ---------------------------------

        void Initialize()
        {
            //register dependencies of DevConService            
            CmdPackageDependencyManager.RegisterDependecies<DeviceManager>();
        }

#endregion ------------------------- INITIALIZE SERVICE ---------------------------------


#region ----------------------------Command Process Utils ---------------------------------


        string BuildToolConsoleCmd(string deviceID, string toolCmd)
        {
            var deviceIDInCmd = " \"@" + deviceID + "\"";
            var testCmd = @"%comspec% /c " + ToolFilePathClientCmd//
                            + " " + toolCmd
                            + deviceIDInCmd
                            + " > \""
                            + Path.Combine(CmdPackageDependencyManager.Get_PackageDirClient(ToolName), "output.txt")
                            + "\"";

            return testCmd;
        }


        async Task ProcessConsoleCmd(string CmdWrap, string toolCmd, string deviceID)
        {
            //if AutomationFactory Not Available   - Environment2.WScriptShell.Value will throw exception
            try
            {
                await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(CmdWrap);

                dynamic devconIO = Environment2.WScriptShell
                                                    .Run(BuildToolConsoleCmd(deviceID, toolCmd), 0, true);


                // MessageBox.Show(" Running on TABLET: [{0}]".Fmt(Environment2.IsRunningOnTablet.S().ToUpper())  );


                File.AppendAllText(ToolOutputFilePathClient,
                    "Operation Finished Time: {0}  ".Fmt(DateTime.Now.ToShortTimeString()));
                //$"Operation Finished Time: {DateTime.Now.ToShortTimeString()}  ");

                MessageBox.Show(
                    "Device with ID [{0}] was SUCCESSFULLY [{1}]ed".Fmt(deviceID, toolCmd));
                //$"Device with ID [{deviceID}] was successfully [{toolCmd}]ed");

            }
            catch (Exception exc)
            {
                MessageBox.Show(
                    " Device was not [{0}]ed with ERROR: [ {1} ]".Fmt(toolCmd, exc.Message));
                //$" Device was not [{toolCmd}]ed with ERROR: [ {exc.Message} ]");
            }
        }


#endregion ----------------------------Command Process Utils ---------------------------------


#region ---------------------------- PUBLIC API- COMMAND WRAP METHODS  -------------------------------

        /// <summary>
        /// Restart Device by it's ID. 
        /// <para/> ID of some device can be found on Events Tab of Device Properties window.
        /// <para/> ID of device looks like "ROOT\WPD\0000"
        /// </summary>
        /// <param name="DeviceID"></param>
        /// [CmdPackageDependency(nameof(DeviceManager.RestartDeviceByID), PackageTypeEn.Tools, ToolName)]
        [CmdPackageDependency("DeviceManager.RestartDeviceByID", PackageTypeEn.Tools, ToolName)]
        public async Task RestartDeviceByID(string deviceID)
        {
#if SL5
            if (!Environment2.EnableSecuredOperations) return;
#endif

            var toolCmd = "restart";
            await ProcessConsoleCmd("DeviceManager.RestartDeviceByID", toolCmd, deviceID);
            //nameof(DeviceManager.RestartDeviceByID), toolCmd, deviceID);

        }


        /// <summary>
        /// Switch device into  Disabled state by Device ID. 
        /// <para/> ID of some device can be found on Events Tab of Device Properties window.
        /// <para/> ID of device looks like "ROOT\WPD\0000"
        /// </summary>
        /// <param name="DeviceID"></param>
        /// [CmdPackageDependency(nameof(DeviceManager.DisableDeviceByID), PackageTypeEn.Tools, ToolName)]
        [CmdPackageDependency("DeviceManager.DisableDeviceByID", PackageTypeEn.Tools, ToolName)]
        public async Task DisableDeviceByID(string deviceID)
        {
#if SL5
            if (!Environment2.EnableSecuredOperations) return;
#endif

            var toolCmd = "disable";
            await ProcessConsoleCmd("DeviceManager.DisableDeviceByID", toolCmd, deviceID);
            //nameof(DeviceManager.DisableDeviceByID), toolCmd, deviceID);
        }


        /// <summary>
        /// Switch Device into Enabled state by Device ID. 
        /// <para/> ID of some device can be found on Events Tab of Device Properties window.
        /// <para/> ID of device looks like "ROOT\WPD\0000"
        /// </summary>
        /// <param name="DeviceID"></param>
        /// [CmdPackageDependency(nameof(DeviceManager.EnableDeviceByID), PackageTypeEn.Tools, ToolName)] 
        [CmdPackageDependency("DeviceManager.EnableDeviceByID", PackageTypeEn.Tools, ToolName)]
        public async Task EnableDeviceByID(string deviceID)
        {
#if SL5
            if (!Environment2.EnableSecuredOperations) return;
#endif

            var toolCmd = "enable";
            await ProcessConsoleCmd("DeviceManager.EnableDeviceByID", toolCmd, deviceID);
            //nameof(DeviceManager.EnableDeviceByID), toolCmd, deviceID);
        }



#endregion ---------------------------- PUBLIC API- COMMAND WRAP METHODS  -------------------------------




    }
}

#endif 
