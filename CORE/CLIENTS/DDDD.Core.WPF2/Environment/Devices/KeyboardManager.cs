﻿
#if NOT_RELEASED
		 

using System;

using System.Threading;
using DDDD.Core.Threading;
using DDDD.Core.Install;

namespace DDDD.Core.Environment
{
    /// <summary>
    /// PC Keyboards Utils. Goes here from Win32API as dynamic objects.
    /// </summary>
    public class KeyboardManager
    {
        /// <summary>
        /// Specifies we wish to retrieve window styles.
        /// </summary>
        public const int GWL_STYLE = -16;

        public const uint KeyboardClosedStyle = 2617245696;

        const string REGKEY_EdgeTargetDockerState = @"HKCU\Software\Microsoft\TabletTip\1.7\EdgeTargetDockedState";

        const string TabTipFilePath = @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe";

        static Timer timer;

        bool tabTipDockedWarningShowed = false;

#if SL5

         /// <summary>
        /// Current Edge Target Docker State - state of Virtual keyboard - if it docked on edge of screen.
        /// </summary>
        public int EdgeTargetDockerState
        {
            get
            {
                return Installer.RegistryReadWSH<int>( REGKEY_EdgeTargetDockerState);
            }
        }

      

#endif


        /// <summary>
        ///  Keyboards by WMIService
        /// </summary>
        public dynamic Keyboards
        {
            get
            {
                return Environment2.WMIService.ExecQuery(@"Select * from Win32_Keyboard");
            }
        }

        //bool _IsHardwareKeyboardConnected = false;
        /// <summary>
        /// Check If Hardware Keyboard Connected
        /// </summary>
        public bool IsHardwareKeyboardConnected
        {
            get
            {
                var reslt = false;

                if (Keyboards.Count() == 0) reslt = false;

                else if (Keyboards.Count() == 1) // Проверка для планшетов HP где есть некая Клавиатура HID
                {
                    foreach (dynamic keyboard in Keyboards)
                        reslt = !(keyboard.Description == "Клавиатура HID");
                }
                else
                    reslt = true;

                return reslt;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void OpenTabTip()
        {
            Environment2.ShellApplication.ShellExecute(TabTipFilePath, "", "", "open", 1);
        }


        public static void CloseTabTip()
        {
            /*using (dynamic application = AutomationFactory.CreateObject("Shell.Application"))
                {
                    application.ShellExecute(@"C:\Windows\System32\taskkill.exe", "/F /IM TabTip.exe", "", "open", 0);
                }*/
            uint WM_SYSCOMMAND = 274;
            uint SC_CLOSE = 61536;
            IntPtr KeyboardWnd = Native.FindWindow("IPTip_Main_Window", null);
            Native.PostMessage(KeyboardWnd.ToInt32(), WM_SYSCOMMAND, (int)SC_CLOSE, 0);
        }


        public void StartTimerForKeyboardClosedEvent(Action timerCloseKeyboardAction)
        {
            if (timer == null)
            {
                timer = new Timer((obj) =>
                {
                    IntPtr KeyboardWnd = Native.FindWindow("IPTip_Main_Window", null);
                    if (KeyboardWnd.ToInt32() == 0 || Native.GetWindowLong(KeyboardWnd, GWL_STYLE) == KeyboardClosedStyle)
                    {
                        //Dispatcher.BeginInvoke(() => MessageInformer.ShowInfo("Closed!"));
                        UIInvoker.InvokeAsynchronously(() => timerCloseKeyboardAction());
                        timer.Dispose();
                        timer = null;
                    }
                }, null, 700, 50);
            }
        }


        //private static void CheckIfHardwareKeyboardConnected()
        //{
        //    new Thread(() =>
        //    {
        //        dynamic keyboards = Environment2.WMIService.ExecQuery(@"Select * from Win32_Keyboard");
        //        if (keyboards.Count() == 0)
        //            HardwareKeyboardConnected = false;
        //        else if (keyboards.Count() == 1) // Проверка для планшетов HP где есть некая Клавиатура HID
        //        {
        //            foreach (dynamic keyboard in keyboards)
        //                HardwareKeyboardConnected = !(keyboard.Description == "Клавиатура HID");
        //        }
        //        else
        //            HardwareKeyboardConnected = true;
        //    }).Start();
        //}



        //Environment2. WScript.Shell
        //private void CheckIfTabTipDockedAndShowWarnng()
        //{
        //    if (tabTipDockedWarningShowed == false)
        //        new Thread(() =>
        //        {
        //            if (EdgeTargetDockerState == 1)
        //            {
        //                tabTipDockedWarningShowed = true;
        //                Dispatcher.BeginInvoke(() => Core. MessageInformer.ShowWarning("Не рекомендуется использовать виртуальную клавиатуру развернутую во всю ширину экрана. " +
        //                    "Это может привести к неполной отрисовке интерфейса CRM."));
        //            }
        //        }).Start();
        //}

    }

}


#endif
