﻿#if NOTRELEASED 
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DDDD.Core.Environment
{

    /// <summary>
    ///  Devices Utils in current PC Environment.
    /// </summary>
    public class Devices
    {

        static DeviceManager _Manager = DeviceManager.Create();

        /// <summary>
        /// Devices Manager - command set from  DevCon(DeviceConsole)  console tool.
        /// <para/> It Looks like Windows DeviceManager.
        /// </summary>
        public static DeviceManager Manager
        {
            get { return _Manager; }
        }


#region ----------------------------- SENSORMANAGER ----------------------------
#if SL5

        static SensorManager _Sensors = new SensorManager();

        /// <summary>
        /// 
        /// </summary>
        public static SensorManager Sensors
        {
            get { return _Sensors; }
        }



#endif



#endregion ----------------------------- SENSORMANAGER ----------------------------


#region ------------------------------- KEYBOARD MANAGER--------------------------


        static KeyboardManager _Keyboards = new KeyboardManager();

        /// <summary>
        /// 
        /// </summary>
        public static KeyboardManager Keyboards
        {
            get { return _Keyboards; }
        }
#endregion ------------------------------- KEYBOARD MANAGER--------------------------


    }

}


 #endif
