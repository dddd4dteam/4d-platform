﻿namespace DDDD.Core.Serialization
{
    /// <summary>
    /// TypeSerializerT  RuntimeIDs of-  {T} Type[TypeID], and its sub types [{EnumUnderlyingTypeID, ArrayElementTypeID, Arg1TypeID, Arg2TypeID}]
    /// </summary>
    public class RuntimeIDs
    {
        /// <summary>
        /// Enum Underlying Type ID in current SetSerializer context
        /// </summary>
        public int EnumUnderlyingTypeID { get; internal set; }

        /// <summary>
        /// Array Element Type ID in current SetSerializer context
        /// </summary>
        public int ArrayElementTypeID { get; internal set; }

        /// <summary>
        /// List_Arg1_  or ObservableCollection_Arg1_ or  Dictionary_Arg1     Type ID in current SetSerializer context
        /// </summary>
        public int Arg1TypeID { get; internal set; }

        /// <summary>
        /// Dictionary_Arg2     Type ID in current SetSerializer context
        /// </summary>
        public int Arg2TypeID { get; internal set; }

    }
}
