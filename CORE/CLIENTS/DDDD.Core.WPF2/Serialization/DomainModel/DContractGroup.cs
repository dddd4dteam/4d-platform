﻿//#if NOT_RELEASED

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;

namespace DDDD.Core.Serialization.Domain
{


    /// <summary>
    /// DomainContractGroup - group/set of Domain Contracts( DContract)
    /// Группа- сгруппирвоанные контракты функциональной модели.ГРуппа не содержит собственой Рантайм Модели/Только Функцинальная модель. 
    /// В функциональной группе можно добавлять и удалять конракты - для работы в консоли. 
    /// После конечного редатирования состава контрактов группы функциональная модель  должна перестроить Рантайм МОдель конратов для protobuf-а
    /// </summary>    
    public class DContractGroup : INotifyPropertyChanged // NinjadbContract, IDisposable,
    {

        public const string DomainGroupFileExt = ".dmngrp";
        

#region -------------------------------------- PROPERTIES -----------------------------------


        /// <summary>
        /// DomainContractGroup  Id - to be compatible with NinjaDB store
        /// </summary>
        public Guid GroupId { get; set; }



        /// <summary>
        /// Service Domain Model to which this Group of Contracts belongs 
        /// </summary>        
        public String DomainModelName
        {
            get;
            set;
        }


        /// <summary>
        /// Infrastructure Contracts Group/ Additional Contracts Group/ Service Models CommonGroups
        /// </summary>        
        public String GroupName
        {
            get;
            set;
        }

        [IgnoreMember]
        ObservableCollection<DContract> _Contracts = new ObservableCollection<DContract>();

        /// <summary>
        /// Дополнительные контракты - Линейный список-словарь 
        /// </summary>    
        public ObservableCollection<DContract> Contracts
        {
            get
            {
                return _Contracts;
            }
            set
            {
                _Contracts = value;
            }
        }


        /// <summary>
        /// Type of Serialize Generator that associated with this Domain Contract Group
        /// </summary>
        [IgnoreMember]
        public Type SerializeGenerator
        {
            get;
            set;
        }


        /// <summary>
        ///  Type.FullName of Type Serialize Generator that associated with this Domain Contract Group.
        /// </summary>
        public String SerializeGeneratorFullName
        {
            get;
            set;
        }



#endregion -------------------------------------- PROPERTIES -----------------------------------


#region ------------------------------  DELEGATES  (for DomainModel)------------------------------

        /// <summary>
        /// Type - ParentType | Type - SubType
        /// </summary>
        internal Action<Type> OnContractAdded;



#endregion ------------------------------  DELEGATES (for DomainModel) ------------------------------


#region ------------------------------------- METHODS------------------------------------




        /// <summary>
        /// Add DContract,  that will be created from the Type, to the current DomainGroup.
        /// </summary>
        /// <param name="currentContract"></param>
        public void AddContract(Type currentContract)
        {
            Contracts.Add(DContract.Create( currentContract));

        }     


        /// <summary>
        /// Add DomainContracts,  that will be created from the TypeProcessor[]'s array, to the current DomainGroup.
        /// </summary>
        /// <param name="contracts"></param>
        public void AddContracts(Type[] contracts)
        {
            if (contracts == null) return;
            foreach (var ctrct in contracts)
            {
                Contracts.Add(DContract.Create( ctrct));
            }

        }





        /// <summary>
        /// If DomainContractGroup  contains Contract of searchForExist
        /// </summary>
        /// <param name="searchForExist"></param>
        /// <returns></returns>
        public bool ContainsContract(Type searchForExist)
        {
            //if (Contracts.Where(dc => dc.TypeInfo.TargetType == searchForExist).FirstOrDefault() != null)
                return true;


            return false;
        }



        /// <summary>
        /// Remove contact from DomainContractGroup if it exist in it.
        /// </summary>
        /// <param name="removingContract"></param>
        /// <returns></returns>
        public bool RemoveContract(Type removingContract)
        {

            if (ContainsContract(removingContract) && Contracts.ByContract(removingContract) == null)
            {
                Contracts.Remove(removingContract);
                return true;
            }


            return false;
        }
        




#region ------------------------------- INotifyPropertyChanged  ------------------------------------

        /// <summary>
        /// Notification event -  will be raised when some property change its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  Raise  PropertyChanged -Notification event - for some Property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property of DomainContractGroup");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }

#endregion -------------------------------  INotifyPropertyChanged  ------------------------------------







#endregion ------------------------------------- METHODS------------------------------------
                

    }



}

//#endif  // NOT RELEASED YET


#region --------------------------------- TSS Development  Domain --------------------------------------




//   inside DContract
#region --------------------------- Processing Policy ---------------------------

//[IgnoreMember]
//TypeProcessingPolicyEn _Policy = TypeProcessingPolicyEn.NotDefined;


///// <summary>
/////  Contract Processing Policy.
///// </summary>
//public TypeProcessingPolicyEn Policy
//{
//    get
//    {
//        return _Policy;
//    }
//    set
//    {
//        _Policy = value;
//        OnPropertyChanged(() => Policy);
//    }

//}


//String[] _OnlyMembers = null;
///// <summary>
/////  OnlyMembers used if you use ONLY_MEMBERS Processing Policy
///// </summary>
//public String[] OnlyMembers
//{
//    get
//    {
//        return _OnlyMembers;
//    }
//    set
//    {
//        _OnlyMembers = value;
//        OnPropertyChanged(() => OnlyMembers);
//    }

//}

#endregion --------------------------- Processing Policy ---------------------------


//   inside DContract
//public static DContract Create(TypeProcessor typeProcessor) // bool pHasSubtypes = false
//{
//    DContract dContract = new DContract();


//    dContract.RuntimeID = typeProcessor.ContractID;
//    dContract.Contract = typeProcessor.Contract;
//    dContract.AssemblyName = typeProcessor.Contract.Assembly.FullName;


//    dContract.GroupName = typeProcessor.TypeSerializeGenerator.GetType().Name;// GroupName;
//    dContract.SerializeGenerator = typeProcessor.TypeSerializeGenerator.GetType();
//    dContract.SerializeGeneratorFullName = typeProcessor.TypeSerializeGenerator.GetType().FullName;

//    dContract.Policy = typeProcessor.ProcessingPolicy;
//    dContract.OnlyMembers = typeProcessor.OnlyMembers;


//    return dContract;
//}




///// <summary>
///// inside DomainContractGroup Add DContract,  that will be created from the TypeProcessor, to the current DomainGroup. 
///// </summary>
///// <param name="typeProcessor"></param>
//public void AddContract(TypeProcessor typeProcessor)
//{
//    Contracts.Add(DContract.Create(typeProcessor));

//}


#endregion -------------------------------- TSS Development  Domain --------------------------------------


#region ---------------------------------   GARBAGE ------------------------------


//OnContractAddedAsSub = null;

///// <summary>
///// Type - ParentType | Type - SubType
///// Action в который может совершить Функциональная модель после добавления конкретного типа в список контрактов группы, 
///// именно при добавлении его как субтипа. В даннной группе(свойство  Contracts) он линеен но в RuntimeModel-и выстраивается дерево 
///// </summary>
//internal Action<Type, Type> OnContractAddedAsSub;


//public static void Serialize(DomainContractGroup group, String FolderPath)
//{
//    if (group.Contracts.Count == 0) return;

//    string groupFileEndPath = Path.Combine(FolderPath, group.GroupName + DomainGroupFileExt);
//    using(var fileStream = new FileStream(groupFileEndPath, FileMode.OpenOrCreate))
//    {
//        var serializer = TSSHubV3.GetDirect(TSSHubV3.DomainModelSerializerKey);
//        serializer.Serialize(fileStream, group);
//    }

//}


//public static void Deserialize( DomainContractGroup group,  String  FuncModelGroupFilePath)
//{

//    if (!File.Exists(FuncModelGroupFilePath)) return;


//    using (var fileStream = new FileStream(FuncModelGroupFilePath, FileMode.Open, FileAccess.Read))
//    {
//        var serializer = TSSHubV3.GetDirect(TSSHubV3.DomainModelSerializerKey);
//        group = serializer.Deserialize<DomainContractGroup>(fileStream).Result;
//    }            

//}


/// <summary>
/// Контракты добавляются всегда - Когда в Группе даже нет данного Парент типа то сначала добавится он а потом и текущий контракт спометкой что он его саб контракт.                                 
/// </summary>
/// <param name="ParentType"></param>
/// <param name="currentContract"></param>
/// <param name="OnContractAdded"></param>
/// <param name="NotUseInRuntimeModel"></param>
/// <param name="HasSubtypes"></param>
/// <param name="ApplyDefaultBehavior"></param>
//public void AddContract<TContract>( Type ParentType = null, Action<Type> OnContractAdded = null, bool NotUseInRuntimeModel = false, bool ApplyDefaultBehavior = true)
//{
//    Type currentContract = typeof(TContract);


//    AddContract(currentContract, ParentType, OnContractAdded, NotUseInRuntimeModel, ApplyDefaultBehavior);
//}


//Dictionary<Type, FuncModelContract> _Contracts = new Dictionary<Type, FuncModelContract>();
///// <summary>
///// Дополнительные контракты - Линейный список-словарь 
///// </summary>        
//[ProtoMember(3)]
//public Dictionary<Type, FuncModelContract> Contracts
//{
//    get
//    {
//        return _Contracts;
//    }
//}


//Dictionary<String, FuncModelContract> _ContractsByNames = new Dictionary<string, FuncModelContract>();
// /// <summary>
// /// 
// /// </summary>
//[ProtoMember(4)]
//public Dictionary<String, FuncModelContract> ContractsByNames
//{
//    get
//    {
//        return _ContractsByNames;
//    }
//    set
//    {
//        _ContractsByNames = value;
//    }
//}


#endregion  ---------------------------------   GARBAGE ------------------------------ 