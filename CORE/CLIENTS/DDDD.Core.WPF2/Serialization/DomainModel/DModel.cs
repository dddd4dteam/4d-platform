﻿//#if NOT_RELEASED

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.Core.Extensions;
using DDDD.Core.Serialization;

namespace DDDD.Core.Serialization.Domain
{




    /// <summary>
    /// DomainModel - it is the config data(from file) that help us to create TSSerializer instance later,
    /// or snapshot of TSSerializer’s  ProcessingDictionary(that consist of TypeProcessors) 
    /// - to transmit it to other side / or save this data  to use later them into  config file.
    /// </summary>    
    public class DModel : INotifyPropertyChanged //IDisposable,
    {

#region ------------------------------- CTOR --------------------------------

        public DModel()
        {
            
        }
        

#endregion ------------------------------- CTOR --------------------------------



#region ----------------------------- CONST --------------------------------

        /// <summary>
        ///  Route case in the Config Store data for FuncModels by Controller ClearName. 
        ///  Where controller ContractName ==   ClearName[Controller] +  
        /// </summary> 
        public const string DomainModelSuffix = "DomainModel";


        /// <summary>
        /// Группа контрактов по умолчанию
        /// </summary>
        public const String DefaultGroupKey = "DefaultGroup";


#endregion ----------------------------- CONST --------------------------------



#region --------------------------- PROPERTIES -------------------------


        /// <summary>
        /// DomainModel  Id - to be compatible with NinjaDB store
        /// </summary>        
        public Guid ModelId { get; set; }



#region ------------------NOTIFIABLE MODEL ITEM: TSS Identity KEY ------------------------------

        [IgnoreMember]
        Identification _Identification = null;

        /// <summary>
        /// TSS Identity Key Info
        /// </summary>
        public Identification Identification
        {
            get
            {
                return _Identification;
            }
            set
            {
                _Identification = value;
                OnPropertyChanged(() => Identification);
            }
        }


#endregion ------------------NOTIFIABLE MODEL ITEM: TSS Identity KEY  ------------------------------



#region ------------------NOTIFIABLE MODEL ITEM: Groups------------------------------

        // ItemName - Groups  -  Ex: Client 
        // Contract - ObservableCollection<String, DomainDataModelGroup>  -  Ex: V_S8_Client_Vo

        ObservableCollection<DContractGroup> _Groups = new ObservableCollection<DContractGroup>();

        /// <summary>
        /// Группы контрактов функциональной модели.В каждой группе масса контрактов
        /// </summary>       
        public ObservableCollection<DContractGroup> Groups
        {
            get { return _Groups; }
            set
            {
                _Groups = value; //Field changing
                OnPropertyChanged(() => Groups);
            }
        }

#endregion ------------------NOTIFIABLE MODEL ITEM: Groups------------------------------




#endregion --------------------------- PROPERTIES -------------------------
        


#region ---------------------------------METHODS-----------------------------



        /// <summary>
        /// Add DomainContractGroup : get if already exist or add then get exist group
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="groupTypes"></param>
        /// <returns></returns>
        public DContractGroup GetOrAddGroup(String GroupName, params Type[] groupTypes)
        {
            bool justAdded = false;//Добавлялась ли группа

            DContractGroup targetGroup = null;
            //Creating and adding new DomainDataModelGroup
            if (Groups.ContainsKey(GroupName) == false)
            {
                targetGroup = new DContractGroup();

                //у этих групп нет функциональной модели, поэтому они указывают на DomainDataModelService
                targetGroup.DomainModelName = Identification.GetIdentity();
                targetGroup.GroupName = GroupName;
                Groups.Add(targetGroup);

                justAdded = true;
            }
            else if (Groups.ContainsKey(GroupName))
            {
                targetGroup = Groups.ByName(GroupName);
            }

            //also we need  add types them before by protobuf attributes
            if (justAdded == true
                    && groupTypes != null
                    && groupTypes.Count() > 0
                    )
            {
                //adding types to Group
                foreach (var type in groupTypes)
                {
                    targetGroup.AddContract(type);
                }
            }

            return targetGroup;

        }


        /// <summary>
        ///  Add DomainContractGroup : get if already exist or add then get exist group 
        /// </summary>
        /// <param name="AddingNotExistedGoup"></param>
        /// <returns></returns>
        public DContractGroup GetOrAddGroup(DContractGroup AddingNotExistedGoup)
        {
            if (!Groups.ContainsKey(AddingNotExistedGoup.GroupName))
            {
                Groups.Add(AddingNotExistedGoup);
            }

            return Groups.ByName(AddingNotExistedGoup.GroupName);

        }



        /// <summary>
        ///  Removing Group by ContractName
        /// </summary>
        /// <param name="GroupName"></param>
        public void RemoveGroup(String GroupName)
        {
            if (Groups.ContainsKey(GroupName) == false)
            {
                Groups.ByName(GroupName);   //.Dispose();
                Groups.Remove(GroupName);
            }
        }



        /// <summary>
        /// If  Groups Contains Group with Name GroupName 
        /// </summary>
        /// <param name="GroupName"></param>
        /// <returns></returns>
        public bool ContainsGroup(String GroupName)
        {
            return Groups.ContainsKey(GroupName);
        }
            


        public const string DomainModelFileExt = ".dmnmdl";




        //public List<ITypeSerializer> GetDomainOnlySerializers()
        //{
        //    return null;
        //}


#endregion ---------------------------------METHODS-----------------------------
        


#region ------------------------------- INotifyPropertyChanged  ------------------------------------

        /// <summary>
        /// Notification event -  will be raised when some property change its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  Raise  PropertyChanged -Notification event - for some Property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property of DomainModel");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }

#endregion -------------------------------  INotifyPropertyChanged  ------------------------------------

    }

}


//#endif  // NOT RELEASED YET


#region -------------------------------- GARBAGE ----------------------------------


///// <summary>
///// Функциональная модель инфраструктуры.  
///// </summary>
//public const String InfrastructureDomainModelKey = "InfrastructureDomainModel";


///// <summary>
///// Первая Группа контрактов в  Функциональной модели инфраструктуры.Она потом копируется во все другие  FM-ы 
///// </summary>
//public const String InfrastructureDomainModelGroupKey = "InfrastructureDomainModelGroup";





//#region  -------------------------- Assemblies Cross Names by CONVENTIONS ----------------------------

//public static void Serialize(DomainModel domainModel, String FolderPath)
//{
//    if (domainModel.Groups.Count == 0) return;

//    string groupFileEndPath = Path.Combine(FolderPath, domainModel.Identification.GetSvcIdentity() + DomainModelFileExt);
//    using (var fileStream = new FileStream(groupFileEndPath, FileMode.OpenOrCreate))
//    {
//        var serializer = TSSHubV3.GetDirect(TSSHubV3.DomainModelSerializerKey);
//        serializer.Serialize(fileStream, domainModel);
//    }

//}


//public static void Deserialize(DomainModel group, String FuncModelFilePath)
//{

//    if (!File.Exists(FuncModelFilePath)) return;


//    using (var fileStream = new FileStream(FuncModelFilePath, FileMode.Open, FileAccess.Read))
//    {
//        var serializer = TSSHubV3.GetDirect(TSSHubV3.DomainModelSerializerKey);
//        group = serializer.Deserialize<DomainModel>(fileStream);
//    }

//}

//static Dictionary<String, String> AssemblyPartReplacements = new Dictionary<String, String>();



///// <summary>
///// Add AssemblyName part replacement.
///// For ex: CRM.DOM.DAL[.Server]  - here [.Server] - is replacement item. 
///// Replacement Aim - to get Assembly ContractName independent from Side  or  Side NetFramework  
///// </summary>
///// <param name="PartKey"></param>
///// <param name="ReplacementVal"></param>
//public static void AddAssemblyNamePartReplacement(String PartKey, String ReplacementVal)
//{
//    if (!AssemblyPartReplacements.ContainsKey(PartKey))
//    {
//        AssemblyPartReplacements.Add(PartKey, ReplacementVal);
//    }

//}


///// <summary>
///// 
///// </summary>
///// <param name="FullAssemblyName"></param>
///// <returns></returns>
//public static String GetCrossAssemblyName(String FullAssemblyName)
//{
//    string crossAssemblyName = FullAssemblyName;
//    foreach (var replaceitm in AssemblyPartReplacements)
//    {
//        crossAssemblyName = crossAssemblyName.Replace(replaceitm.Key, replaceitm.Value);
//    }

//    return crossAssemblyName;
//}




///// <summary>
/////  Встроенные замены - для формирования кроссплатформенного имени сборки фанк-Контрактов; Соглашения по умолчанию
///// </summary>
//private static void InitAssemblyNameDefaultReplacements()
//{
//    switch (GetTargetFramework())
//    {
//        case TargetNetFrameworkEn.NotDefined:
//            break;
//        case TargetNetFrameworkEn.NET4:
//            {
//                AddAssemblyNamePartReplacement(@".Server", "");
//                AddAssemblyNamePartReplacement(@".Desktop", "");

//                break;
//            }

//        case TargetNetFrameworkEn.NET45:
//            {
//                AddAssemblyNamePartReplacement(@".Server", "");
//                AddAssemblyNamePartReplacement(@".Desktop", "");

//                break;
//            }
//        case TargetNetFrameworkEn.SL5:
//            {
//                AddAssemblyNamePartReplacement(@".SL5", "");
//                break;
//            }
//        case TargetNetFrameworkEn.WP8:
//            {
//                AddAssemblyNamePartReplacement(@".WP8", "");
//                break;
//            }
//        case TargetNetFrameworkEn.WP81:
//            {
//                AddAssemblyNamePartReplacement(@".WP81", "");
//                break;
//            }
//        case TargetNetFrameworkEn.Mono:
//            {
//                AddAssemblyNamePartReplacement(@".MServer", "");
//                AddAssemblyNamePartReplacement(@".Mono", "");
//                break;
//            }
//        case TargetNetFrameworkEn.MonoDroid:
//            {
//                AddAssemblyNamePartReplacement(@".MDroid", "");
//                break;
//            }
//        case TargetNetFrameworkEn.MonoTouch:
//            {
//                AddAssemblyNamePartReplacement(@".MTouch", "");
//                break;
//            }
//        default:
//            break;
//    }

//}


//#endregion  -------------------------- Assemblies Cross Names by CONVENTIONS ----------------------------


#endregion -------------------------------- GARBAGE ----------------------------------

