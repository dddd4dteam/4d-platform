﻿
#if SERVER && NET45  || WPF 

using System;
using System.IO;
using System.Xml;
using System.Diagnostics;
using System.Runtime.Serialization;
using DDDD.Core.Serialization;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Serialization.Binary;

namespace DDDD.Core.Serialization.Xml
{
    /// <summary>
    /// XmlBinSerializer inside XmlObjectSerializer to use in ServiceModel.Behaviors.
    /// </summary>
    public class XmlBinSerializer : XmlObjectSerializer
    {

        #region ------------------------------- CTOR -------------------------------

        public XmlBinSerializer(  Type type) //int key, bool isList
        {
            // TODO: Complete member initialization
            //serializer = tsSerializer;
            this.type = type;
        }

        #endregion ------------------------------- CTOR -------------------------------------


        #region -------------------------------- CREATION --------------------------------

        public static XmlBinSerializer TryCreate( Type type)
        {
            //if (tsSerializer == null) throw new ArgumentNullException("tssSerializer");
            if (type == null) throw new ArgumentNullException("type");

            return new XmlBinSerializer(  type); //, isList key,
        }

        #endregion -------------------------------- CREATION --------------------------------


        #region --------------------------------- CONSTS -------------------------------------

        /// <summary>        
        ///  SERVER READ WCF MESSAGES FROM CLENTS -  READ ELEMENT - "ClientCommand". 
        /// </summary>
        private const string DDDDTSS_READ_CLIENTCOMMANDELEMENT = "ClientCommand";
        /// <summary>
        /// SERVER READ WCF MESSAGES FROM CLENTS -  READ ELEMENT - "b:Body". 
        /// </summary>
        private const string DDDDTSS_READ_BBODYELEMENT = "b:Body";

        /// <summary>
        /// <para/> SERVER WRITE WCF MESSAGE ITSELF  WRITEELEMENT - with "ddddtss" tag . 
        /// </summary>
        private const string DDDDTSS_WRITE_DDDDTSSELEMENT = "ddddtss";

        private const string CMD_DATACONTRACTNAMESPACE = "http://schemas.datacontract.org/2004/07/";

        private const string CMD_NAMESPACE = "DDDD.Core.Net.ServiceModel";

        #endregion -------------------------------- CONSTS --------------------------------------


        #region ----------------------------------- FIELDS --------------------------------------

        //private readonly ITypeSetSerializer serializer;

        private Type type;

        #endregion  ----------------------------------- FIELDS --------------------------------------


       

        public override bool IsStartObject(XmlDictionaryReader reader)
        {

            var stringValue = reader.ReadContentAsString();
            if (reader == null) throw new ArgumentNullException("reader");
            reader.MoveToContent();

            if (   reader.NodeType == XmlNodeType.Element 
                && reader.Name == DDDDTSS_READ_CLIENTCOMMANDELEMENT)
            {               
                return true;
            }           
            
            return false;
        }



        public override object ReadObject(Stream stream)
        {            
            //return base.ReadObject(stream);
            using(var reader = XmlDictionaryReader.CreateTextReader(stream, XmlDictionaryReaderQuotas.Max))
            {
                if (IsStartObject(reader))
                {
                  return  ReadObject(reader, true);
   
                }
            }
            return null;

        }

        

        public override object ReadObject(XmlDictionaryReader reader, bool verifyObjectName)
        {
            try
            {

                if (reader == null) throw new ArgumentNullException("reader");
                reader.MoveToContent();

                reader.ReadStartElement(DDDDTSS_READ_CLIENTCOMMANDELEMENT);//element ClientCommand          

                reader.ReadStartElement(DDDDTSS_READ_BBODYELEMENT);//element b:Body 

                bool isSelfClosed = reader.IsEmptyElement, isNil = reader.GetAttribute("nil") == "true";

                //null/ no real content / value
                object result = null;
                if (isNil)              // explicitly null 
                {
                    if (!isSelfClosed)
                    {
                        reader.ReadEndElement();

                    }
                }
                else if (isSelfClosed) // no real content
                {   //isList || isEnum


                }
                else //contain body data
                {
                    DebugAssert(reader.CanReadBinaryContent, "CanReadBinaryContent");

                    //isList/ IsEnum/ complex Contract 
#if DEBUG
                    var datainBody = reader.ReadContentAsBase64();
                    result = BinarySerializer.Deserialize<object>(datainBody);
#elif RELEASE
                    result = serializer.Deserialize<object>(reader.ReadContentAsBase64()); //"b:Body" into object

#endif
                    
                    reader.ReadEndElement(); //element b:Body 
                    reader.ReadEndElement(); //element ClientCommand 
                }

                return result;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($" ERROR IN {nameof(XmlBinSerializer)}.{nameof(ReadObject)}(): - Exception Message : [{exc.Message}]  ", exc);
            }
            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="graph"></param>
        public override void WriteStartObject(XmlDictionaryWriter writer, object graph)
        {
            try
            {
                if (writer == null) throw new ArgumentNullException("writer");
                var command = graph as DCMessage; //CommandMessage;
                              
                writer.WriteStartElement(DDDDTSS_WRITE_DDDDTSSELEMENT);
                writer.WriteAttributeString("xmlns", CMD_DATACONTRACTNAMESPACE + CMD_NAMESPACE);
                
            }
            catch (Exception exc)
            {                
                throw new InvalidOperationException($" ERROR IN {nameof(XmlBinSerializer)}.{nameof(WriteStartObject)}(): - Exception Message : [{exc.Message}]  ", exc);
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="graph"></param>
        public override void WriteObjectContent(XmlDictionaryWriter writer, object graph)
        {

            try
            {
                if (writer == null) throw new ArgumentNullException("writer");
            if (graph == null)
            {
                writer.WriteAttributeString("i:nil", "true");
            }
            else
            {
                using (MemoryStream ms = new MemoryStream())
                {
                   
                    BinarySerializer.Serialize(graph, ms );

                    byte[] buffer = ms.GetBuffer();
                    writer.WriteBase64(buffer, 0, (int)ms.Length);
                }
            }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($" ERROR IN {nameof(XmlBinSerializer)}.{nameof(WriteObjectContent)}(): - Exception Message : [{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public override void WriteEndObject(XmlDictionaryWriter writer)
        {
            if (writer == null) throw new ArgumentNullException("writer");
            writer.WriteEndElement();            
        }
             


        [Conditional("DEBUG")]
        public static void DebugAssert(bool condition, string message)
        {
#if DEBUG
            if (!condition)
            {
#if MF
                Microsoft.SPOT.Debug.Assert(false, message);
#else
                System.Diagnostics.Debug.Assert(false, message);
            }
#endif
#endif
        }


    }
}


#endif



#region --------------------------------- GARBAGE -------------------------------------------

//using (MemoryStream ms = new MemoryStream(reader.ReadContentAsBase64())) //"b:Body"
//{  //isList/ IsEnum/ complex Contract 
//result = serializer.Deserialize<object>(reader.ReadContentAsBase64()); //into object
//}

#endregion --------------------------------- GARBAGE -------------------------------------------
