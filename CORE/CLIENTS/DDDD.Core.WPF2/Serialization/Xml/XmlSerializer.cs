
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;

using DDDD.Core.IO.Memory;

namespace DDDD.Core.Serialization.Xml
{
	public class XmlSerializer
	{
		#region --------------------------- CTOR --------------------------

		static XmlSerializer()
		{
			XmlSerializer.XWSettings = new XmlWriterSettings();
			XmlSerializer.XRSettings = new XmlReaderSettings();
			XmlSerializer.Instance = new XmlSerializer(false, 1048576);
		}



		public XmlSerializer(bool omitXmlDeclaration = false, int maxCharsInDocument = 1048576)
		{
			XmlSerializer.XWSettings.Encoding = new UTF8Encoding(false);
			XmlSerializer.XWSettings.OmitXmlDeclaration = omitXmlDeclaration;
			XmlSerializer.XRSettings.MaxCharactersInDocument = (long)maxCharsInDocument;
		}


		#endregion --------------------------- CTOR --------------------------

		private readonly static XmlWriterSettings XWSettings;

		private readonly static XmlReaderSettings XRSettings;

		public static XmlSerializer Instance;


		private static object Deserialize(string xml, Type type)
		{
			object obj;
			try
			{
				using (XmlReader xmlReader = XmlReader.Create(new StringReader(xml), XmlSerializer.XRSettings))
				{
					obj = (new DataContractSerializer(type)).ReadObject(xmlReader);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new SerializationException(string.Concat("DeserializeDataContract: Error converting type: ", exception.Message), exception);
			}
			return obj;
		}

		public static T DeserializeFromReader<T>(TextReader reader)
		{
			return DeserializeFromString<T>(reader.ReadToEnd());
		}

		public static T DeserializeFromStream<T>(Stream stream)
		{
			return (T)(new DataContractSerializer(typeof(T))).ReadObject(stream);
		}

		public static object DeserializeFromStream(Type type, Stream stream)
		{
			return (new DataContractSerializer(type)).ReadObject(stream);
		}

		public static object DeserializeFromString( Type type, string xml)
		{
			return Deserialize(xml, type);
		}

		public static T DeserializeFromString<T>(string xml)
		{
			return (T)Deserialize(xml, typeof(T));
		}

		public static void SerializeToStream(object obj, Stream stream)
		{
			if (obj == null)
			{
				return;
			}
			using (XmlWriter xmlWriter = XmlWriter.Create(stream, XmlSerializer.XWSettings))
			{
				(new DataContractSerializer(obj.GetType())).WriteObject(xmlWriter, obj);
			}
		}

		public static string SerializeToString<T>(T from)
		{
			string end;
			try
			{
				using (MemoryStream stream = MemoryStreamFactory.GetStream())
				{
					using (XmlWriter xmlWriter = XmlWriter.Create(stream, XmlSerializer.XWSettings))
					{
						(new DataContractSerializer(from.GetType())).WriteObject(xmlWriter, from);
						xmlWriter.Flush();
						stream.Seek((long)0, SeekOrigin.Begin);
						end = (new StreamReader(stream)).ReadToEnd();
					}
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new SerializationException(string.Format("Error serializing object of type {0}", from.GetType().FullName), exception);
			}
			return end;
		}

		public static void SerializeToWriter<T>(T value, TextWriter writer)
		{
			try
			{
				using (XmlWriter xmlWriter = XmlWriter.Create(writer, XmlSerializer.XWSettings))
				{
					(new DataContractSerializer(value.GetType())).WriteObject(xmlWriter, value);
				}
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				throw new SerializationException(string.Format("Error serializing object of type {0}", value.GetType().FullName), exception);
			}
		}
	}
}