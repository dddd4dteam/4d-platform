﻿using System.Linq.Expressions;
using System.Xml.Linq;

namespace DDDD.Core.Serialization.Expressions 
{

    public abstract class CustomExpressionXmlConverter
    {
        public abstract bool TryDeserialize(XElement expressionXml, out Expression e);
        public abstract bool TrySerialize(Expression expression, out XElement x);
    }

}
