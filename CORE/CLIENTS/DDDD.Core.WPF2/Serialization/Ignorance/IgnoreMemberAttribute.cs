﻿using System;


namespace DDDD.Core.Serialization
{
    /*
     /// <para/>You need not to add reference to this assembly(TypeCachecService ) to use just right this attribute.
    /// You can copy this class into your library with your local namespace- and use that class, 
    /// there is no difference.
    /// <para/>There is no difference because the ignore member criterion is Attribute.Name.Contains("IgnoreMember")s.
   
    */


    /// <summary>
    /// D4 serialization way to ignore  marked Member from serialization - property or field.            
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class IgnoreMemberAttribute : Attribute
    {
    }
}
