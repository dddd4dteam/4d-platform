﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.IO.Text;
using DDDD.Core.Reflection;


namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Most commonly used TypeSerializeGenerator that generate Serialize/Deserialize expressions for:
    ///     - non generic custom Class or Struct;
    ///     - Nullable struct TypeSerializer will be added automatically on adding original Struct. So you shouldn't add it manually;
    ///     - types that  are not contained in [Default Contract] List of SerializeGenerator_Primitives generator;
    ///     - types  wich category are not Auto Adding Contract category.     
    /// </summary>
    internal class SerializeGenerator_ComplexTypeJson : ITypeSerializerGeneratorJson
    {

        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_ComplexTypeJson)
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_ComplexTypeJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_ComplexTypeJson)
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_ComplexTypeJson);

        private readonly static object locker = new object();



        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_ComplexStructure:
        ///     - non generic custom Class or Struct;
        ///     - Nullable struct TypeSerializer will be added automatically on adding original Struct. So you shouldn't add it manually;
        ///     - types that are not contained in [Default Contract] List of SerializeGenerator_Primitives generator;
        ///     - types  wich category are not Auto Adding Contract category.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return TypeSerializeAnalyser.CanHandle_ComplexType(type);
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------







        #region  --------------------------- GENERATE  SERIALIZE- TO STRING  WRITE  EXPRESSIONS  ------------------------



        static void WriteMember_ToString<T>(ITypeSerializerJson<T> tpSerializer
                                           , KeyValuePair<string, TypeMember> memberInform
                                           , TextWriter writer
                                           , T instanceValue)
        {
            //  var memberInfo = tpSerializer.AccessorT.GetMemberInfo(member);


            OperationInvoke.CallInMode(nameof(SGName), nameof(WriteMember_ToString)
                , () =>
                {
                    JNP.Write_FormatOnLevel(writer);

                    //for not sealed class, interface , nullable struct - 1 - types category           
                    var memberValue = tpSerializer.AccessorT.GetMember(memberInform.Key, instanceValue); //<TMember>
                    if (memberValue.IsNull())
                    {
                        writer.Write(memberInform.Key + " : " +  Sym.NULL);
                    }
                    else  // if not null  Sure in DataType and redrect if not this type
                    {
                        JNP.Write_MemberName(writer, memberInform.Key);

                        //// get real type
                        var valuetypeEx = memberValue.GetTypeInfoEx();
                        if (valuetypeEx.ID != memberInform.Value.TypeInf.ID)
                        {
                            /// locked code: for auto detecting and adding Contracts 
                            JsonSerializer.AddKnownType(valuetypeEx);

                            tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                                  valuetypeEx.ID
                                , writer
                                , memberValue
                                , true); //Write TypeName TRUE
                        }
                        else
                        {
                            tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                                  memberInform.Value.TypeInf.ID
                                , writer
                                , memberValue
                                , false); // DO NOT WRITE  TypeName
                        }

                    }

                }
                  , errorDetailMessage: " DETAILS-- T:[{0}] | MemberKey:[{1}] | TMember:[{2}]  | "
                       .Fmt(tpSerializer.TargetTypeEx.OriginalType.FullName, memberInform.Key, memberInform.Value.TypeInf.ID.S())
                );


        }




        static void Write_Nullable_ToString<T>(ITypeSerializerJson<T> tpSerializer
                                            , TextWriter writer
                                            , T value
                                            , bool needToWriteType = false)
        {
            // 1 = WE DON't KNOW IF WRITE Type or not
            // 1.1 - TYPE IS THE sAME- WRITE BODY NOT REDIRECT
            // 1.2 - ANOTHER TYPE - DON'T WRITE BODY - REDIRECT AND SAY WRITE TYPE - go to  2 
            // 2 - YES WRITE TYPE AND BODY


            if (needToWriteType == false)
            {  // 1 = WE DON't KNOW IF WRITE Type or not


                // write NULL  if null value
                if (value.IsNull())
                {
                    JNP.Write_Null(writer);
                    return; // stop writing
                }

                //// get real type
                var valuetypeEx = value.GetTypeInfoEx();
                if (tpSerializer.TypeID == valuetypeEx.ID)
                {   // 1.1 - TYPE IS THE sAME- WRITE BODY NOT REDIRECT

                    //write here thisvalue  WITHOUT ADDING Type Name
                    JNP.Write_ObjectStart(writer);

                    var count = tpSerializer.AccessorT.Members.Keys.Count;
                    var idx = 1;
                    foreach (var mmbr in tpSerializer.AccessorT.Members)
                    {
                        WriteMember_ToString<T>(tpSerializer
                                                , mmbr
                                                , writer
                                                , value);

                        if (idx < count)
                        {
                            JNP.Write_Comma(writer);
                        }
                        idx++;
                    }

                    JNP.Write_ObjectEnd(writer);
                }
                else
                {  // 1.2 - ANOTHER TYPE - DON'T WRITE BODY - REDIRECT AND SAY WRITE TYPE - go to  2 

                    /// locked code: for auto detecting and adding Contracts                     
                    var endValueSerializer = JsonSerializer.AddKnownType(valuetypeEx);

                    // WRITE TYPE Name ON THE TOP IN FOLLOWING REDIRECTION
                    endValueSerializer.RaiseSerializeToStringBoxedHandler(
                                            writer
                                            , value
                                            , true);//Trrue - Write Typename

                }

            }
            else
            {   // 2 - YES WRITE TYPE AND BODY

                // we come here from redirection of customBase Type to this child Type
                // so we already knew  value is not null , 
                // and we already added customType to JsnoSerializer 

                JNP.Write_ObjectStart(writer);

                //write here this type with  ADDING Type Name 
                JNP.Write_Type(writer, tpSerializer.TargetTypeEx);
                JNP.Write_Comma(writer);

                var count = tpSerializer.AccessorT.Members.Keys.Count;
                var idx = 1;
                foreach (var mmbr in tpSerializer.AccessorT.Members)
                {
                    WriteMember_ToString<T>(tpSerializer
                                            , mmbr
                                            , writer
                                            , value);

                    if (idx < count)
                    {
                        JNP.Write_Comma(writer);
                    }

                    idx++;

                }

                JNP.Write_ObjectEnd(writer);

            }

        }


        static void Write_NotNullable_ToString<T>(ITypeSerializerJson<T> tpSerializer
                                            , TextWriter writer
                                            , T value
                                            , bool needToWriteType = false)
        {
            //write here this type WITHOUT ADDING Type Name
            JNP.Write_ObjectStart(writer);
            var count = tpSerializer.AccessorT.Members.Keys.Count;
            var idx = 1;
            foreach (var mmbr in tpSerializer.AccessorT.Members)
            {
                WriteMember_ToString<T>(tpSerializer
                                        , mmbr
                                        , writer
                                        , value);

                if (idx < count)
                {
                    JNP.Write_Comma(writer);
                }

                idx++;
            }

            JNP.Write_ObjectEnd(writer);

        }
                                    

        static MethodInfo GetSerializeMethod(TypeInfoEx targetTypeEx)
        {
            if (targetTypeEx.IsCustomStruct && !targetTypeEx.IsNullableType)
            {
                return SGType.GetMethod(nameof(Write_NotNullable_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                     .MakeGenericMethod(targetTypeEx.OriginalType);
            }
            else
            {
                return SGType.GetMethod(nameof(Write_Nullable_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                    .MakeGenericMethod(targetTypeEx.OriginalType);
            }


        }


        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer) //
        {
            try
            {
                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && classes  - with  If(instance != null){}


                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var paramInstanceTypeInfoEx  = Expression.Constant(tpSerializer.TargetTypeEx);
                var parameterNeedToWriteType = Expression.Parameter(typeof(bool), "needToWriteType");


                var methodWrite_ComplexType_ToString = GetSerializeMethod(tpSerializer.TargetTypeEx);

                return Expression.Lambda<SDJson.SerializeToStringT<T>>
                                                       (
                                                        Expression.Call
                                                        (
                                                           methodWrite_ComplexType_ToString
                                                        , typeSerializerInstance
                                                        , parameterWriter
                                                        , parameterInputData
                                                        , parameterNeedToWriteType
                                                        )
                                                       , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                       , new[] {
                                                            parameterWriter
                                                            , parameterInputData
                                                            , parameterNeedToWriteType }

                                                       );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ComplexTypeJson)}.{nameof(GenerateSerializeToString_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        



        #endregion------------------------- GENERATE  SERIALIZE- TO STRING  WRITE  EXPRESSIONS  -------------------------



        #region  -------------------------- GENERATE  DESERIALIZE- FROM STRING  READ  EXPRESSIONS  -------------------------

        static void ReadMembersFromString<T>(ITypeSerializerJson<T> tpSerializer
                                            , ref T tInstance
                                            , string itemJson
                                            , ref int runIndex
                                            , int objectEndIndex
                                          )
        {
            //parse members
            while (runIndex < objectEndIndex) // -1
            {
                //EatMember key , COlon , PassParseValue 
                var memberName = JNP.PassNGetMemberName(itemJson, ref runIndex);

                 
                Validator.ATInvalidOperation(tpSerializer.AccessorT.Members.NotContainsKey(memberName), SGName, nameof(ReadMembersFromString)
                              , " Incorrect Member Key - [{0}]. TypeAccessor of [{1}] do not contain member info with such Key"
                              .Fmt(memberName, tpSerializer.TargetType.FullName));

                // EatValue shouldBe placeds insode handler
                //  Then Parse and return System.object value
                var memberTuple = tpSerializer.AccessorT.Members[memberName];
                var memberSerializer = JsonSerializer.GetTypeSerializer(memberTuple. TypeInf.OriginalType);
                var memberObject = memberSerializer.RaiseDeserializeFromStringBoxedHandler(itemJson, ref runIndex);

                tpSerializer.AccessorT.SetMember(memberName, ref tInstance, memberObject);


                //var currSymbol = stringJson[runIndex];
                if (runIndex < objectEndIndex)
                {
                    JNP.PassComma(itemJson, ref runIndex); //COMMA separator after  members                  
                }               

            }
        }
         


        static T Read_NotNullStruct_FromStringT<T>(ITypeSerializerJson<T> tpSerializer
                                   , string stringJson
                                   , ref int runIndex
                                   , int upperObjectEndIndex = -1
                                   )
        {
            int objectEndIndex = JNP.GetObjectEndIndex(stringJson, ref runIndex);

            JNP.PassObjectSTART(stringJson, ref runIndex);
            
            T tInstance = tpSerializer.LA_CreateDefaultValue.Value(null);

            // READING MEMBERS  FROM STRING
            ReadMembersFromString<T>(tpSerializer
                                    , ref tInstance
                                    , stringJson
                                    , ref runIndex
                                    , objectEndIndex
                                    );

            JNP.PassObjectEND(stringJson, ref runIndex);
            return tInstance;




        }

        

        static T Read_NullableClass_FromStringT<T>( ITypeSerializerJson<T> tpSerializer
                                           , string stringJson
                                           , ref int runIndex
                                           , int upperObjectEndIndex = -1
                                           )
        {
            
            if (upperObjectEndIndex > 0)
                // THIS PATH - FROM REDIRECTION -READED END CLASS TYPE - AND COME TO THIS TYPE
                // START OBJECT CURSL WE NEED NOT TO READ
            {   
                // HERE WE WONT READ START AND END CURLS
              
                T tInstance = tpSerializer.LA_CreateDefaultValue.Value(null);
                
                // READING MEMBERS  FROM STRING
                ReadMembersFromString<T>(tpSerializer
                                        , ref tInstance
                                        , stringJson
                                        , ref runIndex
                                        , upperObjectEndIndex
                                        );
               
                return tInstance;
            }
            else
            {
                //check for NULL VALUE
                if (JNP.PassNullValue(stringJson, ref runIndex))
                {
                    return default(T);// null in Object/Interface
                }

                // OBJECT END INDEX
                int objectEndIndex = JNP.GetObjectEndIndex(stringJson, ref runIndex);
                // START of OBJECT
                JNP.PassObjectSTART(stringJson, ref runIndex);


                // Check Type if found then redirect
                Type redirectType = null;                
                if ( JNP.PassNGetType(stringJson, ref runIndex, out redirectType)  )
                {// redirect really was found

                    var valueTypeEx = redirectType.GetTypeInfoEx();

                    /// locked code: for auto detecting and adding Contracts                     
                    var realTypeSerializer = JsonSerializer.AddKnownType(valueTypeEx);
                    
                  
                    T tInstance = (T)realTypeSerializer.RaiseDeserializeFromStringBoxedHandler(
                                            stringJson
                                            , ref runIndex
                                            , objectEndIndex);  

                    // END of OBJECT
                    JNP.PassObjectEND(stringJson, ref runIndex);
                    return  tInstance;
                }
                else
                { // redirect TYPE NOT FOUND
                  // ITSELVES READING OBJECT 
                  
                    T tInstance = tpSerializer.LA_CreateDefaultValue.Value(null);
                                                           
                    // READING MEMBERS  FROM STRING
                    ReadMembersFromString<T>(tpSerializer
                                            , ref tInstance
                                            , stringJson
                                            , ref runIndex
                                            , objectEndIndex
                                            );
                    // END of OBJECT
                    JNP.PassObjectEND(stringJson, ref runIndex);
                    return tInstance;
                } 
               
            }
             
        }



        static MethodInfo GetDeserializeMethod(TypeInfoEx targetTypeEx)
        {
            if (targetTypeEx.IsCustomStruct && !targetTypeEx.IsNullableType)
            {
                return SGType.GetMethod(nameof(Read_NotNullStruct_FromStringT), BindingFlags.Static | BindingFlags.NonPublic)
                     .MakeGenericMethod(targetTypeEx.OriginalType);
            }
            else
            {
                return SGType.GetMethod(nameof(Read_NullableClass_FromStringT), BindingFlags.Static | BindingFlags.NonPublic)
                    .MakeGenericMethod(targetTypeEx.OriginalType);
            }


        }



        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() 
        {
            try
            {
                ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer( typeof(T)) as ITypeSerializerJson<T>;                
                var typeSerializerInstanceParam = Expression.Constant(tpSerializer);
                var parameterJsonString = Expression.Parameter(Tps.T_string, "jsonString");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndex");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");

                //choose optimal ComplexType Reading method
                var currentType = typeof(T).GetTypeInfoEx();
                var methodDeseriializeFromString = GetDeserializeMethod(currentType);
                                                               
                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>(
                                                           Expression.Call(
                                                              methodDeseriializeFromString
                                                           , typeSerializerInstanceParam
                                                           , parameterJsonString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] {  parameterJsonString
                                                                    ,  parameterRunIndexRef
                                                                    , parameterUpperObjectEndIndex
                                                                   }
                                                           );

            }
            catch (System.Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }            
        }

        #endregion -------------------------- GENERATE  DESERIALIZE- FROM STRING READ EXPRESSIONS  -------------------------

        



#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////
 

        #region  -----------------------------  GENERATE DEBUG SERIALIZETOSTRING WRITE EXPRESSION ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && classes  - with  If(instance != null){}

                // write T typeID - void Write_T_TypeID(Stream ms)-for 1 image: Not nullable struct |  
                //                  void Write_T_TypeID(Stream ms, T data) - for 2 image: nullable structs && classes
                // write members - void TypeSerializer<T>.void WriteMemberToStream<TMember>(ushort typeID, string member, Stream ms, T instanceValue)

                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var parameterNeedWriteTypeAQName = Expression.Parameter(typeof(bool), "needToWriteTypeAQName");                            
                var typeSerializerInstanceParam = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeSeriazerInstance"); //real code  Expression.Const(tpProcessor)

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }
        
        #endregion -----------------------------  GENERATE DEBUG SERIALIZETOSTRING WRITE EXPRESSION ---------------------------


        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ComplexTypeJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");            
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------





#endif



        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        ///  Data Formatting of  this TypeSerializerGenerator - for [ComplexStructure] types. Current Formatting is  Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;
            }
        }


        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }

        


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ComplexTypeJson)} - can't have the Final List of Supported Types");
        }
              

        /// <summary>
        /// Loading default contracts handlers into TYPE SET SERIALIZER - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>        
        public void LoadDefaultTypeSerializers( )
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ComplexTypeJson)} - doesn't have the Final List of Supported Types");
        }
               


        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------




    }
}



