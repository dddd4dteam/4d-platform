﻿using System;
using System.Linq;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    ///Type Serialize Generator for classes which implement IDictionary or IDictionary{,}
    /// </summary>
    internal class SerializeGenerator_DictionaryJson : ITypeSerializerGeneratorJson
    {
        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_DictionaryJson)
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_DictionaryJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_DictionaryJson)
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_DictionaryJson);

        const string KEY = "K";
        const string VALUE = "V";


        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_IDictionary:
        ///     - all classes that based on IDictionary;        
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return typeof(IDictionary).IsAssignableFrom(type);            
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        // Dictionary Key can't be null - WriteDictKey - doesn't contains null check

        //   Dictionary        [ NeedKey (+)    (impossible) ]    [ NeedValue (+)     NullVal (+)  ]  
        //   Dictionary        [ NeedNotKey(-)  (impossible) ]    [ NeedValue (+)     NullVal (+)  ]  
        //   Dictionary        [ NeedKey (+)    (impossible) ]    [ NeedNotValue(-)   NullVal (-)  ]  
        //   Dictionary        [ NeedNotKey(-)  (impossible) ]    [ NeedNotValue(-)   NullVal (-)  ]   


        static void WriteDictionary_ToString<T,TKey,TValue>(
                                ITypeSerializerJson<T> tpSerializer
                                , TextWriter writer
                                , T data
                                , bool needToWriteType = false)
        {
            // Dictioonary IS NULL
            if (data == null)
            {
                JNP.Write_Null(writer);
                return;
            }


            //var dictionary = (IDictionary<TKey, TValue>)instanceValue;

            var dictInstance = data as IDictionary<TKey, TValue>;
            

            JNP.Write_ArrayStart(writer);
            var i = 0;
            foreach (var dictItem in  dictInstance)
            {
                JNP.Write_FormatReturn(writer);

                JNP.Write_ObjectStart(writer, true);

                var keyTypeID = tpSerializer.RuntimeIDents.Arg1TypeID;
                var valueTypeID = tpSerializer.RuntimeIDents.Arg2TypeID;

                //write key
                JNP.Write_MemberName(writer, KEY);
                tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                                    keyTypeID
                                    , writer
                                    , dictItem.Key);

                JNP.Write_Comma(writer);
                //write value
                JNP.Write_MemberName(writer, VALUE);
                tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                                    valueTypeID
                                    , writer
                                    , dictItem.Value);

                JNP.Write_ObjectEnd(writer );

                // after element COMMA
                if (i < (dictInstance.Count - 1))
                {
                    JNP.Write_Comma(writer);
                }               
                i++;
            }
 
            JNP.Write_ArrayEnd(writer);

        }



        static T ReadDictionary_FromString<T,TKey,TValue>(
                                ITypeSerializerJson<T> tpSerializer
                                , string stringJson
                                , ref int runIndex
                                , int upperObjectEndIndex = -1
                                 )
        {
            // Dictionary is NULL itselves
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {
                return default(T);
            }

            // var tDictInstance = 
            var diction = (IDictionary)tpSerializer.LA_CreateDefaultValue.Value(null); 

            var keyTypeID = tpSerializer.RuntimeIDents.Arg1TypeID;
            var valueTypeID = tpSerializer.RuntimeIDents.Arg2TypeID;
            var keySerializer = JsonSerializer.GetTypeSerializer(keyTypeID);           
            var valueSerializer = JsonSerializer.GetTypeSerializer(valueTypeID);

            int dictEndIndex = JNP.GetArrayEndIndex(stringJson, ref runIndex);
                       
            JNP.PassArraySTART(stringJson, ref runIndex);

            //parse members
            while (runIndex < dictEndIndex) // - 1
            {
                JNP.PassObjectSTART(stringJson, ref runIndex);

                //Item KEY                  
                var keyFld = JNP.PassNGetMemberName(stringJson, ref runIndex);
                
                var keyObject = keySerializer
                    .RaiseDeserializeFromStringBoxedHandler(stringJson, ref runIndex);

                //Item VALUE 
                var valueFld = JNP.PassNGetMemberName(stringJson, ref runIndex);
                
                var valueObject = valueSerializer
                    .RaiseDeserializeFromStringBoxedHandler(stringJson, ref runIndex);

                //add to dictionary element value. Value can be null
                diction.Add(keyObject, valueObject);

                JNP.PassObjectEND(stringJson, ref runIndex);

                // COMMA for next element
                //var currSymbol = stringJson[runIndex];
                if (runIndex < dictEndIndex)
                {
                    JNP.PassComma(stringJson, ref runIndex); //COMMA separator after  members                  
                }
            }

            JNP.PassArrayEND(stringJson, ref runIndex);

            return (T)diction;
        }




        static MethodInfo Get_WriteDictionaryMethod<T>(ITypeSerializerJson<T> tpSerializer)
        {
            return SGType.GetMethod(nameof(WriteDictionary_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                                                     .MakeGenericMethod(tpSerializer.TargetType
                                                                       ,tpSerializer.TargetTypeEx.Arg1Type
                                                                       , tpSerializer.TargetTypeEx.Arg2Type
                                                                       );
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(Get_WriteDictionaryMethod)}() - such List Type  is not supported by this serializer [{tpSerializer.TargetType.FullName}] ");
        }


        static MethodInfo Get_ReadDictionaryMethod<T>(ITypeSerializerJson<T> tpSerializer)
        {
            return SGType.GetMethod(nameof(ReadDictionary_FromString), BindingFlags.Static | BindingFlags.NonPublic)
                                                    .MakeGenericMethod(tpSerializer.TargetType
                                                    , tpSerializer.TargetTypeEx.Arg1Type
                                                    , tpSerializer.TargetTypeEx.Arg2Type
                                                    );

            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(Get_ReadDictionaryMethod)}() - such List Type  is not supported by this serializer [{tpSerializer.TargetType.FullName}] ");
        }





        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(
                                                        ITypeSerializerJson<T> tpSerializer) 
        {
            try
            {
                //algorithm:
                // void WriteIListToString<T>(TypeSerializer<T>  typeSerializer,TextWriter writer, T data, bool needToWriteType = false)                
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var parameterNeedToWriteType = Expression.Parameter(typeof(bool), "needToWriteType");


                //choose optimal IList Writing method
                MethodInfo methodWrite_IDict_ToString = Get_WriteDictionaryMethod(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStringT<T>>
                                                        (
                                                         Expression.Call
                                                         (
                                                           methodWrite_IDict_ToString
                                                         , typeSerializerInstance
                                                         , parameterWriter
                                                         , parameterInputData
                                                         , parameterNeedToWriteType
                                                         )
                                                        , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                        , new[] {
                                                            parameterWriter
                                                            , parameterInputData
                                                            , parameterNeedToWriteType }

                                                        );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }

        }

        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>()
        {

            try
            {
                //algorithm:
                //  T ReadIListFromString<T>(TypeSerializer<T> typeSerializer, string listJson, ref int runIndex)
                ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer(typeof(T)) as ITypeSerializerJson<T>;
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterJsonString = Expression.Parameter(Tps.T_string, "jsonString");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");


                //choose optimal IList Reading method
                MethodInfo methodRead_Dict_FromString = Get_ReadDictionaryMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>(
                                                           Expression.Call(
                                                             methodRead_Dict_FromString
                                                           , typeSerializerInstance
                                                           , parameterJsonString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] {  parameterJsonString
                                                                    ,  parameterRunIndexRef
                                                                    , parameterUpperObjectEndIndex
                                                                   }
                                                           );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }



        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamT<T> > GenerateSerialize_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            //algorithm:
            // void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

            try
            {

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                var methodWrite_IDictionary_ToStream = Get_WriteDictionaryMethod(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStreamT<T>>(
                                                       Expression.Call(
                                                         methodWrite_IDictionary_ToStream
                                                       , typeProcessorInstance
                                                       , parameterStream
                                                       , parameterInputData
                                                       )
                                                       , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                       , new[] { parameterStream, parameterInputData }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_DictionaryJson)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        ///  Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            // algorithm:          
            // T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

            try
            {
                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);
                
                var methodRead_IDictionary_FromSteam = Get_ReadDictionaryMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStreamT<T>>(
                                                       Expression.Call(
                                                         methodRead_IDictionary_FromSteam
                                                       , typeProcessorInstance
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                       , new[] { parameterStream, parameterReadedTypeID }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_DictionaryJson)}.{nameof(GenerateDeserialize_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }

          
        }




        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                // void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");


                 var methodWrite_IDictionary_ToStream = Get_WriteDictionaryMethod(tpSerializer);
                
                return Expression.Lambda<SDJson.SerializeToStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodWrite_IDictionary_ToStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterInputData
                                                       )
                                                       , parameterStream
                                                       , parameterInputData,
                                                         parameterTypeProcessor
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_DictionaryJson)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {                
                //algorithm:          
                //  T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");


                var methodRead_IDictionary_FromSteam = Get_ReadDictionaryMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodRead_IDictionary_FromSteam
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       , parameterTypeProcessor
                                                       );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_DictionaryJson)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }

        }



        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------



        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            return null;
        }




        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------




#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }

        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [IDictionary] types. Current Formatting is Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;//  throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_DictionaryJson)} - can't have the Final List of Supported Types");
        }

        /// <summary>
        /// Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_DictionaryJson)} - can't have the Final List of Supported Types");
        }


        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}


#region ---------------------------------------- GARBAGE ------------------------------------

////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T); //just the same as return null  for reference types
//    }
//}

////read dictionary Length 
//int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length
////Creating new Instance
//T result = typeSerializer.LA_CreateDefaultValue.Value(new object[] { collectionLength });
////now get interface IDictionary<TKey, TValue> instance
//var dictionary = (IDictionary<TKey, TValue>)result;


////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T); //just the same as return null  for reference types
//    }
//}

////read dictionary Length 
//int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length
////Creating new Instance
//T result = typeSerializer.LA_CreateDefaultValue.Value(new object[] { collectionLength });
////now get interface IDictionary<TKey, TValue> instance
//var dictionary = (IDictionary<TKey, TValue>)result;




#region  ---------------------------------OLD   READ / WRITE IDICTIONARY HANDLERS ------------------------------------------

//public static void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write Length
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    IDictionary<TKey, TValue> dictionaryInstance = (IDictionary<TKey, TValue>)instanceValue;

//    // write IDictionary length - List Length
//    BinaryReaderWriter.Write(stream, dictionaryInstance.Count);

//    // write all IList elements to Stream
//    foreach (var KeyValueItem in dictionaryInstance)
//    {
//        // write Item.Key Value
//        ushort elementKeyTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Key);
//        BinaryReaderWriter.Write(stream, elementKeyTypeID);

//        //null Key element -hypotetically
//        Validator.AssertTrue<InvalidOperationException>(elementKeyTypeID == 0,
//                                                           "Writing Dictionary Key typeID cannot be null i.e. == 0 value");

//        // write element  body
//        typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementKeyTypeID, stream, KeyValueItem.Key);

//        // write Item.Value Value
//        ushort elementValueTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Value);
//        BinaryReaderWriter.Write(stream, elementValueTypeID);
//        if (elementValueTypeID != 0)
//        {
//            // write element  body
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementValueTypeID, stream, KeyValueItem.Value);

//        }

//    }


//}


//public static T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //read dictionary Length and creating It
//    int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length

//    //now create IDictionary instance
//    var dictionary = (IDictionary<TKey, TValue>)TypeActivator.CreateInstanceT<T>(TypeActivator.DefaultCtorSearchBinding, collectionLength);// //typeProcessor.AccessorT.CreateInstanceT(collectionLength);


//    for (int i = 0; i < collectionLength; i++)
//    {
//        //read Key
//        //var KEY_val = RaiseMemberDeserializeSwitch<TKey>(Arg1TypeID, stream, false);
//        //reading element RealTypeID
//        ushort elementKeyRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        Validator.AssertTrue<InvalidOperationException>(elementKeyRealTypeId == 0,
//                                                           "Reading Dictionary Key typeID cannot be null i.e. == 0 value");


//        //read element Key value
//        var KEY_val = typeProcessor.RaiseElementDeserializeFromStreamTHandler<TKey>(elementKeyRealTypeId, stream);  //true


//        //read Value
//        //var VALUE_val = RaiseMemberDeserializeSwitch<TValue>(Arg2TypeID, stream, false);
//        ushort elementValueRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementValueRealTypeId == 0)
//        {
//            dictionary.Add(KEY_val, (TValue)(object)null);
//        }
//        else
//        {
//            //read element Value value
//            var VALUE_val = typeProcessor.RaiseElementDeserializeFromStreamTHandler<TValue>(elementValueRealTypeId, stream);//true
//            dictionary.Add(KEY_val, VALUE_val);
//        }

//    }


//    return (T)dictionary;
//}


#endregion   ---------------------------------OLD   READ / WRITE IDICTIONARY HANDLERS ------------------------------------------

#endregion---------------------------------------- GARBAGE ------------------------------------
