﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;

using DDDD.Core.IO;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using System.Reflection;
using DDDD.Core.IO.Text;

namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// Type Serialize Generator of enum-s.
    /// </summary>
    internal class SerializeGenerator_EnumJson : ITypeSerializerGeneratorJson
    {

      
        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_EnumJson)
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_EnumJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_EnumJson)
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_EnumJson);



        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        ///   Can Handle  criterion for SerializeGenerator_Enum:
        ///     - type is enum;         
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
           return type.IsEnum;            
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------
        

        public static void Write_NullableEnum<T>(
                                ITypeSerializerJson<T> typeSerializer
                                , TextWriter writer
                                , T value
                                , bool needToWriteType = false
                                )
        {
            if (value.IsNull())
            {
                writer.Write(Sym.NULL);
            }
            else writer.Write(Sym.QUOTE +  value.S() + Sym.QUOTE);
        }

        public static void Write_NotNullableEnum<T>(
                                 ITypeSerializerJson<T> tpSerializer
                                , TextWriter writer
                                , T value
                                , bool needToWriteType = false
                                )
        {           
            writer.Write(Sym.QUOTE + value.S() + Sym.QUOTE);
        }


        static MethodInfo Get_WriteEnumMethod<T>(ITypeSerializerJson<T> tpSerializer )
        {
            MethodInfo methodWriteEnum = null;
            //1 nullable Enum type      - primitive type already exist   
            if (tpSerializer.TargetTypeEx.IsNullableType)
            {
                methodWriteEnum = SGType.GetMethod(nameof(Write_NullableEnum))
                                        .MakeGenericMethod(tpSerializer.TargetType);
            }
            else
            {
                //2 not nullable Enum type  - primitive type already exist

                methodWriteEnum = SGType.GetMethod(nameof(Write_NotNullableEnum))
                                       .MakeGenericMethod(tpSerializer.TargetType);
                

            }

            return methodWriteEnum;
        }
        

        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>
                                                    ( ITypeSerializerJson<T> tpSerializer) 
        {

            try
            {
                //algorithm:
                // void WriteEnumToString<T>(TypeSerializer<T>  typeSerializer,TextWriter writer, T data, bool needToWriteType = false)                
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var parameterNeedToWriteType = Expression.Parameter(typeof(bool), "needToWriteType");
                
                //choose optimal IList Writing method
                MethodInfo methodWrite_Enum_ToString = Get_WriteEnumMethod<T>(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStringT<T>>
                                                        (
                                                         Expression.Call
                                                         (
                                                           methodWrite_Enum_ToString
                                                         , typeSerializerInstance
                                                         , parameterWriter
                                                         , parameterInputData
                                                         , parameterNeedToWriteType
                                                         )
                                                        , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                        , new[] { parameterWriter, parameterInputData, parameterNeedToWriteType }

                                                        );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }






        public static T Read_NotNullableEnum<T>(
                                ITypeSerializerJson<T> tpSerializer
                                , string stringJson
                                , ref int runIndex
                                , int upperObjectEndIndex = -1
                                )
        {

            var enumString = JNP.PassNGetStringContentOnlyValue(stringJson, ref runIndex);            
            return (T)Enum. Parse(
                tpSerializer.TargetType
                ,enumString);
            
        }

        public static T Read_NullableEnum<T>(
                                             ITypeSerializerJson<T> tpSerializer
                                             , string stringJson
                                             , ref int runIndex
                                             , int upperObjectEndIndex = -1
                                            ) //NullableEnum

        {

            //check for NULL VALUE
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {
                //Json1.PassObjectEND(stringJson, ref runIndex);
                return default(T);// null in Object/Interface
            }

            var enumString = JNP.PassNGetStringContentOnlyValue(stringJson, ref runIndex);            
                return   (T)Enum.Parse(
                tpSerializer.TargetTypeEx.WorkingType
                , enumString);
            
        }
               
        static MethodInfo Get_ReadEnumMethod<T>(ITypeSerializerJson<T> tpSerializer)
        {
            if (tpSerializer.TargetType.IsNeedToSureInDataType())
            {
                return SGType.GetMethod(nameof(Read_NullableEnum) )
                             .MakeGenericMethod(tpSerializer.TargetType);
            }
            else
            {
                return SGType.GetMethod(nameof(Read_NotNullableEnum) )
                             .MakeGenericMethod(tpSerializer.TargetType);
            }

            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(Get_ReadEnumMethod)}() - such Enum Type  is not supported by this serializer [{tpSerializer.TargetType.FullName}] ");
        }
               

        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() 
        {
            try
            {
                //algorithm:
                //  T ReadIListFromString<T>(TypeSerializer<T> typeSerializer, string listJson, ref int runIndex)
                ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer(typeof(T)) as ITypeSerializerJson<T>;
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterJsonString = Expression.Parameter(Tps.T_string, "jsonString");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");


                //choose optimal Enum Reading method
                MethodInfo methodRead_Enum_FromString = Get_ReadEnumMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>(
                                                           Expression.Call(
                                                             methodRead_Enum_FromString
                                                           , typeSerializerInstance
                                                           , parameterJsonString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] {  parameterJsonString
                                                                    ,  parameterRunIndexRef
                                                                    , parameterUpperObjectEndIndex
                                                                    }
                                                           );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }
                      


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
               
        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpressionDbg)}() : Message -[ ]  " );
        }

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_EnumJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------


#endif



        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [Enums] types. Current Formatting is Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;
            }
        }

        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers( )
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_EnumJson)} - can't have the Final List of Supported Types");
        }

        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_EnumJson)} - can't have the Final List of Supported Types");
        }


        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}
