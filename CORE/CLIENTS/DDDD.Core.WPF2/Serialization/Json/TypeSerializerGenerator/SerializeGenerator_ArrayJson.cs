﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.Core.IO;
using DDDD.Core.Extensions;
using System.Collections;
using DDDD.Core.Reflection;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Type Serialize Generator of Arrays. Arrays can be of different Rank.
    /// </summary>
    internal class SerializeGenerator_ArrayJson : ITypeSerializerGeneratorJson
    {

        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_ArrayJson)
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_ArrayJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_ArrayJson )
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_ArrayJson);






        /// <summary>
        ///  Can Handle [type] criterion for SerializeGenerator_Array.
        ///   Contract: type.IsArray 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return type.IsArray;
        }






        #region ----------------------------------- WRITE ARRAYS of 1 Dimension ----------------------------------- 


        // 1 way -  NeedNotToSureInItemType -( NullValueEnabled(sealed classes) + NullValueNotEnabled(structs) )


        static void WriteArray_ToString<T>(
                                ITypeSerializerJson<T> tpSerializer
                                , TextWriter writer
                                , T data
                                , bool needToWriteType = false)
        {
            // ARRAY IS NULL
            if (data == null)
            {
                JNP.Write_Null(writer);
                return;
            }

            //var elemtType = tpSerializer.TargetTypeEx.ArrayElementType;
            var listInstance = data as IList;

            JNP.Write_ArrayStart(writer);
            for (int i = 0; i < listInstance.Count; i++)
            {  
                //write element  data                              
                tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                               tpSerializer.RuntimeIDents.ArrayElementTypeID
                             , writer, listInstance[i]);
                if (i != listInstance.Count - 1)
                {
                    JNP.Write_Comma(writer);
                }
            }
            JNP.Write_ArrayEnd(writer);

        }



        static T ReadArray_FromString<T>(
                                ITypeSerializerJson<T> tpSerializer
                                , string stringJson
                                , ref int runIndex
                                , int upperObjectEndIndex = -1
                                 ) 
        {
            // Array is NULL 
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {
                return default(T);
            }

            var elementType = tpSerializer.TargetTypeEx.ArrayElementType;
            var listType = Tps.T_ListGen.MakeGenericType( elementType);
            var arrayItemSerializer = JsonSerializer.GetTypeSerializer(
                tpSerializer.RuntimeIDents.ArrayElementTypeID);
           

            var listForArray = (IList)TypeActivator.CreateInstanceBoxed(listType);            
            int listEndIndex = JNP.GetArrayEndIndex(stringJson, ref runIndex);

            JNP.PassArraySTART(stringJson, ref runIndex);

            //parse members
            while (runIndex < listEndIndex) // - 1
            {
                //Item value                  
               
                var listItemObject = arrayItemSerializer
                    .RaiseDeserializeFromStringBoxedHandler(stringJson, ref runIndex);

                //read element value
                listForArray.Add(listItemObject);
               
                //var currSymbol = stringJson[runIndex];
                if (runIndex < listEndIndex)
                {
                    JNP.PassComma(stringJson, ref runIndex); //COMMA separator after  members                  
                }
            }

            JNP.PassArrayEND(stringJson, ref runIndex);

            //TypeActivator.PrecompileAsNewArrayExpressionT()
            var intArg = (new int[1] { listForArray.Count}).ToObjectArray();

            var arrayRes = tpSerializer.LA_CreateDefaultValue.Value(new object[] { listForArray.Count });            
            listForArray.CopyTo(arrayRes as Array,0);
            listForArray.Clear();
            
            return   arrayRes;
        }





        static MethodInfo Get_WriteArrayMethod<T>(ITypeSerializerJson<T> tpserializer)
        {
            return SGType.GetMethod(nameof(WriteArray_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                   .MakeGenericMethod(tpserializer.TargetType);
                   //, tpserializer.TargetTypeEx.ArrayElementType);
             

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(Get_WriteArrayMethod)}() - such Array Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");

        }

        #endregion ----------------------------------- WRITE ARRAYS of 1-4 Dimension -----------------------------------


        #region ----------------------------------- READ ARRAYS of 1-4 Dimension ----------------------------------- 


       



        static MethodInfo Get_ReadArrayMethod<T>(ITypeSerializerJson<T> tpserializer)
        {
            return SGType.GetMethod(nameof(ReadArray_FromString), BindingFlags.Static | BindingFlags.NonPublic)
                  .MakeGenericMethod(tpserializer.TargetType);
                                    //, tpserializer.TargetTypeEx.ArrayElementType);


            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(Get_ReadArrayMethod)}() - such Array Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");


        }

        #endregion ----------------------------------- READ ARRAYS of 1-4 Dimension ----------------------------------- 



        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
               

        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(
                                 ITypeSerializerJson<T> tpSerializer) 
        {

            try
            {
                //algorithm:
                // void WriteArrayToString<T>(TypeSerializer<T>  typeSerializer,TextWriter writer, T data, bool needToWriteType = false)                
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var parameterNeedToWriteType = Expression.Parameter(typeof(bool), "needToWriteType");


                //choose optimal IList Writing method
                MethodInfo methodWrite_Array_ToString = Get_WriteArrayMethod(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStringT<T>>
                                                        (
                                                         Expression.Call
                                                         (
                                                           methodWrite_Array_ToString
                                                         , typeSerializerInstance
                                                         , parameterWriter
                                                         , parameterInputData
                                                         , parameterNeedToWriteType
                                                         )
                                                        , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                        , new[] {
                                                            parameterWriter
                                                            , parameterInputData
                                                            , parameterNeedToWriteType }

                                                        );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }

        }


        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() //ITypeSerializerJson<T> tpSerializer
        {

            try
            {
                //algorithm:
                //  T ReadIListFromString<T>(TypeSerializer<T> typeSerializer, string listJson, ref int runIndex)
                ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer(typeof(T)) as ITypeSerializerJson<T>;
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterJsonString = Expression.Parameter(Tps.T_string, "jsonString");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");


                //choose optimal IList Reading method
                MethodInfo methodRead_Array_FromString = Get_ReadArrayMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>(
                                                           Expression.Call(
                                                             methodRead_Array_FromString
                                                           , typeSerializerInstance
                                                           , parameterJsonString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] {  parameterJsonString
                                                                    ,  parameterRunIndexRef
                                                                    , parameterUpperObjectEndIndex
                                                                   }
                                                           );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }

        }

        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////
       
        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                //void WriteArrayToStream<T>(TypeSerializer<T> typeProcessor,  Stream stream, T instanceValue)            

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");

                var methodWriteArrayToStream = Get_WriteArrayMethod(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStreamTDbg<T>>(
                                                 Expression.Call(

                                                 methodWriteArrayToStream
                                                , parameterTypeProcessor
                                                , parameterStream
                                                , parameterInputData
                                                ),
                                                 parameterStream
                                                , parameterInputData
                                                , parameterTypeProcessor
                                                );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                // T ReadArrayFromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");
                

                var methodReadArrayFromStream = Get_ReadArrayMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodReadArrayFromStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       , parameterTypeProcessor
                                                       );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }

        }





        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            return null;
        }



        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ArrayJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------



#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [Array] types. Current Formatting is Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;//
            }
        }


        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList() 
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ArrayJson)} - can't have the Final List of Supported Types");
        }


        /// <summary>
        /// Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer. 
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers( )
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ArrayJson)} - can't have the Final List of Supported Types");
        }

       

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------




    }

}



#region ------------------------------- GARBAGE ----------------------------------------

#region  --------------------------------- OLD  WRITE ARRAYS HANDLERS ------------------------------------------

//static void WriteArray_NeedToSureInItemType_ToStream<T, TMember>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //Write typeID
//    //write rank
//    //write[rankIndex]= Lengths

//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var arrayInstance = instanceValue as Array;
//    int arrayRank = arrayInstance.Rank;

//    // write rank - number of array dimensions
//    BinaryReaderWriter.Write(stream, arrayRank);

//    // write i-th dimension length
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        BinaryReaderWriter.Write(stream, arrayInstance.GetLength(i)); 
//    }


//    //write all array elements to Stream
//    foreach (TMember element in arrayInstance)
//    {
//        ushort elementTypeID = typeSerializer.GetElementTypeID(element);

//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.Write(stream, elementTypeID);

//        if (elementTypeID == 0) //null data
//        {
//            continue;
//        }
//        else
//        {
//            //write element  data 
//            typeSerializer.RaiseElementSerializeToStreamTHandler(elementTypeID, stream, element);///RaiseElementSerializeSwitch
//        }

//    }
//}

//static void WriteArray_NeedNotToSureInItemType_ToStream<T, TMember>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //Write typeID
//    //write rank
//    //write[rankIndex]= Lengths

//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var arrayInstance = instanceValue as Array;
//    int arrayRank = arrayInstance.Rank;

//    // write rank - number of array dimensions
//    BinaryReaderWriter.Write(stream, arrayRank);

//    // write i-th dimension length
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        BinaryReaderWriter.Write(stream, arrayInstance.GetLength(i));
//    }

//    //write all array elements to Stream
//    foreach (TMember element in arrayInstance)
//    {
//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        //BinaryReaderWriter.Write(stream, typeSerializer.ArrayElementTypeID); // elementTypeID

//        //write element  data 
//        typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.ArrayElementTypeID, stream, element); ///RaiseElementSerializeSwitch            
//    }
//}

#endregion --------------------------------- OLD  WRITE ARRAYS HANDLERS ------------------------------------------
    
//public static T ReadArray_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //reading array dimensions and creating It
//    int arrayRank = (int)BinaryReaderWriter.Read_int32(stream); //read rank
//    int[] dimensionsLengths = new int[arrayRank];
//    int totalLength = 0;
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        dimensionsLengths[i] = (int)BinaryReaderWriter.Read_int32(stream);//read dimension-length
//        if (totalLength == 0)
//        {
//            totalLength = dimensionsLengths[i];
//        }
//        else
//        {
//            totalLength *= dimensionsLengths[i];
//        }
//    }

//    //now create Array instance
//    Array array = typeSerializer.LA_CreateDefaultValue.Value(dimensionsLengths.ToObjectArray()) as Array;
//    //            Array.CreateInstance( typeSerializer.AccessorT.TAccessEx. ArrayElementType, dimensionsLengths);

//    int[] indices = new int[arrayRank];
//    int itemIndex = 0;

//    for (int i = 0; i < totalLength; i++)
//    {
//        //reading element RealTypeID : for primitive or CustomContract -no difference
//        ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementRealTypeId == 0)
//        { itemIndex++; }     // null value element   
//        else
//        {
//            //read element value
//            var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, true);//false
//            CalculateIndices(itemIndex++, indices, dimensionsLengths);
//            array.SetValue(item, indices);
//        }
//    }


//    return (T)(object)array;

//}




//private static void CalculateIndices(int index,
//                                   int[] indices,
//                                   int[] dimensionsLengths)
//{
//    int rank = indices.Length;
//    bool continueCalculation = true;
//    int remainder = index;
//    for (int i = 0; i < rank; ++i)
//    {
//        if (continueCalculation)
//        {
//            int capacity = CalculateDimensionCapacity(i, dimensionsLengths);
//            indices[i] = remainder / capacity;
//            remainder %= capacity;
//            if (remainder == 0)
//            {
//                continueCalculation = false;
//            }
//        }
//        else
//        {
//            indices[i] = 0;
//        }
//    }
//}


//private static int CalculateDimensionCapacity(int dimension, int[] dimensionsLengths)
//{
//    int capacity = 1;
//    for (int i = dimension + 1; i < dimensionsLengths.Length; ++i)
//    {
//        capacity *= dimensionsLengths[i];
//    }
//    return capacity;
//}

//CalculateIndices(itemIndex++, indices, array.GetDimensionsLengths()); // set current indices

//NeedNotToSureInItemType
//public static T ReadArray_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //reading array dimensions and creating It
//    int arrayRank = (int)BinaryReaderWriter.Read_int32(stream); //read rank
//    int[] dimensionsLengths = new int[arrayRank];
//    int totalLength = 0;
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        dimensionsLengths[i] = (int)BinaryReaderWriter.Read_int32(stream);//read dimension-length
//        if (totalLength == 0)
//        {
//            totalLength = dimensionsLengths[i];
//        }
//        else
//        {
//            totalLength *= dimensionsLengths[i];
//        }
//    }

//    //now create Array instance
//    Array array = typeSerializer.LA_CreateDefaultValue.Value(dimensionsLengths.ToObjectArray()) as Array;
//    //            Array.CreateInstance( typeSerializer.AccessorT.TAccessEx. ArrayElementType, dimensionsLengths);

//    int[] indices = new int[arrayRank];
//    int itemIndex = 0;

//    for (int i = 0; i < totalLength; i++)
//    {
//        //reading element RealTypeID : for primitive or CustomContract -no difference
//        ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementRealTypeId == 0)
//        { itemIndex++; }     // null value element   
//        else
//        {
//            //read element value
//            var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, true);//false
//            CalculateIndices(itemIndex++, indices, dimensionsLengths);
//            array.SetValue(item, indices);
//        }
//    }


//    return (T)(object)array;

//}


// NeedNot Sure In ITEM TYPE
//for (int i = 0; i<array.Length; i++)
//{                
//    //read element value
//    var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeSerializer.ArrayElementTypeID, stream, true);//false
//    CalculateIndices(itemIndex++, indices, array.GetDimensionsLengths()); // set current indices
//    array.SetValue(item, indices);                
//}


#endregion ------------------------------- GARBAGE ----------------------------------------