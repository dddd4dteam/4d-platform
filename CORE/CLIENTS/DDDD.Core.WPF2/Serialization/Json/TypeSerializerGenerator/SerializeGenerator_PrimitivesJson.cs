﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

namespace DDDD.Core.Serialization.Json
{


    /// <summary>
    /// Serialize Generator for Primitive types. All of them are [DefaultContract]. The Primitives are the following types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// </summary>
    internal class SerializeGenerator_PrimitivesJson : ITypeSerializerGeneratorJson
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        ///   Can Handle  criterion for SerializeGenerator_Primitives:           
        ///    Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
        ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
        ///    Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
        ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
        ///    Classes:               string, byte[], Uri, BitArray
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return type.Is4DPrimitiveType();
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        public Expression<SDJson.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializerJson<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        public Expression<SDJson.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializerJson<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }



        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer) //ITypeSerializerJson<T> tpSerializer
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }


        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() //ITypeSerializerJson<T> tpSerializer
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }


        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_PrimitivesJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }



        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_PrimitivesJson)}.{nameof(GenerateSerializeToString_WriteExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }



        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_PrimitivesJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------


#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// <para/> So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        ///  CanLoadDefaultContractHandlers should be true. 
        /// <para/> If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [4DPrimitive ] types. Current Formatting is  Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;        
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {   return Tps.Primitives; //BinaryReaderWriter.GetRegisteredPrimitives();
        }


        /// <summary>
        /// Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        public void LoadDefaultTypeSerializers( )
        {

            ////NOT NULLABLE PrimitiveReaderWriter    
            JsonSerializer.AddKnownType<int>().SetSerializeToStringTHandlers( JsonReaderWriter.WriteT_int32, JsonReaderWriter.ReadT_int32);
            JsonSerializer.AddKnownType<uint>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_uint32, JsonReaderWriter.ReadT_uint32);
            JsonSerializer.AddKnownType<long>().SetSerializeToStringTHandlers (JsonReaderWriter.WriteT_int64, JsonReaderWriter.ReadT_int64);
            JsonSerializer.AddKnownType<ulong>().SetSerializeToStringTHandlers (JsonReaderWriter.WriteT_uint64, JsonReaderWriter.ReadT_uint64);
            JsonSerializer.AddKnownType<short>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_int16, JsonReaderWriter.ReadT_int16);
            JsonSerializer.AddKnownType<ushort>().SetSerializeToStringTHandlers (JsonReaderWriter.WriteT_uint16, JsonReaderWriter.ReadT_uint16);
            JsonSerializer.AddKnownType<byte>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_byte, JsonReaderWriter.ReadT_byte);
            JsonSerializer.AddKnownType<sbyte>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_sbyte, JsonReaderWriter.ReadT_sbyte);
            JsonSerializer.AddKnownType<char>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_char, JsonReaderWriter.ReadT_char);
            JsonSerializer.AddKnownType<bool>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_bool, JsonReaderWriter.ReadT_bool);
            JsonSerializer.AddKnownType<DateTime>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_DateTime, JsonReaderWriter.ReadT_DateTime);
            JsonSerializer.AddKnownType<TimeSpan>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_TimeSpan, JsonReaderWriter.ReadT_TimeSpan);
            JsonSerializer.AddKnownType<DateTimeOffset>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_DateTimeOffset, JsonReaderWriter.ReadT_DateTimeOffset);
            JsonSerializer.AddKnownType<Guid>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_Guid, JsonReaderWriter.ReadT_Guid);
            JsonSerializer.AddKnownType<float>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_float, JsonReaderWriter.ReadT_float);
            JsonSerializer.AddKnownType<double>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_double, JsonReaderWriter.ReadT_double);
            JsonSerializer.AddKnownType<decimal>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_decimal, JsonReaderWriter.ReadT_decimal);



            JsonSerializer.AddKnownType<int?>().SetSerializeToStringTHandlers(  JsonReaderWriter.WriteT_int32_nullable, JsonReaderWriter.ReadT_int32_nullable);
            JsonSerializer.AddKnownType<uint?>().SetSerializeToStringTHandlers( JsonReaderWriter.WriteT_uint32_nullable, JsonReaderWriter.ReadT_uint32_nullable);
            JsonSerializer.AddKnownType<long?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_int64_nullable, JsonReaderWriter.ReadT_int64_nullable);
            JsonSerializer.AddKnownType<ulong?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_uint64_nullable, JsonReaderWriter.ReadT_uint64_nullable);
            JsonSerializer.AddKnownType<short?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_int16_nullable, JsonReaderWriter.ReadT_int16_nullable);
            JsonSerializer.AddKnownType<ushort?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_uint16_nullable, JsonReaderWriter.ReadT_uint16_nullable);
            JsonSerializer.AddKnownType<byte?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_byte_nullable, JsonReaderWriter.ReadT_byte_nullable);
           
            JsonSerializer.AddKnownType<sbyte?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_sbyte_nullable, JsonReaderWriter.ReadT_sbyte_nullable);
            JsonSerializer.AddKnownType<char?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_char_nullable, JsonReaderWriter.ReadT_char_nullable);
            JsonSerializer.AddKnownType<bool?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_bool_nullable, JsonReaderWriter.ReadT_bool_nullable);
            JsonSerializer.AddKnownType<DateTime?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_DateTime_nullable, JsonReaderWriter.ReadT_DateTime_nullable);
            JsonSerializer.AddKnownType<TimeSpan?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_TimeSpan_nullable, JsonReaderWriter.ReadT_TimeSpan_nullable);
            JsonSerializer.AddKnownType<Guid?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_Guid_nullable, JsonReaderWriter.ReadT_Guid_nullable);
            JsonSerializer.AddKnownType<float?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_float_nullable, JsonReaderWriter.ReadT_float_nullable);
            JsonSerializer.AddKnownType<double?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_double_nullable, JsonReaderWriter.ReadT_double_nullable);
            JsonSerializer.AddKnownType<decimal?>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_decimal_nullable, JsonReaderWriter.ReadT_decimal_nullable);
            

            ////primitive classes : string , byte[]
            JsonSerializer.AddKnownType<string>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_string, JsonReaderWriter.ReadT_string);
            JsonSerializer.AddKnownType<byte[]>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_byteArray, JsonReaderWriter.ReadT_byteArray);
            JsonSerializer.AddKnownType<Uri>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_Uri, JsonReaderWriter.ReadT_Uri);
            JsonSerializer.AddKnownType<BitArray>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_BitArray, JsonReaderWriter.ReadT_BitArray);

            // 4 Reflection  Classes - TypeInfo
            JsonSerializer.AddKnownType<TypeInfoEx>().SetSerializeToStringTHandlers(JsonReaderWriter.WriteT_TypeInfoEx, JsonReaderWriter.ReadT_TypeInfoEx);


        }
        
        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------
        

    }

}


