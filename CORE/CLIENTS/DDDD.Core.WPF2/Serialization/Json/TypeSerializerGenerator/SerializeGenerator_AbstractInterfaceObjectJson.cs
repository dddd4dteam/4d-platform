﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;

using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;
using System.ComponentModel;
using DDDD.Core.IO.Text;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Type Serialize Generator for interfaces types and object type.
    /// </summary>
    internal class SerializeGenerator_AbstractInterfaceObjectJson : ITypeSerializerGeneratorJson
    {
        
        private readonly static object locker = new object();

        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_AbstractInterfaceObjectJson )
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_AbstractInterfaceObjectJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_AbstractInterfaceObjectJson )
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_AbstractInterfaceObjectJson);

        /// <summary>
        /// VALUE MEMBER KEY in OBJECT
        /// </summary>
        const string VALUE = "V";


        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_Interfaces_Object:
        ///     - all interfaces and object type;
        ///     -interfaces can't be IList or IDictionary
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            // abstract classes , all inherited enable classes?  
            return TypeSerializeAnalyser.CanHandle_AbstractObjectInterfaceType(type);           
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------




        #region ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------



        public static void Write_object_ToString<T>(
                                            //ITypeSerializerJson<T> tpSerializer
                                             TextWriter writer
                                            , T value
                                            , bool needToWriteType = false
                                            )
        {
            // write NULL  if null value
            if (value.IsNull())
            {
                //Json1.Write_ObjectStart(writer);
                writer.Write( Sym.NULL);
                //Json1.Write_ObjectEnd(writer);
                return;
            }

            // get real type
            var valuetypeEx = value.GetTypeInfoEx();
            
            /// locked code: for auto detecting and adding Contracts            
            JsonSerializer.AddKnownType(valuetypeEx);
           
            JNP.Write_ObjectStart(writer);
            
            //WRITE $Tp: typeName/typeNameLN
            JNP.Write_Type(writer, valuetypeEx);

            JNP.Write_Comma(writer);
            
            //Then  V : the object's value
            JNP.Write_MemberName(writer,VALUE);
            //write VALUE string
            JsonSerializer.GetTypeSerializer(valuetypeEx.ID)
                .RaiseSerializeToStringBoxedHandler(writer,value, needToWriteTypeAQName:false);

            JNP.Write_ObjectEnd(writer);

        }


        public static T Read_object_FromString<T>(
                                           //ITypeSerializerJson<T> tpSerializer
                                            string stringJson
                                           , ref int runIndex
                                           , int upperObjectEndIndex = -1 
                                           )
            {
            
            //TODO -  HOW READ !!!??? REAL TYPE INFO -HERE oR in ComplexType
            // SEND THIS AS PARAM TO REAL OBJECT SERIALIZER 
            //check for NULL VALUE
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {
                //Json1.PassObjectEND(stringJson, ref runIndex);
                return default(T);// null in Object/Interface
            }
            
            upperObjectEndIndex = JNP.GetObjectEndIndex(stringJson, ref runIndex);
                       
            //  cut object curls if it Needs
            JNP.PassObjectSTART(stringJson, ref runIndex);
            Type redirectType = null;
            if (JNP.PassNGetType(stringJson, ref runIndex, out redirectType))
            {
                var redirectTypeEx = redirectType.GetTypeInfoEx();

                /// locked code: for auto detecting and adding Contracts 
                var realTypeSerializer = JsonSerializer.AddKnownType(redirectTypeEx);
                
                JNP.PassComma(stringJson, ref runIndex);

                var valueFieldName = JNP.PassNGetMemberName(stringJson, ref runIndex);
                 
                T realValue = (T)realTypeSerializer.RaiseDeserializeFromStringBoxedHandler(
                                        stringJson
                                        , ref runIndex);//, upperObjectEndIndex
                
                JNP.PassObjectEND(stringJson, ref runIndex);

                return realValue;
            }
            else throw new InvalidCastException("value Type was not determined for object value");
             
        }
        

        #endregion ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------




        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(
            ITypeSerializerJson<T> tpSerializer) //
        {
            try
            {
                //algorithm:            
                // void  Write_object_ToString<T>(  TextWriter writer, T instanceValue)
               
                var typeInfo = typeof(T).GetTypeInfoEx();
                var parameterWriter = Expression.Parameter(Tps.T_TextWriter, "writer");
                var parameterInputData = Expression.Parameter(typeInfo.OriginalType , "data");
                var parameterNeedToWriteTypeAQName = Expression.Parameter(typeof(bool), "needToWriteTypeAQName");



                var methodWrite_Object_ToString =  SGType.GetMethod(nameof(Write_object_ToString))
                                                        .MakeGenericMethod(typeInfo.OriginalType);


                return Expression.Lambda<SDJson.SerializeToStringT<T>>(
                                                       Expression.Call(  methodWrite_Object_ToString
                                                                       //, typeSerializerInstance
                                                                       , parameterWriter
                                                                       , parameterInputData
                                                                       , parameterNeedToWriteTypeAQName
                                                                       )
                                                        , typeInfo.TypeDebugCodeConventionName + "SerializeToString_Write"
                                                       , new[] {
                                                             parameterWriter                                                            
                                                           , parameterInputData
                                                           , parameterNeedToWriteTypeAQName 
                                                            }
                                                       );                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{SGName}.{nameof(GenerateSerializeToString_WriteExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }


        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() 
        {
            try
            {
                //ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer(typeof(T)) as ITypeSerializerJson<T>;
                //var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterInpurSourceString = Expression.Parameter(Tps.T_string, "inputSource");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");


                var methodDeseriializeFromString = SGType.GetMethod(nameof(Read_object_FromString))
                                                        .MakeGenericMethod(typeof(T));
                
                var deserializeCall = Expression.Call(methodDeseriializeFromString
                                     //, typeSerializerInstance
                                    , parameterInpurSourceString
                                    , parameterRunIndexRef
                                    , parameterUpperObjectEndIndex
                                    );
                

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>
                                                (deserializeCall
                                                   , "DeserializeFromStringT"
                                                   , new[] {
                                                             parameterInpurSourceString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           }
                                                );

            }
            catch (System.Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
          
        }






#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------




        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:            
                // void  Write_object_ToString<T>(  TextWriter writer, T instanceValue)
                var typeInfo = TypeInfoEx.Get(typeof(T));
                var parameterWriter = Expression.Parameter(Tps.T_TextWriter, "writer");
                var parameterInputData = Expression.Parameter(typeInfo.OriginalType, "data");
                var parameterNeedToWriteTypeAQName = Expression.Parameter(Tps.T_bool, "needToWriteTypeAQName");
                var parameterTypeSerializer = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeSerializer");


                var methodWrite_Object_ToString = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                        .GetMethod(nameof(Write_object_ToString))
                                                        .MakeGenericMethod(typeInfo.OriginalType);


                return Expression.Lambda<SDJson.SerializeToStringTDbg<T>>(
                                                       Expression.Call(methodWrite_Object_ToString
                                                                       , parameterWriter
                                                                       , parameterInputData
                                                                       )
                                                        , typeInfo.TypeDebugCodeConventionName + "SerializeToString_Write"
                                                       , new[] { parameterWriter
                                                               , parameterInputData
                                                               , parameterNeedToWriteTypeAQName
                                                               , parameterTypeSerializer }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{SGName}.{nameof(GenerateSerializeToString_WriteExpressionDbg)}()  ERROR : Message -[{exc.Message}]  ", exc);
            } 
        }


        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------




#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [interfaces and object] types. Current Formatting is  Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            return new List<Type>() { Tps.T_object };      
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>      
        public void LoadDefaultTypeSerializers()
        {

            var serializeToStringHandler = GenerateSerializeToString_WriteExpression<object>(null).Compile();
            var deserializeFromStringHandler = GenerateDeserializeFromString_ReadExpression<object>().Compile();

            //object and its default 
            JsonSerializer.AddKnownType<object>()
               .SetSerializeToStringTHandlers(serializeToStringHandler, deserializeFromStringHandler);
                                                
        }
        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}
