﻿using System;
using System.IO;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;

using DDDD.Core.IO;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Type Serialize Generator for classes which implement IList or IList{}.
    /// </summary>
    internal class SerializeGenerator_ListJson : ITypeSerializerGeneratorJson
    {
        /// <summary>
        /// Serialize Generator Type - typeof(SerializeGenerator_ListJson)
        /// </summary>
        static readonly Type SGType = typeof(SerializeGenerator_ListJson);
        /// <summary>
        /// Serialize Generator Type.Name - nameof(SerializeGenerator_ListJson)
        /// </summary>
        static readonly string SGName = nameof(SerializeGenerator_ListJson);



        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_List:
        ///     - all classes that based on IList;
        ///     - and also is not an Array;
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return (!type.IsArray  && type.IsImplement_List() );
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region   ---------------------------------   READ / WRITE LIST HANDLERS ------------------------------------------

        // 1 way -  NeedNotToSureInItemType -( NullValueEnabled(sealed classes) + NullValueNotEnabled(structs) )


        static void WriteList_NeedToSureInItemType_ToString<T>(
                                ITypeSerializerJson<T> tpSerializer
                                , TextWriter writer
                                , T data
                                , bool needToWriteType = false)
        {
            // LIST IS NULL
            if (data == null)
            {
                JNP.Write_Null(writer);
                return;
            }

            var listInstance = data as IList;

            JNP.Write_ArrayStart(writer);
            for (int i = 0; i < listInstance.Count; i++)
            {
                //write element  data                              
                tpSerializer.RaiseElementSerializeToStringBoxedHandler(
                                   tpSerializer.RuntimeIDents.Arg1TypeID
                                 , writer, listInstance[i]);
                if (i < ( listInstance.Count - 1) )
                {
                    JNP.Write_Comma(writer);
                }
            }
            JNP.Write_ArrayEnd(writer);

        }



        static T ReadList_NeedToSureInItemType_FromString<T>(
                                ITypeSerializerJson<T> tpSerializer
                                , string stringJson
                                , ref int runIndex
                                , int upperObjectEndIndex = -1
                                 )
        {
            // List is NULL itselves
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {  return default(T);
            }

            var tListInstance = tpSerializer.LA_CreateDefaultValue.Value(null);             
            var list = (IList)tListInstance;

            
            var listItemSerializer = 
                JsonSerializer.GetTypeSerializer(tpSerializer.RuntimeIDents.Arg1TypeID);

            int listEndIndex = JNP.GetArrayEndIndex(stringJson, ref runIndex);

            JNP.PassArraySTART(stringJson, ref runIndex);

            //parse members
            while (runIndex < listEndIndex) // - 1
            {
                //Item value                                              
                var listItemObject = listItemSerializer
                        .RaiseDeserializeFromStringBoxedHandler(stringJson, ref runIndex);

                    //read element value
                list.Add(listItemObject);

               
                //var currSymbol = stringJson[runIndex];
                if (runIndex < listEndIndex)
                { JNP.PassComma(stringJson, ref runIndex); //COMMA separator after  members                  
                }                
            }

            JNP.PassArrayEND(stringJson, ref runIndex);
            return tListInstance;
        }
               


        static void WriteList_NeedNotToSureInItemType_ToString<T, TElement>(
                                ITypeSerializerJson<T> typeSerializer
                                , TextWriter writer
                                , T data
                                , bool needToWriteType = false)
         {            
                // LIST IS NULL
                if (data == null)
                {
                    JNP.Write_Null(writer);
                    return;
                }

                var listInstance = data as IList<TElement>;

                JNP.Write_ArrayStart(writer);
                for (int i = 0; i < listInstance.Count; i++)
                {
                    //write element  data                              
                    typeSerializer.RaiseElementSerializeToStringBoxedHandler(
                                    typeSerializer.RuntimeIDents.Arg1TypeID
                                  , writer, listInstance[i]);

                    if (i < (listInstance.Count - 1) )
                    {
                        JNP.Write_Comma(writer);
                    }
                }
                JNP.Write_ArrayEnd(writer); 
         }


        static T ReadList_NeedNotToSureInItemType_FromString<T,TElement>(
                                ITypeSerializerJson<T> tpSerializer
                                , string stringJson
                                , ref int runIndex
                                , int upperObjectEndIndex = -1
                                )
        {
            // List is NULL itselves
            if (JNP.PassNullValue(stringJson, ref runIndex))
            {
                return default(T);
            }

            var tListInstance = tpSerializer.LA_CreateDefaultValue.Value(null);
            var list = (IList<TElement>)tpSerializer.LA_CreateDefaultValue.Value(null); ;
                        
            var listItemSerializer = 
                JsonSerializer.GetTypeSerializer(tpSerializer.RuntimeIDents.Arg1TypeID);

            int listEndIndex = JNP.GetArrayEndIndex(stringJson, ref runIndex);
            JNP.PassArraySTART(stringJson, ref runIndex);
            
            //parse members
            while (runIndex < listEndIndex)// -1
            {
               
                    var listItemObject =(TElement) listItemSerializer
                                        .RaiseDeserializeFromStringBoxedHandler(stringJson, ref runIndex);

                    //read element value
                    list.Add( listItemObject);

                    //var currSymbol = stringJson[runIndex];
                    if (runIndex < listEndIndex)
                    {
                        JNP.PassComma(stringJson, ref runIndex); //COMMA separator after  members                  
                    }
            }

            JNP.PassArrayEND(stringJson, ref runIndex);
            return tListInstance;
        }


        #endregion   ---------------------------------   READ / WRITE LIST HANDLERS ------------------------------------------



        static MethodInfo Get_WriteListMethod<T>(ITypeSerializerJson<T> tpserializer)
        {
            if (tpserializer.TargetType.IsNeedToSureInDataType())
            {
                return SGType.GetMethod(nameof(WriteList_NeedToSureInItemType_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType);
            }
            else
            {
                return SGType.GetMethod(nameof(WriteList_NeedNotToSureInItemType_ToString), BindingFlags.Static | BindingFlags.NonPublic)
                                                      .MakeGenericMethod(tpserializer.TargetType
                                                                        , tpserializer.TargetTypeEx.Arg1Type);
            }

            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(Get_WriteListMethod)}() - such List Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");

        }


        static MethodInfo Get_ReadListMethod<T>(ITypeSerializerJson<T> tpserializer)
        {
            if (tpserializer.TargetType.IsNeedToSureInDataType())
            {
                return SGType.GetMethod(nameof(ReadList_NeedToSureInItemType_FromString), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType);
            }
            else
            {
                return  SGType.GetMethod(nameof(ReadList_NeedNotToSureInItemType_FromString), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType
                                                                        , tpserializer.TargetTypeEx.Arg1Type);
            }
             
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(Get_ReadListMethod)}() - such List Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");
        }




        #region -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(
            ITypeSerializerJson<T> tpSerializer) 
        {
            try
            {
                //algorithm:
                // void WriteIListToString<T>(TypeSerializer<T>  typeSerializer,TextWriter writer, T data, bool needToWriteType = false)                
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterWriter = Expression.Parameter(typeof(TextWriter), "writer");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var parameterNeedToWriteType = Expression.Parameter(typeof(bool), "needToWriteType");
                

                //choose optimal IList Writing method
                MethodInfo methodWrite_IList_ToString = Get_WriteListMethod(tpSerializer);

                return Expression.Lambda<SDJson.SerializeToStringT<T>>
                                                        (
                                                         Expression.Call
                                                         (
                                                           methodWrite_IList_ToString
                                                         , typeSerializerInstance
                                                         , parameterWriter
                                                         , parameterInputData
                                                         , parameterNeedToWriteType
                                                         )
                                                        , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                        , new[] {
                                                            parameterWriter
                                                            , parameterInputData
                                                            , parameterNeedToWriteType }

                                                        );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>()
        {
            try
            {
                //algorithm:
                //  T ReadIListFromString<T>(TypeSerializer<T> typeSerializer, string listJson, ref int runIndex)
                ITypeSerializerJson<T> tpSerializer = JsonSerializer.GetTypeSerializer(typeof(T)) as ITypeSerializerJson<T>;
                var typeSerializerInstance = Expression.Constant(tpSerializer);
                var parameterJsonString = Expression.Parameter(Tps.T_string, "jsonString");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
                var parameterUpperObjectEndIndex = Expression.Parameter(Tps.T_int, "upperObjectEndIndex");
                

                //choose optimal IList Reading method
                MethodInfo methodRead_List_FromString = Get_ReadListMethod(tpSerializer);

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>(
                                                           Expression.Call(
                                                             methodRead_List_FromString
                                                           , typeSerializerInstance
                                                           , parameterJsonString
                                                           , parameterRunIndexRef
                                                           , parameterUpperObjectEndIndex
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] {  parameterJsonString
                                                                    ,  parameterRunIndexRef
                                                                    , parameterUpperObjectEndIndex
                                                                   }
                                                           );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        #endregion -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////





        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------


        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateSerializeToString_WriteExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }


        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {SGName}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }


        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------


#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [IList ] types. Current Formatting is Json.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Json;//  throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException("SerializeGenerator_List - can't have the Final List of Supported Types");
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers( )
        {
            throw new NotSupportedException("SerializeGenerator_List - can't have the Final List of Supported Types");
        }

     

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}



#region --------------------------------- GARBAGE -------------------------------------


////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = BinaryReaderWriter.ReadT_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T);//just the same as return null  for reference types
//    }
//}

////reading collection Length and creating It
//int collectionLength = BinaryReaderWriter.ReadT_int32(stream); //read collection Length

////now create IList instance
//var list = (IList<TElement>)TypeActivator.CreateInstanceT<T>(TypeActivator.DefaultCtorSearchBinding);//   





// write all IList elements to Stream
//foreach (object element in listInstance)
//{
//    ushort elementTypeID = typeSerializer.SetSerializer.GetTypeID(element);

//    if (elementTypeID == 0) //null data
//    { 
//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.WriteT_uint16(stream, elementTypeID);                    
//    }
//    else
//    {
//        //write element  data                   
//        typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementTypeID, stream, element);
//    }
//}


//foreach (TElement element in listInstance)
//{    //write element  data                              
//     typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID , stream, element);
//}

//// 2 way   NeedNotToSureInItemType + Nullable(items are - sealed  classes)
//public static void WriteIList_NeedNotToSureInItemType_Nullable_ToStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write value typeID - or [null]'s typeID( 0 )
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var listInstance = instanceValue as IList;

//    //write Length
//    BinaryReaderWriter.WriteT_int32(stream, listInstance.Count); // write IList length - List Length

//    //write all IList elements to Stream
//    foreach (object element in listInstance)
//    {             
//        if (element == null) //null data
//        {
//            BinaryReaderWriter.WriteT_uint16(stream, 0);                    
//        }
//        else
//        {
//            //write element  data                     
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(typeSerializer.Arg1TypeID, stream, element);
//        }
//    }

//}//

//public static void WriteIListToStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write Length
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var listInstance = instanceValue as IList;

//    BinaryReaderWriter.WriteT_int32(stream, listInstance.Count); // write IList length - List Length

//    //write all IList elements to Stream
//    foreach (object element in listInstance)
//    {
//        ushort elementTypeID = typeSerializer.SetSerializer.GetTypeID(element);

//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.WriteT_uint16(stream, elementTypeID);

//        if (elementTypeID == 0) //null data
//        {
//            continue;
//        }
//        else
//        {
//            //write element  data 
//            //typeSerializer.RaiseElementSerializeToStreamTHandler(elementTypeID, stream, element);
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementTypeID, stream, element);
//        }
//    }

//}

#endregion --------------------------------- GARBAGE -------------------------------------