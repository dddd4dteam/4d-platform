﻿using System;
using System.Text;

using DDDD.Core.Extensions;
using DDDD.Core.Resources;
using DDDD.Core.Diagnostics;
using System.Collections.Generic;
using DDDD.Core.Reflection;
using System.IO;
using DDDD.Core.Collections;
using System.Runtime.CompilerServices;
using DDDD.Core.IO.Text;

namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// JSON Parser
    /// </summary>
    public class JNP
    {
             public const string FLD_TYPELN = "TypeLN";
             public const string FLD_ASSEMBLY = "Assembly";
         
        #region ----------- FORMATTING -------------------

        /// <summary>
        /// Formatting on writing of json string values
        /// </summary>
        public class Fmt
        {
            [ThreadStatic]
            public static int Deep = 0;

            [ThreadStatic]
            public int Before;
        }

        #endregion ----------- FORMATTING -------------------

        #region ------------------ SETTINGS --------------------

        [ThreadStatic]
        internal static JsonOptions Options = new JsonOptions(); 
        
        #endregion ------------------ SETTINGS --------------------


        #region ------------------------------ CONSTS ------------------------------

        const string className = nameof(JNP);

        /// <summary>
        /// Validator with simple Assertion - Invalid Operation Exception,Null Reference Exceptopm
        /// </summary>
        static readonly ArgValidator Valid = new ArgValidator(className);


        /// <summary>
        /// Not nullable Types that will be written in Json.StringType Value syntax: "value".
        /// </summary>
        public static readonly HashSet<Type> StringTypes = new HashSet<Type>()
        {   Tps.T_char , Tps.T_DateTime
            ,Tps.T_TimeSpan, Tps.T_DateTimeOffset, Tps.T_Guid
            // ++ Enums
        };

        /// <summary>
        ///  Nullable Types that will be written in Json.StringType Value syntax or can have NULL value too.
        /// </summary>
        public static readonly HashSet<Type> StringNullableTypes = new HashSet<Type>()
        {
                Tps.T_string,Tps.T_Uri
                ,Tps.T_charNul , Tps.T_DateTimeNul
                ,Tps.T_TimeSpanNul , Tps.T_DateTimeOffsetNul
                , Tps.T_GuidNul
        };

        /// <summary>
        /// StartEnd string  value  finding points in json string
        /// </summary>
        [ThreadStatic]
        internal static int[] stringEndIndexes = new int[] { -1, -1 };

        /// <summary>
        /// Start End Sybols finding points in json string. It used on finding - Arrays and  Objects
        /// </summary>
        [ThreadStatic]
        internal static int[] charStartEndIndex = new int[] { -1, -1 };

        #endregion ------------------------------ CONSTS ------------------------------

        #region ------------ WRITE FORMATTING  --------------
        static void Write_CharN(TextWriter writer,char symb, int count)
        {
            for (int i = 0; i < count; i++)
            {  writer.Write(symb);       }
        }


        

        /// <summary>
        /// If Optionms UseFormatSymbols - insert  RETURN and TAB(Deep times), then Deep++
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_FormatDown(TextWriter writer, bool cancelFormatHere = false)
        {
            if (Options.UseFormatSymbols == false || cancelFormatHere) return;
            

            if (Fmt.Deep == 0)
            {
                Fmt.Deep++;
                return;
            }
            //write new line
            else if (Fmt.Deep > 0)
            {
                Write_CharN(writer, Sym.RETURN, 1);
                Write_CharN(writer, Sym.TAB, Fmt.Deep);
                Fmt.Deep++;
            }            
        }

        /// <summary>
        ///  If Optionms UseFormatSymbols - insert  RETURN 
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_FormatReturn(TextWriter writer )
        {
            if (Options.UseFormatSymbols == false  ) return;
            Write_CharN(writer, Sym.RETURN, 1);
        }



        /// <summary>
        /// If Optionms UseFormatSymbols - insert  RETURN and TAB(Deep times)
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_FormatOnLevel(TextWriter writer , bool cancelFormatHere = false)
        {
            if (Options.UseFormatSymbols == false || cancelFormatHere) return;


            Write_CharN(writer, Sym.RETURN, 1);
            Write_CharN(writer, Sym.TAB, Fmt.Deep);
        }

        /// <summary>
        /// If Optionms UseFormatSymbols - insert RETURN and TAB(Deep-- times)
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_FormatUp(TextWriter writer, bool cancelFormatHere = false)
        {
            if (Options.UseFormatSymbols == false || cancelFormatHere) return;

            if (Fmt.Deep == 0)
            {
                //Fmt.Deep--;
                Write_CharN(writer, Sym.RETURN, 1);
                return;
            }
            //write new line
            else if (Fmt.Deep > 0)
            {
                Write_CharN(writer, Sym.RETURN, 1);
                Fmt.Deep--;
                Write_CharN(writer, Sym.TAB, Fmt.Deep);
               
            }
        }

            #endregion ------------ WRITE FORMATTING  --------------



            #region ------------- READ/WRITE TYPE NAME -----------------

            //const string TYPEKEY = "$Tp";

            /// <summary>
            ///  Write Type AQ Name or Type Light Name on object serialization
            /// </summary>
            /// <param name="writer"></param>
            /// <param name="tpInfoEx"></param>
            public static void Write_Type(  TextWriter writer
                                      , TypeInfoEx  tpInfoEx)
        { // typeName -here already Selected - ShortTypeName or AQName
            Write_MemberName(writer, Sym.TYPEKEY);
            
            if ( Options.UseTypeLightName)
            {
                Write_String(writer, tpInfoEx.OriginalTypeLightName);                
            }
            else {
                Write_String(writer, tpInfoEx.OriginalTypeAQName);                
            }
        }


        /// <summary>
        /// Pass /Read and return determined Type
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static bool  PassNGetType(string inputSource, ref int runIndex, out  Type foundType)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }


            var tpMemberName = JNP.PassNGetStringContentOnlyValue(inputSource, ref runIndex);
            if (tpMemberName != Sym.TYPEKEY)
            {
                runIndex -= (tpMemberName.Length + 2);
                foundType = null; 
                return false;
            }

            JNP.PassColon(inputSource, ref runIndex);
            var tpTypeName = JNP.PassNGetStringContentOnlyValue(inputSource, ref runIndex);

            if (Options.UseTypeLightName)
            {
                foundType = TypeLN.ParseTypeLightName(tpTypeName);
                return foundType.IsNotNull();
            }
            else
            {
                foundType = Type.GetType(tpTypeName);
                return foundType.IsNotNull();
            } 
        }


        #endregion -------------READ/ WRITE TYPE NAME -----------------






        #region ----------------------- WRITE JSON OBJECT BEGIN/END -------------------------

        /// <summary>
        /// Write  Json object begin symbol : '{'
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_ObjectStart(TextWriter writer, bool cancelFormatHere = false )
        {           
             Write_FormatDown(writer, cancelFormatHere);  

            writer.Write(Sym.OBJECT_START);
        }

        /// <summary>
        /// Write  Json object end symbol : '}'
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_ObjectEnd(TextWriter writer, bool cancelFormatHere = false)
        {
            Write_FormatUp(writer,cancelFormatHere);      

            writer.Write(Sym.OBJECT_END);
        }


        /// <summary>
        /// WRITE COMMA symbol :  ','
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_Comma(TextWriter writer)
        {
            writer.Write(Sym.COMMA);
        }

        /// <summary>
        /// WRITE COLON symbol:  ':'
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_Colon(TextWriter writer)
        {
            writer.Write(Sym.COLON);
        }
        
        /// <summary>
        /// Write  QUOTE symbol : '"'
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_Quote(TextWriter writer)
        {
            writer.Write(Sym.QUOTE);
        }


        /// <summary>
        /// Write [null] value  symbols : "null"
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_Null(TextWriter writer)
        {
            writer.Write(Sym.NULL);
        }


        /// <summary>
        /// Write   [QUOTE + value + QUOTE + COLON]
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="member"></param>
        public static void Write_MemberName(TextWriter writer, string member)
        {
            writer.Write( Sym.QUOTE + member + Sym.QUOTE + Sym.COLON );
        }

        /// <summary>
        /// Write   [QUOTE + value + QUOTE]  
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="member"></param>
        public static void Write_String(TextWriter writer, string value)
        {
            writer.Write(Sym.QUOTE + value + Sym.QUOTE );
        }

        #endregion  --------------------------- WRITE JSON OBJECT BEGIN/END ----------------------------



        #region  -------------------------------  WRITE JSON ARRAY BEGIN/END --------------------------------


        /// <summary>
        /// Write  Json array begin symbol : '['
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_ArrayStart(TextWriter writer)
        {
             Write_FormatDown(writer);                           

            writer.Write(Sym.ARRAY_START);
        }

        /// <summary>
        ///  Write  Json array end symbol : ']'
        /// </summary>
        /// <param name="writer"></param>
        public static void Write_ArrayEnd(TextWriter writer)
        {
            Write_FormatUp(writer);  

            writer.Write(Sym.ARRAY_END);
        }


        #endregion  -------------------------------  WRITE JSON ARRAY BEGIN/END --------------------------------



        #region ---------------------- PASS FORMAT SYMBOLS    -----------------------

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static void GetStartEndIndexes(char Start, char End
                                                , string inputSource
                                                , ref int runIndex)
        {
            charStartEndIndex[0] = -1;//StartIndex
            charStartEndIndex[1] = -1; //EndIndex
            

            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            //int startIndex = -1; int endIndex = -1;

            // get start index
            for (int i = runIndex; i < inputSource.Length; i++)
            {
                if (inputSource[i] == Start)
                {
                    charStartEndIndex[0] = i; break;
                }
            }

            // get end index
            int deep = 1;
            for (int i = (charStartEndIndex[0] + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == Start) { deep++; continue; }

                if (inputSource[i] == End && deep == 1)
                { charStartEndIndex[1] = i; break; }
                else if (inputSource[i] == End) deep--;
            }

            // check end Index was found 
            if (charStartEndIndex[1] <= -1 || charStartEndIndex[1] < charStartEndIndex[0])
            {
                throw new InvalidOperationException(
                    $"Incorrecy end index of OBJECT string value : { charStartEndIndex[1].S()}");
            }
            
            //return indexes;
        }

        /// <summary>
        /// Get save Indexes of string into private ThreadStatic stringEndIndexes int[]
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
       internal static void GetStringIndexes( string inputSource
                                               , ref int runIndex)
        {
            //var indexes = new int[2] { -1, -1 };

            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            stringEndIndexes[0] = -1;
            stringEndIndexes[1] = -1;
            

            // get start index
            for (int i = runIndex; i < inputSource.Length; i++)
            {
                if (inputSource[i] == Sym.QUOTE)
                {
                    stringEndIndexes[0] = i; break;
                }
            }

            // get end index            
            for (int i = (stringEndIndexes[0] + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == Sym.QUOTE)
                {
                    stringEndIndexes[1] = i; break;
                }
            }

            // check end Index was found 
            if (stringEndIndexes[1] <= -1 || stringEndIndexes[1] < stringEndIndexes[0])
            {
                throw new InvalidOperationException(
                    $"Incorrecy end index of OBJECT string value : { stringEndIndexes[1].S()}");
            }

        }




        // PassFormatSymbols

        /// <summary>
        /// Pass format symbols  Spaces/other symbols can be check to ignore them
        /// <ppara/>This Method used with  Settings.UseFormatSymbols TRUE
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static void PassFormatSymbols(string inputSource, ref int runIndex)
        {
            if (Sym.FormatChars.NotContains(inputSource[runIndex]))
                return;

            int startIndex = runIndex;

            for (int i = runIndex; i < inputSource.Length; i++)
            {
                if (Sym.FormatChars.Contains(inputSource[i]))
                    runIndex++;
                else break;
            }                        
        }


        #endregion ---------------------- PASS FORMAT SYMBOLS -----------------------


        #region  ------------------------PASS COLON/ COMMA -----------------------
        // PASSColon
        // PASSComma


        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is  COLON symbol - [':'] .
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>     
        /// <returns></returns>
        public static void PassColon(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            Validator.ATInvalidOperation(inputSource[runIndex] != Sym.COLON, nameof(JNP), nameof(PassColon)
                        , $" Unexpected symbol-[{inputSource[runIndex].S()}] instead of COLON-[':'] symbol ");
            
            if (inputSource[runIndex] == Sym.COLON)
            {
                runIndex++;                
                return; //COLON.S()
            }

            return ;// 
        }



        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is  COMMA symbol - [','] .
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static void PassComma(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            Validator.ATInvalidOperation(inputSource[runIndex] != Sym.COMMA, nameof(JNP), nameof(PassComma)
                        , $" Unexpected symbol-[{inputSource[runIndex].S()}] instead of COMMA-[','] symbol " );
            
            if (inputSource[runIndex] == Sym.COMMA)
            {
                runIndex++;                 
                return;// COMMA.S()
            }

            return; // EMPTY
        }


        #endregion ------------------------ PASS COLON/ COMMA -----------------------



        #region ---------------------- PASS VALUES: NULL/NUMBER/STRING/BOOL ---------------------------


        //  PassNGetNull       
        //  PassNGetStringValue 
        //  PassNGetStringContentOnlyValu
        //  PassNGetMemberName       
        

        /// <summary>
        ///  Pass  [null] value  from json string 
        ///  <para/> If it is [null] return true and move runIndex  +4 symbols.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static bool PassNullValue(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            if (inputSource[runIndex] != Sym.C_n) return false;

            if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == Sym.C_u &&
                       inputSource[runIndex + 2] == Sym.C_l &&
                       inputSource[runIndex + 3] == Sym.C_l)
            {
                runIndex += 4;
                return true;                
            }

            return false;
        }






        /// <summary>
        /// Pass And Get string value from json string - [\"stringValue\"].        
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string PassNGetStringValue(string inputSource, ref int runIndex)
        {          
            GetStringIndexes(inputSource, ref runIndex);

            //move runIndex and return substring            
            runIndex = stringEndIndexes[1] + 1;
            // here we need to use inclusive substring - ["value"]
            return inputSource.Substring(stringEndIndexes[0], (stringEndIndexes[1] - stringEndIndexes[0] + 1));
        }

      
        /// <summary>
        /// Pass And Get string value  ONLY from json string- [stringValue], - there is no QUOTEs.         
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string PassNGetStringContentOnlyValue(string inputSource, ref int runIndex)
        {
            GetStringIndexes( inputSource, ref runIndex);

            //int startIndex = idxes[0];
            //int endIndex = idxes[1];

            //move runIndex and return substring            
            runIndex = stringEndIndexes[1] + 1;
            // here we need to use exclusive substring - [value]
            return inputSource.Substring(stringEndIndexes[0] + 1, (stringEndIndexes[1] - stringEndIndexes[0] - 1));             
        }

        /// <summary>
        ///  Pass And Get string value of ARRAY without its '[' and ']'  symbols
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string PassNGetArrayContentValue(string inputSource, ref int runIndex)
        {
            GetStartEndIndexes(Sym.ARRAY_START
                                    , Sym.ARRAY_END
                                 , inputSource, ref runIndex);
            
            //move runIndex and return substring  
            runIndex = charStartEndIndex[1] + 1;
            //   here we need to use exclusive eating
            return inputSource.Substring(charStartEndIndex[0] + 1, (charStartEndIndex[1] - charStartEndIndex[0] - 1));
            
        }




        /// <summary>
        /// Pass And Get string value  ONLY from json string, Pass COLON too.
        /// <para/>  Here we get [stringValue]: - value, - there is NO QUOTEs. 
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string PassNGetMemberName(string inputSource, ref int runIndex)
        {
            var memberName = PassNGetStringContentOnlyValue(inputSource, ref runIndex);
            PassColon(inputSource, ref runIndex);
            return memberName;
        }


        /// <summary>
        /// Pass And Get boolean value from Json string with runIndex.
        /// <para/> Returns as result boolean [true] or [false].
        /// <para/> If bool walue won't be found InvalidOperation exception will be throw.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static bool PassNGetBoolValue(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }
            
            if (inputSource[runIndex] == Sym.C_t)
            {
                if (inputSource.Length >= 4 &&
                       inputSource[runIndex + 1] == Sym.C_r &&
                       inputSource[runIndex + 2] == Sym.C_u &&
                       inputSource[runIndex + 3] == Sym.C_e)
                {
                    runIndex += 4;
                    return true;//TRUE;
                }
            }

            if (inputSource[runIndex] == Sym.C_f)
            {
                if (inputSource.Length >= 5 &&
                       inputSource[runIndex + 1] == Sym.C_a &&
                       inputSource[runIndex + 2] == Sym.C_l &&
                       inputSource[runIndex + 3] == Sym.C_s &&
                       inputSource[runIndex + 4] == Sym.C_e
                       )
                {
                    runIndex += 5;
                    return false; //FALSE;
                }
            }

            throw new InvalidOperationException("Bool value was no found");
           
        }


        /// <summary>
        /// Pass and Get Number Value-string from Json string with runIndex
        /// <para/> Returns as result string -to Parse it further by the target Number Type.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string PassNGetNumberValue(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }

            if (Sym.NumberStartChars.NotContains(inputSource[runIndex])) return "";

            int startIndex = runIndex;
            for (int i = runIndex + 1; i < inputSource.Length; i++)
            {
                if (Sym.NumberChars.Contains(inputSource[i])) runIndex++;
                else { runIndex++;  break; }
            }

            string bytedString = inputSource.Substring(startIndex, (runIndex - startIndex));
             
            return bytedString;
        }


        #endregion ---------------------- PASS VALUES: NULL/NUMBER/STRING/BOOL ---------------------------



        #region ----------------------- PASS OBJECT/ OBJECT MEMBERS --------------------------


        //  GetObjectEndIndex
        //  GetArrayEndIndex - 
        //  PassObjectSTART
        //  PassObjectEND

        
        



        /// <summary>
        /// Get index of Object  End symbol - '}' in json string.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static int GetObjectEndIndex(string inputSource, ref int runIndex)
        {
            GetStartEndIndexes(  Sym.OBJECT_START
                               , Sym.OBJECT_END
                               ,inputSource, ref runIndex );

            return  charStartEndIndex[1];
        }


        /// <summary>
        /// Get index of Array End symbol - ']' in json string.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static int GetArrayEndIndex(string inputSource, ref int runIndex)
        {
            GetStartEndIndexes(  Sym.ARRAY_START
                               , Sym.ARRAY_END
                               , inputSource, ref runIndex);

            return charStartEndIndex[1]; ;            
        }

        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is '{'.
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        public static  void PassObjectSTART(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }
            
            Valid.ATInvalidOperationDbg(inputSource[runIndex] != Sym.OBJECT_START, nameof(PassObjectSTART)
                        , $" Unexpected symbol-[{ inputSource[runIndex].S() }] instead of OBJECT_START-[{Sym.OBJECT_START}] symbol ");

            if (inputSource[runIndex] == Sym.OBJECT_START)
            { runIndex++; return; }  
            else return; 
        }

        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is '}'.
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        public static void PassObjectEND(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            {  PassFormatSymbols(inputSource, ref runIndex); }
            
            Valid.ATInvalidOperationDbg(inputSource[runIndex] != Sym.OBJECT_END, nameof(PassObjectEND)
                        , $" Unexpected symbol-[{ inputSource[runIndex].S()}] instead of   OBJECT_END-[{Sym.OBJECT_END}] symbol " );
            
            if (inputSource[runIndex] == Sym.OBJECT_END)
            { runIndex++; }  
            else return;
        }
 

        #endregion ----------------------- PASS OBJECT/ OBJECT MEMBERS --------------------------


        #region --------------------------- PASS ARRAY ----------------------------

        //  PassArraySTART
        // PassArrayEND

        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is '['.
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        public static void PassArraySTART(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }
            
            Valid.ATInvalidOperationDbg(inputSource[runIndex] != Sym.ARRAY_START, nameof(PassArraySTART)
                        , " Unexpected symbol-[{0}] instead of  ARRAY_START-['['] symbol ".Fmt(inputSource[runIndex].S()));
            
            if (inputSource[runIndex] == Sym.ARRAY_START)
            { runIndex++; return; } 
            else return; 
        }

        /// <summary>
        /// Pass string and check: if symbol at runIndex in Json strin is ']'.
        /// <para/> If it is not so then throw InvalidOperation exception.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static void PassArrayEND(string inputSource, ref int runIndex)
        {
            if (Options.UseFormatSymbols)
            { PassFormatSymbols(inputSource, ref runIndex); }
            
            Valid.ATInvalidOperationDbg(inputSource[runIndex] != Sym.ARRAY_END, nameof(PassArrayEND )
                        , " Unexpected symbol-[{0}] instead of  ARRAY_END -[']'] symbol ".Fmt(inputSource[runIndex].S()));
            
            if (inputSource[runIndex] == Sym.ARRAY_END)
            { runIndex++; return; } 
            else return;  
        }

        #endregion --------------------------- PASS ARRAY ----------------------------



        #region ------------------- JSON VALUE TYPE -------------------------

        /// <summary>
        /// Get JsonValueType(ECMA-404 Standart JSON Value Type) for targetType
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static JsonValueTypeEn GetJsonValueType(TypeInfoEx targetType)
        {
            var result = JsonValueTypeEn.NotDefined;

            if (targetType.OriginalType == Tps.T_boolNul )
                result = (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null);

            else if (targetType.OriginalType == Tps.T_bool)
                result = JsonValueTypeEn.Boolean;

            else if (StringTypes.Contains(targetType.OriginalType)
                || targetType.OriginalType.IsEnum )
                result = (JsonValueTypeEn.String );

            else if (StringNullableTypes.Contains(targetType.OriginalType))
                result = (JsonValueTypeEn.String|JsonValueTypeEn.Null);

            else if (TypeInfoEx.NumberNullableTypes.Contains(targetType.OriginalType))
                result = (JsonValueTypeEn.Number | JsonValueTypeEn.Null);

            else if (TypeInfoEx.NumberNotNullableTypes.Contains(targetType.OriginalType))
                result = JsonValueTypeEn.Number;

            else if (targetType.OriginalType.IsArray
                || Tps.T_IEnumerable.IsAssignableFrom(targetType.OriginalType)
                //|| targetType.OriginalType == Tps.T_byteArray
                //|| targetType.OriginalType == Tps.T_BitArray
                )
                result = (JsonValueTypeEn.Array | JsonValueTypeEn.Null);

            else if (targetType.OriginalType.IsValueType
              ) return (JsonValueTypeEn.Object);

            else if (targetType.OriginalType == Tps.T_object)
                return (JsonValueTypeEn.Unknown);

            else if (  (targetType.OriginalType.IsClass && !targetType.OriginalType.IsAbstract)
                     || (targetType.OriginalType.IsValueType && targetType.IsNullableType)
                ) return (JsonValueTypeEn.Object | JsonValueTypeEn.Null);
            
            else if ( (targetType.OriginalType.IsClass && targetType.OriginalType.IsAbstract)
                     || targetType.OriginalType.IsInterface
                ) return JsonValueTypeEn.Unknown;
            

            else // for interndfaces // abstract classes 
                result = JsonValueTypeEn.NotDefined;

            return result;

        }





        #endregion -------------------  JSON VALUE TYPE -------------------------




    }
}
