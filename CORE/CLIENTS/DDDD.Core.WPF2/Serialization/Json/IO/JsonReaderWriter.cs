﻿/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Collections;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;
using System.Globalization;
using DDDD.Core.IO.Text;

namespace DDDD.Core.Serialization.Json 
{



    /// <summary>
    /// Reader/Writer for Primitive types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    /// <para/>                          DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    /// <para/>                          DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// <para/>   Reflection             TypeInfoEx 
    /// </summary>
    public static class JsonReaderWriter
    {
        #region ------------------------------- CTOR ----------------------------------

        static JsonReaderWriter()
        {
            InitPrimitiveReadingWritingMethods();
        }

        #endregion ------------------------------- CTOR ----------------------------------
              

        #region --------------------- NULLABLE READ/WRITE WRAPS --------------------------



        static void WriteNullableAction<T>(TextWriter writer, T? value, Action<TextWriter, T?> action) where T : struct
        {
            if (!value.HasValue)
            {
                writer.Write(Sym.NULL);                
            }
            else
            {        
                action(writer, value);
            }
        }

        static object ReadNullableAction<T>(String stringValue, Func<string, T?> function) where T : struct
        {
          
            if (stringValue  == Sym.NULL) //Has No Value
            {
                return null; //      
            }
            else //Has  value 
            {
                return function(stringValue); 
            }
        }



        static T? ReadNullableTAction<T>(String stringValue, Func<string, T?> function) where T : struct
        {
            if (stringValue == Sym.NULL) //Has No Value
            {
                return null; //      
            }
            else //Has  value 
            {
                return function(stringValue);
            }
        }
        

        #endregion --------------------- NULLABLE READ/WRITE WRAPS --------------------------
        

        #region ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------

        static Dictionary<Type, MethodInfo> WritingPrimitiveMethod = new Dictionary<Type, MethodInfo>();
        static Dictionary<Type, MethodInfo> ReadingPrimitiveMethod = new Dictionary<Type, MethodInfo>();



        static void InitPrimitiveReadingWritingMethods()
        {
            OperationInvoke.CallInMode(nameof(JsonReaderWriter), nameof(InitPrimitiveReadingWritingMethods)
                , () =>
                {
                    foreach (var primitiveType in Tps.Primitives)
                    {
                        //writing method 

                        //
                        Type realType = primitiveType.GetWorkingTypeFromNullableType();
                        bool IsNullableType = primitiveType.IsNullable();

                        string writeMethodName = "WriteT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");
                        var writePrimitiveMethod = typeof(JsonReaderWriter).GetMethod(writeMethodName, new[] { typeof(Stream), primitiveType });
                        WritingPrimitiveMethod.Add(primitiveType, writePrimitiveMethod);



                        string readMethodName = "ReadT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");

                        var readPrimitiveMethod = typeof(JsonReaderWriter).GetMethod(readMethodName, BindingFlags.Static | BindingFlags.Public);

                        ReadingPrimitiveMethod.Add(primitiveType, readPrimitiveMethod);
                    }

                });

        }

        public static MethodInfo GetWritePrimitiveMethod(Type primitiveType)
        {
            return WritingPrimitiveMethod[primitiveType];
        }

        public static MethodInfo GetReadPrimitiveMethod(Type primitiveType)
        {
            return ReadingPrimitiveMethod[primitiveType];
        }

        //public static List<Type> GetRegisteredPrimitives()
        //{
        //return TypeInfoEx.PrimitiveTypes;
        //}

        //public static bool IsPrimitiveType(Type type)
        //{
        //    return TypeInfoEx.PrimitiveTypes.Contains(type);
        //}

        #endregion ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------


        #region ---------------------------- READ/Write short-int16 ----------------------------------
            
        public static void Write(TextWriter writer, short value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int16(TextWriter writer, short value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_int16(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return short.Parse(numberString);
        }
        public static short ReadT_int16(string itemValue, ref int runIndex, int upperObjectEndIndex)  
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return short.Parse(numberString);
        }

        #endregion ---------------------------- READ/Write short-int16----------------------------------


        #region ---------------------------- READ/Write ushort-uint16 ----------------------------------


        public static void Write(TextWriter writer, ushort value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint16(TextWriter writer, ushort value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_uint16(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return short.Parse(numberString);
        }

        public static ushort ReadT_uint16(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return ushort.Parse(numberString);
        }


        #endregion ---------------------------- READ/Write ushort-uint16 ----------------------------------


        #region ---------------------------- READ/Write int32----------------------------------



        static int CreateInteger(string svalue, int index, int count)
        {
            int num = 0;
            bool neg = false;
            for (int x = 0; x < count; x++, index++)
            {
                char cc = svalue[index];

                if (cc == '-')
                    neg = true;
                else if (cc == '+')
                    neg = false;
                else
                {
                    num *= 10;
                    num += (int)(cc - '0');
                }
            }
            if (neg) num = -num;

            return num;
        }



        public static void Write(TextWriter writer, int value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int32(TextWriter writer, int value, bool needToWriteTypeAQNameField = false )
        {
            writer.Write(value.S());
        }

        public static object Read_int32(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return int.Parse(numberString);
        }

        public static int ReadT_int32(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return int.Parse(numberString);
        }


        #endregion ---------------------------- READ/Write int32----------------------------------


        #region ---------------------------- READ/Write uint32----------------------------------
        

        public static void Write(TextWriter writer, uint value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint32(TextWriter writer, uint value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_uint32(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex); 
            return uint.Parse(numberString);
        }

        public static uint ReadT_uint32(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return uint.Parse(numberString);
        }



        #endregion ---------------------------- READ/Write uint32----------------------------------


        #region ---------------------------- READ/Write long-int64 ----------------------------------
         

        public static void Write(TextWriter writer, long value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int64(TextWriter writer, long value, bool needToWriteTypeAQNameField = false )
        {
            writer.Write(value.S());
        }

        public static object Read_int64(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return long.Parse(numberString);
        }

        public static long ReadT_int64(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return long.Parse(numberString);
        }

        #endregion ---------------------------- READ/Write long-int64 ----------------------------------


        #region ---------------------------- READ/Write ulong-uint64 ----------------------------------


        public static void Write(TextWriter writer, ulong value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint64(TextWriter writer, ulong value , bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_uint64(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return ulong.Parse(numberString);
        }

        public static ulong ReadT_uint64(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return ulong.Parse(numberString);
        }

        #endregion ---------------------------- READ/Write ulong-uint64----------------------------------


        #region ---------------------------- READ/Write byte----------------------------------
        

        public static void Write(TextWriter writer, byte value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_byte(TextWriter writer, byte value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_byte(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return byte.Parse(numberString);
        }
        public static byte ReadT_byte(string itemValue, ref int runIndex, int upperObjectEndIndex )
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return byte.Parse(numberString);
        }


        #endregion ---------------------------- READ/Write byte----------------------------------


        #region ---------------------------- READ/Write sbyte----------------------------------
        

        public static void Write(TextWriter writer, sbyte value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_sbyte(TextWriter writer, sbyte value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_sbyte(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return sbyte.Parse(numberString);
        }

        public static sbyte ReadT_sbyte(string itemValue,ref int runIndex, int upperObjectEndIndex )
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return sbyte.Parse(numberString);
        }

        #endregion ---------------------------- READ/Write sbyte----------------------------------


        #region ------------------------------ READ/WRITE deciaml -----------------------------------


        public static void Write(TextWriter writer, decimal value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_decimal(TextWriter writer, decimal value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write(value.S());
        }

        public static object Read_decimal(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberDecimal = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return decimal.Parse(numberDecimal);
        }
        public static decimal ReadT_decimal(string itemValue, ref int runIndex, int upperObjectEndIndex)  
        {
            var numberDecimal = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return decimal.Parse(numberDecimal);
        }


        #endregion ------------------------------ READ/WRITE deciaml -----------------------------------


        #region ---------------------------- READ/Write char----------------------------------


        public static void Write(TextWriter writer, char value)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.S());//  TO DO ??? if S()
            JNP.Write_Quote(writer);
        }

        public static void WriteT_char(TextWriter writer, char value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.S());
            JNP.Write_Quote(writer);
        }

        public static object Read_char(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var charString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return char. Parse(charString);
        }

        public static char ReadT_char(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var  charString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return  char.Parse( charString);
        }
        

        #endregion ---------------------------- READ/Write char----------------------------------


        #region ---------------------------- READ/Write bool----------------------------------
        
        public static void Write(TextWriter writer, bool value)
        {
            writer.Write( value ? Sym.TRUE : Sym.FALSE );            
        }
                
        public static void WriteT_bool(TextWriter writer, bool value, bool needToWriteTypeAQNameField = false)
        {
            writer.Write( value ? Sym.TRUE : Sym.FALSE );
        }

        public static object Read_bool(string itemValue, ref int runIndex, int upperObjectEndIndex ) 
        {            
            return JNP.PassNGetBoolValue(itemValue, ref runIndex);
        }
        

        public static bool ReadT_bool( string itemValue, ref int runIndex, int upperObjectEndIndex) 
        {
            return JNP.PassNGetBoolValue(itemValue, ref runIndex);            
        }


        #endregion ---------------------------- READ/Write bool----------------------------------


        #region ---------------------------- READ/Write DateTime----------------------------------
         

        static void write_date_value(TextWriter writer, DateTime dt)
        {            
            
            writer.Write(dt.Year.ToString("0000", NumberFormatInfo.InvariantInfo));
            writer.Write(Sym.DASH); // '-'
            writer.Write(dt.Month.ToString("00", NumberFormatInfo.InvariantInfo));
            writer.Write(Sym.DASH); //'-'
            writer.Write(dt.Day.ToString("00", NumberFormatInfo.InvariantInfo));
            writer.Write('T'); // strict ISO date compliance 
            writer.Write(dt.Hour.ToString("00", NumberFormatInfo.InvariantInfo));
            writer.Write(Sym.COLON);
            writer.Write(dt.Minute.ToString("00", NumberFormatInfo.InvariantInfo));
            writer.Write(Sym.COLON);
            writer.Write(dt.Second.ToString("00", NumberFormatInfo.InvariantInfo));
            if (JNP.Options.DateTimeMilliseconds)
            {
                writer.Write(Sym.DOTc); // '.'
                writer.Write(dt.Millisecond.ToString("000", NumberFormatInfo.InvariantInfo));
            }
        }



        public static void Write(TextWriter writer, DateTime value)
        {
            // datetime format standard : yyyy-MM-dd HH:mm:ss
            DateTime dt = value;
            if (JNP.Options.UseUTCDateTime)
                dt = value.ToUniversalTime();

            JNP.Write_Quote(writer);

            write_date_value(writer, dt);

            if (JNP.Options.UseUTCDateTime)
                writer.Write('Z');

            JNP.Write_Quote(writer);
        }

        public static void WriteT_DateTime(TextWriter writer, DateTime value, bool needToWriteTypeAQNameField = false)
        {
            // datetime format standard : yyyy-MM-dd HH:mm:ss
            DateTime dt = value;
            if (JNP.Options.UseUTCDateTime)
                dt = value.ToUniversalTime();

            JNP.Write_Quote(writer);

            write_date_value(writer, dt);

            if (JNP.Options.UseUTCDateTime)
                writer.Write('Z');

            JNP.Write_Quote(writer);
            
        }



        static DateTime CreateDateTime(string value)
        {
            bool utc = false;
            //                   0123456789012345678 9012 9/3
            // datetime format = yyyy-MM-ddTHH:mm:ss .nnn  Z
            int year;
            int month;
            int day;
            int hour;
            int min;
            int sec;
            int ms = 0;

            year = CreateInteger(value, 0, 4);
            month = CreateInteger(value, 5, 2);
            day = CreateInteger(value, 8, 2);
            hour = CreateInteger(value, 11, 2);
            min = CreateInteger(value, 14, 2);
            sec = CreateInteger(value, 17, 2);
            if (value.Length > 21 && value[19] == '.')
                ms = CreateInteger(value, 20, 3);

            if (value[value.Length - 1] == 'Z')
                utc = true;

            if (JNP.Options.UseUTCDateTime == false && utc == false)
                return new DateTime(year, month, day, hour, min, sec, ms);
            else
                return new DateTime(year, month, day, hour, min, sec, ms, DateTimeKind.Utc).ToLocalTime();
        }



        public static object Read_DateTime(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var stringDate = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);          
            return CreateDateTime(stringDate);//  DateTime.Parse(itemValue);
        }
        public static DateTime ReadT_DateTime(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var stringDate = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);            
            return CreateDateTime(stringDate);//  DateTime.Parse(itemValue);            
        }

        #endregion ---------------------------- READ/Write  DateTime----------------------------------


        #region ---------------------------- READ/Write  TimeSpan ----------------------------------


        public static void Write(TextWriter writer, TimeSpan value)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.Ticks.S());
            JNP.Write_Quote(writer);
        }

        public static void WriteT_TimeSpan(TextWriter writer, TimeSpan value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.Ticks.S());
            JNP.Write_Quote(writer);
        }

        public static object Read_TimeSpan(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            var numberLongTimeSpan = JNP.PassNGetNumberValue(itemValue, ref runIndex);            
            return new TimeSpan(long.Parse(numberLongTimeSpan));
        }

        public static TimeSpan ReadT_TimeSpan(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var numberLongTimeSpan = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            return new TimeSpan(long.Parse(numberLongTimeSpan));
        }

        #endregion ---------------------------- READ/Write  TimeSpan ----------------------------------
        

        #region ----------------------------- DateTimeOffset --------------------------


        public static void Write(TextWriter writer, DateTimeOffset value)
        {
            JNP.Write_Quote(writer);            
            writer.Write(value. Ticks.S());
            JNP.Write_Quote(writer);
        }

        public static void WriteT_DateTimeOffset(TextWriter writer, DateTimeOffset value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.Ticks.S());
            JNP.Write_Quote(writer);
        }

        public static object Read_DateTimeOffset(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var dateTimeOffsetString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return  DateTimeOffset.Parse(dateTimeOffsetString);
        }
        public static DateTimeOffset ReadT_DateTimeOffset(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var dateTimeOffsetString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return DateTimeOffset.Parse(dateTimeOffsetString);
        }
        

        #endregion --------------------- DateTimeOffset ---------------------------

        



        #region ---------------------------- READ/Write Guid -----------------------------------


        public static void Write(TextWriter writer, Guid value)
        {            
            JNP.Write_Quote(writer);
            writer.Write(value.S());
            JNP.Write_Quote(writer);
        }

        public static void WriteT_Guid(TextWriter writer, Guid value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.S());
            JNP.Write_Quote(writer);
        }

        public static object Read_Guid(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var stringGuid = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return Guid.Parse(stringGuid);
        }

        public static Guid ReadT_Guid(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var stringGuid = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return Guid.Parse(stringGuid);
        }


        #endregion ---------------------------- READ/Write Guid -----------------------------------


        #region -------------------------------READ/Write Uri -----------------------------------


        public static void Write(TextWriter writer, Uri value)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.OriginalString);
            JNP.Write_Quote(writer);
        }

        public static void WriteT_Uri(TextWriter writer, Uri value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value.OriginalString);
            JNP.Write_Quote(writer);
        }


        public static object Read_Uri(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            var stringUri = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return new Uri(stringUri);
        }

        public static Uri ReadT_Uri(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            var stringUri = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
            return new Uri(stringUri);
        }

        #endregion -------------------------------READ/Write Uri -----------------------------------


        #region ---------------------------- READ/Write BitArray -----------------------------------


        public static void Write(TextWriter writer, BitArray value)
        {
            JNP.Write_ArrayStart(writer);
            
            //Copy to bool array            
            bool[] bits = new bool[value.Length];
            value.CopyTo(bits, 0);

            for (int i = 0; i < bits.Length; i++)
            {
                writer.Write(bits[i] == true ? "1" : "0"); // "true" : "false");
                if (i < bits.Length - 1) writer.Write(Sym.COMMA);                
            }

            JNP.Write_ArrayEnd(writer);            
        }

        public static void WriteT_BitArray(TextWriter writer, BitArray value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_ArrayStart(writer); 
            //Copy to bool array            
            bool[] bits = new bool[value.Length];
            value.CopyTo(bits, 0);

            for (int i = 0; i < bits.Length; i++)
            {
                writer.Write(bits[i] == true ? "1" : "0"); // "true" : "false");
                if (i < bits.Length - 1) JNP.Write_Comma(writer);
            }

            JNP.Write_ArrayEnd(writer);
        }

        public static object Read_BitArray(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            // value must be like  - "[1,1,0,1,0,0]"

            if (JNP.PassNullValue(itemValue, ref runIndex))
            {  return null;
            }

            var itemsStringOnly = JNP.PassNGetArrayContentValue(itemValue, ref runIndex);
            var itemsInstrings = itemsStringOnly.SplitNoEntries(",");

            var result = new BitArray(itemsInstrings.Count);
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = (itemsInstrings[i] == "1" ? true : false); //  bool.Parse(itemsInstrings[i]);                
            }
            return result;
        }

        public static BitArray ReadT_BitArray(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            // value must be like  - "[1,1,0,1,0,0]"

            if (JNP.PassNullValue(itemValue, ref runIndex))
            {  return null;
            }

            var itemsStringOnly = JNP.PassNGetArrayContentValue(itemValue, ref runIndex);
            var itemsInstrings = itemsStringOnly.SplitNoEntries(",");

            var result = new BitArray(itemsInstrings.Count);
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = (itemsInstrings[i] == "1" ? true : false); //  bool.Parse(itemsInstrings[i]);                
            }
            return result;
        }


        #endregion ---------------------------- READ/Write BitArray -----------------------------------


        #region  -------------------------- READ/Write TypeInfoEx -----------------------------


        //public static void Write(TextWriter writer, TypeInfoEx value)
        //{
        //    // Write:   {TypeLN:"value",Assembly:"value"} 

        //    if (value.State == TypeFoundStateEn.NotFounded) throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");




        //    writer.Write(Json1.Sym.QUOTE);
        //    writer.Write(value.OriginalTypeAQName);
        //    writer.Write(Json1.Sym.QUOTE);
        //}


        //public static void WriteT_TypeInfoEx(TextWriter writer, TypeInfoEx value, bool needToWriteTypeAQNameField = false)
        //{
        //    if (value.State == TypeFoundStateEn.NotFounded) throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");
        //    writer.Write(Json1.Sym.QUOTE);
        //    writer.Write(value.OriginalTypeAQName);
        //    writer.Write(Json1.Sym.QUOTE);
        //}

        ///// <summary>
        ///// Read TypeInfoEx from string value
        ///// </summary>
        ///// <param name="itemValue"></param>
        ///// <param name="runIndex"></param>
        ///// <param name="upperObjectEndIndex"></param>
        ///// <returns></returns>
        //public static object Read_TypeInfoEx(string itemValue, ref int runIndex, int upperObjectEndIndex)
        //{
        //    var stringFullName = Json1.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
        //    return TypeInfoEx.GetByLN(stringFullName);
            
        //}

        ///// <summary>
        ///// Read TypeInfoEx from string value
        ///// </summary>
        ///// <param name="itemValue"></param>
        ///// <param name="runIndex"></param>
        ///// <param name="upperObjectEndIndex"></param>
        ///// <returns></returns>
        //public static TypeInfoEx ReadT_TypeInfoEx(string itemValue, ref int runIndex, int upperObjectEndIndex)
        //{
        //    var stringFullName = Json1.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
        //    return TypeInfoEx.GetByLN(stringFullName);
        //}





        /// <summary>
        /// Write TypeInfoEx  as Json string to TextWriter
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        public static void WriteT_TypeInfoEx(TextWriter writer
                                            , TypeInfoEx value
                                            , bool needToWriteTypeAQNameField = false
                                           )
        {
            // Write:   {"TypeLN":"value","Assembly":"value"} 
            if (value.State == TypeFoundStateEn.NotFounded)
                throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");

            writer.Write(Sym.OBJECT_START);
            JNP.Write_MemberName(writer, JNP.FLD_TYPELN);
            JNP.Write_String(writer, value.OriginalTypeLightName);
            JNP.Write_Comma(writer);
            JNP.Write_MemberName(writer, JNP.FLD_ASSEMBLY);
            JNP.Write_String(writer, value.AssemblyName);
            writer.Write( Sym.OBJECT_END);

        }


        public static TypeInfoEx ReadT_TypeInfoEx(string itemValue
                                                    , ref int runIndex
                                                    , int upperObjectEndIndex)
        {
            // Pass/ Read:   {"TypeLN":"value","Assembly":"value"} 

            JNP.PassObjectSTART(itemValue, ref runIndex);

            var tpLNKey = JNP.PassNGetMemberName(itemValue, ref runIndex);
            var tpLNValue = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            JNP.PassComma(itemValue, ref runIndex);

            var assemblyKey = JNP.PassNGetMemberName(itemValue, ref runIndex);
            var assemblyValue = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            JNP.PassObjectEND(itemValue, ref runIndex);

            return TypeInfoEx.GetByLN(tpLNValue, assemblyValue);

        }


        #endregion -------------------------- READ/Write TypeInfoEx -----------------------------

#if SERVER && UNSAFE

        #region ---------------------------- READ/Write  double----------------------------------


        public static unsafe void Write(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }

        public static unsafe void WriteT_double(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }


        public static unsafe object Read_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }

        public static unsafe double ReadT_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }


        #endregion ---------------------------- READ/Write  double----------------------------------


        #region ---------------------------- READ/Write  float----------------------------------


        public static unsafe void Write(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }

        public static unsafe void WriteT_float(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }


        public static unsafe object Read_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        public static unsafe float ReadT_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        #endregion ---------------------------- READ/Write  float----------------------------------


#else

        #region ---------------------------- READ/Write double----------------------------------


        public static void Write(TextWriter writer, double value)
        {
            ulong ulongVal = (ulong)BitConverter.DoubleToInt64Bits(value);
            writer.Write(ulongVal.S());
           
        }
         
        public static void WriteT_double(TextWriter writer, double value, bool needToWriteTypeAQNameField = false)
        {
            ulong ulongVal = (ulong)BitConverter.DoubleToInt64Bits(value);
            writer.Write(ulongVal.S());
        }

        public static object Read_double(string itemValue,ref int runIndex, int upperObjectEndIndex )
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            ulong ulongVal = ulong.Parse(numberString);// ReadVarint64(stream);
            return BitConverter.Int64BitsToDouble((long)ulongVal);             
        }

        public static double ReadT_double(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);
            ulong ulongVal = ulong.Parse(numberString);// ReadVarint64(stream);
            return BitConverter.Int64BitsToDouble((long)ulongVal);
        }

        #endregion ---------------------------- READ/Write double----------------------------------


        #region ---------------------------- READ/Write float----------------------------------


        public static void Write(TextWriter writer, float value)
        {
            Write(writer, (double)value);
            
        }

        public static void WriteT_float(TextWriter writer, float value, bool needToWriteTypeAQNameField = false)
        {
            Write(writer, (double)value);
        }
            

        public static object Read_float(string itemValue, ref int runIndex, int upperObjectEndIndex )
        {
            double v = (double)Read_double(itemValue,ref runIndex, upperObjectEndIndex);// default(double);

            return (float)v;
           
        }

        public static float ReadT_float(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            double v = (double)Read_double(itemValue,ref runIndex, upperObjectEndIndex);// default(double);

            return (float)v;
        }



        #endregion ---------------------------- READ/Write float----------------------------------

#endif



        #region ---------------------------- READ/Write short?-int16----------------------------------


        public static void Write(TextWriter writer, short? value)
        {

            WriteNullableAction<short>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int16(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_int16_nullable(TextWriter writer, short? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<short>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int16(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_int16_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            {  return null;  }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return short.Parse(numberString);
        }

        public static short? ReadT_int16_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return short.Parse(numberString);
            
        }
        

        #endregion ---------------------------- READ/Write short?----------------------------------


        #region ---------------------------- READ/Write ushort?-ushort ----------------------------------
        

        public static void Write(TextWriter writer, ushort? value)
        {

            WriteNullableAction<ushort>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint16(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_uint16_nullable(TextWriter writer, ushort? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<ushort>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint16(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint16_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return ushort.Parse(numberString);            
        }


        public static ushort? ReadT_uint16_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return ushort.Parse(numberString);                      
        }


        #endregion ---------------------------- READ/Write ushort?-ushort ----------------------------------


        #region ---------------------------- READ/Write int?-int32----------------------------------
        

        public static void Write(TextWriter writer, int? value)
        {

            WriteNullableAction<int>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int32(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_int32_nullable(TextWriter writer, int? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<int>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int32(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_int32_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return int.Parse(numberString);
            
        }

        public static int? ReadT_int32_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return int.Parse(numberString);
             
        }


        #endregion ---------------------------- READ/Write int?-int32 ----------------------------------


        #region ---------------------------- READ/Write uint?-uint32 ----------------------------------
        
        public static void Write(TextWriter writer, uint? value)
        {

            WriteNullableAction<uint>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint32(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_uint32_nullable(TextWriter writer, uint? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<uint>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint32(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint32_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex )
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return uint.Parse(numberString);
             
        }


        public static uint? ReadT_uint32_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex )
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return uint.Parse(numberString);
            
        }
        
        #endregion ---------------------------- READ/Write uint?-uint32 ----------------------------------


        #region ---------------------------- READ/Write long?-int64----------------------------------
        

        public static void Write(TextWriter writer, long? value)
        {
            WriteNullableAction<long>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int64(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_int64_nullable(TextWriter writer, long? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<long>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int64(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_int64_nullable(string itemValue, ref  int  runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return long.Parse(numberString);
             
        }


        public static long? ReadT_int64_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return long.Parse(numberString);
             
        }
        

        #endregion ---------------------------- READ/Write long?-int64----------------------------------


        #region ---------------------------- READ/Write ulong?-uint64----------------------------------
        

        public static void Write(TextWriter writer, ulong? value)
        {

            WriteNullableAction<ulong>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint64(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_uint64_nullable(TextWriter writer, ulong? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<ulong>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint64(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint64_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return ulong.Parse(numberString);
            
        }

        public static ulong? ReadT_uint64_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return ulong.Parse(numberString);
                       
        }

        #endregion ---------------------------- READ/Write ulong?-uint64 ----------------------------------


        #region ------------------------------ READ/WRITE deciaml? -----------------------------------

        public static void Write(TextWriter writer, decimal? value)
        {

            WriteNullableAction<decimal>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_decimal(txtwrtr, vl.Value); }
                             );

        }



        public static void WriteT_decimal_nullable(TextWriter writer, decimal? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<decimal>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_decimal(txtwrtr, vl.Value); }
                             );

        }



        public static object Read_decimal_nullable(string itemValue,ref int runIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return decimal.Parse(numberString);
                      
        }


        public static decimal? ReadT_decimal_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return decimal.Parse(numberString);
           
        }
        
        #endregion ------------------------------ READ/WRITE deciaml? -----------------------------------


        #region ---------------------------- READ/Write byte?----------------------------------


        public static void Write(TextWriter writer, byte? value)
        {

            WriteNullableAction<byte>( writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_byte( txtwrtr, vl.Value ); }
                             );

        }


        public static void WriteT_byte_nullable(TextWriter writer, byte? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<byte>( writer, value,
                                ( txtwrtr, vl) =>
                                { WriteT_byte( txtwrtr, vl.Value); }
                             );
        }



        public static object Read_byte_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return byte.Parse(numberString);
                        
        }


        public static byte? ReadT_byte_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return byte.Parse(numberString);            
        }


        #endregion ---------------------------- READ/Write byte?----------------------------------


        #region ---------------------------- READ/Write sbyte?----------------------------------



        public static void Write(TextWriter writer, sbyte? value)
        {

            WriteNullableAction<sbyte>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_sbyte(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_sbyte_nullable(TextWriter writer, sbyte? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<sbyte>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_sbyte(txtwrtr, vl.Value); }
                             );
        }



        public static object Read_sbyte_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return sbyte.Parse(numberString);                        
        }
        

        public static sbyte? ReadT_sbyte_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            return sbyte.Parse(numberString);
            
        }


        #endregion ---------------------------- READ/Write sbyte?----------------------------------


        #region ---------------------------- READ/Write char?----------------------------------


        public static void Write(TextWriter writer, char? value)
        {

            WriteNullableAction<char>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_char(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_char_nullable(TextWriter writer, char? value, bool needToWriteTypeAQNameField = false )
        {

            WriteNullableAction<char>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_char(txtwrtr, vl.Value); }
                             );
        }
        

        public static object Read_char_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            {   return null;   }

            var charString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            return char.Parse(charString);           
        }


        public static char? ReadT_char_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var charString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            return char.Parse(charString);
           
        }
        
        #endregion ---------------------------- READ/Write char?----------------------------------


        #region ---------------------------- READ/Write bool?----------------------------------


        public static void Write(TextWriter writer, bool? value)
        {

            WriteNullableAction<bool>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_bool(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_bool_nullable(TextWriter writer, bool? value, bool needToWriteTypeAQNameField = false)
        {

            WriteNullableAction<bool>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_bool(txtwrtr, vl.Value); }
                             );
        }


        public static bool? Read_bool_nullable(string inputSource, ref int runIndex, int upperObjectEndIndex)
        {  
            //check for NULL VALUE
            if (JNP.PassNullValue(inputSource, ref runIndex))
            {  return null;  }

            return JNP.PassNGetBoolValue(inputSource, ref runIndex);
        }


        public static bool? ReadT_bool_nullable(string inputSource, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(inputSource, ref runIndex))
            { return null; }

            return JNP.PassNGetBoolValue(inputSource, ref runIndex);            

        }


        #endregion ---------------------------- READ/Write bool?----------------------------------


        #region ---------------------------- READ/Write DateTime?----------------------------------


        public static void Write(TextWriter writer, DateTime? value)
        {
            WriteNullableAction<DateTime>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_DateTime(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_DateTime_nullable(TextWriter writer, DateTime? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<DateTime>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_DateTime(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_DateTime_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var datetimeString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            return CreateDateTime(datetimeString); 
        }


        public static DateTime? ReadT_DateTime_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var datetimeString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            return CreateDateTime(datetimeString);
                                                                      
        }



        #endregion ---------------------------- READ/Write  DateTime?----------------------------------


        #region ---------------------------- READ/Write  TimeSpan? ----------------------------------


        public static void Write(TextWriter writer, TimeSpan? value)
        {
            WriteNullableAction<TimeSpan>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_TimeSpan(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_TimeSpan_nullable(TextWriter writer, TimeSpan? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<TimeSpan>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_TimeSpan(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_TimeSpan_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            var longTicks = long.Parse(numberString);
                return new TimeSpan(longTicks);
             
        }


        public static TimeSpan? ReadT_TimeSpan_nullable(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            var longTicks = long.Parse(numberString);
                return new TimeSpan(longTicks);
             
        }

        #endregion ---------------------------- READ/Write  TimeSpan? ----------------------------------


        #region ---------------------------- READ/Write Guid? -----------------------------------

        
        public static void Write(TextWriter writer, Guid? value)
        {
            WriteNullableAction<Guid>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_Guid(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_Guid_nullable(TextWriter writer, Guid? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<Guid>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_Guid(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_Guid_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var stringString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
             
            return new Guid(stringString);
             
        }


        public static Guid? ReadT_Guid_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var stringString = JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);

            return new Guid(stringString);
            
        }


        #endregion ---------------------------- READ/Write Guid? -----------------------------------



#if UNSAFE


        #region ---------------------------- READ/Write  float? ----------------------------------


        public static unsafe void Write(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }

      
        public static unsafe void WriteT_float_nullable(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }





        public static unsafe object Read_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        public static unsafe float? ReadT_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        #endregion ---------------------------- READ/Write  float? ----------------------------------


        #region ---------------------------- READ/Write  double? ----------------------------------


        public static unsafe void Write(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe void WriteT_double_nullable(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe object Read_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        public static unsafe double? ReadT_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        #endregion ---------------------------- READ/Write  double? ----------------------------------

#elif !UNSAFE

        #region ---------------------------- READ/Write double?----------------------------------
        

        public static void Write(TextWriter writer, double? value)
        {
            WriteNullableAction<double>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_double(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_double_nullable(TextWriter writer, double? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<double>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_double(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_double_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            ulong ulongVal = ulong.Parse(numberString);// ReadVarint64(stream);
                return BitConverter.Int64BitsToDouble((long)ulongVal);                
                      
        }


        public static double? ReadT_double_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            var numberString = JNP.PassNGetNumberValue(itemValue, ref runIndex);

            ulong ulongVal = ulong.Parse(numberString);// ReadVarint64(stream);
                return BitConverter.Int64BitsToDouble((long)ulongVal);             

        }


        #endregion ---------------------------- READ/Write double?----------------------------------


        #region ---------------------------- READ/Write float?----------------------------------


        public static void Write(TextWriter writer, float? value)
        {
            WriteNullableAction<float>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_float(txtwrtr, vl.Value); }
                             );
        }



        public static void WriteT_float_nullable(TextWriter writer, float? value, bool needToWriteTypeAQNameField = false)
        {
            WriteNullableAction<float>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_float(txtwrtr, vl.Value); }
                             );
        }



        public static object Read_float_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            double v = (double)Read_double_nullable(itemValue, ref runIndex, upperObjectEndIndex);
            return (float)v;
        }
        

        public static float? ReadT_float_nullable(string itemValue,ref int runIndex, int upperObjectEndIndex )
        {
            double v = (double)Read_double_nullable(itemValue, ref runIndex, upperObjectEndIndex);
            return (float)v;
        }


        #endregion ---------------------------- READ/Write float?----------------------------------

#endif




#if (NET45 || SL5 || WP81)  //&& !UNSAFE  //  string

        #region ---------------------------- READ/Write string ----------------------------------


        public static void Write(TextWriter writer, string value)
        {
            JNP.Write_Quote(writer);
               writer.Write(value);
            JNP.Write_Quote(writer);
        }

        public static void WriteT_string(TextWriter writer, string value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_Quote(writer);
            writer.Write(value);
            JNP.Write_Quote(writer);
        }

        public static object Read_string(string itemValue,ref int runIndex, int upperObjectEndIndex)
        { //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            return JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);            
            
        }

        public static string ReadT_string(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            //check for NULL VALUE
            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null; }

            return JNP.PassNGetStringContentOnlyValue(itemValue, ref runIndex);
                       
        }
        
        #endregion ---------------------------- READ/Write string ----------------------------------

#elif UNSAFE
        #region ---------------------------- READ/Write string ----------------------------------

        [ThreadStatic]
        static StringHelper s_stringHelper;

        public unsafe static void Write(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
        

         public unsafe static void WriteT_string(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
      

        public static object Read_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }


        
        public static string ReadT_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }

        #endregion ---------------------------- READ/Write string ----------------------------------
#endif


        #region ---------------------------- READ/Write byte[] ----------------------------------
        

        public static void Write(TextWriter writer, byte[] value)
        {            
            JNP.Write_ArrayStart(writer);
           
            for (int i = 0; i < value.Length; i++)
            {
                writer.Write(value[i].S() ); // "true" : "false");
                if (i < (value.Length - 1) ) writer.Write(",");
            }
            JNP.Write_ArrayEnd(writer);
        }

        public static void WriteT_byteArray(TextWriter writer, byte[] value, bool needToWriteTypeAQNameField = false)
        {
            JNP.Write_ArrayStart(writer);

            for (int i = 0; i < value.Length; i++)
            {
                writer.Write(value[i].S()); // "true" : "false");
                if (i < (value.Length - 1)) writer.Write(",");
            }

            JNP.Write_ArrayEnd(writer);
        }

        public static object Read_byteArray(string itemValue,ref int runIndex, int upperObjectEndIndex)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            // NULL or Value

            if (JNP.PassNullValue(itemValue, ref runIndex))
            { return null;
            }

            var itemsStringOnly = JNP.PassNGetArrayContentValue(itemValue, ref runIndex);
            var itemsInstrings = itemsStringOnly.SplitNoEntries(",");

            var result = new byte[itemsInstrings.Count];
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = byte.Parse(itemsInstrings[i]);           
            }
            return result;
        }

        public static byte[] ReadT_byteArray(string itemValue, ref int runIndex, int upperObjectEndIndex)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            // value must be like  - "[1,1,0,1,0,0]"
            // NULL or Value

            if (JNP.PassNullValue(itemValue, ref runIndex))
            {  return null;
            }

            var itemsStringOnly = JNP.PassNGetArrayContentValue(itemValue, ref runIndex);
            var itemsInstrings = itemsStringOnly.SplitNoEntries(",");

            var result = new byte[itemsInstrings.Count];
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = byte.Parse(itemsInstrings[i]);
            }
            return result;

        }


        #endregion ---------------------------- READ/Write byte[] ----------------------------------







    }

}





#region --------------------------- GARBAGE ----------------------------------



//static void ReadWithBufferedStream()
//{
//    using (var ms = new MemoryStream())
//    {
//        BufferedStreamX bs = new BufferedStreamX(ms);

//    }

//}



#region ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------

//static uint EncodeZigZag32(int n)
//{
//    return (uint)((n << 1) ^ (n >> 31));
//}

//static ulong EncodeZigZag64(long n)
//{
//    return (ulong)((n << 1) ^ (n >> 63));
//}

//static int DecodeZigZag32(uint n)
//{
//    return (int)(n >> 1) ^ -(int)(n & 1);
//}

//static long DecodeZigZag64(ulong n)
//{
//    return (long)(n >> 1) ^ -(long)(n & 1);
//}

//static uint ReadVarint32(Stream stream)
//{
//    int result = 0;
//    int offset = 0;

//    for (; offset < 32; offset += 7)
//    {
//        int b = stream.ReadByte();
//        if (b == -1)
//            throw new EndOfStreamException();

//        result |= (b & 0x7f) << offset;

//        if ((b & 0x80) == 0)
//            return (uint)result;
//    }

//    throw new InvalidOperationException();
//}

//static void WriteVarint32(Stream stream, uint value)
//{
//    for (; value >= 0x80u; value >>= 7)
//        stream.WriteByte((byte)(value | 0x80u));

//    stream.WriteByte((byte)value);
//}

//static ulong ReadVarint64(Stream stream)
//{
//    long result = 0;
//    int offset = 0;

//    for (; offset < 64; offset += 7)
//    {
//        int b = stream.ReadByte();
//        if (b == -1)
//            throw new EndOfStreamException();

//        result |= ((long)(b & 0x7f)) << offset;

//        if ((b & 0x80) == 0)
//            return (ulong)result;
//    }

//    throw new InvalidOperationException();
//}

//static void WriteVarint64(Stream stream, ulong value)
//{
//    for (; value >= 0x80u; value >>= 7)
//        stream.WriteByte((byte)(value | 0x80u));

//    stream.WriteByte((byte)value);
//}



#endregion ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------




#endregion --------------------------- GARBAGE ----------------------------------