﻿using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Net;
using System;
using System.IO;

namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// Serialization Delegates Defenitions 
    /// </summary>
    public static class SDJson
    {


#if NET45 && DEBUG

        //Debug Serialize:   Action<Stream, T, TypeProcessor<T>>
        //Debug Deserialize: Func<Stream, bool, TypeProcessor<T>, object>

        //public delegate void DebugSerialize<T>(Stream stream, T instance, ITypeSerializer<T> tpSerializer);
        //public delegate object DebugDeserialize<T>(Stream stream, bool readedTypeID = false, ITypeSerializer<T> tpSerializer = null);        


        #region --------------------------------- [DEBUG] -  T BASED DELEGATES   ----------------------------------

        //[Conditional("DEBUG")]
        public delegate void SerializeToStreamTDbg<T>(Stream stream, T instance, ITypeSerializerJson<T> tpSerializer);
        public delegate T DeserializeFromStreamTDbg<T>(Stream stream, bool readedTypeID = false, ITypeSerializerJson<T> tpSerializer = null);


        public delegate void SerializeToBinWriterTDbg<T>(BinaryWriter bWriter, T instance, ITypeSerializerJson<T> tpSerializer);
        public delegate T DeserializeFromBinReaderTDbg<T>(BinaryReader bReader, bool readedTypeID = false, ITypeSerializerJson<T> tpSerializer= null);


        #endregion --------------------------------- [DEBUG] -  T BASED DELEGATES   ----------------------------------


#endif



        #region --------------------------  OBJECT BASED DELEGATES ---------------------------------------

        public delegate string SerializeToString(TextWriter stringWriter, object instance);
        public delegate object DeserializeFromString(string itemValue, Type type);



        public delegate void SerializeToStream(object instance, Stream stream);
        public delegate object DeserializeFromStream(Type type, Stream stream); // Stream stream, bool readedTypeID = false

   
        public delegate byte[] SerializeToByteArray(object instance);
        public delegate object DeserializeFromByteArray(byte[] serializedData, Type type );
        
        public delegate void SerializeToBinWriter(BinaryWriter bWriter, object instance);
        public delegate object DeserializeFromBinReader(BinaryReader bReader, bool readedTypeID = false);

        #endregion --------------------------  OBJECT BASED DELEGATES ---------------------------------------



        #region  ---------------------------------   T BASED DELEGATES ----------------------------------

        public delegate void SerializeToStringT<T>(TextWriter stringWriter, T instance, bool needToWriteTypeAQNameField = false );
        public delegate void SerializeToStringTDbg<T>(TextWriter stringWriter, T instance, bool needToWriteTypeAQNameField = false, ITypeSerializerJson<T> tpSerializer =null);


        public delegate string EatValueByJsonValueType(string inputSource
                                                    , ref int runIndex);

        public delegate T DeserializeFromStringT<T>(  string stringJson
                                                    , ref int runIndex
                                                    , int upperObjectEndIndex
                                                    );  

        public delegate T DeserializeFromStringTDbg<T>(string itemValue, ref int runIndex, ITypeSerializerJson<T> tpSerializer = null); 



        public delegate void SerializeToStreamT<T>(Stream stream, T instance);
        public delegate T DeserializeFromStreamT<T>(Stream stream, bool readedTypeID = false);



        public delegate void SerializeToBinWriterT<T>(BinaryWriter bWriter, T instance);
        public delegate T DeserializeFromBinReaderT<T>(BinaryReader bReader, bool readedTypeID = false);


        #endregion ---------------------------------   T BASED DELEGATES ----------------------------------



        #region ----------------------------- DCMessage BASED DELEGATES --------------------------------------
        
        public delegate void SerializeMsgToStream(Stream stream, DCMessage message);

        public delegate DCMessage DeserializeMsgFromStream(Stream stream, bool readedTypeID = false);
        
        #endregion ----------------------------- DCMessage BASED DELEGATES --------------------------------------


        //public delegate void PackMessage(DCMessage message);
        //public delegate void UnpackMessage(ref DCMessage message);


        //public delegate void PackMessage2(DCMessage2 message);
        //public delegate void UnpackMessage2(ref DCMessage2 message);



    }
}
