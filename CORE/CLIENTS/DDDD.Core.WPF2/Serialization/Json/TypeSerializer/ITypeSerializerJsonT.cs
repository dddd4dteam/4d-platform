﻿using System;
using System.IO;
using System.Linq.Expressions;


using DDDD.Core.Threading;
using DDDD.Core.Reflection;


namespace DDDD.Core.Serialization.Json
{


    /// <summary>
    /// Type Serializer Generic interface 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITypeSerializerJson<T> : ITypeSerializerJson
    {      
        
        /// <summary>
        ///Type Accessor{T} 
        /// </summary>
        ITypeAccessor<T> AccessorT { get; }       
        
        /// <summary>
        /// Lazy Func to create default value based on type kind: 1 -class; for enum; for struct; 2 - IDictionary, IList 3 - Array with [1-4] dimension
        /// <para/> When we get LA_CreateDefaultValue.Value first time - we choosing and precompiling fitted for T  DefaultValueFunc.
        /// </summary>
        LazyAct<Func<object[], T>> LA_CreateDefaultValue { get; }       
        
        
        //LazyAct<Expression<SDJson.SerializeToStreamT<T>>> LA_Expression_SerializeToStreamTHandler { get;  }
        //LazyAct<SDJson.SerializeToStreamT<T>> LA_SerializeToStreamTHandler { get; }

        //LazyAct<Expression<SDJson.DeserializeFromStreamT<T>>> LA_Expression_DeserializeFromStreamTHandler { get; }
        //LazyAct<SDJson.DeserializeFromStreamT<T>> LA_DeserializeFromStreamTHandler { get;  }



        /// <summary>
        /// Raise Serialize To Stream {T} value Handler.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        //void RaiseSerializeToStreamTHandler(Stream ms, T itemValue);

        /// <summary>
        /// Raise Deserialize From Stream, returns {T} Handler.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        //T RaiseDeserializeFromStreamTHandler(Stream ms, bool readedTypeID = false);

        /// <summary>
        /// Set Serialize To Stream {T} Handlers
        /// </summary>
        /// <param name="serializeToStreamTHandler"></param>
        /// <param name="deserializeFromStreamBoxedHandler"></param>
        //void SetSerializeToStreamTHandlers(SDJson.SerializeToStreamT<T> serializeToStreamTHandler, SDJson.DeserializeFromStreamT<T> deserializeFromStreamBoxedHandler);
        

        void RaiseSerializeToStringTHandler(
                                    TextWriter writer
                                    , T itemValue
                                    ,bool  needToWriteTypeAQName = false
                                    );


        T RaiseDeserializeFromStringTHandler(
                                    string jsonstring
                                    , ref int runIndex
                                    , int upperObjectEndIndex = -1
                                    );
        

        void SetSerializeToStringTHandlers(SDJson.SerializeToStringT<T> serializeToStringTHandler, SDJson.DeserializeFromStringT<T> deserializeFromStringBoxedHandler);
        

    }
}



