﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection.Emit;

using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Serialization.Binary;

namespace DDDD.Core.Serialization.Json
{
    
    


    /// <summary>
    ///  Custom Type serializer - i.e. the purpose of this concept is possibility to build and run [serialize/deserialize] handlers
    /// </summary>
    public interface ITypeSerializerJson
    {
      

        //void SetSerializeToStreamTHandlers(Delegate serializeToStreamTHandler, Delegate deserializeFromStreamTHandler);


        /// <summary>
        /// Target Type is typeof(T) in ITypeSerializer{T} . Really is reference to [Accessor.TAccess] property path
        /// </summary>
        Type TargetType { get; }


        /// <summary>
        /// How this value should be written to writer and Eaten from  json string
        /// </summary>
        JsonValueTypeEn JsonValueType { get;}

        JsonValueTypeEn Arg1JsonValueType { get; }

        JsonValueTypeEn Arg2JsonValueType { get; }

        JsonValueTypeEn ElementJsonValueType { get; }




        /// <summary>
        /// Target Type's extended info. 
        /// </summary>
        TypeInfoEx TargetTypeEx { get; }

        /// <summary>
        /// Type Accessor 
        /// </summary>
        ITypeAccessor Accessor    { get; }

        /// <summary>
        /// Type Serializer Generator - generate  De/Serialize Func for current TargetType. 
        /// </summary>
        ITypeSerializerGeneratorJson TypeSerializeGenerator { get; }


        #region ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------


        /// <summary>
        /// Type ID - serialization ID in TYPE SET Dictionary
        /// </summary>
        int TypeID      { get; }
        

        /// <summary>
        /// Get current data item type's ID from current TypeSetSerializer 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        //int GetElementTypeID(object element);


        /// <summary>
        /// If Element with such elementID  is 4DPrimitive or Enum Type
        /// </summary>
        /// <param name="elementID"></param>
        /// <returns></returns>
        //bool IsElement_EnumOrPrimitive(ushort elementID);


        #endregion ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------
        

        #region ----------------------------RUNTIME - Sub-Types IDs  in current SetSerializer context ----------------------------------------

        /// <summary>
        /// Runtime TargetType's Sub-Type IDs. 
        /// </summary>
        RuntimeIDs RuntimeIDents { get; }


        /// <summary>
        /// Initing TypeSerializer sub Types IDs -  EnumUnderlyingTypeID, ArrayElementTypeID ,  Arg1TypeID, Arg2TypeID - we getting IDs from current TypeSetserializer IDs Dictionary.
        ///  on the Serialize moment only / not on AddKnownType moment.
        /// </summary>
        void InitLazyRuntimeIDs();

        #endregion ----------------------------RUNTIME - Sub-Types IDs  in current SetSerializer context ----------------------------------------



        /// customization options for TAccessor members: IgnoreMembers , OnlyMembers, Selector - when they will be changed then TAccessor will be rebuilded
        #region --------------------------------- CUSTOMIZATION of ACCESSOR MEMBERS ----------------------------------------------

        /// <summary>
        /// Ignore Members- means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        List<string> IgnoreMembers { get; }

        /// <summary>
        /// Only Members - means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        List<string> OnlyMembers { get; }


        /// <summary>
        /// Selector defines strategy of Type.Members collecting. It's predefined BindingFlags combinations and other custom strategy options -like IgnoreMembers, OnlyMembers and Custom BindingFlags. 
        /// </summary>
        TypeMemberSelectorEn Selector { get; }

        #endregion --------------------------------- CUSTOMIZATION of ACCESSOR MEMBERS ----------------------------------------------
        



        //////////////////
        ////    ----- OWN SERIALIZATION - ONLY BOXED Handlers( others in ITypeSerializer{T})
        //////////////////
        

        void RaiseSerializeToStringBoxedHandler(TextWriter writer, object itemValue, bool needToWriteTypeAQName = false);


        object RaiseDeserializeFromStringBoxedHandler(
            string jsonstring
            ,ref int runIndex
            , int upperObjectEndIndex = -1
            );

        //////////////////
        ////    ----- ELEMENT SERIALIZATION - ONLY BOXED Handlers( others in ITypeSerializer{T})
        //////////////////
        void RaiseElementSerializeToStringBoxedHandler(int targetType, TextWriter writer, object itemValue, bool needWriteTypeAQName = false);

         




#if NET45 || NET46

        void BuildDynamicTypeAccessor(TypeBuilder tpBiulder);      

#endif



    }


}


#region ----------------------------------- GARBAGE -----------------------------

//object CreateDefaultValue(int? collectionLengt = 0);
//void SetSerializeToStreamHandlers<T>(SD.SerializeToStreamT<T> serializeToStreamTHandler, SD.DeserializeFromStream deserializeFromStreamBoxedHandler);       
//void SetStreamBoxedHandlers(Delegate serializeToStreamBoxedHandler, Delegate deserializeFromStreamBoxedHandler);

///// <summary>
///// Set Serializer expressions generator - we are talking about SERIALIZE/DESERIALIZE expressions for target Accessor.TAccess
///// </summary>
///// <param name="generator"></param>
//void SetSerializerGenerator(ITypeSerializerGenerator generator);

//TypeSerializerStateEn State { get; }
//ITypeSerializerGenerator SerializerGenerator { get; }

#endregion ----------------------------------- GARBAGE -----------------------------

