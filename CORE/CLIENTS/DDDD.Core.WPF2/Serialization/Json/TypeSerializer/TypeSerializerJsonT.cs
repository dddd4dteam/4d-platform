﻿using System;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;

using DDDD.Core.IO;
using DDDD.Core.Threading;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;


namespace DDDD.Core.Serialization.Json
{





    /// <summary>
    /// Type Serializer - serializer for {T} 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypeSerializerJson<T> : ITypeSerializerJson<T>
    {

        #region ------------------------------- CTOR ---------------------------------

        TypeSerializerJson()//ushort typeID   ITypeSetSerializer2 setSerializer
        {
            Initialize( ); //  setSerializer
        }
        
        #endregion ------------------------------- CTOR ---------------------------------


        const BindingFlags DefaultMembersBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        const BindingFlags DefaultMembersBindingNoBase = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;

        private static readonly object _locker = new object();

        #region ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------

        /// <summary>
        /// Serialization TypeID
        /// </summary>
        public int TypeID { get; private set; }

 

        public RuntimeIDs RuntimeIDents
        { get; private set; }
            
         


        /// <summary>
        /// Updating TypeSerializer2 sub Types IDs -  EnumUnderlyingTypeID, ArrayElementTypeID ,  Arg1TypeID, Arg2TypeID - we getting IDs from current TypeSetserializer IDs Dictionary.
        /// </summary>
        public void InitLazyRuntimeIDs()
        {                           
            RuntimeIDents = new RuntimeIDs();
         
            if (TargetTypeEx.ArrayElementType != null)
            {
                RuntimeIDents.ArrayElementTypeID = Tps.GetTypeID(TargetTypeEx.ArrayElementType);
            }
            if (TargetTypeEx.EnumUnderlyingType != null)
            {
                RuntimeIDents.EnumUnderlyingTypeID = Tps.GetTypeID(TargetTypeEx.EnumUnderlyingType);
            }
            if (TargetTypeEx.Arg1Type != null)
            {
                RuntimeIDents.Arg1TypeID = Tps.GetTypeID(TargetTypeEx.Arg1Type);
            }
            if (TargetTypeEx.Arg2Type != null)
            {
                RuntimeIDents.Arg2TypeID = Tps.GetTypeID(TargetTypeEx.Arg2Type);
            }
        }

        #endregion ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------


        /// <summary>
        /// Target Type is typeof(T) 
        /// </summary>
        public Type TargetType
        { get; private set; }

        /// <summary>
        ///Lazy Target Type's extended info. 
        /// </summary>
        public TypeInfoEx TargetTypeEx
        { get; private set; }


        /// <summary>
        /// Type Members Accessor of TargetType
        /// </summary>
        public ITypeAccessor Accessor
        { get { return AccessorT; } }


        /// <summary>
        /// Type Members Accessor of {T} type
        /// </summary>
        public ITypeAccessor<T> AccessorT
        { get; private set; }
        


        /// <summary>
        /// Selector defines strategy of Type.Members collecting. It's predefined BindingFlags combinations and other custom strategy options -like IgnoreMembers, OnlyMembers and Custom BindingFlags. 
        /// </summary>
        public TypeMemberSelectorEn Selector
        { get; private set; } 

        /// <summary>
        /// Ignore Members- means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer2.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        public List<string> IgnoreMembers
        { get; private set; }


        /// <summary>
        /// Only Members- means other that all other members will be gathered into Members of current TypeSerializer2.
        /// <para/>  Used when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        public List<string> OnlyMembers
        { get; private set; }


         

#region -------------------------------- CREATION -----------------------------------


        private void Initialize()
        {
            //SetSerializer = TypeSetSerializer2.Current;// setSerializer;
            TargetType = typeof(T);            
            TargetTypeEx = TypeInfoEx.Get(TargetType);
            TypeID = TargetTypeEx.ID;

            InitLazyRuntimeIDs();
        }


        static ITypeSerializerJson CreateInternal(Type contract)// , ITypeSetSerializer2 setSerialier
        {
            var typeSerializer = typeof(TypeSerializerJson<>).MakeGenericType(contract);
            return TypeActivator.CreateInstanceTBase<ITypeSerializerJson>(typeSerializer);
          
        }


        /// <summary>
        /// Creation of TypeSerializer2 for  T-contract   with  T as first  arg(not generic arg)
        /// </summary>
        /// <param name="currentType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeSerializerJson Create(Type currentType
                                              //, ITypeSetSerializer setSerialier
                                              , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                              , BindingFlags propertiesBinding = BindingFlags.Default
                                              , BindingFlags fieldsBinding = BindingFlags.Default
                                              , List<string> onlyMembers = null
                                              , List<string> ignoreMembers = null
                                            )
        {
            var typeSerializer = CreateInternal(currentType) as TypeSerializerJson<T>;
            var typeInf = typeSerializer.TargetTypeEx;

            //TypeAccessor  Only dor Classses  AND STRUCTS ACCESSOR SHOULD BE CREATED
            if (  (typeInf.IsCustomClass && typeInf.IsAbstract == false)
                  || typeInf.IsCustomStruct  )
            {             
                var members = TypeAccessor.SelectMembers<T>(selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);                
                typeSerializer.AccessorT = TypeAccessor<T>.Get(true, members.ToArray());
            }
            

            typeSerializer.JsonValueType = JNP.GetJsonValueType(typeInf);
            //typeSerializer.EatJsonValue = Json1.GetEatValueHandlerByType(typeSerializer.JsonValueType);

            if (typeInf.Arg1Type.IsNotNull() )
            {
                var Arg1TypeInf = typeInf.Arg1Type.GetTypeInfoEx();
                typeSerializer.Arg1JsonValueType = JNP.GetJsonValueType(Arg1TypeInf);
            }

            if (typeInf.Arg2Type.IsNotNull())
            {
                var arg2TypeInf = typeInf.Arg2Type.GetTypeInfoEx();
                typeSerializer.Arg2JsonValueType = JNP.GetJsonValueType(arg2TypeInf);
            }

            if (typeInf.ArrayElementType.IsNotNull())
            {
                var arrayElementTypeInf = typeInf.ArrayElementType.GetTypeInfoEx();
                typeSerializer.ElementJsonValueType = JNP.GetJsonValueType( arrayElementTypeInf );
            }

           
            typeSerializer.TypeSerializeGenerator = TypeSerializeAnalyser.GetJsonTypeSerializerGeneratorForType(currentType); // JsonSerializer.GetTypeSerializerGeneratorByType
            

            // Expression_SerializeToStringTHandler
            typeSerializer.LA_Expression_SerializeToStringTHandler = LazyAct<Expression<SDJson.SerializeToStringT<T>>>.Create(typeSerializer.CreateExpression_SerializeToStringT, typeSerializer, null);
            // SerializeToStringTHandler
            typeSerializer.LA_SerializeToStringTHandler = LazyAct<SDJson.SerializeToStringT<T>>.Create(typeSerializer.Create_SerializeToStringT, typeSerializer, null);

            // Expression_DeserializeFromStringTHandler
            typeSerializer.LA_Expression_DeserializeFromStringTHandler = LazyAct<Expression<SDJson.DeserializeFromStringT<T>>>.Create(typeSerializer.CreateExpression_DeserializeToStringT, typeSerializer, null);
            // DeserializeFromStringTHandler
            typeSerializer.LA_DeserializeFromStringTHandler = LazyAct<SDJson.DeserializeFromStringT<T>>.Create(typeSerializer.Create_DeserializeFromStringT, typeSerializer, null);
            
            return typeSerializer;
        }





#endregion -------------------------------- CREATION -----------------------------------



        #region ------------------------- GENERATE / SET DIRECTLY  SERIALIZE/DESERIALIZE HANDLERS ------------------------



        /// <summary>
        /// Serialize Handlers Generator for  Contract
        /// </summary>
        public ITypeSerializerGeneratorJson TypeSerializeGenerator
        {
            get;
            private set;
        }



        #endregion------------------------- GENERATE / SET DIRECTLY  SERIALIZE/DESERIALIZE HANDLERS ------------------------


        #region -------------------------- PASS/WRITE VALUE WAY -----------------------------
      
        // Pass prepared delegate 
        // Write value delegate
        public JsonValueTypeEn JsonValueType { get; private set; }

        public JsonValueTypeEn Arg1JsonValueType { get; private set; }

        public JsonValueTypeEn Arg2JsonValueType { get; private set; }

        public JsonValueTypeEn  ElementJsonValueType { get; private set; }



        #endregion -------------------------- PASS/WRITE VALUE WAY -----------------------------


        #region -------------------------------- Object.HashCode ------------------------------

        /// <summary>
        /// Overrided object.GetHashCode.This instance HashCode is Accessor.TAccess.GetHashCode().
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return TypeID;
        }


        #endregion -------------------------------- Object.HashCode ------------------------------




        #region ------------------------------------ SERIALIZE/DESRIALIZE  HANDLERS --------------------------------


        ///// 
        ///  SerializeToStringTHandler
        ///// 
        public LazyAct<Expression<SDJson.SerializeToStringT<T>>> LA_Expression_SerializeToStringTHandler { get; private set; }
        public LazyAct<SDJson.SerializeToStringT<T>> LA_SerializeToStringTHandler { get; private set; }


        Expression<SDJson.SerializeToStringT<T>> CreateExpression_SerializeToStringT(object[] args)
        {
            return TypeSerializeGenerator.GenerateSerializeToString_WriteExpression<T>(this);
        }

        SDJson.SerializeToStringT<T> Create_SerializeToStringT(object[] args)
        {
            return LA_Expression_SerializeToStringTHandler.Value.Compile();
        }




        ///// 
        ///  DeserializeFromStringTHandler
        ///// 
        public LazyAct<Expression<SDJson.DeserializeFromStringT<T>>> LA_Expression_DeserializeFromStringTHandler { get; private set; }
        public LazyAct<SDJson.DeserializeFromStringT<T>> LA_DeserializeFromStringTHandler { get; private set; }


        Expression<SDJson.DeserializeFromStringT<T>> CreateExpression_DeserializeToStringT(object[] args)
        {
            return TypeSerializeGenerator.GenerateDeserializeFromString_ReadExpression<T>();
        }

        SDJson.DeserializeFromStringT<T> Create_DeserializeFromStringT(object[] args)
        {
            return LA_Expression_DeserializeFromStringTHandler.Value.Compile();
        }


                              


        public void RaiseSerializeToStringTHandler(TextWriter writer, T itemValue, bool needToWriteTypeAQName = false)
        {
            LA_SerializeToStringTHandler.Value(writer,  itemValue, needToWriteTypeAQName);
        }


        public void RaiseSerializeToStringBoxedHandler(TextWriter writer, object itemValue, bool needToWriteTypeAQName = false)
        {
            LA_SerializeToStringTHandler.Value(writer, (T)itemValue, needToWriteTypeAQName);
        }

       public object RaiseDeserializeFromStringBoxedHandler(
                                                 string jsonstring
                                               , ref int runIndex
                                               , int upperObjectEndIndex = -1
                                                )
        {
            return LA_DeserializeFromStringTHandler.Value(jsonstring, ref runIndex, upperObjectEndIndex);
        }

        public T RaiseDeserializeFromStringTHandler(
                                              string jsonstring
                                              , ref int runIndex
                                              , int upperObjectEndIndex = -1
                                              )
        {
            return LA_DeserializeFromStringTHandler.Value(jsonstring, ref runIndex, upperObjectEndIndex);
        }

        #endregion------------------------------------ SERIALIZE/DESRIALIZE  HANDLERS --------------------------------




        // ELEMENT - concept determines collection/array items or  structure's fields/properties
        #region -------------------------- SERIALIZE /DESERIALZIE ELEMENT - SWITCH  HANDLERS --------------------------------


        /// <summary>
        /// Raising Element Serialize to string for object value
        /// </summary>
        /// <param name="itemTypeID"></param>
        /// <param name="writer"></param>
        /// <param name="itemValue"></param>
        /// <param name="needToWriteTypeAQName"></param>
        public void RaiseElementSerializeToStringBoxedHandler(int itemTypeID, TextWriter writer, object itemValue, bool needToWriteTypeAQName = false)
        {
            JsonSerializer.GetTypeSerializer(itemTypeID)
                .RaiseSerializeToStringBoxedHandler(writer, itemValue, needToWriteTypeAQName);
        }


        #endregion  -------------------------- SERIALIZE /DESERIALZIE ELEMENT - SWITCH  HANDLERS --------------------------------



        #region ---------------------------------Utils: LA_CreateDefaultValue    -----------------------------------



        /// <summary>
        /// Lazy Func to create default value based on type kind: 1 -class; for enum; for struct; 2 - IDictionary, IList 3 - Array with [1-4] dimension
        /// <para/> When we get LA_CreateDefaultValue.Value first time - we choosing and precompiling fitted for T  DefaultValueFunc.          
        /// </summary>
        public LazyAct<Func<object[], T>> LA_CreateDefaultValue
        { get; } = LazyAct<Func<object[], T>>.Create(CreateDefaultvalueFuncT, null, null);

        /// <summary>
        /// Choose -what default value func fitted by this type and precompile it
        /// </summary>
        /// <returns></returns>
        static Func<object[], T> CreateDefaultvalueFuncT(object[] args)
        {
            var seriaizeCollectionType = typeof(T).GetBaseCollectionType();// GetTypeInfo();//

            if (seriaizeCollectionType == CollectionTypeEn.NotCollection)
            {
                // for class; for enum; for struct
                return TypeActivator.PrecompileAsNewExpressionT<T>();
            }
            else if ( seriaizeCollectionType == CollectionTypeEn.ObservableCollection )
            {
                // for ObservableCollection types
                return TypeActivator.PrecompileAsNewExpressionT<T>();
            }
            else if (seriaizeCollectionType == CollectionTypeEn.IDictionary
                    || seriaizeCollectionType == CollectionTypeEn.IList)//for collection types
            {
                return TypeActivator.PrecompileAsNewExpressionT<T>(); //WithLength
            }
            else if (seriaizeCollectionType == CollectionTypeEn.Array)
            {
                return TypeActivator.PrecompileAsNewArrayExpressionT<T>();//array dimensions
            }

            return null;
        }




        #endregion ---------------------------------Utils:  LA_CreateDefaultValue------






        #region ---------------------------------- SET SERIALIZETOSTRINGT HANDLERS ----------------------------------------


        /// <summary>
        /// SET OR CHANGE Lazy{T}.Values of serializeToStringTHandler   and deserializeFromStringTHandler
        /// </summary>
        /// <param name="serializeToStringTHandler"></param>
        /// <param name="deserializeFromStringTHandler"></param>
        public void SetSerializeToStringTHandlers(SDJson.SerializeToStringT<T> serializeToStringTHandler, SDJson.DeserializeFromStringT<T> deserializeFromStringTHandler)
        {
            LA_SerializeToStringTHandler.ResetValueDirectly(serializeToStringTHandler);
            LA_DeserializeFromStringTHandler.ResetValueDirectly(deserializeFromStringTHandler);
        }


        /// <summary>
        /// SET OR CHANGE Lazy{T}.Values of SerializeToStringTHandler  and DeserializeFromStringTHandler
        /// </summary>
        /// <param name="serializeToStringTHandler"></param>
        /// <param name="deserializeFromStringTHandler"></param>
        public void SetSerializeToStringTHandlers(Delegate serializeToStringTHandler, Delegate deserializeFromStringTHandler)
        {
            // static fields by default souldn't be included into de/serialization
            Validator.AssertTrue<InvalidOperationException>(
                              serializeToStringTHandler.GetType() != typeof(SDJson.SerializeToStringT<T>),
                               $"SDJson.SerializeToStringT Delegate Type mismatch for T-[{typeof(T).FullName}] ."
                               , typeof(T));

            Validator.AssertTrue<InvalidOperationException>(
                              deserializeFromStringTHandler.GetType() != typeof(SDJson.DeserializeFromStringT<T>),
                               $"SDJson.DeserializeFromStringT Delegate Type mismatch for T-[{typeof(T).FullName}] ."
                               , typeof(T));

            SetSerializeToStringTHandlers(serializeToStringTHandler as SDJson.SerializeToStringT<T>, deserializeFromStringTHandler as SDJson.DeserializeFromStringT<T>);
        }


        #endregion ---------------------------------- SET SERIALIZETOSTRINGT HANDLERS ----------------------------------------



#if NET45 /////// ----------------------- GENERATE DEBUG CODE -----------------------////////

        #region ----------------------- GENERATE DEBUG CODE -----------------------



        public void BuildDynamicTypeAccessor(TypeBuilder tpBiulder)
        {
            throw new NotImplementedException();
        }




        #endregion ----------------------- GENERATE DEBUG CODE -----------------------

#endif


    }
}



#region ------------------------------ GARBAGE -------------------------------



/// <summary>
/// Getting TypeSerializer2 from SetSerializer by typeID
/// </summary>
/// <typeparam name="TMember"></typeparam>
/// <param name="typeID"></param>
/// <returns></returns>
//internal protected ITypeSerializerJson<TMember> GetElementTypeSerializer<TMember>(ushort typeID)
//{
//    return JsonSerializer.GetTypeSerializer(typeID) as ITypeSerializerJson<TMember>;
//}

///// <summary>
///// Getting TypeSerializer2 from SetSerializer by typeID 
///// </summary>
///// <param name="typeID"></param>
///// <returns></returns>
//internal protected ITypeSerializerJson GetElementTypeSerializer(ushort typeID)
//{
//    return JsonSerializer.GetTypeSerializer(typeID);
//}

#region ----------------------------- MEMBERS SELECTION -------------------------------

///// <summary>
///// Get Binding by Type Members Selector 
///// </summary>
///// <param name="policy"></param>
///// <returns></returns>
//public static BindingFlags GetSelectorBinding(TypeMemberSelectorEn membersSelector)
//{

//    switch (membersSelector)
//    {
//        case TypeMemberSelectorEn.NotDefined:
//            {
//                return BindingFlags.Default;
//            }

//        case TypeMemberSelectorEn.Default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.DefaultNoBase:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public | BindingFlags.NonPublic |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.PublicOnly:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.OnlyMembers:
//            {
//                return (BindingFlags.Instance |
//                        BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.Custom:
//            {
//                return (BindingFlags.Default);
//            }
//        default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//    }

//}



//static List<string> SelectMembers<TTarget>(TypeMemberSelectorEn selector
//                        , BindingFlags propertiesBinding = BindingFlags.Default
//                        , BindingFlags fieldsBinding = BindingFlags.Default
//                        , List<string> onlyMembers = null
//                        , List<string> ignoreMembers = null)
//{
//    var resultMembers = new List<string>();
//    var targetType = typeof(TTarget).GetWorkingTypeFromNullableType();
//    //var targetType = typeof(TTarget);

//    var properties = new List<string>();
//    var fields = new List<string>();


//    if (targetType.IsICollectionType() || targetType.IsInterface || targetType == typeof(object) || targetType.Is4DPrimitiveType())
//    {
//        // not collecting members for this type kinds 
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.NotDefined)
//    {
//        throw new InvalidOperationException($"TypeAcessor's members selector can't be undefined in TypeserializerT[{targetType.FullName}] ");
//    }
//    else if (selector == TypeMemberSelectorEn.Default)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                      .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                      .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection 
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.DefaultNoBase)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBindingNoBase).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBindingNoBase).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.PublicOnly)
//    {
//        properties.AddRange(targetType.GetProperties(BindingFlags.Public).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(BindingFlags.Public).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.OnlyMembers)
//    {
//        //check all of elements exists
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        //only member  should be more than 0
//        if (onlyMembers == null || onlyMembers.Count == 0)
//        { throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers, then only members property can't be null or with [0] Count value  "); }

//        //check all of elements  exist in Properties or Fields
//        foreach (var item in onlyMembers)
//        {
//            if (properties.Contains(item) || fields.Contains(item))
//            {
//                throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers,  only member item [{item}]  was not found in Members of T[{targetType.Name}]");
//            }
//        }

//        resultMembers.AddRange(onlyMembers); // but will processing only members                
//    }
//    else if (selector == TypeMemberSelectorEn.Custom)
//    {
//        //only  custom binding should be as input parameter
//        //ignore items
//        properties.AddRange(targetType.GetProperties(fieldsBinding).Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(propertiesBinding).Select(fld => fld.Name));

//        foreach (var ignoreItem in ignoreMembers)
//        {
//            if (properties.Contains(ignoreItem)) { properties.Remove(ignoreItem); }
//            if (fields.Contains(ignoreItem)) { fields.Remove(ignoreItem); }
//        }

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);

//    }

//    //remove autofields <>k_BackingField
//    if (resultMembers.Count > 0)
//    {
//        resultMembers = resultMembers.Where(itm => !itm.Contains(">k__BackingField")).ToList();
//    }

//    //remove members which is read only // or instantiated once at creation




//    return resultMembers;
//}

#endregion ----------------------------- MEMBERS SELECTION -------------------------------




//////////////////////////////////////////////////////////////////////////

//#if DEBUG  //only  for Diagnostics

//            try
//            {
//                //for not sealed class, interface , nullable structs - 1 - types category           
//                var memberValue = AccessorT.GetMember<TMember>(member, instanceValue);                

//                var realTypeID = GetElementTypeID(memberValue); //correct member type id before transfer to element serialize
//                if (WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, realTypeID)) return;  // end - value was Null              

//                // custom structs and custom classes will write their ID in their private handlers
//                RaiseElementSerializeToStreamBoxedHandler(realTypeID, stream, memberValue);                
//            }
//            catch (Exception exc)
//            {
//                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializer2<T>)}.{nameof(WriteMemberToStream_NeedToSureInDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{member}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);                
//            }

//#elif RELEASE
//                //for not sealed class, interface , nullable structs - 1- types category           
//                var memberValue = AccessorT.GetMember<TMember>(member, instanceValue);

//                var realTypeID = GetElementTypeID(memberValue); //correct member type id before transfer to element serialize
//                if (WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, realTypeID)) return;  // end - value was Null              

//                RaiseElementSerializeToStreamBoxedHandler(realTypeID, stream, memberValue);

//#endif





//#if DEBUG
//            //only  for Diagnostics
//            var tType = typeof(T);
//            var tMemberType = typeof(TMember);
//            var memberName = member;
//            try
//            {
//                //for sealed classes or structs - 2 -types category

//                // even it TMember is sealed class  - the typeID need not to be changed - so we don't need write here  null ID - it'll be done in Element TypeSerializer2 
//                RaiseElementSerializeToStreamTHandler(typeID, ms, AccessorT.GetMember<TMember>(member, instanceValue));
//            }
//            catch (Exception exc)
//            {
//                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializer2<T>)}.{nameof(WriteMemberToStream_NeedNotToSureInDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{member}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);                
//            }

//#elif RELEASE

//             //for sealed classes or structs - 2 -types category

//             // even it TMember is sealed class  - the typeID need not to be changed - so we don't need write here  null ID - it'll be done in Element TypeSerializer2 
//             RaiseElementSerializeToStreamTHandler(typeID, ms, AccessorT.GetMember<TMember>(member, instanceValue));
//#endif



     




#region -------------------------------- COLLECTING AUTOITEMS  FROM PROPERTIES & FIELDS  --------------------------------------


///// <summary>
///// If FieldType is interface or enum we Add them 
///// </summary>
///// <param name="fields"></param>
///// <param name="typeSerializer"></param>
//private   void CollectAutoItemsFromFields(FieldInfo[] fields)  // ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var fld in fields)
//    {
//        //Type compareType = Tools.GetWorkingTypeFromNullableType(fld.FieldType);

//        ushort? typeId = 0;
//        //if ((typeId = JsonSerializer.ContainsTypeSerializer(fld.FieldType)) != null) continue;

//        //if (fld.FieldType.IsAutoAdding()) { TypeSetSerializer2.AddKnownType(fld.FieldType); }

//        if (typeof(IEnumerable).IsAssignableFrom(fld.FieldType)) continue;

//        //если это все же класс или структура, но только не инам -то он уже должен быть в словаре Контрактов иначе это ошибка
//        if ((fld.FieldType.IsClass || fld.FieldType.IsValueType) && !fld.FieldType.IsEnum)
//        {
//            Validator.NotNullDbg(typeId,
//                contextType: typeof(TypeSerializerJson<>), contextOperation: nameof(CollectAutoItemsFromFields),
//                contextPoint: $"Member Analyze error: Service Serializer Dictionary doesn't contain FieldType  [{fld.FieldType}]. Look at the contract [{ TargetType.FullName}] in field [{fld.Name}]");
//        }
//    }
//}


///// <summary>
///// If PropertyType is interface or enum we Add them
///// </summary>
///// <param name="properties"></param>     
//private  void CollectAutoItemsFromProperties(PropertyInfo[] properties)  //  ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var prp in properties)
//    {
//        ushort? typeId;
//        //if ((typeId = JsonSerializer.ContainsTypeSerializer(prp.PropertyType)) != null) continue;

//        //if (prp.PropertyType.IsAutoAdding()) TypeSetSerializer2.AddKnownType(prp.PropertyType);     // typeSerializer.SetSerializer. 

//        if (typeof(IEnumerable).IsAssignableFrom(prp.PropertyType)) continue;

//        //если это все же класс или структура, но не инам - то они уже должен быть в словаре Контрактов иначе это ошибка
//        if ((prp.PropertyType.IsClass || prp.PropertyType.IsValueType) && !prp.PropertyType.IsEnum)
//        {
//            Validator.NotNullDbg(typeId,
//                contextType: typeof(TypeSerializerJson<>), contextOperation: nameof(CollectAutoItemsFromProperties),
//                contextPoint: $"Member Analyze error:  Serializer Dictionary doesn't contain PropertyType  [{prp.PropertyType.FullName}]. Look at the contract [{TargetType.FullName}] in property [{prp.Name}]");
//        }

//    }
//}


#endregion -------------------------------- COLLECTING AUTOITEMS  FROM PROPERTIES & FIELDS  --------------------------------------


///// <summary>
///// Init SerializeGenerator Handlers for Contract  
///// </summary>
///// <param name="serializeGenerator"></param>
//internal void SetSerializeGenerator(ITypeSerializerGenerator serializeGenerator)
//{
//    TypeSerializeGenerator = serializeGenerator;
//}

#endregion ------------------------------ GARBAGE -------------------------------

