﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Serialization.Json
{
    public class JsonOptions
    {
               

        /// <summary>
        /// Serialize DateTime milliseconds i.e. yyyy-MM-dd HH:mm:ss.nnn (default = false)
        /// </summary>
        public bool DateTimeMilliseconds
        { get; set; } = false;


        /// <summary>
        /// Use the UTC date format (default = True)
        /// </summary>
        public bool UseUTCDateTime
        { get; set; } = true;


        /// <summary>
        /// Use light TypeName - TypeNameTS instead of full Type.AssemblyQualifiedName
        /// </summary>
        public bool UseTypeLightName
        { get;  set; } = true;

        /// <summary>
        /// Check tab/new line/ return symbols(\t,\n,\r, space) in json string or not.
        /// <para/> If not - means that we have no such format symbols in json string aptiori
        /// i.e. its as usually  runtime json strings that network services use.
        /// </summary>
        public bool UseFormatSymbols
        { get;  set; } = false;

         
#if NET45 /////// --------------------------- WRITE DEBUG CODE ASSEMBLY --------------------------------///////


        /// <summary>
        /// If this flag is true, after each BuildProcessing() call, we'll write debugging code assembly.
        /// By Default it's false. But this feature can't be used in real-time parallel work, be carefull.
        /// </summary>
        internal static bool UseGenerationOfDebugCodeAssembly
        { get; set; }


#endif
       




        ///// ---------- NOT IMPLEMENTED FUTURE FEATURES -----------------


        /// <summary>
        /// Serialize null values to the output (default = True)
        /// </summary>
        //public   bool SerializeNullValues
        //{ get; set; } = true;

        /// <summary>
        /// If you have parametric and no default constructor for you classes (default = False)            /// 
        /// IMPORTANT NOTE : If True then all initial values within the class will be ignored and will not be set
        /// </summary>
        //public  bool ParametricConstructorOverride
        //{ get; set; } = false;


        /// <summary>
        /// Maximum depth for circular references in inline mode (default = 20)
        /// </summary>
        //public byte SerializerMaxDepth
        //{ get; set; } = 20; // public static int MAX_DEPTH = 20;



        /// <summary>
        /// Use escaped unicode i.e. \uXXXX format for non ASCII characters (default = True)
        /// </summary>
        //public  bool UseEscapedUnicode
        //{ get; set; }= true;

        /// <summary>
        /// Output string key dictionaries as "k"/"v" format (default = False) 
        /// </summary>
        //public  bool KVStyleStringDictionary
        //{ get; set; }= false;

        /// <summary>
        /// Output Enum values instead of names (default = False)
        /// </summary>
        //public  bool UseValuesOfEnums
        //{ get; set; }= false;

        /// <summary>
        /// Use the optimized fast Dataset Schema format (default = True)
        /// </summary>
        //public  bool UseOptimizedDatasetSchema
        //{ get; set; }= true;

        /// <summary>
        /// Use the fast GUID format (default = True)
        /// </summary>
        //public  bool UseFastGuid
        //{ get; set; }= true;



    }
}
