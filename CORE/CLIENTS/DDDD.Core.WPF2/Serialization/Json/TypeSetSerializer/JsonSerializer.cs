﻿using System;
using System.Net;
using System.Text;

using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

using DDDD.Core.IO;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Patterns;
using DDDD.Core.Collections;
using DDDD.Core.Collections.Concurrent;

#if NET45
using System.Collections.Concurrent;
#endif


namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    ///  JsonSerializer - de/serialize types that you need  to/from Json string ... 
    ///   <para/> JsonSerializer Automatically analise   and  precompile 
    ///   serializers for all of consistent types of TargetType you need to de/serialize
    public class JsonSerializer : Singleton<JsonSerializer>, ITypeSetSerializerJson
    {
        
        #region  --------------------------------------- INITIALIZATION -----------------------------------------

        JsonSerializer() { }

        protected override void Initialize()
        {
            TypeSerializeAnalyser.Init_JsonSerializeGenerators();
        }


        #endregion --------------------------------------- INITIALIZATION -----------------------------------------
         

        #region ---------------------------- CONST && FIELDS -----------------------


        /// <summary>
        /// Default Encoding for Serialization. Default Value is - UTF8 Encoding. 
        /// </summary>
        public static Encoding DefEncoding
        { get; } = new UTF8Encoding(false);
        

        const string Create = nameof(Create);
        static readonly object _locker = new object();
       
       
        #endregion ---------------------------- CONST && FIELDS -----------------------



        #region ---------------------------- TYPE  SERIALIZERS DICTIONARIES ---------------------------
   
        //static ConcurrentDictionary<Int32, ITypeSerializerJson> TypeSerializers
        //{ get; } = new ConcurrentDictionary<Int32, ITypeSerializerJson>();

        static SafeDictionary2<Int32, ITypeSerializerJson> TypeSerializers
        { get; } = new SafeDictionary2<Int32, ITypeSerializerJson>();
               

        #endregion  ---------------------------- TYPE SERIALIZERS DICTIONARIES ---------------------------


        #region --------------------------- StringBuilder ALLOCATE/FREE Thread static -------------------------


        [ThreadStatic]
        static StringWriter cachedStringWriter;

        [ThreadStatic]
        static StreamWriter cachedStreamWriter;


        internal static StringWriter Allocate()
        {
            var ret = cachedStringWriter;
            if (ret == null)
                return new StringWriter(CultureInfo.InvariantCulture);

            var sb = ret.GetStringBuilder();
            sb.Length = 0;
            cachedStringWriter = null;  //don't re-issue cached instance until it's freed
            return ret;
        }

        //internal static StreamWriter AllocateStrWrt(Stream stream) 
        //{
        //    var ret = cachedStreamWriter;
        //    if (ret == null)
        //        return new StreamWriter(stream);
                       
        //    cachedStreamWriter = null;  //don't re-issue cached instance until it's freed
        //    return ret;
        //}


        internal static void Free(StringWriter writer)
        {
            cachedStringWriter = writer;
        }

        internal static string ReturnAndFree(StringWriter writer)
        {
            var ret = writer.ToString();
            cachedStringWriter = writer;
            return ret;
        }

        #endregion --------------------------- StringBuilder ALLOCATE/FREE Thread static -------------------------



        #region ---------------------- GET/CONTAINS TYPE SERIALIZER BY typeID / TYPE ------------------------
        public static ITypeSerializerJson GetTypeSerializer(Int32 targetTypeID) // public for Test of Debug Code Generation Check
        {
            return TypeSerializers[targetTypeID]; //logicallly targetType Serializer always is here at this moment.
        }

        public static ITypeSerializerJson GetTypeSerializer(Type targetType) // public for Test of Debug Code Generation Check
        {
            return GetTypeSerializer(Tps.GetTypeID(targetType)); //logicallly targetType Serializer always is here at this moment.
        }

        internal static bool ContainsTypeSerializer(Int32 targetTypeID)
        {            
            return TypeSerializers.ContainsKey(targetTypeID);
        }


        internal static bool ContainsTypeSerializer(Type targetType)
        { 
            return ContainsTypeSerializer( Tps.GetTypeID(targetType));
        }

        #endregion  ---------------------- GET/CONTAINS TYPE SERIALIZER BY typeID / TYPE ------------------------


        #region ----------------------- ADD KNOWN TYPES && CreateTypeSerializer -------------------------

        /// <summary>
        /// Add known Type  - analise amd prebuild Type with its subTypes as Json TypeSerializers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeSerializerJson<T> AddKnownType<T>(    // public for test purpose only
                                     TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {

            return AddKnownType(typeof(T).GetTypeInfoEx()
                , selector, propertiesBinding, fieldsBinding
                , onlyMembers, ignoreMembers) as ITypeSerializerJson<T>;

        }


        internal static void AddKnownTypes(Type[] targetTypes)
        {
            if (targetTypes == null || targetTypes.Length == 0) return;

            for (int i = 0; i < targetTypes.Length; i++)
            {
                AddKnownType(targetTypes[i].GetTypeInfoEx());
            }
        }


        /// <summary>
        ///  Add known Type  - analise amd prebuild Type with its subTypes as Json TypeSerializers
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        internal static ITypeSerializerJson AddKnownType(TypeInfoEx targetTypeEx

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {            

            //We cannot add  Contract Type that already exist            
            //if (ContainsTypeSerializer(targetTypeEx.ID)) return TypeSerializers[targetTypeEx.ID];            

            _locker.LockAction( ()=> !ContainsTypeSerializer(targetTypeEx.ID)  
            , () =>
            {            
            
            // TYPES THAT NEEDS NO MEMBERS ANALYSING 
            if (TypeSerializeAnalyser.IsSimplyAddable(targetTypeEx) ) 
                {

                    AddKnownTypeWithoutAnalyze(targetTypeEx 
                                            , selector, propertiesBinding
                                            , fieldsBinding, onlyMembers
                                            , ignoreMembers);

                }
                else
                {
                    // TARGET Type + ITS INTERNAL MEMBER TYPES/ARGTYPE
                    var serializeKnownTypes = TypeSerializeAnalyser
                                            .AnalyzeCollectTypesJson(targetTypeEx
                                            , selector, propertiesBinding, fieldsBinding
                                            , onlyMembers, ignoreMembers);

                    //if  serializeKnownTypesTree is List<Type>  with  0 elements it means that this targetType is not serializable
                    Validator.ATInvalidOperationDbg(serializeKnownTypes.IsNotWorkable(), nameof(JsonSerializer), nameof(AddKnownType)
                         , "This targetType - [{0}]  Can't be Serialized".Fmt(targetTypeEx.OriginalType.FullName));
                    
                    for (int i = 0; i < serializeKnownTypes.Count; i++)
                    {
                        AddKnownTypeWithoutAnalyze(
                                              serializeKnownTypes[i].GetTypeInfoEx()
                                            , selector, propertiesBinding
                                            , fieldsBinding, onlyMembers
                                            , ignoreMembers );
                    }
                    
                }               

            }
            );

            return TypeSerializers[targetTypeEx.ID];          

        }

        internal static void AddKnownTypeWithoutAnalyze(TypeInfoEx targetTypeEx

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {          
            // We cannot add  Contract Type that already exist            
            if (ContainsTypeSerializer(targetTypeEx.ID)) return;// TypeSerializers[targetType];
            
            //not exist then Create Types Serializer and Add it            
            var typeSerializer = CreateTypeSerializerJson(targetTypeEx.OriginalType
                , selector, propertiesBinding
                , fieldsBinding, onlyMembers, ignoreMembers);

            AddTypeSerializer(typeSerializer);

        }

        internal static void AddTypeSerializer(ITypeSerializerJson tpSerializer)
        {
            TypeSerializers.Add(tpSerializer.TargetTypeEx.ID, tpSerializer);
        }


        static ITypeSerializerJson CreateTypeSerializerJson(Type targetType
                                                     //, ITypeSetSerializer2 typeSetSerializer
                                                     , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                     , BindingFlags propertiesBinding = BindingFlags.Default
                                                     , BindingFlags fieldsBinding = BindingFlags.Default
                                                     , List<string> onlyMembers = null
                                                     , List<string> ignoreMembers = null
                                                     )
        {
            var createMethod = typeof(TypeSerializerJson<>)
                             .MakeGenericType(targetType)
                             .GetMethod(Create);

            return createMethod.Invoke(null, new object[] { targetType
                        , selector
                        , propertiesBinding
                        , fieldsBinding
                        , onlyMembers
                        , ignoreMembers }) as ITypeSerializerJson;
        }
               

        #endregion ----------------------- ADD KNOWN TYPES && CreateTypeSerializer -------------------------


        #region  --------------- DE/SERIALIZE TO/FROM String ------------------- 

        /// <summary>
        /// Serialize to string T data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeToString<T>(T data, JsonOptions optionsOnRun = null)
        {
            return Current.SerializeToStringInternal<T>(data, optionsOnRun);
        }

        string SerializeToStringInternal<T>(T data,JsonOptions optionsOnRun = null)
        {
            return SerializeToStringInternal((object)data, optionsOnRun);
        }


        /// <summary>
        /// Serialize to string object data
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SerializeToString(object data, JsonOptions optionsOnRun = null)
        {
            return Current.SerializeToStringInternal(data,optionsOnRun);
        }

        string SerializeToStringInternal(object data, JsonOptions optionsOnRun = null)
        {
            if (optionsOnRun.IsNotNull())
            { JNP.Options = optionsOnRun; }

            if (data.IsNull() || ((object)data).IsDelegate()) //  
            {
                return null;
            }

            var typeinfo = data.GetTypeInfoEx();
            // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
            // thread-safe action
            var tpSerializer = AddKnownType(typeinfo);

            //if (ContainsTypeSerializer(typeinfo.WorkingType) == false)
            //{     AddKnownType(typeinfo);   
            //}

            StringWriter stringWriter = null;
            string result = null;
            try
            {
                stringWriter = Allocate();
                tpSerializer.RaiseSerializeToStringBoxedHandler(stringWriter, data);
            }
            finally
            {
                result = ReturnAndFree(stringWriter);
            }

            return result;
        }


      
                             

        /// <summary>
        /// Deserilize from string to T instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T DeserializeFromString<T>(string value, JsonOptions optionsOnRun = null)
        {
            return Current.DeserializeFromStringInternal<T>(value,optionsOnRun);
        }

        T DeserializeFromStringInternal<T>(string value, JsonOptions optionsOnRun = null)
        {
            if (optionsOnRun.IsNotNull())
            { JNP.Options = optionsOnRun; }

            //T t = default(T);
            if (string.IsNullOrEmpty(value))
            {
                return default(T);
            }

            var typeinfo =  typeof(T).GetTypeInfoEx();
            // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
            // thread-safe action
            var tpSerializer = AddKnownType(typeinfo);

            var runIndex = 0;
            //ID of OriginalType.FullName hashCode
            return (T)tpSerializer.RaiseDeserializeFromStringBoxedHandler(value,ref runIndex);
            
        }

        /// <summary>
        /// Deserialize objecct FromString  to target Type instance
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object DeserializeFromString(Type type, string value , JsonOptions optionsOnRun = null)
        {
            return Current.DeserializeFromStringInternal(value,type, optionsOnRun);
        }

        object DeserializeFromStringInternal(string value, Type type, JsonOptions optionsOnRun = null)
        {
            if (optionsOnRun.IsNotNull())
            { JNP.Options = optionsOnRun; }

            //T t = default(T);           
            if (string.IsNullOrEmpty(value))
            {
                return null;
            }

            var typeinfo = type.GetTypeInfoEx();
            // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
            // thread-safe action
            var tpSerializer = AddKnownType(typeinfo);
            
            var runIndex = 0;
            //ID of OriginalType.FullName hashCode
            return tpSerializer.RaiseDeserializeFromStringBoxedHandler(value, ref runIndex);

        }


        #endregion  --------------- DE/SERIALIZE TO/FROM String ------------------- 


        #region --------------- DE/SERIALIZE TO TextWriter/TextReader ------------------- 

        /// <summary>
        /// Serialize  T value to TextWriter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="writer"></param>
        public static void SerializeToWriter<T>(T value, TextWriter writer, JsonOptions optionsOnRun = null)
        {
            if (optionsOnRun.IsNotNull())
            { JNP.Options = optionsOnRun; }

            if (writer.IsNull()) return;
            if (value.IsNull() || ((object)value).IsDelegate()) return;

            var typeinfo = value.GetTypeInfoEx();
            // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
            // thread-safe action
            var tpSerializer = AddKnownType(typeinfo);

            try
            {
                tpSerializer.RaiseSerializeToStringBoxedHandler(writer, value);
            }                        
            finally
            {
                writer.Flush();
            }
        }

        /// <summary>
        /// Serialize object value to TextWriter
        /// </summary>
        /// <param name="value"></param>
        /// <param name="writer"></param>
        public static void SerializeToWriter(object value, TextWriter writer, JsonOptions optionsOnRun = null)
        {
            if (optionsOnRun.IsNotNull())
            { JNP.Options = optionsOnRun; }

            if (writer.IsNull()) return;
            if (value.IsNull() || value.IsDelegate()) return;

            var typeinfo = value.GetTypeInfoEx();
            // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
            // thread-safe action
            var tpSerializer = AddKnownType(typeinfo);
            
            try
            {
                tpSerializer.RaiseSerializeToStringBoxedHandler(writer, value);
            }
            finally
            {
                writer.Flush();
            }
        }

        /// <summary>
        /// Deserialize from TextReader T instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="reader"></param>
        /// <returns></returns>
        public static T DeserializeFromReader<T>(TextReader reader, JsonOptions optionsOnRun = null)
        {
            return DeserializeFromString<T>(reader.ReadToEnd(), optionsOnRun);
        }

        /// <summary>
        /// Deserialize from TextReader Type instance
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static object DeserializeFromReader(TextReader reader, Type type, JsonOptions optionsOnRun = null)
        {
            return DeserializeFromString( type, reader.ReadToEnd(), optionsOnRun);
        }

        #endregion --------------- DE/SERIALIZE TO TextWriter/TextReader -------------------



        #region --------------- DE/SERIALIZE TO/FROM Stream ------------------- 

        /// <summary>
        /// Deserialize from Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T DeserializeFromStream<T>(Stream stream, JsonOptions optionsOnRun = null)
        {
            T t;
            using (StreamReader streamReader = new StreamReader(stream, DefEncoding))
            {
                t = DeserializeFromString<T>(streamReader.ReadToEnd(), optionsOnRun);
            }
            return t;
        }
       


        /// <summary>
        /// Deserialize from Stream.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object DeserializeFromStream(Type type, Stream stream,  JsonOptions optionsOnRun = null)
        {
            object obj;
            using (StreamReader streamReader = new StreamReader(stream, DefEncoding))
            {
                obj = DeserializeFromString( type, streamReader.ReadToEnd(), optionsOnRun);
            }
            return obj;
        }

         



        /// <summary>
        /// Serialize to stream T data Item
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="data"></param>
        /// <param name="stream"></param>
        public static void SerializeToStream<T>(T data, Stream stream, JsonOptions optionsOnRun = null)
        {
            Current.SerializeToStreamInternal<T>(data, stream, optionsOnRun);
        }

        void SerializeToStreamInternal<T>(T data, Stream stream, JsonOptions optionsOnRun = null)
        {             
            using (var writer = new StreamWriter(stream, DefEncoding))
            {
                SerializeToWriter<T>(data, writer, optionsOnRun);
            }
        }


        /// <summary>
        /// Serialize to stream object data Item
        /// </summary>
        /// <param name="data"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        public static void SerializeToStream(object data, Stream stream, JsonOptions optionsOnRun = null)
        {
            Current.SerializeToStreamInternal(data, stream,optionsOnRun);
        }

        void SerializeToStreamInternal(object data, Stream stream, JsonOptions optionsOnRun = null)
        {
            using (var writer = new StreamWriter(stream, DefEncoding))
            {
                SerializeToWriter(data, writer, optionsOnRun);
            }
        }

        #endregion --------------- DE/SERIALIZE TO/FROM Stream ------------------- 



        #region --------------- DESERIALIZE  Request / Response-------------------

        static WebResponse GetResponse(WebRequest webRequest)
        {
            WebResponse result;
            try
            {
                Task<WebResponse> responseAsync = webRequest.GetResponseAsync();
                responseAsync.Wait();
                result = responseAsync.Result;
            }
            catch (Exception exception)
            {
                throw exception; //.UnwrapIfSingleException()
            }
            return result;
        }


        /// <summary>
        /// Deserialize webRequest data as T instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="webRequest"></param>
        /// <returns></returns>
        public static T DeserializeRequest<T>(WebRequest webRequest)
        {
            T t = default(T);
            using (WebResponse response =  GetResponse(webRequest))
            {
                t = DeserializeResponse<T>(response);
            }
            return t;
        }

        /// <summary>
        /// Deserialize webResponse data as T instance
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        public static T DeserializeResponse<T>(WebResponse webResponse)
        {
            T t = default(T);
            using (Stream responseStream = webResponse.GetResponseStream())
            {
                t = DeserializeFromStream<T>(responseStream);
            }
            return t;
        }




        /// <summary>
        /// Deserialize webRequest data as Type instance
        /// </summary>
        /// <param name="type"></param>
        /// <param name="webRequest"></param>
        /// <returns></returns>
        public static object DeserializeRequest(Type type, WebRequest webRequest)
        {
            object obj = null;
            using (WebResponse response = GetResponse(webRequest))
            {
                obj = DeserializeResponse(type, response);
            }
            return obj;
        }


        /// <summary>
        /// Deserialize webResponse data as Type instance
        /// </summary>
        /// <param name="type"></param>
        /// <param name="webResponse"></param>
        /// <returns></returns>
        public static object DeserializeResponse(Type type, WebResponse webResponse)
        {
            object obj = null;
            using (Stream responseStream = webResponse.GetResponseStream())
            {
                obj = DeserializeFromStream(type, responseStream);
            }
            return obj;
        }

        



        #endregion --------------- DESERIALIZE  Request  / Response -------------------


        #region ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------------------ SERIALIZE TPL ------------------------------------------


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<string> SerializeToStringTPL(object data)
        {
            return SerializeToStringTPL(data, default(CancellationToken), TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="taskCretionOptions"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<string> SerializeToStringTPL(object data
                                        , CancellationToken cancelToken
                                        , TaskCreationOptions taskCretionOptions
                                        , TaskScheduler scheduler)
        {         
            return Task.Factory.StartNew<string>((st) =>
            {
                var inSt = st as  object;
                return SerializeToString(inSt);               

            },
            data,
            cancelToken,
            taskCretionOptions,
            scheduler
            );

        }



        /// <summary>
        /// Serialize object data into stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public static Task SerializeToStreamTPL(Stream stream, object data)
        {
            var state = ThreadCallState<Stream, object>.Create(stream, data);

            return Task.Factory.StartNew((st) =>
            {   
                var inSt = st as ThreadCallState<Stream, object>;             
                SerializeToStream(inSt.Item2, inSt.Item1);               
            }
            , state
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );

        }



#endregion ------------------------------------------ SERIALIZE TPL ------------------------------------------



#region ------------------------------------- ContinueSerializeTPL ------------------------------------------

#if NET45 || WP81
        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream  
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data)
        {
            return ContinueSerializeTPL(streamTask, data, TaskContinuationOptions.AttachedToParent);
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>     
        /// <param name="continueAsOption"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask
                                                , object data
                                                , TaskContinuationOptions continueAsOption)
        {       
            return streamTask.ContinueWith((t, st) =>
            {                      
                SerializeToStream(st as object, t.Result);
            }
            , data
            , CancellationToken.None          
            , continueAsOption
            , TaskScheduler.Default            
            );
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options, and add CancellationToken.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler)
        {
            return streamTask.ContinueWith((t, st) =>
            {              
                var strm = t.Result;
                var inData = st as  object;

                SerializeToStream(inData, strm);   
            }
            , data
            , cancelToken
            , continueAsOption
            , scheduler
            );
        }

#endif

#endregion ------------------------------------- ContinueSerializeTPL ------------------------------------------



#region ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------

        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Task<object> DeserializeTPL(Type targetType, Stream stream)
        {
            var state = ThreadCallState< Type, Stream>.Create(targetType, stream);
            return Task.Factory.StartNew<object>((st) =>
            {
                var inSt = st as ThreadCallState<Type, Stream>;                
                return DeserializeFromStream(inSt.Item1, inSt.Item2);
            }
            , state
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default           
            );

        }

        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public  static  Task<T> DeserializeTPL<T>(Stream stream)
        {           
            return Task.Factory.StartNew<T>((st) =>
            {              
                return DeserializeFromStream<T>(st as Stream);
            }
            , stream
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );
        }



        #endregion ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------



#region ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------

#if NET45 || WP81

        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask,Type targetType)
        {
            return ContinueDeserializeTPL(streamTask, targetType, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }



        /// <summary>
        ///  Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask,Type targetType, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {            
            return
             streamTask.ContinueWith<object>((t, st) =>
             {
                 var targType = st as Type;                 
                 return DeserializeFromStream(targType, t.Result);
             }
            , targetType
            , cancelToken // 
            , continuation
            , scheduler
            );
        }



        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL<T>(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<T>((t, st) =>
             {              
                 return DeserializeFromStream<T>(t.Result);
             }
            , null
            , cancelToken // 
            , continuation
            , scheduler
            );

        }


#endif

#endregion ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------



#region ----------------------------------- DESERIALIZE FROM string -----------------------------------------

        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static Task<T> DeserializeTPL<T>(string serializedData)
        {            
            return Task.Factory.StartNew<T>((st) =>
            {        
                return DeserializeFromString<T>(st as string);                
            }
            , serializedData
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default             
            );
        }

        #endregion ----------------------------------- DESERIALIZE FROM string -----------------------------------------



        #endregion ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------
            


        #region ---------- DE/SERIALIZE TO FILE ----------------

        /// <summary>
        /// Serialize To File Tvalue by filePath
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="filePath"></param>
        public static void SerializeToFile<T>(T value, string filePath)
        {
            using (var flStream = new FileStream(filePath, FileMode.OpenOrCreate))    
            {
                SerializeToStream<T>(value, flStream);
            }
        }

        /// <summary>
        /// Serialize To File Object value by filePath
        /// </summary>
        /// <param name="value"></param>
        /// <param name="filePath"></param>
        public static void SerializeToFile (object value, string filePath)
        {
            using (var flStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                SerializeToStream(value, flStream);
            }
        }

        /// <summary>
        /// Deserialize from File T instance 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static T DeserializeFromFile<T>(string filePath)
        {
            using (var flStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
              return  DeserializeFromStream <T>(flStream);
            }
        }

        /// <summary>
        /// Deserialize from File to Type instance
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="toType"></param>
        /// <returns></returns>
        public static object DeserializeFromFile(string filePath, Type toType)
        {
            using (var flStream = new FileStream(filePath, FileMode.OpenOrCreate))
            {
                return DeserializeFromStream(toType,flStream);
            }
        }

        #endregion---------- DE/SERIALIZE TO FILE ----------------


    }
}

