﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

using DDDD.Core.IO;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Resources;
using DDDD.Core.Patterns;
using DDDD.Core.Collections.Concurrent;

#if NET45
using System.Collections.Concurrent;
#endif


namespace DDDD.Core.Serialization.Binary
{

    /// <summary>
    ///   BinarySerializer - de/serialize types that you need  to/from byte[]  ... 
    ///   <para/>  BinarySerializer Automatically analise   and  precompile 
    ///   serializers for all of consistent types of TargetType-you need to de/serialize
    public class BinarySerializer : Singleton<BinarySerializer>, ITypeSetSerializer
    {

        #region ----------------------------- CTOR---------------------------

        BinarySerializer() { }

        #endregion ----------------------------- CTOR---------------------------


        #region  --------------------------------------- INITIALIZATION -----------------------------------------


        protected override void Initialize()
        {
            TypeSerializeAnalyser.Init_BinSerializeGenerators();
#if NET45
            UseGenerationOfDebugCodeAssembly = false;
#endif
        }

        #endregion --------------------------------------- INITIALIZATION -----------------------------------------


        #region ---------------------------- FIELDS -----------------------

        const string Create = nameof(Create);
        static readonly object _locker = new object();

        #endregion ---------------------------- FIELDS -----------------------



        #region ---------------------------- TYPE  SERIALIZERS DICTIONARIES ---------------------------


        /// <summary>
        ///  Type Item Serializers - Dictionary[ID-ushort, ITypeserializer -Type item Serializer].
        /// </summary>
        static SafeDictionary2<int, ITypeSerializer> TypeSerializers
        { get; } = new SafeDictionary2<int, ITypeSerializer>();

        /// <summary>
        /// Get TypeSerializer
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        internal static ITypeSerializer GetTypeSerializer(int typeID)
        {
            ITypeSerializer rest = null;
            if (TypeSerializers.TryGetValue(typeID, out rest))
            {
                return rest;
            }
            return null;
        }

        internal static List<ITypeSerializer> GetTypeSerializers()
        {
            var listResult = new List<ITypeSerializer>();
            foreach (var item in TypeSerializers)
            {
                if (item.Value.TargetTypeEx.CollectionType == CollectionTypeEn.NotCollection
                    && item.Value.TargetTypeEx.Is4DPrimitive == false)
                {
                    listResult.Add(item.Value);
                }
            }

            return listResult;
        }

        internal static string GetConventionIdentityKey()
        {
            return nameof(BinarySerializer) + "_" + nameof(Current);
        }

        #endregion  ---------------------------- TYPE SERIALIZERS DICTIONARIES ---------------------------



        #region --------------------------------- ADDING KNOWN TYPES -----------------------------------



        /// <summary>
        /// Add known Type  - analise amd prebuild Type with its subTypes as Json TypeSerializers
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeSerializer<T> AddKnownType<T>(    // public for test purpose only
                                     TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {

            return AddKnownType(typeof(T).GetTypeInfoEx()
                , selector, propertiesBinding, fieldsBinding
                , onlyMembers, ignoreMembers) as ITypeSerializer<T>;

        }

        /// <summary>
        /// Add known Types array.
        /// </summary>
        /// <param name="targetTypes"></param>
        public static void AddKnownTypes(params Type[] targetTypes)
        {
            if (targetTypes == null || targetTypes.Length == 0) return;

            for (int i = 0; i < targetTypes.Length; i++)
            {
                AddKnownType(targetTypes[i].GetTypeInfoEx());
            }
        }


        /// <summary>
        ///  Full  AddContract overload Creating TypeSerializer for different Contract Category value.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        internal static ITypeSerializer AddKnownType(TypeInfoEx targetTypeEx

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {



            _locker.LockAction(() => !ContainsTypeSerializer(targetTypeEx.ID)
            , () =>
            {

                // TYPES THAT NEEDS NO MEMBERS ANALYSING 
                if (TypeSerializeAnalyser.IsSimplyAddable(targetTypeEx))
                {

                    AddKnownTypeWithoutAnalyze(targetTypeEx
                                            , selector, propertiesBinding
                                            , fieldsBinding, onlyMembers
                                            , ignoreMembers);

                }
                else
                {
                    // TARGET Type + ITS INTERNAL MEMBER TYPES/ARGTYPE
                    var serializeKnownTypes = TypeSerializeAnalyser
                                            .AnalyzeCollectTypesJson(targetTypeEx
                                            , selector, propertiesBinding, fieldsBinding
                                            , onlyMembers, ignoreMembers);

                    //if  serializeKnownTypesTree is List<Type>  with  0 elements it means that this targetType is not serializable
                    Validator.ATInvalidOperationDbg(serializeKnownTypes.IsNotWorkable(), nameof(BinarySerializer), nameof(AddKnownType)
                         , "This targetType - [{0}]  Can't be Serialized".Fmt(targetTypeEx.OriginalType.FullName));

                    for (int i = 0; i < serializeKnownTypes.Count; i++)
                    {
                        AddKnownTypeWithoutAnalyze(
                                              serializeKnownTypes[i].GetTypeInfoEx()
                                            , selector, propertiesBinding
                                            , fieldsBinding, onlyMembers
                                            , ignoreMembers);
                    }

                }
            }
            );

            return TypeSerializers[targetTypeEx.ID];

        }



        internal static bool ContainsTypeSerializer(Int32 targetTypeID)
        {
            return TypeSerializers.ContainsKey(targetTypeID);
        }

        internal static void AddTypeSerializer(ITypeSerializer tpSerializer)
        {
            TypeSerializers.Add(tpSerializer.TargetTypeEx.ID, tpSerializer);
        }

        internal static void AddKnownTypeWithoutAnalyze(TypeInfoEx targetTypeEx

                                   , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                   , BindingFlags propertiesBinding = BindingFlags.Default
                                   , BindingFlags fieldsBinding = BindingFlags.Default

                                   , List<string> onlyMembers = null
                                   , List<string> ignoreMembers = null

                                   )
        {
            // We cannot add  Contract Type that already exist            
            if (ContainsTypeSerializer(targetTypeEx.ID)) return;// TypeSerializers[targetType];

            //not exist then Create Types Serializer and Add it            
            var typeSerializer = CreateTypeSerializerBin(targetTypeEx.OriginalType
                , selector, propertiesBinding
                , fieldsBinding, onlyMembers, ignoreMembers);

            AddTypeSerializer(typeSerializer);

        }


        static ITypeSerializer CreateTypeSerializerBin(Type targetType
                                                     //, ITypeSetSerializer2 typeSetSerializer
                                                     , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                     , BindingFlags propertiesBinding = BindingFlags.Default
                                                     , BindingFlags fieldsBinding = BindingFlags.Default
                                                     , List<string> onlyMembers = null
                                                     , List<string> ignoreMembers = null
                                                     )
        {
            var createMethod = typeof(TypeSerializer<>)
                             .MakeGenericType(targetType)
                             .GetMethod(Create);

            return createMethod.Invoke(null, new object[] { targetType
                        , selector
                        , propertiesBinding
                        , fieldsBinding
                        , onlyMembers
                        , ignoreMembers }) as ITypeSerializer;
        }



        #endregion --------------------------------- ADDING KNOWN TYPES -----------------------------------



        #region --------------------------------- PLAIN STYLE DE/SERIALIZE -------------------------------------

        /// <summary>
        /// Serialize object data into  stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        public static void SerializeToStream(object value, Stream stream)
        {
            Serialize(value, stream);
        }

        /// <summary>
        /// Serialize {T} object data into  stream.
        /// </summary>
        /// <typeparam name="T"> type of value </typeparam>
        /// <param name="value"> value </param>
        /// <param name="stream"> stream  </param>
        public void SerializeToStream<T>(T value, Stream stream)
        {
            Serialize(value, stream);
        }

        /// <summary>
        /// Serialize   object data into  stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <param name="isNullable"></param>
        public static void Serialize(object data, Stream stream, bool isNullable = false)
        {
            Current.SerializeInternal(stream, data, isNullable);
        }


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] Serialize(object data, bool isNullable = false)
        {
            byte[] result = null;
            using (var ms = new MemoryStream())
            {
                Current.SerializeInternal(ms, data, isNullable);
                result = ms.ToArray();
            }
            return result;
        }




        /// <summary>
        /// Serialize object data into stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public void SerializeInternal(Stream stream, object data, bool isNullable = false)
        {
            if (stream == null) return;
            if (data.IsNull() || ((object)data).IsDelegate()) return;


            var realType = (isNullable) ? typeof(Nullable<>).MakeGenericType(data.GetType()) : data.GetType();
            var typeinfo = realType.GetTypeInfoEx();

            var tpSerializer = AddKnownType(typeinfo);

            if (realType.Is4DPrimitiveType() || realType.IsEnum)
            {
                //write typeID  here only for primitives - because primitives don't write itself TypeID directly
                BinaryReaderWriter.Write(stream, typeinfo.ID);
                TypeSerializers[typeinfo.ID].RaiseSerializeToStreamBoxedHandler(stream, data);
            }
            else
            {
                TypeSerializers[typeinfo.ID].RaiseSerializeToStreamBoxedHandler(stream, data);
            }

            //int? mayBeTypeID = GetIfContainsContract(realType);
            //if (mayBeTypeID == null)
            //{
            //    DetectAddAutoContract(realType);

            //    int typeID = GetTypeID(realType);// now ModelId must be here  or  GetTypeID will throw if not found typeProcessor for realType

            //    if (realType.Is4DPrimitiveType()  || realType.IsEnum)
            //    {
            //        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
            //        BinaryReaderWriter.Write(stream, typeID);
            //        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);                    
            //    }
            //    else
            //    {
            //        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);
            //    }
            //}
            //else if (mayBeTypeID > 0) //already contains need no to DetectAddAutoContract
            //{
            //    if (realType.Is4DPrimitiveType()|| realType.IsEnum)
            //    {
            //        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
            //        BinaryReaderWriter.Write(stream, mayBeTypeID.Value);
            //        TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(stream, data);                    
            //    }
            //    else
            //    {
            //        TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(stream, data);                    
            //    }

            //}
            //else if (mayBeTypeID == 0) return; //null value

        }




        /// <summary>
        /// Deserialize  to object  from byte[]. 
        /// TargetType already written in such byte Array o stream.
        /// </summary>
        /// <param name="dataArray"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static object Deserialize(Type targetType, byte[] dataArray)
        {
            object result = null;
            using (var ms = new MemoryStream(dataArray))
            {
                result = Deserialize(targetType,ms);
            }
            return result;            
        }


        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static T Deserialize<T>(byte[] serializedData)
        {
            T result = default(T);
            using (var ms = new MemoryStream(serializedData))
            {
                result = Deserialize<T>(ms);
            }
            return result;
        }





        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object Deserialize(Type targetType, Stream stream)
        {
            return Current.DeserializeInternal(targetType, stream);
        }

        /// <summary>
        ///  Deserialize to T object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T Deserialize<T>( Stream stream)
        {
            return Current.DeserializeInternal<T>(stream);
        }
        
        object DeserializeInternal(Type targetType, Stream stream)
        {
            if (stream == null || targetType == null) return null;

            AddKnownType( targetType.GetTypeInfoEx() );

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID

            if (typeID == 0) return null;            

            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>(
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);

        }




        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        T DeserializeInternal<T>(Stream stream)
        {
            if (stream == null) return default(T);

            AddKnownType(typeof(T).GetTypeInfoEx());

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID

            if (typeID == 0) return default(T);
            
            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>(
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return (T)TypeSerializers[typeID]
                .RaiseDeserializeFromStreamBoxedHandler(stream, true);


        }




        #endregion ---------------------------------PLAIN  STYLE DE/SERIALIZE -------------------------------------


        #region ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------------------ SERIALIZE TPL ------------------------------------------


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data)
        {
            return SerializeTPL(data, CancellationToken.None, TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }

        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data
                                                , CancellationToken cancelToken
                                                , TaskCreationOptions taskCretionOptions
                                                , TaskScheduler scheduler)
        {
            return Task.Factory.StartNew<byte[]>((st) =>
            {                
                return Serialize(st);
            }
            , data
            , cancelToken
            , taskCretionOptions
            , scheduler
            );
        }



        /// <summary>
        /// Serialize object data into binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public static Task SerializeTPL(object data, Stream stream)
        {
            var state = ThreadCallState< object, Stream>.Create( data, stream );

            return Task.Factory.StartNew((st) =>
            {                 
                //Item1 - stream
                //Item2 - data
                var inSt = st as ThreadCallState<object, Stream>;            
                
                Serialize( inSt.Item1, inSt.Item2);
            }
            , state
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );

        }



        #endregion ------------------------------------------ SERIALIZE TPL ------------------------------------------



        #region ------------------------------------- ContinueSerializeTPL ------------------------------------------

#if NET45 || WP81
        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream  
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data)
        {
            return ContinueSerializeTPL(streamTask, data, TaskContinuationOptions.AttachedToParent);
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options, and add CancellationToken.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, TaskContinuationOptions continueAsOption)
        {
            //var state = ThreadCallState< object>.Create(Current, data);

            return streamTask.ContinueWith( (t,st) =>
            {
                Serialize(st, t.Result);
            }
            , data
            , TaskScheduler.Default            
            );
        }


        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler)
        {           
            return streamTask.ContinueWith((t, st) =>
            { 
                Serialize(st, t.Result);
            }
            , data
            , cancelToken
            , continueAsOption
            , scheduler
            );
        }

#endif

        #endregion ------------------------------------- ContinueSerializeTPL ------------------------------------------



        #region ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------

        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Task<object> DeserializeTPL(Stream stream)
        {
            return Task.Factory.StartNew<object>((st) =>
            {
                return Deserialize<object>(st as Stream);
            }
            , stream
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default // TaskCreationOptions.AttachedToParent
            );
        }

        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Task<T> DeserializeTPL<T>(Stream stream)
        {            
            return Task.Factory.StartNew<T>((st) =>
            {                
               return Deserialize<T>(st as Stream);
            }
            , stream
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );
        }



        #endregion ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------



        #region ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------

#if NET45 || WP81

        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }



        /// <summary>
        ///  Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask
                                                        , CancellationToken cancelToken
                                                        , TaskContinuationOptions continuation
                                                        , TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<object>((t, st) =>
             {                 
                return Deserialize<object>(t.Result); 
             }
            , Current
            , cancelToken // 
            , continuation
            , scheduler
            );

        }



        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL<T>(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask
                                                        , CancellationToken cancelToken
                                                        , TaskContinuationOptions continuation
                                                        , TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<T>((t, st) =>
             {  
                return   Deserialize<T>(t.Result );                                    
             }
            , Current
            , cancelToken // 
            , continuation
            , scheduler
            );

        }


#endif

        #endregion ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------



        #region ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------

        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static Task<T> DeserializeTPL<T>(byte[] serializedData)
        {
            
            return Task.Factory.StartNew<T>((st) =>
            { 
                return Deserialize<T>(st as byte[]);
            }
            , serializedData
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );

        }

        #endregion ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------



        #endregion ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------
        

        #region ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

#if NET45 /////// --------------------------- WRITE DEBUG CODE ASSEMBLY --------------------------------///////


        /// <summary>
        /// If this flag is true, after each BuildProcessing() call, we'll write debugging code assembly.
        /// By Default it's false. But this feature can't be used in real-time parallel work, be carefull.
        /// </summary>
        public static bool UseGenerationOfDebugCodeAssembly
        { get; set; }




#endif
        #endregion ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------
                          


        #region  ----------------------------------- CAN SERIALIZE  TYPE -------------------------------------------

        /// <summary>
        /// Check if current BinarySerializer can serialize  Type - targetType.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool CanSerializeType(Type targetType)
        {
            return TypeSerializeAnalyser.CanSerialize(targetType);
        }

        /// <summary>
        ///  Check if current BinarySerializer can serialize  Type - targetType.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public  bool CanSerialize(Type targetType)
        {
            return CanSerializeType(targetType);
        }


        /// <summary>
        /// Check if current BinarySerializer can serialize  T - typeof(T).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool CanSerialize<T>()
        {
            return CanSerializeType(typeof(T));
        }


        /// <summary>
        ///   Check if current BinarySerializer  can serialize Type of data instance - [dataItem.GetType()].
        /// </summary>
        /// <param name="dataItem"></param>
        /// <param name="throwOnNullValue"></param>
        /// <returns></returns>
        public bool CanSerialize(object dataItem, bool throwOnNullValue)
        {
            if (dataItem == null && throwOnNullValue) throw new InvalidOperationException($" ERROR in {nameof(BinarySerializer)}.{nameof(CanSerialize)}() : dataItem can't be null to determine serialization possibilities ");
            else if (dataItem == null) return false;

            return CanSerializeType(dataItem.GetType());
        }




        #endregion ----------------------------------- CAN SERIALIZE  TYPE -------------------------------------------


        #region ------------------------------------ STERLING INTEGRATION SERIALIZATION OVERLOADS -------------------------------------     

        //NOT IMPLEMENTED 

        public void Serialize(object target, BinaryWriter writer)
        {
            throw new NotImplementedException(RCX.ERR_NotImplementedOperation.Fmt(nameof(BinarySerializer), nameof(Serialize)
                   , "overload with BinaryWriter not implemented yet."));
        }


        public object Deserialize(Type type, BinaryReader reader)
        {
            throw new NotImplementedException(RCX.ERR_NotImplementedOperation.Fmt(nameof(BinarySerializer), nameof(Deserialize)
                , "overload with BinaryReader  not implemented yet."));
        }

        public T Deserialize<T>(BinaryReader reader)
        {
            throw new NotImplementedException(RCX.ERR_NotImplementedOperation.Fmt(nameof(BinarySerializer), nameof(Deserialize)
            , "overload with BinaryReader not implemented yet."));
        }



        #endregion ------------------------------------ STERLING INTEGRATION SERIALIZATION OVERLOADS -------------------------------------






    }
}




