﻿using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using DDDD.Core.Collections.Concurrent;

#if NET45
using System.Collections.Concurrent;
#endif

namespace DDDD.Core.Serialization.Binary
{

    /// <summary>
    /// Type Set Serializer
    /// </summary>
    public interface ITypeSetSerializer : ISterlingSerializer
    {

#if NET45 && DEBUG

        /// <summary>
        /// Get convention readable string  value of current TypeSetSerializer Key-identifier.
        /// </summary>
        /// <returns></returns>
        //string GetConventionIdentityKey();

#endif


        /// <summary>
        /// Type Item Serializers 
        /// </summary>
        //SafeDictionary2<int, ITypeSerializer> TypeSerializers { get; }

    }
}

