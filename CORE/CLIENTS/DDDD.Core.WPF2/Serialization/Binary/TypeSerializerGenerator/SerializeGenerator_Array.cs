﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.Core.IO;
using DDDD.Core.Extensions;

namespace DDDD.Core.Serialization.Binary
{
    /// <summary>
    /// Type Serialize Generator of Arrays. Arrays can be of different Rank.
    /// </summary>
    internal class SerializeGenerator_Array : ITypeSerializerGenerator
    {
        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [Array] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;//
            }
        }


        /// <summary>
        ///  Can Handle [type] criterion for SerializeGenerator_Array.
        ///   Contract: type.IsArray 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return type.IsArray;
        }



        #region ----------------------------------- WRITE ARRAYS of 1-4 Dimension ----------------------------------- 

         
        static void Write1DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[] array1Dim = instanceValue as TMember[];
   
            // don't write array's rank - we know it from type
            
            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, array1Dim.Length);

            //write all array elements to Stream
            for (int i = 0; i < array1Dim.Length; i++)
            {
                var element = array1Dim[i];
                int elementRealTypeID = typeSerializer.GetElementTypeID(element);
                if ( typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementRealTypeID) ) continue;  // end - value was Null              
                
                //write element  data 
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementRealTypeID, stream, element);                                  
            }
        }

        static void Write2DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,] array2Dim = instanceValue as TMember[,];

            // don't write array's rank - we know it from type           

            int dim1length = array2Dim.GetLength(0);
            int dim2length = array2Dim.GetLength(1);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    var element = array2Dim[i,j];
                    int elementRealTypeID = typeSerializer.GetElementTypeID(element);
                    if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementRealTypeID)) continue;  // end - value was Null

                    //write element  data 
                    typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementRealTypeID, stream, element);                    
                }                
            }
        }

        static void Write3DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,,] array3Dim = instanceValue as TMember[,,];

            // don't write array's rank - we know it from type

            int dim1length = array3Dim.GetLength(0);
            int dim2length = array3Dim.GetLength(1);
            int dim3length = array3Dim.GetLength(2);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);
            // write 3-th dimension length
            BinaryReaderWriter.Write(stream, dim3length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    for (int k = 0; k < dim3length; k++)
                    {
                        var element = array3Dim[i, j, k];
                        int elementRealTypeID = typeSerializer.GetElementTypeID(element);
                        if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementRealTypeID)) continue;  // end - value was Null

                        //write element  data 
                        typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementRealTypeID, stream, element);                        
                    }                   
                }
            }

        }

        static void Write4DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,,,] array4Dim = instanceValue as TMember[,,,];

            // don't write array's rank - we know it from type
            
            int dim1length = array4Dim.GetLength(0);
            int dim2length = array4Dim.GetLength(1);
            int dim3length = array4Dim.GetLength(2);
            int dim4length = array4Dim.GetLength(3);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);
            // write 3-th dimension length
            BinaryReaderWriter.Write(stream, dim3length);
            // write 4-th dimension length
            BinaryReaderWriter.Write(stream, dim4length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    for (int k = 0; k < dim3length; k++)
                    {
                        for (int l = 0; l < dim4length; l++)
                        {
                            var element = array4Dim[i, j, k, l ];
                            int elementRealTypeID = typeSerializer.GetElementTypeID(element);
                            if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementRealTypeID)) continue;  // end - value was Null

                            //write element  data 
                            typeSerializer.RaiseElementSerializeToStreamBoxedHandler( elementRealTypeID , stream, element);
                        }                        
                    }
                }
            }
        }


        static void Write1DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[] array1Dim = instanceValue as TMember[];

            // don't write array's rank - we know it from type

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, array1Dim.GetLength(0));

            //write all array elements to Stream
            for (int i = 0; i < array1Dim.Length; i++)
            {
                var element = array1Dim[i];
                //write element  data 
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, element);
            }
        }

        static void Write2DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,] array2Dim = instanceValue as TMember[,];

            // don't write array's rank - we know it from type
            
            int dim1length = array2Dim.GetLength(0);
            int dim2length = array2Dim.GetLength(1);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    var element = array2Dim[i,j];
                    //write element  data 
                    typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, element);
                }
            }
        }

        static void Write3DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,,] array3Dim = instanceValue as TMember[,,];

            // don't write array's rank - we know it from type

            int dim1length = array3Dim.GetLength(0);
            int dim2length = array3Dim.GetLength(1);
            int dim3length = array3Dim.GetLength(2);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);
            // write 3-th dimension length
            BinaryReaderWriter.Write(stream, dim3length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    for (int k = 0; k < dim3length; k++)
                    {
                        var element = array3Dim[i, j, k];
                        //write element  data 
                        typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.RuntimeIDents. ArrayElementTypeID, stream, element);
                    }
                }
            }
        }

        static void Write4DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            //Write typeID
            //write rank
            //write[rankIndex]= Lengths

            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            TMember[,,,] array4Dim = instanceValue as TMember[,,,];

            // don't write array's rank - we know it from type

            int dim1length = array4Dim.GetLength(0);
            int dim2length = array4Dim.GetLength(1);
            int dim3length = array4Dim.GetLength(2);
            int dim4length = array4Dim.GetLength(3);

            // write 1-th dimension length
            BinaryReaderWriter.Write(stream, dim1length);
            // write 2-th dimension length
            BinaryReaderWriter.Write(stream, dim2length);
            // write 3-th dimension length
            BinaryReaderWriter.Write(stream, dim3length);
            // write 4-th dimension length
            BinaryReaderWriter.Write(stream, dim4length);

            //write all array elements to Stream
            for (int i = 0; i < dim1length; i++)
            {
                for (int j = 0; j < dim2length; j++)
                {
                    for (int k = 0; k < dim3length; k++)
                    {
                        for (int l = 0; l < dim4length; l++)
                        {
                            var element = array4Dim[i, j, k, l];
                            //write element  data 
                            typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, element);
                        }
                    }
                }
            }
        }
        

        static MethodInfo Get_WriteArrayMethod<T>(ITypeSerializer<T> tpserializer)
        {
            if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 1)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write1DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);
            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 2)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write2DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 3)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write3DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 4)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write4DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 1)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write1DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 2)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write2DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 3)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write3DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 4)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Write4DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);
            }

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(Get_WriteArrayMethod)}() - such Array Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");

        }

        #endregion ----------------------------------- WRITE ARRAYS of 1-4 Dimension -----------------------------------



        #region ----------------------------------- READ ARRAYS of 1-4 Dimension ----------------------------------- 


        static T Read1DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 1, readedTypeID);
            //if (resultValue == null) return resultValue;

            TMember[] array = (resultValue as TMember[]);

            for (int i = 0; i < array.Length; i++)
            {
                //reading element RealTypeID
                ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

                //check null value element
                if (elementRealTypeId == 0) { continue; }

                //read element value
                var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID = true);
                array[i] = (TMember)item;
            }

            return resultValue;
        }

        static T Read2DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 2, readedTypeID);
            //if (resultValue == null) return resultValue;

            TMember[,] array = (resultValue as TMember[,]);
            var dimLengths = new int[2] { array.GetLength(0), array.GetLength(1) };

            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    //reading element RealTypeID
                    ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

                    //check null value element
                    if (elementRealTypeId == 0) { continue; }

                    //read element value
                    var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID = true);
                    array[i, j] = (TMember)item;

                }
            }

            return resultValue;
        }

        static T Read3DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 3, readedTypeID);
            //if (resultValue == null) return resultValue;

            TMember[,,] array = (resultValue as TMember[,,]);
            var dimLengths = new int[3] { array.GetLength(0), array.GetLength(1), array.GetLength(2) };


            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    for (int k = 0; k < dimLengths[2]; k++)
                    {
                        //reading element RealTypeID
                        ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

                        //check null value element
                        if (elementRealTypeId == 0) { continue; }

                        //read element value
                        var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID = true);
                        array[i, j, k] = (TMember)item;
                    }
                }
            }

            return resultValue;
        }

        static T Read4DimArray_NeedToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 4, readedTypeID);
            //if (resultValue == null) return resultValue;

            TMember[,,,] array = (resultValue as TMember[,,,]);
            var dimLengths = new int[4] { array.GetLength(0), array.GetLength(1), array.GetLength(2), array.GetLength(3) };


            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    for (int k = 0; k < dimLengths[2]; k++)
                    {
                        for (int l = 0; l < dimLengths[3]; l++)
                        {
                            //reading element RealTypeID
                            ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

                            //check null value element
                            if (elementRealTypeId == 0) { continue; }

                            //read element value
                            var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID = true);
                            array[i, j, k, l] = (TMember)item;

                        }
                    }
                }
            }

            return resultValue;
        }
        


        static T Read1DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 1, readedTypeID);
            if (resultValue == null) return resultValue;

            TMember[] array = (resultValue as TMember[]);

            for (int i = 0; i < array.Length; i++)
            {
                //read element value
                var item = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TMember>(typeSerializer.RuntimeIDents. ArrayElementTypeID, stream, readedTypeID = true);
                array[i] = item;
            }

            return resultValue;
        }

        static T Read2DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 2, readedTypeID);
            if (resultValue == null) return resultValue;
            
            TMember[,] array = (resultValue as TMember[,]);
            var dimLengths = new int[2] { array.GetLength(0), array.GetLength(1) };

            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    //read element value
                    var item = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TMember>(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, readedTypeID = true);
                    array[i, j] = item;
                }
            }

            return resultValue;
        }

        static T Read3DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 3,  readedTypeID);
            if (resultValue == null) return resultValue;

            TMember[,,] array = (resultValue as TMember[,,]);
            var dimLengths = new int[3] { array.GetLength(0), array.GetLength(1), array.GetLength(2) };

            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    for (int k = 0; k < dimLengths[2]; k++)
                    {
                        //read element value
                        var item = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TMember>(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, readedTypeID = true);
                        array[i, j, k] = item;
                    }
                }
            }

            return resultValue;
        }

        static T Read4DimArray_NeedNotToSureInItemType_FromStream<T, TMember>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            T resultValue = typeSerializer.ReadArrayTypeID_CreateValue(stream, 4, readedTypeID);
            if (resultValue == null) return resultValue;

            TMember[,,,] array = (resultValue as TMember[,,,]);

            var dimLengths = new int[4] { array.GetLength(0), array.GetLength(1), array.GetLength(2), array.GetLength(3) };
                   

            for (int i = 0; i < dimLengths[0]; i++)
            {
                for (int j = 0; j < dimLengths[1]; j++)
                {
                    for (int k = 0; k < dimLengths[2]; k++)
                    {
                        for (int l = 0; l < dimLengths[3]; l++)
                        {
                            //read element value
                            var item = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TMember>(typeSerializer.RuntimeIDents.ArrayElementTypeID, stream, readedTypeID = true);
                            array[i, j, k, l] = item;
                        }
                    }
                }
            }

            return resultValue;
        }


        static MethodInfo Get_ReadArrayMethod<T>(ITypeSerializer<T> tpserializer)
        {
            if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 1)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read1DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer. TargetTypeEx .ArrayElementType);
            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 2)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read2DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 3)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read3DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 4)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read4DimArray_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 1)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read1DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 2)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read2DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 3)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read3DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }
            else if (!tpserializer.TargetType.IsNeedToSureInDataType() && tpserializer.TargetTypeEx.ArrayRank == 4)
            {
                return typeof(SerializeGenerator_Array).GetMethod(nameof(Read4DimArray_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.ArrayElementType);

            }

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(Get_ReadArrayMethod)}() - such Array Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");


        }

        #endregion ----------------------------------- READ ARRAYS of 1-4 Dimension ----------------------------------- 

        

        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD.SerializeToStreamT<T> > GenerateSerialize_WriteExpression<T>(ITypeSerializer<T> tpSerializer)
        {
            try
            {
                //algorithm:              

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);

                var methodWriteArrayToStream = Get_WriteArrayMethod(tpSerializer);//

                return Expression.Lambda<SD.SerializeToStreamT<T>>(
                                                 Expression.Call(
                                                 methodWriteArrayToStream
                                                , typeProcessorInstance
                                                , parameterStream
                                                , parameterInputData
                                                )
                                                , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                , new[] { parameterStream, parameterInputData }
                                                );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.DeserializeFromStreamT<T>> GenerateDeserialize_ReadExpression<T>(ITypeSerializer<T> tpSerializer)
        {

            try
            {
                //algorithm:            

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);

                var methodReadArrayFromStream = Get_ReadArrayMethod(tpSerializer);


                return Expression.Lambda<SD.DeserializeFromStreamT<T>>(
                                                       Expression.Call(
                                                         methodReadArrayFromStream
                                                       , typeProcessorInstance
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                       , new[] { parameterStream, parameterReadedTypeID }
                                                       );
            }            
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(GenerateDeserialize_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////
       
        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                //void WriteArrayToStream<T>(TypeSerializer<T> typeProcessor,  Stream stream, T instanceValue)            

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeProcessor");

                var methodWriteArrayToStream = Get_WriteArrayMethod(tpSerializer);

                return Expression.Lambda<SD.SerializeToStreamTDbg<T>>(
                                                 Expression.Call(

                                                 methodWriteArrayToStream
                                                , parameterTypeProcessor
                                                , parameterStream
                                                , parameterInputData
                                                ),
                                                 parameterStream
                                                , parameterInputData
                                                , parameterTypeProcessor
                                                );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                // T ReadArrayFromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeProcessor");
                

                var methodReadArrayFromStream = Get_ReadArrayMethod(tpSerializer);

                return Expression.Lambda< SD.DeserializeFromStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodReadArrayFromStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       , parameterTypeProcessor
                                                       );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Array)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }

        }

     

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList() 
        /// </summary>
        public  bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_Array)} - can't have the Final List of Supported Types");
        }


        /// <summary>
        /// Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer. 
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_Array)} - can't have the Final List of Supported Types");
        }

       

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------




    }

}



#region ------------------------------- GARBAGE ----------------------------------------

#region  --------------------------------- OLD  WRITE ARRAYS HANDLERS ------------------------------------------

//static void WriteArray_NeedToSureInItemType_ToStream<T, TMember>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //Write typeID
//    //write rank
//    //write[rankIndex]= Lengths

//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var arrayInstance = instanceValue as Array;
//    int arrayRank = arrayInstance.Rank;

//    // write rank - number of array dimensions
//    BinaryReaderWriter.Write(stream, arrayRank);

//    // write i-th dimension length
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        BinaryReaderWriter.Write(stream, arrayInstance.GetLength(i)); 
//    }


//    //write all array elements to Stream
//    foreach (TMember element in arrayInstance)
//    {
//        ushort elementTypeID = typeSerializer.GetElementTypeID(element);

//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.Write(stream, elementTypeID);

//        if (elementTypeID == 0) //null data
//        {
//            continue;
//        }
//        else
//        {
//            //write element  data 
//            typeSerializer.RaiseElementSerializeToStreamTHandler(elementTypeID, stream, element);///RaiseElementSerializeSwitch
//        }

//    }
//}

//static void WriteArray_NeedNotToSureInItemType_ToStream<T, TMember>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //Write typeID
//    //write rank
//    //write[rankIndex]= Lengths

//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var arrayInstance = instanceValue as Array;
//    int arrayRank = arrayInstance.Rank;

//    // write rank - number of array dimensions
//    BinaryReaderWriter.Write(stream, arrayRank);

//    // write i-th dimension length
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        BinaryReaderWriter.Write(stream, arrayInstance.GetLength(i));
//    }

//    //write all array elements to Stream
//    foreach (TMember element in arrayInstance)
//    {
//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        //BinaryReaderWriter.Write(stream, typeSerializer.ArrayElementTypeID); // elementTypeID

//        //write element  data 
//        typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.ArrayElementTypeID, stream, element); ///RaiseElementSerializeSwitch            
//    }
//}

#endregion --------------------------------- OLD  WRITE ARRAYS HANDLERS ------------------------------------------
    
//public static T ReadArray_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //reading array dimensions and creating It
//    int arrayRank = (int)BinaryReaderWriter.Read_int32(stream); //read rank
//    int[] dimensionsLengths = new int[arrayRank];
//    int totalLength = 0;
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        dimensionsLengths[i] = (int)BinaryReaderWriter.Read_int32(stream);//read dimension-length
//        if (totalLength == 0)
//        {
//            totalLength = dimensionsLengths[i];
//        }
//        else
//        {
//            totalLength *= dimensionsLengths[i];
//        }
//    }

//    //now create Array instance
//    Array array = typeSerializer.LA_CreateDefaultValue.Value(dimensionsLengths.ToObjectArray()) as Array;
//    //            Array.CreateInstance( typeSerializer.AccessorT.TAccessEx. ArrayElementType, dimensionsLengths);

//    int[] indices = new int[arrayRank];
//    int itemIndex = 0;

//    for (int i = 0; i < totalLength; i++)
//    {
//        //reading element RealTypeID : for primitive or CustomContract -no difference
//        ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementRealTypeId == 0)
//        { itemIndex++; }     // null value element   
//        else
//        {
//            //read element value
//            var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, true);//false
//            CalculateIndices(itemIndex++, indices, dimensionsLengths);
//            array.SetValue(item, indices);
//        }
//    }


//    return (T)(object)array;

//}




//private static void CalculateIndices(int index,
//                                   int[] indices,
//                                   int[] dimensionsLengths)
//{
//    int rank = indices.Length;
//    bool continueCalculation = true;
//    int remainder = index;
//    for (int i = 0; i < rank; ++i)
//    {
//        if (continueCalculation)
//        {
//            int capacity = CalculateDimensionCapacity(i, dimensionsLengths);
//            indices[i] = remainder / capacity;
//            remainder %= capacity;
//            if (remainder == 0)
//            {
//                continueCalculation = false;
//            }
//        }
//        else
//        {
//            indices[i] = 0;
//        }
//    }
//}


//private static int CalculateDimensionCapacity(int dimension, int[] dimensionsLengths)
//{
//    int capacity = 1;
//    for (int i = dimension + 1; i < dimensionsLengths.Length; ++i)
//    {
//        capacity *= dimensionsLengths[i];
//    }
//    return capacity;
//}

//CalculateIndices(itemIndex++, indices, array.GetDimensionsLengths()); // set current indices

//NeedNotToSureInItemType
//public static T ReadArray_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //reading array dimensions and creating It
//    int arrayRank = (int)BinaryReaderWriter.Read_int32(stream); //read rank
//    int[] dimensionsLengths = new int[arrayRank];
//    int totalLength = 0;
//    for (int i = 0; i < arrayRank; ++i)
//    {
//        dimensionsLengths[i] = (int)BinaryReaderWriter.Read_int32(stream);//read dimension-length
//        if (totalLength == 0)
//        {
//            totalLength = dimensionsLengths[i];
//        }
//        else
//        {
//            totalLength *= dimensionsLengths[i];
//        }
//    }

//    //now create Array instance
//    Array array = typeSerializer.LA_CreateDefaultValue.Value(dimensionsLengths.ToObjectArray()) as Array;
//    //            Array.CreateInstance( typeSerializer.AccessorT.TAccessEx. ArrayElementType, dimensionsLengths);

//    int[] indices = new int[arrayRank];
//    int itemIndex = 0;

//    for (int i = 0; i < totalLength; i++)
//    {
//        //reading element RealTypeID : for primitive or CustomContract -no difference
//        ushort elementRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementRealTypeId == 0)
//        { itemIndex++; }     // null value element   
//        else
//        {
//            //read element value
//            var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, true);//false
//            CalculateIndices(itemIndex++, indices, dimensionsLengths);
//            array.SetValue(item, indices);
//        }
//    }


//    return (T)(object)array;

//}


// NeedNot Sure In ITEM TYPE
//for (int i = 0; i<array.Length; i++)
//{                
//    //read element value
//    var item = typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeSerializer.ArrayElementTypeID, stream, true);//false
//    CalculateIndices(itemIndex++, indices, array.GetDimensionsLengths()); // set current indices
//    array.SetValue(item, indices);                
//}


#endregion ------------------------------- GARBAGE ----------------------------------------