﻿using System;
using System.IO;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
using DDDD.Core.IO;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;

namespace DDDD.Core.Serialization.Binary
{
    /// <summary>
    /// Type Serialize Generator for classes which implement IList or IList{}.
    /// </summary>
    internal class SerializeGenerator_List : ITypeSerializerGenerator
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_List:
        ///     - all classes that based on IList;
        ///     - and also is not an Array;
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return (!type.IsArray  && type.IsImplement_List() );
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region   ---------------------------------   READ / WRITE LIST HANDLERS ------------------------------------------
                
        // 1 way -  NeedNotToSureInItemType -( NullValueEnabled(sealed classes) + NullValueNotEnabled(structs) )
        static void WriteList_NeedNotToSureInItemType_ToStream<T,TElement>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {   
            //write value typeID - or [null]'s typeID( 0 )        
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            var listInstance = instanceValue as IList<TElement>;

            // write IList length - List Length
            BinaryReaderWriter.WriteT_int32(stream, listInstance.Count);

            //write all IList elements to Stream
            for (int i = 0; i < listInstance.Count; i++)
            {
                //write element  data                              
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.RuntimeIDents.Arg1TypeID, stream, listInstance[i]);
            }

        }

        static T ReadList_NeedNotToSureInItemType_FromStream<T, TElement>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T resultValue = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return resultValue;

            ///now cast to IList instance
            var list = (IList<TElement>)resultValue;// 
            
            for (int i = 0; i < collectionLength; i++)
            {
                //read element value and add it to the list
                list.Add( typeSerializer.RaiseElementDeserializeFromStreamTHandler<TElement>(typeSerializer.RuntimeIDents.Arg1TypeID, stream) );                
            }

            return (T)list;
        }
        


        // 2 way - NeedToSureInItemType ( NullValueEnabled( not sealed classes, interfaces)  )
        static void WriteList_NeedToSureInItemType_ToStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
        {
            // write value typeID - or [null]'s typeID( 0 )
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            var listInstance = instanceValue as IList;

            // write IList length - List Length
            BinaryReaderWriter.WriteT_int32(stream, listInstance.Count);
            
            // write all IList elements to Stream
            for (int i = 0; i < listInstance.Count; i++)
            {
                var element = listInstance[i];
                int elementRealTypeID = typeSerializer.GetElementTypeID(element);
                if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementRealTypeID) ) continue;  // end - value was Null
                
                //write element  data                   
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler( elementRealTypeID, stream, element);                
            }
            
        }

        
        static T ReadList_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T resultValue = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return resultValue;

            ///now cast to IList instance
            var list = (IList)resultValue;//   

            for (int i = 0; i < collectionLength; i++)
            {
                //reading element RealTypeID : for primitive or CustomContract -no difference
                ushort elementRealTypeId = BinaryReaderWriter.ReadT_uint16(stream);

                //null value element
                if (elementRealTypeId == 0)
                { list.Add( null); }     // null value element
                else
                {
                    //read element value
                    list.Add( typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID= true) );//false                                        
                }
            }

            return (T)list;
        }

        #endregion   ---------------------------------   READ / WRITE LIST HANDLERS ------------------------------------------


        //ObservableColletion<T> can't be activated by Ctor with Length as standart IList Collection Types
        #region   ---------------------------------   READ / WRITE ObservableCollection HANDLERS ------------------------------------------
        
        static T ReadObservableCollection_NeedNotToSureInItemType_FromStream<T, TElement>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T resultValue = typeSerializer.ReadObservableCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return resultValue;  
            
            ///now cast to IList instance
            var list = (IList<TElement>)resultValue;//   
       
            for (int i = 0; i < collectionLength; i++)
            {
                //read element value and add it to the list
                list.Add(typeSerializer.RaiseElementDeserializeFromStreamTHandler<TElement>(typeSerializer.RuntimeIDents.Arg1TypeID, stream));
            }

            return (T)list;
        }


        static T ReadObservableCollection_NeedToSureInItemType_FromStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T resultValue = typeSerializer.ReadObservableCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return resultValue;

            ///now cast to IList instance
            var list = (IList)resultValue;//   

            for (int i = 0; i < collectionLength; i++)
            {
                //reading element RealTypeID : for primitive or CustomContract -no difference
                ushort elementRealTypeId = BinaryReaderWriter.ReadT_uint16(stream);

                //null value element
                if (elementRealTypeId == 0)
                { list.Add(null); }     // null value element
                else
                {
                    //read element value
                    list.Add(typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementRealTypeId, stream, readedTypeID = true));//false                                        
                }
            }

            return (T)list;
        }
        
        #endregion   ---------------------------------   READ / WRITE LIST HANDLERS ------------------------------------------








        static MethodInfo Get_WriteListMethod<T>(ITypeSerializer<T> tpserializer)
        {
            if (tpserializer.TargetType.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_List).GetMethod(nameof(WriteList_NeedToSureInItemType_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                       .MakeGenericMethod(tpserializer.TargetType);
            }
            else
            {
                return typeof(SerializeGenerator_List).GetMethod(nameof(WriteList_NeedNotToSureInItemType_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                      .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.Arg1Type);
            }

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(Get_WriteListMethod)}() - such List Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");

        }


        static MethodInfo Get_ReadListMethod<T>(ITypeSerializer<T> tpserializer)
        {
            if (tpserializer.TargetTypeEx.IsObservableCollection)
            {
                if (tpserializer.TargetType.IsNeedToSureInDataType())
                {
                    return typeof(SerializeGenerator_List).GetMethod(nameof(ReadObservableCollection_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                           .MakeGenericMethod(tpserializer.TargetType);
                }
                else
                {
                    return typeof(SerializeGenerator_List).GetMethod(nameof(ReadObservableCollection_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                           .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.Arg1Type);
                }
            }
            else // for not observableCollection  collection types
            {
                if (tpserializer.TargetType.IsNeedToSureInDataType())
                {
                    return typeof(SerializeGenerator_List).GetMethod(nameof(ReadList_NeedToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                           .MakeGenericMethod(tpserializer.TargetType);
                }
                else
                {
                    return typeof(SerializeGenerator_List).GetMethod(nameof(ReadList_NeedNotToSureInItemType_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                           .MakeGenericMethod(tpserializer.TargetType, tpserializer.TargetTypeEx.Arg1Type);
                }
            }


            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(Get_ReadListMethod)}() - such List Type  is not supported by this serializer [{tpserializer.TargetType.FullName}] ");

        }





        #region -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializer<T> tpSerializer)
        {
            try
            {
                //algorithm:
                // void WriteIListToStream<T>(TypeSerializer<T>  typeSerializer, Stream stream,  T instanceValue)                

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");

                var typeProcessorInstance = Expression.Constant(tpSerializer);

                //choose optimal IList Writing method
                MethodInfo methodWrite_IList_ToStream = Get_WriteListMethod(tpSerializer);   

                return Expression.Lambda<SD.SerializeToStreamT<T>>
                                                        (
                                                         Expression.Call
                                                         (
                                                           methodWrite_IList_ToStream
                                                         , typeProcessorInstance
                                                         , parameterStream
                                                         , parameterInputData

                                                         )
                                                        , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                        , new[] { parameterStream, parameterInputData }

                                                        );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
          
        }
        

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializer<T> tpSerializer)
        {
            try
            {

                //algorithm:
                //  T ReadIListFromStream<T>(TypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");

                var typeProcessorInstance = Expression.Constant(tpSerializer);

                //choose optimal IList Reading method
                MethodInfo methodRead_List_FromStream = Get_ReadListMethod(tpSerializer);

                return Expression.Lambda<SD.DeserializeFromStreamT<T>>(
                                                           Expression.Call(
                                                             methodRead_List_FromStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , tpSerializer.TargetTypeEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] { parameterStream, parameterReadedTypeID }
                                                           );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(GenerateDeserialize_ReadExpression)}() : Message -[{exc.Message}]  " , exc);
            }

        }


        
        #endregion -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer<T> tpSerializer = null)
        {
            try
            {
                // void WriteIList_NeedToSureInItemType_ToStream<T>(TypeSerializer<T>  typeSerializer, Stream stream,  T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeSerializer");

                //choose optimal IList Writing method
                MethodInfo methodWrite_IList_ToStream = Get_WriteListMethod(tpSerializer);

                return Expression.Lambda<SD.SerializeToStreamTDbg<T>>
                                                      (
                                                       Expression.Call
                                                       (
                                                         methodWrite_IList_ToStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterInputData

                                                       )
                                                      , parameterStream
                                                      , parameterInputData
                                                      , parameterTypeProcessor
                                                      );


            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                //  T ReadIList_NeedToSureInItemType_FromStream<T>(TypeSerializer<T> typeSerializer, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeSerializer");


                var typeProcessorInstance = Expression.Constant(tpSerializer);

                //choose optimal IList Reading method
                MethodInfo methodRead_IList_FromStream = Get_ReadListMethod(tpSerializer);


                return Expression.Lambda<SD.DeserializeFromStreamTDbg<T>>(
                                                       Expression.Call(
                                                         //typeProcessorInstance
                                                         methodRead_IList_FromStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       , parameterTypeProcessor
                                                       );


            
             }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_List)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

    
        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif

        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [IList ] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;//  throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException("SerializeGenerator_List - can't have the Final List of Supported Types");
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers()
        {
            throw new NotSupportedException("SerializeGenerator_List - can't have the Final List of Supported Types");
        }

     

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}



#region --------------------------------- GARBAGE -------------------------------------


////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = BinaryReaderWriter.ReadT_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T);//just the same as return null  for reference types
//    }
//}

////reading collection Length and creating It
//int collectionLength = BinaryReaderWriter.ReadT_int32(stream); //read collection Length

////now create IList instance
//var list = (IList<TElement>)TypeActivator.CreateInstanceT<T>(TypeActivator.DefaultCtorSearchBinding);//   





// write all IList elements to Stream
//foreach (object element in listInstance)
//{
//    ushort elementTypeID = typeSerializer.SetSerializer.GetTypeID(element);

//    if (elementTypeID == 0) //null data
//    { 
//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.WriteT_uint16(stream, elementTypeID);                    
//    }
//    else
//    {
//        //write element  data                   
//        typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementTypeID, stream, element);
//    }
//}


//foreach (TElement element in listInstance)
//{    //write element  data                              
//     typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID , stream, element);
//}

//// 2 way   NeedNotToSureInItemType + Nullable(items are - sealed  classes)
//public static void WriteIList_NeedNotToSureInItemType_Nullable_ToStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write value typeID - or [null]'s typeID( 0 )
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var listInstance = instanceValue as IList;

//    //write Length
//    BinaryReaderWriter.WriteT_int32(stream, listInstance.Count); // write IList length - List Length

//    //write all IList elements to Stream
//    foreach (object element in listInstance)
//    {             
//        if (element == null) //null data
//        {
//            BinaryReaderWriter.WriteT_uint16(stream, 0);                    
//        }
//        else
//        {
//            //write element  data                     
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(typeSerializer.Arg1TypeID, stream, element);
//        }
//    }

//}//

//public static void WriteIListToStream<T>(ITypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write Length
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    var listInstance = instanceValue as IList;

//    BinaryReaderWriter.WriteT_int32(stream, listInstance.Count); // write IList length - List Length

//    //write all IList elements to Stream
//    foreach (object element in listInstance)
//    {
//        ushort elementTypeID = typeSerializer.SetSerializer.GetTypeID(element);

//        //write elementTypeID: 0 -null or real data type ModelId. For primitive or CustomContract -no difference
//        BinaryReaderWriter.WriteT_uint16(stream, elementTypeID);

//        if (elementTypeID == 0) //null data
//        {
//            continue;
//        }
//        else
//        {
//            //write element  data 
//            //typeSerializer.RaiseElementSerializeToStreamTHandler(elementTypeID, stream, element);
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementTypeID, stream, element);
//        }
//    }

//}

#endregion --------------------------------- GARBAGE -------------------------------------