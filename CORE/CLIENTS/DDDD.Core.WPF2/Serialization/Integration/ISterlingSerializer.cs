﻿using System;
using System.IO;


namespace DDDD.Core.Serialization
{
    /// <summary>
    ///     Serializer for SterlingDB
    /// </summary>
    public interface ISterlingSerializer
    {
        /// <summary>
        /// If we able to serialize this Data Item by this Serializer? - if yes then return true, otherwise return false.
        /// If (dataitem == null) && (throwOnNullVlue == false) we return false 
        /// or throw NullReferenceException if (dataitem == null) && (throwOnNullVlue == true)
        /// </summary>
        /// <param name="dataItem"></param>
        /// <returns></returns>
        bool CanSerialize(object dataItem, bool throwOnNullValue);


        /// <summary>
        /// If we able to serialize this Data Type by this Serializer? - if yes then return true, otherwise return false.
        /// </summary>
        /// <param name="targetType">The target</param>
        /// <returns>True if it can be serialized</returns>
        bool CanSerialize(Type targetType);


        /// <summary>
        /// If we able to serialize this Data Type by this Serializer? - if yes then return true, otherwise return false.
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <returns>True if it can handle it</returns>
        bool CanSerialize<T>();



        /// <summary>
        ///     Serialize the object
        /// </summary>
        /// <param name="target">The target</param>
        /// <param name="writer">The writer</param>
        void Serialize(object target, BinaryWriter writer);


        /// <summary>
        ///     Deserialize the object
        /// </summary>
        /// <param name="type">The type of the object</param>
        /// <param name="reader">A reader to deserialize from</param>
        /// <returns>The deserialized object</returns>
        object Deserialize(Type type, BinaryReader reader);


        /// <summary>
        ///     Typed deserialization
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="reader">The reader</param>
        /// <returns>The type</returns>
        T Deserialize<T>(BinaryReader reader);

    }


    //raptor DB ? 

}
