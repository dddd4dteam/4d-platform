﻿
#if SERVER  || WPF 
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using DDDD.Core.Extensions;

using DDDD.Core.Resources;
using DDDD.Core.Collections.Compare;
using DDDD.Core.Reflection;
using DDDD.Core.Serialization.Json;
using DDDD.Core.Serialization.Binary;
using DDDD.Core.Reflection.Compare;

namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Dynamic assemblies with debugging methods Precompiler
    /// </summary>
    public class DebugCodeCompiler
    {


       

        [Conditional("DEBUG")]
        public static void GenerateDebugAssemblyWith_SortComparerHandlerDbg<T>(params KeyValuePair<string, string>[] sortMembers)
        {
            try
            {
                Type targetType = typeof(T);

                //create assembly 
                string AssemblyBaseName = "DDDD.Core.DynamicComparer_" + targetType.Name + ".SortStubs2.Debug";// serializer.GetTSSerializerConventionIdentityKey() + ".SerializationStubs.Debug";
                string TypeName = "ComparerOf" + targetType.GetConventionTypeName();

                AssemblyBaseName.BuildDynamicAssembly(TypeName
                    , (tpBldr) =>
                     {
                         //create Compare Method
                         var compare_T_MethodDefenition = tpBldr.DefineMethod("Compare"
                                   , MethodAttributes.Public | MethodAttributes.Static
                                   , typeof(int), new[] { targetType, targetType } );
                         
                         var dynComparer = DynamicComparer<T>.AddOrUseExists(sortMembers);
                         dynComparer.CompareFuncExpression.CompileToMethod(compare_T_MethodDefenition);

                         // if we use IL Method Body code generation                
                         //ILGenerator generator = compare_T_MethodDefenition.GetILGenerator();
                         //DynamicComparer<T>.BuildCompareIntoILGenerator(generator, dynComparer);

                     }
                    );
                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException( RCX.ERR_InvalidOperation.Fmt(nameof(DebugCodeCompiler) , nameof(GenerateDebugAssemblyWith_SortComparerHandlerDbg) , exc.Message)
                                                   );
            }
        }


        #region ------------------------------------ ITypeSetSerializer -------------------------------------



        /// <summary>
        /// Generate  dynamic assembly with de/serialize static methods for each of Types of ITypeSetSerializer.
        /// </summary>
        /// <param name="serializer"></param>
        [Conditional("DEBUG")]
        public static void GenerateDebugAssemblyWith_TSSHandlersDbg(ITypeSetSerializer serializer)
        {
            try
            {
                var domainTypesSerializers = BinarySerializer.GetTypeSerializers( );

                if (domainTypesSerializers.Count > 0)
                {

                    //create assembly 
                    string AssemblyBaseName = "DDDD.Core." + BinarySerializer.GetConventionIdentityKey() + ".SerializationStubs.Debug";
                    string ModuleName = AssemblyBaseName + ".dll";

                    var ab = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(AssemblyBaseName), AssemblyBuilderAccess.RunAndSave);
                    var modb = ab.DefineDynamicModule(ModuleName);

                    foreach (var tpSerializer in domainTypesSerializers)
                    {

                        var methodGenerate_T_DeSerializationMethods =
                                  typeof(DebugCodeCompiler).GetMethod(nameof(Build_T_WithSerializationMethodsPair))
                                                                     .MakeGenericMethod(tpSerializer.Accessor.TAccess);

                        //exclude all Default Contracts
                        // -------------------- || -----------------------

                        var methodGenerateSerializeExpression = typeof(ITypeSerializerGenerator).GetMethod(nameof(ITypeSerializerGenerator.GenerateSerialize_WriteExpressionDbg), BindingFlags.Instance | BindingFlags.Public)
                                                                  .MakeGenericMethod(tpSerializer.Accessor.TAccess);

                        var methodGenerateDeserializeGenerator = typeof(ITypeSerializerGenerator).GetMethod(nameof(ITypeSerializerGenerator.GenerateDeserialize_ReadExpressionDbg), BindingFlags.Instance | BindingFlags.Public)
                                                                  .MakeGenericMethod(tpSerializer.Accessor.TAccess);

                        var serializeExpression = methodGenerateSerializeExpression.Invoke(tpSerializer.TypeSerializeGenerator, new[] { tpSerializer });
                        var deserializeExpression = methodGenerateDeserializeGenerator.Invoke(tpSerializer.TypeSerializeGenerator, new[] { tpSerializer });

                        methodGenerate_T_DeSerializationMethods.Invoke(null,
                                                    new[] { modb, tpSerializer, serializeExpression, deserializeExpression });

                    }

                    ab.Save(ModuleName);
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(nameof(DebugCodeCompiler), nameof(GenerateDebugAssemblyWith_TSSHandlersDbg), exc.Message) );
            }

        }


        /// <summary>
        /// Building TypeBuilder with Serialization MethodsPair of T Type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modBilder"></param>
        /// <param name="tpSerializer"></param>
        /// <param name="SerializeExpression"></param>
        /// <param name="DeserializeExpression"></param>
        public static void Build_T_WithSerializationMethodsPair<T>(ModuleBuilder modBilder,
                                                                           ITypeSerializer<T> tpSerializer,
                                                                           Expression<SD.SerializeToStreamTDbg<T>> SerializeExpression,
                                                                           Expression<SD.DeserializeFromStreamTDbg<T>> DeserializeExpression
                                                                     )
        {
            try
            {
                var type = typeof(T);

                var realType = type.GetWorkingTypeFromNullableType();// Tools.GetWorkingTypeFromNullableType();
                string tpConventionName = tpSerializer.Accessor.TAccess.GetConventionTypeName();
                string TypeSerializerName = "DDDD.TSS.Serializer_" + tpConventionName;
                string SerializeMethodName = "Serialize_" + tpConventionName;
                string DeserializeMethodName = "Deserialize_" + tpConventionName;

                //new TypeBuilder for type
                TypeBuilder tpBuilder = modBilder.DefineType(TypeSerializerName, TypeAttributes.Public);


                //create Serialize Method 
                var serialize_T_Method = tpBuilder.DefineMethod(SerializeMethodName
                                                            , MethodAttributes.Public | MethodAttributes.Static
                                                            , null
                                                            , new[] { typeof(Stream), typeof(T), typeof(ITypeSerializer<T>) });

                serialize_T_Method.DefineParameter(1, ParameterAttributes.None, "stream");
                serialize_T_Method.DefineParameter(2, ParameterAttributes.None, "data");
                serialize_T_Method.DefineParameter(3, ParameterAttributes.None, "typeProcessor");
                SerializeExpression.CompileToMethod(serialize_T_Method);


                // create Deserialize Method 
                var deserialize_T_Method = tpBuilder.DefineMethod(DeserializeMethodName
                                                                    , MethodAttributes.Public | MethodAttributes.Static
                                                                    , typeof(object)
                                                                    , new[] { typeof(Stream), typeof(bool), typeof(ITypeSerializer<T>) });

                deserialize_T_Method.DefineParameter(1, ParameterAttributes.None, "stream");
                deserialize_T_Method.DefineParameter(2, ParameterAttributes.None, "readedTypeID");
                deserialize_T_Method.DefineParameter(3, ParameterAttributes.None, "typeProcessor");
                DeserializeExpression.CompileToMethod(deserialize_T_Method);



                //Build Dynamic Type accessor - building dynamic Get/Set-ters funcs/actions
                tpSerializer.BuildDynamicTypeAccessor(tpBuilder);

                tpBuilder.CreateType();

            }
            catch ( Exception  exc )
            {
                throw   new  InvalidOperationException(RCX.ERR_InvalidOperation.Fmt( nameof(DebugCodeCompiler), nameof(Build_T_WithSerializationMethodsPair),  exc.Message ));
            }
        }




        #endregion ------------------------------------ ITypeSetSerializer -------------------------------------


        #region ---------------------------- ITypeSerializerJson -----------------------------


        public static void BuildJson_T_SerializeToStringMethodDbg<T>(ModuleBuilder modBilder,
                                                                          ITypeSerializerJson<T> tpSerializer,
                                                                          Expression<SDJson.SerializeToStringTDbg<T>> SerializeToStringExpressionDbg                                                                 
                                                                    )
        {
            try
            {
                var typeInfo = TypeInfoEx.Get( typeof(T) );

                var realType = typeInfo.WorkingType;
                string tpConventionName = tpSerializer.Accessor.TAccess.GetConventionTypeName();
                string TypeSerializerName = "DDDD.Core.Serialization.Json.Serializer_" + tpConventionName;
                string SerializeToStringMethodName = "SerializeToString_" + tpConventionName;
                
                //new TypeBuilder for type
                TypeBuilder tpBuilder = modBilder.DefineType(TypeSerializerName, TypeAttributes.Public);
                
                //create Serialize Method 
                var serialize_T_Method = tpBuilder.DefineMethod(SerializeToStringMethodName
                                                            , MethodAttributes.Public | MethodAttributes.Static
                                                            , null
                                                            , new[] { typeof(TextWriter), typeof(T), typeof(bool), typeof(ITypeSerializerJson<T>) });

                serialize_T_Method.DefineParameter(1, ParameterAttributes.None, "writer");
                serialize_T_Method.DefineParameter(2, ParameterAttributes.None, "data");
                serialize_T_Method.DefineParameter(3, ParameterAttributes.None, "needToWriteTypeAQName");
                serialize_T_Method.DefineParameter(4, ParameterAttributes.None, "typeProcessor");
                SerializeToStringExpressionDbg.CompileToMethod(serialize_T_Method);
                                
                //Build Dynamic Type accessor - building dynamic Get/Set-ters funcs/actions
                tpSerializer.BuildDynamicTypeAccessor(tpBuilder);

                tpBuilder.CreateType();

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(nameof(DebugCodeCompiler), nameof(Build_T_WithSerializationMethodsPair), exc.Message));
            }
        }




        /// <summary>
        /// Generate  dynamic assembly with de/serialize static methods for each of Types of ITypeSetSerializer.
        /// </summary>
        /// <param name="serializer"></param>
        [Conditional("DEBUG")]
        public static void GenerateDebugAssembly_JsonSerializeToStringTHandlerDbg<T>(ITypeSerializerJson<T> tpSerializer)
        {
            try
            {
                //create assembly 
                string AssemblyBaseName = "DDDD.Core.Serializetion.Json.SerializationStubs.Debug";
                string ModuleName = AssemblyBaseName + ".dll";

                var assBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(AssemblyBaseName), AssemblyBuilderAccess.RunAndSave);
                var modBuilder = assBuilder.DefineDynamicModule(ModuleName);

                // tpSerializer for T
                var methodGenerate_T_DeSerializationMethods =
                                typeof(DebugCodeCompiler).GetMethod(nameof(BuildJson_T_SerializeToStringMethodDbg))
                                                         .MakeGenericMethod(typeof(T));
                               
               //var methodGenerateSerializeExpression = typeof(ITypeSerializerGeneratorJson).GetMethod(nameof(ITypeSerializerGeneratorJson.GenerateSerializeToString_WriteExpressionDbg)) //, BindingFlags.Instance | BindingFlags.Public
               //                                           .MakeGenericMethod(tpSerializer.Accessor.TAccess);

                var serializeExpression = tpSerializer.TypeSerializeGenerator
                    .GenerateSerializeToString_WriteExpressionDbg<T>(tpSerializer); 
              
               methodGenerate_T_DeSerializationMethods.Invoke(null,
                                            new object[] { modBuilder, tpSerializer, serializeExpression });

               assBuilder.Save(ModuleName, PortableExecutableKinds.Preferred32Bit, ImageFileMachine.AMD64 );               
                                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(nameof(DebugCodeCompiler), nameof(GenerateDebugAssembly_JsonSerializeToStringTHandlerDbg), exc.Message));
            }
        }

        
        #endregion ---------------------------- ITypeSerializerJson -----------------------------




    }
}



#endif