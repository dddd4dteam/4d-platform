﻿
using System;
using System.Linq;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;


using DDDD.Core.Threading;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Serialization.Json;
using DDDD.Core.Serialization.Binary;

namespace DDDD.Core.Serialization
{

    /// <summary>
    ///  Analayze Types for possibility to  Binary and Json Serialization.
    /// </summary>
    public class TypeSerializeAnalyser
    {

        #region --------------------------- CONST && FIELDS -----------------------------

        private static readonly object locker = new object();

        #endregion --------------------------- CONST && FIELDS -----------------------------



        #region  ----------------------------------- BINARY TYPESERIALIZER GENERATORS -------------------------------------------

        /// <summary>
        /// It means first  Generator that  can  generate type Handlers will be used. 
        /// <para/> i.e. last added custom generator will be used first to generate type Handlers from it's Type Kind -because it'll be first item from top of the stack.
        /// </summary>
        static LazyAct<List<ITypeSerializerGenerator>> LA_BinSerializeGenerators = LazyAct<List<ITypeSerializerGenerator>>.Create(
            (args) =>
            {
                //Loading all existed type De/Serialzie Generators 
                List<ITypeSerializerGenerator> generatorsResult = new List<ITypeSerializerGenerator>();

                var generatorTypes = typeof(ITypeSerializerGenerator).Assembly.GetTypes()
                          .Where(tp => typeof(ITypeSerializerGenerator).IsAssignableFrom(tp) 
                                              && tp.IsClass && !tp.IsAbstract)    .ToList();

                if (generatorTypes.Count == 0) return generatorsResult;

                foreach (var genType in generatorTypes)
                {
                    var generator = TypeActivator.CreateInstanceTBaseLazy<ITypeSerializerGenerator>(genType);
                    generatorsResult.Add(generator);
                }
                return generatorsResult;
            }
            , null//locker
            , null //args
            );



        internal static void Init_BinSerializeGenerators()
        {
            //Load Default TypeSerializers From SerializeGenerators

            string currentGeneratorName = "";
            try
            {
                foreach (var genrtr in LA_BinSerializeGenerators.Value)
                {
                    currentGeneratorName = genrtr.GetType().FullName;
                    if (genrtr.CanLoadDefaultTypeSerializers)
                    {
                        genrtr.LoadDefaultTypeSerializers();
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(
                    " ERROR in [{0}].[{1}]() - Message : [{2}] ".Fmt(nameof(TypeSerializeAnalyser), nameof(Init_JsonSerializeGenerators), "loading Default TypeSerializers error " + exc.Message));
            }
        }




        /// <summary>
        /// Get Type Serialize Generator by targetType  for Binary Serializer
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        internal static ITypeSerializerGenerator GetBinTypeSerializerGeneratorForType(Type targetType)
        {
            return LA_BinSerializeGenerators.Value.Where(sg => sg.CanHandle(targetType)).FirstOrDefault();
        }



        #endregion -----------------------------------BINARY SERIALIZER GENERATORS -------------------------------------------



        #region  ----------------------------------- JSON TYPESERIALIZER GENERATORS -------------------------------------------

        /// <summary>
        /// It means first  Generator that  can  generate type Handlers will be used. 
        /// <para/> i.e. last added custom generator will be used first to generate type Handlers from it's Type Kind -because it'll be first item from top of the stack.
        /// </summary>
        static LazyAct<List<ITypeSerializerGeneratorJson>> LA_JsonSerializeGenerators = LazyAct<List<ITypeSerializerGeneratorJson>>.Create(
            (args) =>
            {
                //Loading all existed type De/Serialzie Generators 
                List<ITypeSerializerGeneratorJson> generatorsResult = new List<ITypeSerializerGeneratorJson>();

                var generatorTypes = typeof(ITypeSerializerGeneratorJson).Assembly.GetTypes()
                          .Where(tp => typeof(ITypeSerializerGeneratorJson).IsAssignableFrom(tp) && tp.IsClass && !tp.IsAbstract).ToList();

                if (generatorTypes.Count == 0) return generatorsResult;

                foreach (var genType in generatorTypes)
                {
                    var generator = TypeActivator.CreateInstanceTBaseLazy<ITypeSerializerGeneratorJson>(genType);
                    generatorsResult.Add(generator);
                }
                return generatorsResult;
            }
            , null//locker
            , null //args
            );

         

        internal static void  Init_JsonSerializeGenerators()
        {
            //Load Default TypeSerializers From SerializeGenerators

            string currentGeneratorName = "";
            try
            {
                foreach (var genrtr in LA_JsonSerializeGenerators.Value)
                {
                    currentGeneratorName = genrtr.GetType().FullName;
                    if (genrtr.CanLoadDefaultTypeSerializers)
                    {
                        genrtr.LoadDefaultTypeSerializers();
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(  
                    " ERROR in [{0}].[{1}]() - Message : [{2}] ".Fmt(nameof(TypeSerializeAnalyser), nameof(Init_JsonSerializeGenerators), "loading Default TypeSerializers error " + exc.Message  ) );                    
            }
        }




        /// <summary>
        /// Get Type Serialize Generator by targetType for JsonSerializer
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        internal static ITypeSerializerGeneratorJson GetJsonTypeSerializerGeneratorForType(Type targetType)
        {
            return LA_JsonSerializeGenerators.Value.Where(sg => sg.CanHandle(targetType)).FirstOrDefault();
        }



        #endregion -----------------------------------TYPESERIALIZER GENERATORS -------------------------------------------



        /// <summary>
        /// <para/>  Check -if targetType Support Serialization.
        /// <para/>  Type Not Suppot Serialization in CASES:
        /// <para/> 1 - targetType is IsPointer
        /// <para/> 2 - targetType is IsDelegate
        /// <para/> 3 - targetType is IsAutoClass
        /// <para/> 4 - targetType is IsCOMObject
        /// </summary>
        /// <param name="typeInfo"></param>
        public static void CheckSerializationSupport(TypeInfoEx typeInfo)
        {
            if (typeInfo.OriginalType.IsPointer) { throw new InvalidOperationException("Pointer Types can't be serialized - [{0}]".Fmt(typeInfo.OriginalType.FullName)); }
            else if (typeInfo.IsDelegate()) { throw new InvalidOperationException(" Delegate Types can't be serialized - [{0}]".Fmt(typeInfo.OriginalType.FullName)); }
            else if (typeInfo.OriginalType.IsAutoClass) { throw new InvalidOperationException(" Autoclass Types can't be serialized - [{0}]".Fmt(typeInfo.OriginalType.FullName)); }
            else if (typeInfo.OriginalType.IsCOMObject) { throw new InvalidOperationException(" COM object Types can't be serialized - [{0}]".Fmt(typeInfo.OriginalType.FullName)); }
        }



        /// <summary>
        ///  Check if current TSS instance can serialize  Type - targetType.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool CanSerialize(Type targetType)
        {
            // 1 if ICollection  -Array  get ArrayElementType 
            // 2 if ICollection  -List get   Arg1Type 
            // 3 if ICollection  -Dictionary get  Arg1Type + Arg2Type            
            // 4 simple Test     -Contains in Dictionary

            if (targetType.IsArray)
            {
                return CanSerialize(targetType.GetElementType());
            }
            else if (TypeInfoEx.IsImplement_List(targetType))
            {
                return CanSerialize(targetType.GetGenericArguments()[0]);
            }
            else if (TypeInfoEx.IsImplement_Dictionary(targetType))
            {
                return (CanSerialize(targetType.GetGenericArguments()[0])
                     && CanSerialize(targetType.GetGenericArguments()[1]));
            }

            //else if (targetType.IsAutoAdding()) { return true; }

            // it's nullable or usual struct or class type - all of them should already contained in TypeSerializerIDByType
            //else return TypeSerializerIDByType.ContainsKey(targetType);
            return true;
        }




        /// <summary>
        /// Analyze-check targetType:
        /// <para/> 1 Colllect all of Types from targetType members Tree structure + targetType itself. 
        /// <para/>  2 Then check For - What types can be serializable by Json serializers. 
        /// <para/> If such Type can't be serialized the output Type[] will be Empty.
        /// <para/>... targetType NEEDS NO ANALYSING IN CASES:
        ///  <para/>      1 targetTypeEx.Is4DPrimitive
        ///  <para/>      2 targetTypeEx.OriginalType == Tps.T_object
        ///  <para/>      3 targetTypeEx.OriginalType.IsEnum        /// 
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static List<Type> AnalyzeCollectTypesJson(TypeInfoEx targetTypeEx
                                               , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                               , BindingFlags propertiesBinding = BindingFlags.Default
                                               , BindingFlags fieldsBinding = BindingFlags.Default

                                               , List<string> onlyMembers = null
                                               , List<string> ignoreMembers = null
                                                )
        {

            //CHECK-FOR serializable Type - throw Exceptions if not supported Type
            CheckSerializationSupport(targetTypeEx);


            List<Type> collectedTypes = new List<Type>();
            
            // Type's members Tree structure analising - collecting Types
            AnalyzeCollectTypes(targetTypeEx, ref collectedTypes
                            , selector, propertiesBinding, fieldsBinding
                            , onlyMembers, ignoreMembers);

            //INVERT  FOR LOGICAL ADDING SEQUENCE
            collectedTypes = collectedTypes.Invert();            

            return collectedTypes;
        }


        static void AnalyzeCollectTypes(TypeInfoEx  targetTypeEx
                                               , ref List<Type> collectedTypes
                                               , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                               , BindingFlags propertiesBinding = BindingFlags.Default
                                               , BindingFlags fieldsBinding = BindingFlags.Default

                                               , List<string> onlyMembers = null
                                               , List<string> ignoreMembers = null
            )
        {          

            // DO NOT COLLECT TYPES  if 4DPrimitive OR Object
            if ( IsSimplyAddable(targetTypeEx))
            {    return; }            

         
            // ENUM 
            if (targetTypeEx.WorkingType.IsEnum)
            {
                // NOT NULLABLE ENUM TYPE
                if (collectedTypes.NotContains(targetTypeEx.WorkingType))
                    collectedTypes.Add(targetTypeEx.WorkingType);

                // +  NULLABLE ENUM TYPE
                var nullableEnumTp = typeof(Nullable<>)
                                    .MakeGenericType(targetTypeEx.WorkingType);
                if (collectedTypes.NotContains(nullableEnumTp))
                    collectedTypes.Add(nullableEnumTp);
                
                return;
            }

            // Interface / Abstract Class - Add this Type
            else if (targetTypeEx.IsAbstract || targetTypeEx.IsInterface )
            {
                // Add  Type
                if (collectedTypes.NotContains(targetTypeEx.OriginalType))
                    collectedTypes.Add(targetTypeEx.OriginalType);
                return;                
            }

            //Array
            else if (targetTypeEx.CollectionType == CollectionTypeEn.Array)
            {
                //Add Array Type
                if (collectedTypes.NotContains(targetTypeEx.OriginalType))
                    collectedTypes.Add(targetTypeEx.OriginalType);

                //CHECK-FOR serializable  ArrayElementType - throw Exceptions if not supported Type
                var arrElementType = targetTypeEx.ArrayElementType.GetTypeInfoEx();
                CheckSerializationSupport(arrElementType);

                //Add  Array Element  Type
                if (collectedTypes.NotContains(targetTypeEx.ArrayElementType))
                    collectedTypes.Add(targetTypeEx.ArrayElementType);

                
                AnalyzeCollectTypes(arrElementType
                                  , ref collectedTypes, selector, propertiesBinding
                                  , fieldsBinding, onlyMembers, ignoreMembers);
                
                return; 
            }

            // IList
            else if (targetTypeEx.CollectionType == CollectionTypeEn.IList)
            {
                //Add List Type
                if (collectedTypes.NotContains(targetTypeEx.OriginalType))
                    collectedTypes.Add(targetTypeEx.OriginalType);

                //CHECK-FOR serializable  Arg1 Type - throw Exceptions if not supported Type
                var arg1TypeEx = targetTypeEx.Arg1Type.GetTypeInfoEx();
                CheckSerializationSupport(arg1TypeEx);

                //Add  List Arg1 Type
                if (collectedTypes.NotContains(targetTypeEx.Arg1Type))
                    collectedTypes.Add(targetTypeEx.Arg1Type);
                                
                AnalyzeCollectTypes(arg1TypeEx
                                   , ref collectedTypes, selector, propertiesBinding
                                   , fieldsBinding, onlyMembers, ignoreMembers);

                return; 

            }

            // IDictionary
            else if (targetTypeEx.CollectionType == CollectionTypeEn.IDictionary)
            {
                //Add DIctionary Type
                if (collectedTypes.NotContains(targetTypeEx.OriginalType))
                    collectedTypes.Add(targetTypeEx.OriginalType);


                //CHECK-FOR serializable  Arg1 Type - throw Exceptions if not supported Type
                var arg1TypeEx = targetTypeEx.Arg1Type.GetTypeInfoEx();
                CheckSerializationSupport(arg1TypeEx);
                
                //Add DIctionary Arg1 Type
                if (collectedTypes.NotContains(targetTypeEx.Arg1Type))
                    collectedTypes.Add(targetTypeEx.Arg1Type);
                
               
                AnalyzeCollectTypes(arg1TypeEx
                                   , ref collectedTypes, selector, propertiesBinding
                                   , fieldsBinding, onlyMembers, ignoreMembers);


                //CHECK-FOR serializable  Arg2 Type - throw Exceptions if not supported Type
                var arg2TypeEx = targetTypeEx.Arg1Type.GetTypeInfoEx();
                CheckSerializationSupport(arg2TypeEx);


                //Add DIctionary Arg2 Type
                if (collectedTypes.NotContains(targetTypeEx.Arg2Type))
                    collectedTypes.Add(targetTypeEx.Arg2Type);

                AnalyzeCollectTypes(arg2TypeEx
                                   , ref collectedTypes, selector, propertiesBinding
                                   , fieldsBinding, onlyMembers, ignoreMembers);

                return;
            }

            //Custom Struct/Class
            else if (targetTypeEx.IsICollection == false )
            {
                // ADd STRUCT -NOT NULLABLE   OR  CLASS TYPE
                if (collectedTypes.NotContains(targetTypeEx.WorkingType))
                    collectedTypes.Add(targetTypeEx.WorkingType);
                // ADd STRUCT - NULLABLE TYPE
                if (targetTypeEx.IsNullableType)
                {
                    var nullableStructTp =Tps.T_NullableGen
                        .MakeGenericType(targetTypeEx.WorkingType);
                    if (collectedTypes.NotContains(nullableStructTp))
                        collectedTypes.Add(nullableStructTp);
                }

                //Get TypeAccessor   with members Selection inside Get               
                var typeAccessor = TypeAccessor.Get(targetTypeEx.WorkingType
                                              , selector, propertiesBinding, fieldsBinding
                                              , onlyMembers, ignoreMembers);

                foreach (var memberKV in typeAccessor.Members)
                {
                    var memberType = memberKV.Value.MemberInf.GetMemberType();

                    AnalyzeCollectTypes(memberType.GetTypeInfoEx()
                        , ref collectedTypes, selector
                        , propertiesBinding, fieldsBinding
                        , onlyMembers, ignoreMembers);                    
                }
            }
        }



        /// <summary>
        ///  If this Type can be Simply Added as TypeSerializer-
        ///  <para/> - Whithout its member Types analysing .
        /// </summary>
        /// <param name="targetTypeEx"></param>
        /// <returns></returns>
        internal static bool IsSimplyAddable(TypeInfoEx targetTypeEx)
        {
            return (targetTypeEx.Is4DPrimitive
               || targetTypeEx.OriginalType == Tps.T_object);
              
        }

         


            /// <summary>
            /// Analyze-check targetType that all of Types of it's members Tree structure can be serializable by Binary serializers. 
            /// <para/> If such Type can't be serialized the output Type[] will be Empty.
            /// </summary>
            /// <param name="targetType"></param>
            /// <param name="selector"></param>
            /// <param name="propertiesBinding"></param>
            /// <param name="fieldsBinding"></param>
            /// <param name="onlyMembers"></param>
            /// <param name="ignoreMembers"></param>
            /// <returns></returns>
            public static List<Type> CanBeSerializedBinary(Type targetType
                                               , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                               , BindingFlags propertiesBinding = BindingFlags.Default
                                               , BindingFlags fieldsBinding = BindingFlags.Default
             
                                               , List<string> onlyMembers = null
                                               , List<string> ignoreMembers = null
            )
        {
             List<Type> collectedTypes = new List<Type>();



            return collectedTypes;
        }

           


        #region --------------------------- CAN HANDLE ------------------------------
    

        public static bool CanHandle_ComplexType(Type typeToCheck)
        {
            var typeInfo = TypeInfoEx.Get(typeToCheck);

            if (typeInfo.Is4DPrimitive) return false;
            if (typeToCheck.IsEnum) return false;

            //NOT FOR COLLECTIONS AND ARRAYS
            // other enumerable that Auto Adding COntract doesn't support also can't be handled               
            if (typeInfo.IsICollection) return false;
            
            if (typeInfo.IsNullableType)
                return true; //&& typeInfo.OriginalType.IsValueType
            
            // CAN't BE custom generic + not collection based contract 
            if (typeInfo.OriginalType.IsGenericType)
            { return false; }   
            
            if ((typeInfo.OriginalType.IsClass 
                || typeInfo.OriginalType.IsValueType))
            { return true; }  // abstract also can be added
                     

            return false;
        }

        
        public static bool CanHandle_AbstractObjectInterfaceType(Type typeToCheck)
        {
            if (typeToCheck.IsAbstract) return true;
            if (typeToCheck == Tps.T_object) return true;
            if (typeToCheck.IsInterface
                && !Tps.T_IList.IsAssignableFrom(typeToCheck)
                && !Tps.T_IDictionary.IsAssignableFrom(typeToCheck)
                )
            {
                return true;
            }

            return false;
            

        }



        #endregion --------------------------- CAN HANDLE ------------------------------










    }
}




#region ------------------------------GARBAGE ------------------------------

#region ----------------------------- MEMBERS SELECTION -------------------------------


//internal const BindingFlags DefaultMembersBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
//internal const BindingFlags DefaultMembersBindingNoBase = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;


///// <summary>
///// Get Binding by Type Members Selector
///// </summary>
///// <param name="membersSelector"></param>
///// <returns></returns>
//public static BindingFlags GetSelectorBinding(TypeMemberSelectorEn membersSelector)
//{

//    switch (membersSelector)
//    {
//        case TypeMemberSelectorEn.NotDefined:
//            {
//                return BindingFlags.Default;
//            }

//        case TypeMemberSelectorEn.Default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.DefaultNoBase:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public | BindingFlags.NonPublic |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.PublicOnly:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.OnlyMembers:
//            {
//                return (BindingFlags.Instance |
//                        BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.Custom:
//            {
//                return (BindingFlags.Default);
//            }
//        default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//    }

//}



///// <summary>
///// Select Members of [selectableType] by selector nad other binding/ignore/only members  parameters. 
///// </summary>
///// <param name="selectableType"></param>
///// <param name="selector"></param>
///// <param name="propertiesBinding"></param>
///// <param name="fieldsBinding"></param>
///// <param name="onlyMembers"></param>
///// <param name="ignoreMembers"></param>
///// <returns></returns>
//public static List<string> SelectMembers(Type selectableType
//                        , TypeMemberSelectorEn selector
//                        , BindingFlags propertiesBinding = BindingFlags.Default
//                        , BindingFlags fieldsBinding = BindingFlags.Default
//                        , List<string> onlyMembers = null
//                        , List<string> ignoreMembers = null)
//{

//    var resultMembers = new List<string>();
//    var typeinfo = TypeInfoEx.Get(selectableType);
//    var targetType = typeinfo.WorkingType;

//    var properties = new List<string>();
//    var fields = new List<string>();


//    if (typeinfo.IsICollection || targetType.IsInterface || targetType == typeof(object) || targetType.Is4DPrimitiveType()) // targetType.IsICollectionType()
//    {// not collecting members for this type kinds 

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.NotDefined)
//    {
//        throw new InvalidOperationException($"TypeAcessor's members selector can't be undefined in TypeserializerT[{targetType.FullName}] ");
//    }
//    else if (selector == TypeMemberSelectorEn.Default)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                      .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                      .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection 
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.DefaultNoBase)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBindingNoBase).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBindingNoBase).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.PublicOnly)
//    {
//        properties.AddRange(targetType.GetProperties(BindingFlags.Public).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(BindingFlags.Public).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.OnlyMembers)
//    {
//        //check all of elements exists
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        //only member  should be more than 0
//        if (onlyMembers == null || onlyMembers.Count == 0)
//        { throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers, then only members property can't be null or with [0] Count value  "); }

//        //check all of elements  exist in Properties or Fields
//        foreach (var item in onlyMembers)
//        {
//            if (properties.Contains(item) || fields.Contains(item))
//            {
//                throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers,  only member item [{item}]  was not found in Members of T[{targetType.Name}]");
//            }
//        }

//        resultMembers.AddRange(onlyMembers); // but will processing only members                
//    }
//    else if (selector == TypeMemberSelectorEn.Custom)
//    {
//        //only  custom binding should be as input parameter
//        //ignore items
//        properties.AddRange(targetType.GetProperties(fieldsBinding).Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(propertiesBinding).Select(fld => fld.Name));

//        foreach (var ignoreItem in ignoreMembers)
//        {
//            if (properties.Contains(ignoreItem)) { properties.Remove(ignoreItem); }
//            if (fields.Contains(ignoreItem)) { fields.Remove(ignoreItem); }
//        }

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);

//    }

//    //remove autofields <>k_BackingField
//    if (resultMembers.Count > 0)
//    {
//        resultMembers = resultMembers.Where(itm => !itm.Contains(">k__BackingField")).ToList();
//    }

//    //remove members which is read only // or instantiated once at creation

//    return resultMembers;
//}




///// <summary>
///// Select Members of TTarget Type by selector nad other binding/ignore/only members  parameters. 
///// </summary>
///// <typeparam name="TTarget"></typeparam>
///// <param name="selector"></param>
///// <param name="propertiesBinding"></param>
///// <param name="fieldsBinding"></param>
///// <param name="onlyMembers"></param>
///// <param name="ignoreMembers"></param>
///// <returns></returns>
//public static List<string> SelectMembers<TTarget>(TypeMemberSelectorEn selector
//                        , BindingFlags propertiesBinding = BindingFlags.Default
//                        , BindingFlags fieldsBinding = BindingFlags.Default
//                        , List<string> onlyMembers = null
//                        , List<string> ignoreMembers = null)
//{
//    return SelectMembers(typeof(TTarget), selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
//}

#endregion ----------------------------- MEMBERS SELECTION -------------------------------




#region --------------------------------- ADDING KNOWN TYPES OLD -----------------------------------




//static void AddKnownTypeInternal(Type targetType

//                            , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

//                            , BindingFlags propertiesBinding = BindingFlags.Default
//                            , BindingFlags fieldsBinding = BindingFlags.Default

//                            , List<string> onlyMembers = null
//                            , List<string> ignoreMembers = null

//                            )
//{



//    //if (GetIfContainsContract(targetType).IsNotNull()) return;// already exist

//    //ushort? typeID = 0;
//    //typeID = (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);

//    //if (targetType.IsAutoAdding())
//    //{
//    //    // check that contract is really Auto Adding Contract
//    //    // enums/IList/IDictionary/Array and interfaces

//    //    // cannot be other than real AutoAdding Category Contract
//    //    if (targetType.IsNullable())
//    //    {
//    //        var typeSerializer = CreateTypeSerializer(targetType);
//    //        AddItemToTypeSetDictionaries(typeSerializer);

//    //        //1 case concrete serialise /deserialize handlers - as primitives for example   - but initing- by handlers only after Added or Craeted of TypeSerializer 
//    //        //2 case auto adding custom nullable struct 

//    //    }
//    //    // if this Type is  /Interface/Enum /Array that still not exist then add it with args checking and ADDDING TOO if also NOT EXIST
//    //    else if (targetType.IsInterface)
//    //    {
//    //        DetectAddAutoContractArgTypesFromCollection(targetType);

//    //        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
//    //        AddItemToTypeSetDictionaries(typeSerializer);

//    //        //need no Policy and BuildMembers; but need  Generate  Handlers Expression


//    //    }
//    //    else if (targetType.IsEnum)
//    //    {

//    //        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
//    //        AddItemToTypeSetDictionaries(typeSerializer);

//    //        //need no Policy and BuildMembers; but need  Generate  Handlers Expression

//    //    }
//    //    else if (targetType.IsArray)
//    //    {
//    //        //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//    //        DetectAddAutoContractArgTypesFromCollection(targetType);

//    //        var typeSerializer = CreateTypeSerializer(targetType);
//    //        AddItemToTypeSetDictionaries(typeSerializer);

//    //        //need no Policy and BuildMembers; but need to Generate  Handlers Expression                           

//    //    }
//    //    else if (typeof(IList).IsAssignableFrom(targetType))
//    //    {
//    //        //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//    //        DetectAddAutoContractArgTypesFromCollection(targetType);

//    //        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
//    //        AddItemToTypeSetDictionaries(typeSerializer);

//    //        //need no Policy and BuildMembers; but need  Generate  Handlers Expression


//    //    }
//    //    else if (typeof(IDictionary).IsAssignableFrom(targetType))
//    //    {
//    //        //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//    //        DetectAddAutoContractArgTypesFromCollection(targetType);//detect Key or Value Contract Arg

//    //        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
//    //        AddItemToTypeSetDictionaries(typeSerializer);
//    //        //need no Policy and BuildMembers; but need  Generate  Handlers Expression
//    //    }
//    //}
//    //else if (targetType.Is4DPrimitiveType())
//    //{
//    //    // default contract adding: have  serializeHandler & deserializeHandler- need no to generate expressions                        
//    //    // can be Primitive contract                      

//    //    var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
//    //                                                          // PRIMITIVE DEFAULT HANDLERS
//    //                                                          //contractProcessor.SetDefaultSerializeHandler(serializeHandler);
//    //                                                          //contractProcessor.SetDefaultDeserializeHandler(deserializeHandler);

//    //    AddItemToTypeSetDictionaries(typeSerializer);

//    //    //need no Policy and BuildMembers; need no Generate  Handlers Expression

//    //    // break;
//    //}
//    //else
//    //{ //custom Contract  


//    //    //mismath between OnlyMembersPolicy and  empty OnlyMembers array  
//    //    Validator.AssertTrue<InvalidOperationException>(
//    //                      (selector == TypeMemberSelectorEn.OnlyMembers && onlyMembers != null && onlyMembers.Count == 0),
//    //                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_NEED_ITEM_ERR"
//    //                                );


//    //    //mismath between not empty OnlyMembers array and incorrect selector
//    //    Validator.AssertTrue<InvalidOperationException>(
//    //                      (onlyMembers != null && onlyMembers.Count > 0 && selector != TypeMemberSelectorEn.OnlyMembers),
//    //                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_ITEMS_INCORRECT_POLICY_ERR"
//    //                                );


//    //    //policy of member selector mismatch with TypeMemberSelectorEn 
//    //    Validator.AssertTrue<InvalidOperationException>(
//    //             (propertiesBinding != BindingFlags.Default || fieldsBinding != BindingFlags.Default)
//    //          && (selector != TypeMemberSelectorEn.OnlyMembers && selector != TypeMemberSelectorEn.Custom),
//    //                 "Contract Add error: Current Processing Policy doesn't support custom Bindings for Properties or Fields."
//    //               + "Only ONLY_MEMBER and CUSTOM Porocessing policies support this feature. Look  at the  contract{0}"
//    //                           , targetType);


//    //    // By default static members mustn't be included into serialization
//    //    Validator.AssertTrue<InvalidOperationException>(
//    //                       propertiesBinding.HasFlag(BindingFlags.Static) || fieldsBinding.HasFlag(BindingFlags.Static),
//    //                       "Member Analyze error: Your Custom Binding cannot contains Static flag. Look  at the  contract{0}."
//    //                       , targetType);


//    //    var typeSerializer = CreateTypeSerializer(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);//typeID.Value
//    //    AddItemToTypeSetDictionaries(typeSerializer);

//    //    //collect MEmberTypes, and Check -if we have AutoAdding types within them
//    //    foreach (var memberType in typeSerializer.Accessor.CollectMemberTypes())
//    //    {
//    //        DetectAddAutoContractFull(memberType);
//    //    }



//    //    // if  type  - CustomStruct- is ValueType, we'll add it's nullable brother type- CustomStruct?.                        
//    //    if (targetType.IsValueType && targetType.IsNotNullable() && !targetType.IsEnum)
//    //    {
//    //        var contractNullableTwinBrother = typeof(Nullable<>).MakeGenericType(targetType);

//    //        AddKnownTypeInternal(
//    //              contractNullableTwinBrother
//    //             , selector
//    //             , propertiesBinding
//    //             , fieldsBinding
//    //             , onlyMembers
//    //            );

//    //    }

//    //    //collect auto items - list   break;
//    //}
//}


///// <summary>
///// Add Contract to Type Set Serializer
///// </summary>
///// <param name="tp"></param>
///// <returns></returns>
//internal static ITypeSerializerJson AddKnownType(Type tp)
//{
//    return AddKnownTypein(tp, selector: TypeMemberSelectorEn.Default);
//}

///// <summary>
///// Add Contract to Type Set Serializer
///// </summary>
///// <typeparam name="T"></typeparam>
//internal static ITypeSerializerJson<T> AddKnownType<T>()
//{
//    return AddKnownType(typeof(T)) as ITypeSerializerJson<T>;
//}



/// <summary>
/// Add several Known Types
/// </summary>
/// <param name="knownTypes"></param>
//internal static void AddKnownTypesTPL(params Type[] knownTypes)
//{
//    for (int i = 0; i < knownTypes.Length; i++)
//    {
//        AddKnownType(knownTypes[i]);
//    }

//}



#endregion  --------------------------------- ADDING KNOWN TYPES OLD ----------------------------------- 


#region ---------------------------------------- DETECT ADD AUTOCONTARACTS OLD -------------------------------------------

/// <summary>
/// Auto add contract cases: Nullable struct | (Array) | (IList) | (IDictionary) | (Interface) | (Boxed Enum)
/// </summary>       
/// <param name="realTypeContract"></param>       
//static void DetectAddAutoContract(Type realTypeContract)
//{
//    locker.LockAction(GetIfContainsContract(realTypeContract).IsNotNull
//        , () =>
//        {
//            //collection types with additional item types  
//            if (realTypeContract.IsICollectionType() || realTypeContract.IsInterface)
//            {
//                //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//                //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//                //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//                //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//                DetectAddAutoContractArgTypesFromCollection(realTypeContract);

//                AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]

//            }
//            else if (realTypeContract.IsEnum || realTypeContract.IsNullable())
//            {
//                AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]
//            }
//        }
//        );


//}


//static void DetectAddAutoContractFull(Type realTypeContract)
//{
//    if (realTypeContract.Is4DPrimitiveType()) return;


//    locker.LockAction(() => GetIfContainsContract(realTypeContract).IsNotNull()
//      , () =>
//      {
//          //collection types with additional item types  
//          if (realTypeContract.IsICollectionType() || realTypeContract.IsInterface)
//          {
//              //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//              //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//              //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//              //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//              DetectAddAutoContractArgTypesFromCollection(realTypeContract);

//              AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]

//          }
//          else if (realTypeContract.IsEnum || realTypeContract.IsNullable())
//          {
//              AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]
//          }
//          else if (realTypeContract.IsClass || realTypeContract.IsValueType)
//          { // new as  Analyzed New Type
//              AddKnownTypeInternal(realTypeContract);
//          }
//      }
//      );
//}


/// <summary>
/// Detecting if we need to add as [AUTO ADDING Category Contract] one/two of arg-types from collection Contract.
/// </summary>
/// <param name="collectionContract"></param>
//static void DetectAddAutoContractArgTypesFromCollection(Type collectionContract)
//{
//    Type[] argTypes = Type.EmptyTypes;

//    if (collectionContract.IsArray)
//    {
//        argTypes = new[] { collectionContract.GetElementType() };
//    }
//    else if (collectionContract.IsGenericType)
//    {
//        argTypes = collectionContract.GetGenericArguments();
//    }

//    if (argTypes == null) return;//error?? 

//    foreach (var itemArgType in argTypes)
//    {
//        ushort? itemArgTypeId = GetIfContainsContract(itemArgType);
//        if (itemArgTypeId != null) continue;//already contains

//        if (itemArgType.IsInterface)
//        {

//            AddKnownTypeInternal(itemArgType, selector: TypeMemberSelectorEn.Default);

//        }
//        else if (itemArgType.IsEnum)
//        {
//            AddKnownTypeInternal(itemArgType, selector: TypeMemberSelectorEn.Default);

//        }
//        else
//        {
//            Validator.AssertTrue<InvalidOperationException>(itemArgTypeId == 0,
//                "DetectAddAutoContractArgTypesFromCollection(): Searching In Collection [{0}] for arg type [{1}] : Result - NOT DETERMINED. So TypeSerializer  for such Contract  unable to be load to use from Serializer Contract Dictionary."
//               , collectionContract, itemArgType);

//        }

//    }

//}


#endregion ---------------------------------------- DETECT ADD AUTOCONTARACTS -------------------------------------------



//if (typeInfo.CollectionKind != SerializableCollectionEn.NotCollection || typeInfo.OriginalType.IsInterface) // targetType.IsICollectionType() 
//{
//    //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//    //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//    //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//    //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//    DetectAddAutoContractArgTypesFromCollection(typeInfo.OriginalType);

//    AddKnownTypeInternal(typeInfo.OriginalType); //add as [Auto Adding Category Contract]

//}
//else if (typeInfo.OriginalType.IsEnum || typeInfo.IsNullabeValueType) // .IsNullable()
//{
//    AddKnownTypeInternal(typeInfo.OriginalType); //add as [Auto Adding Category Contract]
//}
//else if (typeInfo.OriginalType.IsClass || typeInfo.OriginalType.IsValueType)
//{ // new as  Analyzed New Type
//    AddKnownTypeInternal(typeInfo.OriginalType);
//}


#endregion ------------------------------GARBAGE ------------------------------