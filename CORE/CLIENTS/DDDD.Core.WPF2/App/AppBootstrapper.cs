﻿
using System;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;
using DDDD.Core.Net;
using DDDD.Core.Threading;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Diagnostics;
using DDDD.Core.Net.WebApi;
using DDDD.Core.Reflection;
using DDDD.Core.Data.DA2;






#if (CLIENT && WPF)

using DDDD.Core.UI;
using DDDD.Core.Environment;
using DDDD.Core.UI.Windows;
#endif

#if SERVER
using Cfg = System.Configuration;
using System.ServiceModel.Configuration;
using DDDD.Core.Environment;
using DDDD.Core.Serialization;
#endif


#if SERVER && IIS
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Configuration;
using System.Web.Optimization;
using DDDD.Core.Environment;
using NLog;
#endif


namespace DDDD.Core.App
{


    /// <summary>
    /// Default Application Bootstrapper. 
    /// <para/> Bootstrapper - loading Application with known steps - Load Assemblies to the ComponentsContainer, Registering Components Classes, Build ComponentsContainer, then Init registered( known components and custom components classes ).  
    /// </summary>
    public class AppBootstrapper
#if SERVER || WPF
        : MarshalByRefObject  //can reload AppDomain
#endif
    {

        #region ---------------------------- CTOR -----------------------------

        protected AppBootstrapper()
        {

        }

        #endregion ---------------------------- CTOR -----------------------------


        #region -------------------------------- SINGLETON -------------------------------------


        static LazyAct<AppBootstrapper> LA_CurrentBootstrapper =
               LazyAct<AppBootstrapper>.Create(
                   (args) => { return new AppBootstrapper(); }
                   , null//locker
                   , null //args
                   );

        /// <summary>
        /// Instance of AppBootstrapper  
        /// </summary>
        public static AppBootstrapper Current
        {
            get
            {
                return LA_CurrentBootstrapper.Value;
            }
        }

        /// <summary>
        /// If all components of current app already booted completely. 
        /// </summary>
        public static bool IsAppBooted
        {get; private set;}

        private static readonly object locker = new object();
#endregion -------------------------------- SINGLETON -------------------------------------


        const string Class = nameof(AppBootstrapper);

      

        /// <summary>
        /// Shell Control that can be used to set it to the RootVisual on client side Application
        /// </summary>
        public static UIElement ShellControl
        {
            get; private set;
        }


#region -------------------------- TYPECACHE INIT HELPER ---------------------------------

        

        public  void ConfigureTypeCache(List<string> customExcludingDomainAssemblies, List<Type> includingTypes)
        {
            for (int i = 0; i < customExcludingDomainAssemblies.Count; i++)
            {
                if (!TypeCache.ExcludingAssemblies.Contains(customExcludingDomainAssemblies[i]))
                {
                    TypeCache.ExcludeDomainAssembly(customExcludingDomainAssemblies[i], includingTypes.ToArray());
                }
                

            }
             
        }

#endregion -------------------------- TYPECACHE INIT HELPER ---------------------------------


#region ------------------------------ REGISTER CUSTOM COMPONENTS CLASSES -----------------------------


        /// <summary>
        ///  Registering custom ComponentsClasses. Each ComponentsClasses associated with one Contract-[Interface or BaseClass]. 
        /// </summary>
        /// <param name="registerAllComponentClassesFunc"></param>
        protected static void RegisterCustomComponentsClasses(Func<ComponentDefinition[]> registerAllComponentClassesFunc)
        {
            if (registerAllComponentClassesFunc.IsNull()) return;

            var componentsClasses = registerAllComponentClassesFunc();
            if (componentsClasses.IsWorkable())
            {
                for (int i = 0; i < componentsClasses.Length; i++)
                {
                    ComponentsContainer.RegisterComponentClass(componentsClasses[i]);
                }
            }

        }


        #endregion ------------------------------ REGISTER CUSTOM COMPONENTS CLASSES -----------------------------




#if SERVER && IIS


        /// <summary>
        /// <para/> Load Application workflow: 
        /// <para/>    -  [prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps).
        /// <para/>    1  [registerAllComponentsClassesFunc] - ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitServerFactories, DCManagers. 
        /// <para/>    2  [chooseLoadContainerAssembliesFunc] -  collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// <para/>    3  [registerWebApiAction] - Configure WebApi startup parameters.
        /// <para/>    4  [registerBundlesFunc] - Registering JS Bundles Collection.
        /// <para/>    -  [finishAppAction] - Finishing App Booting with your custom action. 
        /// </summary>
        /// <param name="prepareAppAction"> [prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps). </param>
        /// <param name="registerAllComponentsClassesFunc"> [registerAllComponentsClassesFunc]- ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitServerFactories, DCManagers. 
        /// </param> 
        /// <param name="chooseLoadContainerAssembliesFunc">[chooseLoadContainerAssembliesFunc] - user collecting custom assemblies that will be put into the ComponentsContainer</param>
        /// <param name="registerWebApiAction"> [registerWebApiAction] - Configure WebApi startup parameters. </param>
        /// <param name="registerBundlesFunc"> [registerBundlesFunc] - Registering JS Bundles Collection. </param>
        /// <param name="finishAppAction"> [finishAppAction] - Finishing App Booting with your custom action. </param>
        public void LoadAppComponents_WcfWebApi(
              string appComponentName
              ,Action prepareAppAction
            , Func<ComponentDefinition[]> registerAllComponentsClassesFunc
            , Func<Assembly[]> chooseLoadContainerAssembliesFunc
            , Action<HttpConfiguration> registerWebApiAction
            , Action<BundleCollection> registerBundlesFunc
            , Action finishAppAction 
            
            )
        {

            locker.LockAction(() => IsAppBooted,
                () =>
                {
                    Environment2.AppComponentName = appComponentName;
                    var logFilePath = Environment2.GetAppSubfolder(SubfolderEn.Logs) + Environment2.AppComponentName + ".log";
                    DCLogger.CreateFileLogger(DCLogger.DefaultLogKey, logFilePath, LogLevel.Debug, DCLogger.DefaultMessageRenderLayout);

                    //  SOME CUSTOM ACTIONS BEFORE LOAD APP COMPONENTS
                    //  FOR EXAMPLE LOAD LOCAL CONFIGURATION - FROM LOCAL STORAGE

                    DCLogger.GetLogger(DCLogger.DefaultLogKey)
                            .InfoDbg("Starting App [{0}] Bootstrapping. Mode WCF+WebAPI ".Fmt(Environment2.AppComponentName) ); 

                    if (prepareAppAction.IsNotNull()) prepareAppAction();
                    
                    // Register Component Class - DCCommunicationUnitFactories -one factory for one typology item of string
                    // Now we use WCF Factory -  DCServiceHostFactory that produces Wcf_EnvironmentActivationSvc DCServices, Web API Controllers
                    ComponentsContainer.RegisterComponentClass<IDCCUnitServerFactory>(
                     ComponentClassEn.DCCUnitServerFactories.S(), Init_DCCUnitFactories_ComponentsClass);

                    // Register Component Class - DC managers
                    ComponentsContainer.RegisterComponentClass<DCManager>(
                      ComponentClassEn.DCManagers.S(), Init_DCManagers_ComponentsClass);

                    // Register Component Class - WebApiControllers
                    ComponentsContainer.RegisterComponentClass<ApiController>(
                        ComponentClassEn.WebApiControllers.S(), Init_WebApiControllers_ComponentsClass);


                    // Registering custom- additional Component Classes
                    RegisterCustomComponentsClasses(registerAllComponentsClassesFunc);


                    //LOAD COMPONENTS FROM ASSEMBLIESn
                    LoadComponentsToContainer(chooseLoadContainerAssembliesFunc);

                    // Build-collect Component Types in internal IOC Container.
                    ComponentsContainer.InitializeComponentsClasses();


                    // BundleConfig.RegisterBundles(BundleTable.Bundles);
                    if (registerBundlesFunc.IsNotNull()) registerBundlesFunc(BundleTable.Bundles);

                    // 
                    if (finishAppAction.IsNotNull()) finishAppAction();


                    IsAppBooted = true;
                }
                ); 
        }




        #region -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------


        /// <summary>
        /// 1 if ComponentsContainer now on manual Mode - we need to add any of domain assemblies manually
        /// 2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </summary>
        /// <param name="chooseLoadContainerAssembliesFunc"></param>
        protected static void LoadComponentsToContainer(Func<Assembly[]> chooseLoadContainerAssembliesFunc)
        {

            //add internal factories types
            ComponentsContainer.Current.LoadComponents(new[] { typeof(DCServiceHostFactory), typeof(DCWebServiceHostFactory)
                                                              ,typeof(DCWebApiDirectHandlerServerFactory)}
                                                       );




            if (chooseLoadContainerAssembliesFunc.IsNull()) return;

            var assemblies = chooseLoadContainerAssembliesFunc();
            if (assemblies.IsWorkable())
            {
                for (int i = 0; i < assemblies.Length; i++)
                {
                    ComponentsContainer.Current.LoadComponents(assemblies[i]);
                }
            }

            //if this assembly Meets Contract of TypeCache.IsDomainAssembly
            //ComponentsContainer.Current.LoadComponents(typeof(AppBootstrapper).Assembly);
        }

        #endregion -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------




        #region ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------


        /// <summary>
        /// Init Components Class:  DCCommunicationUnitFactories
        /// </summary>
        /// <param name="dccFactoryComponents"></param>
        protected virtual void Init_DCCUnitFactories_ComponentsClass(List<ComponentMatch> dccFactoryComponents)
        {
          
            foreach (var dcCommunicationFactoryComponent in dccFactoryComponents)
            {
                DCCUnit.TryAddOrUseExistScenario(dcCommunicationFactoryComponent);
            }

            NetConfiguration.SaveConfigIfChanged();

        }

        #endregion ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------
        

        #region ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        /// <summary>
        /// Init Components Class:  DCManagers
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_DCManagers_ComponentsClass(List<ComponentMatch> dcManagerComponents)
        {
            bool configWasChanged = false;

            // 2 Register/config - but not Create instancies of DCCommunicationUnits Dynamically for some owners-DCManagers
            for (int i = 0; i < dcManagerComponents.Count; i++)
            {
                var dcmanagerCommunicationUnitRegInfos = dcManagerComponents[i].ComponentType.GetTypeAttributes<DCCUnitOrderAttribute>();
                for (int j = 0; j < dcmanagerCommunicationUnitRegInfos.Count; j++)
                {
                    //check Factory exist for such DCCUnit exist
                    if (ComponentsContainer.GetComponent<DCCUnitServerFactoryAttribute>(ComponentClassEn.DCCUnitServerFactories
                                                                                       , (mtInf) => (mtInf).ScenarioKey == dcmanagerCommunicationUnitRegInfos[j].ScenarioKey) == null)
                    {
                        continue; // because we have no factory that can produce such unit -with such [technology-service] -CommunicationUnitKey
                    }

                    //register dccUnit if factory existed in NetConfigurionManager,  and change web.config; if web.config was changed -then method RegisterUnitForOwner() return true
                    if (DCCUnit.AddOrUseExistDCCUnit(dcManagerComponents[i].ComponentType, dcmanagerCommunicationUnitRegInfos[j]) != null)
                    {
                        configWasChanged = true;
                    }
                }

            }
            

            // if config really was changed - then we return true
            NetConfiguration.SaveConfigIfChanged();
         
        }


        #endregion ---------------------------------- INIT DCMANAGERS -------------------------------------------------


        #region ------------------------------- INIT WebAPI CONTROLLERS ---------------------------------------
        protected virtual void Init_WebApiControllers_ComponentsClass(List<ComponentMatch> dcPagesComponents)
        {

        }

        #endregion ------------------------------- INIT WebAPI CONTROLLERS ---------------------------------------
#elif CLIENT && WP81

        /// <summary>
        /// <para/> Load Application workflow: 
        /// <para/>   -   [prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps).
        /// <para/>   1  [registerAllComponentsClassesFunc] - ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitServerFactories,DCManagers, UserControls,Pages.
        /// <para/>   2  [chooseLoadContainerAssembliesFunc] -  collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// <para/>   -  [finishAppAction] - Finishing App Booting with your custom action. 
        /// </summary>
        /// <param name="prepareAppAction">[prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps).</param>
        /// <param name="registerAllComponentsClassesFunc"> [registerAllComponentsClassesFunc]- ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitClientFactories,DCManagers, UserControls,Pages. 
        /// </param> 
        /// <param name="chooseLoadContainerAssembliesFunc">[chooseLoadContainerAssembliesFunc] - user collecting custom assemblies that will be put into the ComponentsContainer
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </param>
        /// <param name="finishAppLoadAction">[finishAppLoadAction] - Finishing App Booting with your custom action.</param>
        public void LoadAppComponents(
              Action prepareAppAction
            , Func<ComponentDefinition[]> registerAllComponentsClassesFunc
            , Func<Assembly[]> chooseLoadContainerAssembliesFunc
            , Action finishAppLoadAction
            )
        {

            //  SOME CUSTOM ACTIONS BEFORE LOAD APP COMPONENTS
            //  FOR EXAMPLE LOAD LOCAL CONFIGURATION - FROM LOCAL STORAGE
            if (prepareAppAction.IsNotNull()) prepareAppAction();
            

            //Register Component Class - DCCommunicationUnitFactories -one factory for one typology item of string
            //Now we use Factory -  DCServiceClientFactory that produces Clients for Wcf_EnvironmentActivationSvc scenario
            ComponentsContainer.RegisterComponentClass<IDCCUnitClientFactory>(
               ComponentClassEn.DCCUnitClientFactories.S(), Init_DCCUnitFactories_ComponentsClass);

            //Register Component Class - DC managers
            ComponentsContainer.RegisterComponentClass<DCManager>(
              ComponentClassEn.DCManagers.S(), Init_DCManagers_ComponentsClass);

            //Register Component Class - UserControl
            ComponentsContainer.RegisterComponentClass<UserControl>(
              ComponentClassEn.UserControls.S(), Init_UserControls_ComponentsClass);

            //Register Component Class - Pages
            ComponentsContainer.RegisterComponentClass<Page>(
              ComponentClassEn.Pages.S(), Init_Pages_ComponentsClass);


            // Registering custom- additional Component Classes
            RegisterCustomComponentsClasses(registerAllComponentsClassesFunc);

            //LOAD COMPONENTS FROM ASSEMBLIES
            LoadComponentsToContainer(chooseLoadContainerAssembliesFunc);

            // Build-collect Component Types in internal IOC Container.
            ComponentsContainer.InitializeComponentsClasses();


            InitShellPage();

            if (finishAppLoadAction.IsNotNull()) finishAppLoadAction();

        }


        /// <summary>
        /// Initialize Shell Page/Window/UserControl to App.Startup UI Presentation View.
        /// </summary>
        protected virtual void InitShellPage()
        {
            // firstly lets try to find Shell  UIElement as Page based control
            var shellPageComponent = ComponentsContainer.GetComponent<ShellPageAttribute>(ComponentClassEn.Pages, (shpg) => { return shpg.IsNotNull(); });

            if (shellPageComponent.IsNotNull())
            {
                ShellControl = shellPageComponent.ResolveInstanceTBase<Page>();
            }
            else if (ShellControl.IsNull())
            {
                var userControlComponent = ComponentsContainer.GetComponent<ShellUserControlAttribute>(ComponentClassEn.UserControls, (shuc) => { return shuc.IsNotNull(); });

                if (userControlComponent.IsNotNull())
                {
                    // second if ShellControl is not Page based control, then lets try to find Shell  UIElement as UserControl based control
                    ShellControl = userControlComponent.ResolveInstanceTBase<UserControl>();
                }
            }


            Validator.ATNullReferenceCustomMsg(ShellControl, Class, nameof(InitShellPage), nameof(ShellControl),
                "There is no ShellPage in current Application Domain-create it please.");


            Application.Current.RootVisual = ShellControl;
        }


        
        #region -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------


        /// <summary>
        /// 1 if ComponentsContainer now on manual Mode - we need to add any of domain assemblies manually
        /// 2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </summary>
        /// <param name="chooseLoadContainerAssembliesFunc"></param>
        protected static void LoadComponentsToContainer(Func<Assembly[]> chooseLoadContainerAssembliesFunc)
        {

            //add internal factories types
            ComponentsContainer.Current.LoadComponents(new [] { typeof(DCServiceClientFactory), typeof(DCWebApiDirectHandlerClientFactory) } );




            if (chooseLoadContainerAssembliesFunc.IsNull()) return;
            
            var assemblies = chooseLoadContainerAssembliesFunc();
            if (assemblies.IsWorkable())
            {
                for (int i = 0; i < assemblies.Length; i++)
                {
                    ComponentsContainer.Current.LoadComponents(assemblies[i]);
                }
            }

            //if this assembly Meets Contract of TypeCache.IsDomainAssembly
            //ComponentsContainer.Current.LoadComponents(typeof(AppBootstrapper).Assembly);
        }

        #endregion -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------




        #region ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------

        /// <summary>
        /// Init Components Class:  DCCUnitFactories
        /// </summary>
        /// <param name="dcCUnitClientFactories"></param>
        protected virtual void Init_DCCUnitFactories_ComponentsClass(List<ComponentMatch> dcCommunicationUnitClientFactories)
        {
            //NO INITING ACTION FOR CLIENT FACTORIES ON CLIENT

        }

        #endregion ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------

        #region ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        /// <summary>
        ///   Init Components Class:  DCManagers
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_DCManagers_ComponentsClass(List<ComponentMatch> dcManagerComponents)
        {

            // 2 Register/config - but not Create instancies of DCCommunicationUnits Dynamically for some owners-DCManagers
            for (int i = 0; i < dcManagerComponents.Count; i++)
            {
                var dcmanagerCommunicationUnitOrders = dcManagerComponents[i].ComponentType.GetTypeAttributes<DCCUnitOrderAttribute>();

                // Here we need to understand - if Factory that creates Clients with such Scenario exists
                for (int j = 0; j < dcmanagerCommunicationUnitOrders.Count; j++)
                {
                    //check Client for  Factory exist for such DCCUnit exist
                    if (ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(ComponentClassEn.DCCUnitClientFactories
                                                                                       , (mtInf) => (mtInf).ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey)).IsNull())
                    {
                        continue; // because we have no factory that can produce such unit -with such [technology-service] -CommunicationUnitKey
                    }

                    //if communication Item was not created - then create it here /or simply get reference
                    var dcUnitItem = DCCUnit.TryGetOrAddNewDCCUnit(dcManagerComponents[i].ComponentType, dcmanagerCommunicationUnitOrders[j]);

                    //get target factory and add client into it by dcUnitItem
                    var clientFactoryComponent = ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(
                        ComponentClassEn.DCCUnitClientFactories,
                        (mtinf) => mtinf.ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey));
                    var clientFactory = (IDCCUnitClientFactory)clientFactoryComponent.ResolveInstanceBoxed();

                    clientFactory.AddOrUseExistClient(dcUnitItem);

                }

            }

        }

        #endregion ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        #region ------------------------------------ USER CONTROLS -----------------------------------

        /// <summary>
        /// Init Components Class: UserControl
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_UserControls_ComponentsClass(List<ComponentMatch> dcUserControlsComponents)
        {

        }

        #endregion ------------------------------------ USER CONTROLS -----------------------------------

        #region ------------------------------------ PAGES -----------------------------------

        /// <summary>
        /// Init Components Class:  Pages
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_Pages_ComponentsClass(List<ComponentMatch> dcPagesComponents)
        {

        }

        #endregion ------------------------------------ PAGES -----------------------------------



#elif CLIENT && SL5


        /// <summary>
        /// <para/> Load Application workflow: 
        /// <para/>    -  [prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps).
        /// <para/>    1  [registerAllComponentsClassesFunc] - ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitClientFactories, DCManagers,UserControls,Pages, Windowses. 
        /// <para/>    2  [chooseLoadContainerAssembliesFunc] -  collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// <para/>    -  [finishAppAction] - Finishing App Booting with your custom action. 
        /// </summary>
        /// <param name="prepareAppAction">[prepareAction] - Before booting application we recommend you to customize TypeCache settings to load all your Domain assemblies and Types, also you can do somethig else you need - it's time before any configuration will be registered(in the following steps).</param>
        /// <param name="registerAllComponentsClassesFunc">[registerAllComponentsClassesFunc]  - ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitClientFactories, DCManagers,UserControls,Pages, Windowses.
        /// <param name="chooseLoadContainerAssembliesFunc">[chooseLoadContainerAssembliesFunc] - collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </param>
        /// <param name="finishAppLoadAction">[finishAppLoadAction] - Finishing App Booting with your custom action.</param>
        /// </param>
        public void LoadAppComponents(
              Action prepareAppAction
            , Func<ComponentDefinition[]> registerAllComponentsClassesFunc
            , Func<Assembly[]> chooseLoadContainerAssembliesFunc
            , Action finishAppLoadAction
             )
        {

            //  SOME CUSTOM ACTIONS BEFORE LOAD APP COMPONENTS
            //  FOR EXAMPLE LOAD LOCAL CONFIGURATION - FROM LOCAL STORAGE
            if (prepareAppAction.IsNotNull()) prepareAppAction();
        
          

            //Register Component Class - DCCommunicationUnitFactories -one factory for one typology item of string
            //Now we use Factory -  DCServiceClientFactory that produces Clients for Wcf_EnvironmentActivationSvc scenario
            ComponentsContainer.RegisterComponentClass<IDCCUnitClientFactory>(
               ComponentClassEn.DCCUnitClientFactories.S(), Init_DCCUnitFactories_ComponentsClass);
            
            //Register Component Class - DC managers
            ComponentsContainer.RegisterComponentClass<DCManager>(
              ComponentClassEn.DCManagers.S(), Init_DCManagers_ComponentsClass);

            //Register Component Class - UserControl
            ComponentsContainer.RegisterComponentClass<UserControl>(
              ComponentClassEn.UserControls.S(), Init_UserControls_ComponentsClass);

            //Register Component Class - Pages
            ComponentsContainer.RegisterComponentClass<Page>(
              ComponentClassEn.Pages.S(), Init_Pages_ComponentsClass);


            //Register Component Class - Windows
            ComponentsContainer.RegisterComponentClass<Window>(
               ComponentClassEn.Windows.S(), Init_Windows_ComponentsClass);


            // Registering custom- additional Component Classes
            RegisterCustomComponentsClasses(registerAllComponentsClassesFunc);

            //LOAD COMPONENTS FROM ASSEMBLIES
            LoadComponentsToContainer(chooseLoadContainerAssembliesFunc);

            // Build-collect Component Types in internal IOC Container.
            ComponentsContainer.InitializeComponentsClasses();
            
            InitShellPage();

            if (finishAppLoadAction.IsNotNull()) finishAppLoadAction();

        }


        /// <summary>
        /// Initialize Shell Page/Window/UserControl to App.Startup UI Presentation View.
        /// </summary>
        protected virtual void InitShellPage()
        {
            // firstly lets try to find Shell  UIElement as Page based control
            var shellPageComponent = ComponentsContainer.GetComponent<ShellPageAttribute>(ComponentClassEn.Pages, (shpg) => { return shpg.IsNotNull(); });
            if (shellPageComponent.IsNotNull() )
            {
                ShellControl = shellPageComponent.ResolveInstanceTBase<Page>();
            }
            else if (ShellControl.IsNull())
            {
                var userControlComponent = ComponentsContainer.GetComponent<ShellUserControlAttribute>(ComponentClassEn.UserControls, (shuc) => { return shuc.IsNotNull(); });
                if (userControlComponent.IsNotNull())
                {
                    // second if ShellControl is not Page based control, then lets try to find Shell  UIElement as UserControl based control
                    ShellControl = userControlComponent.ResolveInstanceTBase<UserControl>();
                }                                         
            }


            Validator.ATNullReferenceCustomMsg(ShellControl, Class, nameof(InitShellPage), nameof(ShellControl), 
                "There is no ShellPage in current Application Domain-create it please.");

            Application.Current.RootVisual = ShellControl;
        }


        #region -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------


        /// <summary>
        /// 1 if ComponentsContainer now on manual Mode - we need to add any of domain assemblies manually
        /// 2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </summary>
        /// <param name="chooseLoadContainerAssembliesFunc"></param>
        protected static void LoadComponentsToContainer(Func<Assembly[]> chooseLoadContainerAssembliesFunc)
        {

            //add internal factories types
            ComponentsContainer.Current.LoadComponents(new[] { typeof(DCServiceClientFactory), typeof(DCWebApiDirectHandlerClientFactory) });




            if (chooseLoadContainerAssembliesFunc.IsNull()) return;

            var assemblies = chooseLoadContainerAssembliesFunc();
            if (assemblies.IsWorkable())
            {
                for (int i = 0; i < assemblies.Length; i++)
                {
                    ComponentsContainer.Current.LoadComponents(assemblies[i]);
                }
            }

            //if this assembly Meets Contract of TypeCache.IsDomainAssembly
            //ComponentsContainer.Current.LoadComponents(typeof(AppBootstrapper).Assembly);
        }

        #endregion -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------




        #region ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------

        /// <summary>
        /// Init Components Class:  DCCUnitFactories
        /// </summary>
        /// <param name="dcCUnitClientFactories"></param>
        protected virtual void Init_DCCUnitFactories_ComponentsClass(List<ComponentMatch> dcCommunicationUnitClientFactories)
        {
            //NO INITING ACTION FOR CLIENT FACTORIES ON CLIENT
            
        }

        #endregion ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------
        
        #region ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        /// <summary>
        ///   Init Components Class:  DCManagers
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_DCManagers_ComponentsClass(List<ComponentMatch> dcManagerComponents)
        {

            // 2 Register/config - but not Create instancies of DCCommunicationUnits Dynamically for some owners-DCManagers
            for (int i = 0; i < dcManagerComponents.Count; i++)
            {
                var dcmanagerCommunicationUnitOrders = dcManagerComponents[i].ComponentType.GetTypeAttributes<DCCUnitOrderAttribute>();

                // Here we need to understand - if Factory that creates Clients with such Scenario exists
                for (int j = 0; j < dcmanagerCommunicationUnitOrders.Count; j++)
                {
                    //check Client for  Factory exist for such DCCUnit exist
                    if (ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(ComponentClassEn.DCCUnitClientFactories
                                                                                       , (mtInf) => (mtInf).ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey)).IsNull())
                    {
                        continue; // because we have no factory that can produce such unit -with such [technology-service] -CommunicationUnitKey
                    }

                    //if communication Item was not created - then create it here /or simply get reference
                    var dcUnitItem = DCCUnit.TryGetOrAddNewDCCUnit(dcManagerComponents[i].ComponentType, dcmanagerCommunicationUnitOrders[j]);
                    
                    //get target factory and add client into it by dcUnitItem
                    var clientFactoryComponent = ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(
                        ComponentClassEn.DCCUnitClientFactories,
                        (mtinf) => mtinf.ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey) );
                    var clientFactory = clientFactoryComponent.ResolveInstanceTBase<IDCCUnitClientFactory>();

                    clientFactory.AddOrUseExistClient(dcUnitItem);
                    
                }

            } 
           
        }

        #endregion ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        #region ------------------------------------ USER CONTROLS -----------------------------------

        /// <summary>
        /// Init Components Class: UserControl
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_UserControls_ComponentsClass(List<ComponentMatch> dcUserControlsComponents)
        {

        }

        #endregion ------------------------------------ USER CONTROLS -----------------------------------
                
        #region ------------------------------------ PAGES -----------------------------------

        /// <summary>
        /// Init Components Class:  Pages
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_Pages_ComponentsClass(List<ComponentMatch> dcPagesComponents)
        {

        }

        #endregion ------------------------------------ PAGES -----------------------------------

        #region ------------------------------------ WINDOWS -----------------------------------

        /// <summary>
        ///   Init Components Class:  Windows
        /// </summary>
        /// <param name="windowsComponents"></param>
        protected virtual void Init_Windows_ComponentsClass(List<ComponentMatch> windowsComponents)
        {

        }

        #endregion ------------------------------------ WINDOWS -----------------------------------


#elif CLIENT && WPF


        /// <summary>
        /// <para/> Load Application workflow: 
        /// <para/>      [prepareAction] - Before booting application we recommend you to customize TypeCache settings 
        /// to load all your Domain assemblies and Types
        /// , also you can do somethig else you need - 
        /// it's time before any configuration will be registered(in the following steps):
        /// <para/>    1  [registerAllComponentsClassesFunc] - ComponentsContainer registering components, and build it -to find all existed components in Application.              
        ///                By default we registering also DCCUnitClientFactories, DCServices,UserControls,Pages, Windowses. 
        /// <para/>    2  [chooseLoadContainerAssembliesFunc] - collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin-assemblies from your app folders at this point.
        /// <para/>      [finishAppAction] - Finishing App Booting with your custom action. 
        /// </summary>
        /// <param name="prepareAppAction">- Before booting application we recommend you to customize TypeCache settings 
        /// to load all your Domain assemblies and Types
        /// , also you can do somethig else you need - 
        /// it's time before any configuration will be registered(in the following steps):</param>
        /// <param name="useKnownComponents">  choose  what known Component Classes we use- register and collect, in  ComponentsContainer:   
        /// <para/>    ServiceModels|DCManagers|DCServices|WebApiControllers|UserControls|Pages|Windows
        ///             DCCUnitClientFactories
        /// </param>
        /// <param name="registerCustomComponentClassesFunc"> -  registering custom  ComponentDefinitions 
        ///             in ComponentsContainer  
        /// </param>
        /// <param name="loadAdditionalAssembliesFunc">  - collected by user custom assemblies that will be put into the ComponentsContainer by the following step. 
        ///                 2.1 if ComponentsContainer now on manual Mode - we NEED to add any of domain assemblies manually
        ///                 2.2 also you can load some other plugin/modules-assemblies from your app folders at this point.
        /// </param>
        /// <param name="initModel">  Initing App Model: UI/background ... </param>
        /// <param name="startupAppAction"> startupApp Action on end of App Booting with your custom action.</param>
        public void LoadAppComponents(
              Action prepareAppAction
            , ComponentClassEn useKnownComponents 
            , Func<ComponentDefinition[]> registerCustomComponentClassesFunc
            , Func<Assembly[]> loadAdditionalAssembliesFunc
            , InitAppModelEn initModel              
            ,  Action startupAppAction 
            )
        {

            //  SOME CUSTOM ACTIONS BEFORE LOAD APP COMPONENTS
            //  FOR EXAMPLE LOAD LOCAL CONFIGURATION - FROM LOCAL STORAGE
            if (prepareAppAction.IsNotNull()) prepareAppAction();

            RegisterKnownComponentClasses(useKnownComponents);
             

            // Registering custom- additional Component Classes
            RegisterCustomComponentsClasses(registerCustomComponentClassesFunc);

            //LOAD COMPONENTS FROM ASSEMBLIES also from Additional Assemblies
            LoadComponentsToContainer(loadAdditionalAssembliesFunc);

            // Build-collect Component Types in internal IOC Container.
            ComponentsContainer.InitializeComponentsClasses();

            ///LOAD: SHELL PAGE APP OR BACKGROUND APP 
            LoadInitAppModel(initModel,startupAppAction);
            

        }

        private void LoadInitAppModel(InitAppModelEn initModel, Action startupAppAction)
        {
            if (initModel == InitAppModelEn.InitShell_PagelApp)
            {
                InitShellApp(startupAppAction);
            }
            else if (initModel == InitAppModelEn.InitShell_WindowApp)
            {
                InitShellApp(startupAppAction);
            }
            else if (initModel == InitAppModelEn.InitBackgroun_WindowApp)
            {
                InitBackgroundApp(startupAppAction);
            }
            else if (initModel == InitAppModelEn.InitBackground_EmptyApp)
            {
                InitBackgroundEmpty(startupAppAction);
            }
        }

        private void RegisterKnownComponentClasses(ComponentClassEn useKnownComponents)
        {
            //Register Component Class - DCCommunicationUnitFactories -one factory for one typology item of string
            //Now we use Factory -  DCServiceClientFactory that produces Clients for Wcf_EnvironmentActivationSvc scenario
            if (useKnownComponents.HasFlag(ComponentClassEn.DCCUnitClientFactories))
            {
                ComponentsContainer.RegisterComponentClass<IDCCUnitClientFactory>(
                ComponentClassEn.DCCUnitClientFactories.S(), Init_DCCommunicationUnitFactories_ComponentsClass);
            }

            //Register Component Class - DC managers
            if (useKnownComponents.HasFlag(ComponentClassEn.DCManagers))
            {
                ComponentsContainer.RegisterComponentClass<DCManager>(
                ComponentClassEn.DCManagers.S(), Init_DCManagers_ComponentsClass);
            }

            //Register Component Class - DC services
            if (useKnownComponents.HasFlag(ComponentClassEn.DCServices))
            {
                ComponentsContainer.RegisterComponentClass<IDCService>(
                ComponentClassEn.DCServices.S(), Init_DCServices_ComponentsClass);
            }

            ////Register Component Class -  webApiCpmtrollers
            //if (registerComponentsInContainer.HasFlag(ComponentClassEn.WebApiControllers))
            //{
            //    ComponentsContainer.RegisterComponentClass<DCWebApiController>(
            //  ComponentClassEn.WebApiControllers.S(), Init_WebApiControllers_ComponentsClass);
            //}

            // Register Component Class - ServiceModels - will be loaded from DAL assembly
            // DAL assembly - sshould be one separate assembly.
            if (useKnownComponents.HasFlag(ComponentClassEn.ServiceModels))
            {
                ComponentsContainer.RegisterComponentClass<IServiceModel>(
                ComponentClassEn.ServiceModels.S(), Init_ServiceModels_ComponentsClass);
            }

            // Register Component Class - UserControl-only for custom UserControls
            if (useKnownComponents.HasFlag(ComponentClassEn.UserControls))
            {
                ComponentsContainer.RegisterComponentClass<UserControl>(
                ComponentClassEn.UserControls.S(), Init_UserControls_ComponentsClass);
            }

            //Register Component Class - Pages
            if (useKnownComponents.HasFlag(ComponentClassEn.Pages))
            {
                ComponentsContainer.RegisterComponentClass<Page>(
                ComponentClassEn.Pages.S(), Init_Pages_ComponentsClass);
            }

            //Register Component Class - Windows
            if (useKnownComponents.HasFlag(ComponentClassEn.Windows))
            {
                ComponentsContainer.RegisterComponentClass<Window>(
                ComponentClassEn.Windows.S(), Init_Windows_ComponentsClass);
            }
        }


        /// <summary>
        /// InitBackgroundApp - Initialize background application process - there is no UI windows/controls. 
        /// </summary>
        protected virtual void InitBackgroundApp(Action onStartupAction)
        {
            var backgroundApp = new Application();
            backgroundApp.Startup += (s, e) =>
            {
                if (onStartupAction.IsNotNull()) 
                    onStartupAction();
            };
            backgroundApp.Run(new BackgroundWindow());
        }
        private void InitBackgroundEmpty(Action onStartupAction)
        {
            if (onStartupAction.IsNotNull())
                onStartupAction();
        }



        /// <summary>
        /// Initialize Shell on Window/Page/UserControl 
        /// to App.Startup UI Presentation View.
        /// </summary>
        protected virtual void InitShellApp(Action onStartupAction )
        {
            var shellWindowComponent = ComponentsContainer.GetComponent<ShellWindowAttribute>(ComponentClassEn.Windows
                                          , (shw) => { return shw.IsNotNull(); });
            if (shellWindowComponent.IsNotNull())
            {
                ShellControl = shellWindowComponent.ResolveInstanceTBase<Window>();
            }
            else 
            {
                // firstly lets try to find Shell UIElement as Page based control
                var shellPageComponent = ComponentsContainer.GetComponent<ShellPageAttribute>(ComponentClassEn.Pages
                                         , (shpg) => { return shpg.IsNotNull(); });
                if (shellPageComponent.IsNotNull())
                {
                    ShellControl = shellPageComponent.ResolveInstanceTBase<Page>();
                }
                else 
                {
                    var userControlComponent = ComponentsContainer.GetComponent<ShellUserControlAttribute>(ComponentClassEn.UserControls
                                        , (shuc) => { return shuc.IsNotNull(); });
                    if (userControlComponent.IsNotNull())
                    {
                        // second if ShellControl is not Page based control, then lets try to find Shell  UIElement as UserControl based control
                        ShellControl = userControlComponent.ResolveInstanceTBase<UserControl>();
                    }
                }

            }                     

            Validator.ATNullReferenceCustomMsg(ShellControl,Class, nameof(InitShellApp), nameof(ShellControl), "There is no ShellPage in current Application Domain-create it please.");
  
            Application.Current.MainWindow = ShellControl as Window;
           
            if (onStartupAction.IsNotNull())
                onStartupAction();
        }

      


        #region -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------


        /// <summary>
        /// 1 if ComponentsContainer now on manual Mode - we need to add any of domain assemblies manually
        /// 2 also you can load some other plugin-assemblies from your app folders at this point.
        /// </summary>
        /// <param name="chooseLoadContainerAssembliesFunc"></param>
        protected static void LoadComponentsToContainer(Func<Assembly[]> chooseLoadContainerAssembliesFunc)
        {

            //add internal factories types
            ComponentsContainer.Current.LoadComponents(new[] { 
                                      typeof(DCServiceClientFactory)
                                    , typeof(DCWebApiDirectHandlerClientFactory) } );
            

            if (chooseLoadContainerAssembliesFunc.IsNull()) return;

            var assemblies = chooseLoadContainerAssembliesFunc();
            if (assemblies.IsWorkable())
            {
                for (int i = 0; i < assemblies.Length; i++)
                {
                    ComponentsContainer.Current.LoadComponents(assemblies[i]);
                }
            }

            //if this assembly Meets Contract of TypeCache.IsDomainAssembly
            //ComponentsContainer.Current.LoadComponents(typeof(AppBootstrapper).Assembly);
        }

        #endregion -------------------------------- INIT COMPONENTS CONTAINER ASSEMBLIES -------------------------------

        private void Init_ServiceModels_ComponentsClass(List<ComponentMatch> svcModelCompomemts)
        {
            // 2 Register/config - but not Create instancies of DCCommunicationUnits Dynamically for some owners-DCManagers
            for (int i = 0; i < svcModelCompomemts.Count; i++)
            {
                var svcModelItem = TypeActivator.CreateInstanceTBase<ServiceModel>(svcModelCompomemts[i].ComponentType);
                ServiceModel.AddSvcModelOnAppBoot(svcModelItem);
            }
        }


        private void Init_DCServices_ComponentsClass(List<ComponentMatch> obj)
        {
            //throw new NotImplementedException();
        }
        private void Init_WebApiControllers_ComponentsClass(List<ComponentMatch> obj)
        {
            //throw new NotImplementedException();
        }




        #region ---------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------


        /// <summary>
        /// Init Components Class:  DCCommunicationUnitFactories
        /// </summary>
        /// <param name="dcCommunicationUnitClientFactories"></param>
        protected virtual void Init_DCCommunicationUnitFactories_ComponentsClass(List<ComponentMatch> dcCommunicationUnitClientFactories)
        {
            //NO NEED IN INITING ACTION FOR CLIENT FACTORIES ON CLIENT - DCServiceClientFactory need no initing.

        }

#endregion ------------------------------- INIT DCCOMMUNICATION_UNIT_FACTORIES ----------------------------------------



#region ---------------------------------- INIT DCMANAGERS -------------------------------------------------

        /// <summary>
        /// Init Components Class:  DCManagers.
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected static void Init_DCManagers_ComponentsClass(List<ComponentMatch> dcManagerComponents)
        {
            // 2 Register/config - but not Create instancies of DCCommunicationUnits Dynamically for some owners-DCManagers
            for (int i = 0; i < dcManagerComponents.Count; i++)
            {
                var dcmanagerCommunicationUnitOrders = dcManagerComponents[i].ComponentType.GetTypeAttributes<DCCUnitOrderAttribute>();

                // Here we need to understand - if Factory that creates Clients with such Scenario exists
                for (int j = 0; j < dcmanagerCommunicationUnitOrders.Count; j++)
                {
                    //check Client for  Factory exist for such DCCUnit exist
                    if (ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(ComponentClassEn.DCCUnitClientFactories
                        , (mtInf) => (mtInf).ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey)).IsNull())
                    {
                        continue; // because we have no factory that can produce such unit -with such [technology-service] -CommunicationUnitKey
                    }

                    //if communication Item was not created - then create it here /or simply get reference
                    var dcUnitItem = DCCUnit.TryGetOrAddNewDCCUnit(dcManagerComponents[i].ComponentType, dcmanagerCommunicationUnitOrders[j]);

                    //get target factory and add client into it by dcUnitItem
                    var clientFactoryComponent = ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(
                        ComponentClassEn.DCCUnitClientFactories,
                        (mtinf) => mtinf.ScenarioKeys.Contains(dcmanagerCommunicationUnitOrders[j].ScenarioKey));
                    var clientFactory = (IDCCUnitClientFactory)clientFactoryComponent.ResolveInstanceBoxed();

                    clientFactory.AddOrUseExistClient(dcUnitItem);

                }

            }

        }

#endregion --------------------------------------- INIT DCMANAGERS -------------------------------------------------



#region ------------------------------------ USER CONTROLS -----------------------------------

        /// <summary>
        /// Init Components Class: UserControl
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_UserControls_ComponentsClass(List<ComponentMatch> dcUserControlsComponents)
        {

        }

#endregion ------------------------------------ USER CONTROLS -----------------------------------



#region ------------------------------------ PAGES -----------------------------------

        /// <summary>
        /// Init Components Class:  Pages
        /// </summary>
        /// <param name="dcManagerComponents"></param>
        protected virtual void Init_Pages_ComponentsClass(List<ComponentMatch> dcManagerComponents)
        {

        }

#endregion ------------------------------------ PAGES -----------------------------------
        


#region ------------------------------------ WINDOWS -----------------------------------

        /// <summary>
        ///   Init Components Class:  Windows
        /// </summary>
        /// <param name="windowsComponents"></param>
        protected virtual void Init_Windows_ComponentsClass(List<ComponentMatch> windowsComponents)
        {
            
        }

#endregion ------------------------------------ WINDOWS -----------------------------------


#endif
    }
}



#region ------------------------------GARBAGE ----------------------------------

//  GlobalConfiguration.Configure(WebApiConfig.Register);
//  If we need MVC  RouteConfig, then WCF  Routing can get non workable          
//  AreaRegistration.RegisterAllAreas();
//  FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
//  RouteConfig.RegisterRoutes(RouteTable.Routes); - MVC way   

//.MetaInfo as DCCUnitServerFactoryAttribute).TargetDCCommunicationUnit.S(), dcCommunicationFactoryComponent);

//var InitGlobalConfigMethod = (dcCommunicationFactoryComponent.MetaInfo as DCCUnitServerFactoryAttribute).InitGlobalConfigMethodName;
//if (InitGlobalConfigMethod.IsNull()) continue;

//var methodDelegate = dcCommunicationFactoryComponent.ComponentType.GetMethod(InitGlobalConfigMethod, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

//if (methodDelegate.IsNull()) continue;

////var configWasChangedcurrent = (bool)methodDelegate.Invoke(null, null); // new object[] { dcCommunicationFactoryComponent }
////if (configWasChangedcurrent == true) configWasChanged = true;




//public AppDomainDispatcher(string domain, string process)
//{
//    this.domainName = domain;
//    this.processName = process;
//    ad = AppDomain.CreateDomain(domain);
//}
//internal string domainName;
//internal string processName;
//internal int sleepTime = 1000;
//internal AppDomain ad;
//internal Thread appThrd;
//public void Run()
//{
//    appThrd = new Thread(new ThreadStart(this.RunApp));
//    appThrd.Start();
//}

//private void RunApp()
//{
//    string[] args = new string[] { sleepTime.S() };
//    ad.ExecuteAssembly(this.processName, null, args);
//}


#endregion ------------------------------GARBAGE ----------------------------------
