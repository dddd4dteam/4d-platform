﻿#if   SL5
using Microsoft.Runtime.CompilerServices;
#endif 


using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace DDDD.Core.Threading
{
    internal class TaskShim
    {

#if NET45 || WP81

        public static Task Run(Action func)
        {
            return Task.Run(func);
        }

        public static Task Run(Func<Task> func)
        {
            return Task.Run(func);
        }

        public static Task<T> Run<T>(Func<T> func)
        {
            return Task.Run(func);
        }

        public static Task<T> Run<T>(Func<Task<T>> func)
        {
            return Task.Run(func);
        }

        public static Task<T> FromResult<T>(T value)
        {
            return Task.FromResult(value);
        }

        public static Task<T[]> WhenAll<T>(IEnumerable<Task<T>> tasks)
        {
            return Task.WhenAll(tasks);
        }

        public static Task<T[]> WhenAll<T>(params Task<T>[] tasks)
        {
            return Task.WhenAll(tasks);
        }

        public static Task WhenAll(params Task[] tasks)
        {
            return Task.WhenAll(tasks);
        }

        public static Task WhenAll(IEnumerable<Task> tasks)
        {
            return Task.WhenAll(tasks);
        }

        public static Task<Task<TResult>> WhenAny<TResult>(IEnumerable<Task<TResult>> tasks)
        {
            return Task.WhenAny(tasks);
        }

        public static Task<Task> WhenAny(IEnumerable<Task> tasks)
        {
            return Task.WhenAny(tasks);
        }

        public static Task<Task<TResult>> WhenAny<TResult>(params Task<TResult>[] tasks)
        {
            return Task.WhenAny(tasks);
        }

        public static Task<Task> WhenAny(params Task[] tasks)
        {
            return Task.WhenAny(tasks);
        }


        public static Task Delay(TimeSpan deleayTime)
        {
            return Task.Delay(deleayTime);
        }

        public static Task Delay(int millisecondsTime)
        {
            return Task.Delay(millisecondsTime);
        }


        public static Task Delay(int millisecondsTime, CancellationToken cancelToken)
        {
            return Task.Delay(millisecondsTime, cancelToken);
        }



#elif SL5

          public static Task Run(Action func)
        {
            return TaskEx.Run(func);
        }

        public static Task Run(Func<Task> func)
        {
            return TaskEx.Run(func);
        }

        public static Task<T> Run<T>(Func<T> func)
        {
            return TaskEx.Run(func);
        }

        public static Task<T> Run<T>(Func<Task<T>> func)
        {
            return TaskEx.Run(func);
        }

        public static Task<T> FromResult<T>(T value)
        {
            return TaskEx.FromResult(value);
        }

        public static Task<T[]> WhenAll<T>(IEnumerable<Task<T>> tasks)
        {
            return TaskEx.WhenAll(tasks);
        }

        public static Task<T[]> WhenAll<T>(params Task<T>[] tasks)
        {
            return TaskEx.WhenAll(tasks);
        }

        public static Task WhenAll(IEnumerable<Task> tasks)
        {
            return TaskEx.WhenAll(tasks);
        }

        public static Task WhenAll(params Task[] tasks)
        {
            return TaskEx.WhenAll(tasks);
        }
        
        public static Task<Task<TResult>> WhenAny<TResult>(IEnumerable<Task<TResult>> tasks)
        {
            return TaskEx.WhenAny(tasks);
        }

        public static Task<Task> WhenAny(IEnumerable<Task> tasks)
        {
            return TaskEx.WhenAny(tasks);
        }

        public static Task<Task<TResult>> WhenAny<TResult>(params Task<TResult>[] tasks)
        {
            return TaskEx.WhenAny(tasks);
        }

        public static Task<Task> WhenAny(params Task[] tasks)
        {
            return TaskEx.WhenAny(tasks);
        }

        public static YieldAwaitable Yield()
        {
            return TaskEx.Yield();
        }



        public static Task Delay(TimeSpan deleayTime)
        {
            return TaskEx.Delay(deleayTime);
        }


        public static Task Delay(int millisecondsTime)
        {
            return TaskEx.Delay(millisecondsTime);
        }


        public static Task Delay(int millisecondsTime, CancellationToken cancelToken)
        {
            return TaskEx.Delay(millisecondsTime, cancelToken);
        }



#endif


    }
}
