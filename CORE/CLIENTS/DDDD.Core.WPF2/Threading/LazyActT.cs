﻿//using Nito.AsyncEx;
using System;
using System.Diagnostics;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;

namespace DDDD.Core.Threading
{


    /// <summary>
    /// Thread Safety Lazy Initializer and multiple time use of Init Action. Lazy concept - tell us, that in first time on [Value.Get;] we initializing thread safely this Value- so from second-time on [Value.Get;] we'll have simple and fast [getting;] operation,because [Value] was already inited.
    /// <para/> T initing OPTIONS are the followings:
    /// <para/>         1-The T value can be inited on Create(...) method by your initFunc[Func{object[],T}]
    /// <para/>         2- T value can be resetted by second/...- other initfunc and then reinited agin on next [.Value] get call;
    /// <para/>         3- T value can be resetted directly by your other T value.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class LazyAct<T>
    { 


        #region ------------------------------------ FIELDS ----------------------------------

#if DEBUG

        public int CreateCounter
        { get; private set; }


        public int LazyInitCounter
        { get; private set; }
#endif

        /// <summary>
        /// Global Locker instance
        /// </summary>
        static readonly object locker = new object();


        //Semaphore lockSemaphore = new Semaphore(1, System.Environment.ProcessorCount);
        //private readonly AsyncLock m_lock = new AsyncLock();

        /// <summary>
        /// alternate locking instance
        /// </summary>
        object LockingInstance = null;

        /// <summary>
        /// ReturnFunc - is [Facade GET Func] that will be updated, when CreatedValue will be inited by InitFunc - on first get action
        /// </summary>
        Func<T> ReturnFunc;

        /// <summary>
        /// Func that will create CreatedValue - buffer variable
        /// </summary>
        Func<object[],T> InitFunc;

        /// <summary>
        /// Init Args to call InitFunc.
        /// </summary>
        object[] InitArgs;

        /// <summary>
        /// instance of T - that we get with the help of InitFunc
        /// </summary>
        T CreatedValue;


        

        /// <summary>
        /// inner simple getter func of CreatedValue
        /// </summary>
        /// <returns></returns>
        T CreatedValueGetter() 
        {
          
                return CreatedValue; 
        }



        // bool IsCreateInited = false;

        bool IsLazyInitialized/// tell us that Value already has Inited ReturnFunc value.
        { get; set; }


        /// <summary>
        /// Clean InitFunc or nullify reference to this delegate after inited was done. If InitFunc still will be saved, 
        /// then new InitFunc action call will be called with emptyArgs or you need to set InitArgs separately again.
        /// </summary>
        bool CleanInitActionAfterInited // 
        { get; set; } = true;

        /// <summary>
        /// Clean InitFunc or nullify reference to this delegate after inited was done. If InitFunc stil will be saved, 
        /// then new InitFunc action call will be called with emptyArgs or you need to set InitArgs separately again.
        /// </summary>
        bool CleanInitArgsAfterInited // save InitArgs or nullify reference to this delegate  after inited was done.
        { get; set; } = true;


        /// <summary>
        ///  
        /// </summary>
        bool UseAutoResetToAlwaysLazyInitOnValueGet
        { get; set; } = false;


        #endregion ------------------------------------ FIELDS ----------------------------------


        /// <summary>
        /// Lazy Inited from your initFunc(Func{object[],T}) that you've setted in Create(...) method, in thread safe mode , your Target T value.
        /// <para/> T initing OPTIONS are the followings: 1-The T value can be inited on Create(...) method by your initFunc[Func{object[],T}]
        /// <para/>         2- T value can be resetted by second/...- other initfunc and then reinited agin on next [.Value] get call;
        /// <para/>         3- T value can be resetted directly by your other T value.
        /// </summary>
        public T Value
        {
            get
            {
                if (UseAutoResetToAlwaysLazyInitOnValueGet)
                {
                    var result = ReturnFunc();
                    ResetToLazyInitAgain();
                    return result;
                }
                else 
                return ReturnFunc();
            }
        }


        /// <summary>
        /// /// Creating thread Safety lazy one time slim initializer. Simple get construction from secod-time get - max speed.
        /// It's Thread safe method. For lock we use here Lazy{T} Global Locker instance. 
                /// </summary>
        /// <param name="initFunc"></param>
        /// <param name="lockingInstance"></param>
        /// <param name="args"></param>
        /// <param name="cleanInitActionAfterInited"></param>
        /// <param name="cleanInitArgsAfterInited"></param>
        /// <returns></returns>
        public static LazyAct<T> Create(Func<object[],T> initFunc, object lockingInstance , object[] args, bool cleanInitActionAfterInited= true, bool cleanInitArgsAfterInited = true, bool useAutoResetToAlwaysLazyInitOnValueGet = false)
        {
            Debug.Assert(initFunc != null, $"ERROR in {nameof(LazyAct<T>)} where T:[{typeof(T).FullName}] in {nameof(Create)}() :  parameter  {nameof(initFunc)}  cannot be null");

            try
            {
                LazyAct<T> newLazyItem = null;

                locker.LockAction(()=>newLazyItem.IsNull(),
                    () =>
                    {
                        newLazyItem = new LazyAct<T>();
                        newLazyItem.LockingInstance = lockingInstance; // alternate  locking instance for initing  LazySlim.Value 
                        newLazyItem.InitFunc = initFunc;
                        newLazyItem.InitArgs = args;
                        newLazyItem.CleanInitActionAfterInited = cleanInitActionAfterInited;
                        newLazyItem.CleanInitArgsAfterInited = cleanInitArgsAfterInited;
                        newLazyItem.UseAutoResetToAlwaysLazyInitOnValueGet = useAutoResetToAlwaysLazyInitOnValueGet;

                        BuildCreateGetSwitchFunc(newLazyItem);//
#if DEBUG
                        newLazyItem.CreateCounter++;
#endif
                    }
                    );

#if DEBUG                
                if (newLazyItem.IsNull())
                { throw new InvalidOperationException( "Can't Create new LazySlim item of T[{0}] ".Fmt( typeof(T).FullName )); }
#endif
                return newLazyItem;
                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException( "ERROR in  {0}.{1}()  where T:[{2}] . Exception Message -  [{3}] ".Fmt(nameof(LazyAct<T>), nameof(Create), typeof(T).FullName, exc.Message ), exc);
            }
        }




        /// <summary>
        /// BuildCreateGetSwitchFunc - internal rebuilding ReturnFunc when  InitFunc was changed.
        /// After we'll create  CreatedValue with the help of InitFunc(), InitFunc will be nulled. 
        /// </summary>
        /// <param name="creatingLazy"></param>
       static void BuildCreateGetSwitchFunc(LazyAct<T>   lazyItem) //LazySlim<T> creatingLazy
        {
            try
            {
                Validator.ATNullReferenceArgDbg(lazyItem, nameof(LazyAct<T>), nameof(BuildCreateGetSwitchFunc), nameof(lazyItem));
                

                if (lazyItem.LockingInstance != null)
                {
                    lazyItem.ReturnFunc =  () =>// first lazy initializing
                    {

                            lazyItem.LockingInstance.LockAction(()=> (lazyItem.IsLazyInitialized == false)
                            ,()=>
                            {
                                lazyItem.CreatedValue = lazyItem.InitFunc(lazyItem.InitArgs);
                                if (lazyItem.CleanInitActionAfterInited ) // by default it's true -do reference clean                                 
                                {
                                    lazyItem.InitFunc = null; 
                                }
                                if (lazyItem.CleanInitArgsAfterInited) //  by default it's true -do reference clean                                 
                                {
                                    lazyItem.InitArgs = null;
                                }
                                
                                lazyItem.ReturnFunc = lazyItem.CreatedValueGetter; //simply getted
#if DEBUG
                                lazyItem.LazyInitCounter += 1;
#endif
                                lazyItem.IsLazyInitialized = true;
                                
                            }
                            
                            );

                        return lazyItem.CreatedValue;
                        //throw new InvalidOperationException($"Can't correctly init LazySlim item of T[{typeof(T).FullName}] ");
                    };
                }
                else
                {
                    lazyItem.ReturnFunc = () =>// first lazy initializing
                    {

                            locker.LockAction(()=> (lazyItem.IsLazyInitialized == false)  
                            , () =>
                            {
                                lazyItem.CreatedValue = lazyItem.InitFunc(lazyItem.InitArgs);
                                if (lazyItem.CleanInitActionAfterInited) // by default it's true -do reference clean                                 
                                {
                                    lazyItem.InitFunc = null;
                                }
                                if (lazyItem.CleanInitArgsAfterInited) //  by default it's true -do reference clean                                 
                                {
                                    lazyItem.InitArgs = null;
                                }
                                lazyItem.ReturnFunc =  lazyItem.CreatedValueGetter; //simply getted
#if DEBUG
                                lazyItem.LazyInitCounter += 1;
#endif
                                lazyItem.IsLazyInitialized = true;// 
                                
                            }
                            );

                           return lazyItem.CreatedValue;

                        // throw new InvalidOperationException($"Can't correctly init LazySlim item of T[{typeof(T).FullName}] ");
                    };
                }
            }
            catch (Exception exc)
            {   throw new InvalidOperationException($"ERROR IN {nameof(LazyAct<T>)}.{nameof(BuildCreateGetSwitchFunc)}():  Error on InitFunc raising. Exception Message - [{exc.Message}] ");
            }
        }

        

        /// <summary>
        /// Reset InitFunc to another func.It can be used when we need to change a way of how to get-init {T} Value.
        /// It's Thread safe method.For lock we use here lockingInstance that was inited on Lazy{T} Creation if it is not null, or Lazy{T} Global Locker instance. 
        /// <para/> So we'll have another Created Value on the next  Lazy{T}.Value call.
        /// <para/> Parameter  newInitFunc parameter CAN'T BE NULL.
        /// </summary>
        /// <param name="newInitFunc"></param>
        public void ResetInitFunc(Func<object[],T> newInitFunc, params object[] argsnew)
        {
            Debug.Assert(newInitFunc != null, "ERROR in {0}.{1}()  where T:[{2}] in {3}() :  parameter  {4}  cannot be null".Fmt(nameof(LazyAct<T>), nameof(ResetInitFunc) , typeof(T).FullName, nameof(ResetInitFunc), nameof(newInitFunc)) );

            try
            {
                if (LockingInstance != null)
                {

                    lock (LockingInstance) //locking on creating LazySlim object
                    {
                        InitFunc = newInitFunc; InitArgs = argsnew;
                        BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;
                    }
                }
                else
                {
                    lock (locker) //lock globally on private LazySlim Locker instance
                    {
                        InitFunc = newInitFunc; InitArgs = argsnew;
                        BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("ERROR in  {0} where T:[{1}] in {2}() : exception message -  [{3}] ".Fmt(nameof(LazyAct<T>), typeof(T).FullName, nameof(ResetInitFunc), exc.Message  ), exc);
            }
        }


        /// <summary>
        /// Resetting to Lazy initing on next [Value.get]. The function initFunc should exist or this method will done nothing.
        /// </summary>
        public void ResetToLazyInitAgain()
        {
            if (InitFunc.IsNull()) return;
             
            try
            {
                if (LockingInstance != null)
                {

                    lock (LockingInstance) //locking on creating LazySlim object
                    {
                       
                        if (InitArgs.IsNull()) InitArgs = new object[] { };
                        BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;
                    }
                }
                else
                {
                    lock (locker) //lock globally on private LazySlim Locker instance
                    {
                        if (InitArgs.IsNull()) InitArgs = new object[] { };
                        BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("ERROR in  {0} where T:[{1}] in {2}() : exception message -  [{3}] ".Fmt(nameof(LazyAct<T>), typeof(T).FullName, nameof(ResetToLazyInitAgain), exc.Message), exc);
            }
        }


        /// <summary>
        /// Reset 
        /// </summary>
        /// <param name="argsNew"></param>
        public void ResetInitArgs(params object[] argsNew)
        {
            Debug.Assert(argsNew != null, "ERROR in {0}.{1}()  where T:[{2}] in {3}() :  parameter  {4}  cannot be null".Fmt(nameof(LazyAct<T>), nameof(ResetInitArgs), typeof(T).FullName, nameof(ResetInitArgs), nameof(argsNew)));

            try
            {
                if (LockingInstance != null)
                {

                    lock (LockingInstance) //locking on creating LazySlim object
                    {
                         InitArgs = argsNew;
                        //BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;
                    }
                }
                else
                {
                    lock (locker) //lock globally on private LazySlim Locker instance
                    {
                        InitArgs = argsNew;
                        //BuildCreateGetSwitchFunc(this);//this
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = false;

                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("ERROR in  {0} where T:[{1}] in {2}() : exception message -  [{3}] ".Fmt(nameof(LazyAct<T>), typeof(T).FullName, nameof(ResetInitArgs), exc.Message), exc);
            }
        }


        /// <summary>
        /// Reset Created Value directly to custom end value. It can be used when we need to change a way of how to get {T} Value.
        /// It's Thread safe method.For lock we use here lockingInstance that was inited on Lazy{T} Creation if it is not null, or Lazy{T} Global Locker instance. 
        /// <para/> So we'll have another Created Value on the next  Lazy{T}.Value call. 
        /// <para/>Parameter newCreatedValue also CAN BE NULL for {T} is class type, or DEFAULT(T) if {T} is structure type.
        /// </summary>
        /// <param name="newCreatedValue"></param>
        public void ResetValueDirectly(T newCreatedValue)
        { 
            try
            {
                if (LockingInstance != null)
                {
                    lock (LockingInstance) //locking on crating LazySlim object
                    {
                        InitFunc = null; InitArgs = null;
                        CreatedValue = newCreatedValue;
                        ReturnFunc = CreatedValueGetter;
#if DEBUG
                        LazyInitCounter += 1;
#endif
                        IsLazyInitialized = true;
                    }
                }
                else
                {
                    lock (locker) //lock globally on private LazySlim Locker instance
                    {
                        InitFunc = null; InitArgs = null;
                        CreatedValue = newCreatedValue;
                        ReturnFunc = CreatedValueGetter;
#if DEBUG
                        LazyInitCounter += 1;
#endif

                        IsLazyInitialized = true;
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in  {nameof(LazyAct<T>)} where T:[{typeof(T).FullName}] in {nameof(ResetValueDirectly)}() : exception message -  [{exc.Message}] ", exc);
            }

        }
    }

}


#region ------------------------ GARBAGE --------------------------------




//creatingLazy.ReturnFunc = () =>// first lazy initializing
//{
//    if (creatingLazy.LockingInstance != null)
//    {
//        lock (creatingLazy.LockingInstance) //lock target instance
//        {
//            creatingLazy.CreatedValue = creatingLazy.CreateFunc();

//            creatingLazy.ReturnFunc = () => { return creatingLazy.CreatedValue; }; //simply getted

//            return creatingLazy.CreatedValue;
//        }
//    }
//    else
//    {
//        lock (locker) //lock globally on private LazySlim Locker instance
//        {
//            creatingLazy.CreatedValue = creatingLazy.CreateFunc();

//            creatingLazy.ReturnFunc = () => { return creatingLazy.CreatedValue; }; //simply getted

//            return creatingLazy.CreatedValue;
//        }
//    }

//};


#endregion ------------------------ GARBAGE --------------------------------