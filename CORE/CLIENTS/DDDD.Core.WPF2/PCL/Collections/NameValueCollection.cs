﻿#if CLIENT && (SL5 || WP81)

using System;
using System.Collections;
using System.Runtime.Serialization;
using System.Text;
using DDDD.Core.Extensions;


namespace DDDD.Core.Collections
{
    public class NameValueCollection : NameObjectCollectionBase
    {
        private string[] cachedAllKeys;
        private string[] cachedAll;

        public virtual string[] AllKeys
        {
            get
            {
                if (cachedAllKeys == null)
                    cachedAllKeys = BaseGetAllKeys();
                return cachedAllKeys;
            }
        }

        public string this[int index]
        {
            get
            {
                return Get(index);
            }
        }

        public string this[string name]
        {
            get
            {
                return Get(name);
            }
            set
            {
                Set(name, value);
            }
        }

        public NameValueCollection()
        {
        }

        public NameValueCollection(int capacity)
          : base(capacity)
        {
        }

        public NameValueCollection(NameValueCollection col)
          : base(col.EqualityComparer)
        {
            if (col == null)
                throw new ArgumentNullException("col");
            Add(col);
        }

        protected NameValueCollection(SerializationInfo info, StreamingContext context)
          : base(info, context)
        {
        }

        public NameValueCollection(IEqualityComparer equalityComparer)
          : base(equalityComparer)
        {
        }

        public NameValueCollection(int capacity, IEqualityComparer equalityComparer)
          : base(capacity, equalityComparer)
        {
        }

        public void Add(NameValueCollection c)
        {
            if (IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            if (c == null)
                throw new ArgumentNullException("c");
            InvalidateCachedArrays();
            int count = c.Count;
            for (int index = 0; index < count; ++index)
            {
                string key = c.GetKey(index);
                string[] values = c.GetValues(index);
                if (values != null && values.Length > 0)
                {
                    foreach (string str in values)
                        Add(key, str);
                }
                else
                    Add(key,  null);
            }
        }

        public virtual void Add(string name, string value)
        {
            if (IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            InvalidateCachedArrays();
            ArrayList arrayList1 = (ArrayList)BaseGet(name);
            if (arrayList1 == null)
            {
                ArrayList arrayList2 = new ArrayList();
                if (value != null)
                    arrayList2.Add(value);
                BaseAdd(name, arrayList2);
            }
            else
            {
                if (value == null)
                    return;
                arrayList1.Add(value);
            }
        }

        public virtual void Clear()
        {
            if (IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            this.InvalidateCachedArrays();
            this.BaseClear();
        }

        public void CopyTo(Array dest, int index)
        {
            if (dest == null)
                throw new ArgumentNullException("dest", "Null argument - dest");
            if (index < 0)
                throw new ArgumentOutOfRangeException("index", "index is less than 0");
            if (dest.Rank > 1)
                throw new ArgumentException("dest", "multidim");
            if (cachedAll == null)
                RefreshCachedAll();
            try
            {
                cachedAll.CopyTo(dest, index);
            }
            catch (ArrayTypeMismatchException ex)
            {
                throw new InvalidCastException();
            }
        }

        private void RefreshCachedAll()
        {
            this.cachedAll = null;
            int count = this.Count;
            this.cachedAll = new string[count];
            for (int index = 0; index < count; ++index)
                cachedAll[index] = this.Get(index);
        }

        public virtual string Get(int index)
        {
            return AsSingleString((ArrayList)this.BaseGet(index));
        }

        public virtual string Get(string name)
        {
            return AsSingleString((ArrayList)BaseGet(name));
        }

        private static string AsSingleString(ArrayList values)
        {
            if (values == null)
                return null;
            int count = values.Count;
            switch (count)
            {
                case 0:
                    return null;
                case 1:
                    return (string)values[0];
                case 2:
                    return (string)values[0] + ',' + (string)values[1];
                default:
                    int capacity = count;
                    for (int index = 0; index < count; ++index)
                        capacity += ((string)values[index]).Length;
                    StringBuilder stringBuilder = new StringBuilder((string)values[0], capacity);
                    for (int index = 1; index < count; ++index)
                    {
                        stringBuilder.Append(',');
                        stringBuilder.Append(values[index]);
                    }
                    return stringBuilder.S();
            }
        }

        public virtual string GetKey(int index)
        {
            return BaseGetKey(index);
        }

        public virtual string[] GetValues(int index)
        {
            return AsStringArray((ArrayList)BaseGet(index));
        }

        public virtual string[] GetValues(string name)
        {
            return AsStringArray((ArrayList)BaseGet(name));
        }

        private static string[] AsStringArray(ArrayList values)
        {
            if (values == null)
                return null;
            int count = values.Count;
            if (count == 0)
                return (string[])null;
            string[] strArray = new string[count];
            values.CopyTo((object[])strArray);
            return strArray;
        }

        public bool HasKeys()
        {
            return this.BaseHasKeys();
        }

        public virtual void Remove(string name)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            this.InvalidateCachedArrays();
            this.BaseRemove(name);
        }

        public virtual void Set(string name, string value)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            this.InvalidateCachedArrays();
            ArrayList arrayList = new ArrayList();
            if (value != null)
            {
                arrayList.Add((object)value);
                this.BaseSet(name, (object)arrayList);
            }
            else
                this.BaseSet(name, (object)null);
        }

        protected void InvalidateCachedArrays()
        {
            this.cachedAllKeys = null;
            this.cachedAll = null;
        }
    }
}

#endif