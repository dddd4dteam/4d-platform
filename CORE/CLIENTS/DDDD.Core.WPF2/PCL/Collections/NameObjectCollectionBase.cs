﻿
#if CLIENT && (SL5 || WP81)

using DDDD.Core.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Collections
{
    public abstract class NameObjectCollectionBase : ICollection, IEnumerable
    {
        private Hashtable m_ItemsContainer;
        private _Item m_NullKeyItem;
        private ArrayList m_ItemsArray;
        private IComparer m_comparer;
        private int m_defCapacity;
        private bool m_readonly;
        private SerializationInfo infoCopy;
        private KeysCollection keyscoll;
        private IEqualityComparer equality_comparer;

        internal IEqualityComparer EqualityComparer
        {
            get
            {
                return this.equality_comparer;
            }
        }

        internal IComparer Comparer
        {
            get
            {
                return this.m_comparer;
            }
        }

        public virtual NameObjectCollectionBase.KeysCollection Keys
        {
            get
            {
                if (this.keyscoll == null)
                    this.keyscoll = new NameObjectCollectionBase.KeysCollection(this);
                return this.keyscoll;
            }
        }

        public virtual int Count
        {
            get
            {
                return this.m_ItemsArray.Count;
            }
        }

        bool ICollection.IsSynchronized
        {
            get
            {
                return false;
            }
        }

        object ICollection.SyncRoot
        {
            get
            {
                return (object)this;
            }
        }

        protected bool IsReadOnly
        {
            get
            {
                return this.m_readonly;
            }
            set
            {
                this.m_readonly = value;
            }
        }

        protected NameObjectCollectionBase()
        {
            this.m_readonly = false;
            this.m_comparer = (IComparer)StringComparer.Ordinal;
            this.m_defCapacity = 0;
            this.Init();
        }

        protected NameObjectCollectionBase(int capacity)
        {
            this.m_readonly = false;
            this.m_comparer = (IComparer)StringComparer.Ordinal;
            this.m_defCapacity = capacity;
            this.Init();
        }

        protected NameObjectCollectionBase(IEqualityComparer equalityComparer)
          : this(0, equalityComparer ?? (IEqualityComparer)StringComparer.OrdinalIgnoreCase)
        {
        }

        protected NameObjectCollectionBase(SerializationInfo info, StreamingContext context)
        {
            this.infoCopy = info;
        }

        protected NameObjectCollectionBase(int capacity, IEqualityComparer equalityComparer)
        {
            this.m_readonly = false;
            this.equality_comparer = equalityComparer ?? (IEqualityComparer)StringComparer.OrdinalIgnoreCase;
            this.m_defCapacity = capacity;
            this.Init();
        }

        private void Init()
        {
            if (this.m_ItemsContainer != null)
            {
                this.m_ItemsContainer.Clear();
                this.m_ItemsContainer = (Hashtable)null;
            }
            if (this.m_ItemsArray != null)
            {
                this.m_ItemsArray.Clear();
                this.m_ItemsArray = (ArrayList)null;
            }
            if (this.equality_comparer == null)
                this.equality_comparer = (IEqualityComparer)StringComparer.OrdinalIgnoreCase;
            this.m_ItemsContainer = new Hashtable(this.m_defCapacity, (IEqualityComparer<string>)new StringEqualityComparer(this.equality_comparer));
            this.m_ItemsArray = new ArrayList();
            this.m_NullKeyItem = (NameObjectCollectionBase._Item)null;
        }

        public virtual IEnumerator GetEnumerator()
        {
            return (IEnumerator)new NameObjectCollectionBase._KeysEnumerator(this);
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException("info");
            int count = this.Count;
            string[] strArray = new string[count];
            object[] objArray = new object[count];
            int index = 0;
            foreach (NameObjectCollectionBase._Item obj in (List<object>)this.m_ItemsArray)
            {
                strArray[index] = obj.key;
                objArray[index] = obj.value;
                ++index;
            }
            info.AddValue("KeyComparer", (object)this.equality_comparer, typeof(IEqualityComparer));
            info.AddValue("Version", (object)4, typeof(int));
            info.AddValue("ReadOnly", this.m_readonly);
            info.AddValue("Count", count);
            info.AddValue("Keys", (object)strArray, typeof(string[]));
            info.AddValue("Values", (object)objArray, typeof(object[]));
        }

        void ICollection.CopyTo(Array array, int index)
        {
            ((ICollection)this.Keys).CopyTo(array, index);
        }

        public virtual void OnDeserialization(object sender)
        {
            SerializationInfo serializationInfo = this.infoCopy;
            if (serializationInfo == null)
                return;
            this.infoCopy = (SerializationInfo)null;
            this.m_comparer = (IComparer)serializationInfo.GetValue("Comparer", typeof(IComparer));
            if (this.m_comparer == null)
                throw new SerializationException("The comparer is null");
            this.m_readonly = serializationInfo.GetBoolean("ReadOnly");
            string[] strArray = (string[])serializationInfo.GetValue("Keys", typeof(string[]));
            if (strArray == null)
                throw new SerializationException("keys is null");
            object[] objArray = (object[])serializationInfo.GetValue("Values", typeof(object[]));
            if (objArray == null)
                throw new SerializationException("values is null");
            this.Init();
            int length = strArray.Length;
            for (int index = 0; index < length; ++index)
                this.BaseAdd(strArray[index], objArray[index]);
        }

        protected void BaseAdd(string name, object value)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            NameObjectCollectionBase._Item obj = new NameObjectCollectionBase._Item(name, value);
            if (name == null)
            {
                if (this.m_NullKeyItem == null)
                    this.m_NullKeyItem = obj;
            }
            else if (!this.HasItem(name))
                this.m_ItemsContainer.Add(name, (object)obj);
            this.m_ItemsArray.Add((object)obj);
        }

        protected void BaseClear()
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            this.Init();
        }

        protected object BaseGet(int index)
        {
            return ((NameObjectCollectionBase._Item)this.m_ItemsArray[index]).value;
        }

        protected object BaseGet(string name)
        {
            NameObjectCollectionBase._Item firstMatchedItem = this.FindFirstMatchedItem(name);
            if (firstMatchedItem == null)
                return (object)null;
            return firstMatchedItem.value;
        }

        protected string[] BaseGetAllKeys()
        {
            int count = this.m_ItemsArray.Count;
            string[] strArray = new string[count];
            for (int index = 0; index < count; ++index)
                strArray[index] = this.BaseGetKey(index);
            return strArray;
        }

        protected object[] BaseGetAllValues()
        {
            int count = this.m_ItemsArray.Count;
            object[] objArray = new object[count];
            for (int index = 0; index < count; ++index)
                objArray[index] = this.BaseGet(index);
            return objArray;
        }

        protected object[] BaseGetAllValues(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("'type' argument can't be null");
            int count = this.m_ItemsArray.Count;
            object[] objArray = (object[])Array.CreateInstance(type, count);
            for (int index = 0; index < count; ++index)
                objArray[index] = this.BaseGet(index);
            return objArray;
        }

        protected string BaseGetKey(int index)
        {
            return ((NameObjectCollectionBase._Item)this.m_ItemsArray[index]).key;
        }

        protected bool BaseHasKeys()
        {
            return this.m_ItemsContainer.Count > 0;
        }

        protected void BaseRemove(string name)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            if (name != null)
                this.m_ItemsContainer.Remove(name);
            else
                this.m_NullKeyItem = (NameObjectCollectionBase._Item)null;
            int count = this.m_ItemsArray.Count;
            int index = 0;
            while (index < count)
            {
                if (this.Equals(this.BaseGetKey(index), name))
                {
                    this.m_ItemsArray.RemoveAt(index);
                    --count;
                }
                else
                    ++index;
            }
        }

        protected void BaseRemoveAt(int index)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            string key = this.BaseGetKey(index);
            if (key != null)
                this.m_ItemsContainer.Remove(key);
            else
                this.m_NullKeyItem = (NameObjectCollectionBase._Item)null;
            this.m_ItemsArray.RemoveAt(index);
        }

        protected void BaseSet(int index, object value)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            ((NameObjectCollectionBase._Item)this.m_ItemsArray[index]).value = value;
        }

        protected void BaseSet(string name, object value)
        {
            if (this.IsReadOnly)
                throw new NotSupportedException("Collection is read-only");
            NameObjectCollectionBase._Item firstMatchedItem = this.FindFirstMatchedItem(name);
            if (firstMatchedItem != null)
                firstMatchedItem.value = value;
            else
                this.BaseAdd(name, value);
        }

        private bool HasItem(string name)
        {
            return this.FindFirstMatchedItem(name) != this.m_NullKeyItem;
        }

        private NameObjectCollectionBase._Item FindFirstMatchedItem(string name)
        {
            object obj;
            if (name != null && this.m_ItemsContainer.TryGetValue(name, out obj))
                return obj as NameObjectCollectionBase._Item ?? this.m_NullKeyItem;
            return this.m_NullKeyItem;
        }

        internal bool Equals(string s1, string s2)
        {
            if (this.m_comparer != null)
                return this.m_comparer.Compare((object)s1, (object)s2) == 0;
            return this.equality_comparer.Equals((object)s1, (object)s2);
        }

        internal class _Item
        {
            public string key;
            public object value;

            public _Item(string key, object value)
            {
                this.key = key;
                this.value = value;
            }
        }

        ///[Serializable]
        internal class _KeysEnumerator : IEnumerator
        {
            private NameObjectCollectionBase m_collection;
            private int m_position;

            public object Current
            {
                get
                {
                    if (this.m_position < this.m_collection.Count || this.m_position < 0)
                        return (object)this.m_collection.BaseGetKey(this.m_position);
                    throw new InvalidOperationException();
                }
            }

            internal _KeysEnumerator(NameObjectCollectionBase collection)
            {
                this.m_collection = collection;
                this.Reset();
            }

            public bool MoveNext()
            {
                return ++this.m_position < this.m_collection.Count;
            }

            public void Reset()
            {
                this.m_position = -1;
            }
        }

        //[Serializable]
        public class KeysCollection : ICollection, IEnumerable
        {
            private NameObjectCollectionBase m_collection;

            bool ICollection.IsSynchronized
            {
                get
                {
                    return false;
                }
            }

            object ICollection.SyncRoot
            {
                get
                {
                    return (object)this.m_collection;
                }
            }

            public int Count
            {
                get
                {
                    return this.m_collection.Count;
                }
            }

            public string this[int index]
            {
                get
                {
                    return this.Get(index);
                }
            }

            internal KeysCollection(NameObjectCollectionBase collection)
            {
                this.m_collection = collection;
            }

            public virtual string Get(int index)
            {
                return this.m_collection.BaseGetKey(index);
            }

            void ICollection.CopyTo(Array array, int arrayIndex)
            {
                ArrayList arrayList = this.m_collection.m_ItemsArray;
                if (array == null)
                    throw new ArgumentNullException("array");
                if (arrayIndex < 0)
                    throw new ArgumentOutOfRangeException("arrayIndex");
                if (array.Length > 0 && arrayIndex >= array.Length)
                    throw new ArgumentException("arrayIndex is equal to or greater than array.Length");
                if (arrayIndex + arrayList.Count > array.Length)
                    throw new ArgumentException("Not enough room from arrayIndex to end of array for this KeysCollection");
                if (array != null && array.Rank > 1)
                    throw new ArgumentException("array is multidimensional");
                object[] objArray = (object[])array;
                int index = 0;
                while (index < arrayList.Count)
                {
                    objArray[arrayIndex] = (object)((NameObjectCollectionBase._Item)arrayList[index]).key;
                    ++index;
                    ++arrayIndex;
                }
            }

            public IEnumerator GetEnumerator()
            {
                return (IEnumerator)new NameObjectCollectionBase._KeysEnumerator(this.m_collection);
            }
        }
    }

}

#endif