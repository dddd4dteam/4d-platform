﻿
#if CLIENT && (SL5 || WP81)

using System;
using System.Collections;
//using Core.Collections;

namespace DDDD.Core.Collections
{

    public sealed class SerializationInfoEnumerator : IEnumerator
    {
        private IEnumerator enumerator;

        public SerializationEntry Current
        {
            get
            {
                return (SerializationEntry)this.enumerator.Current;
            }
        }

        object IEnumerator.Current
        {
            get
            {
                return this.enumerator.Current;
            }
        }

        public string Name
        {
            get
            {
                return this.Current.Name;
            }
        }

        public Type ObjectType
        {
            get
            {
                return this.Current.ObjectType;
            }
        }

        public object Value
        {
            get
            {
                return this.Current.Value;
            }
        }

        internal SerializationInfoEnumerator(IEnumerable list)
        {
            this.enumerator = list.GetEnumerator();
        }

        public bool MoveNext()
        {
            return this.enumerator.MoveNext();
        }

        public void Reset()
        {
            this.enumerator.Reset();
        }
    }

}

#endif