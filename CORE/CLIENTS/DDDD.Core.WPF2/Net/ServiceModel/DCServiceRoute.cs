﻿
#if SERVER && IIS


using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;

namespace DDDD.Core.Net.ServiceModel
{

    /// <summary>
    /// 
    /// </summary>
    public class DCServiceRoute : ServiceRoute
    {

        #region --------------------------------------- CTOR --------------------------------

        public DCServiceRoute(Type publishingManager, ServiceHostFactoryBase serviceHostFactory, Type serviceType)
            : base(GetRoutePrifixFromManagerClassName(publishingManager.Name), serviceHostFactory, serviceType)
        {
            PublishingManager = publishingManager;

            RoutePrefix = GetRoutePrifixFromManagerClassName(publishingManager.Name);
        }

        #endregion --------------------------------------- CTOR --------------------------------

        
        public static string GetRoutePrifixFromManagerClassName(string managerClassName)
        {
            return @"wcf/" + managerClassName.Replace(ManagerClassEnding, "");
        }


        /// <summary>
        ///  Manager Classes ending Word - 'Manager'
        /// </summary>
        public const string ManagerClassEnding = "Manager";


        public Type PublishingManager
        { get; private set; }


        public string RoutePrefix
        { get; private set; }


        /// <summary>
        /// Additional(with RouteTable) dictionary of  DCServiceRoutes directly associated with DCManagers
        /// </summary>
        internal protected static Dictionary<Type, DCServiceRoute> ManagerServiceRoutes
        { get; private set; } = new Dictionary<Type, DCServiceRoute>();


        internal static bool IsContainsServiceRouteForManager(Type targetManagerType)
        {
            return ManagerServiceRoutes.ContainsKey(targetManagerType);
        }


        /// <summary>
        /// Create and Register WCF DCService for some Manager.
        /// </summary>
        /// <param name="publishingManager"></param>
        internal static void CreateRegisterWcfServiceForManager(Type publishingManagerType)
        {
            if (ManagerServiceRoutes.ContainsKey(publishingManagerType)) return;            
            var dcServiceRoute = new DCServiceRoute(publishingManagerType, GetWcfCommunicationServiceHostFactory(publishingManagerType), GetWcfCommunicationServiceType());
            ManagerServiceRoutes.Add(publishingManagerType, dcServiceRoute);
            RouteTable.Routes.Add(dcServiceRoute);
        }


        internal protected static Type GetWcfCommunicationServiceType()
        {
            return typeof(DCService);
        }


        internal protected static ServiceHostFactory GetWcfCommunicationServiceHostFactory(Type publishingManager, bool useWebHostFactory = true)
        {
            if (useWebHostFactory == true)
            { var newServiceHostFactory = new DCServiceHostFactory();// WebServiceHostFactory();  was DCServiceHostFactory(publishingManager)
                return newServiceHostFactory;
            }
            else
            { return new ServiceHostFactory();
            }
        }

}

}


#endif
