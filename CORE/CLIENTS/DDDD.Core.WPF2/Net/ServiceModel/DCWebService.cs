﻿
#if SERVER && IIS

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;

using System;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web.Routing;
using System.Collections.Generic;
using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Net.ServiceModel
{


// Then define a new ServiceRoute using the AutofacServiceHostFactory and service implementation type.
/*
   RouteTable.Routes.Add(new ServiceRoute("Service1", new AutofacServiceHostFactory(), typeof(Service1)));
*/
// Finally, add the UrlRoutingModule to the web.config file.
/*
<system.webServer>
  <modules runAllManagedModulesForAllRequests = "true" >
    <add name= "UrlRoutingModule" type= "System.Web.Routing.UrlRoutingModule, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
  </modules>
  <handlers>
   < add name= "UrlRoutingHandler" preCondition= "integratedMode" verb= "*" path= "UrlRouting.axd" />
  </handlers>
</system.webServer>
*/
// After configuring your application in *.config - you will be able to access the WCF service at: http://hostname/appname/Service1

 


    #region  --------------------  SERVICE CONTRACT IMPLEMENTOR --------------------------

    
    /// <summary>
    /// Dynamic Command Service - Implements IDCWebService - WCF ServiceContract on server-side. 
    /// <para/> DCService- Dynamic Command Service here means that we
    /// <para/>    - use  DCMessage custom container to transmit almost any data structure,
    /// <para/>    - and redirect DC item processing to any target DCManager(must exist by command Key).   
    /// </summary>	  
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class DCWebService : IDCWebService, IDCCUnitServer
    {

        #region ---------------------- CTOR ----------------------

        public DCWebService()
        {
        }

        #endregion ---------------------- CTOR ----------------------
         

        /// <summary>
        /// DC Scenario Key
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios.Wcf_EnvironmentActivationREST;
         

        /// <summary>
        /// Processing Command Message on Server side.
        /// </summary>
        /// <param name="ClientCommand"></param>
        /// <returns></returns>
        public IDCMessage ExecuteCommandSL5(IDCMessage clientDCMessage)
        {
            return OperationInvoke.CallInMode(nameof(DCService), nameof(ExecuteCommandSL5)
                , () =>
                {
                    var dcManager = DCManager.GetAndCacheDCManagerByShortKey(clientDCMessage.TargetDCManager);
                    var communicationUnit = dcManager.CommunicationUnits[ScenarioKey];

                    //UnpackBody
                    clientDCMessage = communicationUnit.UnpackMessageIfNeeds(clientDCMessage);
                    clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);

                    dcManager.ExecuteCommandOnServer(ScenarioKey, ref clientDCMessage);

                    clientDCMessage.ClearInputParameters();
                    clientDCMessage.SetClientSendTime(DateTime.Now);

                    //PackToBody 
                    clientDCMessage = communicationUnit.PackMessageIfNeeds(clientDCMessage);

                    return clientDCMessage;
                }
                , "DETAILS - DCManager.Name[{0}] | Scenario[{1}] ".Fmt(clientDCMessage.TargetDCManager, ScenarioKey)
                , throwOnError: false // return default(IDCMessage)
                );

        }


        /// <summary>
        /// Processing Command Message on Server side.		
        /// <para/> Process DCMessage by your [DCManager]'s infrastructure - DCMessage should be processed by DCMessage.TargetProcessingManagerID .
        /// <para/> If client need to see if  result is successful /or not message, 
        ///          then add such message into DCMessage.ProcessingFaultMessage/ DCMessage.ProcessingSuccessMessage with DCMessage.ErrorCode if your system require.        
        /// <para/> On client full faulted exception message should be showned only in debug Mode/ in release  user should see only standart-phrase message. 	
        /// <para/> Clear all input parameters - to save traffic when it'll be transmitted back. 
        /// <para/> Set diagnostics timestamps on received and sending back to client points.
        /// <para/> Send back DCMessage to Client. 
        /// </summary>
        /// <param name="ClientCommand"></param>
        /// <returns></returns>
        public async Task<IDCMessage> ExecuteCommandAsync(IDCMessage clientDCMessage)
        {
            return OperationInvoke.CallInMode(nameof(DCService), nameof(ExecuteCommandAsync)
               , () =>
               {
                   var dcManager = DCManager.GetAndCacheDCManagerByShortKey(clientDCMessage.TargetDCManager);
                   var communicationUnit = dcManager.CommunicationUnits[ScenarioKey];

                   //UnpackBody
                   clientDCMessage = communicationUnit.UnpackMessageIfNeeds(clientDCMessage);
                   clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);

                   dcManager.ExecuteCommandOnServer(ScenarioKey, ref clientDCMessage);

                   clientDCMessage.ClearInputParameters();
                   clientDCMessage.SetClientSendTime(DateTime.Now);

                   //PackToBody 
                   clientDCMessage = communicationUnit.PackMessageIfNeeds(clientDCMessage);

                   return clientDCMessage;
               }
               , "DETAILS - DCManager.Name[{0}] | Scenario[{1}] ".Fmt(clientDCMessage.TargetDCManager, ScenarioKey)
               , throwOnError: false // return default(IDCMessage)
               );

        }

    }


    #endregion  --------------------  SERVICE CONTRACT IMPLEMENTOR --------------------------

}

#endif




#region -------------------------------------- GARBAGE ---------------------------------------

#region ------------------------- INSTANCE PROPERTIES ------------------------


/// <summary>
/// Key value - from [DCManager.Key] -is the same value. 
/// Key is the same for TSSerializer. 
/// </summary>
//public string Key
//{ get; set; }


/// <summary>
/// Manager which Published this DCService instance. 
/// </summary>
//public Type PublishedManager
//{ get; internal set; }


#endregion ------------------------- INSTANCE PROPERTIES ------------------------

#endregion -------------------------------------- GARBAGE ---------------------------------------
