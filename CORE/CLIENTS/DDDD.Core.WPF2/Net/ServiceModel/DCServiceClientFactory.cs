﻿#if CLIENT


using DDDD.Core.HttpRoutes;
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.ComponentModel;
using DDDD.Core.Threading;
using DDDD.Core.ComponentModel.Messaging;

namespace DDDD.Core.Net.ServiceModel
{

    /// <summary>
    /// Factory that produce WCF Clients(that use IDCService contract), and is responsible for string.Wcf_EnvironmentActivationSvc Unit TYPE. 
    /// </summary>
    [DCCUnitClientFactory( nameof(InitFactoryDefaultConfiguration), DCCScenarios.Wcf_EnvironmentActivationSvc )]
    public class DCServiceClientFactory : IDCCUnitClientFactory//<DCServiceClient>
    {



#region -------------------------------- SINGLETON -------------------------------------
        
        public DCServiceClientFactory()
        {
        }
        

        static LazyAct<DCServiceClientFactory> LA_CurrentFactory =
               LazyAct<DCServiceClientFactory>.Create(
                   (args) => { return new DCServiceClientFactory(); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of DCWebApiControllerFactory  
        /// </summary>
        public static DCServiceClientFactory Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


#endregion -------------------------------- SINGLETON -------------------------------------



#region ------------------------------------- FIELDS ----------------------------------------

        private static readonly object _locker = new object();

#endregion ------------------------------------- FIELDS ----------------------------------------


#region ---------------------------- INITIALIZE ----------------------------
        
        private static void InitFactoryDefaultConfiguration()
        {
            // Add Global Known Data Contracts
            DCCUnit.AddKnowDataContracts(typeof(DCMessage));


        }

#endregion ---------------------------- INITIALIZE ----------------------------

       
       
 


        public Type BaseProductType
        { get; } = typeof(DCServiceClient);



        public List<Type> ProductTypes
        { get; } = new List<Type>() { typeof(DCServiceClient) };


        /// <summary>
        /// CommunicationKeys means Scenarios[technology+configuration],  where this Factory Client can be used.
        /// Client-side  Factories Client items can be used in several Scenarious, but Server-side factories Clients can't-they means only one communication technology service.
        /// </summary>
        public string[] ScenarioKeys
        { get; } = {DCCScenarios.Wcf_EnvironmentActivationSvc};

#region ---------------------- FACTORY PRODUCED SERVICE CLIENTS -----------------------------------

        /// <summary>
        /// Produced by current Factory Service Clients. Each of the Services use DCService(on th SERVER) and DCServiceClient(on CLIENT). 
        /// Thats the [Dictionary of- {Type-ownerType(some manager), DCServiceClient - service client, that use IDCService service contract}]. 
        /// </summary>        
        static readonly Dictionary<Type, IDCCUnitClient> ServiceClients
               = new Dictionary<Type, IDCCUnitClient>();


#endregion ---------------------- FACTORY PRODUCED SERVICE CLIENTS -----------------------------------


#region ----------------------------------- CREATE CUSTOM BINDING ------------------------------


#if SERVER || CLIENT && (WPF || SL5)  // All Port except WP81


        /// <summary>
        /// Default CustomBinding for WCF Service.
        /// By default all DCService-s USE this CustomBinding on SERVER and CLIENT side  for Service and Client.
        /// </summary>
        public static Binding DCServiceCustomBindingDefault
        { get; } = CreateCustomBinding(true, TransferMode.StreamedResponse, sendTimeoutMins: 1, sendTimeoutSecs: 30
                                            , receiveTimeoutMins: 1, receiveTimeoutSecs: 30);

       

        /// <summary>
        /// Creating Custom Binding for WCF DCServiceClient service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="transferMode"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , TransferMode transferMode = TransferMode.StreamedResponse
                                                 , int maxBufferSize = int.MaxValue, int maxReceivedMessageSize = int.MaxValue
                                                , int sendTimeoutMins = 0, int sendTimeoutSecs = 30
                                                , int receiveTimeoutMins = 0, int receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,

            });

            CustomBinding binding = new CustomBinding(elements);

            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);//             

            return binding;
        }

#elif CLIENT && WP81
        
        // Wp81 -Has not TransferMode parameter configuration
        
        /// <summary>
        /// Default CustomBinding for WCF Service.
        /// By default all DCService-s USE this CustomBinding on SERVER and CLIENT side  for Service and Client.
        /// </summary>
        public static Binding DCServiceCustomBindingDefault
        { get; } = CreateCustomBinding(true,  sendTimeoutMins: 1, sendTimeoutSecs: 30
                                            , receiveTimeoutMins: 1, receiveTimeoutSecs: 30);




        /// <summary>
        /// Creating Custom Binding for WCF DCServiceClient service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , Int32 maxBufferSize = Int32.MaxValue, Int32 maxReceivedMessageSize = Int32.MaxValue
                                                , Int32 sendTimeoutMins = 0, Int32 sendTimeoutSecs = 30
                                                , Int32 receiveTimeoutMins = 0, Int32 receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                //TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,
            });


            CustomBinding binding = new CustomBinding(elements);


            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);// 
            return binding;
        }
      


#endif


#endregion ----------------------------------- CREATE CUSTOM BINDING ------------------------------


#region --------------------------------- ADD REMOVE CLINETS ---------------------------------


#if CLIENT && (SL5 || WP81)



        public IDCCUnitClient AddOrUseExistClient(DCCUnit communicationUnit)
        {
            return AddOrUseExistClientInternal(communicationUnit);
        }

        /// <summary>
        /// Add new  WCF DCServiceClient service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        protected internal static IDCCUnitClient AddOrUseExistClientInternal( DCCUnit communicationUnit )
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;


            //return if client already exist
            if (  ServiceClients.ContainsKey(communicationUnit.OwnerType) ) return ServiceClients[communicationUnit.OwnerType]; //NetConfiguration 

            //CF- is Channel Factory 
            if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
            {
                lock (_locker)
                {
                    if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
                    {
                        var virtualContractDesc = new ContractDescription(communicationUnit.OwnerName,DCSConsts.ServiceNamespaceDefault);
                        
                        EndpointAddress endPoint = new EndpointAddress(communicationUnit.UriResultAddress);

                        var newDynamicCommandClient = targetHandlerType.CreateInstanceTBase<IDCCUnitClient>(
                            args: new object[] {
                            communicationUnit.OwnerType
                            , virtualContractDesc
                            , endPoint
                            , DCServiceCustomBindingDefault   
                            , communicationUnit.RegistrationInfo.SerializationMode                         
                            } );
                                            
                              

                        ServiceClients.Add(communicationUnit.OwnerType, newDynamicCommandClient);
                                                     
                        return ServiceClients[communicationUnit.OwnerType];
                    }
                }
            }

            return ServiceClients[communicationUnit.OwnerType];
        }


        


#elif CLIENT && WPF


        public IDCCUnitClient AddOrUseExistClient(DCCUnit communicationUnit)
        {
            return AddOrUseExistClientInternal(communicationUnit);
        }

        /// <summary>
        ///  Add new  WCF DCServiceClient service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        protected internal static IDCCUnitClient AddOrUseExistClientInternal(DCCUnit communicationUnit)
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;


            //return if client already exist
            if (ServiceClients.ContainsKey(communicationUnit.OwnerType)) return ServiceClients[communicationUnit.OwnerType]; //NetConfiguration 

            //CF- is Channel Factory 
            if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
            {
                lock (_locker)
                {
                    if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
                    {
                        var virtualContractDesc = new ContractDescription(communicationUnit.OwnerName, DCSConsts.ServiceNamespaceDefault);

                        EndpointAddress endPoint = new EndpointAddress(communicationUnit.UriResultAddress);

                        var newDynamicCommandClient = targetHandlerType.CreateInstanceTBase<DCServiceClient>( 
                            args: new object[] {  communicationUnit.OwnerType
                            , virtualContractDesc
                            , endPoint
                            , DCServiceCustomBindingDefault
                            }
                            );

                        ServiceClients.Add(communicationUnit.OwnerType, newDynamicCommandClient);

                        return ServiceClients[communicationUnit.OwnerType];
                    }
                }
            }

            return ServiceClients[communicationUnit.OwnerType];
        }




#endif





        /// <summary>
        /// Remove  Service client from Hub by its service name 
        /// </summary>
        /// <param name="ServiceName">Name that you use on Creating this service</param>
        public static void RemoveServiceClient(Type ownerType)
        {
            if (ownerType == null) return;

            if (!ServiceClients.ContainsKey(ownerType)) return;

            lock (_locker)
            {
                if (ServiceClients.ContainsKey(ownerType))
                    ServiceClients.Remove(ownerType);
            }

        }


#endregion --------------------------------- ADD REMOVE CLINETS -------------------------



        /// <summary>
        /// Param Key const - used to pack some CommunicationUnit istance as parameter.
        /// </summary>
        public const string communicationUnit = nameof(communicationUnit);


      

        /// <summary>
        /// Create Client - return  DCServiceClient boxed into object as result.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public IDCCUnitClient CreateClient(DCCUnit communicationUnit)
        {
            if (communicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

            return AddOrUseExistClientInternal(communicationUnit);
        }

        /// <summary>
        /// IFactory way to Create Client - return  DCServiceClient boxed into object as result. 
        /// As the input parameters we wait parameter of  DCCUnit instance with the Key==['communicationUnit'].
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object CreateItem(params KeyValuePair<string, object>[] parameters)
        {          
            var paramCommunicationUnit =  parameters.Where(kvp=>kvp.Key== communicationUnit).FirstOrDefault();
            if (paramCommunicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");
            
            return AddOrUseExistClient(paramCommunicationUnit.Value as DCCUnit);
        }

    }

}


#endif


#region -------------------------------------- GARBAGE ------------------------------------------

///// <summary>
///// Create Client - return  DCServiceClient instance ,not object as result.
///// </summary>
///// <param name="communicationUnit"></param>
///// <returns></returns>
//public IDCCUnitClient CreateClientT(DCCUnit communicationUnit)
//{
//   if (communicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

//    return AddOrUseExistClientInternal(communicationUnit);
//}


//public T CreateItemT<T>(params KeyValuePair<string, object>[] parameters)
//{
//    var paramCommunicationUnit = parameters.Where(kvp => kvp.Key == communicationUnit).FirstOrDefault();
//    if (paramCommunicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

//    return (T)AddOrUseExistClient(paramCommunicationUnit.Value as DCCUnit);
//}

//Type ownerType,                                                
//Binding serviceBinding,
//ClientCredentials clentCredentials,
//bool useTSSSerialization = false,
//List<Type> knownTypes = null,
//HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlCommunicationServiceSvc,
//string ServiceFileName = null, string Server = null, string Port = null,string Communication = null, string WebApplication = null, string Subfolder = null, string ActionName = null


///// <summary>
///// Add new  WCF DCServiceClient service into internal static Dictionary.
///// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
///// <para/>  Also you can get this client next time by its ServiceContract.Name of SvcContract type as KEY. 
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>
///// <param name="useTSSSerialization"></param>
///// <param name="useTSSDirectSerialization"></param>
///// <param name="KnownTypesLoaderFunc"></param>
///// <returns></returns>
//public static DCServiceClient AddOrUseExistClient(Type SvcContract,
//                                    String endpointConfigurationName,
//                                    bool useTSSSerialization = false,
//                                    bool useTSSDirectSerialization = false,
//                                    Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                  )
//{
//    //CheckIsSvcContract(SvcContract);

//    var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
//                                 .FirstOrDefault() as ServiceContractAttribute;
//    string ServiceKey = svccontract.Name;

//    //return if client already exist
//    if (ServiceClients.ContainsKey(ServiceKey)) return ServiceClients[ServiceKey];

//    //if not exist check for methods ExecuteCommand
//    //CheckIsSvcContractSupportDynamicCommands(SvcContract);

//    if (!ServiceClients.ContainsKey(ServiceKey))
//    {
//        lock (_locker)
//        {
//            if (!ServiceClients.ContainsKey(ServiceKey))
//            {

//                ServiceClients.Add(ServiceKey,
//                                                null                    
//                                                //new DCServiceClient(
//                                                //      SvcContract
//                                                //     , endpointConfigurationName
//                                                //     , useTSSSerialization               //2 mode seriaization
//                                                //     , useTSSDirectSerialization        //3 mode seriaization                                                             
//                                                //     , KnownTypesLoaderFunc //load now KnownTypes 
//                                                //     )
//                                                     );

//                return ServiceClients[ServiceKey];
//            }
//        }
//    }

//    return ServiceClients[ServiceKey];

//}




///// <summary>
///// Add new  WCF DCServiceClient service into internal static Dictionary.
///// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
///// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>
///// <param name="useTSSSerialization"></param>
///// <param name="KnownTypesLoaderFunc"></param>
///// <returns></returns>
//public static DCServiceClient AddOrUseExistSvcClient(Type SvcContract,
//                                    String endpointConfigurationName, 
//                                    bool useTSSSerialization = false,
//                                    Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                  )
//{
//    //CheckIsSvcContract(SvcContract);

//    var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
//                                 .FirstOrDefault() as ServiceContractAttribute;
//    string ServiceKey = svccontract.Name;

//    //return if client already exist
//    if (ServiceClients.ContainsKey(ServiceKey)) return ServiceClients[ServiceKey];

//    //if not exist check for methods ExecuteCommand
//    //CheckIsSvcContractSupportDynamicCommands(SvcContract);

//    if (!ServiceClients.ContainsKey(ServiceKey))
//    {
//        lock (_locker)
//        {
//            if (!ServiceClients.ContainsKey(ServiceKey))
//            {

//                ServiceClients.Add(ServiceKey,
//                                                     null
//                                                     //new DCServiceClient(
//                                                     //SvcContract
//                                                     //, endpointConfigurationName
//                                                     //, useTSSSerialization                 // useTSSSerializer on dataTransmission
//                                                     //, KnownTypesLoaderFunc //load now KnownTypes 
//                                                     //)
//                                                     );

//                return ServiceClients[ServiceKey];
//            }
//        }
//    }

//    return ServiceClients[ServiceKey];

//}


#endregion -------------------------------------- GARBAGE ------------------------------------------

