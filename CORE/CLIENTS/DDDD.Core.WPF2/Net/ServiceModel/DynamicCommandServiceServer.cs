﻿
#if SERVER && IIS

using System;
using System.Configuration;
using System.IO;
using System.ServiceModel.Configuration;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace DDDD.Core.Net.ServiceModel
{



    
    // 3 Task(OPTIONAL) - you shold choose - Use  *.svc Markup file or only Web.config to register-activate new service:
    //   - If you  want to use *.svc Markup file  registration you'll be able to use MVC COntrollers and Svc services together
    //   - If you want to use use only Web.config changing you may get errors because MVCController Handlers already making their routing registration
    //     	so you'll need to comment   RouteConfig.RegisterRoutes(RouteTable.Routes);   in your global-asax.cs in WebApplication.Application_Start()
    // 
    //  If you decide to use *.svc Markup file   Add Service Markup File to Web Application Project:  $ModuleName$.svc with the next code inside
    // <%@ ServiceHost Language="C#" Debug="true" Service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" %>
    //
    //
    // WEB.CONFIG  CHANGE STEPS 
    //
    // 4 Task- Add this Configuration block To the Web.Config in the Web Application 
    // ----  Configuration  BLOCK :  Here we beleave that we already have defined Behaviour and Binding for all Services earlier;
    // ----  Uncomment from the next Line and Add this Service Config tag to the -   Web.Config to <system.serviceModel>.<services>.<service ..  from next Line:
    // 
    // 
    // ADD TSS BEHAVIOR EXTENSION - adding TSS serialization as ServiceModel Behavior Extension     
    /*  
       <system.serviceModel>
       <extensions>      
          <behaviorExtensions>
            <add name="ddddtss"
                 type="DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69"/>
          </behaviorExtensions>      
        </extensions>
        <system.serviceModel> 
    */
    // 5 Task- So here you should to ensure in  type value-	DDDD.Core.Net.ServiceModel.TSSBehaviorExtension- get and it's Type.AssemblyQualifiedName.
    //  For this task you can use Test from  or you can use Threading_BlockTests.Test_BCL_Reflection_GetTSSServiceModelExtensionFullName_Tests() from DDDD.Core.Test.Net45 project 
    // or with console tool
    //
    //	
    // NEXT STEP: ADD BEHAVIOR CONFIG: - shared custom binding  that will be used by Service configuration
    /* 
     <system.serviceModel>
        <behaviors>

        <endpointBehaviors>
         <behavior name="DDDD.ServiceModel.TSS.EndpointBehavior">
          <ddddtss/>
         </behavior>
        </endpointBehaviors>	

        <serviceBehaviors>	   
          <behavior name="$Subsystem$.Communication.Behaviour">
          <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
          <serviceDebug includeExceptionDetailInFaults="true" />
          <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
          </behavior>
        </serviceBehaviors>

        </behaviors>
    <system.serviceModel> 
    */
    //
    //
    //NEXT STEP:  ADD BINDING CONFIG: - shared custom binding  that will be used by Service configuration
    /*
     <system.serviceModel>
       <bindings>
            <customBinding>
                <binding name="$Subsystem$.Communication.customBinding"  receiveTimeout="00:00:30" >
                <binaryMessageEncoding />
                <httpTransport  transferMode="StreamedResponse" />          
                </binding>
            </customBinding>
        </bindings>
    <system.serviceModel>
     */
    //
    //
    //NEXT STEP:  ADD SERVICE CONFIG: 
    /*  - Usual Service - Without using TSS
     <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
      <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                            contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" />
      <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
     </service>
    */
    // OR
    /*  Service as Below - Service that is going to use TSS serialization
     <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
      <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                            contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" 
                            behaviorConfiguration="TSS.ServiceModel.EndpointBehavior" />						
      <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
     </service>
    */
    //
    //5 (OPTIONAL) If you've decided to use  web.config only registration and activation(instead of *.svc Markup file)  
    //    you'll need to expand form of serviceHostingEnvironment section(like below) and add line with current service activation :
    /*
     <serviceHostingEnvironment aspNetCompatibilityEnabled="true"   multipleSiteBindingsEnabled="true" >
         <serviceActivations>
           <!--YOUR SERVICE ACTIVATION LINE-->
           <add relativeAddress="~/Services/$ModuleName$Service.svc" service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" />                  
         </serviceActivations>
     </serviceHostingEnvironment>
    */
    // Here end test address is - http://localhost/DDDD.Core.Test.Web/Services/ClientReportService.svc
    // Here we have folder in form like-[Project\SubFolder\] -  [DDDD.Core.Test.Web\Services\]   physical dirs . Full path to project example -D:\DDDD\BCL\DDDD.Core.Test.Web\
    // And Here [DDDD.Core.Test.Web] dir also set as local IIS WebApplication 
    // STEPS FINISHED







    public class DynamicCommandServiceServer
    {

    #region ------------------------------ ADD TSS Extension -----------------------------------
   /*  
    <system.serviceModel>
    <extensions>      
      <behaviorExtensions>
        <add name="ddddtss"
             type="DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69"/>
      </behaviorExtensions>      
    </extensions>
	<system.serviceModel> 
   */

        public static void TryAddTSSBehaviorExtension()
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);
                //  <add name="ddddtss"
                //type = "DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69" />
                if (!wServiceSection.Extensions.BehaviorExtensions.ContainsKey("ddddtss"))
                {
                    var extTss = new ExtensionElement("ddddtss", typeof(TSSBehaviorExtension).AssemblyQualifiedName);
                    wServiceSection.Extensions.BehaviorExtensions.Add(extTss);
                }

            }
            catch (System.Exception exc)
            { throw exc;
            }
        }

    #endregion ------------------------------ ADD TSS Extension -----------------------------------


    #region ------------------------------- ADD BEHAVIORS --------------------------------
        // NEXT STEP: ADD BEHAVIOR CONFIG: - shared custom binding  that will be used by Service configuration
        /* 
         <system.serviceModel>
            <behaviors>

            <endpointBehaviors>
             <behavior name="DDDD.ServiceModel.TSS.EndpointBehavior">
              <ddddtss/>
             </behavior>
            </endpointBehaviors>	

            <serviceBehaviors>	   
              <behavior name="$Subsystem$.Communication.Behaviour">
              <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
              <serviceDebug includeExceptionDetailInFaults="true" />
              <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
              </behavior>
            </serviceBehaviors>

            </behaviors>
        <system.serviceModel> 
        */



        /*
          <endpointBehaviors>
          <behavior name = "DDDD.ServiceModel.TSS.EndpointBehavior" >
           < ddddtss />
          </ behavior >
         </ endpointBehaviors >
     */

        public static void TryAddTSSEndpointBehavior()
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);

                var endpointBhvrKey = "DDDD.ServiceModel.TSS.EndpointBehavior";
                var ddddtssExtension = (BehaviorExtensionElement)(wServiceSection.Extensions.BehaviorExtensions["ddddtss"] as object);
                EndpointBehaviorElement eBhvrEl = new EndpointBehaviorElement(endpointBhvrKey);
                eBhvrEl.Add(ddddtssExtension);

                if (!wServiceSection.Behaviors.EndpointBehaviors.ContainsKey(endpointBhvrKey))
                {
                    var extTss = new ExtensionElement("ddddtss", typeof(TSSBehaviorExtension).AssemblyQualifiedName);
                    wServiceSection.Extensions.BehaviorExtensions.Add(extTss);
                }

            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }





        /*
          <serviceBehaviors>	   
              <behavior name="$Subsystem$.Communication.Behaviour">
              <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
              <serviceDebug includeExceptionDetailInFaults="true" />
              <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
              </behavior>
          </serviceBehaviors>
        */


        public static void TryAddServiceBehavior(string serviceBehavName, short maxConcurrentCalls = 4, short maxConcurrentInstancies = 4, short maxConcurrentSessions = 4
                                                  , bool includeExceptionDetailsOnFaults = true
                                                 , bool httpGetEnabled = true, bool httpsGetEnabled = false
                                                )
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);

                /*
                <behavior name="$Subsystem$.Communication.Behaviour">
                <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
                <serviceDebug includeExceptionDetailInFaults="true" />
                <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
                </behavior>
                */

                if (!wServiceSection.Behaviors.ServiceBehaviors.ContainsKey(serviceBehavName))
                {
                    ServiceBehaviorElement svcBhvrEl = new ServiceBehaviorElement(serviceBehavName);

                    var servThrottling = new ServiceThrottlingElement()
                    {
                        MaxConcurrentCalls = maxConcurrentCalls,
                        MaxConcurrentInstances = maxConcurrentInstancies,
                        MaxConcurrentSessions = maxConcurrentSessions
                    };
                    svcBhvrEl.Add(servThrottling);


                    var serviceDebug = new ServiceDebugElement()
                    { IncludeExceptionDetailInFaults = includeExceptionDetailsOnFaults };
                    svcBhvrEl.Add(serviceDebug);


                    var serviceMetaData = new ServiceMetadataPublishingElement()
                    { HttpGetEnabled = httpGetEnabled, HttpsGetEnabled = httpsGetEnabled };
                    svcBhvrEl.Add(serviceMetaData);


                    wServiceSection.Behaviors.ServiceBehaviors.Add(svcBhvrEl);
                }

            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }







    #endregion ------------------------------- ADD BEHAVIORS --------------------------------




    #region ------------------------------- ADD BINDINGS -------------------------------------
        /*
         <system.serviceModel>
           <bindings>
                <customBinding>
                    <binding name="$Subsystem$.Communication.customBinding"  receiveTimeout="00:00:30" >
                    <binaryMessageEncoding />
                    <httpTransport  transferMode="StreamedResponse" />          
                    </binding>
                </customBinding>
            </bindings>
        <system.serviceModel>
        */


        
        public static void TryAddCustomBinding(string custBindingKey, TimeSpan  receiveTimeout  )
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);
            
                
                if (!wServiceSection.Bindings.CustomBinding.Bindings.ContainsKey(custBindingKey) )
                {

                    CustomBindingElement custBindngEl = new CustomBindingElement(custBindingKey);
                    custBindngEl.ReceiveTimeout = receiveTimeout;

                    custBindngEl.Add(new BinaryMessageEncodingElement());
                    custBindngEl.Add(new HttpTransportElement() { TransferMode = System.ServiceModel.TransferMode.StreamedResponse });
                    wServiceSection.Bindings.CustomBinding.Bindings.Add(custBindngEl);

                    // OR
                    //var bindElement = new  StandardBindingElement(custBindingKey);
                    //bidingMessageEncoding
                    //httpTransport
                }

            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }

    #endregion  ------------------------------- ADD BINDINGS -------------------------------------

        

    #region ---------------------------------- ADD SERVICE ------------------------------------



        //NEXT STEP:  ADD SERVICE CONFIG: 
        /*  - Usual Service - Without using TSS
         <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
          <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                                contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" />
          <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
         </service>
        */
        // OR
        /*  Service as Below - Service that is going to use TSS serialization
         <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
          <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                                contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" 
                                behaviorConfiguration="TSS.ServiceModel.EndpointBehavior" />						
          <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
         </service>
        */


        /// <summary>
        /// Try Add Service into ServiceModel Configuration
        /// </summary>
        /// <param name="SubsystemName"></param>
        /// <param name="ModuleName"></param>
        public static void TryAddService(string SubsystemName, string ModuleName)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);
                var targetServiceKey = "";

                if (!wServiceSection.Services.Services.ContainsKey(targetServiceKey))
                {
                    var newServiceElement = new ServiceElement() {
                    Name = $"{SubsystemName}.Dom.Modules.{ModuleName}.I{ModuleName}Service",
                    BehaviorConfiguration = $"{SubsystemName}.Communication.Behavior"};

                    //var newEndpoint = EndpointAddressElementBase. () {  }

                    wServiceSection.Services.Services.Add(newServiceElement );


                }
            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }


    #endregion ---------------------------------- ADD SERVICE ------------------------------------



    #region ---------------------------------- ADD SERVICE  ACTIVATION------------------------------------
        //5 (OPTIONAL) If you've decided to use  web.config only registration and activation(instead of *.svc Markup file)  
        //    you'll need to expand form of serviceHostingEnvironment section(like below) and add line with current service activation :
        /*
         <serviceHostingEnvironment aspNetCompatibilityEnabled="true"   multipleSiteBindingsEnabled="true" >
             <serviceActivations>
               <!--YOUR SERVICE ACTIVATION LINE-->
               <add relativeAddress="~/Services/$ModuleName$Service.svc" service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" />                  
             </serviceActivations>
         </serviceHostingEnvironment>
        */



    #endregion ---------------------------------- ADD SERVICE  ACTIVATION------------------------------------



        //CREATE NEW DYNAMIC COmMAND SERVICE
        //- type builder
        //- configuration 
        //discover , add existed DCS



    }

}

#endif


