﻿#if SERVER && NET45  || WPF 


using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Reflection;
using DDDD.Core.Serialization;
using DDDD.Core.Serialization.Binary;
using DDDD.Core.Serialization.Xml;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace DDDD.Core.Net.ServiceModel
{


    /// <summary>
    /// Describes a WCF operation behaviour that can perform Type Set Serialization
    /// </summary>
    public sealed class BinaryOperationBehavior : DataContractSerializerOperationBehavior
    {

        /// <summary>
        /// Create a new  BinaryOperationBehavior instance
        /// </summary>
        public BinaryOperationBehavior(OperationDescription operation)
            : base(operation)
        {
            var ServiceContract = (ServiceContractAttribute)operation.DeclaringContract.ContractType.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault();

            var ServiceKey = ServiceContract.Name;

            //TypeAQName.Mapper.UseConventionsProcessing = true;
            //TypeAQName.Mapper.UseTypeFullNameOnlyCompareMapping = true;

        }
        
     
        /// <summary>
        /// Creates a BinarySerializer if possible (falling back to the default WCF serializer)
        /// </summary>
        public override XmlObjectSerializer CreateSerializer(Type type, System.Xml.XmlDictionaryString name, System.Xml.XmlDictionaryString ns, IList<Type> knownTypes)
        {            
            return XmlBinSerializer.TryCreate(type) ?? base.CreateSerializer(type, name, ns, knownTypes);            
        }
    }
}

#endif
