﻿

#if SERVER && NET45  || WPF
using System;
using System.ServiceModel.Configuration;

namespace DDDD.Core.Net.ServiceModel
{
    /// <summary>
    /// Configuration element to swap out DatatContractSerilaizer with the TypeSetSerializer for a given endpoint.
    /// </summary>
    /// <seealso cref="TSSEndpointBehavior"/>
    public class TSSBehaviorExtension : BehaviorExtensionElement
    {
        /// <summary>
        /// Creates a new TSSBehaviorExtension instance.
        /// </summary>
        public TSSBehaviorExtension()
        {
        }

        /// <summary>
        /// Gets the type of behavior.
        /// </summary>     
        public override Type BehaviorType
        {
            get
            {
                return typeof(TSSEndpointBehavior);
            }
        }

        /// <summary>
        /// Creates a behavior extension based on the current configuration settings.
        /// </summary>
        /// <returns>The behavior extension.</returns>
        protected override object CreateBehavior()
        {
            return new TSSEndpointBehavior();
        }
    }
}

#endif