﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Net.ServiceModel
{
    /// <summary>
    /// Dynamic Command Services Constans
    /// </summary>
    internal static class DCSConsts
    {
        public const string LoadKnownContractsMethodName = "LoadKnownContracts";
        public const string DSOperation_ExecuteCommand = "ExecuteCommand";
        public const string Server_Default_Error_Message = "Server error happened.";

        public const string Method_ExecuteCommandResponse = "ExecuteCommandResponse";
        public const string Method_ExecuteCommandAsyncResponse = "ExecuteCommandAsyncResponse";
        public const string Method_ExecuteCommand = "ExecuteCommand";
        public const string Method_ExecuteCommandAsync = "ExecuteCommandAsync";

        public const string Method_BeginExecuteCommand = "BeginExecuteCommand";
        public const string Method_EndExecuteCommand = "EndExecuteCommand";


        
        //public const string ExecuteCommandMethod = "ExecuteCommand";
        //public const string ExecuteCommandResponseMethod = nameof(ExecuteCommandResponseMethod);
        public const string ServiceNameDefault = nameof(IDCService);// 
        public const string ServiceNamespaceDefault = "http://tempuri.org";
        public const string ActionUri = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommand;
        public const string ReplyActionUri = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommandResponse;


        public const string ActionSL5Uri = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommand+"SL5";
        public const string ReplyActionSL5Uri = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommand + "SL5Response";



        public const string ActionUriAsync = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommandAsync;
        public const string ReplyActionUriAsync = ServiceNamespaceDefault + "/" + ServiceNameDefault + "/" + Method_ExecuteCommandAsyncResponse;

    }
}
