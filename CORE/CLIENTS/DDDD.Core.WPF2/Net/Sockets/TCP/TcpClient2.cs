﻿using System;
using SN = System.Net.Sockets;
using System.Threading;
using System.Net;
using System.Threading.Tasks;

//using RaptorDB.Common;


namespace DDDD.Core.Net.Sockets
{
    //
    // Header bits format : 0 - json = 1 , bin = 0 
    //                      1 - binaryjson = 1 , text json = 0
    //                      2 - compressed = 1 , uncompressed = 0 
    //
    //     0   : data format
    //     1-4 : data length

    /// <summary>
    /// Simplifying TCP Client realisation.
    /// </summary>
    public class TcpClient2
    {
        internal static class Config
        {
            public static int BufferSize = 32 * 1024;
            public static int LogDataSizesOver = 1000000;
            public static int CompressDataOver = 1000000;
        }

        public TcpClient2(string server, int port)
        {
            _server = server;
            _port = port;
        }
        private  SN.TcpClient _client;
        private string _server;
        private int _port;

        public Guid ClientID = Guid.NewGuid();
        public bool UseBJSON = true;

        public void Connect()
        {
            _client = new SN.TcpClient(_server, _port);
            _client.SendBufferSize = Config.BufferSize;
            _client.ReceiveBufferSize = _client.SendBufferSize;
        }

        public object Send(object data)
        {
            CheckConnection();

            byte[] hdr = new byte[5];
            hdr[0] = (UseBJSON ? (byte)3 : (byte)0);
            byte[] dat = fastBinaryJSON.BJSON.ToBJSON(data);
            byte[] len = Helper.GetBytes(dat.Length, false);
            Array.Copy(len, 0, hdr, 1, 4);
            _client.Client.Send(hdr);
            _client.Client.Send(dat);

            byte[] rechdr = new byte[5];
            using (SN.NetworkStream n = new SN.NetworkStream(_client.Client))
            {
                n.Read(rechdr, 0, 5);
                int c = Helper.ToInt32(rechdr, 1);
                byte[] recd = new byte[c];
                int bytesRead = 0;
                int chunksize = 1;
                while (bytesRead < c && chunksize > 0)
                    bytesRead +=
                      chunksize = n.Read
                        (recd, bytesRead, c - bytesRead);
                if ((rechdr[0] & (byte)4) == (byte)4)
                    recd = RaptorDB.MiniLZO.Decompress(recd);
                if ((rechdr[0] & (byte)3) == (byte)3)
                    return fastBinaryJSON.BJSON.ToObject(recd);
            }
            return null;
        }

        private void CheckConnection()
        {
            // check connected state before sending

            if (_client == null || !_client.Connected)
                Connect();
        }

        public void Close()
        {
            if (_client != null)
                _client.Close();
        }
    }

 
}
