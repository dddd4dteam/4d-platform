﻿#if SERVER && IIS


using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Resources;
using DDDD.Core.Extensions;
using NLog;
using DDDD.Core.Diagnostics;
using System.Net.Http.Headers;
using System.Collections.Generic;

namespace DDDD.Core.Net.WebApi
{


    /// <summary>
    /// DC (Dynamic Command) Web API Direct Handler represents direct Route Handler, that can be used as custom Http Service.
    /// <para/> FEATUERS:
    ///         - Add HttpRequestMessage to DCMessage params with HttpMessage.
    ///         - Auto clean all input Parameters after server Action will be done.
    ///         - Works wih "application/json" // "application/octet-stream" - using GetDCManagerByName/ GetDCManagerByID
    ///         - Non DCMessage request  -  we create empty DCMessage and sending it to client already
    ///         - Logging Points: If you throw some exception into WebApi then IExceptionService will Log, also DCCUnit usually logging all serialization/deserialization errors directly.
    /// </summary>
    public class DCWebApiDirectHandler :  HttpMessageHandler , IDCCUnitServer
                                        // IDisposable, DelegatingHandler
    {

        #region ------------------------------ CTOR -------------------------------

        public DCWebApiDirectHandler(DCCUnit communicationUnit)
        {
            CommunicationUnit = communicationUnit;
        }


        #endregion ------------------------------ CTOR -------------------------------

        #region -------------------------- FIELDS ------------------------------------

        const string application_json = @"application/json";
        const string application_octetStream = @"application/octet-stream";
        const string text_xml = @"text/xml";
        const string text_csv = @"text/csv";
        const string text_plain = @"text/plain";

        static List<string> KnownContentTypes = new List<string>()
        {
            application_json , application_octetStream ,
            text_xml, text_csv, text_plain
        };


        static protected readonly MediaTypeHeaderValue ContentType_Json = new MediaTypeHeaderValue(application_json);
        static protected readonly MediaTypeHeaderValue ContentType_OctetStream = new MediaTypeHeaderValue(application_octetStream);
        static protected readonly MediaTypeHeaderValue ContentType_TextXml = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextCsv = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextPlain = new MediaTypeHeaderValue(text_xml);
         
        static protected readonly MediaTypeWithQualityHeaderValue JsonAccept = new MediaTypeWithQualityHeaderValue(application_json);        
        static protected readonly MediaTypeWithQualityHeaderValue OctetAccept = new MediaTypeWithQualityHeaderValue(application_octetStream);

        #endregion -------------------------- FIELDS ------------------------------------

        #region ------------------------------- PROPERTIES --------------------------------
        /// <summary>
        /// DC Scenario Key
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios.WebApiDirectHandlers;


        /// <summary>
        /// Reference to DCCUnit by which this DCWebApiDirectHandler was builded. 
        /// Contains serialization settings+handlers, OwnerInfo- class that order this communication instance - DCWebApiDirectHandler instance. 
        /// </summary>
        public DCCUnit CommunicationUnit
        { get; private set; }

        #endregion ------------------------------- PROPERTIES --------------------------------



        /// <summary>
        /// Implemented by Default - the logic of Http.Post method's HttpRequestMessage handling. 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancelationToken"></param>
        /// <returns></returns>
        protected virtual async Task<HttpResponseMessage> PostAsync(HttpRequestMessage request, CancellationToken cancelationToken)
        {

            // EXECUTING COMMAND Exception handling Should be placed inside custom DCManager class                                    
            if ( KnownContentTypes.NotContains(request.Content.Headers.ContentType.MediaType) )
            {
                throw new NotSupportedException("[{0}] supports only ContentType with Json/Xml/Csv and Octet request headers"
                                                 .Fmt(nameof(DCWebApiDirectHandler)));
                
            }

            return await PostInernalAsync(request, cancelationToken);

        }


        /// <summary>
        /// POST Text HttpRequestMessage
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancelationToken"></param>
        /// <returns></returns>
        protected virtual async Task<HttpResponseMessage> PostInernalAsync(HttpRequestMessage request, CancellationToken cancelationToken)
        {

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            IDCMessage clientDCMessage = CommunicationUnit.CreateEmptyMessage();// DCMessage2.Empty;


            //UNPACKING frem Request           
            clientDCMessage = await CommunicationUnit.DeserializeFromRequest(request, typeof(DCMessage2)) as DCMessage2;
            if (clientDCMessage.HasServerErrorHappened)
            {
                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
                return response;
            }
            //  Deserialize Parameters if Needable                        
            clientDCMessage = CommunicationUnit.UnpackParametersIfNeeds(clientDCMessage);
            if (clientDCMessage.HasServerErrorHappened)
            {
                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
                return response;
            }

            clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);

            //Adding HttpMessage as Parameter - if logic based on HttpRequestMessage
            clientDCMessage.NewInParam(nameof(HttpRequestMessage), request);

            // EXECUTING COMMAND Exception handling Should be placed inside custom DCManager class
            DCManager.GetAndCacheDCManagerByShortKey(clientDCMessage.TargetDCManager)
                     .ExecuteCommandOnServer(ScenarioKey, ref clientDCMessage);

            clientDCMessage.ClearInputParameters();
            clientDCMessage.SetServerSendToClientTime(DateTime.Now);
            
            //PACKING
            // Serialize Parameters if Needable
            clientDCMessage = CommunicationUnit.PackParametersIfNeeds(clientDCMessage);
            if (clientDCMessage.HasServerErrorHappened)
            {
                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
                return response;
            }

            response = await CommunicationUnit.SerializeToResponse(response, clientDCMessage);
            if (clientDCMessage.HasServerErrorHappened)
            {
                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
                return response;
            }

            return response;
             
        }






        //  Get, Head ,Delete , Put , Options
        protected virtual async Task<HttpResponseMessage> GetAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("[GET] http method Is not implemented by default by  DCWebApiDirectHandler. You can implement it by your custom derived from DCWebApiDirectHandler class.");
        }


        protected virtual async Task<HttpResponseMessage> HeadAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("[HEAD] http method Is not implemented by default by  DCWebApiDirectHandler. You can implement it by your custom derived from DCWebApiDirectHandler class.");
        }



        protected virtual async Task<HttpResponseMessage> DeleteAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("[DELETE] http method Is not implemented by default by  DCWebApiDirectHandler. You can implement it by your custom derived from DCWebApiDirectHandler class.");
        }

        protected virtual async Task<HttpResponseMessage> PutAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("[PUT] http method Is not implemented by default by  DCWebApiDirectHandler. You can implement it by your custom derived from DCWebApiDirectHandler class.");
        }


        protected virtual async Task<HttpResponseMessage> OptionsAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            throw new NotImplementedException("[OPTIONS] http method Is not implemented by default by  DCWebApiDirectHandler. You can implement it by your custom derived from DCWebApiDirectHandler class.");
        }


        /// <summary>
        /// Http Web API Route Handler method. 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {

            if ( request.Method == HttpMethod.Post )
            {
                return await PostAsync(request, cancellationToken);
            }
            else if(request.Method == HttpMethod.Get)  //  Get, Head ,Delete , Put , Options
            {
                return await GetAsync(request, cancellationToken);
            }
            else if (request.Method == HttpMethod.Head)
            {
                return await HeadAsync(request, cancellationToken);
            }
            else if (request.Method == HttpMethod.Delete)
            {
                return await DeleteAsync(request, cancellationToken);
            }
            else if (request.Method == HttpMethod.Put)
            {
                return await PutAsync(request, cancellationToken);
            }
            else if (request.Method == HttpMethod.Options)
            {
                return await OptionsAsync(request, cancellationToken);
            }
            

            throw new NotSupportedException("It's not supported  Http Method");

        }

        
    }

}


#endif




#region ---------------------------------- GARBAGE ------------------------------------


//        /// <summary>
//        /// POST Binary HttpRequestMessage
//        /// </summary>
//        /// <param name="request"></param>
//        /// <param name="cancelationToken"></param>
//        /// <returns></returns>
//        protected virtual async Task<HttpResponseMessage> PostBinaryAsync(HttpRequestMessage request, CancellationToken cancelationToken)
//        {

//            var response = new HttpResponseMessage(HttpStatusCode.OK);
//            IDCMessage3 clientDCMessage = DCMessage3.Empty;

//#if DEBUG
//            // UNPACKING 
//            try
//            {
//                clientDCMessage = await CommunicationUnit.DeserializeFromRequest(request, typeof(DCMessage3)) as DCMessage3;
//                clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);

//                //Adding HttpMessage as Parameter - if logic based on HttpRequestMessage
//                clientDCMessage.Parameters.Add(BinParameter.NewInParam(nameof(HttpRequestMessage), request));

//            }
//            catch (Exception unpackExc)
//            {
//                if (clientDCMessage.IsNull()) clientDCMessage = DCMessage3.Empty;

//                //LOG ABOUT SERIALIZATION EXCEPTION
//                CommunicationUnit.LogPackUnpackFaultError(clientDCMessage, unpackExc, IsUnpackFaultException: true);

//                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
//                return response;
//            }

//            // EXECUTING COMMAND Exception handling Should be placed inside custom DCManager class                                    
//            DCManager.GetAndCacheDCManagerByShortKey(clientDCMessage.TargetDCManager)
//                     .ExecuteCommandOnServer(ScenarioKey, ref clientDCMessage);


//            //PACKING
//            try
//            {
//                //clear all input parameters
//                clientDCMessage.ClearInputParameters();
//                clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);
//                response = await CommunicationUnit.SerializeToResponse(response, clientDCMessage);               
//                return response;
//            }
//            catch (Exception packExc)
//            {
//                //LOG ABOUT SERIALIZATION EXCEPTION
//                CommunicationUnit.LogPackUnpackFaultError(clientDCMessage, packExc, IsPackFaultException: true);

//                response.StatusCode = HttpStatusCode.ExpectationFailed; response.ReasonPhrase = clientDCMessage.ProcessingFaultMessage;
//                return response;
//            }
//#else
//             clientDCMessage = await CommunicationUnit.DeserializeFromRequest(request, typeof(DCMessage3)) as DCMessage3;
//             clientDCMessage.SetServerReceivedFromClientTime(DateTime.Now);
//             clientDCMessage.Parameters.Add(BinParameter.NewInParam(nameof(HttpRequestMessage), request));

//             DCManager.GetAndCacheDCManagerByShortKey(clientDCMessage.TargetDCManager)
//                     .ExecuteCommandOnServer(ScenarioKey, ref clientDCMessage);

//             clientDCMessage.ClearInputParameters();
//             response = await CommunicationUnit.SerializeToResponse(response, clientDCMessage);             
//             return response;
//#endif
//        }


#endregion ---------------------------------- GARBAGE ------------------------------------
