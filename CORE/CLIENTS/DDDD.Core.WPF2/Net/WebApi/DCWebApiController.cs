﻿#if SERVER && IIS


using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;

namespace DDDD.Core.Net.WebApi
{
    /// <summary>
    /// Base Web Api Controller that use DC Messaging
    /// </summary>
    public class DCWebApiController: ApiController, IDCCUnitServer
    {

        /// <summary>
        /// DC Scenario Key
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios. WebApiControllers;


        /// <summary>
        /// Processing Command Message on Server side.		
        /// <para/> Process DCMessage by your [DCManager]'s infrastructure - DCMessage should be processed by DCMessage.TargetProcessingManagerID .
        /// <para/> If client need to see if  result is successful /or not message, 
        ///          then add such message into DCMessage.ProcessingFaultMessage/ DCMessage.ProcessingSuccessMessage with DCMessage.ErrorCode if your system require.        
        /// <para/> On client full faulted exception message should be showned only in debug Mode/ in release  user should see only standart-phrase message. 	
        /// <para/> Clear all input parameters - to save traffic when it'll be transmitted back. 
        /// <para/> Set diagnostics timestamps on received and sending back to client points.
        /// <para/> Send back DCMessage to Client.
        /// </summary>
        /// <param name="clientCommand">DCMessage from Client side</param>
        /// <returns>Send back processed DCMessage to Client.</returns>
        //public DCMessage ExecuteCommand([FromBody]DCMessage clientCommand)
        //{
        //    return DCManager.GetAndCacheDCManagerByID(clientCommand.TargetCommandManagerID) //  PublishedManager.FullName
        //              .ExecuteCommandOnServer(ScenarioKey, ref clientCommand);
        //}


        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{  return new string[] { "value1", "value2" };
        //}


        //// GET api/<controller>/5
        //public string Get(int id)
        //{  return "value";
        //}


        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}


        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}


        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}


        [HttpGet]
        [ActionName("ExecuteCommand")]
        public async Task<IDCMessage> GetExecuteCommand([FromBody] IDCMessage commandToServer)
        {
            return DCManager.GetAndCacheDCManagerByShortKey(commandToServer.TargetDCManager) //  PublishedManager.FullName
                     .ExecuteCommandOnServer(ScenarioKey, ref commandToServer);            
        }


        [HttpPost]
        [ActionName("ExecuteCommand")]
        public async Task<IDCMessage> PostExecuteCommand([FromBody] IDCMessage commandToServer)
        {
            return DCManager.GetAndCacheDCManagerByShortKey(commandToServer.TargetDCManager) //  PublishedManager.FullName
                       .ExecuteCommandOnServer(ScenarioKey, ref commandToServer);
        }


        [HttpPut]
        [ActionName("ExecuteCommand")]
        public async Task<IDCMessage> PutExecuteCommand([FromBody] IDCMessage commandToServer)
        {
            return DCManager.GetAndCacheDCManagerByShortKey(commandToServer.TargetDCManager) //  PublishedManager.FullName
                        .ExecuteCommandOnServer(ScenarioKey, ref commandToServer);
        }


        [HttpDelete]
        [ActionName("ExecuteCommand")]
        public async Task<IDCMessage> DeleteExecuteCommand([FromBody] IDCMessage commandToServer)//HttpResponseMessage
        {
            return DCManager.GetAndCacheDCManagerByShortKey(commandToServer.TargetDCManager) //  PublishedManager.FullName
                       .ExecuteCommandOnServer(ScenarioKey, ref commandToServer);
        }







    }
}

#endif