﻿#if CLIENT

using DDDD.Core.ComponentModel;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Net.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

using DDDD.Core.Extensions;
using DDDD.Core.Threading;

namespace DDDD.Core.Net.WebApi
{

    /// <summary>
    /// Factory that produce web Api Clients(that use IDCService contract), and is responsible for DCCommunicationUnitEn.Wcf_EnvironmentActivationSvc Unit TYPE. 
    /// </summary>
    [DCCUnitClientFactory(nameof(InitFactory), DCCScenarios.WebApiDirectHandlers)]
    public class DCWebApiDirectHandlerClientFactory : IDCCUnitClientFactory//<DCHttpClient>
    {



        #region -------------------------------- SINGLETON -------------------------------------


        DCWebApiDirectHandlerClientFactory()
        {
           
        }


        static LazyAct<DCWebApiDirectHandlerClientFactory> LA_CurrentFactory =
               LazyAct<DCWebApiDirectHandlerClientFactory>.Create(
                   (args) => { return new DCWebApiDirectHandlerClientFactory(); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of DCWebApiControllerFactory  
        /// </summary>
        public static DCWebApiDirectHandlerClientFactory Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


        #endregion -------------------------------- SINGLETON -------------------------------------




        /// <summary>
        /// Param Key const - used to pack some CommunicationUnit istance as parameter.
        /// </summary>
        public const string communicationUnit = nameof(communicationUnit);


        #region ------------------------------------- FIELDS ----------------------------------------

        private static readonly object _locker = new object();

#endregion ------------------------------------- FIELDS ----------------------------------------
                

        #region ---------------------- FACTORY PRODUCED SERVICE CLIENTS -----------------------------------

        /// <summary>
        /// Produced by current Factory Service Clients. Each of the Services use DCService(on th SERVER) and DCServiceClient(on CLIENT). 
        /// Thats the [Dictionary of- {Type-ownerType(some manager), DCServiceClient - service client, that use IDCService service contract}]. 
        /// </summary>  
        [ThreadStatic]      
        static readonly Dictionary<Type, IDCCUnitClient>  ServiceClients
               = new Dictionary<Type, IDCCUnitClient>();
        
        #endregion ---------------------- FACTORY PRODUCED SERVICE CLIENTS -----------------------------------



        #region ---------------------------- IFactory -------------------------------
   
        /// <summary>
        /// Factory Product Type - result Type
        /// </summary> 
        public Type BaseProductType
        { get; } = typeof(DCHttpClient);



        public List<Type> ProductTypes
        { get; } = new List<Type>() { typeof(DCHttpClient) };


        public object CreateItem(params KeyValuePair<string, object>[] parameters)
        {
            var paramCommunicationUnit = parameters.Where(kvp => kvp.Key == communicationUnit).FirstOrDefault();
            if (paramCommunicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

            return AddOrUseExistClientInternal(paramCommunicationUnit.Value as DCCUnit);
        }

        #endregion ---------------------------- IFactory -------------------------------



        #region ----------------------------------- CREATE CUSTOM BINDING ------------------------------


#if SERVER || (CLIENT && (WPF || SL5)) // All Port except WP81


        /// <summary>
        /// Default CustomBinding for WCF Service. 
        /// By default all DCService-s USE this CustomBinding on SERVER and CLIENT side  for Service and Client.
        /// </summary>
        public static Binding DCServiceCustomBindingDefault
        { get; } = CreateCustomBinding(true, TransferMode.StreamedResponse, sendTimeoutMins: 1, sendTimeoutSecs: 30
                                            , receiveTimeoutMins: 1, receiveTimeoutSecs: 30);






        /// <summary>
        /// Scenarios Keys
        /// </summary>
        public string[] ScenarioKeys
        { get; } = { DCCScenarios.WebApiDirectHandlers };





        /// <summary>
        /// Creating Custom Binding for WCF DCServiceClient service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="transferMode"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , TransferMode transferMode = TransferMode.StreamedResponse
                                                 , int maxBufferSize = int.MaxValue, int maxReceivedMessageSize = int.MaxValue
                                                , int sendTimeoutMins = 0, int sendTimeoutSecs = 30
                                                , int receiveTimeoutMins = 0, int receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,

            });

            CustomBinding binding = new CustomBinding(elements);

            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);//             

            return binding;
        }

#elif CLIENT && WP81

        // Wp81 -Has not TransferMode parameter configuration

        /// <summary>
        /// Default CustomBinding for WCF Service.
        /// By default all DCService-s USE this CustomBinding on SERVER and CLIENT side  for Service and Client.
        /// </summary>
        public static Binding DCServiceCustomBindingDefault
        { get; } = CreateCustomBinding(true,  sendTimeoutMins: 1, sendTimeoutSecs: 30
                                            , receiveTimeoutMins: 1, receiveTimeoutSecs: 30);

        /// <summary>
        /// Communication Keys
        /// </summary>
        public string[] ScenarioKeys
        { get; } = { DCCScenarios.WebApiDirectHandlers };


      


        /// <summary>
        /// Creating Custom Binding for WCF DCServiceClient service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , Int32 maxBufferSize = Int32.MaxValue, Int32 maxReceivedMessageSize = Int32.MaxValue
                                                , Int32 sendTimeoutMins = 0, Int32 sendTimeoutSecs = 30
                                                , Int32 receiveTimeoutMins = 0, Int32 receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                //TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,

            });


            CustomBinding binding = new CustomBinding(elements);


            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);// 
            return binding;
        }
      


#endif


        #endregion ----------------------------------- CREATE CUSTOM BINDING ------------------------------
        

        #region --------------------------------- ADD REMOVE CLINETS -------------------------




#if CLIENT && (SL5 || WP81)


        /// <summary>
        /// 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public IDCCUnitClient AddOrUseExistClient(DCCUnit communicationUnit)
        {
            return AddOrUseExistClientInternal(communicationUnit);
        }



        /// <summary>
        /// Add new   IDCCUnitClient client(now to use Web API) into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        protected internal static IDCCUnitClient AddOrUseExistClientInternal(DCCUnit communicationUnit)
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;

            //return if client already exist
            if (ServiceClients.ContainsKey(communicationUnit.OwnerType)) return ServiceClients[communicationUnit.OwnerType]; //NetConfiguration 

            //CF- is Channel Factory 
            if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
            {
                lock (_locker)
                {
                    if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
                    {
                        var virtualContractDesc = new ContractDescription(communicationUnit.OwnerName, DCSConsts.ServiceNamespaceDefault);

                        // EndpointAddress endPoint = new EndpointAddress(communicationUnit.UriResultAddress);
                        // var virtualContractDesc = new ContractDescription(communicationUnit.OwnerName, DCSConsts.ServiceNamespaceDefault);
                        // EndpointAddress endPoint = new EndpointAddress(communicationUnit.UriResultAddress);

                        var newDynamicCommandClient = targetHandlerType.CreateInstanceTBase<IDCCUnitClient>(
                            args: new object[] { communicationUnit });
                        
                        //var newDynamicCommandClient = DCHttpClient.Create(communicationUnit);

                        ServiceClients.Add(communicationUnit.OwnerType, newDynamicCommandClient);

                        return ServiceClients[communicationUnit.OwnerType];
                    }
                }
            }

            return ServiceClients[communicationUnit.OwnerType];
        }






#elif CLIENT && WPF


        
        public IDCCUnitClient AddOrUseExistClient(DCCUnit communicationUnit)
        {
            return AddOrUseExistClientInternal(communicationUnit);
        }



        /// <summary>
        ///   /// Add new  Web API DCHttpClient client into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        protected internal static IDCCUnitClient AddOrUseExistClientInternal(DCCUnit communicationUnit)
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;

            //return if client already exist
            if (ServiceClients.ContainsKey(communicationUnit.OwnerType)) return ServiceClients[communicationUnit.OwnerType]; //NetConfiguration 

            //CF- is Channel Factory 
            if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
            {
                lock (_locker)
                {
                    if (!ServiceClients.ContainsKey(communicationUnit.OwnerType))
                    {
                        var virtualContractDesc = new ContractDescription(communicationUnit.OwnerName, DCSConsts.ServiceNamespaceDefault);

                        EndpointAddress endPoint = new EndpointAddress(communicationUnit.UriResultAddress);

                        var newDynamicCommandClient = targetHandlerType.CreateInstanceTBaseLazy<DCHttpClient>(args: new object[] { communicationUnit  } );
                        
                        ServiceClients.Add(communicationUnit.OwnerType, newDynamicCommandClient);

                        return ServiceClients[communicationUnit.OwnerType];
                    }
                }
            }

            return ServiceClients[communicationUnit.OwnerType];
        }




#endif





        /// <summary>
        /// Remove  Service client from Hub by its service name 
        /// </summary>
        /// <param name="ServiceName">Name that you use on Creating this service</param>
        public static void RemoveServiceClient(Type ownerType)
        {
            if (ownerType == null) return;

            if (!ServiceClients.ContainsKey(ownerType)) return;

            lock (_locker)
            {
                if (ServiceClients.ContainsKey(ownerType))
                    ServiceClients.Remove(ownerType);
            }

        }

        private static void InitFactory()
        {


            
        }

        //public IDCCUnitClient CreateClientT(DCCUnit communicationUnit)
        //{
        //    if (communicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

        //    return AddOrUseExistClientInternal(communicationUnit);
        //}

        public IDCCUnitClient CreateClient(DCCUnit communicationUnit)
        {
            if (communicationUnit.IsNull()) throw new InvalidOperationException("communicationUnit parameter cannot be null");

            return AddOrUseExistClientInternal(communicationUnit);
        }


        #endregion

    }

}

#endif


#region -------------------------------------- GARBAGE ------------------------------------------


//.Create(communicationUnit.OwnerType
//, virtualContractDesc
//, endPoint
//, DCServiceCustomBindingDefault);
//newDynamicCommandClient.PublishedManager = ownerType;
//newDynamicCommandClient.UseTSSerializer = useTSSSerialization;



///// <summary>
///// Add new  WCF DCServiceClient service into internal static Dictionary.
///// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
///// <para/>  Also you can get this client next time by its ServiceContract.Name of SvcContract type as KEY. 
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>
///// <param name="useTSSSerialization"></param>
///// <param name="useTSSDirectSerialization"></param>
///// <param name="KnownTypesLoaderFunc"></param>
///// <returns></returns>
//public static DCServiceClient AddOrUseExistClient(Type SvcContract,
//                                    String endpointConfigurationName,
//                                    bool useTSSSerialization = false,
//                                    bool useTSSDirectSerialization = false,
//                                    Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                  )
//{
//    //CheckIsSvcContract(SvcContract);

//    var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
//                                 .FirstOrDefault() as ServiceContractAttribute;
//    string ServiceKey = svccontract.Name;

//    //return if client already exist
//    if (ServiceClients.ContainsKey(ServiceKey)) return ServiceClients[ServiceKey];

//    //if not exist check for methods ExecuteCommand
//    //CheckIsSvcContractSupportDynamicCommands(SvcContract);

//    if (!ServiceClients.ContainsKey(ServiceKey))
//    {
//        lock (_locker)
//        {
//            if (!ServiceClients.ContainsKey(ServiceKey))
//            {

//                ServiceClients.Add(ServiceKey,
//                                                null                    
//                                                //new DCServiceClient(
//                                                //      SvcContract
//                                                //     , endpointConfigurationName
//                                                //     , useTSSSerialization               //2 mode seriaization
//                                                //     , useTSSDirectSerialization        //3 mode seriaization                                                             
//                                                //     , KnownTypesLoaderFunc //load now KnownTypes 
//                                                //     )
//                                                     );

//                return ServiceClients[ServiceKey];
//            }
//        }
//    }

//    return ServiceClients[ServiceKey];

//}




///// <summary>
///// Add new  WCF DCServiceClient service into internal static Dictionary.
///// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
///// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>
///// <param name="useTSSSerialization"></param>
///// <param name="KnownTypesLoaderFunc"></param>
///// <returns></returns>
//public static DCServiceClient AddOrUseExistSvcClient(Type SvcContract,
//                                    String endpointConfigurationName, 
//                                    bool useTSSSerialization = false,
//                                    Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                  )
//{
//    //CheckIsSvcContract(SvcContract);

//    var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
//                                 .FirstOrDefault() as ServiceContractAttribute;
//    string ServiceKey = svccontract.Name;

//    //return if client already exist
//    if (ServiceClients.ContainsKey(ServiceKey)) return ServiceClients[ServiceKey];

//    //if not exist check for methods ExecuteCommand
//    //CheckIsSvcContractSupportDynamicCommands(SvcContract);

//    if (!ServiceClients.ContainsKey(ServiceKey))
//    {
//        lock (_locker)
//        {
//            if (!ServiceClients.ContainsKey(ServiceKey))
//            {

//                ServiceClients.Add(ServiceKey,
//                                                     null
//                                                     //new DCServiceClient(
//                                                     //SvcContract
//                                                     //, endpointConfigurationName
//                                                     //, useTSSSerialization                 // useTSSSerializer on dataTransmission
//                                                     //, KnownTypesLoaderFunc //load now KnownTypes 
//                                                     //)
//                                                     );

//                return ServiceClients[ServiceKey];
//            }
//        }
//    }

//    return ServiceClients[ServiceKey];

//}


#endregion -------------------------------------- GARBAGE ------------------------------------------

