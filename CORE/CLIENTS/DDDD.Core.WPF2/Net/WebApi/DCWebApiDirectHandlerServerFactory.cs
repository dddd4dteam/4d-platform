﻿
#if SERVER && IIS

using DDDD.Core.ComponentModel;
using DDDD.Core.Threading;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Web.Configuration;
using System.Web.Http.Dispatcher;
using System.Net.Http;
using System.Reflection;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using System.Web.Http.Routing;
using System.Net; 
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.Net.WebApi
{





    /// <summary>
    /// DC WebApiDirectHandler Server side Factory - also implement IDCCUnitServerFactory, and registered as DCCScenarios.WebApiDirectHandlers.      
    /// </summary>
    [DCCUnitServerFactory(DCCScenarios.WebApiDirectHandlers
             , initFactoryGlobalConfigMethodName: nameof(InitFactoryDefaultConfiguration)
             , initFactoryItemConfigMethodName: nameof(InitFactoryItemConfiguration) //factory item initing -add real Http handler
        )]
    public class DCWebApiDirectHandlerServerFactory :  IDCCUnitServerFactory<DCWebApiDirectHandler>                                           
   
    {

        #region -------------------------------- SINGLETON -------------------------------------


        DCWebApiDirectHandlerServerFactory(HttpConfiguration configuration)
        {
            httpConfiguration = configuration;
        }


        static LazyAct<DCWebApiDirectHandlerServerFactory> LA_CurrentFactory =
               LazyAct<DCWebApiDirectHandlerServerFactory>.Create(
                   (args) => { return new DCWebApiDirectHandlerServerFactory(GlobalConfiguration.Configuration); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of DCWebApiControllerFactory  
        /// </summary>
        public static DCWebApiDirectHandlerServerFactory Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


        #endregion -------------------------------- SINGLETON -------------------------------------


        protected HttpConfiguration httpConfiguration;

        /// <summary>
        /// Http Configuration  - reference to GlobalConfiguration.Configuration instance.
        /// </summary>
         protected internal HttpConfiguration HttpConfig
        {
            get { return httpConfiguration; }
        }


        #region ------------------------------ IFactory --------------------------------
         

        /// <summary>
        /// Factory Product Type - result Type
        /// </summary> 
        public Type BaseProductType
        { get; } = typeof(DCWebApiDirectHandler);



        public List<Type> ProductTypes
        { get; } = new List<Type>() { typeof(DCWebApiDirectHandler) };




        /// <summary>
        ///  Creating Item by CreateServiceHost(serviceType , baseAdresses) 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object CreateItem(params KeyValuePair<string, object>[] parameters)
        {
            var communicationUnit = parameters.FirstOrDefault(kv => kv.Key == "communicationUnit").Value;
            if (communicationUnit == null) throw new InvalidOperationException("CreateItem parameter [communicationUnit] was not found within parameters of CreateItem(...)");
                
            return new DCWebApiDirectHandler(communicationUnit as DCCUnit);//
        }

        #endregion------------------------------ IFactory --------------------------------


        #region ------------------------------ IDCCUnitServerFactory -----------------------------

        /// <summary>
        /// This Factory Registered as CommunicationUnit Factory that can produce  [WebApiDirectHandlers] Communication Units.
        /// <para/>  Communication Unit Factory is one the application Component types.
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios.WebApiDirectHandlers;


        /// <summary>
        /// Creating DCWebApiDirectHandler - http handler,  based on DCCommunicatinUnit instance and returns this instance. 
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public object AddOrUseExistService(DCCUnit communicationUnit)
        {
            return AddOrUseExistServiceTInternal(communicationUnit);
        }

        /// <summary>
        /// Creating DCWebApiDirectHandler - http handler,  based on DCCommunicatinUnit instance and returns this instance.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public DCWebApiDirectHandler AddOrUseExistServiceT(DCCUnit communicationUnit)
        {
            return AddOrUseExistServiceTInternal(communicationUnit);
            
        }



        /// <summary>
        /// Add Web API communication Endpoint to App by  DCCUnit
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
         protected internal static DCWebApiDirectHandler AddOrUseExistServiceTInternal(DCCUnit communicationUnit)
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull() )? Current.BaseProductType: communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;
            var apiDirectHandler = targetHandlerType.CreateInstanceTBaseLazy<DCWebApiDirectHandler>(args: new[] { communicationUnit } );
            
            //Add item to the RouteTable
            //GlobalConfiguration.Configuration.Routes.Add();
            // http://stackoverflow.com/questions/25915205/web-api-2-is-it-possible-to-load-a-route-controller-programmatically
            
            Current.HttpConfig.Routes.MapHttpRoute(
                         name: apiDirectHandler.CommunicationUnit.OwnerName // "DefaultApi"
                         , routeTemplate: apiDirectHandler.CommunicationUnit.UriResultAddress //   "webapi/{controller}"
                         , defaults:  null              //  new { id = RouteParameter.Optional },
                         , constraints: null
                         , handler: apiDirectHandler    //  InnerHandler = new HttpControllerDispatcher(config))
                        );
            
            return apiDirectHandler;
        }
        #endregion ------------------------------ IDCCUnitServerFactory -----------------------------




        /// <summary>
        /// Initing Web Api Infrastructure - that this DCCUnitServerFactory needs to use in its work.
        /// </summary>
        /// <returns></returns>
        protected internal static bool InitFactoryDefaultConfiguration()
        {
            return NetConfiguration.ConfigureWebApiInfrastructure();

        }



        /// <summary>
        /// Creating DCWebApiDirectHandler - http handler,  based on DCCommunicatinUnit instance.
        /// </summary>
        /// <param name="dccUnit"></param>
        /// <returns></returns>
        protected internal static bool InitFactoryItemConfiguration(DCCUnit dccUnit)
        {
            bool wasConfigurationInited = false;

            Current.AddOrUseExistServiceT(dccUnit);

            return wasConfigurationInited;
        }




    }



}
#endif


#region --------------------------------- GARBAGE -------------------------------------

//public IReadOnlyList<RouteEntry> GetDirectRoutes(HttpControllerDescriptor controllerDescriptor, IReadOnlyList<HttpActionDescriptor> actionDescriptors, IInlineConstraintResolver constraintResolver)
//{
//    return null;
//    return ((IDirectRouteProvider)Current).GetDirectRoutes(controllerDescriptor, actionDescriptors, constraintResolver);
//}


//static readonly DefaultHttpControllerActivator defaultActivator = new DefaultHttpControllerActivator();


/// <summary>
/// If we want to make cached Map of ControllerSetting for each of the Controller.
/// </summary>
//public static Dictionary<Type, Action<HttpControllerSettings>> Map = new Dictionary<Type, Action<HttpControllerSettings>>();


//public IHttpController Create2(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
//{
//    HttpConfiguration controllerConfig;
//    if (cacheHttpConfigs.TryGetValue(controllerType, out controllerConfig))
//    {
//        controllerDescriptor.Configuration = controllerConfig;
//    }
//    else if (Map.ContainsKey(controllerType))
//    {
//        controllerDescriptor.Configuration = Copy( controllerDescriptor.Configuration, Map[controllerType] );
//        cacheHttpConfigs.TryAdd(controllerType, controllerDescriptor.Configuration);
//    }

//    var result = defaultActivator.Create(request, controllerDescriptor, controllerType);
//    return result;
//}




//public HttpControllerDescriptor SelectController(HttpRequestMessage request)
//{
//    HttpControllerDescriptor controller;
//    try
//    {
//        controller = LA_DefaultSelector.Value.SelectController(request); 
//    }
//    catch (Exception ex)
//    {
//        string controllerName = GetControllerNameInternal(request);
//        Assembly assembly = Assembly.LoadFile(string.Format("{0}pak\\{1}.dll", HostingEnvironment.ApplicationPhysicalPath, controllerName));
//        Type controllerType = assembly.GetTypes()
//          .Where(i => typeof(IHttpController).IsAssignableFrom(i))
//          .FirstOrDefault(i => i.Name.ToLower() == controllerName.ToLower() + "controller");
//        controller = new HttpControllerDescriptor(httpConfiguration, controllerName, controllerType);
//    }
//    return controller;
//}


#endregion --------------------------------- GARBAGE -------------------------------------
