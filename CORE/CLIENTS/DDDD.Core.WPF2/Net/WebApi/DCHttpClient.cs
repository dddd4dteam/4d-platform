﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Reflection;
using System.Net.NetworkInformation;
using System.ServiceModel.Description;

using DDDD.Core.Notification;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using DDDD.Core.Diagnostics;
using DDDD.Core.Threading;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Patterns;
using DDDD.Core.Resources;
using DDDD.Core.Serialization.Binary;

namespace DDDD.Core.Net.WebApi
{

    #region ---------------------------- COMMENTS -----------------------------------------

    // But when using Windows Communication Foundation the usage of the client proxies tends to get very complicated 
    //because the client proxies need to be regenerated when the underlying communication channel is timed out or an exception occurs.
    //using(var client = new ClientProxyOfTypeFoo(someEndpointInformation) /* or proxyFactory.Create() */)
    //{
    //   try
    //   {
    //      client.Bar();
    //   }
    //   catch(CommunicationException coex) { // do something appropriate }
    //   catch(FaultExceptionOfBar fex) { // do something appropriate }
    //}

    #endregion ---------------------------- COMMENTS -----------------------------------------
        

    /// <summary>
    /// Dynamic Http Service based on WebApi/MVC Controllers technologies - Http Client.
    /// <para/>   Dynamic means that you needn't any additional I[ServiceContract] type except standart IDCService,and so
    ///               we can create new DCServiceClient in runtime for some Host Entity Type(now DCManager using ) - which used as key in internal Dictionary  of DCServiceClients.
    /// <para/> That's the wrap on WCF System.ServiceModel.ChannelFactory{IDCService} 
    /// <para/>   We can add new DCServiceClient by AddOrUseExistClient(...) method.   
    /// <para/> DCHttpClient also Allows to us unified TPL calls for every command or command cicles.
    /// <para/> It also support UI Notification about Progress based on TPL  IProgress{T} pattern.      
    /// <para/> By default use/workflow you can Add/Remove these  Clients .
    /// <para/> If you want to change DCMessage Execution:  for your custom MessageContract/for another Notifications - you can create your own derived from this class. 
    /// </summary>
    public class DCHttpClient : HttpClient, IDisposable, IDCCUnitClient
    {

        #region  ----------------------------------------- CTOR -------------------------------------------

        internal DCHttpClient() { }

        internal DCHttpClient(DCCUnit communicationUnit)
        {
             CommunicationUnit = communicationUnit;
        }
        
        #endregion ----------------------------------------- CTOR -------------------------------------------


        #region ------------------------------------------ IDisposable ------------------------------------------

        public void Dispose()
        {                        
            if (TSSerializer.IsNotNull()) TSSerializer = null;

        }

        #endregion ------------------------------------------ IDisposable ------------------------------------------
                

        #region ------------------------------------- FIELDS ----------------------------------------

        private static readonly object _locker = new object();

        #endregion ------------------------------------- FIELDS ----------------------------------------
                     

        #region ----------------------------- CREATION --------------------------------------
        

        protected internal static DCHttpClient Create(DCCUnit communicationUnit)
        {
            var newDcClient = new DCHttpClient();
            newDcClient.CommunicationUnit = communicationUnit; 
            return newDcClient;
        }


        #endregion----------------------------- CREATION --------------------------------------
        

        #region  ---------------------------------- PROPERTIES --------------------------------------

        /// <summary>
        /// Communication Unit - Communication Registration information.
        /// </summary>
        public DCCUnit CommunicationUnit
        {
            get; private set;
        }
        
        #endregion ---------------------------------- PROPERTIES --------------------------------------
        

        #region ----------------------------- SERIALIZATION INTEGRATION  -----------------------------------------


        /// <summary>
        /// Client's TSSerializer.
        /// <para/> Only if flag UseTSSerializer  is  true  then we"ll create  Clients TSSerializer.
        /// <para/> Clients TSSerializer will be stored  in TSSerializer's internal Dictionary of serializers until you delete it by hands( it may cause error in next call time, don't do it),
        /// or when you remove this Client from DCServiceClient internal Dictionary of Clients
        /// </summary>
        public ITypeSetSerializer TSSerializer { get; protected set; }


        #endregion ----------------------------- SERIALIZATION INTEGRATION  -----------------------------------------


        #region------------------------------- PUBLIC DYNAMIC SERVICE CLIENT  CHANGING  POINTS--------------------------------------------


        #endregion ------------------------------- PUBLIC DYNAMIC SERVICE CLIENT  CHANGING  POINTS--------------------------------------------



        #region -------------------------------------- EXECUTE COMMAND -----------------------------------------

#if CLIENT && WPF




        /// <summary>
        ///  Executing some Command Message to Server Command Manager using TPL
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ResultUICallback"> Action that you'll  do on result OK </param>
        /// <param name="TriggerFaultAferAction">Custom trigger Action. Will call always - if fault or not</param>
        /// <param name="reporter"> Progress reporter to notify client  UI  about command State or progress</param>
        /// <returns>Task  let us use async call with ExecuteCommand</returns>
        //public virtual Task ExecuteCommand(DCMessage command,
        //                                 Action<DCMessage> ResultUICallback,
        //                                 Action<Task<DCMessage>> TriggerFaultAferAction = null,
        //                                 DCProgressReporter reporter = null
        //                                )
        //{


        //    // Packing command and Delegates into new Server Call State-into ThreadCallState
        //    ThreadCallState<DCMessage,            //CallServiceKey + Action=Const(ExecuteCommand)  + command  Arguments 
        //                        Action<DCMessage>,      //uicallback Action  
        //                        Action<Task<DCMessage>>, //triggerFaultAferAction      
        //                        DCProgressReporter
        //                        >                      //timeout behavior                                                 
        //    newTaskState = ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>,
        //                        DCProgressReporter>
        //                        .Create(UIInvoker.UIDispatcher, command, ResultUICallback, TriggerFaultAferAction, reporter);



        //    return Task.Factory.StartNew((st) =>
        //    {
        //        ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTS =
        //   st as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;

        //        //set ServiceKey of Current Service - it'll  be transmit this command
        //        insideTS.Item1.SetServiceKey(VirtualContract.Name); // ServiceKey
        //        insideTS.Item1.SetServiceNamespace(VirtualContract.Namespace); // ServiceNamespace


        //        if (!NetworkInterface.GetIsNetworkAvailable())
        //        {
        //            // no network connection
        //            // throw new NotSupportedException("Network doesn't support workable connection now to make service call");
        //            NotifyNetworkDoesntWorkAndStopCommandProcessing(insideTS.Item1, insideTS.Item4);
        //            return insideTS.Item1;
        //        }

        //        var channel = CachedChannelFactory.CreateChannel();// CreateChannelDelegate(); //Factory
        //        //var ExecCommandFunc = DCSConsts.BuildExecuteCommandFunc(channel);


        //        // indication - Starting if newtwork enable start
        //        NotifyStartCommandProcessing(insideTS.Item1, insideTS.Item4);

        //        SetDiagnosticsOnClientSendTimeDbg(insideTS.Item1);

        //        //if (UseTSSDirectSerialization == false)
        //        //{
        //        //    PackInputMessageIfUseTSSSerialize(insideTS.Item1);
        //        //}


        //        return (DCMessage)channel.ExecuteCommand(command);
        //                                   // GetType()
        //                                   //.GetMethod(DCSConsts.Method_ExecuteCommand)
        //                                   //.Invoke(channel, new object[] { command });


        //    },
        //         newTaskState,
        //         TaskCreationOptions.AttachedToParent)
        //         .ContinueWith<DCMessage>(
        //         (t) =>
        //         {
        //             ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTSout =
        //                t.AsyncState as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;


        //             // updating command container result if Task Ran to Completion
        //             if (t.Status == TaskStatus.RanToCompletion)
        //             {
        //                 //updating DCMessage by received from server Result Message 
        //                 //if (UseTSSDirectSerialization == false)
        //                 //{
        //                 //    insideTSout.Item1 = UnpackOutputMessageIfUseTSSSerialize(t.Result);
        //                 //}
        //                 //else
        //                 //{
        //                     insideTSout.Item1 = t.Result;
        //                 //}

        //                 SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(insideTSout.Item1);

        //                 NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

        //                 NotifyAboutCommandSuccessResultProgress(insideTSout.Item1, insideTSout.Item4);


        //                 // ui result callback  
        //                 if (insideTSout.Item2 != null)
        //                 {
        //                     //not very safe way to call delegate on
        //                     UIInvoker.InvokeAsynchronously(() =>
        //                      {
        //                          insideTSout.Item2(insideTSout.Item1);
        //                      });
        //                 }

        //                 //continue TriggerFaultAferAction - Will be Called if it exist
        //                 if (insideTSout.Item3 != null)
        //                 { insideTSout.Item3(t); }


        //             }
        //             else if (t.Status == TaskStatus.Faulted)
        //             {
        //                 //RanToCompletion                  
        //                 //check if FActory not Faulted -if Faulted then recreate it
        //                 CommunicationItemFaultedInternalCheckReset(this, new EventArgs());

        //                 NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

        //                 NotifyAboutCommandTaskFailureExceptionsProgress(t.Exception, insideTSout.Item1, insideTSout.Item4);

        //                 //continue TriggerFaultAferAction - Will be Called if it exist
        //                 if (insideTSout.Item3 != null)
        //                 { insideTSout.Item3(t); }
        //             }

        //             return insideTSout.Item1;
        //         }
        //         );

        //}



#elif CLIENT && (SL5 || WP81)

        /// <summary>
        ///  Executing some Command Message to Server Command Manager using TPL
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ResultUICallback"> Action that you'll  do on result OK </param>
        /// <param name="TriggerFaultAferAction"></param>
        /// <param name="reporter"></param>
        /// <returns></returns>
        //public virtual async Task ExecuteCommand(DCMessage command,
        //                                 Action<DCMessage> ResultUICallback,  
        //                                 Action<Task<DCMessage>> TriggerFaultAferAction = null,
        //                                 DCProgressReporter reporter = null
        //                                )
        //{           


        //    // Packing command and Delegates into new Server Call State-into ThreadCallState
        //    ThreadCallState< DCMessage,            //CallServiceKey + Action=Const(ExecuteCommand)  + command  Arguments 
        //                        Action<DCMessage>,      //uicallback Action  
        //                        Action<Task<DCMessage>>, //triggerFaultAferAction      
        //                        DCProgressReporter
        //                        >                      //timeout behavior                                                 
        //    newTaskState = ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>,
        //                        DCProgressReporter>
        //                        .Create(UIInvoker.UIDispatcher, command, ResultUICallback, TriggerFaultAferAction, reporter);


        //    //set ServiceKey of Current Service - it'll  be transmit this command
        //    command.SetServiceKey(VirtualContract.Name); // ServiceKey 
        //    command.SetServiceNamespace(VirtualContract.Namespace); // ServiceNamespace



        //    //creating Task of Server CALL to process Command
        //    Task<DCMessage> querytask = new Task<DCMessage>((st) =>
        //    {

        //        ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTS =
        //        st as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;

        //        if (!NetworkInterface.GetIsNetworkAvailable())
        //        {
        //           // no network connection
        //           // throw new NotSupportedException("Network doesn't support workable connection now to make service call");
        //            NotifyNetworkDoesntWorkAndStopCommandProcessing(insideTS.Item1, insideTS.Item4);
        //            return insideTS.Item1; 
        //        }


        //        var channel = CachedChannelFactory.CreateChannel(CachedEndpointAddress);
        //        //var beginExecCommandFunc = DCSConsts.BuildBeginExecuteCommandFunc(channel);
        //        //var endExecCommandFunc = DCSConsts.BuildEndExecuteCommandFunc(channel);


        //        // indication - Starting if newtwork enable start
        //        NotifyStartCommandProcessing(insideTS.Item1, insideTS.Item4);

        //        SetDiagnosticsOnClientSendTimeDbg(insideTS.Item1);

        //        PackInputMessageIfUseTSSSerialize(insideTS.Item1);

        //        Task<DCMessage> resltTask = Task<DCMessage>.Factory.FromAsync(
        //                              channel.BeginExecuteCommand,  // channel.BeginExecuteCommand,     
        //                              channel.EndExecuteCommand, //   endExecCommandFunc,    // channel.EndExecuteCommand,         
        //                              command,
        //                              null
        //                             );


        //        return resltTask.Result;
        //    },
        //    newTaskState,
        //    TaskCreationOptions.AttachedToParent
        //    );            


        //    //running our created Task asynchronously
        //    await TaskEx.Run(() =>
        //    {
        //        //starting Task 
        //        querytask.Start();

        //        return querytask.ContinueWith
        //        ((t) => /// on RESULTS CONTINUE - MAY BE SUCCESSFUL/ MAY BE FAULT
        //        {


        //            ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTSout =
        //                t.AsyncState as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;


        //            // updating command container result if Task Ran to Completion
        //            if (t.Status == TaskStatus.RanToCompletion)
        //            {
        //              // updating DCMessage by received from server Result Message 
        //              var returnedFromServerDCommand = UnpackOutputMessageIfUseTSSSerialize(t.Result);
        //              insideTSout.Item1.Result = returnedFromServerDCommand.Result;
        //              insideTSout.Item1.SetServerReceivedFromClientTime(returnedFromServerDCommand.ServerReceivedFromClientTime);
        //              insideTSout.Item1.SetServerSendToClientTime(returnedFromServerDCommand.ServerSendToClientTime);
        //              insideTSout.Item1.ProcessingSuccessMessage = returnedFromServerDCommand.ProcessingSuccessMessage;
        //              insideTSout.Item1.ProcessingFaultMessage = returnedFromServerDCommand.ProcessingFaultMessage;

        //              SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(insideTSout.Item1);

        //              NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

        //              NotifyAboutCommandSuccessResultProgress(insideTSout.Item1, insideTSout.Item4);    


        //                // ui result callback  
        //                if (insideTSout.Item2 != null)
        //                {
        //                    //not very safe way to call delegate on
        //                    UIInvoker.InvokeAsynchronously(() =>
        //                    {
        //                        insideTSout.Item2(insideTSout.Item1);
        //                    });
        //                }

        //                //continue TriggerFaultAferAction - Will be Called if it exist
        //                if (insideTSout.Item3 != null)
        //                { insideTSout.Item3(t); }


        //            }
        //            else if (t.Status == TaskStatus.Faulted)
        //            {
        //                //RanToCompletion                  
        //                //check if FActory not Faulted -if Faulted then recreate it                        
        //                CommunicationItemFaultedInternalCheckReset(this, new EventArgs());

        //                NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);


        //                NotifyAboutCommandTaskFailureExceptionsProgress(t.Exception, insideTSout.Item1, insideTSout.Item4);

        //                //continue TriggerFaultAferAction - Will be Called if it exist
        //                if (insideTSout.Item3 != null)
        //                { insideTSout.Item3(t); }
        //            }

        //            return insideTSout.Item1; //need to process Faulted case 

        //         });

        //    });


        //}


#endif








#if CLIENT && (WP81 || SL5)

        public async Task<IDCMessage> ExecuteCommand(IDCMessage commandMessage,
            DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {
           return await  ExecuteCommandInternal(commandMessage, reporter, customNetworkIsNotEnableHandler);
        }
        
        protected internal virtual async Task<IDCMessage> ExecuteCommandInternal(IDCMessage commandMessage, DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {

            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                customNetworkIsNotEnableHandler?.Invoke(commandMessage, reporter);
                return commandMessage;
            }


            //IDCService serviceProxy = CachedChannelFactory.CreateChannel(CachedEndpointAddress);
            //ICommunicationObject communicationItem = serviceProxy as ICommunicationObject;

            try
            {
                return null;
                //return await Task.Factory.FromAsync(
                //                   serviceProxy.BeginExecuteCommand  // channel.BeginExecuteCommand,     
                //                  ,serviceProxy.EndExecuteCommand //   endExecCommandFunc,    // channel.EndExecuteCommand,                                           
                //                  ,commandMessage 
                //                  ,TaskScheduler.Default
                //                  //TaskCreationOptions.AttachedToParent                                  
                //                 );
            }
            catch (CommunicationObjectAbortedException comAbortExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comAbortExc;
            }
            catch (CommunicationObjectFaultedException comFaultedExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comFaultedExc;
            }
            //finally
            //{
            //    //if (communicationItem?.State == CommunicationState.Faulted)
            //    //{
            //    //    CommunicationItemFaultedResetHandler();
            //    //}
            //}

        }


#elif CLIENT && WPF

        public async Task<IDCMessage> ExecuteCommand(IDCMessage commandMessage,
            DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {
           return await  ExecuteCommandInternal(commandMessage, reporter, customNetworkIsNotEnableHandler);
        }
        
        static void NetworkEnableCheckAction(IDCMessage commandMessage , DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {   
            if (!NetworkInterface.GetIsNetworkAvailable())//not available
            {
                if (customNetworkIsNotEnableHandler.IsNotNull())
                {
                    customNetworkIsNotEnableHandler(commandMessage, reporter);
                    throw new InvalidOperationException(
                      RCX.ERR_InvalidOperation.Fmt(nameof(DCHttpClient), nameof(NetworkEnableCheckAction), " Network is not available ")
                        );
                }
            }
                                    
        } 


        protected internal virtual async Task<IDCMessage> ExecuteCommandInternal(IDCMessage commandMessage, DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {
            try
            {
                // NETWORK ENABLE CHECK
                await RetryOperation.RetryOnFault(
                                () =>
                                { NetworkEnableCheckAction(commandMessage, reporter, customNetworkIsNotEnableHandler);
                                }                                 
                                );
            }
            catch (Exception ex)
            {
                // if still not available return commandMessage
                return commandMessage;
            }
             

            //IDCService serviceProxy = CachedChannelFactory.CreateChannel(CachedEndpointAddress);
            //ICommunicationObject communicationItem = serviceProxy as ICommunicationObject;

            try
            {
                return null;
                //return await Task.Factory.FromAsync(
                //                   serviceProxy.BeginExecuteCommand  // channel.BeginExecuteCommand,     
                //                  ,serviceProxy.EndExecuteCommand //   endExecCommandFunc,    // channel.EndExecuteCommand,                                           
                //                  ,commandMessage 
                //                  ,TaskScheduler.Default
                //                  //TaskCreationOptions.AttachedToParent                                  
                //                 );
            }
            catch (CommunicationObjectAbortedException comAbortExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comAbortExc;
            }
            catch (CommunicationObjectFaultedException comFaultedExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comFaultedExc;
            }
            //finally
            //{
            //    //if (communicationItem?.State == CommunicationState.Faulted)
            //    //{
            //    //    CommunicationItemFaultedResetHandler();
            //    //}
            //}

        }

#elif SERVER

        public async Task<IDCMessage> ExecuteCommand(IDCMessage commandMessage,
         DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {
            return await ExecuteCommandInternal(commandMessage, reporter, customNetworkIsNotEnableHandler);
        }

        protected internal virtual async Task<IDCMessage> ExecuteCommandInternal(IDCMessage commandMessage, DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler)
        {

            if (!NetworkInterface.GetIsNetworkAvailable())
            {
                customNetworkIsNotEnableHandler?.Invoke(commandMessage, reporter);
                return commandMessage;
            }


            //IDCService serviceProxy = CachedChannelFactory.CreateChannel(CachedEndpointAddress);
            //ICommunicationObject communicationItem = serviceProxy as ICommunicationObject;

            try
            {
                return null;
                //return await Task.Factory.FromAsync(
                //                   serviceProxy.BeginExecuteCommand  // channel.BeginExecuteCommand,     
                //                  ,serviceProxy.EndExecuteCommand //   endExecCommandFunc,    // channel.EndExecuteCommand,                                           
                //                  ,commandMessage 
                //                  ,TaskScheduler.Default
                //                  //TaskCreationOptions.AttachedToParent                                  
                //                 );
            }
            catch (CommunicationObjectAbortedException comAbortExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comAbortExc;
            }
            catch (CommunicationObjectFaultedException comFaultedExc)
            {
                CommunicationItemFaultedResetHandler();
                throw comFaultedExc;
            }
            //finally
            //{
            //    //if (communicationItem?.State == CommunicationState.Faulted)
            //    //{
            //    //    CommunicationItemFaultedResetHandler();
            //    //}
            //}

        }


#endif






        private void CommunicationItemFaultedResetHandler()
        {
            // communication exception happened

            var newChannelFactoryStub = LazyAct<bool>.Create(
            (args) =>
            {
                //if (CachedChannelFactory != null && CachedChannelFactory.State == CommunicationState.Faulted)
                //{
                //    lock (_locker)
                //    {
                //        if (CachedChannelFactory != null && CachedChannelFactory.State == CommunicationState.Faulted)
                //        {
                //            CachedChannelFactory.Abort();
                //            CachedChannelFactory = null;
                //            // We need to recreate channel's Factory and Update by currently Cached Values
                //            TryResetClient();                                                        
                //        }
                //    }
                //}             

                return true;
            }
            , null // sync object 
            , null// null arguments
            ).Value;//start initing 

        }






        #endregion -------------------------------------- EXECUTE COMMAND -----------------------------------------


        #region ---------------------------------------- DIAGNOSTICS POINTS -----------------------------------------

        /// <summary>
        /// Last Time(time stamp) when Client sended message to Server-side
        /// </summary>
        public DateTime LastClientSendTimeStamp { get; set; }

        /// <summary>
        /// Last Time(time stamp) when Client Received message from Server-side 
        /// </summary>
        public DateTime LastClientRecievedResultFromServerTimeStamp { get; set; }

        /// <summary>
        /// Current Client suppported scenarios - what DCCommunication Scenarios this client support.
        /// </summary>
        public string[] ScenarioKeys
        { get; } = { DCCScenarios.WebApiDirectHandlers };                    
            
        

        [Conditional("DEBUG")]
        void SetDiagnosticsOnClientSendTimeDbg(DCMessage command)
        {
            command.SetClientSendTime(DateTime.Now);
        }


        [Conditional("DEBUG")]
        void SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(DCMessage command)
        {
            command.SetClientRecievedResultFromServerTime(DateTime.Now);
        }

       
        //public DateTime ServerReceivedFromClientTime { get; set; }
        //public DateTime ServerSendToClientTime { get; set; }


        #endregion ---------------------------------------- DIAGNOSTICS POINTS -----------------------------------------




    }

}




#region ----------------------------------- GARBAGE ---------------------------------


///// <summary>
///// Initializing new DCServiceClient
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="serviceBinding"></param>
///// <param name="serviceAddress"></param>
///// <param name="KnowTypesLoaderFunc"></param>
///// <param name="useTSSSerializer"></param>
///// <param name="useTSSDirectSerialization"></param>
//void Initialize(Type SvcContract,
//                 Binding serviceBinding,
//                 EndpointAddress serviceAddress,
//                 Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
//                 bool useTSSSerializer = false,
//                 bool useTSSDirectSerialization = false
//                )
//{
//    try
//    {
//        ServiceContract = SvcContract;

//        var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
//        ServiceKey = scontract.Name;
//        ServiceNamespace = scontract.Namespace;

//        ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

//        ServiceBinding = serviceBinding;

//        ServiceAddress = serviceAddress;

//        CreateChannelFactoryDelegate = DCSConsts.GetCreateChannelFactoryDelegate(ServiceChannelFactoryContract);

//        Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

//        DCSEndpoint = GetDSEndpoint(Factory);

//        CreateChannelDelegate = DCSConsts.GetCreateChannelDelegate( Factory); //Factory.CreateChannel

//        //addKnownTypes from CustomLoader to ExecuteCommand Operation and set TSSSerialization if enable
//        LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer, useTSSDirectSerialization);


//    }
//    catch (Exception)
//    {
//        throw;
//    }
//}



///// <summary>
///// Initializing new DCServiceClient from Configuration
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>        
///// <param name="KnowTypesLoaderFunc"></param>
///// <param name="useTSSSerializer"></param>
///// <param name="useTSSDirectSerialization"></param>
//void Initialize(Type SvcContract,
//               String endpointConfigurationName,
//               Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
//               bool useTSSSerializer = false,
//               bool useTSSDirectSerialization = false
//              )
//{
//    try
//    {
//        ServiceContract = SvcContract;

//        var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
//        ServiceKey = scontract.Name;
//        ServiceNamespace = scontract.Namespace;

//        ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

//        CreateChannelFactoryDelegate = DCSConsts.GetCreateChannelFactoryFromConfigDelegate(ServiceChannelFactoryContract);

//        Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);
//        //var itm = Factory as ChannelFactory<>;

//        ServiceBinding = Factory.Endpoint.Binding;// serviceBinding;

//        ServiceAddress = Factory.Endpoint.Address;// serviceAddress;

//        DCSEndpoint = GetDSEndpoint(Factory);

//        CreateChannelDelegate = DCSConsts.GetCreateChannelDelegate(Factory);

//        //addKnownTypes from CustomLoader to ExecuteCommand Operation and set TSSSerialization if enable
//        LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer, useTSSDirectSerialization);


//    }
//    catch (Exception)
//    {
//        throw;
//    }
//}


///// <summary>
///// Initializing new DCServiceClient
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="serviceBinding"></param>
///// <param name="serviceAddress"></param>
///// <param name="KnowTypesLoaderFunc"></param>
///// <param name="useTSSSerializer"></param>
//void Initialize( Type  SvcContract,  
//                 System.ServiceModel.Channels.Binding serviceBinding ,
//                 EndpointAddress serviceAddress, 
//                 Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
//                 bool useTSSSerializer = false
//                )
//{
//    try
//    {
//        ServiceContract = SvcContract;

//        var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
//        ServiceKey = scontract.Name;             
//        ServiceNamespace = scontract.Namespace;

//        ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);



//        ServiceBinding = serviceBinding;

//        ServiceAddress = serviceAddress;

//        CreateChannelFactoryDelegate = DCSConsts.GetCreateChannelFactoryDelegate(ServiceChannelFactoryContract);

//        Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

//        DCSEndpoint = GetDSEndpoint(Factory);                

//        CreateChannelDelegate = DCSConsts.GetCreateChannelDelegate(Factory);

//        //addKnownTypes from CustomLoader to ExecuteCommand Operation and set TSSSerialization if enable
//        LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc,useTSSSerializer);


//    }
//    catch (Exception)
//    {                
//        throw;
//    }
//}



///// <summary>
///// Initializing new DCServiceClient from Configuration
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>        
///// <param name="KnowTypesLoaderFunc"></param>
///// <param name="useTSSSerializer"></param>
//void Initialize(Type SvcContract,
//               String endpointConfigurationName,                      
//               Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
//               bool useTSSSerializer = false
//              )
//{
//    try
//    {
//        ServiceContract = SvcContract;

//        var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
//        ServiceKey = scontract.Name;
//        ServiceNamespace = scontract.Namespace;

//        ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);              

//        CreateChannelFactoryDelegate = DCSConsts.GetCreateChannelFactoryFromConfigDelegate(ServiceChannelFactoryContract);

//        Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

//        ServiceBinding = Factory.Endpoint.Binding;// serviceBinding;

//        ServiceAddress = Factory.Endpoint.Address;// serviceAddress;

//        DCSEndpoint = GetDSEndpoint(Factory);

//        CreateChannelDelegate = DCSConsts.GetCreateChannelDelegate(Factory);

//        //addKnownTypes from CustomLoader to ExecuteCommand Operation and set TSSSerialization if enable
//        LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer);


//    }
//    catch (Exception)
//    {
//        throw;
//    }
//}



#if CLIENT && (SL5 || WP81)

///// <summary>
///// Create new  DCServiceClient with using custom binding and endpoint manually.  Only for internal use by Hub/ or protected
///// </summary>
///// <param name="SvcContract">WCF Service Contract Type</param>
///// <param name="serviceBinding"> Binding that this Serice support on Server </param>
///// <param name="useTSSSerializer">switch to use  2 serialization mode - TSSSerializer + DataContractSerializer</param>
///// <param name="KnownTypesLoaderFunc">Func that returns known types-contract that will init serialization</param>
///// <param name="routeTemplate">Http adress building template. You can choose from predefined templates set of HttpRouteTemplateEn</param>
///// <param name="ServiceFileName">Http adress Template Part -[service]. Without [*.svc]  extension. </param>
///// <param name="Server">Http adress Template Part -[server]</param>
///// <param name="Port">Http adress Template Part -[port]</param>
///// <param name="WebApplication">Http adress Template Part -[WebApplication]</param>
///// <param name="Subfolder">Http adress Template Part -[Subfolder]</param>
//protected internal DCServiceClient( Type SvcContract, Binding serviceBinding,
//                                      bool useTSSSerializer = false,
//                                      Func<IEnumerable<Type>> KnownTypesLoaderFunc = null, 
//                                      HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlService,
//                                      String ServiceFileName = null, String Server = null, String Port = null, String WebApplication = null, String Subfolder = null)
//{       

//        //Initialize( SvcContract,  serviceBinding

//        //            , new EndpointAddress(HttpRouteClient.BuildServiceUri(routeTemplate, ServiceFileName, Server, Port,
//        //                                                                        WebApplication, Subfolder)
//        //                          )
//        //             , KnownTypesLoaderFunc, 
//        //             useTSSSerializer 
//        //           );                

//}


///// <summary>
///// Create new  DCServiceClient with using endpoint configuration.  Only for internal use by Hub/ or protected
///// </summary>
///// <param name="SvcContract"></param>
///// <param name="endpointConfigurationName"></param>
///// <param name="useTSSSerializer"></param>
///// <param name="KnownTypesLoaderFunc"></param>
//protected internal DCServiceClient(Type SvcContract,String endpointConfigurationName,
//                                      bool useTSSSerializer = false,
//                                      Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                     )
//{

//    //Initialize(SvcContract //serviceBinding                        
//    //            , endpointConfigurationName
//    //            , KnownTypesLoaderFunc
//    //            , useTSSSerializer
//    //           );

//}

#elif CLIENT && WPF


//        /// <summary>
//        /// Create new  DCServiceClient with using custom binding and endpoint manually.  Only for internal use by Hub/ or protected
//        /// </summary>
//        /// <param name="SvcContract"> WCF Service Contract Type</param>
//        /// <param name="serviceBinding"> Binding that this Serice support on Server </param>
//        /// <param name="useTSSSerializer"> switch to use  2 serialization mode - TSSerializer + DataContractSerializer </param>
//        /// <param name="useTSSDirectSerialization">switch to use  3 serialization mode -  Direct TSSerializer(XmlTSS) </param>
//        /// <param name="KnownTypesLoaderFunc">Func that returns known types-contract that will init serialization</param>
//        /// <param name="routeTemplate">Http adress building template. You can choose from predefined templates set of HttpRouteTemplateEn</param>
//        /// <param name="ServiceFileName">Http adress Template Part -[service]. Without [*.svc]  extension.</param>
//        /// <param name="Server">Http adress Template Part -[server]</param>
//        /// <param name="Port">Http adress Template Part -[port]</param>
//        /// <param name="WebApplication">Http adress Template Part -[WebApplication]</param>
//        /// <param name="Subfolder">Http adress Template Part -[Subfolder]</param>
//        protected internal DCServiceClient(Type SvcContract, Binding serviceBinding,
//                                              bool useTSSSerializer = false,
//                                              bool useTSSDirectSerialization = false,
//                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null,
//                                              HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlService,
//                                              string ServiceFileName = null, string Server = null, string Port = null, string WebApplication = null, String Subfolder = null)
//        {

//            Initialize(SvcContract,
//                         serviceBinding
//                        , new EndpointAddress(HttpRouteClient.BuildServiceUri(routeTemplate, ServiceFileName, Server, Port,
//                                                                                    WebApplication, Subfolder)
//                                      )
//                        , KnownTypesLoaderFunc
//                        , useTSSSerializer
//                        , useTSSDirectSerialization
//                       );

//        }



//        /// <summary>
//        /// Create new  DCServiceClient with using endpoint configuration.  Only for internal use by Hub/ or protected
//        /// </summary>
//        /// <param name="SvcContract"></param>
//        /// <param name="endpointConfigurationName"></param>
//        /// <param name="useTSSSerializer"></param>
//        /// <param name="useTSSDirectSerialization"></param>
//        /// <param name="KnownTypesLoaderFunc"></param>
//        protected internal DCServiceClient(Type SvcContract, string endpointConfigurationName,
//                                              bool useTSSSerializer = false,
//                                              bool useTSSDirectSerialization = false,
//                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
//                                             )
//        {

//            Initialize(SvcContract
//                        , endpointConfigurationName
//                        , KnownTypesLoaderFunc
//                        , useTSSSerializer
//                        , useTSSDirectSerialization
//                       );

//        }




#endif

/// <summary>
/// ServiceContract of current DCServiceClient 
/// </summary>
//public Type ServiceContract { get; protected set; }

/// <summary>
/// Service Name from ServiceContract attribute. By this key -   
/// 1- this DCServiceClient exist in DynamicCommandServiceHub - it means that you can get it back from DynamicCommandServiceHub by this key
/// 2 TSSerializer will be added for this Client if  we  [UseTSSerializer].
/// </summary>
//public String ServiceKey { get; protected set; }

/// <summary>
/// Namespace of  ServiceContract interface
/// </summary>
//public string ServiceNamespace { get; protected set; }

/// <summary>
/// ChannelFactory{IServiceContract} Type
/// </summary>
//public Type ServiceChannelFactoryContract { get; protected set; }

/// <summary>
///  ChannelFactory of <ServiceContract>   of current DCServiceClient
/// </summary>
//protected ChannelFactory Factory { get; set; }




/// <summary>
/// Service Binding that you add on current Client creation
/// </summary>
//public Binding ServiceBinding { get; protected set; }


///// <summary>
///// Current  Client Endpoint Address
///// </summary>
//public EndpointAddress ServiceAddress { get; protected set; }


/// <summary>
/// Current Client Endpoint 
/// </summary>
//protected ServiceEndpoint DCSEndpoint { get; set; }

/// <summary>
/// ExecuteCommand Operation Description of from ServiceContract
/// </summary>
//protected OperationDescription ExecuteCommandOperation
//{
//    get
//    {
//        return DCSEndpoint.Contract.Operations.Where(op => op.Name == DCSConsts.DSOperation_ExecuteCommand).FirstOrDefault();
//    }
//}




/// <summary>
/// You can change Binding Timeouts if it's needable.
/// </summary>
/// <param name="SendTimeoutMins"></param>
/// <param name="SendTimeoutSeconds"></param>
/// <param name="ReceiveTimeOutMins"></param>
/// <param name="ReceiveTimeoutSeconds"></param>
//public void ResetTimeouts(Int32 SendTimeoutMins = 0, Int32 SendTimeoutSeconds = 30, Int32 ReceiveTimeOutMins = 0, Int32 ReceiveTimeoutSeconds = 30)
//{
//    ServiceBinding.SendTimeout = new TimeSpan(0, SendTimeoutMins, SendTimeoutSeconds);
//    ServiceBinding.ReceiveTimeout = new TimeSpan(0, ReceiveTimeOutMins, ReceiveTimeoutSeconds);
//}



#region --------------------------------------- NESTED TYPES ----------------------------------------

/// <summary>
/// Constant values of DCServiceClient  infrastructure
/// </summary>
//protected internal static class DCSConsts
//{

//    public const string LoadKnownContractsMethodName = "LoadKnownContracts";

//    public const string DSOperation_ExecuteCommand = "ExecuteCommand";

//    public const string Server_Default_Error_Message = "Server error happened.";

//#if CLIENT && (SL5 || WP81)

//    public const string Method_BeginExecuteCommand = "BeginExecuteCommand";


//    public const string Method_EndExecuteCommand = "EndExecuteCommand";


//    public static Type BeginExecuteCommandFuncType = typeof(Func<DCMessage, AsyncCallback, object, IAsyncResult>);


//    public static Type EndExecuteCommandFuncType = typeof(Func<IAsyncResult, DCMessage>);




//    public static Func<DCMessage, AsyncCallback, object, IAsyncResult> BuildBeginExecuteCommandFunc(ICommunicationObject channel)
//    {
//        try
//        {
//            return channel.GetType().GetMethod(Method_BeginExecuteCommand)
//                         .CreateDelegate(BeginExecuteCommandFuncType, channel)
//                          as Func<DCMessage, AsyncCallback, object, IAsyncResult>;


//        }
//        catch (Exception exc)
//        {
//            throw exc;
//        }
//    }


//    public static Func<IAsyncResult, DCMessage> BuildEndExecuteCommandFunc(ICommunicationObject channel)
//    {
//        try
//        {
//            return channel.GetType().GetMethod(Method_EndExecuteCommand)
//                         .CreateDelegate(EndExecuteCommandFuncType, channel)
//                          as Func<IAsyncResult, DCMessage>;
//        }
//        catch (Exception exc)
//        {
//            throw exc;
//        }
//    }
//#endif

//#if CLIENT && WPF

//            public const string Method_ExecuteCommand = "ExecuteCommand";
//            public const string Method_ExecuteCommandAsync = "ExecuteCommandAsync";
//            public static Type ExecuteCommandFuncType = typeof(Func<DCMessage, DCMessage>);
//            public static Type ExecuteCommandAsyncFuncType = typeof(Func<DCMessage, Task<DCMessage>>);

//#endif



//    public static Func<Binding, EndpointAddress, ChannelFactory> GetCreateChannelFactoryDelegate(Type factoryContract)
//    {
//        try
//        {
//            var ctor = factoryContract.GetConstructor(new Type[] { typeof(Binding), typeof(EndpointAddress) });

//            ParameterExpression paramBinding = Expression.Parameter(typeof(Binding));
//            ParameterExpression paramEndpoint = Expression.Parameter(typeof(EndpointAddress));

//            var expr = Expression.Lambda<Func<Binding, EndpointAddress, ChannelFactory>>(
//                                         Expression.Convert(
//                                                    Expression.New(ctor, paramBinding, paramEndpoint), typeof(ChannelFactory)),

//                                    paramBinding, paramEndpoint);


//            return expr.Compile();
//        }
//        catch (Exception)
//        {
//            throw;
//        }
//    }

//    public static Func<Binding, EndpointAddress, ChannelFactory> GetCreateChannelFactoryFromConfigDelegate(Type factoryContract)
//    {
//        try
//        {
//            var ctor = factoryContract.GetConstructor(new Type[] { typeof(String) });

//            ParameterExpression paramConfigName = Expression.Parameter(typeof(String));


//            var expr = Expression.Lambda<Func<Binding, EndpointAddress, ChannelFactory>>(
//                                         Expression.Convert(
//                                                    Expression.New(ctor, paramConfigName), typeof(ChannelFactory)),

//                                    paramConfigName);


//            return expr.Compile();
//        }
//        catch (Exception)
//        {
//            throw;
//        }
//    }





//    /// <summary>
//    /// Creating delegate of Channel  of this Client's Factory instance
//    /// </summary>
//    /// <param name="FactoryInstance"></param>
//    /// <returns></returns>
//    public static Func<ICommunicationObject> GetCreateChannelDelegate(object FactoryInstance)
//    {
//        try
//        {
//            var tp = FactoryInstance.GetType();

//            MethodInfo createChannel = tp.GetMethod("CreateChannel", BindingFlags.Public | BindingFlags.Instance, null, Type.EmptyTypes, null);

//            ConstantExpression instance = Expression.Constant(FactoryInstance, typeof(ChannelFactory));
//            //ParameterExpression instance = Expression. Parameter(typeof(ChannelFactory), "instance");

//            Expression block = Expression.Block(
//                                                  Expression.Convert(
//                                                         Expression.Call(
//                                                                Expression.Convert(instance, tp), createChannel)
//                                                                    , typeof(ICommunicationObject)
//                                                                    )
//                                               //, instance
//                                               );

//            var expr = Expression.Lambda<Func<ICommunicationObject>>(block); //ChannelFactory,

//            return expr.Compile();
//        }
//        catch (Exception)
//        {
//            throw;
//        }

//    }

//}

#endregion --------------------------------------- NESTED TYPES ----------------------------------------


#region --------------------------------- SERVICE CONTRACT CHECKS -----------------------------------


//        static bool CheckIsSvcContract(Type serviceType)
//        {
//            var scontract = serviceType.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
//            Validator.AssertTrue<InvalidOperationException>(scontract == null,
//                string.Format("serviceType - [{0}]  that you are going to use as ServiceContract  must have valid ServiceContractAttribute ", serviceType.FullName));

//            return true;
//        }


//        static bool CheckIsSvcContractSupportDynamicCommands(Type serviceType)
//        {
//#if SL5 || WP81

//            var mthdBeginExist = serviceType.GetMethod("BeginExecuteCommand");
//            var mthdEndExist = serviceType.GetMethod("EndExecuteCommand");

//            Validator.AssertFalse<InvalidCastException>((mthdBeginExist != null && mthdEndExist != null),
//                String.Format("serviceType - [{0}] it doesnt have one or both methods BeginExecuteCommand and EndExecuteComand ", serviceType.FullName));

//#elif WPF
//            var mthdExist = serviceType.GetMethod("ExecuteCommand");

//            Validator.AssertFalse<InvalidOperationException>((mthdExist != null),
//               string.Format("serviceType - [{0}] it doesnt have method ExecuteCommand", serviceType.FullName));

//#endif

//            return true;
//        }


#endregion --------------------------------- SERVICE CONTRACT CHECKS -----------------------------------


/// <summary>
/// If Factory will be Fault  then we can Recrete new and call next command by this Service
/// </summary>
//void CheckRecreateFactoryIfFaulted()
//{
//    if (Factory != null && Factory.State == CommunicationState.Faulted)
//    {
//        lock (_locker)
//        {
//            if (Factory != null && Factory.State == CommunicationState.Faulted)
//            {
//                Factory.Abort();
//                Factory = null;
//                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);
//            }
//        }
//    }
//}

/// <summary>
/// There are 3 mode of using TSSSerializer(only for WPF client, other clients have only two ) : 
/// <para/>     1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
/// <para/>     2 mode - integrated into DCMessage data transmitting- here  we first pack DCMessage data into its Body property,
///     and then use DataContractSerialization as usual to serialize DCMessage as usual DataContract, and mirrored sequence on the server.
///     To use that mode set UseTSSSerializer=true on service creation 
/// <para/>     3 mode - direct TSS serialization -  when we only use TSSSerializer without DataContractSerializer. 
///     It means also that we use TSSSerializationBehavior on the client(WPF) and on the Server.
///     On the client(WPF) you need to call SetTSSDirectSerializationBehavior() or set UseTSSDirectSerialization= true on service client creation.
/// 
///<para/>  About modes : 1 and  2 and 3 mode exclude each other         
///<para/>  This flag, in true, show to us that we use 3 mode- direct TSS serialization.
/// </summary>
//public bool UseTSSDirectSerialization
//{
//    get;
//    protected set;
//}




/// <summary>
/// AddKnownTypes from custom KnowTypesLoader On  Client  Initializing
/// </summary>
/// <param name="knownTypesLoaderFunc"></param>
/// <param name="useTSSerializer"></param>
/// <param name="useTSSDirectSerialization"></param>
//void LoadKnowTypesFromKnowTypesLoader(Func<IEnumerable<Type>> knownTypesLoaderFunc, bool useTSSerializer, bool useTSSDirectSerialization)
//{
//    UseTSSerializer = useTSSerializer;
//    UseTSSDirectSerialization = useTSSDirectSerialization;

//    if (!ExecuteCommandOperation.KnownTypes.Contains(typeof(DCMessage)))
//    {
//        ExecuteCommandOperation.KnownTypes.Add(typeof(DCMessage));
//    }

//    if (knownTypesLoaderFunc != null)
//    {
//        foreach (var knownType in knownTypesLoaderFunc())
//        {
//            ExecuteCommandOperation.KnownTypes.Add(knownType);
//        }
//    }

//    if (UseTSSerializer || UseTSSDirectSerialization)
//    {
//        TSSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(ServiceKey);
//        //TSSerializer.Mapping.UseConventionsProcessing = true;
//        //TSSerializer.Mapping.UseTypeFullNameOnlyCompareMapping = true;

//        foreach (var knownType in ExecuteCommandOperation.KnownTypes)
//        {
//            TSSerializer.AddKnownType(knownType); // auto adding contract or Custom Complex structure contract                    
//        }

//        //TSSerializer.BuildProcessingInfo();
//    }

//    if (UseTSSDirectSerialization)
//    {
//        SetTSSDirectSerializationBehavior();
//    }

//}






/// <summary>
/// There are 3 mode of using TSSSerializer(only for WPFclient , other clients have only two ) : 
///     1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
///     2 mode - integrated into DCMessage data transmitting- here  we first pack DCMessage data into its Body property,
///     and then use DataContractSerialization as usual to serialize DCMessage as usual DataContract, and mirrored sequence on the server.
///     To use that mode set UseTSSSerializer=true on service creation 
///     3 mode - direct TSS serialization -  when we only use TSSSerializer without DataContractSerializer. 
///     It means also that we use TSSSerializationBehavior on the client(WPF) and on the Server.
///     On the client(WPF) you need to call SetTSSDirectSerializationBehavior() or set UseTSSDirectSerialization= true on service creation.
///          
/// To set working mode into 3 mode- direct TSS serialization - call this method
/// </summary>
//protected void SetTSSDirectSerializationBehavior()
//{

//    var dataContractBehaviors = ExecuteCommandOperation.Behaviors.Where(bhvr => bhvr.GetType().Name.Contains("DataContractSerializerOperation"));

//    TSSOperationBehavior newSdsBehavior = new TSSOperationBehavior(ExecuteCommandOperation);

//    if (dataContractBehaviors != null && dataContractBehaviors.Count() > 0)
//    {
//        while (dataContractBehaviors.Count() > 0)
//        {
//            var bhvr = dataContractBehaviors.First();
//            if (bhvr is DataContractSerializerOperationBehavior)
//            {
//                newSdsBehavior.MaxItemsInObjectGraph = (bhvr as DataContractSerializerOperationBehavior).MaxItemsInObjectGraph;
//            }
//            ExecuteCommandOperation.Behaviors.Remove(bhvr);
//        }
//    }


//    ExecuteCommandOperation.Behaviors.Add(newSdsBehavior);

//    UseTSSDirectSerialization = true;
//}


///// <summary>
///// Create  ChannelFactory of <ServiceContract> 
///// </summary>
//protected Func<Binding, EndpointAddress, ChannelFactory> CreateChannelFactoryDelegate;//ChannelFactory

///// <summary>
///// Create New Channel from current Factory Function  
///// </summary>
//protected Func<ICommunicationObject> CreateChannelDelegate; //Chanel   ChannelFactory,




///// <summary>
///// Simply return by reflection ServiceEndpoint from Factory Instance
///// </summary>
///// <param name="factory"></param>
///// <returns></returns>
//protected static ServiceEndpoint GetDSEndpoint(ChannelFactory factory)
//{
//    try
//    {
//        var props = factory.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
//        var endpProp = props.Where(prp => prp.Name == "Endpoint").FirstOrDefault();

//        return (ServiceEndpoint)endpProp.GetValue(factory, BindingFlags.NonPublic, null, null, null);

//    }
//    catch (Exception)
//    {
//        throw;
//    }
//}


/// <summary>
/// You can add known types by this method.It's the same as well known ServiceKnownContract type declaration feature in WCF.
/// </summary>
/// <param name="KnownContracts"></param>
//public void AddKnownTypesToTSSSerializer(params Type[] KnownContracts)
//{
//    if (KnownContracts == null) return;

//    foreach (var knownType in KnownContracts)
//    {
//        ExecuteCommandOperation.KnownTypes.Add(knownType);

//        if (UseTSSerializer)
//        {
//            TSSerializer.AddKnownType(knownType); // auto detecting -auto adding contract or custom complex struct type                    
//        }
//    }
//}



///// <summary>
///// AddKnownTypes from custom KnowTypesLoader On  Client  Initializing
///// </summary>
///// <param name="knownTypesLoaderFunc"></param>
//void LoadKnowTypesFromKnowTypesLoader(Func<IEnumerable<Type>> knownTypesLoaderFunc, bool useTSSSerializer)
//{
//    UseTSSerializer = useTSSSerializer;

//    if (!ExecuteCommandOperation.KnownTypes.Contains(typeof(DCMessage)))
//    {
//        ExecuteCommandOperation.KnownTypes.Add(typeof(DCMessage));
//    }


//    if (knownTypesLoaderFunc != null)
//    {
//        foreach (var knownType in knownTypesLoaderFunc())
//        {
//            ExecuteCommandOperation.KnownTypes.Add(knownType);
//        }
//    }


//    if (UseTSSerializer)
//    {
//        TSSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(Key);
//        //ClientsTSSerializer.Mapping.UseConventionsProcessing = true;
//        //ClientsTSSerializer.Mapping.UseTypeFullNameOnlyCompareMapping = true;

//        foreach (var knownType in ExecuteCommandOperation.KnownTypes)
//        {
//            TSSerializer.AddKnownType(knownType);
//        }
//    }

//}



/// <summary>
/// AddKnownTypes from custom KnowTypesLoader On  Client  Initializing
/// </summary>
/// <param name="knownTypesLoaderFunc"></param>
/// <param name="useTSSerializer"></param>
/// <param name="useTSSDirectSerialization"></param>
//void LoadKnowTypesFromKnowTypesLoader(Func<IEnumerable<Type>> knownTypesLoaderFunc, bool useTSSerializer, bool useTSSDirectSerialization)
//{
//    UseTSSerializer = useTSSerializer;
//    UseTSSDirectSerialization = useTSSDirectSerialization;

//    if (!ExecuteCommandOperation.KnownTypes.Contains(typeof(DCMessage)))
//    {
//        ExecuteCommandOperation.KnownTypes.Add(typeof(DCMessage));
//    }

//    if (knownTypesLoaderFunc != null)
//    {
//        foreach (var knownType in knownTypesLoaderFunc())
//        {
//            ExecuteCommandOperation.KnownTypes.Add(knownType);
//        }
//    }

//    if (UseTSSerializer || UseTSSDirectSerialization)
//    {
//        TSSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(ServiceKey);
//        //TSSerializer.Mapping.UseConventionsProcessing = true;
//        //TSSerializer.Mapping.UseTypeFullNameOnlyCompareMapping = true;

//        foreach (var knownType in ExecuteCommandOperation.KnownTypes)
//        {
//            TSSerializer.AddKnownType(knownType); // auto adding contract or Custom Complex structure contract                    
//        }

//        //TSSerializer.BuildProcessingInfo();
//    }

//    if (UseTSSDirectSerialization)
//    {
//        SetTSSDirectSerializationBehavior();
//    }

//}






/// <summary>
/// There are 3 mode of using TSSSerializer(only for WPFclient , other clients have only two ) : 
///     1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
///     2 mode - integrated into DCMessage data transmitting- here  we first pack DCMessage data into its Body property,
///     and then use DataContractSerialization as usual to serialize DCMessage as usual DataContract, and mirrored sequence on the server.
///     To use that mode set UseTSSSerializer=true on service creation 
///     3 mode - direct TSS serialization -  when we only use TSSSerializer without DataContractSerializer. 
///     It means also that we use TSSSerializationBehavior on the client(WPF) and on the Server.
///     On the client(WPF) you need to call SetTSSDirectSerializationBehavior() or set UseTSSDirectSerialization= true on service creation.
///          
/// To set working mode into 3 mode- direct TSS serialization - call this method
/// </summary>
//protected void SetTSSDirectSerializationBehavior()
//{

//    var dataContractBehaviors = ExecuteCommandOperation.Behaviors.Where(bhvr => bhvr.GetType().Name.Contains("DataContractSerializerOperation"));

//    TSSOperationBehavior newSdsBehavior = new TSSOperationBehavior(ExecuteCommandOperation);

//    if (dataContractBehaviors != null && dataContractBehaviors.Count() > 0)
//    {
//        while (dataContractBehaviors.Count() > 0)
//        {
//            var bhvr = dataContractBehaviors.First();
//            if (bhvr is DataContractSerializerOperationBehavior)
//            {
//                newSdsBehavior.MaxItemsInObjectGraph = (bhvr as DataContractSerializerOperationBehavior).MaxItemsInObjectGraph;
//            }
//            ExecuteCommandOperation.Behaviors.Remove(bhvr);
//        }
//    }


//    ExecuteCommandOperation.Behaviors.Add(newSdsBehavior);

//    UseTSSDirectSerialization = true;
//}     

#endregion ----------------------------------- GARBAGE ---------------------------------