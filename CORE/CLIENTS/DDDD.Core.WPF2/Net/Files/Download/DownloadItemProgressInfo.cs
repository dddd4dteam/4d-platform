﻿using System;

using DDDD.Core.Notification;

namespace DDDD.Core.Net.Files.Download
{
    /// <summary>
    /// Downloading  Item progress info
    /// </summary>
    public class DownloadItemProgressInfo : ILoadDataOperationProgressInfo, ICloneable
    {

        /// <summary>
        /// 
        /// </summary>
        public DownloadTargetEn DownloadTarget
        { get; internal set; }

        long _BytesDownloaded;// = 0;
        /// <summary>
        /// Counter of DownloadedBytes
        /// </summary>
        public long BytesDownloaded
        {
            get
            {
                return _BytesDownloaded;
            }
            internal set
            {
                _BytesDownloaded = value;
                if (_BytesDownloaded != 0)
                {
                    PercentComplete = TotalBytesToDownload < 0 ? 0 : TotalBytesToDownload == 0 ? 100 : (int)((100 * BytesDownloaded) / TotalBytesToDownload);
                }
            }
        }
        // = 0;

        
        public long TotalBytesToDownload
        {
            get; internal set;
        }


        /// <summary>
        /// 
        /// </summary>
        public double PercentComplete
        {
            get; internal set;
        }

        /// <summary>
        /// Amount of KBt/sec
        /// </summary>
        public double TransferRate
        {
            get; private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public DateTime TransferStartTime
        {
            get; internal set;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool? Succeeded
        {
            get; internal set;
        }


        /// <summary>
        /// Exception on Downloading that can be 
        /// </summary>
        public string ExceptionMessage
        {
            get; internal set;
        }

        public string ProgressMessage
        {
            get; internal set;
        }

        /// <summary>
        /// If Download Task was cancelled
        /// </summary>
        public bool Cancelled
        {
            get; internal set;
        }


        public string TaskTitle
        {
            get; internal set;
        }


        public void ReCalculateTransferRate()
        {
            TimeSpan totalTime = DateTime.Now - TransferStartTime;
            TransferRate = (BytesDownloaded * 1000.0f) / (totalTime.TotalMilliseconds * 1024.0f);
        }


        public void Reset()
        {
            BytesDownloaded = 0;
            TotalBytesToDownload = -1;
            PercentComplete = 0;
            TransferStartTime = DateTime.Now;
        }


        public object Clone()
        {
            return MemberwiseClone();
        }

    }


}
