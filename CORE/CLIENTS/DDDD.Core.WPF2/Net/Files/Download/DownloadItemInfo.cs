﻿using System;
using System.IO;

namespace DDDD.Core.Net.Files.Download
{

    /// <summary> 
    /// Download Item info - base properties of downloading Item. 
    /// <para/> This INFO structure used in CurrentItem and PreviosItem properties.
    /// </summary>
    public class DownloadItemInfo
    {
        /// <summary>
        /// 
        /// </summary>
        //public static DownloadItemInfo Empty
        //{
        //    get { return default(DownloadItemInfo); }
        //}

        /// <summary>
        /// Target download file or some data uri 
        /// </summary>
        public Uri TargetDownloadUri
        {
            get; internal set;
        }

        /// <summary>
        /// Target Saving File Path if we loading file into the Hard Drive File System.
        /// </summary>
        public string TargetSaveFilePath
        {
            get; internal set;
        }



        string _TargetFileName;
        /// <summary>
        /// Target File Name it only file name without full save path. 
        /// <para/> It won't be null if we set TargetSaveFilePath correctly
        /// </summary>
        public string TargetFileName
        {
            get
            {
                if (_TargetFileName == null && TargetSaveFilePath != null)
                {
                    _TargetFileName = Path.GetFileName(TargetSaveFilePath);
                }

                return TargetFileName;
            }
        }


        /// <summary>
        /// Target download data item size in bytes.
        /// </summary>
        public long TargetDownloadDataItemSize
        {
            get; internal set;
        }

    }


}
