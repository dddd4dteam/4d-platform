﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Net
{


    /// <summary>
    /// Dynamic Command Communication Known Scenarios.
    /// </summary>
    public class DCCScenarios
    {
        /// <summary>
        /// Declaration that Target Host Entity needn't any communication. Logically incorrect state. 
        /// </summary>
       public const  string NeedNotCommunication = nameof(NeedNotCommunication);


        /// <summary>
        ///  [Wcf_EnvironmentActivationSvc] 
        ///  Wcf Communication Service- *.svc map file not used, but service is still coming with the same ServiceHostFactory class 
        ///    and we can use web.config to customize its binding, behaviors. 
        /// <para/> In this case( EnvironmentActivation - concept word in enum field Name )  -it'll have its activation register inside [system.serviceModel]\[serviceHostingEnvironment]\[serviceActivations]\[add ...]  configuration elements in WebConfig.
        ///  Web.Config will be changed automatically on ApplicationStart and only once - if ServiceActivation with such relativeAddress won't be found.
        /// <para/>After service configuration will be added to web.config(after first time app run), you can modify it's default behavior and binding- it won't be rewritten on next time app load.
        /// </summary>     
        public const string Wcf_EnvironmentActivationSvc = nameof(Wcf_EnvironmentActivationSvc);


        /// <summary>
        ///  [WebApiDirectHandlers]    
        ///  WebApi Direct Handlers Communication - Means that DC Manager will initialize the creation of its own DCWebApiDirectHandler to communicate and work with it.
        /// </summary>
        public const string WebApiDirectHandlers = nameof(WebApiDirectHandlers);


        /// <summary>
        ///  [WebApiControllers]    
        ///  WebApi Communication Controller - Means that DC Manager will initialize the creation of its own DCWebApiController to communicate and work with it.
        /// </summary>
        public const string WebApiControllers = nameof(WebApiControllers);



        /// <summary>
        /// Wcf_EnvironmentActivationREST
        /// Wcf Communication Service - service can be called with REST notation,  and service is coming here with the WebServiceHostFactory class. 
        /// <para/> In this case( EnvironmentActivation - concept word in enum field Name )  - service'll have its activation register inside [system.serviceModel]\[serviceHostingEnvironment]\[serviceActivations]\[add ...]  configuration elements in WebConfig.
        /// Web.Config will be changed automatically on ApplicationStart and only once - if ServiceActivation with such relativeAddress won't be found.
        /// <para/> After service configuration will be added to web.config(after first time app run), you can modify its default behavior and binding- it won't be rewritten on next time app load.
        /// </summary>
        public const string Wcf_EnvironmentActivationREST = nameof(Wcf_EnvironmentActivationREST);


        // FOLLOWING OPTIONALS


        ///// <summary>
        /////  HttpHandler Communication Service - service can be called by some registered address and for some content
        ///// </summary>
        //public const string HttpHanderCommunication = nameof(HttpHanderCommunication);
        

        ///// <summary>
        ///// MvcCommunication - Means that DC Manager will initialize the creation of its own DCMvcController to communicate and work with it.
        ///// </summary>          
        //public const string MvcCommunication = nameof(MvcCommunication);


        ///// <summary>
        ///// SignalRCommunication - Means that DC Manager will initialize the creation of its own DCSignalRController to communicate and work with it.
        ///// </summary>
        //public const string SignalRCommunication = nameof(SignalRCommunication);


        ///// <summary>
        ///// ThriftCommunication - Means that DC Manager will initialize the creation of its own DCThriftService to communicate and work with it.
        ///// </summary>
        //public const string ThriftCommunication = nameof(ThriftCommunication);

    }


}
