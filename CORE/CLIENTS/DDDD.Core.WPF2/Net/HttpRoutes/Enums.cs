﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.HttpRoutes
{
    /// <summary>
    /// Templates that we can use to build target Uri for Wcf services, WebApi/Mvc_Controllerand their Actions  
    /// </summary>
    public enum HttpRouteTemplateEn
    {
        // ---------
        // WebAPi/MVC Controllers actions templates
        // ---------

        /// <summary>
        /// HTTP Address Template: [webapi/{controller}]. WebAPi/MVC Controllers actions template.
        /// </summary>
        [HttpRouteTemplate(@"webapi/{controller}")]
        HttpRoute_Contrlr

        ,

        /// <summary>
        /// HTTP Address Template: [webapi/{controller}/{action}]. WebAPi/MVC Controllers actions template.
        /// </summary>
        [HttpRouteTemplate(@"webapi/{controller}/{action}")]
        HttpRoute_ContrlrAct


        ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{communication}/{controller}]. WebAPi/MVC Controllers.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{communication}/{controller}")]
        HttpRoute_BaseurlCommunicationContrlr


         ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{communication}/{controller}/{action}]. WebAPi/MVC Controllers.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{communication}/{controller}/{action}")]
        HttpRoute_BaseurlCommunicationContrlrAct




        // -----
        // WCF Services Clients templates
        // -----

        ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{service}.svc]. WCF Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{service}.svc")]
        HttpRoute_BaseurlServiceSvc

        , 
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{webapplication}/{subfolder}/{service}.svc]. WCF Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{webapplication}/{subfolder}/{service}.svc")]
        HttpRoute_BaseurlWebappSubfldrServiceSvc

        ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{subfolder}/{service}.svc]. WCF Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{subfolder}/{service}.svc")]
        HttpRoute_BaseurlSubfldrServiceSvc

        ,
        /// <summary>
        /// HTTP Address Template: [http://{server}:{port}/{service}.svc]. WCF Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"http://{server}:{port}/{service}.svc")]
        HttpRoute_SrvPortServiceSvc

        
        ,
        /// <summary>
        /// HTTP Address Template: [http://{server}:{port}/{subfolder}/{service}.svc]. WCF Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"http://{server}:{port}/{subfolder}/{service}.svc")]
        HttpRoute_SrvPortSubfldrServiceSvc

        ,
        /// <summary>
        /// HTTP Address Template: [http://{server}:{port}/{webapplication}/{subfolder}/{service}.svc]. WCF Services Clients templates
        /// </summary>
        [HttpRouteTemplate(@"http://{server}:{port}/{webapplication}/{subfolder}/{service}.svc")]
        HttpRoute_SrvPortWebappSubfldrServiceSvc


        // -----
        // WCF Route Services Clients action call templates
        // ----
        
        ,
        
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{service}/{action}/]. WCF Route Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{service}/{action}/")]
        HttpRoute_BaseurlRtServiceAct

        ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{webapplication}/{service}/{action}/]. WCF Route Services Clients template.
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{webapplication}/{service}/{action}/")]
        HttpRoute_BaseurlWebappRtServiceAct

      

        ,
        /// <summary>
        /// HTTP Address Template: [http://{server}:{port}/{webapplication}/{service}/{action}/]. WCF Route Services Clients template.
        /// </summary> 
        [HttpRouteTemplate(@"http://{server}:{port}/{webapplication}/{service}/{action}/")]
        HttpRoute_SrvPortWebappRtServiceAct


        ,
        /// <summary>
        /// HTTP Address Template: [http://{server}:{port}/{service}/{action}/].
        /// </summary>
        [HttpRouteTemplate(@"http://{server}:{port}/{service}/{action}/")]
        HttpRoute_SrvPortRtServiceAct



        ///-----
        /// Routes WITH COMMUNICATION Prefix -{communication}
        ///-----

        ,
        /// <summary>
        /// HTTP Address Template: [{baseurl}/{communication}/{service}.svc]. 
        /// Communication will be one of the following values: wcf
        /// </summary>
        [HttpRouteTemplate(@"{baseurl}/{communication}/{service}.svc")]
        HttpRoute_BaseurlCommunicationServiceSvc

    }



}
