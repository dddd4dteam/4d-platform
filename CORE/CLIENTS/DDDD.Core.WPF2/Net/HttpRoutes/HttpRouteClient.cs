﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using DDDD.Core.Extensions;


namespace DDDD.Core.HttpRoutes
{


    /// <summary>
    /// Http  address segment in route template
    /// </summary>
    public class HttpRouteSegment
    {
        public Int32 Index { get; set; }

        public string Segment { get; set; }

        public string Replaced { get; set; }

        public static HttpRouteSegment Create(int index, string Segment, string Replaced)
        {
            HttpRouteSegment segmnt = new HttpRouteSegment();
            segmnt.Index = index;
            segmnt.Segment = Segment;
            segmnt.Replaced = Replaced;

            return segmnt;
        }
    }


    public enum RouteKeysEn
    {
        /// <summary>
        /// template const -  [{server}]
        /// </summary>
        Server
       ,
        /// <summary>
        /// template const -  [{port}]
        /// </summary>
        Port
       ,
        /// <summary>
        /// template const -  [{webapplication}]
        /// </summary>
        WebApplication
       ,
        /// <summary>
        /// template const -  [{communication}]. It also known as/ Represented as CommunicationPrefix.
        /// </summary>
        Communication
       ,
        /// <summary>
        /// template const -  [{subfolder}]
        /// </summary>
        Subfolder
       ,
        /// <summary>
        /// template const -  [{baseurl}]
        /// </summary>
        BaseUrl
       ,
        /// <summary>
        /// template const -  [{controller}]
        /// </summary>
        Controller
       ,
        /// <summary>
        /// template const -  [{service}] 
        /// </summary>
        Service
       ,
        /// <summary>
        /// template const -  [{action}] 
        /// </summary>
        Action

    }


    ///<summary>
    ///  HttpRoute2 class -  to describe WebApi/MVC  controllers and simplier use their API , also WCF adress mapping
    ///</summary>   
    public class HttpRoute2
    {
        

        static HttpRoute2()
        {
            StartUrl = new BaseUrl();
        }


        /// <summary>
        /// Custom Crossplatform Url. 
        /// </summary>
        public static BaseUrl StartUrl
        {
            get;
            private set;
        }

        Dictionary<int, HttpRouteSegment> SegmentsValues = new Dictionary<int, HttpRouteSegment>();

        static Dictionary<RouteKeysEn, string> RKeys
        { get; } =   new Dictionary<RouteKeysEn, string>()
                    {
                         {RouteKeysEn.Server, "{server}"}
                        ,{RouteKeysEn.Port, "{port}"}
                        ,{RouteKeysEn.WebApplication, "{webapplication}"}
                        ,{RouteKeysEn.Communication, "{communication}"}
                        ,{RouteKeysEn.Subfolder, "{subfolder}"}
                        ,{RouteKeysEn.BaseUrl, "{baseurl}"}
                        ,{RouteKeysEn.Controller, "{controller}"}
                        ,{RouteKeysEn.Service, "{service}"}
                        ,{RouteKeysEn.Action, "{action}"}
                    };


        static List<RouteKeysEn> LowerLetters
        { get; } = new List<RouteKeysEn>()
                        { RouteKeysEn.Controller
                        , RouteKeysEn.Action
                        , RouteKeysEn.Communication };



        /// <summary>
        /// Last Action-Http query Uri.
        /// </summary>
        public string LastActionUri
        {
            get;
            private set;
        }


        /// <summary>
        /// Split Uri path into segments.
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        internal static IList<string> SplitUriToPathSegmentStrings(string uri)
        {
            List<string> parts = new List<string>();

            if (String.IsNullOrEmpty(uri))
            {
                return parts;
            }

            int currentIndex = 0;

            // Split the incoming URI into individual parts
            while (currentIndex < uri.Length)
            {
                int indexOfNextSeparator = uri.IndexOf('/', currentIndex);
                if (indexOfNextSeparator == -1)
                {
                    // If there are no more separators, the rest of the string is the last part
                    string finalPart = uri.Substring(currentIndex);
                    if (finalPart.Length > 0)
                    {
                        parts.Add(finalPart);
                    }
                    break;
                }

                string nextPart = uri.Substring(currentIndex, indexOfNextSeparator - currentIndex);
                if (nextPart.Length > 0)
                {
                    parts.Add(nextPart);
                }

                Contract.Assert(uri[indexOfNextSeparator] == '/', "The separator char itself should always be a '/'.");
                parts.Add("/");
                currentIndex = indexOfNextSeparator + 1;
            }

            return parts;
        }

        /// <summary>
        /// Compare - if parts of HttpRoute are equal.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private static bool RoutePartsEqual(object a, object b)
        {
            string sa = a as string;
            string sb = b as string;
            if (sa != null && sb != null)
            {
                // For strings do a case-insensitive comparison
                return string.Equals(sa, sb, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                if (a != null && b != null)
                {
                    // Explicitly call .Equals() in case it is overridden in the type
                    return a.Equals(b);
                }
                else
                {
                    // At least one of them is null. Return true if they both are
                    return a == b;
                }
            }
        }


        private static string GetLiteral(string segmentLiteral)
        {
            // Scan for errant single { and } and convert double {{ to { and double }} to }

            // First we eliminate all escaped braces and then check if any other braces are remaining
            string newLiteral = segmentLiteral.Replace("{{", string.Empty).Replace("}}", String.Empty);
            if (newLiteral.Contains("{") || newLiteral.Contains("}"))
            {
                return null;
            }

            // If it's a valid format, we unescape the braces
            return segmentLiteral.Replace("{{", "{").Replace("}}", "}");
        }


       
        private static int IndexOfFirstOpenParameter(string segment, int startIndex)
        {
            // Find the first unescaped open brace
            while (true)
            {
                startIndex = segment.IndexOf('{', startIndex);
                if (startIndex == -1)
                {
                    // If there are no more open braces, stop
                    return -1;
                }
                if ((startIndex + 1 == segment.Length) ||
                    ((startIndex + 1 < segment.Length) && (segment[startIndex + 1] != '{')))
                {
                    // If we found an open brace that is followed by a non-open brace, it's
                    // a parameter delimiter.
                    // It's also a delimiter if the open brace is the last character - though
                    // it ends up being being called out as invalid later on.
                    return startIndex;
                }

                // Increment by two since we want to skip both the open brace that
                // we're on as well as the subsequent character since we know for
                // sure that it is part of an escape sequence.
                startIndex += 2;
            }
        }
        


        internal static bool IsSeparator(string s)
        {
            return string.Equals(s, "/", StringComparison.Ordinal);
        }


        private static bool IsValidParameterName(string parameterName)
        {
            if (parameterName.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < parameterName.Length; i++)
            {
                char c = parameterName[i];
                if (c == '/' || c == '{' || c == '}')
                {
                    return false;
                }
            }

            return true;
        }



        private static bool IsValidParameters(IList<string> parameterNames)
        {
            if (parameterNames.Count == 0)
            {
                return false;
            }

            foreach (var item in parameterNames)
            {
                if (IsValidParameterName(item) == false) return false;
            }

            return true;
        }



        internal static bool IsInvalidRouteTemplate(string routeTemplate)
        {
            return routeTemplate.StartsWith("~", StringComparison.Ordinal) ||
                   routeTemplate.StartsWith("/", StringComparison.Ordinal) ||
                   (routeTemplate.IndexOf('?') != -1);
        }


        /// <summary>
        /// Try Parse new ropute Path
        /// </summary>
        /// <param name="NewRoutePath"></param>
        /// <returns></returns>
        public static HttpRoute2 TryParseRoutePath(string NewRoutePath)
        {
            if (string.IsNullOrEmpty(NewRoutePath)) return null;


            IList<string> Elements = SplitUriToPathSegmentStrings(NewRoutePath);

            Contract.Assert(!IsValidParameters(Elements), "Path Elemnts not correctly formed");

            HttpRoute2 newRoute = new HttpRoute2();

            Int32 counter = 0;
            foreach (var elmnt in Elements)
            {
                newRoute.SegmentsValues.Add(counter, HttpRouteSegment.Create(counter, elmnt, elmnt));
                counter++;
            }

            return newRoute;
        }


        /// <summary>
        /// Update Route Path 
        /// </summary>
        /// <param name="UpdatingRouteValues"></param>
        /// <returns></returns>
        public HttpRoute2 UpdateRoutePath(Dictionary<string, string> UpdatingRouteValues)
        {

            foreach (var newElement in UpdatingRouteValues)
            {
                UpdateRouteElement(this, newElement);
            }

            return this;
        }



        void UpdateRouteElement(HttpRoute2 route, KeyValuePair<String, String> newElementValue)
        {
            IEnumerable<HttpRouteSegment> findedSegments = route.SegmentsValues.Where(sg => sg.Value.Segment.Contains(newElementValue.Key))
                                                                              .Select(itm => itm.Value);

            foreach (var fndSegmnt in findedSegments)
            {
                string replacementKeyValue = newElementValue.Key; //build standartKey 
                if (!replacementKeyValue.Contains("{")) replacementKeyValue = "{" + replacementKeyValue;
                if (!replacementKeyValue.Contains("}")) replacementKeyValue = replacementKeyValue + "}";

                fndSegmnt.Replaced = fndSegmnt.Replaced.Replace(replacementKeyValue, newElementValue.Value);
            }


        }



        static void ReplaceBaseurlSegment(ref string actionUriTarget)
        {

            if (actionUriTarget.IsNotContains(RKeys[RouteKeysEn.BaseUrl])) return;


#if SL5
            //replace Starturl
            if (actionUriTarget.Contains( RKeys[RouteKeysEn.BaseUrl] ) && !string.IsNullOrEmpty(StartUrl.Url))
            {
                if (StartUrl.Url[(StartUrl.Url.Length - 1)] == '/')
                {
                    actionUriTarget = actionUriTarget.Replace(RKeys[RouteKeysEn.BaseUrl], 
                                StartUrl.StartRootUri.OriginalString
                                .Remove(StartUrl.StartRootUri.OriginalString.Length - 1) ); //  Url.Remove(StartUrl.Url.Length - 1)  set StartUrl   not .ToLower()
                }
                else
                {
                    actionUriTarget = actionUriTarget.Replace(RKeys[RouteKeysEn.BaseUrl], StartUrl.StartRootUri.OriginalString );//  StartUrl.Url  set StartUrl  not  .ToLower()
                }
            }

#elif WPF || WP81
            
            //replace Starturl
            if (actionUriTarget.Contains( RKeys[RouteKeysEn.BaseUrl]) && !string.IsNullOrEmpty(StartUrl.DefaultServerUri.OriginalString))
            {
                if (StartUrl.DefaultServerUri.OriginalString[(StartUrl.DefaultServerUri.OriginalString.Length - 1)] == '/')
                {
                    actionUriTarget = actionUriTarget
                        .Replace( RKeys[RouteKeysEn.BaseUrl], StartUrl.DefaultServerUri.OriginalString
                        .Remove(StartUrl.DefaultServerUri.OriginalString.Length - 1));//set StartUrl   not .ToLower()
                }
                else
                {
                    actionUriTarget = actionUriTarget.Replace( RKeys[RouteKeysEn.BaseUrl] , StartUrl.DefaultServerUri.OriginalString);//set StartUrl  not  .ToLower()
                }
            }
#elif SERVER

            if (actionUriTarget.Contains(RKeys[RouteKeysEn.BaseUrl]))
            {
                actionUriTarget = actionUriTarget.Replace(RKeys[RouteKeysEn.BaseUrl], "~");
            }
#endif
        }

         

        public string BuildActionUri( KeyValuePair<RouteKeysEn, string>[] replaceValues , params string[] paramValues)
        {
            var lastActionUri = LastActionUri;

            foreach (var item in SegmentsValues)
            {
                if (!string.IsNullOrEmpty(item.Value.Replaced))
                {
                    lastActionUri += item.Value.Replaced;
                }
                else { lastActionUri += item.Value.Segment; } // '/' segmnts forexample
            }

            ReplaceBaseurlSegment(ref lastActionUri);  //default value of BaseUrl

            /// replacements
            foreach (var replacer in replaceValues)
            {
                if (replacer.Key == RouteKeysEn.BaseUrl)
                {
                    ReplaceBaseurlSegment(ref lastActionUri); // custom value of BaseUrl
                }
                else
                {
                    //replacer value
                    if (lastActionUri.Contains( RKeys[replacer.Key] ) && !replacer.Value.IsNullOrEmpty())
                    {
                        if (LowerLetters.Contains(replacer.Key))
                        {
                            lastActionUri = lastActionUri.Replace(RKeys[replacer.Key], replacer.Value.ToLower() );//set segment value
                        }
                        else
                        {
                            lastActionUri = lastActionUri.Replace(RKeys[replacer.Key], replacer.Value); //set segment Value
                        }                        
                    }
                }

            }


            //attach additinal parameters
            if (paramValues != null)
            {
                foreach (var item in paramValues)
                {
                    lastActionUri = lastActionUri + item + @"/";
                }
            }

            LastActionUri = lastActionUri;
            return LastActionUri;
        }








        /// <summary>
        /// Build  Uri for Service on Client or Server side.
        /// </summary>
        /// <param name="routeTemplate"></param>
        /// <param name="replaceValues"></param>
        /// <returns></returns>
        public static string BuildServiceUri(HttpRouteTemplateEn routeTemplate
                            , params KeyValuePair<RouteKeysEn, string>[] replaceValues)

            //, string ServiceName
            //, string Server = null, string Port = null, string CommunicationPrefix = null, String WebApplication = null, String Subfolder = null
        {

            var templateAttrib = routeTemplate.GetType()
                                .GetField(routeTemplate.S())
                                .GetCustomAttributes(typeof(HttpRouteTemplateAttribute), false)
                                .FirstOrDefault() as HttpRouteTemplateAttribute;
            

            var lastActionUri = templateAttrib.Tempalte;
             

            ////////////////////////////////////
            ReplaceBaseurlSegment(ref lastActionUri);

            //-- replacements
            foreach (var replacer in replaceValues)
            {
                if (replacer.Key == RouteKeysEn.BaseUrl)
                {
                    ReplaceBaseurlSegment(ref lastActionUri); // custom value of BaseUrl
                }
                else
                {
                    //replacer value
                    if (lastActionUri.Contains(RKeys[replacer.Key]) && !replacer.Value.IsNullOrEmpty())
                    {
                        if (LowerLetters.Contains(replacer.Key))
                        {
                            lastActionUri = lastActionUri.Replace(RKeys[replacer.Key], replacer.Value.ToLower());//set segment value
                        }
                        else
                        {
                            lastActionUri = lastActionUri.Replace(RKeys[replacer.Key], replacer.Value); //set segment Value
                        }
                    }
                }

            }

            return lastActionUri;
             
        }


    }



}




#region ----------------------------------- GARBAGE --------------------------------------
////replace controller
//if (LastActionUri.Contains(RouteKey_Controller) && !string.IsNullOrEmpty(ServiceName))
//{
//    LastActionUri = LastActionUri.Replace(RouteKey_Controller, ServiceName);
//}

////replace service
//if (LastActionUri.Contains(RouteKey_Service) && !string.IsNullOrEmpty(ServiceName))
//{
//    LastActionUri = LastActionUri.Replace( RouteKey_Service , ServiceName);
//}


////replace WebApplication 
//if (LastActionUri.Contains(RouteKey_WebApplication) && !string.IsNullOrEmpty(WebApplication))
//{
//    LastActionUri = LastActionUri.Replace( RouteKey_WebApplication , WebApplication);//set  WebApplicaion not ToLower
//}

////replace Communication
//if (LastActionUri.Contains(RouteKey_Communication) && !CommunicationPrefix.IsNullOrEmpty())
//{
//    LastActionUri = LastActionUri.Replace(RouteKey_Communication, CommunicationPrefix.ToLower());//set  Communication 
//}

////replace Subfolder 
//if (LastActionUri.Contains(RouteKey_Subfolder) && !string.IsNullOrEmpty(Subfolder))
//{
//    LastActionUri = LastActionUri.Replace( RouteKey_Subfolder , Subfolder);//set  Subfolder not ToLower
//}


////replace Port 
//if (LastActionUri.Contains(RouteKey_Port) && !string.IsNullOrEmpty(Port))
//{
//    LastActionUri = LastActionUri.Replace( RouteKey_Port, Port);//set Port
//}


////replace Server 
//if (LastActionUri.Contains(RouteKey_Server) && !string.IsNullOrEmpty(Server))
//{
//    LastActionUri = LastActionUri.Replace( RouteKey_Server , Server);//set Server  not ToLower
//}

//var somevarval = $"sgsdfgsdfgsdfg {RouteKey_Server}";

//#if CLIENT && SL5
//            //replace Starturl
//            if (LastActionUri.Contains(RouteKey_BaseUrl) && !string.IsNullOrEmpty(StartUrl.Url))
//            {
//                if (StartUrl.Url[(StartUrl.Url.Length - 1)] == '/')
//                {
//                    LastActionUri = LastActionUri.Replace( RouteKey_BaseUrl , StartUrl.Url.Remove(StartUrl.Url.Length - 1));//set StartUrl   not .ToLower()
//                }
//                else
//                {
//                    LastActionUri = LastActionUri.Replace( RouteKey_BaseUrl , StartUrl.Url);//set StartUrl  not  .ToLower()
//                }
//            }
//#elif CLIENT && (WPF || WP81)

//            if (LastActionUri.Contains(RouteKey_BaseUrl) && !String.IsNullOrEmpty(HttpRoute2.StartUrl.DefaultServerUri.OriginalString))
//            {
//                if (HttpRoute2.StartUrl.DefaultServerUri.OriginalString[(HttpRoute2.StartUrl.DefaultServerUri.OriginalString.Length - 1)] == '/')
//                {
//                    LastActionUri = LastActionUri.Replace( RouteKey_BaseUrl, HttpRoute2.StartUrl.DefaultServerUri.OriginalString.Remove(HttpRoute2.StartUrl.DefaultServerUri.OriginalString.Length - 1));//set StartUrl   not .ToLower()
//                }
//                else
//                {
//                    LastActionUri = LastActionUri.Replace( RouteKey_BaseUrl , HttpRoute2.StartUrl.DefaultServerUri.OriginalString);//set StartUrl  not  .ToLower()
//                }
//            }
//#elif SERVER

//            if (LastActionUri.Contains(RouteKey_BaseUrl) )
//            {

//                LastActionUri = LastActionUri.Replace(RouteKey_BaseUrl, "~");

//            }

//#endif

///// <summary>
///// [server] route key
///// </summary>
//public const string RouteKey_Server = "{server}";
///// <summary>
///// [port] route key
///// </summary>
//public const string RouteKey_Port = "{port}";

///// <summary>
///// [webapplication] route key
///// </summary>
//public const string RouteKey_WebApplication = "{webapplication}";

///// <summary>
///// [communication] route key
///// </summary>
//public const string RouteKey_Communication = "{communication}";

///// <summary>
///// [subfolder] route key
///// </summary>
//public const string RouteKey_Subfolder = "{subfolder}";


///// <summary>
///// [baseurl] route key
///// </summary>
//public const string RouteKey_BaseUrl = "{baseurl}";
///// <summary>
///// [controller] route key
///// </summary>
//public const string RouteKey_Controller = "{controller}";

///// <summary>
///// [service] route key for wcf services templates
///// </summary>
//public const string RouteKey_Service = "{service}";



///// <summary>
///// [action] route key 
///// </summary>
//public const string RouteKey_Action = "{action}";



///// <summary>
///// Build  Uri for Controller/Service Action
///// </summary>
///// <param name="BaseUrl"></param>
///// <param name="Controller"></param>
///// <param name="APIAction"></param>
///// <param name="paramValues"></param>
///// <returns></returns>
//public string BuildActionUri( string Controller, string APIAction, string communicationPrefix = null, string WebAplication = null, string Port = null, string Server = null,   params string[] paramValues)
//{
//   var  lastActionUri =  LastActionUri;

//    foreach (var item in SegmentsValues)
//    {
//        if (!string.IsNullOrEmpty(item.Value.Replaced))
//        {
//            lastActionUri += item.Value.Replaced;
//        }
//        else { lastActionUri += item.Value.Segment; } // '/' segmnts forexample
//    }


//    ////////////////////////////////////
//    ReplaceBaseurlSegment(ref lastActionUri);


//    //replace controller
//    if (lastActionUri.Contains(RouteKey_Controller) && !string.IsNullOrEmpty(Controller))
//    {
//        lastActionUri = lastActionUri.Replace( RouteKey_Controller, Controller.ToLower());
//    }


//    //replace APIAction 
//    if (lastActionUri.Contains(RouteKey_Action) && !APIAction.IsNullOrEmpty() )
//    {
//        lastActionUri = lastActionUri.Replace( RouteKey_Action , APIAction.ToLower() );//set Action
//    }


//    //replace WebApplication 
//    if (lastActionUri.Contains(RouteKey_WebApplication) && !WebAplication.IsNullOrEmpty() )
//    {
//        lastActionUri = lastActionUri.Replace(RouteKey_WebApplication , WebAplication);//set  WebApplicaion not ToLower
//    }

//    //replace communication
//    if (lastActionUri.Contains(RouteKey_Communication) && !communicationPrefix.IsNullOrEmpty() )
//    {
//        lastActionUri = lastActionUri.Replace(RouteKey_Communication, communicationPrefix.ToLower());//set  Communication 
//    }


//    //replace Port 
//    if (lastActionUri.Contains(RouteKey_Port) && !Port.IsNullOrEmpty())
//    {
//        lastActionUri = lastActionUri.Replace( RouteKey_Port, Port);//set Port
//    }


//    //replace Server 
//    if (lastActionUri.Contains(RouteKey_Server) && !string.IsNullOrEmpty(Server))
//    {
//        lastActionUri = lastActionUri.Replace( RouteKey_Server, Server);//set Server  not ToLower
//    }


//    //attach additinal parameters
//    if (paramValues != null)
//    {
//        foreach (var item in paramValues)
//        {
//            lastActionUri = lastActionUri + item + @"/";
//        }
//    }

//    LastActionUri = lastActionUri;
//    return lastActionUri;
//}



#endregion  ----------------------------------- GARBAGE --------------------------------------


