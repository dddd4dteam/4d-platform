﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.HttpRoutes
{
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class HttpRouteTemplateAttribute : Attribute
    {
        // See the attribute guidelines at 
        //  http://go.microsoft.com/fwlink/?LinkId=85236
        readonly string positionalString;

        // This is a positional argument
        public HttpRouteTemplateAttribute(string template)
        {
            this.Tempalte = template;
        }

        public string Tempalte
        {
            get;
            private set;
        }

    }

}
