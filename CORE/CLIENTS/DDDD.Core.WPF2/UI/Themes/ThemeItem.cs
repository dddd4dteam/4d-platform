﻿using System;

namespace DDDD.Core.UI.Themes
{
    /// <summary>
    /// Элемент темы - Словарь ресурсов для одного или нескольких типов Контролов/ Его состояние о загруженности/ О его необходимости загрузки/
    /// </summary>
    public struct ThemeItem
    {
        /// <summary>
        /// Тип Контрола
        /// </summary>
        //public Type ControlType;// = null;

        /// <summary>
        /// Файл сброрки - словарь ресурсов - для одного или нескольких контролов
        /// </summary>
        public PackageFileTp ResourceFile;

        /// <summary>
        /// Название файла словаря  ресурсов
        /// </summary>
        public String ResDictionaryName;// = null;

        /// <summary>
        /// Uri файла словаря ресурсов
        /// </summary>
        public Uri ResDictionaryUri;

        /// <summary>
        /// Это действительно необходимый компонент-словарь?
        /// </summary>
        public bool IsNeedableItem;// = false;

        /// <summary>
        /// Этот компонент-словарь уже загружен или нет?
        /// </summary>
        public bool IsLoadedItem;// = false;


    }
}
