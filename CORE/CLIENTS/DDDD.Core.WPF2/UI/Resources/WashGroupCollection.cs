﻿using System.Collections.ObjectModel;

namespace DDDD.Core.UI.Resources.IG
{


    /// <summary>
    /// A collection of groups used by the ResourceWasher for washing colors 
    /// </summary>
    /// <seealso cref="T:WashGroup" />
    /// <seealso cref="T:ResourceWasher" />
    /// <seealso cref="F:ResourceWasher.WashGroupProperty" />
    /// <seealso cref="P:ResourceWasher.WashGroups" />
    public class WashGroupCollection : ObservableCollection<WashGroup>
    {
        public WashGroupCollection()
        {
        }
    }
}
