﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace DDDD.Core.UI.Resources.IG
{

    /// <summary>
    /// The ResourceWasher is a Silveright resource dictionary that is used to clone brush resources defined in another ‘source resource dictionary’ and optional wash their colors.  It exposes properties to specify the ‘source resource dictionary’, the default wash color along with a collection of wash groups. It also registers 2 attached properties that can be set on a Brush to specify its group name and whether its colors should be washed at all.
    /// </summary>
    /// <seealso cref="P:ResourceWasher.AutoWash" />
    /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
    /// <seealso cref="P:ResourceWasher.SourceDictionary" />
    /// <seealso cref="P:ResourceWasher.WashColor" />
    /// <seealso cref="F:ResourceWasher.WashGroupProperty" />
    /// <seealso cref="P:ResourceWasher.WashGroups" />
    public class ResourceWasher : ResourceDictionary, ISupportInitialize
    {
        private bool _autoWash = true;

        private bool _themeWashInitialized;

        private bool _isInitializing;

        private ResourceDictionary _sourceDictionary;

        private ResourceDictionary _clonedDictionary;

        private Color _washColor = Colors.Transparent;

        private WashMode _washMode;

        private WashGroupCollection _washGroups;

        private float _hue;

        private float _saturation;

        private Dictionary<ResourceDictionary, ResourceDictionary> _originalDictioanries;

        private Dictionary<object, object> _originalToCloneTable;

        /// <summary>
        /// Identifies the IsExcludedFromWash attached dependency property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes. When set to true will prevent the brushes' colors from being washed.</para>
        /// </remarks>
        /// <seealso cref="M:ResourceWasher.GetIsExcludedFromWash(System.Windows.DependencyObject)" />
        /// <seealso cref="M:ResourceWasher.SetIsExcludedFromWash(System.Windows.DependencyObject,System.Boolean)" />
        public readonly static DependencyProperty IsExcludedFromWashProperty;

        /// <summary>
        /// Identifies the WashGroup attached dependency property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes to identify which group the brush belongs to.</para>
        /// </remarks>
        /// <seealso cref="M:ResourceWasher.GetWashGroup(System.Windows.DependencyObject)" />
        /// <seealso cref="M:ResourceWasher.SetWashGroup(System.Windows.DependencyObject,System.String)" />
        /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        /// <seealso cref="T:WashGroup" />
        public readonly static DependencyProperty WashGroupProperty;

        /// <summary>
        /// Gets/sets whether the wash will be triggered automatically.
        /// </summary>
        /// <remarks>
        /// <para class="body">
        /// If true will call the <see cref="M:ResourceWasher.WashResources" /> method automatically on ISupportInitialize.EndInit or afterward when any of the <see cref="P:ResourceWasher.SourceDictionary" />, <see cref="P:ResourceWasher.WashColor" /> or <see cref="T:WashGroup" /> property values has changed.
        /// </para>
        /// </remarks>
        [DefaultValue(true)]
        public bool AutoWash
        {
            get
            {
                return this._autoWash;
            }
            set
            {
                if (value != this._autoWash)
                {
                    this.VerifyCanBeModified();
                    this._autoWash = value;
                    this.OnCriteriaChanged();
                }
            }
        }

        /// <summary>
        /// Gets/sets adictionary that contains the resources to clone. 
        /// </summary>
        [DefaultValue(null)]
        public ResourceDictionary SourceDictionary
        {
            get
            {
                return this._sourceDictionary;
            }
            set
            {
                if (value != this._sourceDictionary)
                {
                    this.VerifyCanBeModified();
                    this._sourceDictionary = value;
                    this.OnCriteriaChanged();
                }
            }
        }

        /// <summary>
        /// Gets/sets the default color to use to wash the resources in the SourceDictionary 
        /// </summary>
        /// <remarks>
        /// <para class="body">The color to use to wash any resources not identified to a group in the <see cref="P:ResourceWasher.WashGroups" /> collection</para>
        /// <para class="note"><b>Note:</b> if this property is left to its default value of <b>Colors.Transparent</b> then any affected resources will not be cloned and washed. Instead they will be copied over without cloning or modification.</para>
        /// </remarks>
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        /// <seealso cref="T:WashGroup" />
        public Color WashColor
        {
            get
            {
                return this._washColor;
            }
            set
            {
                if (value != this._washColor)
                {
                    this.VerifyCanBeModified();
                    this._washColor = value;
                    this._hue = ResourceWasher.GetHue(value);
                    this._saturation = ResourceWasher.GetSaturation(value);
                    this.OnCriteriaChanged();
                }
            }
        }

        /// <summary>
        /// Returns a collection of WashGroup objects.
        /// </summary>
        /// <seealso cref="T:WashGroup" />
        public WashGroupCollection WashGroups
        {
            get
            {
                if (this._washGroups == null)
                {
                    this._washGroups = new WashGroupCollection();
                }
                return this._washGroups;
            }
            set
            {
                if (this._washGroups != value)
                {
                    this.VerifyCanBeModified();
                    if (this._washGroups != null)
                    {
                        this._washGroups.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.OnWashGroupsCollectionChanged);
                    }
                    this._washGroups = value;
                    if (this._washGroups != null)
                    {
                        this._washGroups.CollectionChanged += new NotifyCollectionChangedEventHandler(this.OnWashGroupsCollectionChanged);
                    }
                    this.OnCriteriaChanged();
                }
            }
        }

        /// <summary>
        /// Gets/sets the method used to wash colors in the resources in the SourceDictionary.
        /// </summary>
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        /// <seealso cref="T:WashGroup" />
        /// <seealso cref="P:ResourceWasher.WashColor" />
        [DefaultValue(WashMode.SoftLightBlend)]
        public WashMode WashMode
        {
            get
            {
                return this._washMode;
            }
            set
            {
                if (value != this._washMode)
                {
                    this.VerifyCanBeModified();
                    if (!Enum.IsDefined(typeof(WashMode), value))
                    {
                        throw new ArgumentException("WashMode");
                    }
                    this._washMode = value;
                    this.OnCriteriaChanged();
                }
            }
        }

        static ResourceWasher()
        {
            ResourceWasher.IsExcludedFromWashProperty = DependencyProperty.RegisterAttached("IsExcludedFromWash", typeof(bool), typeof(ResourceWasher), new PropertyMetadata(false));
            ResourceWasher.WashGroupProperty = DependencyProperty.RegisterAttached("WashGroup", typeof(string), typeof(ResourceWasher), new PropertyMetadata(null));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ResourceWasher()
        {
            this._originalDictioanries = new Dictionary<ResourceDictionary, ResourceDictionary>();
            this._originalToCloneTable = new Dictionary<object, object>();
        }

        private static void AddToDictionary(ResourceDictionary originalDictionary, object key, object value)
        {
            try
            {
                originalDictionary.Add(key, value);
            }
            catch (InvalidOperationException invalidOperationException)
            {
            }
        }

        private static int AdjustColorComponentValue(int startValue, int baseValue)
        {
            int num = startValue * baseValue / 255;
            return num + startValue * (255 - (255 - startValue) * (255 - baseValue) / 255 - num) / 255;
        }

        /// <summary>
        /// Begins the initialization process.
        /// </summary>
        public void BeginInit()
        {
            this._isInitializing = true;
        }

        internal static ResourceDictionary Clone(ResourceDictionary source)
        {
            ResourceWasher resourceWasher = source as ResourceWasher;
            if (resourceWasher != null)
            {
                resourceWasher.VerifyThemeAccess();
            }
            ResourceDictionary resourceDictionaries = new ResourceDictionary();
            foreach (DictionaryEntry dictionaryEntry in source)
            {
                resourceDictionaries.Add(dictionaryEntry.Key, dictionaryEntry.Value);
            }
            foreach (ResourceDictionary mergedDictionary in source.MergedDictionaries)
            {
                ResourceDictionary resourceDictionaries1 = ResourceWasher.Clone(mergedDictionary);
                resourceDictionaries.MergedDictionaries.Add(resourceDictionaries1);
            }
            return resourceDictionaries;
        }

        private Brush CloneBrush(Brush sourceBrush)
        {
            return this.CloneBrush(sourceBrush, false, null);
        }

        private Brush CloneBrush(Brush sourceBrush, bool washColor, WashGroup group)
        {
            Brush solidColorBrush = null;
            if (sourceBrush is SolidColorBrush)
            {
                solidColorBrush = new SolidColorBrush();
            }
            else if (sourceBrush is LinearGradientBrush)
            {
                solidColorBrush = new LinearGradientBrush();
            }
            else if (sourceBrush is RadialGradientBrush)
            {
                solidColorBrush = new RadialGradientBrush();
            }
            else if (sourceBrush is ImageBrush)
            {
                solidColorBrush = new ImageBrush();
            }
            if (solidColorBrush != null)
            {
                this.CopyBrush(sourceBrush, solidColorBrush, washColor, group);
            }
            return solidColorBrush;
        }

        private static Color ColorFromHLS(byte alpha, float hue, float luminance, float saturation)
        {
            int rGBHelper;
            int num;
            int rGBHelper1;
            float single;
            if ((double)saturation != 0)
            {
                single = (luminance > 0.5f ? luminance + saturation - luminance * saturation : luminance + luminance * saturation);
                float single1 = 2f * luminance - single;
                rGBHelper = (int)ResourceWasher.ToRGBHelper(single1, single, hue + 120f);
                num = (int)ResourceWasher.ToRGBHelper(single1, single, hue);
                rGBHelper1 = (int)ResourceWasher.ToRGBHelper(single1, single, hue - 120f);
            }
            else
            {
                int num1 = (int)((double)luminance * 255);
                rGBHelper1 = num1;
                num = num1;
                rGBHelper = num1;
            }
            rGBHelper = Math.Max(0, Math.Min(255, rGBHelper));
            num = Math.Max(0, Math.Min(255, num));
            rGBHelper1 = Math.Max(0, Math.Min(255, rGBHelper1));
            return Color.FromArgb(alpha, (byte)rGBHelper, (byte)num, (byte)rGBHelper1);
        }

        private void CopyBrush(Brush sourceBrush, Brush destinationBrush)
        {
            this.CopyBrush(sourceBrush, destinationBrush, false, null);
        }

        private void CopyBrush(Brush sourceBrush, Brush destinationBrush, bool washColor, WashGroup group)
        {
            SolidColorBrush solidColorBrush = sourceBrush as SolidColorBrush;
            ResourceWasher.SetWashGroup(destinationBrush, ResourceWasher.GetWashGroup(sourceBrush));
            ResourceWasher.SetIsExcludedFromWash(destinationBrush, ResourceWasher.GetIsExcludedFromWash(sourceBrush));
            destinationBrush.Opacity = sourceBrush.Opacity;
            if (solidColorBrush != null)
            {
                SolidColorBrush color = destinationBrush as SolidColorBrush;
                if (!washColor)
                {
                    color.Color = solidColorBrush.Color;
                    return;
                }
                color.Color = this.WashColorHelper(solidColorBrush.Color, group);
                return;
            }
            GradientBrush gradientBrush = sourceBrush as GradientBrush;
            if (gradientBrush != null)
            {
                GradientBrush gradientBrush1 = destinationBrush as GradientBrush;
                if (gradientBrush1 != null)
                {
                    gradientBrush1.GradientStops.Clear();
                    GradientStopCollection gradientStops = gradientBrush.GradientStops;
                    int count = gradientStops.Count;
                    for (int i = 0; i < count; i++)
                    {
                        GradientStop item = gradientStops[i];
                        GradientStop gradientStop = new GradientStop();
                        if (!washColor)
                        {
                            gradientStop.Color = item.Color;
                        }
                        else
                        {
                            gradientStop.Color = this.WashColorHelper(item.Color, group);
                        }
                        gradientStop.Offset = item.Offset;
                        gradientBrush1.GradientStops.Add(gradientStop);
                    }
                    if (gradientBrush is LinearGradientBrush && destinationBrush is LinearGradientBrush)
                    {
                        LinearGradientBrush linearGradientBrush = (LinearGradientBrush)gradientBrush;
                        LinearGradientBrush startPoint = (LinearGradientBrush)destinationBrush;
                        startPoint.StartPoint = linearGradientBrush.StartPoint;
                        startPoint.EndPoint = linearGradientBrush.EndPoint;
                        return;
                    }
                    if (gradientBrush is RadialGradientBrush && destinationBrush is RadialGradientBrush)
                    {
                        RadialGradientBrush radialGradientBrush = (RadialGradientBrush)gradientBrush;
                        RadialGradientBrush center = (RadialGradientBrush)destinationBrush;
                        center.Center = radialGradientBrush.Center;
                        center.RadiusX = radialGradientBrush.RadiusX;
                        center.RadiusY = radialGradientBrush.RadiusY;
                        center.GradientOrigin = radialGradientBrush.GradientOrigin;
                    }
                }
            }
        }

        internal object CreateWashedResource(object value)
        {
            if (value is Brush)
            {
                return this.WashBrushHelper(value as Brush, null);
            }
            if (value is Color)
            {
                return this.WashColorHelper((Color)value, null);
            }
            if (value is ResourceDictionary)
            {
                ResourceDictionary resourceDictionaries = new ResourceDictionary();
                this.WashResourcesHelper(value as ResourceDictionary, resourceDictionaries, new Dictionary<Style, Style>());
                resourceDictionaries.MergedDictionaries.Insert(0, value as ResourceDictionary);
                value = resourceDictionaries;
            }
            return value;
        }

        /// <summary>
        /// Ends the initialization process.
        /// </summary>
        /// <remarks>
        /// <para class="note"><b>Note: </b> If the <see cref="P:ResourceWasher.AutoWash" /> property is true then the <see cref="M:ResourceWasher.WashResources" /> method will be called automatically by this method.</para>
        /// </remarks>
        public void EndInit()
        {
            this._isInitializing = false;
            if (this._autoWash)
            {
                this.WashResources();
            }
        }

        internal static void ForceAutoWashResources(ResourceDictionary rd)
        {
            ResourceWasher resourceWasher = rd as ResourceWasher;
            if (resourceWasher == null)
            {
                foreach (ResourceDictionary mergedDictionary in rd.MergedDictionaries)
                {
                    ResourceWasher.ForceAutoWashResources(mergedDictionary);
                }
            }
            else if (!resourceWasher.AutoWash)
            {
                resourceWasher.WashResources();
                return;
            }
        }

        internal static float GetBrightness(Color color)
        {
            float r = (float)color.R / 255f;
            float g = (float)color.G / 255f;
            float b = (float)color.B / 255f;
            float single = (r > g ? r : g);
            if (b > single)
            {
                single = b;
            }
            float single1 = (r < g ? r : g);
            if (b < single1)
            {
                single1 = b;
            }
            return (single + single1) / 2f;
        }

        private WashGroup GetGroupFromName(string groupName)
        {
            if (groupName == null || groupName.Length == 0 || this._washGroups == null)
            {
                return null;
            }
            int count = this._washGroups.Count;
            if (count == 0)
            {
                return null;
            }
            for (int i = 0; i < count; i++)
            {
                WashGroup item = this._washGroups[i];
                if (item.Name == groupName)
                {
                    return item;
                }
            }
            return null;
        }

        internal static float GetHue(Color color)
        {
            float r = (float)color.R / 255f;
            float g = (float)color.G / 255f;
            float b = (float)color.B / 255f;
            if (r == g && r == b)
            {
                return 0f;
            }
            float single = (r > g ? r : g);
            if (b > single)
            {
                single = b;
            }
            float single1 = (r < g ? r : g);
            if (b < single1)
            {
                single1 = b;
            }
            float single2 = single - single1;
            float single3 = 0f;
            if (r == single)
            {
                single3 = (g - b) / single2 * 60f;
            }
            else if (g == single)
            {
                single3 = (b - r) / single2 * 60f;
                single3 = single3 + 120f;
            }
            else if (b == single)
            {
                single3 = (r - g) / single2 * 60f;
                single3 = single3 + 240f;
            }
            if (single3 < 0f)
            {
                single3 = single3 + 360f;
            }
            return single3;
        }

        /// <summary>
        /// Gets the value of the 'IsExcludedFromWash' attached property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes. When set to true will prevent the brushes' colors from being washed.</para>
        /// </remarks>
        /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
        /// <seealso cref="M:ResourceWasher.SetIsExcludedFromWash(System.Windows.DependencyObject,System.Boolean)" />
        public static bool GetIsExcludedFromWash(DependencyObject d)
        {
            return (bool)d.GetValue(ResourceWasher.IsExcludedFromWashProperty);
        }

        internal static float GetSaturation(Color color)
        {
            float r = (float)color.R / 255f;
            float g = (float)color.G / 255f;
            float b = (float)color.B / 255f;
            float single = (r > g ? r : g);
            if (b > single)
            {
                single = b;
            }
            float single1 = (r < g ? r : g);
            if (b < single1)
            {
                single1 = b;
            }
            double num = (double)(single - single1);
            if (num == 0)
            {
                return 0f;
            }
            double num1 = (double)(single + single1);
            if (num1 / 2 <= 0.5)
            {
                return (float)(num / num1);
            }
            return (float)(num / (2 - num1));
        }

        private Color GetWashColor(WashGroup group)
        {
            if (group == null || !group.WashColor.HasValue)
            {
                return this.WashColor;
            }
            return group.WashColor.Value;
        }

        /// <summary>
        /// Gets the value of the 'WashGroup' attached property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes to identify which group the brush belongs to.</para>
        /// </remarks>
        /// <seealso cref="F:ResourceWasher.WashGroupProperty" />
        /// <seealso cref="M:ResourceWasher.SetWashGroup(System.Windows.DependencyObject,System.String)" />
        /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        /// <seealso cref="T:WashGroup" />
        public static string GetWashGroup(DependencyObject d)
        {
            return (string)d.GetValue(ResourceWasher.WashGroupProperty);
        }

        private void InitializeResourceTarget(IResourceWasherTarget target, bool storeValues)
        {
            DependencyObject dependencyObject = target as DependencyObject;
            if (dependencyObject != null && ResourceWasher.GetIsExcludedFromWash(dependencyObject))
            {
                return;
            }
            target.ResourceWasher = this;
        }

        private void OnCriteriaChanged()
        {
            if (!this._isInitializing && this._autoWash)
            {
                this.WashResources();
                return;
            }
            this._themeWashInitialized = false;
        }

        private void OnWashGroupsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.OnCriteriaChanged();
        }

        /// <summary>
        /// Washes a color. 
        /// </summary>
        /// <param name="startColor">The starting color.</param>
        /// <param name="washColor">The color to wash the starting color with.</param>
        /// <param name="group">The associated wash group which may be null.</param>
        /// <returns></returns>
        protected virtual Color PerformColorWash(Color startColor, Color washColor, WashGroup group)
        {
            WashMode washMode;
            float hue;
            float saturation;
            if (washColor == Colors.Transparent)
            {
                return startColor;
            }
            washMode = (group == null || !group.WashMode.HasValue ? this._washMode : group.WashMode.Value);
            if (washMode != WashMode.SoftLightBlend)
            {
                if (group == null)
                {
                    hue = this._hue;
                    saturation = this._saturation;
                }
                else
                {
                    hue = group.Hue;
                    saturation = group.Saturation;
                }
                return ResourceWasher.ColorFromHLS(startColor.A, hue, ResourceWasher.GetBrightness(startColor), saturation);
            }
            int num = ResourceWasher.AdjustColorComponentValue((int)startColor.R, (int)washColor.R);
            int num1 = ResourceWasher.AdjustColorComponentValue((int)startColor.G, (int)washColor.G);
            int num2 = ResourceWasher.AdjustColorComponentValue((int)startColor.B, (int)washColor.B);
            return Color.FromArgb(startColor.A, (byte)(num & 255), (byte)(num1 & 255), (byte)(num2 & 255));
        }

        /// <summary>
        /// Resets the <see cref="P:ResourceWasher.WashColor" /> property to its default state
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashColor()
        {
            this._washColor = Colors.Transparent;
        }

        /// <summary>
        /// Resets the <see cref="P:ResourceWasher.WashGroups" /> property to its default state
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashGroups()
        {
            if (this._washGroups != null)
            {
                this._washGroups.Clear();
            }
        }

        /// <summary>
        /// Sets the value of the 'IsExcludedFromWash' attached property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes. When set to true will prevent the brushes' colors from being washed.</para>
        /// </remarks>
        /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
        /// <seealso cref="M:ResourceWasher.GetIsExcludedFromWash(System.Windows.DependencyObject)" />
        public static void SetIsExcludedFromWash(DependencyObject d, bool value)
        {
            d.SetValue(ResourceWasher.IsExcludedFromWashProperty, value);
        }

        /// <summary>
        /// Sets the value of the 'WashGroup' attached property
        /// </summary>
        /// <remarks>
        /// <para class="body">Attached property for Brushes to identify which group the brush belongs to.</para>
        /// </remarks>
        /// <seealso cref="F:ResourceWasher.WashGroupProperty" />
        /// <seealso cref="M:ResourceWasher.GetWashGroup(System.Windows.DependencyObject)" />
        /// <seealso cref="F:ResourceWasher.IsExcludedFromWashProperty" />
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        /// <seealso cref="T:WashGroup" />
        public static void SetWashGroup(DependencyObject d, string value)
        {
            d.SetValue(ResourceWasher.WashGroupProperty, value);
        }

        /// <summary>
        /// Determines if the <see cref="P:ResourceWasher.WashColor" /> property needs to be serialized.
        /// </summary>
        /// <returns>True if the property should be serialized</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashColor()
        {
            return this._washColor != Colors.Transparent;
        }

        /// <summary>
        /// Determines if the <see cref="P:ResourceWasher.WashGroups" /> property needs to be serialized.
        /// </summary>
        /// <returns>True if the property should be serialized</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashGroups()
        {
            if (this._washGroups == null)
            {
                return false;
            }
            return this._washGroups.Count > 0;
        }

        void System.ComponentModel.ISupportInitialize.BeginInit()
        {
            this.BeginInit();
        }

        void System.ComponentModel.ISupportInitialize.EndInit()
        {
            this.EndInit();
        }

        private static float ToRGBHelper(float rm1, float rm2, float rh)
        {
            if (rh > 360f)
            {
                rh = rh - 360f;
            }
            else if (rh < 0f)
            {
                rh = rh + 360f;
            }
            if (rh < 60f)
            {
                rm1 = rm1 + (rm2 - rm1) * rh / 60f;
            }
            else if (rh < 180f)
            {
                rm1 = rm2;
            }
            else if (rh < 240f)
            {
                rm1 = rm1 + (rm2 - rm1) * (240f - rh) / 60f;
            }
            return rm1 * 255f;
        }

        private void VerifyCanBeModified()
        {
            if (base.IsReadOnly)
            {
                throw new Exception("LE_ResourceDictionaryReadOnly");
            }
        }

        internal void VerifyThemeAccess()
        {
            if (this._themeWashInitialized)
            {
                return;
            }
            this.WashResources();
        }

        private Brush WashBrushHelper(Brush sourceBrush, object sourceObject)
        {
            if (ResourceWasher.GetIsExcludedFromWash(sourceBrush))
            {
                return sourceBrush;
            }
            string washGroup = ResourceWasher.GetWashGroup(sourceBrush);
            return this.WashBrushHelper(sourceBrush, this.GetGroupFromName(washGroup));
        }

        private Brush WashBrushHelper(Brush sourceBrush, WashGroup group)
        {
            if (this.GetWashColor(group) == Colors.Transparent)
            {
                return sourceBrush;
            }
            return this.CloneBrush(sourceBrush, true, group);
        }

        private Color WashColorHelper(Color startColor, WashGroup group)
        {
            return this.PerformColorWash(startColor, this.GetWashColor(group), group);
        }

        /// <summary>
        /// Clones and washes the resources from the <see cref="P:ResourceWasher.SourceDictionary" />
        /// </summary>
        /// <remarks>
        /// <para class="body">
        /// Loops thru the source collection (and recursively thru its merged dictionaries) and clones any brushes that are not excluded from the wash via the attached <see cref="F:ResourceWasher.IsExcludedFromWashProperty" />. It washes the cloned resources colors with the appropriate wash color. All other resources are just placed in the collection without cloning them.
        /// </para>
        /// </remarks>
        public void WashResources()
        {
            this._themeWashInitialized = true;
            if (this._clonedDictionary == null)
            {
                this._clonedDictionary = new ResourceDictionary();
            }
            else
            {
                if (base.MergedDictionaries.Contains(this._clonedDictionary))
                {
                    base.MergedDictionaries.Remove(this._clonedDictionary);
                }
                this._clonedDictionary.Clear();
                this._clonedDictionary.MergedDictionaries.Clear();
            }
            if (this._sourceDictionary == null)
            {
                return;
            }
            if (!(this._washColor == Colors.Transparent) || this._originalDictioanries.Count != 0 || this._washGroups != null && this._washGroups.Count != 0)
            {
                Dictionary<Style, Style> styles = new Dictionary<Style, Style>();
                this.WashResourcesHelper(this._sourceDictionary, this._clonedDictionary, styles);
            }
            else
            {
                this._clonedDictionary.MergedDictionaries.Add(ResourceWasher.Clone(this._sourceDictionary));
            }
            base.MergedDictionaries.Add(this._clonedDictionary);
        }

        private void WashResourcesHelper(ResourceDictionary source, ResourceDictionary target, Dictionary<Style, Style> clonedStyles)
        {
            Style style;
            object value;
            object obj;
            Style style1;
            target.MergedDictionaries.Clear();
            foreach (ResourceDictionary mergedDictionary in source.MergedDictionaries)
            {
                ResourceDictionary resourceDictionaries = new ResourceDictionary();
                this.WashResourcesHelper(mergedDictionary, resourceDictionaries, clonedStyles);
                target.MergedDictionaries.Add(resourceDictionaries);
            }
            bool flag = false;
            ResourceDictionary item = null;
            if (this._originalDictioanries.ContainsKey(source))
            {
                item = this._originalDictioanries[source];
            }
            else
            {
                item = new ResourceDictionary();
                this._originalDictioanries.Add(source, item);
                flag = true;
            }
            Dictionary<object, Brush> objs = new Dictionary<object, Brush>();
            foreach (DictionaryEntry dictionaryEntry in source)
            {
                if (flag)
                {
                    Brush brush = dictionaryEntry.Value as Brush;
                    if (brush == null)
                    {
                        IList<Brush> brushes = dictionaryEntry.Value as IList<Brush>;
                        if (brushes == null || brushes.IsReadOnly)
                        {
                            ResourceWasher.AddToDictionary(item, dictionaryEntry.Key, dictionaryEntry.Value);
                        }
                        else
                        {
                            List<Brush> brushes1 = new List<Brush>();
                            foreach (Brush brush1 in brushes)
                            {
                                brushes1.Add(this.CloneBrush(brush1));
                            }
                            item.Add(dictionaryEntry.Key, brushes1);
                        }
                    }
                    else
                    {
                        brush = this.CloneBrush(brush);
                        item.Add(dictionaryEntry.Key, brush);
                        this._originalToCloneTable[dictionaryEntry.Value] = brush;
                    }
                }
                object value1 = dictionaryEntry.Value;
                if (!(value1 is Brush))
                {
                    Style basedOn = value1 as Style;
                    if (basedOn != null)
                    {
                        basedOn = (Style)item[dictionaryEntry.Key];
                        if (!clonedStyles.TryGetValue(basedOn, out style))
                        {
                            style = new Style(basedOn.TargetType)
                            {
                                BasedOn = basedOn
                            };
                            clonedStyles[basedOn] = style;
                        }
                        Dictionary<DependencyProperty, Setter> dependencyProperties = new Dictionary<DependencyProperty, Setter>();
                        while (basedOn != null)
                        {
                            for (int i = basedOn.Setters.Count - 1; i >= 0; i--)
                            {
                                Setter setter = basedOn.Setters[i] as Setter;
                                if (setter != null && setter.Property != null && !dependencyProperties.ContainsKey(setter.Property))
                                {
                                    dependencyProperties[setter.Property] = setter;
                                }
                            }
                            basedOn = basedOn.BasedOn;
                        }
                        foreach (KeyValuePair<DependencyProperty, Setter> dependencyProperty in dependencyProperties)
                        {
                            Setter setter1 = dependencyProperty.Value;
                            try
                            {
                                value = setter1.Value;
                            }
                            catch (Exception exception)
                            {
                                continue;
                            }
                            if (value is IResourceWasherTarget)
                            {
                                this.InitializeResourceTarget(value as IResourceWasherTarget, flag);
                            }
                            Brush brush2 = value as Brush;
                            if (brush2 != null)
                            {
                                if (this._originalToCloneTable.TryGetValue(brush2, out obj))
                                {
                                    brush2 = (Brush)obj;
                                }
                                Setter setter2 = new Setter()
                                {
                                    Property = setter1.Property,
                                    Value = this.WashBrushHelper(brush2, value)
                                };
                                style.Setters.Add(setter2);
                            }
                            else if (!(value is Style))
                            {
                                object obj1 = value;
                                if (obj1 is DependencyObject || obj1 is string || obj1 != null && obj1.GetType().IsValueType)
                                {
                                    continue;
                                }
                                Setter setter3 = new Setter()
                                {
                                    Property = setter1.Property,
                                    Value = obj1
                                };
                                style.Setters.Add(setter3);
                            }
                            else
                            {
                                Style value2 = setter1.Value as Style;
                                if (!clonedStyles.TryGetValue(value2, out style1))
                                {
                                    style1 = new Style(value2.TargetType)
                                    {
                                        BasedOn = value2
                                    };
                                    clonedStyles[value2] = style1;
                                }
                                Setter setter4 = new Setter(setter1.Property, style1);
                                style.Setters.Add(setter4);
                            }
                        }
                        value1 = style;
                    }
                    else if (value1 is IList<Brush>)
                    {
                        IList<Brush> brushes2 = value1 as IList<Brush>;
                        if (!brushes2.IsReadOnly)
                        {
                            IList<Brush> item1 = (IList<Brush>)item[dictionaryEntry.Key];
                            int num = 0;
                            int num1 = Math.Min(brushes2.Count, item1.Count);
                            while (num < num1)
                            {
                                brushes2[num] = this.WashBrushHelper(item1[num], brushes2[num]);
                                num++;
                            }
                        }
                    }
                }
                else
                {
                    Brush item2 = (Brush)item[dictionaryEntry.Key];
                    Brush brush3 = this.WashBrushHelper(item2, value1);
                    item2 = brush3;
                    value1 = brush3;
                    objs.Add(dictionaryEntry.Key, item2);
                }
                ResourceWasher.AddToDictionary(target, dictionaryEntry.Key, value1);
            }
            foreach (KeyValuePair<object, Brush> keyValuePair in objs)
            {
                Brush item3 = (Brush)source[keyValuePair.Key];
                this.CopyBrush(keyValuePair.Value, item3);
            }
        }
    }


}
