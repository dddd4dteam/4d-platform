﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DDDD.Core.UI.IG
{

    /// <summary>
    /// Helper class for working around some Silverlight rooting issues.
    /// </summary>
    internal class DependencyObjectCallbackWrapper
    {
        [ThreadStatic]
        private static ValueHolder _holder;

        private PropertyChangedCallback _callback;

        internal DependencyObjectCallbackWrapper(PropertyChangedCallback originalCallback)
        {
            this._callback = originalCallback;
        }


        public void OnChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (this._callback != null)
            {
                this._callback(d, e);
            }
            DependencyObject newValue = e.NewValue as DependencyObject;
            if (newValue != null && !(newValue is FrameworkElement))
            {
                if ( DependencyObjectCallbackWrapper._holder == null)
                {
                    DependencyObjectCallbackWrapper._holder = new ValueHolder();
                }
                DependencyObjectCallbackWrapper._holder.Value = newValue;
            }
        }

        internal static bool ShouldWrap(Type ownerType, Type propertyType)
        {
            if (!typeof(DependencyObject).IsAssignableFrom(ownerType) || typeof(FrameworkElement).IsAssignableFrom(ownerType))
            {
                return false;
            }
            if (typeof(FrameworkElement).IsAssignableFrom(propertyType))
            {
                return false;
            }
            if (propertyType.IsInterface)
            {
                return true;
            }
            if (typeof(DependencyObject).IsAssignableFrom(propertyType))
            {
                return true;
            }
            return propertyType.IsAssignableFrom(typeof(DependencyObject));
        }
    }

}
