﻿
using System;
using System.Windows;

using DDDD.Core.Events.IG;
using DDDD.Core.Diagnostics;


namespace DDDD.Core.UI.IG
{

    internal class DependencyPropertyKey
    {
        #region -------------------------- CTOR -----------------------------
        
        internal DependencyPropertyKey(DependencyProperty property, PropertyChangeCallbackWrapper wrapper, object defaultValue)
        {
            Validator.ATNullReferenceArgDbg(property, nameof(DependencyPropertyKey), nameof(DependencyProperty), nameof(property));
            Validator.ATNullReferenceArgDbg(wrapper, nameof(DependencyPropertyKey), nameof(DependencyProperty), nameof(wrapper));
             
            this.DependencyProperty = property;
            this.Wrapper = wrapper;
            this.DefaultValue = defaultValue;
        }

        #endregion -------------------------- CTOR -----------------------------


        internal readonly DependencyProperty DependencyProperty;

        internal readonly PropertyChangeCallbackWrapper Wrapper;

        internal readonly object DefaultValue;
    
    }


}
