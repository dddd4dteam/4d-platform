﻿using DDDD.Core.Events.IG;
using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Security;
using System.Windows;

namespace DDDD.Core.UI.IG
{
   



    internal static class DependencyPropertyUtilities
    {

        [Flags]
        internal enum MetadataOptionFlags
        {
            None
            , BindsTwoWayByDefault
            , Journal
        }


        private readonly static object MissingValue;

        [ThreadStatic]
        private static Dictionary<Type, TypeConverter> _typeConverterCache;

        private static Dictionary<DependencyProperty, string> g_dpNames;

        private static Dictionary<DependencyProperty, Type> g_dpTypes;

        static DependencyPropertyUtilities()
        {
            DependencyPropertyUtilities.MissingValue = DependencyProperty.UnsetValue;
            DependencyPropertyUtilities.g_dpNames = new Dictionary<DependencyProperty, string>();
            DependencyPropertyUtilities.g_dpTypes = new Dictionary<DependencyProperty, Type>();
        }

        [Conditional("TYPECONVERTER_MISSING")]
        internal static void CoerceUsingTypeConverter<T>(ref T value, Type typeConverterType)
        {
            TypeConverter typeConverter;
            if (typeof(T) == null)
            {
                throw new InvalidOperationException();
            }
            if (value != null)
            {
                if (_typeConverterCache == null)
                {
                    _typeConverterCache = new Dictionary<Type, TypeConverter>();
                }
                if (!_typeConverterCache.TryGetValue(typeConverterType, out typeConverter))
                {
                    try
                    {
                        typeConverter = TypeActivator.CreateInstanceTBaseLazy<TypeConverter>(typeConverterType); //(TypeConverter)Activator.CreateInstance(typeConverterType);
                    }
                    catch
                    {
                    }
                    _typeConverterCache[typeConverterType] = typeConverter;
                }
                if (typeConverter != null && typeConverter.CanConvertFrom(value.GetType()))
                {
                    value = (T)typeConverter.ConvertFrom(value);
                }
            }
        }

        internal static PropertyMetadata CreateMetadata(object defaultValue)
        {
            return CreateMetadata(defaultValue, null);
        }

        internal static PropertyMetadata CreateMetadata(PropertyChangedCallback propertyChangeCallback)
        {
            return CreateMetadata(DependencyPropertyUtilities.MissingValue, propertyChangeCallback);
        }

        internal static PropertyMetadata CreateMetadata(object defaultValue, PropertyChangedCallback propertyChangeCallback)
        {
            return CreateMetadata(defaultValue, propertyChangeCallback, DependencyPropertyUtilities.MetadataOptionFlags.None);
        }

        internal static PropertyMetadata CreateMetadata(object defaultValue, PropertyChangedCallback propertyChangeCallback, DependencyPropertyUtilities.MetadataOptionFlags flags)
        {
            if (defaultValue == DependencyPropertyUtilities.MissingValue)
            {
                return new PropertyMetadata(propertyChangeCallback);
            }
            return new PropertyMetadata(defaultValue, propertyChangeCallback);
        }

        internal static PropertyChangedCallback CreateTypeConverterCallbackWrapper<T>(PropertyChangedCallback callback)
        {
            return callback;
        }

        /// <summary>
        /// Gets the default value for the specified property for the specified object.
        /// </summary>
        /// <param name="d">Dependency object whose property value is to be evaluated</param>
        /// <param name="dp">Property to evaluate</param>
        /// <returns></returns>
        public static object GetDefaultValue(DependencyObject d, DependencyProperty dp)
        {
            if (d == null)
            {
                return null;
            }
            return GetDefaultValue(d.GetType(), dp);
        }

        /// <summary>
        /// Gets the default value for the specified property for the specified type.
        /// </summary>
        /// <param name="type">the type whose property value is to be evaluated</param>
        /// <param name="dp">Property to evaluate</param>
        /// <returns></returns>
        public static object GetDefaultValue(Type type, DependencyProperty dp)
        {
            PropertyMetadata metadata = dp.GetMetadata(type);
            object obj = (metadata != null ? metadata.DefaultValue : null);
            if (DependencyProperty.UnsetValue == obj)
            {
                obj = null;
            }
            return obj;
        }

        /// <summary>
        /// Returns the Name of the DependencyProperty. The property should have been registered using one of the Register methods of this class.
        /// </summary>
        internal static string GetName(DependencyProperty dp)
        {
            string str;
            g_dpNames.TryGetValue(dp, out str);
            return str;
        }

        /// <summary>
        /// Returns the name of the or caches it if it is able to find the public static field returning the DependencyProperty
        /// </summary>
        internal static string GetName(DependencyObject d, DependencyProperty dp)
        {
            string str;
            if (!g_dpNames.TryGetValue(dp, out str))
            {
                try
                {
                    for (Type i = d.GetType(); i != null && str == null; i = i.BaseType)
                    {
                        FieldInfo[] fields = i.GetFields(BindingFlags.Static | BindingFlags.Public);
                        int num = 0;
                        while (num < (int)fields.Length)
                        {
                            FieldInfo fieldInfo = fields[num];
                            if (!typeof(DependencyProperty).IsAssignableFrom(fieldInfo.FieldType) || fieldInfo.GetValue(null) as DependencyProperty != dp)
                            {
                                num++;
                            }
                            else
                            {
                                string name = fieldInfo.Name;
                                if (name.EndsWith("Property"))
                                {
                                    name = name.Substring(0, name.Length - 8);
                                }
                                str = name;
                                break;
                            }
                        }
                    }
                }
                catch (SecurityException securityException)
                {
                }
                lock (g_dpNames)
                {
                    g_dpNames[dp] = str ?? string.Empty;
                }
            }
            return str;
        }

        internal static Type GetType(DependencyProperty dp)
        {
            Type type;
            DependencyPropertyUtilities.g_dpTypes.TryGetValue(dp, out type);
            return type;
        }

        internal static DependencyProperty Register(string name, Type propertyType, Type ownerType, object defaultValue, PropertyChangedCallback propertyChangeCallback)
        {
            if (DependencyObjectCallbackWrapper.ShouldWrap(ownerType, propertyType))
            {
                propertyChangeCallback = new PropertyChangedCallback(new DependencyObjectCallbackWrapper(propertyChangeCallback).OnChangedCallback);
            }
            return Register(name, propertyType, ownerType, (propertyChangeCallback != null ? DependencyPropertyUtilities.CreateMetadata(defaultValue, propertyChangeCallback) : DependencyPropertyUtilities.CreateMetadata(defaultValue)), false);
        }

        internal static DependencyProperty Register(string name, Type propertyType, Type ownerType, PropertyMetadata metadata)
        {
            return Register(name, propertyType, ownerType, metadata, false);
        }

        private static DependencyProperty Register(string name, Type propertyType, Type ownerType, PropertyMetadata metadata, bool isAttached)
        {
            DependencyProperty dependencyProperty = (isAttached ? DependencyProperty.RegisterAttached(name, propertyType, ownerType, metadata) : DependencyProperty.Register(name, propertyType, ownerType, metadata));
            lock (g_dpNames)
            {
                g_dpNames[dependencyProperty] = name;
                g_dpTypes[dependencyProperty] = propertyType;
            }
            return dependencyProperty;
        }

        internal static DependencyProperty RegisterAttached(string name, Type propertyType, Type ownerType, PropertyMetadata metadata)
        {
            return Register(name, propertyType, ownerType, metadata, true);
        }

        internal static DependencyPropertyKey RegisterAttachedReadOnly(string name, Type propertyType, Type ownerType, object defaultValue, PropertyChangedCallback changeCallback)
        {
            return RegisterReadOnly(name, propertyType, ownerType, defaultValue, changeCallback, true);
        }

        internal static DependencyPropertyKey RegisterReadOnly(string name, Type propertyType, Type ownerType, object defaultValue, PropertyChangedCallback changeCallback)
        {
            return RegisterReadOnly(name, propertyType, ownerType, defaultValue, changeCallback, false);
        }

        internal static DependencyPropertyKey RegisterReadOnly(string name, Type propertyType, Type ownerType, object defaultValue, PropertyChangedCallback changeCallback, bool isAttached)
        {
            if (!isAttached && DependencyObjectCallbackWrapper.ShouldWrap(ownerType, propertyType))
            {
                changeCallback = new PropertyChangedCallback(new DependencyObjectCallbackWrapper(changeCallback).OnChangedCallback);
            }
            PropertyChangeCallbackWrapper propertyChangeCallbackWrapper = new PropertyChangeCallbackWrapper(changeCallback);
            PropertyMetadata propertyMetadatum = DependencyPropertyUtilities.CreateMetadata(defaultValue, new PropertyChangedCallback(propertyChangeCallbackWrapper.OnPropertyChanged));
            DependencyPropertyKey dependencyPropertyKey = new DependencyPropertyKey((isAttached ? DependencyProperty.RegisterAttached(name, propertyType, ownerType, propertyMetadatum) : DependencyProperty.Register(name, propertyType, ownerType, propertyMetadatum)), propertyChangeCallbackWrapper, defaultValue);
            lock (g_dpNames)
            {
                g_dpNames[dependencyPropertyKey.DependencyProperty] = name;
                g_dpTypes[dependencyPropertyKey.DependencyProperty] = propertyType;
            }
            return dependencyPropertyKey;
        }

        internal static void SetCurrentValue(DependencyObject d, DependencyProperty property, object newValue)
        {
            d.SetValue(property, newValue);
        }

        /// <summary>
        /// A helper method for figuring out whether a property needs to be serialized.
        /// </summary>
        /// <param name="d">Dependency object whose property value is to be evaluated</param>
        /// <param name="dp">Property to evaluate</param>
        /// <returns></returns>
        public static bool ShouldSerialize(DependencyObject d, DependencyProperty dp)
        {
            object defaultValue = DependencyPropertyUtilities.GetDefaultValue(d, dp);
            return !object.Equals(defaultValue, d.GetValue(dp));
        }

       
      
    }
}
