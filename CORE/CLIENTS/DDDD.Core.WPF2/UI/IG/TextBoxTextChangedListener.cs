﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace DDDD.Core.UI.IG
{

    internal class TextBoxTextChangedListener
    {
        internal TextBoxTextChangedListener(TextBox textBox)
        {
            textBox.TextChanged += new TextChangedEventHandler( TextBoxTextChangedListener.TextBoxTextChanged);
        }

        private static void TextBoxTextChanged(object sender, TextChangedEventArgs e)
        {
            if (sender is DependencyObject)
            {
                BindingExpression bindingExpression = ((DependencyObject)sender).ReadLocalValue(TextBox.TextProperty) as BindingExpression;
                if (bindingExpression != null && bindingExpression.ParentBinding.Mode == BindingMode.TwoWay)
                {
                    bindingExpression.UpdateSource();
                }
            }
        }
    }

}
