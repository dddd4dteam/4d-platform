﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Collections;

namespace DDDD.Core.UI.Binding
{



    /// <summary>
    /// Binding Object Stores.
    /// Strore is ObservableDictionary. 
    /// </summary>
    public class BS
    {
        /// <summary>
        ///  Binding Keys for known storing objects
        /// </summary>
        public class Key
        {
            public const string WBook = nameof(WBook);

            public const string CheckInfs = nameof(CheckInfs);
        }



        public static Dictionary<string, object> Store
        { get; private set; } = new Dictionary<string, object>();

        public static ObservableDictionary<string, object> Store2
        { get; private set; } = new ObservableDictionary<string, object>();



    }
}
