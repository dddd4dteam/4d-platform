﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
//using WinForms = System.Windows.Forms;
using WND = System.Windows;
using WPF = System.Windows.Controls;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;


namespace DDDD.Core.UI
{
    /// <summary>
    /// UI Manager manages by Views and Windows - instantiating an  caching objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class UIManager<T>:IDisposable
       where T : WND.Window, IHostWindow, new()
    {

        #region ------------------ PROPERTIES  --------------------
        IHostWindow CacheMainWindow
        { get; set; }
        
        Dictionary<int, WPF.UserControl> CacheViews
        { get; set; } = new Dictionary<int, WPF.UserControl>();

        /// <summary>
        /// Maximum Count of Cached Views 
        /// </summary>
        public int MAX_Views
        { get; set; } = 5;

        Dictionary<int, IHostWindow> CacheDialogs
        { get; set; } = new Dictionary<int, IHostWindow>();

        /// <summary>
        /// Maximum Count of Cached Dialogs
        /// </summary>
        public int MAX_Dialogs
        { get; set; } = 5;


        #endregion ------------------ PROPERTIES  --------------------


        public void Dispose()
        {
            CacheMainWindow = null;
            CacheDialogs.Clear();
            CacheViews.Clear();
        }

        public int GetViewID<TView>()
        {
            return typeof(TView).FullName.GetHashCode();
        }

        public int GetViewID (Type viewType)
        {
            return viewType.FullName.GetHashCode();
        }


        public TView GetView<TView>(bool cacheView = false)
        where TView : WPF.UserControl, IView, new()
        {
            TView view = null;
            var viewID = GetViewID<TView>();
            // CACHING VIEW
            if (cacheView && CacheViews.ContainsKey(viewID))
            {
                view = CacheViews[viewID] as TView;
            }
            else if (cacheView && CacheViews.ContainsKey(viewID) == false)
            {
                view = new TView();
                CacheNewView(viewID, view);
            }
            else if (cacheView == false)
            {
                view = new TView();
            }
            return view;
        }

        public IView GetView(Type viewType,bool cacheView = false)
        {
            IView view = null;
            var viewID = GetViewID(viewType);
            // CACHING VIEW
            if (cacheView && CacheViews.ContainsKey(viewID))
            {
                view = CacheViews[viewID] as IView;
            }
            else if (cacheView && CacheViews.ContainsKey(viewID) == false)
            {
                view = TypeActivator.CreateInstanceTBase<IView>(viewType);
                CacheNewView(viewID, view as UserControl);
            }
            else if (cacheView == false)
            {
                view = TypeActivator.CreateInstanceTBase<IView>(viewType);
            }
            return view;
           
        }




        public void SetTabItem<TView>(string tabItemKey, TabControl tabElement)
            where TView : UserControl, IView, new()
        {
            var targetView = GetView<TView>(cacheView: true);            
            var item = tabElement.FindName(tabItemKey) as TabItem;
            item.Content = targetView;
        }

        public void SetTabItem(TabItem tabItemElement, Type viewType)
            
        {
            var tabItem = tabItemElement;
            var targetView = GetView(viewType);
            
            tabItem.Content = targetView;
        }


        public void SetTabItem(string tabItemKey, TabControl tabElement, Type viewType)             
        {
            var targetView = GetView(viewType, cacheView: true);            
            var item = tabElement.FindName(tabItemKey) as TabItem;
            item.Content = targetView;
        }


        /// <summary>
        /// Show TView inside Main Host Window.
        /// Main Host Window Always will be cached.
        /// TView  can be cached.
        /// </summary>
        /// <typeparam name="TView">View - UserControl class</typeparam>
        /// <param name="cacheView">say: if this TView instance  will be Cached</param>
        public void Show<TView>(bool cacheView = false)
        where TView : WPF.UserControl, IView, new()
        {
            //ALWAYS CHECK AND CACHE MAIN WINDOW
            if (CacheMainWindow.IsNull())
            {
                CacheMainWindow = new T();
                CacheMainWindow.HideOnly = true;
            }

            IHostWindow hostWindow = CacheMainWindow;
           
            TView view = GetView<TView>(cacheView);
            
            hostWindow.SetHeader(view.HostHeader);
            hostWindow.SetChild(view);
            view.SetHostWindow(hostWindow);
            
            (hostWindow as WND.Window).Closing  += (s, e) =>
            {
                (s as IHostWindow).SetChild(null);
            };

            try
            {                
                    (hostWindow as Window).Show();                
            }
            catch (System.Exception exc) 
            {
                MessageBox.Show(exc.Message);                
            }            
        }


        /// <summary>
        /// Show TView inside  Host-Dialog Window .
        /// [Host-Dialog Window + View]  can be cached.       
        /// </summary>
        /// <typeparam name="TView"> View - UserControl class </typeparam>
        /// <param name="cacheDialog"> say: if this [Host-Dialog Window + View]  will be Cached</param>       
        public void ShowDialog<TView>(bool cacheDialog = false)
        where TView : WPF.UserControl, IView, new()
        {
            var viewDlgID = GetViewID<TView>();

            IHostWindow hostDialog = null;
            if (cacheDialog && CacheDialogs.ContainsKey(viewDlgID))
            {
                hostDialog = CacheDialogs[viewDlgID];
            }
            else if (cacheDialog == false)
            {
                hostDialog = new T();
                TView view = new TView();

                //ATTACHING VIEW
                hostDialog.SetHeader(view.HostHeader);
                hostDialog.SetChild(view);
                view.SetHostWindow(hostDialog);

            }
            // GET/ CACHE [Dialog + VIEW]            
            else if (cacheDialog && CacheDialogs.ContainsKey(viewDlgID) == false)
            {
                hostDialog = new T();
                hostDialog.HideOnly = true;

                TView view = new TView();

                //ATTACHING VIEW
                hostDialog.SetHeader(view.HostHeader);
                hostDialog.SetChild(view);
                view.SetHostWindow(hostDialog);

                CacheNewDialog(viewDlgID, hostDialog);               
            }            

            try
            {
                (hostDialog as Window).ShowDialog();
            }
            catch (System.Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }


        void CacheNewView (int viewID, WPF.UserControl viewItem )
        {
            if (MAX_Views  > CacheViews.Count)
            {
                CacheViews.Add(viewID, viewItem);
            }
            else if (MAX_Views == CacheViews.Count)
            {
                CacheViews.Remove(0);
                CacheViews.Add(viewID, viewItem);
            }
        }

        void CacheNewDialog( int viewID, IHostWindow dialogItem )
        {
            if (MAX_Dialogs > CacheDialogs.Count)
            {
                CacheDialogs.Add(viewID, dialogItem);
            }
            else if (MAX_Dialogs == CacheDialogs.Count)
            {
                CacheDialogs.Remove(0);
                CacheDialogs.Add(viewID, dialogItem);
            }            
        }


    }

}
