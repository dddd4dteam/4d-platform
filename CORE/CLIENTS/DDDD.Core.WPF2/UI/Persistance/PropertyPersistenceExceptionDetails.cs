using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object that stores information of a Property that couldn't be loaded b/c of an exception.
	/// </summary>
	public class PropertyPersistenceExceptionDetails
	{
		/// <summary>
		/// Gets the Exception that was raised when the property was attempted to be loaded. 
		/// </summary>
		public System.Exception Exception
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the the PropertyInfo for the property that could not be loaded. 
		/// </summary>
		public System.Reflection.PropertyInfo PropertyInfo
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the full PropertyPath of the property that could not be loaded.
		/// </summary>
		public string PropertyPath
		{
			get;
			private set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T:Infragistics.Persistence.PropertyPersistenceExceptionDetails" />
		/// </summary>
		/// <param name="ex"></param>
		/// <param name="pi"></param>
		/// <param name="path"></param>
		public PropertyPersistenceExceptionDetails(System.Exception ex, System.Reflection.PropertyInfo pi, string path)
		{
			this.Exception = ex;
			this.PropertyInfo = pi;
			this.PropertyPath = path;
		}
	}
}