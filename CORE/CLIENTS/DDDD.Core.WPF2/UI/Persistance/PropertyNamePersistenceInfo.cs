using Infragistics.Persistence.Primitives;
using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object that can be used to identify a Property by its Name.
	/// </summary>
	public class PropertyNamePersistenceInfo : PropertyPersistenceInfoBase
	{
		/// <summary>
		/// Gets/sets the options that describe how the PropertyName will matched with a property. 
		/// </summary>
		public PropertyNamePersistenceOptions Options
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the name of the property that will be used to identify a property while its being loaded or saved.
		/// </summary>
		public string PropertyName
		{
			get;
			set;
		}

		/// <summary>
		/// Creates a new instance of PropertyNamePersistenceInfo
		/// </summary>
		public PropertyNamePersistenceInfo()
		{
			this.Options = PropertyNamePersistenceOptions.Contains;
		}

		/// <summary>
		/// Validates if the property name matches the property that is being passed in. 
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <returns></returns>
		public override bool DoesPropertyMeetCriteria(PropertyInfo pi, string propertyPath)
		{
			if (pi != null && this.Options == PropertyNamePersistenceOptions.PropertyName)
			{
				return pi.Name == this.PropertyName;
			}
			if (this.Options == PropertyNamePersistenceOptions.PropertyPath)
			{
				return this.PropertyName == propertyPath;
			}
			if (this.Options != PropertyNamePersistenceOptions.Contains)
			{
				return false;
			}
			return propertyPath.Contains(this.PropertyName);
		}
	}
}