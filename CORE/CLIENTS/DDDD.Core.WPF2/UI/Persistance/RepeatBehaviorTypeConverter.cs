using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Media.Animation;

namespace Infragistics.Persistence
{
	/// <summary>
	/// A <see cref="T:System.ComponentModel.TypeConverter" /> that converts from RepeatBehavior to string, and string to RepeatBehavior.
	/// </summary>
	public class RepeatBehaviorTypeConverter : TypeConverter
	{
		public RepeatBehaviorTypeConverter()
		{
		}

		/// <summary>
		/// Returns true if the sourceType is of type string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sourceType"></param>
		/// <returns></returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string);
		}

		/// <summary>
		/// Returns true if the destinationType is of type string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string);
		}

		/// <summary>
		/// Converts a string into a RepeatBehavior.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			string str = (string)value;
			if (str == "Forever")
			{
				return RepeatBehavior.Forever;
			}
			string[] strArrays = str.Split(new char[] { ':' });
			if ((int)strArrays.Length == 2)
			{
				if (strArrays[0] == "D")
				{
					return new RepeatBehavior(TimeSpan.Parse(strArrays[1]));
				}
				if (strArrays[0] == "C")
				{
					return new RepeatBehavior(double.Parse(strArrays[1]));
				}
			}
			return null;
		}

		/// <summary>
		/// Converts a RepeatBehavior into a string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value == null)
			{
				return null;
			}
			RepeatBehavior repeatBehavior = (RepeatBehavior)value;
			if (repeatBehavior.HasDuration)
			{
				return string.Concat("D:", repeatBehavior.Duration.ToString());
			}
			if (!repeatBehavior.HasCount)
			{
				return "Forever";
			}
			return string.Concat("C:", repeatBehavior.Count.ToString());
		}
	}
}