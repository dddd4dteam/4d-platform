using Infragistics;
using Infragistics.Persistence.Primitives;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Xml.Serialization;

namespace Infragistics.Persistence
{
	/// <summary>
	/// A class that can save a <see cref="T:System.Windows.DependencyObject" /> in its current state and load it back.
	/// </summary>
	public class PersistenceManager
	{
		/// <summary>
		/// The types that should be ignored when saving and loading properties.
		/// </summary>
		private static List<Type> TypesToIgnore;

		private static Dictionary<Type, TypeConverter> PredefinedTypeConverters;

		private static List<string> NewNamespaces;

		private static Dictionary<string, string> UpgradeAssemblyDictionary;

		/// <summary>
		/// An Attached Property for storing <see cref="T:Infragistics.Persistence.PersistenceSettings" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		public readonly static DependencyProperty SettingsProperty;

		/// <summary>
		/// An Attached Property for storing <see cref="T:Infragistics.Persistence.PersistenceSettings" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		public readonly static DependencyProperty PersistenceGroupProperty;

		/// <summary>
		/// An Attached Property that allows a user to specify an identifier that can be used to identify an object when its being saved or loaded.
		/// </summary>
		public readonly static DependencyProperty IdentifierProperty;

		[ThreadStatic]
		private static PersistenceManager.AssemblyLoadInfo _AssemblyLoadInfo;

		private Dictionary<int, string> _typeStore;

		private Dictionary<object, int> _objectStore;

		private Dictionary<int, object> _reverseObjectStore;

		private Dictionary<int, PropertyData> _propDataStore;

		private PersistenceSettings _settings;

		private DependencyObject _rootObject;

		private List<PropertyPersistenceExceptionDetails> _exceptions;

		private Collection<Type> _valueTypes;

		private PersistenceGroup _group;

		private int _currentPropertyDataKey;

		private List<int> _allowableKeys = new List<int>();

		internal List<PropertyPersistenceExceptionDetails> Exceptions
		{
			get
			{
				return this._exceptions;
			}
		}

		/// <summary>
		/// Gets/sets a dictionary of all the root properties that belong to the object. 
		/// </summary>
		public Dictionary<int, PropertyData> PropertyDataStore
		{
			get
			{
				return this._propDataStore;
			}
			set
			{
				this._propDataStore = value;
			}
		}

		/// <summary>
		/// Gets alist of Value types that have been stored by their values. 
		/// </summary>
		public Collection<Type> ValueTypes
		{
			get
			{
				return this._valueTypes;
			}
		}

		/// <summary>
		/// Initializes static members of the <see cref="T:Infragistics.Persistence.PersistenceManager" />
		/// </summary>
		static PersistenceManager()
		{
			List<Type> types = new List<Type>()
			{
				typeof(ControlTemplate),
				typeof(DataTemplate),
				typeof(ItemsPanelTemplate),
				typeof(Style),
				typeof(ResourceDictionary)
			};
			PersistenceManager.TypesToIgnore = types;
			PersistenceManager.PredefinedTypeConverters = new Dictionary<Type, TypeConverter>();
			List<string> strs = new List<string>()
			{
				"Infragistics",
				"Infragistics.Controls",
				"Infragistics.Controls.Primitives",
				"Infragistics.Controls.Editors",
				"Infragistics.Controls.Editors.Primitives",
				"Infragistics.Controls.Menus",
				"Infragistics.Controls.Menus.Primitives",
				"Infragistics.Controls.Interactions",
				"Infragistics.Controls.Interactions.Primitives",
				"Infragistics.Controls.Grids",
				"Infragistics.Controls.Grids.Primitives",
				"Infragistics.Controls.Lists",
				"Infragistics.Controls.Lists.Primitives"
			};
			PersistenceManager.NewNamespaces = strs;
			PersistenceManager.UpgradeAssemblyDictionary = new Dictionary<string, string>();
			PersistenceManager.SettingsProperty = DependencyProperty.RegisterAttached("Settings", typeof(PersistenceSettings), typeof(PersistenceManager), new PropertyMetadata(null));
			PersistenceManager.PersistenceGroupProperty = DependencyProperty.RegisterAttached("PersistenceGroup", typeof(PersistenceGroup), typeof(PersistenceManager), new PropertyMetadata(new PropertyChangedCallback(PersistenceManager.PersistenceGroupChanged)));
			PersistenceManager.IdentifierProperty = DependencyProperty.RegisterAttached("Identifier", typeof(string), typeof(PersistenceManager), new PropertyMetadata(null));
			PersistenceManager.PredefinedTypeConverters.Add(typeof(TimeSpan), new TimeSpanTypeConverter());
			PersistenceManager.PredefinedTypeConverters.Add(typeof(Duration), new DurationTypeConverter());
			PersistenceManager.PredefinedTypeConverters.Add(typeof(RepeatBehavior), new RepeatBehaviorTypeConverter());
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight", "InfragisticsSL5");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.DragDrop", "InfragisticsSL5.DragDrop");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.Compression", "InfragisticsSL5.Compression");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.Excel", "InfragisticsSL5.Documents.Excel");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.Persistence", "InfragisticsSL5.Persistence");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebEditors", "InfragisticsSL5.Controls.Editors");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebMenu", "InfragisticsSL5.Controls.Menus.XamMenu");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebHtmlViewer", "InfragisticsSL5.Controls.Interactions.XamHtmlViewer");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebDialogWindow", "InfragisticsSL5.Controls.Interactions.XamDialogWindow");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebOutlookBar", "InfragisticsSL5.Controls.Menus.XamOutlookBar");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebRibbon", "InfragisticsSL5.Controls.Menus.XamRibbon");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebGrid", "InfragisticsSL5.Controls.Grids.XamGrid");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebTagCloud", "InfragisticsSL5.Controls.Menus.XamTagCloud");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebComboEditor ", "InfragisticsSL5.Controls.Editors.XamComboEditor");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebTree", "InfragisticsSL5.Controls.Menus.XamTree");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.VirtualCollection", "InfragisticsSL5.Collections.VirtualCollection");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebSlider", "InfragisticsSL5.Controls.Editors.XamSlider");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebSpellChecker", "InfragisticsSL5.Controls.Interactions.XamSpellChecker");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.XamWebTileView", "InfragisticsSL5.Controls.Lists.XamTileView");
			PersistenceManager.UpgradeAssemblyDictionary.Add("Infragistics.Silverlight.Chart", "InfragisticsSL5.Charts.XamWebChart");
		}

		private PersistenceManager(PersistenceGroup group, PersistenceSettings settings, DependencyObject rootObject, Dictionary<int, string> typeDataStore, Collection<Type> valueTypes)
		{
			this._group = group;
			this._typeStore = typeDataStore;
			this._objectStore = new Dictionary<object, int>();
			this._reverseObjectStore = new Dictionary<int, object>();
			this._propDataStore = new Dictionary<int, PropertyData>();
			this._settings = settings;
			this._rootObject = rootObject;
			this._valueTypes = valueTypes;
			this._exceptions = new List<PropertyPersistenceExceptionDetails>();
		}

		/// <summary>
		/// Walks through the root object in which this <see cref="T:Infragistics.Persistence.PersistenceManager" /> represents, and build a recursive list of <see cref="T:Infragistics.Persistence.Primitives.PropertyDataInfo" /> that can be used later to rehydrate the object.
		/// </summary>
		/// <returns></returns>
		public Collection<PropertyDataInfo> BuildPropertyTree()
		{
			this.StoreProperty(this._rootObject, null);
			Collection<PropertyDataInfo> propertyDataInfos = this.BuildPropertyTree("", this._rootObject, true);
			PersistenceSavedEventArgs persistenceSavedEventArg = new PersistenceSavedEventArgs();
			this._settings.Events.OnPersistenceSaved(persistenceSavedEventArg);
			return propertyDataInfos;
		}

		private Collection<PropertyDataInfo> BuildPropertyTree(string propertyPath, object obj, bool validateAgainstIgnore)
		{
			Collection<PropertyDataInfo> propertyDataInfos = new Collection<PropertyDataInfo>();
			if (obj != null)
			{
				IProvideCustomObjectPersistence provideCustomObjectPersistence = obj as IProvideCustomObjectPersistence;
				if (provideCustomObjectPersistence != null)
				{
					object obj1 = provideCustomObjectPersistence.SaveObject();
					if (obj1 != null)
					{
						propertyDataInfos.Add(this.CreateAndStorePropertyData(obj1, "", "PersistenceCustomObject", obj1.GetType(), null, null, null, this.BuildPropertyTree("", obj1, false)));
					}
					if (this._settings == null || this._settings.SavePersistenceOptions != PersistenceOption.OnlySpecified)
					{
						return propertyDataInfos;
					}
				}
				bool flag = obj is FrameworkElement;
				IProvidePropertyPersistenceSettings providePropertyPersistenceSetting = obj as IProvidePropertyPersistenceSettings;
				Type type = obj.GetType();
				if (providePropertyPersistenceSetting != null && providePropertyPersistenceSetting.PriorityProperties != null)
				{
					foreach (string priorityProperty in providePropertyPersistenceSetting.PriorityProperties)
					{
						PropertyInfo property = type.GetProperty(priorityProperty);
						if (property == null)
						{
							continue;
						}
						object value = property.GetValue(obj, null);
						PropertyDataInfo propertyDataInfo = this.SaveProperty(propertyPath, value, property, validateAgainstIgnore);
						if (propertyDataInfo == null)
						{
							continue;
						}
						propertyDataInfos.Add(propertyDataInfo);
					}
				}
				List<string> propertiesToIgnore = null;
				if (providePropertyPersistenceSetting != null)
				{
					propertiesToIgnore = providePropertyPersistenceSetting.PropertiesToIgnore;
				}
				PropertyInfo[] properties = type.GetProperties();
				for (int i = 0; i < (int)properties.Length; i++)
				{
					PropertyInfo propertyInfo = properties[i];
					string name = propertyInfo.Name;
					string str = name;
					if (name != null)
					{
						if (str != "Name")
						{
							if (str == "DataContext")
							{
								goto Label0;
							}
						}
						else if (flag)
						{
							goto Label0;
						}
					}
					if (!PersistenceManager.TypesToIgnore.Contains(type))
					{
						if (propertiesToIgnore != null && propertiesToIgnore.Contains(propertyInfo.Name))
						{
							if (this._settings != null)
							{
								string str1 = propertyPath;
								if (str1.Length > 0)
								{
									str1 = string.Concat(str1, ".");
								}
								str1 = string.Concat(str1, propertyInfo.Name);
								if (this._settings.PropertySettings.GetPropertyPersistenceInfo(propertyInfo, str1) == null)
								{
									goto Label0;
								}
							}
							else
							{
								goto Label0;
							}
						}
						object value1 = null;
						bool flag1 = false;
						try
						{
							value1 = propertyInfo.GetValue(obj, null);
						}
						catch (Exception exception)
						{
							flag1 = true;
						}
						if (!flag1)
						{
							PropertyDataInfo propertyDataInfo1 = this.SaveProperty(propertyPath, value1, propertyInfo, validateAgainstIgnore);
							if (propertyDataInfo1 != null)
							{
								propertyDataInfos.Add(propertyDataInfo1);
							}
						}
					}
				Label0:
				}
			}
			return propertyDataInfos;
		}

		private List<int> CheckForPropertiesThatMatch(PropertyData data)
		{
			List<int> nums = null;
			Type type = this.ResolveType(data.AssemblyTypeKey);
			if (data.Properties != null && data.Properties.Count > 0)
			{
				if (type.GetInterface("System.Collections.IEnumerable", true) == null)
				{
					foreach (PropertyDataInfo property in data.Properties)
					{
						PropertyInfo propertyInfo = type.GetProperty(property.PropertyName);
						if (propertyInfo == null)
						{
							continue;
						}
						if (this._settings.PropertySettings.GetPropertyPersistenceInfo(propertyInfo, property.PropertyPath) == null)
						{
							PropertyData propertyDatum = this.ResolvePropertyData(property.DataKey);
							if (propertyDatum == null)
							{
								continue;
							}
							List<int> nums1 = this.CheckForPropertiesThatMatch(propertyDatum);
							if (nums1 == null)
							{
								continue;
							}
							if (nums == null)
							{
								nums = new List<int>();
							}
							nums.AddRange(nums1);
							nums.Add(property.DataKey);
						}
						else
						{
							if (nums == null)
							{
								nums = new List<int>();
							}
							nums.Add(property.DataKey);
						}
					}
				}
				else
				{
					foreach (PropertyDataInfo propertyDataInfo in data.Properties)
					{
						if (this._settings.PropertySettings.GetPropertyPersistenceInfo(null, propertyDataInfo.PropertyPath) == null)
						{
							PropertyData propertyDatum1 = this.ResolvePropertyData(propertyDataInfo.DataKey);
							if (propertyDatum1 == null)
							{
								continue;
							}
							List<int> nums2 = this.CheckForPropertiesThatMatch(propertyDatum1);
							if (nums2 == null)
							{
								continue;
							}
							if (nums == null)
							{
								nums = new List<int>();
							}
							nums.AddRange(nums2);
							nums.Add(propertyDataInfo.DataKey);
						}
						else
						{
							if (nums == null)
							{
								nums = new List<int>();
							}
							nums.Add(propertyDataInfo.DataKey);
						}
					}
				}
			}
			return nums;
		}

		private PropertyDataInfo CreateAndStorePropertyData(object value, string propertyName, string propertyPath, Type t, string id, string stringValue, string converterTypeName, Collection<PropertyDataInfo> props)
		{
			return this.CreateAndStorePropertyData(value, propertyName, propertyPath, t, id, false, stringValue, converterTypeName, props);
		}

		private PropertyDataInfo CreateAndStorePropertyData(object value, string propertyName, string propertyPath, Type t, string id, bool storeValue, string stringValue, string converterTypeName, Collection<PropertyDataInfo> props)
		{
			return this.CreateAndStorePropertyData(value, propertyName, propertyPath, t, id, storeValue, stringValue, converterTypeName, props, null);
		}

		private PropertyDataInfo CreateAndStorePropertyData(object value, string propertyName, string propertyPath, Type t, string id, bool storeValue, string stringValue, string converterTypeName, Collection<PropertyDataInfo> props, Collection<string> lookupKeys)
		{
			string str;
			IProvideCustomStorage provideCustomStorage = value as IProvideCustomStorage;
			if (provideCustomStorage != null)
			{
				str = provideCustomStorage.Save();
			}
			else
			{
				str = null;
			}
			string str1 = str;
			PropertyData propertyDatum = new PropertyData()
			{
				AssemblyTypeKey = this.StoreType(t),
				Identifier = id,
				StringValue = stringValue,
				ConverterTypeName = converterTypeName,
				Properties = props,
				LookUpKeys = lookupKeys,
				CustomStorage = str1
			};
			PropertyData propertyDatum1 = propertyDatum;
			if (storeValue)
			{
				propertyDatum1.Value = value;
				if (!this.ValueTypes.Contains(t))
				{
					this.ValueTypes.Add(t);
				}
			}
			PropertyDataInfo propertyDataInfo = new PropertyDataInfo()
			{
				PropertyName = propertyName,
				PropertyPath = propertyPath,
				DataKey = this.StoreProperty(value, propertyDatum1)
			};
			return propertyDataInfo;
		}

		private static Type FixAssemblyQualifiedName(string oldAssemblyQualifiedName)
		{
			Type type = null;
			int num = oldAssemblyQualifiedName.IndexOf(", ") + 2;
			int num1 = oldAssemblyQualifiedName.IndexOf(".v", num);
			string str = oldAssemblyQualifiedName.Substring(num, num1 - num);
			string str1 = "";
			string assemblyQualifiedName = typeof(PersistenceManager).AssemblyQualifiedName;
			num = assemblyQualifiedName.IndexOf("Version=") + 8;
			num1 = assemblyQualifiedName.IndexOf(",", num);
			string[] strArrays = assemblyQualifiedName.Substring(num, num1 - num).Split(new char[] { '.' });
			string str2 = strArrays[(int)strArrays.Length - 1];
			num = oldAssemblyQualifiedName.IndexOf("Version=") + 8;
			num1 = oldAssemblyQualifiedName.IndexOf(",", num);
			strArrays = oldAssemblyQualifiedName.Substring(num, num1 - num).Split(new char[] { '.' });
			string str3 = strArrays[(int)strArrays.Length - 1];
			oldAssemblyQualifiedName = oldAssemblyQualifiedName.Replace(string.Concat(".", str3), string.Concat(".", str2));
			if (PersistenceManager.UpgradeAssemblyDictionary.TryGetValue(str, out str1))
			{
				string str4 = oldAssemblyQualifiedName.Replace(str, str1);
				type = Type.GetType(str4);
				if (type == null)
				{
					char[] chrArray = new char[] { ',' };
					string str5 = str4.Split(chrArray)[0];
					num = str5.LastIndexOf('[');
					if (num != -1)
					{
						num++;
						str5 = str5.Substring(num, str5.Length - num);
					}
					string[] strArrays1 = str5.Split(new char[] { '.' });
					string str6 = strArrays1[(int)strArrays1.Length - 1];
					str6 = str6.Replace("XamWeb", "Xam");
					foreach (string newNamespace in PersistenceManager.NewNamespaces)
					{
						string str7 = str4.Replace(str5, string.Concat(newNamespace, ".", str6));
						type = Type.GetType(str7);
						if (type == null)
						{
							continue;
						}
						str4 = str7;
						break;
					}
				}
			}
			return type;
		}

		/// <summary>
		/// Gets the identifier specified on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public static string GetIdentifier(DependencyObject obj)
		{
			return (string)obj.GetValue(PersistenceManager.IdentifierProperty);
		}

		/// <summary>
		/// Gets the <see cref="T:Infragistics.Persistence.PersistenceGroup" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns>Null if not set</returns>
		public static PersistenceGroup GetPersistenceGroup(DependencyObject obj)
		{
			return (PersistenceGroup)obj.GetValue(PersistenceManager.PersistenceGroupProperty);
		}

		/// <summary>
		/// Gets the <see cref="T:Infragistics.Persistence.PersistenceSettings" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <returns>Null if not set</returns>
		public static PersistenceSettings GetSettings(DependencyObject obj)
		{
			return (PersistenceSettings)obj.GetValue(PersistenceManager.SettingsProperty);
		}

		private static Type GetTypeFromSerializedName(string oldQualifiedTypeName)
		{
			return Type.GetType(PersistenceManager.SynchQualifiedTypeNameVersion(oldQualifiedTypeName));
		}

		/// <summary>
		/// Loads all objects of a group from a stream.
		/// </summary>
		/// <param name="group">The group that contains the objects that should be loaded into.</param>
		/// <param name="stream">The stream in which the object information should be loaded from.</param>
		public static void Load(PersistenceGroup group, Stream stream)
		{
			PersistenceManager._AssemblyLoadInfo = new PersistenceManager.AssemblyLoadInfo();
			PersistenceManager.LoadGroup(group, stream, null);
			PersistenceManager._AssemblyLoadInfo = null;
		}

		/// <summary>
		/// Loads the properties of an object from a <see cref="T:System.IO.Stream" />
		/// </summary>
		/// <param name="obj">The object that should be filled.</param>
		/// <param name="stream">The stream in which the object information should be loaded from.</param>
		public static void Load(DependencyObject obj, Stream stream)
		{
			PersistenceManager.Load(obj, stream, PersistenceManager.GetSettings(obj));
		}

		/// <summary>
		/// Loads the properties of an object from a <see cref="T:System.IO.Stream" />
		/// </summary>
		/// <param name="obj">The object that should be filled.</param>
		/// <param name="stream">The stream in which the object information should be loaded from.</param>
		/// <param name="settings">The settings that should be followed while loading properties onto the object.</param>
		public static void Load(DependencyObject obj, Stream stream, PersistenceSettings settings)
		{
			PersistenceManager._AssemblyLoadInfo = new PersistenceManager.AssemblyLoadInfo();
			PersistenceGroup persistenceGroup = new PersistenceGroup();
			persistenceGroup.RegisterObject(obj);
			PersistenceManager.LoadGroup(persistenceGroup, stream, settings);
			PersistenceManager._AssemblyLoadInfo = null;
		}

		private static void LoadGroup(PersistenceGroup group, Stream stream, PersistenceSettings settings)
		{
			List<PropertyPersistenceExceptionDetails> propertyPersistenceExceptionDetails = new List<PropertyPersistenceExceptionDetails>();
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(PersistenceObject));
			PersistenceObject persistenceObject = xmlSerializer.Deserialize(stream) as PersistenceObject;
			if (persistenceObject != null)
			{
				StringReader stringReader = new StringReader(persistenceObject.Xml);
				Type[] typeFromSerializedName = new Type[persistenceObject.Types.Count];
				for (int i = 0; i < persistenceObject.Types.Count; i++)
				{
					typeFromSerializedName[i] = PersistenceManager.GetTypeFromSerializedName(persistenceObject.Types[i]);
				}
				string str = "";
				if (typeFromSerializedName.Contains<Type>(null))
				{
					stream.Seek((long)0, SeekOrigin.Begin);
					string end = (new StreamReader(stream)).ReadToEnd();
					string fullName = group.GetType().Assembly.FullName;
					int num = fullName.IndexOf(".v") + 2;
					int num1 = fullName.IndexOf(",", num);
					string str1 = fullName.Substring(num, num1 - num);
					num = end.IndexOf("Infragistics");
					num = end.IndexOf(".v", num) + 2;
					num1 = end.IndexOf(",", num);
					str = end.Substring(num, num1 - num);
					if (str != "10.1")
					{
						foreach (string type in persistenceObject.Types)
						{
							if (PersistenceManager.GetTypeFromSerializedName(type) != null)
							{
								continue;
							}
							num = type.IndexOf(", ") + 2;
							num1 = type.IndexOf(".v", num);
							fullName = typeof(PersistenceManager).AssemblyQualifiedName;
							num = fullName.IndexOf("Version=") + 8;
							num1 = fullName.IndexOf(",", num);
							string[] strArrays = fullName.Substring(num, num1 - num).Split(new char[] { '.' });
							string str2 = strArrays[(int)strArrays.Length - 1];
							num = type.IndexOf("Version=") + 8;
							num1 = type.IndexOf(",", num);
							strArrays = type.Substring(num, num1 - num).Split(new char[] { '.' });
							string str3 = strArrays[(int)strArrays.Length - 1];
							end = end.Replace(string.Concat(".", str3), string.Concat(".", str2));
							break;
						}
					}
					else
					{
						Dictionary<string, string> strs = new Dictionary<string, string>();
						foreach (string type1 in persistenceObject.Types)
						{
							Type type2 = Type.GetType(type1);
							if (type2 != null)
							{
								continue;
							}
							string str4 = type1.Replace(str, str1);
							str4 = str4.Replace(string.Concat(str.Replace(".", ""), "."), string.Concat(str1.Replace(".", ""), "."));
							type2 = PersistenceManager.FixAssemblyQualifiedName(str4);
							if (type2 == null)
							{
								continue;
							}
							strs.Add(type1, type2.AssemblyQualifiedName);
						}
						foreach (KeyValuePair<string, string> keyValuePair in strs)
						{
							end = end.Replace(keyValuePair.Key, keyValuePair.Value);
						}
					}
					end = end.Replace(string.Concat("v", str), string.Concat("v", str1));
					end = end.Replace(string.Concat("=", str), string.Concat("=", str1));
					end = end.Replace(string.Concat(str.Replace(".", ""), "."), string.Concat(str1.Replace(".", ""), "."));
					end = end.Replace("InfragisticsSL4", "InfragisticsSL5");
					persistenceObject = xmlSerializer.Deserialize(new StringReader(end)) as PersistenceObject;
					stringReader = new StringReader(persistenceObject.Xml);
					typeFromSerializedName = new Type[persistenceObject.Types.Count];
					for (int j = 0; j < persistenceObject.Types.Count; j++)
					{
						typeFromSerializedName[j] = PersistenceManager.GetTypeFromSerializedName(persistenceObject.Types[j]);
					}
				}
				xmlSerializer = new XmlSerializer(typeof(GroupData), typeFromSerializedName);
				GroupData groupDatum = xmlSerializer.Deserialize(stringReader) as GroupData;
				Dictionary<int, string> nums = new Dictionary<int, string>();
				if (str != "10.1")
				{
					foreach (TypeDataPair typeStore in groupDatum.TypeStore)
					{
						nums.Add(typeStore.LookupKey, PersistenceManager.SynchQualifiedTypeNameVersion(typeStore.TypeName));
					}
				}
				else
				{
					foreach (TypeDataPair typeDataPair in groupDatum.TypeStore)
					{
						Type type3 = Type.GetType(typeDataPair.TypeName);
						if (type3 == null)
						{
							type3 = PersistenceManager.FixAssemblyQualifiedName(typeDataPair.TypeName);
							if (type3 == null)
							{
								continue;
							}
							nums.Add(typeDataPair.LookupKey, type3.AssemblyQualifiedName);
						}
						else
						{
							nums.Add(typeDataPair.LookupKey, typeDataPair.TypeName);
						}
					}
				}
				if (groupDatum != null)
				{
					bool flag = settings != null;
					int count = groupDatum.Elements.Count;
					if (count != group.RegisteredObjects.Count)
					{
						throw new InvalidPersistenceGroupException();
					}
					for (int k = 0; k < count; k++)
					{
						ElementData item = groupDatum.Elements[k];
						DependencyObject dependencyObject = group.RegisteredObjects[k];
						if (!flag)
						{
							settings = PersistenceManager.GetSettings(dependencyObject);
						}
						if (settings == null)
						{
							settings = new PersistenceSettings();
						}
						INeedInitializationForPersistence needInitializationForPersistence = dependencyObject as INeedInitializationForPersistence;
						if (needInitializationForPersistence != null)
						{
							needInitializationForPersistence.InitObject(null);
						}
						PersistenceManager persistenceManager = new PersistenceManager(group, settings, dependencyObject, nums, new Collection<Type>());
						if (persistenceManager.ResolveType(item.TypeLookupKey) != dependencyObject.GetType())
						{
							throw new InvalidPersistenceGroupException();
						}
						Dictionary<int, PropertyData> nums1 = new Dictionary<int, PropertyData>();
						foreach (PropertyDataPair propDataStore in item.PropDataStore)
						{
							nums1.Add(propDataStore.LookupKey, propDataStore.Data);
						}
						persistenceManager.PropertyDataStore = nums1;
						persistenceManager.UnloadPropertyTree(item.PropertyTree);
						propertyPersistenceExceptionDetails.AddRange(persistenceManager.Exceptions);
					}
				}
			}
			PersistenceLoadedEventArgs persistenceLoadedEventArg = new PersistenceLoadedEventArgs(new ReadOnlyCollection<PropertyPersistenceExceptionDetails>(propertyPersistenceExceptionDetails));
			group.Events.OnPersistenceLoaded(persistenceLoadedEventArg);
		}

		private object LoadProperty(object owner, PropertyDataInfo dataInfo, bool validateAgainstIgnore)
		{
			bool flag = false;
			bool flag1 = false;
			bool flag2 = false;
			int dataKey = dataInfo.DataKey;
			object item = null;
			if (this._reverseObjectStore.ContainsKey(dataKey))
			{
				item = this._reverseObjectStore[dataKey];
				flag1 = true;
			}
			PropertyInfo property = null;
			if (owner != null)
			{
				property = owner.GetType().GetProperty(dataInfo.PropertyName);
			}
			PropertyData propertyDatum = this.ResolvePropertyData(dataInfo.DataKey);
			if (property != null)
			{
				if (property.CanRead && property.GetGetMethod() != null && property.GetGetMethod().IsStatic || property.CanWrite && property.GetSetMethod() != null && property.GetSetMethod().IsStatic)
				{
					return null;
				}
				if (propertyDatum == null)
				{
					return null;
				}
				if (propertyDatum.Identifier == null && propertyDatum.LookUpKeys != null && propertyDatum.LookUpKeys.Count == 0 && propertyDatum.Properties.Count == 0 && propertyDatum.StringValue == null && propertyDatum.Value == null && property.PropertyType.GetInterface("System.Collections.IEnumerable", true) == null)
				{
					this.SetProperty(property, owner, null, propertyDatum.Properties);
					return null;
				}
			}
			PersistenceOption loadPersistenceOptions = this._settings.LoadPersistenceOptions;
			PropertyPersistenceInfoBase propertyPersistenceInfo = this._settings.PropertySettings.GetPropertyPersistenceInfo(property, dataInfo.PropertyPath);
			if (propertyPersistenceInfo != null)
			{
				if (!string.IsNullOrEmpty(propertyDatum.StringValue))
				{
					item = propertyPersistenceInfo.LoadObjectFromString(property, dataInfo.PropertyPath, propertyDatum.StringValue);
				}
				if (item == null && propertyPersistenceInfo.TypeConverter != null && propertyPersistenceInfo.TypeConverter.CanConvertFrom(typeof(string)))
				{
					item = propertyPersistenceInfo.TypeConverter.ConvertFromString(propertyDatum.StringValue);
					flag2 = true;
				}
			}
			else if (loadPersistenceOptions != PersistenceOption.OnlySpecified || !validateAgainstIgnore)
			{
				propertyPersistenceInfo = this._settings.IgnorePropertySettings.GetPropertyPersistenceInfo(property, dataInfo.PropertyPath);
				if (propertyPersistenceInfo != null)
				{
					return null;
				}
			}
			else
			{
				if (!this._allowableKeys.Contains(dataInfo.DataKey))
				{
					List<int> nums = this.CheckForPropertiesThatMatch(propertyDatum);
					if (nums != null && nums.Count > 0)
					{
						this._allowableKeys.AddRange(nums);
						this._allowableKeys.Add(dataInfo.DataKey);
					}
				}
				flag = true;
				if (!this._allowableKeys.Contains(dataInfo.DataKey))
				{
					return null;
				}
			}
			LoadPropertyPersistenceEventArgs loadPropertyPersistenceEventArg = new LoadPropertyPersistenceEventArgs(this._rootObject, property, dataInfo.PropertyPath, propertyDatum.StringValue, propertyDatum.Identifier, owner, propertyDatum.Value);
			this._settings.Events.OnLoadPropertyPersistence(loadPropertyPersistenceEventArg);
			this._group.Events.OnLoadPropertyPersistence(loadPropertyPersistenceEventArg);
			if (loadPropertyPersistenceEventArg.Handled)
			{
				return null;
			}
			if (loadPropertyPersistenceEventArg.LoadedValue != null)
			{
				item = loadPropertyPersistenceEventArg.LoadedValue;
				flag2 = true;
			}
			if (item == null)
			{
				bool flag3 = false;
				if (propertyDatum.Value != null)
				{
					item = propertyDatum.Value;
				}
				else if (!string.IsNullOrEmpty(propertyDatum.Identifier))
				{
					if (Application.Current != null)
					{
						item = Application.Current.Resources[propertyDatum.Identifier];
					}
				}
				else if (!string.IsNullOrEmpty(propertyDatum.StringValue))
				{
					if (string.IsNullOrEmpty(propertyDatum.ConverterTypeName))
					{
						try
						{
							Type type = this.ResolveType(propertyDatum.AssemblyTypeKey);
							if (type != typeof(Type))
							{
								item = Convert.ChangeType(propertyDatum.StringValue, type, CultureInfo.CurrentCulture);
							}
							else
							{
								item = Type.GetType(propertyDatum.StringValue);
							}
						}
						catch (InvalidCastException invalidCastException)
						{
							flag3 = true;
						}
					}
					else
					{
						Type type1 = Type.GetType(propertyDatum.ConverterTypeName) ?? (PersistenceManager.FixAssemblyQualifiedName(propertyDatum.ConverterTypeName) ?? PersistenceManager.GetTypeFromSerializedName(propertyDatum.ConverterTypeName));
						if (type1 != null)
						{
							TypeConverter typeConverter = type1.GetConstructor(new Type[0]).Invoke(null) as TypeConverter;
							if (typeConverter.CanConvertFrom(typeof(string)))
							{
								item = typeConverter.ConvertFrom(null, CultureInfo.CurrentCulture, propertyDatum.StringValue);
								flag2 = true;
							}
						}
					}
				}
				if (flag3 || item == null)
				{
					object value = null;
					if (owner != null && !string.IsNullOrEmpty(dataInfo.PropertyName))
					{
						PropertyInfo propertyInfo = owner.GetType().GetProperty(dataInfo.PropertyName);
						if (propertyInfo != null)
						{
							value = propertyInfo.GetValue(owner, null);
						}
					}
					bool flag4 = true;
					IProvidePersistenceLookupKeys providePersistenceLookupKey = value as IProvidePersistenceLookupKeys;
					if (providePersistenceLookupKey != null && providePersistenceLookupKey.CanRehydrate(propertyDatum.LookUpKeys))
					{
						flag4 = false;
						item = value;
						IEnumerable enumerable = (IEnumerable)item;
						int num = 0;
						foreach (object obj in enumerable)
						{
							if (num < propertyDatum.Properties.Count)
							{
								PropertyDataInfo propertyDataInfo = propertyDatum.Properties[num];
								if (!this._reverseObjectStore.ContainsKey(propertyDataInfo.DataKey))
								{
									PropertyData propertyDatum1 = this.ResolvePropertyData(propertyDataInfo.DataKey);
									this._reverseObjectStore.Add(propertyDataInfo.DataKey, obj);
									IProvideCustomPersistence provideCustomPersistence = obj as IProvideCustomPersistence;
									if (provideCustomPersistence == null)
									{
										this.UnloadPropertyTree(obj, propertyDatum1.Properties, flag);
									}
									else
									{
										provideCustomPersistence.Load(owner, propertyDatum1.StringValue);
									}
								}
								num++;
							}
							else
							{
								flag4 = true;
								break;
							}
						}
						if (!flag4)
						{
							return item;
						}
					}
					if (flag4)
					{
						Type type2 = this.ResolveType(propertyDatum.AssemblyTypeKey);
						if (value != null && !(value is Brush) && value.GetType() == type2)
						{
							item = value;
						}
						else if (type2 != typeof(object) && type2 != null && !PersistenceManager.TypesToIgnore.Contains(type2))
						{
							ConstructorInfo constructor = type2.GetConstructor(new Type[0]);
							if (constructor != null)
							{
								item = constructor.Invoke(new object[0]);
								INeedInitializationForPersistence needInitializationForPersistence = item as INeedInitializationForPersistence;
								if (needInitializationForPersistence != null)
								{
									needInitializationForPersistence.InitObject(this._rootObject);
								}
							}
						}
						if (item != null)
						{
							if (!this._reverseObjectStore.ContainsKey(dataKey))
							{
								this._reverseObjectStore.Add(dataKey, item);
							}
							IProvideCustomPersistence provideCustomPersistence1 = item as IProvideCustomPersistence;
							if (provideCustomPersistence1 == null)
							{
								this.UnloadPropertyTree(item, propertyDatum.Properties, flag);
							}
							else
							{
								provideCustomPersistence1.Load(owner, propertyDatum.StringValue);
							}
						}
					}
				}
				else
				{
					if (!this._reverseObjectStore.ContainsKey(dataKey))
					{
						this._reverseObjectStore.Add(dataKey, item);
					}
					IProvideCustomPersistence provideCustomPersistence2 = item as IProvideCustomPersistence;
					if (provideCustomPersistence2 == null)
					{
						this.UnloadPropertyTree(item, propertyDatum.Properties, flag);
					}
					else
					{
						provideCustomPersistence2.Load(owner, propertyDatum.StringValue);
					}
				}
			}
			if (property != null)
			{
				try
				{
					if (property.GetSetMethod() == null && property.PropertyType.GetInterface("IList", true) != null)
					{
						if (!flag1)
						{
							object value1 = property.GetValue(owner, null);
							IProvideCustomPersistence provideCustomPersistence3 = value1 as IProvideCustomPersistence;
							if (provideCustomPersistence3 == null)
							{
								IList lists = value1 as IList;
								if (lists != null && !lists.IsReadOnly)
								{
									if (!this._reverseObjectStore.ContainsKey(dataKey))
									{
										this._reverseObjectStore.Add(dataKey, lists);
									}
									if (flag2)
									{
										IList lists1 = item as IList;
										if (lists1 != null)
										{
											foreach (object obj1 in lists1)
											{
												lists.Add(obj1);
											}
										}
									}
									else if (!lists.IsReadOnly)
									{
										lists.Clear();
										foreach (PropertyDataInfo property1 in propertyDatum.Properties)
										{
											object obj2 = this.LoadProperty(null, property1, flag);
											lists.Add(obj2);
										}
									}
								}
							}
							else
							{
								provideCustomPersistence3.Load(owner, propertyDatum.StringValue);
							}
						}
					}
					else if (item == null)
					{
						this.SetProperty(property, owner, item, propertyDatum.Properties);
					}
					else
					{
						if (!this._reverseObjectStore.ContainsKey(dataKey))
						{
							this._reverseObjectStore.Add(dataKey, item);
						}
						IList lists2 = item as IList;
						if (lists2 == null || flag1)
						{
							this.SetProperty(property, owner, item, propertyDatum.Properties);
						}
						else
						{
							if (!flag2)
							{
								lists2.Clear();
								foreach (PropertyDataInfo propertyDataInfo1 in propertyDatum.Properties)
								{
									object obj3 = this.LoadProperty(null, propertyDataInfo1, flag);
									lists2.Add(obj3);
								}
							}
							this.SetProperty(property, owner, lists2, propertyDatum.Properties);
						}
					}
				}
				catch (Exception exception1)
				{
					Exception exception = exception1;
					this._exceptions.Add(new PropertyPersistenceExceptionDetails(exception, property, dataInfo.PropertyPath));
				}
			}
			IProvideCustomStorage provideCustomStorage = item as IProvideCustomStorage;
			if (provideCustomStorage != null && propertyDatum != null)
			{
				provideCustomStorage.Load(propertyDatum.CustomStorage);
			}
			return item;
		}

		private static void PersistenceGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
		{
			if (e.OldValue != null)
			{
				((PersistenceGroup)e.OldValue).UnregisterObject(obj);
			}
			if (e.NewValue != null)
			{
				((PersistenceGroup)e.NewValue).RegisterObject(obj);
			}
		}

		private PropertyData ResolvePropertyData(int key)
		{
			if (!this._propDataStore.ContainsKey(key))
			{
				return null;
			}
			return this._propDataStore[key];
		}

		internal Type ResolveType(int key)
		{
			return Type.GetType(this._typeStore[key]);
		}

		/// <summary>
		/// Saves all objects in the group into a single MemoryStream.
		/// </summary>
		/// <param name="group">The group that contains the objects to save.</param>
		public static MemoryStream Save(PersistenceGroup group)
		{
			return PersistenceManager.SaveGroup(group, null);
		}

		/// <summary>
		/// Saves the properties of an object into a <see cref="T:System.IO.MemoryStream" />
		/// </summary>
		/// <param name="obj">The object that should be saved.</param>
		public static MemoryStream Save(DependencyObject obj)
		{
			return PersistenceManager.Save(obj, PersistenceManager.GetSettings(obj));
		}

		/// <summary>
		/// Saves the properties of an object into a <see cref="T:System.IO.MemoryStream" />
		/// </summary>
		/// <param name="obj">The object that should be saved.</param>
		/// <param name="settings">The settings that should be followed while saving properties of the object.</param>
		public static MemoryStream Save(DependencyObject obj, PersistenceSettings settings)
		{
			PersistenceGroup persistenceGroup = new PersistenceGroup();
			persistenceGroup.RegisterObject(obj);
			return PersistenceManager.SaveGroup(persistenceGroup, settings);
		}

		private static MemoryStream SaveGroup(PersistenceGroup group, PersistenceSettings groupSettings)
		{
			Dictionary<int, string> nums = new Dictionary<int, string>();
			Collection<Type> types = new Collection<Type>();
			Collection<ElementData> elementDatas = new Collection<ElementData>();
			GroupData groupDatum = new GroupData();
			foreach (DependencyObject registeredObject in group.RegisteredObjects)
			{
				ElementData elementDatum = new ElementData();
				PersistenceSettings persistenceSetting = groupSettings ?? PersistenceManager.GetSettings(registeredObject) ?? new PersistenceSettings();
				PersistenceManager persistenceManager = new PersistenceManager(group, persistenceSetting, registeredObject, nums, types);
				elementDatum.PropertyTree = persistenceManager.BuildPropertyTree();
				Collection<PropertyDataPair> propertyDataPairs = new Collection<PropertyDataPair>();
				foreach (KeyValuePair<int, PropertyData> propertyDataStore in persistenceManager.PropertyDataStore)
				{
					PropertyDataPair propertyDataPair = new PropertyDataPair()
					{
						LookupKey = propertyDataStore.Key,
						Data = propertyDataStore.Value
					};
					propertyDataPairs.Add(propertyDataPair);
				}
				elementDatum.PropDataStore = propertyDataPairs;
				elementDatum.TypeLookupKey = persistenceManager.StoreType(registeredObject.GetType());
				elementDatas.Add(elementDatum);
			}
			groupDatum.Elements = elementDatas;
			Collection<TypeDataPair> typeDataPairs = new Collection<TypeDataPair>();
			foreach (KeyValuePair<int, string> num in nums)
			{
				TypeDataPair typeDataPair = new TypeDataPair()
				{
					LookupKey = num.Key,
					TypeName = num.Value
				};
				typeDataPairs.Add(typeDataPair);
			}
			groupDatum.TypeStore = typeDataPairs;
			Collection<string> strs = new Collection<string>();
			foreach (Type type in types)
			{
				strs.Add(type.AssemblyQualifiedName);
			}
			XmlSerializer xmlSerializer = new XmlSerializer(typeof(GroupData), types.ToArray<Type>());
			StringBuilder stringBuilder = new StringBuilder();
			xmlSerializer.Serialize(new StringWriter(stringBuilder, CultureInfo.InvariantCulture), groupDatum);
			MemoryStream memoryStream = new MemoryStream();
			PersistenceObject persistenceObject = new PersistenceObject()
			{
				Types = strs,
				Xml = stringBuilder.ToString()
			};
			(new XmlSerializer(typeof(PersistenceObject))).Serialize(memoryStream, persistenceObject);
			memoryStream.Position = (long)0;
			PersistenceSavedEventArgs persistenceSavedEventArg = new PersistenceSavedEventArgs();
			group.Events.OnPersistenceSaved(persistenceSavedEventArg);
			return memoryStream;
		}

		private PropertyDataInfo SaveProperty(string propertyPath, object value, PropertyInfo pi, bool validateAgainstIgnore)
		{
			IEnumerable enumerable;
			PropertyDataInfo propertyDataInfo;
			object[] customAttributes;
			this._currentPropertyDataKey = -1;
			string name = "";
			if (pi != null)
			{
				name = pi.Name;
				if (propertyPath.Length > 0)
				{
					propertyPath = string.Concat(propertyPath, ".");
				}
				propertyPath = string.Concat(propertyPath, name);
			}
			if (pi != null && (pi.CanRead && pi.GetGetMethod() != null && pi.GetGetMethod().IsStatic || pi.CanWrite && pi.GetSetMethod() != null && pi.GetSetMethod().IsStatic))
			{
				return null;
			}
			if (pi != null && pi.GetSetMethod() == null)
			{
				List<PropertyInfo> list = (
					from property in (IEnumerable<PropertyInfo>)pi.PropertyType.GetProperties(BindingFlags.Instance | BindingFlags.Public)
					where property.CanWrite
					select property).ToList<PropertyInfo>();
				if (value == null || list.Count == 0 && !(value is IEnumerable) || value is FrameworkElement)
				{
					return null;
				}
			}
			if (value == null && pi == null)
			{
				return null;
			}
			Type type = (value != null ? value.GetType() : pi.PropertyType);
			bool isValueType = type.IsValueType;
			bool isEnum = type.IsEnum;
			bool isNotPublic = type.IsNotPublic;
			bool isPrimitive = type.IsPrimitive;
			if (value != null && this._objectStore.ContainsKey(value))
			{
				if (!isValueType)
				{
					if (this._settings.SavePersistenceOptions == PersistenceOption.OnlySpecified && !this._propDataStore.ContainsKey(this._objectStore[value]) && this._settings.PropertySettings.GetPropertyPersistenceInfo(pi, propertyPath) == null)
					{
						return null;
					}
					PropertyDataInfo propertyDataInfo1 = new PropertyDataInfo()
					{
						PropertyName = name,
						PropertyPath = propertyPath,
						DataKey = this._objectStore[value]
					};
					return propertyDataInfo1;
				}
				this._currentPropertyDataKey = this._objectStore[value];
			}
			PersistenceOption savePersistenceOptions = this._settings.SavePersistenceOptions;
			PropertyPersistenceInfoBase propertyPersistenceInfo = this._settings.PropertySettings.GetPropertyPersistenceInfo(pi, propertyPath);
			if (propertyPersistenceInfo == null)
			{
				if (savePersistenceOptions == PersistenceOption.OnlySpecified && validateAgainstIgnore)
				{
					goto Label1;
				}
				propertyPersistenceInfo = this._settings.IgnorePropertySettings.GetPropertyPersistenceInfo(pi, propertyPath);
				if (propertyPersistenceInfo != null)
				{
					if (value != null && this._objectStore.ContainsKey(value))
					{
						int item = this._objectStore[value];
						if (!this._propDataStore.ContainsKey(item))
						{
							this._objectStore.Remove(value);
						}
					}
					return null;
				}
			}
			else
			{
				string str = propertyPersistenceInfo.SaveValueToString(pi, propertyPath, value);
				if (string.IsNullOrEmpty(str))
				{
					str = propertyPersistenceInfo.SaveValue;
				}
				if (!string.IsNullOrEmpty(str))
				{
					return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, propertyPersistenceInfo.SaveValue, null, null);
				}
				if (propertyPersistenceInfo.TypeConverter != null && propertyPersistenceInfo.TypeConverter.CanConvertFrom(typeof(string)) && propertyPersistenceInfo.TypeConverter.CanConvertTo(typeof(string)))
				{
					return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, propertyPersistenceInfo.TypeConverter.ConvertToString(value), propertyPersistenceInfo.TypeConverter.GetType().AssemblyQualifiedName, null);
				}
				if (!string.IsNullOrEmpty(propertyPersistenceInfo.Identifier))
				{
					return this.CreateAndStorePropertyData(value, name, propertyPath, type, propertyPersistenceInfo.Identifier, null, null, null);
				}
			}
			SavePropertyPersistenceEventArgs savePropertyPersistenceEventArg = new SavePropertyPersistenceEventArgs(this._rootObject, pi, propertyPath, value);
			this._settings.Events.OnSavePropertyPersistence(savePropertyPersistenceEventArg);
			this._group.Events.OnSavePropertyPersistence(savePropertyPersistenceEventArg);
			if (savePropertyPersistenceEventArg.Cancel)
			{
				if (value != null && this._objectStore.ContainsKey(value))
				{
					int num = this._objectStore[value];
					if (value != null && !this._propDataStore.ContainsKey(num))
					{
						this._objectStore.Remove(value);
					}
				}
				return null;
			}
			if (!string.IsNullOrEmpty(savePropertyPersistenceEventArg.SaveValue))
			{
				return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, savePropertyPersistenceEventArg.SaveValue, null, null);
			}
			if (!string.IsNullOrEmpty(savePropertyPersistenceEventArg.Identifier))
			{
				return this.CreateAndStorePropertyData(value, name, propertyPath, type, savePropertyPersistenceEventArg.Identifier, null, null, null);
			}
			value = savePropertyPersistenceEventArg.Value;
			if (value != null)
			{
				this.StoreProperty(value, null);
				DependencyObject dependencyObject = value as DependencyObject;
				if (dependencyObject != null)
				{
					string identifier = PersistenceManager.GetIdentifier(dependencyObject);
					if (!string.IsNullOrEmpty(identifier))
					{
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, identifier, null, null, null);
					}
				}
				if (PersistenceManager.TypesToIgnore.Contains(type) || isNotPublic && !typeof(Type).IsAssignableFrom(type))
				{
					int item1 = this._objectStore[value];
					if (!this._propDataStore.ContainsKey(item1))
					{
						this._objectStore.Remove(value);
					}
					return null;
				}
				IProvideCustomPersistence provideCustomPersistence = value as IProvideCustomPersistence;
				if (provideCustomPersistence != null)
				{
					return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, provideCustomPersistence.Save(), null, null);
				}
				bool flag = false;
				bool flag1 = false;
				string str1 = null;
				string converterTypeName = null;
				if (!PersistenceManager.PredefinedTypeConverters.ContainsKey(type))
				{
					if (pi != null)
					{
						customAttributes = pi.GetCustomAttributes(typeof(TypeConverterAttribute), true);
					}
					else
					{
						customAttributes = null;
					}
					object[] objArray = customAttributes;
					if (objArray == null || (int)objArray.Length == 0)
					{
						objArray = type.GetCustomAttributes(typeof(TypeConverterAttribute), true);
					}
					object[] objArray1 = objArray;
					for (int i = 0; i < (int)objArray1.Length; i++)
					{
						TypeConverterAttribute typeConverterAttribute = (TypeConverterAttribute)objArray1[i];
						TypeConverter typeConverter = Type.GetType(typeConverterAttribute.ConverterTypeName).GetConstructor(new Type[0]).Invoke(null) as TypeConverter;
						typeConverter.GetType();
						if (typeConverter.CanConvertTo(typeof(string)))
						{
							flag = true;
							str1 = typeConverter.ConvertToString(value);
						}
						if (typeConverter.CanConvertFrom(typeof(string)))
						{
							converterTypeName = typeConverterAttribute.ConverterTypeName;
							flag1 = true;
						}
					}
				}
				else
				{
					TypeConverter typeConverter1 = PersistenceManager.PredefinedTypeConverters[type];
					str1 = typeConverter1.ConvertToString(value);
					int num1 = 1;
					flag = (bool)num1;
					flag1 = (bool)num1;
					converterTypeName = typeConverter1.GetType().FullName;
				}
				enumerable = value as IEnumerable;
				if (flag1 && flag)
				{
					return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, str1, converterTypeName, null);
				}
				if (enumerable != null && value.GetType() != typeof(string))
				{
					goto Label2;
				}
				if (isValueType || isEnum)
				{
					bool flag2 = true;
					PropertyInfo propertyInfo = type.GetProperty("IsEmpty");
					if (propertyInfo != null)
					{
						object obj = propertyInfo.GetValue(value, null);
						if (obj != null)
						{
							flag2 = !(bool)obj;
						}
					}
					if (flag2)
					{
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, true, null, null, null);
					}
				}
				else
				{
					if (value is string || isPrimitive || value is Color)
					{
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, value.ToString(), null, null);
					}
					if (type.GetConstructor(new Type[0]) != null)
					{
						string str2 = "";
						Collection<PropertyDataInfo> propertyDataInfos = this.BuildPropertyTree(propertyPath, value, false);
						if (propertyDataInfos == null || propertyDataInfos.Count == 0)
						{
							str2 = value.ToString();
						}
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, str2, null, propertyDataInfos);
					}
					if (value is Type)
					{
						return this.CreateAndStorePropertyData(value, name, propertyPath, typeof(Type), null, ((Type)value).AssemblyQualifiedName, null, null);
					}
				}
			}
			else if (pi != null)
			{
				return this.CreateAndStorePropertyData(null, name, propertyPath, pi.PropertyType, null, null, null, null);
			}
			if (value != null && this._objectStore.ContainsKey(value))
			{
				int item2 = this._objectStore[value];
				if (!this._propDataStore.ContainsKey(item2))
				{
					this._objectStore.Remove(value);
				}
			}
			return null;
		Label1:
			if (value != null)
			{
				propertyPersistenceInfo = this._settings.IgnorePropertySettings.GetPropertyPersistenceInfo(pi, propertyPath);
				if (propertyPersistenceInfo != null)
				{
					return null;
				}
				IEnumerable enumerable1 = value as IEnumerable;
				if (enumerable1 != null && value.GetType() != typeof(string))
				{
					Collection<PropertyDataInfo> propertyDataInfos1 = new Collection<PropertyDataInfo>();
					foreach (object obj1 in enumerable1)
					{
						PropertyDataInfo propertyDataInfo2 = this.SaveProperty(string.Concat(propertyPath, "[]"), obj1, null, true);
						if (propertyDataInfo2 != null)
						{
							propertyDataInfos1.Add(propertyDataInfo2);
						}
						else
						{
							if (value != null && this._objectStore.ContainsKey(value))
							{
								int num2 = this._objectStore[value];
								if (!this._propDataStore.ContainsKey(num2))
								{
									this._objectStore.Remove(value);
								}
							}
							propertyDataInfo = null;
							return propertyDataInfo;
						}
					}
					if (propertyDataInfos1.Count > 0)
					{
						Collection<string> lookupKeys = null;
						IProvidePersistenceLookupKeys providePersistenceLookupKey = enumerable1 as IProvidePersistenceLookupKeys;
						if (providePersistenceLookupKey != null)
						{
							lookupKeys = providePersistenceLookupKey.GetLookupKeys();
						}
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, false, null, null, propertyDataInfos1, lookupKeys);
					}
				}
				else if (!isValueType && !isEnum && !isEnum && !(value is string) && !isPrimitive && !(value is Color) && !PersistenceManager.TypesToIgnore.Contains(type) && type.GetConstructor(new Type[0]) != null)
				{
					this.StoreProperty(value, null);
					Collection<PropertyDataInfo> propertyDataInfos2 = this.BuildPropertyTree(propertyPath, value, true);
					if (propertyDataInfos2.Count > 0)
					{
						return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, null, null, propertyDataInfos2);
					}
				}
			}
			return null;
		Label2:
			Collection<PropertyDataInfo> propertyDataInfos3 = new Collection<PropertyDataInfo>();
			foreach (object obj2 in enumerable)
			{
				PropertyDataInfo propertyDataInfo3 = this.SaveProperty(string.Concat(propertyPath, "[]"), obj2, null, false);
				if (propertyDataInfo3 != null)
				{
					propertyDataInfos3.Add(propertyDataInfo3);
				}
				else
				{
					if (value != null && this._objectStore.ContainsKey(value))
					{
						int item3 = this._objectStore[value];
						if (!this._propDataStore.ContainsKey(item3))
						{
							this._objectStore.Remove(value);
						}
					}
					propertyDataInfo = null;
					return propertyDataInfo;
				}
			}
			Collection<string> strs = null;
			IProvidePersistenceLookupKeys providePersistenceLookupKey1 = enumerable as IProvidePersistenceLookupKeys;
			if (providePersistenceLookupKey1 != null)
			{
				strs = providePersistenceLookupKey1.GetLookupKeys();
			}
			return this.CreateAndStorePropertyData(value, name, propertyPath, type, null, false, null, null, propertyDataInfos3, strs);
		}

		/// <summary>
		/// Sets the identifier on on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="id"></param>
		public static void SetIdentifier(DependencyObject obj, string id)
		{
			obj.SetValue(PersistenceManager.IdentifierProperty, id);
		}

		/// <summary>
		/// Sets the <see cref="T:Infragistics.Persistence.PersistenceGroup" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="group"></param>
		public static void SetPersistenceGroup(DependencyObject obj, PersistenceGroup group)
		{
			obj.SetValue(PersistenceManager.PersistenceGroupProperty, group);
		}

		private void SetProperty(PropertyInfo pi, object owner, object propertyValue, Collection<PropertyDataInfo> props)
		{
			if (pi != null)
			{
				if (pi.GetSetMethod() == null)
				{
					object value = pi.GetValue(owner, null);
					if (value != null)
					{
						this.UnloadPropertyTree(value, props, false);
					}
				}
				else
				{
					object obj = pi.GetValue(owner, null);
					if (obj != propertyValue)
					{
						if (obj == null)
						{
							pi.SetValue(owner, propertyValue, null);
							return;
						}
						if (!obj.Equals(propertyValue))
						{
							pi.SetValue(owner, propertyValue, null);
							return;
						}
					}
					else if (propertyValue is Style)
					{
						pi.SetValue(owner, null, null);
						pi.SetValue(owner, propertyValue, null);
						return;
					}
				}
			}
		}

		/// <summary>
		/// Sets the <see cref="T:Infragistics.Persistence.PersistenceSettings" /> on a <see cref="T:System.Windows.DependencyObject" />.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="settings"></param>
		public static void SetSettings(DependencyObject obj, PersistenceSettings settings)
		{
			obj.SetValue(PersistenceManager.SettingsProperty, (object)settings);
		}

		private int StoreProperty(object data, PropertyData propData)
		{
			if (data == null)
			{
				data = propData;
			}
			int count = this._objectStore.Count;
			if (!this._objectStore.ContainsKey(data))
			{
				this._objectStore.Add(data, count);
			}
			else
			{
				count = this._objectStore[data];
			}
			if (propData != null && !this._propDataStore.ContainsKey(count))
			{
				this._propDataStore.Add(count, propData);
			}
			return count;
		}

		internal int StoreType(Type t)
		{
			string assemblyQualifiedName = t.AssemblyQualifiedName;
			int hashCode = assemblyQualifiedName.GetHashCode();
			if (!this._typeStore.ContainsKey(hashCode))
			{
				this._typeStore.Add(hashCode, assemblyQualifiedName);
			}
			return hashCode;
		}

		private static string SynchQualifiedTypeNameVersion(string oldQualifiedTypeName)
		{
			if (oldQualifiedTypeName == null)
			{
				return oldQualifiedTypeName;
			}
			string[] infragisticsLiteral = new string[] { PersistenceManager.AssemblyLoadInfo.Infragistics_Literal };
			string[] strArrays = oldQualifiedTypeName.Split(infragisticsLiteral, StringSplitOptions.None);
			bool flag = oldQualifiedTypeName.StartsWith(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal);
			if (!flag && (int)strArrays.Length < 2 || flag && (int)strArrays.Length >= 1 && strArrays[1].Contains("Testing.Tests.Persistence"))
			{
				return oldQualifiedTypeName;
			}
			StringBuilder stringBuilder = new StringBuilder();
			if (!flag)
			{
				stringBuilder.Append(strArrays[0]);
			}
			for (int i = 1; i < (int)strArrays.Length; i++)
			{
				stringBuilder.Append(PersistenceManager.SynchSection(strArrays[i]));
			}
			return stringBuilder.ToString();
		}

		private static string SynchSection(string oldSection)
		{
			AssemblyName assemblyName;
			string str;
			string str1;
			string[] strArrays = new string[] { "]" };
			string[] strArrays1 = oldSection.Split(strArrays, StringSplitOptions.None);
			if (strArrays1[0].Length <= PersistenceManager._AssemblyLoadInfo._infragisticsSuffix.Length)
			{
				return string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, oldSection);
			}
			if (PersistenceManager._AssemblyLoadInfo._infragisticsSuffix.Length > 0 && strArrays1[0].Substring(0, PersistenceManager._AssemblyLoadInfo._infragisticsSuffix.Length) != PersistenceManager._AssemblyLoadInfo._infragisticsSuffix)
			{
				return string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, oldSection);
			}
			string str2 = string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, strArrays1[0]);
			try
			{
				assemblyName = new AssemblyName(str2);
				goto Label0;
			}
			catch (Exception exception)
			{
				str1 = string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, oldSection);
			}
			return str1;
		Label0:
			Version version = assemblyName.Version;
			if (version == PersistenceManager._AssemblyLoadInfo._assemblyVersion)
			{
				return string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, oldSection);
			}
			if (version.Major == 10 && version.Minor == 1)
			{
				string name = assemblyName.Name;
				int num = name.IndexOf(".v", 0);
				if (num > 0)
				{
					name = name.Substring(0, num);
				}
				if (PersistenceManager.UpgradeAssemblyDictionary.TryGetValue(name, out str))
				{
					str2 = str2.Replace(name, str);
					assemblyName = new AssemblyName(str2);
				}
			}
			assemblyName.Version = PersistenceManager._AssemblyLoadInfo._assemblyVersion;
			StringBuilder stringBuilder = new StringBuilder();
			string fullName = assemblyName.FullName;
			if (PersistenceManager._AssemblyLoadInfo._platformVersionSuffix.Length != 0)
			{
				int num1 = fullName.IndexOf(".", 1);
				int num2 = fullName.IndexOf(".v", num1);
				if (num2 >= num1)
				{
					int num3 = fullName.IndexOf(",", num2);
					stringBuilder.Append(PersistenceManager._AssemblyLoadInfo._infragisticsWithPlatformSuffix);
					stringBuilder.Append(PersistenceManager._AssemblyLoadInfo._platformVersionSuffix);
					stringBuilder.Append(fullName.Substring(num1, num2 - num1));
					stringBuilder.Append(PersistenceManager._AssemblyLoadInfo._versionSuffix);
					stringBuilder.Append(fullName.Substring(num3));
				}
			}
			else
			{
				stringBuilder.Append(fullName);
			}
			for (int i = 1; i < (int)strArrays1.Length; i++)
			{
				stringBuilder.Append("]");
				stringBuilder.Append(strArrays1[i]);
			}
			return stringBuilder.ToString();
		}

		/// <summary>
		/// Walks through a tree of <see cref="T:Infragistics.Persistence.Primitives.PropertyDataInfo" /> objects, and attempts to rehydrate the root object in which this <see cref="T:Infragistics.Persistence.PersistenceManager" /> represents, with those values.
		/// </summary>
		/// <param name="tree"></param>
		public void UnloadPropertyTree(Collection<PropertyDataInfo> tree)
		{
			this._reverseObjectStore.Add(0, this._rootObject);
			this._propDataStore.Add(0, new PropertyData()
			{
				Properties = tree
			});
			this.UnloadPropertyTree(this._rootObject, tree, true);
			PersistenceLoadedEventArgs persistenceLoadedEventArg = new PersistenceLoadedEventArgs(new ReadOnlyCollection<PropertyPersistenceExceptionDetails>(this._exceptions));
			this._settings.Events.OnPersistenceLoaded(persistenceLoadedEventArg);
		}

		private void UnloadPropertyTree(object owner, Collection<PropertyDataInfo> propertyTree, bool validateAgainstIgnore)
		{
			IProvidePropertyPersistenceSettings providePropertyPersistenceSetting = owner as IProvidePropertyPersistenceSettings;
			IProvideCustomObjectPersistence provideCustomObjectPersistence = owner as IProvideCustomObjectPersistence;
			foreach (PropertyDataInfo propertyDataInfo in propertyTree)
			{
				if (provideCustomObjectPersistence == null || !(propertyDataInfo.PropertyPath == "PersistenceCustomObject"))
				{
					this.LoadProperty(owner, propertyDataInfo, validateAgainstIgnore);
				}
				else
				{
					provideCustomObjectPersistence.LoadObject(this.LoadProperty(owner, propertyDataInfo, false));
				}
			}
			if (providePropertyPersistenceSetting != null)
			{
				providePropertyPersistenceSetting.FinishedLoadingPersistence();
			}
		}

		private class AssemblyLoadInfo
		{
			internal readonly static string Infragistics_Literal;

			internal Version _assemblyVersion;

			internal string _infragisticsWithPlatformSuffix;

			internal string _infragisticsSuffix;

			internal string _platformVersionSuffix;

			internal string _versionSuffix;

			static AssemblyLoadInfo()
			{
				PersistenceManager.AssemblyLoadInfo.Infragistics_Literal = "Infragistics";
			}

			internal AssemblyLoadInfo()
			{
				string assemblyQualifiedName = typeof(PersistenceManager.AssemblyLoadInfo).AssemblyQualifiedName;
				int num = assemblyQualifiedName.IndexOf(", ") + 2;
				string str = assemblyQualifiedName.Substring(num);
				this._assemblyVersion = (new AssemblyName(str)).Version;
				this._infragisticsSuffix = "SL";
				this._infragisticsWithPlatformSuffix = string.Concat(PersistenceManager.AssemblyLoadInfo.Infragistics_Literal, this._infragisticsSuffix);
				int length = this._infragisticsWithPlatformSuffix.Length;
				int num1 = str.IndexOf(".", length);
				this._platformVersionSuffix = str.Substring(length, num1 - length);
				int num2 = str.IndexOf(".v", num1 + 1);
				if (num2 <= num1)
				{
					this._versionSuffix = string.Empty;
					return;
				}
				int num3 = str.IndexOf(",", num2);
				this._versionSuffix = str.Substring(num2, num3 - num2);
			}
		}
	}
}