using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to pass information for when an object has been loaded. 
	/// </summary>
	public class PersistenceLoadedEventArgs : EventArgs
	{
		/// <summary>
		/// Gets a list of <see cref="T:Infragistics.Persistence.PropertyPersistenceExceptionDetails" /> objects, for properties that could not be loaded. 
		/// </summary>
		public ReadOnlyCollection<PropertyPersistenceExceptionDetails> PropertyExceptions
		{
			get;
			private set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T:Infragistics.Persistence.PersistenceLoadedEventArgs" /> object. 
		/// </summary>
		/// <param name="exceptions"></param>
		public PersistenceLoadedEventArgs(ReadOnlyCollection<PropertyPersistenceExceptionDetails> exceptions)
		{
			this.PropertyExceptions = exceptions;
		}
	}
}