using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to pass information about a property being saved or loaded. 
	/// </summary>
	public abstract class PropertyPersistenceEventArgs : EventArgs
	{
		/// <summary>
		/// Gets the PropertyInfo of an object being saved or loaded. 
		/// </summary>
		public System.Reflection.PropertyInfo PropertyInfo
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the full property path of an object being saved or loaded. 
		/// </summary>
		public string PropertyPath
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the root object that is actually being saved or loaded. 
		/// </summary>
		public DependencyObject RootOwner
		{
			get;
			private set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T:Infragistics.Persistence.PropertyPersistenceEventArgs" /> object. 
		/// </summary>
		/// <param name="rootOwner"></param>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		protected PropertyPersistenceEventArgs(DependencyObject rootOwner, System.Reflection.PropertyInfo pi, string propertyPath)
		{
			this.PropertyPath = propertyPath;
			this.PropertyInfo = pi;
			this.RootOwner = rootOwner;
		}
	}
}