using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object that can be used to identify a Property by its Type.
	/// </summary>
	public class PropertyTypePersistenceInfo : PropertyPersistenceInfoBase
	{
		/// <summary>
		/// Gets/sets the Type of the property that will be used to identify a property while its being loaded or saved.
		/// </summary>
		public Type PropertyType
		{
			get;
			set;
		}

		public PropertyTypePersistenceInfo()
		{
		}

		/// <summary>
		/// Validates if the property type matches the property that is being passed in. 
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <returns></returns>
		public override bool DoesPropertyMeetCriteria(PropertyInfo pi, string propertyPath)
		{
			if (pi == null)
			{
				return false;
			}
			return pi.PropertyType == this.PropertyType;
		}
	}
}