using System;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to pass information for when an object has been saved. 
	/// </summary>
	public class PersistenceSavedEventArgs : EventArgs
	{
		public PersistenceSavedEventArgs()
		{
		}
	}
}