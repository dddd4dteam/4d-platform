using System;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store the AssemblyQualifiedName of a type with its lookup key in a format that can be serialized.
	/// </summary>
	public class TypeDataPair
	{
		/// <summary>
		/// Gets/sets the unique key that identifies the Type
		/// </summary>
		public int LookupKey
		{
			get;
			set;
		}

		/// <summary>
		/// The AssemblyQualifiedName of a Type.
		/// </summary>
		public string TypeName
		{
			get;
			set;
		}

		public TypeDataPair()
		{
		}
	}
}