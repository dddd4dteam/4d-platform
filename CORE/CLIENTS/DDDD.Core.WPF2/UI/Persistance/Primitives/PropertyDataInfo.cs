using System;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store information about a specific property that has been serialzied.
	/// </summary>
	public class PropertyDataInfo
	{
		/// <summary>
		/// The look up key used to identify the property values of this property.
		/// </summary>
		public int DataKey
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the Name of the Property being stored.
		/// </summary>
		public string PropertyName
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the full property path of a a property.
		/// </summary>
		public string PropertyPath
		{
			get;
			set;
		}

		public PropertyDataInfo()
		{
		}
	}
}