using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store all the information to serialize or deserialize an object. 
	/// </summary>
	public class ElementData
	{
		/// <summary>
		/// Gets/sets a list of all the property information that has been stored for this object. 
		/// </summary>
		public Collection<PropertyDataPair> PropDataStore
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a list of all the root properties that belong to the object. 
		/// </summary>
		public Collection<PropertyDataInfo> PropertyTree
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets they lookup key for the type of <see cref="T:System.Windows.DependencyObject" /> that this <see cref="T:Infragistics.Persistence.Primitives.ElementData" /> represents.
		/// </summary>
		public int TypeLookupKey
		{
			get;
			set;
		}

		public ElementData()
		{
		}
	}
}