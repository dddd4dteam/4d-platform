using System;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// Describes the options available for finding properties by a name.
	/// </summary>
	public enum PropertyNamePersistenceOptions
	{
		/// <summary>
		/// Looks for the specific name. 
		/// </summary>
		PropertyName,
		/// <summary>
		/// Looks for the specific name in the property path. 
		/// </summary>
		PropertyPath,
		/// <summary>
		/// Looks in the Propertypath, and see if the specified name is in there.
		/// </summary>
		Contains
	}
}