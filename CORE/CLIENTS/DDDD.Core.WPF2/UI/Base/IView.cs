﻿using System.Windows;

namespace DDDD.Core.UI
{
    public interface IView
    {
        IHostWindow HostWindow
        { get; }
        void SetHostWindow(IHostWindow hostWindow);

        string HostHeader { get; }
    }



}
