﻿using WPF = System.Windows.Controls;

namespace DDDD.Core.UI
{



    public interface IHostWindow
    {
        void SetHeader(string header);

        void SetChild(WPF.UserControl wpfControl);

        /// <summary>
        /// Hide on CLose event - to make this window available to Cache,
        /// and Use Show() to show it on next time.
        /// </summary>
        bool HideOnly { get; set; }

    }

}
