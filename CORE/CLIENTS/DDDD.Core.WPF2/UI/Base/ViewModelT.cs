﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

using DDDD.Core.Extensions;

namespace DDDD.Core.UI
{

    /// <summary>
    /// VIewModel task areas: 
    ///     1 all UI control behaviors 
    ///     2     UI Coposition with TabItem controlss,Border controls,StackPanel controls
    ///     3 all Data Loading/Clearing/Attaching-for Binding             
    /// </summary>
    /// <typeparam name="TView"></typeparam>
    public class ViewModel<TView>
        where TView: UserControl,IView, new()
    {
       protected  ViewModel(TView associatedView)
        {
            View = associatedView;           
        }
        
        public ViewModel<TView> InitLoad()
        {          
            Initialize();
            //Load

            return this;
        }

        protected TView View
        {  get;  private set;  }

        protected Dictionary<string, bool> TabInitialized
        { get; set; } = new Dictionary<string, bool>();


        protected virtual void Initialize()
        {
            Init_ComposeTabItems();
            Init_ComposeBorders();
            Init_ComposeStackPanels();

            Init_AttachControlEventHandlers();

        }

        /// <summary>
        /// 1 COMPOSE TAB CONTROLS
        /// </summary>
       protected void Init_ComposeTabItems()
        {
            if (typeof(TView).GetInterface(nameof(ITabCompositeView)).IsNotNull())
            {
                var tabItemAttribs = typeof(TView).
                    GetCustomAttributes(typeof(TabItemViewAttribute), false);

                foreach (TabItemViewAttribute tabItemView in tabItemAttribs)
                {
                    var tabItem = View.FindName(tabItemView.TabItemKey) as TabItem;
                    tabItem.Loaded += (s, e) =>
                    {
                        if (TabInitialized.ContainsKey(tabItemView.TabItemKey) == false)
                        {
                           // VAService.Current.UIM.SetTabItem(
                           // tabItem, tabItemView.ViewType);
                           // TabInitialized.Add(tabItemView.TabItemKey, true);
                        }
                    };
                }
            }
        }


        protected void Init_ComposeBorders()
        {

        }

        protected void Init_ComposeStackPanels()
        {

        }





        /// <summary>
        /// 2  ATTACH CLICK AND OTHER EVENT HANDLERS 
        /// </summary>
        protected virtual void Init_AttachControlEventHandlers()
        {

        }

    }
}
