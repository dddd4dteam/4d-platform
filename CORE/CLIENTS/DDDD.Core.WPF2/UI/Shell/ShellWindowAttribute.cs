﻿
#if  CLIENT && WPF

using System;
using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;


namespace DDDD.Core.UI
{

    /// <summary>
    /// Shell UI Control based on System.Windows.Window control class. 
    /// Usually it's WPF client.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class ShellWindowAttribute : Attribute , IComponentMatchMetaInfo
    {

        #region ------------------------------ CTOR -----------------------------

        // This is a positional argument
        public ShellWindowAttribute( string shellKey  )
        {
            ShellKey = shellKey;
        }

        #endregion ------------------------------ CTOR -----------------------------
        

        #region ---------------------- IComponentItemMetaInfo --------------------------

        /// <summary>
        /// Component Class Key 
        /// </summary>
        public string ComponentDefinitionKey
        {
            get
            {
                return ComponentClassEn.Windows.S();
            }
        }

        #endregion ---------------------- IComponentItemMetaInfo --------------------------


        /// <summary>
        /// Filter key that can differ one ShellControl from another.
        /// </summary>
        public string ShellKey
        { get; set; }


    }

}


#endif