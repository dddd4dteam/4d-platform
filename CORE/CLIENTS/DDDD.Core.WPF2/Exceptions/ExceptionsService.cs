﻿using DDDD.Core.Reflection;
using System;

namespace DDDD.Core.Exceptions
{
    // Discover, Register, Resolve instance 

    /// <summary>
    /// ExceptionsService - have the following features:   
    /// <para/> 1 Feature  - Precompile and Cache exception  Ctor  Func{TExc}(p1..pn) with the help of TypeActivator;    
    /// <para/> 2 Feature  - CreateException by  { object[] args }, arg  item can't be null   
    /// </summary>  
    public  class ExceptionsService
    {

         /// <summary>
         /// Creating Exception by cached ctor delegate. 
         /// </summary>
         /// <typeparam name="TException"></typeparam>
         /// <param name="args"></param>
         /// <returns></returns>   
         public static Exception CreateException<TException>(params object[] args)
             where TException : Exception
         {  
            return  TypeActivator.CreateInstanceT<TException>(TypeActivator.DefaultCtorSearchBinding, args);             
         }


    }
}



#region ------------------------------- GARBAGE ----------------------------------


/// <summary>
/// Precompile TException creation by all its ctors. 
///   External delegate loks like : IN ( args[])  -  OUT TException
///   Internal delegate loks like : IN (  argsTypes[0] arg1, argTypes[1]  arg2 )  -  OUT TException
/// </summary>
/// <typeparam name="TException"></typeparam>
//public static void Precompile<TException>()
//     where TException : Exception
//{
//    var tExc = typeof(TException);
//    Precompile(tExc);
//}



#endregion ------------------------------- GARBAGE ----------------------------------