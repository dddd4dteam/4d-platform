﻿using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DDDD.Core.ComponentModel
{

  


    /// <summary>
    /// ComponentMatch Info - associated with some Type - target ComponentType and its classification by it's [MetaInfo] property.
    /// </summary>
    public class ComponentMatch
    {

        #region --------------------------- CTOR ------------------------------
        
        ComponentMatch( Type componentType, ComponentDefinition componentClass)
        {
            ComponentClass = componentClass;
            Initialize(componentType);
        }

        #endregion --------------------------- CTOR ------------------------------



        const BindingFlags DefaultCtorSearchBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;


        /// <summary>
        /// You can add your singleton property Name here.
        ///<para/> You should care by yourself about thread safety initialization for your Singleton Instance.
        /// </summary>
        public static List<string> SingletonInstanceProperties
        { get; } = new List<string>() {"Current", "Instance" };
        

        /// <summary>
        /// ShortKey building rule is : Each Type.Name will be the ShortKey only after it'll be Trimmed with this TrimWords.
        /// <para/>  For EX: if Type.Name == 'SomeCoolManager' , we see this value Contains TrimWord-'Manager', then this 'Manager' endword will be removed from Type.Name and we'll got -'SomeCool' as the end ShortKey value. 
        /// </summary>
        public static List<string> ShortKeyTrimWords
        { get; } = new List<string>() { "Manager", "manager", "Service", "service" };




        #region --------------------------------- INITIALIZATION ---------------------------------------


        static PropertyInfo GetSingletonProperty(Type componentType)
        {
            for (int i = 0; i < SingletonInstanceProperties.Count; i++)
            {
                var singletonProperty = componentType.GetProperty(SingletonInstanceProperties[i], BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);
                if (singletonProperty.IsNotNull()) return singletonProperty;
            }

            return null;
        }


        /// <summary>
        /// Getting  ComponentItem.ShortKey by trimming from Type.Name all trim words. 
        /// All known trim words containing in ShortKeyTrimWords list - edit it as/if you need.
        /// <para/> By default we trimming words 'Manager' and 'Service' from Type.Name value.
        /// </summary>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public static string GetShortKey(string typeName)
        {
            var result = typeName;
            for (int i = 0; i < ShortKeyTrimWords.Count; i++)
            {
                result= result.Replace(ShortKeyTrimWords[i], "");
            }
            return result;
        }


        /// <summary>
        /// Initialize  ComponentItemInfo
        /// </summary>
        /// <param name="componentType"></param>
        protected virtual void Initialize(Type componentType)
        {
            try
            {
                ComponentType = componentType;
                TypeKey = ComponentType.FullName;
                ShortKey = GetShortKey(ComponentType.Name);

                ID_Hash = TypeKey.GetHashCode();

                AssemblyName = ComponentType.Assembly.GetAssemblyNameEx(); 
                
                // Mata Info can be null
                MetaInfo = ComponentType.GetAttributesWithBaseType<IComponentMatchMetaInfo>().FirstOrDefault();

                SingletonProperty = GetSingletonProperty(ComponentType);// ComponentType.GetProperty(SingletonInstanceProperty, BindingFlags.Static|BindingFlags.Public| BindingFlags.FlattenHierarchy);
                HasSingletonAccess = SingletonProperty.IsNotNull();
                if ( HasSingletonAccess )
                { SingletonInstance = SingletonProperty.CreateStaticPropertyGetDelegate<object>();//  ComponentType. GenerateSingletonInstanceCall(SingletonInstanceProperty);
                }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(ComponentMatch)}.{nameof(Initialize)}(): Exception Message- [{exc.Message}] ");
            }
        }


        #endregion --------------------------------- INITIALIZATION ---------------------------------------



        #region ----------------------------- PROPERTIES ---------------------------------

        /// <summary>
        /// Components Classification info - suggested Contract
        /// </summary>
        public ComponentDefinition ComponentClass
        { get; private set; }

     
        /// <summary>
        /// ComponentItemMetaAttribute inherited class 
        /// - end MetaInfo class, that contains some information about Component Class 
        /// for whome this component belongs to.
        /// </summary>
        public IComponentMatchMetaInfo MetaInfo
        {  get; private set; }
                

        /// <summary>
        /// End custom component class Type
        /// </summary>
        public Type ComponentType
        { get; protected set; }
        

        /// <summary>
        ///  TypeKey - is the value of ComponentType.FullName .
        /// </summary>
        public string TypeKey
        {  get; private set; }


        /// <summary>
        /// ShortKey - is the value of ComponentType.Name, but trimmed with const [service], [manager] or [Manager],[Service].
        /// </summary>
        public string ShortKey
        { get; private set; }


        /// <summary>
        /// ID - Hash value of the [TypeKey]  string property.
        /// </summary>
        public int ID_Hash
        { get; private set; }
        

        /// <summary>
        /// Name of Assembly. Used for Grouping Component by Assembly. 
        /// Automatically getted from [ComponentType] property value. 
        /// </summary>
        public string AssemblyName
        { get; private set; }


        /// <summary>
        /// Singleton pattern property. For ex. - [Current] or [Instance] called property.
        /// All enabled Property Names, that will be try to detect as [Singleton], contain  in static property [SingletonInstanceProperties]- you also can add your variant.
        /// </summary>
        protected PropertyInfo SingletonProperty
        { get; private set; }


        /// <summary>
        /// If ComponentType has Singleton pattern property [Current]- that must be used get instantiate of this ComponentType.
        /// <para/> If ComponentType really has property [Current], then Component will precompile call of this property to [].
        /// <para/> It means that Component Type  can be instantiated by 
        /// </summary>
        public bool HasSingletonAccess
        { get; private set; }


        /// <summary>
        /// Here Delegate is [Func{ComponentType}] delegate signature - call of singleton property [Current] of ComponentType.
        /// </summary>
        public Func<object> SingletonInstance
        { get; private set; }


        #endregion ----------------------------- PROPERTIES ---------------------------------



        #region ---------------------------- NEW COMPONENT ITEM INFO -------------------------------

        /// <summary>
        /// Create new ComponentItemInfo  for component of {T} Type.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ComponentMatch New<T>(ComponentDefinition componentClass)
        { return New(typeof(T), componentClass); }


        /// <summary>
        /// Create new CnmponentItemInfo  for [componentType]. 
        /// </summary>
        /// <param name="componentType"></param>
        /// <param name="componentClass"></param>
        /// <returns></returns>
        public static ComponentMatch New(Type componentType, ComponentDefinition componentClass)
        { return new ComponentMatch(componentType, componentClass); }

        #endregion ---------------------------- NEW COMPONENT ITEM INFO -------------------------------






        #region ------------------------------- RESOLVE INSTANCE ------------------------------


        /// <summary>
        /// Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// <para/>If targetComponent is null then method returns null;  
        /// <para/>If targetComponent has singleton getter delegate then Singleton instance'll be returned.
        /// <para/>Here we use TypeActvator with precompiling instance creation with such-default ctor.
        /// </summary>
        /// <param name="ignoreSingleton"></param>
        /// <returns></returns>
        public object ResolveInstanceBoxed(bool ignoreSingleton = false)
        {
            if (HasSingletonAccess && ignoreSingleton == false) return SingletonInstance();

            return ComponentType.CreateInstanceBoxed();
        }


        /// <summary>
        /// Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// <para/>If targetComponent is null then method returns null;  
        /// <para/>If targetComponent has singleton getter delegate then Singleton instance'll be returned.
        /// <para/>Here we use TypeActvator with precompiling instance creation with such-default ctor.
        /// </summary>
        /// <param name="ctorSearchBinding"></param>
        /// <param name="ignoreSingleton"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public object ResolveInstanceBoxed(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, bool ignoreSingleton = false, params object[] arguments)
        {
            if (HasSingletonAccess && ignoreSingleton == false) return SingletonInstance();

            return ComponentType.CreateInstanceBoxed( ctorSearchBinding, arguments);
        }


        /// <summary>
        ///  Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// </summary>
        /// <param name="ctorSearchBinding"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public object ResolveInstanceBoxedLazy(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] arguments)
        {
            if (HasSingletonAccess) return SingletonInstance();

            return ComponentType.CreateInstanceBoxedLazy(ctorSearchBinding, arguments);// 
        }


        /// <summary>
        /// Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// <para/>If targetComponent is null then method returns null;  
        /// <para/>If targetComponent has singleton getter delegate then Singleton instance'll be returned.
        /// <para/>Here we use TypeActvator with precompiling instance creation with such-default ctor. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ignoreSingleton"></param>
        /// <returns></returns>
        public T ResolveInstanceTBase<T>(bool ignoreSingleton = false)
        {
            if (HasSingletonAccess && ignoreSingleton == false) return (T)SingletonInstance();

            return ComponentType.CreateInstanceTBase<T>();
        }


        /// <summary>
        /// Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// <para/>If targetComponent is null then method returns null;  
        /// <para/>If targetComponent has singleton getter delegate then Singleton instance'll be returned.
        /// <para/>Here we use TypeActivator with precompiling instance creation with ctor-determined by arguments. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ctorSearchBinding"></param>
        /// <param name="ignoreSingleton"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public T ResolveInstanceTBase<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding
                                                , bool ignoreSingleton = false, params object[] arguments)
        {
            if ( HasSingletonAccess && ignoreSingleton == false) return (T) SingletonInstance();

            return ComponentType.CreateInstanceTBase<T>(ctorSearchBinding, arguments);
        }



        /// <summary>
        /// Lazy Resolving instance of [ConponentItemInfo.ComponentType]. 
        /// <para/>If targetComponent is null then method returns null;  
        /// <para/>If targetComponent has singleton getter delegate then Singleton instance'll be returned.
        /// <para/>Here we use TypeActivator with precompiling instance creation with ctor-determined by arguments. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ctorSearchBinding"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public T ResolveInstanceTBaseLazy<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] arguments)
        {
            if (HasSingletonAccess) return (T)SingletonInstance();

            return ComponentType.CreateInstanceTBaseLazy<T>(ctorSearchBinding, arguments);
        }


      
        #endregion ------------------------------- RESOLVE INSTANCE ------------------------------




    }


}
