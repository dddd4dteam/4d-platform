﻿#if SERVER  && IIS


using System;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// It's meta interface that help us to declare association of that type with one of the DCCommunication Unit Scenarios on Server Side. 
    /// DCCUnit is standart App Component . 
    /// </summary>
    public interface IDCCUnitServer
    {
        string ScenarioKey { get; }
    }

}



#endif
