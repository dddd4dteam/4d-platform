﻿
using DDDD.Core.HttpRoutes;
using DDDD.Core.Extensions;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Serialization;
using DDDD.Core.ComponentModel;
using System;
using System.IO;




namespace DDDD.Core.ComponentModel
{




    /// <summary>
    /// DCCUnitOrderAttribute - register info by which we create DCCommunication Unit with ordered Scenario -[technology+configuration] for Communication Owner Types. 
    /// <para/> We registerig and instantiating service handler instance  on server side, and client handler instance on client side.
    /// <para/> For ex.: DCManager- has ICommunicationOwner interface and is CommunicationOwner , so it can ask for its standart operations - personal Communicaion Unit.
    /// <para/> Communication Unit scenarios -[ [WCF Service: standart ServiceHost  and REST-ful WebServiceHost]/[WebApiController]/[MvcController]/[SignalRHub]/]. 
    /// <para/> Communication Owner can has several communication Items, but only with different [ScenarioKey] property value-scenario.
    /// <para/> Some Communication scenarios can exclude each other look clearly on each of DCCommunicationEn value.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class DCCUnitOrderAttribute : Attribute  /// is not Component meta info - IComponentItemMetaInfo
    {

        #region ------------------------- CTOR -----------------------

        public DCCUnitOrderAttribute(string communicationUnitKey, HttpRouteTemplateEn uriTemplate, DCSerializationModeEn serializationMode, Type customDCCUnitHandlerType = null)
        {
            ScenarioKey = communicationUnitKey;
            UriTemplate = uriTemplate;
            SerializationMode = serializationMode;
            CustomDCCUnitHandlerType = customDCCUnitHandlerType;
        }

        #endregion ------------------------- CTOR -----------------------


        #region ------------------------------ DC Communicaion Unit KEY- Scenario ----------------------------

        /// <summary>
        ///ScenarioKey -  is  the Key of some scenario -[technology+configuration]  in  DCCUnit.  
        /// Also it's the identifier Key for enabled DCCUnits for Owner Type: Owner Type can have only one  DCCUnit with such ScenarioKey.
        /// </summary>
        public string ScenarioKey// CommunicationUnitKey
        {
           get;
           private set;
        }

        #endregion ------------------------------ DC Communicaion Unit KEY- Scenario ----------------------------        


        #region -------------------------DC Communication Unit  Uri --------------------------

        /// <summary>
        /// This is a positional argument
        /// </summary>
        public HttpRouteTemplateEn UriTemplate
        { get; private set; }

              
      

        #endregion ------------------------------  DC Communication Unit  Uri -----------------------------------------
        

        #region ---------------------------- DC Communication Unit Serialization Mode ---------------------------------

        /// <summary>
        /// Dynamic Command Serialization Mode  that will used in current Communication Unit.
        /// </summary>
        public DCSerializationModeEn SerializationMode
        {
            get; private set;
        }



        #endregion ---------------------------- DC Communication Unit Serialization Mode ---------------------------------


        #region ----------------------- DC Communication Unit custom Service Handler ---------------------------------


        /// <summary>
        /// User can use custom Service/Handler type with custom implementation of base needable methods. 
        /// <para/> For ex: custom class  derived from DCWebApiDirectHandler  -to implement handling of HTTP GET,DELETE , HEAD, OPTIONS, or PUT methods. 
        /// <para/> 
        /// </summary>
        public Type CustomDCCUnitHandlerType
        { get; private set; }

        #endregion ----------------------- DC Communication Unit custom Service Handler ---------------------------------


    }
}
