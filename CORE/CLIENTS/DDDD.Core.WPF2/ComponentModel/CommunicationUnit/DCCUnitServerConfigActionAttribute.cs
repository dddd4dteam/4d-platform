﻿using System;

namespace DDDD.Core.ComponentModel
{


#if SERVER && IIS


    /// <summary>
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class DCCUnitServerConfigActionAttribute : Attribute
    {

        #region---------------------------- CTOR ---------------------------------

        public DCCUnitServerConfigActionAttribute(Type registerActorType, string registringActionName)
        {
            RegisterActorType = registerActorType;
            RegistringActionName = registringActionName;// 
        }

        #endregion ---------------------------- CTOR ---------------------------------


        /// <summary>
        /// Class that contains  registration logic  in .config file action- static method.
        /// </summary>
        public Type RegisterActorType
        { get; private set; }


        /// <summary>
        ///Registering DCCUnit in *.config file- Action-static method Name.
        ///<para/> Signature of this method is -  Func{DCCUnit,bool}  unitToRegister.
        /// </summary>
        public string RegistringActionName
        { get; private set; }

    }

#endif


}
