﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;



using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Threading;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Diagnostics;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Environment;
using DDDD.Core.Resources;
using DDDD.Core.Serialization.Json;
using DDDD.Core.Serialization.Xml;
using DDDD.Core.Serialization.Binary;
using System.Threading;



#if !WP81

using System.Net.Http.Headers;

#endif


#if SERVER && IIS

using System.Web;
using System.Net;
//using ServiceStack.Text.Jsv;
using DDDD.Core.Net;


#endif



namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// DCCUnit - DC Communication Unit's - contains info about owner and target communication unit representer class. 
    /// <para/> It does the following: Contains registering info about Owner of this Unit; 
    /// <para/>                        Can manage by serializationMode. 
    /// </summary>
    public class DCCUnit
    {

        #region ---------------------------------- CTOR ---------------------------------------


        DCCUnit(Type ownerType, DCCUnitOrderAttribute registeringInfo)
        {

            InitializeItem(ownerType, registeringInfo);
        }

        static DCCUnit()
        {
            InitializeStatic();
        }



        #endregion ---------------------------------- CTOR ---------------------------------------



        #region ----------------------------- ID , REGISTRATION INFO----------------------------------


        /// <summary>
        /// Identifier of DCCommnication Unit -of this wrap instance.
        /// </summary>
        public Guid ID
        { get; } = Guid.NewGuid();


        /// <summary>
        /// Registration info that Owner wants to use for it's work- some owner ordered string - [technology-configuration-scenario]-it's info about it:string,OwnerType, DCSerializationModeEn, TargetUri .
        /// </summary>
        public DCCUnitOrderAttribute RegistrationInfo
        { get; set; }




        #endregion ----------------------------- ID , REGISTRATION INFO----------------------------------


        #region ----------------------------------- Owner Type and Owner ---------------------------------

        /// <summary>
        /// Owner Type can be setted manually by SetOwnerAndUriResultAddress(Type ownerType)
        /// </summary>
        public Type OwnerType
        {
            get; private set;
        }




        /// <summary>
        /// Owner ShortKey - ComponentItemInfo.GetShortKey(OwnerType.Name)
        /// </summary>
        public string OwnerShortKey
        {
            get { return ComponentMatch.GetShortKey(OwnerType.Name); }
        }




        string _OwnerName;
        /// <summary>
        /// Owner Name is the Type.Name without standart word part. Like [ClientReport]Manager without word [Manager]. And other standart owner words.
        /// </summary>
        public string OwnerName
        {
            get
            {
                if (_OwnerName.IsNull())
                {
                    _OwnerName = GetOwnerName(OwnerType);
                }
                return _OwnerName;
            }
        }


        static string GetOwnerName(Type ownerType)
        {
            return ownerType.Name.Replace("Manager", "");
        }


        #endregion ----------------------------------- Owner Type and Owner ---------------------------------


        #region ------------------------------- INITIALIZATION ---------------------------------

        private static void InitializeStatic()
        {
            //JsConfig.DateHandler = DateHandler.ISO8601;
            //         //JsConfig.ExcludeTypeInfo = true;
            //         JsConfig.IncludePublicFields = true;
            //         JsConfig.InitStatics();

        }



        #endregion ------------------------------- INITIALIZATION ---------------------------------


        #region --------------------------DCCUNITS CACHE Communication Scenarios ------------------------------------


        /// <summary>
        /// DC Communication unit  instance Registering Func(  in *.config Action,or programmatically add-WebAPI ). This register Func will be used on server-side to register service item. 
        /// </summary>
        static Dictionary<string, Func<DCCUnit, bool>> RegisterFactoryItemConfigByScenarioFunc
        { get; } = new Dictionary<string, Func<DCCUnit, bool>>();

        static Type DCCUnitItemRegisterConfigFuncType = typeof(Func<DCCUnit, bool>);


        /// <summary>
        /// DC Communication units Register - Dictionary. By each of the DCCUnit Scenario
        /// - for each Fabric associated with this Unit Type, we register items 
        ///    - DCCUnits where we want to know [OwnerType(Type),DCCUnit]. 
        /// </summary>
        static Dictionary<string, Dictionary<Type, DCCUnit>> DCCUnits
        { get; } = new Dictionary<string, Dictionary<Type, DCCUnit>>();




#if SERVER && IIS

        /// <summary>
        ///  Add  new scenario if its still not exist;
        ///  On adding new Scenario, if Factory type has MetaInfo with not nullable InitConfig method pointer, then this Configuration method will be invoked.
        /// </summary>
        /// <param name="scenario"></param>
        /// <returns></returns>
        public static bool TryAddOrUseExistScenario(ComponentMatch dCCUnitServerFactoryComponent)
        {

            Validator.ATNullReferenceArgDbg(dCCUnitServerFactoryComponent, nameof(DCCUnit), nameof(TryAddOrUseExistScenario), nameof(dCCUnitServerFactoryComponent));


            var scenarioWasAdded = false;
            if (dCCUnitServerFactoryComponent.IsNull()) return scenarioWasAdded;


            var metainfo = (dCCUnitServerFactoryComponent.MetaInfo as DCCUnitServerFactoryAttribute);
            string scenario = metainfo.ScenarioKey.S();

            if (DCCUnits.ContainsKey(scenario)) return scenarioWasAdded; // we don't add it here

            //Init Factory config and add Scenario Key
            if (metainfo.InitFactoryGlobalConfigMethodName.IsNotNullOrEmpty())
            {
                var methodDelegate = dCCUnitServerFactoryComponent.ComponentType.GetMethod(metainfo.InitFactoryGlobalConfigMethodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

                if (methodDelegate.IsNotNull())
                {
                    methodDelegate.Invoke(null, null);
                }
            }

            //Init Factory item's configuration by DCCUnit 
            if (metainfo.InitFactoryItemConfigMethodName.IsNotNullOrEmpty())
            {
                var initFactoryItemConfigMethodFuncMethodInfo = dCCUnitServerFactoryComponent.ComponentType.GetMethod(metainfo.InitFactoryItemConfigMethodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (initFactoryItemConfigMethodFuncMethodInfo.IsNull())
                { scenarioWasAdded = true; return scenarioWasAdded;
                }
                var initFactoryItemConfigMethodFunc = (Func<DCCUnit, bool>)initFactoryItemConfigMethodFuncMethodInfo.CreateDelegate(DCCUnitItemRegisterConfigFuncType);

                if (initFactoryItemConfigMethodFunc.IsNotNull())
                { RegisterFactoryItemConfigByScenarioFunc.Add(scenario, initFactoryItemConfigMethodFunc);
                }

            }

            // after we inited Senario Factory then registering it's Scenario Slot - [scenario, new Dictionary<Type, DCCUnit>()]

            DCCUnits.Add(scenario, new Dictionary<Type, DCCUnit>());  ////_Scenarios.Add(scenario); 

            scenarioWasAdded = true;
            return scenarioWasAdded;
        }

#elif CLIENT

        /// <summary>
        ///  Add  new scenario if its still not exist
        /// </summary>
        /// <param name="scenario"></param>
        /// <returns></returns>
        public static bool TryAddOrUseExistScenario(ComponentMatch  dCCUnitClientFactoryComponent)
        {
           if (dCCUnitClientFactoryComponent.IsNull()) return false;

            // add scenario with   IDCCommunicationUnitServerFactory implemented Type
            var metainfo = (dCCUnitClientFactoryComponent.MetaInfo as DCCUnitClientFactoryAttribute);
            
            //string[] scenarios = metainfo.TargetDCCommunicationUnits.ToArray();// S();
            //if (_Scenarios.Contains(scenarios[0])) return false;
            
            //Init Factory config and add Scenario Key


            return true;
        }

#endif


        #endregion |--------------------------DCCUNITS CACHE Communication Scenarios -----------------------------


        #region ------------------------------- BUILD DC COMMUNICATION UNIT URI ----------------------------------------

        /// <summary>
        /// Get associated with each CommunicationUnit  uri prefix.
        /// </summary>
        /// <param name="unitKey"></param>
        /// <returns></returns>
        public static string GetCommunicationPrefix(string scenarioKey)
        {
            var scenarioKeylower = scenarioKey.ToLower();
            if (scenarioKeylower.Contains(UriPrefixEn.wcf.S()))
            {
                return UriPrefixEn.wcf.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.webapi.S()) || scenarioKeylower.Contains("webapi"))
            {
                return UriPrefixEn.webapi.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.mvc.S()))
            {
                return UriPrefixEn.mvc.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.sigr.S()) || scenarioKeylower.Contains("signalr"))
            {
                return UriPrefixEn.sigr.S();
            }

            if (scenarioKeylower.Contains(UriPrefixEn.thrift.S()))
            {
                return UriPrefixEn.thrift.S();
            }

            return UriPrefixEn.usvc.S();

        }


        /// <summary>
        ///  Build  service-communicationUnit's  uri  .
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <param name="ownerName"></param>
        /// <returns></returns>
        public static string BuildDCCUnitUriStartAddress(DCCUnit communicationUnit)
        {
            return OperationInvoke.CallInMode(nameof(DCCUnit), nameof(BuildDCCUnitUriStartAddress)
                 , () =>
                  {
                     return HttpRoute2.BuildServiceUri(communicationUnit.RegistrationInfo.UriTemplate
                            , RouteKeysEn.Service.KVPair(communicationUnit.OwnerName)
                            , RouteKeysEn.Controller.KVPair(communicationUnit.OwnerName)
                            , RouteKeysEn.Communication.KVPair(GetCommunicationPrefix(communicationUnit.RegistrationInfo.ScenarioKey))
                            , RouteKeysEn.WebApplication.KVPair(Environment2.DefaultUrl.WebApplication)
                             // ServiceName: communicationUnit.OwnerName,
                             // CommunicationPrefix: GetCommunicationPrefix(communicationUnit.RegistrationInfo.ScenarioKey)                           
                             );
                 });
        }


        #endregion ------------------------------- BUILD DC UNIT URI ----------------------------------------


        #region -------------------------- Uri result Address-----------------------------------

        /// <summary>
        /// Result Uri Address that we can build based on UriTemplate and current HttpRouteClient.StartUrl values.
        /// <para/> on the server side this uri value will be always relative.
        /// <para/> You can change/set HttpRouteClient.DefaultStartServer and  then call UriResultAddress() to get updated value of UriResultAddress.
        /// </summary>
        public string UriResultAddress
        { get; private set; }


        void InitializeItem(Type ownerType, DCCUnitOrderAttribute registeringInfo)
        {
            OwnerType = ownerType;
            RegistrationInfo = registeringInfo;

#if !WP81
            if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_D4JSON
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_D4XML               
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.D4JSON
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.D4XML

               )
            {
                UseTextSerialization = true;
            }
            else if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.TSS
                )
            {
                UseBinSerialization = true;
            }
#else
           

            if ( RegistrationInfo.SerializationMode == DCSerializationModeEn.NSJSON )
            {
                UseTextSerialization = true;
            }
            else if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.TSS
                )
            {
                UseBinSerialization = true;
            }
#endif


            if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
#if !WP81
                || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_D4JSON
                || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_D4XML                
#endif
              )
            {
                UsePackUnpackLogic = true;
            }
                       

            //var hostEntityName = "";
            var targetUriAddress = BuildDCCUnitUriStartAddress(this);
            CorrectUriAddressByScenariosOnDifferentSides(ref targetUriAddress);

            if (targetUriAddress.IsNull())
                throw new InvalidOperationException($"Couldn't find any Registration for [{OwnerType.Name}] with DCC Scenario [{RegistrationInfo.ScenarioKey}] ");

            UriResultAddress = targetUriAddress;
            
            //Init Empty Dyagnostics Logger
            Logger = DCLogger.CreateEmptyLogger(OwnerShortKey);

        }

        void CorrectUriAddressByScenariosOnDifferentSides(ref string targetUriAddress)
        {
#if SERVER
            if (RegistrationInfo.ScenarioKey.ToLower().Contains("webapi") && targetUriAddress.StartsWith("~/"))
            {
                targetUriAddress = targetUriAddress.Replace("~/", "");
            }
#endif
        }

        #endregion -------------------------- Uri result Address-----------------------------------


        #region ---------------------- SERIALIZATION --------------------------

      
        /// <summary>
        /// Here we declare that we use one of Text Serializations.
        /// </summary>
        public bool UseTextSerialization { get; private set; }

        /// <summary>
        /// Here we declare that we use Binary Serialization.
        /// </summary>
        public bool UseBinSerialization { get; private set; }


        #region---------------------------------GET/... SERIALIZE/DESERIALIZE TO/FROM STREAM ------------------------------

        /// <summary>
        ///  Serialize To Stream [TSS, XML , Json ]  by  RegistrationInfo.SerializationMode flag value
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="stream"></param>
        protected void SerializeToStream(object instance, Stream stream)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    {
                        BinarySerializer.SerializeToStream(instance, stream);
                        break;
                    }
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        BinarySerializer.SerializeToStream(instance, stream);
                        break;
                    }
                    
                case DCSerializationModeEn.TSS:
                    {
                        BinarySerializer.SerializeToStream(instance, stream);
                        break;
                    }                
                case DCSerializationModeEn.MDCS_D4JSON:
                    {
                        JsonSerializer.SerializeToStream(instance, stream);
                        break;
                    }
                case DCSerializationModeEn.D4JSON:
                    {
                        JsonSerializer.SerializeToStream(instance, stream);
                        break;
                    }
                case DCSerializationModeEn.MDCS_D4XML:
                    {
                        XmlSerializer.SerializeToStream(instance, stream); ;
                        break;
                    }
                case DCSerializationModeEn.D4XML:
                    {
                        XmlSerializer.SerializeToStream(instance, stream); ;
                        break;
                    }                                   
                default:
                    {
                        BinarySerializer.SerializeToStream(instance, stream);
                        break;
                    }
            }
        }


        /// <summary>
        ///  Deserialize From Stream [TSS, XML , Json ]  by  RegistrationInfo.SerializationMode flag value
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="stream"></param>
        protected object DeserializeFromStream( Type type, Stream stream)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    {
                       return BinarySerializer.Deserialize(type, stream );                        
                    }
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return BinarySerializer.Deserialize(type, stream );                        
                    }
                case DCSerializationModeEn.TSS:
                    {
                        return BinarySerializer.Deserialize(type, stream );                        
                    }
                case DCSerializationModeEn.MDCS_D4JSON:
                    {
                        return JsonSerializer.DeserializeFromStream( type, stream);                         
                    }
                case DCSerializationModeEn.D4JSON:
                    {
                        return JsonSerializer. DeserializeFromStream( type, stream);                        
                    }
                case DCSerializationModeEn.MDCS_D4XML:
                    {
                        return XmlSerializer.DeserializeFromStream( type, stream);                        
                    }
                case DCSerializationModeEn.D4XML:
                    {
                        return XmlSerializer.DeserializeFromStream( type, stream);                        
                    }
                default:
                    {
                        return BinarySerializer.Deserialize(type, stream );                        
                    }
            }
        }




        /// <summary>
        /// Serialize instance into Stream with current  SerializeToStreamHandler.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public  Task SerializeToStreamTPL(object instance, Stream inputStream)
        {            
            return  Task.Factory.StartNew(() =>
            {
                SerializeToStream(instance, inputStream);
            }
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default            
            );
        }


        /// <summary>
        /// Deserialize from inputStream as outType. 
        /// </summary>
        /// <param name="outType"></param>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public  Task<object> DeserializeFromStreamTPL(Type outType, Stream inputStream)
        {
            return Task.Factory.StartNew<object>((strm) =>
            {               
               return DeserializeFromStream(outType, strm as Stream);
            }
            , inputStream
            , CancellationToken.None
            , TaskCreationOptions.AttachedToParent
            , TaskScheduler.Default
            );         
            
        }



        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STREAM ------------------------------



        #region---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STRING------------------------------

        /// <summary>
        /// Handler of SerializeToString.[ XML,JSON ]
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        protected string SerializeToString(object instance)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    {
                        return null;
                    }                   
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return null;
                    }
                case DCSerializationModeEn.TSS:
                    {
                        return null;
                    }
                case DCSerializationModeEn.MDCS_D4JSON:
                    {
                        return JsonSerializer.SerializeToString(instance);
                    }
                case DCSerializationModeEn.MDCS_D4XML:
                    {
                        return   XmlSerializer.SerializeToString(instance);
                    }
                case DCSerializationModeEn.D4XML:
                    {
                         return   XmlSerializer.SerializeToString(instance);                        
                    }
                case DCSerializationModeEn.D4JSON:
                    {
                        return   JsonSerializer.SerializeToString(instance);
                    }
                default:
                    {
                        return JsonSerializer.SerializeToString(instance);
                    }
            }
        }


        /// <summary>
        /// Handler of DeserializeFromString [ XML,JSON ]
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        protected object DeserializeFromString(Type targetType,string text)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    {
                        return null;
                    }
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return null;
                    }
                case DCSerializationModeEn.TSS:
                    {
                        return null;
                    }
                case DCSerializationModeEn.MDCS_D4JSON:
                    {
                        return JsonSerializer.DeserializeFromString(targetType,text);
                    }
                case DCSerializationModeEn.MDCS_D4XML:
                    {
                        return XmlSerializer.DeserializeFromString(targetType, text);
                    }
                case DCSerializationModeEn.D4XML:
                    {
                        return XmlSerializer.DeserializeFromString(targetType, text);
                    }
                case DCSerializationModeEn.D4JSON:
                    {
                        return JsonSerializer.DeserializeFromString(targetType, text);
                    }
                default:
                    {
                        return JsonSerializer.DeserializeFromString(targetType, text);
                    }
            }

        }
           

        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STRING ------------------------------



        #region---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM BYTE ARRAY ------------------------------

        /// <summary>
        /// Handler of SerializeToByteArray.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <returns></returns>
        public byte[] SerializeToByteArray(object instance)
        {                       
            using (var dataStream = new MemoryStream())
            {
                SerializeToStream(instance, dataStream);

                return dataStream.ToArray();
            }          
        }

        /// <summary>
        /// Handler of  DeserializeFromByteArray.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <returns></returns>
        public object DeserializeFromByteArray(byte[] serializedData, Type type)
        {
            using (var dataStream = new MemoryStream(serializedData))
            {
                return DeserializeFromStream(type, dataStream);
            }             
        }



        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM BYTE ARRAY ------------------------------


        #region ---------------------- HttpRequestMessage/ HttpResponse SERIALIZE/DESERIALIZE TO/FROM STREAM  --------------------------------


        const string application_json = @"application/json";
        const string application_octetStream = @"application/octet-stream";

        const string text_xml = @"text/xml";
        const string text_csv = @"text/csv";
        const string text_plain = @"text/plain";




#if SERVER && IIS

        static protected readonly MediaTypeHeaderValue ContentType_Json = new MediaTypeHeaderValue(application_json);
        static protected readonly MediaTypeHeaderValue ContentType_OctetStream = new MediaTypeHeaderValue(application_octetStream);
        static protected readonly MediaTypeHeaderValue ContentType_TextXml = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextCsv = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextPlain = new MediaTypeHeaderValue(text_xml);


        static protected readonly MediaTypeWithQualityHeaderValue JsonAccept = new MediaTypeWithQualityHeaderValue(application_json);
        static protected readonly MediaTypeWithQualityHeaderValue OctetAccept = new MediaTypeWithQualityHeaderValue(application_octetStream);


        /// <summary>
        /// Get ContentType Http Header value based on RegistrationInfo.SerializationMode.
        /// </summary>
        /// <returns></returns>
        public MediaTypeHeaderValue GetContentTypeHeaderValue()
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.MDCS_TSS:
                    { return ContentType_OctetStream; }
                case DCSerializationModeEn.MDCS_SSJSON:
                    { return ContentType_Json; }
                case DCSerializationModeEn.MDCS_SSXML:
                    { return ContentType_TextXml; }
                case DCSerializationModeEn.MDCS_SSCSV:
                    { return ContentType_TextCsv; }
                case DCSerializationModeEn.SSCSV:
                    { return ContentType_TextCsv; }
                case DCSerializationModeEn.SSJSV:
                    { return ContentType_TextPlain; }
                case DCSerializationModeEn.TSS:
                    { return ContentType_OctetStream; }
                case DCSerializationModeEn.SSXML:
                    { return ContentType_TextXml; }
                case DCSerializationModeEn.SSJSON:
                    { return ContentType_Json; }
                case DCSerializationModeEn.NSJSON:
                    { return ContentType_Json; }
                default:
                    { return ContentType_Json; }
            }
        }


        /// <summary>
        /// SERVER SIDE - DeserializeFromRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<object> DeserializeFromRequest(HttpRequestMessage request, Type dataType)
        {
            return await OperationInvoke.CallInMode(nameof(DCCUnit), nameof(DeserializeFromRequest),
               async () =>
               {
                   var contentStream = await request.Content.ReadAsStreamAsync();// as StreamContent;
                   return DeserializeFromStreamHandler(dataType, contentStream);
               });
        }


        /// <summary>
        /// SERVER SIDE -  SerializeToResponse
        /// </summary>
        /// <param name="response"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SerializeToResponse(HttpResponseMessage response, object message)
        {
           return  OperationInvoke.CallInMode(nameof(DCCUnit), nameof(SerializeToResponse)
                ,  () =>
                {
                    var stream = new MemoryStream();
                    SerializeToStreamHandler(message, stream);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentType = GetContentTypeHeaderValue();
                    return response;
                }
                );
        }


#elif CLIENT



        //Pack DCMessage    - To   SendToServer
        //Unpack DCMessage  - From ReceiveFromServer

        /// <summary>
        /// CLIENT SIDE - SerializeToRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<HttpRequestMessage> SerializeToRequest(HttpRequestMessage request, object message)
        {
             return  OperationInvoke.CallInMode(nameof(DCCUnit), nameof(SerializeToRequest)
                    , () =>
                     {
                       var stream = new MemoryStream();
                       SerializeToStream(message, stream);
                       var streamContent = new StreamContent(stream);
                       
                       request.Content = streamContent;
                       return request;
                     });
             
        }


        /// <summary>
        /// CLIENT SIDE - DeserializeFromResponse
        /// </summary>
        /// <param name="response"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<object> DeserializeFromResponse(HttpResponseMessage response, Type dataType)
        {

            return await OperationInvoke.CallInMode( nameof(DCCUnit), nameof(DeserializeFromResponse),
                async () =>
                {
                    var contentStream = await response.Content.ReadAsStreamAsync();// as StreamContent;
                    return DeserializeFromStream(dataType, contentStream);
                });
            

        }

#endif


        #endregion ---------------------- HttpRequestMessage/ HttpResponse SERIALIZE/DESERIALIZE TO/FROM STREAM  --------------------------------


        #region ----------------------------WCF Known DCMessage ...  DataContracts ------------------------------

        static List<Type> KnownDataContracts
        { get; } = new List<Type>();

        /// <summary>
        /// We can Add new known Data Contract. All Known DataContracts we can get by   GetKnownDataContracts(). It also used in WCF service contracts - IDCService.
        /// </summary>
        /// <param name="knownContract"></param>
        public static void AddKnowDataContracts(params Type[] knownContracts)
        {
            foreach (var kContract in knownContracts)
            {
                if (KnownDataContracts.NotContains(kContract))
                {
                    KnownDataContracts.Add(kContract);
                }
            }
        }

        /// <summary>
        /// Globally known Data Contracts. It also used in WCF service contracts - IDCService.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetKnownDataContracts(ICustomAttributeProvider provider)
        {
            return KnownDataContracts;
        }

        #endregion ----------------------------WCF Known DCMessage ...  DataContracts ------------------------------


        #endregion ---------------------------- SERIALIZATION ---------------------------------


        #region --------------------------- CREATE/PACK/UNPACK  DCMESSAGE By DCUNIT SCENARIO ------------------------------


        /// <summary>
        /// If we are Scenario use Messages where we need to use Pack/Unpack logic.(it's WCF use scenarios- when we need it).
        /// </summary>
        public bool UsePackUnpackLogic
        { get; private set; }



        /// <summary>
        /// Create Empty Message. Decide what end DCmessage type we will use: DCMessage, DCMessage2, DCmessage3.
        /// </summary>
        /// <returns></returns>
        public IDCMessage CreateEmptyMessage()
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.TSS:
                    { return DCMessage2.Empty; }
              
                case DCSerializationModeEn.NotDefined:
                    { return DCMessage.Empty; }
                case DCSerializationModeEn.MDCS_TSS:
                    { return DCMessage.Empty; }
             

                case DCSerializationModeEn.MDCS_D4JSON:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.MDCS_D4XML:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.D4XML:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.D4JSON:
                    { return DCMessage2.Empty; }
                
                default:
                    { return DCMessage.Empty; }


            }


        }

        /// <summary>
        /// Create IDCMessage based on the SerializationMode of current DCUnit. 
        /// </summary>
        /// <param name="shortKey"></param>
        /// <param name="currentCommandName"></param>
        /// <param name="actionParameters"></param>
        /// <returns></returns>
        public IDCMessage CreateMessage(string shortKey, string currentCommandName, params DCParameter[] actionParameters)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    { return DCMessage.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.TSS:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.MDCS_TSS:
                    { return DCMessage.Create(shortKey, currentCommandName, actionParameters); }
              
                case DCSerializationModeEn.MDCS_D4JSON:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.MDCS_D4XML:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                  case DCSerializationModeEn.D4XML:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.D4JSON:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
              
                default:
                    { return DCMessage.Empty; }
            }
        }



        /// <summary>
        /// Unpack Message,but if it's needable. When we use WCF scenario(Not Rest)/MDxx_xxx serialization Modes we need in this operation.
        /// </summary>
        /// <param name="inputCommand"></param>
        public IDCMessage UnpackMessageIfNeeds(IDCMessage inputCommand)
        {           
            if (UsePackUnpackLogic && inputCommand is DCMessage)
            {
                DCMessage messageEnd = inputCommand as DCMessage;
                messageEnd = (DCMessage)DeserializeFromByteArray(messageEnd.Body, typeof(DCMessage));

                inputCommand = messageEnd;
            }
            return inputCommand;           
        }


        /// <summary>
        /// Pack Message,but if it's needable. When we use WCF scenario(Not Rest)/MDxx_xxx serialization Modes we need in this operation.
        /// </summary>
        /// <param name="inputCommand"></param>
        public IDCMessage PackMessageIfNeeds(IDCMessage inputCommand)
        {         
            if (UsePackUnpackLogic && inputCommand is DCMessage)
            {
                DCMessage messageEnd = inputCommand as DCMessage;
                messageEnd.Body = SerializeToByteArray(messageEnd);
                inputCommand = messageEnd;
            }
            return inputCommand;
        }


        public IDCMessage PackParametersIfNeeds(IDCMessage inputCommand)
        {
            var paramsToUnpack = inputCommand.GetAllParams().Where(prm => prm.UsePackUnpackLogic);

            foreach (var unpackParam in paramsToUnpack)
            {
                SerializeParameter(unpackParam);//.Serialize(this);
            }

            return inputCommand;
        }

        public IDCMessage UnpackParametersIfNeeds(IDCMessage inputCommand)
        {
            var paramsToUnpack = inputCommand.GetAllParams().Where(prm => prm.UsePackUnpackLogic);

            foreach (var unpackParam in paramsToUnpack)
            {
                DeserializeParameter(unpackParam);//  .Deserialize(this);                         
            }

            return inputCommand;

        }




        #endregion --------------------------- CREATE/PACK/UNPACK  DCMESSAGE By DCUNIT SCENARIO ------------------------------



        #region ---------------------------- SERIALIZE / PACK / UNPACK DCPARAMETERS --------------------------------- 

        /// <summary>
        /// Get CSharpTypeName string builder.
        /// </summary>
        /// <param name="targetValueType"></param>
        /// <returns></returns>
        public static string GetCSharpTypeName(Type targetValueType)
        {
            var targetTypeName = targetValueType.FullName;

            var targetAssemblyName = targetValueType.Assembly.GetAssemblyNameEx();

            return targetTypeName + "," + targetAssemblyName;
        }

#if CLIENT



         /// <summary>
        /// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communication unit with prepered serialization handler, based on it's SerializationMode</param>
        public void SerializeParameter(DCParameter parameter)
        {
            if (parameter.Value.IsNull()) return;

            var valueType = parameter.Value.GetType();
            parameter.CSharpTypeFullName = GetCSharpTypeName(valueType); //for Type Script Typed data communication.


            if (UseBinSerialization)
            {
                try
                {
                    parameter.ValueBody = SerializeToByteArray(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            else if ( UseTextSerialization)
            {
                try
                {
                    parameter.ValueText = SerializeToString(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true; 
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            parameter.Value = null; //nullify original data if serialization was successfull or falted
        }





        /// <summary>
        /// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
        public void DeserializeParameter(DCParameter  parameter)
        {
            var targetType = Type.GetType(parameter.CSharpTypeFullName);
            if (targetType.IsNull()) throw new InvalidOperationException("Target Type was not found by Name string [{0}]".Fmt(parameter.CSharpTypeFullName));

            if (UseBinSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueBody.IsNull() ||

                //do not deserialize if it's byte[]
                if (targetType == typeof(byte[])) { parameter.Value = parameter.ValueBody; return; }

                try
                {
                    parameter.Value =  DeserializeFromByteArray(parameter.ValueBody, targetType);
                    parameter.ValueBody = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;

                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter ),
                                    "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueBody -[{2}] | ExceptionMessage-[{3}]"
                                                .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueBody.S(), desExc.Message))
                               );
                }

            }
            else if ( UseTextSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueText.IsNull() ||

                //do not deserialize if it's string
                if (targetType == typeof(string)) { parameter.Value = parameter.ValueText; return; }


                try
                {
                    parameter.Value =  DeserializeFromString( targetType, parameter.ValueText);
                    parameter.ValueText = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;
                    Logger.ErrorDbg(
                      RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter),
                                  "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueText -[{2}] | ExceptionMessage-[{3}]"
                                              .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueText, desExc.Message))
                             );
                }

            }


        }




#elif SERVER

        /// <summary>
        /// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communication unit with prepered serialization handler, based on it's SerializationMode</param>

        public void SerializeParameter(DCParameter parameter)
        {
            if (parameter.Value.IsNull()) return;

            var valueType = parameter.Value.GetType();
            parameter.CSharpTypeFullName = GetCSharpTypeName(valueType); //for Type Script Typed data communication.


            if (UseBinSerialization)
            {
                if (valueType == typeof(byte[])) { parameter.ValueBody = (byte[])parameter.Value; return; }
                try
                {    
                    parameter.ValueBody = SerializeToByteArrayHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            else if ( UseTextSerialization)
            {
                if (valueType == typeof(string)) { parameter.ValueText = (string)parameter.Value; return; }

                try
                {
                     parameter.ValueText = SerializeToStringHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            parameter.Value = null; //nullify original data if serialization was successfull or falted
        }





        /// <summary>
        /// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
        public void DeserializeParameter(DCParameter  parameter)
        {
            var targetType = Type.GetType(parameter.CSharpTypeFullName);
            if (targetType.IsNull()) throw new InvalidOperationException("Target Type was not found by Name string [{0}]".Fmt(parameter.CSharpTypeFullName));

            if (UseBinSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueBody.IsNull() ||

                //do not deserialize if it's byte[]
                if (targetType == typeof(byte[])) { parameter.Value = parameter.ValueBody; return; }

                try
                {
                    parameter.Value =  DeserializeFromByteArrayHandler(parameter.ValueBody, targetType);
                    parameter.ValueBody = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;

                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter ),
                                    "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueBody -[{2}] | ExceptionMessage-[{3}]"
                                                .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueBody.S(), desExc.Message))
                               );
                }

            }
            else if ( UseTextSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueText.IsNull() ||

                //do not deserialize if it's string
                if (targetType == typeof(string)) { parameter.Value = parameter.ValueText; return; }


                try
                {
                    parameter.Value =  DeserializeFromStringHandler(parameter.ValueText, targetType);
                    parameter.ValueText = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;
                    Logger.ErrorDbg(
                      RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter),
                                  "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueText -[{2}] | ExceptionMessage-[{3}]"
                                              .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueText, desExc.Message))
                             );
                }

            }


        }



#endif



        #endregion ---------------------------- SERIALIZE / PACK / UNPACK DCPARAMETERS --------------------------------- 



        #region -------------------------- ADD NEW GET DCCUNIT, GET ADDRESS  ------------------------------


#if CLIENT

        /// <summary>
        /// Create  DCCUnit if instance doesn't exist in internal cache dictionary. Add Register DC Communication Unit - [in CommunicationUnitType dictionary- register  Communication Unit, like-[for some OwnerType,and Unit UriResultAddress] ].
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="registeringInfo"></param>
        /// <returns></returns>
        public static DCCUnit TryGetOrAddNewDCCUnit(Type owner, DCCUnitOrderAttribute registeringInfo)
        {
            try
            {
                //add if not exist - scenario slot
                if (DCCUnits.NotContainsKey(registeringInfo.ScenarioKey))
                    DCCUnits.Add(registeringInfo.ScenarioKey, new Dictionary<Type, DCCUnit>() );

                // check if we already have item in dcCUnit.CommunicationUnitKey
                if (DCCUnits[registeringInfo.ScenarioKey].ContainsKey(owner))
                    return DCCUnits[registeringInfo.ScenarioKey][owner]; // already exist

                // Create DCCUnit   -set owner and uriResultAddress
                var dccUnit = new DCCUnit(owner, registeringInfo);

                DCCUnits[registeringInfo.ScenarioKey].Add(owner, dccUnit);

                return DCCUnits[registeringInfo.ScenarioKey][owner];
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }




#elif SERVER && IIS


        /// <summary>
        /// Create  DCCUnit if instance doesn't exist in internal cache dictionary. Add Register DC Communication Unit - [in CommunicationUnitType dictionary- register  Communication Unit, like-[for some OwnerType,and Unit UriResultAddress] ].
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="registeringInfo"></param>
        /// <returns></returns>
        public static DCCUnit AddOrUseExistDCCUnit(Type owner, DCCUnitOrderAttribute registeringInfo)
        {
            try
            {   // check if we already have item in dcCUnit.CommunicationUnitKey
                if (DCCUnits[registeringInfo.ScenarioKey].ContainsKey(owner))
                    return DCCUnits[registeringInfo.ScenarioKey][owner]; // already exist

                // Create DCCUnit   -set owner and uriResultAddress
                var dccUnit = new DCCUnit(owner, registeringInfo);

                var configWasChanged = RegisterFactoryItemConfigByScenarioFunc[registeringInfo.ScenarioKey](dccUnit); //
                                                                                                                      //change webConfig if need                


                DCCUnits[registeringInfo.ScenarioKey].Add(owner, dccUnit);

                return DCCUnits[registeringInfo.ScenarioKey][owner];
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



#endif



        /// <summary>
        /// Get Registered DC Communication Unit, by some string unit type, and owner Type
        /// </summary>
        /// <param name="ownerToCheck"></param>
        /// <param name="dccUnitScenarioKey"></param>
        /// <returns></returns>
        public static DCCUnit GetDCCUnit(Type ownerToCheck, string dccUnitScenarioKey)
        {
            if (DCCUnits[dccUnitScenarioKey].NotContainsKey(ownerToCheck)) return null;
            return DCCUnits[dccUnitScenarioKey][ownerToCheck];
        }


        /// <summary>
        /// Get Registered DC Communication Unit, by some string unit type, and owner Type.
        /// </summary>
        /// <param name="dccUnitScenarioKey"></param>
        /// <param name="ownerType"></param>
        /// <returns></returns>
        public static string GetRegisteredOwnerUnitAddress(Type ownerType, string dccUnitScenarioKey)
        {
            if (DCCUnits.NotContainsKey(dccUnitScenarioKey)) return null;
            if (DCCUnits[dccUnitScenarioKey].NotContainsKey(ownerType)) return null;

            return DCCUnits[dccUnitScenarioKey][ownerType].UriResultAddress;
        }


        #endregion -------------------------- ADD NEW GET DCCUNIT, GET ADDRESS  ------------------------------



        #region ------------------------------- DIAGNOSTICS/LOGGING ------------------------------------

        /// <summary>
        /// Logger for Service Diagnostics.
        /// </summary>
        public DCLogger Logger
        { get; private set; }

        

        /// <summary>
        /// Authentication Encoding Fnc from DCManager - to encode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecureEncodeDataFunc { get; set; }
        /// <summary>
        /// Authentication Decoding Fnc from DCManager - to decode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecureDecodeDataFunc { get; set; }
       






        #endregion ------------------------------- LOGGING ------------------------------------







    }
}


 