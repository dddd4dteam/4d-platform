﻿using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Net.ServiceModel;
using System;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{



    /// <summary>
    /// It's meta interface that help us to declare association of that type with one of the DCCommunication Unit Scenarios on Client Side.     
    /// </summary>
    public interface IDCCUnitClient
    {
        /// <summary>
        /// Communication Unit Registration info. By This communication registration info target client instance will be created.
        /// </summary>
        DCCUnit CommunicationUnit { get; }

       /// <summary>
       /// Client Representer can be used for several server scenario calls. For Example HttpClient can call WCF, or webApi
       /// </summary>
       string[] ScenarioKeys { get; }

       /// <summary>
       /// Execute command
       /// </summary>
       /// <param name="commandMessage"></param>
       /// <param name="reporter"></param>
       /// <param name="customNetworkIsNotEnableHandler"></param>
       /// <returns></returns>
       Task<IDCMessage> ExecuteCommand(IDCMessage commandMessage, DCProgressReporter reporter, Action<IDCMessage, DCProgressReporter> customNetworkIsNotEnableHandler);

    }

}
