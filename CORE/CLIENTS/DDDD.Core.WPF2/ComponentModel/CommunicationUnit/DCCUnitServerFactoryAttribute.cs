﻿
#if SERVER
		 

using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{

	/// <summary>
	/// Marking class as Factory that can produce one of the type of  DCCUnits
	/// </summary>       
	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public sealed class DCCUnitServerFactoryAttribute : Attribute, IComponentMatchMetaInfo
	{

#region ----------------------------- CTOR ---------------------------------

		/// <summary>
		/// DCCommunicationUnitFactory MetaInfo attribute: string - what DC Communication Unit this factory can produce.
		/// <para/> initGlobalConfigMethodName - this factory can use static method that initilalize some configuration for it's further work. 
		/// Method need to have Func{boll} signature - here it returns true if web.config(or other config) was changed.
		/// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
		/// </summary>
		/// <param name="targetDCCommunicationUnit">string - what DC Communication Unit this factory can produce </param>
		/// <param name="initGlobalConfigMethodName">
		/// initGlobalConfigMethodName - this factory can use static method that initilalize some configuration for it's further work. 
		/// Method need to have Func{boll} signature - here it returns true if web.config(or other config) was changed.
		/// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
		/// </param>
		public DCCUnitServerFactoryAttribute(string targetDCCommunicationUnit, string initFactoryGlobalConfigMethodName = null, string initFactoryItemConfigMethodName = null)            
		{
			ScenarioKey = targetDCCommunicationUnit;
			InitFactoryGlobalConfigMethodName = initFactoryGlobalConfigMethodName;
		    InitFactoryItemConfigMethodName = initFactoryItemConfigMethodName;

		}

#endregion ----------------------------- CTOR ---------------------------------



#region ---------------------- IComponentItemMetaInfo --------------------------
	   
		/// <summary>
		/// Component Class Key 
		/// </summary>
		public string ComponentDefinitionKey
		{
			get
			{
				return ComponentClassEn.DCCUnitServerFactories.S();// CmpCls_DCCommunicationUnitFactories;
			}
		}

		#endregion ---------------------- IComponentItemMetaInfo --------------------------



		/// <summary>
		///   DCCUnit scenario -[technology+configuration] that associates with this factory production.
		/// </summary>
		public string ScenarioKey// string
		{ get; private set; }


		/// <summary>
		/// Method with -  Func{boll} signature. Here method returns true if web.config(or other config) was changed.
		/// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
		/// </summary>
		public string InitFactoryGlobalConfigMethodName
		{ get; private set; }


		/// <summary>
		/// Method with signature - typeof(Func{DCCUnit, bool}) - Function that we need to use to register/configure one DCCUnit item.
		/// </summary>
		public string InitFactoryItemConfigMethodName
		{ get; private set; }
		
	}    

}


#endif

