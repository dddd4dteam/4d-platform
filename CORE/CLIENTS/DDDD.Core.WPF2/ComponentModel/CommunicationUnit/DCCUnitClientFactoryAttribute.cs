﻿#if CLIENT


using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Marking class as Factory that can produce one of the type of DCCommunication Units
    /// </summary>       
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public sealed class DCCUnitClientFactoryAttribute : Attribute, IComponentMatchMetaInfo
    {

        #region ----------------------------- CTOR ---------------------------------

        /// <summary>
        /// DCCommunicationUnitFactory MetaInfo attribute: string - what DC Communication Unit this factory can produce.
        /// <para/> initGlobalConfigMethodName - this factory can use static method that initilalize some configuration for it's further work. 
        /// Method need to have Func{boll} signature - here it returns true if web.config(or other config) was changed.
        /// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
        /// </summary>
        /// <param name="scenarioKeys">string - what DC Communication Unit this factory can produce </param>
        /// <param name="initGlobalConfigMethodName">
        /// initGlobalConfigMethodName - this factory can use static method that initilalize some configuration for it's further work. 
        /// Method need to have Func{boll} signature - here it returns true if web.config(or other config) was changed.
        /// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
        /// </param> 
        public DCCUnitClientFactoryAttribute(string initGlobalConfigMethodName , string scenarioKey1)
        {
            ScenarioKeys = new[] { scenarioKey1 };
            InitGlobalConfigMethodName = initGlobalConfigMethodName;
        }

        public DCCUnitClientFactoryAttribute(string initGlobalConfigMethodName , string scenarioKey1 ,string scenarioKey2)
        {
            ScenarioKeys = new[] { scenarioKey1 ,scenarioKey2 };
            InitGlobalConfigMethodName = initGlobalConfigMethodName;
        }

        public DCCUnitClientFactoryAttribute(string initGlobalConfigMethodName, string scenarioKey1, string scenarioKey2, string scenarioKey3)
        {
            ScenarioKeys = new[] { scenarioKey1, scenarioKey2, scenarioKey3 };
            InitGlobalConfigMethodName = initGlobalConfigMethodName;
        }

        public DCCUnitClientFactoryAttribute(string initGlobalConfigMethodName, string scenarioKey1, string scenarioKey2, string scenarioKey3, string scenarioKey4)
        {
            ScenarioKeys = new[] { scenarioKey1, scenarioKey2, scenarioKey3, scenarioKey4 };
            InitGlobalConfigMethodName = initGlobalConfigMethodName;
        }



        #endregion ----------------------------- CTOR ---------------------------------



        #region ---------------------- IComponentItemMetaInfo --------------------------

        /// <summary>
        /// Component Class Key 
        /// </summary>
        public string ComponentDefinitionKey
        {
            get
            {
                return ComponentClassEn.DCCUnitClientFactories.S();
            }
        }

        #endregion ---------------------- IComponentItemMetaInfo --------------------------



        /// <summary>
        /// Scenarios Keys - DCCUnit scenarios[technology+configuration] that associated with this factory production-client item.
        /// It means that one DCCUnitClient- its associated client, can be used in several scenarios.
        /// </summary>
        public string[] ScenarioKeys
        { get; private set; }


        /// <summary>
        /// Method with -  Func{boll} signature. Here method returns true if web.config(or other config) was changed.
        /// Method will be called in ComponentsContainer.Build() after all ComponentsClasses elements will be collected.        
        /// </summary>
        public string InitGlobalConfigMethodName
        { get; private set; }



    }
     

}


#endif
