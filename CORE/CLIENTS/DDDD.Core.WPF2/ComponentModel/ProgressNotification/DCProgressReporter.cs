﻿using DDDD.Core.Notification;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Dynamic Command Progress Reporter - output messages of DC changed state into the client UI.
    /// <para/> Also to register Notify Actions by DCProgressEn enum. 
    /// </summary>
    public class DCProgressReporter : ProgressReporter<OperationProgressActionsEn>
    {
    }
}
