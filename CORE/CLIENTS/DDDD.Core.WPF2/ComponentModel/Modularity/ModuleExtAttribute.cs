﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Meta.BO;
using Core.Meta.Systems;
using System.Runtime.Serialization;
using System.ComponentModel.Composition;
using Microsoft.Practices.Prism.Modularity;

namespace Core.Composition.Modularity
{
    /// <summary>
    /// Атрибут закрепляющий Менеджер Модуля для клиента 
    /// </summary>    
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public class ModuleExtAttribute : ExportAttribute
    {


        #region ------------------------------  CONSTRUCTOR ------------------------------------
#if (SILVERLIGHT || WINDWOS_PHONE)                
        public ModuleExtAttribute(ServiceModelManager_RegisterEn currentModuleService, String  CurrTargetRegion)
            : base(typeof(IModuleExt)) 
        {
            ModuleService = currentModuleService;
            TargetRegion = CurrTargetRegion;
        }
#else //Server
        public ModuleExtAttribute(ServiceModelManager_RegisterEn currentModuleService) //, String  CurrTargetRegion
            : base(typeof(IModuleExt)) 
        {
            ModuleService = currentModuleService;
            //TargetRegion = CurrTargetRegion;
        }

#endif

        #endregion ------------------------------  CONSTRUCTOR ------------------------------------



        /// <summary>
        /// Модель / Название данного сервиса  
        /// </summary>
        public ServiceModelManager_RegisterEn ModuleService
        {  get;  private set;   }
                

        /// <summary>
        /// Целевой Регион
        /// </summary>
        public String TargetRegion
        { get; set; }

    }
}

