﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Commanding;
using Core.Meta.Systems;
using Core.Composition.DataServices;
using Core.Composition.Communication;
using Microsoft.Practices.Prism.Modularity;
using System.Resources;


#if CLIENT && SL5

using System.Windows.Controls;
using Core.Communication;
using Core.Presentation;
using Core.Modularity;
using Core.Meta.Parametrization;
using System.Threading.Tasks;
//using Cor.Presentation;

#endif


namespace Core.Composition.Modularity
{
    // Инфраструктурой Всех Модулей у нас управляет ModularityInsfrastructureService/ аналог ModuleManager в PRISM. Инкапсулирует менеджер PRISM



    /// <summary>
    ///  N(ew) Module- интерфейс для менеджеров модуля с учетом особенностей внутрисистемного композитинга 
    ///  У модели сервиса есть зависимости  но зависимости от  других моделей сервисов/
    ///  Для модуля список зависимостей моджет отличаться и как правило это так
    /// </summary>
    public interface IModuleExt : IModule
#if CLIENT && SL5
    , IFlowProcessModule      
#endif

    {

        /// <summary>
        /// Менеджер модели Сервиса/ - Которая находится в Модели Доступа данных
        /// </summary>
        IServiceModelManager ServiceModelManager { get; }
        

        /// <summary>
        /// Сервис /Модель Сервиса/ Модель сервиса для данного модуля.( Это разные понятия, но в данном случае все они скрываются под  данным Ключом )  
        /// </summary>
        ServiceModelManager_RegisterEn ModuleService { get; }
        

      

        /// <summary>
        /// Смена системой стаутса для Модуля
        /// </summary>
        /// <param name="StateFromSystem"></param>
        void SetModuleState(ModuleStatesEn StateFromSystem);





        /// <summary>
        /// Менеджер ресурсов
        /// </summary>
        ResourceManager ResourceManager { get; }
         

#if CLIENT && SL5


        /// <summary>
        /// Регион вывода для Модуля
        /// </summary>
        String TargetRegion { get; set; }


        /// <summary>
        /// Название модуля - будет использоваться для навигации
        /// </summary>
        String ModuleName { get; }

        

        /// <summary>
        /// Вывести первую/стартовую  страницу модуля
        /// </summary>
        /// <returns></returns>
        void  ShowStartPage();
        

        /// <summary>
        /// Создать Представление 
        /// </summary>
        /// <param name="ViewKey"></param>
        /// <returns></returns>
        // ViewBase CreateView(String ViewKey);


        /// <summary>
        /// Создать страницу с внутреннимим  вьюшками
        /// </summary>
        /// <param name="PageKey"></param>
        /// <returns></returns>
        PageBase CreatePage(String PageKey);


        /// <summary>
        /// [Obsolete] Запуск команды Через Системный диспетчер если:       
        /// </summary>                
        //void ExecuteCommand(ref CommandContainer commandContainer, CommandResultDelegate ModuleUserCallback);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="commandContainer"></param>
        /// <param name="ModuleUserCallback"></param>
        Task<CommandContainer> ExecuteCommandAsync(CommandContainer commandContainer);


        /// <summary>
        /// Перемещение на страницу даннного модуля
        /// </summary>
        /// <param name="ModuleName"></param>
        /// <param name="PageKey"></param>
        /// <param name="Mode"></param>
        void NavigateTo(String PageKey, PresentationModeEn Mode);

        /// <summary>
        /// Навигация до Флу страницы и с режимами FlowProcessMode.Update / View, и параметрами инициализации  
        /// </summary>
        /// <param name="PageKey"></param>
        /// <param name="Mode"></param>
        /// <param name="targetBO"></param>
        /// <param name="initParams"></param>
        void NavigateTo(String PageKey, PresentationModeEn Mode, params ParameterTp[] initParams);
#endif


    }
}
