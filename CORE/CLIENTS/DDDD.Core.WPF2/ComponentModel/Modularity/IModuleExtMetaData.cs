﻿using System;
using Core.Meta.Systems;
namespace Core.Composition.Modularity
{

    public interface IModuleExtMetaData
    {
        ServiceModelManager_RegisterEn ModuleService { get; }
    }
}
