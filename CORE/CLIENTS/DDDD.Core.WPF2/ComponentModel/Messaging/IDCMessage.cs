﻿using System;
using System.Collections.Generic;

namespace DDDD.Core.ComponentModel.Messaging
{

    /// <summary>
    /// DCMessage - Dynamic Command message.
    /// </summary>
    public interface IDCMessage:IDynamicCommand
    {
                       
        bool HasOperationSuccessfullyCompleted { get; }
        bool HasServerErrorHappened { get; }
       

        DateTime ClientRecievedResultFromServerTime { get; }
        DateTime ServerReceivedFromClientTime { get; }
        DateTime ServerSendToClientTime { get; }
        DateTime ClientSendTime { get; }

        void SetClientRecievedResultFromServerTime(DateTime clientRecievedResultFromServerTime);
        void SetClientSendTime(DateTime clientSendTime);
               
        
        void SetServerReceivedFromClientTime(DateTime serverReceivedFromClientTime);
        void SetServerSendToClientTime(DateTime serverSendToClientTime);

        


    }




}
