﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Serialization;
using System.Xml.Serialization;

namespace DDDD.Core.ComponentModel.Messaging
{


 

    #region --------------------------------------- CUSTOM MESSAGE CONTRACT --------------------------------------
    
    /// <summary>
    /// Dynamic Command Message. It's also the example of custom CommandMessage.
    /// </summary>
    [DataContract]
    public class DCMessage : IDCMessage
    {

        //ServiceName
        //ServiceNamespace



        #region ------------------------------ CTOR ---------------------------------

        public DCMessage()
        {

        }
         
        #endregion ------------------------------ CTOR ---------------------------------


        /// <summary>
        /// Empty Message
        /// </summary>
        public static DCMessage Empty
        { get; } = default(DCMessage);
        
        

        /// <summary>
        /// This property will contains packed DCMessage by secondary Serializer(like TSS or some of ServiceStack's serialiers) from client to server. 
        /// </summary>
        [IgnoreMember] // not serialize by TSS
        [DataMember]   // serialize with DataContractSerializer 
        [XmlIgnore]
        public byte[] Body { get; set; }


        #region -------------------------------- TIME DIAGNOSTICS -------------------------------

        /// <summary>
        /// Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientSendTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        public DateTime ServerReceivedFromClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server sending processed command to client( server time )
        /// </summary>
        public DateTime ServerSendToClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the client received processed command from server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientRecievedResultFromServerTime
        {
            get;
            private set;
        }
        
        #endregion --------------------------------TIME DIAGNOSTICS -------------------------------


        #region -------------------------------- OPERATION PROGRESS --------------------------------

        /// <summary>
        /// Operation Progress in Percents
        /// </summary>
        public int OperationProgressPercent
        {
            get;
            private set;
        }


        /// <summary>
        /// Operation Progress State -string value
        /// </summary>
        public string OperationProgressState
        {
            get;
            private set;
        }

        /// <summary>
        /// If we use Operation Progress info properties - OperationProgressPercent and OperationProgressState
        /// </summary>
        public bool UseOperationProgress
        {   get
            {
                return (OperationProgressPercent >= 0 || OperationProgressState.IsNotNullOrEmpty()); 
            }
        }
        #endregion -------------------------------- OPERATION PROGRESS --------------------------------


        #region --------------------------- TARGET DC MANAGER ------------------------------


        /// <summary>
        /// Target DC Service/Manager class that can process this command                
        /// </summary> 
        [DataMember]
        public string DCManager { get; set; }
        

        /// <summary>
        ///  Command or Method of  TargetDC Service/Manager to call it
        /// </summary>
        [DataMember]
        public string Command { get; set; }


        /// <summary>
        /// Flexible TSS can serialize complex parameter classes/structs and then we box them into the List of params.
        /// </summary>
        public List<DCParameter> Parameters { get; set; }

        /// <summary>
        /// Check if DC Parameters contain Param with [key]
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsParam(string key)
        {
            return Parameters.FirstOrDefault(prm=> prm.Key == key) != null;
        }

        /// <summary>
        /// Get Parameters from Parameters by Key. Result can be null if not exists.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public DCParameter GetParam(string key)
        {
            return Parameters.FirstOrDefault(prm => prm.Key == key);
        }

        public T GetParamVal<T>(string key)
        {
            var param = GetParam(key);
            if (param != null) return (T)param.Value;
            else return default(T);
        }

        public List<DCParameter> GetAllParams()
        {
            return Parameters;
        }


        /// <summary>
        /// Add  In Parameter
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="communicationUnit"></param>
        public void NewInParam(string key, object value )
        {
           Parameters.Add(DCParameter.NewInParam(key, value));             
        }


        /// <summary>
        /// Add  Out Parameter
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="communicationUnit"></param>
        public void NewOutParam(string key, object value)
        {
            Parameters.Add(DCParameter.NewOutParam(key, value));
        }
          

        #endregion --------------------------- TARGET DC MANAGER ------------------------------


        #region -------------------------- RESULT  ----------------------------

        const string ResultKey = "Result";
        /// <summary>
        /// Get Result Parameter. We trying to get Parameter with the [Result] key. 
        /// Return value can be null if parameter with such Key doesn't exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public DCParameter GetResult()
        {
            return Parameters.FirstOrDefault(prm => prm.Key == ResultKey);
        }
         


        /// <summary>
        /// Set result value 
        /// </summary>
        /// <param name="resultValue"></param>
        public void SetResult(object resultValue)
        {
            var resultParam = GetResult();
            if (resultParam != null)
            {
                resultParam.Value = resultValue;
            }
            else
            {
                Parameters.Add(DCParameter.NewOutParam(ResultKey, resultValue));
            }
        }
        #endregion -------------------------- RESULT  ---------------------------- 


        #region ------------------------- ERROR, PROGRESS STATES-------------------------------


        /// <summary>
        /// Message on successful result (if you wish)
        /// </summary>
        public string ProcessingSuccessMessage { get; set; }


        /// <summary>
        /// Message on operation Fault
        /// </summary>
        public string ProcessingFaultMessage { get; set; }


        /// <summary>
        /// Internal System Error Code  
        /// </summary>
        public short ErrorCode { get; set; }



        /// <summary>
        /// Server( Command Controller)  should set this message if error happened  before send CommandMessage back.
        /// After such CommandMessage will be received by client we'll show full error message only in [Debug] Conditional  mode.
        /// In release mode client will see only common message - like [SERVER ERROR HAPPENED] 
        /// </summary>
        public bool HasServerErrorHappened
        {
            get
            {
                return !ProcessingFaultMessage.IsNullOrEmpty();
            }
        }

        /// <summary>
        /// By default Successfull Message is empty string, that means that Command is succsessful  - until server won't set an ErrorMessage.
        /// So if we have Empty String or valid Succesfull message -  command will be means as successful.  
        /// </summary>
        public bool HasOperationSuccessfullyCompleted
        {
            get
            {
                return !ProcessingSuccessMessage.IsNullOrEmpty();
            }
        }



        #endregion ------------------------- ERROR, PROGRESS STATES-------------------------------



        /// <summary>
        ///  Create new  DCMessage
        /// </summary>
        /// <param name="targetDCService"></param>
        /// <param name="command"></param>
        /// <param name="commandParametersBin"></param>
        /// <returns></returns>
        public static DCMessage Create(string targetDCService, string command, params DCParameter[] commandParametersBin)
        {
            return new DCMessage()
            {
                DCManager = targetDCService
                ,
                Command = command
                ,
                Parameters = commandParametersBin.ToList()
            };
        }


        /// <summary>
        /// Clear Input Parameters
        /// </summary>
        public void ClearInputParameters()
        {
            //clear all input parameters            
            var outParameters = new List<DCParameter>();

            for (int i = 0; i < Parameters.Count; i++)
            {
                if (Parameters[i].IsOut) outParameters.Add(Parameters[i]);
            }

            Parameters = outParameters;

            
        }



        /// <summary>
        ///Set  ServerReceivedFromClientTime when - Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        /// <param name="serverReceivedFromClientTime"></param>
        public void SetServerReceivedFromClientTime(DateTime serverReceivedFromClientTime)
        {
            ServerReceivedFromClientTime = serverReceivedFromClientTime;
        }

        /// <summary>
        /// Set  ServerSendToClientTime when - Time point - when the server sending processed command to client( server time )
        /// </summary>
        /// <param name="serverSendToClientTime"></param>
        public void SetServerSendToClientTime(DateTime serverSendToClientTime)
        {
            ServerSendToClientTime = serverSendToClientTime;
        }


        /// <summary>
        /// Set  ClientSendTime when - Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        /// <param name="clientSendTime"></param>
        public void SetClientSendTime(DateTime clientSendTime)
        {
            ClientSendTime = clientSendTime;
        }

        /// <summary>
        /// Set  ClientRecievedResultFromServerTime when - Time point - when the client received processed command from server( client time synchronized with server ) 
        /// </summary>
        /// <param name="clientRecievedResultFromServerTime"></param>
        public void SetClientRecievedResultFromServerTime(DateTime clientRecievedResultFromServerTime)
        {
            ClientRecievedResultFromServerTime = clientRecievedResultFromServerTime;
        }

        

        public DCMessage SayCommandCompleteSuccesfully()
        {
            ProcessingSuccessMessage = $"Command [{Command}] Completed successfully";
            return this;
        }


        public DCMessage SayCommandThrowUnknownError(string fromClass, string exceptionMessage)
        {
            ProcessingFaultMessage = $"source[{fromClass}] .. Command [{Command}] Faulted with [{exceptionMessage}] ";
            return this;
        }

      

      
    }


    #endregion --------------------------------------- CUSTOM MESSAGE CONTRACT --------------------------------------

    




}

