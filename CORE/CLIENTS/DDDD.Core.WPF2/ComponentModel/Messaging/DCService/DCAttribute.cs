﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace  DDDD.Core.ComponentModel.Messaging
{



    /// <summary>
    /// Часть  исполняемой команды, привязанная  к  одному из сторон(может тьеров) Серверной /Клиентская
    /// </summary>
    public enum DCSideEn
    {
        /// <summary>
        /// 
        /// </summary>
        NotDefined,

        /// <summary>
        /// 
        /// </summary>
        Server,

        /// <summary>
        /// 
        /// </summary>
        ServerInContext,

        /// <summary>
        /// 
        /// </summary>
        ServerInContext2,

        /// <summary>
        /// 
        /// </summary>
        ServerInContext3,

        /// <summary>
        /// Первый вариант клиентского метода
        /// </summary>
        Client,

        /// <summary>
        /// Простая перегрузка клиентского метода
        /// </summary>
        ClientOverload,
        ClientOverload2,
        ClientOverload3,
        ClientOverload4,
        ClientOverload5,

        /// <summary>
        /// 
        /// </summary>
        ClientPagableInit,

        /// <summary>
        /// 
        /// </summary>
        ClientPagableTransition
    }



    /// <summary>
    /// DC Dynamic Command - should be attached to the method that executes this Dynamic Command 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple=false) ]
  public sealed class DCAttribute : Attribute
  {      
      public DCAttribute(String commandName, DCSideEn commandSide)
      {
          CommandName = commandName;
          Side = commandSide;
      }

      
      /// <summary>
      /// DC Name - comman0d name( like [SelectItem]...)
      /// </summary>
      public String CommandName
      { get; private set; }
               


      /// <summary>
      /// DC command side  ( like [Server] [Client]... other custom sides names) .
      /// </summary>
      public DCSideEn  Side
      { get; private set; }
        

    }

     
}
