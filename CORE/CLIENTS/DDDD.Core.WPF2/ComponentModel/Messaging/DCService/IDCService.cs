﻿using DDDD.Core.ComponentModel.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel.Messaging 
{
    /// <summary>
    /// Dynamic Command Service
    /// </summary>
    public interface IDCService:IService
    {
        /// <summary>
        /// Execute DC(Dynamic Command) sett result to this DC and return this object        
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        IDCMessage ExecuteCommand(ref IDCMessage command);

    }



}
