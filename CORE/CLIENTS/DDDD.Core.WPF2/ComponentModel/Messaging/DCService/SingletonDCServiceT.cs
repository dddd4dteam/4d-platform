﻿using DDDD.Core.Threading;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDD.Core.ComponentModel.Messaging
{
    /// <summary>
    /// Singleton DC(Dynamic Command) Service  base generic  class. 
    /// <para/>It uses DCMessage object - command and message container, 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SingletonDCService<T> : IDCService
      where T : class
    {

        #region ---------------- IService ----------------


        /// <summary>
        /// Service IDentifier
        /// </summary>
        public Guid SvcID
        { get; private set; }


        /// <summary>
        /// Service Name
        /// </summary>
        public string SvcName
        { get; private set; }

        /// <summary>
        /// Service  Description
        /// </summary>
        public string SvcDescription
        { get; protected set; } = "";

        /// <summary>
        /// Service Type Info Ex
        /// </summary>
        public TypeInfoEx SvcTypeEx
        { get; private set; } = typeof(T).GetTypeInfoEx();

        /// <summary>
        /// Thread Safety created T service instance
        /// </summary>
        public static T Current
        {
            get { return LA_Current.Value; }
        }


        /// <summary>
        /// Instance of DCManager of [TManaget] Type - your custom end DCManager Type
        /// </summary>
        protected static LazyAct<T> LA_Current
        { get; } = LazyAct<T>.Create(
            (args) =>
            {
                // ctor()
                var newTargetT = TypeActivator.CreateInstanceT<T>();// new T();
                // Initializing
                (newTargetT as SingletonService<T>).Initialize();
                return newTargetT;
            }
            , null, null
            );



        #endregion ---------------- IService ----------------


        #region ---------- IDCService ------------------

        /// <summary>
        /// use rule: Method with such name should exist  -so  this code donot check for such key-methodName exist in delegates dictionary
        /// <para/>use rule:  try/catch block  and error/successful definitions for coomand is done in this method
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public virtual IDCMessage ExecuteCommand(ref IDCMessage command)
        {   // method can't to do not contains here
            // try/catch block  and error/successful definitions for coomand is done in this method
            try
            {
                return ServerDCActions[command.Command](ref command);
            }
            catch (Exception exc)
            {
                command.ProcessingFaultMessage = exc.Message;
                //LOG if LOGGING on
                throw exc;
            }                 
        }

        #endregion ---------- IDCService ------------------


        #region ------------ INITIALIZATION BASE --------------
        /// <summary>
        /// Indicator -shpw if singleton  instance was Inited
        /// </summary>
        public bool IsInited
        { get; protected set; }

        /// <summary>
        /// This Initialize() method will be called after T instance will be created - not inside Ctor of T.
        /// </summary>
        protected virtual void Initialize()
        {
            if (IsInited == false)
            {
                SvcID = Guid.NewGuid();
                SvcName = SvcTypeEx.OriginalType.Name;
            }

            // Precompile managers server-side Methods of Two-side Commands
            InitCollectPrecompileMethods();

            // Inited  = true only in Child Class
            // SvcDescription in end Service class 
        }

        #endregion ------------ INITIALIZATION BASE --------------


        #region -------------- PRECOMPILED  SERVER SIDE DC ACtion DELEGATES ----------------
        
        /// <summary>
        ///  ServerDCActions - CommandManager Actions with DCMessage
        /// </summary>
        protected Dictionary<string, DCAction> ServerDCActions
        { get; private set; } = new Dictionary<string, DCAction>();

        /// <summary>
        /// All Custom Methods that will EndsWith keyword [Server] will means as Server-side you Manager API method.
        /// Then such method will be precompiled and end delegate will be saved in [ServerDCActions] dictionary        /// 
        /// </summary>
        //  const string ServerMethodKeyword = "Server";

        /// <summary>
        /// DCAction one parameter Type is - typeof(DCMessage).MakeByRefType()
        /// </summary>
        static readonly Type DcActionParameterType = typeof(IDCMessage).MakeByRefType();



        /// <summary>
        /// Collect all server-methods that  
        /// 1 condition- contains DCAttribute with Side: Server
        /// and 
        /// 2 condition -method has signature of DCAction delegate; 
        /// 3 Method can be static or not
        /// and precompile and cache calls for each of this methods internally. 
        /// </summary>
        protected virtual void InitCollectPrecompileMethods()
        {
            // only one method can be with( Name and of delegate signature - DCAction)
            // methods that is DCAction delegate signature should have DCAttribute.Side == Server            
            var serverCommandMethods = GetType().GetMethods()//ServerSideMethodsSelector)
                .Where(m =>
                m.GetFirstMethodAttribute<DCAttribute>().Side == DCSideEn.Server);



            if (serverCommandMethods.IsWorkable())
            {
                foreach (var possibleServerMethod in serverCommandMethods)
                {
                    // if it's really not method with  target signature of  DCAction delegate then go further
                    if (possibleServerMethod.GetParameters().Length != 1) continue;

                    if (possibleServerMethod
                        .GetParameters()[0].ParameterType == DcActionParameterType)
                    {
                        if (possibleServerMethod.IsStatic)
                        {
                            var serverDCAction = (DCAction)possibleServerMethod.CreateDelegate(typeof(DCAction), null);
                            ServerDCActions.Add(possibleServerMethod.Name, serverDCAction);
                        }
                        else
                        {
                            var serverDCAction = (DCAction)possibleServerMethod.CreateDelegate(typeof(DCAction), this);
                            ServerDCActions.Add(possibleServerMethod.Name, serverDCAction);
                        }
                    }
                }

            }
        }

        #endregion  -------------- PRECOMPILED  SERVER SIDE DC ACtion DELEGATES ----------------


    }
}
