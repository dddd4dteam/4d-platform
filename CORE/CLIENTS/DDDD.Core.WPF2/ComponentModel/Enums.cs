﻿using System;

namespace DDDD.Core.ComponentModel
{




    /// <summary>
    /// Loading Mode - how we'll load assemblies into
    /// </summary>
    public enum AssembliesLoadModeEn
    {
        /// <summary>
        /// In such way programmer needs to add all needable assembly into ComponentsContainer manually by ComponentsContainer's API.
        /// </summary>
        ManualAssembliesLoading

        /// <summary>
        /// Loading assemblies into ComponentsContainer automatically - it'll describe to event [TypeCache.DomainTypesAdded], -so it'll load each of the filtered domain assembly that TypeCache will load in it.
        /// </summary>
       , AutoLoadingDomainAssemblies


    }


    public enum InitAppModelEn
    {
        /// <summary>
        /// UI Application with Shell based on Pages
        /// </summary>
        InitShell_PagelApp
       ,
        /// <summary>
        /// UI Application with Shell based on Windows
        /// </summary>
        InitShell_WindowApp
       ,
        /// <summary>
        /// Background Application- Service/TarayApp/Module(extension in VS ) with  based on Windows management UI
        /// </summary>
        InitBackgroun_WindowApp
       ,
        /// <summary>
        /// Background Application- WebApp/Console/ with Windows management UI
        /// </summary>
        InitBackground_EmptyApp
    }


    /// <summary>
    ///  We have some Application Model, that consist of some Component Classes - Known  Components Classes - their enumeration.  
    /// </summary>
    [Flags]
    public enum ComponentClassEn:int
    {

        /// <summary>
        /// Not Pointed some Component Class
        /// </summary>
        NotDefined = 0
        ,
        /// <summary>
        ///  DA(Data Access) Service data models
        /// </summary>
        ServiceModels = 1
        ,
        /// <summary>
        /// DC Managers - main BL manager unit: can dynamically reserve DCCUnit- Service- on server side/Client - on Client side.  Components of this Class of this Class based on class [DCManager].
        /// </summary>
        DCManagers = 2

        ,
        /// <summary>
        /// DC Services - main BL service unit: can dynamically reserve DCCUnit- Service- on server side/Client - on Client side.  Components of this Class of this Class based on class [DCServer].
        /// </summary>
        DCServices = 4
        , 
        /// <summary>
        /// Web Api Controllers - based on ApiController classes
        /// </summary>
        WebApiControllers = 8

#if SERVER

        ,
        /// <summary>
        /// Component Class - DCCommunicationUnitService Factories. Components of this Class implements interface  [IDCCommunicationUnitServiceFactory].
        /// </summary>
        DCCUnitServerFactories = 16

#endif

#if CLIENT
        ,
        /// <summary>
        /// Component Class - UserControls.  Components of this Class based on class [System.Windows.Controls.UserControl].
        /// </summary>
        UserControls = 32

        ,
        /// <summary>
        /// Component Class - Pages.  Components of this Class based on class [System.Windows.Controls.Page].
        /// </summary>
        Pages = 64

        ,
        /// <summary>
        /// Component Class - Windows. Components of this Class based on class [System.Windows.Controls.Window].
        /// </summary>
        Windows = 128

        ,
        /// <summary>
        /// Component Class - DCCommunicationUnitClient Factories. Components of this Class implements interface  [IDCCommunicationUnitClientFactory].
        /// </summary>
        DCCUnitClientFactories = 256
#endif



    }


}
