﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel 
{
    /// <summary>
    /// Dynamic Command. Any BL command/operation can be expressed by this concept structure.
    /// <para/> Dynamic command contains of  DCParameters list - input and  output params
    /// (looks like in Stored Procedures in SQL).
    /// <para/> Dynamic command  contains a pointer to the executing DCManager
    /// <para/> DCParameter can save almoust all data Type -Bianry/Json serializers makes borders here,    
    /// <para/> IDynamicCommand integrated  here into the  IDCMessage. 
    /// </summary>
    public interface IDynamicCommand
    {

        /// <summary>
        /// Target DC Service/Manager class that can process this command                
        /// </summary>
        /// </summary>
        string DCManager { get; set; }

        /// <summary>
        /// Target Command(method Name) in target DCManager
        /// </summary>
        string Command { get; set; }

        /// <summary>
        /// If we  Use Operation Progress on its Processing
        /// </summary>
        bool UseOperationProgress { get; }


        /// <summary>
        /// Error Code   here it is short 
        /// </summary>
        short ErrorCode { get; set; }

        /// <summary>
        /// Percent of Progress of current command-operation
        /// </summary>
        int OperationProgressPercent { get; }

        /// <summary>
        /// State of Progress of current command-operation
        /// </summary>
        string OperationProgressState { get; }

        /// <summary>
        /// Fault message if error Happened during command-operation processing
        /// </summary>
        string ProcessingFaultMessage { get; set; }

        /// <summary>
        /// Success message if  command-operation processed successfully
        /// </summary>
        string ProcessingSuccessMessage { get; set; }


        /// <summary>
        /// Check if DC Parameters contain Param with [key]
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        bool ContainsParam(string key);

        /// <summary>
        /// Get command Parameter by Key.
        /// </summary>
        /// <param name="key">parameter key</param>
        /// <returns></returns>
        DCParameter GetParam(string key);

        /// <summary>
        /// Get Parameter Value of T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        T GetParamVal<T>(string key);

        /// <summary>
        /// Get list of all command parameters
        /// </summary>
        /// <returns></returns>
        List<DCParameter> GetAllParams();
        

        /// <summary>
        /// CLear all INPUT Parameters
        /// </summary>
        void ClearInputParameters();


        /// <summary>
        /// Set Result value param
        /// </summary>
        /// <param name="resultValue"></param>
        void SetResult(object resultValue);

        /// <summary>
        /// Get Parameter with [Result] key - default convention
        /// </summary>
        /// <returns></returns>
        DCParameter GetResult();



        void NewInParam(string key, object value);
        void NewOutParam(string key, object value);



    }
}
