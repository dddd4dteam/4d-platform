﻿using System;
using System.Net.Http;

using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Serialization;
using DDDD.Core.Resources;




namespace DDDD.Core.ComponentModel
{


    /// <summary>
    /// TextParameter structure  that used in DCMessage2. It used to utilize work with data formatted as Text. 
    /// </summary>
    public class DCParameter 
    {

        #region ---------------------------- PROPERTIES --------------------------------

        /// <summary>
        /// If ValueText or ValueBody is not null then we need to unpack end value into Value. 
        /// </summary>
        public bool UsePackUnpackLogic
        {
            get { return (ValueText.IsNotNull() || ValueBody.IsNotNull()); }
        }


        /// <summary>
        /// Type Full Name. 
        /// </summary>
        public string CSharpTypeFullName { get; set; }



        /// <summary>
        ///IS OUT Parameter? This also means that by default every parameter is IN PARAMETERS.
        /// </summary>
        public bool IsOut { get; internal set; }



        /// <summary>
        /// Parameter key to get it from Parameters array
        /// </summary>
        public string Key { get; internal set; }



        /// <summary>
        /// Some Text data value - serialized with one of the Text serializers(Json, csv, xml) parameter's original value. 
        /// </summary>
        public string ValueText { get; internal set; }



        /// <summary>
        /// Some Binary data value - serialized with  TSS/other binary serializer  parameter's original value. 
        /// </summary>
        public byte[] ValueBody { get; internal set; }



        /// <summary>
        /// Deserialized value.  
        /// </summary>
        public object Value { get; set; }



        #endregion ---------------------------- PROPERTIES --------------------------------



        #region ----------------------------- CREATE NEW IN/OUT PARAMETER -----------------------------


        /// <summary>
        /// Create new IN TextParameter instance.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="paramValue"></param>        
        /// <returns></returns>
        public static DCParameter NewInParam(string key, object paramValue)
        {
            Validator.ATNullReferenceArgDbg(paramValue, nameof(DCParameter), nameof(NewInParam), nameof(paramValue));

            //check that parameter still was not created.

            var newTextParameter = new DCParameter();
            newTextParameter.Key = key;
            newTextParameter.Value = paramValue;

            newTextParameter.IsOut = false;

            newTextParameter.CSharpTypeFullName = DCCUnit.GetCSharpTypeName(paramValue.GetType());


            return newTextParameter;
        }



        /// <summary>
        /// Create new OUT TextParameter instance. 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public static DCParameter NewOutParam(string key, object paramValue)
        {
            Validator.ATNullReferenceArgDbg(paramValue, nameof(DCParameter), nameof(NewOutParam), nameof(paramValue));

            //check that parameter still was not created.

            var newTextParameter = new DCParameter();
            newTextParameter.Key = key;
            newTextParameter.Value = paramValue;

            newTextParameter.IsOut = true;
            newTextParameter.CSharpTypeFullName = DCCUnit.GetCSharpTypeName(paramValue.GetType());

            return newTextParameter;

        }


        #endregion ----------------------------- CREATE NEW IN/OUT PARAMETER -----------------------------


        #region ------------------------ SERIALIZE /DESERIALIZE ------------------------------

        /// <summary>
        /// If some deserialize exception happened during unpacking operation of parameter Value from ValueBody or valueText.  Ignore serializing this field with TSS.
        /// </summary>
        [IgnoreMember]
        public bool HasDeserializeException { get; internal set; }


        /// <summary>
        /// Deserialize exception message if error happened during unpacking operation of parameter Value from ValueBody or valueText. 
        /// </summary>
        [IgnoreMember]
        public string DeserializeExceptionMessage { get; internal set; }


        /// <summary>
        /// Serialization Exception happend on Server side.  Really transmittable field that told use that parameter was not successfully serialized on sender side
        /// </summary>
        public bool HasServerSerializationException { get; internal set; }

        /// <summary>
        /// Serialization Exception happend on Client side.  Really transmittable field that told use that parameter was not successfully serialized on sender side
        /// </summary>
        public bool HasClientSerializationException { get; internal set; }

        public T GetValue<T>()
        {
            return (T)Value;            
        }



        #endregion ------------------------ SERIALIZE /DESERIALIZE ------------------------------






    }

}
