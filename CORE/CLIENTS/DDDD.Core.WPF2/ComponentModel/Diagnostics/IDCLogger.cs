﻿using System;
using System.Collections.Generic;
using DDDD.Core.ComponentModel.Messaging;
using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;
using System.Diagnostics;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// DCLogger(DC-Dynamic Commands) interface. 
    /// </summary>
    public interface IDCLogger
    {
        string OwnerKey { get; }

        bool IsConfigured { get; }

        bool IncludeExceptionStackTrace { get; }

        List<string> DCDiagnosticKeys { get; }


#if SERVER || WPF

        Layout EL_Category { get; set; }
        Layout EL_EntryType { get; set; }
        string EL_MachineName { get; set; }
        int EL_MaxMessageLength { get; set; }
        EventLogTargetOverflowAction EL_OnOverflowAction { get; set; }
        Layout EL_Source { get; set; }
        
        void InitAsWinEventLogger(string eventLogPoint, string machineName, LogLevel loglevel, Layout logMessageLayout, Layout sourceLayout, Layout entryType, Layout categoryLayout, EventLogTargetOverflowAction overflowAction = EventLogTargetOverflowAction.Split, int maxMessageLength = 16384);
      
#endif

        Layout FL_FileName { get; set; }
        Logger LogLogger { get; set; }
        DCLoggingEn LogLogging { get; }
        LoggingRule LogRule { get; set; }
        TargetWithLayout LogTarget { get; set; }
        Layout MessageLayout { get; set; }


        void InitAsFileLogger(string targetFilePath, LogLevel loglevel, Layout logMessageLayout);


        void Debug(string message);

        void Error(string message);

        void Fatal(string message);

        void Info(string message);      
        void Trace(string message);
        void Warn(string message);
   
    }
}