﻿using NLog;
using NLog.Config;
using NLog.Layouts;
using NLog.Targets;

using System;
using System.Text;
using System.Collections.Generic;
using System.Diagnostics;

using DDDD.Core.Extensions;
using DDDD.Core.ComponentModel.Messaging;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Types of enabled to DCLogger Loggings : File Logging, Windows Events Logging, ColoredConsole Logging
    /// </summary>
    public enum DCLoggingEn
    {
        /// <summary>
        ///  File Logging
        /// </summary>
        FileLogging

        ,
        /// <summary>
        /// Windows Events Logging
        /// </summary>
        WinEventsLogging

        ,
        /// <summary>
        /// Colored Console Logging
        /// </summary>
        ColoredConsoleLogging
    }


    /// <summary>
    /// NLog Logging compact form for:  File Logging, Windows Events Logging, ColoredConsole Logging.
    /// </summary>
    public class DCLogger
    {

        #region ----------------------------- CTOR ---------------------------------

        static DCLogger()
        {
            LogManager.Configuration = CommonConfiguration;
        }

        #endregion ----------------------------- CTOR ---------------------------------

        /// <summary>
        /// Default delimeter between message render parts. Default value is [ | ].
        /// </summary>
        public static string Delimetr
        { get; set; } = " | ";

        /// <summary>
        /// Default delimeter const 
        /// </summary>
        public const string D = " | ";

        /// <summary>
        /// Default Log Key -  "Application"
        /// </summary>
        public const string DefaultLogKey = "Application";

        /// <summary>
        /// Default message render layout -  {LRC.date}{D}{LRC.time}{D}{LRC.threadid}{D}{LRC.processname}{D}{LRC.message}
        /// </summary>
        public static string DefaultMessageRenderLayout
        { get; } = $"{LRC.date}{D}{LRC.time}{D}{LRC.threadid}{D}{LRC.processname}{D}{LRC.message}";  


        #region ---------------------------- NLOG TEMPLATE KEYS ----------------------------
        /// <summary>
        /// Layout  Render Constants
        /// </summary>
       public class LRC
        {
            /// <summary>
            ///   ${activityid} - Puts into log a System.Diagnostics trace correlation id.
            /// </summary>
            public const string activityid = "${activityid}"; // 
            /// <summary>
            /// ${all-event-properties} - Log all event context data.
            /// </summary>
            public const string all_event_properties = "${all-event-properties}";
            /// <summary>
            /// ${appdomain} - Current app domain.
            /// </summary>
            public const string appdomain = "${appdomain}";
            /// <summary>
            /// ${asp-application} - ASP Application variable.
            /// </summary>
            public const string asp_application = "${asp-application}";  //
            /// <summary>
            /// ${asp-request} - ASP Request variable.
            /// </summary>
            public const string asp_request = "${asp-request}";      // 
            /// <summary>
            /// ${asp-session} - ASP Session variable.
            /// </summary>
            public const string asp_session = "${asp-session}";      // 
            /// <summary>
            /// ${assembly-version} - The version of the executable in the default application domain.
            /// </summary>
            public const string assembly_version  = "${assembly-version}";       // 
            /// <summary>
            /// ${basedir} - The current application domain's base directory.
            /// </summary>
            public const string basedir = "${basedir}";     // 
            /// <summary>
            /// ${callsite} - The call site(class name, method name and source information).
            /// </summary>
            public const string callsite = "${callsite}";  //
            /// <summary>
            /// ${callsite-linenumber} - The call site source line number.
            /// </summary>
            public const string callsite_linenumber = "${callsite-linenumber}";  //
            /// <summary>
            /// ${counter} - A counter value(increases on each layout rendering).
            /// </summary>
            public const string counter = "${counter}";   //
            /// <summary>
            /// ${date} - Current date and time.
            /// </summary>
            public const string date = "${date}";  //
            /// <summary>
            /// ${document-uri} - URI of the HTML page which hosts the current Silverlight application.
            /// </summary>
            public const string document_uri = "${document-uri}";   //
            /// <summary>
            /// ${environment} - The environment variable.
            /// </summary>
            public const string environment = "${environment}";    //  
            /// <summary>
            /// ${event-properties} - Log event properties data - rename of ${event-context }.
            /// </summary>
            public const string event_properties = "${event-properties}";    //
            /// <summary>
            /// ${exception} - Exception information provided through a call to one of the Logger.* Exception() methods.
            /// </summary>
            public const string exception = "${exception}";    //
            /// <summary>
            /// ${file-contents} - Renders contents of the specified file.
            /// </summary>
            public const string file_contents = "${file-contents}";  //
            /// <summary>
            /// ${gc} - The information about the garbage collector.
            /// </summary>
            public const string gc = "${gc}";     //
            /// <summary>
            /// ${gdc} - Global Diagnostic Context item.Dictionary structure to hold per-application-instance values.
            /// </summary>
            public const string gdc = "${gdc}";  //
            /// <summary>
            /// ${guid} - Globally-unique identifier(GUID).
            /// </summary>
            public const string guid = "${guid}";  // 
            /// <summary>
            /// ${identity} - Thread identity information(name and authentication information).
            /// </summary>
            public const string identity = "${identity}"; //

            /// <summary>
            /// ${install-context} - Installation parameter(passed to InstallNLogConfig).
            /// </summary>
            public const string install_context = "${install-context}"; //
            /// <summary>
            /// ${level} - The log level.
            /// </summary>
            public const string level = "${level}";  //
            /// <summary>
            /// ${literal} - A string literal.
            /// </summary>
            public const string literal = "${literal}"; //
            /// <summary>
            /// ${log4jxmlevent} - XML event description compatible with log4j, Chainsaw and NLogViewer.
            /// </summary>
            public const string log4jxmlevent = "${log4jxmlevent}";  //
            /// <summary>
            /// ${logger} - The logger name.
            /// </summary>
            public const string logger = "${logger}";   //
            /// <summary>
            /// ${longdate} - The date and time in a long, sortable format yyyy-MM-dd HH:mm:ss.ffff.
            /// </summary>
            public const string longdate = "${longdate}";  //
            /// <summary>
            /// ${machinename} - The machine name that the process is running on.
            /// </summary>
            public const string machinename = "${machinename}"; // 
            /// <summary>
            /// ${mdc} - Mapped Diagnostics Context - a thread-local structure.
            /// </summary>
            public const string mdc = "${mdc}";  //
            /// <summary>
            /// ${mdlc} - Async Mapped Diagnostics Context - a thread-local structure.
            /// </summary>
            public const string mdlc = "${mdlc}";   //
            /// <summary>
            /// ${message} - The formatted log message.
            /// </summary>
            public const string message = "${message}";   //
            /// <summary>
            /// ${ndc} - Nested Diagnostics Context - a thread-local structure.
            /// </summary>
            public const string ndc = "${ndc}";  //
            /// <summary>
            /// ${newline} - A newline literal.
            /// </summary>
            public const string newline = "${newline}";   //
            /// <summary>
            /// ${nlogdir} - The directory where NLog.dll is located.
            /// </summary>
            public const string nlogdir = "${nlogdir}";   //
            /// <summary>
            /// ${performancecounter} - The performance counter.
            /// </summary>
            public const string performancecounter = "${performancecounter}";   //
            /// <summary>
            /// ${processid} - The identifier of the current process.
            /// </summary>
            public const string processid = "${processid}";   //
            /// <summary>
            /// ${processinfo} - The information about the running process.
            /// </summary>
            public const string processinfo = "${processinfo}";    //
            /// <summary>
            /// ${processname} - The name of the current process.
            /// </summary>
            public const string processname = "${processname}";   //
            /// <summary>
            /// ${processtime} - The process time in format HH:mm:ss.mmm.
            /// </summary>
            public const string processtime = "${processtime}";   //
            /// <summary>
            /// ${qpc} - High precision timer, based on the value returned from QueryPerformanceCounter() optionally converted to seconds.
            /// </summary>
            public const string qpc = "${qpc}";   //
            /// <summary>
            /// ${registry} - A value from the Registry.
            /// </summary>
            public const string registry = "${registry}";   //

            /// <summary>
            /// ${shortdate} - The short date in a sortable format yyyy-MM-dd.
            /// </summary>
            public const string shortdate = "${shortdate}";    //
            /// <summary>
            /// ${sl-appinfo} - Information about Silverlight application.
            /// </summary>
            public const string sl_appinfo = "${sl-appinfo}";   //
            /// <summary>
            /// ${specialfolder} - System special folder path(includes My Documents, My Music, Program Files, Desktop, and more).
            /// </summary>
            public const string specialfolder = "${specialfolder}";    //
            /// <summary>
            /// ${stacktrace} - Stack trace renderer.
            /// </summary>
            public const string stacktrace = "${stacktrace}";    //
            /// <summary>
            ///  ${tempdir} - A temporary directory.
            /// </summary>
            public const string tempdir = "${tempdir}";      //
            /// <summary>
            /// ${threadid} - The identifier of the current thread.
            /// </summary>
            public const string threadid = "${threadid}";  // 
            /// <summary>
            /// ${threadname} - The name of the current thread.
            /// </summary>
            public const string threadname = "${threadname}";    //
            /// <summary>
            /// ${ticks} - The Ticks value of current date and time.
            /// </summary>
            public const string ticks = "${ticks}";     //
            /// <summary>
            /// ${time} - The time in a 24-hour, sortable format HH:mm:ss.mmm.
            /// </summary>
            public const string time = "${time}";  //
            /// <summary>
            /// ${var} - Render variable(new in 4.1)
            /// </summary>
            public const string var = "${var}";   //
            /// <summary>
            /// ${windows-identity} - Thread Windows identity information(username).
            /// </summary>
            public const string windows_identity = "${windows-identity}";    //
                


        }


        #endregion ---------------------------- NLOG TEMPLATE KEYS ----------------------------


        #region ------------------------- DCLOGGERS && LOGGINGCONFIGURATION(NLOG) ---------------------------------

        static readonly LoggingConfiguration CommonConfiguration = new LoggingConfiguration();
        static Dictionary<string, DCLogger> LoggersDict
        { get; } = new Dictionary<string, DCLogger>();

        /// <summary>
        /// Get DCLogger if Logger already exist with such key, or method returns null.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public  static  DCLogger GetLogger(string key)
        {
           if (LoggersDict.NotContainsKey(key)) return null;         
           return LoggersDict[key];
        } 

        #endregion ------------------------- DCLOGGERS && LOGGINGCONFIGURATION(NLOG) ---------------------------------


        #region --------------------------- PROPERTIES ---------------------------------

        /// <summary>
        ///  Key of current DCLogger. 
        /// </summary>
        public string  Key     { get; private set; }

        /// <summary>
        /// If this DCLogger configure it's LoggerLog with NLog and ready to write Log messages. 
        /// </summary>
        public bool IsConfigured     { get; private set; }


        /// <summary>
        /// If we need to add an exception's stack trace to each Logging Message.
        /// </summary>
        public bool IncludeExceptionStackTrace     { get; private set; }

        /// <summary>
        /// Dynamic Command's Diagnostic Parameter Keys. We are going to attach to Log Message theese Param Values.
        /// </summary>
        public List<string> DCDiagnosticKeys
        { get; } = new List<string>() { };
        

        /// <summary>
        /// Logging Type of current DCLogger : File Logging, Windows Events Logging, ColoredConsole Logging.
        /// </summary>
        public DCLoggingEn LogLogging     { get; private set; }
        

        /// <summary>
        /// The NLog.Logger that will write messages immediately to target.
        /// </summary>
        public Logger LogLogger { get; set; }// = LogManager.GetLogger("File1Logger");

        /// <summary>
        /// Logging Target - To File, To EventLog, To ColoredConsole.
        /// </summary>
        public TargetWithLayout LogTarget { get; set; } // = new FileTarget("File1Target");

        /// <summary>
        /// Logging Rule - object that bind  LogLogger and Logging Target 
        /// </summary>
        public LoggingRule LogRule { get; set; }  //= new LoggingRule("File1Logger", LogLevel.Debug, File1Target);


        /// <summary>
        /// Log Message Layout. Also you can change this layout here.
        /// </summary>
        public Layout MessageLayout
        {
            get
            {
                if (LogTarget == null) return null;
                else return LogTarget.Layout;
            }
            set
            {
                if (value != null) LogTarget.Layout = value;
            }
        }






        #endregion --------------------------- PROPERTIES ---------------------------------


        /// <summary>
        /// Building message with DCMessage.Parameters values - for each key registered in DCDiagnosticKeys. 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="inputcommand"></param>
        /// <param name="FaultExc"></param>
        /// <returns></returns>
        protected virtual string BuildLogMessageWithDiagKeys(string context, IDCMessage inputcommand, Exception FaultExc)
        {
            StringBuilder errorMessageBldr = new StringBuilder(D);

            errorMessageBldr.Append("Context:[{0}]".Fmt(context));
            // "TargetDCManager" , "Command"
            errorMessageBldr.Append("[{0}]:[{1}]{2}".Fmt("TargetDCManager", inputcommand.DCManager,D));
            errorMessageBldr.Append("[{0}]:[{1}]{2}".Fmt("Command", inputcommand.Command, D));


            for (int i = 0; i < DCDiagnosticKeys.Count; i++)
            {
                var diagParam = inputcommand.GetParam(DCDiagnosticKeys[i]);
                     errorMessageBldr.Append("[{0}]:[{1}]{2}".Fmt(DCDiagnosticKeys[i], diagParam.Value.S(), D));                
            }
             
            errorMessageBldr.Append("Exception.Message : [{0}]".Fmt( FaultExc.Message) ) ;

            if (IncludeExceptionStackTrace && FaultExc.StackTrace != null)
            {
                errorMessageBldr.Append("Exception.StackTrace : [{0}]".Fmt(FaultExc.StackTrace));
            }

            return errorMessageBldr.S();

        }


        #region ----------------------- Empty DCLOGGER -------------------------------

        /// <summary>
        /// Empty DCLogger has IsConfigured = false.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DCLogger CreateEmptyLogger(string key)
        {
            var dclogger = new DCLogger();
            dclogger.Key = key;
                   
            dclogger.IsConfigured = false;
             
            return dclogger;
        }


        /// <summary>
        /// Add new Empty DCLogger if there is no item with such key in internal Loggers Dictionary, or use existed DCLogger with such key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DCLogger AddOrUseExist_EmptyLogger(string key)
        {
            if (LoggersDict.ContainsKey(key)) return LoggersDict[key];

            LoggersDict.Add(key, CreateEmptyLogger(key));            
            return LoggersDict[key];
        }


        /// <summary>
        /// Add new Empty DCLogger if it there is no item with such key in internal Loggers Dictionary, but if item alredy exist then it'll be replaced by newly created item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DCLogger AddUpdate_EmptyLogger(string key)
        {
            if (LoggersDict.ContainsKey(key))
            {
                LoggersDict[key] = CreateEmptyLogger(key);
            }
            else
            {
                LoggersDict.Add(key, CreateEmptyLogger(key));
            }
            return LoggersDict[key];            
        }


       

        #endregion ----------------------- Empty DCLOGGER -----------------------------


        #region  -------------------------------- FILE  LOGGING --------------------------------

        /// <summary>
        /// File Log FileName
        /// </summary>
        public Layout FL_FileName
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.FileLogging) return "incorrectFilename.txt";
                else return (LogTarget as FileTarget).FileName;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.FileLogging) return;
                if (value != null) (LogTarget as FileTarget).FileName = value;
            }
        }



        /// <summary>
        /// Create File oriented DCLogger 
        /// </summary>
        /// <param name="ownerKey"></param>
        /// <param name="targetFilePath"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        /// <returns></returns>
        public static DCLogger CreateFileLogger(string key
                                               , string targetFilePath
                                               , LogLevel loglevel
                                               , Layout logMessageLayout)
        {
            var dclogger = new DCLogger();
            dclogger.Key = key;
            string logLoggerName = dclogger.Key + ".Logger";
            string logTargetName = dclogger.Key + ".Target";

            dclogger.LogLogger = LogManager.GetLogger(logLoggerName);

            dclogger.LogTarget = new FileTarget(logTargetName);
            (dclogger.LogTarget as FileTarget).FileName = targetFilePath; //"${basedir}/../../Logs/file1.log";
            (dclogger.LogTarget as FileTarget).Layout = logMessageLayout;
            CommonConfiguration.AddTarget(logTargetName, dclogger.LogTarget);

            dclogger.LogRule = new LoggingRule(logLoggerName, loglevel, dclogger.LogTarget);
            CommonConfiguration.LoggingRules.Add(dclogger.LogRule);

            LogManager.ReconfigExistingLoggers();
            dclogger.IsConfigured = true;
            return dclogger;
        }
        
        
        /// <summary>
        /// Init DCLogger if it was not Configured - IsConfigured = false. If already configured then nothing will be done.
        /// </summary>
        /// <param name="targetFilePath"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        public void InitAsFileLogger( string targetFilePath
                                               , LogLevel loglevel
                                               , Layout logMessageLayout)
        {
            if (IsConfigured == false)
            {
                string logLoggerName = Key + ".Logger";
                string logTargetName = Key + ".Target";

                LogLogger = LogManager.GetLogger(logLoggerName);

                LogTarget = new FileTarget(logTargetName);
                (LogTarget as FileTarget).FileName = targetFilePath; //"${basedir}/../../Logs/file1.log";
                (LogTarget as FileTarget).Layout = logMessageLayout;
                CommonConfiguration.AddTarget(logTargetName, LogTarget);

                LogRule = new LoggingRule(logLoggerName, loglevel, LogTarget);
                CommonConfiguration.LoggingRules.Add(LogRule);

                LogManager.ReconfigExistingLoggers();
                IsConfigured = true;

            }
        }




        /// <summary>
        /// Add new File DCLogger if there is no item with such key in internal Loggers Dictionary, or use existed DCLogger with such key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DCLogger AddOrUseExist_FileLogger(string key
                                               , string targetFilePath
                                               , LogLevel loglevel
                                               , Layout logMessageLayout)
        {
            if (LoggersDict.ContainsKey(key)) return LoggersDict[key];

            LoggersDict.Add(key, CreateFileLogger(key,targetFilePath, loglevel, logMessageLayout) );
            return LoggersDict[key];
        }


        /// <summary>
        /// Add new File DCLogger if it there is no item with such key in internal Loggers Dictionary, but if item alredy exist then it'll be replaced by newly created item.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static DCLogger AddUpdate_FileLogger(string key
                                               , string targetFilePath
                                               , LogLevel loglevel
                                               , Layout logMessageLayout)
        {
            if (LoggersDict.ContainsKey(key))
            {
                LoggersDict[key] = CreateFileLogger(key, targetFilePath, loglevel, logMessageLayout);
            }
            else
            {
                LoggersDict.Add(key, CreateFileLogger(key, targetFilePath, loglevel, logMessageLayout) );
            }
            return LoggersDict[key];
        }




        #endregion-------------------------------- FILE  LOGGING --------------------------------


        #region ------------------------ WIN EVENTS LOGGING -----------------------------------

#if SERVER || WPF
        /// <summary>
        /// Event Log  OnOverflowAction
        /// </summary>
        public EventLogTargetOverflowAction EL_OnOverflowAction
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return EventLogTargetOverflowAction.Split;
                else return (LogTarget as EventLogTarget).OnOverflow;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;
                if (value == EventLogTargetOverflowAction.Discard) (LogTarget as EventLogTarget).OnOverflow = value;
            }
        }


        /// <summary>
        /// Event Log  Machine
        /// </summary>
        public string EL_MachineName
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return ".";
                else return (LogTarget as EventLogTarget).MachineName;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;

                if (!string.IsNullOrEmpty(value)) (LogTarget as EventLogTarget).MachineName = value;
            }
        }

        /// <summary>
        /// Event Log MaxMessageLength
        /// </summary>
        public int EL_MaxMessageLength
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return 16384;
                else return (LogTarget as EventLogTarget).MaxMessageLength;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;

                if (value > 0) (LogTarget as EventLogTarget).MaxMessageLength = value;
            }
        }


        /// <summary>
        /// Event Log  Source.  Also you can change this layout here.
        /// </summary>
        public Layout EL_Source
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return ".";
                else return (LogTarget as EventLogTarget).Source;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;

                if (value != null) (LogTarget as EventLogTarget).Source = value;
            }
        }


        /// <summary>
        /// Event Log Category. Also you can change this layout here.
        /// </summary>
        public Layout EL_Category
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return null;
                else return (LogTarget as EventLogTarget).Category;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;

                if (value != null) (LogTarget as EventLogTarget).Category = value;
            }
        }


        /// <summary>
        ///  Event Log EntryType. Also you can change this layout here.
        /// </summary>
        public Layout EL_EntryType
        {
            get
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return null;
                else return (LogTarget as EventLogTarget).EntryType;
            }
            set
            {
                if (LogTarget == null || LogLogging != DCLoggingEn.WinEventsLogging) return;

                if (value != null) (LogTarget as EventLogTarget).EntryType = value;
            }
        }


        /// <summary>
        /// Create Windows Events oriented DCLogger
        /// </summary>
        /// <param name="key"></param>
        /// <param name="eventLogPoint"></param>
        /// <param name="machineName"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        /// <param name="sourceLayout"></param>
        /// <param name="entryType"></param>
        /// <param name="categoryLayout"></param>
        /// <param name="overflowAction"></param>
        /// <param name="maxMessageLength"></param>
        /// <returns></returns>
        public static DCLogger CreateWinEventLogger(string key
                                                , string eventLogPoint, string machineName
                                                , LogLevel loglevel
                                                , Layout logMessageLayout, Layout sourceLayout, Layout entryType, Layout categoryLayout
                                                , EventLogTargetOverflowAction overflowAction = EventLogTargetOverflowAction.Split
                                                , int maxMessageLength = 16384)
        {


            var dclogger = new DCLogger();
            dclogger.Key = key;
            string logLoggerName = dclogger.Key + ".Logger";
            string logTargetName = dclogger.Key + ".Target";

            dclogger.LogLogger = LogManager.GetLogger(logLoggerName);

            dclogger.LogTarget = new EventLogTarget(logTargetName);
            (dclogger.LogTarget as EventLogTarget).Log = eventLogPoint;// "Application";
            (dclogger.LogTarget as EventLogTarget).Source = sourceLayout;// "EventLogLoggerTestApp";
            (dclogger.LogTarget as EventLogTarget).MachineName = machineName;// ".";

            dclogger.LogTarget.Layout = logMessageLayout;// new SimpleLayout("${message}");
            if (entryType != null) (dclogger.LogTarget as EventLogTarget).EntryType = entryType;
            if (categoryLayout != null) (dclogger.LogTarget as EventLogTarget).Category = categoryLayout;// entryType;

            (dclogger.LogTarget as EventLogTarget).OnOverflow = overflowAction;
            (dclogger.LogTarget as EventLogTarget).MaxMessageLength = maxMessageLength;
            CommonConfiguration.AddTarget(dclogger.LogTarget);

            dclogger.LogRule = new LoggingRule(nameof(EventLogTarget), loglevel, dclogger.LogTarget);
            CommonConfiguration.LoggingRules.Add(dclogger.LogRule);

            LogManager.ReconfigExistingLoggers();
            dclogger.IsConfigured = true;
            return dclogger;
        }



        /// <summary>
        /// Init Windows Events oriented DCLogger 
        /// </summary>
        /// <param name="eventLogPoint"></param>
        /// <param name="machineName"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        /// <param name="sourceLayout"></param>
        /// <param name="entryType"></param>
        /// <param name="categoryLayout"></param>
        /// <param name="overflowAction"></param>
        /// <param name="maxMessageLength"></param>
        public void InitAsWinEventLogger(  string eventLogPoint, string machineName
                                           , LogLevel loglevel
                                           , Layout logMessageLayout, Layout sourceLayout, Layout entryType, Layout categoryLayout
                                           , EventLogTargetOverflowAction overflowAction = EventLogTargetOverflowAction.Split
                                           , int maxMessageLength = 16384)
        {
            if (IsConfigured == false)
            {
                string logLoggerName =  Key + ".Logger";
                string logTargetName =  Key + ".Target";

                 LogLogger = LogManager.GetLogger(logLoggerName);

                LogTarget = new EventLogTarget(logTargetName);
                ( LogTarget as EventLogTarget).Log = eventLogPoint;// "Application";
                ( LogTarget as EventLogTarget).Source = sourceLayout;// "EventLogLoggerTestApp";
                ( LogTarget as EventLogTarget).MachineName = machineName;// ".";

                 LogTarget.Layout = logMessageLayout;// new SimpleLayout("${message}");
                if (entryType != null) ( LogTarget as EventLogTarget).EntryType = entryType;
                if (categoryLayout != null) ( LogTarget as EventLogTarget).Category = categoryLayout;// entryType;

                ( LogTarget as EventLogTarget).OnOverflow = overflowAction;
                ( LogTarget as EventLogTarget).MaxMessageLength = maxMessageLength;
                CommonConfiguration.AddTarget( LogTarget);

                 LogRule = new LoggingRule(nameof(EventLogTarget), loglevel,  LogTarget);
                CommonConfiguration.LoggingRules.Add(LogRule);

                LogManager.ReconfigExistingLoggers();
                IsConfigured = true;                
            }         
          
        }




        /// <summary>
        /// Add new WinEventLogger DCLogger if there is no item with such key in internal Loggers Dictionary, or use existed DCLogger with such key.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="eventLogPoint"></param>
        /// <param name="machineName"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        /// <param name="sourceLayout"></param>
        /// <param name="entryType"></param>
        /// <param name="categoryLayout"></param>
        /// <param name="overflowAction"></param>
        /// <param name="maxMessageLength"></param>
        /// <returns></returns>
        public static DCLogger AddOrUseExist_WinEventLogger(string key
                                                , string eventLogPoint, string machineName
                                                , LogLevel loglevel
                                                , Layout logMessageLayout, Layout sourceLayout, Layout entryType, Layout categoryLayout
                                                , EventLogTargetOverflowAction overflowAction = EventLogTargetOverflowAction.Split
                                                , int maxMessageLength = 16384)
        {
            if (LoggersDict.ContainsKey(key)) return LoggersDict[key];

            LoggersDict.Add(key, CreateWinEventLogger(key, eventLogPoint,machineName, loglevel, logMessageLayout , sourceLayout, entryType, categoryLayout, overflowAction, maxMessageLength));
            return LoggersDict[key];
        }



        /// <summary>
        /// Add new WinEventLogger DCLogger if it there is no item with such key in internal Loggers Dictionary, but if item alredy exist then it'll be replaced by newly created item.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="eventLogPoint"></param>
        /// <param name="machineName"></param>
        /// <param name="loglevel"></param>
        /// <param name="logMessageLayout"></param>
        /// <param name="sourceLayout"></param>
        /// <param name="entryType"></param>
        /// <param name="categoryLayout"></param>
        /// <param name="overflowAction"></param>
        /// <param name="maxMessageLength"></param>
        /// <returns></returns>
        public static DCLogger AddUpdate_WinEventLogger(
                                            string key
                                           , string eventLogPoint, string machineName
                                           , LogLevel loglevel
                                           , Layout logMessageLayout, Layout sourceLayout, Layout entryType, Layout categoryLayout
                                           , EventLogTargetOverflowAction overflowAction = EventLogTargetOverflowAction.Split
                                           , int maxMessageLength = 16384)
        {
            if (LoggersDict.ContainsKey(key))
            {
                LoggersDict[key] = CreateWinEventLogger(key, eventLogPoint, machineName, loglevel, logMessageLayout, sourceLayout, entryType, categoryLayout, overflowAction, maxMessageLength);
            }
            else
            {
                LoggersDict.Add(key, CreateWinEventLogger(key, eventLogPoint, machineName, loglevel, logMessageLayout, sourceLayout, entryType, categoryLayout, overflowAction, maxMessageLength) );
            }
            return LoggersDict[key];
        }



#endif
        #endregion ------------------------ WIN EVENTS LOGGING -----------------------------------

        //  NLogViewerTarget can be good,
        //  DatabaseTarget also can good





        #region ------------------------- SHORT MESSAGE LOGGING CALLS --------------------------------

        /// <summary>
        /// Info message to Log.
        /// </summary>
        /// <param name="message"></param>
        public void Info(string message)
        {
            if (LogLogger.IsNull()) return;
            
            LogLogger.Info(message);
        }

        /// <summary>
        /// Info message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void InfoDbg(string message)
        {
            if (IsConfigured == false)  return;
            LogLogger.Info(message);
        }

        [Conditional("DEBUG")]
        public void InfoDbg(string context,IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Info(BuildLogMessageWithDiagKeys(context,inputComand, FaultExc));
        }


        /// <summary>
        /// Warn message to Log .
        /// </summary>
        /// <param name="message"></param>
        public void Warn(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Warn(message);
        }

        /// <summary>
        /// Warn message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void WarnDbg(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Warn(message);
        }

        [Conditional("DEBUG")]
        public void WarnDbg(string context, IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Warn(BuildLogMessageWithDiagKeys(context, inputComand, FaultExc));
        }

        /// <summary>
        /// Trace message to Log.
        /// </summary>
        /// <param name="message"></param>
        public void Trace(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Trace(message);
        }

        /// <summary>
        /// Trace message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void TraceDbg(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Trace(message);
        }


        [Conditional("DEBUG")]
        public void TraceDbg(string context, IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Trace(BuildLogMessageWithDiagKeys(context, inputComand, FaultExc));
        }



        /// <summary>
        /// Debug message to Log.
        /// </summary>
        /// <param name="message"></param>
        public void Debug(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Debug(message);
        }

        /// <summary>
        /// Debug message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void DebugDbg(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Debug(message);
        }

        [Conditional("DEBUG")]
        public void DebugDbg(string context, IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Debug(BuildLogMessageWithDiagKeys(context, inputComand, FaultExc));
        }



        /// <summary>
        /// Error message to Log.
        /// </summary>
        /// <param name="message"></param>
        public void Error(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Error(message);
        }

        /// <summary>
        /// Error message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void ErrorDbg(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Error(message);
        }

        [Conditional("DEBUG")]
        public void ErrorDbg(string context, IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Error(BuildLogMessageWithDiagKeys(context, inputComand, FaultExc));
        }

        /// <summary>
        /// Fatal message to Log .
        /// </summary>
        /// <param name="message"></param>
        public void Fatal(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Fatal(message);
        }

        /// <summary>
        /// Fatal message to Log only in DEBUG mode.
        /// </summary>
        /// <param name="message"></param>
        [Conditional("DEBUG")]
        public void FatalDbg(string message)
        {
            if (IsConfigured == false) return;
            LogLogger.Fatal(message);
        }

        [Conditional("DEBUG")]
        public void FatalDbg(string context, IDCMessage inputComand, Exception FaultExc)
        {
            if (IsConfigured == false) return;
            LogLogger.Error(BuildLogMessageWithDiagKeys(context, inputComand, FaultExc));
        }


        #endregion ------------------------- SHORT MESSAGE LOGGING CALLS --------------------------------


    }
}



 