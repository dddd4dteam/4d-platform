﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace DDDD.Core.Converters
{



    internal class WildcardToRegexConverter
    {

        #region ---------------------------- CTOR -----------------------------


        private WildcardToRegexConverter(string wildcard)
        {
            this._wildcard = wildcard;
            this._regex = new StringBuilder(wildcard.Length);
        }


        #endregion ---------------------------- CTOR -----------------------------


        private enum ParseState
        {
            Initial,
            Bracket
        }
                

        private const string REGEX_SPECIAL_CHARS = "[]{}()\\.+*?^$";

        private string _wildcard;

        private StringBuilder _regex;

        private string _error;

        private bool _nonGreedy;


        public static Regex Convert(string wildcard, bool ignoreCase, bool matchWholeText = true)
        {
            string str;
            RegexOptions regexOption;
            Exception exception;
            WildcardToRegexConverter.Convert(wildcard, out str, out regexOption, out exception, matchWholeText);
            if (exception != null)
            {
                return null;
            }
            if (ignoreCase)
            {
                regexOption = regexOption | RegexOptions.IgnoreCase;
            }
            return new Regex(str, regexOption);
        }

        public static void Convert(string wildcard, out string regex, out RegexOptions options, out Exception error, bool matchWholeText = true)
        {
            if (wildcard == null || wildcard.Length == 0)
            {
                options = RegexOptions.None;
                regex = null;
                error = new Exception("ERROR: Filter_WildCard_Empty "); //   -  [12/14/2016 A1]
                return;
            }
             WildcardToRegexConverter wildcardToRegexConverter = new  WildcardToRegexConverter(wildcard)
            {
                _nonGreedy = !matchWholeText
            };
            wildcardToRegexConverter.Parse();
            if (wildcardToRegexConverter._error != null || wildcardToRegexConverter._regex.Length <= 0)
            {
                error = new Exception(wildcardToRegexConverter._error);
                options = RegexOptions.None;
                regex = null;
                return;
            }
            regex = wildcardToRegexConverter._regex.ToString();
            if (matchWholeText)
            {
                regex = string.Concat("^", regex, "$");
            }
            options = RegexOptions.ExplicitCapture | RegexOptions.Singleline;
            error = null;
        }

        private void Parse()
        {
             ParseState parseState =   ParseState.Initial;
            int num = -1;
            StringBuilder stringBuilder = this._regex;
            string str = this._wildcard;
            for (int i = 0; i < str.Length; i++)
            {
                char chr = str[i];
                if ( ParseState.Bracket == parseState && 93 == chr)
                {
                    this.ParseSet(1 + num, i - 1);
                    parseState =  ParseState.Initial;
                }
                else if (parseState == ParseState.Initial && 91 == chr)
                {
                    num = i;
                    parseState =  ParseState.Bracket;
                }
                else if (parseState ==  ParseState.Initial && 35 == chr)
                {
                    stringBuilder.Append("\\d");
                }
                else if (parseState ==  ParseState.Initial && 63 == chr)
                {
                    stringBuilder.Append('.');
                }
                else if (parseState ==  ParseState.Initial && 42 == chr)
                {
                    stringBuilder.Append(".*");
                    if (this._nonGreedy)
                    {
                        stringBuilder.Append('?');
                    }
                }
                else if (parseState ==  ParseState.Initial)
                {
                    if ("[]{}()\\.+*?^$".IndexOf(chr) >= 0)
                    {
                        stringBuilder.Append('\\');
                    }
                    stringBuilder.Append(chr);
                }
            }
        }

        private void ParseSet(int si, int ei)
        {
            StringBuilder stringBuilder = this._regex;
            string str = this._wildcard;
            int length = stringBuilder.Length;
            stringBuilder.Append('[');
            if (33 == str[si])
            {
                if (si == ei)
                {
                    this._error = " ERROR : Filter_WildCard_InvalidCharClass "; //     - [12/14/2016 A1]
                    return;
                }
                stringBuilder.Append('\u005E');
                si++;
            }
            for (int i = si; i <= ei; i++)
            {
                char chr = str[i];
                if ("[]{}()\\.+*?^$".IndexOf(chr) >= 0)
                {
                    stringBuilder.Append('\\');
                }
                stringBuilder.Append(chr);
            }
            stringBuilder.Append(']');
        }

    }
}
