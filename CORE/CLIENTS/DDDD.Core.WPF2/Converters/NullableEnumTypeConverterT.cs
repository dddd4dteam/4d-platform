﻿
using System;
using System.ComponentModel;
using System.Globalization;

namespace DDDD.Core.Converters
{
    /// <summary>
    /// Converts Nullable Enum types from strings to the specified enum.
    /// </summary>
    /// <typeparam name="T">The enum that the value should be converted to.</typeparam>
    public class NullableEnumTypeConverter<T> : TypeConverter
    {
        public NullableEnumTypeConverter()
        {
        }

        /// <summary>
        /// Determines whether this converter can convert an object of the specified Nullable enum.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="sourceType">The type that you want to convert from.</param>
        /// <returns>true if sourceType is a <see cref="T:System.String" /> or of the specified type.</returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string) || sourceType == typeof(T))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Converts the specified object to a Nullable enum.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo" /> to use as the current culture.</param>
        /// <param name="value">The object to covert to the Nullable enum type.</param>
        /// <returns></returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (Enum.IsDefined(typeof(T), value))
            {
                return Enum.Parse(typeof(T), value.ToString(), true);
            }
            string str = value as string;
            if (str != null)
            {
                int num = -1;
                if (int.TryParse(str, out num))
                {
                    return Enum.Parse(typeof(T), value.ToString(), true);
                }
            }
            return null;
        }
    }
}
