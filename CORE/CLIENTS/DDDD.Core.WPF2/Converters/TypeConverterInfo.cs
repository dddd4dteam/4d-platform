﻿using System.ComponentModel;

namespace DDDD.Core.Converters
{

    internal class TypeConverterInfo
    {
        internal TypeConverter convertFromTypeConverter;

        internal TypeConverter convertToTypeConverter;

        internal bool useConvertFromTypeConverterFirst;

        internal TypeConverterInfo(TypeConverter convertFromTypeConverter, TypeConverter convertToTypeConverter, bool useConvertFromTypeConverterFirst)
        {
            this.convertFromTypeConverter = convertFromTypeConverter;
            this.convertToTypeConverter = convertToTypeConverter;
            this.useConvertFromTypeConverterFirst = useConvertFromTypeConverterFirst;
        }
    }

}
