﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using DDDD.Core.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DDDD Core Library V2.x [ .NET4.5 Port ]")]
[assembly: AssemblyDescription("DDDD Core Library for   .NET4.5...")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("DDDD(Domain Driven Design Dialect/4D) Team")]
[assembly: AssemblyProduct("DDDD Core Library V2.x [ .NET4.5 Port ]")]
[assembly: AssemblyCopyright("Copyright © 2015 by DDDD(Domain Driven Design Dialect/4D) Team")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e291b6f6-214f-496a-9e71-6ce4d8ff83ed")]

// Default Resource manager for current assembly
[assembly: DefaultResourceManager(typeof(RCX))]


// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.0.1.0")]
[assembly: AssemblyFileVersion("1.0.1.0")]

[assembly: CLSCompliant(true)]

