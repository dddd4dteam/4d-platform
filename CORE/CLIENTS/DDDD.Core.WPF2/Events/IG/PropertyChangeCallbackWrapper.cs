﻿using System;
using System.Windows;

namespace DDDD.Core.Events.IG
{



    internal class PropertyChangeCallbackWrapper
    {

        #region ------------------- CTOR ----------------------- 

        internal PropertyChangeCallbackWrapper(PropertyChangedCallback callback)
        {
            this._changeCallback = callback;
        }

        static PropertyChangeCallbackWrapper()
        {
            UnsetValue = new object();
        }

        #endregion ------------------- CTOR ----------------------


        private readonly static object UnsetValue;


        private PropertyChangedCallback _changeCallback;

        [ThreadStatic]
        internal object TargetValue = UnsetValue;



        public void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!object.Equals(this.TargetValue, e.NewValue))
            {
                throw new InvalidOperationException();
            }
            if (this._changeCallback != null)
            {
                this._changeCallback(d, e);
            }
        }        

    }



}
