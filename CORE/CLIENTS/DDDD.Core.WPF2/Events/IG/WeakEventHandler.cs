﻿
using System;

namespace DDDD.Core.Events.IG
{
	/// <summary>
	/// Helper class for weak event handling.
	/// </summary>
	/// <typeparam name="TInstance"></typeparam>
	/// <typeparam name="TEventSource"></typeparam>
	/// <typeparam name="TEventArgs"></typeparam>
	/// <example>
	/// <code>
	/// <![CDATA[
	/// using System;
	/// using System.Collections;
	/// using System.Collections.Specialized;
	/// using System.ComponentModel;
	/// using Infragistics;
	/// namespace TestApp
	/// {
	///     public class TestClass
	///     {
	///         private IEnumerable _itemsSource;
	///         private WeakCollectionChangedHandler<TestClass> _weakCollectionChanged;
	///         private WeakEventHandler<TestClass, ICollectionView, EventArgs> _weakCollectionViewCurrentChanged;
	///         public IEnumerable ItemsSource
	///         {
	///             get { return _itemsSource; }
	///             set { this.SetItemsSource(value); }
	///         }
	///         private void SetItemsSource(IEnumerable value)
	///         {
	///             if (this._itemsSource == value)
	///             {
	///                 return;
	///             }
	///             if (this._weakCollectionChanged != null)
	///             {
	///                 this._weakCollectionChanged.Detach();
	///                 this._weakCollectionChanged = null;
	///             }
	///             if (this._weakCollectionViewCurrentChanged != null)
	///             {
	///                 this._weakCollectionViewCurrentChanged.Detach();
	///                 this._weakCollectionViewCurrentChanged = null;
	///             }
	///             this._itemsSource = value;
	///             INotifyCollectionChanged notifyCollectionChanged = value as INotifyCollectionChanged;
	///             if (notifyCollectionChanged != null)
	///             {
	///                 this._weakCollectionChanged =
	///                     new WeakCollectionChangedHandler<TestClass>
	///                         (
	///                             this,
	///                             notifyCollectionChanged,
	///                             (instance, s, e) => instance.ItemsSource_CollectionChanged(s, e)
	///                         );
	///                 notifyCollectionChanged.CollectionChanged += this._weakCollectionChanged.OnEvent;
	///             }
	///             ICollectionView collectionView = value as ICollectionView;
	///             if (collectionView != null)
	///             {
	///                 this._weakCollectionViewCurrentChanged =
	///                     new WeakEventHandler<TestClass, ICollectionView, EventArgs>
	///                         (
	///                             this,
	///                             collectionView,
	///                             (instance, s, e) => instance.ItemsSource_CurrentChanged(s, e),
	///                             (weakHandler, eventSource) => eventSource.CurrentChanged -= weakHandler.OnEvent
	///                         );
	///                 collectionView.CurrentChanged += this._weakCollectionViewCurrentChanged.OnEvent;
	///             }
	///         }
	///         private void ItemsSource_CurrentChanged(object sender, EventArgs e)
	///         {
	///             // ...
	///         }
	///         private void ItemsSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
	///         {
	///             // ...
	///         }
	///     }
	/// }
	/// ]]>
	/// </code>
	/// </example>
	internal class WeakEventHandler<TInstance, TEventSource, TEventArgs>
	where TInstance : class
	{
		private readonly WeakReference _weakInstance;

		private readonly WeakReference _weakEventSource;

		public TEventSource EventSource
		{
			get
			{
				return (TEventSource)GetWeakReferenceTargetSafe(this._weakEventSource);
			}
		}

		public TInstance Instance
		{
			get
			{
				return (TInstance)(GetWeakReferenceTargetSafe(this._weakInstance) as TInstance);
			}
		}

		/// <summary>
		/// Gets or sets the delegate that will be invoked when the event should be detached.
		/// </summary>
		/// <remarks>
		/// The delegate must not refer to an instance method.
		/// </remarks>
		private Action<WeakEventHandler<TInstance, TEventSource, TEventArgs>, TEventSource> OnDetachAction
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the delegate that will be invoked when the event is raised.
		/// </summary>
		/// <remarks>
		/// The delegate must not refer to an instance method.
		/// </remarks>
		private Action<TInstance, object, TEventArgs> OnEventAction
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:WeakEventHandler`3" /> class.
		/// </summary>
		/// <param name="instance">The short living object that wants to recieve events from the long living <paramref name="eventSource" /> object.</param>
		/// <param name="eventSource">The long living object that raises the event.</param>
		/// <param name="onEventAction">The delegate that will be invoked when the event is raised.</param>
		/// <param name="onDetachAction">The delegate that will be invoked when the event should be detached</param>
		/// <remarks>
		/// The delegates <paramref name="onEventAction" /> and <paramref name="onDetachAction" /> must not refer to instance methods.
		/// </remarks>
		public WeakEventHandler(TInstance instance, TEventSource eventSource, Action<TInstance, object, TEventArgs> onEventAction, Action<WeakEventHandler<TInstance, TEventSource, TEventArgs>, TEventSource> onDetachAction)
		{
			this._weakInstance = new WeakReference((object)instance);
			this._weakEventSource = new WeakReference((object)eventSource);
			this.OnEventAction = onEventAction;
			this.OnDetachAction = onDetachAction;
		}

		/// <summary>
		/// Invokes <see cref="P:WeakEventHandler`3.OnDetachAction" /> that handles the detaching of <see cref="M:WeakEventHandler`3.OnEvent(System.Object,`2)" /> from the event.
		/// </summary>
		public void Detach()
		{
			TEventSource weakReferenceTargetSafe = (TEventSource)GetWeakReferenceTargetSafe(this._weakEventSource);
			if (this.OnDetachAction != null && this._weakEventSource.IsAlive)
			{
				this.OnDetachAction(this, weakReferenceTargetSafe);
			}
			this.OnDetachAction = null;
		}

		/// <summary>
		/// Handler for the event raised by the long living object.
		/// </summary>
		/// <param name="source">The source of the event.</param>
		/// <param name="eventArgs">The <typeparamref name="TEventArgs" /> instance containing the event data.</param>
		public void OnEvent(object source, TEventArgs eventArgs)
		{
			TInstance weakReferenceTargetSafe = (TInstance)(GetWeakReferenceTargetSafe(this._weakInstance) as TInstance);
			if (weakReferenceTargetSafe == null)
			{
				this.Detach();
			}
			else if (this.OnEventAction != null)
			{
				this.OnEventAction(weakReferenceTargetSafe, source, eventArgs);
				return;
			}
		}


		/// <summary>
		/// Wraps the 'get' of the Target property in a try/catch to prevent unhandled exceptions
		/// </summary>
		/// <param name="weakReference">The WeakRefernce holding the target.</param>
		/// <returns>The Target or null if an exception was thrown.</returns>
		public static object GetWeakReferenceTargetSafe(WeakReference weakReference)
		{
			object target;
			if (weakReference != null)
			{
				try
				{

					target = weakReference.Target;
				}
				catch (Exception exception)
				{
					return null;
				}
				return target;
			}
			return null;
		}






	}
}
