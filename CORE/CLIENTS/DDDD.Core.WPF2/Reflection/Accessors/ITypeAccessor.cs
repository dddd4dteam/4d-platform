﻿
using System;
using System.Collections.Generic;
using System.Reflection;

using DDDD.Core.Collections;

namespace DDDD.Core.Reflection
{


    /// <summary>
    /// Type Accessor needs to collect and set member values.
    ///<para/> We can choose with what members we work by TypeAccessor.SelectMembers methods.
    /// </summary>
    public interface ITypeAccessor// no <T>
    {

        /// <summary>
        /// TypeInfoEx about this TAccess Type 
        /// </summary>
        TypeInfoEx TAccessEx { get; }

        /// <summary>
        /// Target TypeAccessor Type
        /// </summary>
        Type TAccess { get; }


        /// <summary>
        /// Internals Members Dictionary
        /// </summary>
        Dictionary<string, TypeMember > Members { get; }

        /// <summary>
        /// Internal Fields
        /// </summary>
        List<KeyValuePair<string,  TypeMember>> Fields { get; }

        /// <summary>
        /// Internal Properties
        /// </summary>
        List<KeyValuePair<string, TypeMember>> Properties { get; }
               


        //void AddMemberGetDelegate(MemberInfo member);        
        //void AddMemberSetDelegate(MemberInfo member);
        
       
        /// <summary>
        /// Collect member Types.
        /// </summary>
        /// <returns></returns>
        Type[] CollectMemberTypes();

        //int GetMemberIndex(string memberName);
        //bool GetMemberWritable(string memberName);

        /// <summary>
        /// Get member info.
        /// </summary>
        /// <param name="memberName"></param>
        /// <returns></returns>
        TypeMember GetMemberInfo(string memberName);

        /// <summary>
        /// Get value of [instance] member.
        /// </summary>
        /// <typeparam name="TMember"></typeparam>
        /// <param name="memberKey"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        TMember GetMember<TMember>(string memberKey, object instance);

        /// <summary>
        /// Get value of [instance] member .
        /// </summary>
        /// <param name="memberKey"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        object GetMember(string memberKey, object instance);

        /// <summary>
        /// Set [instance] member value.
        /// </summary>
        /// <typeparam name="TMember"></typeparam>
        /// <param name="memberKey"></param>
        /// <param name="instance"></param>
        /// <param name="memberNewValue"></param>
        void SetMember<TMember>(string memberKey, ref object instance, TMember memberNewValue);

    }


}
 