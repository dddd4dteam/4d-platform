﻿using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Reflection 
{
    public abstract class TypeAccessor : ITypeAccessor
    {

        static readonly Type currentType = typeof(TypeAccessor);

        /// <summary>
        /// Target Detailed Type Info of this Accessor
        /// </summary>
        public TypeInfoEx TAccessEx
        { get; protected set; }

        /// <summary>
        /// Target Type of this Accessor   
        /// </summary>        
        public Type TAccess
        { get; protected set; }


        /// <summary>
        /// Internals Members Dictionary
        /// </summary>
        [IgnoreMember]
        public Dictionary<string, TypeMember> Members
        { get; protected set; } = new Dictionary<string, TypeMember>();


        [IgnoreMember]
        List<KeyValuePair<string, TypeMember>> fields = null;

        /// <summary>
		/// Fields of current (TAccess)-Type that we get  from ITypeSerializer
		/// </summary>
		[IgnoreMember]
        public List<KeyValuePair<string, TypeMember>> Fields
        {
            get
            {   if (fields== null)
                {                    
                    fields = Members.Where(mm => mm.Value.MemberInf.MemberType == MemberTypes.Field)
                    .ToList();

                    if (fields == null) fields = new List<KeyValuePair<string, TypeMember>>();
                   
                }
                return fields;
            }
        }



        [IgnoreMember]
        List<KeyValuePair<string, TypeMember>> properties = null;


        /// <summary>
        /// Properties of current (TAccess)-Type that we get from ITypeSerializer
        /// </summary>  
        [IgnoreMember]
        public List<KeyValuePair<string, TypeMember>> Properties
        {
            get
            {
                if (properties == null)
                {
                    properties = Members.Where(mm => mm.Value.MemberInf.MemberType == MemberTypes.Property)
                        .ToList();

                    if (properties == null) properties = new List<KeyValuePair<string, TypeMember>>();

                }
                return properties;
            } 
        }
               
       
        /// <summary>
        /// Collect Member Types
        /// </summary>
        /// <returns></returns>
        public Type[] CollectMemberTypes()
        {
            List<Type> resultMemberTypes = new List<Type>();
            foreach (var member in Members)
            {
                resultMemberTypes.Add(member.Value.TypeInf.OriginalType);
            }

            return resultMemberTypes.ToArray();
        }

        /// <summary>
        /// Get  MemberInfo by memberName of {T} in current accessor. If  memberName don't exist then method return null  value.
        /// </summary>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public TypeMember GetMemberInfo(string memberName)
        {
            if (Members.ContainsKey(memberName))
            {
                return Members[memberName];
            }
            return null;
        }
        

        public abstract void AddMemberGetDelegate(MemberInfo member);

        public abstract void AddMemberSetDelegate(MemberInfo member);




        public abstract TMember GetMember<TMember>(string memberKey, object instance);

        public abstract object GetMember(string memberKey, object instance);

        public abstract void SetMember<TMember>(string memberKey, ref object instance, TMember memberNewValue);



        #region ----------------------------- MEMBERS SELECTION -------------------------------


        public const BindingFlags DefaultMembersBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        public const BindingFlags DefaultMembersBindingNoBase = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;


        /// <summary>
        /// Get Binding by Type Members Selector
        /// </summary>
        /// <param name="membersSelector"></param>
        /// <returns></returns>
        public static BindingFlags GetSelectorBinding(TypeMemberSelectorEn membersSelector)
        {

            switch (membersSelector)
            {
                case TypeMemberSelectorEn.NotDefined:
                    {
                        return BindingFlags.Default;
                    }

                case TypeMemberSelectorEn.Default:
                    {
                        return (BindingFlags.Instance |
                                     BindingFlags.Public | BindingFlags.NonPublic);
                    }
                case TypeMemberSelectorEn.DefaultNoBase:
                    {
                        return (BindingFlags.Instance |
                                    BindingFlags.Public | BindingFlags.NonPublic |
                                    BindingFlags.DeclaredOnly);
                    }
                case TypeMemberSelectorEn.PublicOnly:
                    {
                        return (BindingFlags.Instance |
                                    BindingFlags.Public |
                                    BindingFlags.DeclaredOnly);
                    }
                case TypeMemberSelectorEn.OnlyMembers:
                    {
                        return (BindingFlags.Instance |
                                BindingFlags.Public | BindingFlags.NonPublic);
                    }
                case TypeMemberSelectorEn.Custom:
                    {
                        return (BindingFlags.Default);
                    }
                default:
                    {
                        return (BindingFlags.Instance |
                                     BindingFlags.Public | BindingFlags.NonPublic);
                    }
            }
        }



        /// <summary>
        /// Select Members of [selectableType] by selector nad other binding/ignore/only members  parameters. 
        /// </summary>
        /// <param name="selectableType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static List<string> SelectMembers(Type selectableType
                                , TypeMemberSelectorEn selector
                                , BindingFlags propertiesBinding = BindingFlags.Default
                                , BindingFlags fieldsBinding = BindingFlags.Default
                                , List<string> onlyMembers = null
                                , List<string> ignoreMembers = null)
        {

            var resultMembers = new List<string>();
            var typeinfo = TypeInfoEx.Get(selectableType);
            var targetType = typeinfo.WorkingType;

            var properties = new List<string>();
            var fields = new List<string>();


            if (typeinfo.IsICollection || targetType.IsInterface || targetType == Tps.T_object || targetType.Is4DPrimitiveType())
            {// not collecting members for this type kinds 

                resultMembers.AddRange(properties);
                resultMembers.AddRange(fields);
            }
            else if (selector == TypeMemberSelectorEn.NotDefined)
            {
                throw new InvalidOperationException($"TypeAcessor's members selector can't be undefined in TypeserializerT[{targetType.FullName}] ");
            }
            else if (selector == TypeMemberSelectorEn.Default)
            {
                properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
                                              .Select(prp => prp.Name));
                fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
                                              .Select(fld => fld.Name));

                // not use Ignore Members - only in Custom Selection 
                resultMembers.AddRange(properties);
                resultMembers.AddRange(fields);
            }
            else if (selector == TypeMemberSelectorEn.DefaultNoBase)
            {
                properties.AddRange(targetType.GetProperties(DefaultMembersBindingNoBase).Where(prp => prp.IsWritable().Value)
                                                .Select(prp => prp.Name));
                fields.AddRange(targetType.GetFields(DefaultMembersBindingNoBase).Where(fld => fld.IsWritable().Value)
                                                .Select(fld => fld.Name));

                // not use Ignore Members - only in Custom Selection
                resultMembers.AddRange(properties);
                resultMembers.AddRange(fields);
            }
            else if (selector == TypeMemberSelectorEn.PublicOnly)
            {
                properties.AddRange(targetType.GetProperties(BindingFlags.Public).Where(prp => prp.IsWritable().Value)
                                                .Select(prp => prp.Name));
                fields.AddRange(targetType.GetFields(BindingFlags.Public).Where(fld => fld.IsWritable().Value)
                                                .Select(fld => fld.Name));

                resultMembers.AddRange(properties);
                resultMembers.AddRange(fields);
            }
            else if (selector == TypeMemberSelectorEn.OnlyMembers)
            {
                //check all of elements exists
                properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
                                                .Select(prp => prp.Name));
                fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
                                                .Select(fld => fld.Name));

                //only member  should be more than 0
                if (onlyMembers == null || onlyMembers.Count == 0)
                { throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers, then only members property can't be null or with [0] Count value  "); }

                //check all of elements  exist in Properties or Fields
                foreach (var item in onlyMembers)
                {
                    if (properties.Contains(item) || fields.Contains(item))
                    {
                        throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers,  only member item [{item}]  was not found in Members of T[{targetType.Name}]");
                    }
                }

                resultMembers.AddRange(onlyMembers); // but will processing only members                
            }
            else if (selector == TypeMemberSelectorEn.Custom)
            {
                //only  custom binding should be as input parameter
                //ignore items
                properties.AddRange(targetType.GetProperties(propertiesBinding).Select(prp => prp.Name));
                fields.AddRange(targetType.GetFields(fieldsBinding).Select(fld => fld.Name));

                foreach (var ignoreItem in ignoreMembers)
                {
                    if (properties.Contains(ignoreItem)) { properties.Remove(ignoreItem); }
                    if (fields.Contains(ignoreItem)) { fields.Remove(ignoreItem); }
                }

                resultMembers.AddRange(properties);
                resultMembers.AddRange(fields);

            }

            //++ remove autofields <>k_BackingField
            if (resultMembers.Count > 0)
            {
                resultMembers = resultMembers.Where(itm => !itm.Contains(">k__BackingField")).ToList();
            }

            //++ remove members wich marked by one of Ignore Attribute
            var igoreByAttribMmbrs = new List<string>();
            for (int i = 0; i < resultMembers.Count; i++)
            {
                if (IsIgnorableMember(targetType, resultMembers[i]))
                {
                    igoreByAttribMmbrs.Add(resultMembers[i]);
                }
            }
            for (int i = 0; i < igoreByAttribMmbrs.Count; i++)
            {
                resultMembers.Remove(igoreByAttribMmbrs[i]);
            }

            //remove members which is read only // or instantiated once at creation

            return resultMembers;
        }




        /// <summary>
        /// Select Members of TTarget Type by selector nad other binding/ignore/only members  parameters. 
        /// </summary>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static List<string> SelectMembers<TTarget>(TypeMemberSelectorEn selector
                                , BindingFlags propertiesBinding = BindingFlags.Default
                                , BindingFlags fieldsBinding = BindingFlags.Default
                                , List<string> onlyMembers = null
                                , List<string> ignoreMembers = null)
        {
            return SelectMembers(typeof(TTarget), selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
        }

        #endregion ----------------------------- MEMBERS SELECTION -------------------------------


        #region -------------  IGNORE ATTRIBUTES MEMBERS ----------------


        /// <summary>
        /// Ignore attributes to check for (default : IgnoreMemberAttribute,  XmlIgnoreAttribute, NonSerialized)
        /// </summary>
        public static List<Type> IgnoreAttributes
        { get; } = new List<Type> {  Tps.T_XmlIgnoreAttribute
                                    , Tps.T_IgnoreMemberAttribute
                                    , Tps.T_NonSerializedAttribute
                                  };

        static bool IsIgnorableMember(Type targetType, string member)
        {
            var checkMember = targetType.GetMember(member).FirstOrDefault();

            var attributes = checkMember.GetCustomAttributes();//
            if (attributes == null || attributes.Count() == 9) return false;

            foreach (var atrib in attributes)
            {
                if (IgnoreAttributes.Contains(atrib.GetType()))
                { return true; }
            }
            return false;
        }

        #endregion-------------  IGNORE ATTRIBUTES MEMBERS ----------------

        /// <summary>
        /// Create Accessor for custom struct or class - targetType
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="membSelector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeAccessor Get(   Type targetType
                                           ,TypeMemberSelectorEn membSelector = TypeMemberSelectorEn.Default
                                           , BindingFlags propertiesBinding = BindingFlags.Default
                                           , BindingFlags fieldsBinding = BindingFlags.Default
                                           , List<string> onlyMembers = null
                                           , List<string> ignoreMembers = null
                                        )
        {                       
            return OperationInvoke.CallInMode(nameof(TypeAccessor), nameof(Get)
                 , () =>
                 {
                     var getMethod = currentType.GetMethod("Get"
                                        , new Type[] { 
                                                      typeof(TypeMemberSelectorEn)
                                                      ,typeof( BindingFlags)
                                                      ,typeof( BindingFlags)
                                                      ,typeof( List<string>)
                                                      ,typeof( List<string>)
                                                     }  ).MakeGenericMethod(targetType) ;

                     return getMethod.Invoke(null, new object[] {
                                                 membSelector
                                                , propertiesBinding
                                                ,fieldsBinding
                                                ,onlyMembers
                                                ,ignoreMembers
                                                }) as ITypeAccessor;

                 }
                 , "Can't Get Accessor of T[{0}]".Fmt(targetType.FullName)
             );
            
        }

       

        /// <summary>
        /// Create Accessor for custom struct or class - for T 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="membSelector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeAccessor<T> Get<T>(     
                                            TypeMemberSelectorEn membSelector = TypeMemberSelectorEn.Default
                                            , BindingFlags propertiesBinding = BindingFlags.Default
                                            , BindingFlags fieldsBinding = BindingFlags.Default
                                            , List<string> onlyMembers = null
                                            , List<string> ignoreMembers = null
                                         )
        {

            var members = SelectMembers<T>(membSelector,   propertiesBinding, fieldsBinding ,onlyMembers, ignoreMembers  );

            var accessor = TypeAccessor<T>.Get(true, members.ToArray());
            return accessor;
        }


        /// <summary>
        ///  Create Type Accessor for custom struct or class - for T by [custommembers]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="customMembers"></param>
        /// <returns></returns>
        public static ITypeAccessor<T> Get<T>(List<string> customMembers )
        {
            var accessor = TypeAccessor<T>.Get(true, customMembers.ToArray());
            return accessor;
        }


        /// <summary>
        ///  Create Type Accessor for custom struct or class - for [targetTyp] by [custommembers]
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="customMembers"></param>
        /// <returns></returns>
        public static ITypeAccessor Get(Type targetType, List<string> customMembers)
        {
            return OperationInvoke.CallInMode(nameof(TypeAccessor), nameof(Get)
                 , () =>
                 {
                     var getMethod = currentType.GetMethod("Get"
                                             , new Type[] 
                                             { typeof( List<string>)  })
                                        .MakeGenericMethod(targetType);

                     return getMethod.Invoke(null
                                            , new object[] { customMembers }) as ITypeAccessor;
                 }
                 , "Can't Get Accessor of T[{0}]".Fmt(targetType.FullName)
             );

        }




    }
}
