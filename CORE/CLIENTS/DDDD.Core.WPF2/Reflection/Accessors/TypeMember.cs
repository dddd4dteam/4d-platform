﻿using System.Reflection;

namespace DDDD.Core.Reflection
{


    public class TypeMember
    {
        public TypeMember(TypeInfoEx typeInf, MemberInfo memberInf)
        {
            TypeInf = typeInf;
            MemberInf = memberInf;
        }

        /// <summary>
        /// DataReader Column-Property-member Ordinal
        /// </summary>
        public int Ordinal
        { get; set; } = -1;
        
        /// <summary>
        /// Tyoe of current member 
        /// </summary>
        public TypeInfoEx TypeInf
        { get; private set; }



        /// <summary>
        /// MemberInfo of current Member 
        /// </summary>
        public MemberInfo MemberInf
        { get; private set; }

    }


}
