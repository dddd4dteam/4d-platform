﻿
using DDDD.Core.Collections.Virtual;
using DDDD.Core.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace DDDD.Core.Reflection
{
    /// <summary>
    /// Type Consts to do not get each time typeof(Tx)
    /// </summary>
    public class Tps
    {
        public static readonly Type T_object = typeof(object);        
        public static readonly Type T_NullableGen = typeof(Nullable<>);

        public static readonly Type T_IList = typeof(IList);        
        public static readonly Type T_IDictionary = typeof(IDictionary);        
        public static readonly Type T_ICollection = typeof(ICollection);
        public static readonly Type T_IEnumerable = typeof(IEnumerable);

        public static readonly Type T_Array  = typeof(Array);
        public static readonly Type T_ListGen = typeof(List<>);
        public static readonly Type T_DictionaryGen = typeof(Dictionary<,>);
        public static readonly Type T_IListGen = typeof(IList<>);
        public static readonly Type T_IDictionaryGen = typeof(IDictionary<,>);
        public static readonly Type T_ICollectionGen = typeof(ICollection<>);
        public static readonly Type T_IEnumerableGen = typeof(IEnumerable<>);                
        public static readonly Type T_ObservableCollectionGen = typeof(ObservableCollection<>);
        public static readonly Type T_VirtualCollectionGen = typeof(VirtualCollection<>);

        public static readonly Type T_TextWriter = typeof(TextWriter);
        public static readonly Type T_Stream = typeof(Stream);


        public static readonly Type T_string = typeof(string);
        public static readonly Type T_char = typeof(char);        
        public static readonly Type T_charNul = typeof(char?);

        public static readonly Type T_bool = typeof(bool);

       public static readonly Type T_short = typeof(short);
        public static readonly Type T_ushort = typeof(ushort);
        public static readonly Type T_int = typeof(int);
        public static readonly Type T_uint = typeof(uint);
        public static readonly Type T_Int16 = typeof(Int16);
        public static readonly Type T_UInt16 = typeof(UInt16);
        public static readonly Type T_Int32 = typeof(Int32);
        public static readonly Type T_UInt32 = typeof(UInt32);
        public static readonly Type T_Int64 = typeof(Int64);
        public static readonly Type T_UInt64 = typeof(UInt64);
        public static readonly Type T_long = typeof(long);
        public static readonly Type T_ulong = typeof(ulong);
        public static readonly Type T_byte = typeof(byte);
        public static readonly Type T_sbyte = typeof(sbyte);
        
        public static readonly Type T_float = typeof(float);
        public static readonly Type T_Single = typeof(Single);
        public static readonly Type T_Double = typeof(Double);
        public static readonly Type T_decimal = typeof(decimal);
               
        public static readonly Type T_boolNul = typeof(bool?);
        public static readonly Type T_shortNul = typeof(short?);
        public static readonly Type T_ushortNul = typeof(ushort?);
        
        public static readonly Type T_intNul = typeof(int?);
        public static readonly Type T_uintNul = typeof(uint?); 
        public static readonly Type T_Int16Nul = typeof(Int16?);
        public static readonly Type T_UInt16Nul = typeof(UInt16?);
        public static readonly Type T_Int32Nul = typeof(Int32?);
        public static readonly Type T_UInt32Nul = typeof(UInt32?);
        public static readonly Type T_Int64Nul = typeof(Int64?);
        public static readonly Type T_UInt64Nul = typeof(UInt64?);

        public static readonly Type T_longNul = typeof(long?);
        public static readonly Type T_ulongNul = typeof(ulong?);
        public static readonly Type T_byteNul = typeof(byte?);
        public static readonly Type T_sbyteNul = typeof(sbyte?);
                
        public static readonly Type T_floatNul = typeof(float?);
        public static readonly Type T_SingleNul = typeof(Single?);
        public static readonly Type T_DoubleNul = typeof(Double?);
        public static readonly Type T_decimalNul = typeof(decimal?);
        
        public static readonly Type T_DateTime = typeof(DateTime);
        public static readonly Type T_TimeSpan = typeof(TimeSpan);
        public static readonly Type T_DateTimeOffset = typeof(DateTimeOffset);
        public static readonly Type T_Guid = typeof(Guid);

        public static readonly Type T_DateTimeNul = typeof(DateTime?);
        public static readonly Type T_TimeSpanNul = typeof(TimeSpan?);
        public static readonly Type T_DateTimeOffsetNul = typeof(DateTimeOffset?);
        public static readonly Type T_GuidNul = typeof(Guid?);

        public static readonly Type T_Uri = typeof(Uri);
        public static readonly Type T_byteArray = typeof(byte[]);
        public static readonly Type T_charArray = typeof(char[]);
        public static readonly Type T_BitArray = typeof(BitArray);
        public static readonly Type T_stringArray= typeof(string[]);

        public static readonly Type T_TypeInfoEx = typeof(TypeInfoEx);

        public static readonly Type T_XmlIgnoreAttribute = typeof(XmlIgnoreAttribute);
        public static readonly Type T_IgnoreMemberAttribute = typeof(IgnoreMemberAttribute);
        public static readonly Type T_NonSerializedAttribute = typeof(NonSerializedAttribute);




        /// <summary>
        ///  4D Primitive Types list.List of Types from TypePrimitiveEn enumerable fields.
        /// </summary>
        public static List<Type> Primitives
        { get { return Primitives4D; } }
               

        static readonly List<Type> Primitives4D = new List<Type>()
        {

            #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------

            /// <summary>
            /// Represents a 32-bit signed integer.
            /// </summary>
            T_Int32
            ,
            /// <summary>
            ///  Represents a 32-bit unsigned integer.
            /// </summary>
            T_UInt32
            ,

            /// <summary>
            /// Represents a 64-bit signed integer.
            /// </summary>
            T_Int64
            ,
            /// <summary>
            /// Represents a 64-bit unsigned integer.
            /// </summary>
            T_UInt64
            ,
            /// <summary>
            /// Represents a 16-bit signed integer.
            /// </summary>
            T_Int16
            ,
            /// <summary>
            /// Represents a 16-bit unsigned integer.
            /// </summary>
            T_UInt16
            ,
            /// <summary>
            /// Represents an 8-bit unsigned integer.
            /// </summary>
            T_byte
            ,
            /// <summary>
            /// Represents an 8-bit signed integer.
            /// </summary>
            T_sbyte
            ,

            /// <summary>
            /// Represents a character as a UTF-16 code unit.
            /// </summary>
            T_char
            ,
            /// <summary>
            /// Represents a Boolean value.
            /// </summary>
            T_bool
            ,
            /// <summary>
            ///  Represents an instant in time, typically expressed as a date and time of
            ///     day.
            /// </summary>
            T_DateTime
            ,
            /// <summary>
            ///  Represents a time interval.
            /// </summary>
            T_TimeSpan
            ,

            /// <summary>
            /// Represents a time interval.
            /// </summary>
            T_DateTimeOffset
            ,

            /// <summary>
            /// Represents a globally unique identifier (GUID).
            /// </summary>
            T_Guid
            ,
            /// <summary>
            /// Represents a single-precision floating-point number. Known as Single - Type.Name=Single. 
            /// </summary>
            T_float
            ,
            /// <summary>
            /// Represents a double-precision floating-point number.
            /// </summary>
            T_Double
            ,
            /// <summary>
            /// Represents a decimal number.
            /// </summary>
            T_decimal


            #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


            #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

            ,
            /// <summary>
            /// Represents a Nullable{32-bit signed integer}.
            /// </summary>
            T_Int32Nul
            ,
            /// <summary>
            /// Represents a Nullable{32-bit unsigned integer}.
            /// </summary>
            T_UInt32Nul
            ,

            /// <summary>
            /// Represents a Nullable{64-bit signed integer}.
            /// </summary>
            T_Int64Nul
           ,
            /// <summary>
            /// Represents a Nullable{64-bit unsigned integer}.
            /// </summary>
            T_UInt64Nul
           ,
            /// <summary>
            /// Represents a Nullable{16-bit unsigned integer}.
            /// </summary>
            T_Int16Nul
           ,
            /// <summary>
            /// Represents a Nullable{16-bit unsigned integer}.
            /// </summary>
            T_UInt16Nul
           ,
            /// <summary>
            /// Represents an Nullable{8-bit unsigned integer}.
            /// </summary>
            T_byteNul
           ,
            /// <summary>
            /// Represents an Nullable{8-bit signed integer}.
            /// </summary>
            T_sbyteNul
           ,
            /// <summary>
            /// Represents a Nullable{character as a UTF-16 code unit}.
            /// </summary>
            T_charNul
           ,
            /// <summary>
            ///  Represents a Nullable{Boolean value}.
            /// </summary>
           T_boolNul
           ,
            /// <summary>
            /// Represents a Nullable{instant in time, typically expressed as a date and time of
            ///     day}.
            /// </summary>
             T_DateTimeNul
           ,
            /// <summary>
            /// Represents a Nullable{time interval}.
            /// </summary>
            T_TimeSpanNul
           ,
            /// <summary>
            /// Represents a Nullable{globally unique identifier (GUID)}.
            /// </summary>
            T_GuidNul
           ,
            /// <summary>
            /// Represents a Nullable{single-precision floating-point number}.
            /// </summary>
             T_floatNul
           ,
            /// <summary>
            /// Represents a Nullable{double-precision floating-point number}.
            /// </summary>
            T_DoubleNul
           ,
            /// <summary>
            ///  Represents a Nullable{decimal number}.
            /// </summary>
            T_decimalNul
            ,

            #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


            #region ----------------------------------------  3  Classes:  ---------------------------------------------

            /// <summary>
            /// Represents text as a series of Unicode characters.
            /// </summary>
            T_string
            ,
            /// <summary>
            /// Represents an Array of 8-bit unsigned integer - byte[].
            /// </summary>
            T_byteArray
            ,
            /// <summary>
            /// Represents an Array of chars - char[].
            /// </summary>
            T_charArray
            ,

            /// <summary>
            ///  Provides an object representation of a uniform resource identifier (URI)
            ///     and easy access to the parts of the URI.
            /// </summary>
             T_Uri
            ,
            /// <summary>
            /// Manages a compact array of bit values, which are represented as Booleans,
            ///     where true indicates that the bit is on (1) and false indicates the bit is
            ///     off (0).
            /// </summary>
           T_BitArray

            #endregion---------------------------------------- 3  Classes:  ---------------------------------------------


            #region --------------------------- 4 Reflection Classes ------------------------------

            ,
            ///// <summary>
            ///// Crossplatform Domain Type/Sheme Definition
            ///// </summary>            
             T_TypeInfoEx

            #endregion --------------------------- 4 Reflection Classes ------------------------------

        };


        /// <summary>
        /// Is Some tp( Type ) 4DPromitive Type. 
        /// TRUE if tp is 4DPromitive Type
        /// </summary>
        /// <param name="tp"></param>
        /// <returns></returns>
        public static bool Is4DPrimitive(Type tp)
        {
            if (Primitives4D.Contains(tp)) return true;
            return false;
        }



        /// <summary>
        /// Primitive Types by their Names.
        /// There is no Nullables types.
        /// </summary>
          static Dictionary<string, Type> PrimitivesNoNul
        { get; } = new Dictionary<string, Type>()
        {

            #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------

            { T_Int32.Name, T_Int32 }
            ,
            { T_UInt32.Name, T_UInt32 }
            ,
            { T_Int64.Name, T_Int64 }
            ,
            { T_UInt64.Name , T_UInt64 }
            ,
            { T_Int16.Name , T_Int16 }
            ,
            { T_UInt16.Name , T_UInt16 }
            ,
            { T_byte.Name  ,  T_byte }
            ,
            { T_sbyte.Name ,T_sbyte }
            ,
            {T_char.Name,  T_char}
            ,
            { T_bool.Name , T_bool }            
            ,
            {T_DateTime.Name, T_DateTime }
            ,
            {T_TimeSpan.Name, T_TimeSpan }            
            ,
            { T_DateTimeOffset.Name, T_DateTimeOffset }            
            ,
            { T_Guid.Name, T_Guid }            
            ,
            {T_float.Name , T_float}            
            ,
            {T_Double.Name, T_Double}            
            ,
            {T_decimal.Name, T_decimal }

            #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


            #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

            //,
            //{T_Int32Nul.Name,T_Int32Nul }            
            //,
            //{T_UInt32Nul.Name,T_UInt32Nul }            
            //,
            //{T_Int64Nul.Name, T_Int64Nul }
            //,
            //{T_UInt64Nul.Name,T_UInt64Nul }            
            //,
            //{T_Int16Nul.Name, T_Int16Nul }            
            //,
            //{T_UInt16Nul.Name,T_UInt16Nul }            
            //,
            //{T_byteNul.Name,T_byteNul }            
            //,
            //{  T_sbyteNul.Name,  T_sbyteNul}           
            //,
            //{T_charNul.Name,T_charNul }
            //,
            //{T_boolNul.Name,T_boolNul }
            //,
            //{T_DateTimeNul.Name,T_DateTimeNul }            
            //,
            //{ T_TimeSpanNul.Name ,T_TimeSpanNul }            
            //,
            //{T_GuidNul.Name ,T_GuidNul }            
            //,
            //{T_floatNul.Name ,T_floatNul }
            //,
            //{T_DoubleNul.Name,T_DoubleNul }
            //,
            //{T_decimalNul.Name,T_decimalNul}            

            #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


            #region ----------------------------------------  3  Classes:  ---------------------------------------------
            ,
            {T_string.Name,T_string }            
            ,
            {T_byteArray.Name,T_byteArray }            
            ,
            {T_Uri.Name,T_Uri }                         
            ,
            {T_BitArray.Name,T_BitArray }           
            #endregion---------------------------------------- 3  Classes:  ---------------------------------------------


            #region --------------------------- 4 Reflection Classes ------------------------------

            ,                 
            { T_TypeInfoEx.Name, T_TypeInfoEx }

            #endregion --------------------------- 4 Reflection Classes ------------------------------
            
        };


        /// <summary>
        /// Primitive Type Enumeraton(value) associated with primitive Type(key).
        /// </summary>
        static Dictionary<Type, TypePrimitiveEn> PrimitivesEnum
        { get; } = new Dictionary<Type, TypePrimitiveEn>()
        { 
            
            #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------

            {  T_Int32,TypePrimitiveEn.Int32 }
            ,
            {  T_UInt32,TypePrimitiveEn.UInt32 }
            ,
            {  T_Int64 ,TypePrimitiveEn.Int64}
            ,
            {  T_UInt64,TypePrimitiveEn.UInt64 }
            ,
            {  T_Int16 ,TypePrimitiveEn.Int16 }
            ,
            {  T_UInt16 ,TypePrimitiveEn.UInt16 } 
            ,
            {  T_byte ,TypePrimitiveEn.Byte }
            ,
            {  T_sbyte ,TypePrimitiveEn.SByte }
            ,
            {  T_char, TypePrimitiveEn.Char }
            ,
            {  T_bool , TypePrimitiveEn.Boolean }
            ,
            {  T_DateTime, TypePrimitiveEn.DateTime }
            ,
            {  T_TimeSpan, TypePrimitiveEn. TimeSpan }
            ,
            {  T_DateTimeOffset,TypePrimitiveEn. DateTimeOffset }
            ,
            {  T_Guid,TypePrimitiveEn.Guid }
            ,
            {  T_float,TypePrimitiveEn.Float }
            ,
            {  T_Double,TypePrimitiveEn.Double }
            ,
            {  T_decimal,TypePrimitiveEn.Decimal }

            #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


            #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

            ,
            {  T_Int32Nul, TypePrimitiveEn.Int32_nullable}
            ,
            { T_UInt32Nul, TypePrimitiveEn.UInt32_nullable }
            ,
            { T_Int64Nul, TypePrimitiveEn.Int64_nullable }
            ,
            { T_UInt64Nul, TypePrimitiveEn.UInt64_nullable }
            ,
            { T_Int16Nul, TypePrimitiveEn.Int16_nullable }
            ,
            { T_UInt16Nul, TypePrimitiveEn.UInt16_nullable }
            ,
            { T_byteNul, TypePrimitiveEn.Byte_nullable }
            ,
            { T_sbyteNul, TypePrimitiveEn.SByte_nullable}
            ,
            { T_charNul, TypePrimitiveEn.Char_nullable }
            ,
            { T_boolNul, TypePrimitiveEn.Boolean_nullable }
            ,
            { T_DateTimeNul, TypePrimitiveEn.DateTime_nullable }
            ,
            { T_TimeSpanNul, TypePrimitiveEn.TimeSpan_nullable }
            ,
            { T_GuidNul , TypePrimitiveEn.Guid_nullable}
            ,
            { T_floatNul , TypePrimitiveEn.Float_nullable}
            ,
            { T_DoubleNul, TypePrimitiveEn.Double_nullable }
            ,
            { T_decimalNul  , TypePrimitiveEn.Decimal_nullable}
            
            #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


            #region ----------------------------------------  3  Classes:  ---------------------------------------------
            ,
            { T_string,TypePrimitiveEn.String }
            ,
            { T_byteArray ,TypePrimitiveEn.ByteArray }
            ,
            { T_Uri ,TypePrimitiveEn.Uri }
            ,
            { T_BitArray ,TypePrimitiveEn.BitArray }            

            #endregion---------------------------------------- 3  Classes:  ---------------------------------------------


            #region --------------------------- 4 Reflection Classes -------------------------

            ,
            {  T_TypeInfoEx , TypePrimitiveEn.TypeInfoEx }

            #endregion --------------------------- 4 Reflection Classes -------------------------
        };


        public static TypePrimitiveEn Get4DPrimitiveEnum(Type tp)
        {
            if (PrimitivesEnum.ContainsKey(tp)) return PrimitivesEnum[tp];
            return TypePrimitiveEn.NotPrimitive;
        }

        /// <summary>
        /// Try to get Type from Primitive Types by its Name
        /// </summary>
        /// <param name="typeName"></param>
        /// <param name="tp"></param>
        /// <returns></returns>
        public static bool TryGetPrimitiveTp(string typeName, out Type tp)
        {
            return PrimitivesNoNul.TryGetValue(typeName,out tp);
        }






        /// <summary>
        /// Type Name Part Delimeter in Type Light Name format
        /// </summary>
       // public const string TDM = "|";

        public const string N_DICT = "Dict";
        public const string N_ARRAY = "ARRAY";
        public const string N_LIST = "List";
        public const string N_OBSERVABLECOL = "ObservableCollection";
        public const string N_VIRTUALCOL = "VirtualCollection";


        /// <summary>
        /// Lists Types Names for Type Light Name string format
        /// </summary>
        public static List<string> Lists
        { get; set; } = new List<string>()
        { N_LIST, N_OBSERVABLECOL , N_VIRTUALCOL };

        /// <summary>
        /// Is Type Name belongs to one of List Type Names - in Type Light Name string format.
        /// Now it is: List , ObservableCollection and VirtualCollection
        /// </summary>
        /// <param name="typeNameLN"></param>
        /// <returns></returns>
        public static bool IsListLN(string typeNameLN)
        {
            return Lists.Contains(typeNameLN);
        }


        /// <summary>
        /// Return end type of Generic IList based Colllction from Light Name notation.
        /// They are ListGen  ObservableCollectionGen  VirtualCollectionGen
        /// </summary>
        /// <param name="listName"></param>
        /// <returns></returns>
        public static Type GetListLNEndType(string listName)
        {
            if (listName == N_LIST)
            {
                return Tps.T_ListGen;
            }
            else if (listName == N_OBSERVABLECOL)
            {
                return Tps.T_ObservableCollectionGen;
            }
            else if (listName == N_VIRTUALCOL)
            {
                return Tps.T_VirtualCollectionGen;
            }
            return null;
        }

        /// <summary>
        /// Get LIST Name in Light Name notation from IList implemented Collection Type.Name
        /// </summary>
        /// <param name="listTpName"></param>
        /// <returns></returns>
        public static string GetListLNFromTypeName(string listTpName)
        {
            if (listTpName.StartsWith("List"))
            {
                return N_LIST;
            }
            else if (listTpName.StartsWith("ObservableCollection"))
            {
                return N_OBSERVABLECOL;
            }
            else if (listTpName.StartsWith("VirtualCollection") )
            {
                return N_VIRTUALCOL;
            }
            
            return null;
        }

        /// <summary>
        /// Copy of Hash func for string, like in .NET 4.x
        /// <para/> Refactored function from NET REFERENCIES sources.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int  HashCode(string value)// NET4.0
        {
            int num = 5381;
            int num2 = num;
            for (int i = 0; i < value.Length; i += 2)
            {
                num = (((num << 5) + num) ^ value[i]);

                if (i + 1 < value.Length)
                    num2 = (((num2 << 5) + num2) ^ value[i + 1]);
            }
            return num + num2 * 1566083941;
        }


        public static int GetTypeID(Type targetType)
        {
            return HashCode(targetType);
        }

        /// <summary>
        /// Get HashCode for Type.FullName
        /// <para/> Copy of Hash func for string, like in .NET 4.x
        /// <para/> Refactored function from NET REFERENCIES sources.
        /// </summary>
        /// <param name="tpValue"></param>
        /// <returns></returns>
         static int HashCode(Type tpValue)// NET4.0
        {
            var value = tpValue.FullName; 
            int num = 5381;
            int num2 = num;
            for (int i = 0; i < value.Length; i += 2)
            {
                num = (((num << 5) + num) ^ value[i]);

                if (i + 1 < value.Length)
                    num2 = (((num2 << 5) + num2) ^ value[i + 1]);
            }
            return num + num2 * 1566083941;
        }



    }
}
