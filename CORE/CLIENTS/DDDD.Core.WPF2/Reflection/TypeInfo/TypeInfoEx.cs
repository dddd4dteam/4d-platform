﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Threading;
using System.Collections.ObjectModel;


using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using DDDD.Core.Threading;
using DDDD.Core.Diagnostics;
using DDDD.Core.Collections;
using DDDD.Core.Data.Hash;
using System.IO;
using DDDD.Core.Collections.Concurrent;

namespace DDDD.Core.Reflection
{

    /// <summary>
    /// TypeInfoEx -   
    /// <para/>  1 analyzes and saves  Type Description in comfortable form for serializer.
    /// <para/>  2 Can Create instance by one of it's enabled Ctors.   
    /// <para/> Is 4D Primitive Type so it can be Serialized by Binary and Json.
     /// </summary>
    public  sealed class TypeInfoEx 
    {

        #region -------------------------- CTOR -----------------------------

        TypeInfoEx() //String typeFullName
        {
            State = TypeFoundStateEn.NotFounded;                       
        }


        TypeInfoEx(Type targetType)
        {
            OriginalType = targetType;           
            State = TypeFoundStateEn.TypeFound;
        }

        #endregion -------------------------- CTOR -----------------------------


        #region ---------------------- CONSTS ------------------------

        public static readonly HashSet<Type> NumberNotNullableTypes = new HashSet<Type>()
        {
             Tps.T_Int16  ,Tps.T_short   ,Tps.T_ushort
            ,Tps.T_Int32  ,Tps.T_int     ,Tps.T_uint
            ,Tps.T_Int64  ,Tps.T_long    ,Tps.T_ulong
            ,Tps.T_byte   ,Tps.T_sbyte   ,Tps.T_float
            ,Tps.T_Single ,Tps.T_Double  ,Tps.T_decimal
        };

        public static readonly HashSet<Type> NumberNullableTypes = new HashSet<Type>()
        {
            Tps.T_Int16Nul ,Tps.T_shortNul ,Tps.T_ushortNul
            ,Tps.T_Int32Nul ,Tps.T_intNul   ,Tps.T_uintNul
            ,Tps.T_Int64Nul ,Tps.T_longNul  ,Tps.T_ulongNul
            ,Tps.T_byteNul  ,Tps.T_sbyteNul ,Tps.T_floatNul
            ,Tps.T_SingleNul,Tps.T_DoubleNul,Tps.T_decimalNul
        };

        #endregion ---------------------- CONSTS ------------------------


        #region ---------------------------- FIELDS ------------------------------

        private static readonly object locker = new object();
                 

        /// <summary>
        /// Global TypeInfoEx Cache for all remembered Application Types
        /// </summary>
        internal static SafeDictionary2<int, TypeInfoEx> TypeInfoExCache
        { get; } = new SafeDictionary2<int, TypeInfoEx>();

        #endregion ---------------------------- FIELDS ------------------------------

        #region ------------ OPERATOES ==  and !=  ------------
        public static bool operator !=(TypeInfoEx a, TypeInfoEx b)
        {
            if (a is null && b is null) return false;
            if (a is null && (b is null) == false) return true;
            if ((a is null) == false && b is null) return true;
            return (a.ID != b.ID); 
        }

        public static bool operator ==(TypeInfoEx a, TypeInfoEx b)
        {
            if (a is null &&  b is null ) return true;
            if (a is null && (b is null) == false ) return false;
            if ((a is null) == false && b is null) return false;
            return (a.ID == b.ID); 
        }

        #endregion ------------ OPERATOES ==  and !=  ------------




        #region ---------------------------- GET CALLING TYPE FROM STACK --------------------------------


        /// <summary>
        /// Gets the type which is calling the current method which might be static. 
        /// </summary>
        /// <returns>The type calling the method.</returns>
#if !NETFX_CORE
        [MethodImpl(MethodImplOptions.NoInlining)]
#endif
        public static Type GetCallingType()
        {
#if NET45 || NET4 || NET46
            var frame = new StackFrame(1, false);
            var type = frame.GetMethod().DeclaringType;
#elif NETFX_CORE || PCL
            var type = typeof(object);
#else
            var frame = new StackTrace().GetFrame(2);
            var type = frame.GetMethod().DeclaringType;
#endif

            return type;
        }
        #endregion ---------------------------- GET CALLING TYPE FROM STACK --------------------------------

        #region ------------ GET ASSOCIATED TYPE ACCESSOR ---------


        /// <summary>
        /// Create and return TypeAccessor for  TypeInfoEx.[OriginalType].
        /// </summary>
        /// <param name="membSelector"></param>
        /// <returns></returns>
        public ITypeAccessor GetAccessor(  TypeMemberSelectorEn membSelector = TypeMemberSelectorEn.Default
                                                , BindingFlags propertiesBinding = BindingFlags.Default
                                                , BindingFlags fieldsBinding = BindingFlags.Default
                                                , List<string> onlyMembers = null
                                                , List<string> ignoreMembers = null
                                              )
        {
            /// ONLY FOR STRUCTS OR CLASSES ElSE NULL
            if (IsCustomClass == false && IsCustomStruct == false)
            { return null; }
                        
            return TypeAccessor.Get(OriginalType, membSelector
                , propertiesBinding, fieldsBinding
                ,onlyMembers,ignoreMembers);

        }


       

        #endregion ------------ GET ASSOCIATED TYPE ACCESSOR ---------


        #region ---------------------------- TypeAQName && TYPE FOUND STATE  ----------------------------

        /// <summary>
        /// Type Assembly Qualified Name Parsed struct. 
        /// <para/>  Contains FoudType if it'll be found by some assemblyQualifidName string.
        /// <para/>  Has Lazy get mechanic.
        /// </summary>         
        public TypeAQName TpAQName
        { get; private set; }


        /// <summary>
        /// State About Founding result of Type,
        /// <para/> when Type was tried to be searched/determined by AssemblyQualifiedName.
        /// If TypeInfoEx was inited by System.Type State always will be - [TypeFounded] value  
        /// </summary>
        public TypeFoundStateEn State
        {
            get; private set;
            //{
                //if (TpAQName.LA_FoundType.Value.IsNotNull())
                //{ return TypeFoundStateEn.TypeFound; }
                //return TypeFoundStateEn.NotFounded;
           // }
        }

        #endregion  ---------------------------- TypeAQName && TYPE FOUND STATE  ----------------------------


        #region --------------------------------- Type INFO   ------------------------------


        Type _OriginalType = null;
        
        /// <summary>
        /// (T) - Original  Type. We analyze this Type. 
        /// </summary>
         
        public Type OriginalType
        {
            get
            {
                return _OriginalType;
            }
            private set
            {
                if (value != null)
                {
                    _OriginalType = value;
                    OriginalTypeAQName = _OriginalType.AssemblyQualifiedName;
                }                
            }
        }

        /// <summary>
        /// OriginalType Assembly Name
        /// </summary>
        public string AssemblyName
        { get; private set; }



        /// <summary>
        /// Assembly Qualified Name of Original type string  value( not of type TypeAQName).
        /// It always will be stored in TypeInfoEx instance - even when originalType won't be found by such Name.         
        /// </summary>
        public string OriginalTypeAQName
        { get; private set; }
        

        /// <summary>
        /// Assembly Qualified Name of Original type string  value( not of type TypeAQName) but without info about Assembly Version.        
        /// </summary>
        public string OriginalTypeAQNameShort
        { get; private set; }


        /// <summary>
        /// Type.Name Hash
        /// </summary>
        public int TypeNameHash
        { get; private set; }
        

        
        /// <summary>       
        ///  HashCode of  OriginalType.FullName 
        /// <para/>   TypeinfoEx ID- ID for private TypeInfoEx cache Dictionarys.
        /// </summary>
        public int ID
        {   get; private set;  }


        #endregion --------------------------------- Type INFO  ----------------------------------
        

        #region ------- ID_Light / OriginalTypeLighteName ---------------


        //const string T_LIST = "List";
        //const string T_OBSERVABLECOL = "ObservableCollection";
        //const string T_VIRTUALCOL = "VirtualCollection";

        //const string T_DICT = "Dict";
        //const string T_ARRAY = "ARRAY";
        //const string TDM = "|";

        //static List<string> Lists
        //{ get; set; } = new List<string>()
        //{ T_LIST, T_OBSERVABLECOL , T_VIRTUALCOL };


        /// <summary>
        /// Original Type  simple Name for JS or TypeSript Json parsing simplification
        /// </summary>
        public string OriginalTypeLightName
        { get; private set; }


        /// <summary>
        /// ID from Lisght Type Name - Hash Code of OriginalTypeLightName
        /// </summary>
        public int ID_LN
        { get; private set; }

        ///// <summary>
        ///// Build Type Light Name by TypeInfoEx item private logic
        ///// </summary>
        ///// <param name="tpImfoEx"></param>
        ///// <returns></returns>
        //static string BuildTypeLightName(TypeInfoEx tpImfoEx)
        //{
        //    if ( tpImfoEx.Is4DPrimitive)
        //    {
        //        return tpImfoEx.OriginalType.FullName;
        //    }

        //    // Is Array
        //    if (tpImfoEx.IsArray)
        //    {
        //        return T_ARRAY + TDM + tpImfoEx.ArrayElementType.FullName;
        //    }
        //    // IS LIST / ObservableCollection...
        //    else if (tpImfoEx.IsIListImplemented)
        //    {
        //        // 1 no Generic ARGS 
        //        // 2 has generic args
        //        // 3 arg is List too
        //        if (tpImfoEx.Arg1Type  == null) // Collection Class still
        //        {

        //        }

        //        if (tpImfoEx.Arg1Type.IsImplement_List())
        //        {
        //            var args = tpImfoEx.Arg1Type.GetGenericArguments();

        //            if ( args.Length == 0 )
        //            { return string.Empty;
        //            }

        //            return T_LIST + TDM + T_LIST + TDM
        //                + args[0].FullName;

        //        }
        //        else // simnple List of T
        //        {
        //            return T_LIST + TDM + tpImfoEx.Arg1Type.FullName;
        //        }
        //    }
        //    // IS DICTIONARY
        //    else if (tpImfoEx.IsIDictionaryImplemented)
        //    {
        //        if (tpImfoEx.Arg2Type.IsImplement_List())
        //        { // DICTIONARY of LIST 
        //            var args = tpImfoEx.Arg2Type.GetGenericArguments();

        //            if (args.Length == 0)
        //            {  return string.Empty;
        //            }

        //            return
        //                    T_DICT + TDM + tpImfoEx.Arg1Type.FullName
        //                        + TDM +  T_LIST + TDM + args[0].FullName;

        //        }
        //        else
        //        {
        //            return T_DICT + TDM + tpImfoEx.Arg1Type.FullName
        //                            + TDM + tpImfoEx.Arg2Type.FullName;

        //        }

        //    }
        //    else// Enums + CUstomTypes + Abstract Types + Interfaces
        //    {
        //        return tpImfoEx.OriginalType.FullName;
        //    }
        //}


        //static Type GetTypeByFN(string tpFullName)
        //{
        //    //1
        //    var id = Tps.GetStrHashCode(tpFullName);
        //    TypeInfoEx tpInfoEx = null; 
        //     TypeInfoExCache.TryGetValue(id, out tpInfoEx);
        //    if (tpInfoEx.IsNotNull()) return tpInfoEx.OriginalType;

        //    //2
        //    var resType = TypeCache.GetCustomDomainTypeByFN(tpFullName);                       

        //    return resType;
        //}

        ///// <summary>
        ///// Parse Type Light Name string to get real Type
        ///// </summary>
        ///// <param name="tpLightName"></param>
        ///// <returns></returns>
        //public static Type ParseLightName(string tpLightName)
        //{
        //    var items = tpLightName.SplitNoEntries(TDM);

        //    if (items.Count == 1)
        //    {
        //        //PRIMITIVES
        //        if (Tps.PrimitivesLight.ContainsKey(items[0]))
        //        {
        //            return Tps.PrimitivesLight[items[0]];
        //        }
        //        else  // customType | enum too 
        //        {
        //            return GetTypeByFN(items[0]);
        //        }
        //    }            
        //    else if (items.Count == 2)//List or Array
        //    {
        //        if (Lists.Contains(items[0])) // LIST
        //        {
                    
        //            var arg1Type = GetTypeByFN(items[1]);

        //            if (arg1Type == null)
        //            { return null; }

        //            return Tps.T_IListGen
        //                 .MakeGenericType(arg1Type);
        //        }
        //        else if ( items[0] == T_ARRAY) // ARRAY
        //        {
        //            var arg1Type = GetTypeByFN(items[1]);

        //            if (arg1Type == null)
        //            { return null; }

        //            return Tps.T_Array
        //            .MakeGenericType(arg1Type);
        //        }
        //    }
        //    else if (items.Count == 3)// Dictionary / LIST of LIST of T
        //    {
        //        if (items[0] == T_DICT) //  Dictionary
        //        {
        //            var arg1Type = GetTypeByFN(items[1]);
        //            var arg2Type = GetTypeByFN(items[2]);

        //            if (arg1Type == null || arg2Type == null)
        //            { return null; }

        //            return Tps.T_IDictionaryGen
        //                 .MakeGenericType(arg1Type, arg2Type);
        //        }
        //        else if (Lists.Contains(items[0]) // LIST of LIST of T
        //            && Lists.Contains(items[1])
        //            )
        //        {
        //            var arg1Type = GetTypeByFN(items[2]);

        //            if (arg1Type == null  )
        //            { return null; }

        //            var argList = Tps.T_IListGen
        //                 .MakeGenericType(arg1Type);
                    
        //            return Tps.T_IListGen
        //                 .MakeGenericType(argList);
        //        }
        //    }
        //    else if(items.Count == 4)
        //    {

        //        if ( items[0] == T_DICT
        //        && Lists.Contains(items[2]))// Dictionary of LIST of T
        //        {
        //            var argKeyTp = GetTypeByFN(items[1]);
        //            var argListElemTp = GetTypeByFN(items[3]);

        //            if ( argKeyTp == null || argListElemTp == null )
        //            { return null; }

        //            var argList = Tps.T_IListGen
        //                .MakeGenericType(argListElemTp);

        //            return Tps.T_IDictionaryGen
        //                 .MakeGenericType(argKeyTp, argList);

        //        }
        //    }

        //    return null;
        //}


        #endregion ------- ID_Light / OriginalTypeLighteName ---------------
            
            
        #region  --------------------------------- Type DEFINITION SUB TYPES ----------------------------------


        ///// <summary>
        ///// Cached Generic Arguments Types of OriginalType
        ///// </summary>
        //public Type[] GenericArguments
        //{ get; private set; }
        

        /// <summary>
        /// WorkingType - is T from  Nullable{T},where T is OriginalType; or if OriginalType is not Nullable then WorkType==OriginalType
        /// </summary>
        public Type WorkingType { get; private set; }

        /// <summary>
        /// Interfaces names of OriginalType, We collect interfaces only for Dpmain Types:
        /// <para/> not for Collections/Arrays/ Enums/ Promitives4D
        /// </summary>
        public List<string> Interfaces
        { get; private set; } = new List<string>();


        /// <summary>
        /// Enum Underlying Type 
        /// </summary>
        public Type EnumUnderlyingType { get; private set; }



        /// <summary>
        /// Array Element Type
        /// </summary>
        public Type ArrayElementType { get; private set; }



        /// <summary>
        /// Array Rank
        /// </summary>
        public int ArrayRank { get; private set; }


        /// <summary>
        /// List_Arg1_  or ObservableCollection_Arg1_ or  Dictionary_Arg1     Type
        /// </summary>
        public Type Arg1Type { get; private set; }





        /// <summary>
        /// Dictionary_Arg2     Type
        /// </summary>
        public Type Arg2Type { get; private set; }



        #endregion  --------------------------------- Type DEFINITION SUB TYPES ----------------------------------


        #region ------------------------- INTERFACES -------------------------


        /////// <summary>
        /////// Cached Interfaces Types of OriginalType
        /////// </summary>
        //////public Type[] Interfaces { get; private set; }



        #endregion ------------------------- INTERFACES -------------------------

         

        #region ----------------------------IsNullable && Is4Dprimitive   ---------------------------

        /// <summary>
        /// Is Type Nullable value type - Nullable struct type.
        /// </summary>
        public bool IsNullableType
        { get; private set; }


        /// <summary>
        /// Is this OriginalType - 4D Primitive Type. Tps.Primitives  - all 4D Primitive Types
        /// </summary>         
        public bool Is4DPrimitive
        { get; private set; }


        /// <summary>
        /// Primitive Type Enumeration value
        /// </summary>
        public TypePrimitiveEn Primitive
        { get; private set; } = TypePrimitiveEn.NotPrimitive;


        public bool IsAbstract
        { get { return OriginalType.IsAbstract; }
        }

        public bool IsInterface
        {
            get { return OriginalType.IsInterface; }
        }

        

        /// <summary>
        /// True when it is NOt Abstract Custom Class
        /// </summary>
        public bool IsCustomClass
        { get; private set; }

        public bool IsCustomStruct
        { get; private set; }


        #endregion ----------------------------IsNullable && Is4Dprimitive  ---------------------------


        #region ------------------------------ IsSerializable && IsDisposable  -------------------------------------

        /// <summary>
        ///  If this type Serializable 
        /// </summary>

        public LazyAct<bool> IsSerializable
        { get; private set;  }
        


        /// <summary>
        /// Is this TargetType supports IDisposable
        /// </summary>

        public bool IsDisposable
        { get; private set; }



        #endregion --------------------------- IsSerializable && IsDisposable  --------------------------------


        #region --------------------------- IsTestableByDefCtor --------------------------------

        /// <summary>
        /// Try to understand if we able to test Type's Default Ctor.
        /// <para/> For Collection Types Default Ctor has length argument, like : new TCollection(length). 
        /// <para/> Problem here is that some Types can have internal static increment logic on creation by Default Ctor.
        /// <para/>  - so we'll exclude them from Default Ctor testing.
        /// <para/> By default all types will be tested, excluding array types.  
        /// </summary>      
         
        public bool IsTestableByDefCtor
        {
            get;  set;
        }


        #endregion --------------------------- IsTestableByDefCtor --------------------------------
        
         

        #region ------------------------------TYPES COMPARE - BY TYPE NAME ONLY - FOR INTERFACE AND GENERIC TYPES----------------------------------


        /// <summary>
        /// If  this [targetType] implement interface type [checkInterfaceType], comparing by Type.Name only
        /// <para/> For example if this TargetType implementing - IList, or IList{} or IDIctionary{,}
        /// </summary>
        /// <param name="someClassType"></param>
        /// <param name="checkInterfaceType"></param>
        /// <returns></returns>
        public static bool IsImplementInterfaceByTypeNameOnly(Type someClassType, Type checkInterfaceType)
        {

            if (someClassType == null)  return false;
            if (checkInterfaceType == null) return false;
            if (checkInterfaceType.IsInterface == false) return false;


            return (someClassType.GetInterfaces()
                   .FirstOrDefault(ifc => ifc.Name == checkInterfaceType.Name)
                   != null);
        }


        /// <summary>
        /// If  this [targetType] implement interface type [checkInterfaceType], comparing by Type.Name only
        /// <para/> For example if this TargetType implementing - IList, or IList{} or IDIctionary{,} 
        /// </summary>
        /// <param name="tpInterfaces"></param>
        /// <param name="checkInterfaceType"></param>
        /// <returns></returns>
        public static bool IsImplementInterface(Type[] tpInterfaces, Type checkInterfaceType)
        {
            if (tpInterfaces == null || tpInterfaces.Length == 0) return false;
            if (checkInterfaceType == null) return false;
            if (checkInterfaceType.IsInterface == false) return false;
            
            return (tpInterfaces.FirstOrDefault(ifc => ifc.Name == checkInterfaceType.Name) != null);
            // Contains(checkInterfaceType); - not work fo generic interface with clean args
        }



        /// <summary>
        /// If  this [targetType] is type [checkGenericType], comparing by Type.Name only
        /// <para/> For example if this TargetType is - ObservableCollection{}
        /// </summary>
        /// <param name="someClassType"></param>
        /// <param name="checkGenericType"></param>
        /// <returns></returns>
        internal static bool IsGenericTypeByTypeNameOnly(Type someClassType, Type checkGenericType)
        {
#if DEBUG
            if (someClassType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - someClassType parameter  ");
            if (checkGenericType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - checkGenericType parameter  ");
#endif
            return (someClassType.Name == checkGenericType.Name);
        }

        #endregion ------------------------------TYPES COMPARE - BY TYPE NAME ONLY - FOR INTERFACE AND GENERIC TYPES----------------------------------


        #region --------------------------- IsICollection ,  IsIListImplemented , IsIDictionaryImplemented, IsObservableCollection --------------------------------

        /// <summary>
        /// If WorkingType is Array
        /// </summary>
        public bool IsArray
        {
            get { return WorkingType.IsArray; }
        }

        /// <summary>
        /// If WorkingType is Enum
        /// </summary>
        public bool IsEnum
        {
            get { return WorkingType.IsEnum; }
        }

   


    /// <summary>
    /// If WorkingType Implement  ICollection  base Interface
    /// </summary>
    public bool IsICollection  { get; private set; }


        /// <summary>
        /// If  Type is some collection this Collection Type   property value can tell us the real kind of it: Array, IList, IDictionary, OnservableCollection 
        /// </summary>

        public CollectionTypeEn CollectionType { get; private set; }



        /// <summary>
        /// Is this TargetType - a Collection Type and it implements  IList or IList{T} interfaces
        /// </summary>

        public bool IsIListImplemented { get; private set; }


        /// <summary>
        /// Is this TargetType - a Collection Type and it implements  IDictionary{,} interface
        /// </summary>

        public bool IsIDictionaryImplemented { get; private set; }


        /// <summary>
        /// Is this TargetType - a Collection Type and it implements ObservableCollection 
        /// </summary>

        public bool IsObservableCollection { get; private set; }
        




        /// <summary>
        /// Get Arg of IList based from CustomCollection type. 
        /// <para/> Custom  IList  based Collection Type has the following defenition constraints : it should implement IList or IList{TArg}
        /// </summary>
        /// <param name="iListType"></param>
        /// <returns></returns>
        public static Type Get_IListArgType(Type iListType)
        {
            if (iListType == null)
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}(): iListType parameter cannot be null");

            var interfaces = iListType.GetInterfaces();

            if (interfaces.Length == 0)
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}(): Type [{iListType.FullName}] isn't implemented one of List interfaces- IList or IList<TElement> interfaces ");


            if (iListType.IsImplementInterfaceByTypeNameOnly(Tps.T_IListGen)) // 
            {
                return interfaces.Where(ifc => ifc.Name == Tps.T_IListGen.Name)
                       .FirstOrDefault().GetGenericArguments()[0];
            }
            else if (iListType.IsImplementInterfaceByTypeNameOnly(typeof(IList)))
            {
                return Tps.T_object;
            }
            else
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}():  Cannot determine IList Arg Type ");

        }



        /// <summary>
        /// Get Arg of IDictionary based from CustomCollection type. 
        /// <para/> Custom  IDictionary  based Collection Type has the following defenition constraints : it should implement IList or IDictionary{TArg}
        /// </summary>
        /// <param name="iDictionaryType"></param>
        /// <returns></returns>
        public static Type[] Get_IDictionaryArgTypes(Type iDictionaryType)
        {
            Type[] reslt = new Type[2];

            var interfaces = iDictionaryType.GetInterfaces();
            if (iDictionaryType.IsImplementInterfaceByTypeNameOnly(Tps.T_IDictionaryGen))
            {
                var targetGenericIface = interfaces.FirstOrDefault(ifc => ifc.Name == Tps.T_IDictionaryGen.Name);
                reslt[0] = targetGenericIface.GetGenericArguments()[0];
                reslt[1] = targetGenericIface.GetGenericArguments()[1];
                return reslt;
            }
            else if (iDictionaryType.IsImplementInterfaceByTypeNameOnly(Tps.T_IDictionary))
            {
                var targetPlaneIface = interfaces.FirstOrDefault(ifc => ifc.Name == Tps.T_IDictionary.Name);
                reslt[0] = Tps.T_object;
                reslt[1] = Tps.T_object; 
                return reslt;
            }            

            throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IDictionaryArgTypes)}():  Cannot determine IDictionary Arg Types ");
        }


        /// <summary>
        /// If these interfaces Types contais interfaces IList and IList{}
        /// </summary>
        /// <param name="tpIfaces"></param>
        /// <returns></returns>
        public static bool IsImplement_List(Type[] tpIfaces)
        {
            return IsImplementInterface(tpIfaces, Tps.T_IList)
                  && IsImplementInterface(tpIfaces, Tps.T_IListGen);
        }

        /// <summary>
        /// If this class implements interfaces IList and IList{}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_List(Type targetType)
        {
            return (IsImplementInterfaceByTypeNameOnly(targetType, Tps.T_IList) // C# automatically add support IDictionaryPlane - we need to IDictionary<,> to serialize 
                    &&
                    IsImplementInterfaceByTypeNameOnly(targetType, Tps.T_IListGen));
        }


        /// <summary>
        ///  If these interfaces Types contain interfaces IDictionary and IDictionary{,}
        /// </summary>
        /// <param name="tpIfaces"></param>
        /// <returns></returns>
        public static bool IsImplement_Dictionary(Type[] tpIfaces)
        {
            return IsImplementInterface(tpIfaces, Tps.T_IDictionary)
                    && IsImplementInterface(tpIfaces, Tps.T_IDictionaryGen);
        }


        /// <summary>
        /// If this class implements interfaces IDictionary and IDictionary{,}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_Dictionary(Type targetType)
        {
            return (IsImplementInterfaceByTypeNameOnly(targetType, Tps.T_IDictionary) // C# automatically add support IDictionaryPlane - we need to IDictionary<,> to serialize 
                    &&
                    IsImplementInterfaceByTypeNameOnly(targetType, Tps.T_IDictionaryGen));
        }


        #endregion --------------------------- IsIListImplemented , IsIDictionaruary, IsObservableCollection --------------------------------


        #region --------------------------GET TYPEINFOEX BY TYPE && ASEMBLYQUALIFIEDNAME --------------------------------------------
        
        /// <summary>
        /// Get TypeinfoEx by ID - Type should already exist in Cache.
        /// If not exist in Cache returns null.
        /// </summary>
        /// <param name="typeID"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        public static TypeInfoEx Get ( int typeID, bool useThreadSafetyWay = false)
        {
            TypeInfoEx tpInfCached = null;
            if (TypeInfoExCache.TryGetValue(typeID, out tpInfCached))
            {  return tpInfCached;
            }

            return null;
        }


        /// <summary>
        /// Get TypeInfoEx by T- System.Type - always will be with State:TypeFounded
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx Get<T>(bool useThreadSafetyWay = false)
        {
            return Get(typeof(T), useThreadSafetyWay);
        }

        /// <summary>
        /// Get TypeInfoEx by System.Type - always will be with State:TypeFounded
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx Get(Type targetType, bool useThreadSafetyWay = false)
        {
            var IDKey = Tps.HashCode(targetType.FullName);

            //already exist  
            TypeInfoEx tpInfCached = null;
            if (TypeInfoExCache.TryGetValue(IDKey, out tpInfCached))
            {    return tpInfCached;             
            }

            OperationInvoke.CallWithLockAndModes(nameof(TypeInfoEx), nameof(Get), useThreadSafetyWay
                , locker
                , () => TypeInfoExCache.NotContainsKey(IDKey) // lock use check  Expression
                , (Action)(() =>
                {
                    var tpInfoEx = new TypeInfoEx(targetType);
                    tpInfoEx.ID = IDKey;
                    tpInfoEx.OriginalTypeAQName = targetType.AssemblyQualifiedName;
                    tpInfoEx.TpAQName = new TypeAQName(tpInfoEx.OriginalTypeAQName);

                    tpInfoEx.AssemblyName = tpInfoEx.TpAQName.AssemblyDescriptionString;
                    tpInfoEx.OriginalTypeAQNameShort = tpInfoEx.TpAQName.GetTypeAQNameShortVariant();
                    tpInfoEx.TypeNameHash = Tps.HashCode(tpInfoEx.OriginalType.Name);                                        
                    
                    tpInfoEx.Is4DPrimitive = Is4DPrimitiveType(tpInfoEx.OriginalType);
                    if (tpInfoEx.Is4DPrimitive)
                    { tpInfoEx.Primitive = Tps.Get4DPrimitiveEnum(tpInfoEx.OriginalType);
                    }
                    
                    tpInfoEx.IsNullableType = GetIsNullableType(tpInfoEx.OriginalType);
                    tpInfoEx.WorkingType = GetWorkingTypeFromNullableType(tpInfoEx.OriginalType);
                 
                    if (tpInfoEx.OriginalType != Tps.T_object
                        && tpInfoEx.IsAbstract == false
                        && tpInfoEx.IsInterface == false
                        && tpInfoEx.Is4DPrimitive == false)
                    {
                        //Get Generic Args -caching them too
                        //tpInfoEx.GenericArguments = tpInfoEx.OriginalType.GetGenericArguments();
                        //Get interfaces
                        var ifaces = tpInfoEx.OriginalType.GetInterfaces();                        

                        tpInfoEx.IsICollection = Is_ICollectionType( ifaces);
                        tpInfoEx.IsIListImplemented = IsImplement_List( ifaces);
                        tpInfoEx.IsIDictionaryImplemented = IsImplement_Dictionary( ifaces);
                        tpInfoEx.IsObservableCollection = IsGenericTypeByTypeNameOnly(tpInfoEx.OriginalType, Tps.T_ObservableCollectionGen);

                        tpInfoEx.CollectionType = GetBaseCollectionType(tpInfoEx);
                        tpInfoEx.IsDisposable = IsDisposableType(ifaces);
                        //tpInfEx.IsSerializable = IsSerializableType(tpInfEx.OriginalType);

                        tpInfoEx.IsTestableByDefCtor = IsTestableByDefCtorType(tpInfoEx.OriginalType);

                        //check is enum
                        if (tpInfoEx.IsEnum)
                        {
                            tpInfoEx.EnumUnderlyingType = Enum.GetUnderlyingType(tpInfoEx.OriginalType);
                        }

                        //check is  array
                        else if (tpInfoEx.IsArray)
                        {
                            tpInfoEx.ArrayElementType = tpInfoEx.OriginalType.GetElementType();
                            tpInfoEx.ArrayRank = tpInfoEx.OriginalType.GetArrayRank();
                        }
                        //check is IList
                        else if (tpInfoEx.IsIListImplemented) //
                        {
                            tpInfoEx.Arg1Type = Get_IListArgType(tpInfoEx.OriginalType);//
                        }
                        // check is IDictionary
                        else if (tpInfoEx.IsIDictionaryImplemented) // 
                        {
                            var dictionaryArgs = Get_IDictionaryArgTypes(tpInfoEx.OriginalType);
                            tpInfoEx.Arg1Type = dictionaryArgs[0];
                            tpInfoEx.Arg2Type = dictionaryArgs[1];
                        }
                        else if (tpInfoEx.OriginalType.IsClass)
                        {    // NOT ABSTRACT CUSTOM CLASS
                            tpInfoEx.IsCustomClass = true;
                        }
                        else if (tpInfoEx.OriginalType.IsValueType)
                        {
                            tpInfoEx.IsCustomStruct = true;
                        }

                        // Set Interfaces - Names - we collect interfaces only for Domain class/struct Types
                        if (tpInfoEx.IsCustomClass ||
                             tpInfoEx.IsCustomStruct)
                        {
                            if ( ifaces.Length > 0)
                            {
                                tpInfoEx.Interfaces = ifaces
                                               .Select(ifc => ifc.Name).ToList(); ;

                            }
                        }

                    }

                    // BUILD Type Light Name
                    tpInfoEx.OriginalTypeLightName = TypeLN.BuildTypeLightName(tpInfoEx);
                    tpInfoEx.ID_LN = Tps.HashCode(tpInfoEx.OriginalTypeLightName);

                    //TypeConventionName - used to generate class by Reflection.Emit
                    tpInfoEx.TypeDebugCodeConventionName = tpInfoEx.BuildDebugCodeConventionedTypeName();

                    //Caching by ID_Light- Original Type LightName Hash
                    TypeInfoExCache.Add(tpInfoEx.ID, tpInfoEx);

                })
                //, exceptionAction, error detail message
                );

           
            return TypeInfoExCache[IDKey];
            
        }



        /// <summary>
        /// Get TypeInfoEx by System.Type.FullName - can lead to two State
        /// <para/> - TypeFounded - System.Type was found  
        /// <para/> - NotFounded - System.Type was not found and OriginalType will be null
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx GetByLN(string tpLightName,string assembly = null, bool useThreadSafetyWay = false)
        {
            //var ID_ln = Tps.HashCode(tpLightName);
           
            var type = TypeLN.ParseTypeLightName(tpLightName, assembly);
            if (type == null) // Type NOt DETERMINED
            {
                 //HERE WE CAN't CACHE BECAUSE NO ID
                var tpInfoEx = new TypeInfoEx();
                tpInfoEx.OriginalTypeLightName = tpLightName;
                tpInfoEx.ID_LN = Tps.HashCode(tpInfoEx.OriginalTypeLightName);

                return tpInfoEx;
            }
            else
            {
                return Get(type, useThreadSafetyWay);
            }
        }



        /// <summary>
        /// Get TypeInfoEx by System.Type.AssemblyQualifiedName - can lead to two State
        /// <para/> - TypeFounded - System.Type was found  
        /// <para/> - NotFounded - System.Type was not found and OriginalType will be null
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx GetByAQN(string tpAQName, bool useThreadSafetyWay = false)
        {
            var typeAQN =new TypeAQName(tpAQName);
            if (typeAQN.LA_FoundType.Value != null)
            {
               return Get(typeAQN.LA_FoundType.Value, useThreadSafetyWay);
            }
            else
            {
                var tpInfoEx = new TypeInfoEx();
                tpInfoEx.TpAQName = typeAQN;
                tpInfoEx.ID = Tps.GetTypeID( tpInfoEx.OriginalType );
                tpInfoEx.OriginalTypeAQName  = tpAQName;
                
                return tpInfoEx;
            }
 
        }


        /// <summary>
        ///   Get TypeInfoEx for object item . 
        ///  <para/>  If someValue is null then nulll will be returned        ///   
        /// </summary>
        /// <param name="someValue"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx GetByObj(object someValue, bool useThreadSafetyWay = false)
        {
            if (someValue == null) return null;
            
            var type = someValue.GetType();
            return Get(type, useThreadSafetyWay);                    
        }


        #endregion--------------------------GET TYPEINFOEX BY TYPE && ASEMBLYQUALIFIEDNAME --------------------------------------------






        #region ------------------------- TypeDebugCodeConventionName && BUILD IT && GET IT ------------------------


        /// <summary>
        ///  Type Convention Name that can be used in Lambda Expression
        /// <para/> when we building dynamic emitted methods by TypeBuilder.
        /// <para/>  Here we simply deleting generic brackets and array brackets
        /// </summary>

        public string TypeDebugCodeConventionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Type Name - flat name that can be used in Debug Code generation for any type class name(generic collection/array/ nullable struct).
        /// </summary>
        /// <returns></returns>
        string BuildDebugCodeConventionedTypeName()
        {
            string baseName = null;

            if (OriginalType.IsInterface)
            {
                if (Arg2Type != null && Arg1Type != null)//Length ==2
                {
                    baseName = OriginalType.Name.Replace("`2", "");
                    return baseName + "_" + Arg1Type.Name + "_" + Arg2Type.Name;
                }
                else if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }
            }
            else if (OriginalType.IsEnum)
            {
                baseName = OriginalType.Name;
                return baseName;
            }
            else if (OriginalType.IsArray)
            {

                if (ArrayRank == 1)
                {
                    baseName = OriginalType.Name.Replace("[]", "");
                    return baseName + "1Rank";
                }
                else if (ArrayRank == 2)
                {
                    baseName = OriginalType.Name.Replace("[,]", "");
                    return baseName + "2Rank";
                }
                else if (ArrayRank == 3)
                {
                    baseName = OriginalType.Name.Replace("[,,]", "");
                    return baseName + "3Rank";
                }
                else if (ArrayRank == 4)
                {
                    baseName = OriginalType.Name.Replace("[,,,]", "");
                    return baseName + "4Rank";
                }

            }
            else if (OriginalType.IsImplement_List()) // typeof(IList).IsAssignableFrom(OriginalType)
            {
                if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }

            }
            else if (OriginalType.IsImplement_Dictionary() ) // typeof(IDictionary).IsAssignableFrom(OriginalType))
            {
                if (Arg2Type != null && Arg1Type != null)//Length ==2
                {
                    baseName = OriginalType.Name.Replace("`2", "");
                    return baseName + "_" + Arg1Type.Name + "_" + Arg2Type.Name;
                }
                else if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }
            }
            else if (OriginalType.IsClass) // Generic not supported by Serializer
            {
                baseName = OriginalType.Name;
                return baseName;
            }

            return OriginalType.Name;
        }



        /// <summary>
        /// Type Name - flat name that can be used in Debug Code generation for any type class name(generic collection/array/ nullable struct).
        /// </summary>
        /// <param name="sometype"></param>
        /// <returns></returns>
        public static string GetConventionTypeName(Type sometype)
        {
            if (sometype == null) throw new ArgumentNullException("sometype");

            var code = sometype.GetHashCode();
            if (Get(sometype).State == TypeFoundStateEn.TypeFound)
            {
                return Get(sometype).TypeDebugCodeConventionName;
            }
            return null;           
        }

        #endregion  ------------------------- TypeDebugCodeConventionName && BUILD IT && GET IT ------------------------



        #region --------------------------------- ACCELERATORS --------------------------------
        
        /// <summary>
        /// Gets a value indicating whether this is Nullable{} generic type(Nullable struct type), or  array's element type  is Nullable{} type.
        /// </summary>
        /// <returns>  True, if the type parameter is a closed generic nullable type; otherwise, False. </returns>
        /// <remarks>Arrays of Nullable types are treated as Nullable types.</remarks>
        public static bool GetIsNullableType(Type targetType)
        {       
            return (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == Tps.T_NullableGen);
        }


        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types values can be setted by null Value .  They are - classes  , interface , nullable structs
        /// <para/>    2 category - these Types values can't be setted by null Value. They are - structs 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNullSettableType(Type targetType)
        {
            if ( targetType.IsClass 
               || targetType.IsInterface
               || (targetType.IsValueType && targetType.IsNullable())  //!
              )
            {
                return true;
            }
            else if (  targetType.IsValueType) //for structs only - 2 -types category
            {
                return false;
            }

            throw new NotSupportedException($" ERROR in [{nameof(TypeInfoEx)}.{nameof(IsNullSettableType)}()] -  not supported type kind Type[{targetType.FullName}] - to tell if it's instance can be setted by null value.");
        }
       

        static bool Is_ICollectionType(Type[] tpIfaces)
        {
            return IsImplementInterface(tpIfaces, typeof(ICollection));
        }

        /// <summary>
        /// If this targetType imlemented ICollection interface
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool Is_ICollectionType(Type targetType)
        {
            return (targetType.GetInterface(nameof(ICollection)) != null);
        }



        static bool Is_IList_NeedToSureInDataType(Type iListType)
        {
            var argType = Get_IListArgType(iListType);
            return IsNeedToSureInDataType(argType);            
        }


        static bool Is_IDictionary_NeedToSureInKeyArgType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");
            
            var arg1KeyType = Get_IDictionaryArgTypes(iDictionaryType)[0];
            //var arg2Type = iDictionaryType.GetGenericArguments()[1];
            return (IsNeedToSureInDataType(arg1KeyType));
        }

        static bool Is_IDictionary_NeedToSureInValueArgType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");

            var arg2ValueType = Get_IDictionaryArgTypes(iDictionaryType)[1]; 
            //var arg2Type = iDictionaryType.GetGenericArguments()[1];
            return (IsNeedToSureInDataType(arg2ValueType));
        }


        static bool Is_IDictionary_NeedToSureInDataType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");

            var isKeyNeedToSoreInDataType = Is_IDictionary_NeedToSureInKeyArgType(iDictionaryType);
            var isValueNeedToSoreInDataType = Is_IDictionary_NeedToSureInValueArgType(iDictionaryType);
                        
            return ( isKeyNeedToSoreInDataType || isValueNeedToSoreInDataType);            
        }

        static bool Is_Array_NeedToSureInDataType(Type iArrayType)
        {
            var argType = iArrayType.GetElementType();
            return IsNeedToSureInDataType(argType);
        }


        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types instances can have  different value type than it's declaration type(with the help of inheritance) .  They are - not sealed classes , interface , nullable structs, enum
        /// <para/>    2 category -instances can have  different value type than it's declaration type(can't be inherited) . They are - not sealed classes and structs
        /// <para/>   NOTE : Arrays, Lists ,Dictionaries checking by their argTypes
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNeedToSureInDataType(Type targetType)
        {            
            
            // Rules for Arrays \ List \ Dictionary
            if (targetType.IsArray)
            {
                return Is_Array_NeedToSureInDataType(targetType);
            }

            // firstly we checking  collection types with their args
            else if ( targetType.IsImplement_List() )                
            {
                return Is_IList_NeedToSureInDataType(targetType);
            }
            else if ( targetType.IsImplement_Dictionary() )
            {
                return Is_IDictionary_NeedToSureInDataType(targetType);
            }

            // if type is nullable we also need to sure in this type value
            //if (targetType.Is4DPrimitiveType() && targetType.IsNullable()) return false;

            //some other custom generic type - all not nullable/and nullable primitives
            if (targetType.Is4DPrimitiveType()) return false;


            if (targetType.IsEnum && targetType != typeof(Enum)  &&  !targetType.IsNullable() ) { return false;  } // needn't  to reread target Type - this enum is fully info

            //here we can be sure that this is - custom not generic type
            if ( (targetType.IsClass && !targetType.IsSealed)//for object here
                   || targetType.IsInterface
                   || (targetType.IsValueType && !targetType.IsNullable() )
                   
               ) //for not sealed classes, interface , nullable structs - 1- types category
            {
                return true;
            }
            else if ( (targetType.IsClass &&  targetType.IsSealed)
                      || targetType.IsValueType
                    ) //for not sealed classes and structs - 2 -types category
            {
                return false;
            }

            throw new NotSupportedException($" ERROR in [{nameof(TypeInfoEx)}.{nameof(IsNeedToSureInDataType)}()] -  not supported type kind Type[{targetType.FullName}]- to tell if it's instance can have  different value type than in declaration.");

        }




        /// <summary>
        /// If inputType is Nullable{TStruct} or it can be written like TStruct?,  then  return  Underlying TStruct type
        /// <para/>   If inputType is not Nullable{TStruct} then return inputType again.
        /// </summary>
        /// <param name="inputType"></param>
        /// <returns></returns>
        public static Type GetWorkingTypeFromNullableType(Type inputType)
        {
            if (GetIsNullableType(inputType)) // really is nullable type
            {
                return Nullable.GetUnderlyingType(inputType);
            }
            return inputType;//not nullable type 
        }

        /// <summary>
        /// 4D Default serializable Collection Type :
        /// <para/> - Array
        /// <para/> - IList based collection type
        /// <para/> - IList{T}  based collection type
        /// <para/> - IDictionary{T}  based collection type
        /// <para/> - IDictionary based collection type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static  CollectionTypeEn GetBaseCollectionType( Type type)
        {
            if (type == null) throw new ArgumentNullException("type");

            if (type.IsArray)
            {
                return CollectionTypeEn.Array;
            }
            else if (type.IsObservableCollection())
            {
                return CollectionTypeEn.ObservableCollection;
            }
            else if (type.IsImplement_List())
            {
                return CollectionTypeEn.IList;
            }
            else if (type.IsImplement_Dictionary())
            {
                return CollectionTypeEn.IDictionary;
            }
            
            else return CollectionTypeEn.NotCollection;
        }

        static CollectionTypeEn GetBaseCollectionType(TypeInfoEx tpInfoEx)
        {
            if (tpInfoEx == null) throw new ArgumentNullException("tpInfoEx");
            if (tpInfoEx.IsArray)
            {
                return CollectionTypeEn.Array;
            }
            else if (tpInfoEx.IsObservableCollection)
            {
                return CollectionTypeEn.ObservableCollection;
            }
            else if (tpInfoEx.IsIListImplemented)
            {
                return CollectionTypeEn.IList;
            }
            else if (tpInfoEx.IsIDictionaryImplemented)
            {
                return CollectionTypeEn.IDictionary;
            }

            else return CollectionTypeEn.NotCollection;
        }




            /// <summary>
            /// Is type based on System.Delegate class. It can be in 2 cases: 
            /// <para/> -1 when type is simply Delegate member type or 
            /// <para/> -2 when type is event member type.  
            /// </summary>
            /// <param name="type"></param>
            /// <returns></returns>
            public static bool IsDelegateOrEvent( Type type)
        {
            if (type.IsNull() ) throw new ArgumentNullException("type");
            return type.IsSubclassOf(typeof(Delegate));
        }


        /// <summary>
        /// When Type has generic parameters, 
        /// and is not derive from one of the  SerializableCollections. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsSerializableType(Type type)
        {
            if ( type.IsNull()  ) throw new ArgumentNullException("type");

            if ( type.GetGenericArguments().Length > 0
                &&  GetBaseCollectionType(type) != CollectionTypeEn.NotCollection)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 4D Type test -  Type activation by its default ctor possibility. 
        ///  By default ALL types can be tested,but only Array types don't .
        /// <para/> Problem here is that some Types can have internal static increment logic on creation by Default Ctor.
        /// <para/>  - so we'll exclude them from Default Ctor testing.        
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsTestableByDefCtorType( Type type)
        {
            if ( type.IsNull() ) throw new ArgumentNullException("type");
            
            if ( type.IsArray  )
            {
                return false;
            }
            return true;
        }


        //        /// <summary>
        //        /// If this type implement IDisposable.
        //        /// </summary>
        //        /// <param name="type"></param>
        //        /// <returns></returns>
        //        internal static bool IsDisposableType(TypeInfoEx tpInfoEx)
        //        {
        //#if DEBUG
        //            if (tpInfoEx.IsNull()) throw new ArgumentNullException("tpInfoEx");
        //            if (tpInfoEx.Interfaces.IsNull()) throw new ArgumentNullException("Interfaces");
        //#endif
        //            return IsImplementInterface(tpInfoEx.Interfaces,typeof(IDisposable) );
        //        }

        internal static bool IsDisposableType(Type targetType)
        {

            if (targetType.IsNull()) return false;

          return (targetType.GetInterface(nameof(IDisposable)) !=  null);
            
        }


        static bool IsDisposableType(Type[] tpIfaces)
        {
            if (tpIfaces.IsNull() || tpIfaces.Length == 0) return false;

            return IsImplementInterface(tpIfaces, typeof(IDisposable));
        }


        #endregion  --------------------------------- ACCELERATORS --------------------------------



        #region --------------------------------------- ENUM UTILS ------------------------------------



        /// <summary>
        /// Get Enum.Field value's  TAttribute 
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TAttribute GetEnumValueAttribute<TAttribute>(Enum value)
            where TAttribute : Attribute
        {
            FieldInfo field = GetFieldInfo(value);

            return field.GetFirstFieldAttribute<TAttribute>();
            
        }


        /// <summary>
        /// Get FieldInfo of Enum.Field value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FieldInfo GetFieldInfo(Enum value)
        {
            return value.GetType().GetField(value.S());
        }

        #endregion --------------------------------------- ENUM UTILS ------------------------------------






        #region --------------------------------------BCL 4D PRIMITIVE TYPES EXTENSIONS -------------------------
        

        /// <summary>
        /// IsPrimitive - Is this  Type  the Primitive  DDDD.Core  Type.
        ///<para/> [Primitive Type] concept was entered in DDDD.Core to  classify  .NET types in Serialization Processing.
        ///<para/> DDDD.Core lib define an enumeration with all enabled Primitive Types in TypePrimitivesEn.        
        ///<para/> All Primitive Types are enumerated by TypePrimitivesEn values. 
        ///<para/> Each  value of TypePrimitivesEn enum can return Type with what it is associated by GetPrimitiveType().
        ///<para/> Each  value of TypePrimitivesEn enum can return TypCode with what it is associated by GetPrimitiveTypeCode()
        ///<para/>  , if it doesn't have correct TypeCode the value will be TypeCode.Empty .
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool Is4DPrimitiveType( Type type)
        {
            return  Tps.Is4DPrimitive(type);
        }


        /// <summary>
        ///  TypePrimitivesEn enum value will return Primitive .NET Type with what it is associated.
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static Type Get4DPrimitiveType(TypePrimitiveEn enumValue)
        {
            return GetEnumValueAttribute<TypePrimitiveMapAttribute>(enumValue).TargetType;
        }


        /// <summary>
        /// TypePrimitivesEn enum value will return Primitive .NET TypeCode with what it is associated.
        /// <para/> , if it doesn't have correct TypeCode the value will be TypeCode.Empty 
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static TypeCode Get4DPrimitiveTypeCode( TypePrimitiveEn enumValue )
        {
            return GetEnumValueAttribute<TypePrimitiveMapAttribute>(enumValue).Code;
        }

        #endregion --------------------------------------BCL 4D PRIMITIVE TYPES EXTENSIONS -------------------------
      
        //const char QUOTE = '"';
        //const char COLON = ':';
        //const char COMMA = ',';
        //const char OBJ_START = '{';
        //const char OBJ_END = '}';
        //const string FLD_TYPELN = "TypeLN";
        //const string FLD_ASSEMBLY = "Assembly";





    }
}




