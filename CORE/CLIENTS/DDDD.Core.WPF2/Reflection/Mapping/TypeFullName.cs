﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DDDD.Core.Threading;
using DDDD.Core.Extensions;

namespace DDDD.Core.Reflection
{
    /// http://www.codeproject.com/Tips/624300/AssemblyQualifiedName-Parser -article

    /// <summary>
    /// TypeAQName -  parsing   Type.AssemblyQualifiedName string 
    /// </summary>
    public class TypeFullName
    {

        #region ------------------------------------ CTOR ----------------------------------
        
        static TypeFullName()
        {
           // Mapper = new TypeMapper();
        }

        public TypeFullName(string typeFullName)
        {
            //Get String Type Identification:
            //Name              Type's Type.Name 
            //FullName          Type's Type.FullName.
            //AQName            Type.AssemblyQualifiedName.
            //AssemblyFullName  Type.Assembly.FullName.

            Parse(typeFullName);
        }


        #endregion -- --------------------------------- CTOR --------------------------------


        #region ------------------------------------ INTERNALS --------------------------------

        class block
        {
            internal int iStart;
            internal int iEnd;
            internal int level;
            internal block parentBlock;
            internal List<block> innerBlocks = new List<block>();
            internal TypeFullName parseFullName;
        }

        #endregion ------------------------------------ INTERNALS --------------------------------

        
        

        public string FullName { get; private set; }
       
        public string AssemblyDescriptionString { get; private set; }

        public string ShortAssemblyName { get; private set; }

        public string Version { get; private set; }

        public string Culture { get; private set; }

        public string PublicKeyToken { get; private set; }

        public List<TypeFullName> GenericParameters{ get; private set; }  = new List<TypeFullName>();


        public LazyAct<AssemblyName> LA_AssemblyNameDescriptor;

        public LazyAct<Type> LA_FoundType { get; private set; }

        public LazyAct<string> LA_CSharpStyleName { get; private set; }

        public LazyAct<string> LA_VBNetStyleName { get; private set; }


        #region ---------------------------- TypeMapper -----------------------------
        

        ////parse with TypeMapper options        
        //public string AQNameOrigin { get; private set; }

        //public string AQNameCorrectedByMapper { get; private set; }



        ///// <summary>
        ///// Enabled for public - static TypeMapper
        ///// </summary>
        //public static TypeMapper Mapper
        //{
        //    get;
        //    private set;
        //}

        #endregion ---------------------------- TypeMapper -----------------------------



        public string GetTypeAQNameShortVariant()
        {
            return  FullName + "," + ShortAssemblyName;
        }



        internal string LanguageStyle(string prefix, string suffix)
        {
            if (GenericParameters.Count > 0)
            {
                StringBuilder sb = new StringBuilder(FullName.Substring(0, FullName.IndexOf('`')));
                sb.Append(prefix);
                bool pendingElement = false;
                foreach (var param in GenericParameters)
                {
                    if (pendingElement)
                        sb.Append(", ");
                    sb.Append(param.LanguageStyle(prefix, suffix));
                    pendingElement = true;
                }
                sb.Append(suffix);
                return sb.S();
            }
            else
                return FullName;
        }


        static string LookForPairThenRemove(List<string> strings, string Name)
        {
            for (int istr = 0; istr < strings.Count; istr++)
            {
                string s = strings[istr];
                int i = s.IndexOf(Name);
                if (i == 0)
                {
                    int i2 = s.IndexOf('=');
                    if (i2 > 0)
                    {
                        string ret = s.Substring(i2 + 1);
                        strings.RemoveAt(istr);
                        return ret;
                    }
                }
            }
            return null;
        }

       

        void Parse(string typeFullName)
        {
            //without lazy
            //Name = TargetType.Name;
            //FullName = TargetType.FullName;
            //AQName = TargetType.AssemblyQualifiedName;
            //this.AssemblyFullName = TargetType.Assembly.FullName;
            FullName = typeFullName;
            
            //if (Mapper.UseConventionsProcessing)
            //{
            //    AQNameCorrectedByMapper = Mapper.ProcessTypeNameByConventions(AQNameOrigin);
            //}
            //if (Mapper.UseTypeFullNameOnlyCompareMapping)
            //{
            //    //AQNameAferMapperCorrections = TypeInfoEx.Mapper.GetCustomType ProcessTypeNameByConventions(AQNameAferMapperCorrections);
            //}


            int index = -1;
            block rootBlock = new block();
            {
                int bcount = 0;
                block currentBlock = rootBlock;
                for (int i = 0; i < FullName.Length; ++i)
                {
                    char c = FullName[i];
                    if (c == '[')
                    {
                        if (FullName[i + 1] == ']') // Array type.
                        { i++; }
                        else if (FullName[i + 1] == ',' && FullName[i + 2] == ']') //Array 2Dim
                        { i += 2; }
                        else if (FullName[i + 1] == ',' && FullName[i + 2] == ',' && FullName[i + 3] == ']') //Array 3Dim
                        { i += 3; }
                        else if (FullName[i + 1] == ',' && FullName[i + 2] == ',' && FullName[i + 3] == ',' && FullName[i + 4] == ']') //Array 4Dim
                        { i += 4; }


                        else
                        {//subtypes
                            ++bcount;
                            var b = new block() { iStart = i + 1, level = bcount, parentBlock = currentBlock };
                            currentBlock.innerBlocks.Add(b);
                            currentBlock = b;
                        }
                    }
                    else if (c == ']')
                    {
                        currentBlock.iEnd = i - 1;
                        if (FullName[currentBlock.iStart] != '[')
                        {
                            currentBlock.parseFullName = new TypeFullName(FullName.Substring(currentBlock.iStart, i - currentBlock.iStart));
                            if (bcount == 2)
                                GenericParameters.Add(currentBlock.parseFullName);
                        }
                        currentBlock = currentBlock.parentBlock;
                        --bcount;
                    }
                    else if (bcount == 0 && c == ',')
                    {
                        index = i;
                        break;
                    }
                }
            }

            FullName = FullName.Substring(0, index);

            LA_CSharpStyleName = LazyAct<string>.Create(
                (args) =>
                {
                    return LanguageStyle("<", ">");
                }
                , null //locker
                , null); //arguments


            LA_VBNetStyleName = LazyAct<string>.Create(
                (args) =>
                {
                    return LanguageStyle("(Of ", ")");
                }
                , null //locker
                , null); //arguments


            // get AssemblyDescriptionString
            AssemblyDescriptionString = FullName.Substring(index + 2);            
            {
                List<string> parts = AssemblyDescriptionString.Split(',')
                                                                 .Select(x => x.Trim())
                                                                 .ToList();
                Version = LookForPairThenRemove(parts, "Version");
                Culture = LookForPairThenRemove(parts, "Culture");
                PublicKeyToken = LookForPairThenRemove(parts, "PublicKeyToken");
                if (parts.Count > 0)
                    ShortAssemblyName = parts[0];
            }

            //create AssemblyName
            LA_AssemblyNameDescriptor =  LazyAct<AssemblyName>.Create(
                (args) =>
                { return new AssemblyName(AssemblyDescriptionString);
                }
                , null //locker
                , null); //arguments


            //try search FoundType
            LA_FoundType = LazyAct<Type>.Create(
                (args) =>
                {
                    //1 nearest type search is  TypeCache
                    var searchedType = TypeCache.GetCustomDomainType(AssemblyDescriptionString, FullName);
                    
                    if (searchedType != null)
                        return searchedType;                    

                    //3 global Type's type search
                    searchedType = Type.GetType(FullName);
                    if (searchedType != null)
                        return searchedType;
                     

                    return null; // Not found.
                }
                , null //locker
                , null); //arguments
        }


        /// <summary>
        /// Try to Parse AssemblyQualifiedName. 
        /// If some error happens then null will be returned.
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        public static TypeAQName TryParse(string assemblyQualifiedName)
        {
            try
            {
                return new TypeAQName(assemblyQualifiedName);
            }
            catch (Exception)
            {
                return null;                
            }            
        }

         

        // Makes debugging easier.
        public override string ToString()
        {
            return LA_CSharpStyleName.S();
        }



    }

}
