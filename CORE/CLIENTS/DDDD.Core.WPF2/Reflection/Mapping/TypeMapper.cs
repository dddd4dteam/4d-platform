﻿using DDDD.Core.Diagnostics;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DDDD.Core.Reflection
{


    /// <summary>
    /// TypeMapper - Cross-Sides Type determining  engine.  
    /// <para/> TypeMapper instance always exist as TypeAQName.Mapper, so it can't be create manually. 
    /// <para/> We use TypeMapper to get end .NET type by some string-[Assembly Qualified Name] of type name but from different App side:
    /// <para/>     like on client from server and  on server from client.     
    /// <para/> To do Type Determination by string we use ConventionRules- rules of what to replace and try to determine in that case.
    /// </summary>
    public class TypeMapper
    {
        
	     /// <summary>
         ///  Target Class - TypeMapper -  Ctor
         /// </summary>
        internal TypeMapper( )
        {  
              ReplacementRules = new Dictionary<int, ReplacementRule>();
              InitSideDefaultConventionRules();
        }
       
    
        

        #region ------------------ FIELDS----------------------

        // predefined known type mirrors
        private const string OBSERVABLE_COLLECTION = "System.Collections.ObjectModel.ObservableCollection";
        private const string DRAWING = "System.Drawing";


        private const string VERSION_REGEX_STRING = @"\,\sVersion=\d+\.\d+\.\d+\.\d+\,\sCulture=[^\,]+\,\sPublicKeyToken=\w+";
#if SL5
        private static Regex _versionRegex = new Regex(VERSION_REGEX_STRING);
#else
        private static Regex _versionRegex = new Regex(VERSION_REGEX_STRING, RegexOptions.Compiled);
#endif

        #endregion ------------------ FIELDS----------------------


        #region -------------------------- PROPERTIES-------------------------



        /// <summary>
        /// This Replacements can be used for example - to Search type by its FullTypeName not in Assembly1.Server but in Assembly1.SL5-
        /// <para/>  -it can be done if you add Replacement{".Server",".SL5"}. It is usual way if you share one class between some .Net ports - and so assemblies
        /// <para/> By default ConventionReplacement usualy have value for each .NET target assemblies of UniversalSerializer  : 
        /// <para/>   For SL5 - {".Server", ".SL5"} - we beleave that you can use on SL5 contract that was serialized on Server side.
        /// <para/>   For WP8 - {".Server", ".WP8"} - we beleave that you can use on WinPhone8 contract that was serialized on Server side.
        /// <para/>   For SL5 - {".Server", ".WPF"} - we beleave that you can use on WinPresentationFoundation contract that was serialized on Server side.
        /// <para/>   For SL5 - {".Server", ".WF"} - we beleave that you can use on WindowsForms contract that was serialized on Server side.
        /// </summary>
        public Dictionary<Int32, ReplacementRule> ReplacementRules
        {
            get;
            private set;
        }



        /// <summary>
        /// If we need to do Convention based replacements in TypeFullName on Getting Type/ or not.
        /// <para/> We can change this flag into true- and use ConventionReplacements / or set to false-to use original  FullTypeName.
        /// <para/> By default Tools do not use Convention replacements. 
        /// </summary>
        public bool UseConventionsProcessing
        {
            get;
            set;
        }


        /// <summary>
        /// If type not found by full AssemblyQualifiedName we can turn on this option to do search by fullname only from existed types, 
        /// <para/> and then save this as mapped info.
        /// <para/> By default this option is turned off. 
        /// </summary>
        public bool UseTypeFullNameOnlyCompareMapping
        {
            get;
            set;
        }


        #endregion -------------------------- PROPERTIES-------------------------
        



         void InitSideDefaultConventionRules()
        {

#if SERVER
            AddConventionRule(null, ".SL5", ".Server");
            AddConventionRule(null, ".WP8", ".Server");
            AddConventionRule(null, ".WPF", ".Server");
            AddConventionRule(null, ".WinForms", ".Server");
            AddConventionRule(OBSERVABLE_COLLECTION, VERSION_REGEX_STRING, "");
            AddConventionRule(DRAWING, VERSION_REGEX_STRING, "");


#elif CLIENT && SL5
             AddConventionRule(null, ".Server",".SL5");
                                               
#elif CLIENT && WP8
             AddConventionRule(null, ".Server",".WP8");
            
#elif CLIENT && WPF
            AddConventionRule(null, ".Server",".WPF");
                        
#elif CLIENT && WinForms
            AddConventionRule(null, ".Server",".WinForms");

#endif



        }




        /// <summary>
        /// FullTypeName building.  Adding new Convention Based Replacement 
        /// </summary>
        /// <param name="existedSuffix"></param>
        /// <param name="newSuffix"></param>
        public void AddConventionRule(String containsValue, String RegexReplaceTempalte, String ConventionReplaceTarget)
        {
            //Validator.NotNullDbg(RegexReplaceTempalte, x => x.AddConventionRule,"RegexReplaceTempalte", null);     //, "Convention Rule replace template cannot be null ");
            //Validator.NotNullDbg(ConventionReplaceTarget,()=> AddConventionRule,"ConventionReplaceTarget", null);
            
            var newConventionRegex = new Regex(
               RegexReplaceTempalte,
             RegexOptions.CultureInvariant
#if NET45
 | RegexOptions.Compiled
#endif
);

            var rule = ReplacementRule.Create(containsValue, newConventionRegex, ConventionReplaceTarget);

            ReplacementRules.Add(rule.ID, rule);
        }





        /// <summary>
        /// Convention based AssemblyQualifiedName after - after made all Convention Replacements
        /// </summary>
        /// <param name="primaryAssemblyQualifiedName"></param>
        /// <returns></returns>
        public string ProcessTypeNameByConventions(String primaryFullTypeName)
        {
            //string resultAssemblyQN = primaryAssemblyQualifiedName;
            foreach (var rule in ReplacementRules)
            {
                primaryFullTypeName = rule.Value.TryReplace(primaryFullTypeName);

            }
            return primaryFullTypeName;
        }




        /// <summary>
        /// FullTypeName building. Remove existed Convention Based Replacement
        /// </summary>
        /// <param name="ConventionConstKey"></param>
        public void RemoveConventionRule(Int32 IdConventionRule)
        {
            if (ReplacementRules.ContainsKey(IdConventionRule))
            {
                ReplacementRules.Remove(IdConventionRule);
            }

        }














        #region --------------------------1 LEVEL MAPPING TOOLS - DETERMINE CONTRACT BY TYPENAME----------------------------------------


        Type TryGetTypeByUseConventionsProcessing(string typeName)
        {
            Type type = null;
            if (UseConventionsProcessing)  ////  not generic but still not known - from another side for example
            {
                string conventionTypeName = null;
                conventionTypeName = ProcessTypeNameByConventions(typeName);
                type = Type.GetType(conventionTypeName, false);

            }

            return type;
        }



        Type TryGetTypeByUseTypeFullNameOnly(string typeFullName)
        {
            Type type = null;
            if (UseTypeFullNameOnlyCompareMapping)//try compare only by fullName to  search Processing info and save maped result 
            {
                var fullTypeNameOnly = typeFullName.Split(',').FirstOrDefault();

                type = Type.GetType(fullTypeNameOnly);
            }

            return type;
        }


        #endregion --------------------------1 LEVEL MAPPING TOOLS - DETERMINE CONTRACT BY TYPENAME----------------------------------------



        #region --------------------------------2 LEVEL MAPPING TOOLS - DETERMINE GENERIC COLLECTIONS BY TYPENAME --------------------------------------



        // criterions
        const string criterionIsArray1Rank = "[]";
        const string criterionIsArray2Rank = "[,]";
        const string criterionIsArray3Rank = "[,,]";

        const string criterionIsListCollection = "System.Collections.Generic.List`1[[";
        const string criterionIsDictionaryCollection = "System.Collections.Generic.Dictionary`2[[";
        const string criterionIsObservableCollection = "System.Collections.ObjectModel.ObservableCollection`1[[";

        ////----------------------regex to get generic Args ---------------------------
        /// <summary>
        ///  Regular expression - TypeWith 1 GenericArg - this 1ArgType
        /// </summary>
        public static Regex regex_TypeWith1GenericArg_ArgType = new Regex(
              @"(?<=\[\[).*(?=\]\])",
            RegexOptions.CultureInvariant
#if NET45
 | RegexOptions.Compiled
#endif
);


        /// <summary>
        ///  Regular expression - TypeWith 2 GenericArg - this 1ArgType
        /// </summary>
        public static Regex regex_TypeWith2GenericArg_1ArgType = new Regex(
              @"(?<=\[\[).*(?=\]\,\[)",
            RegexOptions.CultureInvariant
#if NET45
 | RegexOptions.Compiled
#endif
);

        /// <summary>
        ///  Regular expression - TypeWith 2 GenericArg - this 2ArgType
        /// </summary>
        public static Regex regex_TypeWith2GenericArg_2ArgType = new Regex(
              @"(?<=\]\,\[).*(?=\]\])",
            RegexOptions.CultureInvariant
#if NET45
 | RegexOptions.Compiled
#endif
);





        internal Type DetermineTypeIsArray1RankCollection(String typeName)//, ITypeSetSerializer HostSerializer
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsArray1RankType = typeName.Contains(criterionIsArray1Rank);

            if (IsArray1RankType)
            {
                string arrayOf_TypeName = typeName.Replace(criterionIsArray1Rank, "");
                Type arrayArgType = Type.GetType(arrayOf_TypeName, false);

                //null it again if it doesn't exist in HostSerializer - and so null return
                if (arrayArgType != null) { return arrayArgType.MakeArrayType(); }

                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseConventionsProcessing(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(); //if it was determined and exist in Serializer return it 
                }

                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseTypeFullNameOnly(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(); //if it was determined 
                }

            }

            return null;
        }


        internal  Type DetermineTypeIsArray2RankCollection(String typeName) //, ITypeSetSerializer HostSerializer
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsArray2RankType = typeName.Contains(criterionIsArray2Rank);

            if (IsArray2RankType)
            {
                string arrayOf_TypeName = typeName.Replace(criterionIsArray2Rank, "");
                Type arrayArgType = Type.GetType(arrayOf_TypeName, false);
                //ushort? arrayArgTypeId = HostSerializer.GetContainsContract(arrayArgType);

                //null it again if it doesn't exist in HostSerializer - and so null return
                if (arrayArgType != null) { return arrayArgType.MakeArrayType(2); }

                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseConventionsProcessing(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(2); //if it was determined 
                }

                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseTypeFullNameOnly(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(2); //if it was determined 
                }

            }
            return null;
        }


        internal Type DetermineTypeIsArray3RankCollection(String typeName)
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsArray3RankType = typeName.Contains(criterionIsArray3Rank);

            if (IsArray3RankType)
            {
                string arrayOf_TypeName = typeName.Replace(criterionIsArray3Rank, "");
                Type arrayArgType = Type.GetType(arrayOf_TypeName, false);


                //null it again if it doesn't exist in HostSerializer - and so null return
                if (arrayArgType != null) { return arrayArgType.MakeArrayType(3); }


                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseConventionsProcessing(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(3); //if it was determined
                }

                //if arrayArgType still was null
                if ((arrayArgType = TryGetTypeByUseTypeFullNameOnly(arrayOf_TypeName)) != null)
                {
                    return arrayArgType.MakeArrayType(3); //if it was determined 
                }


            }
            return null;
        }


        internal  Type DetermineTypeIsListCollection(string typeName) //, ITypeSetSerializer HostSerializer
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsListCollectionType = typeName.Contains(criterionIsListCollection);

            if (IsListCollectionType)
            {
                string ListOf_ArgTypeName = regex_TypeWith1GenericArg_ArgType.Match(typeName).Value;//
                Type ListOf_ArgType = Type.GetType(ListOf_ArgTypeName, false);


                //null it again if it doesn't exist in HostSerializer - and so null return
                if (ListOf_ArgType != null) return typeof(List<>).MakeGenericType(ListOf_ArgType);


                //if ListOf_ArgType still was null
                if ((ListOf_ArgType = TryGetTypeByUseConventionsProcessing(ListOf_ArgTypeName)) != null)
                {
                    return typeof(List<>).MakeGenericType(ListOf_ArgType); //if it was determined 
                }

                //if ListOf_ArgType still was null
                if ((ListOf_ArgType = TryGetTypeByUseTypeFullNameOnly(ListOf_ArgTypeName)) != null)
                {
                    return typeof(List<>).MakeGenericType(ListOf_ArgType); //if it was determined 
                }

                // at this point - yes ListOf_ArgType was not found or doesn't exist in HostSerializer

            }

            return null;
        }


        internal  Type DetermineTypeIsObservableCollection(String typeName) //, ITypeSetSerializer HostSerializer
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsObservableCollectionType = typeName.Contains(criterionIsObservableCollection);

            if (IsObservableCollectionType)
            {
                string ObservableCollectionOf_ArgTypeName = regex_TypeWith1GenericArg_ArgType.Match(typeName).Value;//
                Type ObservableCollectionOf_ArgType = Type.GetType(ObservableCollectionOf_ArgTypeName, false);

                //null it again if it doesn't exist in HostSerializer - and so null return
                if (ObservableCollectionOf_ArgType != null) { return typeof(ObservableCollection<>).MakeGenericType(ObservableCollectionOf_ArgType); }

                //if arrayArgType still was null
                if ((ObservableCollectionOf_ArgType = TryGetTypeByUseConventionsProcessing(ObservableCollectionOf_ArgTypeName)) != null)
                {
                    return typeof(ObservableCollection<>).MakeGenericType(ObservableCollectionOf_ArgType); //if it was determined 
                }

                //if arrayArgType still was null
                if ((ObservableCollectionOf_ArgType = TryGetTypeByUseTypeFullNameOnly(ObservableCollectionOf_ArgTypeName)) != null)
                {
                    return typeof(ObservableCollection<>).MakeGenericType(ObservableCollectionOf_ArgType); //if it was determined 
                }

                // at this point - yes ObservableCollectionOf_ArgType was not found or doesn't exist in HostSerializer

            }
            return null;
        }


        internal  Type DetermineTypeIsDictionaryCollection(String typeName)
        {
            //get arguments and Validate check if exist in HostSerializer
            bool IsDictionaryType = typeName.Contains(criterionIsDictionaryCollection);

            if (IsDictionaryType)
            {
                Type DictionaryOf_1ArgType = null;
                Type DictionaryOf_2ArgType = null;


                //first arg searching
                string DictionaryOf_1ArgTypeName = regex_TypeWith2GenericArg_1ArgType.Match(typeName).Value;//
                DictionaryOf_1ArgType = Type.GetType(DictionaryOf_1ArgTypeName, false);


                //if first arg still was null
                if (DictionaryOf_1ArgType == null) { DictionaryOf_1ArgType = TryGetTypeByUseConventionsProcessing(DictionaryOf_1ArgTypeName); }

                //if first arg still was null
                if (DictionaryOf_1ArgType == null) { DictionaryOf_1ArgType = TryGetTypeByUseTypeFullNameOnly(DictionaryOf_1ArgTypeName); }

                // at this point - yes if  DictionaryOf_1ArgType still was not found or doesn't exist in HostSerializer return null;
                if (DictionaryOf_1ArgType == null) return null;


                //second arg searching
                string DictionaryOf_2ArgTypeName = regex_TypeWith2GenericArg_2ArgType.Match(typeName).Value;//
                DictionaryOf_2ArgType = Type.GetType(DictionaryOf_2ArgTypeName, false);


                //null it again if it doesn't exist in HostSerializer - and so null return
                if (DictionaryOf_2ArgType != null)
                { return typeof(Dictionary<,>).MakeGenericType(DictionaryOf_1ArgType, DictionaryOf_2ArgType); }


                //if second arg  still was null
                if ((DictionaryOf_2ArgType = TryGetTypeByUseConventionsProcessing(DictionaryOf_2ArgTypeName)) != null)
                { return typeof(Dictionary<,>).MakeGenericType(DictionaryOf_1ArgType, DictionaryOf_2ArgType); } //if it was determined


                //if second arg still was null
                if ((DictionaryOf_2ArgType = TryGetTypeByUseTypeFullNameOnly(DictionaryOf_2ArgTypeName)) != null)
                {
                    return typeof(Dictionary<,>).MakeGenericType(DictionaryOf_1ArgType, DictionaryOf_2ArgType); //if it was determined
                }

                // at this point - yes  Dictionary<DictionaryOf_1ArgType, DictionaryOf_2ArgType > was not found or doesn't exist in HostSerializer

            }
            return null;
        }


        #endregion--------------------------------2 LEVEL MAPPING TOOLS - DETERMINE GENERIC COLLECTIONS BY TYPENAME --------------------------------------


        #region ------------------------------ GET Type by Type.AssemblyQualifiedName  ----------------------------------

        /// <summary>
        /// Get Custom type - get Type by String [AsemblyQualifiedName]
        /// </summary>
        /// <param name="typeAQName"></param>
        /// <param name="errorMessage"></param>
        /// <param name="HostSerializer"></param>
        /// <returns></returns>
        internal Type GetCustomType(string typeAQName) 
        {
            //  process by conventions - and try to look throw in ByName dictionary          
            
            Type type = null; //start search type by combinations
            if (typeAQName.Contains("`")) // For Generic Types.  not exist in Handlers dictionaries. add unknown generic type Handler. 
            {                           // It mostly used For mapping Unknown generic types from other side  

                try
                {
                    type = Type.GetType(typeAQName, false);

                    if (type != null) return type;

                }
                catch (Exception)
                {
                    type = null;
                }

                //for generic types - for collections - some other mechanism to determine type and make  mapping
                //if collection type not exist in  Serializer.TypeDictionary
                //1 -  add it as auto collection type into Serializer.TypeDictionary
                //2 -  add mapping info about input typename into Serializer.MappingDictionary


                if ((type = DetermineTypeIsArray1RankCollection(typeAQName)) != null)
                {
                    return type;
                }
                else if ((type = DetermineTypeIsArray2RankCollection(typeAQName)) != null)
                {
                    return type;
                }
                else if ((type = DetermineTypeIsArray3RankCollection(typeAQName)) != null)
                {
                    return type;
                }
                else if ((type = DetermineTypeIsListCollection(typeAQName)) != null)
                {
                    return type;
                }
                else if ((type = DetermineTypeIsObservableCollection(typeAQName)) != null)
                {
                    return type;
                }
                else if ((type = DetermineTypeIsDictionaryCollection(typeAQName)) != null)
                {
                    return type;
                }


                Validator.AssertTrue<TypeIsNotDeterminedException>(type == null,
                    " GetCustomType(): Searching for Generic Contract [{0}] : Result - NOT DETERMINED. So TypeProcessor  for such Contract  unable to be load to use from Serializer Contract Dictionary.",
                                                                 typeAQName);



            }
            else // for non generic typeName it's much simple
            {    // non generic types cannot be added automatically on GetType - only on BuildingTypeProcessor 

                if ((type = TryGetTypeByUseConventionsProcessing(typeAQName)) != null) return type; // not generic but still not known - from another side for example

                if ((type = TryGetTypeByUseTypeFullNameOnly(typeAQName)) != null) return type;        //not generic but still not known

                Validator.AssertTrue<TypeIsNotDeterminedException>(type == null,
                    " GetCustomType(): Searching for Contract [{0}] : Result - NOT DETERMINED. So TypeProcessor  for such Contract  unable to be load to use from Serializer Contract Dictionary.",
                                                                   typeAQName);

            }

            return type; //null value 
        }


        #endregion ------------------------------ GET Type by Type.AssemblyQualifiedName  ----------------------------------



    }
}



#region -------------------------------------    GARBAGE ---------------------------------------



#region ------------------------------ GET Type by Type.AssemblyQualifiedName  ----------------------------------

/// <summary>
/// Get Custom type - for each ITypeSetSerializer individually with it's own Mapping options
/// </summary>
/// <param name="typeName"></param>
/// <param name="errorMessage"></param>
/// <param name="HostSerializer"></param>
/// <returns></returns>
//internal static Type GetCustomType(string typeName, string errorMessage, ITypeSetSerializer HostSerializer)
//{
//    // 1 level search - look throw in ByName dictionary
//    // 2 level search -  look throw in Variations Names dictionary
//    // 3 level process by conventions - and try to look throw in ByName dictionary
//    if (HostSerializer.ProcessingsByName.ContainsKey(typeName))
//    {
//        return HostSerializer.ProcessingsByName[typeName].Contract;
//    }
//    else if (HostSerializer.ProcessingsByMappedName.ContainsKey(typeName))
//    {
//        return HostSerializer.ProcessingsByMappedName[typeName].Contract;
//    }


//    Type type = null; //start search type by combinations
//    if (typeName.Contains("`")) // For Generic Types.  not exist in Handlers dictionaries. add unknown generic type Handler. 
//    {                           // It mostly used For mapping Unknown generic types from other side  

//        try
//        {
//            type = Type.GetType(typeName, false);
//        }
//        catch (Exception)
//        {
//            type = null;
//        }

//        //for generic types - for collections - some other mechanism to determine type and make  mapping
//        //if collection type not exist in  Serializer.TypeDictionary
//        //1 -  add it as auto collection type into Serializer.TypeDictionary
//        //2 -  add mapping info about input typename into Serializer.MappingDictionary


//        if ( (type = DetermineTypeIsArray1RankCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }
//        else if(  (type = DetermineTypeIsArray2RankCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }
//        else if(  (type = DetermineTypeIsArray3RankCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }
//        else if(  (type = DetermineTypeIsListCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }
//        else if(  (type = DetermineTypeIsObservableCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }
//        else if(  (type = DetermineTypeIsDictionaryCollection(typeName, HostSerializer))  != null)
//        {   TryAddAutoCollectionContractToSerializer(type, HostSerializer); 
//        }


//        Validator.AssertTrue<ContractNotFoundException>(type == null,
//            " GetCustomType(): Searching for Generic Contract [{0}] : Result - NOT DETERMINED. So TypeProcessor  for such Contract  unable to be load to use from Serializer Contract Dictionary.",
//                                                         typeName);

//        //add new map for typeName to type -TypeProcessor
//        AddMappedTypeToSerializer(type, typeName, HostSerializer);

//    }
//    else // for non generic typeName it's much simple
//    {    // non generic types cannot be added automatically on GetType - only on BuildingTypeProcessor 

//        if ((type = TryGetTypeByUseConventionsProcessing(typeName, HostSerializer)) != null) return type; // not generic but still not known - from another side for example

//        if ((type = TryGetTypeByUseTypeFullNameOnly(typeName, HostSerializer)) != null) return type;        //not generic but still not known

//        Validator.AssertTrue<ContractNotFoundException>(type == null,
//            " GetCustomType(): Searching for Contract [{0}] : Result - NOT DETERMINED. So TypeProcessor  for such Contract  unable to be load to use from Serializer Contract Dictionary.",
//                                                           typeName);

//        AddMappedTypeToSerializer(type, typeName, HostSerializer);

//    }

//    return type; //null value 
//}

#endregion ------------------------------ GET Type by Type.AssemblyQualifiedName  ----------------------------------



#region ---------------------------------- ADD MAPPED AND AUTOCOLLECTION CONTRACTS TO SERIALIZER WITH LOCKS -----------------------------------


//static void TryAddAutoCollectionContractToSerializer(Type type) //, ITypeSetSerializer HostSerializer
//{
//    ushort typeId = 0;
//    if (  HostSerializer.GetContainsContract(type)== null   ) return; //do not add if it's already exist


//    //Generic type(Most probably  generic Collection) not null now - add it  serializer Processing Dictionary
//    if (typeof(IList).IsAssignableFrom(type)) //just the same for any collection of IList - ObservableCollections
//    {
//        lock (HostSerializer.OpenLock)
//        {
//            //HostSerializer.AddAutoContract(
//            //                            TypeProcessor.Create(type,
//            //                            TypeProcessingPolicyEn.DefaultPolicy,
//            //                            true, false,
//            //                            ITypeSetSerializer.DH_Short.Serialize_List,
//            //                            ITypeSetSerializer.DH_Short.DeserializeList)
//            //                            );

//        }

//    }
//    else if (typeof(IDictionary).IsAssignableFrom(type))
//    {
//        lock (HostSerializer.OpenLock)
//        {
//            //HostSerializer.AddAutoContract(
//            //                            TypeProcessor.Create(type,
//            //                            TypeProcessingPolicyEn.DefaultPolicy,
//            //                            true, false,
//            //                            ITypeSetSerializer.DH_Short.Serialize_Dictionary,
//            //                            ITypeSetSerializer.DH_Short.DeserializeDictionary)
//            //                            );
//        }

//    }
//}


//static void AddMappedTypeToSerializer(Type determinedType, String TypeMappingName, ITypeSetSerializer HostSerializer)
//{
//    if (!HostSerializer.ProcessingsByMappedName.ContainsKey(TypeMappingName))
//    {
//        lock (HostSerializer)
//        {
//            if (!HostSerializer.ProcessingsByMappedName.ContainsKey(TypeMappingName))
//            {
//                HostSerializer.ProcessingsByMappedName.Add(TypeMappingName, HostSerializer.ProcessingsByContract[determinedType]);
//            }
//        }
//    }
//}


#endregion ---------------------------------- ADD MAPPED AND AUTOCOLLECTION CONTRACTS TO SERIALIZER WITH LOCKS -----------------------------------
    


#region ------------ Singleton  Constructor -----------------------
//
// PARAMS :     
// Class   -         TypeMapper       EX: SystemDispatcher - Singleton Class Name           
// ClassDesc  -      ClassDesc        EX: System Dispatcher
/// <summary>
/// TypeMapper singleton instance. TypeMapper - Cross-Sides Type determining engine.
///  We can use TypeMapper to get end .NET type by some string of type name but from different side:
///     like on client from server and  on server from client.
/// </summary>     
//public static TypeMapper Current
//{
//   get
//   {
//       return TypeMapperCreator.instance;
//   }
//}
///// <summary>
///// Creator Class - who instantiating Singleton object 
///// </summary>
//class TypeMapperCreator
//{ 
//   /// <summary>
//   /// Explicit static constructor to tell C# compiler
//   /// not to mark type as before field init
//   /// </summary>
//   static TypeMapperCreator()
//   {
//   }
//   /// <summary>
//   /// Single target class instance
//   /// </summary>
//   internal static readonly TypeMapper instance = new TypeMapper();
//}
#endregion   ----------- Silngleton  Constructor -----------------------

#endregion
