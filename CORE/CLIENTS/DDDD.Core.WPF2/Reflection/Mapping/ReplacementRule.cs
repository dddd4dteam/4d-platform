﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DDDD.Core.Reflection
{


    /// <summary>
    /// Replacement rule can be used in 2 ways:
    /// <para/>
    /// 1-  If ContainsPredicate is not null -  first check if only string Contains -[ContainsPredicate]- and then try to process this name  with  [ReplaceExpression] and [ReplaceTarget]
    /// <para/>
    /// 2 - If ContainsPredicate is  null - try to process this name  with  [ReplaceExpression] and [ReplaceTarget]
    /// </summary>
    public class ReplacementRule
    {

        public Int32 ID
        {
            get
            {
                return GetHashCode();
            }
        }

        public string ContainsPredicate
        {
            get;
            private set;
        }

        /// <summary>
        /// Regular Expression for replacement
        /// </summary>
        public Regex ReplaceExpression
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ReplaceTarget
        {
            get;
            private set;
        }


        /// <summary>
        /// Create new  Replacement rule.
        /// Replacement rule can be used in 2 ways:
        /// 1-  If ContainsPredicate is not null -  first check if only string Contains -[ContainsPredicate]- and then try to process this name  with  [ReplaceExpression] and [ReplaceTarget]
        /// 2 - If ContainsPredicate is  null - try to process this name  with  [ReplaceExpression] and [ReplaceTarget]
        /// </summary>
        /// <param name="containspredicate"></param>
        /// <param name="replaceExpression"></param>
        /// <param name="replaceTarget"></param>
        /// <returns></returns>
        public static ReplacementRule Create(String containspredicate, Regex replaceExpression, String replaceTarget)
        {
            var rule = new ReplacementRule();
            rule.ContainsPredicate = containspredicate;
            rule.ReplaceExpression = replaceExpression;
            rule.ReplaceTarget = replaceTarget;
            return rule;
        }



        public string TryReplace(String inputName)
        {
            if (ContainsPredicate != null && !inputName.Contains(ContainsPredicate))
            {
                return inputName;                //do not replace this string
            }

            return ReplaceExpression.Replace(inputName, ReplaceTarget);
        }
        

        public override int GetHashCode()
        {
            return ReplaceExpression.GetHashCode() ^ ReplaceTarget.GetHashCode();
        }


    }
}
