﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Extensions; 


namespace DDDD.Core.Reflection 
{
    /// <summary>
    /// Type Light Name string format
    /// </summary>
    public class TypeLN
    {
        /// <summary>
        ///  Type Name Part Delimeter in Type Light Name format
        /// </summary>
        public const string LNDM = "|";

        const char QUOTE = '"';

        static Type GetTypeByFN(string tpFullName, string assembly = null)
        {
            // 1 from TypeInfoEx cache
            var id = Tps.HashCode(tpFullName);
            TypeInfoEx resType = null;
            if( TypeInfoEx.TypeInfoExCache.TryGetValue(id, out resType) )
               return resType.OriginalType;
              
            //2 TypeFN + Assembly
            if (assembly.IsNotNull())
            {
                // 2 - from TypeCache cache or  3 Type.GetType
                var resType2 = TypeCache.Current.GetDomTypeByFNNAssembly(tpFullName, assembly);
                if (resType2.IsNotNull()) return resType2;
            }
           
            return null;
        }
        

        /// <summary>
        /// Parse Type Light Name string to get real Type
        /// </summary>
        /// <param name="tpLightName"></param>
        /// <returns></returns>
        public static Type ParseTypeLightName(string tpLightName,string assembly = null)
        {
            Type restp = null;
            var items = tpLightName.SplitNoEntries(LNDM);
            
            //Clean from QUOTE  
            //for (int i = 0; i < items.Count; i++)
            //{ items[i] = items[i].Trim(QUOTE);
            //}

            if (items.Count == 1)
            {
                //PRIMITIVES
                if (Tps.TryGetPrimitiveTp( items[0], out restp))
                {
                    return restp;
                }
                else  // customType | enum too 
                {
                    return GetTypeByFN(items[0] , assembly);
                }
            }
            else if (items.Count == 2)//List or Array
            {
                if (Tps.IsListLN(items[0])) // LIST
                {
                    var listEndType = Tps.GetListLNEndType(items[0]);
                    var arg1Type = GetTypeByFN(items[1]);

                    if (arg1Type == null)
                    { return null; }

                    return listEndType
                           .MakeGenericType(arg1Type);
                }
                else if (items[0] == Tps.N_ARRAY) // ARRAY
                {
                    var arg1Type = GetTypeByFN(items[1]);

                    if (arg1Type == null)
                    { return null; }

                    return arg1Type.MakeArrayType();
                }
            }
            else if (items.Count == 3)// Dictionary OR  LIST of LIST of T
            {
                if (items[0] == Tps.N_DICT) //  Dictionary
                {
                    var arg1Type = GetTypeByFN(items[1]);
                    var arg2Type = GetTypeByFN(items[2]);

                    if (arg1Type == null || arg2Type == null)
                    { return null; }

                    return Tps.T_DictionaryGen
                         .MakeGenericType(arg1Type, arg2Type);
                }
                else if ( Tps.IsListLN(items[0]) // LIST of LIST of T
                       && Tps.IsListLN(items[1]) )
                {
                    var listEndType0 = Tps.GetListLNEndType(items[0]);
                    var listEndType1 = Tps.GetListLNEndType(items[1]);

                    var arg1Type = GetTypeByFN(items[2]);

                    if (arg1Type == null)
                    { return null; }

                    var argList = listEndType1
                         .MakeGenericType(arg1Type);

                    return listEndType0
                         .MakeGenericType(argList);
                }
            }
            else if (items.Count == 4)
            {
                if (items[0] == Tps.N_DICT //  Dictionary of LIST of T 
                                           //  Key cant be of collection Type
                && Tps.IsListLN(items[2])) //  Value is List collection
                {                    
                    var argKeyTp = GetTypeByFN(items[1]);

                    var listEndType = Tps.GetListLNEndType(items[2]);
                    var argListElemTp = GetTypeByFN(items[3]);

                    if (argKeyTp == null || argListElemTp == null)
                    { return null; }

                    var argList = listEndType
                        .MakeGenericType(argListElemTp);

                    return Tps.T_DictionaryGen
                         .MakeGenericType(argKeyTp, argList);

                }
            }

            return restp;
        }



        /// <summary>
        /// Build Type Light Name by TypeInfoEx item internal logic
        /// </summary>
        /// <param name="tpImfoEx"></param>
        /// <returns></returns>
        internal static string BuildTypeLightName(TypeInfoEx tpImfoEx)
        {
            // Name only for Primitive Type
            if (tpImfoEx.Is4DPrimitive && tpImfoEx.IsNullableType == false)
            {  return tpImfoEx.OriginalType.Name;
            }
            else if (tpImfoEx.Is4DPrimitive && tpImfoEx.IsNullableType)
            {  return tpImfoEx.WorkingType.Name;
            }


            // Is Array
            if (tpImfoEx.IsArray)
            {
                return Tps.N_ARRAY + LNDM + tpImfoEx.ArrayElementType.FullName;
            }
            // IS LIST / ObservableCollection...
            else if (tpImfoEx.IsIListImplemented)
            {
                // 1 no Generic ARGS - ArgType NEVER can be null because we get ArgType from  IList<>                
                // 2 arg is List too                
                // 3 has generic arg - some COntract - not List
                if (tpImfoEx.Arg1Type.IsImplement_List())
                {// List of List

                    var list1Name =Tps.GetListLNFromTypeName(tpImfoEx.OriginalType.Name);
                    var list2Name = Tps.GetListLNFromTypeName(tpImfoEx.Arg1Type.Name);

                    var argSub = TypeInfoEx.Get_IListArgType(tpImfoEx.Arg1Type);

                    if (argSub == null)  return string.Empty;
                    
                    return list1Name + LNDM + list2Name +  LNDM
                        + argSub.FullName;

                }
                else // simple List of T
                {
                    var list1Name = Tps.GetListLNFromTypeName(tpImfoEx.OriginalType.Name);

                    return list1Name + LNDM + tpImfoEx.Arg1Type.FullName;
                }
            }
            // IS DICTIONARY
            else if (tpImfoEx.IsIDictionaryImplemented)
            {
                if (tpImfoEx.Arg2Type.IsImplement_List())
                { // DICTIONARY ofK ,  LIST 

                    var listName = Tps.GetListLNFromTypeName(tpImfoEx.Arg2Type.Name);

                    var argSub = TypeInfoEx.Get_IListArgType(tpImfoEx. Arg2Type);

                    if (argSub == null) return string.Empty;

                    return  Tps.N_DICT + LNDM + tpImfoEx.Arg1Type.FullName
                            + LNDM + listName + LNDM + argSub.FullName;
                }
                else
                {//simple Dictionary of K , T
                    return Tps.N_DICT + LNDM + tpImfoEx.Arg1Type.FullName
                                    + LNDM + tpImfoEx.Arg2Type.FullName;
                }

            }
            else// Enums + CUstomTypes + Abstract Types + Interfaces
            {
                return tpImfoEx.OriginalType.FullName;
            }
        }



    }
}
