﻿using System;
using System.Reflection;

namespace DDDD.Core.Reflection.IG
{
    /// <summary>
    /// Contains information for a property defined at run-time and provided through <see cref="T:System.Reflection.ICustomTypeProvider" /> interface.
    /// </summary>
    public class CustomTypeProperty
    {
        /// <summary>
        /// Gets the <see cref="P:CustomTypeProperty.PropertyInfo" /> object containing property metadata.
        /// </summary>
        public  PropertyInfo PropertyInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <value>
        /// The name of the property.
        /// </value>
        public string PropertyName
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the type of the property.
        /// </summary>
        /// <value>
        /// The type of the property.
        /// </value>
        public Type PropertyType
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:CustomTypeProperty" /> class.
        /// </summary>
        /// <param name="propertyInfo">The property info.</param>
        public CustomTypeProperty(PropertyInfo propertyInfo)
        {
            this.PropertyInfo = propertyInfo;
            this.PropertyName = propertyInfo.Name;
            this.PropertyType = propertyInfo.PropertyType;
        }
    }
}
