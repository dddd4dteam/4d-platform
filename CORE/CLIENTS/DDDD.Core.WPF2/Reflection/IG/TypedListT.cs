﻿using System.Collections;
using System.Collections.Generic;

using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;


namespace DDDD.Core.Reflection.IG
{

    /// <summary>
    /// IList&lt;T&gt; implementation that wraps an IList
    /// </summary>
    /// <typeparam name="T">Type of the items in the collection</typeparam>
    internal class TypedList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IEnumerable
    {
        private IList _list;

        public int Count
        {
            get
            {
                return this._list.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return this._list.IsReadOnly;
            }
        }

        public T this[int index]
        {
            get
            {
                return (T)this._list[index];
            }
            set
            {
                this._list[index] = value;
            }
        }

        internal IList List
        {
            get
            {
                return this._list;
            }
        }

        internal TypedList(IList list)
        {
            Validator.ATNullReferenceArgDbg(list, nameof(TypedList<T>), nameof(TypedList<T>), nameof(list));
            //CoreUtilities.ValidateNotNull(list);
            this._list = list;
        }

        public void Add(T item)
        {
            this._list.Add(item);
        }

        public void Clear()
        {
            this._list.Clear();
        }

        public bool Contains(T item)
        {
            return this._list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            new TypedEnumerable<T>(this._list).CopyTo<T>(array, arrayIndex);
            //CoreUtilities.CopyTo<T>(, array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new TypedEnumerable<T>.Enumerator(this._list.GetEnumerator());
        }

        public int IndexOf(T item)
        {
            return this._list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            this._list.Insert(index, item);
        }

        public bool Remove(T item)
        {
            int num = this._list.IndexOf(item);
            if (num >= 0)
            {
                this._list.RemoveAt(num);
            }
            return num >= 0;
        }

        public void RemoveAt(int index)
        {
            this._list.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._list.GetEnumerator();
        }
    }
}
