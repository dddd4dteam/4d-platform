﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Reflection.IG
{
    /// <summary>
    /// Typed enumerable that wraps a non-typed enumerable.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypedEnumerable<T> : IEnumerable<T>, IEnumerable
    {
        private IEnumerable _e;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="e">Enumerable</param>
        public TypedEnumerable(IEnumerable e)
        {
            object obj = e;
            if (obj == null)
            {
                obj = new T[0];
            }
            this._e = (IEnumerable)obj;
        }

        private IEnumerator<T> GetEnumerator()
        {
            return new TypedEnumerable<T>.Enumerator(this._e.GetEnumerator());
        }

        internal void ResetSourceEnumerable(IEnumerable e)
        {
            object obj = e;
            if (obj == null)
            {
                obj = new T[0];
            }
            this._e = (IEnumerable)obj;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        internal class Enumerator : IEnumerator<T>, IDisposable, IEnumerator
        {
            private IEnumerator _e;

            public T Current
            {
                get
                {
                    return (T)this._e.Current;
                }
            }

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public Enumerator(IEnumerator e)
            {
                this._e = e;
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                return this._e.MoveNext();
            }

            public void Reset()
            {
                this._e.Reset();
            }
        }
    }
}
