﻿

namespace DDDD.Core.Reflection.Compare
{

    public enum DiffCaseEn
    {
        // Difference in Data:
        // - Different - one of values is null - DiffNullValueInOne
        // - Different Types of A and B objects  (DifferentTypes)
        // - Values by path X are not equal
        //      --not eual primitive values     (DiffPrimitiveValues)
        //      --not eual enums values         (DiffEnumValues)
        //  - Arrays Size is not equal          (DiffArraySize)
        //  - List Size is not equal            (DiffListSize)
        //  - Dictionary Size is not equal      (DiffDictionrySize)
        DiffNullValueInOne

            , DifferentTypes

            , DiffPrimitiveValues

            , DiffEnumValues

            , DiffArraySize

            , DiffListSize

            , DiffDictionarySize

    }

    public class Difference
    {         
        public string Path { get; set; }

        public DiffCaseEn DifferenceCase { get; set; }

        public string ObjATypeName { get; set; }

        public string ObjBTypeName { get; set; }
         

        public string Message { get; set; }


        public static Difference NewDiff(DiffCaseEn differenceCase
                                        , string path = ""
                                        , string objATypeName = ""
                                        , string objBTypeName = ""                                                                
                                        )
        {
            var diff = new Difference();
            diff.DifferenceCase = differenceCase;
            diff.ObjATypeName = objATypeName;
            diff.ObjBTypeName = objBTypeName;
            diff.Path = path;


             diff.BuildMessage();
            return diff;
        }

        public Difference BuildMessage()
        {
            Message = $"DIF[{ DifferenceCase.ToString() }]  Path[{Path}] ";
            return this;
        }

    }



}
