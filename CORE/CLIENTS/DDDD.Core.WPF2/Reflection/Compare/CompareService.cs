﻿using DDDD.Core.Extensions;
using DDDD.Core.Patterns;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Reflection.Compare
{
    /// <summary>
    /// Comparing Types and Data of A and B objects
    ///  - for Equality 
    /// </summary>
    public class CompareService//: Singleton<CompareService>
    {
        //protected override void Initialize()
        //{            
 
        //}
        
      
        /// <summary>
        /// Compare for Equakity a and B objects and Collect all differenxies betweeb them.
        /// Here we can compare:
        ///  <para/>           - primitive types,
        ///  <para/>           - custom struct or class -  fields, properties
        ///  <para/>           - arrays, Collections Dictionaries, 
        ///  <para/>           - enums        
        /// </summary>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        public static List<Difference> IsEqualData(object objA
                                                , object objB
                                                ,TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                )
        {
            var diffs = new List<Difference>();
            var rootPath = "";

            if (objA.IsNull() || objB.IsNull()) return diffs;
                        
            

            if(IsNullDifferent(rootPath, ref diffs, objA, objB))  return diffs;
           
            var typeInf = IsEqualTypes(rootPath, ref diffs, objA, objB);
            if (typeInf == null) return diffs;
             
            IsEqualDataInternal(rootPath,ref diffs, objA, objB, typeInf, selector);
                       
            return diffs;
        }


        static void IsEqualDataInternal(string rootPath
                                        , ref List<Difference> diffs
                                        , object objA, object objB
                                        , TypeInfoEx typeInf
                                        , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                       )
        {
            //IsEqual Primitives FALSE
            if (typeInf.Is4DPrimitive)
            {
                IsEqualPrimitives(rootPath, ref diffs, objA, objB, typeInf);               
                return;
            }

            //IsEqual enums FALSE
            if (typeInf.IsEnum)
            {
                IsEqualEnums(rootPath, ref diffs, objA, objB, typeInf);                
                return;
            }

            //IsEqual StructOrClass
            if (typeInf.IsCustomClass || typeInf.IsCustomStruct)
            {
                IsEqualStructs(rootPath,ref diffs, objA, objB, typeInf, selector );
                return;
            }

            //IsEqual array
            if (typeInf.IsArray)
            {
                IsEqualArrays(  rootPath, ref diffs, objA, objB , typeInf );
                return;
            }

            //IsEqual Lists
            if (typeInf.IsIListImplemented)
            {
                IsEqualLists(rootPath, ref diffs, objA, objB, typeInf);
                return;
            }

            //IsEqual Dictionary
            if (typeInf.IsIDictionaryImplemented)
            {
                IsEqualDictionaries(rootPath, ref diffs, objA, objB, typeInf);
                return;
            }

        }


         static void IsEqualPrimitives( string rootPath,ref List<Difference> diffs ,object objA, object objB, TypeInfoEx typeInf)
        {
            bool isDifferent;

            switch (typeInf.Primitive)
            {
                case TypePrimitiveEn.NotPrimitive:
                    {
                        isDifferent = false; break;
                    }
                case TypePrimitiveEn.Int32:
                    {
                        if ( (Int32)objA != (Int32)objB )
                        { isDifferent = true;  }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt32:
                    {
                        if ((UInt32)objA != (UInt32)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Int64:
                    {
                        if ((Int64)objA != (Int64)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt64:
                    {
                        if ((UInt64)objA != (UInt64)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Int16:
                    {
                        if ((Int16)objA != (Int16)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt16:
                    {
                        if ((UInt16)objA != (UInt16)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Byte:
                    {
                        if ((byte)objA != (byte)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.SByte:
                    {
                        if ((sbyte)objA != (sbyte)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Char:
                    {
                        if ((char)objA != (char)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Boolean:
                    {
                        if ((bool)objA != (bool)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.DateTime:
                    {
                        if ((DateTime)objA != (DateTime)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.TimeSpan:
                    {
                        if ((TimeSpan)objA != (TimeSpan)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.DateTimeOffset:
                    {
                        if ((DateTimeOffset)objA != (DateTimeOffset)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Guid:
                    {
                        if ((Guid)objA != (Guid)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Float:
                    {
                        if ((float)objA != (float)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Double:
                    {
                        if ((double)objA != (double)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Decimal:
                    {
                        if ((decimal)objA != (decimal)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Int32_nullable:
                    {
                        if ((Int32?)objA != (Int32?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt32_nullable:
                    {
                        if ((UInt32?)objA != (UInt32?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Int64_nullable:
                    {
                        if ((Int64?)objA != (Int64?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt64_nullable:
                    {
                        if ((UInt64?)objA != (UInt64?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Int16_nullable:
                    {
                        if ((Int16?)objA != (Int16?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.UInt16_nullable:
                    {
                        if ((UInt16?)objA != (UInt16?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Byte_nullable:
                    {
                        if ((byte?)objA != (byte?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.SByte_nullable:
                    {
                        if ((sbyte?)objA != (sbyte?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Char_nullable:
                    {
                        if ((char?)objA != (char?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Boolean_nullable:
                    {
                        if ((bool?)objA != (bool?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.DateTime_nullable:
                    {
                        if ((DateTime?)objA != (DateTime?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.TimeSpan_nullable:
                    {
                        if ((TimeSpan?)objA != (TimeSpan?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Guid_nullable:
                    {
                        if ((Guid?)objA != (Guid?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Float_nullable:
                    {
                        if ((float?)objA != (float?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Double_nullable:
                    {
                        if ((double?)objA != (double?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Decimal_nullable:
                    {
                        if ((decimal?)objA != (decimal?)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.String:
                    {
                        if ((string)objA != (string)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.ByteArray:
                    {
                        if ((byte[])objA != (byte[])objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.Uri:
                    {
                        if ((Uri)objA != (Uri)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.BitArray:
                    {
                        if ((BitArray)objA != (BitArray)objB)
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                case TypePrimitiveEn.TypeInfoEx:
                    {
                        if ( (objA as TypeInfoEx) != ( objB as TypeInfoEx) )
                        { isDifferent = true; }
                        else isDifferent = false;

                        break;
                    }
                default:
                    {
                        isDifferent = false; break;
                    }  
            }


            if (isDifferent)
            {
                diffs.Add(
                Difference.NewDiff(DiffCaseEn.DiffPrimitiveValues
                  , typeInf.OriginalType.Name
                  , typeInf.OriginalType.Name
                  , rootPath)
               );
            }

        }

         static void IsEqualEnums( string rootPath, ref List<Difference> diffs , object objA, object objB, TypeInfoEx typeInf)        
        {
            bool isDifferent;

            if (objA.S() != objB.S())
            {  isDifferent = true;  
            }
            else isDifferent = false;
            
            if (isDifferent)
            {
                diffs.Add(
                            Difference.NewDiff(DiffCaseEn.DiffEnumValues
                           , typeInf.OriginalType.Name
                           , typeInf.OriginalType.Name
                           , rootPath)
                           );
            }

        }

         static void IsEqualStructs( string rootPath
                                   , ref List<Difference> diffs
                                   ,  object objA, object objB
                                   , TypeInfoEx tpEx
                                   , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                   )
        {
            //Compare  struct/class NULL values
            // compare struct/class Types
            // compare struct/class member Values
            
            if (objA == null && objB == null) return;

            //compare struct/class Null Values
            if (IsNullDifferent(rootPath, ref diffs, objA, objB)) return;
            //compare struct/class Types
            var typeInf = IsEqualTypes(rootPath, ref diffs, objA, objB);
            if (typeInf == null) return;              

            // compare struct/class member Values
            var tpAccessor = tpEx.GetAccessor(selector);
            foreach (var mmbr in tpAccessor.Members)
            {
                //check Members 
                var memberPath = rootPath + "." + mmbr.Key;
                var compA = tpAccessor.GetMember(mmbr.Key, objA);
                var compB = tpAccessor.GetMember(mmbr.Key, objB);

                IsEqualDataInternal(memberPath,ref diffs, compA, compB, mmbr.Value.TypeInf);

            }
             
        }
               
         static void IsEqualArrays(string rootPath, ref List<Difference> diffs,  object objA, object objB, TypeInfoEx tpEx)
        {
            //Comapre Array measurres:
            //     compare array Null Values
            //     compare array Types
            //     compare array sizes
            //Comapre Element measurres:
            //     compare element Null Values
            //     compare element Types
            //     compare element Values

            if (objA == null && objB == null) return;

            //Comapre Array measurres:
            //compare  Array Null Values
            if (IsNullDifferent(rootPath, ref diffs, objA, objB)) return;
            //compare  Array Types
            var typeInf = IsEqualTypes(rootPath, ref diffs, objA, objB);
            if (typeInf == null) return;

            var arrA = objA as Array;
            var arrB = objB as Array;
            //     compare array sizes            
            if (arrA.Length != arrB.Length)
            {
                diffs.Add(
                            Difference.NewDiff(DiffCaseEn.DiffArraySize
                            , path: rootPath)
                         );
                return;
            }          
            var Len = arrA.Length;

            //Comapre Element measurres:
            for (int i = 0; i < Len; i++)
            {
                //     compare element Null Values
                //     compare element Types
                //     compare element Values
                var iElemPath = rootPath + $"[" +i.S()+ "]";
                var elemA = arrA.GetValue(i);
                var elemB = arrB.GetValue(i);

                if (elemA == null && elemB == null) continue;

                //   compare element Null Values
                if (IsNullDifferent(iElemPath, ref diffs, elemA, elemB)) continue;
                //   compare  element   Types
                var elemTpInf = IsEqualTypes(iElemPath, ref diffs, elemA, elemB);
                if (elemTpInf == null) continue;
                               
                //     compare element Values
                IsEqualDataInternal(iElemPath, ref diffs, elemA, elemB, elemTpInf);
                
            }
            
        }
        
         static void IsEqualLists(string rootPath, ref List<Difference> diffs, object objA, object objB, TypeInfoEx tpEx)
        {
            //Comapre List measurres:
            //     compare List Null Values
            //     compare List Types
            //     compare List sizes
            //Comapre Element measurres:
            //     compare element Null Values
            //     compare element Types
            //     compare element Values

            if (objA == null && objB == null) return;
            
            //Comapre List measurres:
            //compare  List Null Values
            if (IsNullDifferent(rootPath, ref diffs, objA, objB)) return;
            //compare  List Types
            var typeInf = IsEqualTypes(rootPath, ref diffs, objA, objB);
            if (typeInf == null) return;


            var listA = objA as IList;
            var listB = objB as IList;
            //     compare List sizes            
            if (listA.Count != listB.Count)
            {
                diffs.Add(
                            Difference.NewDiff(DiffCaseEn.DiffListSize
                            , path: rootPath)
                         );
                return;
            }
            var Len = listA.Count;

            //Comapre Element measurres:
            for (int i = 0; i < Len; i++)
            {
                //     compare element Null Values
                //     compare element Types
                //     compare element Values
                var iElemPath = rootPath + $"[" + i.S() + "]";
                var elemA = listA[i];
                var elemB = listB[i];

                if (elemA == null && elemB == null) continue;

                //   compare element Null Values
                if (IsNullDifferent(iElemPath, ref diffs, elemA, elemB)) continue;
                //   compare  element   Types
                var elemTpInf = IsEqualTypes(iElemPath, ref diffs, elemA, elemB);
                if (elemTpInf == null) continue;

                //     compare element Values
                IsEqualDataInternal(iElemPath, ref diffs, elemA, elemB, elemTpInf);

            }

        }

        static void IsEqualDictionaries(string rootPath, ref List<Difference> diffs, object objA, object objB, TypeInfoEx tpEx)
        {
            //Comapre Dictionary measurres:
            //     compare  Dictionary Null Values
            //     compare  Dictionary Types
            //     compare  Dictionary sizes
            //Comapre Element measurres:
            //     compare element Key Types 
            //     compare element Key Values 
            //     compare element Value Null Values
            //     compare element Value Types
            //     compare element Value data

            if (objA == null && objB == null) return;

            //Comapre  Dictionary measurres:
            //compare   Dictionary Null Values
            if (IsNullDifferent(rootPath, ref diffs, objA, objB)) return;
            //compare   Dictionary Types
            var typeInf = IsEqualTypes(rootPath, ref diffs, objA, objB);
            if (typeInf == null) return;
                
            var dictA = objA as IDictionary;
            var dictAKeys = dictA.Keys.ToList<object>();
            var dictAVals = dictA.Values.ToList<object>();

            var dictB = objB as IDictionary;
            var dictBKeys = dictB.Keys.ToList<object>();
            var dictBVals = dictB.Values.ToList<object>();
            
            //     compare  Dictionary sizes            
            if (dictA.Count != dictB.Count)
            {
                diffs.Add(
                            Difference.NewDiff(DiffCaseEn.DiffDictionarySize                             
                            ,path: rootPath)
                         );
                return;
            }
            var Len = dictA.Count;

            //Comapre Element measurres:
            for (int i = 0; i < Len; i++)
            {
                //     compare element Key Types 
                //     compare element Key Values 
                //     compare element Value Null Values
                //     compare element Value Types
                //     compare element Value data
                var iElemPath = rootPath + $"[" + i.S() + "]";
                
                var elemAKey = dictAKeys[i];                              
                var elemBKey = dictBKeys[i];
                
                //      compare element Key Types
                var elKeyTypeInf = IsEqualTypes(iElemPath, ref diffs, elemAKey, elemBKey);
                if (elKeyTypeInf == null) continue;
                
                //     compare element Key Values                  
                IsEqualDataInternal(iElemPath, ref diffs, elemAKey, elemBKey, elKeyTypeInf);


                var elemAVal = dictAVals[i];
                var elemBVal = dictBVals[i];

                //     compare element Value Null Values
                if (IsNullDifferent(iElemPath, ref diffs, elemAVal, elemBVal))
                    continue;
                //     compare element Value Types
                var elValTypeInf = IsEqualTypes(iElemPath, ref diffs, elemAVal, elemBVal);
                if (elValTypeInf == null) 
                    continue; 
                  
                //     compare element Value data
                IsEqualDataInternal(iElemPath, ref diffs, elemAVal, elemBVal, elValTypeInf);
                
            }

        }




        /// <summary>
        /// Compare if A and B Types are Equal.
        /// <para/> If Types are Equal then return A Type.
        /// <para/> If Types are Different - add info to diffs list and returns NULL.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="diffs"></param>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        static TypeInfoEx IsEqualTypes(string rootPath, ref List<Difference> diffs, object objA, object objB)
        {
            var typeExA = objA.GetTypeInfoEx();
            var typeExB = objB.GetTypeInfoEx();

            // compare A and B Types
            if (typeExA.ID != typeExB.ID)
            {
                diffs.Add(
                     Difference.NewDiff(DiffCaseEn.DifferentTypes
                     , typeExA.OriginalType.Name
                     , typeExB.OriginalType.Name
                     , rootPath
                     )
                    );
                return null;//// Types are different
            }

            return typeExA; // Types are Equal
        }


        /// <summary>
        /// Compare if one of A or B value is null but other is NOT.
        /// <para/> If it is NOT so - then returns  FALSE - NOT DDIFFERENT.
        /// <para/>  If it is so  -then add info to diffs list and returns TRUE- DIFFERENT.
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="diffs"></param>
        /// <param name="objA"></param>
        /// <param name="objB"></param>
        /// <returns></returns>
        static bool IsNullDifferent(string rootPath, ref List<Difference> diffs, object objA, object objB)
        {
            //compare struct/class Null Values
            if ((objA == null && objB != null)
                || (objA != null && objB == null)
               )
            {
                diffs.Add(
                        Difference.NewDiff(
                             DiffCaseEn.DiffNullValueInOne
                           , rootPath)
                         );
                return true;// YES NULL DIFFERENT
            }
            return false; // NOT DIFFERENT

        }



    }




}
