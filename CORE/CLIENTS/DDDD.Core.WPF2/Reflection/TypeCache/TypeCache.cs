﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


using DDDD.Core.App;
using DDDD.Core.Events;
using DDDD.Core.Extensions;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Threading;
using DDDD.Core.Net.WebApi;
using DDDD.Core.Collections;
using DDDD.Core.Data.Hash;
using DDDD.Core.Patterns;

#if SERVER && IIS
using System.Web.Http.Dependencies;
using System.Web.Http.Dispatcher;
#endif



namespace DDDD.Core.Reflection
{
    /// <summary>
    /// Delegate with signarture - [void DomainTypesAddedHandler (IEnumerable{Type} addedDomainTypes)] - used/rised  when some really useful-custom-domain types added into TypeCache.
    /// </summary>
    /// <param name="addedDomainTypes"></param>
    public delegate void DomainTypesAddedHandler (Type[] addedDomainTypes);




    /// <summary>
    ///  TypeCache - singleton Service which contains  Domain Types Cache in current Application Domain. 
    /// </summary>
    public class TypeCache: Singleton<TypeCache>
    {

        #region ------------------------------ CTOR --------------------------------

        TypeCache()
        { }

        #endregion ------------------------------ CTOR --------------------------------
        


        #region ----------------------- LOCKS ------------------------

        class InnerLocker1
        {
            internal static readonly object locker1 = new object();
        }

        class InnerLocker2
        {
            internal static readonly object locker2 = new object();
        }

        #endregion ----------------------- LOCKS ------------------------
        


        protected override void Initialize()
        {
            try
            {
                if (IsInited) return;
                
                var assemblies = AppDomain.CurrentDomain.GetAssemblies();
                assemblies = assemblies.Where(IsCustomDomainAssemblyCriterion).ToArray();

                // add including Types to customDomainTypes and to CustomDomainTypesByAssemblies                               
                AddDomTypesByAssemblies(IncludingTypes);

                // Initialize the types of early loaded assemblies

                for (int i = 0; i < assemblies.Length ; i++)
                {
                    TryAttachAssemblyToDomain(assemblies[i]);
                }

#if SERVER || WPF
                AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;

#elif CLIENT && SL5
           
            //  assemblies will be loaded synchronously with IOC container - ComponentsContainer manually
#endif
                IsInited = true;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR on {nameof(TypeCache)} Initialization: {exc.Message}" );
            }            

        }




#if SERVER || WPF

        private   void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
            TryAttachAssemblyToDomain(args.LoadedAssembly);
        }
             

#endif





        #region ------------------------------- CUSTOMIZATION OF DOMAIN ASSEMBLIES -----------------------------------


      
        /// <summary>
        /// Excluding Assemblies. 
        /// We use this list in  Checking Contract/Predicate - checking that assembly can be mean as Domain Assembly .
        /// </summary>
        public static List<string> ExcludingAssemblies
        { get; } =  new List<string>
                    {
#if SERVER
                     "DDDD.Core.Server.Net45"

#elif (CLIENT && WPF)

 
                    "DDDD.Core.WPF2.v.1.0.1.0"
                  ,  "DDDD.Core.WPF"
#endif
        };
      
        
        /// <summary>
        /// Exclude Domain Assembly and include some Types 
        /// that still can exist in that excludingAssembly. 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="stillIncludingTypes"></param>
        public static  void ExcludeDomainAssembly(string assemblyName, params Type[] stillIncludingTypes)
        {
            if (ExcludingAssemblies.Contains(assemblyName)) return;

            // add not existed excludeAssembly
            ExcludingAssemblies.Remove(assemblyName);
                
            for (int i = 0; i < stillIncludingTypes.Length; i++)
            {
                if (IncludingTypes.Contains(stillIncludingTypes[i]))
                    IncludingTypes.Add(stillIncludingTypes[i]);
            }               
            
        }
          


        /// <summary>
        /// Some specific Types that may exist in [Excluding Assemblies], but they are still needable Types. 
        /// This types used in default check for domain type criterion.
        /// </summary>
        public  static List<Type> IncludingTypes
        { get; } = new List<Type>()
        {
#if SERVER && IIS
              typeof(DCServiceHostFactory)  
            ,typeof(DCService), typeof(DCWebServiceHostFactory)
            , typeof(DCWebService)
            , typeof(DCWebApiDirectHandlerServerFactory)
            
#elif CLIENT
            typeof(DCServiceClientFactory)
            ,typeof(DCServiceClient)
            ,typeof(DCWebApiDirectHandlerClientFactory)            
#endif
            , typeof(AppComponentsLoadManager)
        };

       

        /// <summary>
        /// Custom application's Domain Assemblies base recognition criterion. It is First Word before [.] symbol in your custom Domain assembly name string.
        /// <para/> For example [DDDD.Core.Server] - CustomDomainAssemblyNameStartPart = [DDDD.] and so on. 
        ///  It means that we will collect CustomDomainAssemblies-assemblies where name StartsWith this value.
        /// </summary>
        public static string CustomDomainAssemblyNameStartPart
        { get; } = LazyAct<string>.Create(
                    (args) =>
                    {      return Assembly.GetExecutingAssembly()
                                    .GetAssemblyNameEx().Split('.')[0] + ".";                     
                    }
                    , null
                    , null
                    ).Value;

        /// <summary>
        /// Contract/predicate to check some Assembly belongs to our  Domain  scope. 
        /// Default criterion check logic is the following:  1-  we check that AssemblyName.StartsWith(CustomDomainAssemblyNameStartPart) and 2- check condition that assemblyName not contains in [ExcludingAssemblies] list.
        /// You can redefine this default criterion check delegate.
        /// </summary>
        public static Func<Assembly, bool> IsCustomDomainAssemblyCriterion
        { get; set; } = IsCustomDomainAssemblyCheck;

        protected static internal  bool IsCustomDomainAssemblyCheck(Assembly checkingAssembly)
        {
            if (checkingAssembly == null) return false;           
#if SERVER
            if (checkingAssembly.ReflectionOnly) { return false; }
#endif

            var assemblyName = checkingAssembly.GetAssemblyNameEx();

            if (  assemblyName.StartsWith(CustomDomainAssemblyNameStartPart)
                &&   ! ExcludingAssemblies.Contains(assemblyName) //assembly is in Excluding List 
                ) return true;
            
            return false;
        }


        ///// <summary>
        ///// Current Domain Types by Assemblies. Domain Types excludes dynamic Types. 
        ///// </summary>
         Dictionary<string, List<Type>> CustomDomainTypesByAssemblies
        { get; } = new Dictionary<string, List<Type>>();


        /// <summary>
        /// Current Domain Types by Assemblies. 
        /// Domain Typesonly  - excluding dynamic Types. 
        /// </summary>
        //static Dictionary<string, List<string>> CustomDomainTypesByAssemblies
        //{ get; } = new Dictionary<string, List<string>>();



        /// <summary>
        ///  Get Domain's  Assemblies names
        /// </summary>
        /// <returns></returns>
        public  IEnumerable<string> GetDomainAssemblyNames()
        { return CustomDomainTypesByAssemblies.Keys; }


        /// <summary>
        /// If TypeCache Contains Assembly(DomainAssembly) by AssemblyName
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public  bool ContainsAssembly(Assembly assembly)
        {
            var assemblyName = assembly.GetAssemblyNameEx();
            return CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }

        public bool NotContainsAssembly(Assembly assembly)
        {
            var assemblyName = assembly.GetAssemblyNameEx();
            return !CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }



        /// <summary>
        /// If TypeCache Contains Assembly(DomainAssembly) by AssemblyName
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public  bool ContainsAssembly(string assemblyName)
        {            
            return CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }





        /// <summary>
        /// Get Domain Assembly Types by AssemblyName.
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public  List<Type> GetDomAssemblyTypes(string assemblyName)
        {             
            if (CustomDomainTypesByAssemblies.NotContainsKey(assemblyName))
                return new List<Type>();//empty List of Types

            return CustomDomainTypesByAssemblies[assemblyName];     
       }

#endregion ------------------------------- CUSTOMIZATION OF DOMAIN ASSEMBLIES -----------------------------------

#region --------------------- TYPE.FULLNAME ONLY CACHE for JSON ------------------

        /// <summary>
        ///  Cache of [Type.FullName.hashcode - ID, Type ]
        ///  <para/> It is used by Json and TypeScript Serialization
        /// </summary>
       // internal static SafeDictionary2<int, TypeInfoEx> TypeInfoExCache
        //{ get; } = new SafeDictionary2<int, TypeInfoEx>();
        
        
        /// <summary>
        /// Try get Type from [TypesFNCache] 
        /// or find it in custon domain types, cache it and then return it        /// 
        /// <para/> [TypesFNCache]-  [Type.FullName.hashcode - ID, Type ] Cache
        /// </summary>
        /// <returns></returns>
        public  Type GetDomTypeByFN(string typeFullName)
        {
            Type resType = null;
            var id = Tps.HashCode(typeFullName);


            //1 IncludingTypes
            var existInIncludingTypes = IncludingTypes
                            .FirstOrDefault(tp => tp.FullName == typeFullName);
            if (existInIncludingTypes.IsNotNull()) return existInIncludingTypes;
                        
            //2
            var assmblies = GetDomainAssemblyNames().ToList();
            for (int i = 0; i < assmblies.Count; i++)
            {
                var assemblyDomainTypes = GetDomAssemblyTypes(assmblies[i]);
                resType = assemblyDomainTypes
                         .FirstOrDefault(tp => Tps.HashCode(tp.FullName) == id );
                
                // resType = Type.GetType(resTypeName + ',' + assmblies[i]);
                 
            }
            
            //3
            return resType;
        }

        /// <summary>
        /// Get from  1-IncludingTypes, 2-assemblyTypes, 3 -Type.GetType
        /// </summary>
        /// <param name="typeFullName"></param>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public Type GetDomTypeByFNNAssembly(string typeFullName,string assembly)
        {
            Type resType = null;
            var id = Tps.HashCode(typeFullName);
            
            //1 IncludingType
            resType = IncludingTypes
                            .FirstOrDefault(tp => tp.FullName == typeFullName);
            if (resType.IsNotNull()) return resType;

            //2 assemblyDomTypes
            var assemblyDomTypes = GetDomAssemblyTypes(assembly);
            if (assemblyDomTypes.Count > 0 )
            {
                resType = assemblyDomTypes
                           .FirstOrDefault(tp => tp.FullName == typeFullName);
                if (resType.IsNotNull()) return resType;
            }
                                    
            //3 Type.GetType  
            resType = Type.GetType(typeFullName + ',' + assembly);
                    
            return resType;
        }
        
        #endregion --------------------- TYPE.FULLNAME ONLY CACHE for JSON ------------------



            #region ----------------------------- CUSTOMIZATION OF DOMAIN TYPES ---------------------------------

            /// <summary>
            /// Compose Custom Assemblies List based on TypeCache loaded Types sets. 
            /// Assemblies that contain Including types we do not add to DomainAssemblies List .
            /// </summary>
            /// <returns></returns>
            public  List<Assembly> GetCustomDomainAssemblies()
        {
            var customAssemblies = CustomDomainTypesByAssemblies.Keys.ToList();
            var allDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies(); // all app domain assemblies
            
            // but we select only those exist in CustomDomainTypesByAssemblies.Keys
            List<Assembly> resultAssemblies = new List<Assembly>();
            for (int i = 0; i < allDomainAssemblies.Length; i++)
            {
                if (customAssemblies.Contains(allDomainAssemblies[i].GetAssemblyNameEx()))
                {
                    resultAssemblies.Add(allDomainAssemblies[i]);
                }
            }

            return resultAssemblies;
        }



        /// <summary>
        /// This is the Contract/Predicate by which we beleave that cheking  Exported Type, from custom assembly, is Domain Type.
        /// By default we beleave that Type is Domain Type. If it's name doesn't starts from auto Type Symbol [less than sign] 
        /// </summary>
        public Func<Type, bool> IsCustomDamainTypeCriterion
        { get; set; } = IsCustomDomainTypeCheck;

        static bool IsCustomDomainTypeCheck(Type checkingType)
        {
            // Ignore useless types
            if (checkingType.Name.StartsWith("<")) return false;
            if (checkingType.Name.StartsWith("__")) return false;

            if (checkingType.Name.Contains("\\*")) return false;
            if (checkingType.Name.Contains("MS.Internal")) return false;

            if (checkingType.Name.Contains("c__DisplayClass")) return false;

            if (checkingType.Name.Contains("d__")) return false;

            if (checkingType.Name.Contains("o__")) return false;

            if (checkingType.Name.Contains("::")) return false;

            if (checkingType.Name.Contains("f__AnonymousType")) return false;

            if (checkingType.Name.Contains("_extraBytes_")) return false;

            if (checkingType.Name.Contains("CppImplementationDetails")) return false;
            
            if( 
                checkingType.Name.Contains("ProcessedByFody") ||
                checkingType.Name.Contains("FXAssembly") ||
                checkingType.Name.Contains("ThisAssembly") ||
                checkingType.Name.Contains("AssemblyRef")
              )
            {  return false;  }

            return true;
        }


     

        /// <summary>
        /// Get custom Domain Type from TypeCache by Type.AssemblyQualifiedName .
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        /// <returns></returns>
        public  Type GetCustomDomainType(string assemblyName,string typeFullName)
        {
            var existInIncludingTypes = IncludingTypes
                .FirstOrDefault(tp => tp.FullName == typeFullName);
            if (existInIncludingTypes.IsNotNull()) return existInIncludingTypes;
       
            if (CustomDomainTypesByAssemblies.NotContainsKey(assemblyName)) return null;
            return CustomDomainTypesByAssemblies[assemblyName]
                .FirstOrDefault(tp => tp.FullName == typeFullName);            
            
        }

#endregion  ----------------------------- CUSTOMIZATION OF DOMAIN TYPES ---------------------------------
        

#region -------------------------------- ADDING CUSTOM DOMAIN TYPES ----------------------------------
        
        static readonly WeakDelegatesManager domainTypesAddedListeners = new WeakDelegatesManager();

        /// <summary>
        /// DomainTypesAdded Event - will be raised on collection of custom DomainTypes added to TypeCache.CustomDomainTypes and  TypeCache.CustomDomainTypesByAssemblies
        /// </summary>
        public event DomainTypesAddedHandler DomainTypesAdded
        {
            add
            {
                domainTypesAddedListeners.AddListener(value);
            }
            remove
            {
                domainTypesAddedListeners.RemoveListener(value);
            }
        }


        protected internal  void TryAddCustomType(Type someType)
        {
            var tpAssemblyName = someType.Assembly.GetAssemblyNameEx();

            //CustomDomainTypesByAssemblies
            if (CustomDomainTypesByAssemblies.NotContainsKey(tpAssemblyName))
            {
                CustomDomainTypesByAssemblies
                    .Add(tpAssemblyName, new List<Type>()); // add new assembly Slot - List<Type>
            }

            if (CustomDomainTypesByAssemblies[tpAssemblyName].Contains(someType) == false)
            {
                CustomDomainTypesByAssemblies[tpAssemblyName].Add(someType );
            }

        }

        /// <summary>
        /// Add some custom-domain type into CustomDomainTypesByAssemblies internal dictionary - types grouping by Assembly.GetAssemblyNameEx().
        /// </summary>
        /// <param name="newDomainTypes"></param>
        protected internal void AddDomTypesByAssemblies(IList<Type> newDomainTypes)
        {
            for (int i = 0; i < newDomainTypes.Count; i++)
            {
                TryAddCustomType(newDomainTypes[i]);
            }
        
        }


        /// <summary>
        /// Attaching separate assembly to Domain Assemblies
        /// </summary>
        /// <param name="loadedAssembly"></param>
        public  void TryAttachAssemblyToDomain(Assembly loadedAssembly)
        {
            //if not loaded - add  is custom Domain Assembly              
            if (IsCustomDomainAssemblyCheck(loadedAssembly))
            {
                if (CustomDomainTypesByAssemblies.ContainsKey(loadedAssembly.GetAssemblyNameEx())) return;

                lock (InnerLocker1.locker1)
                {
                    // all domain types from assembly to domain types                
                    var assemblyNewDomainTypes = loadedAssembly.GetTypes().Where(IsCustomDamainTypeCriterion).ToList();
                   
                    AddDomTypesByAssemblies(assemblyNewDomainTypes);
                    //CustomDomainTypes.AddRange(assemblyNewDomainTypes);

                    domainTypesAddedListeners.Raise(assemblyNewDomainTypes);// raise event of domain Types added
                }
            }
        }




#endregion -------------------------------ADDING CUSTOM DOMAIN TYPES ------------------------------




        /// <summary>
        /// Full Reset - full update of Collection of Domain Assemblies and all of theirs Exported Types.
        /// </summary>
        public void ResetDomainTypeCache()
        {
            var domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < domainAssemblies.Length; i++)
            {
                TryAttachAssemblyToDomain(domainAssemblies[i]);
            }
            
        }

      


#region ------------------------------------- LOAD CUSTOM DOMAIN TYPES -------------------------------------


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type has BaseType and its value satisfy the following condition : [ tBaseType.IsAssignableFrom(checkingType)]. 
        /// Also we can exclude some endTypes  in [excludeTypes] parameter value.
        /// </summary>
        /// <typeparam name="TBase"></typeparam>
        /// <param name="excludeTBaseItself"></param>
        /// <param name="excludeTypes"></param>
        /// <returns></returns>
        public  List<Type> LoadCustomDomainTypesByBaseTypeT<TBase>(bool excludeTBaseItself = true, params Type[] excludeTypes)
        {
            return LoadCustomDomainTypesByBaseType(typeof(TBase), excludeTBaseItself, excludeTypes);
        }

        /// <summary>
        /// Get list of Domain TypesLoad Types to Cache from the list of Domain Types, where Type has BaseType and its value satisfy the following condition : [ tBaseType.IsAssignableFrom(checkingType)].
        /// </summary>
        /// <param name="tBaseType"></param>
        /// <param name="excludeTBaseItself"></param>
        /// <param name="excludeTypes"></param>
        /// <returns></returns>
        public  List<Type> LoadCustomDomainTypesByBaseType(Type tBaseType, bool excludeTBaseItself = true, params Type[] excludeTypes)
        {
            //var domainTypes = CustomDomainTypes;
            //var asssemblyName = tBaseType
            //var baseType = tBaseType;
            var existedTargets = new List<Type>();

            for (int i = 0; i < IncludingTypes.Count; i++)
            {
                if (excludeTBaseItself == true && IncludingTypes[i] == tBaseType 
                    || excludeTypes.Contains(IncludingTypes[i])) continue;

                if (IncludingTypes[i].BaseType != null 
                    && tBaseType.IsAssignableFrom(IncludingTypes[i]))
                {
                    existedTargets.Add(IncludingTypes[i]);
                }                
            }

            for (int i = 0; i < CustomDomainTypesByAssemblies.Count; i++)
            {
                var assemblyTypes = CustomDomainTypesByAssemblies.ElementAt(i);
                for (int j = 0; j < assemblyTypes.Value.Count; j++)
                {
                    var tp = assemblyTypes.Value[j];                    

                    if (excludeTBaseItself == true && tp.FullName == tBaseType.FullName
                        || excludeTypes.Contains(tp) ) continue;
                    
                    if (tp.BaseType != null && tBaseType.IsAssignableFrom(tp)) 
                    {
                        existedTargets.Add(tp);
                    }
                }
            }
          
            return existedTargets;            
        }


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type implements interface TIface.
        /// </summary>
        /// <typeparam name="TIface"></typeparam>
        /// <returns></returns>
        public  List<Type> LoadCustomDomainTypesByInterfaceT<TIface>()
        {
            return LoadCustomDomainTypesByInterface(typeof(TIface));
        }


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type implements interface TIface.
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <returns></returns>
        public  List<Type> LoadCustomDomainTypesByInterface(Type interfaceType)
        {
            var existedTargets = new List<Type>();

            for (int i = 0; i < IncludingTypes.Count; i++)
            {
                if (IncludingTypes[i].GetInterfaces().Contains(interfaceType))
                {
                    existedTargets.Add(IncludingTypes[i]);
                }
            }

            for (int i = 0; i < CustomDomainTypesByAssemblies.Count; i++)
            {
                var assemblyTypes = CustomDomainTypesByAssemblies.ElementAt(i);
                for (int j = 0; j < assemblyTypes.Value.Count; j++)
                {
                    var tp = assemblyTypes.Value[j];                   

                    if (tp.GetInterfaces().Contains(interfaceType))
                    {
                        existedTargets.Add(tp);
                    }
                }
            }
                
            return existedTargets;           
        }



#endregion ------------------------------------- LOAD CUSTOM DOMAIN TYPES -------------------------------------


    }
}


