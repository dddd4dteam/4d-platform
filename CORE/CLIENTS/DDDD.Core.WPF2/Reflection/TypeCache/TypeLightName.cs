﻿using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Reflection
{
    /// <summary>
    /// Type Name Primitive ,that can be used in JSON in TypeScript
    /// </summary>
    public class TypeLightName
    {
        public TypeLightName(Type dataType)
        {
            DataType = dataType;
            ParseDataType();
            
        }

        public TypeLightName(string shortTypeName)
        {
            LightName = shortTypeName;
            ParseLightName();           
        }


        /// <summary>
        /// Type Name Parts Delimeter
        /// </summary>
        const string TDM = "|";

        const string T_int = "int";
        const string T_string = "string";
        const string T_Double = "Double";
        const string T_decimal = "decimal";
        const string T_float = "float";
        const string T_DateTime = "DateTime";
        const string T_DateTimeOffset = "DateTimeOffset";

        const string T_Int64 = "Int64";
        const string T_Int16 = "Int16";
        const string T_long = "long";

        const string T_DICT = "DICT";// DICTIONARY
        const string T_LIST = "LIST";// LIST
        const string T_ARRAY = "ARRAY";//  ARRAY 


        /// <summary>
        /// Type Full Name
        /// </summary>
        public Type DataType
        { get; private set; }

        /// <summary>
        /// DataType.FullName 
        /// </summary>
        public string FullName
        {   get
            {
                if (DataType.IsNull()) return null;
               return DataType.FullName;
            }
        }


        /// <summary>
        /// Type Short Name value
        /// </summary>
        public string LightName
        { get; private set; }

      


        /// <summary>
        /// Is This Type based on IDictionary<,> interface - from ShortName string
        /// </summary>
        public bool IsDictionary
        { get; private set; }

        /// <summary>
        /// Is This Type based on ILisry<> interface - from ShortName string
        /// </summary>
        public bool IsList
        { get; private set; }

        /// <summary>
        /// Is This Type Array  - from ShortName string
        /// </summary>
        public bool IsArray
        { get; private set; }

        /// <summary>
        /// Generic Arg 1 Type Name   - from ShortName.
        /// <para/> Null if not Exist
        /// </summary>
        public string Arg1TpName
        { get; private set; }

        /// <summary>
        /// Generic Arg 2 Type Name   - from ShortName
        /// <para/> Null if not Exist
        /// </summary>
        public string Arg2TpName
        { get; private set; }

       

        /// <summary>
        /// Parsing Type FullName and building ShortName too
        /// </summary>
        void ParseDataType()
        {
            
            var tpInterfaces = DataType.GetInterfaces();
            var tpArgs = DataType.GetGenericArguments();

            // Is Array
            if (DataType.IsArray)
            {
                IsArray = DataType.IsArray;
                LightName = T_ARRAY + TDM + tpArgs[0].FullName;
            }
            // IS LIST / ObservableCollection...
            else if (tpInterfaces.Contains(Tps.T_IListGen))
            {
                IsList = true;
               LightName = T_LIST + TDM + tpArgs[0].FullName;
            }
            // IS DICTIONARY
            else if (tpInterfaces.Contains(Tps.T_IDictionaryGen))
            {
                IsDictionary = true;
                LightName =   T_DICT
                              + TDM + tpArgs[0].FullName
                              + TDM + tpArgs[1].FullName;
            }
            else// Enums + CUstomTypes
            {
                LightName = DataType.FullName;
            }
        }

        /// <summary>
        /// Parsing TypeNameTS and returning Type by TypeCache's cache
        /// </summary>
        /// <param name="tpNameTS"></param>
        /// <returns></returns>
        public static Type ParseLightName(string tpNameTS)
        {
            var items = tpNameTS.SplitNoEntries(TDM);

            if (items.Count == 1)
            {
                //PRIMITIVES
                if (Tps.PrimitivesLight.ContainsKey(items[0]))
                {
                    return Tps.PrimitivesLight[items[0]];
                }
                else  // customType | enum too 
                {
                    return TypeCache.GetCustomDomainTypeByFN(items[0]);
                }
            }
            else if (items.Count == 2)//List or Array
            {
                var Arg1TpName = items[1];
                var arg1Type = TypeCache.GetCustomDomainTypeByFN(Arg1TpName);

                if (arg1Type == null)
                { return null; }


                if (items[0] == T_LIST)
                {                    
                     return Tps.T_IListGen
                        .MakeGenericType(arg1Type);
                }
                if (items[0] == T_ARRAY)
                {                                     
                     return Tps.T_Array
                        .MakeGenericType(arg1Type);
                }
            }
            else if (items.Count == 3)// Dictionary
            {
               var Arg1TpName = items[1];
               var Arg2TpName = items[2];               

                var arg1Type = TypeCache.GetCustomDomainTypeByFN(Arg1TpName);
                var arg2Type = TypeCache.GetCustomDomainTypeByFN(Arg2TpName);

                if (arg1Type == null || arg2Type == null)
                {  return null;   }

               return Tps.T_IListGen
                        .MakeGenericType(arg1Type, arg2Type);
            }

            return null;
        }



        /// <summary>
        /// Parsing LightName and Getting DataType based on TypeCache's cache
        /// </summary>
        void ParseLightName()
        {
            var items = LightName.SplitNoEntries(TDM);
          
            if (items.Count == 1)
            {
                //PRIMITIVES
                if (Primitives.ContainsKey(items[0]))
                {
                    DataType = Primitives[items[0]];
                }
                else  // customType | enum too 
                {                   
                    DataType = TypeCache.GetCustomDomainTypeByFN(items[0]);
                }
            }
            else if (items.Count == 2)//List or Array
            {
                Arg1TpName = items[1];
                var arg1Type = TypeCache.GetCustomDomainTypeByFN(Arg1TpName);

                if (arg1Type == null)
                {  return; }

                if (items[0] == T_LIST)
                {
                    IsList = true;
                    DataType = Tps.T_IListGen
                        .MakeGenericType(arg1Type);

                }
                if (items[0] == T_ARRAY)
                {
                    IsArray = true;
                    DataType = Tps.T_Array
                        .MakeGenericType(arg1Type);
                }                
            }
            else if (items.Count == 3)// Dictionary
            {                
                Arg1TpName = items[1];
                Arg2TpName = items[2];
                IsDictionary = true;

                var arg1Type = TypeCache.GetCustomDomainTypeByFN(Arg1TpName);
                var arg2Type = TypeCache.GetCustomDomainTypeByFN(Arg2TpName);

                if (arg1Type == null || arg2Type == null)
                { return; }

                DataType = Tps.T_IListGen
                        .MakeGenericType(arg1Type,arg2Type);
            }           
                  
        }
         
 
        public  static string GetTypeLightName(TypeInfoEx tpImfoEx)
        {
            // Is Array
            if (tpImfoEx.IsArray)
            {
                return T_ARRAY + TDM + tpImfoEx.ArrayElementType.FullName;
            }
            // IS LIST / ObservableCollection...
            else if (tpImfoEx.IsIListImplemented)
            {
                return T_LIST + TDM + tpImfoEx.Arg1Type.FullName;
            }
            // IS DICTIONARY
            else if (tpImfoEx.IsIDictionaryImplemented)
            {
               
                return T_DICT + TDM + tpImfoEx.Arg1Type.FullName
                              + TDM + tpImfoEx.Arg2Type.FullName;
            }
            else// Enums + CUstomTypes
            {
               return tpImfoEx.OriginalType.FullName;
            }
        }

    }
}
