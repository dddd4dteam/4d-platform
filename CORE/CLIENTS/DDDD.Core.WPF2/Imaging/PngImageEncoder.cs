
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;
using DDDD.Core.Imaging.Encoder;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Imaging
{
	public class PngImageEncoder : IImageEncoder
	{
		
		#region --------------- CTOR -------------------

		public PngImageEncoder()
		{
		}
 
		#endregion --------------- CTOR -------------------


		/// <summary>
		/// Image Format 
		/// </summary>
		public ImageFormat ImageFormat
		{
			get
			{
				return ImageFormat.Png;
			}
		}


		/// <summary>
		/// Encode  image  into stream. 
		/// </summary>
		/// <param name="image"></param>
		/// <param name="encodingProperties"></param>
		/// <param name="stream"></param>
		public void Encode(BitmapSource image, IList<EncodingProperty> encodingProperties, Stream stream)
		{
            Validator.ATNullReferenceArgDbg(image, nameof(PngImageEncoder), nameof(Encode), nameof(image) );
            Validator.ATNullReferenceArgDbg(stream, nameof(PngImageEncoder), nameof(Encode), nameof(stream));
                      
			PngEncoder.Encode(stream, image as WriteableBitmap ?? Utilities.EvaluateInUiThread<WriteableBitmap>(() => new WriteableBitmap(image), (Exception e) => e));
		}
	}
}