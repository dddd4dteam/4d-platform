using System;
using System.Reflection;

namespace DDDD.Core.Imaging.Filtering
{
	internal class GrayImage
	{
		public float[] Scan0;

		private int _width;

		private int _height;

		public int Height
		{
			get
			{
				return this._height;
			}
		}

		/// <summary>
		/// Access a pixel within the image.
		/// </summary>
		/// <param name="x">X-coordinate</param>
		/// <param name="y">Y-coordinate</param>
		/// <returns>Pixel brightness between 0.0 and 1.0</returns>
		public float this[int x, int y]
		{
			get
			{
				return this.Scan0[y * this._width + x];
			}
			set
			{
				this.Scan0[y * this._width + x] = value;
			}
		}

		public int Width
		{
			get
			{
				return this._width;
			}
		}

		/// <summary>
		/// Returns a new 0.0-initialized image of specified size.
		/// </summary>
		/// <param name="width">Width in pixels</param>
		/// <param name="height">Height in pixels</param>
		public GrayImage(int width, int height)
		{
			this._width = width;
			this._height = height;
			this.Scan0 = new float[width * height];
		}

		/// <summary>
		/// Creates a 0.0 to 1.0 grayscale image from a bitmap.
		/// </summary>
		public GrayImage(byte[,] channel)
		{
			this.Convert(channel);
		}

		private void Convert(byte[,] channel)
		{
			this._width = channel.GetLength(0);
			this._height = channel.GetLength(1);
			this.Scan0 = new float[this._width * this._height];
			int num = 0;
			for (int i = 0; i < this._height; i++)
			{
				for (int j = 0; j < this._width; j++)
				{
					int num1 = num;
					num = num1 + 1;
					this.Scan0[num1] = (float)channel[j, i] / 255f;
				}
			}
		}

		public byte[,] ToByteArray2D()
		{
			byte[,] scan0 = new byte[this._width, this._height];
			int num = 0;
			for (int i = 0; i < this._height; i++)
			{
				for (int j = 0; j < this._width; j++)
				{
					int num1 = num;
					num = num1 + 1;
					scan0[j, i] = (byte)(this.Scan0[num1] * 255f);
				}
			}
			return scan0;
		}
	}
}