using DDDD.Core.Imaging;
using System;
using System.Threading;

namespace DDDD.Core.Imaging.Filtering
{
	internal abstract class Filter
	{
		protected int _newWidth;

		protected int _newHeight;

		protected byte[][,] _sourceData;

		protected byte[][,] _destinationData;

		protected bool _color;

		private FilterProgressEventArgs progressArgs = new FilterProgressEventArgs();

		protected Filter()
		{
		}

		public byte[][,] Apply(byte[][,] imageData, int newWidth, int newHeight)
		{
			this._newHeight = newHeight;
			this._newWidth = newWidth;
			this._color = (int)imageData.Length != 1;
			this._destinationData = Image.CreateRaster(newWidth, newHeight, (int)imageData.Length);
			this._sourceData = imageData;
			this.ApplyFilter();
			return this._destinationData;
		}

		protected abstract void ApplyFilter();

		protected void UpdateProgress(double progress)
		{
			this.progressArgs.Progress = progress;
			if (this.ProgressChanged != null)
			{
				this.ProgressChanged(this, this.progressArgs);
			}
		}

		public event EventHandler<FilterProgressEventArgs> ProgressChanged;
	}
}