using System;

namespace DDDD.Core.Imaging.Filtering
{
	internal class LowpassResize : Filter
	{
		public LowpassResize()
		{
		}

		protected override void ApplyFilter()
		{
			int length = this._sourceData[0].GetLength(0);
			int num = this._sourceData[0].GetLength(1);
			int length1 = (int)this._sourceData.Length;
			double num1 = (double)(length / this._newWidth) * 0.5;
			for (int i = 0; i < length1; i++)
			{
				GrayImage grayImage = new GrayImage(this._sourceData[i]);
				grayImage = Convolution.Instance.GaussianConv(grayImage, num1);
				this._sourceData[i] = grayImage.ToByteArray2D();
			}
			double num2 = (double)length / (double)this._newWidth;
			double num3 = (double)num / (double)this._newHeight;
			NNResize nNResize = new NNResize();
			this._destinationData = nNResize.Apply(this._sourceData, this._newWidth, this._newHeight);
		}
	}
}