
using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace DDDD.Core.Imaging.Encoder
{
	internal class JpegEncoder
	{
		private const int Ss = 0;

		private const int Se = 63;

		private const int Ah = 0;

		private const int Al = 0;

		private JpegEncodeProgressChangedArgs _progress;

		private DecodedJpeg _input;

		private Stream _outStream;

		private HuffmanTable _huf;

		private DCT _dct;

		private int _height;

		private int _width;

		private int _quality;

		private readonly static int[] CompID;

		private readonly static int[] HsampFactor;

		private readonly static int[] VsampFactor;

		private readonly static int[] QtableNumber;

		private readonly static int[] DCtableNumber;

		private readonly static int[] ACtableNumber;

		static JpegEncoder()
		{
			JpegEncoder.CompID = new int[] { 1, 2, 3 };
			JpegEncoder.HsampFactor = new int[] { 1, 1, 1 };
			JpegEncoder.VsampFactor = new int[] { 1, 1, 1 };
			JpegEncoder.QtableNumber = new int[] { 0, 1, 1 };
			JpegEncoder.DCtableNumber = new int[] { 0, 1, 1 };
			JpegEncoder.ACtableNumber = new int[] { 0, 1, 1 };
		}

		public JpegEncoder(WriteableBitmap image, int quality, Stream outStream) : this(JpegEncoder.FromWriteableBitmap(image), quality, outStream)
		{
		}

		internal JpegEncoder(Image image, int quality, Stream outStream) : this(new DecodedJpeg(image), quality, outStream)
		{
		}

		/// <summary>
		/// Encodes a JPEG, preserving the colorspace and metadata of the input JPEG.
		/// </summary>
		/// <param name="decodedJpeg">Decoded Jpeg to start with.</param>
		/// <param name="quality">Quality of the image from 0 to 100.  (Compression from max to min.)</param>
		/// <param name="outStream">Stream where the result will be placed.</param>
		internal JpegEncoder(DecodedJpeg decodedJpeg, int quality, Stream outStream)
		{
			this._input = decodedJpeg;
			this._input.Image.ChangeColorSpace(ColorSpace.YCbCr);
			this._quality = quality;
			this._height = this._input.Image.Height;
			this._width = this._input.Image.Width;
			this._outStream = outStream;
			this._dct = new DCT(this._quality);
			this._huf = new HuffmanTable(null);
		}

		internal void CompressTo(Stream outStream)
		{
			int i;
			int l = 0;
			int m = 0;
			int j = 0;
			int k = 0;
			int n = 0;
			int o = 0;
			byte[,] raster = null;
			float[,] singleArray = new float[8, 8];
			float[,] singleArray1 = new float[8, 8];
			int[] numArray = new int[64];
			int[] numArray1 = new int[this._input.Image.ComponentCount];
			int num = (this._width % 8 != 0 ? (int)(Math.Floor((double)this._width / 8) + 1) * 8 : this._width);
			int num1 = (this._height % 8 != 0 ? (int)(Math.Floor((double)this._height / 8) + 1) * 8 : this._height);
			for (i = 0; i < this._input.Image.ComponentCount; i++)
			{
				num = Math.Min(num, this._input.BlockWidth[i]);
				num1 = Math.Min(num1, this._input.BlockHeight[i]);
			}
			int num2 = 0;
			for (j = 0; j < num1; j++)
			{
				this._progress.EncodeProgress = (double)j / (double)num1;
				if (this.EncodeProgressChanged != null)
				{
					this.EncodeProgressChanged(this, this._progress);
				}
				for (k = 0; k < num; k++)
				{
					num2 = k * 8;
					int num3 = j * 8;
					for (i = 0; i < this._input.Image.ComponentCount; i++)
					{
						int blockWidth = this._input.BlockWidth[i];
						int blockHeight = this._input.BlockHeight[i];
						raster = this._input.Image.Raster[i];
						for (l = 0; l < this._input.VsampFactor[i]; l++)
						{
							for (m = 0; m < this._input.HsampFactor[i]; m++)
							{
								int num4 = m * 8;
								int num5 = l * 8;
								for (n = 0; n < 8; n++)
								{
									int num6 = num3 + num5 + n;
									if (num6 >= this._height)
									{
										break;
									}
									for (o = 0; o < 8; o++)
									{
										int num7 = num2 + num4 + o;
										if (num7 >= this._width)
										{
											break;
										}
										singleArray[n, o] = (float)raster[num7, num6];
									}
								}
								singleArray1 = this._dct.FastFDCT(singleArray);
								numArray = this._dct.QuantizeBlock(singleArray1, JpegEncoder.QtableNumber[i]);
								this._huf.HuffmanBlockEncoder(outStream, numArray, numArray1[i], JpegEncoder.DCtableNumber[i], JpegEncoder.ACtableNumber[i]);
								numArray1[i] = numArray[0];
							}
						}
					}
				}
			}
			this._huf.FlushBuffer(outStream);
		}

		internal void Encode()
		{
			this._progress = new JpegEncodeProgressChangedArgs();
			this.WriteHeaders();
			this.CompressTo(this._outStream);
			this.WriteMarker(new byte[] { 255, 217 });
			this._progress.EncodeProgress = 1;
			if (this.EncodeProgressChanged != null)
			{
				this.EncodeProgressChanged(this, this._progress);
			}
			this._outStream.Flush();
		}

		private static Image FromWriteableBitmap(WriteableBitmap bitmap)
		{
			int pixelWidth = bitmap.PixelWidth;
			int pixelHeight = bitmap.PixelHeight;
#if CLIENT && SL5
			int[] pixels = bitmap.Pixels;
			// ��������� ��� WPF ////////////////////////           
			//int stride =  bitmap.PixelWidth * ( bitmap.Format.BitsPerPixel / 8) + ( bitmap.PixelWidth % 4);
			
#elif CLIENT && WPF
			int stride = bitmap.BackBufferStride;
			int[] pixels = new int[pixelHeight * stride];
			bitmap.CopyPixels(pixels, stride, 0);			
#endif



			byte[][,] numArray = new byte[][,] { new byte[pixelWidth, pixelHeight], new byte[pixelWidth, pixelHeight], new byte[pixelWidth, pixelHeight] };
			int num = 0;
			for (int i = 0; i < pixelHeight; i++)
			{
				for (int j = 0; j < pixelWidth; j++)
				{
					int num1 = num;
					num = num1 + 1;
					int num2 = pixels[num1];
					numArray[0][j, i] = (byte)(num2 >> 16);
					numArray[1][j, i] = (byte)(num2 >> 8);
					numArray[2][j, i] = (byte)num2;
				}
			}
			return new Image(new ColorModel()
			{
				colorspace = ColorSpace.RGB
			}, numArray);
		}

		private void WriteArray(byte[] data)
		{
			int num = ((data[2] & 255) << 8) + (data[3] & 255) + 2;
			this._outStream.Write(data, 0, num);
		}

		internal void WriteHeaders()
		{
			int i;
			int j;
			byte num;
			this.WriteMarker(new byte[] { 255, 216 });
			if (!this._input.HasJFIF)
			{
				this.WriteArray(new byte[] { 255, 224, 0, 16, 74, 70, 73, 70, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0 });
			}
			DDDD.Core.Imaging.IO.BinaryWriter binaryWriter = new DDDD.Core.Imaging.IO.BinaryWriter(this._outStream);
			foreach (JpegHeader metaHeader in this._input.MetaHeaders)
			{
				binaryWriter.Write(255);
				binaryWriter.Write(metaHeader.Marker);
				binaryWriter.Write((short)((int)metaHeader.Data.Length + 2));
				binaryWriter.Write(metaHeader.Data);
			}
			byte[] numArray = new byte[134];
			numArray[0] = 255;
			numArray[1] = 219;
			numArray[2] = 0;
			numArray[3] = 132;
			int num1 = 4;
			for (i = 0; i < 2; i++)
			{
				int num2 = num1;
				num1 = num2 + 1;
				numArray[num2] = (byte)i;
				int[] numArray1 = this._dct.quantum[i];
				for (j = 0; j < 64; j++)
				{
					int num3 = num1;
					num1 = num3 + 1;
					numArray[num3] = (byte)numArray1[ZigZag.ZigZagMap[j]];
				}
			}
			this.WriteArray(numArray);
			byte[] precision = new byte[] { 255, 192, 0, 17, (byte)this._input.Precision, (byte)(this._input.Image.Height >> 8 & 255), (byte)(this._input.Image.Height & 255), (byte)(this._input.Image.Width >> 8 & 255), (byte)(this._input.Image.Width & 255), (byte)this._input.Image.ComponentCount, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			int num4 = 10;
			for (i = 0; i < precision[9]; i++)
			{
				int num5 = num4;
				num4 = num5 + 1;
				precision[num5] = (byte)JpegEncoder.CompID[i];
				int num6 = num4;
				num4 = num6 + 1;
				precision[num6] = (byte)((this._input.HsampFactor[i] << 4) + this._input.VsampFactor[i]);
				int num7 = num4;
				num4 = num7 + 1;
				precision[num7] = (byte)JpegEncoder.QtableNumber[i];
			}
			this.WriteArray(precision);
			num4 = 4;
			int num8 = 4;
			byte[] numArray2 = new byte[17];
			byte[] numArray3 = new byte[] { 255, 196, 0, 0 };
			for (i = 0; i < 4; i++)
			{
				int num9 = 0;
				if (i == 0)
				{
					num = 0;
				}
				else if (i == 1)
				{
					num = 16;
				}
				else
				{
					num = (byte)((i == 2 ? 1 : 17));
				}
				byte num10 = num;
				int num11 = num4;
				num4 = num11 + 1;
				numArray2[num11 - num8] = num10;
				for (j = 0; j < 16; j++)
				{
					int item = this._huf.bitsList[i][j];
					int num12 = num4;
					num4 = num12 + 1;
					numArray2[num12 - num8] = (byte)item;
					num9 = num9 + item;
				}
				int num13 = num4;
				byte[] item1 = new byte[num9];
				for (j = 0; j < num9; j++)
				{
					int num14 = num4;
					num4 = num14 + 1;
					item1[num14 - num13] = (byte)this._huf.val[i][j];
				}
				byte[] numArray4 = new byte[num4];
				Array.Copy(numArray3, 0, numArray4, 0, num8);
				Array.Copy(numArray2, 0, numArray4, num8, 17);
				Array.Copy(item1, 0, numArray4, num8 + 17, num9);
				numArray3 = numArray4;
				num8 = num4;
			}
			numArray3[2] = (byte)(num4 - 2 >> 8 & 255);
			numArray3[3] = (byte)(num4 - 2 & 255);
			this.WriteArray(numArray3);
			byte[] componentCount = new byte[] { 255, 218, 0, 12, (byte)this._input.Image.ComponentCount, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
			num4 = 5;
			for (i = 0; i < componentCount[4]; i++)
			{
				int num15 = num4;
				num4 = num15 + 1;
				componentCount[num15] = (byte)JpegEncoder.CompID[i];
				int num16 = num4;
				num4 = num16 + 1;
				componentCount[num16] = (byte)((JpegEncoder.DCtableNumber[i] << 4) + JpegEncoder.ACtableNumber[i]);
			}
			int num17 = num4;
			num4 = num17 + 1;
			componentCount[num17] = 0;
			int num18 = num4;
			num4 = num18 + 1;
			componentCount[num18] = 63;
			int num19 = num4;
			num4 = num19 + 1;
			componentCount[num19] = 0;
			this.WriteArray(componentCount);
		}

		private void WriteMarker(byte[] data)
		{
			this._outStream.Write(data, 0, 2);
		}

		public event EventHandler<JpegEncodeProgressChangedArgs> EncodeProgressChanged;
	}
}