using System;
using System.Text;

namespace DDDD.Core.Imaging
{
	internal class JpegHeader
	{
		public byte Marker;

		public byte[] Data;

		internal bool IsJFIF;

		public string ToString
		{
			get
			{
				return Encoding.UTF8.GetString(this.Data, 0, (int)this.Data.Length);
			}
		}

		public JpegHeader()
		{
		}
	}
}