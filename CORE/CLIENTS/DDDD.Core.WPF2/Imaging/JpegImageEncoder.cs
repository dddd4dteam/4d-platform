
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;

using DDDD.Core.Diagnostics;
using DDDD.Core.Imaging.Encoder;

namespace DDDD.Core.Imaging
{

	public class JpegImageEncoder : IImageEncoder
	{ 

		#region ------------------- CTOR -----------------

		public JpegImageEncoder()
		{
		}

		#endregion ------------------- CTOR -----------------


		/// <summary>
		/// Image Format
		/// </summary>
		public ImageFormat ImageFormat
		{
			get
			{
				return ImageFormat.Jpeg;
			}
		}


		/// <summary>
		///   Encode  image  into stream. 
		/// </summary>
		/// <param name="image"></param>
		/// <param name="encodingProperties"></param>
		/// <param name="stream"></param>
		public void Encode(BitmapSource image, IList<EncodingProperty> encodingProperties, Stream stream)
		{
			Validator.ATNullReferenceArgDbg(image, nameof(JpegImageEncoder), nameof(Encode), nameof(image));
			Validator.ATNullReferenceArgDbg(stream, nameof(JpegImageEncoder), nameof(Encode), nameof(stream));
			   
			JpegQualityProperty jpegQualityProperty = new JpegQualityProperty()
			{
				Quality = 100
			};
			int quality = EncodingPropertyHelper.GetEncodingPropertyValue<JpegQualityProperty>(encodingProperties, jpegQualityProperty).Quality;
			(new JpegEncoder(image as WriteableBitmap ?? Utilities.EvaluateInUiThread<WriteableBitmap>(() => new WriteableBitmap(image), (Exception e) => e), quality, stream)).Encode();
		}
	}
}