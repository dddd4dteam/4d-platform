using System;

namespace DDDD.Core.Imaging
{
	/// <summary>
	/// Implements the Discrete Cosine Transform with dynamic CIL
	/// </summary>
	internal class DCT
	{
		public const int N = 8;

		private float[] _temp = new float[64];

		private readonly static float[,] c;

		private readonly static float[,] cT;

		public int[][] quantum = new int[2][];

		public double[][] divisors = new double[2][];

		public double[] DivisorsLuminance = new double[64];

		public double[] DivisorsChrominance = new double[64];

		static DCT()
		{
			DCT.c = DCT.buildC();
			DCT.cT = DCT.buildCT();
		}

		internal DCT()
		{
		}

		public DCT(int quality) : this()
		{
			this.Initialize(quality);
		}

		/// <summary>
		/// Precomputes cosine terms in A.3.3 of 
		/// http://www.w3.org/Graphics/JPEG/itu-t81.pdf
		/// Closely follows the term precomputation in the
		/// Java Advanced Imaging library.
		/// </summary>
		private static float[,] buildC()
		{
			float[,] singleArray = new float[8, 8];
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					singleArray[i, j] = (i == 0 ? 0.353553385f : (float)(0.5 * Math.Cos((2 * (double)j + 1) * (double)i * 3.14159265358979 / 16)));
				}
			}
			return singleArray;
		}

		private static float[,] buildCT()
		{
			float[,] singleArray = new float[8, 8];
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					singleArray[j, i] = DCT.c[i, j];
				}
			}
			return singleArray;
		}

		internal float[,] FastFDCT(float[,] input)
		{
			float single;
			float single1;
			float single2;
			float single3;
			float single4;
			float single5;
			float single6;
			float single7;
			float single8;
			float single9;
			float single10;
			float single11;
			float single12;
			float single13;
			float single14;
			float single15;
			float single16;
			float single17;
			float single18;
			int i;
			float[,] singleArray = new float[8, 8];
			for (i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					singleArray[i, j] = input[i, j] - 128f;
				}
			}
			for (i = 0; i < 8; i++)
			{
				single = singleArray[i, 0] + singleArray[i, 7];
				single7 = singleArray[i, 0] - singleArray[i, 7];
				single1 = singleArray[i, 1] + singleArray[i, 6];
				single6 = singleArray[i, 1] - singleArray[i, 6];
				single2 = singleArray[i, 2] + singleArray[i, 5];
				single5 = singleArray[i, 2] - singleArray[i, 5];
				single3 = singleArray[i, 3] + singleArray[i, 4];
				single4 = singleArray[i, 3] - singleArray[i, 4];
				single8 = single + single3;
				single11 = single - single3;
				single9 = single1 + single2;
				single10 = single1 - single2;
				singleArray[i, 0] = single8 + single9;
				singleArray[i, 4] = single8 - single9;
				single12 = (single10 + single11) * 0.707106769f;
				singleArray[i, 2] = single11 + single12;
				singleArray[i, 6] = single11 - single12;
				single8 = single4 + single5;
				single9 = single5 + single6;
				single10 = single6 + single7;
				single16 = (single8 - single10) * 0.382683426f;
				single13 = 0.5411961f * single8 + single16;
				single15 = 1.306563f * single10 + single16;
				single14 = single9 * 0.707106769f;
				single17 = single7 + single14;
				single18 = single7 - single14;
				singleArray[i, 5] = single18 + single13;
				singleArray[i, 3] = single18 - single13;
				singleArray[i, 1] = single17 + single15;
				singleArray[i, 7] = single17 - single15;
			}
			for (i = 0; i < 8; i++)
			{
				single = singleArray[0, i] + singleArray[7, i];
				single7 = singleArray[0, i] - singleArray[7, i];
				single1 = singleArray[1, i] + singleArray[6, i];
				single6 = singleArray[1, i] - singleArray[6, i];
				single2 = singleArray[2, i] + singleArray[5, i];
				single5 = singleArray[2, i] - singleArray[5, i];
				single3 = singleArray[3, i] + singleArray[4, i];
				single4 = singleArray[3, i] - singleArray[4, i];
				single8 = single + single3;
				single11 = single - single3;
				single9 = single1 + single2;
				single10 = single1 - single2;
				singleArray[0, i] = single8 + single9;
				singleArray[4, i] = single8 - single9;
				single12 = (single10 + single11) * 0.707106769f;
				singleArray[2, i] = single11 + single12;
				singleArray[6, i] = single11 - single12;
				single8 = single4 + single5;
				single9 = single5 + single6;
				single10 = single6 + single7;
				single16 = (single8 - single10) * 0.382683426f;
				single13 = 0.5411961f * single8 + single16;
				single15 = 1.306563f * single10 + single16;
				single14 = single9 * 0.707106769f;
				single17 = single7 + single14;
				single18 = single7 - single14;
				singleArray[5, i] = single18 + single13;
				singleArray[3, i] = single18 - single13;
				singleArray[1, i] = single17 + single15;
				singleArray[7, i] = single17 - single15;
			}
			return singleArray;
		}

		/// See figure A.3.3 IDCT (informative) on A-5.
		/// http://www.w3.org/Graphics/JPEG/itu-t81.pdf
		internal byte[,] FastIDCT(float[] input)
		{
			byte[,] numArray = new byte[8, 8];
			float single = 0f;
			int num = 0;
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					single = 0f;
					for (int k = 0; k < 8; k++)
					{
						single = single + input[i * 8 + k] * DCT.c[k, j];
					}
					int num1 = num;
					num = num1 + 1;
					this._temp[num1] = single;
				}
			}
			for (int l = 0; l < 8; l++)
			{
				for (int m = 0; m < 8; m++)
				{
					float single1 = 128f;
					for (int n = 0; n < 8; n++)
					{
						single1 = single1 + DCT.cT[l, n] * this._temp[n * 8 + m];
					}
					if (single1 < 0f)
					{
						numArray[l, m] = 0;
					}
					else if (single1 <= 255f)
					{
						numArray[l, m] = (byte)((double)single1 + 0.5);
					}
					else
					{
						numArray[l, m] = 255;
					}
				}
			}
			return numArray;
		}

		private void Initialize(int quality)
		{
			int i;
			int j;
			int num;
			double[] numArray = new double[] { 1, 1.387039845, 1.306562965, 1.175875602, 1, 0.785694958, 0.5411961, 0.275899379 };
			if (quality <= 0)
			{
				num = 1;
			}
			if (quality > 100)
			{
				num = 100;
			}
			num = (quality >= 50 ? 200 - quality * 2 : 5000 / quality);
			int[] table = JpegQuantizationTable.K1Luminance.getScaledInstance((float)num / 100f, true).Table;
			int num1 = 0;
			for (i = 0; i < 8; i++)
			{
				for (j = 0; j < 8; j++)
				{
					this.DivisorsLuminance[num1] = 1 / ((double)table[num1] * numArray[i] * numArray[j] * 8);
					num1++;
				}
			}
			int[] table1 = JpegQuantizationTable.K2Chrominance.getScaledInstance((float)num / 100f, true).Table;
			num1 = 0;
			for (i = 0; i < 8; i++)
			{
				for (j = 0; j < 8; j++)
				{
					this.DivisorsChrominance[num1] = (double)(1 / ((double)table1[num1] * numArray[i] * numArray[j] * 8));
					num1++;
				}
			}
			this.quantum[0] = table;
			this.divisors[0] = this.DivisorsLuminance;
			this.quantum[1] = table1;
			this.divisors[1] = this.DivisorsChrominance;
		}

		internal int[] QuantizeBlock(float[,] inputData, int code)
		{
			int[] numArray = new int[64];
			int num = 0;
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					numArray[num] = (int)Math.Round((double)inputData[i, j] * this.divisors[code][num]);
					num++;
				}
			}
			return numArray;
		}

		public static void SetValueClipped(byte[,] arr, int i, int j, float val)
		{
			byte num;
			byte[,] numArray = arr;
			int num1 = i;
			int num2 = j;
			if (val < 0f)
			{
				num = 0;
			}
			else if (val > 255f)
			{
				num = 255;
			}
			else
			{
				num = (byte)((double)val + 0.5);
			}
			numArray[num1, num2] = num;
		}
	}
}