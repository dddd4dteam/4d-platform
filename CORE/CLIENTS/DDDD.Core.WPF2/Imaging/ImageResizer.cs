
using System;

using DDDD.Core.Imaging.Filtering;

namespace DDDD.Core.Imaging
{
	internal class ImageResizer
	{
		private ResizeProgressChangedEventArgs progress = new ResizeProgressChangedEventArgs();

		private Image _input;

		public ImageResizer(Image input)
		{
			this._input = input;
		}

		public Image Resize(int maxEdgeLength, ResamplingFilters technique)
		{
			double num = 0;
			num = (this._input.Width <= this._input.Height ? (double)maxEdgeLength / (double)this._input.Height : (double)maxEdgeLength / (double)this._input.Width);
			if (num >= 1)
			{
				throw new ResizeNotNeededException();
			}
			return this.Resize(num, technique);
		}

		public Image Resize(int maxWidth, int maxHeight, ResamplingFilters technique)
		{
			double num = (double)maxWidth / (double)this._input.Width;
			double num1 = (double)maxHeight / (double)this._input.Height;
			double num2 = 0;
			num2 = (num >= num1 ? num1 : num);
			if (num2 >= 1)
			{
				throw new ResizeNotNeededException();
			}
			return this.Resize(num2, technique);
		}

		public Image Resize(double scale, ResamplingFilters technique)
		{
			Filter nNResize;
			int num = (int)(scale * (double)this._input.Height);
			int num1 = (int)(scale * (double)this._input.Width);
			switch (technique)
			{
				case ResamplingFilters.NearestNeighbor:
				{
					nNResize = new NNResize();
					break;
				}
				case ResamplingFilters.LowpassAntiAlias:
				{
					nNResize = new LowpassResize();
					break;
				}
				default:
				{
					throw new NotSupportedException();
				}
			}
			return new Image(this._input.ColorModel, nNResize.Apply(this._input.Raster, num1, num));
		}

		public static bool ResizeNeeded(Image image, int maxEdgeLength)
		{
			return (image.Width > image.Height ? (double)maxEdgeLength / (double)image.Width : (double)maxEdgeLength / (double)image.Height) < 1;
		}

		private void ResizeProgressChanged(object sender, FilterProgressEventArgs e)
		{
			this.progress.Progress = e.Progress;
			if (this.ProgressChanged != null)
			{
				this.ProgressChanged(this, this.progress);
			}
		}

		public event EventHandler<ResizeProgressChangedEventArgs> ProgressChanged;
	}
}