using DDDD.Core.Imaging.IO;
using System.Collections.Generic;
using System.IO;

namespace DDDD.Core.Imaging
{
	internal class HuffmanTable
	{
		public static int HUFFMAN_MAX_TABLES;

		private short[] huffcode = new short[256];

		private short[] huffsize = new short[256];

		private short[] valptr = new short[16];

		private short[] mincode = new short[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

		private short[] maxcode = new short[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 };

		private short[] huffval;

		private short[] bits;

		private int bufferPutBits;

		private int bufferPutBuffer;

		internal int[,] DC_matrix0;

		internal int[,] AC_matrix0;

		internal int[,] DC_matrix1;

		internal int[,] AC_matrix1;

		internal int[][,] DC_matrix;

		internal int[][,] AC_matrix;

		internal int NumOfDCTables;

		internal int NumOfACTables;

		public List<short[]> bitsList;

		public List<short[]> val;

		public static byte JPEG_DC_TABLE;

		public static byte JPEG_AC_TABLE;

		private short lastk;

		static HuffmanTable()
		{
			HuffmanTable.HUFFMAN_MAX_TABLES = 4;
			HuffmanTable.JPEG_DC_TABLE = 0;
			HuffmanTable.JPEG_AC_TABLE = 1;
		}

		internal HuffmanTable(JpegHuffmanTable table)
		{
			if (table != null)
			{
				this.huffval = table.Values;
				this.bits = table.Lengths;
				this.GenerateSizeTable();
				this.GenerateCodeTable();
				this.GenerateDecoderTables();
				return;
			}
			this.bitsList = new List<short[]>()
			{
				JpegHuffmanTable.StdDCLuminance.Lengths,
				JpegHuffmanTable.StdACLuminance.Lengths,
				JpegHuffmanTable.StdDCChrominance.Lengths,
				JpegHuffmanTable.StdACChrominance.Lengths
			};
			this.val = new List<short[]>()
			{
				JpegHuffmanTable.StdDCLuminance.Values,
				JpegHuffmanTable.StdACLuminance.Values,
				JpegHuffmanTable.StdDCChrominance.Values,
				JpegHuffmanTable.StdACChrominance.Values
			};
			this.initHuf();
		}

		/// <summary>
		/// Uses an integer long (32 bits) buffer to store the Huffman encoded bits
		/// and sends them to outStream by the byte.
		/// </summary>
		private void bufferIt(Stream outStream, int code, int size)
		{
			int num = code;
			int num1 = this.bufferPutBits;
			num = num & (1 << (size & 31)) - 1;
			num1 = num1 + size;
			num = num << (24 - num1 & 31);
			num = num | this.bufferPutBuffer;
			while (num1 >= 8)
			{
				int num2 = num >> 16 & 255;
				outStream.WriteByte((byte)num2);
				if (num2 == 255)
				{
					outStream.WriteByte(0);
				}
				num = num << 8;
				num1 = num1 - 8;
			}
			this.bufferPutBuffer = num;
			this.bufferPutBits = num1;
		}

		/// <summary>Figure F.16 - Reads the huffman code bit-by-bit.</summary>
		public int Decode(JPEGBinaryReader JPEGStream)
		{
			short i;
			int num = 0;
			for (i = (short)JPEGStream.ReadBits(1); i > this.maxcode[num]; i = (short)(i | (short)JPEGStream.ReadBits(1)))
			{
				num++;
				i = (short)(i << 1);
			}
			int num1 = this.huffval[i + this.valptr[num]];
			if (num1 < 0)
			{
				num1 = 256 + num1;
			}
			return num1;
		}

		/// <summary>Figure F.12</summary>
		public static int Extend(int diff, int t)
		{
			if (diff < 1 << (t - 1 & 31))
			{
				int num = (-1 << (t & 31)) + 1;
				diff = diff + num;
			}
			return diff;
		}

		public void FlushBuffer(Stream outStream)
		{
			int i;
			int num = this.bufferPutBuffer;
			for (i = this.bufferPutBits; i >= 8; i = i - 8)
			{
				int num1 = num >> 16 & 255;
				outStream.WriteByte((byte)num1);
				if (num1 == 255)
				{
					outStream.WriteByte(0);
				}
				num = num << 8;
			}
			if (i > 0)
			{
				outStream.WriteByte((byte)(num >> 16 & 255));
			}
		}

		/// <summary>See Figure C.2</summary>
		private void GenerateCodeTable()
		{
			short num = 0;
			short num1 = this.huffsize[0];
			short num2 = 0;
			for (short i = 0; i < (int)this.huffsize.Length; i = (short)(i + 1))
			{
				while (this.huffsize[num] == num1)
				{
					this.huffcode[num] = num2;
					num2 = (short)(num2 + 1);
					num = (short)(num + 1);
				}
				num2 = (short)(num2 << 1);
				num1 = (short)(num1 + 1);
			}
		}

		/// <summary>See figure F.15</summary>
		private void GenerateDecoderTables()
		{
			short num = 0;
			for (int i = 0; i < 16; i++)
			{
				if (this.bits[i] != 0)
				{
					this.valptr[i] = num;
				}
				for (int j = 0; j < this.bits[i]; j++)
				{
					if (this.huffcode[j + num] < this.mincode[i] || this.mincode[i] == -1)
					{
						this.mincode[i] = this.huffcode[j + num];
					}
					if (this.huffcode[j + num] > this.maxcode[i])
					{
						this.maxcode[i] = this.huffcode[j + num];
					}
				}
				if (this.mincode[i] != -1)
				{
					this.valptr[i] = (short)(this.valptr[i] - this.mincode[i]);
				}
				num = (short)(num + this.bits[i]);
			}
		}

		/// <summary>See Figure C.1</summary>
		private void GenerateSizeTable()
		{
			short num = 0;
			for (short i = 0; i < (int)this.bits.Length; i = (short)(i + 1))
			{
				for (short j = 0; j < this.bits[i]; j = (short)(j + 1))
				{
					this.huffsize[num] = (short)(i + 1);
					num = (short)(num + 1);
				}
			}
			this.lastk = num;
		}

		/// <summary>
		/// HuffmanBlockEncoder run length encodes and Huffman encodes the quantized data.
		/// </summary>
		internal void HuffmanBlockEncoder(Stream outStream, int[] zigzag, int prec, int DCcode, int ACcode)
		{
			this.NumOfDCTables = 2;
			this.NumOfACTables = 2;
			int num = zigzag[0] - prec;
			int num1 = num;
			int num2 = num;
			if (num2 < 0)
			{
				num2 = -num2;
				num1--;
			}
			int num3 = 0;
			while (num2 != 0)
			{
				num3++;
				num2 = num2 >> 1;
			}
			this.bufferIt(outStream, this.DC_matrix[DCcode][num3, 0], this.DC_matrix[DCcode][num3, 1]);
			if (num3 != 0)
			{
				this.bufferIt(outStream, num1, num3);
			}
			int num4 = 0;
			for (int i = 1; i < 64; i++)
			{
				int num5 = zigzag[ZigZag.ZigZagMap[i]];
				num2 = num5;
				if (num5 != 0)
				{
					while (num4 > 15)
					{
						this.bufferIt(outStream, this.AC_matrix[ACcode][240, 0], this.AC_matrix[ACcode][240, 1]);
						num4 = num4 - 16;
					}
					num1 = num2;
					if (num2 < 0)
					{
						num2 = -num2;
						num1--;
					}
					num3 = 1;
					while (true)
					{
						int num6 = num2 >> 1;
						num2 = num6;
						if (num6 == 0)
						{
							break;
						}
						num3++;
					}
					int num7 = (num4 << 4) + num3;
					this.bufferIt(outStream, this.AC_matrix[ACcode][num7, 0], this.AC_matrix[ACcode][num7, 1]);
					this.bufferIt(outStream, num1, num3);
					num4 = 0;
				}
				else
				{
					num4++;
				}
			}
			if (num4 > 0)
			{
				this.bufferIt(outStream, this.AC_matrix[ACcode][0, 0], this.AC_matrix[ACcode][0, 1]);
			}
		}

		/// <summary>
		/// Initialisation of the Huffman codes for Luminance and Chrominance.
		/// This code results in the same tables created in the IJG Jpeg-6a
		/// library.
		/// </summary>
		public void initHuf()
		{
			int i;
			int j;
			this.DC_matrix0 = new int[12, 2];
			this.DC_matrix1 = new int[12, 2];
			this.AC_matrix0 = new int[255, 2];
			this.AC_matrix1 = new int[255, 2];
			this.DC_matrix = new int[2][,];
			this.AC_matrix = new int[2][,];
			int[] numArray = new int[257];
			int[] numArray1 = new int[257];
			short[] lengths = JpegHuffmanTable.StdDCChrominance.Lengths;
			short[] lengths1 = JpegHuffmanTable.StdACChrominance.Lengths;
			short[] lengths2 = JpegHuffmanTable.StdDCLuminance.Lengths;
			short[] numArray2 = JpegHuffmanTable.StdACLuminance.Lengths;
			short[] values = JpegHuffmanTable.StdDCChrominance.Values;
			short[] values1 = JpegHuffmanTable.StdACChrominance.Values;
			short[] values2 = JpegHuffmanTable.StdDCLuminance.Values;
			short[] values3 = JpegHuffmanTable.StdACLuminance.Values;
			int k = 0;
			for (i = 0; i < 16; i++)
			{
				for (j = 1; j <= lengths[i]; j++)
				{
					int num = k;
					k = num + 1;
					numArray[num] = i + 1;
				}
			}
			numArray[k] = 0;
			int num1 = k;
			int num2 = 0;
			int num3 = numArray[0];
			k = 0;
			while (numArray[k] != 0)
			{
				while (numArray[k] == num3)
				{
					int num4 = k;
					k = num4 + 1;
					numArray1[num4] = num2;
					num2++;
				}
				num2 = num2 << 1;
				num3++;
			}
			for (k = 0; k < num1; k++)
			{
				this.DC_matrix1[values[k], 0] = numArray1[k];
				this.DC_matrix1[values[k], 1] = numArray[k];
			}
			k = 0;
			for (i = 0; i < 16; i++)
			{
				for (j = 1; j <= lengths1[i]; j++)
				{
					int num5 = k;
					k = num5 + 1;
					numArray[num5] = i + 1;
				}
			}
			numArray[k] = 0;
			num1 = k;
			num2 = 0;
			num3 = numArray[0];
			k = 0;
			while (numArray[k] != 0)
			{
				while (numArray[k] == num3)
				{
					int num6 = k;
					k = num6 + 1;
					numArray1[num6] = num2;
					num2++;
				}
				num2 = num2 << 1;
				num3++;
			}
			for (k = 0; k < num1; k++)
			{
				this.AC_matrix1[values1[k], 0] = numArray1[k];
				this.AC_matrix1[values1[k], 1] = numArray[k];
			}
			k = 0;
			for (i = 0; i < 16; i++)
			{
				for (j = 1; j <= lengths2[i]; j++)
				{
					int num7 = k;
					k = num7 + 1;
					numArray[num7] = i + 1;
				}
			}
			numArray[k] = 0;
			num1 = k;
			num2 = 0;
			num3 = numArray[0];
			k = 0;
			while (numArray[k] != 0)
			{
				while (numArray[k] == num3)
				{
					int num8 = k;
					k = num8 + 1;
					numArray1[num8] = num2;
					num2++;
				}
				num2 = num2 << 1;
				num3++;
			}
			for (k = 0; k < num1; k++)
			{
				this.DC_matrix0[values2[k], 0] = numArray1[k];
				this.DC_matrix0[values2[k], 1] = numArray[k];
			}
			k = 0;
			for (i = 0; i < 16; i++)
			{
				for (j = 1; j <= numArray2[i]; j++)
				{
					int num9 = k;
					k = num9 + 1;
					numArray[num9] = i + 1;
				}
			}
			numArray[k] = 0;
			num1 = k;
			num2 = 0;
			num3 = numArray[0];
			k = 0;
			while (numArray[k] != 0)
			{
				while (numArray[k] == num3)
				{
					int num10 = k;
					k = num10 + 1;
					numArray1[num10] = num2;
					num2++;
				}
				num2 = num2 << 1;
				num3++;
			}
			for (int l = 0; l < num1; l++)
			{
				this.AC_matrix0[values3[l], 0] = numArray1[l];
				this.AC_matrix0[values3[l], 1] = numArray[l];
			}
			this.DC_matrix[0] = this.DC_matrix0;
			this.DC_matrix[1] = this.DC_matrix1;
			this.AC_matrix[0] = this.AC_matrix0;
			this.AC_matrix[1] = this.AC_matrix1;
		}
	}
}