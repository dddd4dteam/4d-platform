using Infragistics;
using Infragistics.Controls.Primitives;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Resources;


namespace DDDD.Core.Imaging
{
	internal static class SR
	{
		private static List<ResourceManager> _resourceManagers;

		private static int _customResourcesVersion;

		internal static int CustomResourcesVersion
		{
			get
			{
				return SR._customResourcesVersion;
			}
		}

		static SR()
		{
			Assembly assembly = typeof(SR).Assembly;
			object[] customAttributes = assembly.GetCustomAttributes(typeof(StringResourceLocationAttribute), true);
			if (customAttributes == null || (int)customAttributes.Length == 0)
			{
				throw new InvalidOperationException("Assembly is missing the StringResourceLocation attribute.");
			}
			StringResourceLocationAttribute stringResourceLocationAttribute = customAttributes[0] as StringResourceLocationAttribute;
			if (stringResourceLocationAttribute == null)
			{
				throw new InvalidOperationException("Error retrieving the StringResourceLocation attribute.");
			}
			string[] strArrays = stringResourceLocationAttribute.Location.Split(new char[] { ';' });
			int num = 0;
			string[] strArrays1 = strArrays;
			for (int i = 0; i < (int)strArrays1.Length; i++)
			{
				string str = strArrays1[i];
				if (str != null && str.Length > 0)
				{
					num++;
				}
			}
			SR._resourceManagers = new List<ResourceManager>();
			string[] strArrays2 = strArrays;
			for (int j = 0; j < (int)strArrays2.Length; j++)
			{
				string str1 = strArrays2[j];
				if (str1 != null && str1.Length > 0)
				{
					SR._resourceManagers.Add(new ResourceManager(str1, assembly));
				}
			}
		}

		internal static void AddResource(string name, Assembly assembly)
		{
			ResourceManager resourceManager = new ResourceManager(name, assembly);
			resourceManager.GetString("DummyValue");
			SR._resourceManagers.Insert(0, resourceManager);
			ResourceStringBase.InvalidateCachedResources();
			SR._customResourcesVersion = SR._customResourcesVersion + 1;
		}

		internal static string GetString(string resourceName)
		{
			string str;
			List<ResourceManager>.Enumerator enumerator = SR._resourceManagers.GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					string str1 = enumerator.Current.GetString(resourceName, CultureInfo.CurrentUICulture);
					if (str1 == null)
					{
						continue;
					}
					str = str1;
					return str;
				}
				return null;
			}
			finally
			{
				((IDisposable)enumerator).Dispose();
			}
			return str;
		}

		internal static string GetString(string resourceName, params object[] args)
		{
			string str = SR.GetString(resourceName);
			if (str != null && args != null && (int)args.Length > 0 && str.Length > 2)
			{
				try
				{
					str = string.Format((IFormatProvider)null, str, args);
				}
				catch
				{
				}
			}
			return str;
		}

		internal static void RemoveResource(string name)
		{
			ResourceManager resourceManager = null;
			int num = 0;
			foreach (ResourceManager _resourceManager in SR._resourceManagers)
			{
				if (_resourceManager.BaseName != name)
				{
					num++;
				}
				else
				{
					resourceManager = _resourceManager;
					break;
				}
			}
			if (resourceManager != null)
			{
				SR._resourceManagers.Remove(resourceManager);
				ResourceStringBase.InvalidateCachedResources();
				SR._customResourcesVersion = SR._customResourcesVersion + 1;
			}
		}
	}
}
