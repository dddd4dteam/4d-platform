using System;

namespace DDDD.Core.Imaging
{
	internal sealed class JPEGMarker
	{
		public const byte JFIF_J = 74;

		public const byte JFIF_F = 70;

		public const byte JFIF_I = 73;

		public const byte JFIF_X = 70;

		public const byte JFXX_JPEG = 16;

		public const byte JFXX_ONE_BPP = 17;

		public const byte JFXX_THREE_BPP = 19;

		public const byte XFF = 255;

		public const byte X00 = 0;

		/// <summary>Huffman Table</summary>
		public const byte DHT = 196;

		/// <summary>Quantization Table</summary>
		public const byte DQT = 219;

		/// <summary>Start of Scan</summary>
		public const byte SOS = 218;

		/// <summary>Define Restart Interval</summary>
		public const byte DRI = 221;

		/// <summary>Comment</summary>
		public const byte COM = 254;

		/// <summary>Start of Image</summary>
		public const byte SOI = 216;

		/// <summary>End of Image</summary>
		public const byte EOI = 217;

		/// <summary>Define Number of Lines</summary>
		public const byte DNL = 220;

		public const byte APP0 = 224;

		public const byte APP1 = 225;

		public const byte APP2 = 226;

		public const byte APP3 = 227;

		public const byte APP4 = 228;

		public const byte APP5 = 229;

		public const byte APP6 = 230;

		public const byte APP7 = 231;

		public const byte APP8 = 232;

		public const byte APP9 = 233;

		public const byte APP10 = 234;

		public const byte APP11 = 235;

		public const byte APP12 = 236;

		public const byte APP13 = 237;

		public const byte APP14 = 238;

		public const byte APP15 = 239;

		public const byte RST0 = 208;

		public const byte RST1 = 209;

		public const byte RST2 = 210;

		public const byte RST3 = 211;

		public const byte RST4 = 212;

		public const byte RST5 = 213;

		public const byte RST6 = 214;

		public const byte RST7 = 215;

		/// <summary>Nondifferential Huffman-coding frame (baseline dct)</summary>
		public const byte SOF0 = 192;

		/// <summary>Nondifferential Huffman-coding frame (extended dct)</summary>
		public const byte SOF1 = 193;

		/// <summary>Nondifferential Huffman-coding frame (progressive dct)</summary>
		public const byte SOF2 = 194;

		/// <summary>Nondifferential Huffman-coding frame Lossless (Sequential)</summary>
		public const byte SOF3 = 195;

		/// <summary>Differential Huffman-coding frame Sequential DCT</summary>
		public const byte SOF5 = 197;

		/// <summary>Differential Huffman-coding frame Progressive DCT</summary> 
		public const byte SOF6 = 198;

		/// <summary>Differential Huffman-coding frame lossless</summary>
		public const byte SOF7 = 199;

		/// <summary>Nondifferential Arithmetic-coding frame (extended dct)</summary>
		public const byte SOF9 = 201;

		/// <summary>Nondifferential Arithmetic-coding frame (progressive dct)</summary>
		public const byte SOF10 = 202;

		/// <summary>Nondifferential Arithmetic-coding frame (lossless)</summary>
		public const byte SOF11 = 203;

		/// <summary>Differential Arithmetic-coding frame (sequential dct)</summary>
		public const byte SOF13 = 205;

		/// <summary>Differential Arithmetic-coding frame (progressive dct)</summary>
		public const byte SOF14 = 206;

		/// <summary>Differential Arithmetic-coding frame (lossless)</summary>
		public const byte SOF15 = 207;

		public JPEGMarker()
		{
		}
	}
}