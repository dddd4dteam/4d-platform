using System;

namespace DDDD.Core.Imaging
{
	internal class YCbCr
	{
		public YCbCr()
		{
		}

		public static void fromRGB(ref byte c1, ref byte c2, ref byte c3)
		{
			double num = (double)c1;
			double num1 = (double)c2;
			double num2 = (double)c3;
			c1 = (byte)(0.299 * num + 0.587 * num1 + 0.114 * num2);
			c2 = (byte)(-0.16874 * num - 0.33126 * num1 + 0.5 * num2 + 128);
			c3 = (byte)(0.5 * num - 0.41869 * num1 - 0.08131 * num2 + 128);
		}

		/// * RGB to YCbCr range 0-255 */
		public static float[] fromRGB(float[] data)
		{
			float[] singleArray = new float[] { (float)(0.299 * (double)((float)data[0]) + 0.587 * (double)((float)data[1]) + 0.114 * (double)((float)data[2])), 128f + (float)(-0.16874 * (double)((float)data[0]) - 0.33126 * (double)((float)data[1]) + 0.5 * (double)((float)data[2])), 128f + (float)(0.5 * (double)((float)data[0]) - 0.41869 * (double)((float)data[1]) - 0.08131 * (double)((float)data[2])) };
			return singleArray;
		}

		public static void toRGB(ref byte c1, ref byte c2, ref byte c3)
		{
			byte num;
			byte num1;
			byte num2;
			double num3 = (double)c1;
			double num4 = (double)c2 - 128;
			double num5 = (double)c3 - 128;
			double num6 = num3 + 1.402 * num5;
			double num7 = num3 - 0.34414 * num4 - 0.71414 * num5;
			double num8 = num3 + 1.772 * num4;
			if (num6 > 255)
			{
				num = 255;
			}
			else if (num6 < 0)
			{
				num = 0;
			}
			else
			{
				num = (byte)num6;
			}
			c1 = num;
			if (num7 > 255)
			{
				num1 = 255;
			}
			else if (num7 < 0)
			{
				num1 = 0;
			}
			else
			{
				num1 = (byte)num7;
			}
			c2 = num1;
			if (num8 > 255)
			{
				num2 = 255;
			}
			else if (num8 < 0)
			{
				num2 = 0;
			}
			else
			{
				num2 = (byte)num8;
			}
			c3 = num2;
		}
	}
}