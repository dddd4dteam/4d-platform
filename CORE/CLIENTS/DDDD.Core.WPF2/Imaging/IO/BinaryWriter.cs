using System.IO;

namespace DDDD.Core.Imaging.IO
{
	/// <summary>
	/// A Big-endian binary writer.
	/// </summary>
	internal class BinaryWriter
	{
		private Stream _stream;

		internal BinaryWriter(Stream stream)
		{
			this._stream = stream;
		}

		internal void Write(byte[] val)
		{
			this._stream.Write(val, 0, (int)val.Length);
		}

		internal void Write(byte[] val, int offset, int count)
		{
			this._stream.Write(val, offset, count);
		}

		internal void Write(short val)
		{
			this._stream.WriteByte((byte)(val >> 8 & 255));
			this._stream.WriteByte((byte)(val & 255));
		}

		internal void Write(byte val)
		{
			this._stream.WriteByte(val);
		}
	}
}