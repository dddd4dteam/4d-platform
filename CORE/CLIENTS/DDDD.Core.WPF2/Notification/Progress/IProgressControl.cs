﻿//using Core.Workflow;
using System;
using System.Collections.Generic;



namespace DDDD.BCL
{



    /// <summary>
    /// BusyMode defining UI indication model for Progress Control 
    /// </summary>
    public enum BusyModeEn
    {        
        /// <summary>
        /// InfiniteBusy - Simple Busy indicator with only busy Indication
        /// </summary>
        InfiniteBusy,

        /// <summary>
        /// Режим в котором мы выводим кнопку отмены бесконечного процесса. т.е. начинаем управлять его исполнением
        /// </summary>
        InfiniteCancellableBusy,
        
        /// <summary>
        ///  Кнопки отмены и перезагрузки
        /// </summary>
        InfiniteCancelResetBusy,

        /// <summary>
        /// Percentage progress 
        /// </summary>
        Percentage,
               
        /// <summary>
        /// Percentage progress with cancellation 
        /// </summary>
        PercentageCancellableBusy,

        /// <summary>
        /// Percentage progress with cancellation and resetting(1-n times)  
        /// </summary>
        PercentageCancelResetBusy
    }



    /// <summary>
    /// Сигнальный интерфейс для UI элемента который разработан с поддержкой Нотификации о прогрессе
    /// </summary>
    public interface IProgressControl
    {

        /// <summary>
        /// BusyMode of the progress control:
        ///    InfiniteBusy - простейшая бесконечная занятость
        ///    Percentage - вывод  процентов выполнения для какой-0то задачи
        /// </summary>
        BusyModeEn ProgressBusyMode { get; set; }  


        /// <summary>
        /// Для режима InfineteMode - декламирование простой занятости - бесконечный анимированный-до завершающего сигнала задачи
        /// </summary>
        bool InfiniteIsBusy { get; set; }



        /// <summary>
        /// To Stop or Cancel   [Progress Control] need in [Flow Control]
        /// </summary>
        IFlowComponent FlowComponent { get; }



        


        void ResetFlowComponent(IFlowComponent flowComponent);


        /// <summary>
        ///  
        /// </summary>
        /// <param name="newProgress"></param>
        void ReportProgress(ProgressInfoTp newProgress);


        /// <summary>
        /// We will attach handlers to the control
        /// </summary>
        /// <param name="management"></param>
        void AttachManagement(KeyValuePair<string, Action<object>>[] management);
        


        /// <summary>
        /// 
        /// </summary>
        /// <param name="visibility"></param>
        //void SetVisible(Visibility visibility);
    }



}
