﻿using System;
using System.Net;
using System.ComponentModel;



namespace DDDD.BCL
{

    


    /// <summary>
    /// 
    /// </summary>
    public class ProgressReporter: Progress<ProgressInfoTp>
    {                
	  
    //#region ------------ Singleton  Constructor -----------------------
     
    // // PARAMS :     
    // // Class   -         ProgressReporter            EX: SystemDispatcher - Singleton Class Name           
    // // ClassDesc  -      Progress Reporter to UI- Notification about some states changed in current StateMachine        EX: Диспетчер системы 
	 	 
    // /// <summary>
    // ///  Target Class - ProgressReporter -  Ctor
    // /// </summary>
    // ProgressReporter( )
    // {  
    //     //  Initializing  Target Class         
    // }

    // /// <summary>
    // /// Singleton Getting DevAppID.
    // /// Progress Reporter to UI- Notification about some states changed in current StateMachine
    // /// </summary>     
    // public static ProgressReporter Current
    // {
    //    get
    //    {
    //        return ProgressReporterCreator.instance;
    //    }
    // }
    

    // /// <summary>
    // /// Creator Class - who instantiating Singleton object 
    // /// </summary>
    // class ProgressReporterCreator
    // { 
    //    /// <summary>
    //    /// Explicit static constructor to tell C# compiler
    //    /// not to mark type as beforefieldinit
    //    /// </summary>
    //    static ProgressReporterCreator()
    //    {
    //    }

    //    /// <summary>
    //    /// Single target class instance
    //    /// </summary>
    //    internal static readonly ProgressReporter instance = new ProgressReporter();
    // }

    //#endregion   ----------- Silngleton  Constructor -----------------------
         

        #region  ----------------- CTOR ------------------------
        
           public ProgressReporter(  Action<ProgressInfoTp>  onProgressAction, String ProgressParams=null )
               :base(onProgressAction)
           {
              
           }


           //public ProgressReporter(Action<ProgressInfoTp> onProgressAction, String ProgressParams = null)
           //    : base(onProgressAction)
           //{

           //}

        #endregion ----------------- CTOR ------------------------
        
     
        #region ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------
    
        // ItemName - Progress  -  Ex: Client 
        // Contract - ProgressInfo  -  Ex: V_S8_Client_Vo

        ProgressInfoTp _Progress = new ProgressInfoTp();

        /// <summary>
        ///By default it is making by base ProgressReporter
        ///Description
        /// </summary>
        public ProgressInfoTp Progress
        {
            get{return _Progress;}
            set
            {      
                _Progress = value; //Field changing
                //NotifyProgressChanged();

                OnReport(_Progress);//Reporting 
                //OnPropertyChanged(() => Progress);
            }
            
        }

    #endregion ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------


        /// <summary>
        ///  
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyProgress(ProgressInfoTp progress)
        {
            this.Progress = progress;
        }


        //public static void NotifyProgress( IProgressControl uiProgressElement, ProgressInfoTp progress)
        //{
        //    uiProgressElement.ReportProgress(progress);
            
        //    //this.Progress = progress;
        //}


     }
    

}
