﻿using System;

namespace DDDD.Core.Notification
{


    /// <summary>
    /// Operation Progress change- UI Actions enumeration - that's UI point-change actions. 
    /// It's flagged enum- it means that it can be used for UI multipoint change notification.
    /// </summary>
    [Flags]
    public enum OperationProgressActionsEn
    {
        /// <summary>
        /// Ex:  Show BusyIndicator and Start Busy command
        /// </summary>
        StartCommandIndication = 1,

        /// <summary>
        /// BusyIndicator Content message
        /// </summary>
        ProgressMessage = 2,

        /// <summary>
        /// TextBox Text Message-detail about concrete operation that is continuing now. 
        /// </summary>
        DetailMessage = 4,

        /// <summary>
        ///Clear all TextBox Text Messages that was there before
        /// </summary>
        ClearDetailMessages = 8,

        /// <summary>
        /// Show error message
        /// </summary>
        ErrorMessage = 16,
        
        /// <summary>
        /// Ex: Hide BusyIndicator and Stop Busy command
        /// </summary>
        StopCommandIndication = 32
    }



    /// <summary>
    /// OperationProgressActionsEn 
    /// </summary>
    public interface IOperationProgressInfo<TUIActionsEnum>
    {
        TUIActionsEnum UIActions { get; }

        string OperationTitle { get; }

        string ProgressMessage { get; }

        double PercentComplete { get; }

        bool? Succeeded { get; }

        bool Cancelled { get; }

        string ExceptionMessage { get; }

    }


}
