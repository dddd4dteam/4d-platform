﻿using System;
using DDDD.Core.Extensions;

namespace DDDD.Core.Notification
{




    /// <summary>
    /// Operation Progress Message - to notify UI .
    /// </summary>
    public struct OperationProgressInfo : IOperationProgressInfo<OperationProgressActionsEn>
    {

        public OperationProgressActionsEn UIActions
        { get; private set; }


        /// <summary>
        /// Command ID
        /// </summary>
        public Guid ID
        {
            get;
            private set;
        }


        public bool Cancelled
        { get; private set; }


        string _ProgressMessage;

        public string ProgressMessage
        {
            get
            {
                if (_ProgressMessage.IsNullOrEmpty() && !ExceptionMessage.IsNullOrEmpty())
                {
                    return ExceptionMessage;
                }

                return _ProgressMessage;
            }
            private set { _ProgressMessage = value; }
        }


        public string ExceptionMessage
        { get; private set; }


        public double PercentComplete
        { get; private set; }


        public bool? Succeeded
        { get; private set; }


        public string OperationTitle
        { get; private set; }





        /// <summary>
        /// Operation Progress Message
        /// </summary>
        /// <param name="taskTitle"></param>
        /// <param name="progressMessage"></param>
        /// <param name="exceptionMessage"></param>
        /// <param name="cancelled"></param>
        /// <param name="percentComplete"></param>
        /// <param name="succeeded"></param>
        /// <returns></returns>
        public static OperationProgressInfo Create(OperationProgressActionsEn uiActions, Guid Id, string taskTitle = null, string progressMessage = null, string exceptionMessage = null, bool cancelled = false, double percentComplete = -1, bool? succeeded = null)
        {
            var progress = new OperationProgressInfo();
            progress.UIActions = uiActions;
            progress.ID = Id;
            progress.Cancelled = cancelled;
            progress.ProgressMessage = progressMessage;
            progress.ExceptionMessage = exceptionMessage;
            progress.PercentComplete = percentComplete;
            progress.Succeeded = succeeded;
            progress.OperationTitle = taskTitle;
            return progress;
        }


        //public static TaskProgressInfo Create(string taskTitle = null, string progressMessage = null, string exceptionMessage = null, bool cancelled = false, double percentComplete = -1, bool? succeeded = null)
        //{
        //    var progress = new TaskProgressInfo();

        //    progress.Cancelled = cancelled;
        //    progress.ProgressMessage = progressMessage;
        //    progress.ExceptionMessage = exceptionMessage;
        //    progress.PercentComplete = percentComplete;
        //    progress.Succeeded = succeeded;
        //    progress.TaskTitle = taskTitle;

        //    return progress;
        //}


    }





    ///// <summary>
    /////DCProgressInfoTp - Progress Change Message(s) to UI from custom Dynamic Command handler 
    ///// </summary>    
    //public struct DCProgressInfoTp : IProgessChangedInfo<DCProgressEn>
    //{

    //    /// <summary>
    //    /// Command ID
    //    /// </summary>
    //    public Guid ID
    //    {
    //        get;
    //        private set;
    //    }


    //    /// <summary>
    //    /// Progress Indication Command
    //    /// </summary>
    //    public DCProgressEn Command
    //    {
    //        get;
    //        private set;
    //    }


    //    /// <summary>
    //    /// Progress Indication Message
    //    /// </summary>
    //    public string Message
    //    {
    //        get;
    //        private set;
    //    }

    //    /// <summary>
    //    /// Indication Message on error happend on Server
    //    /// </summary>
    //    public string ErrorMessage
    //    {
    //        get;
    //        private set;
    //    }

    //    /// <summary>
    //    /// Progress Detail Message
    //    /// </summary>
    //    public string DetailMessage
    //    {
    //        get;
    //        private set;
    //    }


    //    /// <summary>
    //    /// Dynamic Command Progress Information- [progress action info] struct that will be transmitted from back-Thread into UI-Thread  
    //    /// </summary>
    //    /// <param name="commandToShowProgress"></param>
    //    /// <returns></returns>
    //    public static DCProgressInfoTp ProgressAction(DCProgressEn commandToShowProgress, Guid Id, string message = null, String detailMessage = null, String errorMessage = null)
    //    {
    //        return new DCProgressInfoTp() { ID = Id, Command = commandToShowProgress, Message = message, DetailMessage = detailMessage, ErrorMessage = errorMessage };
    //    }



    //}

}
