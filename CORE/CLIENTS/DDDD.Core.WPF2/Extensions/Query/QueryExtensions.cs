﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions 
{
    public static class QueryExtensions
    {
        #region   ------------------------ DAL DateTime CONVERSION -------------------------------


        /// <summary>
        /// YYYY-MM-DD HH:MI:SS(24h)     ODBC Canonical 	SELECT CONVERT(VARCHAR(19), GETDATE(), 120) 	1972-01-01 13:42:24
        /// </summary>
        /// <param name="dateTime"></param>
        public static String ToCultureTimeODBCStringFormat(this DateTime dateTime)
        {
            return dateTime.Year.S() + "-" + // "Date2Int(" +

               ((dateTime.Month < 10) ? "0" + dateTime.Month.S() : dateTime.Month.S()) + "-" +
               ((dateTime.Day < 10) ? "0" + dateTime.Day.S() : dateTime.Day.S()) + " " +

               ((dateTime.Hour < 10) ? "0" + dateTime.Hour.S() : dateTime.Hour.S()) + ":" +
               ((dateTime.Minute < 10) ? "0" + dateTime.Minute.S() : dateTime.Minute.S()) + ":" +
               ((dateTime.Second < 10) ? "0" + dateTime.Second.S() : dateTime.Second.S())  //+ ")"             
               ;

        }


        #endregion ------------------------DAL DateTime CONVERSION -------------------------------

    }
}
