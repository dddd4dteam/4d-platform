﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics.Contracts;
using DDDD.Core.HttpRoutes;


namespace DDDD.Core.Extensions
{

    /// <summary>
    /// String class extensions
    /// </summary>
    public static class StringExtensions
    {

        public const string CarriageReturnLineFeed = "\r\n";
        public const string Empty = "";
        public const char CarriageReturn = '\r';
        public const char LineFeed = '\n';
        public const char Tab = '\t';



        #region --------------------------------  To STRING: Str or S ------------------------------------

        /// <summary>
        /// Convert  [someItem] object  to string by  xxx.ToString() method
        /// </summary>
        /// <param name="someItem"></param>
        /// <returns></returns>
        public static string S(this object someItem)
        {
            return (someItem != null) ? someItem.ToString() : "";
        }


        /// <summary>
        /// Convert int32 to String
        /// </summary>
        /// <param name="integerVal"></param>
        /// <returns></returns>
        public static string S(this int integerVal)
        {
            return integerVal.ToString();
        }


        /// <summary>
        /// Convert Enum value to String
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        //public static string S(this Enum enumValue)
        //{
        //    return enumValue.ToString();
        //}
        ///// <summary>
        ///// Convert char to String
        ///// </summary>
        ///// <param name="character"></param>
        ///// <returns></returns>
        //public static string Str(this char character)
        //{
        //    return character.S();
        //}




        #endregion --------------------------------  To STRING: Str or S ------------------------------------


        #region -------------------------------- KV PAIRS by string ----------------------------------

        /// <summary>
        /// Create Struct of KeyValuePair{string-currentString, T- someValue}
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentString"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static KeyValuePair<string, T> KVPair<T>(this string currentString, T value)
        {
            return new KeyValuePair<string, T>(currentString, value);
        }


        /// <summary>
        /// Create Struct of KeyValuePair{string-currentString, string- someValue}
        /// </summary>
        /// <param name="currentString"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static KeyValuePair<string, string> KVPair(this string currentString, string value)
        {
            return new KeyValuePair<string, string>(currentString, value);
        }


        /// <summary>
        /// Create Struct of KeyValuePair{string-currentString, object- someValue}
        /// </summary>
        /// <param name="currentString"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static KeyValuePair<string, object> KVPair(this string currentString, object value)
        {
            return new KeyValuePair<string, object>(currentString, value);
        }

        #endregion -------------------------------- KV PAIRS by string ----------------------------------
        

        #region ------------------------------- STRING FORMATTING -----------------------------


        /// <summary>
        /// String.Format() wrap.
        /// But you can notice that here we have string[] for parameters not object[] as in String.Format.
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <param name="inparams"></param>
        /// <returns></returns>
        public static string Fmt(this string formatMessageMask, params string[] inparams)
        {
            if (inparams.Length > 0)
            {
                return string.Format(formatMessageMask, inparams);
            }
            return formatMessageMask;
        }


        /// <summary>
        /// Extended Fmt() version.
        /// Here we use also use Context parameter,
        /// and try catch block to catch FormattingException and the throw InvalidOperationException with Context info.
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <param name="Context"></param>
        /// <param name="inparams"></param>
        /// <returns></returns>
        public static string FmtEx(this string formatMessageMask, string Context, params string[] inparams)
        {
            try
            {
                string.Format(formatMessageMask, Context);

                if (inparams.Length > 0)
                {
                    return string.Format(formatMessageMask, inparams);
                }

                return formatMessageMask;

            }
            catch (FormatException ex)
            {
                throw new InvalidOperationException(" Formatting exception message error In Context [" + Context + "] : " + ex.Message);
            }
        }
        
        #endregion ------------------------------- STRING FORMATTING -----------------------------



        #region ----------------------------- FORMATTING WWITH PROVIDER ---------------------------------

        /// <summary>
        /// Format string  with Provider. 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <param name="arg0"></param>
        /// <returns></returns>
        public static string FmtWithProv(this string format, IFormatProvider provider, object arg0)
        {
            return format.FmtWithProv(provider, new[] { arg0 });
        }

        /// <summary>
        /// Format string  with Provider.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <returns></returns>
        public static string FmtWithProv(this string format, IFormatProvider provider, object arg0, object arg1)
        {
            return format.FmtWithProv(provider, new[] { arg0, arg1 });
        }

        /// <summary>
        /// Format string  with Provider.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <returns></returns>
        public static string FmtWithProv(this string format, IFormatProvider provider, object arg0, object arg1, object arg2)
        {
            return format.FmtWithProv(provider, new[] { arg0, arg1, arg2 });
        }

        /// <summary>
        /// Format string  with Provider.
        /// </summary>
        /// <param name="format"></param>
        /// <param name="provider"></param>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        /// <param name="arg2"></param>
        /// <param name="arg3"></param>
        /// <returns></returns>
        public static string FmtWithProv(this string format, IFormatProvider provider, object arg0, object arg1, object arg2, object arg3)
        {
            return format.FmtWithProv(provider, new[] { arg0, arg1, arg2, arg3 });
        }


        private static string FmtWithProv(this string format, IFormatProvider provider, params object[] args)
        {
            if (format.IsNullOrEmpty()) throw new InvalidOperationException( "ERROR in [{0}].[{1}]() - Invalid arg [{2}] - value is Null or Empty  ".Fmt(nameof(StringExtensions), nameof(FmtWithProv), nameof(format)  )  );
             
            return string.Format(provider, format, args);
        }



        #endregion ----------------------------- FORMATTING WWITH PROVIDER ---------------------------------




        #region ------------------------------------ CONTAINS /NOT CONTAINS --------------------------------------

        /// <summary>
        /// Полное строковое Имя Содержит один из отрывков строки/
        /// </summary>
        /// <param name="FullName"></param>
        /// <param name="PossibleValues"></param>        
        /// <returns></returns>
        public static bool IsContainsOneOfListItem(this string FullName, List<string> PossibleValues)
        {
            try
            {
                int LengthBefore = FullName.Length;
                if (PossibleValues.Where(pv => FullName.Replace(pv, "").Length < FullName.Length).FirstOrDefault() != null)
                { return true; }
                else return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        ///  Check that current someValue string not contains  [NotContainedString] value
        /// </summary>
        /// <param name="someValue"></param>
        /// <param name="NotContainedString"></param>
        /// <returns></returns>
        public static bool IsNotContains(this string someValue, string NotContainedString)
        {
            return !someValue.Contains(NotContainedString);
        }

        #endregion ------------------------------------ CONTAINS /NOT CONTAINS --------------------------------------
                

        #region ------------------------------ GET BYTES FROM STRING / STRING FROM BYTES -----------------------------------------

        /// <summary>
        ///  Get Bytes from string using direct char[] and Buffer.BlockCopy
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] GetBytes(this string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }


        /// <summary>
        /// Get Bytes from string  with the help of Encoding.UTF8.GetBytes and Buffer.BlockCopy.
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static byte[] GetBytesUTF8(this string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);

            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }


        /// <summary>
        /// Get string from Bytes with the help of Encoding.UTF8.GetString
        /// </summary>
        /// <param name="strdata"></param>
        /// <returns></returns>
        public static string GetStringUTF8(this byte[] strdata)
        {
            return Encoding.UTF8.GetString(strdata, 0, strdata.Length);
        }


        /// <summary>
        ///  Get string from Bytes with the help of direct array and Buffer.BlockCopy
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }



        #endregion ------------------------------ GET BYTES FROM STRING / STRING FROM BYTES -----------------------------------------


        #region ----------------------------------------- SPLITTING OF STRING -----------------------------------------

        /// <summary>
        /// Split string with no empty entries, here we can set delimeters symbols for string-splitting. 
        /// </summary>
        /// <param name="StringValue"></param>
        /// <param name="delimeters"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue,params string[] delimeters)
        {
            try
            {
                if (StringValue == null) return null;
                return StringValue.Split(delimeters, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        /// <summary>
        /// Split string with no empty entries 
        /// </summary>
        /// <param name="StringValue"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue)
        {
            try
            {
                if (StringValue == null) return null;
                return StringValue.Split(new string[] { ",", "|", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }




        /// <summary>
        /// Split string with no empty entries, and also we can use Trim() for each of the parts
        /// </summary>
        /// <param name="StringValue"></param>
        /// <param name="UseTrim"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue, bool UseTrim)
        {
            try
            {
                string[] delimeters = new string[] { ",", "|", ";" };

                if (StringValue == null) return null;
                List<string> resultList = StringValue.Split(delimeters, StringSplitOptions.RemoveEmptyEntries).ToList();

                List<string> resultListTrimmed = null;

                if (UseTrim && resultList != null && resultList.Count > 0)
                {
                    resultListTrimmed = new List<string>();
                    foreach (var item in resultList)
                    {
                        resultListTrimmed.Add(item.Trim());
                    }
                }
                else return resultList;

                return resultListTrimmed;

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        /// <summary>
        ///  Split string  and get Result as string array. Here one String -StringBuilder.S() will be splitted to lines with the help of [delimeter].
        /// </summary>
        /// <param name="inputBldr"></param>
        /// <returns></returns>
        public static string[] SplitResult(this StringBuilder inputBldr, char delimeter = '\n')
        {
            Contract.Requires(inputBldr != null, "SplitResult - inputData   String[]  cannot be null");

            String[] arrayresult = inputBldr.S().SplitToResultExpression();//'\n');
            return arrayresult;
        }


        /// <summary>
        /// Split string  and get Result as string array.  Here one String   will be splitted to lines with the help of [delimeterChar].
        /// Here we also include or exclude Empty strings(true by default).
        /// </summary>
        /// <param name="templateResultString"></param>
        /// <param name="DelimeterChar"></param>
        /// <param name="exludeEmptyString"></param>
        /// <returns></returns>
        public static string[] SplitToResultExpression(this string templateResultString, char delimeterChar = '\n', bool exludeEmptyString = true)
        {
            List<string> endResult = new List<string>();
            string[] splittedArray = templateResultString.Split(delimeterChar);
            foreach (var item in splittedArray)
            {
                if (string.IsNullOrEmpty(item) == false)
                {
                    endResult.Add(item);
                }
            }

            return endResult.ToArray();
        }




        #endregion ----------------------------------------- SPLITTING OF STRING -----------------------------------------



        #region  ---------------------------- CONCATENATING MULTIPLE STRING INTO ONE STRINGBUILDER  ----------------------------
        /// <summary>
        /// Concatenate string array items to one String
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static StringBuilder ToOneString(this string[] inputData)
        {
            Contract.Requires(inputData != null, "ToOneString - inputData String[]  cannot be null");

            StringBuilder strbuilder = new StringBuilder();
            foreach (var item in inputData)
            {
                strbuilder.AppendLine(item);
            }
            return strbuilder;
        }


        #endregion ---------------------------- CONCATENATING MULTIPLE STRING INTO ONE STRINGBUILDER  ----------------------------


        #region -------------------------- IsNull, IsNotNull, IsNullOrEmpty, IsNotNullorEmpty --------------------------

        /// <summary>
        /// Check if current someitem object IS NULL. If someitem IS NULL - then method return true.
        /// </summary>
        /// <param name="someitem"></param>
        /// <returns></returns>
        public static bool IsNull(this object someitem)
        {
            return (someitem == null);
        }

        /// <summary>
        /// Check if current someitem object IS NOT NULL. If someitem IS NOT NULL - then method return true.
        /// </summary>
        /// <param name="someitem"></param>
        /// <returns></returns>
        public static bool IsNotNull(this object someitem)
        {
            return (someitem != null);
        }


        /// <summary>
        /// Condition check for string - if string is not null or empty,then method returns true, else method returns false.
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string formatMessageMask)
        {
            return !string.IsNullOrEmpty(formatMessageMask);
        }



        /// <summary>
        /// Short String.IsNotNullOrEmpty
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string formatMessageMask)
        {
            return string.IsNullOrEmpty(formatMessageMask);
        }

        #endregion -------------------------- IsNull, IsNotNull, IsNullOrEmpty, IsNotNullorEmpty --------------------------



        /// <summary>
        ///  String Value inside [value] between [value.IndexOf([start]) and value.LastIndexOf([end])]
        /// </summary>
        /// <param name="value"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string Between(this string value, string start, string end)
        {
            var startPosition = value.IndexOf(start);
            var endPosition = value.LastIndexOf(end);
            if (startPosition == -1 || endPosition == -1) return string.Empty;

            var adjustedStart = startPosition + start.Length;
            return adjustedStart >= endPosition ? string.Empty : value.Substring(adjustedStart, endPosition - adjustedStart);
        }





        public static KeyValuePair<RouteKeysEn,TVal> KVPair<TVal>(this RouteKeysEn routeKey ,TVal value)
        {
            return new KeyValuePair<RouteKeysEn, TVal>(routeKey, value);
        }

        #region --------------------- DEPENDENCY PROPERTIES EXTENSIONS FROM STRING -------------------------

        /// <summary>
        /// Remove [Property] ending word from static DependencyProperty Name. It may needs only if your static DependencyProperty Name ending with [Property] conventional word.
        /// </summary>
        /// <param name="staticDependencyPropertyName"></param>
        /// <returns></returns>
        public static string RP(this string staticDependencyPropertyName)
        {
            return staticDependencyPropertyName.Replace("Property", "");
        }

        #endregion --------------------- DEPENDENCY PROPERTIES EXTENSIONS FROM STRING -------------------------


    }
}
