﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.Collections;
using DDDD.Core.Diagnostics;

using DDDD.Core.Data.DA2;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Reflection;
using DDDD.Core.Data.Hash;
using System.Resources;

namespace DDDD.Core.Extensions
{



    /// <summary>
    /// IBObject model  extension methods
    /// </summary>
    public static class BOModelExtensions
    {


        #region --------------------- BEGIN Object management -----------------------


        /// <summary>
        /// Get BObjects roles  
        /// </summary>
        /// <param name="currentType"></param>
        /// <returns></returns>
        public static BORoleEn GetBoRoles(this Type currentType)
        {
            Validator.ATNullReferenceArgDbg<Type>(currentType, nameof(GetBoRoles), nameof(currentType));

            var tpMapDA = TypeDAMap.GetExisted(currentType);
            var role = tpMapDA.GetEntMapInf<BORoleEn>(TypeDAMap.Aks.E_BORole);

            return role;
        }


        /// <summary>
        /// Is BO contract contains  BORoleEn [boRole]
        /// </summary>
        /// <param name="currItem"></param>
        /// <param name="boRole"></param>
        /// <returns></returns>
        public static bool IsBObjectInRole(this Type currItem, BORoleEn boRole)
        {
            Validator.ATNullReferenceArgDbg<Type>(currItem, nameof(IsBObjectInRole), nameof(currItem));
            var roles = currItem.GetBoRoles();

            return roles.HasFlag(boRole);
        }

        /// <summary>
        /// If this property roles flagged value contains one [propRole]
        /// </summary>
        /// <param name="currentProptyRoles"></param>
        /// <param name="propRole"></param>
        /// <returns></returns>
        public static bool IsPropertyInRole(this PropertyRoleEn currentProptyRoles, PropertyRoleEn propRole)
        {
            Validator.ATNullReferenceArgDbg<PropertyRoleEn>(currentProptyRoles, nameof(IsPropertyInRole), nameof(currentProptyRoles));
            return (currentProptyRoles.HasFlag(propRole));
        }


        /// <summary>
        /// Get properties which contains  PropertyRoleEn - [role]
        /// </summary>
        /// <param name="currentItemType"></param>
        /// <param name="propertyRole"></param>
        /// <returns></returns>
        public static List<TypeMember> GetPropertiesInRole(this Type currentItemType, PropertyRoleEn propertyRole)
        {
            var tpMapDA = TypeDAMap.GetExisted(currentItemType);
            return tpMapDA.GetPropertiesInRole(propertyRole);
        }



        /// <summary>
        /// If List of BORoles contains [role]         
        /// </summary>
        /// <param name="currentBORoles"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        public static bool BoConatainsRole(this BORolesAttribute currentBORoles, BORoleEn role)
        {
            Validator.ATNullReferenceArgDbg<BORolesAttribute>(currentBORoles, nameof(BoConatainsRole), nameof(currentBORoles));
            return (currentBORoles.ObjectRole.HasFlag(role));
        }





        /// <summary>        
        /// Delete from this collection all values of [MayBeIntersectedCollection] collection.
        /// </summary>
        /// <param name="SourceCollection"></param>
        /// <param name="MayBeIntersectedCollection"></param>
        /// <returns></returns>
        public static String[] RemoveIntersection(this String[] SourceCollection, String[] MayBeIntersectedCollection) //where T : Enum, String
        {
            //Выполнить проверку входных условий если не достаточно данных то
            if (SourceCollection == null || MayBeIntersectedCollection == null) return null;

            List<String> outputCollection = SourceCollection.ToList<String>();

            foreach (var itemMBI in MayBeIntersectedCollection)
            {
                if (SourceCollection.Contains(itemMBI)) outputCollection.Remove(itemMBI);
            }
            return outputCollection.ToArray();
        }


        /// <summary>
        ///Delete from this collection all values of [MayBeIntersectedCollection] collection.
        /// </summary>
        /// <param name="SourceCollection"></param>
        /// <param name="MayBeIntersectedCollection"></param>
        /// <returns></returns>
        public static List<string> RemoveIntersection(this String[] SourceCollection, List<String> MayBeIntersectedCollection) //where T : Enum, String
        {
            //Выполнить проверку входных условий если не достаточно данных то
            if (SourceCollection == null || MayBeIntersectedCollection == null) return null;

            List<String> outputCollection = SourceCollection.ToList<String>();

            foreach (var itemMBI in MayBeIntersectedCollection)
            {
                if (SourceCollection.Contains(itemMBI)) outputCollection.Remove(itemMBI);
            }

            return outputCollection.ToList();
        }



        #region ----------------  PERSISTABLE BO Operations ---------------


        /// <summary>
        /// Is Primary Key of [currentItem] contains  Sql Server  Identity attribute.
        /// </summary>
        /// <param name="currentItemType"></param>
        /// <returns></returns>
        public static bool IsPKIdentity(this Type currentItemType)
        {
            var tpMapDA = TypeDAMap.GetExisted(currentItemType);
            return tpMapDA.IsPrimaryKeyIdentity();
        }


        /// <summary>
        ///Get parent VO-(Value Object)  items for current Type
        ///, by  properties with (PropertyRoleEn.VOFKeyField).
        /// </summary>
        /// <param name="currentType"></param>
        /// <returns></returns>
        public static List<String> GetParentVos(this Type currentItemType)
        {
            var tpMapDA = TypeDAMap.GetExisted(currentItemType);
            return tpMapDA.GetParentVos();
        }








        #endregion ----------------  PERSISTABLE BO Operations ---------------



        #region ---------------   BOContainer --------------------


        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentItemType"></param>
        /// <returns></returns>
        public static BOContainer GetBObjectContainer(this Type currentItemType)
        {
            BOContainer itemBoInfo = null;
            if (currentItemType.IsBObjectInRole(BORoleEn.VObject))
            {
                itemBoInfo = new BOContainer(
                currentItemType.Name,
                currentItemType.FullName,
                currentItemType.IsPKIdentity(),//Уникальный Идентификатор/Знаем даже на клиенте 
                new BOAssociation(currentItemType.GetParentVos()), //Ассоциации пока без LevelToTarget - он проставлется отдельно ниже(в коде который вызывает метод)
                new BOContainerUpdate()
                );
            }

            return itemBoInfo;

        }

        #endregion ---------------  BOContainer --------------------


        ///// <summary>
        ///// Получить валидируемые свойства ОБъекта
        ///// </summary>
        ///// <returns></returns>
        //public static List<PropertyInfo> GetValidatableProperties(this IBObject currentItem)
        //{
        //    try
        //    {
        //        List<PropertyInfo> properties = currentItem.GetType().GetProperties().ToList();

        //        List<PropertyInfo> resultValidatableProperties = new List<PropertyInfo>();
        //        foreach (var itemProperty in properties)
        //        {
        //            List<BOPropertyValidatableAttribute> propertyRoles = itemProperty.GetPropertyAttribute<BOPropertyValidatableAttribute>();

        //            if (propertyRoles != null)
        //            {
        //                foreach (var item in propertyRoles)
        //                {
        //                    if (item.IsValidatable == true) resultValidatableProperties.Add(itemProperty); break;
        //                }
        //            }
        //        }

        //        if (resultValidatableProperties.Count > 0) { return resultValidatableProperties; }
        //        else return null;
        //    }
        //    catch (Exception exc)
        //    { throw exc; }
        //}            


        #endregion --------------------- END Object management -----------------------



        #region ------------- SERVICE MODEL ANALYZE --------------

        /// <summary>
        /// Get ServiceModel of boItemType
        /// </summary>
        /// <param name="boItemType"></param>
        /// <returns></returns>
        public static IServiceModel GetBObjectServiceModel(this Type boItemType)
        {
            //ArgumentValidator.AssertNotNull<Type>(Item, "Type");
            Validator.ATNullReferenceArgDbg<Type>(boItemType, nameof(GetBObjectServiceModel), nameof(boItemType));

            var tpMapDA = TypeDAMap.GetExisted(boItemType);
            return tpMapDA.GetServiceModel();
        }


        /// <summary>
        /// Get assicoated with current targetItemType(VObject) -VObjects  from current VObject Types collection        
        /// </summary>
        /// <param name="VObjetsSet"></param>
        /// <param name="targetItemType"></param>
        /// <returns></returns>
        public static List<Type> GetAssociatedVObjects(this List<Type> VObjetsCollection, Type targetItemType)
        {
            List<Type> associatedTypes = new List<Type>();
            if (targetItemType.IsBObjectInRole(BORoleEn.VObject) == false)
            { return associatedTypes; }

            if (VObjetsCollection[0].IsBObjectInRole(BORoleEn.VObject))
            {
                associatedTypes = VObjetsCollection.Where(tp => tp.GetParentVos()
                                    .Contains(targetItemType.Name))
                                    .ToList();
            }
            return associatedTypes;
        }



        /// <summary>
        /// Attach  BOContextContainer to FlowProcess
        /// </summary>
        /// <param name="dtContext"></param>
        /// <param name="flowInfo"></param>
        public static void AttachBOContextContainerToFlowProcess(this BOContextContainer dtContext, FlowProcessInfo flowInfo)
        {
            dtContext.TargetContainer.Update.FlowContext = flowInfo;

            foreach (var item in dtContext.AllContainers)
            {
                item.Value.Update.FlowContext = flowInfo;
            }
            return;
        }

        /// <summary>
        /// Create BO Navigation Context Container from BOContextContainer
        /// </summary>
        /// <param name="boContext"></param>
        /// <returns></returns>
        public static BONavContextContaier ToNavigationContext(this BOContextContainer boContext)
        {
            return new BONavContextContaier(boContext);
        }

        #endregion ------------- SERVICE MODEL ANALYZE --------------


        /// <summary>
        /// Add to Dictionary of string/string resources strings from ResourceSet object 
        /// </summary>
        /// <param name="crDictionary"></param>
        /// <param name="resourceSet"></param>
        public static void AddToDictionary(this ResourceSet resourceSet, Dictionary<String, String> crDictionary)
        {
            var dictNumerator = resourceSet.GetEnumerator();

            // Get all string resources
            while (dictNumerator.MoveNext())
            {
                // Only string resources
                if (dictNumerator.Value is string && !crDictionary.ContainsKey((string)dictNumerator.Key))
                {
                    crDictionary.Add((string)dictNumerator.Key, (string)dictNumerator.Value);
                }
            }

        }

    }


}
