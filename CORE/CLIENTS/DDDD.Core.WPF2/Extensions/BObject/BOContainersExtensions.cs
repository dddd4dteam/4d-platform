﻿using DDDD.Core.Data.DA2;
using DDDD.Core.Diagnostics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class BOContainersExtensions
    {
        const string AddMethodNm = "Add";
        const string ClearMethodNm = "Clear";

        /// <summary>        
        /// Get Collection Element Type:
        /// <para/> 1 Collection IS NOT Generic and  no Items: - null (ArrayList for example)
        /// <para/> 2 Collection IS Generic :                  - genArg0 Type
        /// <para/> 3 Collection IS NOT Generic AND has Items: - FirstItem Type /// 
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <returns></returns>
        public static Type GetItemType(this IEnumerable currentCollection)
        {
            if (currentCollection == null) return null;

            //1 Collection IS NOT Generic and  no Items: - null (ArrayList for example)
            //2 Collection IS Generic :     - genArg0 Type
            //3 Collection IS NOT Generic AND has Items: - FirstItem Type 


            //1 Collection IS NOT Generic and  no Items: - null (ArrayList for example)
            var collectionType = currentCollection.GetType();
            if (collectionType.IsGenericType == false
                && currentCollection.Count() == 0)
                return null;

            //2 Collection IS Generic :     - genArg0 Type
            if (collectionType.IsGenericType)
            {
                return collectionType.GetGenericArguments()[0];
            }

            //3 Collection IS NOT Generic AND has Items: - FirstItem Type 
            if (currentCollection.Count() > 0)
            {
                return currentCollection.Cast<object>().First().GetType();
            }

            return null;
        }





        /// <summary>
        /// Add range of current collection Items to another collection
        /// Collections should be of the same Type
        /// <para/> 1 If ICollection{IBObject} works then use such way
        /// <para/> 2 If ICollection{IPersistableBO} works then use such way
        /// <para/> 3 If ICollection{object} works then use such way
        /// <para/> 4 use reflection [Add] method if exist
        /// <para/> else  nothig will be added
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="NewItems"></param>
        public static void AddRange(this IEnumerable currentCollection, IEnumerable NewItems)
        {

            if (currentCollection == null || NewItems == null) return;

            Type curColItemType = currentCollection.GetItemType();
            Type newItemType = NewItems.GetItemType();

            //do no add if different types and 
            if (curColItemType != typeof(object)
                && curColItemType != typeof(IBObject)
                && curColItemType != typeof(IPersistableBO)
                && curColItemType != newItemType) return;

            if (NewItems as ICollection<IBObject> != null)
            {
                var iColelction = NewItems as ICollection<IBObject>;
                foreach (var Additem in NewItems)
                { iColelction.Add(Additem as IBObject); }
            }
            else if (NewItems as ICollection<IPersistableBO> != null)
            {
                var iColelction = NewItems as ICollection<IPersistableBO>;
                foreach (var Additem in NewItems)
                { iColelction.Add(Additem as IPersistableBO); }
            }
            else if (NewItems as ICollection<object> != null)
            {
                var iColelction = NewItems as ICollection<object>;
                foreach (var Additem in NewItems)
                { iColelction.Add(Additem); }
            }
            else
            {
                MethodInfo addMethod = currentCollection
                    .GetType().GetMethod(AddMethodNm);
                if (addMethod == null) return;
                foreach (var Additem in NewItems)
                {
                    addMethod.Invoke(currentCollection, new object[] { Additem });
                }
            }
        }

        /// <summary>
        /// Clear all collection elements  if they exist
        /// <para/> 1 If ICollection{IBObject} works then use such way
        /// <para/> 2 If ICollection{IPersistableBO} works then use such way
        /// <para/> 3 If ICollection  works then use such way
        /// <para/> 4 If ICollection{object} works then use such way
        /// <para/> 5 use reflection [Clear] method if exist
        /// <para/>  else  nothig will be clear
        /// </summary>
        /// <param name="currentCollection"></param>
        public static void Clear(this IEnumerable currentCollection)
        {
            if (currentCollection == null) return;

            if (currentCollection as ICollection<IBObject> != null)
            {
                (currentCollection as ICollection<IBObject>).Clear();
            }
            else if (currentCollection as ICollection<IPersistableBO> != null)
            {
                (currentCollection as ICollection<IPersistableBO>).Clear();
            }
            else if (currentCollection as ICollection != null)
            {
                (currentCollection as ICollection).Clear();
            }
            else if (currentCollection as ICollection<object> != null)
            {
                (currentCollection as ICollection<object>).Clear();
            }
            else
            {
                // 2 -просто добавить-когда добавляемый элемент того же типа 
                MethodInfo clearMethod = currentCollection.GetType()
                    .GetMethod(ClearMethodNm);
                if (clearMethod == null) return;

                //Добавим в Коллекцию новый элемент
                clearMethod.Invoke(currentCollection, null);
            }

        }


        /// <summary>
        ///  Count of items in IEnumerable variable
        /// <para/> 1 If ICollection{IBObject} works then use such way
        /// <para/> 2 If ICollection{IPersistableBO} works then use such way
        /// <para/> 3 If ICollection  works then use such way
        /// <para/> 4 If ICollection{object} works then use such way        
        /// <para/>  else count in foreach  of IEnumerable
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static int Count(this IEnumerable collection)
        {
            if (collection.IsNull()) { return 0; }

            if (collection as ICollection<IBObject> != null)
            {
                return (collection as ICollection<IBObject>).Count;
            }
            else if (collection as ICollection<IPersistableBO> != null)
            {
                return (collection as ICollection<IPersistableBO>).Count;
            }
            else if (collection as ICollection != null)
            {
                return (collection as ICollection).Count;
            }
            else if (collection as ICollection<object> != null)
            {
                return (collection as ICollection<object>).Count;
            }
            else
            {
                Int32 count = 0;
                foreach (var item in collection)
                { count++; }
                return count;
            }
        }


        /// <summary>        
        /// Reload means: Clear collection an then AddRange of items from newCollection.
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="Key"></param>
        public static void Reload(this IEnumerable currentCollection, IEnumerable NewCollection)
        {
            if (currentCollection == null || NewCollection == null) { return; }
            if (NewCollection.Count() == 0) { return; }

            if (currentCollection.Count() > 0)
            { currentCollection.Clear(); }
            currentCollection.AddRange(NewCollection);
        }


        /// <summary>
        /// Clone collection: create new List{IBObject}-list, and copy all items from  currentCollection  to this list.
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="NewCollection">IList out Colection as out IEnumerable</param>
        public static IEnumerable CloneCollection(this IEnumerable currentCollection)
        {
            if (currentCollection == null || currentCollection.Count() == 0)
            { return null; }

            List<object> clonedCollection = new List<object>();

            foreach (IBObject item in currentCollection)
            {
                clonedCollection.Add((item as IBObject).Clone());
            }
            return clonedCollection;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <returns></returns>
        public static ObservableCollection<IBObject> CloneBOCollection(this ObservableCollection<IBObject> currentCollection)
        {
            if (currentCollection == null || currentCollection.Count() == 0)
            { return null; }

            ObservableCollection<IBObject> clonedCollection = new ObservableCollection<IBObject>();

            for (int i = 0; i < currentCollection.Count; i++)
            {
                clonedCollection.Add(currentCollection[i].Clone() as IBObject);
            }
            return clonedCollection;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <returns></returns>
        public static ObservableCollection<IBObject> CloneCollectionAsPersistableBO(this ObservableCollection<IBObject> currentCollection)
        {
            if (currentCollection == null || currentCollection.Count() == 0)
            { return null; }

            ObservableCollection<IBObject> clonedCollection = new ObservableCollection<IBObject>();

            for (int i = 0; i < currentCollection.Count; i++)
            {
                //check is VO 
                if (currentCollection[i] is IVobject)
                {
                    var cuurrItem = (currentCollection[i] as IVobject).CloneToTarget();
                    clonedCollection.Add(cuurrItem);
                }
                else
                    clonedCollection.Add(currentCollection[i].Clone() as IPersistableBO);
            }
            return clonedCollection;
        }




        /// <summary>
        /// Remove item : get index of BO item with [itemHashCode] and remove by its index in collection.        /// 
        /// </summary>
        /// <param name="Collection"></param>
        /// <param name="itemHashCode"></param>
        public static void RemoveByHashCode(this ObservableCollection<IBObject> Collection, Int32 itemHashCode)
        {
            Int32 index = -1;
            for (Int32 i = 0; i < Collection.Count; i++)
            {
                if ((Collection[i] as IPersistableBO).GetBObjectHashCode() == itemHashCode) { index = i; break; }
            }

            if (index != -1) Collection.RemoveAt(index);

        }


        /// <summary>
        ///To ObservableCOllectione:  create new  ObservableCollection{object}-newCollection, and copy all items from  currentCollection  to this  newCollection.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <returns></returns>
        public static ObservableCollection<object> ToObservableCollection(this IEnumerable currentCollection)
        // where TItemType1 : class
        {
            if (currentCollection == null) { return null; }

            ObservableCollection<object> objects = new ObservableCollection<object>();
            foreach (object item in currentCollection)
            {
                objects.Add(item);
            }
            return objects;

        }


        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable currentCollection)
        // where TItemType1 : class
        {
            if (currentCollection == null) { return null; }

            ObservableCollection<T> objects = new ObservableCollection<T>();
            foreach (T item in currentCollection)
            {
                objects.Add(item);
            }
            return objects;
        }



        /// <summary>        
        /// Raise Event of collection Changed using reflection
        /// </summary>
        /// <param name="Collection"></param>
        public static void RaiseCollectionCahnged(this INotifyCollectionChanged Collection)
        {
            Validator.ATNullReferenceArgDbg<INotifyCollectionChanged>(Collection, nameof(RaiseCollectionCahnged), nameof(Collection));

            EventInfo evnt = Collection.GetType().GetEvent("CollectionChanged");

            MethodInfo raiseMethod = evnt.GetRaiseMethod();

            if (raiseMethod != null) raiseMethod.Invoke(Collection, null);
        }


    }


}
