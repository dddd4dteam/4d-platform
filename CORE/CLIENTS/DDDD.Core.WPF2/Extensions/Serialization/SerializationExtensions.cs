﻿using System;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace DDDD.Core.Extensions
{
     
    /// <summary>
    /// Serialization Extensions
    /// </summary>
    public static class SerializationExtensions
    {
        /// <summary>
        ///  string Constant in attribute name that told us about ignoring target member. 
        /// </summary>
        const string IgnoreMember = nameof(IgnoreMember);

        /// <summary>
        /// Check if member contains IgnoreMemberAttribute. IgnoreMemberAttribute means that member should be ignored - true. 
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool IsIgnored(this MemberInfo member)
        {
            if (member.GetFirstMemberAttribute<XmlIgnoreAttribute>().IsNotNull()) return true;
#if SERVER || WPF
            if (member.IsField() &&  member.GetFirstMemberAttribute<NonSerializedAttribute>().IsNotNull()) return true;
#endif

            return (member.GetCustomAttributes(false) //non inheritable
                 .Where(at => at.GetType().Name.Contains(IgnoreMember)).FirstOrDefault() != null);
        }


        /// <summary>
        ///  Check if we able to serialize this serializeType by  SerializableAttribute  existance. 
        /// </summary>
        /// <param name="serializeType"></param>
        /// <returns></returns>
        public static bool CanSerialize(this Type serializeType)
        {
#if SERVER || WPF
            if (serializeType.GetFirstTypeAttribute<SerializableAttribute>().IsNotNull()) return true;
#endif
            if (serializeType.IsImplementInterface<IXmlSerializable>()) return true; 
             

            return false;
        }
    }
}
