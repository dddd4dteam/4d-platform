﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class NumbersExtensions
    {


        #region ---------------------- NUMBERS ---------------------------------


        /// <summary>
        ///  If current number  has -the remainder of the division by 2 , equal to zero.
        /// </summary>
        /// <param name="someIntValue"></param>
        /// <returns></returns>
        public static bool IsEvenNumber(this int someIntValue)
        {
            return (someIntValue % 2 == 0);
        }


        /// <summary>
        /// If current number  has -the remainder of the division by 2 , more than zero.
        /// </summary>
        /// <param name="someIntValue"></param>
        /// <returns></returns>
        public static bool IsNotEvenNumber(this int someIntValue)
        {
            return (someIntValue % 2 > 0);
        }


        #endregion ---------------------- NUMBERS ---------------------------------



    }
}
