using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Collections.ObjectModel;

using DDDD.Core.Reflection;
using DDDD.Core.Serialization;

namespace DDDD.Core.Extensions
{

    public static class TypeExtensions
    {


        #region -------------------------------- ENUM TYPE EXTENSIONS --------------------------------------


        /// <summary>
        /// Get enum type values 
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static Array GetEnumValues(Type enumType)
        {
            FieldInfo[] fields = enumType.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);


            object[] values = new object[fields.Length];
            //string[] names = new string[flds.Length];

            for (int i = 0; i < fields.Length; i++)
            {
                //names[i] = flds[i].Name;
                values[i] = fields[i].GetRawConstantValue();
            }

            return values;

        }


        /// <summary>
        /// Get enum type values
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        public static TEnum[] GetEnumValues<TEnum>(this Type type)
        {
            TEnum[] result = new TEnum[0];
            var fields = type.GetFields().Where(fld => fld.Name.IsNotContains("__"));

            if (fields.IsNull() || fields.Count() == 0) return result;
            result = new TEnum[fields.Count()];
            var index = 0;
            foreach (var fld in fields)
            {
                result[index] = (TEnum)Enum.Parse(type, fld.Name, false);
                index++;
            }

            return result;
        }

        /// <summary>
        /// Check if enumType contains field with Name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool? ContainsParam(this Type enumType, string name)
        {
            if (enumType == null || name == null || name == "") return false;
            if (enumType.IsEnum == false) return null;

            string[] names = Enum.GetNames(enumType);

            return names.Contains(name);
        }



        #endregion -------------------------------- ENUM TYPE EXTENSIONS --------------------------------------


        /// <summary>
        /// Get TypeinfoEx value - extended Type information.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static TypeInfoEx GetTypeInfoEx(this Type targetType)
        {
            return TypeInfoEx.Get(targetType);
        }



        /// <summary>
        ///  Get MemberInfo  Only for Field or IsSharedFuncModelGroup
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="MemberKey"></param>
        /// <returns></returns>
        public static MemberInfo GetMemberInfo(this  Type targetType, string MemberKey)
        {
            PropertyInfo pi = targetType.GetProperty(MemberKey);
            FieldInfo fi = targetType.GetField(MemberKey);

            if (pi != null) return pi;
            if (fi != null) return fi;

            throw new NotSupportedException( "GetMemberInfo() support only Field or IsSharedFuncModelGroup. On checking  Member[{0}]".Fmt(MemberKey) );
        }


        /// <summary>
        /// Get Custom Attributes of targetType,  where  attribute is inherited from TBase parent Type. 
        /// </summary>
        /// <typeparam name="TBase"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        public static List<TBase> GetAttributesWithBaseType<TBase>(this Type targetType, bool inherit = false)
        {
            var searchedAttributes = new List<TBase>();
            var serchedCurrentAttributes = targetType.GetCustomAttributes(typeof(TBase), inherit);
            if (serchedCurrentAttributes.IsNull() || serchedCurrentAttributes.Count() == 0)  return searchedAttributes;
            return serchedCurrentAttributes.ToList<TBase>();
        }


        /// <summary>
        /// If currentType implements interface -TIface
        /// </summary>
        /// <typeparam name="TIface"></typeparam>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplementInterface<TIface>(this Type targetType)
        {
            var interfaces = targetType.GetInterfaces();
            if (interfaces.IsNotWorkable()) return false;

            return (interfaces.FirstOrDefault(tIfc => tIfc == typeof(TIface)).IsNotNull());
        }


        /// <summary>
        /// Collect all targetType Attributes which implement interface TInterface. 
        /// </summary>
        /// <typeparam name="TInterface"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="inherit"></param>
        /// <returns></returns>
        public static List<TInterface> GetAttributesWithInterface<TInterface>(this Type targetType, bool inherit = false)
        {            
            var allAttributes = targetType.GetCustomAttributes(inherit);

            if (allAttributes.IsNotWorkable()) return new List<TInterface>();

            var tIfacesAttributes = allAttributes.Where(tAtr => tAtr.GetType().IsImplementInterface<TInterface>());
            if (tIfacesAttributes.IsWorkable())
            {   return tIfacesAttributes.ToList<TInterface>(); }

            return new List<TInterface>();
        }

       




        /// <summary>
        /// Returns the underlying type argument of the specified type.
        /// </summary>
        /// <param name="type">A <see cref="System.Type"/> instance. </param>
        /// <returns><list>
        /// <item>The type argument of the type parameter,
        /// if the type parameter is a closed generic nullable type.</item>
        /// <item>The underlying Type if the type parameter is an enum type.</item>
        /// <item>Otherwise, the type itself.</item>
        /// </list>
        /// </returns>
        public static Type GetUnderlyingType(this Type type)
        {
           // Validator.ATNullReferenceArgDbg(type, nameof(TypeExtensions),nameof(GetUnderlyingType), nameof(type));

            if (type.IsNullable())
                type = type.GetGenericArguments()[0];

            if (type.IsEnum)
                type = Enum.GetUnderlyingType(type);

            return type;
        }



        /// <summary>
        /// Determines whether the specified types are considered equal.
        /// </summary>
        /// <param name="parent">A <see cref="System.Type"/> instance. </param>
        /// <param name="child">A type possible derived from the <c>parent</c> type</param>
        /// <returns>True, when an object instance of the type <c>child</c>
        /// can be used as an object of the type <c>parent</c>; otherwise, false.</returns>
        /// <remarks>Note that nullable types does not have a parent-child relation to it's underlying type.
        /// For example, the 'int?' type (nullable int) and the 'int' type
        /// aren't a parent and it's child.</remarks>
        public static bool IsSameOrParent(this Type child, Type parent )
        {
            if (parent == null) throw new ArgumentNullException("parent");
            if (child == null) throw new ArgumentNullException("child");

            if (parent == child ||
                child.IsEnum && Enum.GetUnderlyingType(child) == parent ||
                child.IsSubclassOf(parent))
            {
                return true;
            }

            if (parent.IsInterface)
            {
                var interfaces = child.GetInterfaces();

                foreach (var t in interfaces)
                    if (t == parent)
                        return true;
            }

            return false;
        }


        /// <summary>
        /// Recursively checks if the type with all of its members are expressable as a value type that may be cast to a pointer.
        /// Equivalent to what compiler does to check for CS0208 error of this statement:
        ///        fixed (int* p = new int[5]) {}
        /// 
        /// An unmanaged-type is any type that isn�t a reference-type and doesn�t contain reference-type fields 
        /// at any level of nesting. In other words, an unmanaged-type is one of the following:
        ///  * sbyte, byte, short, ushort, int, uint, long, ulong, char, float, double, decimal, or bool.
        ///  * Any enum-type.
        ///  * Any pointer-type.
        ///  * Any user-defined struct-type that contains fields of unmanaged-types only.
        /// 
        /// Strings are not in that list, even though you can use them in structs. 
        /// Fixed-size arrays of unmanaged-types are allowed.
        /// </summary>
        public static void ThrowIfNotUnmanagedType(this Type type)
        {
            //ThrowIfNotUnmanagedType(type, new Stack<Type>(4));

            Stack<Type> typesStack = new Stack<Type>(4);

            if ((!type.IsValueType && !type.IsPointer) || type.IsGenericType || type.IsGenericParameter || type.IsArray)
                throw new ArgumentException("Type {0} is not an unmanaged type".Fmt(type.FullName) );

            if (!type.IsPrimitive && !type.IsEnum && !type.IsPointer)
                for (var p = type.DeclaringType; p != null; p = p.DeclaringType)
                    if (p.IsGenericTypeDefinition)
                        throw new ArgumentException(
                            "Type {0} contains a generic type definition declaring type {1}"
                            .Fmt(type.FullName, p.FullName));
            try
            {
                typesStack.Push(type);

                var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

                foreach (var f in fields)
                    if (!typesStack.Contains(f.FieldType))
                        ThrowIfNotUnmanagedType(f.FieldType);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Error in subtype of type {0}. See InnerException.".Fmt(type.FullName), ex);
            }
            finally
            {
                typesStack.Pop();
            }
        }

        
      

        /// <summary>
        /// Substitutes the elements of an array of types for the type parameters
        /// of the current generic type definition and returns a Type object
        /// representing the resulting constructed type.
        /// </summary>
        /// <param name="type">A <see cref="System.Type"/> instance.</param>
        /// <param name="typeArguments">An array of types to be substituted for
        /// the type parameters of the current generic type.</param>
        /// <returns>A Type representing the constructed type formed by substituting
        /// the elements of <paramref name="typeArguments"/> for the type parameters
        /// of the current generic type.</returns>
        /// <seealso cref="System.Type.MakeGenericType"/>
        public static Type TranslateGenericParameters(this Type type, Type[] typeArguments)
        {
            // 'T paramName' case
            //
            if (type.IsGenericParameter)
                return typeArguments[type.GenericParameterPosition];

            // 'List<T> paramName' or something like that.
            //
            if (type.IsGenericType && type.ContainsGenericParameters)
            {
                Type[] genArgs = type.GetGenericArguments();

                for (int i = 0; i < genArgs.Length; ++i)
                    genArgs[i] = TranslateGenericParameters(genArgs[i], typeArguments);

                return type.GetGenericTypeDefinition().MakeGenericType(genArgs);
            }

            // Non-generic type.
            //
            return type;
        }





        #region -------------------------------------- UTILS(TSS.Tools in last) -----------------------------------
    

        /// <summary>
        /// Determine finally if we CanTestDefaultConstructor of Type. 
        /// 1 Check type IsClass and not interface or structure
        /// 2 Check type is not abstract with Message.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="CanTestDefaultConstructorOption"></param>
        /// <returns></returns>
        internal static bool DetermineEnableContractCtorTest(this Type type, bool CanTestDefaultConstructorOption)
        {
            if (CanTestDefaultConstructorOption == false) return false;

            //not interface or structure or enum
            if (type.IsValueType || type.IsInterface || type.IsEnum) return false;

            //not abstract class
            if (type.IsAbstract) return false;

            //TypeDescriptor.Get  // td = new TypeDescriptor();
            return true;
        }


        /// <summary>
        /// Detect if type shouldn't be analyzed by Serializer-so can be add quickly instead of members analyzing  of this complex type. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IfNeedNotToAnalyzeType(this Type type)
        {
            if (type == typeof(byte[])) return false; // byte[] - it's primitive type array

            return (
                type == typeof(Enum)
                || type.IsEnum
                || type.IsInterface
                || type.IsArray
                || type.IsImplement_List()
                || type.IsImplement_Dictionary()
                );
        }


        #endregion  -------------------------------------- UTILS(TSS.Tools in last) -----------------------------------



        #region -------------------------------- 4D Primitive Types -------------------------------------
        /// <summary>
        /// IsPrimitive - Is this  Type  the Primitive  DDDD.BCL  Type.
        ///<para/> [Primitive Type] concept was entered in DDDD.BCL to  classify  .NET types in Serialization Processing.
        ///<para/> DDDD.BCL lib define an enumeration with all enabled Primitive Types in TypePrimitivesEn.        
        ///<para/> All Primitive Types are enumerated by TypePrimitivesEn values. 
        ///<para/> Each  value of TypePrimitivesEn enum can return Type with what it is associated by GetPrimitiveType().
        ///<para/> Each  value of TypePrimitivesEn enum can return TypCode with what it is associated by GetPrimitiveTypeCode()
        ///<para/>  , if it doesn't have correct TypeCode the value will be TypeCode.Empty .
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool Is4DPrimitiveType(this Type type)
        {
            return TypeInfoEx.Is4DPrimitiveType(type);
        }

        /// <summary>
        ///  TypePrimitivesEn enum value will return Primitive .NET Type with what it is associated.
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static Type Get4DPrimitiveType(this TypePrimitiveEn enumValue)
        {
            return TypeInfoEx.Get4DPrimitiveType(enumValue);
        }

        /// <summary>
        /// TypePrimitivesEn enum value will return Primitive .NET TypeCode with what it is associated.
        /// <para/> , if it doesn't have correct TypeCode the value will be TypeCode.Empty 
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static TypeCode Get4DPrimitiveTypeCode(this TypePrimitiveEn enumValue)
        {
            return TypeInfoEx.Get4DPrimitiveTypeCode(enumValue);
        }


        #endregion -------------------------------- 4D PrimitiveTypes -------------------------------------



        #region --------------------------Type MODULE NAME -----------------------------------


        /// <summary>
        /// Get Assembly Name only, without version string. Value  doesn't Contains [.dll] extension.
        /// </summary>
        /// <param name="someAssembly"></param>
        /// <returns></returns>
        public static string GetAssemblyNameEx(this Assembly someAssembly)
        {

#if SERVER || (CLIENT && WPF)
            var reslt = someAssembly.FullName.SplitNoEntries(",")[0].Replace(".dll", ""); ;
#elif CLIENT && (SL5 || WP81)
            var reslt = someAssembly.FullName.SplitNoEntries(",")[0].Replace(".dll", "");;
#endif
            return reslt;

        }


        /// <summary>
        /// Get Assembly-Moduile Name only, without version string. Value Contains [.dll] extension.
        /// </summary>
        /// <param name="someAssembly"></param>
        /// <returns></returns>
        public static string GetModuleName(this Assembly someAssembly)
        {

#if SERVER || (CLIENT &&  WPF)
            var reslt = someAssembly.ManifestModule.Name; // FullName.SplitNoEntries(",")[0] + ".dll";// ManifestModule.Name;//
#elif CLIENT && (SL5 || WP81)
            var reslt = someAssembly.FullName.SplitNoEntries(",")[0] + ".dll";
#endif
            return reslt;

        }


        /// <summary>
        /// Get Type Module file Name. Value Contains [.dll] extension.
        /// </summary>
        /// <param name="someType"></param>
        /// <returns></returns>
        public static string GetTypeModuleName(this Type someType)
        {
            return someType.Assembly.GetModuleName();// FullName.Split(',')[0] + ".dll";//  //ManifestModule.Name; // FullName.Split(',')[0] + ".dll";
        }


        /// <summary>
        /// Get Type Module file Name without [.dll] extension
        /// </summary>
        /// <param name="someType"></param>
        /// <returns></returns>
        public static string GetTypeModuleNameNoExt(this Type someType)
        {
            return someType.Assembly.GetAssemblyNameEx();// FullName.SplitNoEntries(",")[0];  // FullName.Split(',')[0] + ".dll";
        }


        #endregion --------------------------Type MODULE NAME -----------------------------------



        #region ------------------------------- TYPE INFO EX  EXTENSIONS -------------------------------------

        /// <summary>
        /// Gets a value indicating whether this is Nullable{} generic type(Nullable struct type), or  array's element type  is Nullable{} type.
        /// </summary>
        /// <returns>  True, if the type parameter is a closed generic nullable type; otherwise, False. </returns>
        /// <remarks>Arrays of Nullable types are treated as Nullable types.</remarks>
        public static bool IsNullable(this Type type)
        {
            return TypeInfoEx.GetIsNullableType(type);
        }

        /// <summary>
        /// Check that [type] is not Nullable<Type> 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNotNullable(this Type type)
        {
            return !IsNullable(type);
        }


        /// <summary>
        /// If inputType is Nullable{TStruct} or it can be written like TStruct?,  then  return  Underlying TStruct type
        /// <para/>   If inputType is not Nullable{TStruct} then return inputType again.
        /// </summary>
        /// <param name="inputType"></param>
        /// <returns></returns>
        public static Type GetWorkingTypeFromNullableType(this Type inputType)
        {
            return TypeInfoEx.GetWorkingTypeFromNullableType(inputType);
        }


        /// <summary>
        /// 4D Default serializable Collection Type :
        /// <para/> - Array
        /// <para/> - IList based collection type       
        /// <para/> - ObservableCollection  based collection type
        /// <para/> - IDictionary based collection type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static CollectionTypeEn GetBaseCollectionType(this Type type)
        {
            return TypeInfoEx.GetBaseCollectionType(type);
        }

        /// <summary>
        /// When Type has generic parameters, 
        /// and is not derive from one of the  SerializableCollections. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsSerializable(this Type type)
        {
            return TypeInfoEx.IsSerializableType(type);
        }

        /// <summary>
        /// Type Name - flat name that can be used in Debug Code generation for any type class name(generic collection/array/ nullable struct).
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string GetConventionTypeName(this Type type)
        {
            return TypeInfoEx.GetConventionTypeName(type);
        }


      

        /// <summary>
        /// Is type based on System.Delegate class. It can be in 2 cases: 
        /// <para/> -1 when type is simply Delegate member type or 
        /// <para/> -2 when type is event member type.  
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsDelegateOrEvent(this Type type)
        {
            return TypeInfoEx.IsDelegateOrEvent(type);
        }


        /// <summary>
        ///  Try to understand if we able to test Type's Default Ctor. 
        ///  By default ALL types will be tested,and only Array types will be excluded .
        /// <para/> Problem here is that some Types can have internal static increment logic on creation by Default Ctor.
        /// <para/>  - so we'll exclude them from Default Ctor testing.        
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsTestableByDefCtor(this Type type)
        {
            return TypeInfoEx.IsTestableByDefCtorType(type);
        }



        /// <summary>
        /// If this type implement IDisposable.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsDisposable(this Type type)
        {
            return TypeInfoEx.IsDisposableType(type);
        }



        /// <summary>
        /// If  this [targetType] implement ICollection (Arrays/List/Dictionary - all of them implementing ICollection remember this)
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsICollectionType(this Type targetType)
        {
            return TypeInfoEx.Is_ICollectionType(targetType);
        }



        /// <summary>
        /// If  this [targetType] implement interface type [checkInterfaceType], comparing by Type.Name only
        /// <para/> For example if this TargetType implementing - IList, or IList{} or IDIctionary{,}
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="checkInterfaceType"></param>
        /// <returns></returns>
        public static bool IsImplementInterfaceByTypeNameOnly(this Type targetType, Type checkInterfaceType)
        {
            return TypeInfoEx.IsImplementInterfaceByTypeNameOnly(targetType, checkInterfaceType);
        }

        /// <summary>
        /// If  this [targetType] is type [checkGenericType], comparing by Type.Name only
        /// <para/> For example if this TargetType is - ObservableCollection{}
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="checkGenericType"></param>
        /// <returns></returns>
        public static bool IsGenericTypeByTypeNameOnly(this Type targetType, Type checkGenericType)
        {
            return TypeInfoEx.IsGenericTypeByTypeNameOnly(targetType, checkGenericType);
        }




        /// <summary>
        /// If this class implements one of two interfaces IList or IList{}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_List(this Type targetType)
        {
            return TypeInfoEx.IsImplement_List(targetType);
        }


        /// <summary>
        /// If this class implements one of two interfaces IDictionary{,}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_Dictionary(this Type targetType)
        {
            return TypeInfoEx.IsImplement_Dictionary(targetType);
        }


        /// <summary>
        /// Is this Type ObservableCollection{T}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsObservableCollection(this Type targetType)
        {
            return TypeInfoEx.IsGenericTypeByTypeNameOnly(targetType, typeof(ObservableCollection<>));
        }



        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types values can be setted by null Value .  They are - classes  , interface , nullable structs
        /// <para/>    2 category - these Types values can't be setted by null Value. They are - structs 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNullValueEnableType(this Type targetType)
        {
            return TypeInfoEx.IsNullSettableType(targetType);
        }


        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types instances can have  different value type than it's declaration type(with the help of inheritance) .  They are - not sealed classes , interface , nullable structs
        /// <para/>    2 category - these Types instances can't have  different value type than it's declaration type(can't be inherited) . They are - sealed classes and structs
        /// <para/>   NOTE : Arrays, Lists ,Dictionaries checking by their argTypes
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNeedToSureInDataType(this Type targetType)
        {
            return TypeInfoEx.IsNeedToSureInDataType(targetType);
        }


        #endregion ------------------------------- TYPE INFO EX  EXTENSIONS -------------------------------------



        #region -------------------------- TYPE ACTIVATION - CREATEINSTANCE[LAZY] ---------------------------


        const BindingFlags DefaultCtorSearchBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

        /// <summary>
        /// Creating T instance by args parameters. TypeActivator extension wrap.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="someType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T CreateInstanceT<T>(this Type someType, BindingFlags ctorSelector = DefaultCtorSearchBinding, params object[] args)
        {
           return TypeActivator.CreateInstanceT<T>(ctorSelector, args);
        }


        /// <summary>
        /// /// Creating T instance by args parameters.
        /// <para/> Instance will be created in Thread Safety-Lazy Initing way.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="someType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T CreateInstanceTLazy<T>(this Type someType, BindingFlags ctorSelector = DefaultCtorSearchBinding,params object[] args)
        {
            return TypeActivator.CreateInstanceTLazy<T>(ctorSelector, args);
        }

    

        /// <summary>
        /// Creating Boxed into object instance of targetType by Ctor with args parameters. TypeActivator extension wrap.
        /// </summary>
        /// <param name="someType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        public static object CreateInstanceBoxed(this Type someType, BindingFlags ctorSelector = DefaultCtorSearchBinding, params object[] args)
        {
            return TypeActivator.CreateInstanceBoxed(someType, ctorSelector, args);
        }



        /// <summary>
        ///  Creating Boxed into object instance of targetType by Ctor with args parameters
        /// <para/> Instance will be created in Thread Safety-Lazy Initing way.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object CreateInstanceBoxedLazy(this Type targetType,BindingFlags ctorSelector = DefaultCtorSearchBinding, params object[] args)
        {
            return TypeActivator.CreateInstanceBoxedLazy(targetType, ctorSelector, args);
        }




        /// <summary>
        /// Creating TBased  Type sutisfied instance of targetType by Ctor with args parameters        
        /// </summary>
        /// <typeparam name="TBase"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static TBase CreateInstanceTBase<TBase>(this Type targetType, BindingFlags ctorSelector = DefaultCtorSearchBinding, params object[] args)
        {
            return TypeActivator.CreateInstanceTBase<TBase>(targetType, ctorSelector, args);
        }


        /// <summary>
        /// Creating TBased  Type sutisfied instance of targetType by Ctor with args parameters
        /// <para/> Instance will be created in Thread Safety-Lazy Initing way.
        /// </summary>
        /// <typeparam name="TBase"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="ctorSelector"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static TBase CreateInstanceTBaseLazy<TBase>(this Type targetType, BindingFlags ctorSelector = DefaultCtorSearchBinding, params object[] args)
        {
            return TypeActivator.CreateInstanceTBaseLazy<TBase>(targetType, ctorSelector, args);
        }




        #endregion -------------------------- TYPE ACTIVATION - CREATEINSTANCE[LAZY] ---------------------------


    }
}