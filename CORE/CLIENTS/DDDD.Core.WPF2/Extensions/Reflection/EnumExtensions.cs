﻿using DDDD.Core.Diagnostics;
using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class EnumExtensions
    {

        #region  ----------------------- Enum Extensions ----------------------------------------

        /// <summary>
        /// Get enum values as array of TEnum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<TEnum> GetEnumFieldsArray<TEnum>(this Enum someEnum)
           // where TEnum : struct
        {
            Validator.NotNullDbg(someEnum, typeof(EnumExtensions), "GetEnumValuesArray");

            return Enum.GetValues(typeof(TEnum)).Cast<TEnum>();
        }


        /// <summary>
        /// Get enum value  only fields of [enumType] .
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public  static FieldInfo[] GetEnumFields(this Type enumType)
        { 
            var fields = enumType.GetFields(BindingFlags.Static | BindingFlags.FlattenHierarchy);    //GetFieldsEx(BindingFlagsHelper.GetFinalBindingFlags(true, true));

            fields = (from field in fields
                      where field.IsLiteral && field.IsPublic
                      select field).ToArray();

            return fields.ToArray();
        }



        /// <summary>
        /// Get value of nullable TEnum Type from KeyValue{String,String} mapped here as KeyValue{EnumName,FieldName}
        /// </summary>
        /// <typeparam name="TEnum">Type of some custom Enum</typeparam>
        /// <param name="Pair"></param>
        /// <returns>Nullable<TEnum> -  can be null  or TEnum value</returns>
        public static TEnum? GetPairKeyAsEnum<TEnum>(this KeyValuePair<String, String>? Pair)
            where TEnum : struct
        {
            //Validator.NotNullOrEmptyDbg(Pair, "GetPairKeyAsEnum");
            if (Pair == null || Pair.Value.Key == "") return null;

            TEnum tryparsevalue;
            Enum.TryParse<TEnum>(Pair.Value.Key, out tryparsevalue);

            return tryparsevalue;
        }


      



        /// <summary>
        /// Get FieldInfo of Enum.Field value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FieldInfo GetFieldInfo(this Enum value)
        {
            return TypeInfoEx.GetFieldInfo(value);
        }


        /// <summary>
        /// Get Enum.Field value's  TAttribute 
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TAttribute GetEnumValueAttribute<TAttribute>(this Enum value)
            where TAttribute:Attribute
        {
            FieldInfo field = value.GetFieldInfo();
            
            return  Attribute.GetCustomAttribute(field, typeof(TAttribute))
                        as TAttribute;            
        }

        /// <summary>
        /// Get  Enum  Description  attribute
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            var attribute = value.GetEnumValueAttribute<DescriptionAttribute>();

            return attribute == null ? value.S() : attribute.Description;
        }




        /// <summary>
        /// To Lower String Enum.Field value 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToLowerStr(this Enum value)
        {
            return value.S().ToLower();
        }



      


        #endregion----------------------- Enum Extensions ----------------------------------------

    }
}
