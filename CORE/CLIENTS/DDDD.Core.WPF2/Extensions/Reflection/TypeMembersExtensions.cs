﻿using System;
using System.Linq;
using System.Reflection;


namespace DDDD.Core.Extensions
{

    public static class TypeMembersExtensions//
    {

        /// <summary>
        /// Build method Name from member Name.It is used to build method name for Get/Set operation
        /// </summary>
        /// <param name="member"></param>
        /// <param name="StartPart"></param>
        /// <param name="EndPart"></param>
        /// <returns></returns>
        public static string BuildMemberMethodName(this  MemberInfo member, string StartPart, string EndPart = null)
        {
            if (member is FieldInfo) return StartPart + "Field_" + member.Name + EndPart;
            if (member is PropertyInfo) return StartPart + "Property_" + member.Name + EndPart;

            throw new InvalidOperationException(" BuildMemberMethodName() support only Field or IsSharedFuncModelGroup Member");
        }
              
       
        /// <summary>
        /// Is this member PropertyInfo
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool IsProperty(this  MemberInfo member)
        {
            return (member.MemberType == MemberTypes.Property);
        }


        public static bool IsStatic(this PropertyInfo property)
        {
            return property.GetGetMethod().IsStatic;            
        }


        public static bool IsStatic(this FieldInfo field)
        {
            return field.IsStatic;
        }

        public static bool IsStatic(this MemberInfo member)
        {
            if (member.IsProperty()) return (member as PropertyInfo).IsStatic();
            return (member as FieldInfo).IsStatic;
        }



        /// <summary>
        /// If Member writable              -     
        /// <para/> When Member is Field    -  FieldInfo.IsInitOnly  
        /// <para/> When Member is Property -  PropertyInfo.CanWrite
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool? IsWritable(this MemberInfo member)
        {
            if (member == null) return null;
            
            if (member.IsField() )
            {
                return ((member as FieldInfo).IsInitOnly == false); 
            }
            else
            {
                return (member as PropertyInfo).CanWrite;//
            }
        }

        /// <summary>
        /// If this member IS NOT WRITABLE - read only then method returns true.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool? IsNotWritable(this MemberInfo member)
        {
            return ( !IsWritable(member));
        }

        /// <summary>
        /// Is this member FieldInfo
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static bool IsField(this  MemberInfo member)
        {
            return (member is FieldInfo);
        }


        /// <summary>
        /// Only for FieldInfo or PropertyInfo
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public static Type GetMemberType(this  MemberInfo member)
        {
            if (member is FieldInfo) return (member as FieldInfo).FieldType;

            if (member is PropertyInfo) return (member as PropertyInfo).PropertyType;

            throw new NotSupportedException(" GetMemberType() support only Field or IsSharedFuncModelGroup Member");
        }


        /// <summary>
        /// Is this    field.FieldType   assigned from Delegate type- also means that this field is Event.
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public static bool IsDelegateOrEvent(this FieldInfo field)
        {
            return field.FieldType.IsDelegateOrEvent();
        }


        /// <summary>
        /// Is this   property.PropertyType   assigned from Delegate type- also means that this property is Event.
        /// </summary>
        /// <param name="property"></param>
        /// <returns></returns>
        public static bool IsDelegateOrEvent(this PropertyInfo property)
        {
            return    property.PropertyType.IsDelegateOrEvent();
        }



        public static Type[] ToTypeArray(this ParameterInfo[] parameters)
        {
            if (parameters == null) return Type.EmptyTypes;

            return parameters.Select(prmtr => prmtr.ParameterType).ToArray();           
        }
        
        


    }
}
