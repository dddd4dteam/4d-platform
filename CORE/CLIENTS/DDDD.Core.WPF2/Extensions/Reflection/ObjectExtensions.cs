﻿using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class ObjectExtensions
    {

        /// <summary>
        /// Получить значение совйства Объекта
        /// </summary>
        /// <param name="currentObject"></param>
        /// <param name="Property"></param>
        /// <returns></returns>
        public static object GetValue(this object currentObject, String Property)
        {
            if (currentObject == null) return null;

            object reslt;
            PropertyInfo propty = currentObject.GetType().GetProperty(Property);

            if (propty == null) return null;

            reslt = propty.GetValue(currentObject, null);

            return reslt;           
        }


        /// <summary>
        /// Get TypeinfoEx value - extended Type information.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static TypeInfoEx GetTypeInfoEx(this object value )
        {
            return TypeInfoEx.GetByObj(value);
        }


        /// <summary>
        /// If this object is of Deleegate Type. True - yes it is.
        /// </summary>
        /// <param name="targetObject"></param>
        /// <returns></returns>
        public static bool IsDelegate(this object targetObject )
        {
            return (targetObject is Delegate);
        } 

    }
}
