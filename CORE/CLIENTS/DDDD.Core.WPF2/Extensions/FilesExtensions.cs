﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows;

namespace DDDD.Core.Extensions
{
    public static class FilesExtensions
    {



        public static void WriteTextToFileAtomic(this string filePath, string content, Encoding encode, bool createDir, bool append)
        {
            try
            {
                if (filePath.IsNullOrEmpty())
                    return;

                var fileDir = Path.GetDirectoryName(filePath);
                if (createDir)
                {  if (!Directory.Exists(fileDir)) Directory.CreateDirectory(fileDir);
                }
                else if (!Directory.Exists(fileDir))
                   throw new InvalidOperationException($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteTextToFileAtomic)}() - Message DIrectory doesn't exist [{fileDir}]");


                // Write the string array to a new file named "WriteLines.txt".
                
                using (var outputFile = new StreamWriter(filePath, append))
                {
                    outputFile.WriteLine(content);
                }
                //File.WriteAllText(filePath, content);

            }
            catch (Exception exc)
            {
#if DEBUG
                MessageBox.Show($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteTextToFileAtomic)}() - Message [{exc.Message}]");
#endif
                throw new InvalidOperationException($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteTextToFileAtomic)}() - Message [{exc.Message}]", exc);
            }
        }




#if WP81

        public static byte[] ReadAllBytes(this string filepath)
        {
            if (!File.Exists(filepath)) return null;

            var file = new FileInfo(filepath);

            using (var fileReader = file.OpenRead())
            {
                var resultData = new byte[file.Length];
                fileReader.Read(resultData, 0, resultData.Length);
                return resultData;
            }
        }


        public static void WriteAllBytes(this string filepath, byte[] dataToWrite, bool append = false)
        {
            if (dataToWrite == null || dataToWrite.Length == 0) return;

            if (File.Exists(filepath) && append) // append if exist or craete new
            {
                var file = new FileInfo(filepath);

                using (var fileWriter = file.OpenWrite())
                {                   
                    fileWriter.Write(dataToWrite, 0, dataToWrite.Length);                    
                }
            }
            else if (File.Exists(filepath) && append == false) // delete /rewrite file  
            {
                var file = new FileInfo(filepath);

                using (var fileWriter = file.OpenWrite())
                {
                    fileWriter.Write(dataToWrite, 0, dataToWrite.Length);
                }
            } 
            else if ( !File.Exists(filepath)) // file not exist now
            {
                using (var fileWriter = File.Create(filepath))
                {
                    fileWriter.Write(dataToWrite, 0, dataToWrite.Length);
                }
            }
        }




#endif



        public static void WriteBytesToFileAtomic(this string filePath, byte[] content, bool createDir, bool append)
        {
            try
            {
                if (filePath.IsNullOrEmpty())
                    return;

                var fileDir = Path.GetDirectoryName(filePath);
                if (createDir)
                {
                    if (!Directory.Exists(fileDir)) Directory.CreateDirectory(fileDir);
                }
                else if (!Directory.Exists(fileDir)) throw new InvalidOperationException($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteTextToFileAtomic)}() - Message DIrectory doesn't exist [{fileDir}]");
                  
                // Write the string array to a new file named "WriteLines.txt".            
                using (var fileStream = new FileInfo(filePath).OpenWrite()) // write CRM
                {
                    //Buffer.BlockCopy(content.ToCharArray(), 0, dataInBytes, 0, dataInBytes.Length);
                    fileStream.Write(content, 0, content.Length);
                }
                //File.WriteAllBytes(filePath, content); - not all targets(SL5)
            }
            catch (Exception exc)
            {
#if DEBUG

                MessageBox.Show($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteTextToFileAtomic)}() - Message [{exc.Message}]");
#endif

                throw new InvalidOperationException($"ERROR in {nameof(FilesExtensions)}.{nameof(WriteBytesToFileAtomic)}() - Message [{exc.Message}]", exc);
            }
        }





        
        public static void ClearDirectory(this string pathWithDir)
        {
                if (!Directory.Exists(pathWithDir)) return;

#if NET45 || SL5
                var filesInDir = Directory.EnumerateFiles(pathWithDir);

                if (filesInDir != null && filesInDir.Count() > 0)
                {
                    foreach (var fl in filesInDir)
                    {
                        File.Delete(fl);
                    }
                }

                var directoriesInDir = Directory. EnumerateDirectories(pathWithDir);

                if (directoriesInDir != null && directoriesInDir.Count() > 0)
                {
                    foreach (var dr in directoriesInDir) //recursive clear
                    {
                        ClearDirectory(dr);
                    }
                }
            
#elif WP81

            var filesInDir = Directory.GetFiles(pathWithDir);

            if (filesInDir != null && filesInDir.Count() > 0)
            {
                foreach (var fl in filesInDir)
                {
                    File.Delete(fl);
                }
            }

            var directoriesInDir = Directory.GetDirectories(pathWithDir);

            if (directoriesInDir != null && directoriesInDir.Count() > 0)
            {
                foreach (var dr in directoriesInDir) //recursive clear
                {
                    ClearDirectory(dr);
                }
            }
           
#endif

        }





        #region --------------------------- FILE EXTENSION --------------------------------

        /// <summary>
        /// Getting file extension based on full fileName.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetFileExtension(this string fileName)
        {
            if (fileName.IsNullOrEmpty()) return null;

            var nameParts = fileName.SplitNoEntries(".");
            return ("." + nameParts.LastOrDefault());
        }

        #endregion --------------------------- FILE EXTENSION --------------------------------



    }
}
