﻿using System.Collections.Generic;
using System.IO;

namespace DDDD.Core.Extensions
{
    public static class NullReferenceExtensions
    {


        /// <summary>
        /// Is this stream instance is not null and stream.Length is more then 0
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static bool IsWorkable(this Stream stream)
        {
            return (stream != null && stream.Length > 0 );
        }
    }
}
