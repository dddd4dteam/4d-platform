﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Kellerman-Serialization")]
[assembly: AssemblyDescription("Serialization Library for Windows Phone 7, Silverlight, .NET, Mono, MonoDroid, and MonoTouch")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Kellerman Software")]
[assembly: AssemblyProduct("Kellerman-Serialization")]
[assembly: AssemblyCopyright("Copyright © 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d1236fe1-9ba4-4ce7-8e33-b8e67ffcf092")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.07.0.0")]
[assembly: AssemblyFileVersion("2.07.0.0")]

[assembly: CLSCompliant(true)]
