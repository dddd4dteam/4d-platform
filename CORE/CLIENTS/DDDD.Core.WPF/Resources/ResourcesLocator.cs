﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Resources
{
   
    /// <summary>
    /// ResourcesLocator - have the following features:
    /// 1 Feature - Can Discover and Cache Default Resource Managers from AppDomain.CurrentDomain.GetAssemblies() 
    ///      On ResourcesLocator first creating(static ctor) and     
    /// 2 Feature - Can Get Default Resource Manager by some .NET Type  or throw an error.  
    /// </summary>
    /// <remarks>
    /// Notice: ResourcesLocator is Fundamental - 1 level class so here we can't use any Validator Check to avoid recursion and StackOverflow.
    /// </remarks>
    public static class ResourcesLocator
    {
        #region -------------------------------- CTOR ---------------------------------

        static ResourcesLocator()
        {
            Initialize();
        }

        #endregion -------------------------------- CTOR ---------------------------------
        

        #region -------------------------------- FIELDS ------------------------------------

        private static readonly object _locker = new object();

        static readonly Dictionary<String, ResourceManager> cachedAsmblFullNameResManagers = new Dictionary<String, ResourceManager>();

        static readonly List<String> failedAsmblFullName = new List<String>();

        #endregion -------------------------------- FIELDS ------------------------------------


        static bool IsInitialized = false; 


        internal static void Initialize()
        {
            if (IsInitialized == false)
            {
                lock (_locker)
                {
                    if (IsInitialized == false)
                    {
                        DiscoverDefaultResManagers();        
                        IsInitialized = true;
                    }
                }
            }
            
        }


        static void DiscoverDefaultResManagers()
        {
            try
            {

                lock (_locker)
                {

                    var assembliesWithDefResManagers = AppDomain.CurrentDomain.GetAssemblies()
                        .Where(asmbl => asmbl.GetCustomAttributes(typeof(DefaultResourceManagerAttribute), false) != null);

                    foreach (var asmbly in assembliesWithDefResManagers)
                    {
                        var asmblyDefResManagerInfo = asmbly.GetCustomAttributes(typeof(DefaultResourceManagerAttribute), false)
                                                            .FirstOrDefault() as DefaultResourceManagerAttribute;
                        if (asmblyDefResManagerInfo == null) continue;

                        var resourceManager = asmblyDefResManagerInfo.DefResourceManagerType
                                                    .GetProperty("ResourceManager", BindingFlags.NonPublic| BindingFlags.Static)
                                                    .GetValue(null, null) as ResourceManager;

                        cachedAsmblFullNameResManagers.Add(asmbly.FullName, resourceManager);

                    }
                }

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

        /// <summary>
        /// Get Default Resource Manager by some Type.
        /// We will check if tis type Assembly contains Default Resource Manager and return it if it's exist. 
        /// </summary>
        /// <param name="contextType"></param>
        /// <returns>GetDefResourceManager returns:
        ///  - instance of ResourceManager,
        /// -  also can return null value if ResourceManager wasn't found
        /// </returns>
        public static ResourceManager GetDefResourceManager(Type contextType)
        {
            ResourceManager resultResManager = null;

            if (cachedAsmblFullNameResManagers.TryGetValue(contextType.Assembly.FullName, out resultResManager))
            {   return resultResManager;
            }

            if (failedAsmblFullName.Contains(contextType.Assembly.FullName)) // if it already known failed assembly
            {  return null;   
            }

            lock (_locker) //if not founded default  assembly Default Resource Manager - try to get
            {
                if (!cachedAsmblFullNameResManagers.TryGetValue(contextType.Assembly.FullName, out resultResManager))
                 {
                     var asmblyDefResManagerInfo = contextType.Assembly.GetCustomAttributes(typeof(DefaultResourceManagerAttribute) , false).FirstOrDefault() as DefaultResourceManagerAttribute;
                     if (asmblyDefResManagerInfo == null)
                     {
                         //add to failed assembly- assembly that doesn't contains Default ResoueceManager
                         failedAsmblFullName.Add(contextType.Assembly.FullName);
                         return null;
                     }
                     resultResManager = asmblyDefResManagerInfo.DefResourceManagerType
                                                .GetProperty("ResourceManager", BindingFlags.Static| BindingFlags.NonPublic)
                                                .GetValue(null, null) as ResourceManager;
                     cachedAsmblFullNameResManagers.Add(contextType.Assembly.FullName, resultResManager);
                 } 
            }

            return resultResManager;


        }


        /// <summary>
        /// Try to get Default Resource Manager by some Type. 
        /// TryGetDefResourceManager returns: 
        /// - true  - If defResourceManager was setted correctly-it's value is not null 
        /// - false - If defResourceManager was setted by null- it's value is null
        /// </summary>
        /// <param name="type"></param>
        /// <param name="defResourceManager"></param>
        /// <returns>
        /// TryGetDefResourceManager returns: 
        /// - true  - If defResourceManager was setted correctly-it's value is not null 
        /// - false - If defResourceManager was setted by null- it's value is null
        /// </returns>
        public static bool TryGetDefResourceManager(Type type, out ResourceManager defResourceManager)
        {
            defResourceManager = GetDefResourceManager(type);

            if (defResourceManager == null) return false;
            return true;
        }



    }
}




#region ------------------------------GARBAGE ---------------------------------------

//static readonly Dictionary<Type, ResourceManager> cachedTypeResManagers = new Dictionary<Type, ResourceManager>();

///// <summary>
///// 
///// </summary>
///// <param name="asmblName"></param>
///// <returns>Can be null if assembly doesn't declare Default Resource Manager</returns>
//public static ResourceManager GetAsmblyDefResManager( Type classInAssembly)
//{
//    ResourceManager resultResManager = null;
//    //1 level cache
//    if( cachedTypeResManagers.TryGetValue(classInAssembly, out resultResManager)  )            
//    {  return resultResManager;
//    }

//    //2 level cache 
//    var asmblName = classInAssembly.Assembly.GetName();
//    if (cachedResManagers.TryGetValue(asmblName, out resultResManager))
//    {
//        lock (_locker)
//        {
//            if (!cachedTypeResManagers.TryGetValue(classInAssembly, out resultResManager))
//            {
//                cachedTypeResManagers.Add(classInAssembly, resultResManager);        
//            }                    
//        }

//        return resultResManager;
//    }
//    else //assemble need to cache 
//    {
//        lock (_locker)
//        {
//            if (cachedResManagers.TryGetValue(asmblName, out resultResManager))
//            {

//                var asmblyDefResManagerInfo = classInAssembly.Assembly.GetCustomAttribute<DefaultResourceManagerAttribute>();
//                cachedResManagers.Add(asmblName,
//                                        asmblyDefResManagerInfo.DefResourceManagerType
//                                            .GetProperty("ResourceManager")
//                                            .GetValue(null) as ResourceManager
//                                        );
//            }

//            resultResManager = cachedResManagers[asmblName];

//            cachedTypeResManagers.Add(classInAssembly, resultResManager);
//        }
//    }            

//    return resultResManager;

//}

#endregion ------------------------------GARBAGE ---------------------------------------