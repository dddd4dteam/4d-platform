﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Resources
{
   
    
    /// <summary>
    /// Assembly register default Resource manager for messages of all classes inside it.
    /// <para/> All classes inside this assembly can use centralized string messages store in *.resx files by locations.
    /// <para/> All of these *.resx files should have one ResourceManager class that should be declared by this assembly attribute.
    /// <para/> So we'll get Association of [type -> its ResourceManger(by Type.Assembly)]. 
    /// <para/> And to get Type's ResourceManager we can use -ResourceLocator.GetDefResourceManager(Type contextType) . 
    /// <para/> Classes Validator suggest its check method overloads, where we need to point OperationContext 
    /// and by OperationContext.Type it will find the appropriate message.   
    /// </summary>
    [AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
    public sealed class DefaultResourceManagerAttribute : Attribute
    {

        // This is a positional argument
        public DefaultResourceManagerAttribute(Type defResourceManagerType)
        {
            _DefResourceManagerType = defResourceManagerType;
        }
        

        /// <summary>
        /// See the attribute guidelines at 
        /// </summary>
        readonly Type _DefResourceManagerType;


        public Type DefResourceManagerType
        {
            get { return _DefResourceManagerType; }
        }

    }


}
