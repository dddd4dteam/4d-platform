﻿
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.Diagnostics;
using DDDD.Core.Exceptions;
using DDDD.Core.Resources;

namespace DDDD.Core.Resources
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageInspector
    {
        
        //RK - Resource Key
        const string RK_GetMessageTemplate_1ArgCheck  = "MessageInspector_GetMessageTemplate_1ArgNullCheck";
         


        /// <summary>
        ///  Get Resource String by key. Here we don't use Fmt with OperationContext 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        static string GetRS(string key)
        {
            return 
                RCX.ResourceManager
                            .GetString(key, CultureInfo.CurrentUICulture)
                            .Fmt(key) ;
        }




        #region ----------------------------------- Operation Context -------------------------------------

        //[Class]_[Method]_
        //[Class]_[Method]_[Point]

        /// <summary>
        /// Get Operation Context.
        /// Returns Tuple{Type,String} where :
        ///    Class -  Tuple.Item1 - Type ,
        ///    Method - Tuple.Item2  -String.          
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Operation"></param>
        /// <param name="Point"></param>
        /// <returns></returns>
        public static Tuple<Type, String> GetOperationContext<T>(String operation, String point = null)
        {
            var contextType = typeof(T);
            var typeName = contextType.Name;
            var methodName = operation;
            var key = (point == null) ?
                      typeName + "_" + methodName
                    : typeName + "_" + methodName + "_" + point;

            return Tuple.Create<Type, String>(contextType, key);
        }


        public static Tuple<Type, String> GetOperationContext(Type contextType,String contextOperation, String contextPoint = null)
        {
            //var contextType = typeof(T);
            var typeName = contextType.Name;
            var methodName = contextOperation;
            var key = (contextPoint == null) ?
                      typeName + "_" + methodName
                    : typeName + "_" + methodName + "_" + contextPoint;

            return Tuple.Create<Type, String>(contextType, key);
        }




        /// <summary>
        /// Returns Tuple{Type,String} where :
        ///    Class -  Tuple.Item1 - Type ,
        ///    Method - Tuple.Item2 - String.                   
        /// </summary>
        /// <typeparam name="TDelegate"></typeparam>
        /// <param name="Operation"></param>
        /// <param name="Point"></param>
        /// <returns></returns>
        public static Tuple<Type, String> GetOperationContext<TDelegate>(TDelegate operation, String point = null)
        {
            var operationMethod = (operation as Delegate).Method;
            var contextType = operationMethod.DeclaringType;
            var typeName = contextType.Name;
            var methodName = operationMethod.Name;
            var key = (point == null) ?
                     typeName + "_" + methodName
                   : typeName + "_" + methodName + "_" + point;

            return Tuple.Create<Type, String>(contextType, key);
        }

        #endregion ----------------------------------- Operation Context -------------------------------------
        

        #region ----------------------------------- GetMessageTemplate ------------------------------------
            


        /// <summary>
        /// Get Message Template for some Operation Context:
        ///    Method - Operation, 
        ///    Class- typeof(T).      
        /// </summary>
        /// <param name="operationContext"></param>
        /// <returns></returns>
        public static string GetMessageTemplate(Tuple<Type,String> operationContext)
        {
            if (operationContext == null)
            {
                throw ExceptionsService.CreateException<NullReferenceException>(new object[] { GetRS(RK_GetMessageTemplate_1ArgCheck) });
            }

            ResourceManager defResManager = null;           

            if (ResourcesLocator.TryGetDefResourceManager(operationContext.Item1, out defResManager))
            {//Def Resource Manager really exist
                return defResManager.GetString(operationContext.Item2, CultureInfo.CurrentCulture);
            }
            else
            {// Default Resource Manager  is null- i. e. means doesn't exist   
                throw new InvalidOperationException(GetRS(operationContext.Item2));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationContext"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public static string GetMessageTemplate(Tuple<Type, String> operationContext,CultureInfo culture)
        {
            if (operationContext == null)
            {
                throw ExceptionsService.CreateException<NullReferenceException>(new object[] { GetRS(RK_GetMessageTemplate_1ArgCheck) });
            }

            ResourceManager defResManager = null;

            if (ResourcesLocator.TryGetDefResourceManager(operationContext.Item1, out defResManager))
            {//Def Resource Manager really exist
                return defResManager.GetString(operationContext.Item2, culture);
            }
            else
            {// Default Resource Manager  is null- i. e. means doesn't exist   
                throw new InvalidOperationException(GetRS(operationContext.Item2));
            }
        }

        #endregion ----------------------------------- GetMessageTemplate ------------------------------------
        
    }

}
