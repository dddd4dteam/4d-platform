﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Caching.IG
{
    /// <summary>
    /// Represents a single contiguous block of items
    /// </summary>
    /// <typeparam name="T">The type of items</typeparam>
    internal class CachedBlock<T>
    {
        private readonly CacheManager<T> _manager;

        public int AccessCount
        {
            get;
            set;
        }

        public int Count
        {
            get
            {
                return this.Items.Count;
            }
        }

        public int FirstAddedIndex
        {
            get;
            private set;
        }

        public int FirstCachedIndex
        {
            get;
            private set;
        }

        public int FirstRemovedIndex
        {
            get;
            private set;
        }

        public bool IsEmpty
        {
            get
            {
                if (this.Items.Count == 0)
                {
                    return true;
                }
                return this.FirstCachedIndex < 0;
            }
        }

        public T this[int index]
        {
            get
            {
                CachedBlock<T> accessCount = this;
                accessCount.AccessCount = accessCount.AccessCount + 1;
                CacheManager<T> cacheManager = this._manager;
                int num = cacheManager.AccessCount + 1;
                int num1 = num;
                cacheManager.AccessCount = num;
                this.LastAccess = num1;
                if (this._manager.AccessCount == 2147483647 || this.AccessCount > 1073741823)
                {
                    this._manager.ShrinkAccessCounts();
                }
                int num2 = index - this.FirstCachedIndex;
                if (num2 < 0 || num2 >= this.Items.Count)
                {
                    return default(T);
                }
                return this.Items[num2];
            }
        }

        public List<T> Items
        {
            get;
            private set;
        }

        public int LastAccess
        {
            get;
            set;
        }

        public int LastAddedIndex
        {
            get;
            private set;
        }

        public int LastCachedIndex
        {
            get
            {
                if (this.IsEmpty)
                {
                    return -1;
                }
                return this.FirstCachedIndex + this.Count - 1;
            }
        }

        public int LastRemovedIndex
        {
            get;
            private set;
        }

        public int LoadCount
        {
            get;
            set;
        }

        public int MaxCachedItems
        {
            get;
            set;
        }

        public double Weight
        {
            get
            {
                if (this.IsEmpty)
                {
                    return -1;
                }
                double loadCount = (double)(this._manager.LoadCount - this.LoadCount);
                loadCount = (loadCount >= 3 ? 0 : 2147483647);
                double accessCount = (double)(2147483647 - (this._manager.AccessCount - this.LastAccess));
                double num = accessCount + loadCount + (double)(this.AccessCount / this._manager.LoadSize);
                return num;
            }
        }

        public CachedBlock(CacheManager<T> manager, int maxItems)
        {
            this.Items = new List<T>();
            this.MaxCachedItems = maxItems;
            this._manager = manager;
            this.Clear();
        }

        public void Clear()
        {
            this.Invalidate();
            this.Items.Clear();
        }

        public bool CopyItemsTo(IList destList, int startIndex, int size)
        {
            if (!this.IsCached(startIndex) || !this.IsCached(startIndex + size - 1))
            {
                return false;
            }
            destList.Clear();
            startIndex = startIndex - this.FirstCachedIndex;
            for (int i = 0; i < size; i++)
            {
                int num = startIndex;
                startIndex = num + 1;
                destList.Add(this.Items[num]);
            }
            return true;
        }

        public void Invalidate()
        {
            this.ResetChanges();
            this.FirstCachedIndex = -1;
            int num = 0;
            int num1 = num;
            this.AccessCount = num;
            this.LoadCount = num1;
        }

        public bool IsCached(int index)
        {
            int num = index - this.FirstCachedIndex;
            return (this.FirstCachedIndex < 0 || num < 0 ? false : num < this.Items.Count);
        }

        public bool IsOverwritten(int collectionIndex)
        {
            if (this.FirstRemovedIndex > collectionIndex)
            {
                return false;
            }
            return collectionIndex <= this.LastRemovedIndex;
        }

        /// <summary>
        /// Loads the items.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="items">The items.</param>
        /// <param name="count">The count of items.</param>
        public void LoadItems(int index, IEnumerable<T> items, int count)
        {
            this.RemoveRange(this.FirstCachedIndex, this.LastCachedIndex);
            this.Items.AddRange(items);
            int num = index;
            int num1 = num;
            this.FirstAddedIndex = num;
            this.FirstCachedIndex = num1;
            this.LastAddedIndex = index + count - 1;
            CacheManager<T> cacheManager = this._manager;
            int loadCount = cacheManager.LoadCount + 1;
            int num2 = loadCount;
            cacheManager.LoadCount = loadCount;
            this.LoadCount = num2;
        }

        public void RemoveRange(int firstIndex, int lastIndex)
        {
            if (this.IsEmpty || this.FirstCachedIndex > lastIndex || this.LastCachedIndex < firstIndex)
            {
                return;
            }
            int num = Math.Max(firstIndex, this.FirstCachedIndex);
            int num1 = Math.Min(lastIndex, this.LastCachedIndex);
            if (num == this.FirstCachedIndex || num1 == this.LastCachedIndex)
            {
                int firstCachedIndex = num - this.FirstCachedIndex;
                int num2 = num1 - num + 1;
                this.Items.RemoveRange(firstCachedIndex, num2);
                this.FirstRemovedIndex = num;
                this.LastRemovedIndex = num1;
                if (firstCachedIndex == 0)
                {
                    CachedBlock<T> cachedBlock = this;
                    cachedBlock.FirstCachedIndex = cachedBlock.FirstCachedIndex + num2;
                    return;
                }
            }
            else
            {
                this.Clear();
            }
        }

        public void ResetChanges()
        {
            this.FirstRemovedIndex = -1;
            this.LastRemovedIndex = -2;
        }
    }
}
