﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDD.Core.Caching.IG
{
    /// <summary>
    /// A class used to manage the VirtualCollection internal cache 
    /// </summary>
    /// <typeparam name="T">The type of items</typeparam>
    internal class CacheManager<T>
    {
        private const int MinCachedBlocks = 6;

        private const int MinCachedBlockSize = 10;

        private const int MinCachedItems = 100;

        private const int MaxCachedBlocks = 20;

        private readonly List<CachedBlock<T>> _theCache;

        private int _blocksCount;

        private int _loadSize;

        private int _currentIndex;

        public int AccessCount
        {
            get;
            set;
        }

        public int BlocksCount
        {
            get
            {
                return this._blocksCount;
            }
            set
            {
                if (this._blocksCount != value)
                {
                    if (value < 6)
                    {
                        throw new ArgumentOutOfRangeException("value", "The number of cached blocks is too small to work properly");
                    }
                    int num = value - this._blocksCount;
                    this._blocksCount = value;
                    if (num < 0)
                    {
                        this.RemoveBlocks(-num);
                        return;
                    }
                    this.AddBlocks(num);
                }
            }
        }

        public CachedBlock<T> Current
        {
            get
            {
                return this._theCache[this._currentIndex];
            }
        }

        public int LoadCount
        {
            get;
            set;
        }

        public int LoadSize
        {
            get
            {
                return this._loadSize;
            }
            set
            {
                if (this._loadSize != value)
                {
                    this._loadSize = Math.Max(10, value);
                    this.BlocksCount = Math.Max(6, (int)Math.Ceiling(100 / (double)this._loadSize));
                    this.ClearCache();
                }
            }
        }

        public CacheManager()
        {
            this._theCache = new List<CachedBlock<T>>();
            this.BlocksCount = 6;
        }

        private void AddBlocks(int blocksCount)
        {
            while (blocksCount > 0)
            {
                this.AddCache();
                blocksCount--;
            }
        }

        public void AddCache()
        {
            this._theCache.Add(new CachedBlock<T>(this, this.LoadSize));
            if (this._currentIndex < 0)
            {
                this._currentIndex = 0;
            }
        }

        public void ClearCache()
        {
            int num = 0;
            int num1 = num;
            this.AccessCount = num;
            this.LoadCount = num1;
            foreach (CachedBlock<T> cachedBlock in this._theCache)
            {
                cachedBlock.Clear();
            }
            this._theCache.Clear();
            this.AddBlocks(this.BlocksCount);
        }

        public bool Extend()
        {
            if (this.BlocksCount >= 20)
            {
                return false;
            }
            CacheManager<T> blocksCount = this;
            blocksCount.BlocksCount = blocksCount.BlocksCount + 1;
            return true;
        }

        public CachedBlock<T> GetCacheForItem(int index)
        {
            for (int i = 0; i < this._theCache.Count; i++)
            {
                if (this._theCache[i].IsCached(index))
                {
                    this._currentIndex = i;
                    return this._theCache[i];
                }
            }
            return null;
        }

        public T GetItem(int index)
        {
            T t;
            this.TryGetItem(index, out t);
            return t;
        }

        public int IndexOf(T item)
        {
            for (int i = 0; i < this._theCache.Count; i++)
            {
                CachedBlock<T> cachedBlock = this._theCache[i];
                if (!cachedBlock.IsEmpty)
                {
                    int firstCachedIndex = cachedBlock.Items.IndexOf(item);
                    if (firstCachedIndex > -1)
                    {
                        firstCachedIndex = firstCachedIndex + cachedBlock.FirstCachedIndex;
                        return firstCachedIndex;
                    }
                }
            }
            return -1;
        }

        public int LoadItems(int index, IEnumerable<T> items)
        {
            int num = items.Count<T>();
            int num1 = index + num - 1;
            double weight = double.MaxValue;
            this.RemoveRange(index, num1);
            for (int i = 0; i < this._theCache.Count; i++)
            {
                CachedBlock<T> item = this._theCache[i];
                if (weight > item.Weight)
                {
                    weight = item.Weight;
                    this._currentIndex = i;
                    if (item.Weight < 1)
                    {
                        break;
                    }
                }
            }
            this.Current.LoadItems(index, items, num);
            return num;
        }

        private void RemoveBlocks(int blocksCount)
        {
            while (blocksCount > 0)
            {
                this.RemoveCache();
                blocksCount--;
            }
        }

        public void RemoveCache()
        {
            double weight = double.MaxValue;
            CachedBlock<T> item = null;
            for (int i = 0; i < this._theCache.Count; i++)
            {
                item = this._theCache[i];
                if (weight > item.Weight)
                {
                    weight = item.Weight;
                    if (item.Weight < 1)
                    {
                        break;
                    }
                }
            }
            if (item != null)
            {
                this._theCache.Remove(item);
            }
            if (this._currentIndex >= this._theCache.Count)
            {
                this._currentIndex = 0;
            }
        }

        private void RemoveRange(int firstIndex, int lastIndex)
        {
            for (int i = 0; i < this._theCache.Count; i++)
            {
                this._theCache[i].RemoveRange(firstIndex, lastIndex);
            }
        }

        public void ShrinkAccessCounts()
        {
            CacheManager<T> accessCount = this;
            accessCount.AccessCount = accessCount.AccessCount / 2;
            CacheManager<T> loadCount = this;
            loadCount.LoadCount = loadCount.LoadCount / 2;
            for (int i = 0; i < this._theCache.Count; i++)
            {
                CachedBlock<T> item = this._theCache[i];
                CachedBlock<T> cachedBlock = item;
                cachedBlock.LoadCount = cachedBlock.LoadCount / 2;
                CachedBlock<T> accessCount1 = item;
                accessCount1.AccessCount = accessCount1.AccessCount / 2;
            }
        }

        public bool TryGetItem(int index, out T item)
        {
            CachedBlock<T> current = this.Current;
            if (current.IsCached(index))
            {
                if (current.AccessCount == 0)
                {
                    current.AccessCount = this.LoadCount;
                }
                item = current[index];
                return true;
            }
            current = this.GetCacheForItem(index);
            if (current == null)
            {
                item = default(T);
                return false;
            }
            CachedBlock<T> accessCount = current;
            accessCount.AccessCount = accessCount.AccessCount + this.LoadCount;
            item = current[index];
            return true;
        }


        public bool TrySetCurrent(int index, int size)
        {
            int num = index + size - 1;
            for (int i = 0; i < this._theCache.Count; i++)
            {
                if (this._theCache[i].IsCached(index) && this._theCache[i].IsCached(num))
                {
                    this._currentIndex = i;
                    return true;
                }
            }
            return false;
        }
    }
}
