﻿
using System;

 
using DDDD.Core.Reflection.IG;

namespace DDDD.Core.Caching.IG
{

    /// <summary>
    /// Object used to store the Type, and in WPF the PropertyDescriptors for a Type.
    /// </summary>
    public class CachedTypedInfo
    {
        /// <summary>
        /// The type of the object.
        /// </summary>
        public Type CachedType
        {
            get;
            set;
        }

        /// <summary>
        /// The CustomTypeProperies for the specified type.
        /// </summary>
        public CustomTypePropertyCollection CustomTypeProperties
        {
            get;
            set;
        }

        public CachedTypedInfo()
        {
        }
    }
}
