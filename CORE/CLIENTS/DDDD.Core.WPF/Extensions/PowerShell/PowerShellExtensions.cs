﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDDD.Core.Notification;
using DDDD.Core.Environment;

namespace DDDD.Core.Extensions
{
    public static class PowerShellExtensions
    {

        internal const string NL = "\r\n";

        /// <summary>
        /// Get Parameter Value when  we parse ParameterValue line,where line has format like [{Parameter} : {Value}]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="psParamValueLine"></param>
        /// <param name="ParamKey"></param>
        /// <param name="useCase"></param>
        /// <returns></returns>
        public static T GetParameterValue<T>(this string psParamValueLine, string ParamKey, bool useCase = false)
        {
            if (psParamValueLine.IsNullOrEmpty()) return default(T);
            var parameterLines = psParamValueLine.SplitNoEntries(new[] { NL });
            var paramsKeyValues = new Dictionary<string, string>();
            foreach (var prmVal in parameterLines)
            {
                var kV = prmVal.SplitNoEntries(new[] { " : " });
                if (kV.Count == 2)
                {
                    paramsKeyValues.Add(kV[0].Trim(), kV[1].Trim());
                }
            }

            if (useCase == false) // default - use Lower string value Comparison
            {
                var keyOnLowerComparison = paramsKeyValues.Keys.Where(key => key.ToLower() == ParamKey.ToLower()).FirstOrDefault();

                if (keyOnLowerComparison != null)
                {
                    string targetValue = paramsKeyValues[keyOnLowerComparison];
                    return (T)Convert.ChangeType(targetValue, typeof(T), null);
                }
            }
            else if (useCase) // precision string  with Cases - custom option
            {
                if (paramsKeyValues.ContainsKey(ParamKey))
                {
                    var targetValue = paramsKeyValues[ParamKey];
                    return (T)Convert.ChangeType(targetValue, typeof(T), null);
                }
            }


            return default(T);


        }

         

#if CLIENT
        //// INSTALLER  EXTENSION

        /// <summary>
        /// Send OperationProgress Message to UI
        /// </summary>
        /// <param name="sayProgressToUI"></param>
        /// <param name="taskTitle"></param>
        /// <param name="progressMessage"></param>
        /// <param name="exceptionMessage"></param>
        public static void SendUIMessage(this Action<OperationProgressInfo> sayProgressToUI, OperationProgressActionsEn operationProgressAcion,  string taskTitle, string progressMessage, string exceptionMessage = null)
        {
            if (sayProgressToUI == null) return;

            if (taskTitle.IsNullOrEmpty() && progressMessage.IsNullOrEmpty()) return;

            if (taskTitle.IsNullOrEmpty() && exceptionMessage.IsNullOrEmpty()) return;


            Environment2.DISP.BeginInvoke(sayProgressToUI,
               new object[] {   OperationProgressInfo.Create(operationProgressAcion, Guid.Empty , taskTitle , progressMessage, exceptionMessage)//  command.ID
                                     
               });

        }
#endif 


        public static string TryGetTargetFileName2(this Uri uriItem)
        {
            if (uriItem == null) return null;


            var absoluteUri = uriItem.GetComponents(UriComponents.AbsoluteUri, UriFormat.Unescaped);

            if (string.IsNullOrEmpty(uriItem.Query) && absoluteUri != null)
            {
                return absoluteUri.Split(new[] { @"/" }, StringSplitOptions.RemoveEmptyEntries).Last();
            }

            return null;
        }



    }


}
