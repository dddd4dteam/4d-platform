﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace DDDD.Core.Extensions
{
    /// <summary>
    /// Regular expressions extensions
    /// </summary>
    public static class RegExpressionsExtensions
    {
       

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="trimSymbols"></param>
        /// <param name="toReplacement"></param>
        /// <returns></returns>
        public static string TrimSymbols(this string inputData, string[] trimSymbols, string toReplacement)
        {
            //string inputData = @"{EnumName}{EnumItemName}[flag1].[flag2].[flag3].[flag4]";

            string pattern = CreateTrimPattern(trimSymbols);

            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
                                        ;

            Regex regex = new Regex(pattern, regexOptions);
            string result = regex.Replace(inputData, toReplacement);

            return result;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="trimSymbols"></param>
        /// <returns></returns>
        public static string CreateTrimPattern(string[] trimSymbols)
        {
            string pattern = "";
            int i = 0;
            foreach (var item in trimSymbols)
            {
                if (i == 0)
                {
                    pattern = @"\" + @item;
                    i++;
                    continue;
                }
                pattern += @"|\" + @item;
                i++;
            }

            return pattern;
        }


      



        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="KnownReplacements"></param>
        /// <returns></returns>
        public static string GetReplacedString(this string inputData, Dictionary<string, string> KnownReplacements)
        {
            // Collecting flags            
            string outputResult = inputData;

            foreach ( var item in KnownReplacements )
            {
                RegexOptions regexOptions = RegexOptions.None;
                Regex regexReplace = new Regex(item.Key, regexOptions);
                string replacement = item.Value;
                outputResult = regexReplace.Replace(outputResult, replacement);
            }

            return outputResult;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static string[] SplitString(this string inputData, string RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.None;
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            string[] result = regex.Split(inputData);
            return result;
        }




      

        /// <summary>
        /// Replace one string Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="KnownFlagsReplacements"></param>
        /// <returns></returns>
        public static string ReplaceManyRe(this string inputData, Dictionary<string, string> KnownReplacements)
        {
            // Collecting flags            
            string outputResult = inputData;

            foreach (var item in KnownReplacements)
            {
                outputResult = outputResult.ReplaceRe(item.Key, item.Value);
            }

            return outputResult;
        }


        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <param name="ReplacementString"></param>
        /// <returns></returns>
        public static string ReplaceRe(this string inputData, string RegexPattern = @"\/", String ReplacementString = "")
        {
            // Collecting flags            

            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                      | RegexOptions.IgnorePatternWhitespace
#if NET45
                                      | RegexOptions.Compiled
#endif
;

            Regex regexReplace = new Regex(RegexPattern, regexOptions);
            string outputResult = regexReplace.Replace(inputData, ReplacementString);


            return outputResult;
        }





        /// <summary>
        /// Split one string Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static string[] SplitRe(this string inputData, string RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase
                                        | RegexOptions.CultureInvariant
#if NET45
                                        | RegexOptions.Compiled
#endif
;
            Regex regex = new Regex(pattern, regexOptions);

            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";

            string[] result = regex.Split(inputData);

            List<String> outNotEmptyResult = new List<String>();
            foreach (var item in result)
            {
                if (item != "" && item != " " && item != "\t")
                {
                    outNotEmptyResult.Add(item);
                }
            }
            return outNotEmptyResult.ToArray();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static String[] SplitRe(this string[] inputData, String RegexPattern = @"\/")
        {
            StringBuilder strBuilder = new StringBuilder();
            foreach (var item in inputData)
            {
                strBuilder.AppendLine(item);
            }

            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            string[] result = regex.Split(strBuilder.S());



            List<String> outNotEmptyResult = new List<string>();
            foreach (var item in result)
            {
                if (item != "" && item != " " && item != "\t")
                {
                    outNotEmptyResult.Add(item);
                }
            }
            return outNotEmptyResult.ToArray();

            //return result;
        }




        /// <summary>
        /// Get First Match for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Match FirstMatch(this String inputData, String RegexPattern = @"\/", Int32 startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            Match result = regex.Match(inputData, startat);
            return result;
        }


        /// <summary>
        /// Is current input text Contains one or more matches. Using Regular Expressions  
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static bool IsMatch(this string inputData, string RegexPattern = @"\/", int startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            bool result = regex.IsMatch(inputData, startat);
            return result;
        }


        /// <summary>
        /// Get All Matches for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static MatchCollection AllMatches(this string inputData, string RegexPattern = @"\/", int startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            MatchCollection result = regex.Matches(inputData, startat);
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Dictionary<int, Match> PointedMatches(this string[] inputData, string RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            Dictionary<Int32, Match> result = new Dictionary<int, Match>();

            for (int i = 0; i < inputData.Length; i++)
            {
                Match matchItem = inputData[i].FirstMatch(RegexPattern);
                if (matchItem.Success)
                {
                    result.Add(i, matchItem);
                }
            }

            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Dictionary<int, int> MatchLines(this string[] inputData, string RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;


            Regex regex = new Regex(pattern, regexOptions);

            Dictionary<Int32, Int32> result = new Dictionary<Int32, Int32>();
            Int32 j = 0;
            for (int i = 0; i < inputData.Length; i++)
            {
                if (inputData[i].IsMatch(RegexPattern))
                {
                    result.Add(j, i);
                    j++;
                }
            }

            return result;
        }



        /// <summary>
        /// Get All Matches count for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Int32 MatchesCount(this String inputData, String RegexPattern = @"\/", Int32 StartAt = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
#if NET45
                                        | RegexOptions.Compiled
#endif
;

            Regex regex = new Regex(pattern, regexOptions);


            MatchCollection result = regex.Matches(inputData, StartAt);
            return result.Count;
        }
        

    }
}
