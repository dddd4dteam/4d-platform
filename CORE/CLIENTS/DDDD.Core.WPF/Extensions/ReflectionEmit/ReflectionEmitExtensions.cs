﻿using DDDD.Core.Diagnostics;
using DDDD.Core.Threading;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;


namespace DDDD.Core.Extensions
{

    /// <summary>
    /// System.Reflection.Emit extensions
    /// </summary>
    public static class ReflectionEmitExtensions
    {
        //static ReflectionEmitExtensions()
        //{           
        //}


        static LazyAct<Dictionary<Type, OpCode>> LA_TypeOpCodeMap 
            = LazyAct<Dictionary<Type, OpCode>>.Create(
                (args)=>
                {
                   var TypeOpCodeMap = new Dictionary<Type, OpCode>();

                    TypeOpCodeMap.Add(typeof(bool), OpCodes.Ldind_I1);
                    TypeOpCodeMap.Add(typeof(char), OpCodes.Ldind_U2);
                    TypeOpCodeMap.Add(typeof(float), OpCodes.Ldind_R4);
                    TypeOpCodeMap.Add(typeof(double), OpCodes.Ldind_R8);
                    TypeOpCodeMap.Add(typeof(string), OpCodes.Ldstr);
                    TypeOpCodeMap.Add(typeof(sbyte), OpCodes.Ldind_I1);
                    TypeOpCodeMap.Add(typeof(int), OpCodes.Ldind_I4);
                    TypeOpCodeMap.Add(typeof(long), OpCodes.Ldind_I8);
                    TypeOpCodeMap.Add(typeof(byte), OpCodes.Ldind_U1);
                    TypeOpCodeMap.Add(typeof(short), OpCodes.Ldind_I2);
                    TypeOpCodeMap.Add(typeof(ushort), OpCodes.Ldind_U2);
                    TypeOpCodeMap.Add(typeof(uint), OpCodes.Ldind_U4);
                    TypeOpCodeMap.Add(typeof(ulong), OpCodes.Ldind_I8);

                    return TypeOpCodeMap;
                }
                , null // lock instance
                , null // args
                );




        /// <summary>
        /// ILGen generate next logic :  if (ReflectedType.IsClass) - OpCodes.Castclass;  if (ReflectedType.IsValueType) - Unbox_Any,
        /// </summary>
        /// <param name="ILGen"></param>
        /// <param name="ReflectedType"></param>
        public static void AddToIL_CastObjectToReflectedType(this  ILGenerator ILGen, Type ReflectedType)
        {
            Validator.AssertTrue<ArgumentNullException>((ILGen == null), "CastObjectToReflectedType(): ILGen cannot be null ");
            Validator.AssertTrue<ArgumentNullException>((ReflectedType == null), "CastObjectToReflectedType(): ReflectedType cannot be null ");


            //object instance Unbox any struct / cast to Class
            if (ReflectedType.IsClass)
            {
                ILGen.Emit(OpCodes.Castclass, ReflectedType); //Cast class or struct                
            }
            else if (ReflectedType.IsValueType)
            {
                ILGen.Emit(OpCodes.Unbox_Any, ReflectedType); //Cast class or struct                
            }

        }




        /// <summary>
        /// ILGen generate next logic :  if (secondargType.IsValueType) -1 Unbox it and 2 try to load by OpCodeTypesMap or as OpCodes.Ldobj
        /// </summary>
        /// <param name="ILGen"></param>
        /// <param name="secondargType"></param>
        public static void AddToIL_LoadSecondArgFromObject(this ILGenerator ILGen, Type secondargType)
        {
            Validator.AssertTrue<ArgumentNullException>( ILGen.IsNull(), "LoadSecondArgFromObject): ILGen cannot be null ");
            Validator.AssertTrue<ArgumentNullException>( secondargType.IsNull(), "LoadSecondArgFromObject(): ReflectedType cannot be null ");
            
            ILGen.Emit(OpCodes.Ldarg_1);		 //Load the second argument  (value object)   
            if (secondargType.IsValueType)
            {
                ILGen.Emit(OpCodes.Unbox_Any, secondargType);			        //Unbox it 	
                if (LA_TypeOpCodeMap.Value.ContainsKey(secondargType))				//and load
                {
                    OpCode load = (OpCode)LA_TypeOpCodeMap.Value[secondargType];
                    ILGen.Emit(load);
                }
                else
                {
                    ILGen.Emit(OpCodes.Ldobj, secondargType);
                }
            }
        }




        #region --------------------------------- DynamicMethod for MemberInfo  METHODS  --------------------------------


        public static DynamicMethod DynamicMethod_Setter(this MemberInfo member)
        {
            string MethodName = member.BuildMemberMethodName("_set");


            return new DynamicMethod(
                    MethodName
                   , typeof(void)
                   , new[] { typeof(object), typeof(object) }
#if NET45
, member.ReflectedType
                   , true
#endif
);

        }


        public static DynamicMethod DynamicMethod_Getter(this MemberInfo member, bool UseReturnAsMemberType = false)
        {
            string MethodName = member.BuildMemberMethodName("_get");


            return new DynamicMethod(
                    MethodName
                   , UseReturnAsMemberType ? member.GetMemberType() : typeof(object)// ParamType
                   , new[] { typeof(object) }
#if NET45
, member.ReflectedType
                   , true
#endif
);

        }


        #endregion --------------------------------- DynamicMethod for MemberInfo  METHODS  --------------------------------




        #region ---------------------------------- CREATE CTOR,INSTANCE METHOD  DELEGATES  ---------------------------------------



        /// <summary>
        /// Create  Ctor with parameters delegate 
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="ctor"></param>
        /// <returns></returns>
        public static Func<object[], TOut> CreateCtorWithParamsDelegate<TOut>(this ConstructorInfo ctor)
        {
            List<Expression> InnerValues = new List<Expression>();
            Type[] ctorParamTypes = ctor.GetParameters().ToTypeArray();

            var outerCtorFuncType = Expression.GetFuncType(typeof(object[]), typeof(TOut));
            //outer object array param
            var ParamObjectArgs = Expression.Parameter(typeof(object[]));
            int index = -1;
            foreach (var argType in ctorParamTypes)
            { // ? check if arg is null - not get Type
                InnerValues.Add(Expression.Convert(
                                                      Expression.ArrayIndex(ParamObjectArgs, Expression.Constant(++index))
                                                    , argType)
                               );
            }

            var commonExpression = (InnerValues.Count == 0) ? Expression.New(ctor)
                                                           : Expression.New(ctor, InnerValues);

            var delegateOuter = Expression.Lambda<Func<object[], TOut>>(commonExpression, ParamObjectArgs);//outerCtorFuncType,
            return delegateOuter.Compile();

            //return expr.Compile();
        }


        /// <summary>
        /// Creating Delegate with [new TOut()] expression from some constructor instance of TOut.
        /// <para/> In DEBUG mode Constructor should belong to TOut - i.e  [ctor.DeclaringType == typeof(TOut)], else  exception will be thrown. 
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="ctor"></param>
        /// <returns></returns>
        public static Func<TOut> CreateCtorDelegate<TOut>(this ConstructorInfo ctor)
        {
            Validator.AssertTrueDbg<InvalidOperationException>(ctor.DeclaringType != typeof(TOut)
                      , "ERROR in {0}.{1}<TOut>(): TOut doesn't belong to the ctor.DeclaringType ".Fmt(nameof(ReflectionEmitExtensions), nameof(CreateCtorDelegate))  );


            //var delegateResult = Delegate.CreateDelegate(typeof(Func<T>), declaringType.GetMethod(".ctor", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic));                
            

            Expression<Func<TOut>> expr =
                Expression.Lambda<Func<TOut>>(
                        Expression.New(ctor),
                        null
                        );

            return expr.Compile();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="ctor"></param>
        /// <returns></returns>
        public static Func<TParam1, TOut> CreateCtorDelegate<TParam1, TOut>(this ConstructorInfo ctor)
        {

            Validator.AssertTrueDbg<InvalidOperationException>(ctor.DeclaringType != typeof(TOut)
                      , "ERROR in {0}.{1}<TParam1,TOut>(): TOut doesn't belong to the ctor.DeclaringType ".Fmt(nameof(ReflectionEmitExtensions), nameof(CreateCtorDelegate)));
            
            ParameterExpression param1 = Expression.Parameter(typeof(TParam1), "param1");

            Expression<Func<TParam1, TOut>> expr =
                Expression.Lambda<Func<TParam1, TOut>>(
                        Expression.New(ctor),
                        param1
                        );

            return expr.Compile();

        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TParam1"></typeparam>
        /// <typeparam name="TParam2"></typeparam>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="ctor"></param>
        /// <returns></returns>
        public static Func<TParam1, TParam2, TOut> CreateCtorDelegate<TParam1, TParam2, TOut>(this ConstructorInfo ctor)
        {
            ParameterExpression param1 = Expression.Parameter(typeof(TParam1), "param1");
            ParameterExpression param2 = Expression.Parameter(typeof(TParam2), "param2");

            Expression<Func<TParam1, TParam2, TOut>> expr =
                Expression.Lambda<Func<TParam1, TParam2, TOut>>(
                        Expression.New(ctor),
                        param1, param2
                        );

            return expr.Compile();
        }



        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="instance"></param>
        /// <param name="MethodName"></param>
        /// <param name="binding"></param>
        /// <returns></returns>
        public static Func<TOut> CreateInstanceMethodDelegate<TOut>(this object instance, string MethodName, BindingFlags binding = BindingFlags.Default)
        {
            try
            {
                //Func<ICommunicationObject, Delegate>
                if (instance == null) throw new ArgumentNullException("instance");


                var tp = instance.GetType();// channelContract;

                MethodInfo methodEnd = tp.GetMethod(MethodName, binding);
                if (methodEnd == null) throw new ArgumentNullException(MethodName);

                // Delegate.CreateDelegate(delegateType, mi)
                var delegateout = (Func<TOut>)Delegate.CreateDelegate(typeof(Func<TOut>), methodEnd);

                return delegateout;
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TOut"></typeparam>
        /// <param name="staticProperty"></param>
        /// <returns></returns>
        public static Func<TOut> CreateStaticPropertyGetDelegate<TOut>(this PropertyInfo staticProperty)
        {
            try
            {
                //Func<ICommunicationObject, Delegate>
                if ( staticProperty.IsNull() || !staticProperty.IsStatic()) throw new ArgumentNullException("staticProperty can't be null and must be static Property");

                MethodInfo methodGetter = staticProperty.GetGetMethod();                
                
                var delegateout = (Func<TOut>)Delegate.CreateDelegate(typeof(Func<TOut>), methodGetter);

                return delegateout;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private static Expression<Func<object>> GenerateStaticNullablePropertyGetterBoxedExpression(PropertyInfo staticPropertyInfo)
        {
            var targetStaticProeprtyAccessExpr = Expression.Property(null, staticPropertyInfo);

            return Expression.Lambda<Func<object>>(targetStaticProeprtyAccessExpr
                                                    , "GetPrty__" + staticPropertyInfo.Name,
                                                    null// no parameters 
                                                   );
        }

        /// <summary>
        /// Here we generating  Getter delegate of some [staticProperty] of [targetType].
        /// It can be used to call static property like Singleton instance - [Current] property, and other static properties also too.
        /// </summary>
        /// <param name="targetSingletonProperty"></param>
        /// <returns></returns>
        //public static Func<object> GenerateSingletonInstanceCall(this Type targetType, string staticProperty )
        //{
        //    var staticPropertyInfo = targetType.GetProperty(staticProperty, BindingFlags.Static | BindingFlags.Public);
        //    var getDelegate = staticProperty.
        //    return GenerateStaticNullablePropertyGetterBoxedExpression(staticPropertyInfo).Compile();
        //}




        #endregion ---------------------------------- CREATE CTOR,INSTANCE METHOD  DELEGATES  ---------------------------------------







    }
}
