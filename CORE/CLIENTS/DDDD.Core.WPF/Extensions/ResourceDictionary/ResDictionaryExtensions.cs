﻿using DDDD.Core.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DDDD.Core.Extensions
{

    public static class ResDictionaryExtensions
    {

        #region -------------------------------- ResourceDictionary EXTENSIONS --------------------------------------

        //  Merged  Resource Dictionaries- ResourceDictionary Extensions


        // GetMergedResDictIndex - String  из словаря ресурсов -(независимо какого) .. получить индекс сущесвующего словаря в слитых ресурсах/расширение

        // RemoveResourceDictionary - ResourceDictionary
        // RemoveResourceDictionary - string
        // RemoveResourceDictionary - Uri


        // AddResourceDictionary- ResourceDictionary new ResourceDictionary       
        // AddResourceDictionary- string
        // AddResourceDictionary- Uri 


        // Replace
        // ReplaceResourceDictionary- String
        // ReplaceResourceDictionary- Uri
        // ReplaceResourceDictionary- ReosurceDictionary 




        // AddResourceDictionaryGroup- List<ResourceDictionary> new ResourceDictionar-ies
        // AddResourceDictionaryGroup- List<String> - uhe
        // AddResourceDictionaryGroup- List<Uri>



        // Adding ResourceDictionaries of one theme:   one  ResourceDictionary  for each Control   from assembly
        // AddTheme(String ThemeName, String AssemblySourceName,List<String>) 
        // AddTheme(String ThemeName, String AssemblySourceName,List<Type>) 


        // AddToolkitTheme(String ThemeName) 





        #endregion -------------------------------- ResourceDictionary EXTENSIONS --------------------------------------


        /// <summary>
        /// Возвращает индекс словаря MergedDictionaries в текущем словаре. 
        /// По сути значение  значение >= 0 говорит нам о том что словарь уже загружен
        /// </summary>
        /// <param name="ResDictUri"></param>
        /// <returns>больше (-1) если он существует и (-1) если его нет </returns>
        public static Int32 GetResourceDictionaryIndex(this ResourceDictionary dictionary, String ResDictUri)
        {
            Contract.Requires(dictionary != null, "GetResDictIndex: dictionary can not be null ");
            Contract.Requires((String.IsNullOrEmpty(ResDictUri) == false), "GetResDictIndex: ResDictUri can not be null or empty ");

            Int32 ResDictIndex = 0;
            foreach (var resDict in dictionary.MergedDictionaries)
            {
                //if (resDict.Source != null && resDict.Source.AbsoluteUri == ResDictUri)
                String originalRDString = null;
                try
                {
                    //не у всех словарей в принципе может содержаться Source.OriginalString - может здесь находиться лиш иксепшен
                    //но останавливать здесь обработку нет смлсла поэтому здесь try catch
                    originalRDString = resDict.Source.OriginalString;
                }
                catch (System.Exception)
                {
                    continue;
                }

                if (originalRDString == ResDictUri)
                {
                    return ResDictIndex;// т.е. если найден словарьс таким ключом
                }
                else
                {
                    ResDictIndex++; // т.е. если текущий в цикле словарь с таким ключом не был искомым продолжить сравнения
                }
            }

            return -1; // т.е. если в итоге среди веех словарей не найден такой ключ
        }



        /// <summary>
        /// Удалить словарь ресурсов по его Uri из родительского словаря - из коллекции MergedResources Родительского словаря
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictUri"></param>
        /// <returns></returns>
        public static bool RemoveResourceDictionaryByUri(this ResourceDictionary dictionary, String ResDictUri)
        {
            Contract.Requires(dictionary != null, "RemoveResourceDictionaryByUri: dictionary can not be null ");
            Contract.Requires(String.IsNullOrEmpty(ResDictUri), "RemoveResourceDictionaryByUri: ResDictUri can not be null or empty ");

            Int32 cResDictIndex = dictionary.GetResourceDictionaryIndex(ResDictUri);
            if (cResDictIndex != -1)
            {
                dictionary.RemoveResourceDictionaryByIndex(cResDictIndex);
                return true;
            }

            return false;
        }



        /// <summary>
        /// Удалить словарь ресурсов по его Uri из родительского словаря - из коллекции MergedResources Родительского словаря
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictIndex"></param>
        public static void RemoveResourceDictionaryByIndex(this ResourceDictionary dictionary, Int32 ResDictIndex)
        {
            Contract.Requires(dictionary != null, "RemoveResourceDictionaryByIndex: dictionary can not be null ");
            Contract.Requires(ResDictIndex > -1, "RemoveResourceDictionaryByIndex: ResDictIndex can not be null or empty ");


            // Удалить если только индекс не больше имеющего последнего элемента в коллекции MergedDictionaries
            if (dictionary.MergedDictionaries.Count - 1 >= ResDictIndex)
            {
                dictionary.MergedDictionaries.RemoveAt(ResDictIndex);
            }

        }



        /// <summary>
        /// Удалить словарь ресурсов по его Uri из родительского словаря - из коллекции MergedResources Родительского словаря
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDict"></param>
        /// <returns></returns>
        public static bool RemoveResourceDictionary(this ResourceDictionary dictionary, ResourceDictionary ResDict)
        {
            Contract.Requires(dictionary != null, "RemoveResourceDictionary: dictionary can not be null ");
            Contract.Requires(ResDict != null, "RemoveResourceDictionary: ResourceDictionary can not be null ");
            Contract.Requires(ResDict.Source.OriginalString != null, "RemoveResourceDictionary: ResourceDictionary . Source . OriginalString can not be null or empty ");


            Int32 cResDictIndex = dictionary.GetResourceDictionaryIndex(ResDict.Source.OriginalString);
            if (cResDictIndex != -1)
            {
                dictionary.RemoveResourceDictionaryByIndex(cResDictIndex);
                return true;
            }

            return false;
        }



        /// <summary>
        /// Добавить(попробовать добавить) словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictUri"></param>
        public static void AddResourceDictionary(this ResourceDictionary dictionary, string ResDictUri)
        {
            Contract.Requires(dictionary != null, "AddMergedResDict: dictionary can not be null ");
            Contract.Requires((String.IsNullOrEmpty(ResDictUri) == false), "AddMergedResDict: ResDictUri can not be null or empty ");

            Uri resDictionaryUri = new Uri(ResDictUri, UriKind.RelativeOrAbsolute);
            dictionary.AddResourceDictionary(resDictionaryUri);

        }


        /// <summary>
        /// Добавить(попробовать добавить) словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictUri"></param>
        public static void AddResourceDictionary(this ResourceDictionary dictionary, Uri ResDictUri)
        {
            Contract.Requires(dictionary != null, "AddMergedResDict: dictionary can not be null ");
            Contract.Requires(ResDictUri != null, "AddMergedResDict: ResDictUri can not be null or empty ");

            // если  уже есть- т.е когда какой-то отличный от -1 индекс то ничего не добавляем -просто выходим
            if (dictionary.GetResourceDictionaryIndex(ResDictUri.OriginalString) != -1)
            {
                return;
            }

            // Попробовать Подгрузить словарь ресурсов по Uri и добавить в коллекцию                  
            ResourceDictionary AddingResourceDictionary = ResourceLoader.LoadResourceDictionary(ResDictUri);
            Contract.Assert(AddingResourceDictionary != null, "AddMergedResDict: ResourceDictionary cannot be null");


            // Добавление новогор словаря ресурсов выставить Uri словарю для его собственной ориентации на местности
            AddingResourceDictionary.Source = ResDictUri;
            dictionary.MergedDictionaries.Add(AddingResourceDictionary);
        }



        /// <summary>
        /// Добавить(попробовать добавить) словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDict"></param>
        public static void AddResourceDictionary(this ResourceDictionary dictionary, ResourceDictionary ResDict)
        {
            Contract.Requires(dictionary != null, "AddResourceDictionary: dictionary can not be null ");
            Contract.Requires(ResDict != null, "AddResourceDictionary: ResDict can not be null or empty ");
            Contract.Requires(string.IsNullOrEmpty(ResDict.Source.OriginalString), "AddResourceDictionary: ResDict  . Source. OroginalString can not be null or empty ");

            // если  уже есть- т.е когда какой-то отличный от -1 индекс то ничего не добавляем -просто выходим
            if (dictionary.GetResourceDictionaryIndex(ResDict.Source.OriginalString) != -1)
            {
                return;
            }


            // Добавление новогор словаря ресурсов выставить Uri словарю для его собственной ориентации на местности            
            dictionary.MergedDictionaries.Add(ResDict);
        }



        /// <summary>
        /// Заменить/просто добавить словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictUri"></param>
        public static void ReplaceResourceDictionary(this ResourceDictionary dictionary, string ResDictUri)
        {
            Contract.Requires(dictionary != null, "ReplaceMergedResDict: dictionary can not be null ");
            Contract.Requires((string.IsNullOrEmpty(ResDictUri) == false), "ReplaceMergedResDict: ResDictUri can not be null or empty ");

            Uri resDictionary = new Uri(ResDictUri, UriKind.RelativeOrAbsolute);

            dictionary.ReplaceResourceDictionary(resDictionary);
        }

        /// <summary>
        /// Заменить/просто добавить словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDictUri"></param>
        public static void ReplaceResourceDictionary(this ResourceDictionary dictionary, Uri ResDictUri)
        {
            Contract.Requires(dictionary != null, "ReplaceResourceDictionary: dictionary can not be null ");
            Contract.Requires(ResDictUri != null, "ReplaceResourceDictionary: ResDictUri can not be null or empty ");
            //ArgumentValidator.AssertNotNull(, );


            // Попробовать Подгрузить словарь ресурсов по Uri и добавить в коллекцию                  
            ResourceDictionary AddingResourceDictionary = ResourceLoader.LoadResourceDictionary(ResDictUri);
            if (AddingResourceDictionary == null) return;

            //если  уже есть под этим Uri-ем-  то будем удалять + уже должен быть загружен словарь ресурсов 
            Int32 ResDictIndex = dictionary.GetResourceDictionaryIndex(ResDictUri.OriginalString);
            if (ResDictIndex != -1)
            {  // здесь мы удаляем этот словарь
                dictionary.RemoveResourceDictionaryByIndex(ResDictIndex);
            }


            //Добавление нового словаря ресурсов выставить Uri словарю для его собственной ориентации на местности
            //AddingResourceDictionary.Source = ResDictUri;
            dictionary.MergedDictionaries.Add(AddingResourceDictionary);
        }


        /// <summary>
        /// Заменить/просто добавить словарь в список MergedDictionaries
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ResDict"></param>
        public static void ReplaceResourceDictionary(this ResourceDictionary dictionary, ResourceDictionary ResDict)
        {
            Contract.Requires(dictionary != null, "ReplaceResourceDictionary: dictionary can not be null ");
            Contract.Requires(ResDict != null, "ReplaceResourceDictionary: ResDictUri can not be null or empty ");
            Contract.Requires((string.IsNullOrEmpty(ResDict.Source.OriginalString) == false), "ReplaceResourceDictionary: ResDict. Source. OriginalString can not be null or empty ");


            // Попробовать Подгрузить словарь ресурсов по Uri и добавить в коллекцию                  
            //ResourceDictionary AddingResourceDictionary = LoadResourceDictionary(ResDictUri);
            //ArgumentValidator.AssertNotNull(AddingResourceDictionary, "ReplaceMergedResDict: ResourceDictionary cannot be null");

            //если  уже есть под этим Uri-ем-  то будем удалять + уже должен быть загружен словарь ресурсов 
            Int32 ResDictIndex = dictionary.GetResourceDictionaryIndex(ResDict.Source.OriginalString);
            if (ResDictIndex != -1)
            {  // здесь мы удаляем
                dictionary.RemoveResourceDictionaryByIndex(ResDictIndex);
            }

            //Добавление нового словаря ресурсов выставить Uri словарю для его собственной ориентации на местности            
            dictionary.MergedDictionaries.Add(ResDict);
        }



        public static void AddResourceDictionaryGroup(this ResourceDictionary dictionary, List<ResourceDictionary> ResourceDictionaries)
        {
            Contract.Requires(dictionary != null, "AddResourceDictionaryGroup: dictionary can not be null ");
            Contract.Requires(ResourceDictionaries != null, "AddResourceDictionaryGroup: ResourceDictionaries can not be null or empty ");
            Contract.Requires(ResourceDictionaries.Count > 0, "AddResourceDictionaryGroup: ResourceDictionaries Count can not be 0  to merge with other resources ");

            foreach (var resDict in ResourceDictionaries)
            {
                dictionary.ReplaceResourceDictionary(resDict);
            }

        }

        public static void AddResourceDictionaryGroup(this ResourceDictionary dictionary, List<String> ResourceDictionariesUri)
        {
            Contract.Requires(dictionary != null, "AddResourceDictionaryGroup: dictionary can not be null ");
            Contract.Requires(ResourceDictionariesUri != null, "AddResourceDictionaryGroup: ResourceDictionaries can not be null or empty ");
            Contract.Requires(ResourceDictionariesUri.Count > 0, "AddResourceDictionaryGroup: ResourceDictionaries Count can not be 0  to merge with other resources ");


            foreach (var resDictUri in ResourceDictionariesUri)
            {
                dictionary.ReplaceResourceDictionary(resDictUri);
            }

        }

        public static void AddResourceDictionaryGroup(this ResourceDictionary dictionary, List<Uri> ResourceDictionariesUri)
        {
            Contract.Requires(dictionary != null, "AddResourceDictionaryGroup: dictionary can not be null ");
            Contract.Requires(ResourceDictionariesUri != null, "AddResourceDictionaryGroup: ResourceDictionaries can not be null or empty ");
            Contract.Requires(ResourceDictionariesUri.Count > 0, "AddResourceDictionaryGroup: ResourceDictionaries Count can not be 0  to merge with other resources ");

            //ArgumentValidator.AssertNotNull(ResourceDictionariesUri, );
            //ArgumentValidator.AssertGreaterThan(ResourceDictionariesUri.Count, 0, );

            foreach (var resDictUri in ResourceDictionariesUri)
            {
                dictionary.ReplaceResourceDictionary(resDictUri);
            }

        }

    }
}
