﻿//if NOT_RELEASED


using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Serialization.Domain;


namespace DDDD.Core.Extensions
{
    public static class DomainModelExtensions
    {

#region ---------------------------------- DomainDataModel ---------------------------------------

        public static bool ContainsKey(this ObservableCollection<DModel> DModels, String DMName)
        {
            if (DModels == null || String.IsNullOrEmpty(DMName)) return false;

            return (DModels.Where(gr => gr.Identification.GetIdentity() == DMName).FirstOrDefault() != null);
        }


        public static DModel ByName(this ObservableCollection<DModel> FModels, String DMName)
        {
            if (FModels == null || String.IsNullOrEmpty(DMName)) return null;

            return FModels.Where(gr => gr.Identification.GetIdentity() == DMName).FirstOrDefault();
        }


        public static Int32 IndexOf(this ObservableCollection<DModel> DModels, String DMName)
        {
            if (DModels == null || String.IsNullOrEmpty(DMName)) return -1;

            Int32 i = -1;
            foreach (var dmdl in DModels)
            {
                i++;
                if (dmdl.Identification.GetIdentity() == DMName)
                {
                    return i;
                }
            }

            return i;
        }


        public static bool TryRemove(this ObservableCollection<DModel> DModels, String DMName)
        {
            if (DModels == null || String.IsNullOrEmpty(DMName)) return false;

            Int32 index = DModels.IndexOf(DMName);

            if (index != -1)
            {
                DModels.RemoveAt(index);
                return true;
            }
            return false;
        }







#endregion ---------------------------------- DomainDataModel ---------------------------------------


#region ----------------------------------DomainDataModelGroup--------------------------------------------


        public static bool ContainsKey(this ObservableCollection<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return false;

            return (Groups.Where(gr => gr.GroupName == GroupName).FirstOrDefault() != null);
        }


        public static DContractGroup ByName(this ObservableCollection<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return null;

            return Groups.Where(gr => gr.GroupName == GroupName).FirstOrDefault();
        }



        public static Int32 IndexOf(this ObservableCollection<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return -1;

            Int32 i = -1;
            foreach (var grp in Groups)
            {
                i++;
                if (grp.GroupName == GroupName)
                {
                    return i;
                }
            }

            return i;
        }


        public static void Remove(this ObservableCollection<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return;

            Int32 index = Groups.IndexOf(GroupName);

            if (index != -1)
            {
                Groups.RemoveAt(index);
            }
        }

        // 

        public static bool ContainsKey(this List<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return false;

            return (Groups.Where(gr => gr.GroupName == GroupName).FirstOrDefault() != null);
        }


        public static DContractGroup ByName(this List<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return null;

            return Groups.Where(gr => gr.GroupName == GroupName).FirstOrDefault();
        }



        public static Int32 IndexOf(this List<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return -1;

            Int32 i = -1;
            foreach (var grp in Groups)
            {
                i++;
                if (grp.GroupName == GroupName)
                {
                    return i;
                }
            }

            return i;
        }


        public static void Remove(this List<DContractGroup> Groups, String GroupName)
        {
            if (Groups == null || String.IsNullOrEmpty(GroupName)) return;

            Int32 index = Groups.IndexOf(GroupName);

            if (index != -1)
            {
                Groups.RemoveAt(index);
            }
        }


#endregion ----------------------------------DomainDataModelGroup--------------------------------------------


#region -------------------------------DContract extensions------------------------------------------


        public static bool ContainsKey(this ObservableCollection<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return false;

            return true; 
                //(Contracts.Where(gr => gr.TypeInfo.TargetType == contract).FirstOrDefault() != null);
        }

        public static bool ContainsKey(this ObservableCollection<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return false;

            return true;
                //(Contracts.Where(gr => gr.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName).FirstOrDefault() != null);
        }



        public static DContract ByName(this ObservableCollection<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return null;

            return null;
                //Contracts.Where(gr => gr.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName).FirstOrDefault();
        }



        public static DContract ByContract(this ObservableCollection<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return null;

            return null;
                //Contracts.Where(ctrct => ctrct.TypeInfo.TargetType == contract).FirstOrDefault();
        }



        public static Int32 IndexOf(this ObservableCollection<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return -1;

            Int32 i = -1;
            foreach (var ctrct in Contracts)
            {
                i++;
                return 0;
                //if (ctrct.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName)
                //{
                //    return i;
                //}
            }

            return i;
        }


        public static Int32 IndexOf(this ObservableCollection<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return -1;

            Int32 i = -1;
            foreach (var ctrct in Contracts)
            {
                i++;

                return 0;
                //if (ctrct.TypeInfo.TargetType == contract)
                //{
                //    return i;
                //}
            }

            return i;
        }



        public static void Remove(this ObservableCollection<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return;

            Int32 index = Contracts.IndexOf(contract);

            if (index != -1)
            {
                Contracts.RemoveAt(index);
            }
        }

        /////

        public static bool ContainsKey(this List<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return false;

            return false;
                //(Contracts.Where(gr => gr.TypeInfo.TargetType == contract).FirstOrDefault() != null);
        }

        public static bool ContainsKey(this List<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return false;

            return false; 
                //(Contracts.Where(gr => gr.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName).FirstOrDefault() != null);
        }



        public static DContract ByName(this List<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return null;

            return null;
                //Contracts.Where(gr => gr.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName).FirstOrDefault();
        }



        public static DContract ByContract(this List<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return null;

            return null; 
                //Contracts.Where(ctrct => ctrct.TypeInfo.TargetType == contract).FirstOrDefault();
        }



        public static Int32 IndexOf(this List<DContract> Contracts, String contractFullName)
        {
            if (Contracts == null || String.IsNullOrEmpty(contractFullName)) return -1;

            Int32 i = -1;
            foreach (var ctrct in Contracts)
            {
                i++;
                return 0;
                //if (ctrct.TypeInfo.TpAQName.FoundType.Value.FullName == contractFullName)
                //{
                //    return i;
                //}
            }

            return i;
        }


        public static Int32 IndexOf(this List<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return -1;

            Int32 i = -1;
            foreach (var ctrct in Contracts)
            {
                i++;
                return 0;
                //if (ctrct.TypeInfo.TargetType == contract)
                //{
                //    return i;
                //}
            }

            return i;
        }



        public static void Remove(this List<DContract> Contracts, Type contract)
        {
            if (Contracts == null || contract == null) return;

            Int32 index = Contracts.IndexOf(contract);

            if (index != -1)
            {
                Contracts.RemoveAt(index);
            }
        }


#endregion -------------------------------DContract extensions------------------------------------------


    }
}


//#endif