﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class AttributesExtensions
    {


        #region   ---------------- BEGIN Attributes Extensions  -----------------

        /// <summary>
        /// Get custom attributes of {TAttribute} of [instance] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetInstance"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetTypeAttributes<TAttribute>(this object targetInstance, bool useInheritedAttributes = false )
            where TAttribute : Attribute
        { 

            var result = new List<TAttribute>();
            if (targetInstance.IsNull()) return result;

            var attribType = typeof(TAttribute);
            
            var attribs = targetInstance.GetType().GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }
             
            return result;
        }

        /// <summary>
        /// Get first attribute of {TAttribute} of [instance] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetInstance"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstTypeAttribute<TAttribute>(this object targetInstance, bool useInheritedAttributes = false)
           where TAttribute : Attribute
        {
            return  GetTypeAttributes<TAttribute>(targetInstance, useInheritedAttributes).FirstOrDefault();
             
        }

        /// <summary>
        ///  Get custom attributes of {TAttribute} of [targetType] Type.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetTypeAttributes<TAttribute>(this Type targetType, bool useInheritedAttributes = false)
            where TAttribute : Attribute
        {
            var result = new List<TAttribute>();
            if (targetType.IsNull()) return result;

            var attribType = typeof(TAttribute);
            
            var attribs = targetType.GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }

            return result; 
        }

        /// <summary>
        /// Get first attribute of {TAttribute} of [instance] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetType"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstTypeAttribute<TAttribute>(this Type targetType, bool useInheritedAttributes = false)
           where TAttribute : Attribute
        {
            return GetTypeAttributes<TAttribute>(targetType, useInheritedAttributes).FirstOrDefault();

        }


        /// <summary>
        ///  Get custom attributes of {TAttribute} of [taretProperty].
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="taretProperty"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetPropertyAttributes<TAttribute>(this PropertyInfo taretProperty, bool useInheritedAttributes = false)
            where TAttribute : Attribute
        {
            var result = new List<TAttribute>();
            if (taretProperty.IsNull()) return result;

            var attribType = typeof(TAttribute);
            
            var attribs = taretProperty.GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }

            return result;
        }

        /// <summary>
        /// Get first attribute of {TAttribute} of [targetProperty] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetProperty"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstPropertyAttribute<TAttribute>(this PropertyInfo targetProperty, bool useInheritedAttributes = false)
         where TAttribute : Attribute
        {
            return GetPropertyAttributes<TAttribute>(targetProperty, useInheritedAttributes).FirstOrDefault();

        }


        /// <summary>
        ///  Get custom attributes of {TAttribute} of [taretField].
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetField"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetFieldAttributes<TAttribute>(this FieldInfo targetField,  bool useInheritedAttributes = false)
            where TAttribute : System.Attribute
        {
            var result = new List<TAttribute>();
            if (targetField.IsNull()) return result;

            var attribType = typeof(TAttribute);

            var attribs = targetField.GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }

            return result;  
        }

        /// <summary>
        ///  Get first attribute of {TAttribute} of [targetField] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetField"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstFieldAttribute<TAttribute>(this FieldInfo targetField, bool useInheritedAttributes = false)
         where TAttribute : Attribute
        {
            return GetFieldAttributes<TAttribute>(targetField, useInheritedAttributes).FirstOrDefault();

        }


        /// <summary>
        ///  Get custom attributes of {TAttribute} of [targetMember].
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetMember"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetMemberAttributes<TAttribute>(this MemberInfo targetMember, bool useInheritedAttributes = false)
           where TAttribute : System.Attribute
        {
            var result = new List<TAttribute>();
            if (targetMember.IsNull()) return result;

            var attribType = typeof(TAttribute);

            var attribs = targetMember.GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }

            return result;
        }

        /// <summary>
        ///  Get first attribute of {TAttribute} of [targetMember] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetMember"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstMemberAttribute<TAttribute>(this MemberInfo targetMember, bool useInheritedAttributes = false)
         where TAttribute : Attribute
        {
            return GetMemberAttributes<TAttribute>(targetMember, useInheritedAttributes).FirstOrDefault();

        }


        /// <summary>
        /// Get custom attributes of {TAttribute} of [targetMethod].
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetMethod"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static List<TAttribute> GetMethodAttributes<TAttribute>(this MethodInfo targetMethod, bool useInheritedAttributes = false)
          where TAttribute : System.Attribute
        {
            var result = new List<TAttribute>();
            if (targetMethod.IsNull()) return result;

            var attribType = typeof(TAttribute);

            var attribs = targetMethod.GetCustomAttributes(attribType, useInheritedAttributes);
            if (attribs.IsNotWorkable()) return result;

            for (int i = 0; i < attribs.Length; i++)
            {
                if (attribs[i].GetType().Name == typeof(TAttribute).Name)
                { result.Add(attribs[i] as TAttribute); }
            }
            return result;           
        }

        /// <summary>
        /// Get first attribute of {TAttribute} of [targetField] value.
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="targetMethod"></param>
        /// <param name="useInheritedAttributes"></param>
        /// <returns></returns>
        public static TAttribute GetFirstMethodAttribute<TAttribute>(this MethodInfo targetMethod, bool useInheritedAttributes = false)
        where TAttribute : Attribute
        {
            return GetMethodAttributes<TAttribute>(targetMethod, useInheritedAttributes).FirstOrDefault();

        }






        #endregion  ---------------- BEGIN Attributes  Utilities -----------------

    }
}
