﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class ObjectExtensions
    {

        /// <summary>
        /// If this object is of Deleegate Type. True - yes it is.
        /// </summary>
        /// <param name="targetObject"></param>
        /// <returns></returns>
        public static bool IsDelegate(this object targetObject )
        {
            return (targetObject is Delegate);
        } 

    }
}
