﻿using System;
using System.Collections;
using System.Collections.Generic;


namespace DDDD.Core.Extensions
{

    public static class EnumerableExtensions
    {

        //const string ERR_ArgNull = "ERROR in {0}.{1}(): argument {2} can't be null";


        /// <summary>
        ///  Count of items in IEnumerable variable
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static int Count(this IEnumerable collection)
        {
            if (collection.IsNull()) { return 0; }

            if ((collection as ICollection).IsNotNull())
            {
                return (collection as ICollection).Count;
            }
            else
            {
                Int32 count = 0;
                foreach (var item in collection)
                { count++; }
                return count;
            }
        }




        /// <summary>
        /// Checking that this IEnumerable is not null and  has one or more items.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="somecollection"></param>
        /// <returns></returns>
        public static bool IsWorkable<T>(this IEnumerable<T> somecollection)
        {
            return (somecollection.IsNotNull() && somecollection.Count() > 0);
        }

        /// <summary>
        ///  Checking that this IEnumerable IS null or  has no even one item - then returns true.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="somecollection"></param>
        /// <returns></returns>
        public static bool IsNotWorkable<T>(this IEnumerable<T> somecollection)
        {
            return (somecollection.IsNull() || somecollection.Count() == 0);
        }








        /// <summary>
        /// If enumeration of items contains even one item, where item return true value in predicateItemCheck function - then return true, otherwise false.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentEnumerable"></param>
        /// <param name="predicateItemCheck"></param>
        /// <returns></returns>
        public static bool ContainsItemWhere<T>(this IEnumerable<T> currentEnumerable, Func<T,bool>  predicateItemCheck)
        {
            if (currentEnumerable == null || currentEnumerable.Count() == 0 || predicateItemCheck == null) return false;

            if (currentEnumerable is IList<T>)
            {
                var listEnumerable = currentEnumerable as IList<T>;
                for (int i = 0; i < listEnumerable.Count; i++)
                {
                    if (predicateItemCheck(listEnumerable[i]) == true) return true;

                }
            }
            else if (currentEnumerable is IList)
            {
                var listEnumerable = currentEnumerable as IList;
                for (int i = 0; i < listEnumerable.Count; i++)
                {
                    if (predicateItemCheck((T)listEnumerable[i]) == true) return true;

                }
            }
            else
            {
                foreach (var item in currentEnumerable)
                {
                    if (predicateItemCheck(item) == true) return true;
                }
            }
            
            return false;
        }


        /// <summary>
        ///  Check that some (HashSet{string} DO NOT CONTAINS checkingContainsItem). Method return True only If (HashSet{string} DO NOT CONTAINS checkingContainsItem).
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="checkContainsItem"></param>
        /// <returns></returns>
        public static bool NotContains(this HashSet<string> currentCollection, string checkContainsItem)
        {
            return !currentCollection.Contains(checkContainsItem);
        }

        /// <summary>
        ///  Check that some (HashSet{string} DO NOT CONTAINS checkingContainsItem). Method return True only If (HashSet{string} DO NOT CONTAINS checkingContainsItem).
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="checkContainsItem"></param>
        /// <returns></returns>
        public static bool NotContains(this ICollection<string> currentCollection, string checkContainsItem)
        {
            return !currentCollection.Contains(checkContainsItem);
        }

        /// <summary>
        ///  Check that some (HashSet{string} DO NOT CONTAINS checkingContainsItem). Method return True only If (HashSet{string} DO NOT CONTAINS checkingContainsItem).
        /// </summary>
        /// <param name="currentCollection"></param>
        /// <param name="checkContainsItem"></param>
        /// <returns></returns>
        public static bool NotContains(this ICollection<Type> currentCollection, Type checkContainsItem)
        {
            return !currentCollection.Contains(checkContainsItem);
        }


        /// <summary>
        /// Check that some Dictionary{TKey,TValue} NOT CONTAINS KEY VALUE. Method return True If Dictionary{TKey,TValue}.
        /// </summary>
        /// <typeparam name="TKey">TKey- Key Type of Dictionary{TKey,TValue}</typeparam>
        /// <typeparam name="TValue">TValue- Value Type of Dictionary{TKey,TValue}</typeparam>
        /// <param name="somedictionary"> Dictionary{TKey,TValue} instance</param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool NotContainsKey<TKey,TValue>(this IDictionary<TKey,TValue> somedictionary, TKey key)
        {
            //Validator.AssertTrueDbg<ArgumentNullException>(somedictionary.IsNull(), ERR_ArgNull.Fmt(nameof(EnumerableExtensions), nameof(NotContainsKey), nameof(somedictionary)));
            if (somedictionary.IsNotWorkable()) return false;             

            return !somedictionary.ContainsKey(key);
        }




        /// <summary>
        /// Create  new List{TItem} and add into it all items from someEnumerable
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <param name="someEnumerable"></param>
        /// <returns></returns>
        public static List<TItem> ToList<TItem>(this IEnumerable someEnumerable)
        {
            List<TItem> resultCollection = new List<TItem>();

            if (someEnumerable is IList)
            {
                var list = (someEnumerable as IList);
                for (int i = 0; i < list.Count; i++)
                {
                    resultCollection.Add((TItem)list[i]);
                }
            }
            else
            {
                foreach (var item in someEnumerable)
                {
                    resultCollection.Add((TItem)item);
                }
            }
            
            return resultCollection;
        }


        /// <summary>
        /// Create  new object[] and add into it all items from someEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tEnumerableCollection"></param>
        /// <returns></returns>
        public static object[]  ToObjectArray<T>(this IEnumerable<T> tEnumerableCollection)
        {
            if (tEnumerableCollection == null) return null;

            var resultarray = new object[ tEnumerableCollection.Count() ];

            int index = 0;

            foreach (var item in tEnumerableCollection)
            {       resultarray[index++] = item;
            }

            return resultarray;
        }


        /// <summary>
        /// Get int[] of dimensions  of some array
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int[] GetDimensionsLengths(this Array array)
        {
            int[] dimensionLengths = new int[array.Rank];
            for (int i = 0; i < array.Rank; i++)
            { dimensionLengths[i] = array.GetLength(i);
            }            
            return dimensionLengths;
        }

#if NET45 

        /// <summary>
        /// Create  new List{TItem} and add into it all items from someEnumerable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static List<T> ToListOfT<T>(this ArrayList array)
        {
            var result = new List<T>();

            if (array == null || array.Count == 0) return result;

            for (int i = 0; i < array.Count; i++)
            {
                result.Add((T)array[i]);
            }
            return result;
        }

#endif

        /// <summary>
        ///  extension for  List{Type} collections - listofTypes.Add(typeof(T))
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="listofTypes"></param>
        public static void Add<T>(this List<Type> listofTypes)
        {
            if (listofTypes == null) return;
            listofTypes.Add(typeof(T));
        }


        /// <summary>
        /// Checks whether a collection is the same as another collection.
        /// </summary>
        /// <param name="listA">The list A.</param>
        /// <param name="listB">The list B.</param>
        /// <returns>
        /// True if the two collections contain all the same items in the same order.
        /// </returns>
        public static bool IsEqualTo(this IEnumerable listA, IEnumerable listB)
        {
            if (listA == listB)
            {
                return true;
            }

            if ((listA == null) || (listB == null))
            {
                return false;
            }

            if (ReferenceEquals(listA, listB))
            {
                return true;
            }

            var enumeratorA = listA.GetEnumerator();
            var enumeratorB = listB.GetEnumerator();

            var enumAHasValue = enumeratorA.MoveNext();
            var enumBHasValue = enumeratorB.MoveNext();

            while (enumAHasValue && enumBHasValue)
            {
                var currentA = enumeratorA.Current;
                var currentB = enumeratorB.Current;

                if (currentA == currentB)
                {
                    enumAHasValue = enumeratorA.MoveNext();
                    enumBHasValue = enumeratorB.MoveNext();

                    continue;
                }

                if (! AreEqual(currentA, currentB))
                {
                    return false;
                }

                enumAHasValue = enumeratorA.MoveNext();
                enumBHasValue = enumeratorB.MoveNext();
            }

            // If we get here, and both enumerables don't have any value left, they are equal
            return !(enumAHasValue || enumBHasValue);
        }


        /// <summary>
        ///   Checks whether the 2 specified objects are equal. This method is better, simple because it also checks boxing so
        ///   2 integers with the same values that are boxed are equal.
        /// </summary>
        /// <param name = "object1">The first object.</param>
        /// <param name = "object2">The second object.</param>
        /// <returns><c>true</c> if the objects are equal; otherwise <c>false</c>.</returns>
        public static bool AreEqual(object object1, object object2)
        {
            if ((object1 == null) && (object2 == null))
            {
                return true;
            }

            if ((object1 == null) || (object2 == null))
            {
                return false;
            }

            if (ReferenceEquals(object1, object2))
            {
                return true;
            }

            var firstTagAsString = object1 as string;
            var secondTagAsString = object2 as string;

            if ((firstTagAsString != null) && (secondTagAsString != null))
            {
                return string.Compare(firstTagAsString, secondTagAsString, StringComparison.Ordinal) == 0;
            }

            if (object1 == object2)
            {
                return true;
            }

            if (object1.Equals(object2))
            {
                return true;
            }

            return false;
        }






        /// <summary>
        /// Copy from [source] IEnumerable{T} to T[] [dest] array, and use  [destStartIndex] as the beginnig point of items replacement-copy. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="destStartIndex"></param>
        public static void CopyTo<T>(this IEnumerable<T> source, T[] dest, int destStartIndex)
        {
            ICollection<T> ts = source as ICollection<T>;
            if (ts != null)
            {
                ts.CopyTo(dest, destStartIndex);
                return;
            }
            foreach (T t in source)
            {
                int num = destStartIndex;
                destStartIndex = num + 1;
                dest[num] = t;
            }
        }

        /// <summary>
        /// Copy from [source] IEnumerable{T} to T[] [dest] array, and use  [destStartIndex] as the beginnig point of items replacement-copy. 
        /// </summary>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        /// <param name="destStartIndex"></param>
        public static void CopyTo(this IEnumerable source, Array dest, int destStartIndex)
        {
            foreach (object obj in source)
            {
                int num = destStartIndex;
                destStartIndex = num + 1;
                dest.SetValue(obj, num);
            }

        }





    }
}




#region --------------------------------------- GARBAGE ------------------------------------------

///// <summary>
///// Check that some (IEnumerable{T} IS NULL OR LENGTH EQUALS ZERO). Method return True only If (IEnumerable{T} IS NULL OR LENGTH EQUALS ZERO).
///// </summary>
///// <typeparam name="T"></typeparam>
///// <param name="someEnumerable"></param>
///// <returns></returns>
//public static bool IsNullOrLengthEqualsZero<T>(this IEnumerable<T> someEnumerable)
//{
//    return (someEnumerable.IsNull() || someEnumerable.Count() == 0);
//}

///// <summary>
///// Check that some (IEnumerable{T} IS NOT NULL AND LENGTHER THAN ZERO). Method return True only If (IEnumerable{T} IS NOT NULL AND LENGTHER THAN ZERO).
///// </summary>
///// <typeparam name="T"></typeparam>
///// <param name="someEnumerable"></param>
///// <returns></returns>
//public static bool IsNotNullAndLengtherThanZero<T>(this IEnumerable<T> someEnumerable)
//{
//    return (someEnumerable.IsNotNull() && someEnumerable.Count()> 0);
//}

#endregion --------------------------------------- GARBAGE ------------------------------------------

