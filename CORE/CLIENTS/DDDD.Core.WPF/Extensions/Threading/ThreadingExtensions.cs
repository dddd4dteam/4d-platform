﻿using DDDD.Core.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Extensions
{
    public static class ThreadingExtensions
    {

        #region --------------------------- LOCKING ACTIONS for OBJECT ------------------------------- 



        /// <summary>
        ///  Here we  run locked Action inside construction - [lock(){ Action}]. 
        /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True; and  Action will be called if  (conditionToUseLockAndToDoAction) is True;
        /// <para/> In DEBUG mode this method checks that lockingObject and lockedAction argument values is not null, else ArgumentNullException will be thrown.
        /// </summary>
        /// <param name="conditionToUseLockAndToDoAction">[lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True,  and  Action will be called if(conditionToUseLockAndToDoAction) is True</param>
        /// <param name="lockingObject">object for [lock(item){}] construction</param>
        /// <param name="lockedAction">Action inside locked block  - [lock(item){ YOUR Action HERE }]</param>
        /// <exception cref="ArgumentNullException">If lockingObject or lockedAction argument values is null</exception>
        public static void LockAction(this object lockingObject, Func<bool> conditionToUseLockAndToDoAction,  Action lockedAction)
        {
            Lock.LockAction(conditionToUseLockAndToDoAction, lockingObject, lockedAction);
        }



        /// <summary>
        ///  Here we  run locked Action inside construction - [lock(){ Action}]. 
        /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLock) is True; Action will be called if  (conditionInsideLock) is True;
        /// <para/> In DEBUG mode this method checks that lockingObject and lockedAction argument values is not null, else ArgumentNullException will be thrown.
        /// </summary>
        /// <param name="conditionToUseLock">[lock(){}]  will be organized if (conditionToUseLock) is True</param>
        /// <param name="conditionInsideLock">Action will be called if  (conditionInsideLock) is True</param>
        /// <param name="lockingObject">object for [lock(item){}] construction</param>
        /// <param name="lockedAction">Action inside locked block  - [lock(item){ YOUR Action HERE }]</param>
        public static void LockAction2(this object lockingObject, Func<bool> conditionToUseLock, Func<bool> conditionInsideLock, Action lockedAction)
        {
            Lock.LockAction2(conditionToUseLock, conditionInsideLock, lockingObject, lockedAction);
        }




        /// <summary>
        /// Here we  run locked Func{T} inside construction - [lock(){ Func{T}}].
        /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True; and  Func{T} will be called if  (conditionToUseLockAndToDoAction) is True;
        /// <para/> In DEBUG mode this method checks that lockingObject and lockedFunc argument values is not null, else ArgumentNullException will be thrown.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="lockingObject">object for [lock(item){}] construction</param>
        /// <param name="conditionToUseLockAndToDoAction">[lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True,  and Func{T} will be called if(conditionToUseLockAndToDoAction) is True</param>
        /// <param name="lockedFunc">Func{T} inside locked block  - [lock(item){ YOUR Action HERE }]</param>
        /// <returns></returns>
        //public static  void LockFunc<T>(this object lockingObject , bool conditionToUseLockAndToDoAction, Func<T> lockedFunc)
        //{
        //    Lock.LockFunc(conditionToUseLockAndToDoAction, lockingObject, lockedFunc);
        //}




        #endregion --------------------------- LOCKING ACTIONS for OBJECT -------------------------------
    }

}
