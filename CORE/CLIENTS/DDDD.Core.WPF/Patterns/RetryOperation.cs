﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Threading;

using DDDD.Core.Threading;
using DDDD.Core.Extensions;

namespace DDDD.Core.Patterns
{


	/// <summary>
	/// Retry Operation Pattern. 
	/// We run Operation a few times, with a certain delay before the next Run of the operation, if the last operation was completed with an error.
	/// </summary>
	public class RetryOperation
	{   
					
		static readonly TimeSpan defaultDelay = new TimeSpan(0, 0, 10);//10  seconds


		/// <summary>
		/// Retry  custom operation(Action) on it's Fault.
		/// </summary>
		/// <param name="count"></param>
		/// <param name="waitMilliseconds"></param>
		/// <param name="caughtExceptionType"></param>
		/// <param name="retryOperation"></param>
		public static void RetryOnFault(Action retryOperation, int triesCount, Type caughtExceptionType, TimeSpan delayBetweenRetryTime = default(TimeSpan)  )//  = typeof(InvalidOperationException) )
		{   
			delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
			for (int i = 0; i < triesCount; i++)
			{
				try
				{
					retryOperation();
				}
				catch (Exception exc)
				{
					if (i == triesCount - 1) throw exc;                    
				}
				Thread.Sleep(delayBetweenRetryTime);               
			} 
			
		}



		/// <summary>
		/// Retry  custom operation(Action) on it's Fault.
		/// If last retry will throw exception then this method will rethrow exception again.
		/// </summary>
		/// <param name="retryOperation"></param>
		/// <param name="triesCount"></param>
		/// <param name="delayBetweenRetryTime"></param>
		/// <returns></returns>
		public static Task RetryOnFault(Action retryOperation, int triesCount = 3, TimeSpan delayBetweenRetryTime = default(TimeSpan))
		{
			return Task.Factory.StartNew(
				() =>
				{
					delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
					for (int i = 0; i < triesCount; i++)
					{
						try
						{
							retryOperation(); break;// this operation should throw if false
						}
						catch (Exception exc)
						{
							if (i == triesCount - 1) throw exc; // throw after all retries - if each time false
						}
						Thread.Sleep(delayBetweenRetryTime);
					}
				}
				);
		}



		/// <summary>
		/// Retry  custom operation(Action) on it's Fault.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="count"></param>
		/// <param name="waitMilliseconds"></param>
		/// <param name="caughtExceptionType"></param>
		/// <param name="callback"></param>
		/// <returns></returns>
		public static T RetryOnFault<T>(Func<T> callback, int count, int waitMilliseconds, Type caughtExceptionType )
		{
			T t;
			int retryCount = count;
			Exception lastException = null;
			while (retryCount > 0)
			{
				try
				{
					t = callback();
				}
				catch (Exception exception)
				{
					Exception e = exception;
					if (!e.GetType().IsAssignableFrom(caughtExceptionType))
					{
						throw;
					}
					lastException = e;
					retryCount--;
					Thread.Sleep(waitMilliseconds);
					continue;
				}
				return t;
			}
			throw lastException;
		}

		/// <summary>
		/// Retry
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="count"></param>
		/// <param name="waitMilliseconds"></param>
		/// <param name="callback"></param>
		/// <param name="expectation"></param>
		/// <returns></returns>
		public static T RetryOnFault<T>(int count, int waitMilliseconds, Func<T> callback, Func<T, int, Exception, bool> expectation)
		{
			int retryCount = count;
			T result = default(T);
			while (retryCount > 0)
			{
				Exception error = null;
				try
				{
					result = callback();
				}
				catch (Exception exception)
				{
					error = exception;
				}
				if (expectation(result, retryCount, error))
				{
					break;
				}
				retryCount--;
				Thread.Sleep(waitMilliseconds);
			}
			return result;
		}
		 



		/// <summary>
		/// Retry custom operation(Action) on it's Fault.
		/// If last retry will throw exception then this method will rethrow exception again. 
		/// </summary>
		/// <param name="retryOperation"></param>
		/// <param name="maxTries"></param>
		/// <param name="delayBetweenRetryTime"></param>
		/// <returns></returns>
		public static async Task RetryOnFaultAsync(Action retryOperation, int maxTries = 3, TimeSpan delayBetweenRetryTime = default(TimeSpan))
		{
			delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
			for (int i = 0; i < maxTries; i++)
			{
				try
				{
					retryOperation(); break;// this operation should throw if false
				}
				catch (Exception exc)
				{                   
					if (i == maxTries - 1 ) throw exc; // throw after all retries - if each time false
				}
				await TaskShim.Delay(delayBetweenRetryTime);
			}
		}




		/// <summary>
		/// Retry custom operation(Func{T}) on it's Fault.
		/// If last retry will throw exception then this method will rethrow exception again.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="retryOperation"></param>
		/// <param name="maxTries"></param>
		/// <param name="delayBetweenRetryTime"></param>
		/// <returns></returns>
		public static async Task<T> RetryOnFaultAsync<T>(Func<T> retryOperation, int maxTries = 3, TimeSpan delayBetweenRetryTime = default(TimeSpan))
		{
			delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
			for (int i = 0; i < maxTries; i++)
			{
				try
				{
				  return  retryOperation();  // this operation should throw if false
				}
				catch (Exception exc)
				{
					if (i == maxTries - 1) throw exc; // throw after all retries - if each time false
				}
				await TaskShim.Delay(delayBetweenRetryTime);
			}

			return default(T);
		}


		/// <summary>
		/// Retry custom operation(Func{T}) on it's Fault.
		/// If last retry will throw exception then this method will rethrow exception again.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="retryOperation"></param>
		/// <param name="maxTries"></param>
		/// <param name="delayBetweenRetryTime"></param>
		/// <returns></returns>
		public static Task<T> RetryOnFault<T>(Func<T> retryOperation, int maxTries = 3, TimeSpan delayBetweenRetryTime = default(TimeSpan))
		{
			return Task.Factory.StartNew (
				() =>
				{
					delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
					for (int i = 0; i < maxTries; i++)
					{
						try
						{
							return retryOperation(); // this operation should throw if false
						}
						catch (Exception exc)
						{
							if (i == maxTries - 1) throw exc; // throw after all retries - if each time false
						}
						Thread.Sleep(delayBetweenRetryTime);
					}
					return default(T);
				}
				);

		}




		/// <summary>
		/// Retry  custom Action on it's Fault. 
		/// If last retry will throw exception then this method will rethrow exception again.
		/// </summary>
		/// <param name="retryOperation"></param>
		/// <param name="isTransientFault"></param>
		/// <param name="maxTries"></param>
		/// <param name="delayBetweenRetryTime"></param>
		/// <returns></returns>
		public static async Task RetryOnFaultWithTransient(Action retryOperation, Func<Exception, bool> isTransientFault = null,  int maxTries = 3, TimeSpan delayBetweenRetryTime = default(TimeSpan))
		{
			delayBetweenRetryTime = (delayBetweenRetryTime == default(TimeSpan)) ? defaultDelay : delayBetweenRetryTime;
			Func<Exception, bool> transientCheck = (isTransientFault.IsNull()) ? IsTransient : isTransientFault;
			for (int i = 0; i < maxTries; i++)
			{
				try
				{
					retryOperation(); break;// this operation should throw if false
				}
				catch (Exception exc)
				{
				   // Trace.TraceError("Operation Exception");                     

					// Check if the exception thrown was a transient exception
					// based on the logic in the error detection strategy.
					// Determine whether to retry the operation, as well as how 
					// long to wait, based on the retry strategy.
					if (i == (maxTries - 1) || !transientCheck(exc))
					{
						// If this is not a transient error 
						// or we should not retry re-throw the exception. 
						throw;
					}
				}
				await TaskShim.Delay(delayBetweenRetryTime);
			}

		}

		/// <summary>
		/// Default Check - if Exception is  Transient Fault.
		/// </summary>
		/// <param name="ex"></param>
		/// <returns></returns>
		protected static bool IsTransient(Exception ex)
		{
			// Determine if the exception is transient.
			// In some cases this may be as simple as checking the exception type, in other 
			// cases it may be necessary to inspect other properties of the exception.
			if (ex.GetType().Name == "OperationTransientException") //Retry with Transient Exception
				return true;
#if SERVER ||  WPF 
			var webException = ex as WebException;
			if (webException != null)
			{
				// If the web exception contains one of the following status values 
				// it may be transient.
				return new[] { WebExceptionStatus.ConnectionClosed,
				  WebExceptionStatus.Timeout,
				  WebExceptionStatus.RequestCanceled }.Contains(webException.Status);
			}
#elif SL5 || WP81
			var webException = ex as CommunicationException;

			if (webException != null)
			{
				return true;
				 
			}
#endif


			// Additional exception checking logic goes here.
			return false;
		}



	   



		#region ---------------------- PROPERTIES -------------------------

		///// <summary>
		/////  Async method that wraps a call to a remote service (details not shown).
		///// </summary>
		//protected  Task   RetryTask { get; set; }


		///// <summary>
		///// Retry Count. By default it equals 3. 
		///// </summary>
		//public int RetryCount { get; set; } = 3;


		///// <summary>
		///// Delay time between retry. By default it equals 10 seconds. 
		///// </summary>
		//public TimeSpan DelayBetweenRetryTime
		//{ get; set; } = new TimeSpan(0, 0, seconds: 10);


		#endregion ---------------------- PROPERTIES ------------------------- 


		//string pageContents = await RetryPattern.RetryOnFault(() => DownloadStringAsync(url)
		//                                , 3 , () => Task.Delay(1000) );
		//public static async Task RetryOnFault(Task retryOperationT , int maxTries, TimeSpan delayTime)
		//{
		//    for (int i = 0; i < maxTries; i++)
		//    {
		//        try
		//        {
		//           retryOperationT.Start();
		//        }
		//        catch
		//        {
		//            if (i == maxTries - 1) throw;
		//        }
		//        await TaskShim.Delay(delayTime);
		//    }
		//}
		//public static async Task RetryOnFault(Task<bool> retryOperationT, int maxTries, TimeSpan delayTime)
		//{
		//    
		//    for (int i = 0; i < maxTries; i++)
		//    {
		//        try
		//        {
		//            retryOperationT.Start();
		//        }
		//        catch
		//        {
		//            if (i == maxTries - 1) throw;
		//        }
		//        await TaskShim.Delay(delayTime);
		//    }
		//
		//}




		//// https://msdn.microsoft.com/en-us/library/dn589788.aspx        
		//public async Task OperationWithBasicRetryAsync<TArg1>(TArg1 argument1)
		//{
		//    int currentRetry = 0;

		//    for (;;)
		//    {
		//        try
		//        {
		//            // Calling external service.
		//            RetryTask.Start();
		//            //await RetryOperation(argument1);

		//            // Return or break.
		//            break;
		//        }
		//        catch (Exception ex)
		//        {
		//            Trace.TraceError("Operation Exception");

		//            currentRetry++;

		//            // Check if the exception thrown was a transient exception
		//            // based on the logic in the error detection strategy.
		//            // Determine whether to retry the operation, as well as how 
		//            // long to wait, based on the retry strategy.
		//            if (currentRetry > RetryCount || !IsTransient(ex))
		//            {
		//                // If this is not a transient error 
		//                // or we should not retry re-throw the exception. 
		//                throw;
		//            }
		//        }

		//        // Wait to retry the operation.
		//        // Consider calculating an exponential delay here and 
		//        // using a strategy best suited for the operation and fault.
		//        await TaskShim.Delay(DelayBetweenRetryTime);
		//    }
		//}



		// Async method that wraps a call to a remote service (details not shown).
		//public static async Task<TArg1> RetryOnFaultInternal<TArg1>(Task<TArg1> retryOperationT, int maxTries, TimeSpan delayTime)
		//{
		//    for (int i = 0; i < maxTries; i++)
		//    {
		//        try
		//        {
		//            retryOperationT.S <TArg1>();
		//        }
		//        catch
		//        {
		//            if (i == maxTries - 1) throw;
		//        }
		//        await TaskShim.Delay(delayTime);
		//    }
		//}
		//
		//public async Task  RunWithRetryOnFault()
		//{
		//     await RetryOnFaultInternal(RetryOperation, RetryCount, ()=> TaskShim.Delay(DelayBetweenRetryTime) );
		//}
		//
		//
		//public async Task<T> RunWithRetryOnFault<T>(Func<Task<T>> retryOperationT)
		//{
		//   return await RetryOnFaultInternal(retryOperationT, RetryCount, () => TaskShim.Delay(DelayBetweenRetryTime));
		//}
		//
		//
		//
		//private async Task TransientOperationAsync()
		//{
		//
		//}

	}
}
