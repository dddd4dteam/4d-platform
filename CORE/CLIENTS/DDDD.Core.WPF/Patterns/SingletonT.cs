﻿
using DDDD.Core.Threading;
using DDDD.Core.Reflection;

namespace DDDD.Core.Patterns
{



    public class Singleton<T>
        where T : class//, new()
    {


        /// <summary>
        /// Instance of DCManager of [TManaget] Type - your custom end DCManager Type
        /// </summary>
        protected static LazyAct<T> LA_Current
        { get; } = LazyAct<T>.Create(
            (args) =>
            {
                var newTargetT = TypeActivator.CreateInstanceT<T>();// new T();
                
                return newTargetT;
            }
            , null, null
            );
        

    }
}
