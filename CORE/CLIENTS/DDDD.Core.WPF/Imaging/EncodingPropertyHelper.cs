using System.Linq;
using System.Collections.Generic;

namespace DDDD.Core.Imaging
{
	internal class EncodingPropertyHelper
	{
		public EncodingPropertyHelper()
		{
		}

		internal static T GetEncodingPropertyValue<T>(IList<EncodingProperty> encodingProperties, T defaultValue)
		where T : EncodingProperty
		{
			T current;
			if (encodingProperties != null)
			{
				using (IEnumerator<EncodingProperty> enumerator = (
					from property in encodingProperties
					where property.GetType() == typeof(T)
					select property).GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						current = (T)enumerator.Current;
					}
					else
					{
						return defaultValue;
					}
				}
				return current;
			}
			return defaultValue;
		}
	}
}