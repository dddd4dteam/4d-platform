﻿namespace DDDD.Core.Imaging
{
	/// <summary>
	/// Class equivalent to System.Drawing.ImageFormat used for Imaging support
	/// </summary>
	public class ImageFormat
	{
		/// <summary>
		/// Gets the bitmap (BMP) image format.
		/// </summary>
		public readonly static System.Guid BmpGuid;

		/// <summary>
		/// Gets the Device-Independent Bitmap (DIB) image format.
		/// </summary>
		public readonly static System.Guid DibGuid;

		/// <summary>
		/// Gets the enhanced metafile (EMF) image format.
		/// </summary>
		public readonly static System.Guid EmfGuid;

		/// <summary>
		/// Gets the Graphics Interchange Format (GIF) image format.
		/// </summary>
		public readonly static System.Guid GifGuid;

		/// <summary>
		/// Gets the Joint Photographic Experts Group (JPEG) image format.
		/// </summary>
		public readonly static System.Guid JpegGuid;

		/// <summary>
		/// Gets the W3C Portable Network Graphics (PNG) image format.
		/// </summary>
		public readonly static System.Guid PngGuid;

		/// <summary>
		/// Gets the Tagged Image File Format (TIFF) image format.
		/// </summary>
		public readonly static System.Guid TiffGuid;

		/// <summary>
		/// Gets the Windows metafile (WMF) image format.
		/// </summary>
		public readonly static System.Guid WmfGuid;

		/// <summary>
		/// Static variable for Bmp image format
		/// </summary>
		public readonly static ImageFormat Bmp;

		/// <summary>
		/// Static variable for Dib image format
		/// </summary>
		public readonly static ImageFormat Dib;

		/// <summary>
		/// Static variable for Emf image format
		/// </summary>
		public readonly static ImageFormat Emf;

		/// <summary>
		/// Static variable for Gif image format
		/// </summary>
		public readonly static ImageFormat Gif;

		/// <summary>
		/// Static variable for Jpeg image format
		/// </summary>
		public readonly static ImageFormat Jpeg;

		/// <summary>
		/// Static variable for Png image format
		/// </summary>
		public readonly static ImageFormat Png;

		/// <summary>
		/// Static variable for Tiff image format
		/// </summary>
		public readonly static ImageFormat Tiff;

		/// <summary>
		/// Static variable for Wmf image format
		/// </summary>
		public readonly static ImageFormat Wmf;

		/// <summary>
		/// Gets the <see cref="P:DDDD.Core.Imaging.ImageFormat.Guid" /> that identifies this <see cref="T:DDDD.Core.Imaging.ImageFormat" />
		/// </summary>
		public System.Guid Guid
		{
			get;
			private set;
		}

		static ImageFormat()
		{
			ImageFormat.BmpGuid = new System.Guid("B96B3CAA-0728-11D3-9D7B-0000F81EF32E");
			ImageFormat.DibGuid = new System.Guid("824CA8E2-A984-4668-8A14-077225372B1C");
			ImageFormat.EmfGuid = new System.Guid("B96B3CAC-0728-11D3-9D7B-0000F81EF32E");
			ImageFormat.GifGuid = new System.Guid("1FE88D21-FE3D-4F4A-ABD3-8DBAD86C510A");
			ImageFormat.JpegGuid = new System.Guid("0C487171-1412-4D76-B20A-4AEA380FFAA7");
			ImageFormat.PngGuid = new System.Guid("7B369CF6-0507-4F15-ABE8-5768C70D1454");
			ImageFormat.TiffGuid = new System.Guid("069B1E5E-A617-4498-AAEA-D71AF8933FD3");
			ImageFormat.WmfGuid = new System.Guid("B96B3CAD-0728-11D3-9D7B-0000F81EF32E");
			ImageFormat.Bmp = new ImageFormat(ImageFormat.BmpGuid);
			ImageFormat.Dib = new ImageFormat(ImageFormat.DibGuid);
			ImageFormat.Emf = new ImageFormat(ImageFormat.EmfGuid);
			ImageFormat.Gif = new ImageFormat(ImageFormat.GifGuid);
			ImageFormat.Jpeg = new ImageFormat(ImageFormat.JpegGuid);
			ImageFormat.Png = new ImageFormat(ImageFormat.PngGuid);
			ImageFormat.Tiff = new ImageFormat(ImageFormat.TiffGuid);
			ImageFormat.Wmf = new ImageFormat(ImageFormat.WmfGuid);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:DDDD.Core.Imaging.ImageFormat" /> class.
		/// </summary>
		/// <param name="formatGuid">The <see cref="P:DDDD.Core.Imaging.ImageFormat.Guid" /> used to identify this <see cref="T:DDDD.Core.Imaging.ImageFormat" />.</param>
		protected ImageFormat(System.Guid formatGuid)
		{
			this.Guid = formatGuid;
		}
	}
}
