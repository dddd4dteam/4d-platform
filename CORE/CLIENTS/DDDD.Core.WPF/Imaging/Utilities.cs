
using System;
using System.Threading;

using DDDD.Core.Environment;

namespace DDDD.Core.Imaging
{
	internal static class Utilities
	{
		internal static T EvaluateInUiThread<T>(Func<T> function, Func<Exception, Exception> exceptionBuilder)
		{
			Exception exception1 = null;
			T t = default(T);
			AutoResetEvent autoResetEvent = new AutoResetEvent(false);
			if (!Environment2.DISP.CheckAccess()) // Deployment.Current.Dispatcher
			{
				Environment2.DISP.BeginInvoke( new Action(() => { //Deployment.Current.Dispatcher
					try
					{
						try
						{
							t = function();
						}
						catch (Exception exception)
						{
							exception1 = exception;
						}
					}
					finally
					{
						autoResetEvent.Set();
					}
				})//Action
				);
				autoResetEvent.WaitOne();
			}
			else
			{
				try
				{
					t = function();
				}
				catch (Exception exception2)
				{
					exception1 = exception2;
				}
			}
			if (exception1 != null)
			{
				throw exceptionBuilder(exception1);
			}
			return t;
		}
	}
}