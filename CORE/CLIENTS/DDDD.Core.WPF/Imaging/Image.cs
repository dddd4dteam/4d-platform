using System;

namespace DDDD.Core.Imaging
{
	internal class Image
	{
		private ColorModel _cm;

		private byte[][,] _raster;

		private int width;

		private int height;

		public ColorModel ColorModel
		{
			get
			{
				return this._cm;
			}
		}

		public int ComponentCount
		{
			get
			{
				return (int)this._raster.Length;
			}
		}

		/// <summary> X density (dots per inch).</summary>
		public double DensityX
		{
			get;
			set;
		}

		/// <summary> Y density (dots per inch).</summary>
		public double DensityY
		{
			get;
			set;
		}

		public int Height
		{
			get
			{
				return this.height;
			}
		}

		public byte[][,] Raster
		{
			get
			{
				return this._raster;
			}
		}

		public int Width
		{
			get
			{
				return this.width;
			}
		}

		public Image(ColorModel cm, byte[][,] raster)
		{
			this.width = raster[0].GetLength(0);
			this.height = raster[0].GetLength(1);
			this._cm = cm;
			this._raster = raster;
		}

		/// <summary>
		/// Converts the colorspace of an image (in-place)
		/// </summary>
		/// <param name="cs">Colorspace to convert into</param>
		/// <returns>Self</returns>
		public Image ChangeColorSpace(ColorSpace cs)
		{
			if (this._cm.colorspace == cs)
			{
				return this;
			}
			if (this._cm.colorspace == ColorSpace.RGB && cs == ColorSpace.YCbCr)
			{
				for (int i = 0; i < this.width; i++)
				{
					for (int j = 0; j < this.height; j++)
					{
						YCbCr.fromRGB(ref this._raster[0][i, j], ref this._raster[1][i, j], ref this._raster[2][i, j]);
					}
				}
				this._cm.colorspace = ColorSpace.YCbCr;
			}
			else if (this._cm.colorspace == ColorSpace.YCbCr && cs == ColorSpace.RGB)
			{
				for (int k = 0; k < this.width; k++)
				{
					for (int l = 0; l < this.height; l++)
					{
						YCbCr.toRGB(ref this._raster[0][k, l], ref this._raster[1][k, l], ref this._raster[2][k, l]);
					}
				}
				this._cm.colorspace = ColorSpace.RGB;
			}
			else if (this._cm.colorspace != ColorSpace.Gray || cs != ColorSpace.YCbCr)
			{
				if (this._cm.colorspace != ColorSpace.Gray || cs != ColorSpace.RGB)
				{
					throw new Exception("Colorspace conversion not supported.");
				}
				this.ChangeColorSpace(ColorSpace.YCbCr);
				this.ChangeColorSpace(ColorSpace.RGB);
			}
			else
			{
				byte[,] numArray = new byte[this.width, this.height];
				byte[,] numArray1 = new byte[this.width, this.height];
				for (int m = 0; m < this.width; m++)
				{
					for (int n = 0; n < this.height; n++)
					{
						numArray[m, n] = 128;
						numArray1[m, n] = 128;
					}
				}
				byte[][,] numArray2 = new byte[][,] { this._raster[0], numArray, numArray1 };
				this._raster = numArray2;
				this._cm.colorspace = ColorSpace.YCbCr;
			}
			return this;
		}

		public static byte[][,] CreateRaster(int width, int height, int bands)
		{
			byte[][,] numArray = new byte[bands][,];
			for (int i = 0; i < bands; i++)
			{
				numArray[i] = new byte[width, height];
			}
			return numArray;
		}
	}
}