﻿namespace DDDD.Core.Imaging
{

    /// <summary>
    ///  Base class for EncodingProperties in <see cref="T:DDDD.Core.Imaging.IImageEncoder">the IImageEncoder interface</see>/&gt;
    /// </summary>
    public abstract class EncodingProperty
    {
        protected EncodingProperty()
        {
        }
    }

}
