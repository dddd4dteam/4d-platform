using System;

namespace DDDD.Core.Imaging
{
	internal struct ColorModel
	{
		public ColorSpace colorspace;

		public bool Opaque;
	}
}