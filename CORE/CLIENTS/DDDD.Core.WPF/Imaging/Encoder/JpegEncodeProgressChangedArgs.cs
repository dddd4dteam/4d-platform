using System;

namespace DDDD.Core.Imaging.Encoder
{
	internal class JpegEncodeProgressChangedArgs : EventArgs
	{
		public double EncodeProgress;

		public JpegEncodeProgressChangedArgs()
		{
		}
	}
}