﻿using System.Collections.Generic;
using System.IO;
using System.Windows.Media.Imaging;

namespace DDDD.Core.Imaging.Encoder
{

    /// <summary>
    /// An interface used to encode images. The implementations are in the Imaging assembly.
    /// </summary>
    public interface IImageEncoder
    {
        /// <summary>
        /// Gets the image format supported by the encoder.
        /// </summary>
        /// <value>The image format.</value>
        ImageFormat ImageFormat
        {
            get;
        }

        /// <summary>
        /// Encodes the specified image in the stream.
        /// </summary>
        /// <param name="image">The image.</param>
        /// <param name="encodingProperties">A list of encoding properties. Each encoder provides subclasses of EncodingProperty with their specific ones. If it's null, default values will be used. </param>
        /// <param name="stream">The stream where the image will be serialized to</param>
        void Encode(BitmapSource image, IList<EncodingProperty> encodingProperties, Stream stream);
    }
}
