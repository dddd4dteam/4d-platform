using System;
using System.IO;
using System.Windows.Media.Imaging;

namespace DDDD.Core.Imaging.Encoder
{
	public class PngEncoder
	{
		private const int _ADLER32_BASE = 65521;

		private const int _MAXBLOCK = 65535;

		private static byte[] _HEADER;

		private static byte[] _IHDR;

		private static byte[] _IDAT;

		private static byte[] _IEND;

		private static byte[] _ARGB;

		private static uint[] _crcTable;

		private static bool _crcTableComputed;

		static PngEncoder()
		{
			PngEncoder._HEADER = new byte[] { 137, 80, 78, 71, 13, 10, 26, 10 };
			PngEncoder._IHDR = new byte[] { 73, 72, 68, 82 };
			PngEncoder._IDAT = new byte[] { 73, 68, 65, 84 };
			PngEncoder._IEND = new byte[] { 73, 69, 78, 68 };
			byte[] numArray = new byte[13];
			numArray[8] = 8;
			numArray[9] = 6;
			PngEncoder._ARGB = numArray;
			PngEncoder._crcTable = new uint[256];
			PngEncoder._crcTableComputed = false;
		}

		public PngEncoder()
		{
		}

		private static uint ComputeAdler32(byte[] buf)
		{
			uint num = 1;
			uint num1 = 0;
			int length = (int)buf.Length;
			for (int i = 0; i < length; i++)
			{
				num = (num + buf[i]) % 65521;
				num1 = (num1 + num) % 65521;
			}
			return (num1 << 16) + num;
		}

		public static Stream Encode(byte[] data, int width, int height)
		{
			MemoryStream memoryStream = new MemoryStream();
			PngEncoder.Encode(memoryStream, data, width, height);
			return memoryStream;
		}

		internal static void Encode(Stream stream, WriteableBitmap writeableBitmap)
		{
			var bitmapPixels = WritableBitmapEx.GetPixels(writeableBitmap);

			using (MemoryStream memoryStream = new MemoryStream())
			{
				BinaryWriter binaryWriter = new BinaryWriter(memoryStream);
				int pixelWidth = 0;
				for (int i = 0; i < writeableBitmap.PixelHeight; i++)
				{
					binaryWriter.Write((byte)0);
					for (int j = 0; j < writeableBitmap.PixelWidth; j++)
					{

						int pixels = bitmapPixels[pixelWidth + j];//  - only for Silverlight - writeableBitmap. Pixels[pixelWidth + j];

						byte num = (byte)(pixels >> 24);
						byte num1 = (byte)(pixels >> 16);
						byte num2 = (byte)(pixels >> 8);
						byte num3 = (byte)pixels;
						binaryWriter.Write(num << 24 | num3 << 16 | num2 << 8 | num1);
					}
					pixelWidth = pixelWidth + writeableBitmap.PixelWidth;
				}
				PngEncoder.Encode(stream, memoryStream.GetBuffer(), writeableBitmap.PixelWidth, writeableBitmap.PixelHeight);
			}
		}

		internal static void Encode(Stream stream, byte[] data, int width, int height)
		{ 
			// Version from Jet Brains      [12/21/2016 A1]
			stream.Write(PngEncoder._HEADER, 0, PngEncoder._HEADER.Length);
			byte[] bytes1 = BitConverter.GetBytes(width);
			PngEncoder._ARGB[0] = bytes1[3];
			PngEncoder._ARGB[1] = bytes1[2];
			PngEncoder._ARGB[2] = bytes1[1];
			PngEncoder._ARGB[3] = bytes1[0];
			byte[] bytes2 = BitConverter.GetBytes(height);
			PngEncoder._ARGB[4] = bytes2[3];
			PngEncoder._ARGB[5] = bytes2[2];
			PngEncoder._ARGB[6] = bytes2[1];
			PngEncoder._ARGB[7] = bytes2[0];
			PngEncoder.WriteChunk(stream, PngEncoder._IHDR, PngEncoder._ARGB);
			uint num1 = (uint)(width * 4 + 1);
			uint num2 = num1 * (uint)height;
			uint adler32 = PngEncoder.ComputeAdler32(data);
			MemoryStream memoryStream = new MemoryStream();
			uint num3 = (uint)ushort.MaxValue / num1 * num1;
			uint num4 = num2;
			uint num5 = (int)(num2 % num3) != 0 ? num2 / num3 + 1U : num2 / num3;
			memoryStream.WriteByte((byte)120);
			memoryStream.WriteByte((byte)218);
			for (uint index = 0; index < num5; ++index)
			{
				ushort num6 = num4 < num3 ? (ushort)num4 : (ushort)num3;
				if ((int)num6 == (int)num4)
					memoryStream.WriteByte((byte)1);
				else
					memoryStream.WriteByte((byte)0);
				memoryStream.Write(BitConverter.GetBytes(num6), 0, 2);
				memoryStream.Write(BitConverter.GetBytes(~num6), 0, 2);
				memoryStream.Write(data, (int)index * (int)num3, (int)num6);
				num4 -= num3;
			}
			PngEncoder.WriteReversedBuffer((Stream)memoryStream, BitConverter.GetBytes(adler32));
			memoryStream.Seek(0L, SeekOrigin.Begin);
			byte[] numArray = new byte[memoryStream.Length];
			memoryStream.Read(numArray, 0, (int)memoryStream.Length);
			PngEncoder.WriteChunk(stream, PngEncoder._IDAT, numArray);
			PngEncoder.WriteChunk(stream, PngEncoder._IEND, new byte[0]);
			stream.Seek(0L, SeekOrigin.Begin);


		}

		private static uint GetCRC(byte[] buf)
		{
			// Version from Jet Brains    [12/21/2016 A1]
			return PngEncoder.UpdateCRC(uint.MaxValue, buf, buf.Length) ^ uint.MaxValue;
		}

		private static void MakeCRCTable()
		{			
			for (int index1 = 0; index1 < 256; ++index1)
			{
				uint num = (uint)index1;
				for (int index2 = 0; index2 < 8; ++index2)
				{
					if ((num & 1U) > 0U)
						num = 3988292384U ^ num >> 1;
					else
						num >>= 1;
				}
				PngEncoder._crcTable[index1] = num;
			}
			PngEncoder._crcTableComputed = true;
		}

		private static uint UpdateCRC(uint crc, byte[] buf, int len)
		{ 
			// Version from Jet brains   [12/21/2016 A1]
			uint num = crc;
			if (!PngEncoder._crcTableComputed)
				PngEncoder.MakeCRCTable();
			for (int index = 0; index < len; ++index)
				num = PngEncoder._crcTable[(uint)(((int)num ^ (int)buf[index]) & (int)byte.MaxValue)] ^ num >> 8; /// no (IntPrt) in index - [12/21/2016 A1]
			return num;

		 }

		private static void WriteChunk(Stream stream, byte[] type, byte[] data)
		{
			int i;
			int length = (int)type.Length;
			byte[] numArray = new byte[(int)type.Length + (int)data.Length];
			for (i = 0; i < (int)type.Length; i++)
			{
				numArray[i] = type[i];
			}
			for (i = 0; i < (int)data.Length; i++)
			{
				numArray[i + length] = data[i];
			}
			PngEncoder.WriteReversedBuffer(stream, BitConverter.GetBytes((int)data.Length));
			stream.Write(numArray, 0, (int)numArray.Length);
			PngEncoder.WriteReversedBuffer(stream, BitConverter.GetBytes(PngEncoder.GetCRC(numArray)));
		}

		private static void WriteReversedBuffer(Stream stream, byte[] data)
		{
			int length = (int)data.Length;
			byte[] numArray = new byte[length];
			for (int i = 0; i < length; i++)
			{
				numArray[i] = data[length - i - 1];
			}
			stream.Write(numArray, 0, length);
		}
	}
}