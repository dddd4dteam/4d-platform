﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DDDD.Core.Imaging
{


	public class WritableBitmapEx
	{	 
		
	  /// <summary>
	  ///  Get Pixels of [bitmap]  with different .Net targets corrections. 
	  /// </summary>
	  /// <param name="bitmap"></param>
	  /// <returns></returns>
	  public static int[]  GetPixels(WriteableBitmap  bitmap)
	  {
			int pixelWidth = bitmap.PixelWidth;
			int pixelHeight = bitmap.PixelHeight;
#if CLIENT && SL5
			int[] pixels = bitmap.Pixels;
			// Adoption for  WPF ///           
			// int stride =  bitmap.PixelWidth * ( bitmap.Format.BitsPerPixel / 8) + ( bitmap.PixelWidth % 4);
			
#elif CLIENT && WPF
			int stride = bitmap.BackBufferStride;
			int[] pixels = new int[pixelHeight * stride];
			bitmap.CopyPixels(pixels, stride, 0);
#endif
			return pixels;
	  }


	}
}
