﻿namespace DDDD.Core.Imaging.Decoder
{
	internal enum BlockUpsamplingMode
	{
		/// <summary> The simplest upsampling mode. Produces sharper edges. </summary>
		BoxFilter,
		/// <summary> Smoother upsampling. May improve color spread for some images. </summary>
		Interpolate
	}
}