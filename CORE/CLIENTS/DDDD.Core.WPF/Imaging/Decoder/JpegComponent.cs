
using System;
using System.Collections.Generic;
using System.Reflection.Emit;

using DDDD.Core.Imaging.IO;

namespace DDDD.Core.Imaging.Decoder
{
	internal class JpegComponent
	{
		public byte factorH;

		public byte factorV;

		public byte component_id;

		public byte quant_id;

		public int width;

		public int height;

		public HuffmanTable ACTable;

		public HuffmanTable DCTable;

		private int[] quantizationTable;

		public float previousDC;

		private JpegScan parent;

		private float[,][] scanMCUs;

		private List<float[,][]> scanData = new List<float[,][]>();

		private List<byte[,]> scanDecoded = new List<byte[,]>();

		public int spectralStart;

		public int spectralEnd;

		public int successiveLow;

		private JpegComponent.QuantizeDel _quant;

		private DCT _dct = new DCT();

		public JpegComponent.DecodeFunction Decode;

		public int BlockCount
		{
			get
			{
				return this.scanData.Count;
			}
		}

		private int factorUpH
		{
			get
			{
				return this.parent.MaxH / this.factorH;
			}
		}

		private int factorUpV
		{
			get
			{
				return this.parent.MaxV / this.factorV;
			}
		}

		public int[] QuantizationTable
		{
			set
			{
				this.quantizationTable = value;
				this._quant = this.EmitQuantize();
			}
		}

		public JpegComponent(JpegScan parentScan, byte id, byte factorHorizontal, byte factorVertical, byte quantizationID, byte colorMode)
		{
			this.parent = parentScan;
			if (colorMode == JPEGFrame.JPEG_COLOR_YCbCr)
			{
				if (id != 1)
				{
					this.ACTable = new HuffmanTable(JpegHuffmanTable.StdACChrominance);
					this.DCTable = new HuffmanTable(JpegHuffmanTable.StdACLuminance);
				}
				else
				{
					this.ACTable = new HuffmanTable(JpegHuffmanTable.StdACLuminance);
					this.DCTable = new HuffmanTable(JpegHuffmanTable.StdDCLuminance);
				}
			}
			this.component_id = id;
			this.factorH = factorHorizontal;
			this.factorV = factorVertical;
			this.quant_id = quantizationID;
		}

		/// <summary>
		/// Generated from text on F-23, F.13 - Huffman decoded of AC coefficients
		/// on ISO DIS 10918-1. Requirements and Guidelines.
		/// </summary>
		internal void decode_ac_coefficients(JPEGBinaryReader JPEGStream, float[] zz)
		{
			for (int i = 1; i < 64; i++)
			{
				int num = this.ACTable.Decode(JPEGStream);
				int num1 = num >> 4;
				num = num & 15;
				if (num == 0)
				{
					if (num1 != 15)
					{
						return;
					}
					i = i + 15;
				}
				else
				{
					i = i + num1;
					num1 = JPEGStream.ReadBits(num);
					num = HuffmanTable.Extend(num1, num);
					zz[i] = (float)num;
				}
			}
		}

		/// <summary>
		/// Generated from text on F-22, F.2.2.1 - Huffman decoding of DC
		/// coefficients on ISO DIS 10918-1. Requirements and Guidelines.
		/// </summary>
		/// <param name="JPEGStream">Stream that contains huffman bits</param>
		/// <returns>DC coefficient</returns>
		public float decode_dc_coefficient(JPEGBinaryReader JPEGStream)
		{
			int num = this.DCTable.Decode(JPEGStream);
			float single = (float)JPEGStream.ReadBits(num);
			single = (float)HuffmanTable.Extend((int)single, num);
			single = this.previousDC + single;
			this.previousDC = single;
			return single;
		}

		public void DecodeACFirst(JPEGBinaryReader stream, float[] zz)
		{
			if (stream.eob_run > 0)
			{
				JPEGBinaryReader eobRun = stream;
				eobRun.eob_run = eobRun.eob_run - 1;
				return;
			}
			for (int i = this.spectralStart; i <= this.spectralEnd; i++)
			{
				int num = this.ACTable.Decode(stream);
				int num1 = num >> 4;
				num = num & 15;
				if (num == 0)
				{
					if (num1 != 15)
					{
						stream.eob_run = 1 << (num1 & 31);
						if (num1 != 0)
						{
							JPEGBinaryReader jPEGBinaryReader = stream;
							jPEGBinaryReader.eob_run = jPEGBinaryReader.eob_run + stream.ReadBits(num1);
						}
						JPEGBinaryReader eobRun1 = stream;
						eobRun1.eob_run = eobRun1.eob_run - 1;
						return;
					}
					i = i + 15;
				}
				else
				{
					i = i + num1;
					num1 = stream.ReadBits(num);
					num = HuffmanTable.Extend(num1, num);
					zz[i] = (float)(num << (this.successiveLow & 31));
				}
			}
		}

		public void DecodeACRefine(JPEGBinaryReader stream, float[] dest)
		{
			int num = 1 << (this.successiveLow & 31);
			int num1 = -1 << (this.successiveLow & 31);
			int num2 = this.spectralStart;
			if (stream.eob_run == 0)
			{
				while (num2 <= this.spectralEnd)
				{
					int num3 = this.ACTable.Decode(stream);
					int num4 = num3 >> 4;
					num3 = num3 & 15;
					if (num3 != 0)
					{
						if (num3 != 1)
						{
							throw new Exception("Decode Error");
						}
						num3 = (stream.ReadBits(1) != 1 ? num1 : num);
					}
					else if (num4 != 15)
					{
						stream.eob_run = 1 << (num4 & 31);
						if (num4 <= 0)
						{
							break;
						}
						JPEGBinaryReader eobRun = stream;
						eobRun.eob_run = eobRun.eob_run + stream.ReadBits(num4);
						break;
					}
					do
					{
						if (dest[num2] == 0f)
						{
							int num5 = num4 - 1;
							num4 = num5;
							if (num5 < 0)
							{
								break;
							}
						}
						else if (stream.ReadBits(1) == 1 && ((int)dest[num2] & num) == 0)
						{
							if (dest[num2] < 0f)
							{
								dest[num2] = dest[num2] + (float)num1;
							}
							else
							{
								dest[num2] = dest[num2] + (float)num;
							}
						}
						num2++;
					}
					while (num2 <= this.spectralEnd);
					if (num3 != 0 && num2 < 64)
					{
						dest[num2] = (float)num3;
					}
					num2++;
				}
			}
			if (stream.eob_run > 0)
			{
				while (num2 <= this.spectralEnd)
				{
					if (dest[num2] != 0f && stream.ReadBits(1) == 1 && ((int)dest[num2] & num) == 0)
					{
						if (dest[num2] < 0f)
						{
							dest[num2] = dest[num2] + (float)num1;
						}
						else
						{
							dest[num2] = dest[num2] + (float)num;
						}
					}
					num2++;
				}
				JPEGBinaryReader jPEGBinaryReader = stream;
				jPEGBinaryReader.eob_run = jPEGBinaryReader.eob_run - 1;
			}
		}

		public void DecodeBaseline(JPEGBinaryReader stream, float[] dest)
		{
			float single = this.decode_dc_coefficient(stream);
			this.decode_ac_coefficients(stream, dest);
			dest[0] = single;
		}

		public void DecodeDCFirst(JPEGBinaryReader stream, float[] dest)
		{
			int num = this.DCTable.Decode(stream);
			num = HuffmanTable.Extend(stream.ReadBits(num), num);
			num = (int)this.previousDC + num;
			this.previousDC = (float)num;
			dest[0] = (float)(num << (this.successiveLow & 31));
		}

		public void DecodeDCRefine(JPEGBinaryReader stream, float[] dest)
		{
			if (stream.ReadBits(1) == 1)
			{
				dest[0] = (float)((int)dest[0] | 1 << (this.successiveLow & 31));
			}
		}

		public void DecodeMCU(JPEGBinaryReader jpegReader, int i, int j)
		{
			this.Decode(jpegReader, this.scanMCUs[i, j]);
		}

		private JpegComponent.QuantizeDel EmitQuantize()
		{
			Type[] typeArray = new Type[] { typeof(float[]) };
			DynamicMethod dynamicMethod = new DynamicMethod("Quantize", null, typeArray);
			ILGenerator lGenerator = dynamicMethod.GetILGenerator();
			for (int i = 0; i < (int)this.quantizationTable.Length; i++)
			{
				float single = (float)this.quantizationTable[i];
				lGenerator.Emit(OpCodes.Ldarg_0);
				lGenerator.Emit(OpCodes.Ldc_I4_S, (short)i);
				lGenerator.Emit(OpCodes.Ldarg_0);
				lGenerator.Emit(OpCodes.Ldc_I4_S, (short)i);
				lGenerator.Emit(OpCodes.Ldelem_R4);
				lGenerator.Emit(OpCodes.Ldc_R4, single);
				lGenerator.Emit(OpCodes.Mul);
				lGenerator.Emit(OpCodes.Stelem_R4);
			}
			lGenerator.Emit(OpCodes.Ret);
			return (JpegComponent.QuantizeDel)dynamicMethod.CreateDelegate(typeof(JpegComponent.QuantizeDel));
		}

		/// <summary>
		/// Run the Inverse DCT method on all of the block data
		/// </summary>
		public void idctData()
		{
			float[] singleArray = new float[64];
			float[] item = null;
			for (int i = 0; i < this.scanData.Count; i++)
			{
				for (int j = 0; j < this.factorV; j++)
				{
					for (int k = 0; k < this.factorH; k++)
					{
						item = this.scanData[i][k, j];
						ZigZag.UnZigZag(item, singleArray);
						this.scanDecoded.Add(this._dct.FastIDCT(singleArray));
					}
				}
			}
		}

		/// <summary>
		/// If a restart marker is found with too little of an MCU count (i.e. our
		/// Restart Interval is 63 and we have 61 we copy the last MCU until it's full)
		/// </summary>
		public void padMCU(int index, int length)
		{
			this.scanMCUs = new float[this.factorH, this.factorV][];
			for (int i = 0; i < length; i++)
			{
				if (this.scanData.Count < index + length)
				{
					for (int j = 0; j < this.factorH; j++)
					{
						for (int k = 0; k < this.factorV; k++)
						{
							this.scanMCUs[j, k] = (float[])this.scanData[index - 1][j, k].Clone();
						}
					}
					this.scanData.Add(this.scanMCUs);
				}
			}
		}

		/// <summary>
		/// Run the Quantization backward method on all of the block data.
		/// </summary>
		public void quantizeData()
		{
			for (int i = 0; i < this.scanData.Count; i++)
			{
				for (int j = 0; j < this.factorV; j++)
				{
					for (int k = 0; k < this.factorH; k++)
					{
						this._quant(this.scanData[i][k, j]);
					}
				}
			}
		}

		/// <summary>
		/// Reset the interval by setting the previous DC value
		/// </summary>
		public void resetInterval()
		{
			this.previousDC = 0f;
		}

		/// <summary>
		/// Stretches components as needed to normalize the size of all components.
		/// For example, in a 2x1 (4:2:2) sequence, the Cr and Cb channels will be 
		/// scaled vertically by a factor of 2.
		/// </summary>
		public void scaleByFactors(BlockUpsamplingMode mode)
		{
			int num = this.factorUpV;
			int num1 = this.factorUpH;
			if (num == 1 && num1 == 1)
			{
				return;
			}
			for (int i = 0; i < this.scanDecoded.Count; i++)
			{
				byte[,] item = this.scanDecoded[i];
				int length = item.GetLength(0);
				int length1 = item.GetLength(1);
				int num2 = length * num;
				int num3 = length1 * num1;
				byte[,] numArray = new byte[num2, num3];
				switch (mode)
				{
					case BlockUpsamplingMode.BoxFilter:
					{
						for (int j = 0; j < num3; j++)
						{
							int num4 = j / num1;
							for (int k = 0; k < num2; k++)
							{
								int num5 = k / num;
								numArray[k, j] = item[num5, num4];
							}
						}
						break;
					}
					case BlockUpsamplingMode.Interpolate:
					{
						for (int l = 0; l < num3; l++)
						{
							for (int m = 0; m < num2; m++)
							{
								int num6 = 0;
								for (int n = 0; n < num1; n++)
								{
									int num7 = (l + n) / num1;
									if (num7 >= length1)
									{
										num7 = length1 - 1;
									}
									for (int o = 0; o < num; o++)
									{
										int num8 = (m + o) / num;
										if (num8 >= length)
										{
											num8 = length - 1;
										}
										num6 = num6 + item[num8, num7];
									}
								}
								numArray[m, l] = (byte)(num6 / (num1 * num));
							}
						}
						break;
					}
					default:
					{
						throw new ArgumentException("Upsampling mode not supported.");
					}
				}
				this.scanDecoded[i] = numArray;
			}
		}

		public void setACTable(JpegHuffmanTable table)
		{
			this.ACTable = new HuffmanTable(table);
		}

		public void SetBlock(int idx)
		{
			if (this.scanData.Count < idx)
			{
				throw new Exception("Invalid block ID.");
			}
			if (this.scanData.Count != idx)
			{
				this.scanMCUs = this.scanData[idx];
				return;
			}
			this.scanMCUs = new float[this.factorH, this.factorV][];
			for (int i = 0; i < this.factorH; i++)
			{
				for (int j = 0; j < this.factorV; j++)
				{
					this.scanMCUs[i, j] = new float[64];
				}
			}
			this.scanData.Add(this.scanMCUs);
		}

		public void setDCTable(JpegHuffmanTable table)
		{
			this.DCTable = new HuffmanTable(table);
		}

		public void writeBlock(byte[][,] raster, byte[,] data, int compIndex, int x, int y)
		{
			int length = raster[0].GetLength(0);
			int num = raster[0].GetLength(1);
			byte[,] numArray = raster[compIndex];
			int length1 = data.GetLength(0);
			if (y + length1 > num)
			{
				length1 = num - y;
			}
			int num1 = data.GetLength(1);
			if (x + num1 > length)
			{
				num1 = length - x;
			}
			for (int i = 0; i < length1; i++)
			{
				for (int j = 0; j < num1; j++)
				{
					numArray[x + j, y + i] = data[i, j];
				}
			}
		}

		private void writeBlockScaled(byte[][,] raster, byte[,] blockdata, int compIndex, int x, int y, BlockUpsamplingMode mode)
		{
			int length = raster[0].GetLength(0);
			int num = raster[0].GetLength(1);
			int num1 = this.factorUpV;
			int num2 = this.factorUpH;
			int length1 = blockdata.GetLength(0);
			int length2 = blockdata.GetLength(1);
			int num3 = length1 * num1;
			int num4 = length2 * num2;
			byte[,] numArray = raster[compIndex];
			int num5 = num3;
			if (y + num5 > num)
			{
				num5 = num - y;
			}
			int num6 = num4;
			if (x + num6 > length)
			{
				num6 = length - x;
			}
			if (mode != BlockUpsamplingMode.BoxFilter)
			{
				throw new ArgumentException("Upsampling mode not supported.");
			}
			if (num1 == 1 && num2 == 1)
			{
				for (int i = 0; i < num6; i++)
				{
					for (int j = 0; j < num5; j++)
					{
						numArray[i + x, y + j] = blockdata[j, i];
					}
				}
				return;
			}
			if (num2 != 2 || num1 != 2 || num6 != num4 || num5 != num3)
			{
				for (int k = 0; k < num6; k++)
				{
					int num7 = k / num2;
					for (int l = 0; l < num5; l++)
					{
						int num8 = l / num1;
						numArray[k + x, y + l] = blockdata[num8, num7];
					}
				}
				return;
			}
			for (int m = 0; m < length2; m++)
			{
				int num9 = m * 2 + x;
				for (int n = 0; n < length1; n++)
				{
					byte num10 = blockdata[n, m];
					int num11 = n * 2 + y;
					numArray[num9, num11] = num10;
					numArray[num9, num11 + 1] = num10;
					numArray[num9 + 1, num11] = num10;
					numArray[num9 + 1, num11 + 1] = num10;
				}
			}
		}

		public void writeDataScaled(byte[][,] raster, int componentIndex, BlockUpsamplingMode mode)
		{
			int length = 0;
			int num = 0;
			int num1 = 0;
			int num2 = 0;
			int num3 = 0;
			int length1 = raster[0].GetLength(0);
			raster[0].GetLength(1);
			while (num3 < this.scanDecoded.Count)
			{
				int length2 = 0;
				int length3 = 0;
				if (length >= length1)
				{
					length = 0;
					num = num + num2;
				}
				for (int i = 0; i < this.factorV; i++)
				{
					length2 = 0;
					for (int j = 0; j < this.factorH; j++)
					{
						int num4 = num3;
						num3 = num4 + 1;
						byte[,] item = this.scanDecoded[num4];
						this.writeBlockScaled(raster, item, componentIndex, length, num, mode);
						length2 = length2 + item.GetLength(1) * this.factorUpH;
						length = length + item.GetLength(1) * this.factorUpH;
						length3 = item.GetLength(0) * this.factorUpV;
					}
					num = num + length3;
					length = length - length2;
					num1 = num1 + length3;
				}
				num = num - num1;
				num2 = num1;
				num1 = 0;
				length = length + length2;
			}
		}

		internal delegate void DecodeFunction(JPEGBinaryReader jpegReader, float[] zigzagMCU);

		private delegate void QuantizeDel(float[] arr);
	}
}