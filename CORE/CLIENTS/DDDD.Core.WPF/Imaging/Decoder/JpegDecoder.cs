
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using DDDD.Core.Imaging.IO;

namespace DDDD.Core.Imaging.Decoder
{
	internal class JpegDecoder
	{
		/// <summary>
		/// This decoder expects JFIF 1.02 encoding.
		/// </summary>
		internal const byte MAJOR_VERSION = 1;

		internal const byte MINOR_VERSION = 2;

		public static long ProgressUpdateByteInterval;

		private JpegDecodeProgressChangedArgs DecodeProgress = new JpegDecodeProgressChangedArgs();

		private byte majorVersion;

		private byte minorVersion;

		private JpegDecoder.UnitType Units;

		private ushort XDensity;

		private ushort YDensity;

		private byte Xthumbnail;

		private byte Ythumbnail;

		private byte[] thumbnail;

		private Image image;

		private int width;

		private int height;

		private bool progressive;

		private byte marker;

		/// <summary>
		/// The length of the JFIF field not including thumbnail data.
		/// </summary>
		internal static short JFIF_FIXED_LENGTH;

		/// <summary>
		/// The length of the JFIF extension field not including extension data.
		/// </summary>
		internal static short JFXX_FIXED_LENGTH;

		private JPEGBinaryReader jpegReader;

		private List<JPEGFrame> jpegFrames = new List<JPEGFrame>();

		private JpegHuffmanTable[] dcTables = new JpegHuffmanTable[4];

		private JpegHuffmanTable[] acTables = new JpegHuffmanTable[4];

		private JpegQuantizationTable[] qTables = new JpegQuantizationTable[4];

		public DDDD.Core.Imaging.Decoder.BlockUpsamplingMode BlockUpsamplingMode
		{
			get;
			set;
		}

		static JpegDecoder()
		{
			JpegDecoder.ProgressUpdateByteInterval = (long)100;
			JpegDecoder.JFIF_FIXED_LENGTH = 16;
			JpegDecoder.JFXX_FIXED_LENGTH = 8;
		}

		public JpegDecoder(Stream input)
		{
			this.jpegReader = new JPEGBinaryReader(input);
			if (this.jpegReader.GetNextMarker() != 216)
			{
				throw new Exception("Failed to find SOI marker.");
			}
		}

		public DecodedJpeg Decode()
		{
			JPEGFrame item = null;
			int num = 0;
			bool flag = false;
			bool flag1 = false;
			List<JpegHeader> jpegHeaders = new List<JpegHeader>();
			while (!this.DecodeProgress.Abort)
			{
				switch (this.marker)
				{
					case 192:
					case 194:
					{
						this.progressive = this.marker == 194;
						this.jpegFrames.Add(new JPEGFrame());
						item = this.jpegFrames[this.jpegFrames.Count - 1];
						item.ProgressUpdateMethod = new Action<long>(this.UpdateStreamProgress);
						this.jpegReader.ReadShort();
						item.setPrecision(this.jpegReader.ReadByte());
						item.ScanLines = this.jpegReader.ReadShort();
						item.SamplesPerLine = this.jpegReader.ReadShort();
						item.ComponentCount = this.jpegReader.ReadByte();
						this.DecodeProgress.Height = item.Height;
						this.DecodeProgress.Width = item.Width;
						this.DecodeProgress.SizeReady = true;
						if (this.DecodeProgressChanged != null)
						{
							this.DecodeProgressChanged(this, this.DecodeProgress);
							if (this.DecodeProgress.Abort)
							{
								return null;
							}
						}
						for (int i = 0; i < item.ComponentCount; i++)
						{
							byte num1 = this.jpegReader.ReadByte();
							byte num2 = this.jpegReader.ReadByte();
							byte num3 = this.jpegReader.ReadByte();
							byte num4 = (byte)(num2 >> 4);
							item.AddComponent(num1, num4, (byte)(num2 & 15), num3);
						}
						goto case 253;
					}
					case 193:
					case 195:
					case 197:
					case 198:
					case 199:
					case 201:
					case 202:
					case 203:
					case 205:
					case 206:
					case 207:
					{
						throw new NotSupportedException("Unsupported codec type.");
					}
					case 196:
					{
						int num5 = this.jpegReader.ReadShort() - 2;
						while (num5 > 0)
						{
							byte num6 = this.jpegReader.ReadByte();
							byte num7 = (byte)(num6 >> 4);
							byte num8 = (byte)(num6 & 15);
							short[] numArray = new short[16];
							for (int j = 0; j < (int)numArray.Length; j++)
							{
								numArray[j] = this.jpegReader.ReadByte();
							}
							int num9 = 0;
							for (int k = 0; k < 16; k++)
							{
								num9 = num9 + numArray[k];
							}
							num5 = num5 - (num9 + 17);
							short[] numArray1 = new short[num9];
							for (int l = 0; l < (int)numArray1.Length; l++)
							{
								numArray1[l] = this.jpegReader.ReadByte();
							}
							if (num7 != HuffmanTable.JPEG_DC_TABLE)
							{
								if (num7 != HuffmanTable.JPEG_AC_TABLE)
								{
									continue;
								}
								this.acTables[num8] = new JpegHuffmanTable(numArray, numArray1);
							}
							else
							{
								this.dcTables[num8] = new JpegHuffmanTable(numArray, numArray1);
							}
						}
						goto case 253;
					}
					case 200:
					case 204:
					case 208:
					case 209:
					case 210:
					case 211:
					case 212:
					case 213:
					case 214:
					case 215:
					case 216:
					case 222:
					case 223:
					case 240:
					case 241:
					case 242:
					case 243:
					case 244:
					case 245:
					case 246:
					case 247:
					case 248:
					case 249:
					case 250:
					case 251:
					case 252:
					case 253:
					{
						if (!flag)
						{
							try
							{
								this.marker = this.jpegReader.GetNextMarker();
								continue;
							}
							catch (EndOfStreamException endOfStreamException)
							{
							}
							return new DecodedJpeg(this.image, jpegHeaders);
						}
						else
						{
							flag = false;
							continue;
						}
					}
					case 217:
					{
						if (this.jpegFrames.Count == 0)
						{
							throw new NotSupportedException("No JPEG frames could be located.");
						}
						if (this.jpegFrames.Count != 1)
						{
							throw new NotSupportedException("Unsupported Codec Type: Hierarchial JPEG");
						}
						byte[][,] numArray2 = Image.CreateRaster((int)item.Width, (int)item.Height, (int)item.ComponentCount);
						IList<JpegComponent> components = item.Scan.Components;
						int count = components.Count * 3;
						int num10 = 0;
						for (int m = 0; m < components.Count; m++)
						{
							JpegComponent table = components[m];
							table.QuantizationTable = this.qTables[table.quant_id].Table;
							table.quantizeData();
							int num11 = num10 + 1;
							num10 = num11;
							this.UpdateProgress(num11, count);
							table.idctData();
							int num12 = num10 + 1;
							num10 = num12;
							this.UpdateProgress(num12, count);
							table.writeDataScaled(numArray2, m, this.BlockUpsamplingMode);
							int num13 = num10 + 1;
							num10 = num13;
							this.UpdateProgress(num13, count);
							table = null;
							GC.Collect();
						}
						if (item.ComponentCount != 1)
						{
							if (item.ComponentCount != 3)
							{
								throw new NotSupportedException("Unsupported Color Mode: 4 Component Color Mode found.");
							}
							ColorModel colorModel = new ColorModel()
							{
								colorspace = ColorSpace.YCbCr,
								Opaque = true
							};
							this.image = new Image(colorModel, numArray2);
						}
						else
						{
							ColorModel colorModel1 = new ColorModel()
							{
								colorspace = ColorSpace.Gray,
								Opaque = true
							};
							this.image = new Image(colorModel1, numArray2);
						}
						Func<double, double> units = (double x) => {
							if (this.Units == JpegDecoder.UnitType.Inches)
							{
								return x;
							}
							return x / 2.54;
						};
						this.image.DensityX = units((double)this.XDensity);
						this.image.DensityY = units((double)this.YDensity);
						this.height = item.Height;
						this.width = item.Width;
						goto case 253;
					}
					case 218:
					{
						this.jpegReader.ReadShort();
						byte num14 = this.jpegReader.ReadByte();
						byte[] numArray3 = new byte[num14];
						for (int n = 0; n < num14; n++)
						{
							byte num15 = this.jpegReader.ReadByte();
							byte num16 = this.jpegReader.ReadByte();
							int num17 = num16 >> 4 & 15;
							int num18 = num16 & 15;
							item.setHuffmanTables(num15, this.acTables[(byte)num18], this.dcTables[(byte)num17]);
							numArray3[n] = num15;
						}
						byte num19 = this.jpegReader.ReadByte();
						byte num20 = this.jpegReader.ReadByte();
						byte num21 = this.jpegReader.ReadByte();
						if (!this.progressive)
						{
							item.DecodeScanBaseline(num14, numArray3, num, this.jpegReader, ref this.marker);
							flag = true;
						}
						if (!this.progressive)
						{
							goto case 253;
						}
						item.DecodeScanProgressive(num21, num19, num20, num14, numArray3, num, this.jpegReader, ref this.marker);
						flag = true;
						goto case 253;
					}
					case 219:
					{
						short num22 = (short)(this.jpegReader.ReadShort() - 2);
						for (int o = 0; o < num22 / 65; o++)
						{
							byte num23 = this.jpegReader.ReadByte();
							int[] numArray4 = new int[64];
							if ((byte)(num23 >> 4) == 0)
							{
								for (int p = 0; p < 64; p++)
								{
									numArray4[p] = this.jpegReader.ReadByte();
								}
							}
							else if ((byte)(num23 >> 4) == 1)
							{
								for (int q = 0; q < 64; q++)
								{
									numArray4[q] = this.jpegReader.ReadShort();
								}
							}
							this.qTables[num23 & 15] = new JpegQuantizationTable(numArray4);
						}
						goto case 253;
					}
					case 220:
					{
						item.ScanLines = this.jpegReader.ReadShort();
						goto case 253;
					}
					case 221:
					{
						this.jpegReader.BaseStream.Seek((long)2, SeekOrigin.Current);
						num = this.jpegReader.ReadShort();
						goto case 253;
					}
					case 224:
					case 225:
					case 226:
					case 227:
					case 228:
					case 229:
					case 230:
					case 231:
					case 232:
					case 233:
					case 234:
					case 235:
					case 236:
					case 237:
					case 238:
					case 239:
					case 254:
					{
						JpegHeader jpegHeader = this.ExtractHeader();
						if (jpegHeader.Marker == 225 && (int)jpegHeader.Data.Length >= 6)
						{
							byte[] data = jpegHeader.Data;
							if (data[0] == 69 && data[1] == 120 && data[2] == 105 && data[3] == 102 && data[4] == 0)
							{
								byte num24 = data[5];
							}
						}
						if ((int)jpegHeader.Data.Length >= 5 && jpegHeader.Marker == 238)
						{
							string str = Encoding.UTF8.GetString(jpegHeader.Data, 0, 5);
						}
						jpegHeaders.Add(jpegHeader);
						if (flag1 || this.marker != 224)
						{
							goto case 253;
						}
						flag1 = this.TryParseJFIF(jpegHeader.Data);
						if (!flag1)
						{
							goto case 253;
						}
						jpegHeader.IsJFIF = true;
						this.marker = this.jpegReader.GetNextMarker();
						if (this.marker != 224)
						{
							flag = true;
							goto case 253;
						}
						else
						{
							jpegHeader = this.ExtractHeader();
							jpegHeaders.Add(jpegHeader);
							goto case 253;
						}
					}
					default:
					{
						goto case 253;
					}
				}
			}
			return null;
		}

		private JpegHeader ExtractHeader()
		{
			int num = this.jpegReader.ReadShort() - 2;
			byte[] numArray = new byte[num];
			this.jpegReader.Read(numArray, 0, num);
			JpegHeader jpegHeader = new JpegHeader()
			{
				Marker = this.marker,
				Data = numArray
			};
			return jpegHeader;
		}

		/// <summary>
		/// Tries to parse the JFIF APP0 header
		/// See http://en.wikipedia.org/wiki/JFIF
		/// </summary>
		private bool TryParseJFIF(byte[] data)
		{
			DDDD.Core.Imaging.IO.BinaryReader binaryReader = new DDDD.Core.Imaging.IO.BinaryReader(new MemoryStream(data));
			int length = (int)data.Length + 2;
			if (length < JpegDecoder.JFIF_FIXED_LENGTH)
			{
				return false;
			}
			byte[] numArray = new byte[5];
			binaryReader.Read(numArray, 0, (int)numArray.Length);
			if (numArray[0] != 74 || numArray[1] != 70 || numArray[2] != 73 || numArray[3] != 70 || numArray[4] != 0)
			{
				return false;
			}
			this.majorVersion = binaryReader.ReadByte();
			this.minorVersion = binaryReader.ReadByte();
			if (this.majorVersion != 1 || this.majorVersion == 1 && this.minorVersion > 2)
			{
				return false;
			}
			this.Units = (JpegDecoder.UnitType)binaryReader.ReadByte();
			if (this.Units != JpegDecoder.UnitType.None && this.Units != JpegDecoder.UnitType.Inches && this.Units != JpegDecoder.UnitType.Centimeters)
			{
				return false;
			}
			this.XDensity = binaryReader.ReadShort();
			this.YDensity = binaryReader.ReadShort();
			this.Xthumbnail = binaryReader.ReadByte();
			this.Ythumbnail = binaryReader.ReadByte();
			int xthumbnail = 3 * this.Xthumbnail * this.Ythumbnail;
			if (length > JpegDecoder.JFIF_FIXED_LENGTH && xthumbnail != length - JpegDecoder.JFIF_FIXED_LENGTH)
			{
				return false;
			}
			if (xthumbnail > 0)
			{
				this.thumbnail = new byte[xthumbnail];
				if (binaryReader.Read(this.thumbnail, 0, xthumbnail) != xthumbnail)
				{
					return false;
				}
			}
			return true;
		}

		private void UpdateProgress(int stepsFinished, int stepsTotal)
		{
			if (this.DecodeProgressChanged != null)
			{
				this.DecodeProgress.DecodeProgress = (double)stepsFinished / (double)stepsTotal;
				this.DecodeProgressChanged(this, this.DecodeProgress);
			}
		}

		private void UpdateStreamProgress(long StreamPosition)
		{
			if (this.DecodeProgressChanged != null)
			{
				this.DecodeProgress.ReadPosition = StreamPosition;
				this.DecodeProgressChanged(this, this.DecodeProgress);
			}
		}

		public event EventHandler<JpegDecodeProgressChangedArgs> DecodeProgressChanged;

		private enum UnitType
		{
			None,
			Inches,
			Centimeters
		}
	}
}