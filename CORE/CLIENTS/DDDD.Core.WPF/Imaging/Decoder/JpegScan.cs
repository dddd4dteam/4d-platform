using System.Collections.Generic;
using System.Linq;

namespace DDDD.Core.Imaging.Decoder
{
	internal class JpegScan
	{
		private List<JpegComponent> components = new List<JpegComponent>();

		private int maxV;

		private int maxH;

		public IList<JpegComponent> Components
		{
			get
			{
				return this.components.AsReadOnly();
			}
		}

		internal int MaxH
		{
			get
			{
				return this.maxH;
			}
		}

		internal int MaxV
		{
			get
			{
				return this.maxV;
			}
		}

		public JpegScan()
		{
		}

		public void AddComponent(byte id, byte factorHorizontal, byte factorVertical, byte quantizationID, byte colorMode)
		{
			JpegComponent jpegComponent = new JpegComponent(this, id, factorHorizontal, factorVertical, quantizationID, colorMode);
			this.components.Add(jpegComponent);
			this.maxH = this.components.Max<JpegComponent, byte>((JpegComponent x) => x.factorH);
			this.maxV = this.components.Max<JpegComponent, byte>((JpegComponent x) => x.factorV);
		}

		public JpegComponent GetComponentById(byte Id)
		{
			return this.components.First<JpegComponent>((JpegComponent x) => x.component_id == Id);
		}
	}
}