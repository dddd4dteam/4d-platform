
using System;
using DDDD.Core.Imaging.IO;

namespace DDDD.Core.Imaging.Decoder
{
	internal class JPEGFrame
	{
		public static byte JPEG_COLOR_GRAY;

		public static byte JPEG_COLOR_RGB;

		public static byte JPEG_COLOR_YCbCr;

		public static byte JPEG_COLOR_CMYK;

		public byte precision = 8;

		public byte colorMode = JPEGFrame.JPEG_COLOR_YCbCr;

		public JpegScan Scan = new JpegScan();

		public Action<long> ProgressUpdateMethod;

		public byte ColorMode
		{
			get
			{
				if (this.ComponentCount != 1)
				{
					return JPEGFrame.JPEG_COLOR_YCbCr;
				}
				return JPEGFrame.JPEG_COLOR_GRAY;
			}
		}

		public byte ComponentCount
		{
			get;
			set;
		}

		public ushort Height
		{
			get;
			private set;
		}

		public ushort SamplesPerLine
		{
			set
			{
				this.Width = value;
			}
		}

		public ushort ScanLines
		{
			set
			{
				this.Height = value;
			}
		}

		public ushort Width
		{
			get;
			private set;
		}

		static JPEGFrame()
		{
			JPEGFrame.JPEG_COLOR_GRAY = 1;
			JPEGFrame.JPEG_COLOR_RGB = 2;
			JPEGFrame.JPEG_COLOR_YCbCr = 3;
			JPEGFrame.JPEG_COLOR_CMYK = 4;
		}

		public JPEGFrame()
		{
		}

		public void AddComponent(byte componentID, byte sampleHFactor, byte sampleVFactor, byte quantizationTableID)
		{
			this.Scan.AddComponent(componentID, sampleHFactor, sampleVFactor, quantizationTableID, this.colorMode);
		}

		private void DecodeScan(byte numberOfComponents, byte[] componentSelector, int resetInterval, JPEGBinaryReader jpegReader, ref byte marker)
		{
			jpegReader.eob_run = 0;
			int num = 0;
			int num1 = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			long position = jpegReader.BaseStream.Position;
			while (true)
			{
				if (this.ProgressUpdateMethod != null && jpegReader.BaseStream.Position >= position + JpegDecoder.ProgressUpdateByteInterval)
				{
					position = jpegReader.BaseStream.Position;
					this.ProgressUpdateMethod(position);
				}
				try
				{
					if (numberOfComponents != 1)
					{
						for (int i = 0; i < numberOfComponents; i++)
						{
							JpegComponent componentById = this.Scan.GetComponentById(componentSelector[i]);
							componentById.SetBlock(num1);
							for (int j = 0; j < componentById.factorV; j++)
							{
								for (int k = 0; k < componentById.factorH; k++)
								{
									componentById.DecodeMCU(jpegReader, k, j);
								}
							}
						}
						num++;
						num1++;
					}
					else
					{
						JpegComponent jpegComponent = this.Scan.GetComponentById(componentSelector[0]);
						jpegComponent.SetBlock(num);
						jpegComponent.DecodeMCU(jpegReader, num2, num3);
						int num5 = this.mcus_per_row(jpegComponent);
						int num6 = (int)Math.Ceiling((double)this.Width / (double)(8 * jpegComponent.factorH));
						num2++;
						num4++;
						if (num2 == jpegComponent.factorH)
						{
							num2 = 0;
							num++;
						}
						if (num4 % num5 == 0)
						{
							num4 = 0;
							num3++;
							if (num3 != jpegComponent.factorV)
							{
								num = num - num6;
								if (num2 != 0)
								{
									num++;
									num2 = 0;
								}
							}
							else
							{
								if (num2 != 0)
								{
									num++;
									num2 = 0;
								}
								num3 = 0;
							}
						}
					}
				}
				catch (JPEGMarkerFoundException jPEGMarkerFoundException)
				{
					marker = jPEGMarkerFoundException.Marker;
					if (marker == 208 || marker == 209 || marker == 210 || marker == 211 || marker == 212 || marker == 213 || marker == 214 || marker == 215)
					{
						for (int l = 0; l < numberOfComponents; l++)
						{
							JpegComponent componentById1 = this.Scan.GetComponentById(componentSelector[l]);
							if (l > 1)
							{
								componentById1.padMCU(num1, resetInterval - num);
							}
							componentById1.resetInterval();
						}
						num1 = num1 + (resetInterval - num);
						num = 0;
					}
					else
					{
						break;
					}
				}
			}
		}

		public void DecodeScanBaseline(byte numberOfComponents, byte[] componentSelector, int resetInterval, JPEGBinaryReader jpegReader, ref byte marker)
		{
			for (int i = 0; i < numberOfComponents; i++)
			{
				JpegComponent componentById = this.Scan.GetComponentById(componentSelector[i]);
				componentById.Decode = new JpegComponent.DecodeFunction(componentById.DecodeBaseline);
			}
			this.DecodeScan(numberOfComponents, componentSelector, resetInterval, jpegReader, ref marker);
		}

		public void DecodeScanProgressive(byte successiveApproximation, byte startSpectralSelection, byte endSpectralSelection, byte numberOfComponents, byte[] componentSelector, int resetInterval, JPEGBinaryReader jpegReader, ref byte marker)
		{
			byte num = (byte)(successiveApproximation >> 4);
			byte num1 = (byte)(successiveApproximation & 15);
			if (startSpectralSelection > endSpectralSelection || endSpectralSelection > 63)
			{
				throw new Exception("Bad spectral selection.");
			}
			bool flag = startSpectralSelection == 0;
			bool flag1 = num != 0;
			if (flag)
			{
				if (endSpectralSelection != 0)
				{
					throw new Exception("Bad spectral selection for DC only scan.");
				}
			}
			else if (numberOfComponents > 1)
			{
				throw new Exception("Too many components for AC scan!");
			}
			for (int i = 0; i < numberOfComponents; i++)
			{
				JpegComponent componentById = this.Scan.GetComponentById(componentSelector[i]);
				componentById.successiveLow = num1;
				if (!flag)
				{
					componentById.spectralStart = startSpectralSelection;
					componentById.spectralEnd = endSpectralSelection;
					if (!flag1)
					{
						componentById.Decode = new JpegComponent.DecodeFunction(componentById.DecodeACFirst);
					}
					else
					{
						componentById.Decode = new JpegComponent.DecodeFunction(componentById.DecodeACRefine);
					}
				}
				else if (!flag1)
				{
					componentById.Decode = new JpegComponent.DecodeFunction(componentById.DecodeDCFirst);
				}
				else
				{
					componentById.Decode = new JpegComponent.DecodeFunction(componentById.DecodeDCRefine);
				}
			}
			this.DecodeScan(numberOfComponents, componentSelector, resetInterval, jpegReader, ref marker);
		}

		private int mcus_per_row(JpegComponent c)
		{
			return ((this.Width * c.factorH + (this.Scan.MaxH - 1)) / this.Scan.MaxH + 7) / 8;
		}

		public void setHuffmanTables(byte componentID, JpegHuffmanTable ACTable, JpegHuffmanTable DCTable)
		{
			JpegComponent componentById = this.Scan.GetComponentById(componentID);
			if (DCTable != null)
			{
				componentById.setDCTable(DCTable);
			}
			if (ACTable != null)
			{
				componentById.setACTable(ACTable);
			}
		}

		public void setPrecision(byte data)
		{
			this.precision = data;
		}
	}
}