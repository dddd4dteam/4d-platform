using System;
using System.Collections.Generic;
using System.Text;

namespace DDDD.Core.Imaging
{
	internal class DecodedJpeg
	{
		private DDDD.Core.Imaging.Image _image;

		internal int[] BlockWidth;

		internal int[] BlockHeight;

		internal int Precision = 8;

		internal int[] HsampFactor = new int[] { 1, 1, 1 };

		internal int[] VsampFactor = new int[] { 1, 1, 1 };

		internal bool[] lastColumnIsDummy = new bool[3];

		internal bool[] lastRowIsDummy = new bool[3];

		internal int[] compWidth;

		internal int[] compHeight;

		internal int MaxHsampFactor;

		internal int MaxVsampFactor;

		private List<JpegHeader> _metaHeaders;

		public bool HasJFIF
		{
			get;
			private set;
		}

		public DDDD.Core.Imaging.Image Image
		{
			get
			{
				return this._image;
			}
		}

		public IList<JpegHeader> MetaHeaders
		{
			get
			{
				return this._metaHeaders.AsReadOnly();
			}
		}

		public DecodedJpeg(DDDD.Core.Imaging.Image image, IEnumerable<JpegHeader> metaHeaders)
		{
			this._image = image;
			this._metaHeaders = (metaHeaders == null ? new List<JpegHeader>(0) : new List<JpegHeader>(metaHeaders));
			foreach (JpegHeader _metaHeader in this._metaHeaders)
			{
				if (!_metaHeader.IsJFIF)
				{
					continue;
				}
				this.HasJFIF = true;
				break;
			}
			int componentCount = this._image.ComponentCount;
			this.compWidth = new int[componentCount];
			this.compHeight = new int[componentCount];
			this.BlockWidth = new int[componentCount];
			this.BlockHeight = new int[componentCount];
			this.Initialize();
		}

		public DecodedJpeg(DDDD.Core.Imaging.Image image) : this(image, null)
		{
			this._metaHeaders = new List<JpegHeader>();
			string str = "Jpeg Codec | fluxcapacity.net ";
			List<JpegHeader> jpegHeaders = this._metaHeaders;
			JpegHeader jpegHeader = new JpegHeader()
			{
				Marker = 254,
				Data = Encoding.UTF8.GetBytes(str)
			};
			jpegHeaders.Add(jpegHeader);
		}

		/// <summary>
		/// This method creates and fills three arrays, Y, Cb, and Cr using the input image.
		/// </summary>
		private void Initialize()
		{
			int i;
			int width = this._image.Width;
			int height = this._image.Height;
			this.MaxHsampFactor = 1;
			this.MaxVsampFactor = 1;
			for (i = 0; i < this._image.ComponentCount; i++)
			{
				this.MaxHsampFactor = Math.Max(this.MaxHsampFactor, this.HsampFactor[i]);
				this.MaxVsampFactor = Math.Max(this.MaxVsampFactor, this.VsampFactor[i]);
			}
			for (i = 0; i < this._image.ComponentCount; i++)
			{
				this.compWidth[i] = (width % 8 != 0 ? (int)Math.Ceiling((double)width / 8) * 8 : width) / this.MaxHsampFactor * this.HsampFactor[i];
				if (this.compWidth[i] != width / this.MaxHsampFactor * this.HsampFactor[i])
				{
					this.lastColumnIsDummy[i] = true;
				}
				this.BlockWidth[i] = (int)Math.Ceiling((double)this.compWidth[i] / 8);
				this.compHeight[i] = (height % 8 != 0 ? (int)Math.Ceiling((double)height / 8) * 8 : height) / this.MaxVsampFactor * this.VsampFactor[i];
				if (this.compHeight[i] != height / this.MaxVsampFactor * this.VsampFactor[i])
				{
					this.lastRowIsDummy[i] = true;
				}
				this.BlockHeight[i] = (int)Math.Ceiling((double)this.compHeight[i] / 8);
			}
		}
	}
}