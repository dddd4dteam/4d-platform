namespace DDDD.Core.Imaging
{
	public class JpegQualityProperty : EncodingProperty
	{
		public int Quality
		{
			get;
			set;
		}

		public JpegQualityProperty()
		{
		}
	}
}