using System.IO;

namespace DDDD.Core.Imaging.IO
{
	internal class JPEGBinaryReader : BinaryReader
	{
		public int eob_run;

		private byte marker;

		private byte _bitBuffer;

		protected int _bitsLeft;

		public int BitOffset
		{
			get
			{
				return (8 - this._bitsLeft) % 8;
			}
			set
			{
				if (this._bitsLeft != 0)
				{
					base.BaseStream.Seek((long)-1, SeekOrigin.Current);
				}
				this._bitsLeft = (8 - value) % 8;
			}
		}

		public JPEGBinaryReader(Stream input) : base(input)
		{
		}

		/// <summary>
		/// Seeks through the stream until a marker is found.
		/// </summary>
		public byte GetNextMarker()
		{
			byte marker;
			try
			{
				while (true)
				{
					this.ReadJpegByte();
				}
			}
			catch (JPEGMarkerFoundException jPEGMarkerFoundException)
			{
				marker = jPEGMarkerFoundException.Marker;
			}
			return marker;
		}

		/// <summary>
		/// Places n bits from the stream, where the most-significant bits
		/// from the first byte read end up as the most-significant of the returned
		/// n bits.
		/// </summary>
		/// <param name="n">Number of bits to return</param>
		/// <returns>Integer containing the bits desired -- shifted all the way right.</returns>
		public int ReadBits(int n)
		{
			int num = 0;
			if (this._bitsLeft >= n)
			{
				JPEGBinaryReader jPEGBinaryReader = this;
				jPEGBinaryReader._bitsLeft = jPEGBinaryReader._bitsLeft - n;
				num = this._bitBuffer >> (8 - n & 31);
				this._bitBuffer = (byte)(this._bitBuffer << (n & 31));
				return num;
			}
			while (n > 0)
			{
				if (this._bitsLeft == 0)
				{
					this._bitBuffer = this.ReadJpegByte();
					this._bitsLeft = 8;
				}
				int num1 = (n <= this._bitsLeft ? n : this._bitsLeft);
				num = num | this._bitBuffer >> (8 - num1 & 31) << (n - num1 & 31);
				this._bitBuffer = (byte)(this._bitBuffer << (num1 & 31));
				JPEGBinaryReader jPEGBinaryReader1 = this;
				jPEGBinaryReader1._bitsLeft = jPEGBinaryReader1._bitsLeft - num1;
				n = n - num1;
			}
			return num;
		}

		protected byte ReadJpegByte()
		{
			byte num;
			byte num1 = base.ReadByte();
			if (num1 == 255)
			{
				do
				{
					num = base.ReadByte();
					num1 = num;
				}
				while (num == 255);
				if (num1 != 0)
				{
					this.marker = num1;
					throw new JPEGMarkerFoundException(this.marker);
				}
				num1 = 255;
			}
			return num1;
		}
	}
}