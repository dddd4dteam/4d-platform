using System;

namespace DDDD.Core.Imaging.IO
{
	internal class JPEGMarkerFoundException : Exception
	{
		public byte Marker;

		public JPEGMarkerFoundException(byte marker)
		{
			this.Marker = marker;
		}
	}
}