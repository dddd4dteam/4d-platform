using System;
using System.Threading;

namespace DDDD.Core.Imaging.Filtering
{
	internal class Convolution
	{
		public readonly static Convolution Instance;

		static Convolution()
		{
			Convolution.Instance = new Convolution();
		}

		public Convolution()
		{
		}

		/// <summary>
		/// Vanilla 2D convolution.  Not optimized.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="op"></param>
		/// <returns></returns>
		public GrayImage Conv2D(GrayImage data, GrayImage op)
		{
			GrayImage grayImage = new GrayImage(data.Width, data.Height);
			if (op.Width % 2 == 0 || op.Height % 2 == 0)
			{
				throw new ArgumentException("Operator must have an odd number of rows and columns.");
			}
			int width = op.Width / 2;
			int height = op.Height / 2;
			for (int i = 0; i < data.Height; i++)
			{
				for (int j = 0; j < data.Width; j++)
				{
					float item = 0f;
					float single = 0f;
					for (int k = 0; k < op.Height; k++)
					{
						int num = i - height + k;
						if (num >= 0 && num < data.Height)
						{
							for (int l = 0; l < op.Width; l++)
							{
								int num1 = j - width + l;
								if (num1 >= 0 && num1 < data.Width)
								{
									float item1 = op[l, k];
									single = single + Math.Abs(item1);
									item = item + data[num1, num] * item1;
								}
							}
						}
					}
					grayImage[j, i] = item / single;
				}
			}
			return grayImage;
		}

		public GrayImage Conv2DSeparable(GrayImage data, float[] filter)
		{
			GrayImage grayImage = this.Filter1DSymmetric(data, filter, true);
			return this.Filter1DSymmetric(grayImage, filter, true);
		}

		/// <summary>
		/// Convolves an GrayImage with a 2D-symmetric operator.
		/// </summary>
		/// <param name="data">Data to be convolved with the operator</param>
		/// <param name="opUL">Lower-right quadrant of the operator.</param>
		/// <returns></returns>
		private GrayImage Conv2DSymm(GrayImage data, GrayImage opLR)
		{
			if (opLR.Width % 2 != 0 || opLR.Height % 2 != 0)
			{
				throw new ArgumentException("Operator must have an even number of rows and columns.");
			}
			int width = opLR.Width - 1;
			int height = opLR.Height - 1;
			GrayImage grayImage = new GrayImage(data.Width - 2 * width, data.Height - 2 * height);
			for (int i = height; i < data.Height - height; i++)
			{
				for (int j = width; j < data.Width - width; j++)
				{
					float item = data[j, i] * opLR.Scan0[0];
					for (int k = 1; k < opLR.Height; k++)
					{
						item = item + (data[j, i + k] + data[j, i - k]) * opLR[0, k];
					}
					for (int l = 1; l < opLR.Width; l++)
					{
						item = item + (data[j + l, i] + data[j - l, i]) * opLR[l, 0];
					}
					int num = 1;
					for (int m = 1; m < opLR.Height; m++)
					{
						int width1 = (i + m) * data.Width + j;
						int num1 = (i - m) * data.Width + j;
						for (int n = 1; n < opLR.Width; n++)
						{
							item = item + (data.Scan0[width1 + n] + data.Scan0[num1 + n] + data.Scan0[width1 - n] + data.Scan0[num1 - n]) * opLR.Scan0[num];
							num++;
						}
						num++;
					}
					grayImage[j - width, i - height] = item;
				}
			}
			return grayImage;
		}

		public GrayImage Conv2DSymmetric(GrayImage data, GrayImage opLR)
		{
			int width = opLR.Width - 1;
			int height = opLR.Height - 1;
			GrayImage grayImage = new GrayImage(data.Width + 2 * width, data.Height + 2 * height);
			int num = 0;
			for (int i = 0; i < data.Height; i++)
			{
				int width1 = (i + height) * (data.Width + 2 * width) + width;
				for (int j = 0; j < data.Width; j++)
				{
					grayImage.Scan0[width1 + j] = data.Scan0[num];
					num++;
				}
			}
			return this.Conv2DSymm(grayImage, opLR);
		}

		/// <summary>
		/// Filters an GrayImage with a 1D symmetric filter along the X-axis.
		/// (This operation is multithreaded)
		/// </summary>
		/// <param name="data">GrayImage to be operated on</param>
		/// <param name="filter">Filter to use (center tap plus right-hand-side)</param>
		/// <param name="transpose">Transpose the result?</param>
		/// <returns>Transposed, filtered GrayImage.</returns>
		public GrayImage Filter1DSymmetric(GrayImage data, float[] filter, bool transpose)
		{
			GrayImage grayImage = (transpose ? new GrayImage(data.Height, data.Width) : new GrayImage(data.Width, data.Height));
			int height = 0;
			int num = (transpose ? height : height * grayImage.Width);
			Convolution.FilterJob filterJob = new Convolution.FilterJob()
			{
				filter = filter,
				data = data,
				destPtr = num,
				result = grayImage,
				start = height,
				end = data.Height / 2
			};
			Convolution.FilterJob height1 = filterJob;
			ParameterizedThreadStart parameterizedThreadStart = (transpose ? new ParameterizedThreadStart(this.FilterPartSymmetricT) : new ParameterizedThreadStart(this.FilterPartSymmetric));
			Thread thread = new Thread(parameterizedThreadStart);
			thread.Start(height1);
			height = data.Height / 2;
			num = (transpose ? height : height * grayImage.Width);
			height1.start = height;
			height1.destPtr = num;
			height1.end = data.Height;
			parameterizedThreadStart(height1);
			thread.Join();
			return grayImage;
		}

		/// <summary>
		/// Convolves an GrayImage with a 1D filter along the X-axis.
		/// </summary>
		/// <param name="filterJob">Filter operation details</param>
		private void FilterPartSymmetric(object filterJob)
		{
			Convolution.FilterJob filterJob1 = (Convolution.FilterJob)filterJob;
			GrayImage grayImage = filterJob1.data;
			float[] scan0 = grayImage.Scan0;
			float[] singleArray = filterJob1.filter;
			float[] scan01 = filterJob1.result.Scan0;
			int length = (int)singleArray.Length - 1;
			int num = filterJob1.destPtr;
			for (int i = filterJob1.start; i < filterJob1.end; i++)
			{
				int width = i * grayImage.Width;
				int num1 = filterJob1.dataPtr + width;
				for (int j = 0; j < length; j++)
				{
					float single = scan0[num1] * singleArray[0];
					for (int k = 1; k < j + 1; k++)
					{
						single = single + (scan0[num1 + k] + scan0[num1 - k]) * singleArray[k];
					}
					for (int l = j + 1; l < (int)singleArray.Length; l++)
					{
						single = single + (scan0[num1 + l] + scan0[num1 + l]) * singleArray[l];
					}
					int num2 = num;
					num = num2 + 1;
					scan01[num2] = single;
					num1++;
				}
				for (int m = length; m < grayImage.Width - length; m++)
				{
					float single1 = scan0[num1] * singleArray[0];
					for (int n = 1; n < (int)singleArray.Length; n++)
					{
						single1 = single1 + (scan0[num1 + n] + scan0[num1 - n]) * singleArray[n];
					}
					int num3 = num;
					num = num3 + 1;
					scan01[num3] = single1;
					num1++;
				}
				for (int o = grayImage.Width - length; o < grayImage.Width; o++)
				{
					float single2 = scan0[num1] * singleArray[0];
					for (int p = 0; p < grayImage.Width - o; p++)
					{
						single2 = single2 + (scan0[num1 + p] + scan0[num1 - p]) * singleArray[p];
					}
					for (int q = grayImage.Width - o; q < (int)singleArray.Length; q++)
					{
						single2 = single2 + (scan0[num1 + q] + scan0[num1 - q]) * singleArray[q];
					}
					int num4 = num;
					num = num4 + 1;
					scan01[num4] = single2;
					num1++;
				}
			}
		}

		/// <summary>
		/// Convolves part of an GrayImage with a 1D filter along the X-axis
		/// and transposes it as well.
		/// </summary>
		/// <param name="filterJob">Filter operation details</param>
		private void FilterPartSymmetricT(object filterJob)
		{
			Convolution.FilterJob filterJob1 = (Convolution.FilterJob)filterJob;
			GrayImage grayImage = filterJob1.data;
			float[] scan0 = grayImage.Scan0;
			float[] singleArray = filterJob1.filter;
			GrayImage grayImage1 = filterJob1.result;
			int length = (int)singleArray.Length - 1;
			for (int i = filterJob1.start; i < filterJob1.end; i++)
			{
				int width = i * grayImage.Width;
				for (int j = 0; j < length; j++)
				{
					float single = scan0[width] * singleArray[0];
					for (int k = 1; k < j + 1; k++)
					{
						single = single + (scan0[width + k] + scan0[width - k]) * singleArray[k];
					}
					for (int l = j + 1; l < (int)singleArray.Length; l++)
					{
						single = single + (scan0[width + l] + scan0[width + l]) * singleArray[l];
					}
					grayImage1[i, j] = single;
					width++;
				}
				for (int m = length; m < grayImage.Width - length; m++)
				{
					float single1 = scan0[width] * singleArray[0];
					for (int n = 1; n < (int)singleArray.Length; n++)
					{
						single1 = single1 + (scan0[width + n] + scan0[width - n]) * singleArray[n];
					}
					grayImage1[i, m] = single1;
					width++;
				}
				for (int o = grayImage.Width - length; o < grayImage.Width; o++)
				{
					float single2 = scan0[width] * singleArray[0];
					for (int p = 1; p < grayImage.Width - o; p++)
					{
						single2 = single2 + (scan0[width + p] + scan0[width - p]) * singleArray[p];
					}
					for (int q = grayImage.Width - o; q < (int)singleArray.Length; q++)
					{
						single2 = single2 + (scan0[width - q] + scan0[width - q]) * singleArray[q];
					}
					grayImage1[i, o] = single2;
					width++;
				}
			}
		}

		public GrayImage GaussianConv(GrayImage data, double std)
		{
			return this.Conv2DSeparable(data, this.GaussianFilter(std));
		}

		public float[] GaussianFilter(double std)
		{
			double num = std * std;
			double num1 = Math.Sqrt(-1 * num * Math.Log(0.00999999977648258));
			int num2 = (int)Math.Ceiling(num1);
			float[] singleArray = new float[num2];
			double num3 = -1;
			for (int i = 0; i < num2; i++)
			{
				double num4 = Math.Exp(-0.5 * (double)(i * i) / num);
				singleArray[i] = (float)num4;
				num3 = num3 + 2 * num4;
			}
			for (int j = 0; j < num2; j++)
			{
				singleArray[j] = singleArray[j] / (float)num3;
			}
			return singleArray;
		}

		private struct FilterJob
		{
			public float[] filter;

			public int start;

			public int end;

			public GrayImage data;

			public GrayImage result;

			public int dataPtr;

			public int destPtr;
		}
	}
}