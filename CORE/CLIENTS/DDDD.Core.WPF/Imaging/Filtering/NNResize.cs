using System;

namespace DDDD.Core.Imaging.Filtering
{
	internal class NNResize : Filter
	{
		public NNResize()
		{
		}

		protected override void ApplyFilter()
		{
			int length = this._sourceData[0].GetLength(0);
			int num = this._sourceData[0].GetLength(1);
			double num1 = (double)length / (double)this._newWidth;
			double num2 = (double)num / (double)this._newHeight;
			double num3 = 0.5 * num1;
			double num4 = 0.5 * num2;
			for (int i = 0; i < this._newHeight; i++)
			{
				int num5 = (int)num4;
				num3 = 0;
				base.UpdateProgress((double)i / (double)this._newHeight);
				for (int j = 0; j < this._newWidth; j++)
				{
					int num6 = (int)num3;
					this._destinationData[0][j, i] = this._sourceData[0][num6, num5];
					if (this._color)
					{
						this._destinationData[1][j, i] = this._sourceData[1][num6, num5];
						this._destinationData[2][j, i] = this._sourceData[2][num6, num5];
					}
					num3 = num3 + num1;
				}
				num4 = num4 + num2;
			}
		}
	}
}