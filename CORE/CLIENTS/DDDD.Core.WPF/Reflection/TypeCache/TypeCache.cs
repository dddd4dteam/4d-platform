﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


using DDDD.Core.App;
using DDDD.Core.Events;
using DDDD.Core.Extensions;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Threading;
using DDDD.Core.Net.WebApi;

#if SERVER && IIS
using System.Web.Http.Dependencies;
using System.Web.Http.Dispatcher;
#endif



namespace DDDD.Core.Reflection
{
    /// <summary>
    /// Delegate with signarture - [void DomainTypesAddedHandler (IEnumerable{Type} addedDomainTypes)] - used/rised  when some really useful-custom-domain types added into TypeCache.
    /// </summary>
    /// <param name="addedDomainTypes"></param>
    public delegate void DomainTypesAddedHandler (Type[] addedDomainTypes);




    /// <summary>
    ///  TypeCache -  Domain Types Cache in current Application. 
    /// </summary>
    public class TypeCache
    {

        #region ------------------------------ CTOR --------------------------------

        static TypeCache()
        {
            Initialize();
        }

        #endregion ------------------------------ CTOR --------------------------------
        


        #region ----------------------- LOCKS ------------------------

        class InnerLocker1
        {
            internal static readonly object locker1 = new object();
        }

        class InnerLocker2
        {
            internal static readonly object locker2 = new object();
        }

        #endregion ----------------------- LOCKS ------------------------
        


        private static void Initialize()
        {
            var assemblies = AppDomain.CurrentDomain.GetAssemblies();
            assemblies = assemblies.Where(IsCustomDomainAssemblyCriterion).ToArray();

            // add including Types to customDomainTypes and to CustomDomainTypesByAssemblies
            //CustomDomainTypes.AddRange(IncludingTypes);
            // add to CustomDomainTypesByAssemblies
            AddCustomDomainTypesByAssemblies(IncludingTypes);

            // Initialize the types of early loaded assemblies
           
            foreach (var assembly in assemblies)
            {
                TryAttachAssemblyToDomain(assembly);
            }


#if SERVER || WPF
            AppDomain.CurrentDomain.AssemblyLoad += CurrentDomain_AssemblyLoad;
               
#elif CLIENT && SL5
           
            //  assemblies will be loaded synchronously with IOC container - ComponentsContainer manually
#endif

        }




#if SERVER || WPF

        private static void CurrentDomain_AssemblyLoad(object sender, AssemblyLoadEventArgs args)
        {
            TryAttachAssemblyToDomain(args.LoadedAssembly);
        }
             

#endif





        #region ------------------------------- CUSTOMIZATION OF DOMAIN ASSEMBLIES -----------------------------------


        static List<string> excludingAssemblies =  new List<string>()//by default we exclude all types from 4dCore,but include only some component Types in the other init point.
        {
#if SERVER
            "DDDD.Core.Server.Net45"
#elif CLIENT && SL5
            "DDDD.Core.SL5"
#elif CLIENT && WPF
            "DDDD.Core.WPF"
#elif CLIENT && WP81
            "DDDD.Core.WP81"
#endif
        };

        /// <summary>
        /// Excluding Assemblies. 
        /// We use this list in  Checking Contract/Predicate - checking that assembly can be mean as Domain Assembly .
        /// </summary>
        public static List<string> ExcludingAssemblies
        {
            get
            {
                return new List<string>(excludingAssemblies);
            }
        } 
      
        
        /// <summary>
        /// Exclude Domain Assembly and include some Types that still can exist in that excludingAssembly. 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="stillIncludingTypes"></param>
        public static void ExcludeDomainAssembly(string assemblyName, params Type[] stillIncludingTypes)
        {
            if (excludingAssemblies.Contains(assemblyName)) return;

            // add not existed excludeAssembly
            excludingAssemblies.Remove(assemblyName);
                
            for (int i = 0; i < stillIncludingTypes.Length; i++)
            {
                if (IncludingTypes.Contains(stillIncludingTypes[i])) IncludingTypes.Add(stillIncludingTypes[i]);
            }               
            
        }
          


        /// <summary>
        /// Some specific Types that may exist in [Excluding Assemblies], but they are still needable Types. 
        /// This types used in default check for domain type criterion.
        /// </summary>
        public static List<Type> IncludingTypes
        { get; } = new List<Type>()
        {
#if SERVER && IIS
              typeof(DCServiceHostFactory)  ,typeof(DCService), typeof(DCWebServiceHostFactory), typeof(DCWebService)
            , typeof(DCWebApiDirectHandlerServerFactory)
            , typeof(AppComponentsLoadManager)
#elif CLIENT 
            typeof(DCServiceClientFactory),typeof(DCServiceClient),typeof(DCWebApiDirectHandlerClientFactory)
            , typeof(AppComponentsLoadManager)
#endif
        };

       

        /// <summary>
        /// Custom application's Domain Assemblies base recognition criterion. It is First Word before [.] symbol in your custom Domain assembly name string.
        /// <para/> For example [DDDD.Core.Server] - CustomDomainAssemblyNameStartPart = [DDDD.] and so on. 
        ///  It means that we will collect CustomDomainAssemblies-assemblies where name StartsWith this value.
        /// </summary>
        public static string CustomDomainAssemblyNameStartPart
        { get; } = LazyAct<string>.Create(
                    (args) =>
                    {      return Assembly.GetExecutingAssembly()
                                    .GetAssemblyNameEx().Split('.')[0] + ".";                     
                    }
                    , null
                    , null
                    ).Value;

        /// <summary>
        /// Contract/predicate to check some Assembly belongs to our  Domain  scope. 
        /// Default criterion check logic is the following:  1-  we check that AssemblyName.StartsWith(CustomDomainAssemblyNameStartPart) and 2- check condition that assemblyName not contains in [ExcludingAssemblies] list.
        /// You can redefine this default criterion check delegate.
        /// </summary>
        public static Func<Assembly, bool> IsCustomDomainAssemblyCriterion
        { get; set; } = IsCustomDomainAssemblyCheck;

        protected internal static bool IsCustomDomainAssemblyCheck(Assembly checkingAssembly)
        {
            if (checkingAssembly == null) return false;
            var assemblyName = checkingAssembly.GetAssemblyNameEx();
#if SERVER
            if (checkingAssembly.ReflectionOnly) { return false; }
#endif
            if (  assemblyName.StartsWith(CustomDomainAssemblyNameStartPart)
                &&   !ExcludingAssemblies.Contains(assemblyName) //assembly is in Excluding List 
                ) return true;
            
            return false;
        }


        /// <summary>
        /// Current Domain Types by Assemblies. Domain Types excludes dynamic Types. 
        /// </summary>
        static Dictionary<string, List<Type>> CustomDomainTypesByAssemblies
        { get; } = new Dictionary<string, List<Type>>();

        /// <summary>
        ///  Get Domain's  Assemblies names
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<string> GetDomainAssemblyNames()
        { return CustomDomainTypesByAssemblies.Keys; }


        /// <summary>
        /// If TypeCache Contains Assembly(DomainAssembly) by AssemblyName
        /// </summary>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static bool ContainsAssembly(Assembly assembly)
        {
            var assemblyName = assembly.GetAssemblyNameEx();
            return CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }

        public static bool NotContainsAssembly(Assembly assembly)
        {
            var assemblyName = assembly.GetAssemblyNameEx();
            return !CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }



        /// <summary>
        /// If TypeCache Contains Assembly(DomainAssembly) by AssemblyName
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static bool ContainsAssembly(string assemblyName)
        {            
            return CustomDomainTypesByAssemblies.ContainsKey(assemblyName);
        }





        /// <summary>
        /// Get Domain Assembly Types by AssemblyName.
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDomainAssemblyTypes(string assemblyName)
        {             
            if (CustomDomainTypesByAssemblies.NotContainsKey(assemblyName))
                return new List<Type>();//empty List of Types

            return CustomDomainTypesByAssemblies[assemblyName];     
       }

        #endregion ------------------------------- CUSTOMIZATION OF DOMAIN ASSEMBLIES -----------------------------------


        #region ----------------------------- CUSTOMIZATION OF DOMAIN TYPES ---------------------------------
        
        /// <summary>
        /// Compose Custom Assemblies List based on TypeCache loaded Types sets. 
        /// Assemblies that contain Including types we do not add to DomainAssemblies List .
        /// </summary>
        /// <returns></returns>
        public static List<Assembly> GetCustomDomainAssemblies()
        {
            var customAssemblies = CustomDomainTypesByAssemblies.Keys.ToList();
            var allDomainAssemblies = AppDomain.CurrentDomain.GetAssemblies(); // all app domain assemblies


            // but we select only those exist in CustomDomainTypesByAssemblies.Keys
            List<Assembly> resultAssemblies = new List<Assembly>();
            for (int i = 0; i < allDomainAssemblies.Length; i++)
            {
                if (customAssemblies.Contains(allDomainAssemblies[i].GetAssemblyNameEx()))
                {
                    resultAssemblies.Add(allDomainAssemblies[i]);
                }
            }

            return resultAssemblies;
        }



        /// <summary>
        /// This is the Contract/Predicate by which we beleave that cheking  Exported Type, from custom assembly, is Domain Type.
        /// By default we beleave that Type is Domain Type. If it's name doesn't starts from auto Type Symbol [less than sign] 
        /// </summary>
        public static Func<Type, bool> IsCustomDamainTypeCriterion
        { get; set; } = IsCustomDomainTypeCheck;

        static bool IsCustomDomainTypeCheck(Type checkingType)
        {
            // Ignore useless types
            if (checkingType.Name.StartsWith("<")) return false;
            if (checkingType.Name.StartsWith("__")) return false;

            if (checkingType.Name.Contains("\\*")) return false;
            if (checkingType.Name.Contains("MS.Internal")) return false;

            if (checkingType.Name.Contains("c__DisplayClass")) return false;

            if (checkingType.Name.Contains("d__")) return false;

            if (checkingType.Name.Contains("o__")) return false;

            if (checkingType.Name.Contains("::")) return false;

            if (checkingType.Name.Contains("f__AnonymousType")) return false;

            if (checkingType.Name.Contains("_extraBytes_")) return false;

            if (checkingType.Name.Contains("CppImplementationDetails")) return false;
            
            if( 
                checkingType.Name.Contains("ProcessedByFody") ||
                checkingType.Name.Contains("FXAssembly") ||
                checkingType.Name.Contains("ThisAssembly") ||
                checkingType.Name.Contains("AssemblyRef")
              )
            {  return false;  }

            return true;
        }


     

        /// <summary>
        /// Get custom Domain Type from TypeCache by Type.AssemblyQualifiedName .
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        /// <returns></returns>
        public static Type GetCustomDomainType(string assemblyName,string typeFullName)
        {

            var existInIncludingTypes = IncludingTypes.FirstOrDefault(tp => tp.FullName == typeFullName);
            if (existInIncludingTypes.IsNotNull()) return existInIncludingTypes;
            

            if (CustomDomainTypesByAssemblies.NotContainsKey(assemblyName)) return null;

            return CustomDomainTypesByAssemblies[assemblyName].FirstOrDefault(tp => tp.FullName == typeFullName);
            
            //return CustomDomainTypes.FirstOrDefault(tp => tp.AssemblyQualifiedName == assemblyQualifiedName);
        }

        #endregion  ----------------------------- CUSTOMIZATION OF DOMAIN TYPES ---------------------------------
        

        #region -------------------------------- ADDING CUSTOM DOMAIN TYPES ----------------------------------
        
        static readonly WeakDelegatesManager domainTypesAddedListeners = new WeakDelegatesManager();

        /// <summary>
        /// DomainTypesAdded Event - will be raised on collection of custom DomainTypes added to TypeCache.CustomDomainTypes and  TypeCache.CustomDomainTypesByAssemblies
        /// </summary>
        public static event DomainTypesAddedHandler DomainTypesAdded
        {
            add
            {
                domainTypesAddedListeners.AddListener(value);
            }
            remove
            {
                domainTypesAddedListeners.RemoveListener(value);
            }
        }


        protected internal static void TryAddCustomType(Type someType)
        {
            var tpAssemblyName = someType.Assembly.GetAssemblyNameEx();

            //CustomDomainTypesByAssemblies
            if (CustomDomainTypesByAssemblies.NotContainsKey(tpAssemblyName))
            {
                CustomDomainTypesByAssemblies.Add(tpAssemblyName, new List<Type>()); // add new assembly Slot - List<Type>
            }

            if (!CustomDomainTypesByAssemblies[tpAssemblyName].Contains(someType))
            {
                CustomDomainTypesByAssemblies[tpAssemblyName].Add(someType);
            }

        }

        /// <summary>
        /// Add some custom-domain type into CustomDomainTypesByAssemblies internal dictionary - types grouping by Assembly.GetAssemblyNameEx().
        /// </summary>
        /// <param name="newDomainTypes"></param>
        protected internal static void AddCustomDomainTypesByAssemblies(IList<Type> newDomainTypes)
        {
            for (int i = 0; i < newDomainTypes.Count; i++)
            {
                TryAddCustomType(newDomainTypes[i]);
            }
        
        }


        /// <summary>
        /// Attaching separate assembly to Domain Assemblies
        /// </summary>
        /// <param name="loadedAssembly"></param>
        public static void TryAttachAssemblyToDomain(Assembly loadedAssembly)
        {
            //if not loaded - add  is custom Domain Assembly              
            if (IsCustomDomainAssemblyCheck(loadedAssembly))
            {
                if (CustomDomainTypesByAssemblies.ContainsKey(loadedAssembly.GetAssemblyNameEx())) return;

                lock (InnerLocker1.locker1)
                {
                    // all domain types from assembly to domain types                
                    var assemblyNewDomainTypes = loadedAssembly.GetTypes().Where(IsCustomDamainTypeCriterion).ToList();
                   
                    AddCustomDomainTypesByAssemblies(assemblyNewDomainTypes);
                    //CustomDomainTypes.AddRange(assemblyNewDomainTypes);

                    domainTypesAddedListeners.Raise(assemblyNewDomainTypes);// raise event of domain Types added
                }
            }
        }




        #endregion -------------------------------ADDING CUSTOM DOMAIN TYPES ------------------------------




        /// <summary>
        /// Full Reset - full update of Collection of Domain Assemblies and all of theirs Exported Types.
        /// </summary>
        public static void ResetDomainTypeCache()
        {
            var domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < domainAssemblies.Length; i++)
            {
                TryAttachAssemblyToDomain(domainAssemblies[i]);
            }
            
        }

      


#region ------------------------------------- LOAD CUSTOM DOMAIN TYPES -------------------------------------


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type has BaseType and its value satisfy the following condition : [ tBaseType.IsAssignableFrom(checkingType)]. 
        /// Also we can exclude some endTypes  in [excludeTypes] parameter value.
        /// </summary>
        /// <typeparam name="TBase"></typeparam>
        /// <param name="excludeTBaseItself"></param>
        /// <param name="excludeTypes"></param>
        /// <returns></returns>
        public static List<Type> LoadCustomDomainTypesByBaseTypeT<TBase>(bool excludeTBaseItself = true, params Type[] excludeTypes)
        {
            return LoadCustomDomainTypesByBaseType(typeof(TBase), excludeTBaseItself, excludeTypes);
        }

        /// <summary>
        /// Get list of Domain TypesLoad Types to Cache from the list of Domain Types, where Type has BaseType and its value satisfy the following condition : [ tBaseType.IsAssignableFrom(checkingType)].
        /// </summary>
        /// <param name="tBaseType"></param>
        /// <param name="excludeTBaseItself"></param>
        /// <param name="excludeTypes"></param>
        /// <returns></returns>
        public static List<Type> LoadCustomDomainTypesByBaseType(Type tBaseType, bool excludeTBaseItself = true, params Type[] excludeTypes)
        {
            //var domainTypes = CustomDomainTypes;
            //var asssemblyName = tBaseType
            //var baseType = tBaseType;
            var existedTargets = new List<Type>();

            for (int i = 0; i < IncludingTypes.Count; i++)
            {
                if (excludeTBaseItself == true && IncludingTypes[i] == tBaseType || excludeTypes.Contains(IncludingTypes[i])) continue;

                if (IncludingTypes[i].BaseType != null && tBaseType.IsAssignableFrom(IncludingTypes[i]))
                {
                    existedTargets.Add(IncludingTypes[i]);
                }
                
            }

            foreach (var assemblyDomainTypes in CustomDomainTypesByAssemblies.Values)
            {
                for (int i = 0; i < assemblyDomainTypes.Count; i++)
                {
                    if (excludeTBaseItself == true && assemblyDomainTypes[i] == tBaseType || excludeTypes.Contains(assemblyDomainTypes[i])) continue;

                    if (assemblyDomainTypes[i].BaseType != null && tBaseType.IsAssignableFrom(assemblyDomainTypes[i]))
                    {
                        existedTargets.Add(assemblyDomainTypes[i]);
                    }
                }
            }

          
            return existedTargets;
            
        }


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type implements interface TIface.
        /// </summary>
        /// <typeparam name="TIface"></typeparam>
        /// <returns></returns>
        public static List<Type> LoadCustomDomainTypesByInterfaceT<TIface>()
        {
            return LoadCustomDomainTypesByInterface(typeof(TIface));
        }


        /// <summary>
        /// Load Types to Cache from the list of Domain Types, where Type implements interface TIface.
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <returns></returns>
        public static List<Type> LoadCustomDomainTypesByInterface(Type interfaceType)
        {
            var existedTargets = new List<Type>();


            for (int i = 0; i < IncludingTypes.Count; i++)
            {
                if (IncludingTypes[i].GetInterfaces().Contains(interfaceType))
                {
                    existedTargets.Add(IncludingTypes[i]);
                }

            }


            foreach (var assemblyDomainTypes in CustomDomainTypesByAssemblies.Values)
            {
                for (int i = 0; i < assemblyDomainTypes.Count; i++)
                {
                    if (assemblyDomainTypes[i].GetInterfaces().Contains(interfaceType))
                    {
                        existedTargets.Add(assemblyDomainTypes[i]);
                    }
                }
            }            
                
            return existedTargets;
           
        }



#endregion ------------------------------------- LOAD CUSTOM DOMAIN TYPES -------------------------------------


    }
}




#region -------------------------------------GARBAGE -----------------------------------

/// <summary>
/// Get Current  Domain's Custom Types.
/// </summary>
/// <returns></returns>
//public static IEnumerable<Type> GetCustomDomainTypes()
//{
//    return CustomDomainTypes;
//}


/// <summary>
/// Current Domain Types. // Domain Types exclude dynamic  Types. 
/// </summary>
//static List<Type> CustomDomainTypes
//{ get; } = new List<Type>();



///// <summary>
///// Application Domain Assemblies - assemblies that we get by default filter criterion value - value in property [CustomDomainAssemblyNameStartPart].
///// </summary>
//static List<Assembly> CustomDomainAssemblies
//{ get; } = new List<Assembly>();


///// <summary>
/////  Get Current Domain's Custom Assemblies
///// </summary>
///// <returns></returns>
//public static IEnumerable<Assembly> GetCustomDomainAssemblies() { return CustomDomainAssemblies; }


//= LazySlim<IEnumerable<Assembly>>.Create(Load_CustomDomainAssemblies, InnerLocker1.locker1, null);


//static IEnumerable<Assembly> Load_CustomDomainAssemblies(object[] arguments)
//{
//    ///return AppDomain.CurrentDomain.  GetAssemblies().Where(IsCustomDamainAssemblyCriterion);
//}


/// <summary>
/// The list of loaded assemblies which do not required additional initialization again.
/// <para />
/// This is required because the AppDomain.AssemblyLoad might be called several times for the same AppDomain
/// </summary>
//private static HashSet<string> LoadedAssemblies
//{ get; } = new HashSet<string>();

#endregion -------------------------------------GARBAGE -----------------------------------