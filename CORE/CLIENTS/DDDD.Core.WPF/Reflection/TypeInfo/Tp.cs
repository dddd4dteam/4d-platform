﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;

namespace DDDD.Core.Reflection
{
    /// <summary>
    /// Type Consts to do not get each time typeof(Tx)
    /// </summary>
    public class Tps
    {
        public static readonly Type T_object = typeof(object);        
        public static readonly Type T_NullableGen = typeof(Nullable<>);

        public static readonly Type T_IList = typeof(IList);        
        public static readonly Type T_IDictionary = typeof(IDictionary);        
        public static readonly Type T_ICollection = typeof(ICollection);
        public static readonly Type T_IEnumerable = typeof(IEnumerable);
                
        public static readonly Type T_IListGen = typeof(IList<>);
        public static readonly Type T_IDictionaryGen = typeof(IDictionary<,>);
        public static readonly Type T_ICollectionGen = typeof(ICollection<>);
        public static readonly Type T_IEnumerableGen = typeof(IEnumerable<>);                
        public static readonly Type T_ObservableCollectionGen = typeof(ObservableCollection<>);


        public static readonly Type T_TextWriter = typeof(TextWriter);
        public static readonly Type T_Stream = typeof(Stream);


        public static readonly Type T_string = typeof(string);
        public static readonly Type T_char = typeof(char);        
        public static readonly Type T_charNul = typeof(char?);

        public static readonly Type T_bool = typeof(bool);

        public static readonly Type T_Int16 = typeof(Int16);
        public static readonly Type T_short = typeof(short);
        public static readonly Type T_ushort = typeof(ushort);
        public static readonly Type T_Int32 = typeof(Int32);
        public static readonly Type T_int = typeof(int);
        public static readonly Type T_uint = typeof(uint);
        public static readonly Type T_Int64 = typeof(Int64);
        public static readonly Type T_long = typeof(long);
        public static readonly Type T_ulong = typeof(ulong);
        public static readonly Type T_byte = typeof(byte);
        public static readonly Type T_sbyte = typeof(sbyte);
        


        public static readonly Type T_float = typeof(float);
        public static readonly Type T_Single = typeof(Single);
        public static readonly Type T_Double = typeof(Double);
        public static readonly Type T_decimal = typeof(decimal);



        public static readonly Type T_boolNul = typeof(bool?);
        public static readonly Type T_Int16Nul = typeof(Int16?);
        public static readonly Type T_shortNul = typeof(short?);
        public static readonly Type T_ushortNul = typeof(ushort?);
        public static readonly Type T_Int32Nul = typeof(Int32?);
        public static readonly Type T_intNul = typeof(int?);
        public static readonly Type T_uintNul = typeof(uint?);
        public static readonly Type T_Int64Nul = typeof(Int64?);
        public static readonly Type T_longNul = typeof(long?);
        public static readonly Type T_ulongNul = typeof(ulong?);
        public static readonly Type T_byteNul = typeof(byte?);
        public static readonly Type T_sbyteNul = typeof(sbyte?);
        
        
        public static readonly Type T_floatNul = typeof(float?);
        public static readonly Type T_SingleNul = typeof(Single?);
        public static readonly Type T_DoubleNul = typeof(Double?);
        public static readonly Type T_decimalNul = typeof(decimal?);


        public static readonly Type T_DateTime = typeof(DateTime);
        public static readonly Type T_TimeSpan = typeof(TimeSpan);
        public static readonly Type T_DateTimeOffset = typeof(DateTimeOffset);
        public static readonly Type T_Guid = typeof(Guid);

        public static readonly Type T_DateTimeNul = typeof(DateTime?);
        public static readonly Type T_TimeSpanNul = typeof(TimeSpan?);
        public static readonly Type T_DateTimeOffsetNul = typeof(DateTimeOffset?);
        public static readonly Type T_GuidNul = typeof(Guid?);

        public static readonly Type T_Uri = typeof(Uri);
        public static readonly Type T_byteArray = typeof(byte[]);
        public static readonly Type T_BitArray = typeof(BitArray);
        public static readonly Type T_stringArray= typeof(string[]);
    }
}
