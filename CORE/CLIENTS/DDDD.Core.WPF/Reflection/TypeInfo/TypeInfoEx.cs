﻿
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Reflection;
using System.Threading;
using System.Collections.ObjectModel;


using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using DDDD.Core.Threading;
using DDDD.Core.Diagnostics;
using DDDD.Core.Collections;

namespace DDDD.Core.Reflection
{

    /// <summary>
    /// TypeInfoEx -   
    /// <para/>  1 analyzes and saves  Type Description in comfortable form for serializer.
    /// <para/>  2 Can Create instance by one of it's enabled Ctors.   
    /// <para/> Is 4D Primitive Type so it can be Serialized by Binary and Json.
     /// </summary>
    public  sealed class TypeInfoEx // <T>
    {

        #region -------------------------- CTOR -----------------------------

        TypeInfoEx(String assemblyQualifiedName)
        {
            OriginalTypeAQNameString = assemblyQualifiedName;           
            TpAQName = new TypeAQName(OriginalTypeAQNameString);            
        }


        TypeInfoEx(Type targetType)
        {
            OriginalType = targetType;
            TpAQName = new TypeAQName(OriginalTypeAQNameString);
        }

        #endregion -------------------------- CTOR -----------------------------


        #region ---------------------------- FIELDS ------------------------------

        private static readonly object locker = new object();
                 

        /// <summary>
        /// Global TypeInfoEx Cache for all remembered Application Types
        /// </summary>
        internal static SafeDictionary2<int, TypeInfoEx> TypeInfoExCache = new SafeDictionary2<int, TypeInfoEx>();
        
        #endregion ---------------------------- FIELDS ------------------------------
             

        #region ---------------------------- GET CALLING TYPE FROM STACK --------------------------------


        /// <summary>
        /// Gets the type which is calling the current method which might be static. 
        /// </summary>
        /// <returns>The type calling the method.</returns>
#if !NETFX_CORE
        [MethodImpl(MethodImplOptions.NoInlining)]
#endif
        public static Type GetCallingType()
        {
#if NET45 || NET4 || NET46
            var frame = new StackFrame(1, false);
            var type = frame.GetMethod().DeclaringType;
#elif NETFX_CORE || PCL
            var type = typeof(object);
#else
            var frame = new StackTrace().GetFrame(2);
            var type = frame.GetMethod().DeclaringType;
#endif

            return type;
        }
        #endregion ---------------------------- GET CALLING TYPE FROM STACK --------------------------------


        #region ---------------------------- TypeAQName && TYPE FOUND STATE  ----------------------------

        /// <summary>
        /// Type Assembly Qualified Name Parsed struct. 
        /// <para/>  Contains FoudType if it'll be found by some assemblyQualifidName string.
        /// <para/>  Has Lazy get mechanic.
        /// </summary>
         
        public TypeAQName TpAQName
        { get; private set; }


        /// <summary>
        /// State About Founding result of Type,
        /// <para/> when Type was tried to be searched/determined by AssemblyQualifiedName.
        /// If TypeInfoEx was inited by System.Type State always will be - [TypeFounded] value  
        /// </summary>
        public TypeFoundStateEn State
        {
            get
            {
                if (TpAQName.LA_FoundType.Value.IsNotNull())
                { return TypeFoundStateEn.TypeFound; }
                return TypeFoundStateEn.NotFounded;
            }
        }

        #endregion  ---------------------------- TypeAQName && TYPE FOUND STATE  ----------------------------


        #region --------------------------------- Type INFO   ------------------------------


        Type _OriginalType = null;
        
        /// <summary>
        /// (T) - Original  Type. We analyze this Type. 
        /// </summary>
         
        public Type OriginalType
        {
            get
            {
                return _OriginalType;
            }
            private set
            {
                if (value != null)
                {
                    _OriginalType = value;
                    OriginalTypeAQNameString = _OriginalType.AssemblyQualifiedName;
                }                
            }
        }


        /// <summary>
        /// Assembly Qualified Name of Original type string  value( not of type TypeAQName).
        /// It always will be stored in TypeInfoEx instance - even when originalType won't be found by such Name.         
        /// </summary>
        public string OriginalTypeAQNameString { get; private set; }


         
        int _ID = -1;
        /// <summary>
        /// TypeinfoEx ID in internal global TypeInfoEx Dictionary of all TypeInfoEx instancies.
        /// <para/> HashCode of  OriginalTypeAQNameString
        /// </summary>
        public int ID
        {
            get
            {
                if (_ID == -1)
                {
                    _ID = OriginalTypeAQNameString.GetHashCode();
                }
                return _ID;
            }
        }


        #endregion --------------------------------- Type INFO  ----------------------------------



        #region  --------------------------------- Type DEFINITION SUB TYPES ----------------------------------


        /// <summary>
        /// Cached Generic Arguments Types of OriginalType
        /// </summary>

        public Type[] GenericArguments { get; private set; }


        /// <summary>
        /// Cached Interfaces Types of OriginalType
        /// </summary>

        public Type[] Interfaces { get; private set; }



        /// <summary>
        /// WorkingType - is T from  Nullable{T},where T is OriginalType; or if OriginalType is not Nullable then WorkType==OriginalType
        /// </summary>

        public Type WorkingType { get; private set; }


        /// <summary>
        /// Enum Underlying Type 
        /// </summary>

        public Type EnumUnderlyingType { get; private set; }



        /// <summary>
        /// Array Element Type
        /// </summary>

        public Type ArrayElementType { get; private set; }



        /// <summary>
        /// Array Rank
        /// </summary>

        public int ArrayRank { get; private set; }


        /// <summary>
        /// List_Arg1_  or ObservableCollection_Arg1_ or  Dictionary_Arg1     Type
        /// </summary>

        public Type Arg1Type { get; private set; }





        /// <summary>
        /// Dictionary_Arg2     Type
        /// </summary>

        public Type Arg2Type { get; private set; }



        #endregion  --------------------------------- Type DEFINITION SUB TYPES ----------------------------------
         


        #region ----------------------------IsNullable && Is4Dprimitive   ---------------------------

        /// <summary>
        /// Is Type Nullable value type - Nullable struct type.
        /// </summary>

        public bool IsNullabeValueType
        { get; protected set; }

        /// <summary>
        /// Primitive Type By DDDD.BCL Notation of primitive Types
        /// </summary>
         
        public bool Is4DPrimitive
        { get; protected set; }


       


        #endregion ----------------------------IsNullable && Is4Dprimitive  ---------------------------


        #region ------------------------------ IsSerializable && IsDisposable  -------------------------------------

        /// <summary>
        ///  If this type Serializable 
        /// </summary>
         
        public LazyAct<bool> IsSerializable
        { get; private set;  }
        


        /// <summary>
        /// Is this TargetType supports IDisposable
        /// </summary>

        public bool IsDisposable
        { get; private set; }



        #endregion --------------------------- IsSerializable && IsDisposable  --------------------------------


        #region --------------------------- IsTestableByDefCtor --------------------------------

        /// <summary>
        /// Try to understand if we able to test Type's Default Ctor.
        /// <para/> For Collection Types Default Ctor has length argument, like : new TCollection(length). 
        /// <para/> Problem here is that some Types can have internal static increment logic on creation by Default Ctor.
        /// <para/>  - so we'll exclude them from Default Ctor testing.
        /// <para/> By default all types will be tested, excluding array types.  
        /// </summary>      
         
        public bool IsTestableByDefCtor
        {
            get;  set;
        }


        #endregion --------------------------- IsTestableByDefCtor --------------------------------
        
         

        #region ------------------------------TYPES COMPARE - BY TYPE NAME ONLY - FOR INTERFACE AND GENERIC TYPES----------------------------------


        /// <summary>
        /// If  this [targetType] implement interface type [checkInterfaceType], comparing by Type.Name only
        /// <para/> For example if this TargetType implementing - IList, or IList{} or IDIctionary{,}
        /// </summary>
        /// <param name="someClassType"></param>
        /// <param name="checkInterfaceType"></param>
        /// <returns></returns>
        public static bool IsImplementInterfaceByTypeNameOnly(Type someClassType, Type checkInterfaceType)
        {
            if (someClassType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - someClassType parameter  ");
            if (checkInterfaceType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - checkInterfaceType parameter  ");
            if (checkInterfaceType.IsInterface == false) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() : checkInterfaceType is not interface Type ");

            return (someClassType.GetInterfaces().Where(ifc => ifc.Name == checkInterfaceType.Name).FirstOrDefault() != null);
        }


        /// <summary>
        /// If  this [targetType] is type [checkGenericType], comparing by Type.Name only
        /// <para/> For example if this TargetType is - ObservableCollection{}
        /// </summary>
        /// <param name="someClassType"></param>
        /// <param name="checkGenericType"></param>
        /// <returns></returns>
        public static bool IsGenericTypeByTypeNameOnly(Type someClassType, Type checkGenericType)
        {
            if (someClassType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - someClassType parameter  ");
            if (checkGenericType == null) throw new InvalidOperationException($" ERROR in {nameof(TypeInfoEx)}.{nameof(IsImplementInterfaceByTypeNameOnly)}() :   Null Reference - checkGenericType parameter  ");

            return (someClassType.Name == checkGenericType.Name);
        }

        #endregion ------------------------------TYPES COMPARE - BY TYPE NAME ONLY - FOR INTERFACE AND GENERIC TYPES----------------------------------


        #region --------------------------- IsICollection ,  IsIListImplemented , IsIDictionaryImplemented, IsObservableCollection --------------------------------



        /// <summary>
        /// If WorkingType Implement  ICollection  base Interface
        /// </summary>
        public bool IsICollection  { get; private set; }


        /// <summary>
        /// If  Type is some collection this Collection Type   property value can tell us the real kind of it: Array, IList, IDictionary, OnservableCollection 
        /// </summary>

        public CollectionTypeEn CollectionType { get; private set; }



        /// <summary>
        /// Is this TargetType - a Collection Type and it implements  IList or IList{T} interfaces
        /// </summary>

        public bool IsIListImplemented { get; private set; }


        /// <summary>
        /// Is this TargetType - a Collection Type and it implements  IDictionary{,} interface
        /// </summary>

        public bool IsIDictionaryImplemented { get; private set; }


        /// <summary>
        /// Is this TargetType - a Collection Type and it implements ObservableCollection 
        /// </summary>

        public bool IsObservableCollection { get; private set; }
        



        static readonly Type iListPlaneIface = typeof(IList);
        static readonly Type iListGenericIface = typeof(IList<>);
        static readonly Type iDictionaryPlaneIface = typeof(IDictionary);
        static readonly Type iDictionaryGenericIface = typeof(IDictionary<,>);
        static readonly Type tObservableCollectionGenericType = typeof(ObservableCollection<>);

        /// <summary>
        /// Get Arg of IList based from CustomCollection type. 
        /// <para/> Custom  IList  based Collection Type has the following defenition constraints : it should implement IList or IList{TArg}
        /// </summary>
        /// <param name="iListType"></param>
        /// <returns></returns>
        public static Type Get_IListArgType(Type iListType)
        {
            if (iListType == null)
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}(): iListType parameter cannot be null");

            var interfaces = iListType.GetInterfaces();

            if (interfaces.Length == 0)
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}(): Type [{iListType.FullName}] isn't implemented one of List interfaces- IList or IList<TElement> interfaces ");


            if (iListType.IsImplementInterfaceByTypeNameOnly(typeof(IList<>))) // 
            {
                return interfaces.Where(ifc => ifc.Name == iListGenericIface.Name)
                       .FirstOrDefault().GetGenericArguments()[0];
            }
            else if (iListType.IsImplementInterfaceByTypeNameOnly(typeof(IList)))
            {
                return typeof(object);
            }
            else
                throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IListArgType)}():  Cannot determine IList Arg Type ");

        }



        /// <summary>
        /// Get Arg of IDictionary based from CustomCollection type. 
        /// <para/> Custom  IDictionary  based Collection Type has the following defenition constraints : it should implement IList or IDictionary{TArg}
        /// </summary>
        /// <param name="iDictionaryType"></param>
        /// <returns></returns>
        public static Type[] Get_IDictionaryArgTypes(Type iDictionaryType)
        {
            Type[] reslt = new Type[2];

            var interfaces = iDictionaryType.GetInterfaces();
            if (iDictionaryType.IsImplementInterfaceByTypeNameOnly(iDictionaryGenericIface))
            {
                var targetGenericIface = interfaces.Where(ifc => ifc.Name == iDictionaryGenericIface.Name).First();
                reslt[0] = targetGenericIface.GetGenericArguments()[0];
                reslt[1] = targetGenericIface.GetGenericArguments()[1];
                return reslt;
            }
            else if (iDictionaryType.IsImplementInterfaceByTypeNameOnly(iDictionaryPlaneIface))
            {
                var targetPlaneIface = interfaces.Where(ifc => ifc.Name == iDictionaryPlaneIface.Name).First();
                reslt[0] = typeof(object);
                reslt[1] = typeof(object);
                return reslt;
            }
            

            throw new InvalidOperationException($"ERROR in {nameof(TypeInfoEx)}.{nameof(Get_IDictionaryArgTypes)}():  Cannot determine IDictionary Arg Types ");

        }




        /// <summary>
        /// If this class implements one of two interfaces IList or IList{}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_List(Type targetType)
        {
            return (IsImplementInterfaceByTypeNameOnly(targetType, iListPlaneIface)
                   || IsImplementInterfaceByTypeNameOnly(targetType, iListGenericIface));

        }

        /// <summary>
        /// If this class implements one of two interfaces IDictionary{,}
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsImplement_Dictionary(Type targetType)
        {
            return (//IsImplementInterfaceByTypeNameOnly(targetType , typeof(IDictionary)) // C# automatically add support IDictionaryPlane - we need to IDictionary<,> to serialize 
                    //||
                   IsImplementInterfaceByTypeNameOnly(targetType, iDictionaryGenericIface));
        }

      

        #endregion --------------------------- IsIListImplemented , IsIDictionaruary, IsObservableCollection --------------------------------
        

        #region --------------------------GET TYPEINFOEX BY TYPE && ASEMBLYQUALIFIEDNAME --------------------------------------------
        
        /// <summary>
        /// Get TypeInfoEx by T- System.Type - always will be with State:TypeFounded
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx Get<T>()
        {
            return Get(typeof(T));
        }

        /// <summary>
        /// Get TypeInfoEx by System.Type - always will be with State:TypeFounded
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx Get(Type targetType, bool useThreadSafetyWay = false)
        {
            var IDKey = targetType.AssemblyQualifiedName.GetHashCode();
            //already exist                    
            if (TypeInfoExCache.ContainsKey(IDKey))
            {    return TypeInfoExCache[IDKey];             
            }

            OperationInvoke.CallWithLockAndModes(nameof(TypeInfoEx), nameof(Get), useThreadSafetyWay
                , locker
                , () => TypeInfoExCache.NotContainsKey(IDKey) // lock use check  Expression
                , () =>
                {

                    var tpInfEx = new TypeInfoEx(targetType);

                    //Get Generic Args -caching them too
                    tpInfEx.GenericArguments = tpInfEx.OriginalType.GetGenericArguments();
                    tpInfEx.Interfaces = tpInfEx.OriginalType.GetInterfaces();                   

                    tpInfEx.IsNullabeValueType = IsNullableType(tpInfEx.OriginalType);
                    tpInfEx.WorkingType = GetWorkingTypeFromNullableType(tpInfEx.OriginalType);
                    tpInfEx.Is4DPrimitive = Is4DPrimitiveType(tpInfEx.OriginalType);

                    tpInfEx.CollectionType = GetBaseCollectionType(tpInfEx.OriginalType);
                    //tpInfEx.IsSerializable = IsSerializableType(tpInfEx.OriginalType);
                    tpInfEx.IsDisposable = IsDisposableType(tpInfEx.OriginalType);

                    tpInfEx.IsIListImplemented = IsImplement_List(tpInfEx.OriginalType);
                    tpInfEx.IsIDictionaryImplemented = IsImplement_Dictionary(tpInfEx.OriginalType);
                    tpInfEx.IsObservableCollection = IsGenericTypeByTypeNameOnly(tpInfEx.OriginalType, tObservableCollectionGenericType);

                    
                    tpInfEx.IsTestableByDefCtor = IsTestableByDefCtorType(tpInfEx.OriginalType);

                    //check is enum
                    if (tpInfEx.OriginalType.IsEnum)
                    {
                        tpInfEx.EnumUnderlyingType = Enum.GetUnderlyingType(tpInfEx.OriginalType);
                    }
                    //check is  array
                    else if (tpInfEx.OriginalType.IsArray)
                    {
                        tpInfEx.ArrayElementType = tpInfEx.OriginalType.GetElementType();
                        tpInfEx.ArrayRank = tpInfEx.OriginalType.GetArrayRank();
                    }
                    //check is IList
                    else if (tpInfEx.IsIListImplemented) //
                    {
                        tpInfEx.Arg1Type = Get_IListArgType(tpInfEx.OriginalType);//
                    }
                    // check is IDictionary
                    else if (tpInfEx.IsIDictionaryImplemented) // 
                    {
                        var dictionaryArgs = Get_IDictionaryArgTypes(tpInfEx.OriginalType);
                        tpInfEx.Arg1Type = dictionaryArgs[0];
                        tpInfEx.Arg2Type = dictionaryArgs[1];
                    }

                    //TypeConventionName - used to generate class by Reflection.Emit
                    tpInfEx.TypeDebugCodeConventionName = tpInfEx.BuildDebugCodeConventionedTypeName();

                    TypeInfoExCache.Add(tpInfEx.ID, tpInfEx);

                }
                //, exceptionAction, error detail message
                );

            // return TypeInfoExRepository[tpInfEx.ID];
            return TypeInfoExCache[IDKey];
            
        }


        /// <summary>
        /// Get TypeInfoEx by System.Type.AssemblyQualifiedName - can lead to two State
        /// <para/> - TypeFounded - System.Type was found  
        /// <para/> - NotFounded - System.Type was not found and OriginalType will be null
        /// </summary>
        /// <param name="assemblyQualifiedName"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TypeInfoEx Get(String assemblyQualifiedName, bool useThreadSafetyWay = false)
        {
            var IDKey = assemblyQualifiedName.GetHashCode();
            
            if (TypeInfoExCache.ContainsKey(IDKey)) { return TypeInfoExCache[IDKey]; } //already exist                    
            
            OperationInvoke.CallWithLockAndModes(nameof(TypeInfoEx), nameof(Get), useThreadSafetyWay
               , locker
               , () => TypeInfoExCache.NotContainsKey(IDKey)
               , () =>
               {
                   var tpInfoEx = new TypeInfoEx(assemblyQualifiedName);
                   if (tpInfoEx.State == TypeFoundStateEn.TypeFound ) // Type successfully Founded by TypeAQName on current App side and Platform
                   {
                       tpInfoEx.GenericArguments = tpInfoEx.OriginalType.GetGenericArguments();
                       tpInfoEx.Interfaces = tpInfoEx.OriginalType.GetInterfaces();
                       
                       tpInfoEx.IsNullabeValueType = IsNullableType(tpInfoEx.OriginalType);
                       tpInfoEx.WorkingType = GetWorkingTypeFromNullableType(tpInfoEx.OriginalType);
                       tpInfoEx.Is4DPrimitive = Is4DPrimitiveType(tpInfoEx.OriginalType);

                       tpInfoEx.IsICollection = Is_ICollectionType(tpInfoEx.OriginalType);
                       tpInfoEx.CollectionType = GetBaseCollectionType(tpInfoEx.OriginalType);                       
                       tpInfoEx.IsDisposable = IsDisposableType(tpInfoEx.OriginalType);

                       //tpInfoEx.IsAutoAdding = IsAutoAddingType(tpInfoEx.OriginalType);
                       tpInfoEx.IsTestableByDefCtor = IsTestableByDefCtorType(tpInfoEx.OriginalType);

                       //check is enum
                       if (tpInfoEx.OriginalType.IsEnum)
                       {
                           tpInfoEx.EnumUnderlyingType = Enum.GetUnderlyingType(tpInfoEx.OriginalType);
                       }
                       //check is  array
                       else if (tpInfoEx.OriginalType.IsArray)
                       {
                           tpInfoEx.ArrayElementType = tpInfoEx.OriginalType.GetElementType();
                           tpInfoEx.ArrayRank = tpInfoEx.OriginalType.GetArrayRank();
                       }
                       else if (tpInfoEx.OriginalType.IsImplement_List()) // IsGenericType && typeof(IList).IsAssignableFrom(tpInfEx.OriginalType)
                       {
                           tpInfoEx.Arg1Type = Get_IListArgType(tpInfoEx.OriginalType);//   (tpInfEx.GenericArguments.Length == 1) ? tpInfEx.GenericArguments[0] : null;
                       }
                       // check is IDictionary
                       else if (tpInfoEx.OriginalType.IsImplement_Dictionary()) //  tpInfEx.OriginalType.IsGenericType && tpInfEx.OriginalType.IsImplement_Dictionary() )// typeof(IDictionary).IsAssignableFrom(tpInfEx.OriginalType))
                       {
                           var dictionaryArgs = Get_IDictionaryArgTypes(tpInfoEx.OriginalType);
                           tpInfoEx.Arg1Type = dictionaryArgs[0];
                           tpInfoEx.Arg2Type = dictionaryArgs[1];
                       }

                       //TypeConventionName - used to generate class by Reflection.Emit
                       tpInfoEx.TypeDebugCodeConventionName = tpInfoEx.BuildDebugCodeConventionedTypeName();

                   }

                   TypeInfoExCache.Add(tpInfoEx.ID, tpInfoEx);                    
               }
               );

            return TypeInfoExCache[IDKey];          
            
        }

        #endregion--------------------------GET TYPEINFOEX BY TYPE && ASEMBLYQUALIFIEDNAME --------------------------------------------



        #region ------------------------- TypeDebugCodeConventionName && BUILD IT && GET IT ------------------------


        /// <summary>
        ///  Type Convention Name that can be used in Lambda Expression
        /// <para/> when we building dynamic emitted methods by TypeBuilder.
        /// <para/>  Here we simply deleting generic brackets and array brackets
        /// </summary>

        public string TypeDebugCodeConventionName
        {
            get;
            private set;
        }

        /// <summary>
        /// Type Name - flat name that can be used in Debug Code generation for any type class name(generic collection/array/ nullable struct).
        /// </summary>
        /// <returns></returns>
        string BuildDebugCodeConventionedTypeName()
        {
            string baseName = null;

            if (OriginalType.IsInterface)
            {
                if (Arg2Type != null && Arg1Type != null)//Length ==2
                {
                    baseName = OriginalType.Name.Replace("`2", "");
                    return baseName + "_" + Arg1Type.Name + "_" + Arg2Type.Name;
                }
                else if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }
            }
            else if (OriginalType.IsEnum)
            {
                baseName = OriginalType.Name;
                return baseName;
            }
            else if (OriginalType.IsArray)
            {

                if (ArrayRank == 1)
                {
                    baseName = OriginalType.Name.Replace("[]", "");
                    return baseName + "1Rank";
                }
                else if (ArrayRank == 2)
                {
                    baseName = OriginalType.Name.Replace("[,]", "");
                    return baseName + "2Rank";
                }
                else if (ArrayRank == 3)
                {
                    baseName = OriginalType.Name.Replace("[,,]", "");
                    return baseName + "3Rank";
                }
                else if (ArrayRank == 4)
                {
                    baseName = OriginalType.Name.Replace("[,,,]", "");
                    return baseName + "4Rank";
                }

            }
            else if (OriginalType.IsImplement_List()) // typeof(IList).IsAssignableFrom(OriginalType)
            {
                if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }

            }
            else if (OriginalType.IsImplement_Dictionary() ) // typeof(IDictionary).IsAssignableFrom(OriginalType))
            {
                if (Arg2Type != null && Arg1Type != null)//Length ==2
                {
                    baseName = OriginalType.Name.Replace("`2", "");
                    return baseName + "_" + Arg1Type.Name + "_" + Arg2Type.Name;
                }
                else if (Arg1Type != null)//Length ==1
                {
                    baseName = OriginalType.Name.Replace("`1", "");
                    return baseName + "_" + Arg1Type.Name;
                }
                else
                {
                    baseName = OriginalType.Name;
                    return baseName;
                }
            }
            else if (OriginalType.IsClass) // Generic not supported by Serializer
            {
                baseName = OriginalType.Name;
                return baseName;
            }

            return OriginalType.Name;
        }



        /// <summary>
        /// Type Name - flat name that can be used in Debug Code generation for any type class name(generic collection/array/ nullable struct).
        /// </summary>
        /// <param name="sometype"></param>
        /// <returns></returns>
        public static string GetConventionTypeName(Type sometype)
        {
            if (sometype == null) throw new ArgumentNullException("sometype");

            var code = sometype.GetHashCode();
            if (Get(sometype).State == TypeFoundStateEn.TypeFound)
            {
                return Get(sometype).TypeDebugCodeConventionName;
            }
            return null;           
        }

        #endregion  ------------------------- TypeDebugCodeConventionName && BUILD IT && GET IT ------------------------



        #region --------------------------------- ACCELERATORS --------------------------------
        
        /// <summary>
        /// Gets a value indicating whether this is Nullable{} generic type(Nullable struct type), or  array's element type  is Nullable{} type.
        /// </summary>
        /// <returns>  True, if the type parameter is a closed generic nullable type; otherwise, False. </returns>
        /// <remarks>Arrays of Nullable types are treated as Nullable types.</remarks>
        public static bool IsNullableType(Type targetType)
        {       
            return (targetType.IsGenericType && targetType.GetGenericTypeDefinition() == typeof(Nullable<>));
        }


        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types values can be setted by null Value .  They are - classes  , interface , nullable structs
        /// <para/>    2 category - these Types values can't be setted by null Value. They are - structs 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNullValueEnableType(Type targetType)
        {
            if ( targetType.IsClass 
               || targetType.IsInterface
               || (targetType.IsValueType && targetType.IsNullable())  //!
              )
            {
                return true;
            }
            else if (  targetType.IsValueType) //for structs only - 2 -types category
            {
                return false;
            }

            throw new NotSupportedException($" ERROR in [{nameof(TypeInfoEx)}.{nameof(IsNullValueEnableType)}()] -  not supported type kind Type[{targetType.FullName}] - to tell if it's instance can be setted by null value.");
        }


        /// <summary>
        /// If this targetType imlemented ICollection interface
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool Is_ICollectionType(Type targetType)
        {
            return (typeof(ICollection).IsAssignableFrom(targetType));            
        }


        static bool Is_IList_NeedToSureInDataType(Type iListType)
        {
            var argType = Get_IListArgType(iListType);
            return IsNeedToSureInDataType(argType);            
        }


        static bool Is_IDictionary_NeedToSureInKeyArgType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");
            
            var arg1KeyType = Get_IDictionaryArgTypes(iDictionaryType)[0];
            //var arg2Type = iDictionaryType.GetGenericArguments()[1];
            return (IsNeedToSureInDataType(arg1KeyType));
        }

        static bool Is_IDictionary_NeedToSureInValueArgType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");

            var arg2ValueType = Get_IDictionaryArgTypes(iDictionaryType)[1]; 
            //var arg2Type = iDictionaryType.GetGenericArguments()[1];
            return (IsNeedToSureInDataType(arg2ValueType));
        }


        static bool Is_IDictionary_NeedToSureInDataType(Type iDictionaryType)
        {
            if (iDictionaryType.IsImplement_Dictionary() == false) throw new InvalidOperationException("It's not dictionary type");

            var isKeyNeedToSoreInDataType = Is_IDictionary_NeedToSureInKeyArgType(iDictionaryType);
            var isValueNeedToSoreInDataType = Is_IDictionary_NeedToSureInValueArgType(iDictionaryType);
                        
            return ( isKeyNeedToSoreInDataType || isValueNeedToSoreInDataType);            
        }

        static bool Is_Array_NeedToSureInDataType(Type iArrayType)
        {
            var argType = iArrayType.GetElementType();
            return IsNeedToSureInDataType(argType);
        }


        /// <summary>
        /// We have 2 Type catogories : 
        /// <para/>    1 category - these Types instances can have  different value type than it's declaration type(with the help of inheritance) .  They are - not sealed classes , interface , nullable structs, enum
        /// <para/>    2 category -instances can have  different value type than it's declaration type(can't be inherited) . They are - not sealed classes and structs
        /// <para/>   NOTE : Arrays, Lists ,Dictionaries checking by their argTypes
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool IsNeedToSureInDataType(Type targetType)
        {            
            
            // Rules for Arrays \ List \ Dictionary
            if (targetType.IsArray)
            {
                return Is_Array_NeedToSureInDataType(targetType);
            }

            // firstly we checking  collection types with their args
            else if ( targetType.IsImplement_List() )                
            {
                return Is_IList_NeedToSureInDataType(targetType);
            }
            else if ( targetType.IsImplement_Dictionary() )
            {
                return Is_IDictionary_NeedToSureInDataType(targetType);
            }

            // if type is nullable we also need to sure in this type value
            //if (targetType.Is4DPrimitiveType() && targetType.IsNullable()) return false;

            //some other custom generic type - all not nullable/and nullable primitives
            if (targetType.Is4DPrimitiveType()) return false;


            if (targetType.IsEnum && targetType != typeof(Enum)  &&  !targetType.IsNullable() ) { return false;  } // needn't  to reread target Type - this enum is fully info

            //here we can be sure that this is - custom not generic type
            if ( (targetType.IsClass && !targetType.IsSealed)//for object here
                   || targetType.IsInterface
                   || (targetType.IsValueType && !targetType.IsNullable() )
                   
               ) //for not sealed classes, interface , nullable structs - 1- types category
            {
                return true;
            }
            else if ( (targetType.IsClass &&  targetType.IsSealed)
                      || targetType.IsValueType
                    ) //for not sealed classes and structs - 2 -types category
            {
                return false;
            }

            throw new NotSupportedException($" ERROR in [{nameof(TypeInfoEx)}.{nameof(IsNeedToSureInDataType)}()] -  not supported type kind Type[{targetType.FullName}]- to tell if it's instance can have  different value type than in declaration.");

        }




        /// <summary>
        /// If inputType is Nullable{TStruct} or it can be written like TStruct?,  then  return  Underlying TStruct type
        /// <para/>   If inputType is not Nullable{TStruct} then return inputType again.
        /// </summary>
        /// <param name="inputType"></param>
        /// <returns></returns>
        public static Type GetWorkingTypeFromNullableType(Type inputType)
        {
            if (IsNullableType(inputType)) // really is nullable type
            {
                return Nullable.GetUnderlyingType(inputType);
            }
            return inputType;//not nullable type 
        }

        /// <summary>
        /// 4D Default serializable Collection Type :
        /// <para/> - Array
        /// <para/> - IList based collection type
        /// <para/> - IList{T}  based collection type
        /// <para/> - IDictionary{T}  based collection type
        /// <para/> - IDictionary based collection type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static  CollectionTypeEn GetBaseCollectionType( Type type)
        {
            if (type == null) throw new ArgumentNullException("type");

            if (type.IsArray)
            {
                return CollectionTypeEn.Array;
            }
            else if (type.IsObservableCollection())
            {
                return CollectionTypeEn.ObservableCollection;
            }
            else if (type.IsImplement_List())
            {
                return CollectionTypeEn.IList;
            }
            else if (type.IsImplement_Dictionary())
            {
                return CollectionTypeEn.IDictionary;
            }
            
            else return CollectionTypeEn.NotCollection;
        }




        /// <summary>
        /// Is type based on System.Delegate class. It can be in 2 cases: 
        /// <para/> -1 when type is simply Delegate member type or 
        /// <para/> -2 when type is event member type.  
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsDelegateOrEvent( Type type)
        {
            if (type.IsNull() ) throw new ArgumentNullException("type");
            return type.IsSubclassOf(typeof(Delegate));
        }


        /// <summary>
        /// When Type has generic parameters, 
        /// and is not derive from one of the  SerializableCollections. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsSerializableType(Type type)
        {
            if ( type.IsNull()  ) throw new ArgumentNullException("type");

            if ( type.GetGenericArguments().Length > 0
                &&  GetBaseCollectionType(type) != CollectionTypeEn.NotCollection)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// 4D Type test -  Type activation by its default ctor possibility. 
        ///  By default ALL types can be tested,but only Array types don't .
        /// <para/> Problem here is that some Types can have internal static increment logic on creation by Default Ctor.
        /// <para/>  - so we'll exclude them from Default Ctor testing.        
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsTestableByDefCtorType( Type type)
        {
            if ( type.IsNull() ) throw new ArgumentNullException("type");
            
            if ( type.IsArray  )
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// If this type implement IDisposable.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsDisposableType(Type type)
        {
            if (type.IsNull()) throw new ArgumentNullException("type");

            return type.GetInterfaces().Contains(typeof(IDisposable));
        }


        #endregion  --------------------------------- ACCELERATORS --------------------------------
        


        #region --------------------------------------- ENUM UTILS ------------------------------------

       

        /// <summary>
        /// Get Enum.Field value's  TAttribute 
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TAttribute GetEnumValueAttribute<TAttribute>(Enum value)
            where TAttribute : Attribute
        {
            FieldInfo field = GetFieldInfo(value);

            return field.GetFirstFieldAttribute<TAttribute>();
            
        }


        /// <summary>
        /// Get FieldInfo of Enum.Field value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FieldInfo GetFieldInfo(Enum value)
        {
            return value.GetType().GetField(value.S());
        }

        #endregion --------------------------------------- ENUM UTILS ------------------------------------




        static List<TItem> ToList<TItem>(IEnumerable someEnumerable)
        {
            List<TItem> resultCollection = new List<TItem>();
            foreach (var item in someEnumerable)
            {
                resultCollection.Add((TItem)item);
            }

            return resultCollection;

        }


        #region --------------------------------------BCL 4D PRIMITIVE TYPES EXTENSIONS -------------------------
        

        static List<Type> Load4DPrimitives(object[] args)
        {
            List<Type> resultTypes = new List<Type>();
            ToList<TypePrimitiveEn>( typeof(TypePrimitiveEn).GetEnumValues<TypePrimitiveEn>() )//.ToList<TypePrimitivesEn>()
                      .ForEach((te) =>
                      {
                          if (resultTypes.NotContains(GetEnumValueAttribute<TypePrimitiveMapAttribute>(te).TargetType))
                          {
                              resultTypes.Add(GetEnumValueAttribute<TypePrimitiveMapAttribute>(te).TargetType);
                          }
                          
                      });

            return resultTypes;
        }

        /// <summary>
        /// 4D Primitive Types list.
        /// </summary>
       public static List<Type> PrimitiveTypes
        { get { return LA_PrimitiveTypes.Value; } }

       static LazyAct<List<Type>> LA_PrimitiveTypes = LazyAct<List<Type>>.Create( Load4DPrimitives, null, null);


        /// <summary>
        /// IsPrimitive - Is this  Type  the Primitive  DDDD.Core  Type.
        ///<para/> [Primitive Type] concept was entered in DDDD.Core to  classify  .NET types in Serialization Processing.
        ///<para/> DDDD.Core lib define an enumeration with all enabled Primitive Types in TypePrimitivesEn.        
        ///<para/> All Primitive Types are enumerated by TypePrimitivesEn values. 
        ///<para/> Each  value of TypePrimitivesEn enum can return Type with what it is associated by GetPrimitiveType().
        ///<para/> Each  value of TypePrimitivesEn enum can return TypCode with what it is associated by GetPrimitiveTypeCode()
        ///<para/>  , if it doesn't have correct TypeCode the value will be TypeCode.Empty .
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool Is4DPrimitiveType( Type type)
        {
            return PrimitiveTypes.Contains(type);
        }


        /// <summary>
        ///  TypePrimitivesEn enum value will return Primitive .NET Type with what it is associated.
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static Type Get4DPrimitiveType(TypePrimitiveEn enumValue)
        {
            return GetEnumValueAttribute<TypePrimitiveMapAttribute>(enumValue).TargetType;
        }


        /// <summary>
        /// TypePrimitivesEn enum value will return Primitive .NET TypeCode with what it is associated.
        /// <para/> , if it doesn't have correct TypeCode the value will be TypeCode.Empty 
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static TypeCode Get4DPrimitiveTypeCode( TypePrimitiveEn enumValue )
        {
            return GetEnumValueAttribute<TypePrimitiveMapAttribute>(enumValue).Code;
        }

        #endregion --------------------------------------BCL 4D PRIMITIVE TYPES EXTENSIONS -------------------------
        

    }
}





#region -------------------------------- GARBAGE -----------------------------------

//tpInfEx.IsAutoAdding = IsAutoAddingType(tpInfEx.OriginalType);

///// <summary>
///// Auto adding Type -  if Type is :
///// <para/>    interface, or enum,or Nullable struct, or some Collection Kind 
///// <para/>  -  it need no to be explicitly/or manually Added to serializer.ContractDictionary by AddContract/but Serializer should do it automatically.
///// <para/>    Such Type will be detecting and auto adding to Serializer.ContractDictionary  on Building Members of Type;
///// </summary>
///// <param name="type"></param>
///// <returns></returns>
//public static bool IsAutoAddingType( Type type)
//{
//    if (type == null) throw new ArgumentNullException("type");

//    if (IsNullableType(type)
//        || type.IsInterface
//        || type.IsEnum
//        || (GetSerializableCollection(type) != SerializableCollectionEn.NotCollection)// IS Serializable Collection
//        )
//    {
//        return true;
//    }
//    return false;

//}




/// <summary>
/// Auto adding Type -  if Type is :
/// <para/>    interface, or enum, or Nullable struct, or some Collection Kind 
/// <para/>  -  it needs no to be explicitly/or manually Added to TSS.TypesDictionary by AddKnownType()/but Serializer will do it automatically.
/// <para/>    Such Type will be detected and auto added to Serializer.ContractDictionary on Host Type  member analyzing on TSS.AddKnownType() ;
/// </summary>

//public bool IsAutoAdding
//{ get; protected set; }




#region ---------------------------------- Precompiled  Default Ctors-------------------------------------------


//LazySlim<TPart> InnerTPart { get; set; }

//public object CreateInstance() {return  InnerTPart.Value.CreateInstance(); }

//public object CreateInstance(int capacity) { return InnerTPart.Value.CreateInstance(capacity); }

//public object CreateInstance(params object[] args) { return InnerTPart.Value.CreateInstance(args); }

//public bool ContainsCtorFuncWithArgs(object[] args) { return InnerTPart.Value.ContainsCtorFuncWithArgs(args); }



//internal abstract class TPart // : TPart
//{

//    protected internal abstract bool ContainsCtorFuncWithArgs(object[] args);

//    protected internal abstract object CreateInstance(object[] args);
//    protected internal abstract object CreateInstance();
//    protected internal abstract object CreateInstance(int capacity);

//}


//internal class TPart<T> : TPart
//{

//    TPart()
//    {
//        try
//        {
//            PrecompiledCtors = TypeActivator.PrecompileCtorsT<T>();
//            Cache_DefaultCtorFunc = SelectFuncByArgs();
//            Cache_CtorWithCapacityFunc = SelectFuncByArgs(1);

//        }
//        catch (Exception exc)
//        {
//            throw new InvalidOperationException(nameof(TypeInfoEx) + "." + nameof(OriginalType) + "." + nameof(TPart) + ".Init():" + exc.Message);
//        }
//    }


//    protected internal Func<object[], T> Cache_DefaultCtorFunc = null;
//    protected internal Func<object[], T> Cache_CtorWithCapacityFunc = null;

//    Dictionary<ConstructorInfo, Func<object[], T>> PrecompiledCtors = null;




//    private Func<object[], T> SelectFuncByArgs(params object[] args)
//    {
//        Func<object[], T> targetCtorFunc = null;

//        foreach (var ctorFunc in PrecompiledCtors)
//        {
//            var argsTypes = ctorFunc.Key.GetParameters().ToTypeArray();
//            //1 не совпадают размеры массивов - break иперейти к другому конструктору
//            if (ctorFunc.Key.GetParameters().Length != args.Length) continue;

//            for (Int32 position = 0; position <= argsTypes.Length - 1; position++)
//            {
//                //2 не совпал параметр на данной позиции -  break и перейти к другому конструктору
//                //3 если все совпали и это был последний параметр значит ктор найден

//                if (argsTypes[position] != typeof(Type) && argsTypes[position] != args[position].GetType()) break;
//                if (argsTypes[position] == typeof(Type) && !(args[position] is Type)) break;


//                if (position == argsTypes.Length - 1)
//                { targetCtorFunc = ctorFunc.Value; break; }
//            }
//            if (targetCtorFunc != null) break;
//        }

//        //if (targetCtorFunc == null) throw new NotSupportedException("Ctor Delegate of type [{0}]  with such arguments wasn't found ".Fmt(typeof(T).FullName));

//        return targetCtorFunc;

//    }


//    protected internal override object CreateInstance(object[] args)
//    {
//        var func = SelectFuncByArgs(args);
//        return (func != null) ? func(args) : default(T);                
//    }

//    protected internal override object CreateInstance()
//    {
//        return (Cache_DefaultCtorFunc != null) ? Cache_DefaultCtorFunc(null) : default(T);
//    }

//    protected internal override object CreateInstance(int capacity)
//    {
//        return (Cache_CtorWithCapacityFunc != null) ? Cache_CtorWithCapacityFunc(new object[] { capacity }) : default(T);
//    }


//    protected internal override bool ContainsCtorFuncWithArgs(object[] args)
//    {
//        return (SelectFuncByArgs(args) != null);
//    }
//}


#endregion ---------------------------------- Precompiled  Default Ctor----------------------------------------





//check is IList
//else if (tpInfoEx.OriginalType.IsGenericType && typeof(IList).IsAssignableFrom(tpInfoEx.OriginalType))
//{
//    tpInfoEx.Arg1Type = (tpInfoEx.GenericArguments.Length == 1) ? tpInfoEx.GenericArguments[0] : null;
//}
//// check is IDictionary
//else if (tpInfoEx.OriginalType.IsGenericType && typeof(IDictionary).IsAssignableFrom(tpInfoEx.OriginalType))
//{
//    tpInfoEx.Arg1Type = (tpInfoEx.GenericArguments.Length == 1) ? tpInfoEx.GenericArguments[0] : null;
//    tpInfoEx.Arg2Type = (tpInfoEx.GenericArguments.Length == 2) ? tpInfoEx.GenericArguments[1] : null;
//}

//precompiled Ctors - Lazy and Thread safety
//tpInfoEx.InnerTPart =  LazySlim<TPart>.Create(() =>
//                        {
//                            return (TPart)TypeActivator.CreateInstanceBoxed(
//                                typeof(TPart<>).MakeGenericType(tpInfoEx.OriginalType));
//                        }
//                        , LazyThreadSafetyMode.PublicationOnly);





//Dictionary<Type,ushort>



///// <summary>
///// Enum Underlying Type Serialization ID - (ushort) type.. Can be used in Serializer's Type Processing Dictionary.
///// </summary>
//[IgnoreMember]
//public ushort EnumUnderlyingTypeID { get; set; }



///// <summary>
/////  Target  Type Serialization ID - (ushort) type. Can be used in Serializer's Type Processing Dictionary.
///// </summary>
//[IgnoreMember]
//public ushort TypeID { get; set; }

///// <summary>
/////  Array Element TypeID
///// </summary>
//[IgnoreMember]
//public ushort ArrayElementTypeID { get; set; }
///// <summary>
/////  List_Arg1_  or ObservableCollection_Arg1_ or  Dictionary_Arg1     TypeID 
///// </summary>
//[IgnoreMember]
//public ushort Arg1TypeID { get; private set; }
///// <summary>
///// Dictionary_Arg2     TypeID
///// </summary>
//[IgnoreMember]
//public ushort Arg2TypeID { get; private set; }    



/// <summary>
/// Contract ctor. We use LINQ precompile to create delegate from ConstructorInfo and cache it to this property.  
/// </summary>
//internal protected Func<object> DefaultCtorFunc
//{
//    get
//    {
//        return InnerTPart.Value. Cache_DefaultCtorFunc;
//    }
//}


/// <summary>
///collection Contract  ctor func for Known Started Length. For such as List/Dictionary/Array 
/// </summary>
//internal protected Func<int, object> CtorWithCapacityFunc
//{
//    get
//    {
//        return InnerTPart.Value.Cache_CtorWithCapacityFunc;
//    }
//}




//tpInfoEx.State = TypeFoundStateEn.TypeFound;
//tpInfoEx.State = TypeFoundStateEn.NotFounded;
//var testTypeValue = tpInfEx.TpAQName.FoundType.Value;
//tpInfEx.State = TypeFoundStateEn.TypeFound;

//IComparer comparer = System.Comparer.Default;
//for (int i = 1; i < values.Length; i++)
//{
//    int j = i;
//    string tempStr = names[i];
//    object val = values[i];
//    bool exchanged = false;

//    // Since the elements are sorted we only need to do one comparision, we keep the check for j inside the loop.
//    while (comparer.Compare(values[j - 1], val) > 0)
//    {
//        names[j] = names[j - 1];
//        values[j] = values[j - 1];
//        j--;
//        exchanged = true;
//        if (j == 0)
//            break;
//    }

//    if (exchanged)
//    {
//        names[j] = tempStr;
//        values[j] = val;
//    }
//}

//enumNames = names;
//enumValues = values;

//[IgnoreMember]
//String _Name = null;
///// <summary>
///// Type's Type.Name  . 
///// Will be setted only once- on creation.
///// </summary>
//public String Name
//{
//    get
//    {
//        return _Name;
//    }
//    set
//    {
//        _Name = value;
//        //OnPropertyChanged(() => ContractName);
//    }
//}



//[IgnoreMember]
//String _FullName = null;
///// <summary>
/////Type's Type.FullName.
///// </summary>       
//public String FullName
//{
//    get
//    {
//        return _FullName;
//    }
//    set
//    {
//        _FullName = value;
//        //OnPropertyChanged(() => FullName);
//    }
//}


//[IgnoreMember]//isn't serializable - AQName property is enough for it
//String _AQName = null;
///// <summary>
///// Type's Type.AssemblyQualifiedName.
///// </summary>       
//public String AQName
//{
//    get
//    {
//        return _AQName;
//    }
//    set
//    {
//        _AQName = value;
//        //OnPropertyChanged(() => AQName);
//    }
//}


//[IgnoreMember]
//String _AssemblyFullName = null;

///// <summary>
/////Contract's  Type.Assembly.FullName.
/////
///// </summary>        
//public String AssemblyFullName
//{
//    get
//    {
//        if (_AssemblyFullName == null && TargetType != null)
//        {
//            _AssemblyFullName = TargetType.Assembly.FullName;
//        }

//        return _AssemblyFullName;
//    }
//    set
//    {
//        _AssemblyFullName = value;
//        //OnPropertyChanged(() => AssemblyName);
//    }
//}


/// <summary>
///     Categories of Type can be the followings: 
///     Default adding Type - all contracts that will be added to Serializer by  Type-Serialize-Generators,
///       which( type generators) exist in current assembly or by some custom Type-Serialize-Generator;     
///     
///     Auto adding Type -  if Type is enum, or some Collection Kind, or interface-  it need no to implicitly AddContract call.
///     Such Type will be detecting and auto adding to Serializer.ContractDictionary  on Building Members of Type;
///     
///     Custom adding Type - all custom structs or class that is not  AutoAddingContract - enum, some of collection Kind, interface;
/// </summary>
//[IgnoreMember]
//public ContractCategoryEn Category { get; internal set; }


#endregion -------------------------------- GARBAGE -----------------------------------
