﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using DDDD.Core.Collections.Concurrent;
using DDDD.Core.Collections;


namespace DDDD.Core.Reflection
{


    /// <summary>
    /// TypeAccessor Creator
    /// </summary>
	public class TypeAccessorCreator
	{
		const string Create = nameof(Create);

		/// <summary>
		/// Create TypeAccessor{T} for targetType 
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="chacheItem"></param>
		/// <param name="members"></param>
		/// <returns></returns>
		public static ITypeAccessor CreateTypeAccessor(Type targetType, bool chacheItem = true, params string[] members)
		{
			var createMethod = typeof(TypeAccessor<>).MakeGenericType(targetType).GetMethod(Create);
			return createMethod.Invoke(null, new object[] { chacheItem, members }) as ITypeAccessor;
		}
	}


	/// <summary>
	/// TypeAccessor{T}" - Simply generate Accessors for Type. Memer Set 
	/// <para/>  1  
	/// <para/>   By TypeMembersSelector we compose FieldsBinding and PropertiesBinding and only then Set of Members{Fields and Properties}.
	/// <para/>  2 builds Type's Fields and Properties Data Accessors - Get/Set Delegates.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class TypeAccessor<T> : ITypeAccessor<T>
	{

		#region ------------------------------------ CTOR -----------------------------------
		
		 TypeAccessor(params string[] members)
		{
			Initialize(members);
		}
		
		#endregion ------------------------------------ CTOR -----------------------------------


		private void Initialize(string[] members)
		{
			//for Collection Type also we need To Return
			if (!(typeof(T).GetBaseCollectionType() == CollectionTypeEn.NotCollection)) return;             

			// for 4DPrimitive Types we doesn't use members get/set logic
			if (typeof(T).Is4DPrimitiveType()) return;

			if (members == null || members.Length == 0 ) { return; }

			var realWorkableType = typeof(T).GetWorkingTypeFromNullableType();
			
			//load all members into activator
			foreach (var mmbr in members)
			{
				TryAddMember(realWorkableType.GetMember(mmbr, DefaultMembersBinding)
											.FirstOrDefault());                    
			}           
		}


		#region -------------------------------- FIELDS ---------------------------------

		const BindingFlags DefaultMembersBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

		private static object locker = new object();

		#endregion -------------------------------- FIELDS ---------------------------------


		#region ---------------------- CACHED TypeACCESSORS --------------------------

		static readonly SafeDictionary2<Type, ITypeAccessor> CachedTypeAccessors = new SafeDictionary2<Type, ITypeAccessor>();

		#endregion ---------------------- CACHED TypeACCESSORS --------------------------


		/// <summary>
		/// T Type Accessor to its members  
		/// </summary>        
		public Type TAccess
		{
			get
			{
				return typeof(T);
			}
		}

		/// <summary>
		/// Target Type Detailed Description
		/// </summary>
		public TypeInfoEx TAccessEx
		{
			get
			{
				return TypeInfoEx.Get(typeof(T));
			}
		}




		/// <summary>
		///  Create TypeAccessor{T}.
		///  <para/> TypeAccessor{T} contains it's own separate Cache Dictionary where we always caching TypeAccessor{T} if {T} Type is new and [chacheItem] param value is true.
		/// </summary>
		/// <param name="chacheItem"></param>
		/// <param name="members"></param>
		/// <returns></returns>
		public static ITypeAccessor<T> Create(bool chacheItem = true, params string[] members)
		{
			var targetType = typeof(T);
			if (CachedTypeAccessors.ContainsKey(targetType)) return CachedTypeAccessors[targetType] as ITypeAccessor<T>;

			lock (locker)
			{
				if ( !CachedTypeAccessors.ContainsKey(targetType))
				{
					var tpAccessor = new TypeAccessor<T>(members);
					CachedTypeAccessors.Add(targetType, tpAccessor);
				}                    
			}

			return CachedTypeAccessors[targetType] as ITypeAccessor<T>;
		}
				

		#region ---------------------  ACCESS MEMBERS - NONSERIALIZABLE -------------------------------

		/// <summary>
		/// Internals Members Dictionary
		/// </summary>
		[IgnoreMember]
		public Dictionary<string, MemberInfo> Members
		{ get; private set; }  = new Dictionary<string, MemberInfo>();



		/// <summary>
		/// Fields of current (T)-Type that we get  from ITypeSerializer
		/// </summary>
		[IgnoreMember]
		public List<KeyValuePair<string, MemberInfo>> Fields
		{
			get
			{
				return Members.Where(mm => mm.Value.MemberType == MemberTypes.Field).ToList();
			}            
		}


		/// <summary>
		/// Properties of current (T)-Type that we get from ITypeSerializer
		/// </summary>  
		[IgnoreMember]
		public List<KeyValuePair<string, MemberInfo>> Properties
		{
			get
			{
				return Members.Where(mm => mm.Value.MemberType == MemberTypes.Property).ToList();
			}            
		}





		/// <summary>
		/// If Member with such key doesn't exist add it to TypeAccessor Members.
		/// <para/>If Member wirh such NAme already exist  but Types are different - we remove previos and add this Member.
		/// <para/>If Member wirh such NAme and Type already exist then  Accessor returns null.
		/// </summary>
		/// <param name="member"></param>
		void TryAddMember(MemberInfo member)
		{
			if (member == null) return;
			
			if ( Members.ContainsKey( member.Name )  &&  Members[member.Name].MemberType ==  member.MemberType )
			{  return;  }
			else
			{
				
					if (Members.ContainsKey(member.Name) && Members[member.Name].MemberType == member.MemberType)
					{ return; }

					Members.Add(member.Name, member);

					//Resorting && Reindexing
					Members = Members.OrderBy( k =>k.Key).ToDictionary(k => k.Key, k => k.Value);
					int index = 0;
					MemberIndexes = Members.ToDictionary(k => k.Key, k => index++);

					AddMemberGetDelegate(member);
					AddMemberSetDelegate(member);				
				
			}
		}
		
		/// <summary>
		/// Remove Member if it ealready exist in Members Dictionary
		/// </summary>
		/// <param name="Member"></param>
		void TryRemoveMember(string MemberName)
		{
			if (MemberName.IsNullOrEmpty()) return;

			if (!Members.ContainsKey(MemberName))
			{ return; }
			else
			{
				
					if (!Members.ContainsKey(MemberName))
					{ return; }

					Members.Remove(MemberName);

					//Remove Setters and Getters Expressions and Delegates
					MemberSetterExpressions.Remove(MemberName);
					MemberSetters.Remove(MemberName);

					MemberGetterExpressions.Remove(MemberName);
					MemberGetters.Remove(MemberName);

					//Resorting && Reindexing
					Members = Members.OrderBy(k => k.Key).ToDictionary(k => k.Key, k => k.Value);
					int index = 0; 
					MemberIndexes = Members.ToDictionary(k => k.Key, k=> index++);
				
			}
		}



		#endregion ---------------------  ACCESS MEMBERS - NONSERIALIZABLE -------------------------------



		#region ------------------------- CACHED MEMBERS INDEXES ---------------------------


		/// <summary>
		/// Indexes of sorted  by name TargetType.Members.
		/// It's common index dictionary for fields and properties.
		/// </summary>
		Dictionary<string, int> MemberIndexes = new Dictionary<string, int>();
		

		[Conditional("DEBUG")]
		void ContainsMemberInIndexedDictionary(string memberName)
		{
			if (!MemberIndexes.ContainsKey(memberName))
			{
				throw new InvalidOperationException($" Type-[{TAccess.FullName}] MemberIndexes Dict doesn't contains Key-[{memberName}]  ");
			}

		}

		/// <summary>
		/// Inernal index of memberName of  {T} in current accessor. 
		/// </summary>
		/// <param name="memberName"></param>
		/// <returns></returns>
		public int GetMemberIndex(string memberName)
		{
			ContainsMemberInIndexedDictionary(memberName);

			return MemberIndexes[memberName];
		}


		/// <summary>
		/// Get  MemberInfo by memberName of {T} in current accessor. If  memberName don't exist then method return null  value.
		/// </summary>
		/// <param name="memberName"></param>
		/// <returns></returns>
		public MemberInfo GetMemberInfo(string memberName)
		{
			if (Members.ContainsKey(memberName) ) 
			{
			return Members[memberName];
			}
			return null;
		}


		#endregion ------------------------- CACHED MEMBERS INDEXES ---------------------------


		

		#region ---------------------------------- Precompiled  Default Ctor -------------------------------------------



		/// <summary>
		/// Contract ctor. We use LINQ precompile to create delegate from ConstructorInfo and cache it to this property.  
		/// </summary>
		internal protected Func<T> DefaultCtorFunc { get; internal set; }


		/// <summary>
		/// Collection Contract  ctor func for Known Started Length. For such as List/Dictionary/Array 
		/// </summary>
		internal protected Func<int, T> CtorWithCapacityFunc { get; internal set; }



		#endregion ---------------------------------- Precompiled  Default Ctor----------------------------------------
		


		#region -------------------------------------  MEMBER   GETTERS ---------------------------------------


		#region --------------------------------------- ADD /USE GETTER -----------------------------------------


		public TMember GetMember<TMember>(string MemberKey, T instance)
		{
			return (TMember)MemberGetters[MemberKey](instance);
		}


		public TMember GetMember<TMember>(string MemberKey, object instance)
		{
			return GetMember<TMember>(MemberKey, (T)instance);
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="member"></param>
		public void AddMemberGetDelegate(MemberInfo member)
		{
			if (!MemberGetters.ContainsKey(member.Name))
			{               
				if (!MemberGetters.ContainsKey(member.Name) && !MemberGetters.ContainsKey(member.Name))
				{
					MemberGetterExpressions[member.Name] = GenerateMemberGetterBoxedExpression(member, TAccess.IsNullable());
					MemberGetters[member.Name] = MemberGetterExpressions[member.Name].Compile();
				}               
			}
		}


		#endregion --------------------------------------- ADD /USE GETTER -----------------------------------------


		#region ----------------------------------- GETTERS GENERATION --------------------------------------

		// T - host
		// TMember - one of Member type

		private readonly Dictionary<string, Expression<Delegate>> MemberGetterExpressionsT = new Dictionary<string, Expression<Delegate>>();//
		private readonly Dictionary<string, Delegate> MemberGettersT = new Dictionary<string, Delegate>();

		private readonly Dictionary<string, Expression<Func<T, object>>> MemberGetterExpressions = new Dictionary<string, Expression<Func<T, object>>>();
		private readonly Dictionary<string, Func<T, object>> MemberGetters = new Dictionary<string, Func<T, object>>();

		private Func<T, object> GenerateMemberGetterBoxed(MemberInfo member)
		{
			return GenerateMemberGetterBoxedExpression(member , TAccess.IsNullable()).Compile();
		}


		private static Expression<Func<T, object>> GenerateMemberGetterBoxedExpression(MemberInfo member, bool isNullableValueType)
		{
			if (isNullableValueType)         // for nullable valueType - struct? or Nullable_Tp_
			{
				if (member is FieldInfo)
				{
					return GenerateFieldGetterForNullableBoxedExpression(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					return GeneratePropertyGetterForNullableBoxedExpression(member as PropertyInfo);
				}
			}
			else if (!isNullableValueType) // for not nullable valueType - struct 
			{
				if (member is FieldInfo)
				{
					return GenerateFieldGetterBoxedExpression(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					return GeneratePropertyGetterBoxedExpression(member as PropertyInfo);
				}
			}

			throw new InvalidOperationException("GenerateMemberGetterExpression() Cannot generate Get Delegate besides than for Properties and Fields member types");

		}


		private static Expression<Func<T, TMember>> GenerateMemberGetterTMemberExpression<TMember>(MemberInfo member, bool isNullableValueType)
		{
			if (isNullableValueType)         // for nullable valueType - struct? or Nullable_Tp_
			{
				if (member is FieldInfo)
				{
					return GenerateFieldGetterForNullableTMemberExpression<TMember>(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					//return GeneratePropertyGetterForNullableBoxedExpression  GeneratePropertyGetterForNullableTExpression(member as PropertyInfo);
				}
			}
			else if (!isNullableValueType) // for not nullable valueType - struct 
			{
				if (member is FieldInfo)
				{
					return GenerateFieldGetterTMemberExpression<TMember>(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					return GeneratePropertyGetterTMemberExpression<TMember>(member as PropertyInfo);
				}
			}

			throw new InvalidOperationException("GenerateMemberGetterExpression() Cannot generate Get Delegate besides than for Properties and Fields member types");
		}




		private static Expression<Func<T, object>> GenerateFieldGetterBoxedExpression(FieldInfo fieldInfo)
		{
			var param_classThis = Expression.Parameter(typeof(T), "instance"); //

			var member = Expression.Field(param_classThis, fieldInfo);
			return Expression.Lambda<Func<T, object>>(Expression.Convert(member, typeof(object))
														, "GetFld__" + fieldInfo.Name
														, new[] { param_classThis });
		}

		private static Expression<Func<T, TMember>> GenerateFieldGetterTMemberExpression<TMember>(FieldInfo fieldInfo)
		{
			var param_classThis = Expression.Parameter(typeof(T), "instance"); //

			var member = Expression.Field(param_classThis, fieldInfo);
			return Expression.Lambda<Func<T, TMember>>(Expression.Convert(member, typeof(TMember))
														, "GetFld__" + fieldInfo.Name
														, new[] { param_classThis });
		}


		private static Expression<Func<T, object>> GeneratePropertyGetterBoxedExpression(PropertyInfo propertyInfo)
		{
			var param_classThis = Expression.Parameter(typeof(T), "instance");

			var member = Expression.Property(param_classThis, propertyInfo);
			return Expression.Lambda<Func<T, object>>(Expression.Convert(member, typeof(object))
														, "GetPrty__" + propertyInfo.Name
														, new[] { param_classThis });
		}
		
		private static Expression<Func<T, TMember>> GeneratePropertyGetterTMemberExpression<TMember>(PropertyInfo propertyInfo)
		{
			var param_classThis = Expression.Parameter(typeof(T), "instance");

			var member = Expression.Property(param_classThis, propertyInfo);
			return Expression.Lambda<Func<T, TMember>>(Expression.Convert(member, typeof(TMember))
														, "GetPrty__" + propertyInfo.Name
														, new[] { param_classThis });
		}




		private static Expression<Func<T, object>> GenerateFieldGetterForNullableBoxedExpression(FieldInfo fieldInfo)
		{
			var param_structTNullableInstance = Expression.Parameter(typeof(T), "nullableInstance");

			var ValueProperty = Expression.Property(param_structTNullableInstance, typeof(T).GetProperty("Value"));
			var member = Expression.Field(ValueProperty, fieldInfo);

			var variableMemberValue = Expression.Variable(typeof(object), "memberValue");
			var ifBlock =
							Expression.Block(typeof(object),

												 new[] { variableMemberValue }
												, Expression.IfThen(Expression.NotEqual(param_structTNullableInstance, Expression.Constant(null))
																	, Expression.Assign(variableMemberValue, Expression.Convert(member, typeof(object)))
																	)
												, variableMemberValue
											);


			return Expression.Lambda<Func<T, object>>(ifBlock
													   , "GetFld__" + fieldInfo.Name
													   , new[] { param_structTNullableInstance });
		}
		
		private static Expression<Func<T, TMember>> GenerateFieldGetterForNullableTMemberExpression<TMember>(FieldInfo fieldInfo)
		{
			var param_structTNullableInstance = Expression.Parameter(typeof(T), "nullableInstance");

			var ValueProperty = Expression.Property(param_structTNullableInstance, typeof(T).GetProperty("Value"));
			var member = Expression.Field(ValueProperty, fieldInfo);

			var variableMemberValue = Expression.Variable(typeof(TMember), "memberValue");
			var ifBlock =
							Expression.Block(typeof(TMember),

												 new[] { variableMemberValue }
												, Expression.IfThen(Expression.NotEqual(param_structTNullableInstance, Expression.Constant(null))
																	, Expression.Assign(variableMemberValue, Expression.Convert(member, typeof(TMember)))
																	)
												, variableMemberValue
											);


			return Expression.Lambda<Func<T, TMember>>(ifBlock
													   , "GetFld__" + fieldInfo.Name
													   , new[] { param_structTNullableInstance });
		}


		private static Expression<Func<T, object>> GeneratePropertyGetterForNullableBoxedExpression(PropertyInfo propertyInfo)
		{
			var param_structTNullableInstance = Expression.Parameter(typeof(T), "nullableInstance");

			var ValueProperty = Expression.Property(param_structTNullableInstance, typeof(T).GetProperty("Value"));
			var member = Expression.Property(ValueProperty, propertyInfo);

			var variableMemberValue = Expression.Variable(propertyInfo.PropertyType, "memberValue");

			var ifBlock =
				Expression.Block(typeof(object)

								   , new[] { variableMemberValue }
								   , Expression.IfThen(Expression.NotEqual(param_structTNullableInstance, Expression.Constant(null))
													, Expression.Assign(variableMemberValue, Expression.Convert(member, typeof(object)))
													)
								  , variableMemberValue
								);


			return Expression.Lambda<Func<T, object>>(ifBlock
													  , "GetPrty__" + propertyInfo.Name
													  , new[] { param_structTNullableInstance });
		}

		private static Expression<Func<T, TMember>> GeneratePropertyGetterForNullableTMemberExpression<TMember>(PropertyInfo propertyInfo)
		{
			var param_structTNullableInstance = Expression.Parameter(typeof(T), "nullableInstance");

			var ValueProperty = Expression.Property(param_structTNullableInstance, typeof(T).GetProperty("Value"));
			var member = Expression.Property(ValueProperty, propertyInfo);

			var variableMemberValue = Expression.Variable(propertyInfo.PropertyType, "memberValue");

			var ifBlock =
				Expression.Block(typeof(TMember)

								   , new[] { variableMemberValue }
								   , Expression.IfThen(Expression.NotEqual(param_structTNullableInstance, Expression.Constant(null))
													, Expression.Assign(variableMemberValue, Expression.Convert(member, typeof(TMember)))
													)
								  , variableMemberValue
								);


			return Expression.Lambda<Func<T, TMember>>(ifBlock
													  , "GetPrty__" + propertyInfo.Name
													  , new[] { param_structTNullableInstance });

		}


		#endregion ----------------------------------- GETTERS GENERATION --------------------------------------


		#endregion -------------------------------------  MEMBER   GETTERS ---------------------------------------



		#region --------------------------------- MEMBER SETTERS -------------------------------

		public delegate void TypeMemberSetterBoxedDelegate(ref T instance, object value);

		public delegate void TypeMemberSetterTMemberDelegate<TMember>(ref T instance, TMember value);
	
		#region --------------------------------------- ADD USE SETTER ----------------------------------------


		/// <summary>
		/// Set T-Contract  Member new Value by [MemberKey] 
		/// </summary>
		/// <typeparam name="TMember"></typeparam>
		/// <param name="MemberKey"></param>
		/// <param name="instance"></param>
		/// <param name="MemberNewValue"></param>
		public void SetMember<TMember>(string MemberKey, ref T instance, TMember MemberNewValue)
		{
			MemberSetters[MemberKey](ref instance, MemberNewValue);
		}

		public void SetMember<TMember>(string MemberKey, ref object instance, TMember MemberNewValue)
		{
			T valueInstance = (T)instance;
			SetMember<TMember>(MemberKey, ref valueInstance, MemberNewValue);
		}

		public void AddMemberSetDelegate(MemberInfo member)
		{
			if (!MemberSetters.ContainsKey(member.Name) && !MemberSetters.ContainsKey(member.Name))
			{
				//lock (_locker)
				//{
					if (!MemberSetters.ContainsKey(member.Name) && !MemberSetters.ContainsKey(member.Name))
					{
						MemberSetterExpressions[member.Name] = GenerateValueTypeMemberSetterExpression(member, TAccess.IsNullable());
						MemberSetters[member.Name] = MemberSetterExpressions[member.Name].Compile();

					}
				//}
			}

		}


		#endregion --------------------------------------- ADD USE SETTER ----------------------------------------


		#region --------------------------------------- SETTERS GENERATION ---------------------------------------
		
		Dictionary<string, Expression<TypeMemberSetterBoxedDelegate>> MemberSetterExpressions = new Dictionary<string, Expression<TypeMemberSetterBoxedDelegate>>();
		Dictionary<string, TypeMemberSetterBoxedDelegate> MemberSetters = new Dictionary<string, TypeMemberSetterBoxedDelegate>();


		private static TypeMemberSetterBoxedDelegate GenerateValueTypeMemberSetter(MemberInfo member, bool isNullableValueType)
		{
			return GenerateValueTypeMemberSetterExpression( member, isNullableValueType ).Compile();
		}
		private static TypeMemberSetterBoxedDelegate GenerateValueTypeFieldSetter(FieldInfo fi)
		{
			return GenerateValueTypeFieldSetterExpression(fi).Compile();
		}
		private static TypeMemberSetterBoxedDelegate GenerateValueTypePropertySetter(PropertyInfo pi)
		{
			return GenerateValueTypePropertySetterExpression(pi).Compile();
		}

		#region ---------------------------------- VALUE TYPES SETTERS ------------------------------------


		private static Expression<TypeMemberSetterBoxedDelegate> GenerateValueTypeMemberSetterExpression(MemberInfo member, bool isNullableValueType)
		{
			if (isNullableValueType)         // for nullable valueType - struct? or Nullable_Tp_
			{
				if (member is FieldInfo)
				{
					return GenerateValueTypeFieldSetterForNullableTExpression(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					return GenerateValueTypePropertySetterForNullableTExpression(member as PropertyInfo);
				}
			}
			else
			{
				if (member is FieldInfo)
				{
					return GenerateValueTypeFieldSetterExpression(member as FieldInfo);
				}
				else if (member is PropertyInfo)
				{
					return GenerateValueTypePropertySetterExpression(member as PropertyInfo);
				}
			}

			return null;
		}



		private static Expression<TypeMemberSetterBoxedDelegate> GenerateValueTypeFieldSetterForNullableTExpression(FieldInfo fi)
		{
			var param_NullableStruct = Expression.Parameter( typeof(T).MakeByRefType(), "nullableInstance");     //nullableInstance
			var param_value = Expression.Parameter(typeof(object), "value");

			var varStructValue = Expression.Variable(typeof(T).GetWorkingTypeFromNullableType() , "structValue");

			var ifBlock = Expression.IfThenElse(Expression.NotEqual(param_NullableStruct, Expression.Constant(null))
												, //true 
													Expression.Block(typeof(void), new[] { varStructValue },
																	 Expression.Assign(varStructValue, Expression.Property(param_NullableStruct, "Value"))
																   , Expression.Assign(Expression.Field(varStructValue, fi), Expression.Convert(param_value, fi.FieldType))
																   , Expression.Assign(param_NullableStruct, Expression.Convert(varStructValue, typeof(T)))
																	)
											//false
													, Expression.Constant(null)
												);

			var bodyExpression = Expression.Block(typeof(void), new[] { varStructValue }, ifBlock);

			return Expression.Lambda<TypeMemberSetterBoxedDelegate>(bodyExpression
																		, "SetFld__" + fi.Name
																		, new[] { param_NullableStruct, param_value });


		}



		private static Expression<TypeMemberSetterBoxedDelegate> GenerateValueTypePropertySetterForNullableTExpression(PropertyInfo pi)
		{
			var param_NullableStruct = Expression.Parameter(typeof(T).MakeByRefType(), "nullableInstance");     //nullableInstance
			var param_value = Expression.Parameter(typeof(object), "value");

			var varStructValue = Expression.Variable(typeof(T).GetWorkingTypeFromNullableType() , "structValue");

			var ifBlock = Expression.IfThenElse(Expression.NotEqual(param_NullableStruct, Expression.Constant(null))
													, //true 
													Expression.Block(typeof(void), new[] { varStructValue },
																	 Expression.Assign(varStructValue, Expression.Property(param_NullableStruct, "Value"))
																   , Expression.Assign(Expression.Property(varStructValue, pi), Expression.Convert(param_value, pi.PropertyType))
																   , Expression.Assign(param_NullableStruct, Expression.Convert(varStructValue, typeof(T)))
																	)
				//false
													, Expression.Constant(null)
												);

			var bodyExpression = Expression.Block(typeof(void), new[] { varStructValue }, ifBlock);

			return Expression.Lambda<TypeMemberSetterBoxedDelegate>(bodyExpression
																	   , "SetPrty__" + pi.Name
																	   , new[] { param_NullableStruct, param_value });


		}



		private static Expression<TypeMemberSetterBoxedDelegate> GenerateValueTypeFieldSetterExpression(FieldInfo fi)
		{
			var instanceRefType = typeof(T).MakeByRefType();

			var param_struct = Expression.Parameter(instanceRefType, "instance");
			var param_value = Expression.Parameter(typeof(object), "value");

			var field = Expression.Field(param_struct, fi);
			var cast_param = Expression.Convert(param_value, fi.FieldType);
			var assign = Expression.Assign(field, cast_param);

			var lambda = Expression.Lambda<TypeMemberSetterBoxedDelegate>(assign
																			, "SetFld__" + fi.Name
																			, new[] { param_struct, param_value });


			return lambda;

		}



		private static Expression<TypeMemberSetterBoxedDelegate> GenerateValueTypePropertySetterExpression(PropertyInfo pi)
		{
			var instanceRefType = typeof(T).MakeByRefType();

			var param_struct = Expression.Parameter(instanceRefType, "instance");

			var param_value = Expression.Parameter(typeof(object), "value");

			var Property = Expression.Property(param_struct, pi);
			var cast_param = Expression.Convert(param_value, pi.PropertyType);
			var assign = Expression.Assign(Property, cast_param); // ExpressionEx.Assign

			var lambda = Expression.Lambda<TypeMemberSetterBoxedDelegate>(assign
																			 , "SetPrty__" + pi.Name
																			   , new[] { param_struct, param_value });


			return lambda;

		}


		#endregion ----------------------------------VALUE TYPES SETTERS ------------------------------------

		#endregion --------------------------------------- SETTERS GENERATION ---------------------------------------
		
		#endregion ---------------------------------  MEMBER SETTERS -------------------------------
		
	   

		/// <summary>
		/// If Contract  Is Collection then Creating instance by CtorWithCapacityFunc(length) - Collection of Length = = [length].
		///<para/> If Contract  Is not  Collection then Creating instance by CtorFunc().        
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		protected internal T CreateDefaultValue(int length = 0)
		{
			if (TAccess.GetBaseCollectionType() != CollectionTypeEn.NotCollection && CtorWithCapacityFunc != null)
				return CtorWithCapacityFunc(length); //some collections can -doesn't contain ctor with Length(ObservableCollection )
			else return DefaultCtorFunc();
		}
		


		#region ----------------------------------- ANALYZE/BUILD TYPE MEMBERS | DELEGATES -------------------------------------
		

		internal protected virtual void BuildMembersGettersSetters()
		{

			foreach (var fld in Fields)
			{
				AddMemberGetDelegate(fld.Value);
				AddMemberSetDelegate(fld.Value);
			}


			foreach (var prop in Properties)
			{
				AddMemberGetDelegate(prop.Value);
				AddMemberSetDelegate(prop.Value);
			} 
		}


		#endregion----------------------------------- ANALYZE/BUILD TYPE MEMBERS | DELEGATES  -------------------------------------



		//TypeAccessor<T> - GETTER SETTER DELEGATES
		#region ----------------------------------- BUILD MEMBERS  INDEPENDENT FROM SERIALIZER ------------------------------------

#if NET45 || NET46


		/// <summary>
		/// 
		/// </summary>
		/// <param name="typeBiulder"></param>
		public void GenerateTypeAccessor(TypeBuilder typeBiulder)
		{

			foreach (var itemGetMethod in MemberGetterExpressions)
			{
				try
				{
					var itemGetMB = typeBiulder.DefineMethod("Get__" + itemGetMethod.Key
												  , MethodAttributes.Public | MethodAttributes.Static
												  , typeof(object)//return type
												  , new[] { typeof(object) } //instance 
												  );

					itemGetMB.DefineParameter(1, ParameterAttributes.None, "instance");
					//itemGetMB.Define (0, ParameterAttributes.None, "instance");

					itemGetMethod.Value.CompileToMethod(itemGetMB);


				}
				catch (Exception)
				{

					throw;
				}

			}


			foreach (var itemSetMethod in MemberSetterExpressions)
			{
				try
				{

					var itemSetMB = typeBiulder.DefineMethod("Set__" + itemSetMethod.Key
													, MethodAttributes.Public | MethodAttributes.Static
													, null//return none only set
													, new[] { typeof(object), typeof(object) } //instance , new value
													);

					itemSetMB.DefineParameter(1, ParameterAttributes.None, "this");
					itemSetMB.DefineParameter(2, ParameterAttributes.None, "value");

					itemSetMethod.Value.CompileToMethod(itemSetMB);
				}
				catch (Exception)
				{
					throw;
				}
			}

		}

#endif

		public Type[] CollectMemberTypes()
		{
			List<Type> resultMemberTypes = new List<Type>();
			foreach (var member in Members)
			{ resultMemberTypes.Add(member.Value.GetMemberType());
			}

			return resultMemberTypes.ToArray();
		}




		#endregion ----------------------------------- BUILD MEMBERS  INDEPENDENT FROM SERIALIZER ------------------------------------











	}
}







#region -------------------------------------- GARBAGE ----------------------------------


#region ---------------------------------- Precompiled  Default Ctors-------------------------------------------


//LazySlim< TPart > InnerTPart            //<T>
//{ get; } = LazySlim<TPart>.Create(  () =>  {    return TypeActivator.CreateInstanceT<TPart>();  }  ); //<T>

//public T CreateInstanceT() { return TypeActivator.CreateInstanceT<T>(); }
//public T CreateInstanceT(int capacity) { return InnerTPart.Value.CreateInstanceT(capacity); }
//public T CreateInstanceT(params object[] args) { return InnerTPart.Value.CreateInstanceT(args); }

//public bool ContainsCtorFuncWithArgs(object[] args) { return InnerTPart.Value.ContainsCtorFuncWithArgs(args); }


////internal abstract class TPart
////{

////    protected internal abstract bool ContainsCtorFuncWithArgs(object[] args);

////    protected internal abstract object GetInstance(object[] args);
////    protected internal abstract object GetInstance();
////    protected internal abstract object GetInstance(int capacity);

////}


//internal class TPart //: TPart //<T> <T>
//{

//    TPart()
//    {
//        try
//        {
//            PrecompiledCtors = TypeActivator.PrecompileCtorsT<T>();
//            Cache_DefaultCtorFunc = SelectFuncByArgs();
//            Cache_CtorWithCapacityFunc = SelectFuncByArgs(1);//ctor with one int arg

//        }
//        catch (Exception exc)
//        {
//            throw new InvalidOperationException(nameof(TypeInfoEx) + "." + nameof(T) + "." + nameof(TPart) + ".Init():" + exc.Message);
//        }
//    }


//    protected internal Func<object[], T> Cache_DefaultCtorFunc = null;
//    protected internal Func<object[], T> Cache_CtorWithCapacityFunc = null;

//    Dictionary<ConstructorInfo, Func<object[], T>> PrecompiledCtors = null;




//    private Func<object[], T> SelectFuncByArgs(params object[] args)
//    {
//        Func<object[], T> targetCtorFunc = null;

//        foreach (var ctorFunc in PrecompiledCtors)
//        {
//            var argsTypes = ctorFunc.Key.GetParameters().ToTypeArray();
//            //1 не совпадают размеры массивов - break иперейти к другому конструктору
//            if (ctorFunc.Key.GetParameters().Length != args.Length) continue;

//            for (Int32 position = 0; position <= argsTypes.Length - 1; position++)
//            {
//                //2 не совпал параметр на данной позиции -  break и перейти к другому конструктору
//                //3 если все совпали и это был последний параметр значит ктор найден

//                if (argsTypes[position] != typeof(Type) && argsTypes[position] != args[position].GetType()) break;
//                if (argsTypes[position] == typeof(Type) && !(args[position] is Type)) break;


//                if (position == argsTypes.Length - 1)
//                { targetCtorFunc = ctorFunc.Value; break; }
//            }
//            if (targetCtorFunc != null) break;
//        }

//        //if (targetCtorFunc == null) throw new NotSupportedException("Ctor Delegate of type [{0}]  with such arguments wasn't found ".Fmt(typeof(T).FullName));

//        return targetCtorFunc;

//    }


//    protected internal  T CreateInstanceT(object[] args)//override
//    {
//        var func = SelectFuncByArgs(args);
//        return (func != null) ? func(args) : default(T);
//    }

//    protected internal  T CreateInstanceT()//override
//    {
//        return (Cache_DefaultCtorFunc != null) ? Cache_DefaultCtorFunc(null) : default(T);
//    }

//    protected internal T CreateInstanceT(int capacity)//override 
//    {
//        return (Cache_CtorWithCapacityFunc != null) ? Cache_CtorWithCapacityFunc(new object[] { capacity }) : default(T);
//    }


//    protected internal bool ContainsCtorFuncWithArgs(object[] args)//override 
//    {
//        return (SelectFuncByArgs(args) != null);
//    }
//}


#endregion ---------------------------------- Precompiled  Default Ctor----------------------------------------
	

#region ------------------------------- TypeAccessor STATE -----------------------------------

/// <summary>
/// True -yes mismatches Exist
/// False - equals
/// </summary>
/// <returns></returns>
//bool IsMembersCountMismatchFromGettersOrSetters()
//{
//    //1 - simply not satisfied  by counts 
//    if (Members.Count != 0 
//        && (Members.Count != MemberGetters.Count || Members.Count != MemberSetters.Count)
//        )
//    {  return true;
//    }  


//    //compare  MemberGetters&&MemberSetters By names 




//    return false;
//}



//void CheckAndChangeState()
//{
//    if (Members.Count == 0) 
//    {
//        _State = TypeAccessorStateEn.NoMembersAdded;
//    }
//    else if (IsMembersCountMismatchFromGettersOrSetters() == true)
//    {
//        _State = TypeAccessorStateEn.NeedToGenerate;
//    }
//    else if (IsMembersCountMismatchFromGettersOrSetters() == false)
//    {
//        _State = TypeAccessorStateEn.Generated;
//    }
//}



//TypeAccessorStateEn _State;

///// <summary>
///// 
///// </summary>
//public TypeAccessorStateEn State
//{
//    get
//    {
//        return _State;
//    }
//    private set
//    {
//        if (value != _State)
//        {
//            _State = value;
//        }                
//    }
//}



#endregion ------------------------------- TypeAccessor STATE -----------------------------------


///// <summary>
///// Analyzing and Building TypeProcessor  Fields and Properties
///// </summary>
///// <param name="processingItem"></param>
///// <param name="HostSerializer"></param>
//public void BuildMembersTypeProcessor() //ITypeSetSerializer HostSerializer
//{
//    AnalyzeBuildFieldsInternal(); //, HostSerializer
//    AnalyzeBuildPropertiesInternal(); //, HostSerializer

//    CheckAllOnlyMembersFounded();

//    //now build Get Set Delegates
//    BuildMembersGetSetDelegates();
//}

// /// <summary>
///// If PropertyType is interface or enum we Add them 
///// </summary>
///// <param name="fields"></param>
///// <param name="typeProcessor"></param>
//private static void CollectAutoItemsFromFields(FieldInfo[] fields)  // , TypeProcessor typeProcessor ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var fld in fields)
//    {
//        //Type compareType = Tools.GetWorkingTypeFromNullableType(fld.FieldType);

//        ushort? typeId = 0;
//        if ((typeId = typeProcessor.HostSerializer.GetContainsContract(fld.FieldType)) != null) continue;

//        typeProcessor.HostSerializer.DetectAddAutoContract(fld.FieldType);                

//        if (typeof(IEnumerable).IsAssignableFrom(fld.FieldType)) continue;

//        //если это все же класс или структура, но только не инам -то он уже должен быть в словаре Контрактов иначе это ошибка
//        if ((fld.FieldType.IsClass || fld.FieldType.IsValueType) && !fld.FieldType.IsEnum)
//        {
//            Validator.AssertTrue<InvalidOperationException>(typeId == null,
//                "Member Analyze error: Service Serializer Dictionary doesn't contain FieldType  [{0}]. Look at the contract [{1}] in field [{2}]",
//               fld.FieldType, typeProcessor.Contract, fld.Name);
//        }
//    }
//}


///// <summary>
///// If PropertyType is interface or enum we Add them
///// </summary>
///// <param name="properties"></param>
///// <param name="typeProcessor"></param>
//private  void CollectAutoItemsFromProperties(PropertyInfo[] properties)  //  , TypeProcessor typeProcessor  ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var prp in properties)
//    {
//        ushort? typeId;
//        if ((typeId = typeProcessor.HostSerializer.GetContainsContract(prp.PropertyType)) != null) continue;

//        typeProcessor.HostSerializer.DetectAddAutoContract( prp.PropertyType );

//        if (typeof(IEnumerable).IsAssignableFrom(prp.PropertyType)) continue;

//        //если это все же класс или структура, но не инам - то они уже должен быть в словаре Контрактов иначе это ошибка
//        if ((prp.PropertyType.IsClass || prp.PropertyType.IsValueType) && !prp.PropertyType.IsEnum)
//        {
//            Validator.AssertTrue<InvalidOperationException>(
//                typeId == null,
//            "Member Analyze error:  Serializer Dictionary doesn't contain PropertyType  [{0}]. Look at the contract [{1}] in property [{2}]",
//             prp.PropertyType, WorkingType, prp.Name);
//        }

//    }
//}


#endregion -------------------------------------- GARBAGE ----------------------------------



