﻿
using System;
using System.Collections.Generic;
using System.Reflection;

using DDDD.Core.Collections;

namespace DDDD.Core.Reflection
{
    
    public interface ITypeAccessor// no <T>
    {
        /// <summary>
        /// Target TypeAccessor Type
        /// </summary>
        Type TAccess { get; }

        /// <summary>
        /// TypeInfoEx about this TAccess Type 
        /// </summary>
        TypeInfoEx TAccessEx { get;  }



        Dictionary<string, MemberInfo> Members { get; }

        List<KeyValuePair<string, MemberInfo>> Fields { get; }

        List<KeyValuePair<string, MemberInfo>> Properties { get; }




        void AddMemberGetDelegate(MemberInfo member);
        void AddMemberSetDelegate(MemberInfo member);


        
        Type[] CollectMemberTypes();

        int GetMemberIndex(string memberName);
        //bool GetMemberWritable(string memberName);
        MemberInfo GetMemberInfo(string memberName);

        TMember GetMember<TMember>(string MemberKey, object instance);
        void SetMember<TMember>(string MemberKey, ref object instance, TMember MemberNewValue);

    }


}

#region -------------------------------- GARBAGE --------------------------------

/// <summary>
/// TypeAccessorState used by TypeAccessor to indicate if it generate all GET-ers && SET-ers for all of it's members.
/// </summary>
//TypeAccessorStateEn State { get;  }


//void BuildMembers(BindingFlags fieldsBinding = BindingFlags.Default, BindingFlags propertiesBinding = BindingFlags.Default);
//void BuildMembersTypeProcessor();

#endregion -------------------------------- GARBAGE --------------------------------
