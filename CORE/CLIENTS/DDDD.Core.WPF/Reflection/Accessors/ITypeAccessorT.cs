﻿namespace DDDD.Core.Reflection
{

    public interface ITypeAccessor<T> : ITypeAccessor
    {
     
        void SetMember<TMember>(string MemberKey, ref T instance, TMember MemberNewValue);
        TMember GetMember<TMember>(string MemberKey, T instance);
        
    }
}
