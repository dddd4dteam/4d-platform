﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Reflection
{

    /// <summary>
    /// TypePrimitiveMapAttribute -   mapping Attribute helps us to  associate TypePrimitivesEn.field with System.Type 
    /// /and if exist with System.TypeCode
    /// </summary>
    [AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    internal sealed class TypePrimitiveMapAttribute : Attribute
    {

        #region ------------------------------- CTOR -----------------------------------

        public TypePrimitiveMapAttribute(TypeCode tcode, Type targetType = null)
        {
            Code = tcode;
            TargetType = targetType;
        }

        public TypePrimitiveMapAttribute(Type targetType)
        {
            Code = TypeCode.Empty;
            TargetType = targetType;
        }

        #endregion ------------------------------- CTOR -----------------------------------


        #region ---------------------------------- PROPERTIES -----------------------------

        /// <summary>
        /// Equivalent to enum.Field -  System.TypeCode .
        /// </summary>
        public TypeCode Code
        {
            get;
            private set;
        }


        /// <summary>
        /// TargetType to enum.Field - System.Type . 
        /// </summary>
        public Type TargetType
        {
            get;
            private set;
        }

        #endregion ---------------------------------- PROPERTIES -----------------------------

    }
   
    
}
