﻿
using System;
using DDDD.Core.Extensions;


namespace DDDD.Core.Reflection
{
    /// <summary>
    /// TypeIsNotDeterminedException - Exception When MapppingAdapter wasn't able to determine the real .NET Type by String value.
    /// </summary>
    public class TypeIsNotDeterminedException : Exception
    {

        internal TypeIsNotDeterminedException() { }


        internal TypeIsNotDeterminedException(String Message, Type Contract)
            : base(String.Format(Message, Contract.FullName)) { }

        internal TypeIsNotDeterminedException(String Message, Type CollectionContract, Type ContractArg)
            : base(String.Format(Message, CollectionContract.FullName, ContractArg.FullName)) { }

        internal TypeIsNotDeterminedException(String Message, String ContractFullName)
            : base(String.Format(Message, ContractFullName)) { }

        internal TypeIsNotDeterminedException(String Message, Enum ContractKind) :
            base(String.Format(Message, ContractKind.S()) ) { }

        internal TypeIsNotDeterminedException(string message) : base(message) { }


        internal TypeIsNotDeterminedException(string message, Exception inner) : base(message, inner) { }


    }

}
