﻿using System;
using System.Collections;

namespace DDDD.Core.Reflection
{



    /// <summary> 
    ///  Base  Collection Types  - base collection interface Types : IList,IDictionary,  Array, ObservableCollection.       
    /// </summary>
    public enum CollectionTypeEn
    {

        /// <summary>
        /// Type is not collection type( nor Array, nor IList based ,nor IDictionary based ).
        /// </summary>
        NotCollection,


        /// <summary>
        /// Type is Array collection type.
        /// </summary>
        Array,


        /// <summary>
        /// Type is  IList or IList{} based collection type.
        /// </summary>
        IList,

        /// <summary>
        /// ObservableCollection -is separate Kind of Collection type because this collection Creating/Activating without Length argument as List{T} or Dictionary{K,V}
        /// </summary>
        ObservableCollection,

        /// <summary>
        /// Type/Contract is IDictionary or IDictionary{,} based collection type.
        /// </summary>
        IDictionary
    }





    /// <summary>
    ///  TypePrimitivesEn - .NET Primitive Types Enumeration - just one other classification.
    /// <para/>   1)  Each field associated with equivalent System.Type value - So you can to know assocaited type like <c>TypePrimitivesEn.GetRealType()</c> .            
    /// <para/>   2)  Most of the fields has also associated with System.TypeCode enumeration value - - So you can assocaited type like TypePrimitivesEn.GetTypeCode()/ or return TypeCode.Empty.
    /// <para/>   3)  TypePrimitivesEn values doesn't contain values associated with the following TypeCode values: 
    /// <para/>        { TypeCode.Empty | TypeCode.DBNull | TypeCode.Object }
    /// <para/>  TypePrimitivesEn consist of:
    /// <para/>       1) Not Nullable structs:  int(Int32),  uint(UInt32),  long(Int64),  ulong(UInt64), 
    /// <para/>                                 short(Int16),  ushort(UInt16),  byte(Byte),  sbyte(SByte),  char(Char),  bool(Boolean),  
    /// <para/>                                 DateTime,  TimeSpan, Guid,  
    /// <para/>                                 float(Single), double(Double), decimal(Decimal)                       
    /// <para/>                    
    /// <para/>       2) Nullable structs:      int?(Int32?),  uint?(UInt32?),  long?(Int64?),  ulong?(UInt64?), 
    /// <para/>                                 short(Int16?),  ushort(UInt16?),  byte(Byte?),  sbyte(SByte?),  char(Char?),  bool(Boolean?),  
    /// <para/>                                 DateTime?,  TimeSpan?, Guid?,  
    /// <para/>                                 float?(Single), double?(Double), decimal?(Decimal)
    /// <para/>                                 
    /// <para/>      3) Classes:               string, byte[], Uri, BitArray
    /// <para/>      4) Reflection Classes:    TypeInfoEx -  Crossplatform  Domain Type/Sheme  Definition 
    /// </summary>
    public enum TypePrimitiveEn
    {

        #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------
       
       /// <summary>
       /// Represents a 32-bit signed integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Int32, typeof(Int32))] Int32
            ,
       /// <summary>
       ///  Represents a 32-bit unsigned integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.UInt32, typeof(UInt32))] UInt32
            ,    
     
       /// <summary>
       /// Represents a 64-bit signed integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Int64, typeof(Int64))] Int64
            ,
       /// <summary>
       /// Represents a 64-bit unsigned integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.UInt64, typeof(UInt64))] UInt64
            ,
       /// <summary>
       /// Represents a 16-bit signed integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Int16, typeof(Int16))] Int16
            ,
       /// <summary>
       /// Represents a 16-bit unsigned integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.UInt16, typeof(UInt16))] UInt16
            ,
       /// <summary>
       /// Represents an 8-bit unsigned integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Byte, typeof(Byte))]   Byte
            ,
       /// <summary>
       /// Represents an 8-bit signed integer.
       /// </summary>
       [TypePrimitiveMap(TypeCode.SByte, typeof(SByte))] SByte
            ,       

       /// <summary>
       /// Represents a character as a UTF-16 code unit.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Char, typeof(Char))]    Char
            ,
       /// <summary>
       /// Represents a Boolean value.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Boolean, typeof(Boolean))]    Boolean
            ,
       /// <summary>
       ///  Represents an instant in time, typically expressed as a date and time of
       ///     day.
       /// </summary>
       [TypePrimitiveMap(TypeCode.DateTime, typeof(DateTime))] DateTime
            ,
       /// <summary>
       ///  Represents a time interval.
       /// </summary>
       [TypePrimitiveMap( typeof(TimeSpan))] TimeSpan
            ,        
       /// <summary>
       /// Represents a globally unique identifier (GUID).
       /// </summary>
       [TypePrimitiveMap( typeof(Guid))]Guid
      
            ,
        /// <summary>
        /// Represents a single-precision floating-point number. Known as Single - Type.Name=Single. 
        /// </summary>
        [TypePrimitiveMap(TypeCode.Single, typeof(float))] Float
            ,
       /// <summary>
       /// Represents a double-precision floating-point number.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Double, typeof(Double))] Double
            ,
       /// <summary>
       /// Represents a decimal number.
       /// </summary>
       [TypePrimitiveMap(TypeCode.Decimal, typeof(Decimal))] Decimal
      

        #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


        #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

            ,
        /// <summary>
        /// Represents a Nullable{32-bit signed integer}.
        /// </summary>
        [TypePrimitiveMap( typeof(Int32?))] Int32_nullable
            ,
       /// <summary>
        /// Represents a Nullable{32-bit unsigned integer}.
       /// </summary>
        [TypePrimitiveMap( typeof(UInt32?))] UInt32_nullable
            ,    
     
       /// <summary>
        /// Represents a Nullable{64-bit signed integer}.
       /// </summary>
        [TypePrimitiveMap(typeof(Int64?))]  Int64_nullable
           ,
       /// <summary>
        /// Represents a Nullable{64-bit unsigned integer}.
       /// </summary>
        [TypePrimitiveMap(typeof(UInt64?))] UInt64_nullable
           ,
        /// <summary>
        /// Represents a Nullable{16-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(Int16?))]  Int16_nullable
           ,
        /// <summary>
        /// Represents a Nullable{16-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(UInt16?))]UInt16_nullable
           ,
        /// <summary>
        /// Represents an Nullable{8-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap( typeof(Byte?))]   Byte_nullable           
           ,
        /// <summary>
        /// Represents an Nullable{8-bit signed integer}.
        /// </summary>
        [TypePrimitiveMap( typeof(SByte?))] SByte_nullable
           ,
        /// <summary>
        /// Represents a Nullable{character as a UTF-16 code unit}.
        /// </summary>
        [TypePrimitiveMap( typeof(Char?))]    Char_nullable
           ,
        /// <summary>
        ///  Represents a Nullable{Boolean value}.
        /// </summary>
        [TypePrimitiveMap( typeof(Boolean?))]    Boolean_nullable
           ,
        /// <summary>
        /// Represents a Nullable{instant in time, typically expressed as a date and time of
        ///     day}.
        /// </summary>
        [TypePrimitiveMap(TypeCode.DateTime, typeof(DateTime?))] DateTime_nullable
           ,
        /// <summary>
        /// Represents a Nullable{time interval}.
        /// </summary>
        [TypePrimitiveMap( typeof(TimeSpan?))] TimeSpan_nullable
           ,        
        /// <summary>
        /// Represents a Nullable{globally unique identifier (GUID)}.
        /// </summary>
        [TypePrimitiveMap( typeof(Guid?))] Guid_nullable
           ,
        /// <summary>
        /// Represents a Nullable{single-precision floating-point number}.
        /// </summary>
        [TypePrimitiveMap( typeof(Single?))] Float_nullable
           ,
        /// <summary>
        /// Represents a Nullable{double-precision floating-point number}.
        /// </summary>
        [TypePrimitiveMap( typeof(Double?))] Double_nullable
           ,
        /// <summary>
        ///  Represents a Nullable{decimal number}.
        /// </summary>
        [TypePrimitiveMap( typeof(Decimal?))] Decimal_nullable
            ,

        #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


        #region ----------------------------------------  3  Classes:  ---------------------------------------------
       
        /// <summary>
        /// Represents text as a series of Unicode characters.
        /// </summary>
        [TypePrimitiveMap(TypeCode.String, typeof(String))]  String 
            ,
        /// <summary>
        /// Represents an Array of 8-bit unsigned integer - byte[].
        /// </summary>
        [TypePrimitiveMap( typeof(byte[]))] byteArray       
            ,
        /// <summary>
        ///  Provides an object representation of a uniform resource identifier (URI)
        ///     and easy access to the parts of the URI.
        /// </summary>
        [TypePrimitiveMap( typeof(Uri))]  Uri 
            ,
        /// <summary>
        /// Manages a compact array of bit values, which are represented as Booleans,
        ///     where true indicates that the bit is on (1) and false indicates the bit is
        ///     off (0).
        /// </summary>
        [TypePrimitiveMap( typeof(BitArray))]  BitArray

        #endregion---------------------------------------- 3  Classes:  ---------------------------------------------


        #region --------------------------- 4 Reflection Classes ------------------------------
            
            ,
        /// <summary>
        /// Crossplatform  Domain Type/Sheme  Definition 
        /// </summary>
        [TypePrimitiveMap(typeof(TypeInfoEx))]
        TypeInfoEx

        #endregion --------------------------- 4 Reflection Classes ------------------------------




    }








    /// <summary>    
    /// <para/> Type Members Selector - information flag that help us to  determine - how to get the [Type MEMBERS SET] that will be processed.    
    /// <para/> To Select members we use standart  System.Reflection.BindingFlags combination value. 
    /// <para/> But in our serializer we are limited to use only next flags: 
    /// <para/>                            BindingFlags.Instance     - this means that we  can select members only from instance 
    /// <para/>                             BindingFlags.Public       - this means that we will include public members(fields/properties) into SET
    /// <para/>                             BindingFlags.NonPublic    - this means that we will include Non-Public:{internal/ private /protected}  members(fields/properties) into SET
    /// <para/>                            BindingFlags.DeclaredOnly - this means that we will include only our Type's members and exclude base type members.
    /// <para/> 
    /// <para/> We have several standart type processing policies:
    /// <para/>    - [NOTDEFINED] Selector - uses next BindingFlags combination: BindingFlags.Default,
    /// <para/>          error if policy not defined(for [CustomAdding Category Type] );   
    /// <para/>    - [DEFAULT] Selector - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
    /// <para/>          no other customization to build [Type MEMBERS SET]; 
    /// <para/>    - [DEFAULTNOBASE] Selector - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
    /// <para/>          no other customization to build [Type MEMBERS SET]; 
    /// <para/>    - [PUBLIC] - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
    /// <para/>          no other customization to build [Type MEMBERS SET]; 
    /// <para/>    - [ONLY_MEMBERS] Selector -  uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public |  | BindingFlags.NonPublic,  
    /// <para/>          and this flags used to search the [ONLY MEMBERS] that we can set manually. 
    /// <para/>          If some [ONLY MEMBERS] won't found then  TypeMemberAnalyzeException will be thrown; 
    /// <para/>    - [CUSTOM] Selector -  uses next BindingFlags combination by default : BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic ,
    /// <para/>          and you can change BindingFlags combination into combination by your wish.
    /// </summary>
    public enum TypeMemberSelectorEn
    {
        /// <summary>
        /// [NOTDEFINED] Selector - uses next BindingFlags combination: BindingFlags.Default,
        ///          error if Selector not defined(for [CustomAdding Category Type] )
        /// </summary>
        NotDefined = 0,

        /// <summary>
        /// [DEFAULT] Selector - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
        ///          no other customization to build [Type MEMBERS SET]
        /// </summary>
        Default = 1,

        /// <summary>
        /// [DEFAULTNOBASE] Selector - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;
        ///          no other customization to build [Type MEMBERS SET]
        /// </summary>
        DefaultNoBase = 2,

        /// <summary>
        /// [PUBLIC] Selector - uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly;
        ///          no other customization to build [Type MEMBERS SET]
        /// </summary>
        PublicOnly = 3,

        /// <summary>
        /// [ONLY_MEMBERS] Selector -  uses next BindingFlags combination: BindingFlags.Instance | BindingFlags.Public |  | BindingFlags.NonPublic,  
        ///          and this flags used to search the [ONLY MEMBERS] that we can set manually. 
        ///          If some [ONLY MEMBERS] won't found then  TypeMemberAnalyzeException will be thrown
        /// </summary>
        OnlyMembers = 4,

        /// <summary>
        /// [CUSTOM] Selector -  uses next BindingFlags combination by default : BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic ,
        ///          and you can change BindingFlags combination into combination by your wish. 
        /// <para/> Here you can use also IgnoreMembers option.
        /// </summary>
        Custom = 5
    }



    /// <summary>
    /// TypeInfoEx  has two states:  
    /// <para/> TypeFound  - 
    /// <para/> NotFounded -
    /// TypeFoundState used by TypeInfoEx to indicate if it's TypeAQName.FoundType is not null.
    /// </summary>
    public enum TypeFoundStateEn
    {
        TypeFound
        ,
        NotFounded
    }




}
