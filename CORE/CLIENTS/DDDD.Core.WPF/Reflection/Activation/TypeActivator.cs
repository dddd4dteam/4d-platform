﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Threading;
using DDDD.Core.Resources;

#if NET45
using System.Collections.Concurrent;
using DDDD.Core.Collections;
using DDDD.Core.Diagnostics;
#endif


namespace DDDD.Core.Reflection
{


	/// <summary>
	///  TypeActivator - simple conceptual formula of its functionality is : [ Precompile, Chache, Activate]
	/// <para/>    1) Precompile types Activation expressions into Funcs - into [Func{object[], object}]- Boxed Func and  into [Func{object[], T }] - determined Func.,   
	/// <para/>    2) Cache precompiled Funcs:
	/// <para/>                we have 2 lines of Caching - Boxed line and T-line.  Each of lines has 2 cache Dictionaries(4 cache dictionaries at all) 
	/// <para/>                    -  one for default Ctor Activation Funcs ,
	/// <para/>                    -  and second for Ctor with one or more args Activation Funcs 
	/// <para/>    3) then Create instancies by existed Funcs(or precompile just on the way).
	/// <para/>  Optional behaviors:
	/// <para/> We  exclude  customType  from Activation in  the  following  cases:  - customType.IsInterface ; - customType.IsAbstract ;   - customType.IsPointer 
	/// <para/> Also we have Excluding from Activation List of Types (ExcludingTypes property). For example: we cannot create [typeof(Type)] by default, and Pointer types too.
	/// <para/> We  usually work - with separate ctors, but also we can Precompile and Cache  all Ctors for some Type.
	/// <para/> All precompiled Ctor Funcs never will be deleted after it'll be created
	/// </summary>
	public static class TypeActivator
	{

		#region ---------------------------- CTOR ------------------------------

		static TypeActivator()
		{      Initialize();
		}

		private static void Initialize()
		{            
			try
			{
				//caching Def Ctor of [string]
				AddSafeToDefCtorTypeBoxedFuncs(   typeof(string), PrecompileAsDefaultExpressionBoxed(typeof(string))   );
				AddSafeToDefCtorTypeTFuncs( PrecompileAsDefaultExpressionT<string>() );

				//caching Def Ctor of [Uri] 
				AddSafeToDefCtorTypeBoxedFuncs(   typeof(Uri), PrecompileAsDefaultExpressionBoxed(typeof(Uri))   );
				AddSafeToDefCtorTypeTFuncs( PrecompileAsDefaultExpressionT<Uri>() );
													
			}
			catch (Exception exc)
			{
				throw new InvalidOperationException($" {nameof(TypeActivator)}.{nameof(Initialize)}(): TypeActivator initialization ERROR: [{exc.Message}] ");
			}            
		}


		#endregion ---------------------------- CTOR ------------------------------



		#region --------------------------- NESTED ARGVALIDATOR -------------------------------
		/// <summary>
		///  NESTED ARG Validator class
		/// </summary>
		class ArgValidator
		{
			/// To get Message we use template from Default ResourceManager by Validator class.
			/// Message template Key: [T-Validator] + [Operation Name - Operation].  
			
			/// <summary>
			/// If  nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException.  Works only in DEBUG mode.
			/// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
			/// </summary>
			/// <param name="nullChekingItem"></param>
			/// <param name="methodName"></param>
			/// <param name="parameterName"></param>
			[Conditional("DEBUG")]
			public static void ATNullReferenceArgDbg(object nullChekingItem,string methodName, string parameterName)
			{
				if (nullChekingItem.IsNull())
				{
					throw new  NullReferenceException(RCX.ERR_NullReferenceValue.Fmt(nameof(TypeActivator), methodName, parameterName));
				}
			}


			/// <summary>
			/// If  nullChekingItem IS NOT NULL - (FALSE that nullChekingItem.IsNull()- .IsNotNull()) then throw NullReferenceException.  Works only in DEBUG mode.
			/// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
			/// </summary>
			/// <param name="nullChekingItem"></param>
			/// <param name="methodName"></param>
			/// <param name="parameterName"></param>
			[Conditional("DEBUG")]
			public static void AssertFalse_NullReferenceArgDbg(object nullChekingItem, string methodName, string parameterName)
			{
				if (nullChekingItem.IsNotNull())
				{
					throw new NullReferenceException(RCX.ERR_NullReferenceValue.Fmt(nameof(TypeActivator), methodName, parameterName));
				}
			}




			/// <summary>
			/// If condition is TRUE - then throw InvalidOperationException. Works only in DEBUG mode.
			/// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].".
			/// </summary>
			/// <param name="condition"></param>
			/// <param name="methodName"></param>
			/// <param name="userMessage"></param>
			[Conditional("DEBUG")]
			public static void ATInvalidOperationDbg(bool condition,string methodName, string userMessage)
			{
				if (condition)
				{
					throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(nameof(TypeActivator), methodName, userMessage)) ;
				}
			}



			/// <summary>
			/// If condition is FALSE - then throw InvalidOperationException. Works only in DEBUG mode.
			/// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].". 
			/// </summary>
			/// <param name="condition"></param>
			/// <param name="methodName"></param>
			/// <param name="userMessage"></param>
			[Conditional("DEBUG")]
			public static void AssertFalse_InvalidOperationDbg(bool condition, string methodName, string userMessage)
			{
				if (!condition)
				{
					throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(nameof(TypeActivator), methodName, userMessage));
				}
			}



		   

		}// END NESTED ARG Validator class


		#endregion --------------------------- NESTED ARGVALIDATOR -------------------------------



		#region ------------------------- FIELDS -----------------------------

		static readonly object locker = new object();

		/// <summary>
		/// Default binding to get Ctors {Instance|Public|NonPublic}
		/// </summary>
		public const BindingFlags DefaultCtorSearchBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic ;

		#endregion ------------------------- FIELDS -----------------------------

		

		#region ------------------------ EXCLUDING TYPES ----------------------------

		/// <summary>
		/// Types whose we excluding from Activation 
		/// <para/>By  default Such types are: typeof(Type) 
		/// </summary>
		public static List<Type> ExcludingTypes
		{ get; } = new List<Type>();

		#endregion ------------------------ EXCLUDING TYPES ----------------------------



		#region ------------------------ CACHE DICTIONARIES -----------------------------


		/// <summary>
		/// Cache Dictionary  of Ctor Funcs of (Func{object[], T}). 
		/// </summary>
		static SafeDictionary2<ConstructorInfo, Delegate> GlobalTFuncs
		{ get; } = new SafeDictionary2<ConstructorInfo, Delegate>();

		/// <summary>
		/// Cache Dictionary  of Ctor Funcs of (Func{object[], object}).
		/// </summary>
		static SafeDictionary2<ConstructorInfo, Func<object[], object>> GlobalBoxedFuncs
		{ get; } = new SafeDictionary2<ConstructorInfo, Func<object[], object>>();


		/// <summary>
		/// Cache Dictionary of default Type Ctor Func (Func{object[], T} in side base type (Delegate) )
		/// <para/> We'll store here - Type Default Ctor Func as Default value Func
		/// <para/>By  default types are: typeof(string), typeof(Uri) will be precompiled and added here on the TypeActivator Instantiating
		/// </summary>
		static SafeDictionary2<Type, Delegate> DefCtorTFuncs
		{ get; } = new SafeDictionary2<Type, Delegate>();// classes that doesn't have default constructors

		/// <summary>
		///Cache Dictionary of default Type Ctor Func ( - Func{object[], object})
		/// <para/> We'll store here - Type Default Ctor Func as Default value Func
		/// <para/>By  default types are: typeof(string), typeof(Uri) will be precompiled and added here on the TypeActivator Instantiating
		/// </summary>
		static SafeDictionary2<Type, Func<object[], object>> DefCtorBoxedFuncs
		{ get; } = new SafeDictionary2<Type, Func<object[], object>>();

		//#endif






		#endregion ------------------------ CACHE DICTIONARIES -----------------------------
			

		#region -------------------- ADD TO CACHED DICTIONARIES ------------------------

		/// <summary>
		/// Adding Func( of Func{object[],object} ) to  [DefCtorTypeBoxedFuncs] internal cache Dictionary
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="precompiledFunc"></param>
		public static void AddSafeToDefCtorTypeBoxedFuncs(Type targetType,  Func<object[],object> precompiledFunc)
		{
				var operation = nameof(AddSafeToDefCtorTypeBoxedFuncs);
                OperationInvoke.CallInMode( nameof(TypeActivator),operation
                ,()=> {

					// OPERATION CODE BEGIN
					ArgValidator.ATNullReferenceArgDbg(targetType, operation, nameof(targetType));
					ArgValidator.ATNullReferenceArgDbg(precompiledFunc, operation, nameof(precompiledFunc) );

					locker.LockAction(()=> DefCtorBoxedFuncs.NotContainsKey(targetType)
						   , () => { DefCtorBoxedFuncs.Add(targetType, precompiledFunc); });
					// OPERAION END
				}
				);
						
			
		}

		/// <summary>
		/// Adding Func( of Func{object[],T} ) to  [DefCtorTypeFuncs] internal cache Dictionary
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="precompiledFunc"></param>
		public static void AddSafeToDefCtorTypeTFuncs<T>( Func<object[], T> precompiledFunc)
		{
			var operation = nameof(AddSafeToDefCtorTypeTFuncs);

            OperationInvoke.CallInMode(nameof(TypeActivator), operation
                , () => {

				  // OPERATION CODE BEGIN
				  ArgValidator.ATNullReferenceArgDbg(precompiledFunc, operation, nameof(precompiledFunc));
				  
				  Type targetType = typeof(T);

				  locker.LockAction(()=>DefCtorTFuncs.NotContainsKey(targetType)
							 , () => { DefCtorTFuncs.Add(targetType, precompiledFunc); });

				  // OPERAION END
			  });

              //OperationCodeBlock(operation,

        }

        #endregion -------------------- ADD TO CACHED DICTIONARIES ------------------------



        #region --------------------------------- PRECOMPILING CTORS -----------------------------

        /// <summary>
        /// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], object} ] delegate , when we can need to use [new T()] expression as creation method
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> PrecompileAsNewExpressionBoxed(Type targetType)
		{
			var operation = nameof(PrecompileAsNewExpressionBoxed);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  //)  OperationCodeBlock(operation,
            , () => {

				// OPERATION CODE BEGIN
				ArgValidator.ATNullReferenceArgDbg(targetType, operation, nameof(targetType));
				
				return Expression.Lambda<Func<object[], object>>(
								   Expression.Convert(Expression.New(targetType.GetWorkingTypeFromNullableType()), typeof(object))
								 , Expression.Parameter(typeof(object[]))).Compile();

				// OPERATION END

			});

		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], T} ] delegate , when we can need to use [new T()] expression as creation method
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> PrecompileAsNewExpressionT<T>()
		{

			var operation = nameof(PrecompileAsNewExpressionT);
			return OperationInvoke.CallInMode( nameof(TypeActivator), operation //OperationCodeBlock(operation,
                ,() =>
				 {
					// OPERATION CODE BEGIN

					var targetType = typeof(T);
					return Expression.Lambda<Func<object[], T>>(
										Expression.Convert(Expression.New(targetType.GetWorkingTypeFromNullableType()), typeof(T))
									  , Expression.Parameter(typeof(object[]))).Compile();

					// OPERATION END
				 });
		}

		/// <summary>
		///  Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], T} ] delegate , when we can need to use [new T[(1)-(4) dimension]() array]  expression as creation method. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> PrecompileAsNewArrayExpressionT<T>() 
		{
			var operation = nameof(PrecompileAsNewArrayExpressionT);
			return OperationInvoke.CallInMode( nameof(TypeActivator) , operation  //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN
					var targetType = typeof(T);

					var arrayDefaultCtor = targetType.GetConstructors().First();
					int arrayRank = arrayDefaultCtor.GetParameters().Count();//targetType.GetArrayRank(); 


					arrayDefaultCtor.CreateCtorDelegate<T>();

					ParameterExpression param_argsArray = Expression.Parameter(typeof(object[]), "args");

					switch (arrayRank)
					{
						case 1:
							return Expression.Lambda<Func<object[], T>>(
								Expression.Convert(
									Expression.New(arrayDefaultCtor,
										Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(0)), typeof(int)) // one measure - length
										), typeof(T)
									)
								, param_argsArray).Compile();
						case 2:
							return Expression.Lambda<Func<object[], T>>(
								Expression.Convert(
									Expression.New(arrayDefaultCtor,
										Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(0)), typeof(int)) // one measure - length
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(1)), typeof(int)) // second measure - hight
										), typeof(T)
									)
								, param_argsArray).Compile();
						case 3:
							return Expression.Lambda<Func<object[], T>>(
								Expression.Convert(
									Expression.New(arrayDefaultCtor,
										Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(0)), typeof(int)) // one measure - length
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(1)), typeof(int)) // second measure - hight
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(2)), typeof(int)) // second measure - deep
										), typeof(T)
									)
								, param_argsArray).Compile();
						case 4:
							return Expression.Lambda<Func<object[], T>>(
								Expression.Convert(
									Expression.New(arrayDefaultCtor,
										Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(0)), typeof(int)) // one measure - length
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(1)), typeof(int)) // second measure - hight
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(2)), typeof(int)) // second measure - deep
										, Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(3)), typeof(int)) // second measure - deep
										), typeof(T)
									)
								, param_argsArray).Compile();
					}


					throw new InvalidOperationException("ERROR in {0}.{1}(): Cannot create default array value Func -for array of T[{2}]".Fmt( nameof(TypeActivator),operation,typeof(T).Name) );

					// OPERATION END
				});

		   
		}



		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], T} ] delegate , when we can need to use [new T(length)] expression as creation method
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="args"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> PrecompileAsNewWithLengthExpressionT<T>()
		{
			var operation = nameof(PrecompileAsNewWithLengthExpressionT);
			return OperationInvoke.CallInMode( nameof(TypeActivator), operation // OperationCodeBlock(operation,
				,() =>
				{

					// OPERATION CODE BEGIN
					var targetType = typeof(T);

					ParameterExpression param_argsArray = Expression.Parameter(typeof(object[]), "args");
					ConstructorInfo ctorWithLength = null;
					TryGetConstructorByArgs(targetType, out ctorWithLength, DefaultCtorSearchBinding, 2);

					return Expression.Lambda<Func<object[], T>>(
										Expression.Convert(
											  Expression.New(ctorWithLength,
																	   Expression.Convert(Expression.ArrayIndex(param_argsArray, Expression.Constant(0)), typeof(int))
															), typeof(T)
														  )
									  , param_argsArray).Compile();
					// OPERATION END
				});
		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], object} ] delegate , when we can need to use [default(T)] expression as creation method(for example for struct types)
		/// </summary>
		/// <param name="defValueDefCtorType"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> PrecompileAsDefaultExpressionBoxed(Type defValueDefCtorType)
		{

			var operation = nameof(PrecompileAsDefaultExpressionBoxed);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation // OperationCodeBlock(operation,
				, () =>
				{

					// OPERATION CODE BEGIN
					ArgValidator.ATNullReferenceArgDbg(defValueDefCtorType, operation, nameof(defValueDefCtorType));

					ParameterExpression param_argsArray = Expression.Parameter(typeof(object[]), "args");

					return Expression.Lambda<Func<object[], object>>(
									 Expression.Default(defValueDefCtorType)
								   , param_argsArray).Compile();
					// OPERATION END
				});
		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], T} ]  delegate , when we can need to use [default(T)] expression as creation method
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> PrecompileAsDefaultExpressionT<T>()
		{
			var operation = nameof(PrecompileAsDefaultExpressionT);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation // OperationCodeBlock(operation,
			   ,() =>
			   {
				   // OPERATION CODE BEGIN
				   return Expression.Lambda<Func<object[], T>>(
									Expression.Default(typeof(T))
								  , Expression.Parameter(typeof(object[]))).Compile(); 
				   
				   // OPERATION END
			   });

		}

#if NET45

		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as  [ Func{object[], T } ]   delegate ,  from targetType only info with the help of  FormatterServices.GetUninitializedObject
		/// </summary>
		/// <param name="targetType"></param>
		/// <returns></returns>        
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> PrecompileAsUninitializedBoxed(Type targetType)
		{
			var operation = nameof(PrecompileAsUninitializedBoxed);
			return OperationInvoke.CallInMode( nameof(TypeActivator), operation // OperationCodeBlock(operation,
				,() =>
				{
					// OPERATION CODE BEGIN

					var methodGetUninitializedObject = typeof(FormatterServices).GetMethod(nameof(FormatterServices.GetUninitializedObject), BindingFlags.Static | BindingFlags.Public);
					var targetType_Parameter = Expression.Parameter(targetType);
					return Expression.Lambda<Func<object[], object>>(
																	Expression.Call(methodGetUninitializedObject, targetType_Parameter)
																	, targetType_Parameter).Compile();

					// OPERATION END
				});

		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as  [ Func{object[], T } ]   delegate ,  from targetType only info with the help of  FormatterServices.GetUninitializedObject
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> PrecompileAsUninitializedT<T>()
		{
			var operation = nameof(PrecompileAsUninitializedT);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation   //OperationCodeBlock(operation,
                , () =>
				{
					// OPERATION CODE BEGIN

					var targetType = typeof(T);
					var methodGetUninitializedObject = typeof(FormatterServices).GetMethod(nameof(FormatterServices.GetUninitializedObject), BindingFlags.Static | BindingFlags.Public);
					var targetType_Parameter = Expression.Parameter(targetType);
					return Expression.Lambda<Func<object[], T>>(Expression.Call(methodGetUninitializedObject, targetType_Parameter)
																	, targetType_Parameter).Compile();

					// OPERATION END
				});
		}


#endif


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as  [ Func{object[], T } ]   delegate , from ctor - ConstructoInfo item.
		/// <para/> ctor.DeclaredType must me the same as T 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="ctor"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static Func<object[], T> PrecompileCtorT<T>(ConstructorInfo ctor) 
		{
			var operation = nameof(PrecompileCtorT);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation    //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					ArgValidator.ATNullReferenceArgDbg(ctor, operation, nameof(ctor) );

					ArgValidator.ATInvalidOperationDbg( typeof(T) != ctor.DeclaringType , operation
								   ,"ctor.DeclaringType[{0}] must be the same as T [{1}] ".Fmt( ctor.DeclaringType.FullName, typeof(T).FullName) );

					Type declaringType = ctor.DeclaringType;
					Type[] ctorParamTypes = ctor.GetParameters().ToTypeArray();

					// if we contain one of arg type as Pointer then we return null instead of real func for this ctor - ???
					if (ctorParamTypes.Where(prmTp => prmTp.IsPointer).FirstOrDefault() != null)
						return (args) => { return default(T); };

					var outerCtorFuncType = Expression.GetFuncType(typeof(object[]), declaringType);
					NewExpression newExpression = null;

					//inner object array - Function parameters
					var ParamObjectArgs = Expression.Parameter(typeof(object[]));

					//if it's default Ctor then simply createDelegate from Ctor.Method without args
					if (ctorParamTypes.IsNotWorkable())
					{
						//to use Cached  functors after this will be created we can't use  (Func<T>) and ctor.CreateCtorDelegate<T>()                
						//var targetFunc = ctor.CreateCtorDelegate<T>();
						newExpression = Expression.New(ctor);
					}
					else // ctor has real inner parameters-arg values  
					{

						int index = -1;
						List<Expression> InnerValues = new List<Expression>();
						for (int i = 0; i < ctorParamTypes.Length; i++)
						{
							InnerValues.Add(Expression.Convert(
															Expression.ArrayIndex(ParamObjectArgs, Expression.Constant(++index))
														   , ctorParamTypes[i])
									   );
						}

					  

						newExpression = Expression.New(ctor, InnerValues);
					}

					var delegateOuter = Expression.Lambda(outerCtorFuncType, newExpression, ParamObjectArgs);

					return (Func<object[], T>)delegateOuter.Compile();

					// OPERATION END
				});

		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as  [ Delegate ]  delegate on the facade, and [ Func{object[], T } ]real  value type,   from ctor - ConstructoInfo item.
		/// </summary>
		/// <param name="ctor"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		static Delegate PrecompileCtorT(ConstructorInfo ctor) 
		{
			var operation = nameof(PrecompileCtorT);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  // OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN
					ArgValidator.ATNullReferenceArgDbg(ctor, operation, nameof(ctor));					

					Type declaringType = ctor.DeclaringType;
					Type[] ctorParamTypes = ctor.GetParameters().ToTypeArray();

					// if we contain one of arg type as Pointer then we return null instead of real func for this ctor - ???
					if (ctorParamTypes.Where(prmTp => prmTp.IsPointer).FirstOrDefault() != null) return null;

					var outerCtorFuncType = Expression.GetFuncType(typeof(object[]), declaringType);

					//outer object array param
					var ParamObjectArgs = Expression.Parameter(typeof(object[]));

					//real inner param-values 
					int index = -1;
					List<Expression> InnerValues = new List<Expression>();
					for (int i = 0; i < ctorParamTypes.Length; i++)
					{
						InnerValues.Add(Expression.Convert(
														Expression.ArrayIndex(ParamObjectArgs, Expression.Constant(++index))
													   , ctorParamTypes[i])
								   );
					}

					var commonExpression = (InnerValues.Count == 0) ? Expression.New(ctor)
																   : Expression.New(ctor, InnerValues);

					var delegateOuter = Expression.Lambda(outerCtorFuncType, commonExpression, ParamObjectArgs);

					return delegateOuter.Compile();

					// OPERATION END
				});
		   
		}


		/// <summary>
		/// Precompile  .NET Classes Type's Ctor Func  as [ Func{object[], object} ]   delegate , from ctor - ConstructoInfo item.
		/// </summary>
		/// <param name="ctor"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> PrecompileCtorBoxed(ConstructorInfo ctor) 
		{
			var operation = nameof(PrecompileCtorBoxed);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					ArgValidator.ATNullReferenceArgDbg(ctor, operation, nameof(ctor));

					Type[] ctorParamTypes = ctor.GetParameters().ToTypeArray(); //?? can throw exception ???

					// if we contain one of arg type as Pointer then we return null instead of real func for this ctor -  Expressions wont build funcs with Pointer  types
					if (ctorParamTypes.Where(prmTp => prmTp.IsPointer).FirstOrDefault() != null) return null;

					var outerCtorFuncType = Expression.GetFuncType(typeof(object[]), typeof(object));

					//outer object array param
					var ParamObjectArgs = Expression.Parameter(typeof(object[]));

					//real inner param-values 
					int index = -1;
					List<Expression> InnerValues = new List<Expression>();

					for (int i = 0; i < ctorParamTypes.Length; i++)
					{
						InnerValues.Add(Expression.Convert(
														Expression.ArrayIndex(ParamObjectArgs, Expression.Constant(++index))
													   , ctorParamTypes[i])
								   );
					}

					var commonExpression = (InnerValues.Count == 0) ? Expression.New(ctor)
																   : Expression.New(ctor, InnerValues);

					var delegateOuter = Expression.Lambda(outerCtorFuncType, commonExpression, ParamObjectArgs);

					return delegateOuter.Compile() as Func<object[], object>;

					// OPERATION END
				});
			
		}



		/// <summary>
		/// Precompile all T Ctors Funcs found by ctorsBinding, then cache them into internal dictionary(GlobalTFuncs), then return dictionary of precompiled Funcs for each ctor. 
		/// <para/>  Returning  T ctors in form of - Dictionary{ConstructorInfo, Func{object[], T}}.            
		/// <para/> ctorsBinding by default = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Dictionary<ConstructorInfo, Func<object[], T>> PrecompileCtorsT<T>(BindingFlags ctorsBinding = DefaultCtorSearchBinding)
		{

			var operation = nameof(PrecompileCtorsT);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					var currentType = typeof(T);

					var outTypeCtorFuncsT = new Dictionary<ConstructorInfo, Func<object[], T>>(); //

					//TypeKind Exclusions
					if (currentType.IsInterface || currentType.IsAbstract ) return outTypeCtorFuncsT; //|| currentType.IsArray

					// Some Concrete Types types Excludings

					var ctors = currentType.GetConstructors(ctorsBinding);
					for (int i = 0; i < ctors.Length; i++)
					{
#if DEBUG
						Type[] arguments = ctors[i].GetParameters().Select(prmtr => prmtr.ParameterType).ToArray();
#endif
						var innerDelegate = PrecompileCtorT<T>(ctors[i]);
						if (innerDelegate != null)
						{
							GlobalTFuncs.Add(ctors[i], innerDelegate);        // global cache - Delegate  

							outTypeCtorFuncsT.Add(ctors[i], innerDelegate);   // current T ctors - Func<object[], T>
						}

					}

					return outTypeCtorFuncsT;

					// OPERATION END
				});
             

		}



		/// <summary>
		/// Build  .NET Type's Ctor Func  as  [ Func{object[], TBase } ]   delegate , from ctor - ConstructoInfo item.
		/// </summary>
		/// <typeparam name="TBase"></typeparam>
		/// <param name="ctor"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], TBase> PrecompileCtorTBase<TBase>(ConstructorInfo ctor)
		{
			var operation = nameof(PrecompileCtorTBase);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation   //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					ArgValidator.ATNullReferenceArgDbg(ctor, operation, nameof(ctor));
					
					ArgValidator.ATInvalidOperationDbg( 
								  (typeof(TBase).IsAssignableFrom(ctor.DeclaringType) == false), operation
								   , " ctor.DeclaredType[{0}] can't assigned to its TBase Type[{1}]".Fmt( ctor.DeclaringType.FullName, typeof(TBase).FullName ) );


					Type declaringType = ctor.DeclaringType;
					ParameterInfo[] ctorParameters = ctor.GetParameters();
					Type[] ctorParamTypes = ctorParameters.ToTypeArray();

					// if we contain one of arg type as Pointer then we return null instead of real func for this ctor - ??? Expressions
					if (ctorParamTypes.Where(prmTp => prmTp.IsPointer).FirstOrDefault() != null) return null;

					var outerCtorFuncType = Expression.GetFuncType(typeof(object[]), declaringType);

					var funcargs = new List<Type>(); funcargs.AddRange(ctorParamTypes); funcargs.Add(declaringType);
					var realCtorFuncType = Expression.GetFuncType(funcargs.ToArray());

					//outer object array param
					var ParamObjectArgs = Expression.Parameter(typeof(object[]));

					//real inner param-values 
					Int32 index = -1;
					List<Expression> InnerValues = new List<Expression>();
					for (int i = 0; i < ctorParamTypes.Length; i++)
					{
						InnerValues.Add(Expression.Convert(
													 Expression.ArrayIndex(ParamObjectArgs, Expression.Constant(++index))
													  , ctorParamTypes[i])
									);
					}
					
					var commonExpression = (InnerValues.Count == 0) ? Expression.New(ctor)
																   : Expression.New(ctor, InnerValues);

					var delegateOuter = Expression.Lambda(outerCtorFuncType, commonExpression, ParamObjectArgs);

					return (Func<object[], TBase>)delegateOuter.Compile();

					// OPERATION END
				});

		}



		#endregion --------------------------------- PRECOMPILING CTORS -----------------------------



		#region ------------------------ COMPARE/GET CTOR BY ARGS ------------------------
		
		/// <summary>
		/// Compare if ctor.Parameters.Types match with such args.Types.  
		/// </summary>
		/// <param name="ctor"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool CompareArgsMatchCtor(ConstructorInfo ctor, object[] args)
		{
			return CompareArgsMatchCtorInternal(ctor, args);
		}


		internal static bool CompareArgsMatchCtorInternal(ConstructorInfo ctor, object[] args)
		{
			var operation = nameof(CompareArgsMatchCtorInternal);
			return  OperationInvoke.CallInMode(nameof(TypeActivator), operation                
				    , () =>
				   {
					// OPERATION CODE BEGIN

					 ArgValidator.ATNullReferenceArgDbg(ctor, operation, nameof(ctor));
					
					 var parameters = ctor.GetParameters();
					 var parameterTypes = (parameters.Length > 0) ? parameters.Select(prm => prm.ParameterType).ToList() : null;

					 if (args == null && parameters.Length == 0) return true;               //default ctor case
					 if (args != null && args.Length != parameters.Length) return false;     //ctor parameters length mismatch with args length

					 //check each parameters type with args type
					 for (int i = 0; i < args.Length; i++)
					 {
						var paramType = parameters[i].ParameterType;

						if (args[i] == null)
						{
							//still enabled [NullValue] for Classes  -appropriate
							if (paramType.IsClass) continue;

							// if it is [NullValue] but paramType Is Not Nullable struct
							else if (!paramType.IsNullable()) return false;

						}
						else //if(args[i] != null)
						{
							var argIType = args[i].GetType();
							// arg[i] Type is one to one correct param type
							if (argIType == paramType) continue;

							//arg type really  is not the same as parameters Type- but for Interface or BaseClass, or object it still can be passed as param value
							else if (paramType.IsAssignableFrom(argIType) == false) return false;
						}

					 }

					 return true;

					 // OPERATION END
				   });


		}


		/// <summary>
		/// Comparing ctor instance parameterTypes with some custom argTypes - if both are matching.
		/// </summary>
		/// <param name="ctor"></param>
		/// <param name="argTypes"></param>
		/// <returns></returns>
		public static bool CompareArgTypesMatchCtor(ConstructorInfo ctor, params Type[] argTypes)
		{
			//Get existed default ctors 
			var ctorParameters = ctor.GetParameters();

			if (ctorParameters.Length == 0 && argTypes.Length == 0) { return true; }
			else if (ctorParameters.Length != argTypes.Length) { return false; }
			else if (ctorParameters.Length == argTypes.Length)
			{
				for (int i = 0; i < ctorParameters.Length; i++)
				{
					if (ctorParameters[i].ParameterType != argTypes[i]) return false;
				}
				return true;
			}

			return false;
		}


		/// <summary>
		/// Try to Get Ctor by args within existed ctors of targetType with filter of ctorSearchBinding  
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="resultCtor"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static bool TryGetConstructorByArgs(Type targetType, out ConstructorInfo resultCtor, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params  object[] args)
		{
			return TryGetConstructorByArgsInternal(targetType, out resultCtor, ctorSearchBinding, args);
		}



		static bool TryGetConstructorByArgsInternal(Type targetType, out ConstructorInfo resultCtor, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
				var operation = nameof(TryGetConstructorByArgsInternal);
		   
				ArgValidator.ATNullReferenceArgDbg(targetType, operation, nameof(targetType)); 
					
				resultCtor = null;

				//Get existed default ctors 
				var foundedCtors = targetType.GetConstructors(ctorSearchBinding);

				if (foundedCtors.Length == 0) return false;

				//if ctor with appropriate args won't be found between foundedCtors then return false
				for (int i = 0; i < foundedCtors.Length; i++)
				{
					if (CompareArgsMatchCtor(foundedCtors[i], args))
					{ resultCtor = foundedCtors[i]; return true; }
				}    
		
				return false; 
		}

		static bool TryGetConstructorByArgTypesInternal(Type targetType, out ConstructorInfo resultCtor, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params Type[] argTypes)
		{
			var operation = nameof(TryGetConstructorByArgTypesInternal);

			ArgValidator.ATNullReferenceArgDbg(targetType, operation, nameof(targetType));

			resultCtor = null;

			//Get existed default ctors 
			var foundedCtors = targetType.GetConstructors(ctorSearchBinding);

			if (foundedCtors.Length == 0) return false;

			//if ctor with appropriate args won't be found between foundedCtors then return false
			for (int i = 0; i < foundedCtors.Length; i++)
			{
				if ( CompareArgTypesMatchCtor( foundedCtors[i], argTypes ) )
				{ resultCtor = foundedCtors[i]; return true; }
			}

			return false;

		}


		#endregion ------------------------ COMPARE/GET CTOR BY ARGS ------------------------



		#region ------------------------  GET /or ADD PRECOMPILED CTORS -----------------------------

		///      1 - [Get existed Func ( of [Func{object[], T}]  ) from Boxed Cache-Dictionaries] or
		///<para/> 2 - if Func doesn't exist [Create Func on the way, then Cache and then Get].
		/// <para/>        Cache in this case are Cache-Dictionaries  for the following targets:
		/// <para/>             1 in [ DefCtorTypeFuncs ] - for class's default Ctors, nullable/notnullable structs  
		/// <para/>             2 or in [ GlobalTFuncs ]   - for class's and struct's Ctors with one or more args


		/// <summary>
		/// Get precompiled and cached TFunc of Ctor with args[] as parameter values.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> GetWithCachingTypeCtorTFunc<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{

			var operation = nameof(GetWithCachingTypeCtorTFunc);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation   //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					var targetType = typeof(T);
					// 1 Activation by default ctor -
					//              nullable/notnullable structs - instantiation using  [new]  Expression 
					//              class - by its default ctor
					// 2 - Activation by non default Ctors(with 1 or more args)


					//if targetType is one of the excluded types (for example: typeof(Type)) , then throw Exception 
					ArgValidator.ATInvalidOperationDbg(
								ExcludingTypes.Contains(targetType), operation
							   , " T=[{0}] is the type that Excluded from Activation, so it won't be instantiated".Fmt(targetType.FullName));

					//For cases where we can use Constructors:
					// - def ctor of class  
					// - not default ctors of class or struct( with one or more args)
					ConstructorInfo targetCtor = null;

					var argsString = "args.Types : ";
					if (args.IsWorkable())
					{
						for (int i = 0; i < args.Length; i++)
						{
							argsString += (args[i] == null) ? "; Null Value" : ("; " + args[i].GetType().FullName);
						}
					}

					//target func :1 for default ctor - for nullable/notnullable structs and for class with its default ctor 
					//             2 for non default Ctors(with one or more args)       
					Func<object[], T> targetFunc = null;

					// 1 Activation by default ctor -
					//              nullable/notnullable structs - instantiation using  [new]  Expression 
					//              class - by its default ctor
					if (args == null || args.Length == 0)
					{
						// Try get from already Known Default Ctor Func from DefCtorTypeBoxedFuncs Dictionary
						if (DefCtorTFuncs.ContainsKey(targetType)) return DefCtorTFuncs[targetType] as Func<object[], T>;

						if (targetType.IsValueType) //if we searching default Ctor for structs where default ctor can't exist, and classes that has no default ctors
						{
							// default ctor for nullable or not struct as new value expression
							targetFunc = PrecompileAsNewExpressionT<T>();
						}
						else if (targetType.IsClass) // class type
						{
							TryGetConstructorByArgsInternal(targetType, out targetCtor, ctorSearchBinding, args);

							if (targetCtor != null)
							{ targetFunc = PrecompileCtorT<T>(targetCtor); } // PrecompileCtorBoxed()

#if NET45
							else
							{
								targetFunc = PrecompileAsUninitializedT<T>();
							}
#endif

						}

						ArgValidator.ATInvalidOperationDbg(targetFunc.IsNull(),operation
								, " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString)); // if appropriate ctor of T doesn't exist -then throw  

						//save/cache precompiled targetFunc into global dictionary of [DefaultCtors]
						locker.LockAction(()=> DefCtorTFuncs.NotContainsKey(targetType)
							, () =>
							{
								DefCtorTFuncs.Add(targetType, targetFunc);
							}
							);
						

						return DefCtorTFuncs[targetType] as Func<object[], T>;
					}
					else //2 - Activation by non default Ctors(with 1 or more args)
					{
						TryGetConstructorByArgsInternal(targetType, out targetCtor, ctorSearchBinding, args);

						ArgValidator.ATInvalidOperationDbg(targetCtor.IsNull(), operation // targetFunc
								, "  Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString) ); // if appropriate ctor of T doesn't exist -then throw  


						// Try get from already Known Ctor Func from GlobalFuncs Dictionary
						if (GlobalBoxedFuncs.ContainsKey(targetCtor)) return GlobalTFuncs[targetCtor] as Func<object[], T>;

						//precompiled targetFunc and save/cache it into global dictionary [GlobalFuncs]

						locker.LockAction(()=> GlobalTFuncs.NotContainsKey(targetCtor)
							, () =>
							{
								targetFunc = PrecompileCtorT<T>(targetCtor);

								if (!GlobalTFuncs.ContainsKey(targetCtor))
								{ GlobalTFuncs.Add(targetCtor, targetFunc); }
							}
							);
						
						//here  ctor Func already exist( by 100 %) in GlobalFuncs ...return it
						return (GlobalTFuncs[targetCtor] as Func<object[], T>);
					}

					// OPERATION END
				}); 
	  
		}


		/// <summary>
		///  Get precompiled and cached Func of Ctor with argTypes[] - Ctor definition, and args[] as parameter values.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], T> GetWithCachingTypeCtorByArgTypesTFunc<T>(Type[] argTypes,BindingFlags ctorSearchBinding = DefaultCtorSearchBinding )
		{
			var operation = nameof(GetWithCachingTypeCtorByArgTypesTFunc);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  // OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN

					var targetType = typeof(T);
					// 1 Activation by default ctor -
					//              nullable/notnullable structs - instantiation using  [new]  Expression 
					//              class - by its default ctor
					// 2 - Activation by non default Ctors(with 1 or more args)


					//if targetType is one of the excluded types (for example: typeof(Type)) , then throw Exception 
					ArgValidator.ATInvalidOperationDbg(
								ExcludingTypes.Contains(targetType), operation
							   , " T=[{0}] is the type that Excluded from Activation, so it won't be instantiated".Fmt(targetType.FullName));

					//For cases where we can use Constructors:
					// - def ctor of class  
					// - not default ctors of class or struct( with one or more args)
					ConstructorInfo targetCtor = null;

					var argsString = "args.Types : ";
					if (argTypes.IsWorkable())
					{
						for (int i = 0; i < argTypes.Length; i++)
						{
							argsString += (argTypes[i] == null) ? "; Null Value" : ("; " + argTypes[i].FullName);
						}
					}

					//target func :1 for default ctor - for nullable/notnullable structs and for class with its default ctor 
					//             2 for non default Ctors(with one or more args)       
					Func<object[], T> targetFunc = null;

					// 1 Activation by default ctor -
					//              nullable/notnullable structs - instantiation using  [new]  Expression 
					//              class - by its default ctor
					if (argTypes.IsNotWorkable()) //== null || args.Length == 0
					{
						// Try get from already Known Default Ctor Func from DefCtorTypeBoxedFuncs Dictionary
						if (DefCtorTFuncs.ContainsKey(targetType)) return DefCtorTFuncs[targetType] as Func<object[], T>;

						if (targetType.IsValueType) //if we searching default Ctor for structs where default ctor can't exist, and classes that has no default ctors
						{
							// default ctor for nullable or not struct as new value expression
							targetFunc = PrecompileAsNewExpressionT<T>();
						}
						else if (targetType.IsClass) // class type
						{
							TryGetConstructorByArgTypesInternal(targetType, out targetCtor, ctorSearchBinding, argTypes);

							if (targetCtor.IsNotNull() )
							{ targetFunc = PrecompileCtorT<T>(targetCtor); } // PrecompileCtorBoxed()

#if NET45
							else
							{
								targetFunc = PrecompileAsUninitializedT<T>();
							}
#endif
							  
						}

						// Ctor was not found  check
						ArgValidator.ATInvalidOperationDbg(targetFunc.IsNull(), operation
								, " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString)); // if appropriate ctor of T doesn't exist -then throw  

						//save/cache precompiled targetFunc into global dictionary of [DefaultCtors]
						locker.LockAction(() => DefCtorTFuncs.NotContainsKey(targetType)
							, () =>
							{
								DefCtorTFuncs.Add(targetType, targetFunc);
							}
							);


						return DefCtorTFuncs[targetType] as Func<object[], T>;
					}
					else //2 - Activation by non default Ctors(with 1 or more args)
					{
						TryGetConstructorByArgTypesInternal(targetType, out targetCtor, ctorSearchBinding, argTypes);

						ArgValidator.ATInvalidOperationDbg(targetCtor.IsNull(), operation // targetFunc
								, "  Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString)); // if appropriate ctor of T doesn't exist -then throw  


						// Try get from already Known Ctor Func from GlobalFuncs Dictionary
						if (GlobalBoxedFuncs.ContainsKey(targetCtor)) return GlobalTFuncs[targetCtor] as Func<object[], T>;

						//precompiled targetFunc and save/cache it into global dictionary [GlobalFuncs]

						locker.LockAction(() => GlobalTFuncs.NotContainsKey(targetCtor)
							, () =>
							{
								targetFunc = PrecompileCtorT<T>(targetCtor);

								if (!GlobalTFuncs.ContainsKey(targetCtor))
								{ GlobalTFuncs.Add(targetCtor, targetFunc); }
							}
							);

						//here  ctor Func already exist( by 100 %) in GlobalFuncs ...return it
						return (GlobalTFuncs[targetCtor] as Func<object[], T>);
					}

					// OPERATION END
				});

		}



		///     1 - [Get existed Func ( of [Func{object[], object}]  ) from Boxed Cache-Dictionaries] or
		///<para/>    2 - if Func doesn't exist [Create Func on the way, then Cache and then Get].
		/// <para/>        Cache in this case are Cache-Dictionaries  for the following targets:
		/// <para/>            1 in [ DefCtorTypeBoxedFuncs ] - for class's default Ctors, nullable/notnullable structs  
		/// <para/>            2 or in [ GlobalFuncsBoxed ]   - for class's and struct's Ctors with one or more args



		/// <summary>
		/// Get precompiled and cached Func of Ctor with args[] as parameter values.
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> GetWithCachingTypeCtorBoxedFunc(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			// 1 Activation by default ctor -
			//              nullable/notnullable structs - instantiation using  [new]  Expression 
			//              class - by its default ctor
			// 2 - Activation by non default Ctors(with 1 or more args)

			var operation = nameof(GetWithCachingTypeCtorBoxedFunc);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  //  OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN
					//if targetType is one of the excluded types (for example: typeof(Type)) , then throw Exception 
					ArgValidator.ATInvalidOperationDbg(
								ExcludingTypes.Contains(targetType) ,operation
							   , " T=[{0}] is the type that Excluded from Activation, so it won't be instantiated".Fmt(targetType.FullName));

					//For cases where we can use Constructors:
					// - def ctor of class  
					// - not default ctors of class or struct( with one or more args)
					ConstructorInfo targetCtor = null;

					var argsString = "args.Types : ";
					args?.ToList().ForEach(arg =>    argsString += (arg == null) ? 
											"; Null Value" :  ("; " + arg.GetType().FullName) );

					//target func :1 for default ctor - for nullable/notnullable structs and for class with its default ctor 
					//             2 for non default Ctors(with one or more args)       
					Func<object[], object> targetFunc = null;

					// 1 Activation by default ctor -
					//              nullable/notnullable structs - instantiation using  [new]  Expression 
					//              class - by its default ctor
					if ((args == null || args.Length == 0))
					{
						// Try get from already Known Default Ctor Func from DefCtorTypeBoxedFuncs Dictionary
						if (DefCtorBoxedFuncs.ContainsKey(targetType)) return DefCtorBoxedFuncs[targetType];

						if (targetType.IsValueType) //if we searching default Ctor for structs where default ctor can't exist, and classes that has no default ctors
						{
							// default ctor for nullable or not struct as new value expression
							targetFunc = PrecompileAsNewExpressionBoxed(targetType);
						}
						else if (targetType.IsClass) // class type
						{
							TryGetConstructorByArgsInternal(targetType, out targetCtor, ctorSearchBinding, args);

							if (targetCtor.IsNotNull()) // != null  - [12/18/2016 A1]
							{ targetFunc = PrecompileCtorBoxed(targetCtor); }
#if NET45
							else
							{
								targetFunc = PrecompileAsUninitializedBoxed(targetType);
							}
#endif

						}

						ArgValidator.ATInvalidOperationDbg(targetFunc.IsNull(),operation
								, " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString) ); // if appropriate ctor of T doesn't exist -then throw  

						//save/cache precompiled targetFunc into global dictionary of [DefaultCtors]
						locker.LockAction(()=> DefCtorBoxedFuncs.NotContainsKey(targetType)
							, () =>
							{
								DefCtorBoxedFuncs.Add(targetType, targetFunc);
							}
							);
						

						return DefCtorBoxedFuncs[targetType];
					}
					else //2 - Activation by non default Ctors(with 1 or more args)
					{
						TryGetConstructorByArgsInternal(targetType, out targetCtor, ctorSearchBinding, args);

						ArgValidator.ATInvalidOperationDbg(targetCtor.IsNull(),operation
								, " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString ) ); // if appropriate ctor of T doesn't exist -then throw  


						// Try get from already Known Ctor Func from GlobalFuncs Dictionary
						if (GlobalBoxedFuncs.ContainsKey(targetCtor)) return GlobalBoxedFuncs[targetCtor];

						//precompiled targetFunc and save/cache it into global dictionary [GlobalFuncs]
						locker.LockAction( ()=> GlobalBoxedFuncs.NotContainsKey(targetCtor)
							, () =>
							{
								targetFunc = PrecompileCtorBoxed(targetCtor);

								if (GlobalBoxedFuncs.NotContainsKey(targetCtor))
								{ GlobalBoxedFuncs.Add(targetCtor, targetFunc); }
							}
							);


						//here  ctor Func already exist( by 100 %) in GlobalFuncs ...return it
						return (GlobalBoxedFuncs[targetCtor]);
					}

					// OPERATION END
				});

			
		}


		/// <summary>
		///  Get precompiled and cached Func of Ctor with args[] as parameter values.
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="argTypes"></param>
		/// <returns></returns>
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Func<object[], object> GetWithCachingTypeCtorByArgTypesBoxedFunc(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params Type[] argTypes)
		{

			var operation = nameof(GetWithCachingTypeCtorByArgTypesBoxedFunc);
			return OperationInvoke.CallInMode(nameof(TypeActivator), operation  //OperationCodeBlock(operation,
               ,() =>
			   {
				   // OPERATION CODE BEGIN
				   // if targetType is one of the excluded types (for example: typeof(Type)) , then throw Exception 
				   ArgValidator.ATInvalidOperationDbg(
							  ExcludingTypes.Contains(targetType), operation
							 , " T=[{0}] is the type that Excluded from Activation, so it won't be instantiated".Fmt(targetType.FullName));

				   //For cases where we can use Constructors:
				   // - def ctor of class  
				   // - not default ctors of class or struct( with one or more args)
				   ConstructorInfo targetCtor = null;

				   var argsString = "args.Types : ";
				   argTypes?.ToList().ForEach(argType => argsString += (argType.IsNull()) ?
										   "; Null Value" : ("; " + argType.FullName));

				   //target func :1 for default ctor - for nullable/notnullable structs and for class with its default ctor 
				   //             2 for non default Ctors(with one or more args)       
				   Func<object[], object> targetFunc = null;

				   // 1 Activation by default ctor -
				   //              nullable/notnullable structs - instantiation using  [new]  Expression 
				   //              class - by its default ctor
				   if (argTypes.IsNotWorkable()) //: IsNull() || argTypes.Length == 0)
				   {
					   // Try get from already Known Default Ctor Func from DefCtorTypeBoxedFuncs Dictionary
					   if (DefCtorBoxedFuncs.ContainsKey(targetType)) return DefCtorBoxedFuncs[targetType];

					   if (targetType.IsValueType) //if we searching default Ctor for structs where default ctor can't exist, and classes that has no default ctors
					   {
						   // default ctor for nullable or not struct as new value expression
						   targetFunc = PrecompileAsNewExpressionBoxed(targetType);
					   }
					   else if (targetType.IsClass) // class type
					   {
						   TryGetConstructorByArgTypesInternal(targetType, out targetCtor, ctorSearchBinding, argTypes);

						   if (targetCtor.IsNotNull()) // != null  - [12/18/2016 A1]
						   { targetFunc = PrecompileCtorBoxed(targetCtor); }
#if NET45
						   else
						   {
							   targetFunc = PrecompileAsUninitializedBoxed(targetType);
						   }
#endif

					   }

					   ArgValidator.ATInvalidOperationDbg(targetFunc.IsNull(), operation
							   , " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString)); // if appropriate ctor of T doesn't exist -then throw  

					   //save/cache precompiled targetFunc into global dictionary of [DefaultCtors]
					   locker.LockAction(() => DefCtorBoxedFuncs.NotContainsKey(targetType)
						  , () =>
						  {
							  DefCtorBoxedFuncs.Add(targetType, targetFunc);
						  }
						  );

					   return DefCtorBoxedFuncs[targetType];
				   }
				   else //2 - Activation by non default Ctors(with 1 or more args)
				   {
					   TryGetConstructorByArgTypesInternal(targetType, out targetCtor, ctorSearchBinding, argTypes);

					   ArgValidator.ATInvalidOperationDbg(targetCtor.IsNull(), operation
							   , " Ctor of T=[{0}] was not Found for Args[{1}] ".Fmt(targetType.FullName, argsString)); // if appropriate ctor of T doesn't exist -then throw  
					   
					   // Try get from already Known Ctor Func from GlobalFuncs Dictionary
					   if (GlobalBoxedFuncs.ContainsKey(targetCtor)) return GlobalBoxedFuncs[targetCtor];

					   //precompiled targetFunc and save/cache it into global dictionary [GlobalFuncs]
					   locker.LockAction(() => GlobalBoxedFuncs.NotContainsKey(targetCtor)
						  , () =>
						  {
							  targetFunc = PrecompileCtorBoxed(targetCtor);

							  if (GlobalBoxedFuncs.NotContainsKey(targetCtor))
							  { GlobalBoxedFuncs.Add(targetCtor, targetFunc); }
						  }
						  );
					   
					   //here  ctor Func already exist( by 100 %) in GlobalFuncs ...return it
					   return (GlobalBoxedFuncs[targetCtor]);
				   }

				   // OPERATION END
			   });
		}


		#endregion ------------------------  GET /or ADD PRECOMPILED CTORS -----------------------------



		#region ---------------------------- CREATEINSTANCE<T>, CreateInstanceTLazy<T>  ---------------------------

		/// <summary>
		///  Creating T instance by Default Ctor (no args- null ).
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public static T CreateInstanceT<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding)
		{
			var currentType = typeof(T);

			if (currentType.IsInterface || currentType.IsAbstract || currentType.IsPointer) return default(T); //|| currentType.IsArray 

			 
			return GetWithCachingTypeCtorTFunc<T>(ctorSearchBinding)(null);
		}


		/// <summary>
		/// Creating T instance by args parameters
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static T CreateInstanceT<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			var currentType = typeof(T);

			if (currentType.IsInterface || currentType.IsAbstract || currentType.IsPointer) return default(T); //|| currentType.IsArray

			return GetWithCachingTypeCtorTFunc<T>(ctorSearchBinding, args)(args);
		}



		/// <summary>
		///  Creating T instance by Ctor with ArgTypes (no args- null ), and arg Values.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static T CreateInstanceT<T>(Type[] argTypes, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			var currentType = typeof(T);

			if (currentType.IsInterface || currentType.IsAbstract || currentType.IsPointer) return default(T); //|| currentType.IsArray 

			//if ctor was not found then message will be thhrown             
			return GetWithCachingTypeCtorByArgTypesTFunc<T>(argTypes, ctorSearchBinding)(args);
		}




		/// <summary>
		/// Creating T instance by args parameters.
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static T CreateInstanceTLazy<T>(BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
		  return  LazyAct<T>.Create(
				(argsInt) =>
				{
					var bindingInt = (BindingFlags)argsInt[0];
					var tArguments = argsInt[1] as object[];
					return CreateInstanceT<T>(bindingInt, tArguments);                    
				}
				, locker
				, new object[] { ctorSearchBinding, args }
				).Value;
		}


		/// <summary>
		/// Creating T instance by Ctor of argTypes, and args values .
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static T CreateInstanceTLazy<T>(Type[] argTypes, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return LazyAct<T>.Create(
				  (argsInt) =>
				  {
					  var argumTypesInt = (Type[])argsInt[0];
					  var bindingInt = (BindingFlags)argsInt[1];
					  var tArguments = argsInt[2] as object[];
					  return CreateInstanceT<T>(argumTypesInt, bindingInt, tArguments);
				  }
				  , locker
				  , new object[] {argTypes, ctorSearchBinding, args }
				  ).Value;
		}


		#endregion ---------------------------- CREATEINSTANCE<T> WITH its DEPENDENCIES ---------------------------



		#region ---------------------------- CREATEINSTANCE BOXED, CreateInstanceBoxedLazy  ---------------------------


		/// <summary>
		///  Creating Boxed into object instance of targetType by Default Ctor(no args parameters).
		/// </summary>
		/// <returns></returns>
		public static object CreateInstanceBoxed(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding)
		{
			return CreateInstanceBoxed( targetType, ctorSearchBinding: ctorSearchBinding,args:null);
		}

		/// <summary>
		/// Creating Boxed into object instance of targetType by Ctor with args parameters
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static object CreateInstanceBoxed(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{            
			if (targetType.IsInterface || targetType.IsAbstract || targetType.IsPointer ) return null;// || targetType.IsArray

			return GetWithCachingTypeCtorBoxedFunc(targetType, ctorSearchBinding, args)(args);

		}

		/// <summary>
		/// Create Boxed into object instance by Ctor with [argTypes] - its Parameter Types, and with args as parameter values 
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="argTypes"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static object CreateInstanceBoxed(Type targetType, Type[] argTypes , BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			if (targetType.IsInterface || targetType.IsAbstract || targetType.IsPointer) return null;// || targetType.IsArray

			return GetWithCachingTypeCtorByArgTypesBoxedFunc(targetType, ctorSearchBinding, argTypes)(args);

		}



		/// <summary>
		/// Creating Boxed into object instance of targetType by Ctor with args parameters
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static object CreateInstanceBoxedLazy(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{

			return LazyAct<object>.Create(
				  (argsInt) =>
				  {
					  var targetTypeInt = argsInt[0] as Type;
					  var bindingInt = (BindingFlags)argsInt[1];
					  var tArguments = argsInt[2] as object[];
					  return CreateInstanceBoxed(targetTypeInt, bindingInt, tArguments);
				  }
				  , locker
				  , new object[] { targetType, ctorSearchBinding, args }
				  ).Value;
		}


		/// <summary>
		/// Creating Boxed into object instance of targetType by Ctor with [argTypes] - its Parameter Types, and with args as parameter values.
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <param name="targetType"></param>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static object CreateInstanceBoxedLazy(Type targetType, Type[] argTypes, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return LazyAct<object>.Create(
				  (argsInt) =>
				  {
					  var targetTypeInt = argsInt[0] as Type;
					  var argTypesInt = argsInt[1] as Type[];
					  var bindingInt = (BindingFlags)argsInt[2];
					  var tArguments = argsInt[3] as object[];
					  return CreateInstanceBoxed( targetTypeInt, argTypesInt, bindingInt, tArguments);
				  }
				  , locker
				  , new object[] { targetType, argTypes, ctorSearchBinding, args }
				  ).Value;
		}



		#endregion ---------------------------- CREATEINSTANCE  BOXED  ---------------------------



		#region ------------------------- CREATEINSTANCE  TBASE ------------------------

		/// <summary>
		/// Creating TBased  Type sutisfied instance of targetType by Ctor with args parameters		
		/// </summary>
		/// <typeparam name="TBase"></typeparam>
		/// <param name="targetType"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static TBase CreateInstanceTBase<TBase>(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return (TBase)CreateInstanceBoxed(targetType, ctorSearchBinding, args);
			
		}


		/// <summary>
		/// Creating TBased  Type sutisfied instance of targetType by Ctor of Type[], and with args parameter values.
		/// </summary>
		/// <typeparam name="TBase"></typeparam>
		/// <param name="targetType"></param>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static TBase CreateInstanceTBase<TBase>(Type targetType, Type[] argTypes,  BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return (TBase)CreateInstanceBoxed(targetType, argTypes, ctorSearchBinding, args);

		}



		/// <summary>
		/// Creating TBased  Type sutisfied instance of targetType by Ctor  with args parameter values.
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <typeparam name="TBase"></typeparam>
		/// <param name="targetType"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static TBase CreateInstanceTBaseLazy<TBase>(Type targetType, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return LazyAct<TBase>.Create(
				  (argsInt) =>
				  {
					  var targetTypeInt = argsInt[0] as Type;
					  var bindingInt = (BindingFlags)argsInt[1];
					  var tArguments = argsInt[2] as object[];
					  return CreateInstanceTBase<TBase>(targetTypeInt, bindingInt, tArguments);
				  }
				  , locker
				  , new object[] {targetType, ctorSearchBinding, args }
				  ).Value;

		}


		/// <summary>
		/// Creating TBased  Type sutisfied instance of targetType by Ctor of Type[], and with args parameter values.
		/// <para/> Instance will be created in Thread Safety-Lazy Initing way.
		/// </summary>
		/// <typeparam name="TBase"></typeparam>
		/// <param name="targetType"></param>
		/// <param name="argTypes"></param>
		/// <param name="ctorSearchBinding"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		public static TBase CreateInstanceTBaseLazy<TBase>(Type targetType, Type[] argTypes, BindingFlags ctorSearchBinding = DefaultCtorSearchBinding, params object[] args)
		{
			return LazyAct<TBase>.Create(
				  (argsInt) =>
				  {
					  var targetTypeInt = argsInt[0] as Type;
					  var argTypesInt = argsInt[1] as Type[];
					  var bindingInt = (BindingFlags)argsInt[2];
					  var tArguments = argsInt[3] as object[];
					  return CreateInstanceTBase<TBase>(targetTypeInt, argTypesInt, bindingInt, tArguments);
				  }
				  , locker
				  , new object[] { targetType, ctorSearchBinding, args }
				  ).Value;

		}



		#endregion ------------------------- CREATEINSTANCE  TBASE ------------------------


		// APPLICATION OF TYPE ACTIVATOR -  CtorFunc for TYPEACCESSOR{T} || Test of Default Ctor{T}
		#region -------------------------------------- APPLICATIONS --------------------------------------

		/// <summary>
		/// Testing {T} Type by  its Default Ctor. 
		///  <para/> If default Ctor will contains internal static Increment
		///  <para/> , then Testing will be finished, even if IsTestableByDefCtor was true after analysis of Type.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="typeAccessor">TypeAccessor</param>
		/// <param name="testCtorContainsIncrementLogic">If default Ctor will contains internal static Increment
		///     <para/> , then Testing will be finished, even if IsTestableByDefCtor = true
		/// </param>
		public static void TryDefaultCtorTest<T>(TypeAccessor<T> typeAccessor, bool testCtorContainsIncrementLogic = false)
		{
			var operation = nameof(TryDefaultCtorTest);
                OperationInvoke.CallInMode(nameof(TypeActivator), operation     //OperationCodeBlock(operation,
                ,() =>
				{
					// OPERATION CODE BEGIN
					if (typeAccessor.TAccessEx.IsTestableByDefCtor && testCtorContainsIncrementLogic == false) // CanTestDefaultCtorOption
					{
						// ctor(DefaultCtor  or Ctor WithLength) cannot be null
						ArgValidator.ATInvalidOperationDbg(typeAccessor.DefaultCtorFunc.IsNull() && typeAccessor.CtorWithCapacityFunc.IsNull()
									, operation ,"CtorFunc can't be null if we want to test CTOR" );
						
						//Creating by ctor delegate
						object ItemInstance = typeAccessor.CreateDefaultValue(); //CtorFunc(); // ci.Invoke(null);

						//it means that we need  raise IDispose before finish this test
						if (typeAccessor.TAccessEx.IsDisposable == true)
						{
							((IDisposable) ItemInstance).Dispose();
						}
						ItemInstance = null;
					}

					// OPERATION END
				});

			
		}

#endregion -------------------------------------- APPLICATIONS --------------------------------------


	}
}




#region ------------------------------------ GARBAGE -------------------------------------



#region --------------------------- OPERATION CODE BLOCK --------------------------------

//		/// <summary>
//		/// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
//		/// Here className is [nameof(TypeActivator)].
//		/// </summary>
//		/// <param name="operationMethodName"></param>
//		/// <param name="operationAction"></param>
//		static void OperationCodeBlock(string operationMethodName, Action operationAction)
//		{

//#if DEBUG
//			try
//			{
//				operationAction();
//			}
//			catch (Exception exc)
//			{
//				throw new InvalidOperationException(" ERROR in {0}.{1}(): message - [{2}]"
//						  .Fmt(nameof(TypeActivator), operationMethodName, exc.Message)
//						  );
//			}
//#else
//			operationAction();
//#endif

//		}


//		/// <summary>
//		/// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
//		/// Here className is [nameof(TypeActivator)].
//		/// </summary>
//		/// <typeparam name="TRes"></typeparam>
//		/// <param name="operationMethodName"></param>
//		/// <param name="operationFunc"></param>
//		/// <returns></returns>
//		static TRes OperationCodeBlock<TRes>(string operationMethodName, Func<TRes> operationFunc)
//		{

//#if DEBUG
//			try
//			{
//				return operationFunc();
//			}
//			catch (Exception exc)
//			{
//				throw new InvalidOperationException(" ERROR in {0}.{1}(): message - [{2}]"
//						  .Fmt(nameof(TypeActivator), operationMethodName, exc.Message)
//						  );
//			}
//#else
//				return operationFunc();
//#endif

//		}

#endregion --------------------------- OPERATION CODE BLOCK --------------------------------






//#if NET45
//        /// <summary>
//        /// Cache Dictionary  of Ctor Funcs of (Func{object[], T}). 
//        /// </summary>
//        static ConcurrentDictionary<ConstructorInfo, Delegate> GlobalTFuncs
//        { get; } = new ConcurrentDictionary<ConstructorInfo, Delegate>();


//        /// <summary>
//        /// Cache Dictionary  of Ctor Funcs of (Func{object[], object}).
//        /// </summary>
//        static ConcurrentDictionary<ConstructorInfo, Func<object[], object>> GlobalBoxedFuncs
//        { get; } = new ConcurrentDictionary<ConstructorInfo, Func<object[], object>>();


//        /// <summary>
//        /// Cache Dictionary of default Type Ctor Func (Func{object[], T} in side base type (Delegate) )
//        /// <para/> We'll store here - Type Default Ctor Func as Default value Func
//        /// <para/>By  default types are: typeof(string), typeof(Uri) will be precompiled and added here on the TypeActivator Instantiating
//        /// </summary>
//        static ConcurrentDictionary<Type, Delegate> DefCtorTFuncs
//        { get; } = new ConcurrentDictionary<Type, Delegate>();// classes that doesn't have default constructors


//        /// <summary>
//        ///Cache Dictionary of default Type Ctor Func ( - Func{object[], object})
//        /// <para/> We'll store here - Type Default Ctor Func as Default value Func
//        /// <para/>By  default types are: typeof(string), typeof(Uri) will be precompiled and added here on the TypeActivator Instantiating
//        /// </summary>
//        static ConcurrentDictionary<Type, Func<object[], object>> DefCtorBoxedFuncs
//        { get; } = new ConcurrentDictionary<Type, Func<object[], object>>();

//#else


//still enabled [NullValue] for Classes  -appropriate
//if (args[i] == null && paramType.IsClass) continue;

//// if it is [NullValue] but paramType Is Not Nullable struct
//if (args[i] == null && !paramType.IsNullable()) return false;

//// param type is one to one correct arg[i] Type
//if (args[i] != null && args[i].GetType() == paramType) continue;

////arg type reallly  is not the same as parameters Type
//if (args[i] != null && args[i].GetType().IsAssignableFrom( paramType )  )   return false;   

//        /// <summary>
//        /// Try to get or create  CtorFunc for TYPEACCESSOR{T}
//        /// </summary>
//        /// <typeparam name="T"></typeparam>
//        /// <param name="typeAccessor"></param>
//        /// <returns></returns>
//        [MethodImpl(MethodImplOptions.AggressiveInlining)]
//        public static Func<T> TrySetCtorFunc<T>(TypeAccessor<T> typeAccessor)
//        {
//            ArgValidator.NotNullDbg(typeAccessor, nameof(TrySetCtorFunc));

//            var Contract = typeAccessor.TAccess;// typeof(T);
//            if (Contract.IsInterface || Contract.IsAbstract || Contract.IsArray) return null; //not for arrays |Abstract|Interfaces they 'll be created as Array.New(rank, dimensions)


//            if (typeof(IEnumerable).IsAssignableFrom(Contract) && Contract != typeof(string)) //for collections first we  try to get ctor with Capacity arg  
//            {
//                ConstructorInfo ctor = Contract.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
//                                                                    null,
//                                                                    new Type[] { typeof(int) },
//                                                                    null
//                                                                    );

//                if (ctor != null)
//                {
//                    var capacityExpression = Expression.Parameter(typeof(int));

//                    var newValueExpression = Expression.New(ctor, capacityExpression);

//                    //var returnFunc = Expression.GetFuncType( typeof(int) , Contract);
//                    var lambda = Expression.Lambda(
//                                        Expression.Convert(newValueExpression, Contract),
//                                        capacityExpression
//                                    );

//                    typeAccessor.CtorWithCapacityFunc = (Func<Int32, T>)lambda.Compile();

//                    return null;
//                }
//            }

//            // special  primitive classes that doesn't have default constructors
//            if (Contract == typeof(string)
//                || Contract == typeof(Uri))
//            {
//                var lambda = Expression.Lambda<Func<T>>(
//                               Expression.Convert(Expression.Default(Contract), Contract) // ?? Convert need
//                             );

//                typeAccessor.DefaultCtorFunc = lambda.Compile();
//                return null;
//            }



//            if (Contract.IsClass)
//            {
//                var ctorDef = Contract.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic,
//                                                      null,
//                                                      Type.EmptyTypes,
//                                                      null
//                                                   );               

//                if (ctorDef != null)
//                    typeAccessor.DefaultCtorFunc = Expression
//                                            .Lambda<Func<T>>(Expression.New(Contract))
//                                            .Compile();

//#if NET45
//                else
//                    typeAccessor.DefaultCtorFunc = () => (T)FormatterServices.GetUninitializedObject(Contract);
//#endif
//                ///typeAccessor.CtorFunc == null;


//            }
//            else if (Contract.IsValueType && !typeAccessor.TAccess.IsNullable()) //for not nullable structs
//            {
//                var newValueExpression = Expression.New(Contract);

//                var lambda = Expression.Lambda<Func<T>>(
//                                Expression.Convert(newValueExpression, typeof(T))
//                             );

//                typeAccessor.DefaultCtorFunc = lambda.Compile();
//            }
//            else if (Contract.IsValueType && typeAccessor.TAccessEx.IsNullabeValueType)
//            {
//                //
//                var newValueExpression = Expression.New(typeAccessor.TAccessEx.WorkingType);

//                var lambda = Expression.Lambda<Func<T>>(
//                                Expression.Convert(newValueExpression, typeof(T))
//                             );

//                typeAccessor.DefaultCtorFunc = lambda.Compile();

//            }

//            return null;
//        }




//ParameterInfo[] ctorParameters = ctor.GetParameters().ToTypeArray();
//Type[] argumentTypes = ctorParameters.ToTypeArray();


//internal delegate T CreateGenericFunc<T>();
//CreateGenericFunc<T> CreateGenerally
//Func<T> 

//static Func<object[], Type> PrecompileCreateInstanceCall()
//{
//    MethodInfo GenericCreate = typeof(TypeActivator).GetMethod("CreateInstance", BindingFlags.Static, null, Type.EmptyTypes, null);
//    GenericCreate.MakeGenericMethod(someType);
//    Expression.Call()

//}


//public static object CreateInstance(Type someType)
//{
//    MethodInfo GenericCreate = typeof(TypeActivator).GetMethod("CreateInstance", BindingFlags.Static, null, Type.EmptyTypes, null);
//    GenericCreate.MakeGenericMethod(someType);

//    return null;

//}




//ParameterInfo[] ctorParameters = ctor.GetParameters().ToTypeArray();
//Type[] argumentTypes = ctorParameters.ToTypeArray();
//
//var realCtorFuncArgs = new List<Type>(); realCtorFuncArgs.AddRange(argumentTypes); realCtorFuncArgs.Add(declaringType);
//var realCtorFuncType = Expression.GetFuncType(realCtorFuncArgs.ToArray());
//
//
//var realCtorFuncArgs = new List<Type>(); realCtorFuncArgs.AddRange(argumentTypes); realCtorFuncArgs.Add(declaringType);
//var realCtorFuncType = Expression.GetFuncType(realCtorFuncArgs.ToArray());
// 
//if (ctorDef != null)
//{
//    newValueExpression = Expression.New(ctorDef);
//}
//else if (ctorDef == null)
//{      
//    newValueExpression = Expression.New(Contract);
//}
//var lambda = Expression.Lambda<Func<T>>(
//               Expression.Convert(newValueExpression, typeof(T))
//             );

//typeAccessor.CtorFunc = lambda.Compile();

#endregion ------------------------------------ GARBAGE -------------------------------------


