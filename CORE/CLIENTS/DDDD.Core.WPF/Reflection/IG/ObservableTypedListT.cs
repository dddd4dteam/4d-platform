﻿using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;

namespace DDDD.Core.Reflection.IG
{

    /// <summary>
    /// IList&lt;T&gt; implementation that wraps an IList and supports propogating the collection and property changes from the source collection.
    /// </summary>
    /// <typeparam name="T">Type of the items in the collection</typeparam>
    internal class ObservableTypedList<T> : TypedList<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        internal ObservableTypedList(IList list) : base(list)
        {
            INotifyCollectionChanged notifyCollectionChanged = list as INotifyCollectionChanged;
            if (notifyCollectionChanged != null)
            {
                notifyCollectionChanged.CollectionChanged += new NotifyCollectionChangedEventHandler(this.OnCollectionChanged);
            }
            INotifyPropertyChanged notifyPropertyChanged = list as INotifyPropertyChanged;
            if (notifyPropertyChanged != null)
            {
                notifyPropertyChanged.PropertyChanged += new PropertyChangedEventHandler(this.OnCollectionPropertyChanged);
            }
        }

        private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            NotifyCollectionChangedEventHandler notifyCollectionChangedEventHandler = this.CollectionChanged;
            if (notifyCollectionChangedEventHandler != null)
            {
                notifyCollectionChangedEventHandler(this, e);
            }
        }

        private void OnCollectionPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler propertyChangedEventHandler = this.PropertyChanged;
            if (propertyChangedEventHandler != null)
            {
                propertyChangedEventHandler(this, e);
            }
        }

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public event PropertyChangedEventHandler PropertyChanged;
    }

}
