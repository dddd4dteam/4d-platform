﻿using System.Collections.ObjectModel;
using System.Linq;

namespace DDDD.Core.Reflection.IG
{
	/// <summary>
	/// Represents a collection of <see cref="T:CustomTypeProperty" /> objects.
	/// </summary>
	public class CustomTypePropertyCollection : Collection<CustomTypeProperty>
	{
		/// <summary>
		/// Gets the <see cref="T:CustomTypeProperty" /> with the specified property name.
		/// </summary>
		public CustomTypeProperty this[string propertyName]
		{
			get
			{
				if (string.IsNullOrEmpty(propertyName))
				{
					return null;
				}
				return (
					from i in this
					where i.PropertyName.Equals(propertyName)
					select i).FirstOrDefault<CustomTypeProperty>();
			}
		}

		public CustomTypePropertyCollection()
		{
		}
	}
}
