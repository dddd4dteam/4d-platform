﻿
using System;
using System.Linq;
using System.Windows;
using DDDD.Core.Environment;
using DDDD.Core.Extensions;
#if SL5
using System.Windows.Browser;
#endif

namespace DDDD.Core.HttpRoutes
{
    /// <summary>
    /// BaseUrl - Base Url represents starting point for some Default network service( WCF or Web API or...).
    /// <para/> We can use this class to build Some Url for some server  app Components( like Services, Modules, Asseblies...)
    /// <para/> With Base Url we can 
    /// <para/> -1  determine automatically/semi-automatically uri from browser doc from your Host webApplication  or 
    /// <para/> -2  define it manually when it is non-browser client App is starting.  
    /// <para/> BaseUrl can be accessed from EnvironmentEx static class.
    /// </summary>
    public class BaseUrl
    {
        #region ------------------------------------ CTOR ------------------------------------

        internal BaseUrl() { }

        #endregion ------------------------------------ CTOR ------------------------------------


        #region --------------------------------------- URI fragments NET45 -------------------------------------

#if NET45

        //Lazy<Uri> _StartRootUri = new Lazy<System.Uri>(() =>
        //{
        //if (StartToBuildUriFragment.IsNullOrEmpty()) return null;
        //return new Uri(StartToBuildUriFragment);
        //});

        Uri _StartRootUri = null;
        /// <summary>
        /// Start App Root Uri  -  [WebAppRoot]
        /// <para/> with values like:  [http://localhost:56923/] or  [http://lc-testpc/DEVELOPER3] ...
        /// </summary>
        public Uri StartRootUri
        {
            get
            {
                if (_StartRootUri == null)
                {
                    if (StartToBuildUriFragment.IsNullOrEmpty()) return null;
                    _StartRootUri = new Uri(StartToBuildUriFragment);
                }

                return _StartRootUri;
            }
        }




        string _StartToBuildUri = null;
        /// <summary>
        /// Uri Fragment string that used to build (Communication service/Http Handler/Controller/Package)'s Uris(svc's ).
        /// </summary>
        public string StartToBuildUriFragment 
        {
            get
            {
                if (_StartToBuildUri.IsNullOrEmpty())
                {
                    if (!StartPageFragment.IsNullOrEmpty())
                    {
                        _StartToBuildUri = StartRequestUrlFragment.Replace(StartPageFragment, "");
                    }
                    else
                        _StartToBuildUri = StartRequestUrlFragment;
                }

                return _StartToBuildUri;
            }
        }


        string _StartRequestUrlFragment;
        /// <summary>
        /// Default Server Uri's Path Fragment: all Uri path before parameters.
        /// </summary>
        public string StartRequestUrlFragment
        {
            get
            {

                if (Application.Current.StartupUri != null)
                {
                    _StartRequestUrlFragment = Application.Current.StartupUri
                        .GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);//"/"  UriEscaped
                }

                return _StartRequestUrlFragment;
            }
        }




        string _StartLocalPathFragment;

        /// <summary>
        /// Local relational path from  relational Application root folder
        ///</summary>
        public string StartLocalPathFragment
        {
            get
            {
                return _StartLocalPathFragment;
            }
        }




        string _StartSchemeAndServerFragment;


        /// <summary>
        ///   Manually Inited Default Server Uri's Path Fragment: [scheme] + [server].
        /// </summary>  
        public string StartSchemeAndServerFragment
        {
            get
            {
                if (Application.Current.StartupUri != null)
                {
                    _StartSchemeAndServerFragment = Application.Current.StartupUri
                        .GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);//"/"  UriEscaped
                }
                return _StartSchemeAndServerFragment;
            }
        }


        string _StartPageFragment;
        /// <summary>
        /// SL5 Start Page Name
        ///</summary>
        public string StartPageFragment
        {
            get
            {
                _StartPageFragment = StartRequestUrlFragment.Split('/')
                                                                .LastOrDefault();

                return _StartPageFragment;
            }

        }



        /// <summary>
        /// Manually Inited Default Server Uri's Fragment: Server Port.
        /// </summary>
        public int Port
        {
            get
            {
                return Application.Current.StartupUri.Port;
            }
        }
#endif
        #endregion --------------------------------------- URI fragments NET45  -------------------------------------


        #region --------------------------------------- URI fragments WP81 -------------------------------------

#if WP81


    

        Uri _StartRootUri = null;
        /// <summary>
        /// Start App Root Uri  -  [WebAppRoot]
        /// <para/> like:  [http://localhost:56923/] or  [http://lc-testpc/DEVELOPER3] ...
        /// </summary>
        public  Uri StartRootUri
        {
            get
            {
                if (_StartRootUri == null)
                {
                    if (StartToBuildUriFragment.IsNullOrEmpty()) return null;
                    _StartRootUri = new Uri(StartToBuildUriFragment);
                }

                return _StartRootUri;
            }
        }



        string _StartToBuildUri = null;
        /// <summary>
        /// Uri Fragment string that used to build Communication services/Http Handlers/Controllers/Packages Uris(svc's ).
        /// </summary>
        public string StartToBuildUriFragment
        {
            get
            {
                if (_StartToBuildUri.IsNullOrEmpty())
                {
                    if (!StartPageFragment.IsNullOrEmpty())
                    {
                        _StartToBuildUri = StartRequestUrlFragment.Replace(StartPageFragment, "");
                    }
                    else
                        _StartToBuildUri = StartRequestUrlFragment;
                }

                return _StartToBuildUri;
            }
        }



        string _StartRequestUrlFragment;
        
        /// <summary>
        /// Default Server Uri's Path Fragment: all Uri path before parameters, for the base we get app hosted page document address.
        /// </summary>
        public String StartRequestUrlFragment
        {
            get
            {
                return _StartRequestUrlFragment;
            }
        }



        string _StartLocalPathFragment;
      
        /// <summary>
        /// Local relational path from  relational Application root folder
        ///</summary>
        public string StartLocalPathFragment
        {
            get
            {               
                return _StartLocalPathFragment;
            }
        }






        string _StartSchemeAndServerFragment;
        
        /// <summary>
        ///   Manually Inited Default Server Uri's Path Fragment: [scheme] + [server].
        /// </summary>  
        public String StartSchemeAndServerFragment
        {
            get
            {
                return _StartSchemeAndServerFragment;
            }            
        }


        String _StartPageFragment;
        
        /// <summary>
        /// SL5 Start Page Name
        ///</summary>
        public String StartPageFragment
        {
            get
            {
               

                return _StartPageFragment;
            }

        }





        
        /// <summary>
        /// Manually Inited Default Server Uri's Fragment: Server [Port].
        /// </summary>
        public Int32 Port
        {
            get
            {
                 return HttpRoute2.StartUrl.DefaultServerUri.Port;
            }
        }

#endif

        #endregion --------------------------------------- URI fragments WP81 -------------------------------------


        #region --------------------------------------- URI fragments SL5 -------------------------------------
#if SL5

        //{ return new Uri(StartToBuildUriFragment); }  );            

        //Lazy<Uri> _StartRootUri = new Lazy<System.Uri>(() =>
        //{
        //    if (StartToBuildUriFragment.IsNullOrEmpty()) return null;
        //    return new Uri(StartToBuildUriFragment);
        //});




        ///// <summary>
        ///// Start App Root Uri  -  [WebAppRoot]
        ///// <para/> like:  [http://localhost:56923/] or  [http://lc-testpc/DEVELOPER3] ...
        ///// </summary>
        //public Uri StartRootUri
        //{
        //    get
        //    {
        //        return _StartRootUri.Value;
        //    }
        //}

        Uri _StartRootUri = null;
        /// <summary>
        /// Start App Root Uri  -  [WebAppRoot]
        /// <para/> with values like: [http://localhost:56923/] or  [http://lc-testpc/DEVELOPER3] ...
        /// </summary>
        public Uri StartRootUri
        {
            get
            {
                if (_StartRootUri == null)
                {
                    if (StartToBuildUriFragment.IsNullOrEmpty()) return null;
                    _StartRootUri = new Uri(StartToBuildUriFragment);
                }

                return _StartRootUri;
            }
        }


        String _StartToBuildUri = null;
        /// <summary>
        /// Uri Fragment string that used to build Communication services/Http Handlers/Controllers/Packages Uris(svc's ).
        /// </summary>
        public String StartToBuildUriFragment
        {
            get
            {
                if (_StartToBuildUri.IsNullOrEmpty())
                {
                    if (!StartPageFragment.IsNullOrEmpty())
                    {
                        _StartToBuildUri = StartRequestUrlFragment.Replace(StartPageFragment, "");
                    }
                    else
                        _StartToBuildUri = StartRequestUrlFragment;
                }

                return _StartToBuildUri;
            }
        }




        String _StartRequestUrlFragment = null;
        /// <summary>
        /// Default Server Uri's Path Fragment: all Uri path before parameters.
        /// </summary>
        public String StartRequestUrlFragment
        {
            get
            {

                if (_StartRequestUrlFragment == null)
                {
                    _StartRequestUrlFragment = HtmlPage.Document.DocumentUri
                        .GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);//"/"  UriEscaped
                }

                return _StartRequestUrlFragment;
            }
        }




        String _StartSchemeAndServerFragment;
        /// <summary>
        ///  Start page Uri Path Fragment: [scheme] + [server].
        /// </summary>
        public String StartSchemeAndServerFragment
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _StartSchemeAndServerFragment == null)
                {
                    _StartSchemeAndServerFragment = HtmlPage.Document.DocumentUri
                        .GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);//"/"  UriEscaped
                }
                return _StartSchemeAndServerFragment;
            }

        }




        /// <summary>
        /// Manually Inited Default Server Uri's Fragment: Server Port.
        /// </summary>
        public Int32 Port
        {
            get
            {
                return HtmlPage.Document.DocumentUri.Port;
            }
        }


        String _StartLocalPathFragment;

        /// <summary>
        /// Local relational path from  relational Application root folder
        ///</summary>
        public String StartLocalPathFragment
        {
            get
            {


                if (Application.Current.IsRunningOutOfBrowser == false && _StartLocalPathFragment == null)
                {
                    _StartLocalPathFragment = HtmlPage.Document.DocumentUri
                        .GetComponents(UriComponents.Path, UriFormat.Unescaped); // UriEscaped
                }


                return _StartLocalPathFragment;
            }
        }





        String _StartPageFragment;
        /// <summary>
        /// SL5 Start Page Name
        ///</summary>
        public String StartPageFragment
        {
            get
            {

                if (Application.Current.IsRunningOutOfBrowser == false && _StartPageFragment == null)
                {
                    _StartPageFragment = StartRequestUrlFragment.Split('/')
                                                                .LastOrDefault();
                }


                return _StartPageFragment;
            }

        }



        String _XapPackageFragment;

        /// <summary>
        /// SL5 Xap package of current Application
        ///</summary>
        public String XapPackageFragment
        {
            get
            {

                if (Application.Current.IsRunningOutOfBrowser == false && _XapPackageFragment == null)
                {
                    _XapPackageFragment = Application.Current.Host.Source
                        .GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped) // UriEscaped
                            .Split('/').LastOrDefault();
                }

                return _XapPackageFragment;
            }

        }


        String _Host;

        /// <summary>
        /// StartPage Uri Path Fragment : Host
        ///</summary>
        public String Host
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _Host == null)
                {
                    _Host = HtmlPage.Document.DocumentUri.Host;
                }


                return _Host;
            }

        }
#endif
        #endregion --------------------------------------- URI fragments SL5 -------------------------------------



        /// <summary>
        /// Get Base Uri
        /// </summary>
        /// <param name="pRelativeUri"></param>
        /// <returns></returns>
        public string GetBaseUri(string pRelativeUri)
        {
            if (pRelativeUri.IsNullOrEmpty())
            {
                return null;
            }

            string svcBaseUri = null;
            if (pRelativeUri.ToArray()[0] == '/')
            {
                svcBaseUri = StartToBuildUriFragment.Remove(StartToBuildUriFragment.Length - 1, 1) + pRelativeUri; // StartRequestUrlFragment
            }
            else
            {
                svcBaseUri = StartToBuildUriFragment + pRelativeUri;
            }

            return svcBaseUri;

        }


        /// <summary>
        /// Rebuild Base Url.It used when  BaseUrlSegmentsCountFromLocalPath will be changed
        /// </summary>
        /// <returns></returns>
        string RebuildBaseUrl()
        {
            string primaryValue = StartRequestUrlFragment.Replace(StartLocalPathFragment, "");

            //add segments if such condition is true
            if (BaseUrlSegmentsCountFromLocalPath > 0)
            {
                string[] localPathSegments = StartLocalPathFragment.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; (i < BaseUrlSegmentsCountFromLocalPath && i < localPathSegments.Length); i++)
                {
                    primaryValue += localPathSegments[i];
                }
            }

            return primaryValue;
        }

#if SL5
        string _Url;

        /// <summary>
        /// Base Url.
        /// <para/>If Applicaiotn start from browser -default server base address will be catched from as started host address - for example like silverlight application in browser.
        /// <para/> It'll be setted by the rule:  DefaultServerUri= [StartRequestUrlFragment] - [StartLocalPathFragment].         
        /// <para/> Primary Value of BaseUrl can be different with real workable valid Uri,that can be used to build Uri's for Communication Services/Units.
        /// <para/> To correct our Baseurl to the real workable valid Uri, we can use adding fragments from [StartLocalPathFragment].
        /// </summary>
        public string Url
        {
            get
            {

                if (Application.Current.IsRunningOutOfBrowser == false && _Url == null)
                {
                    _Url = RebuildBaseUrl();
                }

                return _Url;
            }
            private set
            {
                if (!String.IsNullOrEmpty(value) && Uri.IsWellFormedUriString(value, UriKind.RelativeOrAbsolute))
                {
                    _Url = value;
                }
            }
        }


#elif WPF || WP81

        Uri _DefaultServerUrl;


        /// <summary>
        /// DefaultServer Uri.       
        /// <para/>If Applicaiotn start from browser -default server base address will be catched from as started host address - for example like silverlight application in browser.
        /// <para/> It'll be setted by the rule:  DefaultServerUri= [StartRequestUrlFragment] - [StartLocalPathFragment].         
        /// <para/> Primary Value of BaseUrl can be different with real workable valid Uri,that can be used to build Uri's for Communication Services/Units.
        /// <para/> To correct our Baseurl to the real workable valid Uri, we can use adding fragments from [StartLocalPathFragment].
        /// </summary>
        public Uri DefaultServerUri
        {
            get
            {
                return _DefaultServerUrl;
            }
            private set
            {
                _DefaultServerUrl = value;

            }
        }



        /// <summary>
        /// Each client can set(or change it's default server Uri address). 
        /// <para/> By default server we beleave server from where we can load last modules, configurations, data ...  
        /// <para/> Just right ComponentsTasksManager- use this address by default to check application components-modules with last versions. 
        /// </summary>
        /// <param name="defalutServerUri"></param>
        public void SetDefalutServerUri(String defalutServerUri)
        {
            if (!String.IsNullOrEmpty(defalutServerUri) && Uri.IsWellFormedUriString(defalutServerUri, UriKind.RelativeOrAbsolute))
            {
                _DefaultServerUrl = new Uri(defalutServerUri);
            }
        }


#endif





        int _BaseUrlSegmentsCountFromLocalPath = 0;

        /// <summary>
        /// Segments Count in [StartLocalPathFragment], those we should add to the standart value of BaseUrl.
        /// <para/> If  value of segments  you set  is more than segments count in [StartLocalPathFragment] (we always excluding the  page  document name) -   then  any of segments won’t  be added to it  target url-because they are not exist simply. We just tune here segments count from StartLocalPath that we need to add to  DefaultServerUri .
        /// <para/> And this value should be setted manually – it’s not autocomputed value.
        /// </summary>
        public int BaseUrlSegmentsCountFromLocalPath
        {
            get
            {
                return _BaseUrlSegmentsCountFromLocalPath;
            }

            set
            {

                _BaseUrlSegmentsCountFromLocalPath = value;

                //updating all states
                _IsManuallySetLocalPathSegemnts = true;
                _IsBaseUrlCheckedAndValid = false;
                _IsValidBaseUrl = false;
                //algorithm of changing segments in BaseUrl. - new value of BaseUrlSegmentsCountFromLocalPath  must change BaseUrl
                RebuildBaseUrl();
            }
        }



        #region  ------------------------------- Auto Checking  Base Url for  valid value ---------------------------------------------

        //autodetect in WebApiCommunication BaseUrl by test calls of WebApiControllerBase

        bool _IsBaseUrlCheckedAndValid = false;
        /// <summary>
        /// Testing   BaseUrl for valid address. If not valid   after test then IsValidBaseUrl= false/  true if valid after first some service call or test service call 
        /// </summary>
        public bool IsBaseUrlCheckedAndValid
        {
            get
            {
                return _IsBaseUrlCheckedAndValid;
            }
            internal set
            {
                _IsBaseUrlCheckedAndValid = value;
            }
        }



        bool _IsValidBaseUrl = false;
        /// <summary>
        /// Is BaseUrl value  tested and it's value is - [OK] and is not -[NOT FOUND]
        /// </summary>
        public bool IsValidBaseUrl
        {
            get
            {
                return _IsValidBaseUrl;
            }
            internal set
            {
                _IsValidBaseUrl = value;
            }
        }


        #endregion ------------------------------- Auto Checking  Base Url for  valid value ---------------------------------------------




        // manually we can set counts of of fragments that we need to add to primary result of BaseUrl            
        bool _IsManuallySetLocalPathSegemnts = false;


        /// <summary>
        /// Count of segments that we  added to  BaseUrl from StartLocalPath manually        
        /// </summary> 
        public bool IsManuallySetLocalPathSegemnts
        {
            get
            {
                return _IsManuallySetLocalPathSegemnts;
            }
            internal set
            {
                _IsManuallySetLocalPathSegemnts = value;
            }
        }



        string _WebApplication;
        /// <summary>
        /// WebApplication segment only - if (BaseUrlSegmentsCountFromLocalPath > 0) then WebApplication segment - it's the first segment of StartLocalPath segments.        
        /// </summary>
        public string WebApplication
        {
            get
            {
#if SL5
                if (Application.Current.IsRunningOutOfBrowser == false && _WebApplication == null && BaseUrlSegmentsCountFromLocalPath > 0)
                {
                    _WebApplication = StartLocalPathFragment.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                }
#endif
                if( BaseUrlSegmentsCountFromLocalPath > 0 )
                {
                    _WebApplication = StartLocalPathFragment.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
                }

                return _WebApplication;
            }

        }



    }

}




#region ------------------------------- GARBAGE ----------------------------------------
//if (Application.Current.IsRunningOutOfBrowser == false && _StartPageFragment == null)
//{
//    _StartPageFragment = StartRequestUrlFragment.Split('/')
//                                                .LastOrDefault();
//}


// Lazy<Uri> _StartRootUri = new Lazy<System.Uri>(() =>        
//{
//    if (StartToBuildUriFragment.IsNullOrEmpty()) return null;            
//    return new Uri( StartToBuildUriFragment);
//});

///// <summary>
///// Start App Root Uri  -  [WebAppRoot]
///// <para/> with values like: [http://localhost:56923/] or  [http://lc-testpc/DEVELOPER3] ...
///// </summary>
//public Uri StartRootUri
//{
//    get
//    {
//        return _StartRootUri.Value;
//    }
//}


#endregion ------------------------------- GARBAGE ----------------------------------------

