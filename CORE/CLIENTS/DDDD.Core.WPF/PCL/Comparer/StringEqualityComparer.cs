﻿#if CLIENT && (SL5 || WP81)

using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Collections
{
    public class StringEqualityComparer : IEqualityComparer<string>
    {
        private readonly IEqualityComparer comparer;

        public StringEqualityComparer(IEqualityComparer comparer)
        {
            this.comparer = comparer;
        }

        public bool Equals(string x, string y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return this.comparer.Equals((object)x, (object)y);
        }

        public int GetHashCode(string obj)
        {
            return this.comparer.GetHashCode((object)obj);
        }
    }
}

#endif