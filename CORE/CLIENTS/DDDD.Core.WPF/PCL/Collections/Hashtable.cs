﻿#if CLIENT && (SL5 || WP81)

using System.Collections.Generic;

namespace DDDD.Core.Collections
{
    public class Hashtable : Dictionary<string, object>
    {
        public Hashtable(IDictionary<string, object> dictionary, IEqualityComparer<string> comparer)
          : base(dictionary, comparer)
        {
        }

        public Hashtable(int capacity, IEqualityComparer<string> comparer)
          : base(capacity, comparer)
        {
        }

        public Hashtable(int capacity)
          : base(capacity)
        {
        }

        public Hashtable(IDictionary<string, object> dictionary)
          : base(dictionary)
        {
        }

        public Hashtable(IEqualityComparer<string> comparer)
          : base(comparer)
        {
        }

        public Hashtable()
        {
        }
    }
}

#endif