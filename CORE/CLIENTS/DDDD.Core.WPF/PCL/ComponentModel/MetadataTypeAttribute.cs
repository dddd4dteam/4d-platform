﻿#if CLIENT && (SL5 || WP81)


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.ComponentModel.DataAnnotations
{


    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class MetadataTypeAttribute : Attribute
    {

        /// <summary>
        ///     Initializes a new instance of the System.ComponentModel.DataAnnotations.MetadataTypeAttribute
        ///     class.
        /// </summary>
        /// <param name="metadataClassType">The metadata class to reference.</param>
        /// <exception cref="T:System.ArgumentNullException">metadataClassType is null</exception>        
        public MetadataTypeAttribute(Type metadataClassType)
        {
            if (metadataClassType == null) throw new ArgumentNullException(" MetadataTypeAttribute ctor craetion - metadataClassType  parameter cannot be null.");
            
            MetadataClassType = metadataClassType;
        }
        
       
        /// <summary>
        /// Gets the metadata class that is associated with a data-model partial class.
        /// </summary>        
        public Type MetadataClassType { get; private set; }
    }
}

#endif