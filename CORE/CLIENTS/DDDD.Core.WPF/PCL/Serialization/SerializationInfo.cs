﻿
#if CLIENT && (SL5 || WP81)

using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Collections
{

    public sealed class SerializationInfo
    {
        private Dictionary<string, SerializationEntry> serialized = new Dictionary<string, SerializationEntry>();
        private List<SerializationEntry> values = new List<SerializationEntry>();
        private string assemblyName;
        private string fullTypeName;
        private IFormatterConverter converter;

        public string AssemblyName
        {
            get
            {
                return this.assemblyName;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Argument is null.");
                this.assemblyName = value;
            }
        }

        public string FullTypeName
        {
            get
            {
                return this.fullTypeName;
            }
            set
            {
                if (value == null)
                    throw new ArgumentNullException("Argument is null.");
                this.fullTypeName = value;
            }
        }

        public int MemberCount
        {
            get
            {
                return this.serialized.Count;
            }
        }

        private SerializationInfo(Type type)
        {
            this.assemblyName = type.AssemblyQualifiedName;
            this.fullTypeName = type.FullName;
            this.converter = (IFormatterConverter)new FormatterConverter();
        }

        private SerializationInfo(Type type, SerializationEntry[] data)
        {
            int length = data.Length;
            this.assemblyName = type.AssemblyQualifiedName;
            this.fullTypeName = type.FullName;
            this.converter = (IFormatterConverter)new FormatterConverter();
            for (int index = 0; index < length; ++index)
            {
                this.serialized.Add(data[index].Name, data[index]);
                this.values.Add(data[index]);
            }
        }

        [CLSCompliant(false)]
        public SerializationInfo(Type type, IFormatterConverter converter)
        {
            if (type == null)
                throw new ArgumentNullException("type", "Null argument");
            if (converter == null)
                throw new ArgumentNullException("converter", "Null argument");
            this.converter = converter;
            this.assemblyName = type.AssemblyQualifiedName;
            this.fullTypeName = type.FullName;
        }

        public void AddValue(string name, object value, Type type)
        {
            if (name == null)
                throw new ArgumentNullException("name is null");
            if (type == null)
                throw new ArgumentNullException("type is null");
            if (this.serialized.ContainsKey(name))
                throw new SerializationException("Value has been serialized already.");
            SerializationEntry serializationEntry = new SerializationEntry(name, type, value);
            this.serialized.Add(name, serializationEntry);
            this.values.Add(serializationEntry);
        }

        public object GetValue(string name, Type type)
        {
            if (name == null)
                throw new ArgumentNullException("name is null.");
            if (type == null)
                throw new ArgumentNullException("type");
            if (!this.serialized.ContainsKey(name))
                throw new SerializationException("No element named " + name + " could be found.");
            SerializationEntry serializationEntry = this.serialized[name];
            if (serializationEntry.Value != null && !type.IsAssignableFrom(serializationEntry.Value.GetType()))
                return this.converter.Convert(serializationEntry.Value, type);
            return serializationEntry.Value;
        }

        internal bool HasKey(string name)
        {
            return this.serialized.ContainsKey(name);
        }

        public void SetType(Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type is null.");
            this.fullTypeName = type.FullName;
            this.assemblyName = type.AssemblyQualifiedName;
        }

        public SerializationInfoEnumerator GetEnumerator()
        {
            return new SerializationInfoEnumerator((IEnumerable)this.values);
        }

        public void AddValue(string name, short value)
        {
            this.AddValue(name, (object)value, typeof(short));
        }

        [CLSCompliant(false)]
        public void AddValue(string name, ushort value)
        {
            this.AddValue(name, (object)value, typeof(ushort));
        }

        public void AddValue(string name, int value)
        {
            this.AddValue(name, (object)value, typeof(int));
        }

        public void AddValue(string name, byte value)
        {
            this.AddValue(name, (object)value, typeof(byte));
        }

        public void AddValue(string name, bool value)
        {
            this.AddValue(name, (object)(value ? true : false), typeof(bool)); //(bool)
        }

        public void AddValue(string name, char value)
        {
            this.AddValue(name, (object)value, typeof(char));
        }

        [CLSCompliant(false)]
        public void AddValue(string name, sbyte value)
        {
            this.AddValue(name, (object)value, typeof(sbyte));
        }

        public void AddValue(string name, double value)
        {
            this.AddValue(name, (object)value, typeof(double));
        }

        public void AddValue(string name, Decimal value)
        {
            this.AddValue(name, (object)value, typeof(Decimal));
        }

        public void AddValue(string name, DateTime value)
        {
            this.AddValue(name, (object)value, typeof(DateTime));
        }

        public void AddValue(string name, float value)
        {
            this.AddValue(name, (object)value, typeof(float));
        }

        [CLSCompliant(false)]
        public void AddValue(string name, uint value)
        {
            this.AddValue(name, (object)value, typeof(uint));
        }

        public void AddValue(string name, long value)
        {
            this.AddValue(name, (object)value, typeof(long));
        }

        [CLSCompliant(false)]
        public void AddValue(string name, ulong value)
        {
            this.AddValue(name, (object)value, typeof(ulong));
        }

        public void AddValue(string name, object value)
        {
            if (value == null)
                this.AddValue(name, value, typeof(object));
            else
                this.AddValue(name, value, value.GetType());
        }

        public bool GetBoolean(string name)
        {
            return this.converter.ToBoolean(this.GetValue(name, typeof(bool)));
        }

        public byte GetByte(string name)
        {
            return this.converter.ToByte(this.GetValue(name, typeof(byte)));
        }

        public char GetChar(string name)
        {
            return this.converter.ToChar(this.GetValue(name, typeof(char)));
        }

        public DateTime GetDateTime(string name)
        {
            return this.converter.ToDateTime(this.GetValue(name, typeof(DateTime)));
        }

        public Decimal GetDecimal(string name)
        {
            return this.converter.ToDecimal(this.GetValue(name, typeof(Decimal)));
        }

        public double GetDouble(string name)
        {
            return this.converter.ToDouble(this.GetValue(name, typeof(double)));
        }

        public short GetInt16(string name)
        {
            return this.converter.ToInt16(this.GetValue(name, typeof(short)));
        }

        public int GetInt32(string name)
        {
            return this.converter.ToInt32(this.GetValue(name, typeof(int)));
        }

        public long GetInt64(string name)
        {
            return this.converter.ToInt64(this.GetValue(name, typeof(long)));
        }

        [CLSCompliant(false)]
        public sbyte GetSByte(string name)
        {
            return this.converter.ToSByte(this.GetValue(name, typeof(sbyte)));
        }

        public float GetSingle(string name)
        {
            return this.converter.ToSingle(this.GetValue(name, typeof(float)));
        }

        public string GetString(string name)
        {
            object obj = this.GetValue(name, typeof(string));
            if (obj == null)
                return (string)null;
            return this.converter.ToString(obj);
        }

        [CLSCompliant(false)]
        public ushort GetUInt16(string name)
        {
            return this.converter.ToUInt16(this.GetValue(name, typeof(ushort)));
        }

        [CLSCompliant(false)]
        public uint GetUInt32(string name)
        {
            return this.converter.ToUInt32(this.GetValue(name, typeof(uint)));
        }

        [CLSCompliant(false)]
        public ulong GetUInt64(string name)
        {
            return this.converter.ToUInt64(this.GetValue(name, typeof(ulong)));
        }

        private SerializationEntry[] get_entries()
        {
            SerializationEntry[] serializationEntryArray = new SerializationEntry[this.MemberCount];
            int num = 0;
            foreach (SerializationEntry serializationEntry in this)
                serializationEntryArray[num++] = serializationEntry;
            return serializationEntryArray;
        }
    }


}

#endif
