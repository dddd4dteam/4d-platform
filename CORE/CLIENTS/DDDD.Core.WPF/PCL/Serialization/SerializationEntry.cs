﻿#if CLIENT && (SL5 || WP81)

using System;
using System.Runtime.InteropServices;

namespace DDDD.Core.Collections
{

    [ComVisible(true)]
    public struct SerializationEntry
    {
        private string name;
        private Type objectType;
        private object value;

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        public Type ObjectType
        {
            get
            {
                return this.objectType;
            }
        }

        public object Value
        {
            get
            {
                return this.value;
            }
        }

        internal SerializationEntry(string name, Type type, object value)
        {
            this.name = name;
            objectType = type;
            this.value = value;
        }
    }
}

#endif