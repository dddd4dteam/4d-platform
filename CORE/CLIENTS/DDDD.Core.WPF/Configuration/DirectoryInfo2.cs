﻿using System;

using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;

namespace DDDD.Core.Configuration
{

    /// <summary>
    /// Directory Info - PhysicalPath of current dir, Relation to root Node Dir path, and Node Type of this Physical Directory: Category or Content Item Node Dir type. 
    /// </summary>
    public class DirectoryInfo2
    {

        #region -------------------------- CONST --------------------------------
        
        /// <summary>
        /// Category  prefix in folder name
        /// </summary>
        public const string CategoryPref = "CG";

        /// <summary>
        /// Content Item prefix in folder name
        /// </summary>
        public const string ContentPref = "CNT";


        #endregion -------------------------- CONST --------------------------------


        #region ------------------------------------ CTOR ------------------------------------


        /// <summary>
        /// Directory Info - PhysicalPath of current dir, Relation to root Node Dir path, and Node Type of this Physical Directory: Category or Content Item Node Dir type. 
        /// </summary>
        /// <param name="physicalPath"></param>
        /// <param name="rootPath"></param>
        public DirectoryInfo2(string physicalPath,string rootPath)
        {
            Validator.ATNullReferenceArgDbg(physicalPath, nameof(DirectoryInfo2), nameof(DirectoryInfo2), nameof(physicalPath));

            //PhysicalPath
            PhysicalPath = physicalPath;
            if (!PhysicalPath.EndsWith(@"\")) PhysicalPath += @"\";
            //PhysicalPath segments
            var segments = PhysicalPath.SplitNoEntries(@"\");
            Validator.ATInvalidOperationDbg(segments.Count == 0, nameof(DirectoryInfo2), nameof(DirectoryInfo2), "Invalid PhysicalPath value. Segments count is 0");

            //FoderName
            FolderName = segments[segments.Count - 1];

            // Directory Type
            if (FolderName.StartsWith(CategoryPref)) DirectoryType = DirectoryTypeEn.CategoryDir;
            else if (FolderName.StartsWith(ContentPref)) DirectoryType = DirectoryTypeEn.ContentItemDir;
            else if (FolderName.S() == "image"
                     || FolderName.S() == "audio"
                     || FolderName.S() == "video"
                     || FolderName.S() == "text"
                      || FolderName.S() == "application"
                    ) DirectoryType = DirectoryTypeEn.DataFilesDir;
            else throw new InvalidOperationException("[{0}].[{1}]() - ERROR message : Can't determine  Node Type of directory - [{2}] "
                                                     .Fmt(nameof(DirectoryInfo2), nameof(DirectoryInfo2), PhysicalPath));

            //FolderNameIndex
            if (DirectoryType == DirectoryTypeEn.CategoryDir)
            {
                FolderNameIndex = int.Parse(FolderName.Replace(CategoryPref, ""));
            }
            else if (DirectoryType == DirectoryTypeEn.ContentItemDir)
            {
                FolderNameIndex = int.Parse(FolderName.Replace(ContentPref, ""));
            }

            UpdateDirectoryRelations(rootPath);           
        }


        #endregion ------------------------------------ CTOR ------------------------------------



        #region -------------------------- PROPERTIES ----------------------------------


        /// <summary>
        /// Directory Type -  what is Node Type of this Physical Directory: Category or Content Item Node.
        /// </summary>
        public DirectoryTypeEn DirectoryType
        {
            get; private set;
        }

        /// <summary>
        /// FolderName -last segment in Physical path.
        /// </summary>
        public string FolderName
        { get; private set; }

        /// <summary>
        /// Index of folder in FolderName, like - [CG]_Index, or [CNT]_Index.
        /// </summary>
        public int FolderNameIndex
        { get; private set; }

        /// <summary>
        /// Path of current Dir.
        /// </summary>
        public string PhysicalPath
        { get; private set; }

        /// <summary>
        ///  Relative to Root Node Dir path .
        /// </summary>
        public string RelativePath
        { get; private set; }


        #endregion -------------------------- PROPERTIES ----------------------------------

        /// <summary>
        /// Update relative path from Root Node Directory.
        /// </summary>
        /// <param name="rootPhysicalpath"></param>
        public void UpdateDirectoryRelations(string rootPhysicalpath)
        {
            RelativePath = PhysicalPath.Replace(rootPhysicalpath, "");
        }
    }
} 