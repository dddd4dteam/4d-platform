﻿

//using DDDD.BCL;

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;

using DDDD.Core.Extensions;
using DDDD.Core.Events;
using DDDD.Core.Diagnostics;
using DDDD.Core.Resources;

namespace DDDD.Core.Configuration.ECE
{

    /// <summary>
    /// Enum Parameter will be created after Parsing some Enum Type field.
    /// It will contains your Custom value(target param value) inside Content Property. 
    /// Sterling Indexes:      Kind, ParamTypeName, ParamTypeCode, FinalType 
    /// </summary>
    public class EnumParameter : INotifyPropertyChanged
    {

        #region ------------------------------ CTOR ---------------------------------------

        public EnumParameter()
        {
            Initialize();
        }

        #endregion ------------------------------ CTOR ---------------------------------------

        // *** Lock ***
        private object PropertyLock = new object();


        #region -------------------------- Initialization -----------------------------------

        private void Initialize()
        {
            Locales = new Dictionary<String, EnumParamCultureLocales>();
        }

        #endregion -------------------------- Initialization -----------------------------------





        #region ------------------------------INotifyPropertyChanged--------------------------------


        private readonly   WeakDelegatesManager propertyChangedWDManager = new WeakDelegatesManager();


        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                //PropertyChangedEventManager. AddHandler( value);
                propertyChangedWDManager.AddListener(value);
            }
            remove
            {
                propertyChangedWDManager.RemoveListener(value);
            }
        }

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }

            propertyChangedWDManager.Raise(new PropertyChangedEventArgs(propertyInfo.Name));

        }


        #endregion ------------------------------INotifyPropertyChanged--------------------------------



        #region  --------------------------------- TYPOLOGY -------------------------------------


        /// <summary>
        /// EnunConfigurationEngine  parameter Classification - Kind of Target Param Type.
        /// </summary>
        public ParamKindEn Kind
        {
            get;
            internal set;
        }


        /// <summary>
        /// EnumParameter Type FullName
        /// </summary>
        public String ParamTypeName
        {
            get { return ParamType.Name; }
        }




        /// <summary>
        /// Тип параметра. Времено все виды содержимого могут нахриться в упакованном в объект виде. 
        /// Но данное свойство говорит что мы собираемся проверять валидность типа  возвращаемого значения параметра.
        /// </summary>
        public Type ParamType
        {
            get;
            internal set;
        }

        /// <summary>
        /// Current Content Type it can be different than ParamtType in case when ValidationLevel == 0
        /// </summary>
        public Type CurrentType
        {
            get
            {
                if (Content != null) return Content.GetType();
                else return typeof(object);
            }
        }



        string[] _NamespaceParts = null;
        /// <summary>
        /// Парты неймспайса параметра. Можно использовать для поиска
        /// </summary>
        public string[] NamespaceParts
        {
            get
            {
                if (_NamespaceParts == null)
                    _NamespaceParts = ParamType.Namespace.Split(new char[] { '.' });
                return _NamespaceParts;
            }
        }



        #endregion --------------------------------- TYPOLOGY -------------------------------------


        #region  ---------------------------- identification ---------------------------------

        string _GlobalKey = null;
        /// <summary>
        /// Global String ParamItemKey that build with the help of ConfigEnum.ItemKeyMask 
        /// </summary>
        public String GlobalKey
        {
            get
            {
                return _GlobalKey;
            }
            internal set
            {
                _GlobalKey = value;
                GlobalID = value.GetHashCode();
            }
        }

        /// <summary>
        /// Global Configuration Engine Dictionary Key 
        /// </summary>
        public Int32 GlobalID
        {
            get;
            private set;

        }



        /// <summary>
        /// Global MaskKey used to build end GlobalKey string.
        /// </summary>
        public String GlobalKeyMask
        {
            get;
            internal set;
        }




        /// <summary>
        /// Тип родительского Перечисления конфигурации 
        /// </summary>
        public Type ParentEnumType
        {
            get;
            internal set;
        }

        /// <summary>
        /// Родительское перечисление. поле параметра  :  "CustomEnum.FiledValue"
        /// </summary>
        public String EnumValueKey
        {
            get;
            internal set;
        }

        #endregion ---------------------------- Identification ---------------------------------



        #region ------------------------------- TARGET PARAM CONTENT ----------------------------------

        /// <summary>
        /// Param Value: DefaultValue/ CustomValue
        /// </summary>
        public object Content
        {
            get;
            private set;
        }

        /// <summary>
        /// Some Additional to Content  value - for Ex: MethodName to get Delegate value as Value
        /// </summary>
        public object Tag
        {
            get;
            private set;
        }


        #endregion ------------------------------- TARGET PARAM CONTENT ----------------------------------


        #region --------------------------------  SEARCH Filters ---------------------------------

        /// <summary>
        ///  Группировка парамтера- Группа.  Фильтр для дальнейшего поиска параметра в общей таблице
        /// </summary>
        public String GroupFilter
        {
            get;
            set;
        }


        /// <summary>
        /// Группировка парамтера- ПодГруппа.  Фильтр для дальнейшего поиска параметра в общей таблице
        /// </summary>
        public String SubGroupFilter
        {
            get;
            set;
        }


        #endregion -------------------------------- SEARCH Filters --------------------------------


        #region ----------------------------------- Validation ---------------------------------------

        /// <summary>
        ///  Validation Level - validation ( Content only value) behavior.   Can be setted  only as 0 or 1 value
        ///  If Validation Level is 0 - it means exception won't throw - it's, the Parameter means nothing
        ///  If Validation Level is 1 - exception will throw- the Parameter is important  - for your Object Model
        /// </summary>
        public Int32 ValidationLevel
        {
            get;
            internal set;
        }




        internal void ValidateNewContent(object NewContent)
        {
            //new value after auto Parse simple type value from string
            if (ValidationLevel == 1)
            {
                var ntype = NewContent.GetType();

                Validator.ATInvalidOperationDbg( (ParamType.IsNullable() == false && NewContent == null)
                    ,nameof(EnumParameter),nameof(ValidateNewContent)
                    ,"ParamType is not nullable type, but NewContent is null - so value cannot be setted to null");

                Validator.ATInvalidOperationDbg(  (ntype != ParamType &&  !ntype.IsSubclassOf(ParamType))
                    ,nameof(EnumParameter), nameof(ValidateNewContent)
                    , " New Content value cannot be different type than ParamType was declared " );
                
            }
        }




        #endregion ----------------------------------- Validation ---------------------------------------


        #region  ----------------------------------Lazy/ Loading Value ----------------------------------
        /// <summary>
        /// 
        /// </summary>
        public String LoadPath
        {
            get;
            internal set;
        }


        /// <summary>
        /// 
        /// </summary>
        public bool IsLazyLoad
        {
            get;
            internal set;
        }


        /// <summary>
        /// Indicator that shows Loaded State
        /// </summary>
        public bool IsLoaded
        {
            get;
            private set;
        }

        #endregion ----------------------------------Lazy/ Loading Value ----------------------------------



        #region ----------------------------  Persistance (SterlingDB Store Engine ) -----------------------------------

        /// <summary>
        /// if we need to store this configuration param in the EnumConfigurationEngine.PersistanceStore
        /// </summary>
        public bool IsPersistable
        {
            get;
            internal set;
        }

        /// <summary>
        /// Comparing  other EnumParameter with current instance 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            var other = obj as EnumParameter;
            if (other == null)
            {
                return false;
            }

            return GlobalKey.Equals(other.GlobalKey);

        }

        /// <summary>
        /// Getting Hash Code  equal EnumParameter.GlobalID value
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return GlobalID;
        }



        #endregion ----------------------------  Persistance (SterlingDB Store Engine ) -----------------------------------







        #region ----------------------------------- LOCALIZATION SUPPORT ---------------------------
        //AssemblyName - CTG.Core
        //FileName - [AssemblyName].Resources.resx -  
        //FilePath - [AssemblyName]\Resources\[FileName]
        //ClassName -[AssemblyName(./_)]_Resources + public access
        //ClassProperty - [ClassName].ResourceManager


        ///Dictionary<CultureInfo,bool> IsFirstTimeLoad
        ///ResourceManager LocaleResourceManager = null;
        ///static ResourceManager GetResourceManager()
        ///{
        ///    return null;
        ///}


        internal Dictionary<String, EnumParamCultureLocales> Locales
        {
            get;
            set;
        }


        /// <summary>
        /// Ключ локализации ИМЕНИ параметра.
        /// Используется для получения значения LocalName данного параметра на текущем языке установленной глобальной культуры
        /// </summary>
        public String LocalNameKey
        {
            get;
            set;
        }


        /// <summary>
        /// Значение  ИМЕНИ параметра полученное с поможщью Ключ локализации(LocalNameKey)
        /// </summary>
        public String LocalName
        {
            get
            {
                //if (LocaleResourceManager == null && IsResManagerTriedToLoad == false)
                //{
                //    LocaleResourceManager = GetResourceManager();
                //    IsResManagerTriedToLoad = true;
                //}

                //if (LocaleResourceManager != null)
                //{
                //    return LocaleResourceManager.GetString(LocalNameKey, CultureInfo.CurrentCulture.DisplayName);
                //}

                return null;
            }
            set
            {
                //
                //NotifyPropertyChanged
            }

        }

        /// <summary>
        /// Ключ локализации  ОПИСАНИЯ данного параметра. Используется для получения значения LocalDescription текущем языке установленной глобальной культуры
        /// </summary>
        public String LocalDescriptionKey
        {
            get;
            set;
        }

        /// <summary>
        /// Значение  ОПИСАНИЯ параметра полученное с поможщью Ключ локализации(LocalDescriptionKey) 
        /// </summary>
        public String LocalDescription
        {
            get
            {
                //if (LocaleResourceManager == null && IsResManagerTriedToLoad == false)
                //{
                //    LocaleResourceManager = GetResourceManager();
                //    IsResManagerTriedToLoad = true;
                //}

                //if (LocaleResourceManager != null)
                //{
                //    return LocaleResourceManager.GetString(LocalDescriptionKey, CultureInfo.CurrentCulture);
                //}

                return null;
                //get by EnumType - from assembly where it's defined. We trying to search integrated ResourceDictionary and get resource by LocalParamNameKey  
            }
            set
            {
                //
                //NotifyPropertyChanged
            }

        }




        //Первичное Получение ресурса из сборки енама

        private void SaveResource(Type EnumConfig, String ParamResourceKey, String ParamValue)
        {
            //EnumConfigurationEngine.ContainsParam()
        }



        #endregion ----------------------------------- LOCALIZATION SUPPORT ---------------------------





        #region  --------------------------------------- LAZY LOADING Contents -----------------------------------------


        /// <summary>
        /// 
        /// </summary>
        private void LoadLazyXmlRefValue(String ResourceKey, bool NeedDeserialize, bool NeedUnpack)
        {
            String XmlStringRes = null;
            //if (Kind == ParamKindEn.XmlRef && ParamType != null)
            //{   //check Assembly param value
            //}

            if (NeedUnpack)
            {
            }

            if (NeedDeserialize)
            {
                //RuntimeTypeModel.Default.Default[typeof(Message)] .AddSubType(1, typeof(LogonRequest));
                //ProtoBuf.Serializer.NonGeneric.
                //ProtoBuf.Serializer.DeserializeWithLengthPrefix<
                //ProtoBuf.Serializer.GlobalOptions.
                //ProtoBuf.Serializer.Deserialize(ResourceLoader.LoadStringStream(ResourceKey) , ParamType);
            }

            XmlStringRes = ResourceLoader.LoadStringRes(ResourceKey);

        }

        #endregion --------------------------------------- LAZY LOADING Contents -----------------------------------------






        internal static List<String> CollectFlags(String inputData, String Pattern, string[] KnownKeys)
        {
            // Collecting flags
            //string pattern = @"\[([A-Za-z0-9]+)\]|\{([A-Za-z0-9]+)\}";
            Regex regex = new Regex(Pattern);
            string[] splitArray = regex.Split(inputData);
            List<string> FlagArray = new List<string>();

            foreach (var item in splitArray)
            {
                if (String.IsNullOrEmpty(item)) continue;

                string clearItem = item.TrimSymbols(new string[] { "[", "]", "{", "}", ".", ",", ":", ";" }, "");

                if (String.IsNullOrEmpty(clearItem)) continue;

                if (KnownKeys.Contains(clearItem) == false)
                {
                    FlagArray.Add(item);
                }
            }


            return FlagArray;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="NewValue"></param>
        //internal void ResetValue(Type ConfigEnumType, String pGlobalKey, String pGlobalKeyMask, String SimpleEnumValueKey, string[] KnownKeys)
        //{

        //    GlobalKeyMask = pGlobalKeyMask;

        //    GlobalKey = pGlobalKey;

        //    SemanticFlags = CollectFlags(pGlobalKeyMask, EnumConfigurationEngine.ParamKeyPattern, KnownKeys);

        //    ParentEnumType = ConfigEnumType;

        //    EnumValueKey = SimpleEnumValueKey;
        //}
        
        
        internal void UpdateContentOnly(object newValue)
        {
            Content = newValue;
        }

        internal void UpdateTagOnly(object newValue)
        {
            Tag = newValue;
        }
        
    }



}
