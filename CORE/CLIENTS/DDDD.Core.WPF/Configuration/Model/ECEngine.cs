﻿

using System;
using System.Linq;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;


using DDDD.Core.Extensions;
using DDDD.Core.Patterns;
using DDDD.Core.Diagnostics;
using System.Windows;

namespace DDDD.Core.Configuration.ECE
{

    /// <summary>
    /// Enum Configuration Engine - let us the direct storing(save load collections) for complex structured types.
    /// Here we use enum values as keys for Collections{T} where T is complex - not plain struct or class.
    /// To de/serialize  data we use DDDD.SDS binary serializer.
    /// Data Collections will be stored in order as [file for each Collections{T}].
    /// We simply should define relative path for Collection data File or use default rules. 
    /// On server  or desktop we use file system to sore files, on  clients we'll use IsolationStore.
    /// </summary>
    public class ECEngine : Singleton<ECEngine> //INotifyPropertyChanged
    {

        #region ------------ Singleton  Constructor -----------------------

        // PARAMS :     
        // Class   -         ECEngine            EX: SystemDispatcher - Singleton Class Name           
        // ClassDesc  -      Enum COnfiguration Engine Description        EX:  

        /// <summary>
        ///  Target Class - ECEngine -  Ctor
        /// </summary>
        ECEngine()
        {
            //  Initializing  Target Class   
            Initialize();

        }

        /// <summary>
        /// Singleton Getting Property.
        /// Enum Configuration Engine Description
        /// </summary>     
        public static ECEngine Current
        {
            get
            {
                return LA_Current.Value; 
            }
        }
 
        #endregion   ----------- Singleton  Constructor -----------------------


        #region  ------------------------ CONST -----------------------------


        internal const String ParamKeyPattern = @"\[([A-Za-z0-9]+)\]|\{([A-Za-z0-9]+)\}";
        const string ParamNotContains = "ParamNotContains";


        #endregion ------------------------ CONST -----------------------------


        #region ------------------------------------ FIELDS -----------------------------------

        private static readonly object _lockObject = new object();

        #endregion ------------------------------------ FIELDS -----------------------------------


       

        #region -------------------------------- INITIALIZATION ---------------------------------

        /// <summary>
        /// not exist in Params 
        /// </summary>
        /// <param name="initParams"></param>
        protected virtual void Initialize()
        {
            Params = new Dictionary<long, EnumParameter>();
            ConfigEnums = new Dictionary<Int32, Type>();
            ParsedAssemblies = new List<String>();
            ParameterKeyParts = new Dictionary<string, Func<Type, FieldInfo, String>>();

            //Initing Storage - SterlingDB
            //PersistEngine = new SterlingEngine();

            //if (Debugger.IsAttached)
            //{
            //    Logger = new SterlingDefaultLogger(SterlingLogLevel.Verbose);
            //}

            ////PersistEngine.SterlingDatabase.RegisterSerializer<MySerializer>();

            //PersistEngine.Activate();
            //PersistDatabase = PersistEngine.SterlingDatabase.RegisterDatabase<EnumConfigPersistDatabase>();

            //_observer = new PropertyObserver<ECEngine>(this)
            //            .RegisterHandler(n => n.IsInitialized, OnInitializedHandler);

            InitParameterKeyParts();

            if (IsInitialized == false)
            { IsInitialized = true; }

        }


        /// <summary>
        /// If ECE initialized
        /// </summary>
        public bool IsInitialized
        {  get; private set;   }


        private static void OnInitializedHandler(ECEngine engine)
        {
            //TO DO

        }

        #endregion -------------------------------- INITIALIZATION ----------------------------------
        



        #region ---------------------SYNTAX KEY- indexator -------------------

        /// <summary>
        /// Getting EnumParameter by GlobalID  of Enum.Field 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public EnumParameter this[Int32 key]
        {
            get
            {
                if (Params.ContainsKey(key))
                    return Params[key];
                else
                    return null;
            }
        }

        /// <summary>
        /// Getting EnumParameter from Configuration Engine
        /// </summary>
        /// <param name="EnumKey"></param>
        /// <returns></returns>
        public EnumParameter this[Enum EnumKey]
        {
            get
            {
                Int32? BuildedKey = GetEnumValueGlobalID(EnumKey);

                if (BuildedKey != null && Params.ContainsKey(BuildedKey.Value))
                {
                    return Params[BuildedKey.Value];
                }
                else return null;
            }
        }


        #endregion --------------------- KEY indexator -------------------


        #region -------------------- PROPERTIES ------------------------


        /// <summary>
        /// Configuration Parameters
        /// </summary>
        public Dictionary<long, EnumParameter> Params
        {
            get;
            protected set;
        }


        /// <summary>
        /// Configuration Parameters
        /// </summary>
        public Dictionary<Int32, Type> ConfigEnums
        {
            get;
            protected set;
        }


        /// <summary>
        /// Assemblies that was Scanned for Configuration Enum types
        /// </summary>
        public List<String> ParsedAssemblies
        {
            get;
            private set;
        }





        #endregion -------------------- PROPERTIES ------------------------


        #region  ------------------------  METHODS ---------------------------




        #region ------------------------------ IS CONFIG ENUM ------------------------------------


        /// <summary>
        /// If this enum Type is marked up as Configuration enum.
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static bool IsConfigEnum(Type enumType)
        {
            return (enumType.IsEnum && enumType.GetCustomAttribute<ConfigEnumAttribute>() != null);
        }


        /// <summary>
        /// If this [enumType] is marked up as Configuration enum.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool IsConfigEnum<T>()
        {
            return IsConfigEnum(typeof(T));
        }


        /// <summary>
        ///  If this  enum Type of [enumValue] is marked up as Configuration enum.
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static bool IsConfigEnum(Enum enumValue)
        {
            return IsConfigEnum(enumValue.GetType());
        }


        #endregion ------------------------------ IS CONFIG ENUM ------------------------------------



        #region  ------------------------------- EnumParameter Key  -----------------------------------


        const String EnumNameKnownConst = "EnumName";
        const String EnumNamespaceKnownConst = "EnumNamespace";
        const String EnumItemNameKnownConst = "EnumItemName";
        const String EnumParameterMaskKey = "{EnumNamespace}{EnumName}{EnumItemName}";



        /// <summary>
        /// {ItemName} - where ItemName - is EnumItem - constant word that will be replaced on Building Param Unique Key(String) 
        /// </summary>
        public Dictionary<string, Func<Type, FieldInfo, String>> ParameterKeyParts
        {   get;  private set;  }



        /// <summary>
        /// Init  check known replacement Consts by default 
        /// </summary>
        /// <returns></returns>
        private Dictionary<String, Func<Type, FieldInfo, String>> InitParameterKeyParts()
        {

            Func<Type, FieldInfo, String> EnumNameReplaceFunc = (t, f) =>
            {
                return t.Name; // EnumName   
            };

            Func<Type, FieldInfo, String> EnumItemNameReplaceFunc = (t, f) =>
            {
                return f.Name; // EnumItemName   
            };

            Func<Type, FieldInfo, String> EnumNamespaceReplaceFunc = (t, f) =>
            {
                return t.Namespace; // EnumNamespace   
            };

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumNameKnownConst, EnumNameReplaceFunc);

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumItemNameKnownConst, EnumItemNameReplaceFunc);// 

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumNamespaceKnownConst, EnumNamespaceReplaceFunc);// 

            return ParameterKeyParts;
        }





        /// <summary>
        /// Collect end of Enum and EnumItem conversions
        /// </summary>
        /// <param name="EnumType"></param>
        /// <param name="field"></param>
        /// <returns></returns> 
        private static Dictionary<String, String> CollectEnumValueKeyParts(FieldInfo field)
        {
            Dictionary<String, String> replacements = new Dictionary<String, String>();

            foreach (var item in Current.ParameterKeyParts)
            {
                replacements.Add(item.Key, item.Value(field.DeclaringType, field));
            }

            return replacements;
        }


        /// <summary>
        /// Building EnumParameter  GlobalKey
        /// </summary>
        /// <param name="ConfigEnumType"></param>
        /// <param name="enumField"></param>
        /// <returns></returns>
        private static String BuildEnumValueGlobalKey(FieldInfo EnumField)
        {
            string FinalGlobalKey = EnumParameterMaskKey.ReplaceManyRe(CollectEnumValueKeyParts(EnumField));

            return FinalGlobalKey;
        }


        /// <summary>
        /// Getting Enum EnumValue GlobalID - to get target EnumParameter Content 
        /// </summary>
        /// <param name="EnumField"></param>
        /// <returns></returns>
        internal static Int32 GetEnumValueGlobalID(FieldInfo EnumField)
        {
            ConfigEnumAttribute configEnumInfo = EnumField.DeclaringType.GetCustomAttribute<ConfigEnumAttribute>();//.FirstOrDefault();

            Validator.ATInvalidOperationDbg(configEnumInfo.IsNull(), nameof(ECEngine), nameof(GetEnumValueGlobalID),
                "This enum type is not Configuration Enum and cannot be use to GetGlobalID of it's value. Mark your enum type as ConfigEnum and set parameters for each field. ");
            

            String resultMaskString = BuildEnumValueGlobalKey(EnumField);

            //If we could Built Param Key
            return resultMaskString.GetHashCode();
        }

        /// <summary>
        /// Getting Enum EnumValue GlobalID - to get target EnumParameter Content 
        /// </summary>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        internal static Int32 GetEnumValueGlobalID(Enum EnumValue)
        {
            return GetEnumValueGlobalID(EnumValue.GetFieldInfo());

        }

        #endregion ------------------------------- EnumParameter Key  ---------------------------------


        #region  ----------------------------------- BUILD EnumParameter -------------------------------------




        static void ValidateEnumParameterDeclaration(String EnumFieldName, EnumParamAttribute paramInfo)
        {
            Validator.ATInvalidOperationDbg(paramInfo.Kind == ParamKindEn.NotDefined, nameof(ECEngine), nameof(ValidateEnumParameterDeclaration)
                , "EnumParameter Declaration(EnumParameterAttribute) of {0}  has [NotDefined] impossible Kind value".Fmt(EnumFieldName));
            
            Validator.ATInvalidOperationDbg(paramInfo.ParamType.IsNull(), nameof(ECEngine), nameof(ValidateEnumParameterDeclaration)
               , " EnumParameter Declaration(EnumParameterAttribute)  of {0}    has impossible ParamType value ".Fmt(EnumFieldName));
            
        }




        /// <summary>
        /// Building configuration EnumParameter  from EnumParamAttribute
        /// </summary>
        /// <param name="targetField"></param>
        /// <returns></returns>
        private static EnumParameter BuildConfigParam(FieldInfo targetField) //Type ParentEnumConfig, String EnumValueKey
        {

            var paramInfo = targetField.GetFieldAttributes<EnumParamAttribute>().FirstOrDefault();
            Validator.ATNullReferenceArgDbg(paramInfo, nameof(ECEngine), nameof(BuildConfigParam)
                 , "Field {0}  doesn't contain parameter Declaration".Fmt(targetField.Name) );
             
            // Contract.Assert(paramInfo != null, String.Format("Field {0} doesn't contain parameter Declaration", Field.Name));

            //base InitContent
            paramInfo.InitContent();


            // Validate EnumParameter Info- EnumParamAttribute
            ValidateEnumParameterDeclaration(targetField.DeclaringType.Name + "." + targetField.Name, paramInfo);


            var newParameter = new EnumParameter()
            {
                //Typology
                Kind = paramInfo.Kind,
                ParamType = paramInfo.ParamType,

                //Parent Enum.Field  info
                ParentEnumType = targetField.DeclaringType,
                EnumValueKey = targetField.DeclaringType.Name + "." + targetField.Name,

                //identification                
                GlobalKey = BuildEnumValueGlobalKey(targetField),

                // load Content by lazy way
                IsLazyLoad = paramInfo.IsLazyLoad,
                LoadPath = paramInfo.LoadPath,

                //persisting in the store (Sterling) after first load of Enum
                IsPersistable = paramInfo.IsPersistable,

                ValidationLevel = paramInfo.ValidationLevel,

                //filter fields
                GroupFilter = paramInfo.GroupFilter,
                SubGroupFilter = paramInfo.SubGroupFilter,

                //localization
                LocalNameKey = paramInfo.LocalNameKey,
                LocalDescriptionKey = paramInfo.LocalDescriptionKey
            };
            //set Contents
            newParameter.UpdateContentOnly(paramInfo.Content);
            newParameter.UpdateTagOnly(paramInfo.Tag);



            return newParameter;
        }
          

        #endregion ----------------------------------- BUILD EnumParameter -------------------------------------


        #region  --------------------------- LOAD ASSEMBLY CONFIGS & PARAMS ------------------------------

        /// <summary>
        /// Loading Base Assembly(ECEngine class assembly) configs
        /// </summary>
        protected static void LoadAssemblyConfigs()
        {
#if CLIENT          

          //ECEngine.LoadConfigEnum<TaskProcessOptionsEn>();
          //ECEngine.LoadConfigEnum<EnvironmentParamsEn>();
            
          //Current assembly config  - ConfigManager state configuration
          
#elif SERVER
            LoadAssemblyConfigs(typeof(ECEngine).Assembly);
#endif

        }



#if SERVER

        /// <summary>
        /// Loading Base Assembly(ConfigManagerBase class assembly)  configs
        /// </summary>
        public static void LoadAssemblyConfigs(Assembly targetAssembly)
        {

            Validator.ATNullReferenceArgDbg(targetAssembly, nameof(ECEngine), nameof(LoadAssemblyConfigs)
              , "Assembly cannot be null On Loading  it's Configurations" );

           
            Type[] tps = targetAssembly.GetTypes();
            List<Type> enumsTypes = targetAssembly.GetTypes().ToList();//.Where((t) => t.IsEnum == true).ToList();

            // Get All Config Enums
            List<Type> configenums = targetAssembly.GetTypes()
                .Where((t) => t.IsEnum == true
                        && t.GetFirstTypeAttribute<ConfigEnumAttribute>().IsNotNull()
                        ).ToList();

            //BuildParams for each Config Enum
            foreach (var configItem in configenums)
            {
                LoadConfigEnum(configItem);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        public static void LoadAssemblyConfigs<TEnum>()
        {

            // Get All Config Enums
            List<Type> configenums = typeof(TEnum).Assembly.GetTypes()
                .Where((t) => t.IsEnum == true
                      && t.GetFirstTypeAttribute<ConfigEnumAttribute>().IsNotNull()
                       ).ToList();


            //BuildParams for each Config Enum
            foreach (var configItem in configenums)
            {
                LoadConfigEnum(configItem);
            }

        }


#endif  //---------------------- SERVER -----------------------------


        public static void LoadConfigEnum<TConfigEnum>()
        {
            LoadConfigEnum(typeof(TConfigEnum));
        }


        /// <summary>
        /// Loading Base Assembly(ConfigManagerBase class assembly) configs
        /// </summary>
        internal static void LoadConfigEnum(Type ConfigEnum)
        {
            //get all enum fields
            FieldInfo[] enumItems = ConfigEnum.GetFields();

            //Build Params  Keys  and  Values
            List<EnumParameter> configParams = new List<EnumParameter>();
            foreach (var item in enumItems)
            {
                //try validate and createnew Parameter
                var newParameter = BuildConfigParam(item);

                configParams.Add(newParameter);
            }

            try
            {
                // Add params to the Global Params Dictionary - 
                foreach (var paramtItem in configParams)
                {
                    Validator.ATInvalidOperationDbg(Current.Params.NotContainsKey(paramtItem.GlobalID), nameof(ECEngine), nameof(LoadConfigEnum)
                                , " Adding Param Error. Cannot Add existing  Unique Key for  [{0}]".Fmt(paramtItem.EnumValueKey));
                    
                    Current.Params.Add(paramtItem.GlobalID, paramtItem);
                }

                // Add Config to the Global Enums Dictionary -   
                Int32 enumHashCode = ConfigEnum.GetHashCode();
                Current.ConfigEnums.Add(enumHashCode, ConfigEnum);

                // Add To list Of Parsed Assemblies
                if (!Current.ParsedAssemblies.Contains(ConfigEnum.Assembly.FullName))
                    Current.ParsedAssemblies.Add(ConfigEnum.Assembly.FullName);

            }
            catch (System.Exception ex)
            {
                // Log about config loading Error 
            }
        }


        #endregion --------------------------- LOAD CONFIGS & PARAMS ------------------------------


        #region ----------------------------- CONTAINS PARAMS -----------------------------


        /// <summary>
        /// If ECEngine Already Contains/Parsed  ConfigurationEnum 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsEnum(Enum EnumValue)
        {

            { ECEngine.LoadConfigEnum(EnumValue.GetType()); }

            return (Current.ConfigEnums.Values.Where(cfgen => cfgen == EnumValue.GetType()).FirstOrDefault() != null) ? true : false;
        }


        /// <summary>
        ///  Is Config Params Contains Value for EnumKey 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsParam(Enum EnumValue)
        {
            Int32? UniqueKey = GetEnumValueGlobalID(EnumValue);

            if (UniqueKey == null) return false;//Enum but not not ConfigurationEnum

            return Current.Params.ContainsKey(UniqueKey.Value);
        }

        #endregion ----------------------------- CONTAINS PARAMS -----------------------------



        #region ---------------------------- GETTING PARAM INFO ------------------------------------

        /// <summary>
        /// Get Enum parameter Info.
        /// </summary>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        private static EnumParamAttribute GetParamInfo(Enum EnumValue)
        {
            Attribute paramInfoAttribute = Attribute.GetCustomAttribute(EnumValue.GetFieldInfo(), typeof(EnumParamAttribute));
            if (paramInfoAttribute == null) return null;

            return paramInfoAttribute as EnumParamAttribute;
        }

        static bool IsScalarParam(EnumParamAttribute paramDeclaration)
        {
            return (paramDeclaration.Kind == ParamKindEn.ValueType || paramDeclaration.Kind == ParamKindEn.String || paramDeclaration.Kind == ParamKindEn.Enum);
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //private static Type GetParamType(Enum EnumValue)
        //{
        //    ParamAttribute paramInfo = GetParamInfo(value);
        //    if (paramInfo == null) return null;

        //    Contract.Assume(paramInfo.FinalType != null, value.GetType() + " Parameter.FinalType must be declared before.");

        //    return paramInfo.FinalType;
        //}


        #endregion ---------------------------- GETTING PARAM INFO ------------------------------------


        #region  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------

        /// <summary>
        /// Get Parameter's scalar Value of T type. 
        /// EnumParameter.ParamType and (T) should be Equal.
        /// We simply get parameter from Memory and then cast EnumParameter.Content value to determined T form.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        internal static T GetScalarValue<T>(Enum EnumValue)
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload

            EnumParamAttribute paramInfo = GetParamInfo(EnumValue);

            Validator.ATInvalidOperationDbg(paramInfo.ParamType != typeof(T), nameof(ECEngine), nameof(GetScalarValue)
                                            , " {T} type  can't be different than declared ParamType");
            Validator.ATInvalidOperationDbg(IsScalarParam(paramInfo), nameof(ECEngine), nameof(GetScalarValue)
                                          , "  GetScalarParamValue can be done only for Scalar Params - i.e. for params declared as ValueType/String/Enum  Kind ");

            // Contract.Assert(paramInfo.ParamType != typeof(T), "GetScalarParamValue - (T) type  can't be different than declared ParamType ");
            // Contract.Assert(IsScalarParam(paramInfo), "GetScalarParamValue can be done only for Scalar Params - i.e. for params declared as Struct/String/Enum  Kind");

            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);

            // T or ParamType -Nullable  && Content - null -->  default(T)
            // T or ParamType - (Nullable || !Nullabe)  && Content - !null --> return (T)
            // T or ParamType -!Nullable && Content - null -> error - cannot be so. BaseInit for EnumParam exclude this case.
            if (Current.Params[GlobalID].Content != null) return (T)Current.Params[GlobalID].Content;
            //if (Current.Params[GlobalID].ParamType.IsNullableType() &&  Current.Params[GlobalID].Content == null  ) return default(T);
            return default(T);
        }



        ///// <summary>
        ///// Get Parameter's  Value of object type.
        ///// </summary>
        ///// <param name="UniqueKey"></param>
        ///// <returns></returns>
        //private static object GetValue(Int32 GlobalID)
        //{
        //    return Current.Params[GlobalID].Content;          
        //}


        /// <summary>
        ///  Get Parameter's  Value of object type.
        /// </summary>
        /// <param name="paramKey"></param>
        /// <returns></returns>
        internal static object GetValue(Enum EnumValue)
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload

            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);
            return Current.Params[GlobalID].Content;
        }


        /// <summary>
        /// Get Parameter's  Value of object type and set it to ParamValue ref variable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="EnumValue"></param>
        /// <param name="ParamValue"></param>     
        internal static void TryGetValue<T>(Enum EnumValue, ref T ParamValue)//, bool IsFullEnumItem = false
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload

            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);

            if (Current.Params[GlobalID].Content != null) ParamValue = (T)Current.Params[GlobalID].Content;

            ParamValue = default(T);
        }



        /// <summary>
        /// Get Parameter's  Value of object type and set it to ParamValue ref variable
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="ParamValue"></param>
        /// <param name="IsFullEnumItem"></param>
        internal static void TryGetValue(Enum EnumValue, ref object ParamValue) //, bool IsFullEnumItem = false
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload

            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);

            if (Current.Params[GlobalID].Content != null) ParamValue = Current.Params[GlobalID].Content;

            ParamValue = Current.Params[GlobalID].Content.ConvertToValue(Current.Params[GlobalID].ParamType, null);

        }



        #endregion  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------


        #region  ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------


        /// <summary>
        /// Trying to update Parameter Value.         
        /// If new value will succesfully Validated current Content Value will be chaged to NewValue.        
        /// </summary>
        /// <param name="ConfigEnum"></param>
        /// <param name="itemValue"></param>
        /// <param name="enumItemNewValue"></param>
        /// <param name="IsFullEnumItem"></param>
        public static void TryUpdateValue(FieldInfo itemValue, object enumItemNewValue)
        {
            Int32 GlobalID = GetEnumValueGlobalID(itemValue);

            Current.Params[GlobalID].ValidateNewContent(enumItemNewValue);

            Current.Params[GlobalID].UpdateContentOnly(enumItemNewValue);

        }



        /// <summary>
        /// Trying to update Parameter Value.         
        /// If new value will succesfully Validated current Content Value will be chaged to NewValue.        
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="enumItemNewValue"></param>        
        public static void TryUpdateValue(Enum paramKey, object enumItemNewValue)
        {
            TryUpdateValue(paramKey.GetFieldInfo(), enumItemNewValue);

        }

        #endregion ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------




        #region  ------------------------------- Multiple Enum and Params Updating ----------------------------------

        /// <summary>
        /// Updating Parameter Values in such way:  ECEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum) config enum, and nothing more.  
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="updateSource"></param>
        internal static void UpdateConfigValues<TEnum>(IDictionary<string, string> updateSource) //override
        {
            Validator.ATNullReferenceArgDbg(updateSource, nameof(ECEngine), nameof(UpdateConfigValues), nameof(updateSource));
                  
            //need to add auto preload OPTION -!!!!

            Type ConfigEnum = typeof(TEnum);
            // ConfigEnum with this type loaded
            if (!Current.ConfigEnums.ContainsKey(ConfigEnum.GetHashCode()))
            {
                return;
            }

            foreach (var item in ConfigEnum.GetFields())
            {
                if (updateSource.ContainsKey(item.Name))
                {
                    TryUpdateValue(item, updateSource[item.Name]);
                }
            }

        }

        /// <summary>
        /// Updating Parameter Values in such way:  ECEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1>(UpdateSource);
            UpdateConfigValues<TEnum2>(UpdateSource);
        }


        /// <summary>
        /// Updating Parameter Values in such way:  ECEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3)  config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2>(UpdateSource);
            UpdateConfigValues<TEnum3>(UpdateSource);
        }


        /// <summary>
        /// Updating Parameter Values in such way:  ECEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2, TEnum3>(UpdateSource);
            UpdateConfigValues<TEnum4>(UpdateSource);
        }


        /// <summary>
        ///  Updating Parameter Values in such way:  ECEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) & (TEnum5) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <typeparam name="TEnum5"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(UpdateSource);
            UpdateConfigValues<TEnum5>(UpdateSource);
        }


        #endregion ------------------------------- Multiple Enum and Params Updating ----------------------------------


        #region  ---------------------------------- Init AppConfiguration ------------------------------------

#if CLIENT


        /// <summary>
        /// Initializing ECEngine  - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration(object sender, StartupEventArgs e)
        {
            //1 load Current assembly enum Configurations
            ECEngine.LoadAssemblyConfigs();                

        }


        /// <summary>
        /// Initializing ECEngine - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum>( UpdateSource = e.InitParams ). 
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum>(object sender, StartupEventArgs e )
        {
            //1 load Current assembly enum Configurations
            ECEngine.LoadAssemblyConfigs();        

            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            ECEngine.LoadConfigEnum<TEnum>();// AssemblyConfigs<TEnum>();

            //2 Updating Loaded Params
            //UpdateConfigValues<TEnum>(e.InitParams); //CommunicationParamsEn  from Server Params Source          
            
            UpdateClientEnvironment();            
            
            //if (ECEngine.Current.IsInitialized Initialized != null)
            //{ Initialized(null, new EventArgs()); }
   
        }



        private static void  UpdateClientEnvironment()
        {
            //if (ECEngine.ConfigEnums.ContainsValue(typeof(EnvironmentParamsEn)) )
            //{
            //    ECEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri, HtmlPage.Document.DocumentUri.ToString());
            //    ECEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri_OriginalString, HtmlPage.Document.DocumentUri.OriginalString);
            //}
        }



        /// <summary>
        /// Initializing ECEngine - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2>( UpdateSource = e.InitParams ). 
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2>(object sender, StartupEventArgs e)        
        {  
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1>(sender, e);            
            ECEngine.LoadConfigEnum<TEnum2>();

            // load the assembly into our app domain
            
            //2 Updating Loaded Params
           // UpdateConfigValues<TEnum1,TEnum2>(e.InitParams); //CommunicationParamsEn            
        }


        /// <summary>
        /// Initializing ECEngine - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3>(sender, e);            
            ECEngine.LoadConfigEnum<TEnum3>();

            //UpdateConfigValues<TEnum1, TEnum2, TEnum3>(e.InitParams); //CommunicationParamsEn            
        }


        /// <summary>
        /// Initializing ECEngine - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3,TEnum4>(sender, e);
            ECEngine.LoadConfigEnum<TEnum4>();

           // UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(e.InitParams); //CommunicationParamsEn            
        }

        /// <summary>
        /// Initializing ECEngine - Loading all Configuration Enums from the  ECEngine type assembly into ECEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <typeparam name="TEnum5"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(sender, e);
            ECEngine.LoadConfigEnum<TEnum4>();


            //UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(e.InitParams); //CommunicationParamsEn            
        }

#elif SERVER


#endif



        #endregion ---------------------------------- Init AppConfiguration ------------------------------------


        #region  ------------------------ DELEGATE PARAMS ---------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="paramKey"></param>
        /// <param name="paramsValues"></param>
        /// <returns></returns>
        internal static TResult InvokeStaticDelegateParam<TResult>(Enum paramKey, object[] paramsValues)
        {
            //[Param(typeof(Func< Func<Task>, HttpLoader, bool, Task>), ParamKindEn.Delegate, typeof(Core.Communication.CommunicationService), "HttpQueryWrapper")] 
            //Key - HttpPostQueryWrapper,
            //FinalType -  Func< Func<Task>, HttpLoader, bool, Task>
            //value - typeof(Core.Communication.CommunicationService)
            //supportValue - "HttpQueryWrapper"

            EnumParamAttribute paramInfo = GetParamInfo(paramKey);

            Validator.ATInvalidOperationDbg(paramInfo.IsNull(), nameof(ECEngine), nameof(InvokeStaticDelegateParam)
                , "Params with such Key does not exist in Params");
            Validator.ATInvalidOperationDbg(paramInfo.Kind != ParamKindEn.Delegate, nameof(ECEngine), nameof(InvokeStaticDelegateParam)
                , "Not Delegate Params can't be Called");
            Validator.ATInvalidOperationDbg( (paramInfo.Content as Type).IsNull() , nameof(ECEngine), nameof(InvokeStaticDelegateParam)
                , " paramInfo.Value must be Type contract");
              

            MethodInfo methodToCall = (paramInfo.Content as Type).GetMethod(paramInfo.Tag.ToString(), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);

            Validator.ATInvalidOperationDbg(methodToCall.IsNull(), nameof(ECEngine), nameof(InvokeStaticDelegateParam)
                , "Param with Key{0} was not set correctly to get method:{1} of type:{2} delegate".Fmt( paramKey.ToString(), paramInfo.Tag.S(), (paramInfo.Content as Type).Name ) );
           
            return (TResult)methodToCall.Invoke(null, paramsValues);

        }


        #endregion ------------------------ DELEGATE PARAMS ---------------------------------





        #endregion ------------------------  METHODS ---------------------------


    }
}


#region ---------------------------------------- GARABAGE ---------------------------------------------


//#region ------------------NOTIFIABLE MODEL ITEM:  IsInitialized------------------------------

//// ItemName - IsInitialized  -  Ex: Client 
//// Contract - bool  -  Ex: V_S8_Client_Vo

//bool _IsInitialized = new bool();

///// <summary>
///// Инициализирован ли даннный экземпляр 
///// </summary>
//public bool IsInitialized
//{
//    get { return _IsInitialized; }
//    set
//    {
//        _IsInitialized = value; //Field changing
//        OnPropertyChanged(() => IsInitialized);
//    }
//}


//#endregion ------------------NOTIFIABLE MODEL ITEM: IsInitialised------------------------------

//#region ------------------------------INotifyPropertyChanged--------------------------------

//PropertyObserver<ECEngine> _observer;

//public event PropertyChangedEventHandler PropertyChanged;

//protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
//{
//    var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;

//    Validator.AssertTrueDbg<ArgumentNullException>(propertyInfo == null, "The lambda expression 'property' should point to a valid Property");


//    PropertyChangedEventHandler handler = this.PropertyChanged;
//    if (PropertyChanged != null)
//        PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
//}


//#endregion ------------------------------INotifyPropertyChanged--------------------------------






//#region ----------------------------------  PERSISTANCE (STERLING) -----------------------------------

///// <summary>
///// Sterling Engine to store some parameters in the Windows Application Isolated Storage
///// </summary>
//private SterlingEngine PersistEngine;

///// <summary>
///// Parameters Database to save/load configuration parameters
///// </summary>
//public ISterlingDatabaseInstance PersistDatabase { get; private set; }

///// <summary>
///// Default Logger for SterlingEngine
///// </summary>
//private SterlingDefaultLogger Logger;

//#endregion ---------------------------------- PERSISTANCE (STERLING) -----------------------------------


#endregion ---------------------------------------- GARABAGE ---------------------------------------------
