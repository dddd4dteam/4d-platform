﻿using System;
using System.IO;
using System.Windows;
using System.Linq;
using System.Xml.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;

using Core.Collections;
using Core.Extensions;

#if CLIENT
using System.Windows.Browser;
using Core.Net;
using Core.Threading;

#endif 

namespace DDDD.Core.Configuration.ECE
{

	/// <summary>
	/// 
	/// </summary>
	public  class ConfigurationEngine
	{

		#region  ----------------------------- CTOR --------------------------------- 

		static ConfigurationEngine()
		{
			Params = new Dictionary<long, ParamAttribute>();
			ConfigEnums = new Dictionary<Int32, Type>();
			ParsedAsemblies = new List<String>();
			CheckKnownConsts = new Dictionary<string, Func<Type , FieldInfo, String> >();
			ParamKeyPattern = @"\[([A-Za-z0-9]+)\]|\{([A-Za-z0-9]+)\}";
			
			InitCheckKnownConsts();

			//LoadAssemblyConfigs();
		}

#endregion ----------------------------- CTOR ---------------------------------
		

		const string ParamNotContains = "ParamNotContains";

		/// <summary>
		/// Event  On Initialized
		/// </summary>
		public static event EventHandler Initialized;


		public static String ParamKeyPattern
		{
			get;
			private set;
		}
		

		#region --------------------- KEY indexator -------------------
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public  ParamAttribute this[Int32 key]
		{
			get
			{
				if ( Params.ContainsKey(key))
					 return Params[key];
				else
					return null;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public  ParamAttribute this[Enum EnumKey]
		{
			get
			{
				Int32? BuildedKey = BuildParamUniqueId(EnumKey.GetType(), EnumKey.GetFieldInfo());

				if (BuildedKey != null && Params.ContainsKey(BuildedKey.Value) ) 
				{
					return Params[BuildedKey.Value];
				}                
				else return null;
			}
		}


		#endregion --------------------- KEY indexator -------------------
		

		#region -------------------- PROPERTIES ------------------------
						

		/// <summary>
		/// Configuration Parameters
		/// </summary>
		public static Dictionary<Int64, ParamAttribute> Params //ConfigParamsEn
		{
			get;
			protected set;
		}


		/// <summary>
		/// Configuration Parameters
		/// </summary>
		public static Dictionary<Int32, Type> ConfigEnums //ConfigParamsEn
		{
			get;
			protected set;
		}


		/// <summary>
		/// Assemblies that was Scanned for Configuration Enum types
		/// </summary>
		public static List<String> ParsedAsemblies
		{
			get;
			private set;
		}


		
		/// <summary>
		/// {ItemName} - where ItemName - is EnumItem - constant word that will be replaced on Building Param Unique Key(String) 
		/// </summary>
		public static Dictionary<string, Func<Type, FieldInfo, String>> CheckKnownConsts
		{
			get;
			private set;
		}
		
		#endregion -------------------- PROPERTIES ------------------------
				 

		#region  ------------------------  METHODS ---------------------------

		/// <summary>
		/// not exist in Params 
		/// </summary>
		/// <param name="initParams"></param>
		protected virtual void Initialize()
		{
			var doc = XDocument.Load("ServiceReferences.ClientConfig");

			var dict = (from settingNode in
							doc.Descendants("appSettings").Descendants("add")
						select new
						{
							Key = settingNode.Attribute("key").Value,
							Value = settingNode.Attribute("value").Value
						}).ToDictionary(s => s.Key, s => s.Value);
								   
		}


		
		#region  ------------------------------- CheckKnownConstants -----------------------------------

		/// <summary>
		/// Collect end of Enum and EnumItem conversions
		/// </summary>
		/// <param name="EnumType"></param>
		/// <param name="WindowsService"></param>
		/// <returns></returns> 
		private static Dictionary<String,String> CollectCheckWords(Type EnumType, FieldInfo field)
		{
			Dictionary<String, String> replacements  = new Dictionary<String, String>();

			foreach (var item in ConfigurationEngine.CheckKnownConsts)
			{
				replacements.Add( item.Key , item.Value(EnumType,field) );
			}            

			return replacements;
		}
		

		/// <summary>
		/// Init  check konwn replcement Consts by default 
		/// </summary>
		/// <returns></returns>
		private static Dictionary<String, Func<Type, FieldInfo, String> > InitCheckKnownConsts()
		{
		
			Func<Type, FieldInfo, String> EnumNameReplaceFunc = (t,f) =>
				{
					return t.Name; // EnumName   
				};

			Func<Type, FieldInfo, String> EnumItemNameReplaceFunc = (t,f) =>
			{
				return f.Name; // EnumItemName   
			};


			AddCheckKnownConst(@"EnumName", EnumNameReplaceFunc);
			AddCheckKnownConst(@"EnumItemName", EnumItemNameReplaceFunc);

			return ConfigurationEngine.CheckKnownConsts;
		}


		/// <summary>
		/// Adding some Customer Check Known Const
		/// </summary>
		/// <param name="Marker"></param>
		/// <param name="gettingFunc"></param>
		public static void AddCheckKnownConst(String Marker, Func<Type, FieldInfo, String> GetOutputValueFunc)
		{
			ConfigurationEngine.CheckKnownConsts.Add(Marker, GetOutputValueFunc);
		}
		

		#endregion ------------------------------- CheckKnownConstants ---------------------------------
		

		#region  ----------------------------------- BUILD ConfigEnumType, enumField -------------------------------------
		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ConfigEnumType"></param>
		/// <param name="enumField"></param>
		/// <returns></returns>
		private static String BuildParamGlobalKey(Type ConfigEnumType, FieldInfo enumField)
		{
			ConfigEnumAttribute configEnumInfo = ConfigEnumType.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault();

			if (configEnumInfo == null)
			{ return null; } // because it's not configuration enumeration

			string FinalGlobalKey = configEnumInfo.ItemKeyMask.GetReplacedString(
															   CollectCheckWords(ConfigEnumType, enumField)
															   );

			return FinalGlobalKey;
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="WindowsService"></param>
		/// <param name="ParentEnumConfig"></param>
		/// <returns></returns>
		private static ParamAttribute BuildConfigParam(FieldInfo field, Type ParentEnumConfig,String EnumValueKey)
		{
			ConfigEnumAttribute configEnumInfo = ParentEnumConfig.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault();

			ParamAttribute paramitem = field.GetFieldAttribute<ParamAttribute>().FirstOrDefault();

			//Constant flags
			String FinalGlobalKey = BuildParamGlobalKey(ParentEnumConfig, field);

			Int32 paramHashCode = FinalGlobalKey.GetHashCode();

			paramitem.ResetValue(paramHashCode,
								 ParentEnumConfig,
								 FinalGlobalKey,
								 configEnumInfo.ItemKeyMask,
								 EnumValueKey,          // Enum.Value - simple key 
								 ConfigurationEngine.CheckKnownConsts.Keys.ToArray()); //GetCheckWords()

			return paramitem;
		}


	  
		/// <summary>
		/// 
		/// </summary>
		/// <param name="ConfigEnum"></param>
		/// <param name="enumItem"></param>
		/// <returns></returns>
		private static Int32? BuildParamUniqueId(Type ConfigEnum, FieldInfo enumItem)
		{
			String  resultMaskString = BuildParamGlobalKey(ConfigEnum, enumItem);
			if(resultMaskString == null) return null;

			//If we could Built Param Key
			return  resultMaskString.GetHashCode();
		}
		

		#endregion ----------------------------------- BUILD ConfigEnumType, enumField -------------------------------------
	  

		#region  --------------------------- LOAD ASSEMBLY CONFIGS & PARAMS ------------------------------
		
		/// <summary>
		/// Loading Base Assembly(ConfigurationEngine class assembly) configs
		/// </summary>
		protected static void LoadAssemblyConfigs()
		{
#if CLIENT          

		  ConfigurationEngine.LoadConfigEnum<TaskProcessOptionsEn>();
		  ConfigurationEngine.LoadConfigEnum<EnvironmentParamsEn>();
			
		  //Current assembly config  - ConfigManager state configuration
		  
#elif SERVER
			LoadAssemblyConfigs(typeof(ConfigurationEngine).Assembly);            
#endif

		}



#if SERVER

 /// <summary>
/// Loading Base Assembly(ConfigManagerBase class assembly)  configs
/// </summary>
public static void LoadAssemblyConfigs(Assembly asmbly)
{
	Contract.Requires(asmbly != null, " Assembly cannot be null On Loading  it's Configurations ");

	Type[] tps = asmbly.GetTypes();
	List<Type> enumsTypes = asmbly. GetTypes().ToList();//.Where((t) => t.IsEnum == true).ToList();

	// Get All Config Enums
	List<Type> configenums = asmbly.GetTypes()
		.Where((t) => t.IsEnum == true
				&& t.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault() != null
				).ToList();

	//BuildParams for each Config Enum
	foreach (var configItem in configenums)
	{
		LoadConfigEnum(configItem);
	}
}


		 /// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		public static void LoadAssemblyConfigs<TEnum>()
		{
		 
			// Get All Config Enums
			List<Type> configenums = typeof(TEnum).Assembly.GetTypes()
				.Where((t) => t.IsEnum == true
					  && t.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault() != null
					   ).ToList();


			//BuildParams for each Config Enum
			foreach (var configItem in configenums)
			{
				LoadConfigEnum(configItem);
			}

		}


#endif  //---------------------- SERVER -----------------------------



		public static void LoadConfigEnum<TConfigEnum>()
		{
			LoadConfigEnum(typeof(TConfigEnum));
		}


		/// <summary>
		/// Loading Base Assembly(ConfigManagerBase class assembly) configs
		/// </summary>
		private static void LoadConfigEnum(Type ConfigEnum)
		{
			//get all enum fields
			FieldInfo[] enumItems = ConfigEnum.GetFields();

			//Build Params  Keys  and  Values
			List<ParamAttribute> configParams = new List<ParamAttribute>();
			foreach (var item in enumItems)
			{
				String EnumValueKey = ConfigEnum.Name+"."+ item.Name;

				if(item.GetFieldAttribute<ParamAttribute>().FirstOrDefault()!= null)
				configParams.Add(BuildConfigParam(item, ConfigEnum , EnumValueKey));
			}

			// Validate ConfigEnum
			// Validate EnumParams
			try            
			{
				// Add params to the Global Params Dictionary - 
				foreach (var paramtItem in configParams)
				{
					Contract.Assume(Params.ContainsKey(paramtItem.UniqueHashKey) == false, " Adding Param Error. Cannot Add existing  Unique Key for " + paramtItem.EnumValueKey);  //ConfigEnum.Name
					Params.Add(paramtItem.UniqueHashKey, paramtItem);
				}
				
				// Add Config to the Global Enums Dictionary -   
				Int32 enumHashCode = ConfigEnum.GetHashCode();
				ConfigEnums.Add(enumHashCode, ConfigEnum);

				// Add To list Of Parsed Assemblies
				if (!ParsedAsemblies.Contains(ConfigEnum.Assembly.FullName))
					ParsedAsemblies.Add(ConfigEnum.Assembly.FullName);

			}
			catch (System.Exception ex)
			{
				// Log about config loading Error 
			}
		}

				
		#endregion --------------------------- LOAD CONFIGS & PARAMS ------------------------------
		

		#region ----------------------------- CONTAINS PARAMS -----------------------------


		/// <summary>
		///  Is Config Params Contains Value for EnumKey 
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static bool ContainsParam(Enum value)
		{
			Int32? UniqueKey = BuildParamUniqueId(value.GetType(), value.GetFieldInfo());

			if (UniqueKey == null) return false;//Enum but not not ConfigurationEnum

			return Params.ContainsKey(UniqueKey.Value);
		}

		#endregion ----------------------------- CONTAINS PARAMS -----------------------------


		#region  ----------------------------------- IS PARAMS CONVERTED ------------------------------------------

		/// <summary>
		/// 
		/// </summary>
		/// <param name="UniqueKey"></param>
		/// <returns></returns>
		public static bool IsParamConverted(Int32 UniqueKey)
		{
			if (Params.ContainsKey(UniqueKey) && Params[UniqueKey].ConvertedType == Params[UniqueKey].FinalType)
				return true;
			return false;

		}

		public static bool IsParamConverted<T>(Int32 UniqueKey)
		{
			if (Params.ContainsKey(UniqueKey) && Params[UniqueKey].ConvertedType == typeof(T))
				return true;
			return false;

		}


		#endregion ----------------------------------- IS PARAMS CONVERTED ------------------------------------------
		

		#region ---------------------------- GETTING PARAM INFO ------------------------------------

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static ParamAttribute GetParamInfo(Enum value)
		{
			Attribute paramInfoAttribute = Attribute.GetCustomAttribute(value.GetFieldInfo(), typeof(ParamAttribute));
			if (paramInfoAttribute == null) return null;

			return paramInfoAttribute as ParamAttribute;
		}



		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		public static Type GetParamType(Enum value)
		{
			ParamAttribute paramInfo = GetParamInfo(value);
			if (paramInfo == null) return null;

			Contract.Assume(paramInfo.FinalType != null,   value.GetType() + " Parameter.FinalType must be declared before.");            

			return paramInfo.FinalType;
		}


		#endregion ---------------------------- GETTING PARAM INFO ------------------------------------

		
		#region  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------
	   
		/// <summary>
		/// Get Parameter's scalar Value of T type. 
		/// If T is reference type(not string) there will be throw InvalidOperationException 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="paramKey"></param>
		/// <returns></returns>
		public static T GetScalarValue<T>(Enum paramKey)
		{
			ParamAttribute paramInfo = GetParamInfo(paramKey);
			Contract.Assume(paramInfo.Kind == ParamKindEn.Struct || paramInfo.Kind == ParamKindEn.String  ||  paramInfo.Kind == ParamKindEn.Enum, "GetScalarParamValue can be done only for  Params on Struct Kind");


			if (paramInfo.Kind == ParamKindEn.Struct || paramInfo.Kind == ParamKindEn.String || paramInfo.Kind == ParamKindEn.Enum)
			{
				Int32? UniqueKey = BuildParamUniqueId(paramKey.GetType(), paramKey.GetFieldInfo());

				if (UniqueKey == null) return default(T);//Enum but not not ConfigurationEnum

				else if (IsParamConverted<T>(UniqueKey.Value))
				{
					//Updating Param GettingTime 
					Params[UniqueKey.Value].UpdateGetTime();

					return (T)Params[UniqueKey.Value].Value;
				}
				else if (Params.ContainsKey(UniqueKey.Value))
				{
					//Updating Param GettingTime 
					Params[UniqueKey.Value].UpdateGetTime();

					return (T)Params[UniqueKey.Value].Value.ConvertToValue<T>();
				}            
			}
			

			 return default(T);
			
		}
		

		/// <summary>
		/// Get Parameter's scalar Value of T type. 
		/// If T is reference type(not string) there will be throw InvalidOperationException
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="paramEnumValueKey"></param>
		/// <returns></returns>
		public static T GetScalarValue<T>(String paramEnumValueKey)
		{
			
			//Contract.Assume(paramInfo.Kind == ParamKindEn.Struct || paramInfo.Kind == ParamKindEn.String || paramInfo.Kind == ParamKindEn.Enum, "GetScalarParamValue can be done only for  Params on Struct Kind");

			ParamAttribute paramSimpleKey = Params.Values.Where(pr => pr.EnumValueKey == paramEnumValueKey).FirstOrDefault();

			if (paramSimpleKey == null) return default(T);


			if (paramSimpleKey.Kind == ParamKindEn.Struct || paramSimpleKey.Kind == ParamKindEn.String || paramSimpleKey.Kind == ParamKindEn.Enum)
			{
				Int32? UniqueKey = paramSimpleKey.UniqueHashKey; //BuildParamUniqueId(paramSimpleKey.GetType(), paramSimpleKey.GetFieldInfo() );

				if (UniqueKey == null) return default(T);//Enum but not not ConfigurationEnum

				else if (IsParamConverted<T>(UniqueKey.Value))
				{
					//Updating Param GettingTime 
					Params[UniqueKey.Value].UpdateGetTime();

					return (T)Params[UniqueKey.Value].Value;
				}
				else if (Params.ContainsKey(UniqueKey.Value))
				{
					//Updating Param GettingTime 
					Params[UniqueKey.Value].UpdateGetTime();

					return (T)Params[UniqueKey.Value].Value.ConvertToValue<T>();
				}
			}


			return default(T);

		}


		/// <summary>
		/// Get Parameter's  Value of object type.
		/// </summary>
		/// <param name="UniqueKey"></param>
		/// <returns></returns>
		public static object GetValue(Int32 UniqueKey)
		{
			if (Params.ContainsKey(UniqueKey))
			{
				//Updating Param GettingTime 
				Params[UniqueKey].UpdateGetTime();

				return Params[UniqueKey].Value;
			}

			return null;// ParamNotContains;
		}


		/// <summary>
		///  Get Parameter's  Value of object type.
		/// </summary>
		/// <param name="paramKey"></param>
		/// <returns></returns>
		public static object GetValue(Enum paramKey)
		{
			Int32? UniqueKey = BuildParamUniqueId(paramKey.GetType(), paramKey.GetFieldInfo());
			
			if (UniqueKey == null) return null;
					 
			return GetValue(UniqueKey.Value);
		}
		

		/// <summary>
		///  Get Parameter's  Value of object type and set it to ParamValue ref variable.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="paramKey"></param>
		/// <param name="ParamValue"></param>
		/// <param name="IsFullEnumItem"></param>
		public static void TryGetValue<T>(Enum paramKey, ref T ParamValue)//, bool IsFullEnumItem = false
		{
			Int32? UniqueKey = BuildParamUniqueId(paramKey.GetType(), paramKey.GetFieldInfo());

			if (UniqueKey == null) return;//Enum but not not ConfigurationEnum
			
			if ( IsParamConverted<T>(UniqueKey.Value) )
			{
				ParamValue = (T)Params[UniqueKey.Value].Value;
				return;
			}
			else if (Params.ContainsKey(UniqueKey.Value))
			{
				ParamValue = (T)Params[UniqueKey.Value].Value.ConvertToValue<T>();
				return;
			}

		}



		/// <summary>
		/// Get Parameter's  Value of object type and set it to ParamValue ref variable
		/// </summary>
		/// <param name="paramKey"></param>
		/// <param name="ParamValue"></param>
		/// <param name="IsFullEnumItem"></param>
		public static void TryGetValue(Enum paramKey, ref object ParamValue) //, bool IsFullEnumItem = false
		{
			Int32? UniqueKey = BuildParamUniqueId(paramKey.GetType(), paramKey.GetFieldInfo());

			if (UniqueKey == null) return;//Enum but not not ConfigurationEnum

			if ( IsParamConverted(UniqueKey.Value) )
			{
				ParamValue = Params[UniqueKey.Value].Value; 
				return;
			}
			else if (Params.ContainsKey(UniqueKey.Value))
			{
				ParamValue = Params[UniqueKey.Value].Value.ConvertToValue(Params[UniqueKey.Value].FinalType, null);
				return;
			}
			throw new InvalidOperationException("Cоnfiguration doesn't have Parameter : with type");

		}



		#endregion  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------
		

		#region  ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------


		/// <summary>
		/// Trying to update Parameter Value- if there is no such key in ConfigurationEngine.Params so we won't be able to update anything. 
		/// For Parameter wich Value we are going to Update we need [ParamEnumType.Param_Field] to get ConfigKey(long). 
		/// Then if the Key exist we will change current Value into  NewValue.
		/// This Method is Reflection Overloading: You can use the same method with ( Enum paramKey...) -param instead of this (Type ConfigEnum, FieldInfo itemValue.....).
		/// </summary>
		/// <param name="ConfigEnum"></param>
		/// <param name="itemValue"></param>
		/// <param name="enumItemNewValue"></param>
		/// <param name="IsFullEnumItem"></param>
		public static void TryUpdateValue(Type ConfigEnum, FieldInfo itemValue, object enumItemNewValue, bool IsFullEnumItem = false)
		{            
			Int32? UniqueKey = BuildParamUniqueId(ConfigEnum, itemValue);

			if (UniqueKey == null) return;            

			
			object CurrentParamValue = GetValue(UniqueKey.Value);
			
			if (CurrentParamValue == ParamNotContains)
			{ return; }

			if (enumItemNewValue.GetType() == Params[UniqueKey.Value].FinalType) // если типы у
			{
				Params[UniqueKey.Value].UpdateValueOnly(enumItemNewValue);
				
				//Lifetime Updating
				Params[UniqueKey.Value].UpdateCreateTime();
			}
			else
			{
				//  throw castException -  param finaltype for value  and newValue type is mismatched
			}

		}
		


		/// <summary>
		/// Trying to update Parameter Value- if there is no such key in ConfigurationEngine.Params so we won't be able to update anything.
		/// For Parameter wich Value we are going to Update we need [ParamEnumType.Param_Field] to get ConfigKey(long). 
		/// Then if the Key exist we will change current Value into  NewValue.        
		/// </summary>
		/// <param name="paramKey"></param>
		/// <param name="enumItemNewValue"></param>
		/// <param name="IsFullEnumItem"></param>
		public static void TryUpdateValue( Enum paramKey,  object enumItemNewValue, bool IsFullEnumItem = false)
		{
			TryUpdateValue(paramKey.GetType(), paramKey.GetFieldInfo(), enumItemNewValue, IsFullEnumItem);

		}
		


		/// <summary>
		/// Updating Parameter Values in such way:  ConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
		/// We using TryUpdateValue method to do this update from Source.
		/// Here We trying to update only fields for (TEnum) config enum, and nothing more.  
		/// </summary>
		/// <param name="initParams"></param>
		/// <param name="ConfigEnum"></param>
		public static void UpdateConfigValues<TEnum>(IDictionary<string, string> UpdateSource) //override
		{
			Contract.Requires(UpdateSource != null, "ConfigManager.LoadInitParams() initParams cannot be null");

			Type ConfigEnum = typeof(TEnum);
			// ConfigEnum with this type loaded
			if (!ConfigEnums.ContainsKey(ConfigEnum.GetHashCode()))
			{
				return;
			}

			foreach (var item in ConfigEnum.GetFields())
			{
				if (UpdateSource.ContainsKey(item.Name))
				{
					TryUpdateValue(ConfigEnum, item, UpdateSource[item.Name]);

					//"HttpUploadHandler.ashx"
				}
			}

		}

		/// <summary>
		/// Updating Parameter Values in such way:  ConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
		/// We using TryUpdateValue method to do this update from Source.
		/// Here We trying to update only fields for (TEnum1) &  (TEnum2) config enum, and nothing more.
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <param name="UpdateSource"></param>
		public static void UpdateConfigValues<TEnum1, TEnum2>(IDictionary<string, string> UpdateSource) //override
		{
			UpdateConfigValues<TEnum1>(UpdateSource);
			UpdateConfigValues<TEnum2>(UpdateSource);
		}


		/// <summary>
		/// Updating Parameter Values in such way:  ConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
		/// We using TryUpdateValue method to do this update from Source.
		/// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3)  config enum, and nothing more.
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <param name="UpdateSource"></param>
		public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3>(IDictionary<string, string> UpdateSource) //override
		{
			UpdateConfigValues<TEnum1, TEnum2>(UpdateSource);
			UpdateConfigValues<TEnum3>(UpdateSource);
		}


		/// <summary>
		/// Updating Parameter Values in such way:  ConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
		/// We using TryUpdateValue method to do this update from Source.
		/// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) config enum, and nothing more.
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <typeparam name="TEnum4"></typeparam>
		/// <param name="UpdateSource"></param>
		public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(IDictionary<string, string> UpdateSource) //override
		{
			UpdateConfigValues<TEnum1, TEnum2, TEnum3>(UpdateSource);
			UpdateConfigValues<TEnum4>(UpdateSource);
		}


		/// <summary>
		///  Updating Parameter Values in such way:  ConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
		/// We using TryUpdateValue method to do this update from Source.
		/// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) & (TEnum5) config enum, and nothing more.
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <typeparam name="TEnum4"></typeparam>
		/// <typeparam name="TEnum5"></typeparam>
		/// <param name="UpdateSource"></param>
		public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(IDictionary<string, string> UpdateSource) //override
		{
			UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(UpdateSource);
			UpdateConfigValues<TEnum5>(UpdateSource);
		}




		


#endregion ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------
	 

		#region  ---------------------------------- Init AppConfiguration ------------------------------------

#if CLIENT 


		/// <summary>
		/// Initializing ConfigurationEngine  - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration(object sender, StartupEventArgs e)
		{
			//1 load Current assembly enum Configurations
			ConfigurationEngine.LoadAssemblyConfigs();                

		}


		/// <summary>
		/// Initializing ConfigurationEngine - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params. 
		/// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum>( UpdateSource = e.InitParams ). 
		/// </summary>
		/// <typeparam name="TEnum"></typeparam>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration<TEnum>(object sender, StartupEventArgs e )
		{
			//1 load Current assembly enum Configurations
			ConfigurationEngine.LoadAssemblyConfigs();        

			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			ConfigurationEngine.LoadConfigEnum<TEnum>();// AssemblyConfigs<TEnum>();

			//2 Updating Loaded Params
			UpdateConfigValues<TEnum>(e.InitParams); //CommunicationParamsEn  from Server Params Source          
			
			UpdateClientEnvironment();            
			
			if (ConfigurationEngine.Initialized != null)
			{ Initialized(null, new EventArgs()); }
   
		}

		public static void InitAppConfiguration<TEnum>( StartupEventArgs e)
		{
			//1 load Current assembly enum Configurations
			ConfigurationEngine.LoadAssemblyConfigs();

			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			ConfigurationEngine.LoadConfigEnum<TEnum>();// AssemblyConfigs<TEnum>();

			//2 Updating Loaded Params
			UpdateConfigValues<TEnum>(e.InitParams); //CommunicationParamsEn  from Server Params Source          

			UpdateClientEnvironment();

			if (ConfigurationEngine.Initialized != null)
			{ Initialized(null, new EventArgs()); }

		}






		private static void  UpdateClientEnvironment()
		{
			if (ConfigurationEngine.ConfigEnums.ContainsValue(typeof(EnvironmentParamsEn)) )
			{
				ConfigurationEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri, HtmlPage.Document.DocumentUri.ToString());
				ConfigurationEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri_OriginalString, HtmlPage.Document.DocumentUri.OriginalString);
			}
		}



		/// <summary>
		/// Initializing ConfigurationEngine - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params. 
		/// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2>( UpdateSource = e.InitParams ). 
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration<TEnum1, TEnum2>(object sender, StartupEventArgs e)        
		{  
			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			InitAppConfiguration<TEnum1>(sender, e);            
			ConfigurationEngine.LoadConfigEnum<TEnum2>();

			// load the assembly into our app domain
			
			//2 Updating Loaded Params
			UpdateConfigValues<TEnum1,TEnum2>(e.InitParams); //CommunicationParamsEn            
		}


		/// <summary>
		/// Initializing ConfigurationEngine - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params. 
		/// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3>( UpdateSource = e.InitParams ).
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3>(object sender, StartupEventArgs e)
		{
			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			InitAppConfiguration<TEnum1, TEnum2, TEnum3>(sender, e);            
			ConfigurationEngine.LoadConfigEnum<TEnum3>();

			UpdateConfigValues<TEnum1, TEnum2, TEnum3>(e.InitParams); //CommunicationParamsEn            
		}


		/// <summary>
		/// Initializing ConfigurationEngine - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params. 
		/// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>( UpdateSource = e.InitParams ).
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <typeparam name="TEnum4"></typeparam>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4>(object sender, StartupEventArgs e)
		{
			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			InitAppConfiguration<TEnum1, TEnum2, TEnum3,TEnum4>(sender, e);
			ConfigurationEngine.LoadConfigEnum<TEnum4>();

			UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(e.InitParams); //CommunicationParamsEn            
		}

		/// <summary>
		/// Initializing ConfigurationEngine - Loading all Configuration Enums from the  ConfigurationEngine type assembly into ConfigurationEngine.Params. 
		/// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>( UpdateSource = e.InitParams ).
		/// </summary>
		/// <typeparam name="TEnum1"></typeparam>
		/// <typeparam name="TEnum2"></typeparam>
		/// <typeparam name="TEnum3"></typeparam>
		/// <typeparam name="TEnum4"></typeparam>
		/// <typeparam name="TEnum5"></typeparam>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(object sender, StartupEventArgs e)
		{
			//1 load Current assembly   Enum Configurations      
			//2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
			InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(sender, e);
			ConfigurationEngine.LoadConfigEnum<TEnum4>();


			UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(e.InitParams); //CommunicationParamsEn            
		}

#elif SERVER


#endif



		#endregion ---------------------------------- Init AppConfiguration ------------------------------------


		#region  ------------------------ DELEGATE PARAMS ---------------------------------

		/// <summary>
		/// 
		/// </summary>
		/// <typeparam name="TResult"></typeparam>
		/// <param name="paramKey"></param>
		/// <param name="paramsValues"></param>
		/// <returns></returns>
		public static TResult InvokeStaticDelegateParam<TResult>(Enum paramKey, object[] paramsValues)
		{
			//[Param(typeof(Func< Func<Task>, HttpLoader, bool, Task>), ParamKindEn.Delegate, typeof(Core.Communication.CommunicationService), "HttpQueryWrapper")] 
			//Key - HttpPostQueryWrapper,
			//FinalType -  Func< Func<Task>, HttpLoader, bool, Task>
			//value - typeof(Core.Communication.CommunicationService)
			//supportValue - "HttpQueryWrapper"
			
			ParamAttribute paramInfo = GetParamInfo(paramKey);
			Contract.Assume(paramInfo != null, "Params with such Key does not exist in Params");
			Contract.Assume(paramInfo.Kind != ParamKindEn.Delegate, "Not Delegate Params can't be Called");
			Contract.Assume((paramInfo.Value as Type) != null, "paramInfo.Value must be Type contract");

			MethodInfo methodToCall = (paramInfo.Value as Type).GetMethod(paramInfo.SupportValue.ToString(), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
			Contract.Assume(methodToCall != null, 
				String .Format( "Param with Key{0} was not set correctly to get method:{1} of type:{2} delegate",   paramKey.ToString(), paramInfo.SupportValue, (paramInfo.Value as Type).Name )  );
			
			return (TResult)methodToCall.Invoke(null, paramsValues);

		}


#endregion ------------------------ DELEGATE PARAMS ---------------------------------

		
		


		#endregion ------------------------  METHODS ---------------------------
		

	}
	
}
