﻿using System.ComponentModel;
using System;


namespace Core.Configuration
{


  



   
    /// <summary>
    /// Configuration Enum in Some Assembly
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum, AllowMultiple=false)]
    public class ConfigEnumAttribute : Attribute
    {        
     
        // The constructor is called when the attribute is set.
        #region -------------------------CTOR ---------------------------
        
        /// <summary>
        /// Ctor for configuration attribute
        /// </summary>
        /// <param name="cacheKeyMask">Store this config in the local cache </param>
        /// <param name="itemKeyMask">
        /// ItemKeyMask: we decide - how each of the enum item will be adding to the global dictionary
        /// {EnumType}  - EnumTypeName
        /// {EnumValue} - EnumTypeName
        /// . Example: "{EnumType}.{EnumValue}_mod1" - so key in global configuration Dictionary 
        /// will be looks like this: "Enum1.Value1_mod1" </param>
        /// <param name="enableClientCaching">Enable to store such config in the local cache. </param>
        /// <param name="cacheProvider">Configuration Cache Store  Provider </param>
        public ConfigEnumAttribute(
                                       String cacheKeyMask = null, 
                                       String itemKeyMask = null,
                                       ConfigLevelEn configLevel = ConfigLevelEn.Global,
                                       bool enableClientCaching = false,
                                       ConfigStoreProviderEn cacheProvider = ConfigStoreProviderEn.SterlingDB                                       
                                   )
        {
            CacheKeyMask = cacheKeyMask;

            ItemKeyMask = itemKeyMask;

            EnableClientCaching = enableClientCaching;

            CacheProvider = cacheProvider;

            Level = configLevel;
        }
        
        #endregion -------------------------CTOR --------------------------
        

        #region ---------------------------- PROPERTIES --------------------------- 


        /// <summary>
        /// ItemKeyMask: we decide - how each of the enum item will be adding to the global dictionary
        /// {EnumType}  - EnumTypeName
        /// {EnumValue} - EnumTypeName
        /// Result: [{EnumType}.{EnumValue}_mod1] - so key in global configuration Dictionary will be looks like this: "Enum1.Value1_mod1" 
        /// </summary>
        public string ItemKeyMask
        {
            get;
            private set;
        }



        /// <summary>
        /// Store this config in the local cache .
        /// </summary>
        public string CacheKeyMask
        {
            get;
            private set;
        }
        

        /// <summary>
        /// Enable to store such config in the local cache. 
        /// </summary>
        public bool EnableClientCaching
        {
            get;
            private set;
        }
        


        /// <summary>
        /// Configuration Cache Store  Provider 
        /// </summary>
        public ConfigStoreProviderEn CacheProvider
        {
           get;
           private set;
        }



        /// <summary>
        /// Configuration Level  - how often params can be changed
        /// </summary>
        public ConfigLevelEn Level
        {
           get;
           private set;
        }


        #endregion ---------------------------- PROPERTIES ----------------------------------           
        
    }

}
