﻿
#if NOT_RELEASED

using DDDD.Core.Install;
using System;
using System.Collections.Generic;

namespace DDDD.Core.Environment
{
    /// <summary>
    /// Sensors Utils. Goes here from Win32API as dynamic objects.
    /// </summary>
    public class SensorManager
    {

#region -------------------------------- CTOR -------------------------------

        internal SensorManager()
        {
            Initialize();
        }

#endregion -------------------------------- CTOR -------------------------------


        void Initialize()
        {
            KnownSensorsID.Add(NAME_HP_GNSS_Sensor, WMIID_HP_GNSS_Sensor);
            KnownSensorsID.Add(NAME_SimpleLocationSensor, WMIID_SimpleLocationSensor);
            KnownSensorsID.Add(NAME_MSVSLocationSimulatorSensor, WMIID_MSVSLocationSimulatorSensor);
        }


#region ------------------------------- CONST -------------------------------

        /// <summary>
        /// [HP GNSS Sensor] Device  WMI ID
        /// </summary>
        public const string NAME_HP_GNSS_Sensor = "HP GNSS Sensor";
        public const string WMIID_HP_GNSS_Sensor = @"USB\VID_03F0&PID_521D&MI_02\6&192C714E&3&0002";


        /// <summary>
        /// Microsoft Visual Studio Location Simulator Sensor
        /// </summary>
        public const string NAME_MSVSLocationSimulatorSensor = "Microsoft Visual Studio Location Simulator Sensor";
        public const string WMIID_MSVSLocationSimulatorSensor = @"ROOT\WPD\0000";



        /// <summary>
        /// [Simple Location(GPS) Sensor] Device  WMI ID
        /// </summary>
        public const string NAME_SimpleLocationSensor = "Простой датчик ориентации устройства";
        public const string WMIID_SimpleLocationSensor = @"SWD\SENSORSANDLOCATIONENUM\SENSORSSERVICESENSORSWDEVICE";

        /// devconx64 / r enable "@USB\VID_03F0&PID_521D&MI_02\6&192C714E&3&0002" > a.txt
        /// devconx64 / r disable "@SWD\SENSORSANDLOCATIONENUM\SENSORSSERVICESENSORSWDEVICE" > a.txt

#endregion ------------------------------- CONST -------------------------------


        readonly Dictionary<string, string> _KnownSensorsID = new Dictionary<string, string>();


        /// <summary>
        /// This dictionary stores  associations of [Sensor.Name, Sensor.WMI_ID]. 
        /// <para/> Sensor.Name is not strongly associated with WMI DeviceName. 
        /// <para/> But Sensor.WMIID must satisfy WindowsDevice.WMI_ID.
        /// </summary>
        public Dictionary<string, string> KnownSensorsID
        {
            get { return _KnownSensorsID; }
        }


        public async void DisableGPSSensors()
        {


            foreach (var knownSensor in KnownSensorsID)
            {
                Installer.DisableDeviceByName(knownSensor.Key);
                //await  Devices.Manager.DisableDeviceByID(knownSensor.Value);
            }
        }


        public async void EnableGPSSensors()
        {
            foreach (var knownSensor in KnownSensorsID)
            {
                Installer.EnableDeviceByName(knownSensor.Key);
                //await Devices.Manager.EnableDeviceByID(knownSensor.Value);
            }
        }


        public async void RestartGPSSensors()
        {
            foreach (var knownSensor in KnownSensorsID)
            {
                Installer.RestartDeviceByName(knownSensor.Key);

                //await Devices.Manager.RestartDeviceByID(knownSensor.Value);
            }
        }



    }

}


#endif
