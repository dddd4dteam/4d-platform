﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Windows;
using DDDD.Core.Extensions;
using HANDLE = System.IntPtr;


namespace DDDD.Core.Environment
{


    [Flags]
    internal enum LocalMemoryFlags : int
    {
        LMEM_FIXED = 0x0000,
        LMEM_MOVEABLE = 0x0002,
        LMEM_NOCOMPACT = 0x0010,
        LMEM_NODISCARD = 0x0020,
        LMEM_ZEROINIT = 0x0040,
        LMEM_MODIFY = 0x0080,
        LMEM_DISCARDABLE = 0x0F00,
        LMEM_VALID_FLAGS = 0x0F72,
        LMEM_INVALID_HANDLE = 0x8000,
        LHND = (LMEM_MOVEABLE | LMEM_ZEROINIT),
        LPTR = (LMEM_FIXED | LMEM_ZEROINIT),
        NONZEROLHND = (LMEM_MOVEABLE),
        NONZEROLPTR = (LMEM_FIXED),
    }

    internal enum TOKEN_INFORMATION_CLASS
    {
        TokenUser = 1,
        TokenGroups,
        TokenPrivileges,
        TokenOwner,
        TokenPrimaryGroup,
        TokenDefaultDacl,
        TokenSource,
        TokenType,
        TokenImpersonationLevel,
        TokenStatistics,
        TokenRestrictedSids,
        TokenSessionId
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)] //, Size = 256  
    public class TOKEN_USER          // for NETFx we can use struct, but not class we use here for SL5   
    {
        public _SID_AND_ATTRIBUTES User;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
    public class _SID_AND_ATTRIBUTES // for NETFx we can use struct, but not class we use here for SL5
    {
        public IntPtr Sid;
        public int Attributes;
    }





    /// <summary>
    /// Native Win32 API DllImport Methods
    /// </summary>
    public static class Native
    {
        const string lib_user32 = "user32.dll";
        const string lib_kernel32 = "kernel32.dll";
        const string lib_advapi32 = "advapi32.dll";


        [SecuritySafeCritical]
        internal static T ProcessWinAPi<T>(Func<T> apiFunc)
        {
            try
            {
                return apiFunc();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message); // Other Environment Informer / Logger
                return default(T);
            }
        }




        [DllImport("winspool.drv", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern bool EnumPrinters(PrinterEnumFlags Flags, string Name, uint Level, IntPtr pPrinterEnum, uint cbBuf, ref uint pcbNeeded, ref uint pcReturned);



        #region -------------------------- PROCESS AUTOMATION ----------------------------------


        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process);


        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        internal static extern HANDLE GetCurrentProcess();


        [DllImport("kernel32.dll", SetLastError = true)]
        internal static extern int GetProcessId(HANDLE hProcess);


        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        internal static extern IntPtr GetModuleHandle(string moduleName);


        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        internal static extern IntPtr GetProcAddress(IntPtr hModule,
            [MarshalAs(UnmanagedType.LPStr)]string procName);




        //:: Terminate process
        //$strComputer = "."
        //$colProcesses = Get-WmiObject -Class Win32_Process -ComputerName $strComputer | Where-Object { $_.ProcessId -eq '11432' }
        //foreach ($objProcess in $colProcesses) { $objProcess.Terminate() }


        #endregion -------------------------- PROCESS AUTOMATION ----------------------------------



        #region ------------------------HANDLE ALLOC/FREE  ------------------------

        //[DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        //internal static extern IntPtr LocalAlloc([In] int uFlags, [In] IntPtr sizetdwBytes);

        [DllImport("kernel32.dll", EntryPoint = "LocalAlloc")]
        internal static extern IntPtr LocalAlloc_NoSafeHandle(LocalMemoryFlags uFlags, IntPtr sizetdwBytes);





        [SecurityCritical]  // auto-generated_required        
        public static IntPtr AllocHGlobal(IntPtr cb)
        {
            //WellKnonwSidType

            // For backwards compatibility on 32 bit platforms, ensure we pass values between 
            // Int32.MaxValue and UInt32.MaxValue to Windows.  If the binary has had the 
            // LARGEADDRESSAWARE bit set in the PE header, it may get 3 or 4 GB of user mode
            // address space.  It is remotely that those allocations could have succeeded,
            // though I couldn't reproduce that.  In either case, that means we should continue
            // throwing an OOM instead of an ArgumentOutOfRangeException for "negative" amounts of memory.
            UIntPtr numBytes;
#if WIN32
            numBytes = new UIntPtr(unchecked((uint)cb.ToInt32()));
#else
            //numBytes = new IntPtr( (long)cb.ToInt64() );
#endif
            IntPtr pNewMem = LocalAlloc_NoSafeHandle(LocalMemoryFlags.LMEM_FIXED, cb);

            //IntPtr pNewMem = LocalAlloc((int)LocalMemoryFlags.LPTR, cb);

            if (pNewMem == IntPtr.Zero)
            {
                throw new OutOfMemoryException();
            }
            return pNewMem;
        }


        [SecurityCritical]
        public static IntPtr AllocHGlobal(int size)
        {
            // Allocate the memory, zeroing it in the progress
            // IntPtr memPtr = LocalAlloc(LocalMemoryFlags.LPTR, new IntPtr(size));
            IntPtr memPtr = LocalAlloc_NoSafeHandle(LocalMemoryFlags.LMEM_FIXED, new IntPtr(size));


            // Throw an OutOfMemoryException if out of memory
            if (memPtr == IntPtr.Zero)
                throw new OutOfMemoryException();
            return memPtr;
        }




        [DllImport("Kernel32.dll", SetLastError = true)]
        static extern uint FormatMessage(uint dwFlags, HANDLE lpSource,
                                         uint dwMessageId, uint dwLanguageId, ref HANDLE lpBuffer,
                                         uint nSize, HANDLE pArguments);


        [DllImport("kernel32.dll", SetLastError = true, ExactSpelling = true)]
        internal static extern IntPtr LocalFree(IntPtr hMem);

        [DllImport("kernel32.dll")]
        internal static extern bool CloseHandle(HANDLE handle);


        [SecurityCritical] // auto-generated_required       
        internal static void FreeHGlobal(IntPtr hglobal)
        {
            //if (IsNotWin32Atom(hglobal))
            //{
            if (IntPtr.Zero != LocalFree(hglobal))
            {
                Int32 error = Marshal.GetLastWin32Error();
                throw new InvalidOperationException("Last Windows Error CODE:  " + error.S());// ThrowExceptionForHR(GetHRForLastWin32Error());
            }
            //}
        }

        #endregion ------------------------HANDLE ALLOC FREE  ------------------------



        #region ----------------------------- TABLET/DESKTOP PC INFO ----------------------------


        [DllImport(lib_user32)]
        internal static extern int GetSystemMetrics(int nIndex);
        // System metric constant for Windows XP Tablet PC Edition
        internal const int SM_TABLETPC = 86;


        #endregion ----------------------------- TABLET/DESKTOP PC INFO ----------------------------
        


        #region ---------------------- WINDOW OPERATIONS -  FindWindow /Post Message --------------------------

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport(lib_user32, SetLastError = true)]
        public static extern bool PostMessage(int hWnd, uint Msg, int wParam, int lParam);

        [DllImport(lib_user32)]
        public static extern IntPtr FindWindow(String sClassName, String sAppName);

        [DllImport(lib_user32, SetLastError = true)]
        public static extern UInt32 GetWindowLong(IntPtr hWnd, int nIndex);



        #endregion ---------------------- WINDOW OPERATIONS -  FindWindow /Post Message --------------------------
        


        #region -------------------------- LOAD SQL SERVER NATIVE ASSEMBLIES ------------------------------
        
        //Load Native SQL Server Assemblies
        //LoadAssembly
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr LoadLibrary(string libname);

        /// <summary>
        /// Loads the required native assemblies for the current architecture (x86 or x64)
        /// </summary>
        /// <param name="rootApplicationPath">
        /// Root path of the current application. Use Server.MapPath(".") for ASP.NET applications
        /// and AppDomain.CurrentDomain.BaseDirectory for desktop applications.
        /// </param>
        public static void LoadNativeAssemblies(string rootApplicationPath)
        {
            var nativeBinaryPath = IntPtr.Size > 4
                ? Path.Combine(rootApplicationPath, @"SqlServerTypes\x64\")
                : Path.Combine(rootApplicationPath, @"SqlServerTypes\x86\");

            LoadNativeAssembly(nativeBinaryPath, "msvcr100.dll");
            LoadNativeAssembly(nativeBinaryPath, "SqlServerSpatial110.dll");
        }



        private static void LoadNativeAssembly(string nativeBinaryPath, string assemblyName)
        {
            var path = Path.Combine(nativeBinaryPath, assemblyName);
            var ptr = LoadLibrary(path);
            if (ptr == HANDLE.Zero)
            {
                throw new Exception( "Error loading {0} (ErrorCode: {1})"
                           .Fmt( assemblyName, Marshal.GetLastWin32Error().S() ) );
            }
        }

        #endregion -------------------------- LOAD NATIVE ASSEMBLIES ------------------------------



 

        /// <summary>
        /// The function determins whether a method exists in the export 
        /// table of a certain module.
        /// </summary>
        /// <param name="moduleName">The name of the module</param>
        /// <param name="methodName">The name of the method</param>
        /// <returns>
        /// The function returns true if the method specified by methodName 
        /// exists in the export table of the module specified by moduleName.
        /// </returns>
        public static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            HANDLE moduleHandle = GetModuleHandle(moduleName);
            if (moduleHandle == HANDLE.Zero)
            {
                return false;
            }
            return ( GetProcAddress(moduleHandle, methodName) != HANDLE.Zero);
        }






    }


}
