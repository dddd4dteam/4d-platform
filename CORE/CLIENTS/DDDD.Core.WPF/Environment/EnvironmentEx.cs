﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows;


#if CLIENT && SL5 

using System.Windows.Browser;
using System.IO;

#elif  SERVER

using System.Web.Http;
using System.Web;
using System.IO;

#endif


namespace DDDD.BCL
{




    /// <summary>
    /// Переменные окружения.  
    /// </summary>
    public class EnvironmentEx
    {

        #region -------------------------- Current User --------------------------
        
#if  SERVER
        static WindowsPrincipal _CurrentUser = null;
        /// <summary>
        /// Текущий пРИнципал которому просчитывается доверение прав
        /// </summary>
        public static WindowsPrincipal CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    _CurrentUser = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                }
                return _CurrentUser;
            }
        }

#endif
        //

        #endregion -------------------------- Current User --------------------------
                                                                                   

        #region ------------------------- APPLICATION INFO --------------------------

        /// <summary>
        /// Флаг проверки загрузки приложения
        /// </summary>
        static bool IsAppConfigLoaded = false;

        
        #region  --------------------- Process Profile -----------------------
        
        //ProcessProfile(require on server) – 
       

#if SERVER

        static  ProcessProfileEn _ProcessProfile = ProcessProfileEn.APPSERVER;

        /// <summary>
        /// Профиль Процесса  - функциональная характеристика
        /// </summary>
        public static ProcessProfileEn ProcessProfile
        {
            get
            {
                if (IsAppConfigLoaded== false)
                {
                    TryGetAppConfiguration();
                }
                
                return  _ProcessProfile;
            }
            private set
            {
                _ProcessProfile = value;
            }

        }

#elif CLIENT && SL5
        
        /// <summary>
        /// Профиль Процесса
        /// </summary>
        public static ProcessProfileEn ProcessProfile
        {
            get
            {
                return ProcessProfileEn.CLIENT_SL5;
            }
        }

#endif

        #endregion --------------------- Process Profile -----------------------

        //Other Parameters in DevAppIdentityAttribute
        //Tier(require on server) -
        //Application - 
        //Subsystem(not require) – 




        /// <summary>
        /// Попытка считать базовую информацию для данного процесса приложения
        /// </summary>
        /// <returns></returns>
        static void TryGetAppConfiguration()
        {
            //here we need to load from configuraion    
            //V1 Config  - from Settings                
                                                        
            //V2 Config - from NinjaDBPro               
            //DesignerProperties                        

        }
   

        #endregion ------------------------- APPLICATION INFO --------------------------

     
    #if CLIENT

        
            #region ---------------- DESIGN MODE ------------------------



            private static bool? _isInDesignMode;


            public static bool IsInDesignMode
            {
                get
                {
                    if (!_isInDesignMode.HasValue)
                    {
    #if CLIENT && SL5 
                        _isInDesignMode = DesignerProperties.IsInDesignTool;

    #elif CLIENT && WPF 
                        var prop = DesignerProperties.IsInDesignModeProperty;
                        _isInDesignMode = (bool)DependencyPropertyDescriptor .FromProperty(prop, typeof(FrameworkElement)).Metadata.DefaultValue;
    #endif
                    }

                    return _isInDesignMode.Value;
                }
            }

     
            #endregion ---------------- DESIGN MODE ------------------------

    #endif





        #region ----------------------- PROPERTIES ------------------------



        /// <summary>
        /// Хост процесса приложения: IIS / Desktop / Browser
        /// </summary>
        public static ProcessHostEn ProcessHost
        {
            get;
            internal set;
        }


        /// <summary>
        /// Процесс с типом Звена
        /// </summary>
        public static ProcessSideEn ProcessTier
        {
            get;
            internal set;
        }


        /// <summary>
        /// Логическая  роль процесса в архитектуре всей EIS
        /// </summary>
        public static ProcessArchitectureRoleEn ProcessArchitectureRole
        {
            get;
            internal set;
        }


        /// <summary>
        /// Рассматривать загрузку приложения, его компонентов локально относительно файла запуска.  
        /// </summary>
        public static ProcessLoadingTypeEn ProcessLoadingType
        {
            get;
            internal set;
        }


        /// <summary>
        /// Если это Веб ориентированнный процесс- то какой у него скоп -Интранет или Интернет. 
        /// В зависимости от этого можно усматривать варианты инициализации
        /// </summary>
        public static ProcessWebScopeEn ProcessWebScope
        {
            get;
            internal set;
        }



        #endregion ----------------------- PROPERTIES ------------------------



#if CLIENT && SL5 


        public static bool AutoZoom
        {
            get{ return  Application.Current.Host.Settings.EnableAutoZoom;}
        }

        
        public static bool CacheVizualization
        {
            get{ return  Application.Current.Host.Settings.EnableCacheVisualization;}
        }
        
        public static bool GPUAcceleration
        {
            get{ return  Application.Current.Host.Settings.EnableGPUAcceleration;}
        }
        
        public static bool HTMLAccess
        {
            get{ return  Application.Current.Host.Settings.EnableHTMLAccess;}
        }


        #region --------------------------------------- URI fragments -------------------------------------

        //
        // WebApplication
        // WebApplication
        //

      



        static String _StartSchemeAndServerFragment;
        /// <summary>
        /// Фрагмент  Пути  от адреса документа Страницы старта - До  схемы+сервера
        /// </summary>
        public static String StartSchemeAndServerFragment
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _StartSchemeAndServerFragment == null)
                {
                    _StartSchemeAndServerFragment = HtmlPage.Document.DocumentUri.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);//"/"  UriEscaped
                }
                return _StartSchemeAndServerFragment;
            }

        }


        /// <summary>
        /// Порт сервера с которого стартовало приложение SL5
        /// </summary>
        public static Int32 Port
        {
            get
            {
                return HtmlPage.Document.DocumentUri.Port; 
            }            
        }




        static String _StartRequestUrlFragment;
        /// <summary>
        /// Фрагмент Пути адреса c которого стартует приложение - т.е. весь путь Uri до Параметров от адреса документа Страницы
        /// </summary>
        public static String StartRequestUrlFragment
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _StartRequestUrlFragment == null)
                {
                    _StartRequestUrlFragment = HtmlPage.Document.DocumentUri.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped);//"/"  UriEscaped
                }
                return _StartRequestUrlFragment;
            }
        }

        static String _StartLocalPathFragment;
        /// <summary>
        /// Локальный относительный путь от относительного рутового каталога приложения
        /// </summary>
        public static String StartLocalPathFragment
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _StartLocalPathFragment == null)
                {
                    _StartLocalPathFragment = HtmlPage.Document.DocumentUri.GetComponents(UriComponents.Path, UriFormat.Unescaped); // UriEscaped
                }
                return _StartLocalPathFragment;
            }
        }


      


        static String _StartPageFragment;
        /// <summary>
        /// Название стартовой страницы приложения Silverlight. 
        /// </summary>
        public static String StartPageFragment
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _StartPageFragment == null)
                {                 
                    _StartPageFragment = StartRequestUrlFragment.Split('/')
                                                                .LastOrDefault();
                }
                return _StartPageFragment;
            }

        }



        static String _XapPackageFragment;
        /// <summary>
        /// Название  файла Xap пакета с которого происходит загрузка данного SL приложения
        /// </summary>
        public static String XapPackageFragment
        {
            get
            {
                if( Application.Current.IsRunningOutOfBrowser== false &&  _XapPackageFragment == null)
                {
                    _XapPackageFragment = Application.Current.Host.Source.GetComponents(UriComponents.HttpRequestUrl, UriFormat.Unescaped) // UriEscaped
                                                                         .Split('/')
                                                                         .LastOrDefault();
                }
                return _XapPackageFragment;
            }

        }


        static String _Host;
        /// <summary>
        /// Фрагмент  Пути  от адреса документа Страницы старта - Просто название Сервера
        /// </summary>
        public static String Host
        {
            get
            {
                if (Application.Current.IsRunningOutOfBrowser == false && _Host == null)
                {
                    _Host = HtmlPage.Document.DocumentUri.Host;
                }
                return _Host;
            }

        }

             

        #endregion --------------------------------------- URI fragments -------------------------------------



#endif // SL5 only options










        /// <summary>
        /// Целевой фреймворк - на каком фреймворке работает процессс/приложение.
        /// Техническая-физическая характеристика. You need to add defined precompie symbols for your Case on build  
        /// </summary>
        /// <returns></returns>
        public static TargetNetFrameworkEn GetTargetFramework()
        {
#if    NET4
                        return TargetNetFrameworkEn.NET4;
#elif   NET45
                        return TargetNetFrameworkEn.NET45;
#elif  SL5
                        return TargetNetFrameworkEn.SL5;
#elif  WP8       
                        return  TargetNetFrameworkEn.WP8;
#elif  WP81       
                        return  TargetNetFrameworkEn.WP81;
#elif  Mono
                        return  TargetNetFrameworkEn.Mono;
#elif  MonoDroid
                        return  TargetNetFrameworkEn.MonoDroid;
#elif  MonoTouch
                        return  TargetNetFrameworkEn.MonoTouch;
#else
                    throw new InvalidOperationException("Can't get correct side NetFramework. Check your assembly contains one of precompiled symbols: ");
#endif

        }


        /// <summary>
        /// Получить Путь к подпапке на основе пути к директории старта Приложения
        /// </summary>
        /// <param name="SubDirectory"></param>
        /// <returns></returns>
        public static String GetStartupPathWithSubDir(String SubDirectory="Temp")
        {
            
#if CLIENT && SL5
            
            return  Path.Combine(@"C:\" + SubDirectory);
            //return //HttpRuntime.AppDomainAppPath + SubDirectory;            
            
#else
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, SubDirectory);
#endif
        }



#if SERVER &&  (TEST || DEBUG)
      
        
        /// <summary>
        /// Задать профиль всего процесса приложения в котором будет запускаться инфраструктура.
        /// </summary>
        /// <param name="profile"></param>
        public static void SetProcessProfile(ProcessProfileEn profile)
        {
            ProcessProfile = profile;
        }

        public int DevAppID
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

#endif



    }
}
