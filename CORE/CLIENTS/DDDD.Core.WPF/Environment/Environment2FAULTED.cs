﻿

using System;
using System.Windows;
using HANDLE = System.IntPtr;

#if SERVER  
using System.Diagnostics.Contracts;
using System.Web;
using System.Diagnostics;
using System.ComponentModel.Composition;

#elif SL5
using System.Windows.Browser;
using System.Runtime.InteropServices.Automation;
 
#elif WPF
using System.Security.Principal;

#endif


using System.Security;
using System.IO;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Windows.Threading;
using System.Net.NetworkInformation;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;


using DDDD.Core.HttpRoutes;
using DDDD.Core.Threading;
using DDDD.Core.Extensions;
//using DDDD.Core.Install;
using System.Security.Principal;

namespace DDDD.Core.Environment
{


    /// <summary>
    /// Environment 2 - extended environment options for - Client/Server App primary cooperation,  Getting more Windows Information.
    /// <para/> Environment 2 includes options: 
    ///         , BaseUrl engine
    /// <para/> DESIGN/RUNTIME MODE state for controls  
    /// <para/> Netowrk Info - Windows User &  Domain  & ComputerName 
    /// <para/> Global COM Objects -  WScriptShell, WScriptNetwork      
    /// </summary>
    public sealed class Environment2
    {

        /// <summary>
        /// Is Network Enable
        /// </summary>
        public static bool IsNetworkEnable
        {
            get { return NetworkInterface.GetIsNetworkAvailable(); }
        }

        //shouldn't use static CTOR

#if CLIENT //for SL5 or WPF not for SERVER

        #region ---------------- DESIGN MODE ------------------------

        private static bool? _isInDesignMode;


        public static bool IsInDesignMode
        {
            get
            {
                if (!_isInDesignMode.HasValue)
                {
#if CLIENT && SL5
                    _isInDesignMode = DesignerProperties.IsInDesignTool;

#elif CLIENT && WPF
                    var prop = DesignerProperties.IsInDesignModeProperty;
                    _isInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty(prop, typeof(FrameworkElement)).Metadata.DefaultValue;
#endif
                }

                return _isInDesignMode.Value;
            }
        }

        #endregion ---------------- DESIGN MODE ------------------------

#endif


        static dynamic CreateCOMObject(string COMObjectName)
        {
            try
            {
#if SL5 && DEBUG
                // Check to see if stuff is available
                if (!AutomationFactory.IsAvailable)
                    throw new InvalidOperationException("AutomationFactory is unavailable. Program must be run as out-of-browser with elevated permissions.");
                return AutomationFactory.CreateObject(COMObjectName);

                // return AutomationFactory.CreateObject(COMObjectName);
#elif NET45

                var comObjType = Type.GetTypeFromProgID(COMObjectName);
                return Activator.CreateInstance(comObjType);
#else

                throw new InvalidOperationException("AutomationFactory is unavailable. Program must be run as out-of-browser with elevated permissions.");                 

#endif
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"Environment2.CreateCOMObject([{COMObjectName}]):  " + exc.Message);
            }

        }









        #region ----------------------- PROPERTIES ------------------------



        /// <summary>
        /// Профиль Процесса
        /// </summary>
        public static ProcessProfileEn ProcessProfile
        {
            get;

            protected internal set;

            //#if CLIENT && SL5             

            //        return ProcessProfileEn.CLIENT_SL5;

            //#elif CLIENT && WINRT  

            //        return ProcessProfileEn.CLIENT_WINRT;

            //#elif CLIENT && WPF          

            //        return ProcessProfileEn.CLIENT_WPF;

            //#elif CLIENT && WP                  

            //        return ProcessProfileEn.CLIENT_WP;

            //#elif CLIENT && MONO          

            //        return ProcessProfileEn.CLIENT_MONO;

            //#elif CLIENT && MONODROID                  

            //        return ProcessProfileEn.CLIENT_MONODROID;

            //#elif CLIENT && MONOTOUCH        

            //        return ProcessProfileEn.CLIENT_MONOTOUCH;

            //#elif SERVER
            //        return ProcessProfileEn.WEBSERVER;

            //#elif SERVER && MONO        

            //        return ProcessProfileEn.WEBSERVER_MONO;

            //#elif SERVER && MIDLETIER        

            //      return ProcessProfileEn.APPSERVER;

            //#elif SERVER && MIDLETIER && MONO        

            //      return ProcessProfileEn.APPSERVER_MONO;

            //#endif

        }




        /// <summary>
        /// Хост процесса приложения: IIS / Desktop / Browser
        /// </summary>
        public static ProcessHostEn ProcessHost
        {
            get;
            internal set;
        }


        /// <summary>
        /// Процесс с типом Звена
        /// </summary>
        public static ProcessTierEn ProcessTier
        {
            get;
            internal set;
        }


        /// <summary>
        /// Логическая  роль приложения
        /// </summary>
        public static ProcessLogicalRoleEn ProcessLogicalRole
        {
            get;
            internal set;
        }


        /// <summary>
        /// Рассматривать загрузку приложения, его компонентов локально относительно файла запуска 
        /// </summary>
        public static ProcessLoadingTypeEn ProcessLoadingType
        {
            get;
            internal set;
        }


        /// <summary>
        /// Если это Веб ориентированнный процесс- то какой у него скоп -Интранет или Интернет. 
        /// В зависимости от этого можно усматривать варианты инициализации
        /// </summary>
        public static ProcessWebScopeEn ProcessWebScope
        {
            get;
            internal set;
        }


        /// <summary>
        /// UI  Thread Dispatcher to Report Download  Progress Info
        /// </summary>
        internal static Dispatcher DISP
        {
            get
            {
#if NET45
                return Application.Current.Dispatcher;
#elif SL5
                return Deployment.Current.Dispatcher;
#elif WP81 
                return Deployment.Current.Dispatcher;
#endif

            }
        }




        #endregion ----------------------- PROPERTIES ------------------------



        #region ------------------------- BASEURL ------------------------------

        static BaseUrl _DefaultUrl = new BaseUrl();
        /// <summary>
        /// DefautUrl - Default AppUrl for current Application. 
        /// </summary>
        public static BaseUrl DefaultUrl
        {
            get
            {
                return _DefaultUrl;
            }
        }


        #endregion ------------------------- BASEURL ------------------------------



        #region -------------------------- APP FOLDERS------------------------------


        public static string GetLocalAppFolder(string appSubDirectory)
        {
            return Path.Combine(ClientAppRootDir, appSubDirectory);
        }


        /// <summary>
        /// Path Combination of specialFolder and subPath.
        /// <para/> If Special Folder Access denied then Message Box will be showned and result will be returned as null.
        /// </summary>
        /// <param name="specialFolder"></param>
        /// <param name="subPath"></param>
        /// <returns></returns>
        public static string GetFolder(System.Environment.SpecialFolder specialFolder, string subPath)
        {
            try
            {
                return Path.Combine(System.Environment.GetFolderPath(specialFolder), subPath);
            }
            catch (SecurityException secExc)
            {
                MessageBox.Show(secExc.Message);
                return null;
            }
        }


        static string _ClientAppRootDir = null;
        /// <summary>
        /// ClientAppRootDir -  Root directory of Enterprise Subsystem App on Client
        /// <para/> ClientAppRootDir wil be setted in the same time when Subsystem property value will be setted/or changed
        /// <para/> Rule of Path of ClientAppRootDir on client  - [ENVAPPDIR]\[Subsystem] 
        /// <para/> like:  [c:\Userdata\CRM]
        /// </summary>
        public static string ClientAppRootDir
        {
            get
            {
                if (_ClientAppRootDir == null)
                {
#if     WP81 
                    _ClientAppRootDir = GetFolder(System.Environment.SpecialFolder.ApplicationData, Subsystem);
#else
                    _ClientAppRootDir = GetFolder(System.Environment.SpecialFolder.CommonApplicationData, Subsystem);
#endif


                }
                return _ClientAppRootDir;
            }
            private set
            {
                _ClientAppRootDir = value;
            }
        }


        public static string ClientTempDir
        {
            get { return GetLocalAppFolder("Temp"); }
        }


        /// <summary>
        ///  Ditrib subfolder on Client: [AppRoot]\Distrib 
        /// </summary>
        public static string ClientDistribDir
        {
            get { return GetLocalAppFolder("Distrib"); }
        }


        /// <summary>
        /// Tools subfolder on Client: [AppRoot]\Tools 
        /// </summary>
        public static string ClientToolsDir
        {
            get { return GetLocalAppFolder("Tools"); }
        }






        static string _Subsystem = EnterpriseAppsConsts.FirstSubsystem;
        /// <summary>
        /// Enterprise Subsystem App Name
        /// <para/> like CRM
        /// </summary>
        public static string Subsystem
        {
            get { return _Subsystem; }
            set
            {
                _Subsystem = value;
#if WP81
                ClientAppRootDir = GetFolder(System.Environment.SpecialFolder.ApplicationData, Subsystem);
#else
                ClientAppRootDir = GetFolder(System.Environment.SpecialFolder.CommonApplicationData, Subsystem);
#endif


                //Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.CommonApplicationData)
                //                         , value);
            }
        }


        #endregion -------------------------- APP FOLDERS------------------------------




        #region  -------------------------- METHODS ----------------------------------

         


        #endregion -------------------------- METHODS ----------------------------------

        //
        // WebApplication
        // WebApplication
        //


        #region --------- SL5  SETTINGS AUTO ZOOM/CacheVisualization/GPUAcceleration/HTMLAccess/ IsRunningOutOfBrowser/ HasElevatedPermissions --------

#if SL5

        /// <summary>
        /// Gets or sets a value that indicates whether the Silverlight plug-in will resize
        /// <para/>    its content based on the current browser zoom setting.
        /// <para/> Shortcut to  [Application.Current.Host.Settings.EnableAutoZoom]
        /// </summary>
        public static bool AutoZoom
        {
            get { return Application.Current.Host.Settings.EnableAutoZoom; }
        }

        /// <summary>
        /// Gets or sets a value that indicates whether to use a non-production analysis
        /// <para/>    visualization mode, which shows areas of a page that are not being GPU accelerated
        ///  <para/>   with a colored overlay. Do not use in production code.
        /// <para/> Shortcut to  [Application.Current.Host.Settings.EnableCacheVisualization]
        /// </summary>
        public static bool CacheVizualization
        {
            get { return Application.Current.Host.Settings.EnableCacheVisualization; }
        }

        /// <summary>
        /// Gets a value that indicates whether to use graphics processor unit (GPU) hardware
        /// <para/>    acceleration for cached compositions, which potentially results in graphics optimization.
        /// <para/> Shortcut to  [Application.Current.Host.Settings.EnableGPUAcceleration]
        /// </summary>
        public static bool GPUAcceleration
        {
            get { return Application.Current.Host.Settings.EnableGPUAcceleration; }
        }

        /// <summary>
        /// Gets a value that indicates whether the Silverlight plug-in allows hosted content
        //  <para/>   or its runtime to access the HTML DOM.
        /// <para/> Shortcut to  [Application.Current.Host.Settings.EnableHTMLAccess]
        /// </summary>
        public static bool HTMLAccess
        {
            get { return Application.Current.Host.Settings.EnableHTMLAccess; }
        }

        /// <summary>
        ///  Gets a value that indicates whether the application was launched from the out-of-browser state. 
        /// <para/> Shortcut to  [Application.Current.IsRunningOutOfBrowser]
        /// </summary>
        public static bool IsRunningOutOfBrowser
        {
            get
            {
                return Application.Current.IsRunningOutOfBrowser;
            }
        }

        /// <summary>
        ///  Gets a value that indicates whether the application is running with elevated  permissions.        ///    
        /// <para/> Shortcut to  [Application.Current.HasElevatedPermissions]
        /// </summary>
        public static bool HasElevatedPermissions
        {
            get
            {
                return Application.Current.HasElevatedPermissions;
            }
        }




#endif
        #endregion --------- SL5  SETTINGS AUTO ZOOM/CacheVisualization/GPUAcceleration/HTMLAccess/ IsRunningOutOfBrowser/ HasElevatedPermissions --------



        #region  -------------------------------------- TARGET FRAMEWORK ---------------------------------------

        /// <summary>
        /// Get Target Framework - information flag that help us to determine on what TargetNetFramework current Process compiled and runned.           /// </summary>
        /// <returns></returns>
        static TargetNetFrameworkEn GetTargetFramework()
        {
#if NET4
                        return TargetNetFrameworkEn.NET4;
#elif NET45
            return TargetNetFrameworkEn.NET45;
#elif NET46
            return TargetNetFrameworkEn.NET46;
#elif SL5
            return TargetNetFrameworkEn.SL5;
#elif WPA8
                        return  TargetNetFrameworkEn.WPA8;
#elif WP8
                        return  TargetNetFrameworkEn.WP8;
#elif WPA81
                        return  TargetNetFrameworkEn.WPA81;
#elif WP81
                        return  TargetNetFrameworkEn.WP81;
#elif MONO
                        return  TargetNetFrameworkEn.MONO;
#elif MONOANDROID
                        return  TargetNetFrameworkEn.MONOANDROID;
#elif MONOTOUCH
                        return  TargetNetFrameworkEn.MONOTOUCH;
#else
            throw new InvalidOperationException("Can't determine  TargetNetFramework Port. Check your assembly contains one of precompiled symbols:  NET4  or  NET45  or  SL5  or  WP8  or  WP8SL5  or  WP81  or  WP81SL5  or  Mono  or  MonoDroid  or  MonoTouch ");
#endif

        }



        static TargetNetFrameworkEn _CurrentNetFramework = GetTargetFramework();
        /// <summary>
        /// Get Current TargetNetFramework
        /// </summary>
        public static TargetNetFrameworkEn CurrentNetFramework
        {
            get
            {
                return _CurrentNetFramework;
            }
        }

        #endregion -------------------------------------- TARGET FRAMEWORK --------------------------------------




        #region -------------------------- SYSTEM PRINTERS ------------------------------


        static LazySlim<ObservableCollection<PrinterInformation>> LS_PrintersFunc =
             LazySlim<ObservableCollection<PrinterInformation>>.Create((args) => { return LoadPrinters(PrinterEnumFlags.Local); }, null, null);
        /// <summary>
        /// System Printers 
        /// </summary>
        public static ObservableCollection<PrinterInformation> Printers
        { get { return LS_PrintersFunc.Value; } }



        internal static ObservableCollection<PrinterInformation> LoadPrinters(PrinterEnumFlags Flags)
        {
            ObservableCollection<PrinterInformation> _Printers = new ObservableCollection<PrinterInformation>(); //.Clear();

            uint cbNeeded = 0;
            uint cReturned = 0;
            if (  Native.EnumPrinters(Flags, null, 2, HANDLE.Zero, 0, ref cbNeeded, ref cReturned) )
            {
                // nothing
                return _Printers;
            }

            int lastWin32Error = Marshal.GetLastWin32Error();
            if (lastWin32Error == ERROR_INSUFFICIENT_BUFFER)
            {
                HANDLE pAddr =   Native.AllocHGlobal((HANDLE)cbNeeded); //LocalAlloc_NoSafeHandle(LocalMemoryFlags.LMEM_FIXED, (HANDLE)cbNeeded);

                if ( Native.EnumPrinters(Flags, null, 2, pAddr, cbNeeded, ref cbNeeded, ref cReturned))
                {
                    int offset = pAddr.ToInt32();
                    int increment = Marshal.SizeOf(typeof(PRINTER_INFO_2));
                    for (int i = 0; i < cReturned; i++)
                    {
                        PrinterInformation printer = new PrinterInformation();
                        PRINTER_INFO_2 pinfo = new PRINTER_INFO_2();

                        Marshal.PtrToStructure(new HANDLE(offset), pinfo);

                        printer.Attributes = (PrinterAttributes)pinfo.Attributes;
                        printer.AveragePagesPerMinute = pinfo.AveragePPM;
                        printer.Comment = pinfo.pComment;
                        printer.DefaultPrintProcessorParameters = pinfo.pParameters;
                        printer.DefaultPriority = pinfo.Priority;
                        printer.DriverName = pinfo.pDriverName;
                        printer.Location = pinfo.pLocation;
                        printer.PortName = pinfo.pPortName;
                        printer.PrinterName = pinfo.pPrinterName;
                        printer.PrintProcessor = pinfo.pPrintProcessor;
                        printer.Priority = pinfo.Priority;
                        printer.QueuedJobCount = pinfo.cJobs;
                        printer.SeparatorPageFileName = pinfo.pSepFile;
                        printer.ServerName = pinfo.pServerName;
                        printer.ShareName = pinfo.pShareName;
                        printer.StartTime = pinfo.StartTime;    // should xform this
                        printer.Status = (PrinterStatus)pinfo.Status;
                        printer.UntilTime = pinfo.UntilTime;    // should xform this

                        _Printers.Add(printer);

                        offset += increment;
                    }

                    Native.LocalFree(pAddr);

                }
                else
                {
                    lastWin32Error = Marshal.GetLastWin32Error();
                    throw new Exception("Win32 Error: " + lastWin32Error);
                }
            }

            return _Printers;
        }
        #endregion -------------------------- SYSTEM PRINTERS ------------------------------





        #region --------------------------IsAdministrator USER ----------------------------------
#if SL5



#elif SERVER || WPF

        //http://www.digitalcitizen.life/how-run-programs-administrator-windows-7?page=0%2C1
        //http://stackoverflow.com/questions/4753279/run-explorer-exe-as-administrator-user-within-windows-7
        //http://stackoverflow.com/questions/3600322/check-if-the-current-user-is-administrator

        public static bool IsAdministrator()
        {
            var identity = WindowsIdentity.GetCurrent();
            var principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static bool IsAdministrator2()
        {
            return (new WindowsPrincipal(WindowsIdentity.GetCurrent()))
                    .IsInRole(WindowsBuiltInRole.Administrator);
        }


#endif






        #endregion --------------------------IsAdministrator USER  ----------------------------------


        #region -----------------------WMI from ["WbemScripting.SWbemLocator.SWbemServices"]  AUTOMATION -------------------------

        public const string WMIServiceCOMName = "WbemScripting.SWbemLocator";
        static LazySlim<dynamic> LS_GetWMIService = LazySlim<dynamic>.Create((args) =>
        {
            {
                using (dynamic SWbemLocator = CreateCOMObject(WMIServiceCOMName))
                {
                    SWbemLocator.Security_.ImpersonationLevel = 3;
                    SWbemLocator.Security_.AuthenticationLevel = 4;

                    //   strUser, _
                    //   strPassword, _
                    //    "MS_409", _
                    //    "NTLMDomain:" + strDomain) 

                    // return SWbemServices   .ConnecServer  https://msdn.microsoft.com/en-us/library/aa393720%28v=vs.85%29.aspx
                    return SWbemLocator.ConnectServer(".", @"root\cimv2");
                }
            }
        }
        , null
        , null);


        /// <summary>
        ///  We can use   [WMIService  as  SWbemServices object] to do some WMI operations.
        /// <para/> MSDN info - https://msdn.microsoft.com/en-us/library/aa393854%28v=vs.85%29.aspx
        /// </summary>
        public static dynamic WMIService
        {
            get
            {
                return LS_GetWMIService.Value;
            }
        }


        #endregion -----------------------WMI from ["WbemScripting.SWbemLocator.SWbemServices"]  AUTOMATION -------------------------


        #region -------------------------["Scripting.FileSystemObject"]  AUTOMATION -----------------------------

        public const string ScriptingFileSystemObjectCOMName = "Scripting.FileSystemObject";

        /// <summary>
        ///   [FileSystemObject object]  can work with files.
        /// <para/> MSDN Link: https://msdn.microsoft.com/en-us/library/6tkce7xa%28v=VS.85%29.aspx
        /// <para/> Example of use
        /// <para/>   using (dynamic fso = GetFileSystemObject())
        /// <para/>    {
        /// <para/>       dynamic file = fso.CreateTextFile(@"C:\tmp.txt");
        /// <para/>        file.WriteLine(@"I just wrote to c:\ !!");
        /// <para/>        file.Close();
        /// <para/>    }
        /// </summary>
        public static dynamic GetFileSystemObject()
        {
            return CreateCOMObject(ScriptingFileSystemObjectCOMName);
        }



        #endregion ------------------------- ["Scripting.FileSystemObject"]  AUTOMATION -----------------------------



        #region -------------------------- OS INFO ---------------------------

        private const int ERROR_INSUFFICIENT_BUFFER = 122;
        const string NL = "\r\n";

        // http://stackoverflow.com/questions/336633/how-to-detect-windows-64-bit-platform-with-net?rq=1
        /// <summary>
        /// Is This OS use 64bit Processor architecture 
        /// </summary>
        public static bool Is64BitOS
        { get { return LS_Is64BitOSFunc.Value; } }

        static LazySlim<bool> LS_Is64BitOSFunc = LazySlim<bool>.Create(GetIs64BitOperatingSystem, null, null);

        /// <summary>
        /// The function determines whether the current operating system is a 
        /// 64-bit operating system.
        /// </summary>
        /// <returns>
        /// The function returns true if the operating system is 64-bit; 
        /// otherwise, it returns false.
        /// </returns>
        static bool GetIs64BitOperatingSystem(params object[] args)
        {
            if (HANDLE.Size == 8)  // 64-bit programs run only on Win64
            {
                return true;
            }
            else  // 32-bit programs run on both 32-bit and 64-bit Windows
            {
                // Detect whether the current process is a 32-bit process 
                // running on a 64-bit system.
                bool flag;
                return ((Native.DoesWin32MethodExist("kernel32.dll", "IsWow64Process") &&
                   Native.IsWow64Process(Native.GetCurrentProcess(), out flag)) && flag);
            }
        }


        // System metric constant for Windows XP Tablet PC Edition
        internal const int SM_TABLETPC = 86;
        //internal static readonly bool tabletEnabled;


        public static bool IsRunningOnTablet
        { get { return LS_IsRunningOnTabletFunc.Value; } }

        static LazySlim<bool> LS_IsRunningOnTabletFunc = LazySlim<bool>.Create(
            (args) => { return (Native.GetSystemMetrics(SM_TABLETPC) != 0); },  null ,null );


         



        /// <summary>
        /// Application Current Culture
        /// </summary>
        public static CultureInfo CurrentCulture
        { get; } = CultureInfo.CurrentCulture;


        /// <summary>
        /// Application Current UI-Culture
        /// </summary>
        public static CultureInfo CurrentUICulture
        { get; } = CultureInfo.CurrentUICulture;




#endregion --------------------------OS INFO ---------------------------



#region ---------------------------------------- APP PROCESS- INFO -----------------------------------------
         

        /// <summary>
        /// Current Process ID
        /// </summary>
        public static int CurrentProcessID
        { get { return LS_CurrentProcessIDFunc.Value; } }
        static LazySlim<int> LS_CurrentProcessIDFunc = LazySlim<int>.Create(GetCurrentProcessID, null, null);



        static int GetCurrentProcessID(params object[] args)
        {
            return Native.GetProcessId(Native.GetCurrentProcess());
            
        }

        /// <summary>
        /// Is this OS- Windows x64 bit OS
        /// </summary>
        public static bool IsWindows64Process
        { get { return LS_IsWindows64ProcessFunc.Value; } } // = GetIsWindows64Process();

        static LazySlim<bool> LS_IsWindows64ProcessFunc = LazySlim<bool>.Create(GetIsWindows64Process, null, null);


        static bool GetIsWindows64Process(params object[] args)
        {
            bool resIs64Process = false;
            Native.IsWow64Process(Native.GetCurrentProcess(), out resIs64Process);
            return resIs64Process;             

        }




#endregion ---------------------------------------- APP PROCESS- INFO -----------------------------------------



#region ---------------------- RunningOnTablet & EnableSecuredOperations ----------------------------


#if SL5
        /// <summary>
        /// Check only on SL5 :  HasElevatedPermissions && AutomationFactory.IsAvailable
        /// </summary>
        public static bool EnableSecuredOperations
        {
            get
            {
                return (HasElevatedPermissions && AutomationFactory.IsAvailable);
            }
        }


        [Conditional("DEBUG")]
        public static void EnableSecuredOperationsDbg()
        {
            if (EnableSecuredOperations == false)
            {
                MessageBox.Show("Нет ElevatedTrust"); return;
            }
        }

#endif

#endregion ---------------------- RunningOnTablet & EnableSecuredOperations ----------------------------




    }
}



#if NOT_RELEASED
#endif

#region ------------------------------- GARBAGE NOT_USED -----------------------------------


///// <summary>
/////  Загрузка процесса приложения 
///// </summary>
///// <param name="processProfile"></param>
///// <param name="processHost"></param>
///// <param name="processTier"></param>
///// <param name="processLogicalRole"></param>
///// <param name="processLoading"></param>
///// <param name="processWebScope"></param>
//public static void BootEnvironment(
//#if WP81
//            ProcessProfileEn processProfile = ProcessProfileEn.CLIENT_WP81,
//            ProcessHostEn processHost = ProcessHostEn.Desktop,
//#elif SL5
//            ProcessProfileEn processProfile = ProcessProfileEn.CLIENT_SL5,
//    ProcessHostEn processHost = ProcessHostEn.Browser,
//#elif WPF
//            ProcessProfileEn processProfile = ProcessProfileEn.CLIENT_WPF,
//            ProcessHostEn processHost = ProcessHostEn.Desktop,
//#endif

//#if CLIENT
//            ProcessTierEn processTier = ProcessTierEn.CLIENT,
//    ProcessLogicalRoleEn processLogicalRole = ProcessLogicalRoleEn.CLIENTAPPPROCESS,
//    ProcessLoadingTypeEn processLoading = ProcessLoadingTypeEn.LoadFromPlatform,
//    ProcessWebScopeEn processWebScope = ProcessWebScopeEn.IntranetWeb

//#elif SERVER
//            ProcessProfileEn processProfile = ProcessProfileEn.WEBSERVER,
//            ProcessHostEn processHost = ProcessHostEn.WebServerIIS,
//            ProcessTierEn processTier = ProcessTierEn.SERVER,
//            ProcessLogicalRoleEn processLogicalRole = ProcessLogicalRoleEn.WEBENDPOINT,
//            ProcessLoadingTypeEn processLoading = ProcessLoadingTypeEn.LoadLocally,
//            ProcessWebScopeEn processWebScope = ProcessWebScopeEn.IntranetWeb                         
//#endif

//)
//{

//    //set global environment -Process Settings 
//    ProcessProfile = processProfile;
//    ProcessHost = processHost;
//    ProcessTier = processTier;
//    ProcessLogicalRole = processLogicalRole;
//    ProcessLoadingType = processLoading;
//    ProcessWebScope = processWebScope;


//}
 

#region ----------------------- ["Shell.Application"]  AUTOMATION -------------------------

//public const string ShellApplicationCOMName = "Shell.Application";
//static LazySlim<dynamic> LS_GetShellApplication = LazySlim<dynamic>.Create((args) =>
//{ return CreateCOMObject(WScriptNetworkCOMName); }, null, null);


///// <summary>
///// COM Automated Object  ["Shell.Application"] - Windows Explorer Application object.
///// <para/> MSDN info - https://msdn.microsoft.com/en-us/library/windows/desktop/bb774063%28v=vs.85%29.aspx
/////  <para/> - API method 1: ShellApplication.ShellExecute([1]FilePath, [2]"", [3]"", [4]"open", [5]1) - make call of some tool and waits when the process will be completed.
/////  <para/> --- [1] FIlePath - like @"C:\Program Files\Common Files\microsoft shared\ink\TabTip.exe"
/////  <para/> --- [2] FIlePath - like ""
/////  <para/> --- [3] FIlePath - like ""
/////  <para/> --- [4] Verb     - like "open" - to open
/////  <para/> --- [5] Style    - like 1 or 0. o to visible mode
///// </summary>
//public static dynamic ShellApplication
//{
//    get
//    {
//        return LS_GetShellApplication.Value;
//    }
//}


#endregion ----------------------- ["Shell.Application"]  AUTOMATION -------------------------


#region ----------------------- ["WScript.Shell"]  AUTOMATION -------------------------

//public const string WScriptShellCOMName = "WScript.Shell";

//static LazySlim<dynamic> LS_GetWScriptShellFunc = LazySlim<dynamic>.Create((args) =>
//{ return CreateCOMObject(WScriptShellCOMName); },null, null);


///// <summary>
///// COM Automated Object  ["WScript.Shell"] .  It can be used to call some  cmd console command. 
///// <para/> MSDN info - https://msdn.microsoft.com/en-us/library/aew9yb99%28v=vs.84%29.aspx        
///// <para/> - API method 1:  WScriptShell.Run([1]string,[2]0/1,[3]bool) - make call of some tool and waits when the process will be completed. 
///// <para/> --- [1] - cmd string with call of some tool; 
///// <para/> --- [2] - '0' or '1' hidden or visible ConsoleWindow ; 
///// <para/> --- [3] - true - wait for process finishes call or - false ... 
///// <para/> - API method 2:  WScriptShell.Exec([1]string command) - make call  of some tool and return focus/or tool COM object  immediately, even if command was not completed 
///// <para/> --- [1] - cmd string with call of some tool; 
///// <para/> - API method 3:  WScriptShell.RegWrite([1]string RegPath,[2] object value); or 
///// <para/>                               RegWrite([1]string RegPath,[2] object value, [3] RegValueType) - write some value to register. But use Environment2.RegisterWrite()
///// <para/> - API method 4:  WScriptShell.RegRead([1]string RegPath) - read some value from registry path. But use Environment2.RegisterRead{T}()
///// </summary>
//public static dynamic WScriptShell
//{
//    get
//    {
//        return LS_GetWScriptShellFunc.Value;
//    }
//}





//public static void StartShutDown(string parameters)
//{
//    WScriptShell.Run(@"cmd /C shutdown " + parameters, 0, false);
//}

//public static void Restart()
//{
//    StartShutDown("-f -r -t 5");
//}

#endregion ----------------------- ["WScript.Shell"]  AUTOMATION -------------------------



#region ----------------------- ["WScript.Network"]  AUTOMATION -------------------------

//        public const string WScriptNetworkCOMName = "WScript.Network";
//        static LazySlim<dynamic> LS_GetWScriptNetworkFunc = LazySlim<dynamic>.Create((args) =>
//        { return CreateCOMObject(WScriptNetworkCOMName); } , null, null);


//        /// <summary>
//        /// COM Automated Object  ["WScript.Network"] .
//        ///<para/> MSDN info - https://msdn.microsoft.com/en-us/library/s6wt333f%28v=vs.84%29.aspx
//        ///<para/> API property .UserName     - current Windows Account UserName 
//        ///<para/> API property .ComputerName - current  PC  Name 
//        /// </summary>
//        public static dynamic WScriptNetwork
//        {
//            get
//            {
//                return LS_GetWScriptNetworkFunc.Value;
//            }
//        }

//        /// <summary>
//        /// Current ComputerName  from Network settings
//        /// <para/>  Property shortcut to [WScriptNetwork.ComputerName]
//        /// </summary>
//        public static string ComputerName
//        {
//            get
//            {
//#if SL5 || WP81
//                return WScriptNetwork.ComputerName;
//#elif NET45
//                return System.Environment.MachineName;
//#endif
//            }
//        }

//        /// <summary>
//        /// UserName from Current Windows Account from Network settings.
//        /// <para/>  Property shortcut to  [WScriptNetwork.UserName]
//        /// </summary>
//        public static string UserName
//        {
//            get
//            {
//#if SL5 || WP81
//                return WScriptNetwork.UserName;
//#elif NET45
//                return System.Environment.UserName;
//#endif
//            }
//        }

//        /// <summary>
//        /// UserDomain info of Current SL Application from Network settings.
//        /// <para/>  Property shortcut to  [WScriptNetwork.UserDomain]
//        /// </summary>
//        public static string UserDomain
//        {
//            get
//            {
//#if SL5 || WP81
//                return WScriptNetwork.UserDomain;
//#elif NET45
//                return System.Environment.UserDomainName;
//#endif
//            }
//        }


#endregion ----------------------- ["WScript.Network"]  AUTOMATION -------------------------





//Check IE exist in RUNASADMIN register List
//If not Add IE to RUNASADMIN register List


//public static bool CheckIERunnedAsAdmin()
//{
//    var regKeyPath = @"HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\";
//    var regKeyParam = @" C:\Program Files\Internet Explorer\iexplore.exe";
//    var reslt = Installer.RegistryReadPS<string>(regKeyPath, regKeyParam); // Environment2.RegisterRead<string>(regKeyPath, regKeyParam);

//    return (reslt == " ~ RUNASADMIN");
//}


//public static bool SetIERunAsAdmin()
//{
//    var regKeyPath = @"HKEY_CURRENT_USER\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\";
//    var regKeyParam = @" C:\Program Files\Internet Explorer\iexplore.exe";

//    var regKeyPathValue = " ~ RUNASADMIN";
//    Installer.RegistryWritePS(regKeyPath, regKeyParam, regKeyPathValue);

//    return true;
//}



///// <summary>
///// OS installed MUI Languages
///// </summary>
//public static string[] MUILanguages
//{ get { return LS_MUILanguagesFunc.Value; } }

//static LazySlim<string[]> LS_MUILanguagesFunc = LazySlim<string[]>.Create(GetMUILanguages, null, null);


//static string[] GetMUILanguages(params object[] args)
//{
//    try
//    {
//        return OSInfo["MUILanguages"].Replace("{", "").Replace("}", "")
//                                     .Split(new[] { ";", "," }, StringSplitOptions.RemoveEmptyEntries);
//    }
//    catch (Exception exc)
//    {
//        throw exc;
//    }
//}




////Get-WinSystemLocale
//public static string WinSystemLocale
//{ get { return LS_WinSystemLocaleFunc.Value; } }
//static LazySlim<string> LS_WinSystemLocaleFunc = LazySlim<string>.Create(GetWinSystemLocale, null, null);


//static string GetWinSystemLocale(params object[] args)
//{
//    try
//    {
//        var reslt = PowerShellManager.RunPSScript("Get-WinSystemLocale"
//                                                 , returnResult: true, waitOnRun: true
//                                                 , selectProperties: "*"
//                                                 , format: PSFormatEn.FormatList)
//                                                 .GetParameterValue<string>("Name");
//        return reslt;
//    }
//    catch (Exception exc)
//    {
//        throw exc;
//    }
//}




//#if SL5
//static LazySlim<string> LS_CurrentProcessExecutionFilePathFunc =
//   LazySlim<string>.Create(GetCurrentProcessExecutionFilePath, null, null);

//static string GetCurrentProcessExecutionFilePath(params object[] args)
//{
//    // Get-WmiObject -Class Win32_Process -ComputerName "." | Where-Object { $_.ProcessId -eq '10232' } | select Path | fl
//    var getCurProcessExecFilePath = "Get-WmiObject -Class Win32_Process -ComputerName \".\" | Where-Object { $_.ProcessId -eq " + CurrentProcessID.S() + @" }";

//    return PowerShellManager.RunPSScript(getCurProcessExecFilePath,
//                                          returnResult: true, waitOnRun: true,
//                                          selectProperties: "Path", format: PSFormatEn.FormatList)
//                             .GetParameterValue<string>("Path");
//}

///// <summary>
///// Current Process Execution File Path
///// </summary>
//public static string CurrentProcessExecutionFilePath
//{ get { return LS_CurrentProcessExecutionFilePathFunc.Value; } }

//#endif





//#if SL5
///// <summary>
///// <para/> OS Info Contains the following keys:        
///// <para/> PSComputerName                            : WIN81NBX
///// <para/> Status                                    : OK
///// <para/> Name                                      : Microsoft Windows 8.1 Pro|C:\Windows|\Device\Harddisk0\Partition2
///// <para/> FreePhysicalMemory                        : 5123984
///// <para/> FreeSpaceInPagingFiles                    : 1202332
///// <para/> FreeVirtualMemory                         : 5393932
///// <para/> BootDevice                                : \Device\HarddiskVolume1
///// <para/> BuildNumber                               : 9600
///// <para/> BuildType                                 : Multiprocessor Free
///// <para/> Caption                                   : Microsoft Windows 8.1 Pro
///// <para/> CodeSet                                   : 1251
///// <para/> CountryCode                               : 7
///// <para/> CreationClassName                         : Win32_OperatingSystem
///// <para/> CSCreationClassName                       : Win32_ComputerSystem
///// <para/> CSDVersion                                : 
///// <para/> CSName                                    : WIN81NBX
///// <para/> CurrentTimeZone                           : 240
///// <para/> DataExecutionPrevention_32BitApplications : True
///// <para/> DataExecutionPrevention_Available         : True
///// <para/> DataExecutionPrevention_Drivers           : True
///// <para/> DataExecutionPrevention_SupportPolicy     : 2
///// <para/> Debug                                     : False
///// <para/> Description                               : 
///// <para/> Distributed                               : False
///// <para/> EncryptionLevel                           : 256
///// <para/> ForegroundApplicationBoost                : 0
///// <para/> InstallDate                               : 20140604120258.000000+240
///// <para/> LargeSystemCache                          : 
///// <para/> LastBootUpTime                            : 20151117190626.498493+240
///// <para/> LocalDateTime                             : 20151122130052.219000+240
///// <para/> Locale                                    : 0419
///// <para/> Manufacturer                              : Microsoft Corporation
///// <para/> MaxNumberOfProcesses                      : 4294967295
///// <para/> MaxProcessMemorySize                      : 137438953344
///// <para/> MUILanguages                              : {en-US}
///// <para/> NumberOfLicensedUsers                     : 0
///// <para/> NumberOfProcesses                         : 113
///// <para/> NumberOfUsers                             : 3
///// <para/> OperatingSystemSKU                        : 48
///// <para/> Organization                              : 
///// <para/> OSArchitecture                            : 64-bit
///// <para/> OSLanguage                                : 1033
///// <para/> OSProductSuite                            : 256
///// <para/> OSType                                    : 18
///// <para/> OtherTypeDescription                      : 
///// <para/> PAEEnabled                                : 
///// <para/> PlusProductID                             : 
///// <para/> PlusVersionNumber                         : 
///// <para/> PortableOperatingSystem                   : False
///// <para/> Primary                                   : True
///// <para/> ProductType                               : 1
///// <para/> RegisteredUser                            : A1
///// <para/> SerialNumber                              : 00261-50000-00000-AA989
///// <para/> ServicePackMajorVersion                   : 0
///// <para/> ServicePackMinorVersion                   : 0
///// <para/> SizeStoredInPagingFiles                   : 1310720
///// <para/> SuiteMask                                 : 272
///// <para/> SystemDevice                              : \Device\HarddiskVolume2
///// <para/> SystemDirectory                           : C:\Windows\system32
///// <para/> SystemDrive                               : C:
///// <para/> TotalSwapSpaceSize                        : 
///// <para/> TotalVirtualMemorySize                    : 9689960
///// <para/> TotalVisibleMemorySize                    : 8379240
///// <para/> Version                                   : 6.3.9600
///// <para/> WindowsDirectory                          : C:\Windows
///// <para/> Scope                                     : System.Management.ManagementScope
///// <para/> Path                                      : \\WIN81NBX\root\cimv2:Win32_OperatingSystem=@
///// <para/> Options                                   : System.Management.ObjectGetOptions
///// <para/> ClassPath                                 : \\WIN81NBX\root\cimv2:Win32_OperatingSystem
///// <para/> Properties                                : {BootDevice, BuildNumber, BuildType, Caption...}
///// <para/> SystemProperties                          : {__GENUS, __CLASS, __SUPERCLASS, __DYNASTY...}
///// <para/> Qualifiers                                : {dynamic, Locale, provider, Singleton...}
///// <para/> Site                                      : 
///// <para/> Container                                 : 
///// </summary>
//public static Dictionary<string, string> OSInfo
//{
//    get { return LS_OSInfoFunc.Value; }
//}

//static LazySlim<Dictionary<string, string>> LS_OSInfoFunc = LazySlim<Dictionary<string, string>>
//                                                                .Create(GetOSInfo, null, null);

//private static Dictionary<string, string> GetOSInfo(params object[] args)
//{
//    var osinfoScript = "Get-WmiObject -class win32_operatingsystem -computername '.' ";
//    var result = PowerShellManager.RunPSScript(osinfoScript
//        , returnResult: true, waitOnRun: true, selectProperties: "*");


//    try
//    {
//        if (result.IsNullOrEmpty()) throw new InvalidOperationException($"{nameof(Environment2)}.{nameof(GetOSInfo)}(): Cant get OS Info Parameters");
//        //parse results    
//        var lines = result.SplitNoEntries(new string[] { NL, System.Environment.NewLine });

//        var dictOSParameters = new Dictionary<string, string>();
//        foreach (var ln in lines)
//        {

//            var keyValue = ln.SplitNoEntries(new string[] { ":" });
//            if (keyValue[0].Contains("__") || keyValue[1].Contains("__")) continue;

//            dictOSParameters.Add(keyValue[0].Trim(), keyValue[1].Trim());
//        }

//        return dictOSParameters;
//    }
//    catch (Exception)
//    {
//        throw;
//    }

//}

//#endif



#region --------------------------- [WScriptShell WINDOWS LUA]  ------------------------------------


//#if SL5
//const string WindowsLUA_RegistryKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System\EnableLUA";
///// <summary>
///// Is Current Windows LUA Policy Turned ON
///// </summary>
//public static bool IsWindowsLUAOn
//{
//    get
//    {
//        //var registerKey = @"HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System\EnableLUA";
//        var regReaded = Installer.RegistryReadWSH<int>(WindowsLUA_RegistryKey);
//        return (regReaded == 1);
//    }
//}
//#endif

#endregion ----------------------------------- [WScriptShell WINDOWS LUA]  ------------------------------------


#endregion  ------------------------------- GARBAGE NOT_USED -----------------------------------


