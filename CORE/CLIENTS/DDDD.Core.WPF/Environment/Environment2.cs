﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Net.NetworkInformation;
using System.Windows;
using System.Windows.Threading;
using System.IO;
using System.Security;
using System.Reflection;
using Microsoft.Win32;

using static System.Environment;

using DDDD.Core.Extensions;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Diagnostics;

using DDDD.Core.ComponentModel;


#if CLIENT && WPF


#elif CLIENT && SL5

   using System.Runtime.InteropServices.Automation;

#elif CLIENT && WP81



#elif SERVER && IIS
using System.Web;


#endif




namespace DDDD.Core.Environment
{


    /// <summary>
    /// Environment 2 with additional useful universal cross-platfom app/process properties and methods.   
    /// <para/> - Network availability - [IsNetworkEnable]
    /// <para/> - DeafultUrl - BaseUrl class mechanism to determine base application/it's server url, to build based on it urls of sub services or communicate with server for components. 
    /// <para/> - Subsystem - Key aka AppRole concept in Azure applications
    /// <para/> - DesignMode - crossplatform info about Design Mode
    /// <para/> - DISP - Application UI thread cross-platform Dispatcher reference
    /// <para/> - ClientAppRootDir - base folder value in different 
    /// </summary>
    public class Environment2
    {
        // shouldn't use static CTOR

        #region -------------------------SHARED  PROPERTIES ------------------------------

        /// <summary>
        /// Is Network Enable
        /// </summary>
        public static bool IsNetworkEnable
        {
            get { return NetworkInterface.GetIsNetworkAvailable(); }
        }





        /// <summary>
        /// DefautUrl - Default AppUrl for current Application. 
        /// </summary>
        public static BaseUrl DefaultUrl
        { get; } = new BaseUrl();



        #region ---------------------- DEFAULT APP LOGGER ---------------------------

        static DCLogger _Logger = null;
        /// <summary>
        /// Default  App Logger 
        /// </summary>
        public static DCLogger Logger
        {
            get
            {
                if (_Logger.IsNull())
                {
                    _Logger = DCLogger.AddOrUseExist_EmptyLogger(DCLogger.DefaultLogKey);  //standart primary    File targed Logger for Application.
                }
                return _Logger;
            }
        }
        #endregion ---------------------- DEFAULT APP LOGGER ---------------------------





        #endregion -------------------------SHARED  PROPERTIES ------------------------------



        ///  SOME SPECIFIC CODE FOR CLIENTS AND SERVER 
#if SERVER && IIS



        /// <summary>
        /// StackTrace value from System.Environment
        /// </summary>
        public static string StackTrace
        {
            get { return System.Environment.StackTrace; }
        }




        static string _AppComponentName = "CRM.Server.IIS";
        /// <summary>
        /// Your current App Key - we also means here this App as Component on the Apps Level of Enterprise System.         
        /// By default it is 'CRM'
        /// </summary>
        public static string AppComponentName
        {
            get
            {
                return _AppComponentName;
            }
            set
            {
                if (value.IsNotNullOrEmpty() && _AppComponentName != value)
                {
                    _AppComponentName = value;                    
                }
            }
        }


      
        //System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;

        static string _ServerAppRootDir = HttpContext.Current.Server.MapPath("~");
        /// <summary>
        /// ServerAppRootDir -  Root directory of [Enterprise.AppComponent] on Client. This value is highly recommended path to store/save application or its components inside OS.        
        /// <para/> Rule of Path of ServerAppRootDir on client  - HttpContext.Current.Server.MapPath("~")
        /// </summary>
        public static string ServerAppRootDir
        {
            get
            {   
                return _ServerAppRootDir;
            }             
        }

        /// <summary>
        /// Get path of Client Application subfolder as:  [AppRoot]\Subfolder 
        /// </summary>
        /// <param name="subfolder"></param>
        /// <returns></returns>
        public static string GetAppSubfolder(SubfolderEn subfolder)
        {
            return GetLocalAppFolder(subfolder.S());
        }



        /// <summary>
        /// Based on [ClientAppRootDir]  folder  of  current  Application.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="appSubDirectory"></param>
        /// <returns></returns>
        public static string GetLocalAppFolder(string appSubDirectory)
        {     return Path.Combine(ServerAppRootDir, appSubDirectory); 
        }




        #region ----------------------- USERNAME && USERDOMAIN -----------------------------


        /// <summary>
        /// UserName from Current Windows Account from Network settings.
        /// <para/>  Property shortcut to  [WScriptNetwork.UserName]
        /// </summary>
        public static string UserName
        {
            get
            {
                return System.Environment.UserName;
            }
        }

        /// <summary>
        /// UserDomain info of Current SL Application from Network settings.
        /// <para/>  Property shortcut to  [WScriptNetwork.UserDomain]
        /// </summary>
        public static string UserDomain
        {
            get
            {
                return System.Environment.UserDomainName;
            }
        }


        #endregion ----------------------- USERNAME && USERDOMAIN -----------------------------



#elif CLIENT && SL5


        /// <summary>
        ///   StackTrace value   - null value
        /// </summary>
        public static string StackTrace
        {
            get { return null; }
        }


        private static bool? isInDesignMode;

        /// <summary>
        /// IS in Design Mode.
        /// </summary>
        public static bool IsInDesignMode
        {
            get
            {
                if (!isInDesignMode.HasValue)
                {
                    isInDesignMode = DesignerProperties.IsInDesignTool;
                }

                return isInDesignMode.Value;
            }
        }


        /// <summary>
        /// UI  Thread Dispatcher to Report Download  Progress Info
        /// </summary>
        internal static Dispatcher DISP
        { get; } = Deployment.Current.Dispatcher;




        internal static dynamic CreateCOMObject(string COMObjectName)
        {
           return OperationInvoke.CallInMode( nameof(Environment2), nameof(CreateCOMObject),
                ()=>
                {
                    if (!AutomationFactory.IsAvailable)
                        throw new InvalidOperationException("AutomationFactory is unavailable. Program must be run as out-of-browser or with elevated permissions.");
                    return AutomationFactory.CreateObject(COMObjectName);
                }
                );           
        }




        #region ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  

        /// <summary>
        ///  Executing Assembly
        /// </summary>
        public static Assembly ExecutingAssembly
        { get; } = Assembly.GetExecutingAssembly();

        /// <summary>
        /// Executing Assembly  Location if  Executing Assembly exists in current context.
        /// </summary>
        public static string ExecutingAssemblyLocation
        {
            get
            {
                if (ExecutingAssembly.IsNull()) return null;
                return ExecutingAssembly.Location;
            }
        }

        static string _AppComponentName = "CRM"; 
        /// <summary>
        /// Your current App Key - we also means here this App as Component on the Apps Level of Enterprise System.         
        /// By default it is 'CRM'
        /// </summary>
        public static string AppComponentName
        { get {
                return _AppComponentName;
              }
          set{
               if (value.IsNotNullOrEmpty() && _AppComponentName != value)
               {
                    _AppComponentName = value;
                    ClientAppRootDir = GetSpecFolder(SpecialFolder.CommonApplicationData, _AppComponentName);
                }
             } 
         }





         
        /// <summary>
        /// ClientAppRootDir -  Root directory of [Enterprise.AppComponent] on Client. This value is highly recommended path to store/save application or its components inside OS.
        /// <para/> ClientAppRootDir wil be setted in the same time when AppComponentName property value will be setted/or changed
        /// <para/> Rule of Path of ClientAppRootDir on client  - [ENVAPPDIR]\[AppComponentName], like:  [c:\Userdata\CRM]         
        /// </summary>
        public static string ClientAppRootDir
        { get; private set; } = GetSpecFolder(SpecialFolder.CommonApplicationData, AppComponentName);




        /// <summary>
        /// Based on [ClientAppRootDir]  folder  of  current  Application.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="appSubDirectory"></param>
        /// <returns></returns>
        public static string GetLocalAppFolder(string appSubDirectory)
        {           
           return Path.Combine(ClientAppRootDir, appSubDirectory);            
        }






        /// <summary>
        /// Path Combination of specialFolder and subPath.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="specialFolder"> It's System.Environment.SpecialFolder value-Key </param>
        /// <param name="subPath">Sub path after [specialFolder] value</param>
        /// <returns></returns>
        public static string GetSpecFolder(SpecialFolder specialFolder, string subPath)
        {             
             return Path.Combine(GetFolderPath(specialFolder), subPath);             
        }


        /// <summary>
        /// Get path of Client Application subfolder as:  [AppRoot]\Subfolder 
        /// </summary>
        /// <param name="subfolder"></param>
        /// <returns></returns>
        public static string GetAppSubfolder(SubfolderEn subfolder)
        {
            return GetLocalAppFolder(subfolder.S());
        }



        #endregion ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  



#elif CLIENT && WPF

        /// <summary>
        /// StackTrace value from System.Environment
        /// </summary>
        public static string StackTrace
        {
            get { return System.Environment.StackTrace;  }
        }

        private static bool? isInDesignMode;
        
        /// <summary>
        /// IS in Design Mode
        /// </summary>
        public static bool IsInDesignMode
        {
            get
            {
                if (!isInDesignMode.HasValue)
                { 
                    var prop = DesignerProperties.IsInDesignModeProperty;
                    isInDesignMode = (bool)DependencyPropertyDescriptor.FromProperty(prop, typeof(FrameworkElement)).Metadata.DefaultValue; 
                }

                return isInDesignMode.Value;
            }
        }



        static Dispatcher _DISP = null;
        /// <summary>
        /// UI  Thread Dispatcher to Report Download  Progress Info
        /// </summary>
        internal static Dispatcher DISP
        {   get
            {
                if (_DISP == null)
                { _DISP = Application.Current.Dispatcher;
                }
                return _DISP;
            }
        } 




        #region ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  

        /// <summary>
        /// Entry Assembly
        /// </summary>
        public static Assembly EntryAssembly
        { get; } = Assembly.GetEntryAssembly();

        /// <summary>
        /// Entry Assembly Location if Entry Assembly exist in current context.
        /// </summary>
        public static string  EntryAssemblyLocation
        { get{
                if (EntryAssembly.IsNull()) return null;                 
                return  EntryAssembly.Location;
            }
        }



        static string _AppComponentName = "CRM"; 
        /// <summary>
        /// Your current App Key - we also means here this App as Component on the Apps Level of Enterprise System.         
        /// By default it is 'CRM'
        /// </summary>
        public static string AppComponentName
        { get {
                return _AppComponentName;
              }
          set{
               if (value.IsNotNullOrEmpty() && _AppComponentName != value)
               {
                    _AppComponentName = value;
                     ClientAppRootDir = GetSpecFolder(SpecialFolder.CommonApplicationData, _AppComponentName);
                }
             } 
         }



        static string _ClientAppRootDir = ""; //= GetSpecFolder(SpecialFolder.CommonApplicationData, AppComponentName);

        /// <summary>
        /// ClientAppRootDir -  Root directory of [Enterprise.AppComponent] on Client. This value is highly recommended path to store/save application or its components inside OS.
        /// <para/> ClientAppRootDir wil be setted in the same time when AppComponentName property value will be setted/or changed
        /// <para/> Rule of Path of ClientAppRootDir on client  - [ENVAPPDIR]\[AppComponentName], like:  [c:\Userdata\CRM]         
        /// </summary>
        public static string ClientAppRootDir
        { get
            {
                return _ClientAppRootDir;
            }
            private set
            {
                _ClientAppRootDir = value;
            }
        } 

         

        /// <summary>
        /// Based on [ClientAppRootDir]  folder  of  current  Application.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="appSubDirectory"></param>
        /// <returns></returns>
        public static string GetLocalAppFolder(string appSubDirectory)
        {
            return Path.Combine(ClientAppRootDir, appSubDirectory);             
        }
        /// <summary>
        /// Get path of Client Application subfolder as:  [AppRoot]\Subfolder 
        /// </summary>
        /// <param name="subfolder"></param>
        /// <returns></returns>
        public static string GetAppSubfolder(SubfolderEn subfolder)
        {
            return GetLocalAppFolder(subfolder.S());

        }

        /// <summary>
        /// Path Combination of specialFolder and subPath.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="specialFolder"> It's System.Environment.SpecialFolder value-Key </param>
        /// <param name="subPath">Sub path after [specialFolder] value</param>
        /// <returns></returns>
        public static string GetSpecFolder(SpecialFolder specialFolder, string subPath)
        {           
            return Path.Combine(GetFolderPath(specialFolder), subPath);            
        }


       

        #endregion ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  



        #region -------------------------- OS REGISTRY CHECKS/ADD/SET/UPDATE ------------------------------
        /// <summary>
        /// Check Win Registry Key exist
        /// </summary>
        /// <param name="regPath"></param>
        /// <returns></returns>
        public static bool RegistryCheckKeyExistPS(string regPath, string checkStringPathValue)
        {
            try
            {
                var readScriptResult = Registry.CurrentUser.OpenSubKey(regPath);
                if (readScriptResult.IsNull()) return false;

                var paramsInRegkey = readScriptResult.GetValueNames();
                return (paramsInRegkey.IsNotNull() && paramsInRegkey.Contains(checkStringPathValue));

            }
            catch (Exception exc)
            {                
                Logger.ErrorDbg(exc.Message);
                return false;
            }
        }


        #endregion -------------------------- OS REGISTRY CHECKS/ADD/SET/UPDATE ------------------------------



        #region ------------------------- Install App to OS Startup -------------------------------




        /// <summary>
        /// Check if the Application Entry doesn't exist then -write it,but If App Entry already exist  in registry - then compare it's value with current EntryAssemblyLocation and update if values are different.
        /// </summary>
        public static void CheckExistAddUpdateToOSStartup(RegistryKey registryLeaf )
        {
            var regpath = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
            if ( RegistryCheckKeyExistPS(regpath, AppComponentName) == false)
            {
                var key = registryLeaf.OpenSubKey(regpath, true);
                key.SetValue(AppComponentName, EntryAssemblyLocation);
            }
            else
            {
                //if already exist - then compare values if is the same then break? else update to currentValue
                var key = registryLeaf.OpenSubKey(regpath, true);
                var valueInKey = key.GetValue(AppComponentName);
                if ((valueInKey as string) != EntryAssemblyLocation)
                {
                    key.SetValue(AppComponentName, EntryAssemblyLocation);
                }
            }

        }

        #endregion ------------------------- Install App to OS Startup -------------------------------




        #region ----------------------- USERNAME && USERDOMAIN -----------------------------


        /// <summary>
        /// UserName from Current Windows Account from Network settings.
        /// <para/>  Property shortcut to  [WScriptNetwork.UserName]
        /// </summary>
        public static string UserName
        {
            get
            {
                return System.Environment.UserName;
            }
        }

        /// <summary>
        /// UserDomain info of Current SL Application from Network settings.
        /// <para/>  Property shortcut to  [WScriptNetwork.UserDomain]
        /// </summary>
        public static string UserDomain
        {
            get
            {
                return System.Environment.UserDomainName;
            }
        }


        #endregion ----------------------- USERNAME && USERDOMAIN -----------------------------



#elif WP81

        /// <summary>
        /// StackTrace value   - null value  
        /// </summary>
        public static string StackTrace
        {
            get { return null; }
        }



        private static bool? isInDesignMode;
        /// <summary>
        /// IS in Design Mode.
        /// </summary>
        public static bool IsInDesignMode
        {
            get
            {
                if (!isInDesignMode.HasValue)
                {
                    isInDesignMode = DesignerProperties.IsInDesignTool;
                }
                return isInDesignMode.Value;
            }
        }


        /// <summary>
        /// UI  Thread Dispatcher to Report Download  Progress Info
        /// </summary>
        internal static Dispatcher DISP
        { get; } = Deployment.Current.Dispatcher;





        #region ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  

        static string _AppComponentName = "CRM";
        /// <summary>
        /// Your current App Key - we also means here this App as Component on the Apps Level of Enterprise System.         
        /// By default it is 'CRM'
        /// </summary>
        public static string AppComponentName
        {
            get
            {
                return _AppComponentName;
            }
            set
            {
                if (value.IsNotNullOrEmpty() && _AppComponentName != value)
                {
                    _AppComponentName = value;
                    ClientAppRootDir = GetSpecFolder(SpecialFolder.ApplicationData, _AppComponentName);
                }
            }
        }



        /// <summary>
        /// ClientAppRootDir -  Root directory of [Enterprise.AppComponent] on Client. This value is highly recommended path to store/save application or its components inside OS.
        /// <para/> ClientAppRootDir wil be setted in the same time when AppComponentName property value will be setted/or changed
        /// <para/> Rule of Path of ClientAppRootDir on client  - [ENVAPPDIR]\[AppComponentName], like:  [c:\Userdata\CRM]         
        /// </summary>
        public static string ClientAppRootDir
        { get; private set; } = GetSpecFolder(SpecialFolder.ApplicationData, AppComponentName);

        



        /// <summary>
        /// Path Combination of specialFolder and subPath.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="specialFolder"> It's System.Environment.SpecialFolder value-Key </param>
        /// <param name="subPath">Sub path after [specialFolder] value</param>
        /// <returns></returns>
        public static string GetSpecFolder(SpecialFolder specialFolder, string subPath)
        {
            return OperationInvoke.CallInMode(nameof(Environment2), nameof(GetSpecFolder),
                  () =>
                  {
                      return Path.Combine(GetFolderPath(specialFolder), subPath);
                  }
                  );

        }


        /// <summary>
        /// Get path of Client Application subfolder as:  [AppRoot]\Subfolder 
        /// </summary>
        /// <param name="subfolder"></param>
        /// <returns></returns>
        public static string GetAppSubfolder(SubfolderEn subfolder)
        {
            return GetLocalAppFolder(subfolder.S());

        }



        /// <summary>
        /// Based on [ClientAppRootDir]  folder  of  current  Application.
        /// This operation works ONLY if App Has Elevated Permissions.
        /// </summary>
        /// <param name="appSubDirectory"></param>
        /// <returns></returns>
        public static string GetLocalAppFolder(string appSubDirectory)
        {
            return OperationInvoke.CallInMode(nameof(Environment2), nameof(GetLocalAppFolder)
            , () =>
            {
                return Path.Combine(ClientAppRootDir, appSubDirectory);
            });

        }
        #endregion ------------------------------- APPCOMPONENTNAME/ CLIENTAPPROOTDIR / SPEC FOLDERS -------------------------------  







#endif // END      SERVER  AND CLIENT+(WPF/SL5/WP81)



        ///////  ------- COMMON  FOR ALL CLIENTS - ONLY CLIENTS/ nor for SERVERS  ---------   
#if CLIENT  /// COMMON FOR ALL CLIENTS







#endif








    }
}


#region -------------------------------- GARBAGE -------------------------------


//static dynamic CreateCOMObject(string COMObjectName)
//{
//    try
//    { 
//        var comObjType = Type.GetTypeFromProgID(COMObjectName);
//        return Activator.CreateInstance(comObjType);  
//    }
//    catch (Exception exc)
//    {
//        throw new InvalidOperationException("Environment2.CreateCOMObject([{0}]):  ".Fmt(COMObjectName) + exc.Message);
//    }
//}


#endregion  -------------------------------- GARBAGE -------------------------------
