﻿using System.Collections.Generic;
using DDDD.Core.Extensions;

namespace DDDD.Core.Environment
{



    /// <summary>
    /// Application instance Corporation Production Consts
    /// </summary>
    public class EnterpriseAppsConsts
    {
        static readonly object locker = new object();

        /// <summary>
        /// Application instance Corporation Production Consts.
        /// </summary>
        public static Dictionary<string, string> Consts
        { get; } = new Dictionary<string, string>();



        /// <summary>
        /// Add All Consts
        /// </summary>
        /// <param name="consts"></param>
        public static void Init(params KeyValuePair<string, string>[] consts)
        {
            lock (locker)
            {
                foreach (var constItem in consts)
                {
                    if (!Consts.ContainsKey(constItem.Key))
                    {
                        Consts.Add(constItem.Key, constItem.Value);
                    }
                }
            }
        }


        #region ----------------------------------- ENTERPRISE CONSTS ------------------------------

        public const string FirmName = "Rovese RUS";
        public const string FirmSite = "http://www.rovese.com/";

        public const string OldFirmName = "Cersanit RUS";
        public const string OldFirmSite = "http://cersanit.ru/";

        public const string FirstSubsystem = "CRM";

        public const string ServiceContractNamespace = "cersanit.crm.com"; // http://cersanit.crm.com/2011/08/
        public const string DataContractNamespace = "cersanit.crm.com";

        public const string ProductName = "CRM";
        public const string DomainModulesNameSpace = "CRM.Dom.Modules";
        
        /// <summary>
        /// This Method should set pseudo data- Enterprise consts, as example. 
        /// </summary>
        public static void InitDefaults()
        {
            Init(nameof(FirmName).KVPair(FirmName)
                  , nameof(FirmSite).KVPair(FirmSite)
                  , nameof(OldFirmName).KVPair(OldFirmName)
                  , nameof(OldFirmSite).KVPair(OldFirmSite)
                  , nameof(FirstSubsystem).KVPair(FirstSubsystem)


                  , nameof(ServiceContractNamespace).KVPair(ServiceContractNamespace)
                  , nameof(DataContractNamespace).KVPair(DataContractNamespace)
                  , nameof(ServiceContractNamespace).KVPair(ServiceContractNamespace)
                  , nameof(ProductName).KVPair(ProductName)
                  , nameof(DomainModulesNameSpace).KVPair(DomainModulesNameSpace)

                );
        }

        #endregion ----------------------------------- ENTERPRISE CONSTS ------------------------------





    }

}
