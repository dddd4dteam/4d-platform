﻿#if SERVER && IIS

using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Net.ServiceModel
{

    public class DCServiceHost :  ServiceHost //ServiceHost
    {
        public DCServiceHost(Type publishingManager, Uri[] baseAdresses)
            : base(typeof(DCService), baseAdresses)
        {
            PublishedManager = publishingManager;
        }

        public DCServiceHost(Type publishingManager, object singletonInstance, Uri[] baseAdresses)
            : base(singletonInstance, baseAdresses)
        {
            PublishedManager = publishingManager;
        }


        public Type PublishedManager
        {
            get; private set;
        }

    }


}

#endif