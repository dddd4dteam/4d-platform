﻿
#if SERVER && IIS

using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;
using DDDD.Core.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace DDDD.Core.Net.ServiceModel
{



    #region ------------------------------- COMMENTS ---------------------------

    // HOW TO USE DCServiceHostFactory   
    // -1 define a new ServiceRoute using the DCServiceHostFactory   and service implementation type.
    //  RouteTable.Routes.Add(new ServiceRoute("Service1", new DCServiceHostFactory(), typeof(Service1)));
    // - also Route can looks like this:
    // RouteTable.Routes.Add(new ServiceRoute("DCSModuleService2", new WebServiceHostFactory(), typeof(DCSModuleServiceImplementor)));
    // RouteTable.Routes.Add(new ServiceRoute("DynamicCommands2", new WebServiceHostFactory(), typeof(DCSModuleServiceImplementor))); 
    // RouteTable.Routes.Add("DynamicServices", new DynamicServiceRoute("DynamicCommands", null, new ServiceHostFactory(), typeof(DCSModuleServiceImplementor)));
    // - So end service call uri will be - http://hostname/appname/Service1
    //
    // -2 add the UrlRoutingModule to the web.config file
    // <system.webServer>
    //  <modules runAllManagedModulesForAllRequests = "true" >
    //    <add name="UrlRoutingModule" type="System.Web.Routing.UrlRoutingModule, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
    //  </modules>
    //  <handlers>
    //    <add name = "UrlRoutingHandler" preCondition="integratedMode" verb="*" path="UrlRouting.axd" />
    //  </handlers>
    //</system.webServer>
    //
    // -also Handler can looks like this:s
    //<add name="UrlRoutingHandler"
    //preCondition="integratedMode"
    //verb="*" path="UrlRouting.axd"
    //type="System.Web.HttpForbiddenHandler, 
    //System.Web, Version=4.0.0.0, Culture=neutral, 
    //PublicKeyToken=b03f5f7f11d50a3a" />

    #endregion ------------------------------- COMMENTS ---------------------------



    /// <summary>
    /// DC ServiceHost Factory - also implement IDCCommunicationUnitHostFactory, and registered as string.Wcf_EnvironmentActivationSvc.      
    /// </summary>
    [DCCUnitServerFactory(DCCScenarios.Wcf_EnvironmentActivationREST
        , initFactoryGlobalConfigMethodName: nameof(InitFactoryDefaultConfiguration)
        , initFactoryItemConfigMethodName: nameof(InitFactoryItemConfiguration)
        ) ]
    public class DCWebServiceHostFactory : WebServiceHostFactory, IDCCUnitServerFactory<WebServiceHost> 
    {

      

        #region -------------------------------- SINGLETON -------------------------------------

        public DCWebServiceHostFactory()
        {
        }


        static LazyAct<DCWebServiceHostFactory> LA_CurrentFactory =
               LazyAct<DCWebServiceHostFactory>.Create(
                   (args) => { return new DCWebServiceHostFactory(); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of DCWebApiControllerFactory  
        /// </summary>
        public static DCWebServiceHostFactory Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


        #endregion -------------------------------- SINGLETON -------------------------------------


        #region ------------------------------ IDCCUnitServerFactory -----------------------------

        /// <summary>
        /// This Factory Registered as CommunicationUnit Factory that can produce  [Wcf_EnvironmentActivationREST] Communication Units.
        /// <para/>  Communication Unit Factory is one the application Component types.
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios.WebApiDirectHandlers;
        
        #endregion ------------------------------ IDCCUnitServerFactory -----------------------------
        

        #region ------------------------------ IFactory --------------------------------

        /// <summary>
        /// Factory Product Type - result Type
        /// </summary> 
        public Type BaseProductType
        { get; } = typeof(DCWebService);



        public List<Type> ProductTypes
        { get; } = new List<Type>() { typeof(DCWebService) };



        /// <summary>
        ///  Creating Item by CreateServiceHost(serviceType , baseAdresses) 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object CreateItem(params KeyValuePair<string, object>[] parameters)
        {
            var serviceType = parameters.Where(kv => kv.Key == "serviceType").FirstOrDefault().Value;
            if (serviceType == null) throw new InvalidOperationException("CreateItem parameter [serviceType] was not found within parameters of CreateItem(...)");

            var baseAddresses = parameters.Where(kv => kv.Key == "baseAddresses").FirstOrDefault().Value;
            if (baseAddresses == null) throw new InvalidOperationException("CreateItem parameter [baseAddresses] was not found within parameters of CreateItem(...)");

            return CreateServiceHost(serviceType as Type, baseAddresses as Uri[]);
        }

        #endregion------------------------------ IFactory --------------------------------


        #region ------------------------------------- DC COMMUNICATION  UNIT FACTORY: CONFIGURATION ------------------------------------

        /*

  <system.serviceModel>   
    
    <behaviors>     

      <serviceBehaviors>

        <behavior name="DDDD.Core.Net.ServiceModel.Behaviour1">
          <serviceDebug includeExceptionDetailInFaults="true" />
          <serviceMetadata httpGetEnabled="true" httpsGetEnabled="false" />
          <serviceThrottling maxConcurrentCalls="5" maxConcurrentInstances="5" maxConcurrentSessions="5" />
          <!--<serviceAuthorization principalPermissionMode="UseAspNetRoles" />-->
        </behavior>
        
      </serviceBehaviors>
      
    </behaviors>
    
    <bindings>

      <customBinding>
        <binding name="DDDD.Core.Net.ServiceModel.CustomBinding1" receiveTimeout="00:00:30">
          <binaryMessageEncoding />
          <httpTransport transferMode="StreamedResponse" />
        </binding>
      </customBinding>

    </bindings>


    <services>

      <service name="DDDD.Core.Net.ServiceModel.DCService" behaviorConfiguration="DDDD.Core.Net.ServiceModel.Behaviour1">
       <endpoint address="" binding="customBinding" bindingConfiguration="DDDD.Core.Net.ServiceModel.CustomBinding1" contract="DDDD.Core.Net.ServiceModel.IDCService" />
       <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
      </service>

    </services>
    
                
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true">
      <serviceActivations>
        <add relativeAddress="~/wcf/ClientReport.svc" service="DDDD.Core.Net.ServiceModel.DCService" factory="DDDD.Core.Net.ServiceModel.DCServiceHostFactory" />       
      </serviceActivations>
    </serviceHostingEnvironment>
 
 </system.serviceModel>   

        */

        #endregion ------------------------------------- DC COMMUNICATION  UNIT FACTORY: CONFIGURATION ------------------------------------


        #region  -------------------------------- DC Communication Unit Factory ----------------------------------


        /// <summary>
        /// This DC CommunicationUnit Factory init web.cong  default configuration.
        /// </summary>
        /// <returns></returns>
        private static bool InitFactoryDefaultConfiguration()
        {

            bool wasConfigurationInited = false;
            //var config = WebConfigurationManager.OpenWebConfiguration("~");

            /*       

             <serviceBehaviors>
                  <behavior name="DDDD.Core.Net.ServiceModel.Behaviour1">
                  <serviceDebug includeExceptionDetailInFaults="true" />
                  <serviceMetadata httpGetEnabled="true" httpsGetEnabled="false" />
                  <serviceThrottling maxConcurrentCalls="5" maxConcurrentInstances="5" maxConcurrentSessions="5" />
                  <!--<serviceAuthorization principalPermissionMode="UseAspNetRoles" />-->
                  </behavior>
             </serviceBehaviors>

            */

            var serviceBehaviorConfig = "DDDD.Core.Net.ServiceModel.Behaviour1";
            var serviceBindingConfig = "DDDD.Core.Net.ServiceModel.CustomBinding1";
            var serviceImplementContract = typeof(DCService);
            var serviceEndpointContract = typeof(IDCService);

            NetConfiguration.TryAddServiceBehavior(serviceBehaviorConfig);



            /*                         
             <customBinding>
                <binding name="DDDD.Core.Net.ServiceModel.CustomBinding1" receiveTimeout="00:00:30">
                <binaryMessageEncoding />
                <httpTransport transferMode="StreamedResponse" />
                </binding>
             </customBinding>                         
            */
            NetConfiguration.TryAddCustomBinding(serviceBindingConfig, TimeSpan.FromSeconds(30));
          


            /*              
              <service name="DDDD.Core.Net.ServiceModel.DCService" behaviorConfiguration="DDDD.Core.Net.ServiceModel.Behaviour1">
                <endpoint address="" binding="customBinding" bindingConfiguration="DDDD.Core.Net.ServiceModel.CustomBinding1" contract="DDDD.Core.Net.ServiceModel.IDCService" />
                <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
              </service> 
            */
            NetConfiguration.TryAddService(
                  svcName: serviceImplementContract.FullName, svcBehaviorConfiguration: serviceBehaviorConfig
                , endpointContract: serviceEndpointContract
                , endpointBinding: "customBinding"
                , endpointBindingConfiguration: serviceBindingConfig
                , endpointBehaviorConfiguration: null
                , useMetaDataExchangeEndpoint: true);

            wasConfigurationInited = true;


            return wasConfigurationInited;
        }


        private static bool InitFactoryItemConfiguration(DCCUnit dccUnit)
        {
            bool wasConfigurationInited = false;
            ///  Changing Configuration file(web.config) -addding wcf service default configuration
            ///  NetConfigurationManager.TryAddDCServiceActivationInEnvironment(dccUnit);
                       

            return wasConfigurationInited; //  not need to resave Configuration
        }
        #endregion -------------------------------- DC Communication Unit Factory ----------------------------------



        #region -----------------------  SERVICE HOST FACTORY -------------------------------


        /// <summary>
        /// Creates a <see cref="T:System.ServiceModel.ServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="serviceType">Specifies the type of service to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array"/> of type <see cref="T:System.Uri"/> that contains the base addresses for the service hosted.</param>
        /// <returns>
        /// A <see cref="WebServiceHost"/> for the type of service specified with a specific base address.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Thrown if <paramref name="serviceType" /> or <paramref name="baseAddresses" /> is <see langword="null" />.
        /// </exception>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            if (baseAddresses == null)
                throw new ArgumentNullException("baseAddresses");

            return new WebServiceHost(serviceType, baseAddresses);                        
        }

        /// <summary>
        /// Creates a <see cref="WebServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="singletonInstance">Specifies the singleton service instance to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array"/> of type <see cref="T:System.Uri"/> that contains the base addresses for the service hosted.</param>
        /// <returns>
        /// A <see cref="WebServiceHost"/> for the singleton service instance specified with a specific base address.
        /// </returns>
        protected WebServiceHost CreateSingletonServiceHost(object singletonInstance, Uri[] baseAddresses)
        {
            if (singletonInstance == null)
                throw new ArgumentNullException(nameof(singletonInstance));

            if (baseAddresses == null)
                throw new ArgumentNullException(nameof(baseAddresses) );

            return new WebServiceHost(singletonInstance, baseAddresses); //DCServiceHost(PublishedManager, singletonInstance, baseAddresses);
        }


        /// <summary>
        /// Creates a <see cref="WebServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public WebServiceHost AddOrUseExistServiceT(DCCUnit communicationUnit)
        {
            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;

            return (WebServiceHost)CreateServiceHost(targetHandlerType,
              new[] { new Uri(communicationUnit.UriResultAddress) });
        }


        /// <summary>
        /// Creates a <see cref="ServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public object AddOrUseExistService(DCCUnit communicationUnit)
        {
            return AddOrUseExistServiceT(communicationUnit);
        }






        #endregion -----------------------  SERVICE HOST FACTORY -------------------------------


    }



}
#endif



#region --------------------------------- GARBAGE --------------------------------

//Standart Listen Endpoint
//webServiceHost.AddServiceEndpoint(typeof(IDCService), DCServiceStandartBinding, baseAddresses[0].AbsolutePath);
//webServiceHost.Description.
//webServiceHost.exdPoim Description.Behaviors.Add(new WebHttpBehavior());

// < behavior name = "CRM.Communication.Behaviour" >
//     < serviceThrottling maxConcurrentCalls = "5" maxConcurrentInstances = "5" maxConcurrentSessions = "5" />
//     < serviceDebug includeExceptionDetailInFaults = "true" />
//      < serviceMetadata httpGetEnabled = "true" httpsGetEnabled = "false" />
//</ behavior >            




//SERVICE DEBUG
//var debugBehavior = webServiceHost.Description.Behaviors.Find<ServiceDebugBehavior>();
//debugBehavior.HttpHelpPageEnabled = true;
//debugBehavior.IncludeExceptionDetailInFaults = true;

////SERVICE THROTTLING 
//var serviceThrottlingBehavior = webServiceHost.Description.Behaviors.Find<ServiceThrottlingBehavior>();
//if (serviceThrottlingBehavior == null)
//{
//    serviceThrottlingBehavior = new ServiceThrottlingBehavior() { MaxConcurrentCalls = 5, MaxConcurrentInstances = 5, MaxConcurrentSessions = 5 };
//    webServiceHost.Description.Behaviors.Add(serviceThrottlingBehavior);
//}
//else
//{
//    serviceThrottlingBehavior.MaxConcurrentCalls = 5;
//    serviceThrottlingBehavior.MaxConcurrentInstances = 5;
//    serviceThrottlingBehavior.MaxConcurrentSessions = 5;
//}


//// SERVICE METADATA 
//var serviceMetaDataBehavior = new ServiceMetadataBehavior();
//webServiceHost.Description.Behaviors.Add(serviceMetaDataBehavior);
//webServiceHost.AddServiceEndpoint(typeof(IMetadataExchange), MetadataExchangeBindings.CreateMexHttpBinding(), "mex");


#endregion --------------------------------- GARBAGE --------------------------------
