﻿
#if SERVER && IIS

using DDDD.Core.ComponentModel;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Extensions;

using DDDD.Core.Threading;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Web.Configuration;
using System.Xml;
using System.Xml.Linq;

namespace DDDD.Core.Net.ServiceModel
{


    // 3 Task(OPTIONAL) - you shold choose - Use  *.svc Markup file or only Web.config to register-activate new service:
    //   - If you  want to use *.svc Markup file  registration you'll be able to use MVC Controllers and Svc services together
    //   - If you want to use use only Web.config changing you may get errors because MVC Controller Handlers already making their routing registration
    //     	so you'll need to comment   RouteConfig.RegisterRoutes(RouteTable.Routes);   in your global-asax.cs in WebApplication.Application_Start()
    // 
    //  If you decide to use *.svc Markup file. Add Service Markup File to Web Application Project:  $ModuleName$.svc with the next code inside
    // <%@ ServiceHost Language="C#" Debug="true" Service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" %>
    //
    //
    // WEB.CONFIG  CHANGE STEPS 
    //
    // 4 Task- Add this Configuration block To the Web.Config in the Web Application 
    // ----  Configuration  BLOCK :  Here we beleave that we already have defined Behaviour and Binding for all Services earlier;
    // ----  Uncomment from the next Line and Add this Service Config tag to the -   Web.Config to <system.serviceModel>.<services>.<service ..  from next Line:
    // 
    // 
    // ADD TSS BEHAVIOR EXTENSION - adding TSS serialization as ServiceModel Behavior Extension     
    /*  
       <system.serviceModel>
       <extensions>      
          <behaviorExtensions>
            <add name="ddddtss"
                 type="DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69"/>
          </behaviorExtensions>      
        </extensions>
        <system.serviceModel> 
    */
    // 5 Task- So here you should to ensure in  type value-	DDDD.Core.Net.ServiceModel.TSSBehaviorExtension- get and it's Type.AssemblyQualifiedName.
    //  For this task you can use Test from  or you can use Threading_BlockTests.Test_BCL_Reflection_GetTSSServiceModelExtensionFullName_Tests() from DDDD.Core.Test.Net45 project 
    // or with console tool
    //
    //	
    // NEXT STEP: ADD BEHAVIOR CONFIG: - shared custom binding  that will be used by Service configuration
    /* 
     <system.serviceModel>
        <behaviors>

        <endpointBehaviors>
         <behavior name="DDDD.ServiceModel.TSS.EndpointBehavior">
          <ddddtss/>
         </behavior>
        </endpointBehaviors>	

        <serviceBehaviors>	   
          <behavior name="$Subsystem$.Communication.Behaviour">
          <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
          <serviceDebug includeExceptionDetailInFaults="true" />
          <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
          </behavior>
        </serviceBehaviors>

        </behaviors>
    <system.serviceModel> 
    */
    //
    //
    //NEXT STEP:  ADD BINDING CONFIG: - shared custom binding  that will be used by Service configuration
    /*
     <system.serviceModel>
       <bindings>
            <customBinding>
                <binding name="$Subsystem$.Communication.customBinding"  receiveTimeout="00:00:30" >
                <binaryMessageEncoding />
                <httpTransport  transferMode="StreamedResponse" />          
                </binding>
            </customBinding>
        </bindings>
    <system.serviceModel>
     */
    //
    //
    //NEXT STEP:  ADD SERVICE CONFIG: 
    /*  - Usual Service - Without using TSS
     <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
      <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                            contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" />
      <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
     </service>
    */
    // OR
    /*  Service as Below - Service that is going to use TSS serialization
     <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
      <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                            contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" 
                            behaviorConfiguration="TSS.ServiceModel.EndpointBehavior" />						
      <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
     </service>
    */
    //
    //5 (OPTIONAL) If you've decided to use  web.config only registration and activation(instead of *.svc Markup file)  
    //    you'll need to expand form of serviceHostingEnvironment section(like below) and add line with current service activation :
    /*
     <serviceHostingEnvironment aspNetCompatibilityEnabled="true"   multipleSiteBindingsEnabled="true" >
         <serviceActivations>
           <!--YOUR SERVICE ACTIVATION LINE-->
           <add relativeAddress="~/Services/$ModuleName$Service.svc" service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" />                  
         </serviceActivations>
     </serviceHostingEnvironment>
    */
    // Here end test address is - http://localhost/DDDD.Core.Test.Web/Services/ClientReportService.svc
    // Here we have folder in form like-[Project\SubFolder\] -  [DDDD.Core.Test.Web\Services\]   physical dirs . Full path to project example -D:\DDDD\BCL\DDDD.Core.Test.Web\
    // And Here [DDDD.Core.Test.Web] dir also set as local IIS WebApplication 
    // STEPS FINISHED





    // HOW TO USE DCServiceHostFactory   
    // -1 define a new ServiceRoute using the DCServiceHostFactory   and service implementation type.
    //  RouteTable.Routes.Add(new ServiceRoute("Service1", new DCServiceHostFactory(), typeof(Service1)));
    // - also Route can looks like this:
    // RouteTable.Routes.Add(new ServiceRoute("DCSModuleService2", new WebServiceHostFactory(), typeof(DCSModuleServiceImplementor)));
    // RouteTable.Routes.Add(new ServiceRoute("DynamicCommands2", new WebServiceHostFactory(), typeof(DCSModuleServiceImplementor))); 
    // RouteTable.Routes.Add("DynamicServices", new DynamicServiceRoute("DynamicCommands", null, new ServiceHostFactory(), typeof(DCSModuleServiceImplementor)));
    // - So end service call uri will be - http://hostname/appname/Service1
    //
    // -2 add the UrlRoutingModule to the web.config file
    // <system.webServer>
    //  <modules runAllManagedModulesForAllRequests = "true" >
    //    <add name="UrlRoutingModule" type="System.Web.Routing.UrlRoutingModule, System.Web, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" />
    //  </modules>
    //  <handlers>
    //    <add name = "UrlRoutingHandler" preCondition="integratedMode" verb="*" path="UrlRouting.axd" />
    //  </handlers>
    //</system.webServer>
    //
    // -also Handler can looks like this:s
    //<add name="UrlRoutingHandler"
    //preCondition="integratedMode"
    //verb="*" path="UrlRouting.axd"
    //type="System.Web.HttpForbiddenHandler, 
    //System.Web, Version=4.0.0.0, Culture=neutral, 
    //PublicKeyToken=b03f5f7f11d50a3a" />


    /// <summary>
    /// DC ServiceHost Factory - also implement IDCCommunicationUnitHostFactory, and registered as string.Wcf_EnvironmentActivationSvc.      
    /// </summary>
    [DCCUnitServerFactory( DCCScenarios.Wcf_EnvironmentActivationSvc
                           , initFactoryGlobalConfigMethodName: nameof(InitFactoryDefaultConfiguration)
                           , initFactoryItemConfigMethodName: nameof(InitFactoryItemConfiguration)
        ) ]
    public class DCServiceHostFactory : ServiceHostFactory, IDCCUnitServerFactory<ServiceHost> 
    {
          


        #region -------------------------------- SINGLETON -------------------------------------
        
        public DCServiceHostFactory()
        {
        }
        

        static LazyAct<DCServiceHostFactory> LA_CurrentFactory =
               LazyAct<DCServiceHostFactory>.Create(
                   (args) => { return new DCServiceHostFactory(); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of DCWebApiControllerFactory  
        /// </summary>
        public static DCServiceHostFactory Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


        #endregion -------------------------------- SINGLETON -------------------------------------



        /// <summary>
        /// This Factory Registered as CommunicationUnit Factory that can produce  [Wcf_EnvironmentActivationSvc] Communication Units.
        /// <para/>  Communication Unit Factory is one the application Component types.
        /// </summary>
        public string ScenarioKey
        { get; } = DCCScenarios.Wcf_EnvironmentActivationSvc;


        ///// <summary>
        ///// Factory Product  Type
        ///// </summary>
        //public Type ProductType
        //{ get; } = typeof (ServiceHost);

        /// <summary>
        /// Factory Product  Type
        /// </summary>
        public Type BaseProductType
        { get; } = typeof(ServiceHost);



        public List<Type> ProductTypes
        { get; } = new List<Type>() { typeof(ServiceHost) };



        #region ------------------------------------- DC COMMUNICATION  UNIT FACTORY: CONFIGURATION ------------------------------------

        /*

  <system.serviceModel>   
    
    <behaviors>     

      <serviceBehaviors>

        <behavior name="DDDD.Core.Net.ServiceModel.Behaviour1">
          <serviceDebug includeExceptionDetailInFaults="true" />
          <serviceMetadata httpGetEnabled="true" httpsGetEnabled="false" />
          <serviceThrottling maxConcurrentCalls="5" maxConcurrentInstances="5" maxConcurrentSessions="5" />
          <!--<serviceAuthorization principalPermissionMode="UseAspNetRoles" />-->
        </behavior>
        
      </serviceBehaviors>
      
    </behaviors>
    
    <bindings>

      <customBinding>
        <binding name="DDDD.Core.Net.ServiceModel.CustomBinding1" receiveTimeout="00:00:30">
          <binaryMessageEncoding />
          <httpTransport transferMode="StreamedResponse" />
        </binding>
      </customBinding>

    </bindings>


    <services>

      <service name="DDDD.Core.Net.ServiceModel.DCService" behaviorConfiguration="DDDD.Core.Net.ServiceModel.Behaviour1">
       <endpoint address="" binding="customBinding" bindingConfiguration="DDDD.Core.Net.ServiceModel.CustomBinding1" contract="DDDD.Core.Net.ServiceModel.IDCService" />
       <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
      </service>

    </services>
    
                
    <serviceHostingEnvironment aspNetCompatibilityEnabled="true" multipleSiteBindingsEnabled="true">
      <serviceActivations>
        <add relativeAddress="~/wcf/ClientReport.svc" service="DDDD.Core.Net.ServiceModel.DCService" factory="DDDD.Core.Net.ServiceModel.DCServiceHostFactory" />       
      </serviceActivations>
    </serviceHostingEnvironment>
 
 </system.serviceModel>   

        */


        /// <summary>
        /// This DC CommunicationUnit Factory init web.cong  default configuration.
        /// </summary>
        /// <returns></returns>
        private static bool InitFactoryDefaultConfiguration()
        {

            bool wasConfigurationInited = false;

            // Add Global Known Data Contracts
            DCCUnit.AddKnowDataContracts(typeof(DCMessage));
            
            // Build Configuration
            /*       
             <serviceBehaviors>
                  <behavior name="DDDD.Core.Net.ServiceModel.Behaviour1">
                  <serviceDebug includeExceptionDetailInFaults="true" />
                  <serviceMetadata httpGetEnabled="true" httpsGetEnabled="false" />
                  <serviceThrottling maxConcurrentCalls="5" maxConcurrentInstances="5" maxConcurrentSessions="5" />
                  <!--<serviceAuthorization principalPermissionMode="UseAspNetRoles" />-->
                  </behavior>
             </serviceBehaviors>
            */

            var serviceBehaviorConfig = "DDDD.Core.Net.ServiceModel.Behaviour1";
            var serviceBindingConfig = "DDDD.Core.Net.ServiceModel.CustomBinding1";
            var serviceImplementContract = typeof(DCService);
            var serviceEndpointContract = typeof(IDCService);

            NetConfiguration.TryAddServiceBehavior(serviceBehaviorConfig);


            /*             
            
             <customBinding>
                <binding name="DDDD.Core.Net.ServiceModel.CustomBinding1" receiveTimeout="00:00:30">
                <binaryMessageEncoding />
                <httpTransport transferMode="StreamedResponse" />
                </binding>
             </customBinding>             
            
            */

            NetConfiguration.TryAddCustomBinding(serviceBindingConfig, TimeSpan.FromSeconds(30));

            /*
              
              <service name="DDDD.Core.Net.ServiceModel.DCService" behaviorConfiguration="DDDD.Core.Net.ServiceModel.Behaviour1">
                <endpoint address="" binding="customBinding" bindingConfiguration="DDDD.Core.Net.ServiceModel.CustomBinding1" contract="DDDD.Core.Net.ServiceModel.IDCService" />
                <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
              </service> 

            */

            NetConfiguration.TryAddService(
                svcName: serviceImplementContract.FullName, svcBehaviorConfiguration: serviceBehaviorConfig
              , endpointContract: serviceEndpointContract
              , endpointBinding: "customBinding"
              , endpointBindingConfiguration: serviceBindingConfig
              , endpointBehaviorConfiguration: null
              , useMetaDataExchangeEndpoint: true);
            wasConfigurationInited = true;

            return wasConfigurationInited;
        }




        private static bool InitFactoryItemConfiguration(DCCUnit dccUnit)
        {
            // Changing Configuration file(web.config) -addding wcf service default configuration
            NetConfiguration.TryAddDCServiceActivationInEnvironment(dccUnit);

            return false;// not try resave Configuration
        }

        #endregion ------------------------------------- DC COMMUNICATION  UNIT FACTORY: CONFIGURATION ------------------------------------


        


        #region -----------------------  SERVICE HOST FACTORY -------------------------------


        /// <summary>
        /// Creates a <see cref="T:System.ServiceModel.ServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="serviceType">Specifies the type of service to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array"/> of type <see cref="T:System.Uri"/> that contains the base addresses for the service hosted.</param>
        /// <returns>
        /// A <see cref="WebServiceHost"/> for the type of service specified with a specific base address.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// Thrown if <paramref name="serviceType" /> or <paramref name="baseAddresses" /> is <see langword="null" />.
        /// </exception>
        protected override ServiceHost CreateServiceHost(Type serviceType, Uri[] baseAddresses)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            if (baseAddresses == null)
                throw new ArgumentNullException("baseAddresses");

            var webServiceHost = new ServiceHost(serviceType, baseAddresses); 
                      
            return webServiceHost;
        }

        /// <summary>
        /// Creates a <see cref="T:System.ServiceModel.ServiceHost"/> for a specified type of service with a specific base address.
        /// </summary>
        /// <param name="singletonInstance">Specifies the singleton service instance to host.</param>
        /// <param name="baseAddresses">The <see cref="T:System.Array"/> of type <see cref="T:System.Uri"/> that contains the base addresses for the service hosted.</param>
        /// <returns>
        /// A <see cref="WebServiceHost"/> for the singleton service instance specified with a specific base address.
        /// </returns>
        protected ServiceHost CreateSingletonServiceHost(object singletonInstance, Uri[] baseAddresses)
        {
            if (singletonInstance == null)
                throw new ArgumentNullException("singletonInstance");

            if (baseAddresses == null)
                throw new ArgumentNullException("baseAddresses");

            return new ServiceHost(singletonInstance, baseAddresses); 
        }





        #endregion -----------------------  SERVICE HOST FACTORY -------------------------------


        #region -------------------------- IDCCUnitServerFactory<ServiceHost>  -----------------------------



        /// <summary>
        ///  Creating Item by CreateServiceHost(serviceType , baseAdresses) 
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object CreateItem(params KeyValuePair<string, object>[] parameters)
        {
            var serviceType = parameters.Where(kv => kv.Key == "serviceType").FirstOrDefault().Value;
            if (serviceType == null) throw new InvalidOperationException("CreateItem parameter [serviceType] was not found within parameters of CreateItem(...)");

            var baseAddresses = parameters.Where(kv => kv.Key == "baseAddresses").FirstOrDefault().Value;
            if (baseAddresses == null) throw new InvalidOperationException("CreateItem parameter [baseAddresses] was not found within parameters of CreateItem(...)");

            return CreateServiceHost(serviceType as Type, baseAddresses as Uri[]);
        }


        /// <summary>
        /// Creating and Add Item   
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public ServiceHost AddOrUseExistServiceT(DCCUnit communicationUnit)
        {

            var targetHandlerType = (communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType.IsNull()) ? Current.BaseProductType : communicationUnit.RegistrationInfo.CustomDCCUnitHandlerType;
           

            return CreateServiceHost(targetHandlerType,
                   new[] { new Uri(communicationUnit.UriResultAddress) });
        }


        /// <summary>
        /// Creating and Add Item
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        public object AddOrUseExistService(DCCUnit communicationUnit)
        {
            return AddOrUseExistServiceT(communicationUnit);
        }
        #endregion -------------------------- IDCCUnitServerFactory<ServiceHost>  -----------------------------



    }



}
#endif


