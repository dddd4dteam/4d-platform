﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;

using System.Text;
using System.Threading.Tasks;
using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.ComponentModel;
using System.Reflection;




#if SERVER && IIS
using System.ServiceModel.Activation;

#endif

namespace DDDD.Core.Net.ServiceModel
{





    #region  -------------------------- SERVICE KNOWN TYPES(both for SERVER & CLIENT)  --------------------------------

    ///// <summary>
    ///// Known Types
    ///// </summary>
    //public static class ShellServiceModelKnownTypesHelper
    //{

    //    /// <summary>
    //    /// This class has the method named GetKnownTypes that returns a generic IEnumerable. 
    //    /// </summary>
    //    /// <param name="provider"></param>
    //    /// <returns></returns>
    //    public static IEnumerable<Type> GetShellKnownTypes(ICustomAttributeProvider provider)
    //    {
    //        return DCCUnit.GetKnownDataContracts(); //SystemDispatcher.Instance.GetServiceClientContractsFromSystem(ServiceModelManager_RegisterEn.Shell);
    //    }

    //}

    #endregion  -------------------------- SERVICE KNOWN TYPES(both for SERVER & CLIENT)  --------------------------------





#if SERVER || (CLIENT && WPF)

      
        /// <summary>
        /// Server side WCF's service. 
        /// Also this interface is ServiceContract that use DCMessage infrastructure. 
        /// </summary>    
        [ServiceKnownType(nameof(DCCUnit.GetKnownDataContracts), typeof(DCCUnit))]
        [ServiceContract(Name = DCSConsts.ServiceNameDefault, Namespace = DCSConsts.ServiceNamespaceDefault)]
        public interface IDCService
        {

            /// <summary>
            ///  Overload for SL5 wcf client
            /// </summary>
            /// <param name="ClientCommand"></param>
            /// <returns></returns>
            [OperationContract(Action = DCSConsts.ActionSL5Uri, ReplyAction  = DCSConsts.ReplyActionSL5Uri)]
            IDCMessage ExecuteCommandSL5(IDCMessage ClientCommand);




        /// <summary>
        /// Processing Command Message on Server side:		
        /// <para/> Process DCMessage by your IDCService's Owner Component .  DCMessage should be processed by DCMessage.TargetProcessingManagerID .
        /// <para/> If client need to see if  result is successful /or not message, 
        ///          then add such message into DCMessage.ProcessingFaultMessage/ DCMessage.ProcessingSuccessMessage with DCMessage.ErrorCode if your system require.        
        /// <para/>  On client full faulted exception message should be showned only in debug Mode/ in release  user should see only standart-phrase message. 	
        /// <para/> Clear all input parameters - to save traffic when it'll be transmitted back. 
        /// <para/> Set diagnostics timestamps on received and sending back to client points.
        /// <para/> Send back DCMessage to Client.
        /// </summary>
        /// <param name="ClientCommand">DCMessage from Client side</param>
        /// <returns>Send back processed DCMessage to Client.</returns>
        [Description(" Processing Command Message on Server side:"
                                    + "<para/> rocess DCMessage by your IDCService's Owner Component .  DCMessage should be processed by DCMessage.TargetProcessingManagerID ."
                                    + "<para/> If client need to see if  result is successful /or not message,"
                                    + "   /r/n      then add such message into DCMessage.ProcessingFaultMessage/ DCMessage.ProcessingSuccessMessage with DCMessage.ErrorCode if your system require."
                                    + "<para/>  On client full faulted exception message should be showned only in debug Mode/ in release  user should see only standart-phrase message."
                                    + "<para/> Clear all input parameters - to save traffic when it'll be transmitted back."
                                    + "<para/> Set diagnostics timestamps on received and sending back to client points."
                                    + "<para/> Send back DCMessage to Client.")]           
            [OperationContract(Action = DCSConsts.ActionUri, ReplyAction = DCSConsts.ReplyActionUri)]
            Task<IDCMessage> ExecuteCommandAsync(IDCMessage ClientCommand);

        }


#elif CLIENT && (SL5 || WP81)

    /// <summary>
    ///  Client side WCF's proxy.
    /// Also this interface is ServiceContract that use DCMessage infrastructure.       
    /// </summary>      
    [ServiceContract(Name = DCSConsts.ServiceNameDefault, Namespace = DCSConsts.ServiceNamespaceDefault)]
        public interface IDCService
        {
            /// <summary>
            /// Send Command Message to Server:
            /// <para/> Build  DCMessage by your client,on client side by your CommandManagersHub infrastructure.
            /// <para/> Set diagnostics timestamps on sending to server points.
            /// <para/> Send  DCMessage to Server.
            /// </summary>
            /// <param name="ClientCommand">Client side DCMessage</param>
            /// <param name="callback">Action on server return processed DCMessage</param>
            /// <param name="asyncState">Thread State object</param>
            /// <returns></returns>
            [ServiceKnownType(nameof(DCCUnit.GetKnownDataContracts), typeof(DCCUnit))]
            [OperationContract(AsyncPattern = true, Action = DCSConsts.ActionSL5Uri, ReplyAction = DCSConsts.ReplyActionSL5Uri)]                    
            IAsyncResult BeginExecuteCommandSL5(IDCMessage ClientCommand, AsyncCallback callback, object asyncState);

            IDCMessage EndExecuteCommandSL5(IAsyncResult result);//DCMessage

       }

   


#endif


}




