﻿#if SERVER && NET45 


using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Reflection;
using DDDD.Core.Serialization;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace DDDD.Core.Net.ServiceModel
{


    /// <summary>
    /// Describes a WCF operation behaviour that can perform Type Set Serialization
    /// </summary>
    public sealed class TSSOperationBehavior : DataContractSerializerOperationBehavior
    {
        ITypeSetSerializer _serializer;

        public ITypeSetSerializer Serializer
        {
            get { return _serializer; }
            set
            {
                if (value == null) throw new ArgumentNullException("Serializer can't be setted by null value");
                _serializer = value;
            }
        }


        /// <summary>
        /// Create a new ProtoOperationBehavior instance
        /// </summary>
        public TSSOperationBehavior(OperationDescription operation)
            : base(operation)
        {
            var ServiceContract = (ServiceContractAttribute)operation.DeclaringContract.ContractType.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault();// Name;// .Get // DeclaringContract

            var ServiceKey = ServiceContract.Name;

            _serializer = TypeSetSerializer.AddOrUseExistTSS(operation.DeclaringContract.Name);
            //_serializer.AddKnownType<CommandMessage>();
            //TypeAQName.Mapper.UseConventionsProcessing = true;
            //TypeAQName.Mapper.UseTypeFullNameOnlyCompareMapping = true;

        }
        
     
        /// <summary>
        /// Creates a TSSerializer if possible (falling back to the default WCF serializer)
        /// </summary>
        public override XmlObjectSerializer CreateSerializer(Type type, System.Xml.XmlDictionaryString name, System.Xml.XmlDictionaryString ns, IList<Type> knownTypes)
        {
            if (_serializer == null) throw new InvalidOperationException("No Model instance has been assigned to the TSSOperationBehavior");
            return XmlTSSerializer.TryCreate(_serializer, type) ?? base.CreateSerializer(type, name, ns, knownTypes);
            
        }
    }
}

#endif
