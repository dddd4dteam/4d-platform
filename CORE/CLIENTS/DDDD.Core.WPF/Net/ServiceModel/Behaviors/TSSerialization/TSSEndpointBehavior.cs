﻿#if SERVER && NET45 

using System.Linq;
using System.ServiceModel.Description;

namespace DDDD.Core.Net.ServiceModel
{
    /// <summary>
    /// Please use snippet with more comments - DynamicCommandService_TSS.snippet
    /// <para/> Behavior to swap out DatatContractSerilaizer with the XmlTSSerializer for a given endpoint.
    /// <example>
    /// Add the following to the server and client app.config in the system.serviceModel section:
    ///  <extensions>
    ///  <behaviorExtensions>
    ///  <add name="ddddtss"
    ///       type="DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.1.0.0.1, Version=1.0.0.1, Culture=neutral, PublicKeyToken=e364950485695b69"/>
    ///  </behaviorExtensions>
    ///  </extensions>
    ///  <behaviors>
    ///  <endpointBehaviors>
    ///     <behavior name="DDDD.ServiceModel.TSS.EndpointBehavior">
    ///        <ddddtss/>
    ///     </behavior>
    ///  </endpointBehaviors>
    ///  </behaviors>
    ///  
    /// 
    /// Configure your endpoints to have a behaviorConfiguration as follows:
    /// 
    ///  <service name="TK.Framework.Samples.ServiceModel.Contract.SampleService">
    ///    <endpoint address="http://myhost:9003/SampleService" binding="basicHttpBinding" 
    ///     behaviorConfiguration="DDDD.ServiceModel.TSS.EndpointBehavior"
    ///     bindingConfiguration="basicHttpBindingConfig" 
    ///     name="basicHttpTSS" contract="ISampleServiceContract" />
    ///  </service>
    ///  
    ///  <client>
    ///       <endpoint address="http://localhost:61272/SampleService" 
    ///       binding="basicHttpBinding"
    ///       bindingConfiguration="basicHttpBindingConfig" contract="ISampleServiceContract"
    ///       name="BasicHttpTSSEndpoint" 
    ///       behaviorConfiguration="DDDD.ServiceModel.TSS.EndpointBehavior"/>
    ///   </client>
    ///   
    /// </example>
    /// </summary>
    public class TSSEndpointBehavior : IEndpointBehavior
    {
#region IEndpointBehavior Members

        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            ReplaceDataContractSerializerOperationBehavior(endpoint);
        }

        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {
            ReplaceDataContractSerializerOperationBehavior(endpoint);
        }

        void IEndpointBehavior.Validate(ServiceEndpoint endpoint)
        {
        }

        private static void ReplaceDataContractSerializerOperationBehavior(ServiceEndpoint serviceEndpoint)
        {
            foreach (OperationDescription operationDescription in serviceEndpoint.Contract.Operations)
            {
                ReplaceDataContractSerializerOperationBehavior(operationDescription);
            }
        }


        private static void ReplaceDataContractSerializerOperationBehavior(OperationDescription description)
        {
            //DataContractSerializerOperationBehavior dcsOperationBehavior = description.Behaviors.Find<DataContractSerializerOperationBehavior>();
            
            //if (dcsOperationBehavior != null)
            //{
            //    description.Behaviors.Remove(dcsOperationBehavior);
            //    TSSOperationBehavior newBehavior = new TSSOperationBehavior(description);
            //    newBehavior.MaxItemsInObjectGraph = dcsOperationBehavior.MaxItemsInObjectGraph;
            //    description.Behaviors.Add(newBehavior);
            //}

            var dataContractBehaviors = description.Behaviors.Where(bhvr => bhvr.GetType().Name.Contains("DataContractSerializerOperation"));
            
            TSSOperationBehavior newTssBehavior = new TSSOperationBehavior(description);

            if (dataContractBehaviors != null && dataContractBehaviors.Count() > 0)
            {
                while (dataContractBehaviors.Count() > 0)
                {
                    var bhvr = dataContractBehaviors.First();
                    if (bhvr is DataContractSerializerOperationBehavior)
                    {
                        newTssBehavior.MaxItemsInObjectGraph = (bhvr as DataContractSerializerOperationBehavior).MaxItemsInObjectGraph;
                    }
                    description.Behaviors.Remove(bhvr);
                }
            }
            description.Behaviors.Add(newTssBehavior);
        }


#endregion
    }
}


#endif
