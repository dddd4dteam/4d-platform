﻿
using System;
using System.IO;
using System.Net;

using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Collections.Generic;

using DDDD.Core.Extensions;

#if SL5 || WP81
using DDDD.Core.Collections;
#elif NET45
using System.Collections.Specialized;
#endif

namespace DDDD.Core.Net.Files.Download
{

    // TASK: GetCOMPONENT -  TO GET TargetFileName from URI
    // -TASK: DIAGNOSTICS- LOGGING/TRACING IN DEBUG DURING WHOLE DOWNLOAD PROCESS  
    // -TASK: Exception MESSAGE in PROGRESS DATA
    // -?TASK: TargetFileName from TargetSaveFilePath - by extension - we get from file Uri
    // TASK: DOWNLOAD FILE INTO MEMORYSTREAM 
    // TASK: STOP/CANCEL CLIENT DOWNLOADING
    // TASK: CHECK NETWORK IS ENABLE BEFORE START LOADING
    // TASK: CHECK FILE ALREADY LOCKED AND SKIP NEW STARTING UNTIL IT FINISHED- not QUEUING QUERY


    // TASK: PUBLIC SET Credentials to DOWNLOAD by NETWORK PROXY ALSO IN PCL CLIENTS( SILVERLIGHT -slim proxy ...)
    // TASK: PUBLIC SET  API for Task Creation Option - Long or Attached Task Creation
    // -TASK: PUBLIC SET  API for REQUEST HEADERS to REQUEST
    // -TASK: PUBLIC SET  API for Add REQUEST PARAMETERS to QUERY/?
    // -TASK: PUBLIC SET  API for Title for Downloading Item Task in PROGRESS 


    /// <summary>
    /// Download Client - can be used for [load-save on disk] and [load-save in memory] operations as well as for small and big data files by Http.  
    /// </summary>
    public class DownloadClient : IDisposable
    {

        #region ------------------------ CTOR ------------------------

        DownloadClient()
        {
            Progress = new DownloadItemProgressInfo();
        }

        #endregion ------------------------ CTOR ------------------------


        #region ---------------------------- NESTED CLASSES ------------------------------


        internal void Finalize()
        {
            UIStartAction = null;
            UIProgressAction = null;
            UICompletedAction = null;

            if (ReadStream != null)
            {
                ReadStream.Close();
            }

            if (Progress.DownloadTarget == DownloadTargetEn.DownloadToFile)
            {
                WriteStream.Close();
                if (Progress.Succeeded == false)
                {
                    // Security Review: If we were able to create a file we should be able to delete it
                    File.Delete(CurrentItem.TargetSaveFilePath);
                }
                WriteStream = null;
            }
            else if (Progress.DownloadTarget == DownloadTargetEn.DownloadToMemory)
            {
                if (Progress.Succeeded == false)
                {
                    WriteStream = null;
                }
            }

            Progress.DownloadTarget = DownloadTargetEn.NotDefined;

            if (DownloadCancelTokenSource != null)
            {
                DownloadCancelTokenSource.Dispose();
                DownloadCancelTokenSource = null;
            }

            // nulled CurrentItem 
            PreviosItem = CurrentItem;
            CurrentItem = null;// DownloadItemInfo.Empty;// null;

            Interlocked.Decrement(ref m_AlreadyIsBusy); //set that this Download Client already finish his job
        }


        public void Dispose()
        {
            Finalize();
        }

        #endregion ---------------------------- DISPOSE  FINALIZE----------------------------


        #region ------------------------------- CONST ---------------------------

        const int sz_1K = 1024;
        const int sz_1M = 1024 * sz_1K;
        const int DefaultCopyBufferLength = 8192;
        const string DefaultDownloadFileContentType = "application/octet-stream";
        const int DefaultDownloadBufferLength = 65536;

        #endregion ------------------------------- CONST ---------------------------


        #region -----------------------FIELDS --------------------

        static readonly object locker = new object();

        static HashSet<string> HttpMethods
        { get; } = new HashSet<string>() { "POST", "GET", "HEAD", "PUT", "CONNECT", "OPTIONS" };


        /// TASK OPTIONS
        TaskCreationOptions DownloadTaskCreateOption
        { get; set; } = TaskCreationOptions.LongRunning;


        CancellationTokenSource DownloadCancelTokenSource = null;


#if NET45

        System.Net.Cache.RequestCachePolicy m_CachePolicy;

#endif
        DownloadItemProgressInfo Progress;        // progress info into UI   
        HttpWebRequest m_WebRequest;              // webrequest  
        HttpWebResponse m_WebResponse;            // webresponse     



        Stream ReadStream;   //   Response network stream
        Stream WriteStream;  //   file stream where we are going to write loading by chunks data 
        byte[] ChunkBuffer;  //   chunck Buffer             
        long Length;         // - InnerBuffer's Length

        //bool m_Cancelled;             // cancelling option - in progress State  
        private int m_AlreadyIsBusy;    // Interlock - something already working ?

        #endregion -----------------------FIELDS --------------------




        #region ------------------------------  NETWORK:  HTTP , CREDENTIALS PROXY INFO ----------------------------

        public string Method
        {
            get; internal set;
        }


        public WebHeaderCollection Headers
        {
            get; internal set;
        }


        public NameValueCollection RequestParameters
        {
            get; internal set;
        }


        public ICredentials Credentials
        {
            get; internal set;
        }


        public IWebProxy Proxy
        {
            get; internal set;
        }



        #endregion ------------------------------ NETWORK:  HTTP , CREDENTIALS PROXY INFO ----------------------------


        #region ------------------------------ TARGET INFO -------------------------------

        /// <summary>
        /// CurrentItem - currently Created or Downloading Item, will have actual info from Created moment till the Loading process will be finished.
        /// <para/> After the Loading Process will be finished this property will be nulled, and this info will be transmit to PreviosItem property.  
        /// </summary>
        public DownloadItemInfo CurrentItem { get; private set; }


        /// <summary>
        /// Previos Item will have actual info after Loading process will be finished(successfully or with exception). 
        /// <para/> We can use this info to compare downloaded file result after loading action.
        /// <para/> On downloadClient Created moment - this property will be null.
        /// <para/> From second time downloading process will be started - this property will be null.
        /// </summary>
        public DownloadItemInfo PreviosItem { get; private set; }

        #endregion ------------------------------ TARGET INFO -------------------------------



        /// <summary>
        /// UI  Thread Dispatcher to Report Download  Progress Info
        /// </summary>
        internal static Dispatcher DISP
        {
            get
            {
#if NET45
                return Application.Current.Dispatcher;
#elif SL5 || WP81
                return Deployment.Current.Dispatcher;
#endif

            }
        }



        #region ------------------------------ GET  URI  -----------------------------

        private Uri GetUri(Uri address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            Uri uri = address;


            if ((uri.Query == null || uri.Query == string.Empty) && RequestParameters != null)
            {

                StringBuilder sb = new StringBuilder();
                string delimiter = string.Empty;

                for (int i = 0; i < RequestParameters.Count; ++i)
                {
                    sb.Append(delimiter
                              + RequestParameters.AllKeys[i]
                              + "="
                              + RequestParameters[i]
                              );
                    delimiter = "&";
                }

                UriBuilder ub = new UriBuilder(uri);

                ub.Query = sb.S();
                uri = ub.Uri;
            }

            return uri;
        }

        #endregion ------------------------------ GET  URI  -----------------------------


        #region -------------------------------------SET WEB REQUEST -----------------------------------

        private void CopyHeadersToRequest()
        {
            if ((Headers != null) && (m_WebRequest is HttpWebRequest))
            {
                string accept = Headers[HttpRequestHeader.Accept];
                string connection = Headers[HttpRequestHeader.Connection];
                string contentType = Headers[HttpRequestHeader.ContentType];
                string userAgent = Headers[HttpRequestHeader.UserAgent];

                m_WebRequest.Headers = Headers;

                if ((accept != null) && (accept.Length > 0))
                {
                    m_WebRequest.Accept = accept;
                }
                if ((contentType != null) && (contentType.Length > 0))
                {
                    m_WebRequest.ContentType = contentType;
                }
                if ((userAgent != null) && (userAgent.Length > 0))
                {
                    m_WebRequest.UserAgent = userAgent;
                }


            }



        }

        private void CopyRequestParametersToRequest()
        {
            //NEED TO DO
        }

        protected virtual void SetWebRequest()
        {

            m_WebRequest = HttpWebRequest.CreateHttp(CurrentItem.TargetDownloadUri);

            CopyHeadersToRequest();

            CopyRequestParametersToRequest();

            if (Method != null)
            {
                m_WebRequest.Method = Method;
            }

            if (Credentials != null)
            {
                m_WebRequest.Credentials = Credentials;
            }

#if NET45
            if (Proxy != null)
            {
                m_WebRequest.Proxy = Proxy;
            }
#endif


#if NET45
            if (m_CachePolicy != null)
            {
                m_WebRequest.CachePolicy = m_CachePolicy;
            }
#endif


        }

        #endregion ------------------------------------- SET WEB REQUEST -----------------------------------


        #region ------------------------------------ GET  WEB RESPONSE  && RESPONSE STREAM -------------------------------------------

        protected virtual void SetWebResponse()
        {
            // get Long Connected RESPONSE - to  load data from its NetworkStream by chunks 
            m_WebResponse = Task<WebResponse>
                                    .Factory.FromAsync(m_WebRequest.BeginGetResponse,
                                                       m_WebRequest.EndGetResponse, null).Result as HttpWebResponse;

            Progress.TotalBytesToDownload = m_WebResponse.ContentLength;
            CurrentItem.TargetDownloadDataItemSize = m_WebResponse.ContentLength;
        }


        void GetResponseStream()
        {
            if (Progress.TotalBytesToDownload == -1 || Progress.TotalBytesToDownload > DefaultDownloadBufferLength)
            {
                Length = DefaultDownloadBufferLength; // Read buffer length
            }
            else
            {
                Length = Progress.TotalBytesToDownload; // Read buffer length
            }

            // If we are not writing to a stream, we are accumulating in memory
            // We are putting a cap on the size we will accumulate in memory
            if (WriteStream == null && Progress.TotalBytesToDownload > Int32.MaxValue)
            {
                throw new WebException("net_webstatus_MessageLengthLimitExceeded", WebExceptionStatus.MessageLengthLimitExceeded);
            }

            ChunkBuffer = new byte[(int)Length];

            ReadStream = m_WebResponse.GetResponseStream();
            if (m_WebResponse.ContentLength >= 0) //(Async && 
            {
                Progress.TotalBytesToDownload = m_WebResponse.ContentLength;
                CurrentItem.TargetDownloadDataItemSize = m_WebResponse.ContentLength;
            }


        }


        #endregion ------------------------------------ GET  WEB RESPONSE  && RESPONSE STREAM -------------------------------------------


        #region -------------------------PROGRESS NOTIFICATION -----------------------

        Action<DownloadItemProgressInfo> UIStartAction;
        Action<DownloadItemProgressInfo> UIProgressAction;
        Action<DownloadItemProgressInfo> UICompletedAction;

        static void RaiseUIStartAction(DownloadClient dLoader)
        {
            if (dLoader.UIStartAction == null) return;

            DISP.BeginInvoke(dLoader.UIStartAction, dLoader.Progress);
        }


        static void RaiseUIProgressAction(DownloadClient dLoader)
        {
            if (dLoader.UIProgressAction == null) return;

            dLoader.Progress.ReCalculateTransferRate();
            DISP.BeginInvoke(dLoader.UIProgressAction, dLoader.Progress);

        }


        /// <summary>
        ///  Report into UI about  progress
        /// </summary>
        /// <param name="dLoader"></param>
        static void RaiseUICompletedAction(DownloadClient dLoader)
        {
            if (dLoader.UICompletedAction == null) return;

            DISP.BeginInvoke(dLoader.UICompletedAction, dLoader.Progress);
        }


        #endregion -------------------------PROGRESS NOTIFICATION -----------------------



        #region --------------------------- CREATE DOWNLOADCLIENT ---------------------------

        public static DownloadClient Create(
                              Uri targetDownloadFileUri

                            )
        {
            return Create(targetDownloadFileUri
                        , null
                        , null, null, null, null, null, null, null);
        }



        /// <summary>
        /// Create  Downloader client instance.
        /// </summary>
        /// <param name="targetDownloadFileUri"></param>
        /// <param name="targetSaveFilePath"></param>
        /// <returns></returns>
        public static DownloadClient Create(
                                Uri targetDownloadFileUri
                              , string targetSaveFilePath
                              )
        {
            return Create(targetDownloadFileUri, targetSaveFilePath,
                          null, null, null, null, null, null, null);
        }

        /// <summary>
        /// Create  Downloader client instance.
        /// </summary>
        /// <param name="targetDownloadFileUri"></param>
        /// <param name="targetSaveFilePath"></param>
        /// <param name="proxy"></param>
        /// <param name="credentials"></param>
        /// <returns></returns>
        public static DownloadClient Create(
                                Uri targetDownloadFileUri
                              , string targetSaveFilePath
                              , IWebProxy proxy = null
                              , ICredentials credentials = null
                             )
        {
            return Create(targetDownloadFileUri, targetSaveFilePath,
                          null, null, null, null, null, null, null);
        }



        /// <summary>
        /// Create  Downloader client instance.
        /// </summary>
        /// <param name="targetDownloadFileUri"></param>
        /// <param name="targetSaveFilePath"></param>
        /// <param name="proxy"></param>
        /// <param name="credentials"></param>
        /// <param name="method"></param>
        /// <param name="headers"></param>
        /// <param name="requestParameters"></param>
        /// <param name="uiStartAction"></param>
        /// <param name="uiReportProgressAction"></param>
        /// <param name="uiCompletedAction"></param>
        /// <returns></returns>
        public static DownloadClient Create(
                   Uri targetDownloadFileUri,
                   string targetSaveFilePath
                 , IWebProxy proxy = null
                 , ICredentials credentials = null
                 , string method = null
                 , Dictionary<HttpRequestHeader, string> headers = null
                 , Dictionary<string, string> requestParameters = null
                 , Action<DownloadItemProgressInfo> uiStartAction = null
                 , Action<DownloadItemProgressInfo> uiReportProgressAction = null
                 , Action<DownloadItemProgressInfo> uiCompletedAction = null
         )
        {

            if (targetDownloadFileUri == null) throw new ArgumentNullException($"{nameof(DownloadClient)}.{nameof(Create)}(): targetDownloadFileUri can't be null or empty ");

            var dLoader = new DownloadClient();
            dLoader.CurrentItem = new DownloadItemInfo();

            dLoader.CurrentItem.TargetDownloadUri = dLoader.GetUri(targetDownloadFileUri);

            //Target Save File Path
            if (!string.IsNullOrEmpty(targetSaveFilePath))
            {
                if (Path.GetFileName(targetSaveFilePath) == null) throw new InvalidOperationException($"{nameof(DownloadClient)}.{nameof(Create)}():  targetSaveFilePath doesn't contains file name ");
                dLoader.CurrentItem.TargetSaveFilePath = targetSaveFilePath;

                //create directory if not exist 
                var directoryName = Path.GetDirectoryName(dLoader.CurrentItem.TargetSaveFilePath);
                if (!Directory.Exists(directoryName))
                {
                    Directory.CreateDirectory(directoryName);
                }
            }


            if (string.IsNullOrEmpty(method) && HttpMethods.Contains(method))
            {
                dLoader.Method = method;
            }

            if (proxy != null)
                dLoader.Proxy = proxy;

            if (credentials != null)
                dLoader.Credentials = credentials;

            if (dLoader.Proxy != null && dLoader.Credentials != null)
                dLoader.Proxy.Credentials = dLoader.Credentials;
#if NET45
            if (headers != null)
            {
                foreach (var hdrKV in headers)
                {
                    dLoader.Headers.Add(hdrKV.Key, hdrKV.Value);
                }
            }
#endif
            if (requestParameters != null)
            {
                foreach (var reqPram in requestParameters)
                {
                    dLoader.RequestParameters.Add(reqPram.Key, reqPram.Value);
                }
            }



            if (uiStartAction != null)
                dLoader.UIStartAction = uiStartAction;

            if (uiReportProgressAction != null)
                dLoader.UIProgressAction = uiReportProgressAction;

            if (uiCompletedAction != null)
                dLoader.UICompletedAction = uiCompletedAction;


            return dLoader;
        }


        #endregion --------------------------- CREATE DOWNLOADCLIENT ---------------------------


        #region ----------------------------------- PUBLIC SET API OPTIONS ----------------------------

        /// <summary>
        /// Set Proxy for  Downloading WebRequest
        /// </summary>
        /// <param name="proxyAddress"></param>
        /// <param name="BypassOnLoacal"></param>
        /// <returns></returns>
        public DownloadClient SetProxy(string proxyAddress, bool BypassOnLoacal = true)
        {
            if (string.IsNullOrEmpty(proxyAddress)) throw new ArgumentNullException("DownloadClient.SetProxy(): proxyAddress can't be null or empty");
#if NET45
            var wbProxy = new WebProxy(proxyAddress, BypassOnLoacal);

            Proxy = wbProxy;
#endif


            return this;
        }

        /// <summary>
        /// Set Credentials for  Downloading WebRequest
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="domain"></param>
        /// <returns></returns>
        public DownloadClient SetCredentials(string userName, string password, string domain = null)
        {
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException("DownloadClient.SetCredentials(): UserName can't be null or empty");
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException("DownloadClient.SetCredentials(): Password can't be null or empty");


            ICredentials netCredentials = null;
            if (!string.IsNullOrEmpty(domain))
            {
                netCredentials = new NetworkCredential(userName, password, domain);
            }
            else
            {
                netCredentials = new NetworkCredential(userName, password);
            }

            Credentials = netCredentials;

            if (Proxy != null)
                Proxy.Credentials = Credentials;

            return this;
        }


        //SET HEADERS
        //SET REQUEST PARAMETERS
        #endregion ----------------------------------- PUBLIC SET API OPTIONS ----------------------------


        #region ----------------------------- DOWNLOAD FILE ASYNC ----------------------------

        /// <summary>
        /// Download File Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="address"></param>
        /// <param name="fileName"></param>
        /// <param name="proxy"></param>
        /// <param name="credentials"></param>
        /// <param name="uiReportProgressData"></param>
        /// <returns></returns>
        public static Task DownloadFileAsync(Uri targetDownloadFileUri
                                , string targetSaveFilePath
                                , IWebProxy proxy = null
                                , ICredentials credentials = null
                                , Action<DownloadItemProgressInfo> uiStartAction = null
                                , Action<DownloadItemProgressInfo> uiReportProgressAction = null
                                , Action<DownloadItemProgressInfo> uiCompletedAction = null
                                )
        {

            //if (Logging.On) Logging.Enter(Logging.Web, this, "DownloadFile", address + ", " + fileName);


            var dClient = Create(targetDownloadFileUri, targetSaveFilePath
                                 , proxy, credentials
                                 , null, null, null //method/headers/requestParams
                                 , uiStartAction, uiReportProgressAction, uiCompletedAction);

            return DownloadFileAsync(dClient);

        }

        /// <summary>
        /// Download File Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="uiStartAction"></param>
        /// <param name="uiReportProgressAction"></param>
        /// <param name="uiCompletedAction"></param>
        /// <returns></returns>
        public Task DownloadFileAsync(Action<DownloadItemProgressInfo> uiStartAction = null
                                     , Action<DownloadItemProgressInfo> uiReportProgressAction = null
                                     , Action<DownloadItemProgressInfo> uiCompletedAction = null
            )
        {
            UIStartAction = uiStartAction;
            UIProgressAction = uiReportProgressAction;
            UICompletedAction = uiCompletedAction;

            return DownloadFileAsync(this);
        }


        /// <summary>
        /// Download File Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="downloadClient"></param>
        /// <returns></returns>
        public static Task DownloadFileAsync(DownloadClient downloadClient)
        {
            if (downloadClient.IsAlreadyStartedNotCompleted())
                throw new InvalidOperationException("DOWNLOAD CLIENT ALREADY STARTED.");

            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                throw new WebException("Network is not enable now");

            if (string.IsNullOrEmpty(downloadClient.CurrentItem.TargetSaveFilePath))
                throw new InvalidOperationException("TargetSaveFilePath was not setted -so You cant use DownloadFileAsync(). Instead of this you can use DownloadFileToMemoryAsync");

            downloadClient.Progress.DownloadTarget = DownloadTargetEn.DownloadToFile;
            downloadClient.RecreateCancellationToken();


            return Task.Factory.StartNew(
                 (st) =>
                 {
                     var downloader = st as DownloadClient;

                     try
                     {
                         Interlocked.Increment(ref downloadClient.m_AlreadyIsBusy); //set that this Download Client already busy

                         downloader.ClearWebClientState();

                         downloader.SetWriteStream();    //  Creating file and open write stream 

                         downloader.SetWebRequest();     //  Creating WebRequest 

                         downloader.SetWebResponse();    //  Call Get WebResponse synchronously


                         // REPORT TO UI about START DOWNLOADING 
                         RaiseUIStartAction(downloader);

                         downloader.GetResponseStream();

                         // CHUNKS LOADING CYCLE BEGIN
                         bool completed = false; // means that we do no complete
                         do
                         {
                             //stop if cancelled was called
                             if (downloader.DownloadCancelTokenSource.IsCancellationRequested)
                             {
                                 downloader.Progress.Cancelled = true;
                                 break;
                             }

                             //TCP never fill ChunkBuffer by it's size. 
                             // Amount of real valid bytes we need to know  after each chunk reading 
                             int bytesRetrieved = downloader.ReadNewChunk();

                             if (bytesRetrieved > 0)
                             {
                                 downloader.WriteLastChunk(bytesRetrieved);
                             }
                             else
                                 completed = true; //all bytes readed and writed

                             //REPORT TO UI PROGRESS DOWNLOADING 
                             RaiseUIProgressAction(downloader);

                         } while (!completed);
                         // CHUNKS LOADING CYCLE END

                         if (!downloader.Progress.Cancelled)
                         {
                             downloader.Progress.Succeeded = true;
                         }
                     }
                     catch (ThreadAbortException thExc)
                     {
                         var outexc = new InvalidOperationException($"{ nameof(DownloadClient)}.{ nameof(DownloadFileAsync)}():  Thread Abort Exception  on loading - Uri[{downloader.CurrentItem.TargetDownloadUri.AbsoluteUri}] File[{downloader.CurrentItem.TargetFileName}]  To[{downloader.CurrentItem.TargetSaveFilePath}], Message[{thExc.Message}]"); //, thExc
                         downloader.SetExceptionMessage(outexc);
                         AbortRequestOnException(downloader.m_WebRequest);
                     }
                     catch (StackOverflowException stOvExc)
                     {
                         var outexc = new InvalidOperationException($"{ nameof(DownloadClient)}.{ nameof(DownloadFileAsync)}():  Stack Overflow Exception  on loading - Uri[{downloader.CurrentItem.TargetDownloadUri.AbsoluteUri}] File[{downloader.CurrentItem.TargetFileName}]  To[{downloader.CurrentItem.TargetSaveFilePath}], Message[{stOvExc.Message}]"); //, stOvExc
                         downloader.SetExceptionMessage(outexc);
                         AbortRequestOnException(downloader.m_WebRequest);
                     }
                     catch (OutOfMemoryException outMemExc)
                     {
                         var outexc = new InvalidOperationException($"{ nameof(DownloadClient)}.{ nameof(DownloadFileAsync)}():  Out of Memmory Exception on loading - Uri[{downloader.CurrentItem.TargetDownloadUri.AbsoluteUri}] File[{downloader.CurrentItem.TargetFileName}]  To[{downloader.CurrentItem.TargetSaveFilePath}], Message[{outMemExc.Message}]"); //, outMemExc
                         downloader.SetExceptionMessage(outexc);
                         AbortRequestOnException(downloader.m_WebRequest);
                     }
                     catch (WebException wbExc)
                     {
                         var outexc = new InvalidOperationException($"{ nameof(DownloadClient)}.{ nameof(DownloadFileAsync)}():  Web Exception on loading - Uri[{downloader.CurrentItem.TargetDownloadUri.AbsoluteUri}] File[{downloader.CurrentItem.TargetFileName}]  To[{downloader.CurrentItem.TargetSaveFilePath}], Message[{wbExc.Message}]"); //, thExc
                         downloader.SetExceptionMessage(outexc);
                         AbortRequestOnException(downloader.m_WebRequest);
                     }
                     catch (SecurityException secExc)
                     {
                         var outexc = new InvalidOperationException($"{ nameof(DownloadClient)}.{ nameof(DownloadFileAsync)}():  Security Exception on loading - Uri[{downloader.CurrentItem.TargetDownloadUri.AbsoluteUri}] File[{downloader.CurrentItem.TargetFileName}]  To[{downloader.CurrentItem.TargetSaveFilePath}], Message[{secExc.Message}]"); //, thExc
                         downloader.SetExceptionMessage(outexc);
                         AbortRequestOnException(downloader.m_WebRequest);
                     }
                     finally
                     {
                         // REPORT TO UI COMPLETED DOWNLOADING 
                         RaiseUICompletedAction(downloader); //if false then Progress.Exception won't be null
                         downloader.Finalize();

                         //if (Logging.On) Logging.Exit(Logging.Web, this, "DownloadFile", "");
                     }


                 } //task end bracket
                 , downloadClient
                 , downloadClient.DownloadCancelTokenSource.Token //  Download 
                 , downloadClient.DownloadTaskCreateOption // TaskCreationOptions.LongRunning
                 , TaskScheduler.Default
                 );
        }



        #endregion ----------------------------- DOWNLOAD FILE ASYNC ----------------------------



        #region --------------------------------DOWNLOAD FILE INTO MEMORY -------------------------


        /// <summary>
        /// Download File into Memory Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="address"></param>
        /// <param name="fileName"></param>
        /// <param name="proxy"></param>
        /// <param name="credentials"></param>
        /// <param name="uiReportProgressData"></param>
        /// <returns></returns>
        public static Task<Stream> DownloadFileToMemoryAsync(Uri targetDownloadFileUri
                                , IWebProxy proxy = null
                                , ICredentials credentials = null
                                , Action<DownloadItemProgressInfo> uiStartAction = null
                                , Action<DownloadItemProgressInfo> uiReportProgressAction = null
                                , Action<DownloadItemProgressInfo> uiCompletedAction = null
                                )
        {

            //if (Logging.On) Logging.Enter(Logging.Web, this, "DownloadFile", address + ", " + fileName);

            var dClient = Create(targetDownloadFileUri
                                 , null //targetSaveFilePath
                                 , proxy, credentials
                                 , null, null, null //method/headers/requestParams
                                 , uiStartAction, uiReportProgressAction, uiCompletedAction);

            return DownloadFileToMemoryAsync(dClient);

        }

        /// <summary>
        /// Download File into Memory Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="uiStartAction"></param>
        /// <param name="uiReportProgressAction"></param>
        /// <param name="uiCompletedAction"></param>
        /// <returns></returns>
        public Task<Stream> DownloadFileToMemoryAsync(Action<DownloadItemProgressInfo> uiStartAction = null
                                     , Action<DownloadItemProgressInfo> uiReportProgressAction = null
                                     , Action<DownloadItemProgressInfo> uiCompletedAction = null
            )
        {
            UIStartAction = uiStartAction;
            UIProgressAction = uiReportProgressAction;
            UICompletedAction = uiCompletedAction;

            return DownloadFileToMemoryAsync(this);
        }


        /// <summary>
        /// Download File into Memory Asynchronously in Separate Thread with Reporting progress info to UI 
        /// </summary>
        /// <param name="downloadClient"></param>
        /// <returns></returns>
        public static Task<Stream> DownloadFileToMemoryAsync(DownloadClient downloadClient)
        {

            if (downloadClient.IsAlreadyStartedNotCompleted())
                throw new InvalidOperationException("DOWNLOAD CLIENT ALREADY STARTED.");

            if (!System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
                throw new WebException("Network is not enable now");

            // downloadClient.TargetSaveFilePat - can be not null -it means nothing if we want to load file into memory 
            downloadClient.Progress.DownloadTarget = DownloadTargetEn.DownloadToMemory;
            downloadClient.RecreateCancellationToken();


            return Task.Factory.StartNew<Stream>(
                 (st) =>
                 {
                     var downloader = st as DownloadClient;

                     try
                     {
                         Interlocked.Increment(ref downloadClient.m_AlreadyIsBusy); //set that this Download Client already busy

                         downloader.ClearWebClientState();

                         downloader.SetWriteStream();     //  creating file/or Memory Stream to write loading file there

                         downloader.SetWebRequest();     //  creating WebRequest 

                         downloader.SetWebResponse();    // Call Get WebResponse synchronously


                         // REPORT TO UI about START DOWNLOADING 
                         RaiseUIStartAction(downloader);

                         downloader.GetResponseStream();

                         // CHUNKS LOADING CYCLE BEGIN
                         bool completed = false; // means that we do no complete
                         do
                         {
                             //stop if cancelled was called
                             if (downloader.DownloadCancelTokenSource.IsCancellationRequested)
                             {
                                 downloader.Progress.Cancelled = true;
                                 break;
                             }

                             //TCP never fill ChunkBuffer by it's size. 
                             // Amount of real valid bytes we need to know  after each chunk reading 
                             int bytesRetrieved = downloader.ReadNewChunk();

                             if (bytesRetrieved > 0)
                             {
                                 downloader.WriteLastChunk(bytesRetrieved);
                             }
                             else
                                 completed = true; //all bytes readed and writed

                             //REPORT TO UI PROGRESS DOWNLOADING 
                             RaiseUIProgressAction(downloader);

                         } while (!completed);
                         // CHUNKS LOADING CYCLE END

                         if (!downloader.Progress.Cancelled)
                         {
                             downloader.Progress.Succeeded = true;
                         }

                     }
                     catch (Exception e)
                     {
                         if (e is ThreadAbortException || e is StackOverflowException || e is OutOfMemoryException)
                         {
                             e = new InvalidOperationException("threadAbort _webclient ", e);
                         }
                         else if (!(e is WebException || e is SecurityException))
                         {
                             e = new WebException("net _webclient ", e);//  SR.GetString(SR.net_webclient), e);
                         }


                         downloader.SetExceptionMessage(e);
                         AbortRequestOnException(downloader.m_WebRequest);
                         throw e;
                     }
                     finally
                     {
                         // REPORT TO UI COMPLETED DOWNLOADING 
                         RaiseUICompletedAction(downloader);
                         downloader.Finalize();

                     }

                     //if (Logging.On) Logging.Exit(Logging.Web, this, "DownloadFile", "");
                     return downloader.WriteStream;
                 }
                 , downloadClient
                 , downloadClient.DownloadCancelTokenSource.Token //  Download 
                 , downloadClient.DownloadTaskCreateOption // TaskCreationOptions.LongRunning
                 , TaskScheduler.Default
                 );
        }

        #endregion--------------------------------DOWNLOAD FILE INTO MEMORY -------------------------






        #region ------------------------------ DONLOADING REQUEST STATES SWITCH --------------------------------

        public bool IsAlreadyStartedNotCompleted()
        {
            return (m_AlreadyIsBusy > 0);
        }

        public const string Message_AlreadyStarted = "DOWNLOAD CLIENT ALREADY STARTED.";

        void RecreateCancellationToken()
        {
            DownloadCancelTokenSource = new CancellationTokenSource();
        }


        private void SetExceptionMessage(Exception exc)
        {
            if (exc.InnerException != null && exc.InnerException.InnerException != null && exc.InnerException.InnerException.Message != null)
            {
                Progress.ExceptionMessage = exc.InnerException.InnerException.Message;
            }
            else if (exc.InnerException != null && exc.InnerException.Message != null)
            {
                Progress.ExceptionMessage = exc.InnerException.Message;
            }
            else if (exc.InnerException == null)
            {
                Progress.ExceptionMessage = exc.Message;
            }
        }

        /// <summary>
        /// Cancell Loading of Item if DownloadClient really busy now 
        /// </summary>
        public void CancelLoad()
        {
            if (IsAlreadyStartedNotCompleted())
            {
                lock (locker)
                {
                    if (IsAlreadyStartedNotCompleted())
                    {
                        DownloadCancelTokenSource.Cancel();
                    }
                }
            }
        }


        private void ClearWebClientState()
        {
            m_WebResponse = null;
            m_WebRequest = null;

            Method = null;

            if (Progress != null)
                Progress.Reset();

            PreviosItem = null; //resetting State
        }


        void SetWriteStream()
        {
            if (Progress.DownloadTarget == DownloadTargetEn.DownloadToFile)
            {
                WriteStream = new FileStream(CurrentItem.TargetSaveFilePath, FileMode.Create, FileAccess.Write); //fileName
            }
            else if (Progress.DownloadTarget == DownloadTargetEn.DownloadToMemory)
            {
                WriteStream = new MemoryStream();
            }
        }

        private static void AbortRequestOnException(WebRequest request)
        {
            try
            {
                if (request != null)
                {
                    request.Abort();
                }
            }
            catch (Exception exception)
            {
                if (exception is OutOfMemoryException || exception is StackOverflowException || exception is ThreadAbortException)
                {
                    throw;
                }
            }
        }

        #endregion------------------------------ DONLOADING REQUEST STATES SWITCH --------------------------------


        #region ------------------------------------ READING CHUNKS ---------------------------------


        int ReadNewChunk()
        {
            if (ReadStream == null || ReadStream == Stream.Null)
                return 0;
            else
                //ReadStream.Read(InnerBuffer, 0, (int)Length);
                return ReadStream.Read(ChunkBuffer, 0, (int)Length); // Offset  , - Offset
        }

        void WriteLastChunk(int bytesRetrieved)
        {
            if (WriteStream != null)
            {
                WriteStream.Write(ChunkBuffer, 0, bytesRetrieved);
            }

            //state - we received and wrote chunk data into file
            Progress.BytesDownloaded += bytesRetrieved;
        }


        #endregion ------------------------------------ READING CHUNKS ---------------------------------




    }
}
