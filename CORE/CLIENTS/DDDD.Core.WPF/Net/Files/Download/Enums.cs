﻿namespace DDDD.Core.Net.Files.Download
{


    /// <summary>
    /// Download target palces- directly into file, or into memory  
    /// </summary>
    public enum DownloadTargetEn
    {
        NotDefined

           ,
        /// <summary>
        /// DownloadClient will download file into local client file in custom place of hard drive
        /// </summary>
        DownloadToFile

           ,
        /// <summary>
        /// DownloadClient will download file into memory
        /// </summary>
        DownloadToMemory
    }
}
