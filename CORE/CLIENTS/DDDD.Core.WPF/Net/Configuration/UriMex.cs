﻿using System;
using System.Reflection;

namespace DDDD.Core.Net
{
    /// <summary>
    /// UriMex - Uri with [mex] value. It let us to use incorrect uri string value ["mex"] and we won't get any exception on uri string validation. 
    /// <para/> This Uri value can be used with ConfigurationElement classes to write this value into *.config file, like:
    ///   {endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" /} 
    /// </summary>
    public class UriMex : Uri
    {
        const string defaultAddress = "http://localhost";
        static FieldInfo field_mString = typeof(Uri).GetField("m_String", BindingFlags.NonPublic | BindingFlags.Instance);

        public UriMex() : base(defaultAddress)
        {
            field_mString.SetValue((this as Uri), "mex");
        }

        public override string ToString()
        {
            return OriginalString;
        }

    }
}
