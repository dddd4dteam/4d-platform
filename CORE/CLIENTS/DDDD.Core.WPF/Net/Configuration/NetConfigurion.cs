﻿
using System;
using Cfg = System.Configuration;
using System.ServiceModel.Description;

using DDDD.Core.Extensions;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Threading;
using DDDD.Core.ComponentModel;






#if SERVER && IIS

using System.ServiceModel.Configuration;
using System.Web.Configuration;
using DDDD.Core.Net.WebApi;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Http.ExceptionHandling;


#endif


namespace DDDD.Core.Net
{




    /*
   ------------------------TARGET SERVICEMODEL EXAMPLE BEGIN CONFIG -------------------------
   // discover , add existed DCS
        
 <system.serviceModel>      
    <extensions>

      <behaviorExtensions>
        <add name="ddddsds" type="DDDD.Core.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.0.9.0.1, Version=0.9.0.1, Culture=neutral, PublicKeyToken=e364950485695b69" />
      </behaviorExtensions>

    </extensions>

    <behaviors>
    
      <endpointBehaviors>
        <behavior name="TSS.ServiceModel.EndpointBehavior">
          <ddddsds />
        </behavior>
      </endpointBehaviors>

      <serviceBehaviors>

        <behavior name="DDDD.Core.Net.ServiceModel.Behaviour1">
          <serviceDebug includeExceptionDetailInFaults="true" />
          <serviceMetadata httpGetEnabled="true" httpsGetEnabled="false" />
          <serviceThrottling maxConcurrentCalls="5" maxConcurrentInstances="5" maxConcurrentSessions="5"/>
          <!--<serviceAuthorization principalPermissionMode="UseAspNetRoles" />-->
        </behavior>

      </serviceBehaviors>
    </behaviors>

    <bindings>

      <customBinding>
        <binding name="DDDD.Core.Net.ServiceModel.CustomBinding1" receiveTimeout="00:00:30">
          <binaryMessageEncoding />
          <httpTransport transferMode="StreamedResponse" />
        </binding>
      </customBinding>

    </bindings>

    </system.serviceModel>
         
 ------------------------TARGET SERVICEMODEL EXAMPLE BEGIN CONFIG -------------------------
   */



    /// <summary>
    /// Net configuration class helps us -          
    /// <para/> 1  - to configure [system.ServiceModel] elements in web.Config.
    /// <para/> 2  - to configure primary WebApi infrastructure - Routings and redefined base services by WebApiInfrastructureService.
    /// </summary>
    public class NetConfiguration
    {

        private static readonly object locker = new object();
    

        #region ------------------------------- APPLICATION NETWORK CONFIGURATION ----------------------------

        /// <summary>
        /// True - means that  some of Configuration Modification methods LA_AppConfiguration was really changed and needs to be save.
        /// False - means  that LA_AppConfiguration was not changed.
        /// </summary>
        public static bool IsAppConfigChanged
        { get; private set; }



#if (SERVER && IIS) || WPF

        public static LazyAct<Cfg.Configuration> LA_AppConfiguration
            = LazyAct<Cfg.Configuration>.Create(
            (args) =>
            {
                return GetAppConfiguration();
            }
            , null
            , null
            );

#endif


#if SERVER && IIS
        static Cfg.Configuration GetAppConfiguration()
        {
            return WebConfigurationManager.OpenWebConfiguration("~");
        }

#elif CLIENT && WPF // 

        static Cfg.Configuration GetAppConfiguration()
        {
            return Cfg.ConfigurationManager.OpenExeConfiguration(Cfg.ConfigurationUserLevel.None);
        }
#endif

        #endregion ------------------------------- APPLICATION NETWORK CONFIGURATION ----------------------------
        

#if SERVER && IIS

        public static LazyAct<ServiceModelSectionGroup> LA_ServiceModelCfgSection = LazyAct<ServiceModelSectionGroup>
            .Create(
            (args) =>
            {
                return ServiceModelSectionGroup.GetSectionGroup(LA_AppConfiguration.Value);
            }
            , null
            , null
            );

        
        #region ------------------------------ ADD TSS Extension -----------------------------------
        /*  
         <system.serviceModel>
         <extensions>      
           <behaviorExtensions>
             <add name="ddddtss"
                  type="DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69"/>
           </behaviorExtensions>      
         </extensions>
         <system.serviceModel> 
        */

        /// <summary>
        /// TSS( Type Set Serializer -   binary serializer) extension Name - Name of  [add]  element  in [system.serviceModel]\[extensions]\[behaviorExtensions] elements collection in web.config.
        /// </summary>
        public const string TSSBehaviorExtensionName = "ddddtss";
        
        /// <summary>
        /// TSS( Type Set Serializer -   binary serializer) extension Type - Type of [DDDD.Core.Net.ServiceModel.TSSBehaviorExtension].
        /// </summary>
        public static readonly Type TSSBehaviorExtensionType = typeof(TSSBehaviorExtension);



        /// <summary>
        /// Try Add Tss  Endpoint Behavior Extension
        /// </summary>
        /// <returns></returns>
        public static void TryAddTSSBehaviorExtension()
        {
            try
            {
                //Cfg.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                //ServiceModelSectionGroup wServiceSection = LA_ServiceModelCfgSection.Value;// ServiceModelSectionGroup.GetSectionGroup(config);
                
                //  <add name="ddddtss"
                //type = "DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69" />
                if (!LA_ServiceModelCfgSection.Value.Extensions.BehaviorExtensions.ContainsKey(TSSBehaviorExtensionName))
                {
                    var extTss = new ExtensionElement(TSSBehaviorExtensionName, typeof(TSSBehaviorExtension).AssemblyQualifiedName);
                    LA_ServiceModelCfgSection.Value.Extensions.BehaviorExtensions.Add(extTss);

                    //LA_AppConfiguration.Value.Save();
                    IsAppConfigChanged = true;
                    //return true;
                }

                //return false;// false Configuration was not changed - so we needn't  configuration.Save() call 
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion ------------------------------ ADD TSS Extension -----------------------------------


        #region ------------------------------- ADD BEHAVIORS --------------------------------

        // NEXT STEP: ADD BEHAVIOR CONFIG: - shared custom binding  that will be used by Service configuration
        /* 
         <system.serviceModel>
            <behaviors>

            <endpointBehaviors>
             <behavior name="DDDD.ServiceModel.TSS.EndpointBehavior">
              <ddddtss/>
             </behavior>
            </endpointBehaviors>	

            <serviceBehaviors>	   
              <behavior name="$Subsystem$.Communication.Behaviour">
              <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
              <serviceDebug includeExceptionDetailInFaults="true" />
              <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
              </behavior>
            </serviceBehaviors>

            </behaviors>
        <system.serviceModel> 
        */



        /* FIRST - ENDPOINT BEHAVIOR
          <endpointBehaviors>
          <behavior name = "DDDD.ServiceModel.TSS.EndpointBehavior" >
           < ddddtss />
          </ behavior >
         </ endpointBehaviors >
        */
        
        /// <summary>
        /// Try Add TSS Endpoint Behavior
        /// </summary>
        /// <returns></returns>
        public static void TryAddTSSEndpointBehavior()
        {
            try
            {
                //Cfg.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                //ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);

                var endpointBhvrKey = "DDDD.ServiceModel.TSS.EndpointBehavior";
                var ddddtssExtension = (BehaviorExtensionElement)(LA_ServiceModelCfgSection.Value.Extensions.BehaviorExtensions["ddddtss"] as object);
                EndpointBehaviorElement eBhvrEl = new EndpointBehaviorElement(endpointBhvrKey);
                eBhvrEl.Add(ddddtssExtension);

                if (!LA_ServiceModelCfgSection.Value.Behaviors.EndpointBehaviors.ContainsKey(endpointBhvrKey))
                {
                    var extTss = new ExtensionElement("ddddtss", typeof(TSSBehaviorExtension).AssemblyQualifiedName);
                    LA_ServiceModelCfgSection.Value.Extensions.BehaviorExtensions.Add(extTss);

                    //LA_AppConfiguration.Value.Save();
                    IsAppConfigChanged = true;

                }

                //return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }





        /* SECOND  - SERVICE BEHAVIOR
          <serviceBehaviors>	   
              <behavior name="$Subsystem$.Communication.Behaviour">
              <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
              <serviceDebug includeExceptionDetailInFaults="true" />
              <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
              </behavior>
          </serviceBehaviors>
        */

        /// <summary>
        /// Try add service Behavior to web.config
        /// </summary>
        /// <param name="serviceBehavName"></param>
        /// <param name="maxConcurrentCalls"></param>
        /// <param name="maxConcurrentInstancies"></param>
        /// <param name="maxConcurrentSessions"></param>
        /// <param name="includeExceptionDetailsOnFaults"></param>
        /// <param name="httpGetEnabled"></param>
        /// <param name="httpsGetEnabled"></param>
        /// <returns></returns>
        public static void TryAddServiceBehavior(string serviceBehavName, short maxConcurrentCalls = 4, short maxConcurrentInstancies = 4, short maxConcurrentSessions = 4
                                                  , bool includeExceptionDetailsOnFaults = true
                                                 , bool httpGetEnabled = true, bool httpsGetEnabled = false
                                                )
        {
            try
            {
                //Cfg.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                //ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);

                /*
                <behavior name="$Subsystem$.Communication.Behaviour">
                <serviceThrottling maxConcurrentCalls="$ConcurrentCalls$" maxConcurrentInstances="$ConcurrentInstancies$" maxConcurrentSessions="$ConcurrentSessions$"/>
                <serviceDebug includeExceptionDetailInFaults="true" />
                <serviceMetadata httpGetEnabled="true"  httpsGetEnabled="false"/>
                </behavior>
                */

                if (!LA_ServiceModelCfgSection.Value.Behaviors.ServiceBehaviors.ContainsKey(serviceBehavName))
                {
                    ServiceBehaviorElement svcBhvrEl = new ServiceBehaviorElement(serviceBehavName);

                    var servThrottling = new ServiceThrottlingElement()
                    {
                        MaxConcurrentCalls = maxConcurrentCalls,
                        MaxConcurrentInstances = maxConcurrentInstancies,
                        MaxConcurrentSessions = maxConcurrentSessions
                    };
                    svcBhvrEl.Add(servThrottling);


                    var serviceDebug = new ServiceDebugElement()
                    { IncludeExceptionDetailInFaults = includeExceptionDetailsOnFaults };
                    svcBhvrEl.Add(serviceDebug);


                    var serviceMetaData = new ServiceMetadataPublishingElement()
                    { HttpGetEnabled = httpGetEnabled, HttpsGetEnabled = httpsGetEnabled };
                    svcBhvrEl.Add(serviceMetaData);


                    LA_ServiceModelCfgSection.Value.Behaviors.ServiceBehaviors.Add(svcBhvrEl);
                    //LA_AppConfiguration.Value.Save();
                    IsAppConfigChanged = true;
                }

               // return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion ------------------------------- ADD BEHAVIORS --------------------------------


        #region ------------------------------- ADD BINDINGS -------------------------------------

        /*
         <system.serviceModel>
           <bindings>
                <customBinding>
                    <binding name="$Subsystem$.Communication.customBinding"  receiveTimeout="00:00:30" >
                    <binaryMessageEncoding />
                    <httpTransport  transferMode="StreamedResponse" />          
                    </binding>
                </customBinding>
            </bindings>
        <system.serviceModel>
        */


        /// <summary>
        /// Try add custom binding configuration to web.config
        /// </summary>
        /// <param name="custBindingKey"></param>
        /// <param name="receiveTimeout"></param>
        /// <returns></returns>
        public static void TryAddCustomBinding(string custBindingKey, TimeSpan receiveTimeout)
        {
            try
            {
                //Cfg.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
                //ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(config);


                if (!LA_ServiceModelCfgSection.Value.Bindings.CustomBinding.Bindings.ContainsKey(custBindingKey))
                {

                    CustomBindingElement custBindngEl = new CustomBindingElement(custBindingKey);
                    custBindngEl.ReceiveTimeout = receiveTimeout;

                    custBindngEl.Add(new BinaryMessageEncodingElement());
                    custBindngEl.Add(new HttpTransportElement() { TransferMode = System.ServiceModel.TransferMode.StreamedResponse });
                    LA_ServiceModelCfgSection.Value.Bindings.CustomBinding.Bindings.Add(custBindngEl);

                    //LA_AppConfiguration.Value.Save();
                    IsAppConfigChanged = true;
                }

               // return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion  ------------------------------- ADD BINDINGS -------------------------------------


        #region ---------------------------------- ADD SERVICE ------------------------------------

        const string MexHttpBinding = "mexHttpBinding";

        //NEXT STEP:  ADD SERVICE CONFIG: 
        /*  - Usual Service - Without using TSS
         <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
          <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                                contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" />
          <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
         </service>
        */
        // OR
        /*  Service as Below - Service that is going to use TSS serialization
         <service name="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" behaviorConfiguration="$Subsystem$.Communication.Behaviour">
          <endpoint address="" binding="customBinding" bindingConfiguration="$Subsystem$.Communication.customBinding" 
                                contract="$Subsystem$.Dom.Modules.$ModuleName$.I$ModuleName$Service" 
                                behaviorConfiguration="TSS.ServiceModel.EndpointBehavior" />						
          <endpoint address="mex" binding="mexHttpBinding" contract="IMetadataExchange" />
         </service>
        */


        /// <summary>
        /// Try Add Service Configuration with associated serviceBindingConfiguration and serviceBehaviorConfiguration
        /// <para/>    ,add  private endpoint Configuration
        /// <para/>    ,add   also mex endpoint Configuraion
        /// </summary>
        /// <param name="svcName"></param>
        /// <param name="svcBehaviorConfiguration"></param>
        /// <param name="endpointContract"></param>
        /// <param name="endpointBinding"></param>
        /// <param name="endpointBindingConfiguration"></param>
        /// <param name="endpointBehaviorConfiguration"></param>
        /// <param name="useMetaDataExchangeEndpoint"></param>
        /// <returns></returns>
        public static void TryAddService(string svcName, string svcBehaviorConfiguration 
                                         , Type endpointContract  
                                         , string endpointBinding , string  endpointBindingConfiguration 
                                         , string endpointBehaviorConfiguration 
                                         , bool useMetaDataExchangeEndpoint = true
                                            )
        {
            try
            {
                
                if (  !LA_ServiceModelCfgSection.Value.Services.Services.ContainsKey(svcName)  )
                {
                    var newServiceElement = new ServiceElement()
                    {   Name = svcName ,
                        BehaviorConfiguration = svcBehaviorConfiguration                        
                    };
                    
                    var newEndpoint = new ServiceEndpointElement(); 
                    newEndpoint.Contract = endpointContract.FullName;
                    newEndpoint.Binding = endpointBinding;  
                    newEndpoint.BindingConfiguration = endpointBindingConfiguration;
                    if ( endpointBehaviorConfiguration.IsNotNull() ) // 
                    { newEndpoint.BehaviorConfiguration = endpointBehaviorConfiguration;  }
                    newServiceElement.Endpoints.Add(newEndpoint);

                    if ( useMetaDataExchangeEndpoint )   // 
                    {                        
                        //Enable metadata exchange                        
                        var mexServiceEnpoint = new ServiceEndpointElement(new UriMex(), nameof(IMetadataExchange));
                        mexServiceEnpoint.Binding = MexHttpBinding;                        
                        newServiceElement.Endpoints.Add( mexServiceEnpoint);
                    }

                    LA_ServiceModelCfgSection.Value.Services.Services.Add(newServiceElement);

                    IsAppConfigChanged = true;
                }

               // return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        

        #endregion ---------------------------------- ADD SERVICE ------------------------------------


        #region ---------------------------------- ADD SERVICE ENVIRONMENT ACTIVATION------------------------------------
        // 5 (OPTIONAL) If you've decided to use  web.config only registration and activation(instead of *.svc Markup file)  
        //    you'll need to expand form of serviceHostingEnvironment section(like below) and add line with current service activation :
        /* 
         <serviceHostingEnvironment aspNetCompatibilityEnabled="true"   multipleSiteBindingsEnabled="true" >
             <serviceActivations>
               <!--YOUR SERVICE ACTIVATION LINE-->
               <add relativeAddress="~/Services/$ModuleName$Service.svc" service="$Subsystem$.Dom.Modules.$ModuleName$.$ModuleName$ServiceImplementor" />                  
             </serviceActivations>
         </serviceHostingEnvironment>
        */
          

        /// <summary>
        ///  Add DC service Activation item to [serviceHostingEnvironment]\[serviceActivations]\[add]  for some owner( DCManager ) Type.  
        /// </summary>
        /// <param name="ownerTypeName"></param>
        /// <param name="ownerRelativeAddress"></param>
        /// <returns></returns>
        internal static void TryAddDCServiceActivationInEnvironment( DCCUnit dcCommunicationUnit)
        {
            try
            {
                //var dccUnitTypeKey = DCCScenarios.Wcf_EnvironmentActivationSvc;
                var registeredAddress = dcCommunicationUnit.UriResultAddress;//  GetRegisteredOwnerUnitAddress(dccUnitTypeKey, dcCommunicationUnit.OwnerType);

                if (registeredAddress.IsNotNull()) return;// false;//already registered
                                        
                    if (!LA_ServiceModelCfgSection.Value.ServiceHostingEnvironment.ServiceActivations.ContainsKey(dcCommunicationUnit.UriResultAddress))
                    {
                        var newServiceActivation = new ServiceActivationElement(
                                            relativeAddress: dcCommunicationUnit.UriResultAddress // ownerRelativeAddress
                                        , service: typeof(DCService).FullName
                                        , factory: typeof(DCServiceHostFactory).FullName
                                        );
                        LA_ServiceModelCfgSection.Value.ServiceHostingEnvironment.ServiceActivations.Add(newServiceActivation);

                    //add tor internal DCCommunication Unit Register 
                    //RegisterCommunicationUnit();
                    //LA_AppConfiguration.Value.Save();
                    IsAppConfigChanged = true;
                    
                    }

                //return false;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion  ---------------------------------- ADD SERVICE ENVIRONMENT ACTIVATION------------------------------------

       

        /// <summary>
        /// Save Configuraion -in web.Config
        /// </summary>
        public static void SaveConfigIfChanged()
        {
            try
            {
                if (IsAppConfigChanged)
                {
                    LA_AppConfiguration.Value.Save();
                }                
            }
            catch (Exception exc)
            {  throw exc;
            }
        }

        

        #region ----------------------- WEB API INFRASTRUCTURE CONFIGURATION --------------------------

        
        protected internal static bool IsWebApiInsfrastructureConfigured { get; private set; }


        protected internal static void ConfigureWebApiRouting(bool useMapHttpattributeRoutes = true, bool addDefault_ControllerActionId_Route = true)
        {
            // Web API configuration and services

            // Web API routes
            if (useMapHttpattributeRoutes    )
            {
                WebApiInfrastructureService.Current.HttpConfig.MapHttpAttributeRoutes();
            }
            

            if (addDefault_ControllerActionId_Route)
            {
                WebApiInfrastructureService.Current.HttpConfig.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
                            );

            }
            

        }



        /// <summary>
        /// Configuring Web Api Services and primary Routing.
        /// <para/>    Routing - by default we use Map by HttpAttributeRoutes, and adding route [DefaultApi] with [api/{controller}/{action}/{id}]  route template.
        /// <para/>    Services - We setting WebApiInfrastructureService as implementation of IHttpControllerActivator, IHttpControllerSelector, IHttpControllerTypeResolver 
        /// </summary>
        /// <returns></returns>
        protected internal static bool ConfigureWebApiInfrastructure()
        {
            bool wasConfigurationInited = true;


            locker.LockAction( ()=>(IsWebApiInsfrastructureConfigured == false)
                , () =>
                 {

                     // Web API Routing Configuring
                     ConfigureWebApiRouting(true, true);
                     
                     // Cannot register additional ComponentsClasses here ComponentsContainer.InitializeComponentsClasses();
                     
                     // Set ComponentsContainer  as Dependency Resolver
                     WebApiInfrastructureService.Current.HttpConfig.DependencyResolver = ComponentsContainer.Current;
                     
                     // Replacing webApi internal services configuring
                     WebApiInfrastructureService.Current.HttpConfig.Services.Replace(typeof(IHttpControllerActivator), WebApiInfrastructureService.Current);
                     WebApiInfrastructureService.Current.HttpConfig.Services.Replace(typeof(IHttpControllerSelector), WebApiInfrastructureService.Current);
                     WebApiInfrastructureService.Current.HttpConfig.Services.Replace(typeof(IHttpControllerTypeResolver), WebApiInfrastructureService.Current);

                     // Logging Interface service  
                     WebApiInfrastructureService.Current.HttpConfig.Services.Replace(typeof(IExceptionLogger), WebApiInfrastructureService.Current);

                     WebApiInfrastructureService.Current.HttpConfig.EnsureInitialized();

                 }
                );

            return wasConfigurationInited;
        }
                

        #endregion ----------------------- WEB API INFRASTRUCTURE CONFIGURATION --------------------------




#endif











    }


}



#region ----------------------------------- GARBAGE ------------------------------
///// <summary>
///// DC CommunicationUnitFactories default configurations for all items, and custom DCCUnit Item's configurations for personal configuration for some separate DCCUnits .
///// </summary>
//public static Dictionary<string, Func<object[], bool>> NetConfigurationScenarios
//{ get; } = new Dictionary<string, Func<object[], bool>>();
///// <summary>
///// 
///// </summary>
///// <param name="scenarioKey"></param>
///// <param name="customConfiguration"></param>
//public static void AddCustomConfigScenario(string scenarioKey, Func<object[], bool> customConfiguration)
//{
//    lock (NetConfigurationScenarios)
//    {                 
//        NetConfigurationScenarios.Add(scenarioKey, customConfiguration);                
//    }            
//}

/// <summary>
/// Register DC Communication Unit address. 
/// </summary>
/// <param name="communicationUnit"></param>
/// <param name="ownerType"></param>
/// <param name="ownerAddress"></param>
//public static bool RegisterUnitForOwner(string communicationUnit, Type ownerType, string ownerAddress)
//{
//    try
//    {
//        if (!DCCommunicationUnits.ContainsKey(communicationUnit))
//        {
//            DCCommunicationUnits.Add(communicationUnit, new Dictionary<Type, string>());
//        }
//        else if (DCCommunicationUnits[communicationUnit].ContainsKey(ownerType)) return false; // already exist

//        DCCommunicationUnits[communicationUnit].Add(ownerType, ownerAddress);


//        var configWasChanged = false;
//        //change webConfig if need 
//        if (communicationUnit == string.Wcf_EnvironmentActivationSvc)
//        {
//            configWasChanged = TryAddDCServiceActivationInEnvironment(ownerType, ownerAddress);
//        }


//        return configWasChanged;
//    }
//    catch (Exception exc)
//    {
//        throw exc;
//    }

//}

#endregion ----------------------------------- GARBAGE ------------------------------


