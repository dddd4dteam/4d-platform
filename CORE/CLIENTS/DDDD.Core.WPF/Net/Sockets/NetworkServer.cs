﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.Net.Sockets
{

    public class NetworkServer
    {
        public delegate object ProcessPayload(object data);

       // private ILog log = LogManager.GetLogger(typeof(NetworkServer));
        ProcessPayload _handler;
        private bool _run = true;
        private int count = 0;
        private int _port;

        public void Start(int port, ProcessPayload handler)
        {
            _handler = handler;
            _port = port;
            ThreadPool.SetMinThreads(50, 50);
            System.Timers.Timer t = new System.Timers.Timer(1000);
            t.AutoReset = true;
            t.Start();
            t.Elapsed += new System.Timers.ElapsedEventHandler(t_Elapsed);
            Task.Factory.StartNew(() => Run(), TaskCreationOptions.AttachedToParent);
        }

        private void Run()
        {
            TcpListener listener = new TcpListener(IPAddress.Any, _port);
            listener.Start();

            while (_run)
            {
                try
                {
                    TcpClient c = listener.AcceptTcpClient();
                    Task.Factory.StartNew(() => Accept(c));
                }
                catch (Exception ex) {
                    //log.Error(ex); 
                }
            }
        }

        void t_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (count > 0)
               // log.Debug("tcp connects/sec = " + count);
            count = 0;
        }

        public void Stop()
        {
            _run = false;
        }

        void Accept(TcpClient client)
        {
            using (NetworkStream n = client.GetStream())
            {
                while (client.Connected)
                {
                    this.count++;
                    byte[] c = new byte[5];
                    n.Read(c, 0, 5);
                    int count = BitConverter.ToInt32(c, 1);
                    byte[] data = new byte[count];
                    int bytesRead = 0;
                    int chunksize = 1;
                    while (bytesRead < count && chunksize > 0)
                        bytesRead +=
                          chunksize = n.Read
                            (data, bytesRead, count - bytesRead);

                    object o = fastBinaryJSON.BJSON.ToObject(data);

                    object r = _handler(o);
                    bool compressed = false;
                    data = fastBinaryJSON.BJSON.ToBJSON(r);
                    if (data.Length > NetworkClient.Config.CompressDataOver)
                    {
                        //log.Debug("compressing data over limit : " + data.Length.ToString("#,#"));
                        compressed = true;
                        data = RaptorDB.MiniLZO.Compress(data);
                        //log.Debug("new size : " + data.Length.ToString("#,#"));
                    }
                    if (data.Length > NetworkClient.Config.LogDataSizesOver) { }
                       // log.Debug("data size (bytes) = " + data.Length.ToString("#,#"));

                    byte[] b = BitConverter.GetBytes(data.Length);
                    byte[] hdr = new byte[5];
                    hdr[0] = (byte)(3 + (compressed ? 4 : 0));
                    Array.Copy(b, 0, hdr, 1, 4);
                    n.Write(hdr, 0, 5);
                    n.Write(data, 0, data.Length);

                    int wait = 0;
                    while (n.DataAvailable == false)
                    {
                        wait++;
                        if (wait < 10000) // kludge : for insert performance
                            Thread.Sleep(0);
                        else
                            Thread.Sleep(1);
                        // FEATURE : if wait > 10 min -> close connection 
                    }
                }
            }
        }
    }
}
