﻿

#if SERVER && IIS

using DDDD.Core.Threading;
using DDDD.Core.Extensions;
using DDDD.Core.ComponentModel;

using System;
using System.Net;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Reflection;
using System.Web.Http.ExceptionHandling;
using System.Threading;
using System.Threading.Tasks;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.Net.WebApi
{
    // 
    // http://aspnetboilerplate.com/Pages/Documents/Dynamic-Web-API - 
    // 

    /// <summary>
    /// WebApiInfrastructureService implements IHttpControllerSelector
    ///                                    , IHttpControllerActivator                                            
    ///                                        , IHttpControllerTypeResolver
    /// </summary>
    public class WebApiInfrastructureService : IHttpControllerSelector
                                            , IHttpControllerActivator
                                            , IHttpControllerTypeResolver
                                            , IExceptionLogger
    //, IDirectRouteProvider


    {

        static IDCLogger _Logger = null;
        static IDCLogger Logger
        {
            get
            {
                if (_Logger.IsNull())
                {
                    _Logger = ComponentsContainer.GetComponent(typeof(IDCLogger)).ResolveInstanceTBaseLazy<IDCLogger>();
                }
                return _Logger;
            }
        }

        #region -------------------------------- SINGLETON -------------------------------------


        WebApiInfrastructureService(HttpConfiguration configuration)
        {
            httpConfiguration = configuration;
        }


        static LazyAct<WebApiInfrastructureService> LA_CurrentFactory =
               LazyAct<WebApiInfrastructureService>.Create(
                   (args) => { return new WebApiInfrastructureService(GlobalConfiguration.Configuration); }
                   , null//locker
                   , null //args 
                   );

        /// <summary>
        /// Instance of WebApiInfrastructureService  
        /// </summary>
        public static WebApiInfrastructureService Current
        {
            get
            {
                return LA_CurrentFactory.Value;
            }
        }


#endregion -------------------------------- SINGLETON -------------------------------------



        private readonly HttpConfiguration httpConfiguration;

        /// <summary>
        /// Http Configuration  - referece to GlobalConfiguration.Configuration instance.
        /// </summary>
        internal HttpConfiguration HttpConfig
        {
            get { return httpConfiguration; }
        }


#region ---------------------------------- IHttpControllerActivator ---------------------------------

        /// <summary>
        /// Thread Safety, but you need to organize Controller without collision static resources
        /// </summary>        
        static ConcurrentDictionary<string, IHttpController> ControllersCache = new ConcurrentDictionary<string, IHttpController>();



        private readonly ConcurrentDictionary<Type, HttpConfiguration> cacheHttpConfigs = new ConcurrentDictionary<Type, HttpConfiguration>();



        /// <summary>
        /// Create Controller or get Existed instance. by request, controllerDescriptor , controllerType
        /// </summary>
        /// <param name="request"></param>
        /// <param name="controllerDescriptor"></param>
        /// <param name="controllerType"></param>
        /// <returns></returns>
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
#if DEBUG
            try
            {
                if (ControllersCache.ContainsKey(controllerDescriptor.Configuration.VirtualPathRoot))
                { return ControllersCache[controllerDescriptor.Configuration.VirtualPathRoot]; }

                IHttpController targetController = controllerDescriptor.ControllerType.CreateInstanceTBaseLazy<IHttpController>();
                ControllersCache.TryAdd(controllerDescriptor.Configuration.VirtualPathRoot, targetController);

                return targetController;
            }
            catch (Exception exc)
            {
                throw exc;
            }
#else
               if (ControllersCache.ContainsKey(controllerDescriptor.Configuration.VirtualPathRoot))
                { return ControllersCache[controllerDescriptor.Configuration.VirtualPathRoot]; }

                IHttpController targetController = controllerDescriptor.ControllerType.CreateInstanceBoxedLazy() as IHttpController;
                ControllersCache.Add(controllerDescriptor.Configuration.VirtualPathRoot, targetController);

                return targetController;

#endif
        }




#endregion ---------------------------------- IHttpControllerActivator ---------------------------------


#region -------------------------------- IHttpControllerSelector ------------------------------------

        static LazyAct<DefaultHttpControllerSelector> LA_DefaultSelector =
          LazyAct<DefaultHttpControllerSelector>.Create(
              (args) => { return new DefaultHttpControllerSelector(Current.HttpConfig); }
              , null, null
              );


        public string GetControllerName(HttpRequestMessage request)
        {
            var controllerName = LA_DefaultSelector.Value.GetControllerName(request);
            var versionFinder = new VersionFinder();
            var version = versionFinder.GetVersionFromRequest(request);

            if (version > 0)
            {
                return GetVersionedControllerName(request, controllerName, version);
            }

            return controllerName;
        }

        private string GetVersionedControllerName(HttpRequestMessage request, string baseControllerName, int version)
        {
            var versionControllerName = "{0}v{1}".Fmt(baseControllerName, version.S());
            HttpControllerDescriptor descriptor;
            if (GetControllerMapping().TryGetValue(versionControllerName, out descriptor))
            {
                return versionControllerName;
            }

            throw new HttpResponseException(request.CreateErrorResponse(
                    HttpStatusCode.NotFound,
                    "No HTTP resource was found that matches the URI {0} and version number {1}".Fmt(request.RequestUri.S(), version.S())));
        }



        public HttpControllerDescriptor SelectController(HttpRequestMessage request)
        {
            var assembly = Assembly.LoadFile("c:/myAssembly.dll");
            var types = assembly.GetTypes(); //GetExportedTypes doesn't work with dynamic assemblies
            var matchedTypes = types.Where(i => typeof(IHttpController).IsAssignableFrom(i)).ToList();

            var controllerName = GetControllerName(request);
            var matchedController =
                matchedTypes.FirstOrDefault(i => i.Name.ToLower() == controllerName.ToLower() + "controller");

            return new HttpControllerDescriptor(Current.HttpConfig, controllerName, matchedController);
        }



        public IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            return LA_DefaultSelector.Value.GetControllerMapping();
        }


#endregion -------------------------------- IHttpControllerSelector ------------------------------------


#region ---------------------------- IHttpControllerTypeResolver -------------------------------

        /// <summary>
        /// ComponentsClass of WebApiControllers Key = [WebApiControllers]
        /// </summary>
        const string WebApiControllers = nameof(WebApiControllers);


        static readonly DefaultHttpControllerTypeResolver defaultTypeResolver = new DefaultHttpControllerTypeResolver(IsHttpEndpoint);


        /// <summary>
        ///IsHttpEndpoint-  is Predicate that checks - if some Type is end ApiControllerType 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        internal static bool IsHttpEndpoint(Type t)
        {
            if (t == null) throw new ArgumentNullException("t");

            return
             t.IsClass &&
             t.IsVisible &&
             !t.IsAbstract
              && typeof(ApiController).IsAssignableFrom(t);
        }

        internal static bool IsControllerType(Type t)
        {
            return
            t != null &&
            t.IsClass &&
            t.IsVisible &&
            !t.IsAbstract &&
            typeof(IHttpController).IsAssignableFrom(t);
            //&& HasValidControllerName(t);
        }



        public ICollection<Type> GetControllerTypes(IAssembliesResolver assembliesResolver)
        {
            return ComponentsContainer.GetComponents(WebApiControllers).Select(cmpt => cmpt.ComponentType).ToList();

        }

        #endregion ---------------------------- IHttpControllerTypeResolver -------------------------------


        #region ------------------------------------- IException Logger  ----------------------------------------

        public virtual Task LogAsync(ExceptionLoggerContext context, CancellationToken cancellationToken)
        {
            return Task.Factory.StartNew(
                () =>
                {
                    var message = "ERROR in WebApi [{0}] :   Exception:[{1}] \n Stack[{2}]".Fmt(
                             context.Request.RequestUri.AbsoluteUri
                            , context.Exception.Message
                            , context.Exception.StackTrace);

                    Logger.Error(message);
                }
                , cancellationToken
                );

        }
        #endregion ------------------------------------- IException Logger  ----------------------------------------



        public static HttpConfiguration Copy(HttpConfiguration configuration, Action<HttpControllerSettings> settings)
        {
            var controllerSettings = new HttpControllerSettings(configuration);
            settings(controllerSettings);

            // var constructor = typeof(HttpConfiguration).GetConstructor(BindingFlags.NonPublic | BindingFlags.Instance, null, new[] { typeof(HttpConfiguration), typeof(HttpControllerSettings) }, null);
            // typeof(HttpConfiguration)constructor.Invoke(new object[] { configuration, controllerSettings });
            var instance =
                 typeof(HttpConfiguration).CreateInstanceTLazy<HttpConfiguration>(args: new object[] { configuration, controllerSettings });


            return instance;
        }


    }
}


#endif