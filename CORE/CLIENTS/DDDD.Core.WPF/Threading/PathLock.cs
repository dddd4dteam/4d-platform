using System.Collections.Generic;

#if NET45 
namespace DDDD.Core.Threading
{
    /// <example>    
    /// The following code shows the usage of the  PathLock .
    /// <code>
    ///      var pathLock = PathLock.GetLock( type.FullName );
    ///
    ///      using ( await pathLock.LockAsync().ConfigureAwait( false ) )
    ///      {
    ///         using ( var indexFile = _fileHelper.GetReader( indexPath ) )
    ///             {
    ///                 var count = indexFile.ReadInt32();
    ///             }
    ///      }
    /// </code>
    /// </example>
    /// <summary>
    /// Path  Lock
    /// </summary>
    internal static class PathLock
    {
        private static readonly Dictionary<int, AsyncLock> _pathLocks = new Dictionary<int, AsyncLock>();

        public static AsyncLock GetLock( string path )
        {
            var hash = path.GetHashCode();

            lock ( _pathLocks )
            {
                AsyncLock aLock = null;

                if ( _pathLocks.TryGetValue( hash, out aLock ) == false )
                {
                    aLock = _pathLocks[ hash ] = new AsyncLock();
                }

                return aLock;
            }
        }
    }

}

#endif