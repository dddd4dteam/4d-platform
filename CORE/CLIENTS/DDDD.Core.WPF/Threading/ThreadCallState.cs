﻿using DDDD.Core.Resources;
using System;
using System.Collections.Generic;
using System.Windows.Threading;





namespace DDDD.Core.Threading
{

    /// <summary>
    /// TCS(Thread Call State) Item Key En
    /// </summary>
    public enum TCSItemKeyEn
    {

#if WPF|| SL5  ||WP81 
        /// <summary>
        /// UI Dispatcher for CLIENT port
        /// </summary>
        Dispatcher,
#endif
        /// <summary>
        /// 1 Component Key in Task Operation state with type T1 
        /// </summary>
        Item1,

        /// <summary>
        /// 2 Component Key in Task Operation state with type T2 
        /// </summary>
        Item2,
        /// <summary>
        /// 3 Component Key in Task Operation state with type T3
        /// </summary>
        Item3,
        /// <summary>
        /// 4 Component Key in Task Operation state with type T4
        /// </summary>
        Item4,
        /// <summary>
        /// 5 Component Key in Task Operation state with type T5
        /// </summary>
        Item5,
        /// <summary>
        /// 6 Component Key in Task Operation state with type T6
        /// </summary>
        Item6,
        /// <summary>
        /// 7 Component Key in Task Operation state with type T7
        /// </summary>
        Item7,
        /// <summary>
        /// 8 Component Key in Task Operation state with type T8
        /// </summary>
        Item8,
    }



    /// <summary>
    /// ThreadCallState - base abstract class for State object that can be used in Thread/Task Invocation. 
    /// Each component of ThreadCallState wrapped into WeakReference.
    /// </summary>
    public abstract class ThreadCallState
    {

        #region  ----------------------- CTOR -----------------------------
        protected ThreadCallState()
        {
            ItemsDictionary = new Dictionary<TCSItemKeyEn, WeakReference>();
        }
        #endregion ----------------------- CTOR -----------------------------

     
        #region     ----------------------------PROP ITEM  :  ItemsDictionary ---------------------------------
        // PARAMS :
        // PropName   -      ItemsDictionary         EX: AddNewButton     
        // PropType   -      Dictionary<TCSItemKeyEn,object>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Cловарь объектов  состяния         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// ThreadCallState State object components dictionary
        /// </summary>        
        protected Dictionary<TCSItemKeyEn, WeakReference> ItemsDictionary
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  ItemsDictionary ---------------------------------


#if  WPF|| SL5  ||WP81

        #region     ----------------------------PROP ITEM  :  UIdispatcher ---------------------------------

        // PARAMS :
        // PropName   -      UIdispatcher     EX: AddNewButton     
        // PropType   -      Dispatcher       EX: Returning Result Type
        // PropAccess   -    PropAccess       EX: public
        // PropSetAccess   - PropSetAccess    EX: internal
        // PropDesc  -       PropDesc         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Client  UI  Dispatcher 
        /// </summary>        
        public Dispatcher UIDispatcher
        {
            get
            {
                try
                {
                    return (Dispatcher)ItemsDictionary[TCSItemKeyEn.Dispatcher].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException("Task Operation State, Error on getting instance of UIdispatcher: Cast to [Dispatcher] error or UIDispatcher is null");
                }
            }
        }

        #endregion  -------------------------- PROP ITEM  :  UIdispatcher ---------------------------------

#endif

    }



#if  WPF|| SL5  ||WP81

    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (1 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>    
    public class ThreadCallState<T1> : ThreadCallState
    {

        #region  --------------------------------- CTOR------------------------------------

        ThreadCallState() : base() { }

        #endregion --------------------------------- CTOR------------------------------------
        


        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------




        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(1 компонент+ у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>        
        /// <returns></returns>
        public static ThreadCallState<T1> Create(Dispatcher uiDispatcher, T1 item1)
        {
            ThreadCallState<T1> newState = new ThreadCallState<T1>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            return newState;
        }


    }

#endif



    /// <summary> 
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (2 component + UIDispatcher on client side )
    /// </summary>      
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2> : ThreadCallState
    {
        ThreadCallState()
            : base()
        { }


#if     WPF|| SL5  ||WP81


        /// <summary>
        ///Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(2 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2> Create(Dispatcher uiDispatcher, T1 item1, T2 item2)
        {
            ThreadCallState<T1, T2> newState = new ThreadCallState<T1, T2>();

            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            return newState;
        }


#endif


        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception)
                {                    
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR,"1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------




        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object 
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2" ));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------






        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(2 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2> Create(T1 item1, T2 item2)
        {
            ThreadCallState<T1, T2> newState = new ThreadCallState<T1, T2>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            return newState;
        }


    }



    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (3 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2, T3> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(3 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3)
        {
            ThreadCallState<T1, T2, T3> newState = new ThreadCallState<T1, T2, T3>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            return newState;
        }

#endif


        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR,"1", typeof(T1).Name, "1" ));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format( RCX.ThreadCallState_ItemNGetting_ERR,"2", typeof(T2).Name, "2") ); 
                        
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------



        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(3 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3> Create(T1 item1, T2 item2, T3 item3)
        {
            ThreadCallState<T1, T2, T3> newState = new ThreadCallState<T1, T2, T3>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            return newState;
        }



    }



    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (4 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>    
    /// <typeparam name="T4">Component 4 Type in ThreadCallState State object</typeparam>        
    public class ThreadCallState<T1, T2, T3, T4> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки какой-то многокомпонентной операции(4 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3, T4 item4)
        {
            ThreadCallState<T1, T2, T3, T4> newState = new ThreadCallState<T1, T2, T3, T4>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            return newState;
        }

#endif


        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException( String.Format(  RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1" ));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object 
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item4 ---------------------------------
        // PARAMS :
        // PropName   -      Item4         EX: AddNewButton     
        // PropType   -      T4         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 4          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 4 in ThreadCallState - composite State object 
        /// </summary>        
        public T4 Item4
        {
            get
            {
                try
                {
                    return (T4)ItemsDictionary[TCSItemKeyEn.Item4].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "4", typeof(T4).Name, "4"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item4] = new WeakReference((T4)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item4 ---------------------------------




        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(4 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4> Create(T1 item1, T2 item2, T3 item3, T4 item4)
        {
            ThreadCallState<T1, T2, T3, T4> newState = new ThreadCallState<T1, T2, T3, T4>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            return newState;
        }

    }



    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (5 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T4">Component 4 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T5">Component 5 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2, T3, T4, T5> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(5 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            ThreadCallState<T1, T2, T3, T4, T5> newState = new ThreadCallState<T1, T2, T3, T4, T5>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            return newState;
        }

#endif


        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }

        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object 
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2"));
                }
            }

            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }

        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object 
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item4 ---------------------------------
        // PARAMS :
        // PropName   -      Item4         EX: AddNewButton     
        // PropType   -      T4         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 4          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 4 in ThreadCallState - composite State object 
        /// </summary>        
        public T4 Item4
        {
            get
            {
                try
                {
                    return (T4)ItemsDictionary[TCSItemKeyEn.Item4].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "4", typeof(T4).Name, "4"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item4] = new WeakReference((T4)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item4 ---------------------------------


        #region     ----------------------------PROP ITEM :  Item5 ---------------------------------
        // PARAMS :
        // PropName   -      Item5         EX: AddNewButton     
        // PropType   -      T5         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 5          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 5 in ThreadCallState - composite State object 
        /// </summary>        
        public T5 Item5
        {
            get
            {
                try
                {
                    return (T5)ItemsDictionary[TCSItemKeyEn.Item5].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "5", typeof(T5).Name, "5"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item5] = new WeakReference((T5)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item5 ---------------------------------



        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(5 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5)
        {
            ThreadCallState<T1, T2, T3, T4, T5> newState = new ThreadCallState<T1, T2, T3, T4, T5>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            return newState;
        }

    }



    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (6 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T4">Component 4 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T5">Component 5 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T6">Component 6 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2, T3, T4, T5, T6> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(6 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            return newState;
        }

#endif



        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception exc)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object 
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object 
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item4 ---------------------------------
        // PARAMS :
        // PropName   -      Item4         EX: AddNewButton     
        // PropType   -      T4         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 4          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 4 in ThreadCallState - composite State object 
        /// </summary>        
        public T4 Item4
        {
            get
            {
                try
                {
                    return (T4)ItemsDictionary[TCSItemKeyEn.Item4].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "4", typeof(T4).Name, "4"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item4] = new WeakReference((T4)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item4 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item5 ---------------------------------
        // PARAMS :
        // PropName   -      Item5         EX: AddNewButton     
        // PropType   -      T5         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 5          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 5 in ThreadCallState - composite State object 
        /// </summary>        
        public T5 Item5
        {
            get
            {
                try
                {
                    return (T5)ItemsDictionary[TCSItemKeyEn.Item5].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "5", typeof(T5).Name, "5"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item5] = new WeakReference((T5)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item5 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item6 ---------------------------------
        // PARAMS :
        // PropName   -      Item6         EX: AddNewButton     
        // PropType   -      T6         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 6          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 6 in ThreadCallState - composite State object
        /// </summary>        
        public T6 Item6
        {
            get
            {
                try
                {
                    return (T6)ItemsDictionary[TCSItemKeyEn.Item6].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "6", typeof(T6).Name, "6"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item6] = new WeakReference((T6)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item6 ---------------------------------



        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(6 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            return newState;
        }




    }




    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (7 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T4">Component 4 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T5">Component 5 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T6">Component 6 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T7">Component 7 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2, T3, T4, T5, T6, T7> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(7 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <param name="item7"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6, T7> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6, T7> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6, T7>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item7, new WeakReference(item7));
            return newState;
        }

#endif



        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object 
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object 
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item4 ---------------------------------
        // PARAMS :
        // PropName   -      Item4         EX: AddNewButton     
        // PropType   -      T4         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 4          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 4 in ThreadCallState - composite State object 
        /// </summary>        
        public T4 Item4
        {
            get
            {
                try
                {
                    return (T4)ItemsDictionary[TCSItemKeyEn.Item4].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "4", typeof(T4).Name, "4"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item4] = new WeakReference((T4)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item4 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item5 ---------------------------------
        // PARAMS :
        // PropName   -      Item5         EX: AddNewButton     
        // PropType   -      T5         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 5          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 5 in ThreadCallState - composite State object
        /// </summary>        
        public T5 Item5
        {
            get
            {
                try
                {
                    return (T5)ItemsDictionary[TCSItemKeyEn.Item5].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "5", typeof(T5).Name, "5"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item5] = new WeakReference((T5)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item5 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item6 ---------------------------------
        // PARAMS :
        // PropName   -      Item6         EX: AddNewButton     
        // PropType   -      T6         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 6          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 6 in ThreadCallState - composite State object
        /// </summary>        
        public T6 Item6
        {
            get
            {
                try
                {
                    return (T6)ItemsDictionary[TCSItemKeyEn.Item6].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "6", typeof(T6).Name, "6"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item6] = new WeakReference((T6)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item6 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item7 ---------------------------------
        // PARAMS :
        // PropName   -      Item6         EX: AddNewButton     
        // PropType   -      T7         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 7          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 7 in ThreadCallState - composite State object
        /// </summary>        
        public T7 Item7
        {
            get
            {
                try
                {
                    return (T7)ItemsDictionary[TCSItemKeyEn.Item7].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "7", typeof(T7).Name, "7"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item7] = new WeakReference((T7)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item6 ---------------------------------


        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(7 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <param name="item7"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6, T7> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6, T7> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6, T7>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item7, new WeakReference(item7));
            return newState;
        }




    }



    /// <summary>
    /// ThreadCallState - Generic State object that can be used in Thread/Task Invocation as composite State object. (8 component + UIDispatcher on client side )
    /// </summary>
    /// <typeparam name="T1">Component 1 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T2">Component 2 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T3">Component 3 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T4">Component 4 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T5">Component 5 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T6">Component 6 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T7">Component 7 Type in ThreadCallState State object</typeparam>
    /// <typeparam name="T8">Component 8 Type in ThreadCallState State object</typeparam>
    public class ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8> : ThreadCallState
    {

#if  WPF|| SL5  ||WP81

        /// <summary>
        /// Состояние для вызова в потоке|работает также с Таск-ами. Предназначен для упаковки  какой-то многокомпонентной операции(8 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="uiDispatcher"></param>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <param name="item7"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8> Create(Dispatcher uiDispatcher, T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7,T8 item8)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Dispatcher, new WeakReference(uiDispatcher));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item7, new WeakReference(item7));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item8, new WeakReference(item8));
            return newState;
        }

#endif



        #region     ----------------------------PROP ITEM  :  Item1 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 1          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 1 in ThreadCallState - composite State object 
        /// </summary>        
        public T1 Item1
        {
            get
            {
                try
                {
                    return (T1)ItemsDictionary[TCSItemKeyEn.Item1].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "1", typeof(T1).Name, "1"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item1] = new WeakReference((T1)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item1 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item2 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 2          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 2 in ThreadCallState - composite State object 
        /// </summary>        
        public T2 Item2
        {
            get
            {
                try
                {
                    return (T2)ItemsDictionary[TCSItemKeyEn.Item2].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "2", typeof(T2).Name, "2"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item2] = new WeakReference((T2)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item2 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item3 ---------------------------------
        // PARAMS :
        // PropName   -      Item1         EX: AddNewButton     
        // PropType   -      T1         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 3          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 3 in ThreadCallState - composite State object 
        /// </summary>        
        public T3 Item3
        {
            get
            {
                try
                {
                    return (T3)ItemsDictionary[TCSItemKeyEn.Item3].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "3", typeof(T3).Name, "3"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item3] = new WeakReference((T3)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item3 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item4 ---------------------------------
        // PARAMS :
        // PropName   -      Item4         EX: AddNewButton     
        // PropType   -      T4         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 4          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 4 in ThreadCallState - composite State object 
        /// </summary>        
        public T4 Item4
        {
            get
            {
                try
                {
                    return (T4)ItemsDictionary[TCSItemKeyEn.Item4].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "4", typeof(T4).Name, "4"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item4] = new WeakReference((T4)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item4 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item5 ---------------------------------
        // PARAMS :
        // PropName   -      Item5         EX: AddNewButton     
        // PropType   -      T5         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 5          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 5 in ThreadCallState - composite State object
        /// </summary>        
        public T5 Item5
        {
            get
            {
                try
                {
                    return (T5)ItemsDictionary[TCSItemKeyEn.Item5].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "5", typeof(T5).Name, "5"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item5] = new WeakReference((T5)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item5 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item6 ---------------------------------
        // PARAMS :
        // PropName   -      Item6         EX: AddNewButton     
        // PropType   -      T6         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 6          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 6 in ThreadCallState - composite State object 
        /// </summary>        
        public T6 Item6
        {
            get
            {
                try
                {
                    return (T6)ItemsDictionary[TCSItemKeyEn.Item6].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "6", typeof(T6).Name, "6"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item6] = new WeakReference((T6)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item6 ---------------------------------


        #region     ----------------------------PROP ITEM  :  Item7 ---------------------------------
        // PARAMS :
        // PropName   -      Item6         EX: AddNewButton     
        // PropType   -      T7         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 7          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 7 in ThreadCallState - composite State object 
        /// </summary>        
        public T7 Item7
        {
            get
            {
                try
                {
                    return (T7)ItemsDictionary[TCSItemKeyEn.Item7].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "7", typeof(T7).Name, "7"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item7] = new WeakReference((T7)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item7 ---------------------------------
        

        #region     ----------------------------PROP ITEM  :  Item8 ---------------------------------
        // PARAMS :
        // PropName   -      Item8         EX: AddNewButton     
        // PropType   -      T8         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступ к объекту 8          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Component 8 in ThreadCallState - composite State object
        /// </summary>        
        public T8 Item8
        {
            get
            {
                try
                {
                    return (T8)ItemsDictionary[TCSItemKeyEn.Item8].Target;
                }
                catch (Exception)
                {
                    throw new InvalidCastException(String.Format(RCX.ThreadCallState_ItemNGetting_ERR, "8", typeof(T8).Name, "8"));
                }
            }
            set
            {
                ItemsDictionary[TCSItemKeyEn.Item8] = new WeakReference((T8)value);
            }
        }

        #endregion  -------------------------- PROP ITEM  :  Item8 ---------------------------------



        /// <summary>
        /// Состояние для обработки Таск-ами какой-то многокомпонентной операции(8 компонентов + у Клиента UIDispatcher)
        /// </summary>
        /// <param name="item1"></param>
        /// <param name="item2"></param>
        /// <param name="item3"></param>
        /// <param name="item4"></param>
        /// <param name="item5"></param>
        /// <param name="item6"></param>
        /// <param name="item7"></param>
        /// <param name="item8"></param>
        /// <returns></returns>
        public static ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8> Create(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6, T7 item7,T8 item8)
        {
            ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8> newState = new ThreadCallState<T1, T2, T3, T4, T5, T6, T7, T8>();
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item1, new WeakReference(item1));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item2, new WeakReference(item2));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item3, new WeakReference(item3));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item4, new WeakReference(item4));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item5, new WeakReference(item5));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item6, new WeakReference(item6));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item7, new WeakReference(item7));
            newState.ItemsDictionary.Add(TCSItemKeyEn.Item8, new WeakReference(item8));
            return newState;
        }




    }



}
