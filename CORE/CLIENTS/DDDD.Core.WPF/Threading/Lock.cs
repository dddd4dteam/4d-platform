﻿using System;
using System.Diagnostics;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;


namespace DDDD.Core.Threading
{


    /// <summary>
    /// Class that contains wraps for [lock(){ Action - YOUR Action HERE}]  construction usings.
    /// </summary>
    internal class Lock
    {


        #region --------------------------- NESTED ARGVALIDATOR -------------------------------
        /// <summary>
        ///  NESTED ARG Validator class
        /// </summary>
        class ArgValidator
        {

            /// To get Message we use template from Default ResourceManager by Validator class.
            /// Message template Key: [T-Validator] + [Operation Name - Operation].                        

            const string ERR_NullReferenceValue = "ERROR in {0}.{1}(): argument [{2}] value cannot be null.";
            const string ERR_InvalidOperation = "ERROR in {0}.{1}(): invalid operation - [{2}]";

            const string Class = nameof(Lock);


            /// <summary>
            /// If  nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException.  Works only in DEBUG mode.
            /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
            /// </summary>
            /// <param name="nullChekingItem"></param>
            /// <param name="methodName"></param>
            /// <param name="parameterName"></param>
            [Conditional("DEBUG")]
            public static void ATNullReferenceArgDbg(object nullChekingItem, string methodName, string parameterName)
            {
                if (nullChekingItem.IsNull())
                {
                    throw new NullReferenceException(ERR_NullReferenceValue.Fmt(Class, methodName, parameterName));
                }
            }


            /// <summary>
            /// If  nullChekingItem IS NOT NULL - (FALSE that nullChekingItem.IsNull()- .IsNotNull()) then throw NullReferenceException.  Works only in DEBUG mode.
            /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
            /// </summary>
            /// <param name="nullChekingItem"></param>
            /// <param name="methodName"></param>
            /// <param name="parameterName"></param>
            [Conditional("DEBUG")]
            public static void AssertFalse_NullReferenceArgDbg(object nullChekingItem, string methodName, string parameterName)
            {
                if (nullChekingItem.IsNotNull())
                {
                    throw new NullReferenceException(ERR_NullReferenceValue.Fmt(Class, methodName, parameterName));
                }
            }




            /// <summary>
            /// If condition is TRUE - then throw InvalidOperationException. Works only in DEBUG mode.
            /// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].".
            /// </summary>
            /// <param name="condition"></param>
            /// <param name="methodName"></param>
            /// <param name="userMessage"></param>
            [Conditional("DEBUG")]
            public static void ATInvalidOperationDbg(bool condition, string methodName, string userMessage)
            {
                if (condition)
                {
                    throw new InvalidOperationException(ERR_InvalidOperation.Fmt(Class, methodName, userMessage));
                }
            }



            /// <summary>
            /// If condition is FALSE - then throw InvalidOperationException. Works only in DEBUG mode.
            /// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].". 
            /// </summary>
            /// <param name="condition"></param>
            /// <param name="methodName"></param>
            /// <param name="userMessage"></param>
            [Conditional("DEBUG")]
            public static void AssertFalse_InvalidOperationDbg(bool condition, string methodName, string userMessage)
            {
                if (!condition)
                {
                    throw new InvalidOperationException(ERR_InvalidOperation.Fmt(Class, methodName, userMessage));
                }
            }





        }// END NESTED ARG Validator class


        #endregion --------------------------- NESTED ARGVALIDATOR -------------------------------





        /// <summary>
        ///  Here we  run locked Action inside construction - [lock(){ Action}]. 
        /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True; and  Action will be called if  (conditionToUseLockAndToDoAction) is True;
        /// <para/> In DEBUG mode this method checks that lockingObject and lockedAction argument values is not null, else ArgumentNullException will be thrown.
        /// </summary>
        /// <param name="conditionToUseLockAndToDoAction">[lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True,  and  Action will be called if(conditionToUseLockAndToDoAction) is True</param>
        /// <param name="lockingObject">object for [lock(item){}] construction</param>
        /// <param name="lockedAction">Action inside locked block  - [lock(item){ YOUR Action HERE }]</param>
        public static void LockAction(Func<bool> conditionToUseLockAndToDoAction, object lockingObject, Action lockedAction)
        {
            ArgValidator.ATNullReferenceArgDbg(lockingObject, nameof(LockAction), nameof(lockingObject));
            ArgValidator.ATNullReferenceArgDbg(lockedAction, nameof(LockAction), nameof(lockedAction));

            if (conditionToUseLockAndToDoAction())
            {
                lock (lockingObject)
                {
                    if (conditionToUseLockAndToDoAction())
                    {
                        lockedAction();
                    }

                }

            }

        }
        

        /// <summary>
        ///  Here we  run locked Action inside construction - [lock(){ Action}]. 
        /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLock) is True; Action will be called if  (conditionInsideLock) is True;
        /// <para/> In DEBUG mode this method checks that lockingObject and lockedAction argument values is not null, else ArgumentNullException will be thrown.
        /// </summary>
        /// <param name="conditionToUseLock">[lock(){}]  will be organized if (conditionToUseLock) is True</param>
        /// <param name="conditionInsideLock">Action will be called if  (conditionInsideLock) is True</param>
        /// <param name="lockingObject">object for [lock(item){}] construction</param>
        /// <param name="lockedAction">Action inside locked block  - [lock(item){ YOUR Action HERE }]</param>
        public static void LockAction2(Func<bool> conditionToUseLock, Func<bool> conditionInsideLock, object lockingObject, Action lockedAction)
        {
            ArgValidator.ATNullReferenceArgDbg(lockingObject, nameof(LockAction), nameof(lockingObject));
            ArgValidator.ATNullReferenceArgDbg(lockedAction, nameof(LockAction), nameof(lockedAction));

            if (conditionToUseLock())
            {
                lock(lockingObject)
                {
                    if (conditionInsideLock())
                    {
                        lockedAction();
                    }
                }

            }

        }


       




    }



    #region ---------------------------------GARBAGE ---------------------------------------


    /// <summary>
    /// Here we  run locked Func{T} inside construction - [lock(){ Func{T}}].
    /// <para/> Lock - [lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True; and  Func{T} will be called if  (conditionToUseLockAndToDoAction) is True;
    /// <para/> In DEBUG mode this method checks that lockingObject and lockedFunc argument values is not null, else ArgumentNullException will be thrown.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="lockingObject">object for [lock(item){}] construction</param>
    /// <param name="conditionToUseLockAndToDoAction">[lock(){}]  will be organized if (conditionToUseLockAndToDoAction) is True,  and Func{T} will be called if(conditionToUseLockAndToDoAction) is True</param>
    /// <param name="lockedFunc">Func{T} inside locked block  - [lock(item){ YOUR Action HERE }]</param>
    /// <returns></returns>
    //public static T LockFunc<T>(bool conditionToUseLockAndToDoAction, object lockingObject, Func<T> lockedFunc)
    //{
    //    Validator.AssertTrueDbg<ArgumentNullException>(lockingObject.IsNull(), ERR_ArgNull.Fmt(nameof(Lock), nameof(LockFunc), nameof(lockingObject)));
    //    Validator.AssertTrueDbg<ArgumentNullException>(lockedFunc.IsNull(), ERR_ArgNull.Fmt(nameof(Lock), nameof(LockFunc), nameof(lockedFunc)));


    //    if (conditionToUseLockAndToDoAction)
    //    {
    //        lock (lockingObject)
    //        {
    //            if (conditionToUseLockAndToDoAction)
    //            {
    //               return lockedFunc();
    //            }

    //        }
    //    }

    //    //throw new InstanceLockLostException("{0}.{1}(): error - not  Lock Losted for locking object of [{2}] ".Fmt(nameof(Lock), nameof(LockFunc), lockingObject.GetType().DeclaringType.FullName)   );

    //}

    

    #endregion ---------------------------------GARBAGE ---------------------------------------


}
