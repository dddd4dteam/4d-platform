﻿using System;
using System.Threading;
using System.Windows;
using System.Diagnostics.Contracts;
using System.Windows.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.Threading
{



    /// <summary>
    /// UI thread delegate invoker - asynchronously and synchronously 
    /// </summary>
    public static class UIInvoker
    {

        #region  ---------------------------- CTOR -----------------------------

        static UIInvoker()
        {
            Initialize();
        }

        #endregion ---------------------------- CTOR -----------------------------
        

#if   WPF || NET45

        /// <summary>
        /// Threading Dispatcher to do synchronized  with UI calls.
        /// <para/> Shortcut of Application.Current.Dispatcher.
        /// </summary>
        public static Dispatcher UIDispatcher
        {
            get { return Application.Current.Dispatcher; } 
        }

        /// <summary>
        /// Threading DispatcherSynchronizationContext to do synchronized  with UI calls .
        /// <para/> Will be Inited on first property get access.
        /// </summary>
        public static DispatcherSynchronizationContext UISyncContext
        {
            get;
            private set;
        }



        /// <summary>
        /// Private Initialization of UIInvoker
        /// </summary>
        static void Initialize()
        {
            Contract.Requires(UIDispatcher != null, "UIInvoker.Initialize(): UIDispatcher cannot be null to Initialize UISyncContext");
            Contract.Ensures(UISyncContext != null, "UIInvoker.Initialize(): UISyncContext cannot be null on output");

            UISyncContext = new DispatcherSynchronizationContext(UIDispatcher);                       

        }

        #region  --------------------------------------- Invoking Asynchronously ---------------------------------------



        public static Action WrapAction(this Action currentAction)
        {
            if (currentAction == null) return null;

            Action HandledAction = () =>
            {
                currentAction();               
            };

            return HandledAction;
        }

        public static Action<T1> WrapAction<T1>(this Action<T1> currentAction, T1 ParamT1)
        {
            if (currentAction == null) return null;

            Action<T1> HandledAction = (prm) =>
            {
                currentAction(prm);                
            };

            return HandledAction;
        }







        /// <summary>
        /// Invoke Delegate - SendOrPostCallback asynchronously to UI, also we can use custom state object
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        public static void InvokeAsynchronously(SendOrPostCallback callback, object state)
        {
            //ArgumentValidator.EnsureNotNull(callback, "callback");
            if (callback == null) return;

            UISyncContext.Post(callback, state);
        }


        public static void InvokeAsynchronously(Action action, bool WrapToSafeHandler = false)
        {
            if (action == null) return;

            if (WrapToSafeHandler)
            {
                action = action.WrapAction();
            }

            if (UIDispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                UIDispatcher.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }




        /// <summary>
        /// Invoke Delegate -Action asynchronously to UI
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeAsynchronously<T1>(Action<T1> action, T1 p1)
        {

            if (action == null) return;


            if (UIDispatcher.CheckAccess())
            {
                action(p1);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }

        }



        public static void InvokeAsynchronously<T1, T2>(Action<T1, T2> action, T1 p1, T2 p2)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2);
            }
            else
            {
                UIDispatcher.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3>(Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3);
            }
            else
            {
                UIDispatcher.BeginInvoke(action ,  DispatcherPriority.Normal);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3, T4 p4)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4);
            }
            else
            {
                UIDispatcher.BeginInvoke(action ,  DispatcherPriority.Normal);
            }
        }

        public static void InvokeAsynchronously<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5);
            }
            else
            {
                UIDispatcher.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5, p6);
            }
            else
            {
                UIDispatcher.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5, p6, p7);
            }
            else
            {
                UIDispatcher.BeginInvoke(action, DispatcherPriority.Normal);
            }
        }






        #endregion --------------------------------------- Invoking Asynchronously ---------------------------------------


#endif


        /// <summary>
        /// DoCrossThread call with  SynchronizationContext
        /// </summary>
        /// <param name="action"></param>
        /// <param name="state"></param>
        public static void TryDoCrossThreadingCall(Action action, object state)
        {
            // Get the current SynchronizationContext.
            // NOTE: Must make the call on the UI thread, NOT
            // the background thread to get the proper
            // context.
            SynchronizationContext context = SynchronizationContext.Current;

            // Start some work on a new Task (4.0)
            Task.Factory.StartNew(
                () =>
                {
                    // Do some lengthy operation.
                    //...

                    context.Post(c => action(), state);
                    // Notify the user.  Do not need to wait.
                    //context.Post(o => MessageBox.Show("Progress"));

                    // Do some more stuff.
                    //...

                    // Wait on result.
                    // Notify the user.
                    context.Send(s => action(), state);
                    //context.Send(o => MessageBox.Show("Progress, waiting on OK"));
                }
                  );
        }


#if (SL5 || WP81)

        
     

        #region  ---------------------------- PROPERTIES  ---------------------------------

        /// <summary>
        /// Threading Dispatcher to do synchronized  with UI calls.
        /// <para/> Shortcut of Application.Current.Dispatcher.
        /// </summary>
        public static Dispatcher UIDispatcher
        {
            get { return Deployment.Current.Dispatcher; }
        }


        /// <summary>
        /// Threading DispatcherSynchronizationContext to do synchronized  with UI calls .
        /// <para/> Will be Inited on first property get access.
        /// </summary>
        public static DispatcherSynchronizationContext UISyncContext
        {
            get;// { return _UISyncContext; }
            private set;
        }



        #endregion  ---------------------------- PROPERTIES  ---------------------------------



        // THREADINGS  SYNCHRONIZATION
        #region  -------------------------------METHODS  -----------------------------------

        /// <summary>
        /// Private Initialization of UIInvoker
        /// </summary>
        static void Initialize()
        {
            Contract.Requires(UIDispatcher != null, "UIInvoker.Initialize(): UIDispatcher cannot be null to Initialize UISyncContext");
            Contract.Ensures(UISyncContext != null, "UIInvoker.Initialize(): UISyncContext cannot be null on output");

            UISyncContext = new DispatcherSynchronizationContext(UIDispatcher);


        }


        #region  --------------------------------------- Invoking Asynchronously ---------------------------------------




        public static Action WrapAction(this Action currentAction)
        {
            if (currentAction == null) return null;
            
            Action HandledAction = ()=>
                {
                    currentAction();                    
                };

            return HandledAction;
        }

        public static Action<T1> WrapAction<T1>(this Action<T1> currentAction, T1 ParamT1)
        {
            if (currentAction == null) return null;
            
            Action<T1> HandledAction = (prm)=>
                {                   
                  currentAction(prm);                   
                };

            return HandledAction;
        }


       




        /// <summary>
        /// Invoke Delegate - SendOrPostCallback asynchronously to UI, also we can use custom state object
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        public static void InvokeAsynchronously(SendOrPostCallback callback, object state)
        {
            //ArgumentValidator.EnsureNotNull(callback, "callback");
            if (callback == null) return;

            UISyncContext.Post(callback, state);
        }


        public static void InvokeAsynchronously(Action action, bool WrapToSafeHandler = false)
        {
            if (action == null) return;
            
            if (WrapToSafeHandler)
            {  action = action.WrapAction();
            }
            
            if (UIDispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }




        /// <summary>
        /// Invoke Delegate -Action asynchronously to UI
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeAsynchronously<T1>(Action<T1> action, T1 p1)
        {

               if (action == null) return; 
                

               if (UIDispatcher.CheckAccess())
               {
                   action(p1);
               }
               else
               {
                   UIDispatcher.BeginInvoke(action);
               }                 
            
        }



        public static void InvokeAsynchronously<T1, T2>(Action<T1, T2> action, T1 p1, T2 p2)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1,p2);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3>(Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2,p3);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4>(Action<T1, T2, T3, T4> action, T1 p1, T2 p2, T3 p3,T4 p4)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3,p4);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }

        public static void InvokeAsynchronously<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4,T5 p5)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5,T6 p6)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5, p6);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }


        public static void InvokeAsynchronously<T1, T2, T3, T4, T5, T6,T7>(Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
        {
            if (action == null) return;

            if (UIDispatcher.CheckAccess())
            {
                action(p1, p2, p3, p4, p5, p6, p7);
            }
            else
            {
                UIDispatcher.BeginInvoke(action);
            }
        }






        #endregion --------------------------------------- Invoking Asynchronously ---------------------------------------


      


        #region ------------------------------ InvokingSynchronously Action ---------------------------------

        /// <summary>
        /// Invoke Delegate - SendOrPostCallback synchronously to UI, also we can use custom state object
        /// </summary>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        public static void InvokeSynchronously(SendOrPostCallback callback, object state)
        {
            //ArgumentValidator.EnsureNotNull(callback, "callback");

            UISyncContext.Send(callback, state);
        }


        /// <summary>
        /// Invoking Synchronously to UI | 1_Param
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        public static void InvokeSynchronously<T1>(Action<T1> action, T1 p1)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1>, T1> state = ThreadCallState<Action<T1>, T1>.Create(UIDispatcher, action, p1);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1>, T1> stIn = st as ThreadCallState<Action<T1>, T1>;

                    stIn.Item1(stIn.Item2);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }



        /// <summary>
        /// Invoking Synchronously to UI | 2_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        public static void InvokeSynchronously<T1,T2>(Action<T1,T2> action, T1 p1, T2 p2)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1,T2>, T1, T2> state = ThreadCallState<Action<T1,T2>, T1,T2>.Create(UIDispatcher, action, p1, p2);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2>, T1, T2> stIn = st as ThreadCallState<Action<T1, T2>, T1, T2>;

                    stIn.Item1(stIn.Item2, stIn.Item3);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }

        /// <summary>
        /// Invoking Synchronously to UI | 3_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        public static void InvokeSynchronously<T1, T2, T3>(Action<T1, T2, T3> action, T1 p1, T2 p2, T3 p3)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2, p3);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1, T2, T3>, T1, T2, T3> state = ThreadCallState<Action<T1, T2, T3>, T1, T2, T3>.Create(UIDispatcher, action, p1, p2, p3);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2, T3>, T1, T2, T3> stIn = st as ThreadCallState<Action<T1, T2, T3>, T1, T2, T3>;

                    stIn.Item1(stIn.Item2, stIn.Item3, stIn.Item4);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }



        /// <summary>
        /// Invoking Synchronously to UI | 4_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        public static void InvokeSynchronously<T1, T2, T3, T4>(Action<T1, T2, T3,T4> action, T1 p1, T2 p2, T3 p3,T4 p4)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2, p3, p4);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1, T2, T3, T4>, T1, T2, T3, T4> state = ThreadCallState<Action<T1, T2, T3, T4>, T1, T2, T3 , T4>.Create(UIDispatcher, action, p1, p2, p3, p4);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2, T3, T4>, T1, T2, T3, T4> stIn = st as ThreadCallState<Action<T1, T2, T3, T4>, T1, T2, T3, T4>;

                    stIn.Item1(stIn.Item2, stIn.Item3, stIn.Item4, stIn.Item5);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }


        /// <summary>
        /// Invoking Synchronously to UI | 5_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        public static void InvokeSynchronously<T1, T2, T3, T4, T5>(Action<T1, T2, T3, T4, T5> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2, p3, p4, p5);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1, T2, T3, T4, T5>, T1, T2, T3, T4, T5> state = ThreadCallState<Action<T1, T2, T3, T4, T5>, T1, T2, T3, T4, T5>.Create(UIDispatcher, action, p1, p2, p3, p4, p5);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2, T3, T4, T5>, T1, T2, T3, T4, T5> stIn = st as ThreadCallState<Action<T1, T2, T3, T4, T5>, T1, T2, T3, T4, T5>;

                    stIn.Item1(stIn.Item2, stIn.Item3, stIn.Item4, stIn.Item5,stIn.Item6);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }


        /// <summary>
        /// Invoking Synchronously to UI | 6_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <param name="p6"></param>
        public static void InvokeSynchronously<T1, T2, T3, T4, T5, T6>(Action<T1, T2, T3, T4, T5, T6> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2, p3, p4, p5, p6);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1, T2, T3, T4, T5, T6>, T1, T2, T3, T4, T5, T6> state = ThreadCallState<Action<T1, T2, T3, T4, T5,T6>, T1, T2, T3, T4, T5,T6>.Create(UIDispatcher, action, p1, p2, p3, p4, p5, p6);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2, T3, T4, T5, T6>, T1, T2, T3, T4, T5, T6> stIn = st as ThreadCallState<Action<T1, T2, T3, T4, T5, T6>, T1, T2, T3, T4, T5, T6>;

                    stIn.Item1(stIn.Item2, stIn.Item3, stIn.Item4, stIn.Item5, stIn.Item6,stIn.Item7);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }


        /// <summary>
        /// Invoking Synchronously to UI | 7_Params
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <typeparam name="T4"></typeparam>
        /// <typeparam name="T5"></typeparam>
        /// <typeparam name="T6"></typeparam>
        /// <typeparam name="T7"></typeparam>
        /// <param name="action"></param>
        /// <param name="p1"></param>
        /// <param name="p2"></param>
        /// <param name="p3"></param>
        /// <param name="p4"></param>
        /// <param name="p5"></param>
        /// <param name="p6"></param>
        /// <param name="p7"></param>
        public static void InvokeSynchronously<T1, T2, T3, T4, T5, T6, T7>(Action<T1, T2, T3, T4, T5, T6, T7> action, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6, T7 p7)
        {
            SynchronizationContext currentContext = SynchronizationContext.Current;

            // Dispatcher is free to access UI and currentSyncContext is - UISyncContext
            if (UIDispatcher.CheckAccess() && currentContext != null && UISyncContext == currentContext)
            {
                action(p1, p2, p3, p4, p5, p6, p7);
            }
            // Or Dispatcher cant be accessd |Or currentSyncContext is not UISyncContext
            else if (UIDispatcher.CheckAccess() == false || UISyncContext != currentContext)
            {
                //create state with 1 param -p1
                ThreadCallState<Action<T1, T2, T3, T4, T5, T6, T7>, T1, T2, T3, T4, T5, T6, T7> state = ThreadCallState<Action<T1, T2, T3, T4, T5, T6, T7>, T1, T2, T3, T4, T5, T6, T7>.Create(UIDispatcher, action, p1, p2, p3, p4, p5, p6, p7);

                //create common delegate
                SendOrPostCallback wrappedDelegate = (st) =>
                {
                    ThreadCallState<Action<T1, T2, T3, T4, T5, T6, T7>, T1, T2, T3, T4, T5, T6, T7> stIn = st as ThreadCallState<Action<T1, T2, T3, T4, T5, T6, T7>, T1, T2, T3, T4, T5, T6, T7>;

                    stIn.Item1(stIn.Item2, stIn.Item3, stIn.Item4, stIn.Item5, stIn.Item6, stIn.Item7, stIn.Item8);
                };

                UISyncContext.Send(wrappedDelegate, state);
            }

        }



        #endregion ------------------------------ InvokingSynchronously Action ---------------------------------



        /// <summary>
        /// Invoke Delegate -Action synchronously to UI
        /// </summary>
        /// <param name="action"></param>
        public static void InvokeSynchronously(Action action)
        {
            //ArgumentValidator.EnsureNotNull(action, "action");

            if (UIDispatcher.CheckAccess())
            {
                action();
            }
            else
            {
                UISyncContext.Send(delegate { action(); }, null);
            }
        }

        #endregion --------------------------------METHODS  -----------------------------------

#endif

    }
}
