﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Converters
{
    /// <summary>
    /// Custom converter for converting a Single type to/from string.
    /// </summary>
    public class SingleTypeConverter : TypeConverter
    {
        public SingleTypeConverter()
        {
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            object obj;
            if (!(value is string))
            {
                return base.ConvertFrom(context, culture, value);
            }
            try
            {
                obj = float.Parse(value.ToString(), culture);
            }
            catch
            {
                throw new InvalidCastException();
            }
            return obj;
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return ((float)value).ToString(culture);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

}
