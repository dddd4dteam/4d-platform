﻿using System;
using System.ComponentModel;
using System.Globalization;

namespace DDDD.Core.Converters
{
    /// <summary>
    /// Custom converter for converting a DateTime type to/from string.
    /// </summary>
    public class DateTimeTypeConverter : TypeConverter
    {
        public DateTimeTypeConverter()
        {
        }

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType.Equals(typeof(string)))
            {
                return true;
            }
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return true;
            }
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            object obj;
            if (!(value is string))
            {
                return base.ConvertFrom(context, culture, value);
            }
            try
            {
                obj = DateTime.Parse(value.ToString(), culture);
            }
            catch
            {
                throw new InvalidCastException();
            }
            return obj;
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType.Equals(typeof(string)))
            {
                return ((DateTime)value).ToString(culture);
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
