﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Media;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;


namespace DDDD.Core.Converters
{

    /// <summary>
    /// Custom converter for converting a nullable type to/from string.
    /// </summary>
    /// <typeparam name="T">The value type that is the underlying type for the nullable</typeparam>
    public class NullableConverter<T> : TypeConverter
        where T : struct
    {

        #region  ----------------------- CTOR -------------------------

        static NullableConverter()
        {
            NullableConverter<T>._converter = NullableConverter<T>.GetConverter(typeof(T));
        }

        public NullableConverter()
        {
        }

        #endregion ----------------------- CTOR -------------------------

        private readonly static TypeConverter _converter;

    

        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if ( NullableConverter<T>._converter != null &&  NullableConverter<T>._converter.CanConvertFrom(context, sourceType))
            {
                return true;
            }
            if (sourceType == typeof(T))
            {
                return true;
            }
            return sourceType == typeof(string);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if ( NullableConverter<T>._converter != null &&  NullableConverter<T>._converter.CanConvertTo(context, destinationType))
            {
                return true;
            }
            if (destinationType == typeof(T))
            {
                return true;
            }
            return destinationType == typeof(string);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            object obj;
            if (value is T || value == null)
            {
                return value;
            }
            string str = value as string;
            if (str == null)
            {
                if ( NullableConverter<T>._converter != null &&  NullableConverter<T>._converter.CanConvertFrom(context, value.GetType()))
                {
                    return  NullableConverter<T>._converter.ConvertFrom(context, culture, value);
                }
                return base.ConvertFrom(context, culture, value);
            }
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }
            if (typeof(T).IsEnum)
            {
                try
                {
                    obj = Enum.Parse(typeof(T), str, false);
                }
                catch
                {
                    throw new ArgumentException(string.Format("LE_InvalidEnumValue", str, typeof(T).Name)); //SR.GetString(   -  [12/15/2016 A1]
                }
                return obj;
            }
            if ( NullableConverter<T>._converter != null &&  NullableConverter<T>._converter.CanConvertFrom(context, typeof(string)))
            {
                return  NullableConverter<T>._converter.ConvertFrom(context, culture, value);
            }
            return Convert.ChangeType(value, typeof(T), culture);
        }

        public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
        {
            if (value != null && value.GetType() == destinationType)
            {
                return value;
            }
            if (destinationType != typeof(string))
            {
                if ( NullableConverter<T>._converter != null &&  NullableConverter<T>._converter.CanConvertFrom(context, destinationType))
                {
                    return NullableConverter<T>._converter.ConvertTo(context, culture, value, destinationType);
                }
                return base.ConvertTo(context, culture, value, destinationType);
            }
            if (value == null)
            {
                return string.Empty;
            }
            if ( NullableConverter<T>._converter == null || ! NullableConverter<T>._converter.CanConvertTo(context, destinationType))
            {
                return Convert.ChangeType(value, destinationType, culture);
            }
            return  NullableConverter<T>._converter.ConvertTo(context, culture, value, destinationType);
        }

        private static TypeConverter GetConverter(Type type)
        {
            bool isEnum = type.IsEnum;
            if (type == null || IsKnownType(type) || isEnum || type == typeof(object)) //CoreUtilities. //  [12/27/2016 A1]
            {
                return null;
            }
            TypeConverter typeConverter = null;
            try
            {
                object[] customAttributes = type.GetCustomAttributes(typeof(TypeConverterAttribute), true);
                if (customAttributes != null && (int)customAttributes.Length > 0)
                {
                    TypeConverterAttribute typeConverterAttribute = customAttributes[0] as TypeConverterAttribute;
                    Type type1 = Type.GetType(typeConverterAttribute.ConverterTypeName, false);
                    if (type1 != null)
                    {
                        typeConverter = TypeActivator.CreateInstanceTBaseLazy<TypeConverter>(type1); //(TypeConverter)Activator.CreateInstance(type1);
                    }
                }
            }
            catch
            {
            }
            return typeConverter;
        }



        /// <summary>
        /// Checks if the type is a known type (to Infragistics controls).
        /// </summary>
        internal static bool IsKnownType(Type type)
        {
            if (type == null)
            {
                return false;
            }
            type = type.GetWorkingTypeFromNullableType();// CoreUtilities.GetUnderlyingType(type);
            if (!type.IsPrimitive && typeof(decimal) != type && typeof(string) != type && typeof(DateTime) != type && typeof(DayOfWeek) != type && typeof(Color) != type && !typeof(Brush).IsAssignableFrom(type))
            {
                return false;
            }
            return true;
        }


    }


}
