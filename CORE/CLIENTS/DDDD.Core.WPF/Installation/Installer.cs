﻿


using System;
using System.IO;
using System.Linq;
//using System.Text;
using System.Threading.Tasks;
using System.Windows;
using DDDD.Core.Environment;
using DDDD.Core.Extensions;
using DDDD.Core.Notification;
using DDDD.Core.Dependencies;
using System.Threading;
using System.Windows.Threading;
using DDDD.Core.Net;
using System.Text;


#if SL5
using DDDD.Core.PowerShell;
using System.Runtime.InteropServices.Automation;
#endif

namespace DDDD.Core.Install
{

    /// <summary>
    /// Installer manage by: 
    ///  <para/>  OS REGISTRY - Read/Write Operations- by WScript and PowerShell;  
    ///  <para/>  OS STATE - Restarting/Shutdowning OS;
    ///  <para/>  CURRENT APP PROCESS - Restarting  IE with current Application
    ///  <para/>  POWERSHELL EXECUTION POLICY - Set available execution Policy
    ///  <para/>  RUNASADMIN JUMPLIST - Add/Remove App  from this list 
    ///  <para/>  ZIP PACKAGES - Pack/Unpack zip files
    ///  <para/>  WINDWOS OPTIONAL Features switching/installing(begin from Windows 8 ver.) - by PowerShell 
    ///  <para/>  WINDOWS INSTALLED PROGRAMS - Check Program installed; 
    /// </summary>
    public class Installer
    {
        const string NL = "\r\n";
        const string WinTool = "WinTool";


        static Lazy<string> WinToolExeFunc = new Lazy<string>(
            () =>
            {
                return Path.Combine(Environment2.ClientToolsDir, $"{WinTool}", $"{WinTool}.exe");
            }
            );


        static string WinToolExe
        {
            get { return WinToolExeFunc.Value; }
        }

        static Installer()
        {
            CmdPackageDependencyManager.RegisterDependecies<Installer>();
        }


#region ----------------------------------- LOG---------------------------------


        /// <summary>
        /// Install Log Builder
        /// </summary>
        static StringBuilder InstallLog
        { get; } = new StringBuilder();

        const string LogDelimeter = ":::";


        /// <summary>
        /// Log and/or Response UI message. 
        /// <para/> Response to UI - If sayProgressToUI is not null  and  (task+message) || (task+exceptionMessage) is not null
        /// <para/> Install Log - If useInstallLog is true then bulid LogLine like message [{task}{LogDelimeter}{message}] and add it to InstallLog.
        /// </summary>
        /// <param name="sayProgressToUI"></param>
        /// <param name="task"></param>
        /// <param name="message"></param>
        /// <param name="exceptionMessage"></param>
        /// <param name="useInstallLog"></param>
        /// <param name="clearLog"></param>
        static void LogResponseUI(Action<OperationProgressInfo> sayProgressToUI
                                    , string task, string message, string exceptionMessage
                                    , bool useInstallLog = true, bool clearLog = false)
        {

            //prepare Installing Log Builder - clearing Log -for new Installation
            if (useInstallLog && clearLog)
            {
                InstallLog.Clear();
            }

            // Install Log
            if (useInstallLog && !task.IsNullOrEmpty() && !message.IsNullOrEmpty())
            {
                InstallLog.AppendLine($"{task} {LogDelimeter} {message} ");
            }

            if (useInstallLog && !task.IsNullOrEmpty() && !exceptionMessage.IsNullOrEmpty())
            {
                InstallLog.AppendLine($"{task} {LogDelimeter} {exceptionMessage} ");
            }

            // UI Responsing
            if (sayProgressToUI != null && !task.IsNullOrEmpty() && !message.IsNullOrEmpty())
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, task, message);
            }

            if (sayProgressToUI != null && !task.IsNullOrEmpty() && !exceptionMessage.IsNullOrEmpty())
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ErrorMessage  ,task, null, exceptionMessage );
            }

        }


#endregion ----------------------------------- LOG---------------------------------



#if SL5

#region -------------------------------------- REGISTRY OPERATIONS -------------------------------------

        /// <summary>
        /// Read value from Windows System Register.        
        /// <para/> Can't be used to read Keys with parameters with Name like Path -[X:\Folder1\Folder2...] . Use RegisterReadPS.
        /// <para/> We try to convert output dynamic value to T.
        /// <para/> If error happens MessageBox will show Exception Message. 
        /// <para/> Syntax: WScriptShell.RegRead(RegPath)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="RegPath"></param>
        /// <returns></returns>
        public static T RegistryReadWSH<T>(string RegPath)
        {
            try
            {
                return (T)Environment2.WScriptShell.RegRead(RegPath);
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                return default(T);
            }
        }


        /// <summary>
        /// Write value into Windows System Register by WScriptShell.
        /// <para/> If error happens MessageBox will show Exception Message. 
        /// <para/> Syntax: WScriptShell.RegWrite(strName, anyValue[, strType])           
        /// </summary>
        /// <param name="RegPath"></param>
        /// <param name="value"></param>
        /// <param name="regValueType"> can be { "REG_SZ" | "REG_DWORD" | "REG_BINARY" | "REG_EXPAND_SZ" } </param>
        public static void RegistryWriteWSH(string RegPath, object value, string regValueType = null)
        {
            try
            {
                if (string.IsNullOrEmpty(regValueType))
                {
                    Environment2.WScriptShell.RegWrite(RegPath, value);
                }
                Environment2.WScriptShell.RegWrite(RegPath, value, regValueType);

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }



        /// <summary>
        /// Read value from Windows System Register.
        /// <para/> If error happens MessageBox will show Exception Message.     
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="regPath"></param>
        /// <param name="paramName"></param>
        /// <param name="FormatFilter"></param>
        /// <returns></returns>
        public static T RegistryReadPS<T>(string regPath, string paramName, string FormatFilter = null)
        {
            try
            {
                //Get-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers' -Name 'C:\Program Files\Internet Explorer\iexplore.exe'| Fl c:*  | Out-File 'C:\temp\log1111.txt' unicode 
                var readScript = $"Get-ItemProperty -Path '{regPath.Trim()}' -Name '{paramName.Trim()}'";

                return PowerShellManager.RunPSScript(readScript
                                                    , returnResult: true, waitOnRun: true
                                                    , format: PSFormatEn.FormatList, formatSymbol: FormatFilter)
                                        .GetParameterValue<T>(paramName, useCase: false);

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
                return default(T);
            }
        }

        /// <summary>
        /// Check Win Registry Key exist
        /// </summary>
        /// <param name="regPath"></param>
        /// <returns></returns>
        public static bool RegistryCheckKeyExistPS(string regPath, string checkStringPathValue)
        {
            try
            {
                if (regPath.IsNullOrEmpty()) return false;

                var readScript = $"Get-Item -Path '{ regPath.Trim() }'";

                var lastRegPathElement = regPath.SplitNoEntries(new[] { @"\" }).LastOrDefault();
                if (lastRegPathElement.IsNullOrEmpty()) return false;

                var readScriptResult = PowerShellManager.RunPSScript(readScript
                                                    , returnResult: true, waitOnRun: true
                                                    , format: PSFormatEn.FormatList)
                                        .GetParameterValue<string>("Name");

                return (!readScriptResult.IsNullOrEmpty() && readScriptResult.Contains(checkStringPathValue));

            }
            catch (Exception exc)
            {
                // log - notEXIST
                return false;
            }
        }


        /// <summary>
        /// Create new Key in OS Registry
        /// </summary>
        /// <param name="regPath"></param>
        /// <returns></returns>
        public static bool RegistryNewKeyPS(string regPath)
        {
            try
            {
                if (regPath.IsNullOrEmpty()) return false;
                var newItemScript = $"New-Item -Path '{ regPath.Trim() }' -Force | Format-List PSChildName";

                var lastRegPathElement = regPath.SplitNoEntries(new[] { @"\" }).LastOrDefault();
                if (lastRegPathElement.IsNullOrEmpty()) return false;

                var readScriptResult = PowerShellManager.RunPSScript(newItemScript
                                                    , returnResult: true, waitOnRun: true
                                                    , format: PSFormatEn.FormatList)
                                        .GetParameterValue<string>("PSChildName");

                return (readScriptResult == lastRegPathElement);

            }
            catch (Exception exc)
            {
                // log - notEXIST
                return false;
            }
        }



        /// <summary>
        /// Write value into Windows System Register with  the PowerShell.
        /// </summary>
        /// <param name="regKeyPath"></param>
        /// <param name="regKeyParam"></param>
        /// <param name="value"></param>
        public static void RegistryWritePS(string regKeyPath, string regKeyParam, object value)
        {
            try
            {
                if (!regKeyPath.IsNullOrEmpty() && !regKeyParam.IsNullOrEmpty())// regValueType.IsNullOrEmpty() &&
                {
                    // Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers\" -Name "C:\Program Files\Internet Explorer\iexplore.exe" -Value "~RUNASASAS234234" -PassThru
                    var regWriteScript = $"Set-ItemProperty -Path '{regKeyPath.Trim()}' -Name '{regKeyParam.Trim()}' -Value '{ value.S() }' -PassThru";
                    PowerShellManager.RunPSScript(regWriteScript
                                               , returnResult: false, waitOnRun: true);
                }

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
        }

#endregion -------------------------------------- REGISTRY OPERATIONS -------------------------------------

        
#region ------------------------ RUNASADMIN JumpList- Add/Remove/CheckExist App  in  ------------------------

        public const string RUNASADMIN = "~ RUNASADMIN";
        public const string IEx64PATH = @"C:\Program Files\Internet Explorer\iexplore.exe";
        public const string IEx86PATH = @"C:\Program Files (x86)\Internet Explorer\iexplore.exe";

        public const string REGKEY_RUNAS_JUMPLIST = @"HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers";
        public const string REGKEY_RUNAS_JUMPLIST_NODRIVE = @"\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers";

        /// <summary>
        /// Checking IE(x64 and x86)will always run as ADMIN. It means that IE(x64 and x86) added in System registry. 
        /// </summary>
        /// <returns></returns>
        public static bool CheckAppExistInRunAsAdminJumpList(string appExecutionFilePath)
        {
            if (!File.Exists(appExecutionFilePath)) return false;

            var driveletter = Path.GetPathRoot(appExecutionFilePath);
            //Get-ItemProperty -Path 'HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers' -Name 'C:\Program Files\Internet Explorer\iexplore.exe'| Fl c:*  | Out-File 'C:\temp\log1111.txt' unicode             
            //var regKeyParam = Environment2.CurrentProcessExecutionFilePath;// GetCurrentProcessExecutionFilePath();// IEx64PATH;
            var filterSymbols = driveletter + "*"; ///@"c:\*";//if path  drive is  c:\

            var resltKeyValue = RegistryReadPS<string>(REGKEY_RUNAS_JUMPLIST, @appExecutionFilePath, filterSymbols);

            if (resltKeyValue.IsNullOrEmpty()) return false;
            if (resltKeyValue != RUNASADMIN) return false;



            return true;
        }

        /// <summary>
        /// Add  [appExecutionFilePath] IE(x64 and x86) to [RUNASADMIN JumpList REGKEY]. 
        /// </summary>
        public static void AddAppToRunAsAdminJumpList(string appExecutionFilePath)
        {
            if (!File.Exists(appExecutionFilePath)) return;

            RegistryWritePS(REGKEY_RUNAS_JUMPLIST, appExecutionFilePath, RUNASADMIN); // regKeyParam_IEx64             , true

        }







#endregion  ------------------------ RUNASADMIN JumpList- Add/Remove/CheckExist App  in  ------------------------
        
        
#region ------------------------------ TERMINATE & REASTART IE APP ---------------------------------


        /// <summary>
        /// Terminate current Process
        /// </summary>
        public static void TerminateCurrentProcess()
        {
            // "Get-WmiObject -Class Win32_Process -ComputerName \".\" | Where-Object { $_.ProcessId -eq '" + procID + @"' } | ForEach-Object { $_.Terminate() }" + "\r\n";
            var terminatePS_WMIScript = PowerShellManager.BuildTerminateCurrentProcessScript(Environment2.CurrentProcessID);

            PowerShellManager.RunPSScript(terminatePS_WMIScript, returnResult: false, waitOnRun: false);
        }


        /// <summary>
        /// RestartingIEApplication  - we run new IE Process Instance that will load current App start Url
        /// <para/> Start Url - [Environment2.DefaultUrl.StartRequestUrlFragment] 
        /// </summary>
        public static void RestartCurrentIEAppProcess()
        {
            var procID = Environment2.CurrentProcessID.S();

            //current App's IE process Path
            var appIEProcessExecFilePath = Environment2.CurrentProcessExecutionFilePath;// GetCurrentProcessExecutionFilePath();

            var argList = Environment2.DefaultUrl.StartRequestUrlFragment;//APP(CRM) START AGAIN URL

            var terminate_restartScript =
                //OPEN
                "Start-Process -FilePath '" + @appIEProcessExecFilePath + "' -ArgumentList '" + argList + "' -PassThru -WindowStyle Maximized;"
                + NL
                //SLEEP 1 seconds
                + "Start-Sleep -Milliseconds 1000;"
                + NL
                 // CLOSE
                 + "Get-WmiObject -Class Win32_Process -ComputerName \".\" | Where-Object { $_.ProcessId -eq '" + procID + @"' } | ForEach-Object { $_.Terminate() }";

            var res = PowerShellManager.RunPSScript(terminate_restartScript, returnResult: false, waitOnRun: false);
        }



#endregion ------------------------------ TERMINATE & REASTART IE APP ---------------------------------


#region ------------------------------ ZIP - PACK Directory/UNPACK zipFile -----------------------------------

        //For BIG-SIZE zip packages 

        /// <summary>
        /// Unpack [Zip Package File] to some [Destination Directory]. For Big data archives.
        /// </summary>
        /// <param name="srcZipFilePath"></param>
        /// <param name="destinationPath"></param>
        public static void UnpackZipPackage(string srcZipFilePath, string destinationPath)
        {
            if (!File.Exists(srcZipFilePath)) return;

            if (Directory.Exists(destinationPath))
            {
                Directory.Delete(destinationPath, true);
            }
            //Add-Type -Assembly "System.IO.Compression.FileSystem" ;
            //[System.IO.Compression.ZipFile]::ExtractToDirectory($pkgZipFile, $destinationFile);

            var unpackZipPackageScript = "Add-Type -Assembly 'System.IO.Compression.FileSystem';  "
                                        + NL
                                        + "[System.IO.Compression.ZipFile]::ExtractToDirectory('" + srcZipFilePath + "', '" + destinationPath + "');";

            PowerShellManager.RunPSScript(unpackZipPackageScript, returnResult: false, waitOnRun: true);
        }


        /// <summary>
        /// Pack [Source Directory] to [Destination Zip Package File].  For Big data archives. 
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="destinationZipPackagePath"></param>
        public static void PackToZipPackage(string directoryPath, string destinationZipPackagePath)
        {
            if (!Directory.Exists(directoryPath)) return;

            if (File.Exists(destinationZipPackagePath))
            {
                File.Delete(destinationZipPackagePath);
            }


            //Add-Type -Assembly "System.IO.Compression.FileSystem" ;
            //[System.IO.Compression.ZipFile]::CreateFromDirectory("c:\your\directory\to\compress", "yourfile.zip");

            var packZipPackageScript = "Add-Type -Assembly 'System.IO.Compression.FileSystem';  "
                                    + NL
                                    + "[System.IO.Compression.ZipFile]::CreateFromDirectory('" + directoryPath + "', '" + destinationZipPackagePath + "');";

            PowerShellManager.RunPSScript(packZipPackageScript, returnResult: false, waitOnRun: true);
        }


        public static void UnPackZipPackageAsync(string directoryPath, string destinationZipPackagePath
                                                   , Action<OperationProgressInfo> progressAction
                                                  )
        {
            throw new NotImplementedException($"UnPackZipPackageAsync");
        }


#endregion ------------------------------ ZIP - PACK Directory/UNPACK zipFile -----------------------------------


#region ----------------------------------- WINDOWS OPTIONAL FEATURES-CHECK EXISTS && INSTALL ---------------------------------

        //download windows81 image pack

        /// <summary>
        /// Check that [checkFeature]  -Win81 OS Optional Feature is  installed in Current Windows
        /// </summary>
        /// <param name="checkFeature"></param>
        /// <returns></returns>
        [CmdPackageDependency(nameof(Installer) + "." + nameof(CheckWin81FeatureEnabled), PackageTypeEn.Tools, WinTool)]
        public static async Task<bool> CheckWin81FeatureEnabled(Windows81OptionalFeatures checkFeature)
        {
            //Get-WindowsOptionalFeature –Online –FeatureName NetFx3
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(CheckWin81FeatureEnabled));

            var targetFeatureName = checkFeature.S().Replace("_", "-");
            var tempResultOutFile = Path.Combine(PowerShellManager.PSTempFolder, $"tmprslt_{Guid.NewGuid().S().Replace("-", "_").Substring(0, 7)}.rslt");

            var queryScript = $"{WinToolExe} IsWindowsFeatureEnabled Feature#NetFx3 \"OutFile#{tempResultOutFile}\"";

            //var queryScript = $"{WinToolExe} Get_WmiObject \"Query#select name,installstate from win32_optionalfeature where name = '{targetFeatureName}'\" OutFields#name,installstate OutFile#{tempResultOutFile}";
            string output = "";
            try
            {
                var runResultOut = Environment2.WScriptShell.Run(queryScript, 0, true);

                //read result from out file
                if (File.Exists(tempResultOutFile))
                {
                    output = File.ReadAllText(tempResultOutFile);
                    File.Delete(tempResultOutFile);
                }
            }
            catch (FileNotFoundException fnfex)
            {
                throw new FileNotFoundException("PowerShell is not installed on the local computer.", fnfex);
            }

            //var featureState = false;
            if (!output.IsNullOrEmpty() && output.Trim() == "TRUE") return true;


            return false;
        }


        [CmdPackageDependency(nameof(Installer) + "." + nameof(EnableWin81Feature), PackageTypeEn.Tools, WinTool)]
        public async static void EnableWin81Feature(Windows81OptionalFeatures checkFeature, string winSourcesSxsFolderPath)
        {
            //Enable-WindowsOptionalFeature –Online –FeatureName NetFx3 –All -LimitAccess -Source C:\DISTRIB\Windows8.1\Inst\sources\sxs
            //C:\Windows\system32\dism.exe /online /norestart /enable-feature /featurename:netfx3

            var Feature = checkFeature.S().Replace("_", "-");

            //Get-WindowsOptionalFeature –Online –FeatureName NetFx3
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(EnableWin81Feature));

            var queryScript = $"{WinToolExe} Enable_WindowsFeature Feature#{Feature} \"Source#{winSourcesSxsFolderPath}\" \"OutFile#c:\\Temp\\wintool_Get_WindowsFeature.rslt\"";

            try
            {
                var runResultOut = Environment2.WScriptShell.Run(queryScript, 0, true);
            }
            catch (FileNotFoundException fnfex)
            {
                throw new FileNotFoundException("PowerShell is not installed on the local computer.", fnfex);
            }
        }


        public static void DisableWin81Feature(Windows81OptionalFeatures checkFeature)
        {

            var targetFeatureName = checkFeature.S().Replace("_", "-");

            //Disable-WindowsOptionalFeature -Online -FeatureName ""
            var disableWinOptionalFeatureScript = $@"Disable-WindowsOptionalFeature –Online –FeatureName {targetFeatureName} -LimitAccess";

            PowerShellManager.RunPSScript(disableWinOptionalFeatureScript
                                            , returnResult: false, waitOnRun: true);



        }



        /// <summary>
        /// Install NetFx3 Win Optional Feature into current Windows OS scenario 
        /// </summary>
        /// <param name="sayProgressToUI"></param>
        /// <returns></returns>
        public async static Task<bool> TryInstallWinOptionalFeature_NetFx3(Action<OperationProgressInfo> sayProgressToUI = null,
                                                                          bool useInstallLog = true, bool clearInstallLogOnStart = true)
        {
            try
            {

                //prepare Installing Log Builder
                if (useInstallLog && clearInstallLogOnStart)
                {  InstallLog.Clear();
                }

                // WIN OPTIONAL FEATURE: NETFX3 
                if (!await CheckWin81FeatureEnabled(Windows81OptionalFeatures.NetFx3))
                {

                    if (useInstallLog)
                    {  InstallLog.AppendLine("Установка Компонента Windows [.NET Framework 3.5] ::: Компонент Windows [.NET Framework 3.5]  НЕ УСТАНОВЛЕН. Начинаем его установку. ");
                    }
                    sayProgressToUI.SendUIMessage( OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                        , progressMessage: "Компонент Windows [.NET Framework 3.5]  НЕ УСТАНОВЛЕН. Начинаем его установку. ");


                    // Download Win81Distrib.zip 
                    var Win81DistribZipPackageName = "Win81Distrib.zip";
                    var Win81DistribZipPackageNameOnServer = Environment2.DefaultUrl. GetBaseUri( "Tools/" + Win81DistribZipPackageName);

                    var appToolsPath = Environment2.GetLocalAppFolder("Tools");
                    var Win81DistribZipPackagePath = Path.Combine(appToolsPath, Win81DistribZipPackageName);



                    if (useInstallLog)
                    { InstallLog.AppendLine("Установка Компонента Windows [.NET Framework 3.5] ::: Загружаем пакет [Win81Distrib.zip]  для установки Компонента Windows [.NET Framework 3.5].");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                        , progressMessage: "Загружаем пакет [Win81Distrib.zip]  для установки Компонента Windows [.NET Framework 3.5]. ");


                    // if file not exist locally -  [Tools\Win81Distrib.zip] - download it from [~/Tools/Win81Distrib.zip]
                    if (!File.Exists(Win81DistribZipPackagePath))
                    {    await DownloadClient.DownloadFileAsync(new Uri(Win81DistribZipPackageNameOnServer), Win81DistribZipPackagePath);
                    }



                    // Check file  not downloaded -  [Tools\Win81Distrib.zip] - exit -its ERROR.
                    if (!File.Exists(Win81DistribZipPackagePath))
                    {
                        if (useInstallLog)
                        { InstallLog.AppendLine("Установка Компонента Windows [.NET Framework 3.5] ::: Не удалось загрузить Пакет [Win81Distrib.zip].");
                        }

                        sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]" , null
                             , exceptionMessage: "Не удалось загрузить Пакет [Win81Distrib.zip].");

                        return false;
                    }
                    else
                    {
                        if (useInstallLog)
                        { InstallLog.AppendLine("Установка Компонента Windows [.NET Framework 3.5] ::: Пакет [Win81Distrib.zip] Загружен успешно");
                        }

                        sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]" 
                             , progressMessage: "Пакет [Win81Distrib.zip] Загружен успешно");
                    }



                    //Unblock zipPackage
                    UnblockFile(Win81DistribZipPackagePath);
                    if (useInstallLog)
                    {  InstallLog.AppendLine("Установка Компонента Windows [.NET Framework 3.5] ::: Файл Пакет [Win81Distrib.zip]  был разблокирован");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                     , progressMessage: "Файл Пакет [Win81Distrib.zip]  был разблокирован");


                    //Загрузка Пакета [Win81Distrib.zip]
                    var Win81DistribZipPackageUnzipFolderName = "Win81Distrib";
                    var Win81DistribZipPackageUnzipFolderPath = Path.Combine(appToolsPath, Win81DistribZipPackageUnzipFolderName);
                    UnpackZipPackage(Win81DistribZipPackagePath, Win81DistribZipPackageUnzipFolderPath);
                    if (useInstallLog)
                    {  InstallLog.AppendLine($"Установка Компонента Windows [.NET Framework 3.5] ::: Файл Пакета [Win81Distrib.zip]  был успешно разархиврован  FROM:{Win81DistribZipPackagePath}  TO: {Win81DistribZipPackageUnzipFolderPath}");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                     , progressMessage: $"Файл Пакета [Win81Distrib.zip]  был успешно разархиврован  FROM:{Win81DistribZipPackagePath}  TO: {Win81DistribZipPackageUnzipFolderPath}");


                    // run install WindowsFeature NetFx3  command 
                    if (useInstallLog)
                    {  InstallLog.AppendLine($"Установка Компонента Windows [.NET Framework 3.5] ::: Запускаем непосредственную  команду уставноки Компонента Windows [.NET Framework 3.5]");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                     , progressMessage: $"Запускаем непосредственную  команду уставноки Компонента Windows [.NET Framework 3.5] ");
                    var SxsWin8FeaturesDirectory = Path.Combine(Win81DistribZipPackageUnzipFolderPath, "sxs");
                    EnableWin81Feature(Windows81OptionalFeatures.NetFx3, SxsWin8FeaturesDirectory);

                    if (useInstallLog)
                    {  InstallLog.AppendLine($"Установка Компонента Windows [.NET Framework 3.5] :::  Уставнока Компонента Windows [.NET Framework 3.5] произошла успешно.");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка Компонента Windows [.NET Framework 3.5]"
                     , progressMessage: $" Уставнока Компонента Windows [.NET Framework 3.5] произошла успешно ");


                    return true;
                }
                else
                {
                    if (useInstallLog)
                    { InstallLog.AppendLine($"Установка Компонента Windows [.NET Framework 3.5] ::: Компонент Windows [.NET Framework 3.5] УЖЕ УСТАНОВЛЕН.");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка компонента Windows .NET Framework 3.5"
                       , progressMessage: "Компонент Windows [.NET Framework 3.5] УЖЕ УСТАНОВЛЕН.");

                    return true;
                }
            }
            catch (Exception exc)
            {
                if (useInstallLog)
                { InstallLog.AppendLine($"Установка Компонента Windows [.NET Framework 3.5] :::  ERROR: IN [Установка компонента Windows .NET Framework 3.5] - [ {exc.Message} ] ");
                }
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка компонента Windows .NET Framework 3.5", null
                    ,exceptionMessage: $"ERROR: IN [Установка компонента Windows .NET Framework 3.5] - [ {exc.Message} ] ");
                throw exc;
            }
            finally
            {
                if (useInstallLog)
                {
                    File.WriteAllText( $@"C:\Temp\{nameof(TryInstallWinOptionalFeature_NetFx3)}_trace.log", InstallLog.S());
                }
            }

        }



#endregion ----------------------------------- WINDOWS OPTIONAL FEATURES-CHECK EXISTS && INSTALL ---------------------------------


#region ----------------------- WINDOWS INSTALLED PROGRAMS - CHECK PROGRAM INSTALLED ------------------------------

        [CmdPackageDependency(nameof(Installer) + "." + nameof(CheckWin8LikeProgramInstalled), PackageTypeEn.Tools, WinTool)]
        public async static Task<bool> CheckWin8LikeProgramInstalled(string programNameLikeValue)
        {
            //Get-WmiObject Win32_Product | Sort-Object Name |where Name -Contains 'Microsoft SQL Server 2014' | Format-Table Name, Version, Vendor | Out-File C:\Temp\programs_res2.dat
            //Get-WmiObject Win32_Product | Where - Object {$_.name - Like "Microsoft SQL Server 2014*"}| Select Name | Fl | Out - File C:\Temp\programs_res2.dat
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(CheckWin8LikeProgramInstalled));

            var winProgramSetupScript = $"Get-WmiObject Win32_Product | Sort-Object Name |where Name -Like '{programNameLikeValue}' ";

            var reslt = PowerShellManager.RunPSScript(winProgramSetupScript, returnResult: true, waitOnRun: true);

            if (!reslt.IsNullOrEmpty()) return true;//found some items - it seems that it s already installed

            return false;//not found it still doesn't installed;

        }




        static dynamic CreateCOMObject(string COMObjectName)
        {
            try
            {
#if SL5 && DEBUG
                // Check to see if stuff is available
                if (!AutomationFactory.IsAvailable)
                    throw new InvalidOperationException("AutomationFactory is unavailable. Program must be run as out-of-browser with elevated permissions.");
                return AutomationFactory.CreateObject(COMObjectName);
#elif SL5
               return AutomationFactory.CreateObject(COMObjectName);
#elif NET45

                var comObjType = Type.GetTypeFromProgID(COMObjectName);
                return Activator.CreateInstance(comObjType);
#endif
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("Environment2.CreateCOMObject(): " + exc.Message);
            }

        }


#endregion ----------------------- WINDOWS INSTALLED PROGRAMS - CHECK PROGRAM INSTALLED ------------------------------


#region -------------------------------------- CAB SFX EXTRACTING --------------------------------------------

        /// <summary>
        /// Extracting Sfx Cab file -[sfxCabFilePath] 
        /// <para/> LINK1: http://www.mdgx.com/INF_web/iexpress.htm   LINK2: https://support.microsoft.com/en-us/kb/197147
        /// </summary>
        /// <param name="sfxCabFilePath"></param>
        /// <param name="destinationDir"></param>
        /// <param name="cancelInstall"></param>
        /// <param name="quietMode"></param>
        [CmdPackageDependency(nameof(Installer) + "." + nameof(ExtractSfxCab), PackageTypeEn.Tools, WinTool)]
        public async static Task<bool> ExtractSfxCab(string sfxCabFilePath, string destinationDir, bool cancelInstall = true, bool quietMode = true)
        {
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(ExtractSfxCab));
            // ExtractSilent-SfxCAB "CabSfxFile#c:\ProgramData\CRM\Distrib\SQLEXPRWT_x64_ENU.exe"

            var runScriptCmd = $"{WinToolExe} ExtractSilent_SfxCAB \"CabSfxFile#{sfxCabFilePath}\" ";
            
            try
            {
                //runCmd = @"c:\ProgramData\CRM\Distrib\RunExtractSilent.bat";
               Environment2.WScriptShell.Run(runScriptCmd, 0, true); // await

                return true;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(ExtractSfxCab)}(): " + exc.Message);
            }
        }

#endregion -------------------------------------- CAB SFX EXTRACTING --------------------------------------------


#region ------------------------------ DEVICES ENABLE/ DISable /RESTART ---------------------------------


        [CmdPackageDependency(nameof(Installer) + "." + nameof(RestartDeviceByName), PackageTypeEn.Tools, WinTool)]
        public async static void RestartDeviceByName(string deviceName)
        {
            // Package WinTool
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(RestartDeviceByName));

            var tempResFile = Path.Combine(Environment2.ClientTempDir, $"trslt_{Guid.NewGuid().S().Substring(0, 7)}.rslt");
            var queryScript = $"{WinToolExe} {nameof(RestartDeviceByName)} \"DeviceName#{deviceName}\" \"OutFile#{tempResFile}\"";

            try
            {
                Environment2.WScriptShell.Run(queryScript, 0, true);

                if (File.Exists(tempResFile))
                {
                    var operationResult = File.ReadAllText(tempResFile);
                    File.Delete(tempResFile);
                    var successFlag = operationResult.GetParameterValue<bool>(  nameof(RestartDeviceByName) );
                }

                // error  ?? LOG or MESSAGE

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($" WinTool return  error: {exc.Message} ");
            }
        }



        [CmdPackageDependency(nameof(Installer) + "." + nameof(DisableDeviceByName), PackageTypeEn.Tools, WinTool)]
        public async static void DisableDeviceByName(string deviceName)
        {
            // Package WinTool
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(DisableDeviceByName));

            var tempResFile = Path.Combine(Environment2.ClientTempDir, $"trslt_{Guid.NewGuid().S().Substring(0, 7)}.rslt");
            var queryScript = $"{WinToolExe} {nameof(DisableDeviceByName)} \"DeviceName#{deviceName}\" \"OutFile#{tempResFile}\"";

            try
            {
                Environment2.WScriptShell.Run(queryScript, 0, true);

                if (File.Exists(tempResFile))
                {
                    var operationResult = File.ReadAllText(tempResFile);
                    File.Delete(tempResFile);
                    var successFlag = operationResult.GetParameterValue<bool>( nameof(DisableDeviceByName));

                }

                // error  ?? LOG or MESSAGE

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($" WinTool return  error: {exc.Message} ");
            }
        }




        [CmdPackageDependency(nameof(Installer) + "." + nameof(EnableDeviceByName), PackageTypeEn.Tools, WinTool)]
        public async static void EnableDeviceByName(string deviceName)
        {
            // Package WinTool
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(EnableDeviceByName));

            var tempResFile = Path.Combine(Environment2.ClientTempDir, $"trslt_{Guid.NewGuid().S().Substring(0, 7)}.rslt");
            var queryScript = $"{WinToolExe} {nameof(EnableDeviceByName)} \"DeviceName#{deviceName}\" \"OutFile#{tempResFile}\"";

            try
            {
                Environment2.WScriptShell.Run(queryScript, 0, true);

                if (File.Exists(tempResFile))
                {
                    var operationResult = File.ReadAllText(tempResFile);
                    File.Delete(tempResFile);
                    var successFlag = operationResult.GetParameterValue<bool>(nameof(EnableDeviceByName));

                }

                // error  ?? LOG or MESSAGE

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException( $" WinTool return  error: {exc.Message} " );
            }
        }



#endregion ------------------------------ DEVICES ENABLE/ DISable /RESTART ---------------------------------


#region -------------------------------- PS  ExecutionPolicy  : SET UNRESTRICTED / SET ALLSIGNED / SET REMOTE SIGNED -------------------------------------

        /// <summary>
        /// Check if current POWERSHELL Execution Policy is not Unrestricted then Set it to Unrestricted  
        /// </summary>
        public static void CheckSetPSExecPolicyToUnrestricted()
        {
            if (PowerShellManager.ExecutionPolicy == PSExecutionPolicyEn.Unrestricted) return;

            PowerShellManager.SetPSExecPolicy(PSExecutionPolicyEn.Unrestricted);


            if (PowerShellManager.ExecutionPolicy != PSExecutionPolicyEn.Unrestricted)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(CheckSetPSExecPolicyToUnrestricted)}(): PS Execution Policy  wasn't reset to Unrestricted mode");
            }
        }

        /// <summary>
        /// Check if current POWERSHELL Execution Policy is not AllSigned then Set it to AllSigned  
        /// </summary>
        public static void CheckSetPSExecPolicyToAllSigned()
        {
            if (PowerShellManager.ExecutionPolicy == PSExecutionPolicyEn.AllSigned) return;

            PowerShellManager.SetPSExecPolicy(PSExecutionPolicyEn.AllSigned);


            if (PowerShellManager.ExecutionPolicy != PSExecutionPolicyEn.AllSigned)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(CheckSetPSExecPolicyToAllSigned)}(): PS Execution Policy  wasn't reset to AllSigned mode");
            }
        }


        /// <summary>
        /// Check if current POWERSHELL Execution Policy is not RemoteSigned then Set it to RemoteSigned
        /// </summary>
        public static void CheckSetPSExecPolicyToRemoteSigned()
        {
            if (PowerShellManager.ExecutionPolicy == PSExecutionPolicyEn.RemoteSigned) return;

            PowerShellManager.SetPSExecPolicy(PSExecutionPolicyEn.RemoteSigned);


            if (PowerShellManager.ExecutionPolicy != PSExecutionPolicyEn.RemoteSigned)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(CheckSetPSExecPolicyToRemoteSigned)}(): PS Execution Policy  wasn't reset to RemoteSigned mode");
            }
        }



#endregion -------------------------------- PS  ExecutionPolicy  : SET UNRESTRICTED / SET ALLSIGNED / SET REMOTE SIGNED -------------------------------------


#region ---------------------------------- SQL SERVER UTILS: INSTALL / ENGINES INFO/ RUN  SCENARIO by PS SQL MODULE --------------------------------------


        /// <summary>
        /// This Method based on SQLPS PowerShell package which will be available only after SSMT will be installed
        /// </summary>
        /// <returns></returns>
        public static string[] GetSqlEngineInstancies()
        {
            // cd SQLSERVER:\SQL
            // Get-ChildItem | Fl 

            var sqlServerInstanciesScript = @"cd SQLSERVER:\SQL"
                                              + NL
                                              + "Get-ChildItem | Fl ";

            var resltInstancies = PowerShellManager.RunPSScript(sqlServerInstanciesScript
                                                   , returnResult: true, waitOnRun: true
                                                    );
            return null;
        }



        /// <summary>
        /// Full Sql Server 2014 Express installation 
        /// <para/> Msdn Help for Sql Server 2014 Express Setup Command Prompt Parameters - https://technet.microsoft.com/en-us/library/ms144259%28v=sql.120%29.aspx#Install
        /// <para/> On Windows 8/8.1 before run install we need to turn off Windows Defender.
        /// <para/> We can find MS SQl Server 2014 Express install Log in - [c:\Program Files\Microsoft SQL Server\120\Setup Bootstrap\Log\{installtry_date_time}\summary_pcname_date_time]
        /// <para/> We can find this Installer.TryInstallSqlServerExpress2014() Log in [C:\Temp\TryInstallSqlServerExpress2014_trace.log]
        /// </summary>
        public async static Task<bool> TryInstallSqlServerExpress2014(Action<OperationProgressInfo> sayProgressToUI = null
                                                                     , bool useInstallSqlLog = true, bool clearInstallLogOnStart = true)
        {
            try
            { 
                //prepare Installing Log Builder - CLearing log for this Installation tracing
                LogResponseUI(null, null, null, null, useInstallLog: useInstallSqlLog, clearLog: clearInstallLogOnStart);
                

                // elevated  Permissions check
                if (Environment2.HasElevatedPermissions== false || AutomationFactory.IsAvailable == false)
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                       null, exceptionMessage: $"ERROR iN [Установка SQL Server 2014] : Приложение не обладает Elevated Permissions  или  AutomationFactory все еще не доступна"
                       , useInstallLog: useInstallSqlLog);
                                                            
                    return false;
                }

                // Update WinTool till last version - this package must exist  on client
                if ( !await CmdPackageDependencyManager.TryUpdateClientPackage(PackageTypeEn.Tools, WinTool) )
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                      null, exceptionMessage: $"ERROR iN [Установка SQL Server 2014] :  Необходимый пакет [{WinTool}] не был найден или же не мог быть загружен с сервера. Проверьте связь с сервером."
                      , useInstallLog: useInstallSqlLog);

                    return false;
                }

              

                //Проверить что существует запись для текущий файл IE процесса-( согласно его битности) существует в списке RUNASJUMPLIST
                if (!CheckAppExistInRunAsAdminJumpList(Environment2.CurrentProcessExecutionFilePath))
                {                     
                    LogResponseUI(sayProgressToUI,"Установка SQL Server 2014", 
                        null , exceptionMessage: $"ERROR iN [Установка SQL Server 2014] : Процесс приложения не имеет прав Администратора. Критерий наличие записи  в системном списке [RUNASADMIN JUMPLIST] для файла [{Environment2.CurrentProcessExecutionFilePath}]"
                        , useInstallLog:useInstallSqlLog );
                                        
                    return false;
                }



                // -------------------------
                // Turn off Windows Defender. Realtime Monitoring                                        
                //                
                SetMsProtectionalPreference("DisableRealtimeMonitoring", true);
                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                     $" Отключен мониторинг в режиме рантайм  для  Защитника Windows", null
                    , useInstallLog: useInstallSqlLog);





                // CHECK ALREADY SQL INSTALLED
                LogResponseUI(sayProgressToUI,    "Установка SQL Server 2014", 
                    $"Проверяем наличие уже Установленного SQL server.Если он уже установлен устновка старт установки отменится."
                     , null, useInstallLog: useInstallSqlLog);
                                
                if (await CheckWin8LikeProgramInstalled("Microsoft SQL Server 2014 Express*"))
                {

                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                        $"SQL server 2014 уже установлен. Устновка  ОТМЕНЕНА и ЗАВЕРШЕНА."
                       ,null , useInstallLog: useInstallSqlLog);
                    
                    return true; //await
                }
                
                
                //Install Cancellation token creating
                var taskCancelTokenSource = new CancellationTokenSource();

                // WIN OPTIONAL FEATURE: NETFX3 
                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                        , $"Провека и установка  Компонента Windows [.NET Framework 3.5]. Необходим для успешного страта  установки SQL Server 2014 "
                       , null, useInstallLog: useInstallSqlLog);
                
                if (!await TryInstallWinOptionalFeature_NetFx3(sayProgressToUI, useInstallLog: true, clearInstallLogOnStart: false))
                {  return false;   }
                


                
                ///
                /// DOWNLOADING START
                ///
                /// Windows MUILanguages and  OS-bit Architecture
                /// RUS :
                ///  ExpressAndTools 32BIT\SQLEXPRWT_x86_RUS.exe :  https://download.microsoft.com/download/4/E/3/4E38FD5A-8859-446F-8C58-9FC70FE82BB1/ExpressAndTools%2032BIT/SQLEXPRWT_x86_RUS.exe
                ///  ExpressAndTools 64BIT\SQLEXPRWT_x64_RUS.exe :  https://download.microsoft.com/download/4/E/3/4E38FD5A-8859-446F-8C58-9FC70FE82BB1/ExpressAndTools%2064BIT/SQLEXPRWT_x64_RUS.exe
                /// ENG :
                ///  ExpressAndTools 32BIT\SQLEXPRWT_x86_ENU.exe :  https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2032BIT/SQLEXPRWT_x86_ENU.exe
                ///  ExpressAndTools 64BIT\SQLEXPRWT_x64_ENU.exe :  https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2064BIT/SQLEXPRWT_x64_ENU.exe
                Uri targetSqlExpressDownloadurl = null;
                /// get appropriate SQLServer distrib 
                if (Environment2.MUILanguages.Contains("en-US") && Environment2.Is64BitOS)
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                    ,$"Поиск Link-а SQL Server 2014. Выбрана версия Культура [en-US], Windows [64]bit ", null
                    , useInstallLog: useInstallSqlLog);

                                      
                    //download 64bit SQL Server 2014 Express with Tools  sfx cab  from  microsoft.download site
                    targetSqlExpressDownloadurl = new Uri("https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2064BIT/SQLEXPRWT_x64_ENU.exe");
                }
                else if (Environment2.MUILanguages.Contains("en-US") && !Environment2.Is64BitOS) //for 32 bit OS
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                  , $"Поиск Link-а SQL Server 2014. Выбрана версия Культура[en-US], Windows [32]bit", null
                  , useInstallLog: useInstallSqlLog);
                                        
                    //download 32bit SQL Server 2014 Express with Tools  sfx cab  from  microsoft.download site
                    targetSqlExpressDownloadurl = new Uri("https://download.microsoft.com/download/E/A/E/EAE6F7FC-767A-4038-A954-49B8B05D04EB/ExpressAndTools%2064BIT/SQLEXPRWT_x64_ENU.exe");
                }
                else if (Environment2.MUILanguages.Contains("ru-RU") && Environment2.Is64BitOS)
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                    , $"Поиск Link-а SQL Server 2014. Выбрана версия Культура[ru-RUS], Windows [64]bit", null
                    , useInstallLog: useInstallSqlLog);
                                        
                    targetSqlExpressDownloadurl = new Uri("https://download.microsoft.com/download/4/E/3/4E38FD5A-8859-446F-8C58-9FC70FE82BB1/ExpressAndTools%2064BIT/SQLEXPRWT_x64_RUS.exe");
                }
                else if (Environment2.MUILanguages.Contains("ru-RU") && !Environment2.Is64BitOS)
                {
                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                   , $"Поиск Link-а SQL Server 2014. Выбрана версия Культура[ru-RUS], Windows [32]bit" , null
                   , useInstallLog: useInstallSqlLog);
                                                           
                    targetSqlExpressDownloadurl = new Uri("https://download.microsoft.com/download/4/E/3/4E38FD5A-8859-446F-8C58-9FC70FE82BB1/ExpressAndTools%2032BIT/SQLEXPRWT_x86_RUS.exe");
                }

                //target sql download file path
                var savefileName = targetSqlExpressDownloadurl.TryGetTargetFileName2();
                var targetDownloadSqlExpressDistribFilePath = Path.Combine(Environment2.ClientDistribDir, savefileName);


                // -----------
                //Check Delete existed sql distrib file before downloading it again from web
                try
                {
                    if (File.Exists(targetDownloadSqlExpressDistribFilePath))
                        File.Delete(targetDownloadSqlExpressDistribFilePath);
                }
                catch (Exception exc)
                {

                    LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                      , null, exceptionMessage: $"ERROR iN [Установка SQL Server 2014] : Не могу удалить файл [{targetDownloadSqlExpressDistribFilePath}] c ошибкой [{exc.Message}]"
                      , useInstallLog: useInstallSqlLog);

                    return false;
                }
                // ------------


                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014"
                , $"Загрузка SQL Server 2014.  Используется uri: {targetSqlExpressDownloadurl.AbsoluteUri}", null
                , useInstallLog: useInstallSqlLog);

                var downloadSqlClient = DownloadClient.Create( targetSqlExpressDownloadurl , targetDownloadSqlExpressDistribFilePath );
                await downloadSqlClient.DownloadFileAsync();
                                
                // check downloaded file really exist and has the same size as on server
                if (File.Exists(targetDownloadSqlExpressDistribFilePath) )
                {
                    var fileInf = new FileInfo(targetDownloadSqlExpressDistribFilePath);
                    if (fileInf.Length != downloadSqlClient.PreviosItem.TargetDownloadDataItemSize)
                    {
                        LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                      null, exceptionMessage: $"ERROR iN [Установка SQL Server 2014] :Загрузка SQL Server 2014. При загрузке дистрибутива SQL Server 2014 произошла ошибка."
                      , useInstallLog: useInstallSqlLog);

                       
                        return false;
                    }
                }                
                //                
                //DOWNLOADING END
                // -------------------


                /// ------------------
                // EXTRACTING SFX CAB 
                // result  c:\ProgramData\CRM\Distrib\somefile.exe
                // target sfx unpack destination
                //         c:\ProgramData\CRM\Distrib\somefile\
                //                                
                var targetDownloadSqlExpressDistribFilePathExtractDir = Path.Combine(Environment2.ClientDistribDir, savefileName.Replace(".exe", ""));
                if (Directory.Exists(targetDownloadSqlExpressDistribFilePathExtractDir))
                    Directory.Delete(targetDownloadSqlExpressDistribFilePathExtractDir, recursive:true);

                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                      $"Начинаем распаковку SQL Server 2014 Express sfx-cab файл-а [{targetDownloadSqlExpressDistribFilePathExtractDir}]", null
                     , useInstallLog: useInstallSqlLog);
               
               await ExtractSfxCab(targetDownloadSqlExpressDistribFilePath, targetDownloadSqlExpressDistribFilePathExtractDir, cancelInstall: true, quietMode: true);

                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                      $"Распаковка SQL Server 2014 Express sfx-cab файл-а [{targetDownloadSqlExpressDistribFilePathExtractDir}] ЗАВЕРШЕНА", null
                     , useInstallLog: useInstallSqlLog);
                // EXTRACTING SFX CAB  END
                /// ------------------
                



                /// ------------------
                //Run SQL Server 2014 Setup process                 
                ///                 
                var setupFilePath = Path.Combine(targetDownloadSqlExpressDistribFilePathExtractDir, "SETUP.EXE");

                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                      $"Запускается setup SQL Server-а по  пути [{setupFilePath}]", null
                     , useInstallLog: useInstallSqlLog);
                
                if ( await RunWinProgramSetupProcessAsync(setupFilePath,
                     "Установка SQL Server 2014 Express"
                     , taskCancelTokenSource.Token
                     ,true
                     , sayProgressToUI
                     , "/ACTION=install /ENABLERANU /IACCEPTSQLSERVERLICENSETERMS=1 /INDICATEPROGRESS /SQMREPORTING=0 /Q /HIDECONSOLE /FEATURES=SQLENGINE,SSMS,REPLICATION /INSTANCENAME=SQLEXPRESS /SECURITYMODE=SQL /SAPWD=p8gTd51mP2 /FILESTREAMLEVEL=3 /FILESTREAMSHARENAME=SQLEXPRESSLOCAL" /// we cannot add here redirect log [ > C:\\Temp\\sqlInstallLog.log"] -settings will be incorrected

                     ) == false) 
                {
                    //FAILED RUN  - MESSAGE
                    return false;
                }

                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                     "Установка SQL Server-а поностью выполнена", null
                    , useInstallLog: useInstallSqlLog);
                /// 
                //Run SQL Server 2014 Setup process
                /// ------------------


                //---------------------
                //remove loaded distrib sfx-cab and extracted folder
                if (File.Exists(targetDownloadSqlExpressDistribFilePath))
                {   File.Delete(targetDownloadSqlExpressDistribFilePath);
                }
                targetDownloadSqlExpressDistribFilePathExtractDir.ClearDirectory();
                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                    "Все файлы установки удалены", null
                   , useInstallLog: useInstallSqlLog);


                // ------------------------                 
                // Turn on Windows Defender. Realtime Monitoring back                                
                //   
                SetMsProtectionalPreference("DisableRealtimeMonitoring", false);
                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                     $" Включен обратно мониторинг в режиме рантайм  для  Защитника Windows", null
                    , useInstallLog: useInstallSqlLog);



                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                    "Установка завершена", null
                   , useInstallLog: useInstallSqlLog);


                return true;
                
            }
            catch (Exception exc)
            {

                LogResponseUI(sayProgressToUI, "Установка SQL Server 2014",
                   null, exceptionMessage: $"ERROR iN [Установка SQL Server 2014] : [{exc.Message}]"
                   , useInstallLog: useInstallSqlLog);

                return false;

            }
            finally
            {
                if (useInstallSqlLog)
                {  File.WriteAllText($@"C:\Temp\{nameof(TryInstallSqlServerExpress2014)}_trace.log", InstallLog.S());
                }
            }

        }
        

#endregion ---------------------------------- SQL SERVER UTILS: INSTALL / ENGINES INFO/ RUN  SCENARIO by PS SQL MODULE --------------------------------------
      

#region ---------------------------------- IE UTILS - RUNASADMIN JUMPLIST -----------------------------------


        /// <summary>
        /// Installing/Adding IE into RUNASADMIN JUMPLIST 
        /// </summary>
        public static bool  TryInstallIESettings(Action<OperationProgressInfo> sayProgressToUI = null
                                                , bool useInstallLog = true, bool clearInstallLogOnStart = true)
        {
            // elevated 
            if (Environment2.HasElevatedPermissions == false || AutomationFactory.IsAvailable == false)
            {
                sayProgressToUI. SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN", null
                    , exceptionMessage: $"Приложение не обладает Elevated Permissions  или  AutomationFactory  все еще не доступна");

                return false;
            }
            
            bool needrestart = false;

            sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                   , progressMessage: $@"Проверяем в реестре наличие ключа [HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers] и создаем если не было.");
                        
            if (RegistryCheckKeyExistPS(REGKEY_RUNAS_JUMPLIST,REGKEY_RUNAS_JUMPLIST_NODRIVE) == false)
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                  , progressMessage: $@"Ключа [HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers] в реестре не обнаружено, попытаемся добавить его.");
                

                //add new Key Layers
                if (RegistryNewKeyPS(REGKEY_RUNAS_JUMPLIST) == false)
                {
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN" , null
                  , exceptionMessage: $@" НЕ УДАЛОСЬ ДОБАВИТЬ ключ [HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers] в реестр ");
                                      
                    return false;
                }
                
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                , progressMessage: $@"Ключ [HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers] УСПЕШНО ДОБАВЛЕН в реестр ");
                
            }
            else
            {

                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                   , progressMessage: $@"Ключ [HKCU:\Software\Microsoft\Windows NT\CurrentVersion\AppCompatFlags\Layers] УЖЕ СУЩЕСТВУЕТ в реестре.");                

            }
            
            ////////////
            //check IE paths in  [Layers- Key].Properties
            ////////////

            sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                   , progressMessage: "Проверяем существует ли IEx64 свойство для  ключа Layers");           
            if (!CheckAppExistInRunAsAdminJumpList(IEx64PATH))
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                    ,progressMessage: " IEx64 свойство для  ключа Layers не существует, добавим IEx64 свойство");
                
                AddAppToRunAsAdminJumpList(IEx64PATH);
                needrestart = true;
            }
            else
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                    , progressMessage: "IEx64  свойство для  ключа Layers уже существует в реестре");

            }


            sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                            , progressMessage: "Проверяем существует ли IEx86 свойство для  ключа Layers");            
            if (!CheckAppExistInRunAsAdminJumpList(IEx86PATH))
            {

                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                    , progressMessage: " IEx86 свойство для  ключа Layers не существует, добавим IEx86 свойство");
                //MessageBox.Show(" IEx86 not exist in RunAsJumpList,let's Try to Add IEx86 to RunAsJumpList");
                AddAppToRunAsAdminJumpList(IEx86PATH);
                needrestart = true;
            }
            else
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                   , progressMessage: "IEx86  свойство для  ключа Layers уже существует в реестре");
            }
                
            if (needrestart)
            {
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, "Установка IE RUNASADMIN"
                  , progressMessage: "Перезагружаем текущий IE Process данного приложения");               
                RestartCurrentIEAppProcess();
            }

            return true;
        }



#endregion---------------------------------- IE UTILS - RUNASADMIN JUMPLIST -----------------------------------


#region ----------------------------------------FULL CUSTOM APP INSTALLS ---------------------------------------------
        

        public static async Task<bool> RunWinProgramSetupProcessAsync(string setupFilePath,
            string taskTitleRun,            
            CancellationToken setupCancelToken,
            bool useLogSB = true,
            Action<OperationProgressInfo> sayProgressToUI = null,
            params string[] args)
        {
            //SQLEXPRWT_x64_ENU\setup.exe / QUIETSIMPLE / ACTION = install / FEATURES = SQLENGINE,SSMS / QS / INSTANCENAME = DSI5000 / IACCEPTSQLSERVERLICENSETERMS = 1 / SQLSYSADMINACCOUNTS = "WIN-2GO0V336FQR" / SAPWD = "Pa$$w0rd"
            // C:\DISTRIB\BD\SQL_SERVER2014\SQLEXPRWT_x64_ENU\setup.exe /QUIETSIMPLE /ACTION=install /FEATURES=SQLENGINE,SSMS  /QS /INSTANCENAME=SQLEXPRESS /IACCEPTSQLSERVERLICENSETERMS=1 /SAPWD="p8gTd51mP2"

            if (!File.Exists(setupFilePath))
            {
                //UI INfo Starting  : 
                if (useLogSB)
                {  InstallLog.AppendLine($"{taskTitleRun} ::: Файл запуска не существует [{ setupFilePath}]");
                }
                sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, taskTitleRun, $"Файл запуска не существует [{ setupFilePath}]");
                return false;
            }

            var winProgramSetupScript = setupFilePath;
            foreach (var arg in args) { winProgramSetupScript += " " + arg; }

            //  Tuple<string, Action<OperationProgressInfo>> state = new Tuple<string, Action<OperationProgressInfo>>(winProgramSetupScript, sayProgressToUI);
            //return await Task.Factory.StartNew( (s) =>
            //{
            //    //unpack s to internal state
            //    Tuple<string, Action<OperationProgressInfo>> intSt = (Tuple<string, Action<OperationProgressInfo>>)s;

                try
                {
                    //UI INfo Starting  : 
                    if (useLogSB)
                    { InstallLog.AppendLine($"{taskTitleRun} ::: Запускаем установку со следующими параметрами [{winProgramSetupScript}]");    //{intSt.Item1}
                }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, taskTitleRun, $" Запускаем установку со следующими параметрами [{winProgramSetupScript}]");    // {intSt.Item1}

                //do not use Environment2.WScriptShell - ThreadAccessException - so create here WScriptShell item in method scope
                //var wscriptObject = CreateCOMObject(Environment2.WScriptShellCOMName);

                    //wscriptObject.Run(intSt.Item1, 0, true); //waiting till setup will be completed                                                            
                     Environment2.WScriptShell.Run(winProgramSetupScript, 0, true); // await 

                return true;

                }
                catch (Exception exc)
                {
                    if (useLogSB)
                    { InstallLog.AppendLine($"{taskTitleRun} ::: ERROR: [{ exc.Message}]");
                    }
                    sayProgressToUI.SendUIMessage(OperationProgressActionsEn.ProgressMessage, taskTitleRun, null, $" ERROR: [{ exc.Message}]");
                    throw new InvalidOperationException($"{nameof(Installer)}.{nameof(RunWinProgramSetupProcessAsync)}(): " + exc.Message);
                }

             // }
            //, state
            //, setupCancelToken
            //, TaskCreationOptions.AttachedToParent//  
            //, TaskScheduler.Default
            //);

            

        }

#endregion ----------------------------------------FULL CUSTOM APP INSTALLS ---------------------------------------------


#region ----------------------------- WINDOWS UNBLOCKING DOWNLOADED FILES --------------------------------------

        public static void UnblockFile(string unblockFilePath)
        {
            var script = $"Unblock-File -Path '{unblockFilePath}'";
            PowerShellManager.RunPSScript(script, returnResult: false, waitOnRun: true);
        }

#endregion ----------------------------- WINDOWS UNBLOCKING DOWNLOADED FILES --------------------------------------


#region ----------------------------------- MS Protection Preferencies  ---------------------------------


        /// <summary>
        /// GetMsProtectionalPreference -  Get one of Win81 OS protectional Preferencies item value
        /// </summary>
        /// <param name="checkFeature"></param>
        /// <returns></returns>
        [CmdPackageDependency(nameof(Installer) + "." + nameof(GetMsProtectionalPreference), PackageTypeEn.Tools, WinTool)]
        public static async Task<T> GetMsProtectionalPreference<T>(string PreferenceKey)
        {
            //Get-WindowsOptionalFeature –Online –FeatureName NetFx3
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(GetMsProtectionalPreference));

            //var targetFeatureName = checkFeature.S().Replace("_", "-");
            var tempResultOutFile = Path.Combine(PowerShellManager.PSTempFolder, $"tmprslt_{Guid.NewGuid().S().Replace("-", "_").Substring(0, 7)}.rslt");

            //  Get_MsProtectionPreference PreferenceKey#ComputerID "OutFile#c:\ProgramData\CRM\PSTemp\trslt_432g23234r4232.rslt"
            var queryScript = $"{WinToolExe} Get_MsProtectionPreference PreferenceKey#{PreferenceKey} \"OutFile#{tempResultOutFile}\"";
                        
            string output = "";
            try
            {
                var runResultOut = Environment2.WScriptShell.Run(queryScript, 0, true);

                //read result from out file
                if (File.Exists(tempResultOutFile))
                {
                    output = File.ReadAllText(tempResultOutFile);
                    File.Delete(tempResultOutFile);
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(GetMsProtectionalPreference)}(): {exc.Message}");
            }
            
            var tval = output.GetParameterValue<T>(PreferenceKey);
            return tval;
            
        }

        /// <summary>
        /// Set MS Protectional Preference - set one of Win81 OS protectional Preferencies item by new value
        /// </summary>
        /// <param name="PreferenceKey"></param>
        /// <param name="newValue"></param>
        [CmdPackageDependency(nameof(Installer) + "." + nameof(SetMsProtectionalPreference), PackageTypeEn.Tools, WinTool)]
        public async static void SetMsProtectionalPreference(string PreferenceKey,object newValue)
        {
            await CmdPackageDependencyManager.CheckInstallDependenciesForCmdWrap(nameof(Installer) + "." + nameof(SetMsProtectionalPreference));
            
            string targetValue = "";           
            if (newValue.GetType() == typeof(bool)  )
            {
                if ( ((bool)newValue) ==  true)
                {  targetValue = "$true";
                } 
                else if(((bool)newValue) == false )
                {  targetValue = "$false";
                }
            }
            else
            {   targetValue = newValue.S();
            }
            
            var queryScript = $"{WinToolExe} Set_MsProtectionPreference PreferenceKey#{PreferenceKey} PreferenceValue#{targetValue}";
            
            try
            {
                var runResultOut = Environment2.WScriptShell.Run(queryScript, 0, true);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Installer)}.{nameof(SetMsProtectionalPreference)}(): {exc.Message}");
            }
        }





#endregion ----------------------------------- MS Protection Preferencies  ---------------------------------


#endif


#region ------------------------------ OS RESTART & SHUTDOWN ---------------------------------

        /// <summary>
        /// Send  to Windows OS Command - SHUTDOWN PC
        /// </summary>
        /// <param name="parameters"></param>
        public static void ShutdownPC(string parameters)
        {
            Environment2.WScriptShell.Run(@"cmd /C shutdown " + parameters, 0, false);
        }

        /// <summary>
        /// Send  to Windows OS Command - RESTART PC
        /// </summary>
        public static void RestartPC()
        {
            ShutdownPC("-f -r -t 5");
        }

        #endregion ------------------------------ OS RESTART & SHUTDOWN ---------------------------------

      
    }
}




#region ---------------------------------- GARBAGE --------------------------------------

//$pkgZipFile = 'C:\DISTRIB\Windows8.1\Win8.1Sources.zip';
//$destinationFile = 'C:\DISTRIB\Windows8.1\Inst';


//public static bool IsWindowsBlockingWebSaveFiles
//{ get; private set; } = GetIsWindowsBlockingWebSaveFiles();


//private static bool GetIsWindowsBlockingWebSaveFiles()
//{
//    // HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Attachments
//    var readKey = $@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Attachments\SaveZoneInformation";

//    var reslt = RegisterReadWSH<int>( readKey);

//    return (reslt != 2);            
//}



//public static void SetWindowsToNonBlockWebSaveFiles()
//{
//    // HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Attachments
//    var readKey = $@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Policies\Attachments\SaveZoneInformation";

//    RegisterWriteWSH(readKey, 2);

//    IsWindowsBlockingWebSaveFiles = GetIsWindowsBlockingWebSaveFiles();
//    if ( IsWindowsBlockingWebSaveFiles ==true) throw new InvalidOperationException(" Windows Still blocking web save file");

//}


//var resltKeyValueIEx64 = RegisterReadPS<string>(REGKEY_RUNAS_JUMPLIST, IEx64PATH, filterSymbols)
//                        .GetParameterValue<String>(IEx64PATH);
//if (resltKeyValueIEx64.IsNullOrEmpty()) return false;
//if (resltKeyValueIEx64 != RUNASADMIN) return false;


//var resltKeyValueIEx86 = RegisterReadPS<string>(REGKEY_RUNAS_JUMPLIST, IEx86PATH, filterSymbols)
//                        .GetParameterValue<String>(IEx86PATH);
//if (resltKeyValueIEx86.IsNullOrEmpty()) return false;
//if (resltKeyValueIEx86 != RUNASADMIN) return false;





#endregion ---------------------------------- GARBAGE --------------------------------------


#if NOT_RELEASED
#endif

