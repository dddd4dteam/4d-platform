﻿

using System;
using System.Diagnostics;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;


using DDDD.Core.Notification;
using DDDD.Core.Serialization;
using DDDD.Core.Threading;
using DDDD.Core.Extensions;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Diagnostics;


#if SERVER && IIS

using System.ServiceModel.Configuration;
using System.Web.Routing;
using System.ServiceModel.Activation;

#endif




namespace DDDD.Core.ComponentModel
{


    /// <summary>
    ///  Action with referenced instance of DCMessage: [void DCAction(ref DCMessage inputCommand)] 
    /// </summary>
    /// <param name="inputCommand"></param>
    public delegate void DCAction(ref IDCMessage inputCommand);


    ///// <summary>
    /////  Action with referenced instance of DCMessage2: [void DCAction2(ref DCMessage2 inputCommand)]
    ///// </summary>
    ///// <param name="inputCommand"></param>
    //public delegate void DCAction2(ref DCMessage2 inputCommand);


    ///// <summary>
    /////  Action with referenced instance of DCMessage2: [void DCAction3(ref DCMessage3 inputCommand)]
    ///// </summary>
    ///// <param name="inputCommand"></param>
    //public delegate void DCAction3(ref DCMessage3 inputCommand);


    /// <summary>
    /// Conceptually DCManager  - is the base unit(app component too) of cross-tier/side (one code file for CLIENT and SERVER side logic - splitted by precompile directives) Business Logic operations.
    /// <para/> DCManager Generic  base class which publishes static TManager Current property, where TManager is your custom DCmanager Type.
    /// <para/> By naming convention [[CustomManagerName]Manager] -your manager class Name, always should be ending with word [Manager]
    /// </summary>
    /// <typeparam name="TManager"></typeparam>
    public class DCManager<TManager> : DCManager
        where TManager : DCManager,new()
    {

        #region -------------------------------- SINGLETON -------------------------------------


        static LazyAct<TManager> LA_CurrentBootstrapper =
               LazyAct<TManager>.Create(
                   (args) => { return new TManager(); }
                   , null//locker
                   , null //args
                   );

        /// <summary>
        /// Instance of AppBootstrapper  
        /// </summary>
        public static TManager Current
        {
            get
            {
                return LA_CurrentBootstrapper.Value;
            }
        }

        #endregion -------------------------------- SINGLETON -------------------------------------


    }



    /// <summary>
    /// Conceptually DCManager - is the base unit of cross-tier/side (one code file for CLIENT and SERVER side logic - splitted by precompile directives) Business Logic operations. 
    /// <para/> It also contains static Dictionary with all of the DCManager that was found with the help of IOC Container( ComponentContainer ). 
    /// <para/> Manager can contains one or more known communication-type services/controllers( not only one type of Communication), also can exist without any Communication service( for example like your custom CRUDManager ...)   
    /// <para/> Conventional Manager Name - [[CustomManagerName]Manager] - it is used to create Wcf service client and WebApi Controller's HttpClient client as [CustomManagerName] 
    /// </summary>
    public class DCManager
    {

        #region ------------------------ CTOR ---------------------------

        protected DCManager()
        {
            Initialize();
        }

        #endregion ------------------------ CTOR ---------------------------


        private static readonly object locker = new object();



        #region ----------------------- INITIALIZE -----------------------

#if CLIENT

        /// <summary>
        /// Here we need to define,load, init :
        /// - KnownTypes  for TSS Serializer with such Key as in DCManager
        /// - Initialize TSSerializer with the same key as this Manager with loaded KnownTypes
        /// </summary>
        protected virtual void Initialize()
        {            

            InitManagerIdentification();

            InitializeDomainKnownTypes();

            InitializeCommunication();
                      
            //Collect Precompile Client Methods(); - on client side we needn't in       

        }



        /// <summary>
        /// DCManager can care of it's Serializers and Model's KnownTypes for it's communication serialization operations; 
        ///     It can be done by the following ways: 
        /// <para/>  1Way -  Manager can be marked by ServiceKnownContract(typeof KnownTypesLoader, method - KnownTypes method)
        /// <para/>  2Way -  Current Manager can contains it's own static method - [static IEnumerable{Type} LoadKnownTypes()]
        /// <para/>  REMEMBER: If manager doesn't point to any model's KnownType  its Serializer continue to be null-value-inited.
        /// </summary>
        protected virtual void InitializeDomainKnownTypes()
        {
            // Initial Point of SERIALIZER
            ITypeSetSerializer targetSerializer = TypeSetSerializer.AddOrUseExistTSS(ShortKey);
            targetSerializer.AddKnownTypesTPL( typeof(DCMessage) );  // typeof(CommandMessage),

            //next : overriden manager class's another custom Domain types adding to TSS
            //AddDomainKnownTypes(customTypes here)
        }

        /// <summary>
        /// Domain Types will be Added to TSS serializer
        /// </summary>
        /// <param name="newDomainTypes"></param>
        protected void AddDomainKnownTypes(params Type[]  newDomainTypes)
        {
            ITypeSetSerializer targetSerializer = TypeSetSerializer.AddOrUseExistTSS(ShortKey);
            targetSerializer.AddKnownTypesTPL(newDomainTypes);
        }




#elif SERVER

        /// <summary>
        /// DCAction one parameter Type is - typeof(IDCMessage).MakeByRefType()
        /// </summary>
        static readonly Type DcActionParameterType = typeof(IDCMessage).MakeByRefType();

      


        /// <summary>
        /// All Custom Methods that will EndsWith keyword [Server] will means as Server-side you Manager API method.
        /// Then such method will be precompiled and end delegate will be saved in [ServerDCActions] dictionary        /// 
        /// </summary>
        const string ServerMethodKeyword = "Server";

        /// <summary>
        /// Methods Selector on Server side - Reflection Binding.
        /// <para/> By default ServerSideMethodsSelector = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy
        /// </summary>
        public BindingFlags ServerSideMethodsSelector
        { get; set; } = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;


        /// <summary>
        ///  ServerDCActions - CommandManager Actions with DCMessage
        /// </summary>
        protected Dictionary<string, DCAction> ServerDCActions
        { get; private set; } = new Dictionary<string, DCAction>();


        ///// <summary>
        /////  ServerDCActions - CommandManager Actions with DCMessage2
        ///// </summary>
        //protected Dictionary<string, DCAction2> ServerDCActions2
        //{ get; private set; } = new Dictionary<string, DCAction2>();

        ///// <summary>
        ///// ServerDCActions - CommandManager Actions with DCMessage3
        ///// </summary>
        //protected Dictionary<string, DCAction3> ServerDCActions3
        //{ get; private set; } = new Dictionary<string, DCAction3>();



        /// <summary>
        /// Here we need to define, load, init : 
        /// <para/>  - ManagerIdentification ,  Initialize manager's identification
        /// <para/>  - Precompile managers server-side Methods of Two-side Commands
        /// <para/>  - load KnownTypes - Initialize TSSerializer/other serializers with known Types, we  Use this Manager Key 
        /// <para/>  - Initialize All server-side DCCUnits's Configurations 
        /// </summary>
        protected virtual void Initialize()
        {
            // Initialize manager's identification 
            InitManagerIdentification();

            // Precompile managers server-side Methods of Two-side Commands
            InitCollectPrecompileMethods();

            // load KnownTypes - Initialize TSSerializer/other serializers with known Types, we  Use this Manager Key 
            InitializeDomainKnownTypes();

            Logger = DCLogger.CreateEmptyLogger(ShortKey);
            //Initialize All server-side DCCUnits's Configurations 
            InitializeCommunication();

        }



        /// <summary>
        /// DCManager can care of it's Serializers and Model's KnownTypes for it's communication serialization operations; 
        ///     It can be done by the following way:         
        ///   Current Manager can contains it's own static method - [static IEnumerable{Type} InitializeDomainKnownTypes()]        
        /// </summary>
        protected virtual void InitializeDomainKnownTypes()
        {
            // Initial Point of SERIALIZER
            ITypeSetSerializer targetSerializer = TypeSetSerializer.AddOrUseExistTSS(ShortKey);
            targetSerializer.AddKnownTypesTPL( typeof(DCMessage)); 

            ///
            /// next : override to add other custom Domain types to TSS        
            ///     you can use for this -  AddDomainKnownTypes(customTypes here)

        }

        /// <summary>
        /// Domain Types will be Added to TSS serializer
        /// </summary>
        /// <param name="newDomainTypes"></param>
        protected void AddDomainKnownTypes(params Type[] newDomainTypes)
        {
            ITypeSetSerializer targetSerializer = TypeSetSerializer.AddOrUseExistTSS(ShortKey);
            targetSerializer.AddKnownTypesTPL(newDomainTypes);
        }


      

        /// <summary>
        /// Collect all server-methods that  1 condition- contains word [Server] and  2 condition -can be called by DCAction delegate; 
        /// and precompile and cache calls for each of this methods internally. 
        /// </summary>
        protected virtual void InitCollectPrecompileMethods()
        {
            var serverCommandMethods = GetType().GetMethods(ServerSideMethodsSelector)
                .Where(m => m.Name.Contains(ServerMethodKeyword));

            if (serverCommandMethods.IsWorkable())
            {
                foreach (var possibleServerMethod in serverCommandMethods)
                {
                    // if it's really not method with  target signature of  DCAction delegate then go further
                    if (possibleServerMethod.GetParameters().Count() != 1) continue;

                    if (possibleServerMethod.GetParameters()[0].ParameterType == DcActionParameterType)
                    {
                        if (possibleServerMethod.IsStatic)
                        {
                            var serverDCAction2 = (DCAction)possibleServerMethod.CreateDelegate(typeof(DCAction), null);
                            ServerDCActions.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction2);
                        }
                        else
                        {
                            var serverDCAction2 = (DCAction)possibleServerMethod.CreateDelegate(typeof(DCAction), this);
                            ServerDCActions.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction2);
                        }
                    }
                  

                }

            }
        }


#endif





        #endregion ----------------------- INITIALIZE -----------------------
        

        #region ----------------- IDENTIFICATION KEY by CONVENTIONS ---------------------------
        

        /// <summary>
        /// ManagerType - Type of this DCManager instance. 
        /// It's cached  GetType() value. 
        /// </summary>
        public Type ManagerType
        { get; private set; }

        /// <summary>
        /// ManagerName is the same as  ManagerType.Name.
        /// </summary>
        public string Name
        { get; private set; }

        /// <summary>
        /// This Key value - [ManagerType.FullName]       
        /// </summary>
        public string FullName
        { get; private set; }


        /// <summary>
        /// This ShortKey value - [ManagerType.Name] after ShortKey Trimming. ShortKey previosly known as OwnerName.
        /// </summary>
        public string ShortKey
        { get; private set; }
        

        /// <summary>
        /// Get Hash Code for ID - [ManagerType.FullName.GetHashCode()]
        /// </summary>
        public int ID
        { get; private set; }




        /// <summary>
        /// Initing the following properties as : 
        /// <para/>   [ManagerType] as- current Manager Type
        /// <para/>   , [FullName] as -ManagerType.FullName
        /// <para/>   , [ID] as - Key.GetHashCode() 
        /// <para/>   , [ShortKey] as - ComponentItemInfo.GetShortKey(ManagerType.Name)
        /// </summary>
        protected virtual void InitManagerIdentification()
        {
            ManagerType = GetType();
            FullName = ManagerType.FullName;
            ShortKey = ComponentMatch.GetShortKey(ManagerType.Name);
            ID = FullName.GetHashCode();
            
        }


        #endregion ----------------- IDENTIFICATION KEY by CONVENTIONS ---------------------------


        #region ------------------------- DCMANAGERS CACHE DICTIONARIES ----------------------------

        /// <summary>
        /// DCManager instancies dictionary by Key(Type.FullName - Key). 
        /// Each of DCManager always has only one singleton instance.  That's only references dictionary. 
        /// </summary>
        protected static Dictionary<string, DCManager> ManagersByKey
        { get; } = new Dictionary<string, DCManager>();
                


        /// <summary>
        /// DCManager instancies dictionary by ShortKey(Type.Name - ShortKey).  
        /// Each of DCManager always has only one singleton instance.  That's only references dictionary. 
        /// </summary>
        protected static Dictionary<string, DCManager> ManagersByShortKey
        { get; } = new Dictionary<string, DCManager>();


        /// <summary>
        /// DCManager instancies dictionary by ID(Type.FullName.GetHashCode() - ID).
        /// Each of DCManager always has only one singleton instance.  That's only references dictionary. 
        /// </summary>
        protected static Dictionary<int, DCManager> ManagersByID
        { get; } = new Dictionary<int, DCManager>();

        #endregion ------------------------- DCMANAGERS CACHE DICTIONARIES ----------------------------
        

        #region ------------------------------ GET/CREATE MANAGER BY SHORTKEY ------------------------------


        /// <summary>
        /// Get instance of DCManager,  it can exist in LoadedManagersByKey internal dictionary of DCManagers, 
        /// Or create DCManager's instance, then cache this instance into LoadedManagersByKey dictionary, and then return instance as existed item from LoadedManagersByKey.
        /// </summary>
        /// <param name="shortKey"></param>
        /// <returns></returns>
        protected internal static DCManager GetAndCacheDCManagerByShortKey(string shortKey)
        {
            if (ManagersByShortKey.ContainsKey(shortKey)) return ManagersByShortKey[shortKey];

            lock (locker)
            {//thread safety activating Manager class only on its direct method call

                if (ManagersByShortKey.ContainsKey(shortKey)) return ManagersByShortKey[shortKey];

                //create-Add instance to dictionary - new DCManager with FullName
                ManagersByShortKey.Add(shortKey, CreateManagerByShortKey(shortKey));//get singleton instance simply
                ManagersByID.Add(ManagersByShortKey[shortKey].ID, ManagersByShortKey[shortKey]);
                ManagersByKey.Add(ManagersByShortKey[shortKey].FullName, ManagersByShortKey[shortKey]);

                return ManagersByShortKey[shortKey];
            }
        }



        /// <summary>
        /// Create DCManager instance, it can exist in internal dictionary of DCManagers.         
        /// </summary>
        /// <param name="shortKey"></param>
        /// <returns></returns>
        static DCManager CreateManagerByShortKey(string shortKey)
        {
            var dcManagersComponentClass = ComponentsContainer.GetComponentsClass(ComponentClassEn.DCManagers);

            var targetComponent = dcManagersComponentClass.GetComponentWithShortKey(shortKey);

            return targetComponent.ResolveInstanceTBase<DCManager>();// 
        }

        #endregion ------------------------------ GET MANAGER BY SHORTKEY ------------------------------
        
        
        #region ------------------------- COMMUNICATION  COMMON ----------------------


        /// <summary>
        /// Info about Communication Units that this  DCManager is going to use in its work.
        /// </summary>
        public Dictionary<string, DCCUnit> CommunicationUnits
        { get; private set; } = new Dictionary<string, DCCUnit>();
        
        private void InitializeCommunication()
        {
            var CommunicationUnitInfos = this.GetTypeAttributes<DCCUnitOrderAttribute>();
            CommunicationUnitInfos.ForEach(  (unInf) => CommunicationUnits.Add(unInf.ScenarioKey
                                             , DCCUnit.GetDCCUnit(ManagerType, unInf.ScenarioKey)  )  );
#if CLIENT
            // create clients for each CommunicationUnit
            foreach (var dcUnit in CommunicationUnits)
            {
                // if we already contains end Client for this DC Communication Unit
                if (DCClients.ContainsKey( dcUnit.Value.RegistrationInfo.ScenarioKey)) continue;


                //check that DCCUnitClientFactory for such dcUnit.CommunicationUnitKey  exists in Application
                var dcUnitTargetFabric = ComponentsContainer.GetComponent<DCCUnitClientFactoryAttribute>(ComponentClassEn.DCCUnitClientFactories
                                                                                                        , (cf) => cf.ScenarioKeys.Contains(dcUnit.Key))
                                                                                                        .ResolveInstanceBoxed() as IDCCUnitClientFactory;
                
                if (dcUnitTargetFabric.IsNull()) continue;

                //create target client for DCUnit
                DCClients[dcUnit.Value.RegistrationInfo.ScenarioKey] = dcUnitTargetFabric.CreateClient(dcUnit.Value) as IDCCUnitClient;


            }
#endif
        }
        
        #endregion ------------------------- COMMUNICATION  COMMON ----------------------
        

        #region ----------------------  COMMUNICATION  CLIENTS ---------------------------------

#if CLIENT

        /// <summary>
        /// All ordered by this owner DCCommunication Clients by Scenario keys.
        /// </summary>
        protected Dictionary<string, IDCCUnitClient> DCClients
        { get; } = new Dictionary<string, IDCCUnitClient>();
        

#elif SERVER

        ///// <summary>
        ///// Wcf REST ServiceRoute in RouteTable that using for current DCManager communication with WCF communication technology
        ///// </summary>
        //public DCServiceRoute DCWcfServiceRoute
        //{ get; protected internal set; }


#endif

        #endregion ----------------------  COMMUNICATION  CLIENTS ---------------------------------


        #region --------------------------------------- NOTIFICATION UI Client about COMMAND PROGRESS  ----------------------------------------


        /// <summary>
        /// Notifying UI Client that Network is  currently unavailable
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        protected virtual void NotifyNetworkDoesntWorkAndStopCommandProcessing(IDCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;

            // no network connection
            // throw new NotSupportedException("Network doesn't support workable connection now to make service call");

            //reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.StopCommandIndication, command.ID));
            reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.DetailMessage, Guid.Empty, null
                                                    , "Network doesn't work. Command cannot be processed")
                                                  );


        }



        /// <summary>
        /// Notifying UI Client that we Starting DCMessage Processing 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        protected virtual void NotifyStartCommandProcessing(IDCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;

            reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.ClearDetailMessages, Guid.Empty )); //command.ID
            reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.StartCommandIndication | OperationProgressActionsEn.ProgressMessage | OperationProgressActionsEn.DetailMessage
                                             , Guid.Empty //command.ID
                                             , "Starting  Command...."
                                             , "to  Controller:[{0}],   Name :[{1}]".Fmt( command.TargetDCManager, command.Command) 
                                                                                 )
                                         );
        }



        /// <summary>
        /// Notifying UI Client About received DCMessage result
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        protected virtual void NotifyAboutCommandSuccessResultProgress(IDCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;


            if (command.HasOperationSuccessfullyCompleted)// SuccessfullyCompleted
            {
                reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.StopCommandIndication, Guid.Empty ));  

                if (command.ProcessingSuccessMessage != string.Empty) //some real successfully message
                {
                    reporter.NotifyProgress(OperationProgressInfo.Create(OperationProgressActionsEn.DetailMessage, Guid.Empty ,  
                                                                                        null,
                                                                                        command.ProcessingSuccessMessage));
                }

            }
        }



        /// <summary>
        /// Notifying UI Client - about command Failure results on server , if it's so - command.HasServerErrorHappened
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        protected virtual void NotifyAboutCommandFailureResultProgress(IDCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;
            var ServerDefaultErrorMessage = DCSConsts.Server_Default_Error_Message;

            //checking results from server  and show Progress Messages 
            if (command.HasServerErrorHappened)// Server can say about some error 
            {
#if DEBUG                  //Debug Message about error
                reporter.NotifyProgress(
                                    OperationProgressInfo.Create(OperationProgressActionsEn.ErrorMessage, Guid.Empty , null, null, command.ProcessingFaultMessage)//  command.ID
                                                );

#endif
                //if not debug - still show to the client some common  phrase like [SERVER ERROR HAPPENED]
                reporter.NotifyProgress(
                                    OperationProgressInfo.Create(OperationProgressActionsEn.ErrorMessage, Guid.Empty , null, null, ServerDefaultErrorMessage) //  command.ID
                                                );
            }
        }



        /// <summary>
        ///  Notifying UI Client - about Communication Exceptions that happened during a try to call WCF Service
        /// </summary>
        /// <param name="aggexc"></param>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        [Conditional("DEBUG")]
        protected virtual void NotifyAboutCommandTaskFailureExceptionsProgress(AggregateException aggexc, DCMessage command, DCProgressReporter reporter)
        {
            if (aggexc == null || aggexc.InnerExceptions == null || aggexc.InnerExceptions.Count == 0) return;
            if (reporter == null) return;

            //var DefaultErrorMessage = "Unknown Command execution error happened.";

            foreach (var excItem in aggexc.InnerExceptions)
            {
                // TODO CONCATENATE MESSAGES
                reporter.NotifyProgress(
                                        OperationProgressInfo.Create(OperationProgressActionsEn.DetailMessage, Guid.Empty , null, null, excItem.Message) // command.ID
                                       );
            }
        }


        #endregion ---------------------------------------  NOTIFICATIONS COMMAND PROGRESS  ----------------------------------------



        #region -------------------------- EXECUTE COMMAND ----------------------------------


#if CLIENT && SL5
             
        
    
        /// <summary>
        /// Wrap of functional action of ExecuteCommand, it adds pack/unpack /logging/ diagnostics   utils and details.
        /// You can add  custom server logging in
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <param name="commandOperation"></param>
        /// <returns></returns>
        protected internal virtual async Task<IDCMessage> ExecuteCommandOnClient(
                                                                  string scenarioKey,  
                                                                  string currentCommandName  
                                                                , DCProgressReporter reporter
                                                                , params DCParameter[] actionParameters)
        {

            if (!CommunicationUnits.ContainsKey(scenarioKey))
                throw new InvalidOperationException("Current Manager  doesn't contain Client with pointed ScenarioKey");

            
            var targetCommunicationUnit = CommunicationUnits[scenarioKey];

            var commandMessage = targetCommunicationUnit.CreateMessage(ShortKey,currentCommandName, actionParameters);
            // indication - Starting if newtwork enable start
            NotifyStartCommandProcessing(commandMessage, reporter);

            commandMessage.SetClientSendTime(DateTime.Now); // SetDiagnosticsOnClientSendTimeDbg(commandMessage);

            //PACK INTERNAL SERIALIZATION     only FOR WCF - precisionly for all MDCxx_xxxxx SerializationModes                   
            commandMessage = targetCommunicationUnit.PackMessageIfNeeds(commandMessage);


            var targetDCClient = DCClients[targetCommunicationUnit.RegistrationInfo.ScenarioKey];

            return await targetDCClient.ExecuteCommand(commandMessage, reporter, NotifyNetworkDoesntWorkAndStopCommandProcessing)
                  .ContinueWith(
                            (taskResultMessage) =>
                            {
                                if (taskResultMessage.Status == TaskStatus.RanToCompletion)
                                {
                                    // analising DCMessage received from server Result Message 
                                    var serverMessage = taskResultMessage.Result;
                                    serverMessage.SetClientRecievedResultFromServerTime(DateTime.Now);
                                    //UNPACK MESSAG IF NEED IT. INTERNAL SERIALIZATION only FOR WCF - precisionly for all MDCxx_xxxxx SerializationModes
                                    commandMessage = targetCommunicationUnit.UnpackMessageIfNeeds(serverMessage);                                     
                                    
                                    //if HasServerErrorHappened - notify
                                    NotifyAboutCommandFailureResultProgress(commandMessage, reporter); //

                                    // successfully completed on server - notify
                                    NotifyAboutCommandSuccessResultProgress(commandMessage, reporter);
                                    
                                    //continue TriggerFaultAferAction - Will be Called if it exist
                                    //if (insideTSout.Item3 != null)
                                    //{ insideTSout.Item3(t); }

                                }
                                else if (taskResultMessage.Status == TaskStatus.Faulted)
                                {
                                    commandMessage.ProcessingFaultMessage = "Communication not sended to server";
                                    return commandMessage;
                                }
                                return commandMessage;
                            }
                 );            
      
        }







#elif SERVER && IIS


        /// <summary>
        /// Wrap of functional action of ExecuteCommand, it adds pack/unpack /logging/ diagnostics   utils and details.
        /// You can add  custom server logging in
        /// </summary>
        /// <param name="inputCommand"></param>
        /// <param name="commandOperation"></param>
        /// <returns></returns>
        protected internal virtual IDCMessage ExecuteCommandOnServer(string dccScenarioKey, ref IDCMessage inputCommand)
        {
            var communicationUnit = CommunicationUnits[dccScenarioKey];
            try
            {
                //inputCommand = communicationUnit.UnpackMessageIfNeeds(inputCommand);
                
                ////deserialize ParametersIf that needs in it                
                //inputCommand = communicationUnit.UnpackParametersIfNeeds(inputCommand);

                ////set diagnostics  time[in] info
                //inputCommand.SetServerReceivedFromClientTime(DateTime.Now);

                // we need To Determine Action and call it's processing for current DCMessage                
                ServerDCActions[inputCommand.Command](ref inputCommand);
                                
            }
            catch (Exception exc)
            {
                //LOGGING :
                //to client  - we beleave that client also should known some about server error and communication channel should never be broken.
                // on server too - of course server also can save this error into it's own log. Add your server logging code into LogError()
                
                //LogOperationFault(inputCommand, exc);

            }
            //here we must clean our input Parameters to not to sent them again as server output parameters - it's only internal command scope logic
            //inputCommand.ClearInputParameters();
            //inputCommand.SetServerSendToClientTime(DateTime.Now);
            //Serialize ParametersIf that needs in it                
            //inputCommand = communicationUnit.PackParametersIfNeeds(inputCommand);
            //inputCommand  = communicationUnit.PackMessageIfNeeds(inputCommand);           
            return inputCommand;
        }



#endif

        #endregion -------------------------- EXECUTE COMMAND ----------------------------------

        #region ------------------------ LOGGING ------------------------------
       
        
        /// <summary>
        /// Logger instance - for logging on DCManager diagnostic logging.
        /// </summary>
        public DCLogger Logger
        { get; private set; }

        #endregion ------------------------ LOGGING ------------------------------



        #region----------------------- SECURITY, ENCRIPTION ------------------------------------

        /// <summary>
        /// Authentication Encoding Fnc from DCManager - to encode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecEncodeDataFunc { get; set; }
        /// <summary>
        /// Authentication Decoding Fnc from DCManager - to decode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecDecodeDataFunc { get; set; }


        #endregion ------------------------------- SECURITY ------------------------------------


    }

}






#region -------------------------------- GARBAGE ------------------------------
//public bool IncludeExceptionStackInfo { get; private set; }


//protected internal virtual void LogOperationFault(IDCMessage inputMessage, Exception packException)
//{
//    //inputMessage.CreateInParam("Place", "operation");
//    var contextErrorMessage = BuildLogContextMessage(inputMessage, packException);
//    Log.ErrorDbg(contextErrorMessage);//NLOG           
//}


/// <summary>
/// Log about  encountered errors  during  ExecuteCommand  processing.
/// Add your server logging code into LogError() if need
/// </summary>
/// <param name="inputcommand"></param>
/// <param name="FaultExc"></param>
/// <param name="IsUnpackFaultException"></param>
/// <param name="IsPackFaultException"></param>
//        protected internal virtual void LogFaultError(IDCMessage inputcommand, Exception FaultExc, bool IsUnpackFaultException = false, bool IsPackFaultException = false)
//        {

//#if DEBUG   // if debug save full fault exception info into ErrorMessage
//            string errorMessage = null;
//            if (IsUnpackFaultException)
//            {
//                errorMessage = "Unpacking Fault with Exception -[{0}]]:  message- [{1}] ".Fmt(FaultExc.GetType().Name, FaultExc.Message);
//            }
//            else if (IsPackFaultException)
//            {
//                errorMessage = "Packing Fault with Exception -[{0}]]:  message- [{1}] ".Fmt(FaultExc.GetType().Name, FaultExc.Message);
//            }
//            else
//            {
//                errorMessage = "Processing Fault with Exception -[{0}]]:  message- [{1}]  ".Fmt(FaultExc.GetType().Name, FaultExc.Message);
//            }

//            inputcommand.ProcessingFaultMessage = errorMessage;


//            //  SERVER LOGGING HERE:            
//            //  external LOG PROVIDER INTEGRATION TO-DO here
//            //  save this error into the server log  Log4net/NLOG/EnterpriseLibraryLogger ... 
//            Log.Error(inputcommand.ProcessingFaultMessage);//NLOG



//#elif RELEASE   // if release save only common fault  standart phrase message 

//            inputcommand.ProcessingFaultMessage = " The server encountered an unknown error";
//#endif
//        }

//protected internal virtual void LogPackingError(IDCMessage inputMessage, Exception packException)
//{
//    inputMessage.CreateInParam("Place", "Packing");
//    var contextErrorMessage = BuildLogContextMessage(inputMessage, packException);
//    Log.ErrorDbg(contextErrorMessage);//NLOG     
//}

//protected internal virtual void LogUnpackingError(IDCMessage inputMessage, Exception packException)
//{
//    inputMessage.CreateInParam("Place", "Unpacking");
//    var contextErrorMessage = BuildLogContextMessage(inputMessage, packException);
//    Log.ErrorDbg(contextErrorMessage);//NLOG 
//}


#region ------------------------------ GET/CREATE MANAGER BY KEY ------------------------------

///// <summary>
///// Get instance of DCManager we it already exist in LoadedManagersByKey internal dictionary of DCManagers, 
///// Or create DCManager's instance, then cache this instance into LoadedManagersByKey dictionary, and then return instance as existed item from LoadedManagersByKey.
///// </summary>
///// <param name="key"></param>
///// <returns></returns>
//protected internal static DCManager GetAndCacheDCManagerByKey(string key)
//{
//    if (ManagersByKey.ContainsKey(key)) return ManagersByKey[key];

//    lock (locker)
//    {//thread safety activating Manager class only on its direct method call

//        if (ManagersByKey.ContainsKey(key)) return ManagersByKey[key];

//        //create-Add instance to dictionary - new DCManager with FullName
//        ManagersByKey.Add( key, CreateManagerByKey(key) );
//        ManagersByID.Add( ManagersByKey[key].ID, ManagersByKey[key] );
//        ManagersByShortKey.Add( ManagersByKey[key].ShortKey , ManagersByKey[key] );

//        return ManagersByKey[key];
//    }
//}


///// <summary>
///// Create DCManager instance, we it already exist in LoadedManagersByKey internal dictionary of DCManagers.         
///// </summary>
///// <param name="Key"></param>
///// <returns></returns>
//static DCManager CreateManagerByKey(string Key)
//{
//    var dcManagersComponentClass = ComponentsContainer.GetComponentsClass(ComponentClassEn.DCManagers);

//    var targetComponent = dcManagersComponentClass.GetComponentWithKey(Key);

//    return targetComponent.ResolveInstanceTBase<DCManager>();// 
//}

#endregion ------------------------------ GET MANAGER BY KEY ------------------------------


#region ------------------------------ GET/CREATE MANAGER BY ID ------------------------------

///// <summary>
///// Get instance of DCManager we it already exist in LoadedManagersByID internal dictionary of DCManagers, 
///// Or create DCManager's instance, then cache this instance into LoadedManagersByID dictionary, and then return instance as existed item from LoadedManagersByID.
///// </summary>
///// <param name="id_Hash"></param>
///// <returns></returns>
//protected internal static DCManager GetAndCacheDCManagerByID(int id_Hash)
//{
//    if (ManagersByID.ContainsKey(id_Hash)) return ManagersByID[id_Hash];

//    lock (locker)
//    {//lazy activating Manager class only on its direct method call

//        if (ManagersByID.ContainsKey(id_Hash)) return ManagersByID[id_Hash];

//        //create-Add instance to dictionary - new DCManager with FullName
//        ManagersByID.Add(id_Hash, CreateManagerByID(id_Hash));
//        ManagersByKey.Add(ManagersByID[id_Hash].FullName, ManagersByID[id_Hash]);
//        ManagersByShortKey.Add(ManagersByID[id_Hash].ShortKey, ManagersByID[id_Hash]);
//        return ManagersByID[id_Hash];
//    }
//}


//static DCManager CreateManagerByID(int id_Hash)
//{
//    var dcManagersComponentClass = ComponentsContainer.GetComponentsClass(ComponentClassEn.DCManagers);
//    var targetComponent = dcManagersComponentClass.GetComponentWithID(id_Hash);
//    return targetComponent.ResolveInstanceTBase<DCManager>(ignoreSingleton: true);
//}

#endregion ------------------------------ GET/CREATE MANAGER BY ID ------------------------------



///// <summary>
///// DCAction2 one parameter Type is - typeof(DCMessage2).MakeByRefType()
///// </summary>
//static readonly Type DcAction2ParameterType = typeof(DCMessage2).MakeByRefType();


///// <summary>
///// DCAction3 one parameter Type is - typeof(DCMessage3).MakeByRefType()
///// </summary>
//static readonly Type DcAction3ParameterType = typeof(DCMessage3).MakeByRefType();


//protected internal virtual DCMessage2 ExecuteCommandOnServer(string dccScenarioKey, ref DCMessage2 inputCommand)
//{
//    var communicationUnit = CommunicationUnits[dccScenarioKey];
//    //need no unpack+ pack operations
//    try
//    {
//        inputCommand.SetServerReceivedFromClientTime(DateTime.Now);

//        // we need To Determine Action and call it's processing for current DCMessage                
//        ServerDCActions[inputCommand.Command](ref inputCommand);                

//        inputCommand.SetServerSendToClientTime(DateTime.Now);
//    }
//    catch (Exception exc)
//    {
//        //LOGGING :
//        //to client  - we beleave that client also should known some about server error and communication channel should never be broken.
//        // on server too - of course server also can save this error into it's own log. Add your server logging code into LogError()
//       LogFaultError(inputCommand, exc);
//    }

//    return inputCommand;
//}

//protected internal virtual DCMessage3 ExecuteCommandOnServer(string dccScenarioKey, ref DCMessage3 inputCommand)
//{
//    var communicationUnit = CommunicationUnits[dccScenarioKey];
//    //need no unpack+ pack operations
//    try
//    {
//        inputCommand.SetServerReceivedFromClientTime(DateTime.Now);

//        // we need To Determine Action and call it's processing for current DCMessage                
//        ServerDCActions3[inputCommand.Command](ref inputCommand);

//        inputCommand.SetServerSendToClientTime(DateTime.Now);
//    }
//    catch (Exception exc)
//    {
//        //LOGGING :
//        //to client  - we beleave that client also should known some about server error and communication channel should never be broken.
//        // on server too - of course server also can save this error into it's own log. Add your server logging code into LogError()
//        LogFaultError(inputCommand, exc);
//    }

//    return inputCommand;
//}


//else if (possibleServerMethod.GetParameters()[0].ParameterType == DcAction2ParameterType)
//{
//    if (possibleServerMethod.IsStatic )
//    {
//        var serverDCAction2 = (DCAction2)possibleServerMethod.CreateDelegate(typeof(DCAction2), null);
//        ServerDCActions2.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction2);
//    }
//    else
//    {
//        var serverDCAction2 = (DCAction2)possibleServerMethod.CreateDelegate(typeof(DCAction2), this);
//        ServerDCActions2.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction2);
//    }                        
//}
//else if (possibleServerMethod.GetParameters()[0].ParameterType == DcAction3ParameterType)
//{
//    if (possibleServerMethod.IsStatic)
//    {
//        var serverDCAction3 = (DCAction3)possibleServerMethod.CreateDelegate(typeof(DCAction3), null);
//        ServerDCActions3.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction3);
//    }
//    else
//    {
//        var serverDCAction3 = (DCAction3)possibleServerMethod.CreateDelegate(typeof(DCAction3), this);
//        ServerDCActions3.Add(possibleServerMethod.Name.Replace(ServerMethodKeyword, ""), serverDCAction3);
//    }
//}


/// <summary>
/// Instance of DCManager of [TManaget] Type - your custom end DCManager Type
/// </summary>
//public static TManager Current
//{
//    get
//    {
//        return GetAndCacheDCManager<TManager>();
//    }
//}




/// <summary>
/// Owner Name sound looks like this - [OwnerName]Manager - for example ClientReportManager class name, and it's OwnerName=ClientReport.
/// </summary>
//  public string OwnerName
//  { get; protected set; }
//  OwnerName = ManagerType.Name.Replace(ManagerClassEnding, "");



///// <summary>
///// Get instance of DCManager we it already exist in LoadedManagersByID internal dictionary of DCManagers, 
///// Or create DCManager's instance, then cache this instance into LoadedManagersByID dictionary, and then return instance as existed item from LoadedManagersByID.
///// </summary>
///// <typeparam name="TManager"></typeparam>
///// <returns></returns>
//protected internal static TManager GetAndCacheDCManager<TManager>()
//   where TManager : DCManager
//{
//    int id_Hash = typeof(TManager).FullName.GetHashCode();

//    locker.LockAction(
//        ()=> ManagersByID.NotContainsKey(id_Hash),
//        () =>
//        {
//            //create-Add instance to dictionary - new DCManager with FullName
//            ManagersByID.Add(id_Hash, CreateManagerByID(id_Hash));
//            ManagersByKey.Add(ManagersByID[id_Hash].Key, ManagersByID[id_Hash]);
//        }
//        );

//    return ManagersByID[id_Hash] as TManager;
//}




// (DCAction)Delegate.CreateDelegate(DcActionParameterType, possibleServerMethod);
//var parameterDCMessage = Expression.Parameter(DcActionParameterType);
//var serverDCAction = Expression.Lambda<DCAction>(
//        Expression.Call(Expression.Constant(this), possibleServerMethod, parameterDCMessage)
//      , parameterDCMessage).Compile();

///// <summary>
///// Dictionary of Lazy Collected Domain DCManager  Types 
///// </summary>
//protected static LazySlim<Dictionary<string, Type>> LA_DomainManagerTypes
//{ get; } = LazySlim<Dictionary<string, Type>>.Create(
//            (args) =>
//            {
//                var resultDCManagerTypes = new Dictionary<string, Type>(); // 
//                var existedManagerTypes = TypeCache.GetCustomDomainTypesByBaseType<DCManager>(true ,  excludeTypes: typeof(DCManager<>));
//                if (existedManagerTypes.Count > 0)
//                {
//                    foreach (var dcManagerType in existedManagerTypes)
//                    {
//                        resultDCManagerTypes.Add(dcManagerType.FullName, dcManagerType);
//                    }
//                }
//                //collect Managers - from IOC Container with some Route Configuration to Collect them
//                return resultDCManagerTypes;
//            }
//            , null
//            , null
//           );



/// <summary>
/// 
/// </summary>
//public static void RegisterWcfCommunicationRoutesForManagers()
//{
//    foreach (var mngr in Managers.Value.Values.ToList<DCManager>())
//    {
//        //if (mngr.CommunicationType.HasFlag(DCManagerCommunicationEn.Wcf_Communication))
//        //{
//        //    var managerWcfServiceType = typeof(DCService); //  GetCustomAttributesData().Add( );// CustomAttributes.

//        //    //TypeBuilder tb =  TypeBuilder. ();
//        //    //tb.BaseType = typeof(DCService);
//        //    //tb.CustomAttributes = new 
//        //    RouteTable.Routes.Add( new ServiceRoute(Key, new WebServiceHostFactory(), typeof(DCService)));

//        //}
//    }
//}






//protected internal virtual async Task<DCMessage3> ExecuteCommandOnClient(
//                                                          string scenarioKey,
//                                                          string currentCommandName
//                                                        , DCProgressReporter reporter
//                                                        , params BinParameter[] actionParameters)
//{

//    if (!CommunicationUnits.ContainsKey(scenarioKey))
//        throw new InvalidOperationException("Current Manager  doesn't contain Client with pointed ScenarioKey");


//    var targetCommunicationUnit = CommunicationUnits[scenarioKey];

//    var commandMessage = DCMessage3.Create(ShortKey, currentCommandName, actionParameters);

//    // indication - Starting if network enable start
//    //NotifyStartCommandProcessing(commandMessage, reporter);

//    //SetDiagnosticsOnClientSendTimeDbg(commandMessage);

//    //PACK INTERNAL SERIALIZATION
//    //DCMessage3.PackMessage(ref commandMessage, targetCommunicationUnit);
//    commandMessage.DeserializeParameters(targetCommunicationUnit);

//    var targetDCClient = DCClients[targetCommunicationUnit.RegistrationInfo.ScenarioKey];
//    return await targetDCClient.ExecuteCommand(commandMessage, reporter, NotifyNetworkDoesntWorkAndStopCommandProcessing)
//          .ContinueWith(
//                    (taskResultMessage) =>
//                    {
//                        if (taskResultMessage.Status == TaskStatus.RanToCompletion)
//                        {
//                            // analising DCMessage received from server Result Message 
//                            var serverMessage = taskResultMessage.Result;

//                            //UNPACK INTERNAL SERIALIZATION
//                            DCMessage3.UnpackMessage(ref commandMessage, targetCommunicationUnit);

//                            commandMessage = serverMessage;

//                            SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(commandMessage);

//                            //if HasServerErrorHappened - notify
//                            NotifyAboutCommandFailureResultProgress(commandMessage, reporter); //

//                            // successfully completed on server - notify
//                            NotifyAboutCommandSuccessResultProgress(commandMessage, reporter);


//                            //continue TriggerFaultAferAction - Will be Called if it exist
//                            //if (insideTSout.Item3 != null)
//                            //{ insideTSout.Item3(t); }

//                        }
//                        else if (taskResultMessage.Status == TaskStatus.Faulted)
//                        {
//                            commandMessage.ProcessingFaultMessage = "Communication not sended to server";
//                            return commandMessage;
//                        }
//                        return commandMessage;
//                    }
//         );

//}






//protected virtual List<Type> LoadKnownTypes()
//{
//    return new List<Type>();  // empty list of Types
//}

//protected virtual void InitializeTSSerilizerWithKnownTypes()
//{            

//    LA_Serializer = LazySlim<ITypeSetSerializer>.Create(
//    (args) =>
//    {
//        var key = args[0] as string;
//        var targetSrlzr = TypeSetSerializer.AddOrUseExistTypeSetSerializer(key);
//        targetSrlzr.AddKnownTypesTPL(typeof(CommandMessage), typeof(DCMessage));
//        //

//        return targetSrlzr;
//    }
//    , null
//    , new object[] { Key });

//}

#endregion -------------------------------- GARBAGE ------------------------------