﻿using System;
using System.Net.Http;
 
using DDDD.Core.Extensions;


#if !WP81
using ServiceStack.Text;
using DDDD.Core.Serialization;
#endif


namespace DDDD.Core.ComponentModel.Messaging
{


    /// <summary>
    /// BinParameter structure  that used in DCMessage2. It used to utilize work with data formatted as Text. 
    /// </summary>
    public class DCParameter : IDCParameter
    {
        #region --------------------------------- PROPERTIES --------------------------------
        
        /// <summary>
        /// Type Full Name. 
        /// </summary>
        public string CSharpTypeFullName { get; private set; }


        /// <summary>
        /// IS OUT Parameter? This also means that by default every parameter is IN PARAMETERS.
        /// </summary>
        public bool IsOut { get; private set; }


        /// <summary>
        /// Parameter key to get it from Parameters array
        /// </summary>
        public string Key { get; private set; }


        /// <summary>
        /// Some Binary(byte[]) data value - serialized  by TSS  from  parameter original value. 
        /// </summary>
        public byte[] ValueBin { get; private set; }


        /// <summary>
        /// Deserialized value.  
        /// </summary>
        public object Value { get; internal set; }

        #endregion --------------------------------- PROPERTIES --------------------------------
         

        #region ---------------------------- NEW PARAMETER  -------------------------------------

        /// <summary>
        /// Create new IN BinParameter instance.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="paramValue"></param>        
        /// <returns></returns>
        public static DCParameter NewInParam(string key, object paramValue)
        {
            //check that parameter still was not created.

            var newBinParameter = new DCParameter();
            newBinParameter.Key = key;
            newBinParameter.Value = paramValue;

            newBinParameter.IsOut = false;

            if (paramValue.IsNotNull())
            {
                var valueType = paramValue.GetType();
                newBinParameter.CSharpTypeFullName = valueType.FullName + ',' + valueType.Assembly.GetAssemblyNameEx();
            }

            return newBinParameter;
        }



        /// <summary>
        /// Create new OUT BinParameter instance. 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public static DCParameter NewOutParam(string key, object paramValue)
        {
            //check that parameter still was not created.

            var newBinParameter = new DCParameter();
            newBinParameter.Key = key;
            newBinParameter.Value = paramValue;

            newBinParameter.IsOut = true;

            if (paramValue.IsNotNull())
            {
                var valueType = paramValue.GetType();
                newBinParameter.CSharpTypeFullName = valueType.FullName + ',' + valueType.Assembly.GetAssemblyNameEx();
            }

            return newBinParameter;
        }



        #endregion ---------------------------- NEW PARAMETER  -------------------------------------


        #region ------------------------------- SERIALIZE /DESERIALIZE PARAMETER ----------------------------


        /// <summary>
        /// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
        public void Deserialize(DCCUnit communicationUnit)
        {
            if (CSharpTypeFullName.IsNullOrEmpty())
                throw new InvalidOperationException("Target Type name [BinParameter.CSharpTypeFullName] cannot be null or empty - BinParameter Key[{0}] ".Fmt(Key));

            var targetType = Type.GetType(CSharpTypeFullName);
            if (targetType.IsNull())
                throw new InvalidOperationException("Target Type was not found by Name string [{0}], BinParameter Key[{1}] ".Fmt(CSharpTypeFullName, Key));

            Value = communicationUnit.DeserializeFromByteArrayHandler(ValueBin, targetType);
            ValueBin = null;
        }


        /// <summary>
        /// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communication unit with prepered serialization handler? based on it's SerializationMode</param>
        public void Serialize(DCCUnit communicationUnit)
        {
            if (Value.IsNull()) return;

            var valueType = Value.GetType();
            CSharpTypeFullName = valueType.FullName + ',' + valueType.Assembly.GetAssemblyNameEx();

            ValueBin = communicationUnit.SerializeToByteArrayHandler(Value);
            Value = null; //nullify serialized data
        }



        /// <summary>
        /// Get CSharpTypeName string builder.
        /// </summary>
        /// <param name="targetValueType"></param>
        /// <returns></returns>
        public static string GetCSharpTypeName(Type targetValueType)
        {
            var targetTypeName = targetValueType.FullName;

            var targetAssemblyName = targetValueType.Assembly.GetAssemblyNameEx();

            return targetTypeName + "," + targetAssemblyName;
        }

    }


    #endregion ------------------------------- SERIALIZE /DESERIALIZE PARAMETER ----------------------------

}
