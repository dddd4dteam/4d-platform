﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using System.Xml.Serialization;
using System.Net.Http;

#if !WP81
using ServiceStack.Text;
#endif

namespace DDDD.Core.ComponentModel.Messaging
{




    /// <summary>
    /// Dynamic Command Message3 - used with DCCommunications based on binary data serializations , such as TSS.
    /// </summary>     
    public class DCMessage3 : IDCMessage
    {

        #region ------------------------------ CTOR ---------------------------------
        public DCMessage3()
        {

        }
        #endregion ------------------------------ CTOR ---------------------------------



        /// <summary>
        /// Empty Value of DCMessage3
        /// </summary>
        public static DCMessage3 Empty
        {
            get { return default(DCMessage3); }
        }
        

#region ----------------------TARGET COMMAND RECEIVER-COMMAND MANAGER --------------------------

        /// <summary>
        /// Targeting BL or infrastructure operations( like CRUDManager) responsible Command Manager's [Key-TargetCommandManager]. 
        /// Where [ TargetCommandManager = [Type.FullName] = Key ].
        /// </summary>      
        public string TargetDCManager { get; set; }

        

        /// <summary>
        ///  Command or Method of  TargetDCManager to call it
        /// </summary>       
        public string Command { get; set; }


        /// <summary>
        /// Flexible TSS can serialize complex parameter classes/structs and then we box them into the Dictionary, each by  its Key 
        /// </summary>
        public List<DCParameter> Parameters { get; set; }


        /// <summary>
        /// Get Parameters from Parameters by Key. Result can be null if not exists.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public IDCParameter GetParam(string key)
        {
            return Parameters.FirstOrDefault(prm => prm.Key == key);
        }

        /// <summary>
        /// Add  In Parameter
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="communicationUnit"></param>
        public void CreateInParam(string key, object value )
        {
            Parameters.Add(DCParameter.NewInParam(key, value));
        }


        /// <summary>
        /// Add  Out Parameter
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="communicationUnit"></param>
        public void CreateOutParam(string key, object value )
        {
            Parameters.Add(DCParameter.NewOutParam(key, value));
        }



        #endregion ----------------------TARGETCOMMAND RECEIVER-COMMAND MANAGER --------------------------



        #region -------------------------------- TIME DIAGNOSTICS -------------------------------

        /// <summary>
        /// Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientSendTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        public DateTime ServerReceivedFromClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server sending processed command to client( server time )
        /// </summary>
        public DateTime ServerSendToClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the client received processed command from server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientRecievedResultFromServerTime
        {
            get;
            private set;
        }
        
#endregion --------------------------------TIME DIAGNOSTICS -------------------------------


        #region -------------------------------- OPERATION PROGRESS --------------------------------

        /// <summary>
        /// Operation Progress in Percents
        /// </summary>
        public int OperationProgressPercent
        {
            get;
            private set;
        }


        /// <summary>
        /// Operation Progress State -string value
        /// </summary>
        public string OperationProgressState
        {
            get;
            private set;
        }

        /// <summary>
        /// If we use Operation Progress info properties - OperationProgressPercent and OperationProgressState
        /// </summary>
        public bool UseOperationProgress
        {   get
            {
                return (OperationProgressPercent >= 0 || OperationProgressState.IsNotNullOrEmpty()); 
            }
        }
        #endregion -------------------------------- OPERATION PROGRESS --------------------------------


        #region -------------------------- RESULT  ----------------------------

        const string ResultKey = "Result";


        /// <summary>
        /// Get Result Parameter. We trying to get Parameter with the [Result] key. 
        /// Return value can be null if parameter with such Key doesn't exist.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public IDCParameter GetResult()
        {
            return Parameters.FirstOrDefault(prm => prm.Key == ResultKey);

        }

        /// <summary>
        /// Set result value 
        /// </summary>
        /// <param name="resultValue"></param>
        public void SetResult(object resultValue)
        {
            var resultParam = Parameters.FirstOrDefault(prm => prm.Key == ResultKey);
            if (resultParam != null)
            {
                resultParam.Value = resultValue;
            }
            else
            {
                Parameters.Add(DCParameter.NewOutParam(ResultKey, resultValue));
            }

        }
        #endregion -------------------------- RESULT  ---------------------------- 


        #region ------------------------- ERROR, PROGRESS STATES-------------------------------


        /// <summary>
        /// Message on successful result (if you wish)
        /// </summary>
        public string ProcessingSuccessMessage { get; set; }


        /// <summary>
        /// Message on operation Fault
        /// </summary>
        public string ProcessingFaultMessage { get; set; }


        /// <summary>
        /// Internal System Error Code  
        /// </summary>
        public short ErrorCode { get; set; }



        /// <summary>
        /// Server( Command Controller)  should set this message if error happened  before send CommandMessage back.
        /// After such CommandMessage will be received by client we'll show full error message only in [Debug] Conditional  mode.
        /// In release mode client will see only common message - like [SERVER ERROR HAPPENED] 
        /// </summary>
        public bool HasServerErrorHappened
        {
            get
            {
                return !ProcessingFaultMessage.IsNullOrEmpty();
            }
        }

        /// <summary>
        /// By default Successfull Message is empty string, that means that Command is succsessful  - until server won't set an ErrorMessage.
        /// So if we have Empty String or valid Succesfull message -  command will be means as successful.  
        /// </summary>
        public bool HasOperationSuccessfullyCompleted
        {
            get
            {
                return !ProcessingSuccessMessage.IsNullOrEmpty();
            }
        }


        #endregion ------------------------- ERROR, PROGRESS STATES-------------------------------



        /// <summary>
        /// Create new  DCMessage3.  
        /// </summary>
        /// <param name="targetCommandManager"></param>
        /// <param name="command"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static DCMessage3 Create(string targetCommandManager, string command, params IDCParameter[] commandParametersBin)
        {
            return new DCMessage3()
            {
                TargetDCManager = targetCommandManager
                ,
                Command = command
                ,
                Parameters = commandParametersBin.ToList<DCParameter>()
            };
        }
        
        

       

        /// <summary>
        ///Set  ServerReceivedFromClientTime when - Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        /// <param name="serverReceivedFromClientTime"></param>
        public void SetServerReceivedFromClientTime(DateTime serverReceivedFromClientTime)
        {
            ServerReceivedFromClientTime = serverReceivedFromClientTime;
        }

        /// <summary>
        /// Set  ServerSendToClientTime when - Time point - when the server sending processed command to client( server time )
        /// </summary>
        /// <param name="serverSendToClientTime"></param>
        public void SetServerSendToClientTime(DateTime serverSendToClientTime)
        {
            ServerSendToClientTime = serverSendToClientTime;
        }


        /// <summary>
        /// Set  ClientSendTime when - Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        /// <param name="clientSendTime"></param>
        public void SetClientSendTime(DateTime clientSendTime)
        {
            ClientSendTime = clientSendTime;
        }

        /// <summary>
        /// Set  ClientRecievedResultFromServerTime when - Time point - when the client received processed command from server( client time synchronized with server ) 
        /// </summary>
        /// <param name="clientRecievedResultFromServerTime"></param>
        public void SetClientRecievedResultFromServerTime(DateTime clientRecievedResultFromServerTime)
        {
            ClientRecievedResultFromServerTime = clientRecievedResultFromServerTime;
        }



        /// <summary>
        /// Deserialize Parameters 
        /// </summary>
        /// <param name="communicationUnit"></param>
        public void DeserializeParameters(DCCUnit communicationUnit)
        {
            for (int i = 0; i < Parameters.Count; i++)
            {
                Parameters[i].Deserialize(communicationUnit);
            }
        }



        /// <summary>
        /// Serialize Parameters
        /// </summary>
        /// <param name="serializationMode"></param>
        public void SerializeParameters(DCCUnit communicationUnit)
        {
            for (int i = 0; i < Parameters.Count; i++)
            {
                Parameters[i].Serialize(communicationUnit);
            }

        }



        /// <summary>
        /// Clear all input parameters
        /// </summary>
        public void ClearInputParameters()
        {
            //clear all input parameters            
            var outParameters = new List<DCParameter>();
            
            for (int i = 0; i < Parameters.Count; i++)
            {
                if (Parameters[i].IsOut ) outParameters.Add(Parameters[i]);
            }

            Parameters = outParameters;

        }
         


    }

     
}
