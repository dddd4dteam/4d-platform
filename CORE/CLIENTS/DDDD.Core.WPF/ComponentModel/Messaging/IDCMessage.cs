﻿using System;
using System.Collections.Generic;

namespace DDDD.Core.ComponentModel.Messaging
{

    /// <summary>
    /// DCMessage - Dynamic Command message.
    /// </summary>
    public interface IDCMessage
    {



        //int TargetDCManagerID { get; set; }
        string TargetDCManager { get; set; }
        string Command { get; set; }



        short ErrorCode { get; set; }
        bool HasOperationSuccessfullyCompleted { get; }
        bool HasServerErrorHappened { get; }
        int OperationProgressPercent { get; }
        string OperationProgressState { get; }
        string ProcessingFaultMessage { get; set; }
        string ProcessingSuccessMessage { get; set; }


        DateTime ClientRecievedResultFromServerTime { get; }
        DateTime ServerReceivedFromClientTime { get; }
        DateTime ServerSendToClientTime { get; }
        DateTime ClientSendTime { get; }

        void SetClientRecievedResultFromServerTime(DateTime clientRecievedResultFromServerTime);
        void SetClientSendTime(DateTime clientSendTime);
        void SetResult(object resultValue);
        void SetServerReceivedFromClientTime(DateTime serverReceivedFromClientTime);
        void SetServerSendToClientTime(DateTime serverSendToClientTime);


        bool UseOperationProgress { get; }


        void NewInParam(string key, object value);
        void NewOutParam(string key, object value);

      

        DCParameter GetParam(string key);
        List<DCParameter> GetAllParams();
        DCParameter GetResult();
        void ClearInputParameters();


       
    }
}