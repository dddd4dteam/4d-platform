﻿namespace DDDD.Core.ComponentModel
{




    /// <summary>
    /// Loading Mode - how we'll load assemblies into
    /// </summary>
    public enum AssembliesLoadModeEn
    {
        /// <summary>
        /// In such way programmer needs to add all needable assembly into ComponentsContainer manually by ComponentsContainer's API.
        /// </summary>
        ManualAssembliesLoading

        /// <summary>
        /// Loading assemblies into ComponentsContainer automatically - it'll describe to event [TypeCache.DomainTypesAdded], -so it'll load each of the filtered domain assembly that TypeCache will load in it.
        /// </summary>
       , AutoLoadingDomainAssemblies


    }





    /// <summary>
    ///  We have some Application Model, that consist of some Component Classes - Known  Components Classes - their enumeration.  
    /// </summary>
    public enum ComponentClassEn
    {

        /// <summary>
        /// Not Pointed some Component Class
        /// </summary>
        NotDefined
        ,
        /// <summary>
        /// DC Managers - main BL manager unit: can dynamically reserve DCCUnit- Service- on server side/Client - on Client side.  Components of this Class of this Class based on class [DCManager].
        /// </summary>
        DCManagers


        ,
        /// <summary>
        /// Web Api Controllers - based on ApiController classes
        /// </summary>
        WebApiControllers
        
        

#if SERVER

        ,
        /// <summary>
        /// Component Class - DCCommunicationUnitService Factories. Components of this Class implements interface  [IDCCommunicationUnitServiceFactory].
        /// </summary>
        DCCUnitServerFactories

#endif

#if CLIENT
        ,
        /// <summary>
        /// Component Class - UserControls.  Components of this Class based on class [System.Windows.Controls.UserControl].
        /// </summary>
        UserControls
        

        ,
        /// <summary>
        /// Component Class - Pages.  Components of this Class based on class [System.Windows.Controls.Page].
        /// </summary>
        Pages

        ,
        /// <summary>
        /// Component Class - Windows. Components of this Class based on class [System.Windows.Controls.Window].
        /// </summary>
        Windows

        ,
        /// <summary>
        /// Component Class - DCCommunicationUnitClient Factories. Components of this Class implements interface  [IDCCommunicationUnitClientFactory].
        /// </summary>
        DCCUnitClientFactories
#endif



    }


}
