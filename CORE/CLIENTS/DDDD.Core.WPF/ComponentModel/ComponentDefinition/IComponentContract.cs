﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Component Definition's  Contract.
    /// </summary>
    public interface IComponentContract
    {

        #region ---------------------- PROPERTIES -----------------------

        Type ContractType { get; }

        bool IsInterface { get; }

        bool IsBaseClass { get; }


        /// <summary>
        /// Type [someType] meets criterion - when it IS NOT (someType.IsEnum || someType.IsAbstract || someType.IsInterface),
        /// AND also (  (someType  IsAssignable(ContractType))-when ContractType is base class Type , 
        ///     OR (someType implements interface(ContractType) - when ContractType is interface Type )
        /// </summary>
        /// <param name="somType"></param>
        /// <returns></returns>
        bool IsTypeMatchesWithContract(Type somType);

        #endregion ---------------------- PROPERTIES -----------------------

    }


  
}
