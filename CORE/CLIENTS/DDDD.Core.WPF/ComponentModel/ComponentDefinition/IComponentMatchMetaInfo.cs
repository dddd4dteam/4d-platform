﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DDDD.Core.Extensions;


namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Component Match is the end Type that match with some [Component Definition.Contract].
    /// <para/> Notes: Custom ComponentTypeMetaAttribute - base class for abstract Component Match Metadata, that also belong  to some Component Conceptual class.
    /// <para/> With such attribute class we can create inherited class- detailed metadata about  Type of ComponentItem. For example metadata class for IDCCUnitClientFactory implementation component item- DCCUnitClientFactory , or some WebApiController.    
    /// </summary>
    public interface IComponentMatchMetaInfo
    {

        /// <summary>
        /// Component Definition Key. 
        /// </summary>
        string ComponentDefinitionKey { get; }

    }
     


}
