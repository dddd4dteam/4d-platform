﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;


namespace DDDD.Core.ComponentModel
{


    /// <summary>
    /// Components Definition - declaring new Component by its ComponentContract 
    /// </summary>
    public class ComponentDefinition
    {


        #region ----------------------------------- CTOR -------------------------------

         ComponentDefinition(string key, IComponentContract contract, Action<List<ComponentMatch>> initComponentClassAction = null)
        {
            Key = key;
            Contract = contract;
            InitComponentClassAction = initComponentClassAction;
        }



        #endregion ----------------------------------- CTOR -------------------------------

      
        #region ------------------------------- CONST ---------------------------------

        const string Class = nameof(ComponentDefinition);

        #endregion ------------------------------- CONST ---------------------------------


        #region ---------------------------------- FIELDS --------------------------------

        /// <summary>
        /// Global Locker instance
        /// </summary>
        static readonly object locker = new object();

        #endregion ---------------------------------- FIELDS --------------------------------
        

        #region ------------------------------ PROPERTIES ----------------------------------


        /// <summary>
        /// Identification string Key for current ComponentClass
        /// </summary>
        public string Key
        { get; private set; }


        /// <summary>
        /// Component Class's Contract - BaseType or Interface
        /// </summary>
        public IComponentContract Contract
        { get; private set; }

           


        /// <summary>
        /// Component belongs to current ComponentClass
        /// </summary>
        public List<ComponentMatch> Matches
        { get; private set; } = new List<ComponentMatch>();



        /// <summary>
        /// Global Init action for current ComponentClass.
        /// It'll be called after all ComponentClasses will collect their components, next step will be initing(this action) call cicle for each of the ComponentClass 
        /// -these steps will be runned inside ComponetnContainer.Build() method.
        /// </summary>
        public Action<List<ComponentMatch>> InitComponentClassAction
        { get; private set; }


        #endregion ------------------------------ PROPERTIES ----------------------------------


        #region ---------------------------- CREATION -------------------------------------

        /// <summary>
        /// Create new ComponentClass 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="criterionType"></param>
        /// <param name="initComponentClassAction"></param>
        /// <returns></returns>
        public static ComponentDefinition New(string key, Type criterionType, Action<List<ComponentMatch>> initComponentClassAction = null)
        {
            return new ComponentDefinition(key, ComponentContract.New(criterionType), initComponentClassAction);
        }

        #endregion ---------------------------- CREATION -------------------------------------


        #region --------------------------- INITIALIZATION --------------------------------

        /// <summary>
        /// Rise init-prepare procedure  of this ComponentClass if InitAction IS NOT NULL.
        /// </summary>
        public void RiseInitIfActionExist()
        {
            if (InitComponentClassAction.IsNull() || Matches.Count == 0) return;
            InitComponentClassAction(Matches);
        }

        #endregion --------------------------- INITIALIZATION --------------------------------


        #region --------------------- ADDING COMPONENT ------------------------------


        /// <summary>
        /// Add component. If component really Not existed add new ComponentItemInfo to Components and ComponentByModule collections of current ComponentClass.
        /// </summary>
        /// <param name="componentType"></param>
        protected internal ComponentMatch AddComponentIfNotExist(Type componentType,bool useScannedAssemblyCheckAndAdd = false)
        {
            // 1 Add newcomponent to ComponentClass's inner [Components] collection 
            // 2 Add newcomponent to ComponentClass's inner [ComponentsByModules] collection 
            // 3 ComponentContainer - Global collection of ComponentsByAssemblies

            ComponentsContainer.InnerLocker.LockAction( ()=> NotContainsComponentWithID(componentType.FullName.GetHashCode() )
                , () =>
                {
                    ComponentMatch newComponent =  ComponentMatch.New(componentType, this);
                                     
                    //1 Add newcomponent to ComponentClass's inner [Components] collection 
                    Matches.Add(newComponent);
                    
                    // when we adding some set of Types from one Assembly then we needn't to Check each time, if theirs Assembly was scanned - it will already be done previosly in upper method
                    if (useScannedAssemblyCheckAndAdd == true)
                        ComponentsContainer.CheckAssemblyNotScannedThenAddSlot(newComponent.ComponentType.Assembly);
                    
                    // 3 ComponentContainer - Global collection of ComponentsByAssemblies
                    // we can
                    var assemblyComponentKey = Key + "," + newComponent.TypeKey;                     
                    ComponentsContainer.Current.ComponentsByAssemblies[newComponent.AssemblyName].Add(assemblyComponentKey, newComponent); //                    
                    
                }
                );

            return GetComponentWithID(componentType.FullName.GetHashCode());
            
        }


        #endregion --------------------- ADDING COMPONENT ------------------------------



        #region ------------------------------- CONTAINS COMPONENT ---------------------------------


        /// <summary>
        /// Check if Component Class contains Component with TypeKey. Here Component.TypeKey value should be compared with some Type.FullName value
        /// </summary>
        /// <param name="componentKey"></param>
        /// <returns></returns>
        public bool ContainsComponentWithKey(string componentKey)
        {
            return Matches.ContainsItemWhere(cmp => cmp.TypeKey == componentKey);
        }

        /// <summary>
        /// Check if Component Class doesn't contains Component with Key. Here Component.Key value should be compared with some Type.FullName value.
        /// </summary>
        /// <param name="componentKey"></param>
        /// <returns></returns>
        public bool NotContainsComponentWithKey(string componentKey)
        {
            return !ContainsComponentWithKey(componentKey);
        }


        /// <summary>
        /// Check if Component Class contains Component with ShortKey. Here Component.ShortKey value should be compared with some Type.Name value.
        /// </summary>
        /// <param name="shortKey"></param>
        /// <returns></returns>
        public bool ContainsComponentWithShortKey(string shortKey)
        {
            return Matches.ContainsItemWhere(cmp => cmp.ShortKey == shortKey );
        }


        /// <summary>
        /// Check if Component Class doesn't contains Component with ShortKey. Here Component.ShortKey value should be compared with some Type.Name value.
        /// </summary>
        /// <param name="shortKey"></param>
        /// <returns></returns>
        public bool NotContainsComponentWithShortKey(string shortKey)
        {
            return !ContainsComponentWithShortKey(shortKey);
        }


        /// <summary>
        /// Check if Component Class contains Component with ID. Here Component.ID value should be compared with some Type.FullName.GetHashCode()  value 
        /// </summary>
        /// <param name="id_hash"></param>
        /// <returns></returns>
        public bool ContainsComponentWithID(int id_hash)
        {
            return Matches.ContainsItemWhere(cmp => cmp.ID_Hash == id_hash);
        }


        /// <summary>
        /// Check if Component Class doesn't contains Component with ID. Here Component.ID value should be compared with some Type.FullName.GetHashCode()  value 
        /// </summary>
        /// <param name="id_hash"></param>
        /// <returns></returns>
        public bool NotContainsComponentWithID(int id_hash)
        {
            return !ContainsComponentWithID(id_hash);
        }

        #endregion ------------------------------- CONTAINS COMPONENT ---------------------------------


        #region ----------------------------  GET COMPONENT by  [componentKey], [id_hash] -------------------------------


        /// <summary>
        /// Get Component with TypeKey. If Component with such TypeKey won't be found InvalidOperationException will be thrown.
        /// </summary>
        /// <param name="componentKey"></param>
        /// <returns></returns>
        public ComponentMatch GetComponentWithKey(string componentKey)
        {
            if (NotContainsComponentWithKey(componentKey))
                throw new InvalidOperationException($"ERROR  in [{nameof(GetComponentWithKey)}()]: Inside ComponentClass -[{Key}], Component With Key-[{componentKey}] doesn't exist or was not found.");

            return Matches.FirstOrDefault(cmp => cmp.TypeKey == componentKey);
        }


        /// <summary>
        /// Get Component with ShortKey. If Component with such ShortKey won't be found InvalidOperationException will be thrown.  
        /// </summary>
        /// <param name="shortKey"></param>
        /// <returns></returns>
        public ComponentMatch GetComponentWithShortKey(string shortKey)
        {
            if (NotContainsComponentWithShortKey(shortKey))
                throw new InvalidOperationException($"ERROR in [{nameof(GetComponentWithShortKey)}()]: Inside ComponentClass -[{Key}], Component With Short Key-[{shortKey}] doesn't exist or was not found.");

            return Matches.FirstOrDefault(cmp => cmp.ShortKey == shortKey );
        }



        /// <summary>
        /// Get Component with ID_hash . If Component with such id_hash won't be found InvalidOperationException will be thrown.
        /// </summary>
        /// <param name="id_hash"></param>
        /// <returns></returns>
        public ComponentMatch GetComponentWithID(int id_hash)
        {
            if (NotContainsComponentWithID(id_hash))
                throw new InvalidOperationException($"ERROR  in [{nameof(GetComponentWithID)}()]: Inside ComponentClass -[{Key}], Component With ID-[{id_hash}] doesn't exist or was not found.");

            return Matches.FirstOrDefault(cmp => cmp.ID_Hash == id_hash);
        }


        #endregion ----------------------------  GET COMPONENT by  [componentKey], [id_hash] -------------------------------


        // GetComponent
        // Get Component by [ComponentItemInfo selector Func]
        // Get Component by [TComponentMetaInfo selector Func]
        // Get Component by [Type selector Func]
        #region -------------- GET CLASS COMPONENT by [ComponentItemInfo selector Func],[TComponentMetaInfo selector Func], [Type selector Func] ----------


        /// <summary>
        /// Get first Component of this ComponentClass by [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        public ComponentMatch GetComponent(Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (Matches.Count == 0 || selector.IsNull()) return null;

            return Matches.FirstOrDefault(cmp => selector(cmp));
        }


        /// <summary>
        /// Get first Component of this ComponentClass by [TComponentMetaInfo selector Func]
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="selector"></param>
        /// <returns></returns>
        public ComponentMatch GetComponent<TComponentMetaInfo>(Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : Attribute, IComponentMatchMetaInfo
        {
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (Matches.Count == 0 || selector.IsNull()) return null;

            return Matches.FirstOrDefault(cmp => selector(cmp.MetaInfo as TComponentMetaInfo));
        }



        /// <summary>
        /// Get first Component of this ComponentClass by [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        public ComponentMatch GetComponent(Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (Matches.Count == 0 || selector.IsNull()) return null;

            return Matches.FirstOrDefault(cmp => selector(cmp.ComponentType));
        }

        #endregion-------------- GET CLASS COMPONENT by [ComponentItemInfo selector Func],[TComponentMetaInfo selector Func], [Type selector Func] ----------



       

       

        

    }




}
