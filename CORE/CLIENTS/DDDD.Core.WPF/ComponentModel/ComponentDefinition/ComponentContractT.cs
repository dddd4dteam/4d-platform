﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    ///  Contract by which we are going to group assemblyTypes into [ComponentDefinitions.Matches].
    /// </summary>
    public class ComponentContract : IComponentContract
    {


        #region ------------------------------- CTOR -------------------------------------

        protected ComponentContract(Type contractType)
        {
            if (contractType.IsEnum || contractType.IsValueType)
                throw new InvalidOperationException("Contract Type can only be Class or Interface. ");

            ContractType = contractType;
            IsBaseClass = ContractType.IsClass;
            IsInterface = ContractType.IsInterface;
        }

        #endregion ------------------------------- CTOR -------------------------------------
        

        #region ---------------------- PROPERTIES -----------------------

        public Type ContractType { get; private set; }// = typeof(T);
         
        public bool IsBaseClass { get; private set; }

        public bool IsInterface { get; private set; }

        #endregion ---------------------- PROPERTIES -----------------------





        static bool IsTypeMeetsContractInternal(Type someType, ComponentContract contract)
        {
            if (someType.IsEnum || someType.IsAbstract || someType.IsInterface) return false;

            if (contract.IsBaseClass)
            {
                return contract.ContractType.IsAssignableFrom(someType);
            }
            else //(contract.IsInterface)
            {
                return someType.GetInterfaces().Contains(contract.ContractType);
            }
        }




        /// <summary>
        /// Type [someType] meets contract - when it IS NOT (someType.IsEnum || someType.IsAbstract || someType.IsInterface),
        /// AND also (  (someType  IsAssignable(ContractType))-when ContractType is base class Type , 
        ///     OR (someType implements interface(ContractType) - when ContractType is interface Type )
        /// </summary>
        /// <param name="someType"></param>
        /// <returns></returns>
        public bool IsTypeMatchesWithContract(Type someType)
        {
            return IsTypeMeetsContractInternal(someType, this);            
        }


        /// <summary>
        /// Create new Contract with {T} Type. 
        /// <para/> Each [get;] operation of this property returns new instance of ComponentContract{T}. 
        /// </summary>
        /// <returns></returns>
        public static ComponentContract New(Type contractType)
        { return new ComponentContract(contractType); }
 
    }

 
}
