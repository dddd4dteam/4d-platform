﻿//#if SERVER 


using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Navigation;
using DDDD.Core.Diagnostics;
using DDDD.Core.Threading;


#if SERVER && IIS
using System.Web.Http.Dependencies;
using System.Web.Http.Dispatcher;
#endif


namespace DDDD.Core.ComponentModel
{




    /// <summary>
    /// Application's Components Container. IOC container.
    /// <para/> This Container groups some assembly Types into - ComponentClasses with theirs Components. 
    /// <para/> Each Component associated as [1..1] with its ComponentType.
    /// <para/> ComponentClass contains Components those [component.ComponentType] sutisfied with its [Contract Contract - CriterionType] .
    /// <para/> Container grouping Component as - 
    ///             1 Component that belongs to ComponentsClass
    ///             2 Component that belongs to ComponentsClass and exist in some assembly
    /// <para/> This container doesn't bore of the storing of reference of non singleton component instancies , and doesn't manage by LifiTime Scopes of such-non static instancies.
    /// <para/> Container can be loaded in two Modes- see AssembliesLoadModeEn 
    /// </summary>
    public class ComponentsContainer : IDisposable
#if SERVER && IIS
        ,IDependencyScope, IDependencyResolver, IAssembliesResolver 
#endif

    {


        #region ------------------- CTOR, Singleton Property  -----------------------


        protected ComponentsContainer()
        {             
            Initialize();
        }


        static LazyAct<ComponentsContainer> LA_CurrentIOCContainer =
               LazyAct<ComponentsContainer>.Create(
                   (args) => { return new ComponentsContainer(); }
                   , null//locker
                   , null //args
                   );

        /// <summary>
        /// Instance of AppBootstrapper  
        /// </summary>
        public static ComponentsContainer Current
        {
            get
            {
                return LA_CurrentIOCContainer.Value;
            }
        }

        

        #endregion ------------ CTOR, Singleton Property  -----------------------


        #region --------------------- INITIALIZATION ------------------------
        
        protected virtual void Initialize()
        {
            if (AssembliesLoadMode == AssembliesLoadModeEn.AutoLoadingDomainAssemblies)
            {
                TypeCache.DomainTypesAdded += LoadComponents;                
            }
            // else when we use Manual mode we still need to add this core assembly
            
        }
        
        #endregion --------------------- INITIALIZATION ------------------------


        #region  -------------------------- CONST -----------------------------

        public const BindingFlags DefaultCtorSearchBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        protected const string Class = nameof(ComponentsContainer);

        #endregion  -------------------------- CONST -----------------------------


        #region  -------------------------- FIELDS -----------------------------

        static readonly object locker = new object();

        internal static object InnerLocker
        {  get { return locker; }   }

        #endregion  -------------------------- FIELDS -----------------------------




        #region -------------------------- PROPERTIES -------------------------------

        /// <summary>
        /// Registered Components Classes - each ComponentsClass has its Contract and Components.
        /// Each ComponentClass also groping its components by assemblies inside it.
        /// </summary>
        protected internal Dictionary<string, ComponentDefinition> ComponentClasses
        { get; } = new Dictionary<string, ComponentDefinition>();


        

        /// <summary>
        /// Just the same components grouped by Assemblies that each belongs to.
        /// </summary>
        protected internal Dictionary<string, Dictionary<string, ComponentMatch>> ComponentsByAssemblies
        { get; } = new Dictionary<string, Dictionary<string, ComponentMatch>>();


        /// <summary>
        /// Loading Asssemblies into Container Mode: 
        ///     1 we can use autoloading mode- when we use TypeCache.AddAssemblyToDomain event
        ///     2 manual assemblies loading - we adding all the assemblies manually by ComponentsContainer API
        /// </summary>
        public static AssembliesLoadModeEn AssembliesLoadMode
        { get; set; }


        /// <summary>
        /// Excluding from components Types 
        /// </summary>
        public List<Type> ExcludingTypes
        { get; } = new List<Type>() { typeof(DCManager<>) };



        #endregion -------------------------- PROPERTIES -------------------------------


        #region ------------------------- REGISTERING COMPONENTS CLASS --------------------------------


        /// <summary>
        /// Register new Component Class with its Contract based on (TCriterion) Type  for its Components recognition
        /// </summary>
        /// <typeparam name="TCriterion"> TCriterion means your [IService] - as interface or [CustomService] - as base Type </typeparam>
        /// <param name="componentClass"></param>
        /// <param name="initComponentClass"></param>
        public static void RegisterComponentClass<TCriterion>(string componentClass, Action<List<ComponentMatch>> initComponentClass)
        {
            Validator.ATNullReferenceArgDbg(componentClass, Class, nameof(RegisterComponentClass), nameof(componentClass));
            Validator.ATNullReferenceArgDbg(initComponentClass, Class, nameof(RegisterComponentClass), nameof(initComponentClass));

            RegisterComponentClass(componentClass, typeof(TCriterion), initComponentClass);
        }

        /// <summary>
        /// ReScan - Scanning again each of AllPrevioslyScanned assemblies
        /// </summary>
        public static void ReScan()
        {
            var domainAssemblies = AppDomain.CurrentDomain.GetAssemblies();

            for (int i = 0; i < domainAssemblies.Length; i++)
            {
                
                var assemblyName = domainAssemblies[i].GetAssemblyNameEx();

                if (HasScannedAssembly(assemblyName))//was scanned below
                {
                    //rescan assembly 
                   Current.LoadComponents(domainAssemblies[i]);
                }
            }
             
        }



        /// <summary>
        ///  Register new Component Class with its componentClassKey and Contract for its Components recognition. 
        ///  Here we creating instance of  ComponentClassInfo and then adding it into ComponentsContainer.
        /// </summary>
        /// <param name="componentClassKey"></param>
        /// <param name="componentCriterion"></param>
        /// <param name="initComponentClass"></param>
        public static void RegisterComponentClass(string componentClassKey, Type componentCriterion, Action<List<ComponentMatch>> initComponentClass)
        {
            Validator.ATNullReferenceArgDbg(componentClassKey, Class, nameof(RegisterComponentClass), nameof(componentClassKey));
            Validator.ATNullReferenceArgDbg(componentCriterion, Class, nameof(RegisterComponentClass), nameof(componentCriterion));
            Validator.ATNullReferenceArgDbg(initComponentClass, Class, nameof(RegisterComponentClass), nameof(initComponentClass));

            locker.LockAction( ()=> Current.ComponentClasses.NotContainsKey(componentClassKey)
                , () =>
                {
                    Current.ComponentClasses.Add(componentClassKey
                        , ComponentDefinition.New(componentClassKey, componentCriterion, initComponentClass));
                }
                );
        }


        /// <summary>
        /// Register new Component Class. 
        /// Here we adding ready made instance of ComponentsClass and then adding it into ComponentsContainer.
        /// </summary>
        /// <param name="componentClass"></param>
        public static void RegisterComponentClass(ComponentDefinition componentClass)
        {
            Validator.ATNullReferenceArgDbg(componentClass, Class, nameof(RegisterComponentClass), nameof(componentClass));

            locker.LockAction(()=> Current.ComponentClasses.NotContainsKey(componentClass.Key)
                , () =>
                {
                    Current.ComponentClasses.Add(componentClass.Key, componentClass);
                });
        }


        /// <summary>
        /// Check that we already tried  to scan assembly Types. Assembly where components weren't really found, but still was tried to load earlier - is ScannedAssembly.
        /// IF ALREADY ADDED - returns TRUE, instead FALSE.
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static bool HasScannedAssembly(string assemblyName)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(HasScannedAssembly), nameof(assemblyName));

            return Current.ComponentsByAssemblies.ContainsKey(assemblyName);
        }

        /// <summary>
        /// HasNotScannedAssembly - returns result equals to !HasScannedAssembly(asm)
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static bool HasNotScannedAssembly(string assemblyName)
        {
            return !HasScannedAssembly(assemblyName);
        }


        /// <summary>
        /// Check that assembly was already scanned/ or wasn't - if it wasn't scanned then add slot with this assemblyName 
        /// </summary>
        /// <param name="assemblyName"></param>
        internal static void CheckAssemblyNotScannedThenAddSlot(Assembly someAssembly)
        {
            var assemblyName = someAssembly.GetAssemblyNameEx();
            if ( HasNotScannedAssembly(assemblyName) )
            {
                Current.ComponentsByAssemblies.Add(assemblyName, new Dictionary<string, ComponentMatch>());
            }

            //Attach assembly to TypeCache if it was not there 
#if CLIENT && (SL5 || WP81)
            if ( !TypeCache.ContainsAssembly(someAssembly) )
            {
                TypeCache.TryAttachAssemblyToDomain( someAssembly);
            }
#elif SERVER || WPF
            if (AssembliesLoadMode == AssembliesLoadModeEn.ManualAssembliesLoading && TypeCache.NotContainsAssembly(someAssembly))
            {
                TypeCache.TryAttachAssemblyToDomain(someAssembly);
            }
#endif

        }


        #endregion ---------------------------------- REGISTERING COMPONENTS CLASS ----------------------------------



            #region ----------------------------------- LOADING ASSEMBLIES TO CONTAINER --------------------------------

         /// loadComponents from - Assembly, IEnumerable{Type} ,automatically from TypeCache domain assemblies.




        /// <summary>
        /// Load Assembly Types into the Current ComponentsContainer by Registered ComponentClasses contracts.
        /// Here we use mode of [AssembliesLoadMode == AssembliesLoadModeEn.AutoLoadingDomainAssemblies] and loading Components from domain asssemblis of TypeCache.
        /// </summary>
        public  void LoadComponents()
        {
            if (AssembliesLoadMode == AssembliesLoadModeEn.AutoLoadingDomainAssemblies)
            {
                foreach (var registeredComponentClass in  ComponentClasses)
                {
                    if (registeredComponentClass.Value.Contract.IsBaseClass)
                    {
                        var sutisfiedComponentTypes = TypeCache.LoadCustomDomainTypesByBaseType(registeredComponentClass.Value.Contract.ContractType, true, Current.ExcludingTypes.ToArray());
                        sutisfiedComponentTypes.ForEach(cmpTp => registeredComponentClass.Value.AddComponentIfNotExist(cmpTp));
                    }
                    else if (registeredComponentClass.Value.Contract.IsInterface)
                    {
                        var sutisfiedComponentTypes = TypeCache.LoadCustomDomainTypesByInterface(registeredComponentClass.Value.Contract.ContractType);
                        sutisfiedComponentTypes.ForEach(cmpTp => registeredComponentClass.Value.AddComponentIfNotExist(cmpTp));
                    }
                }

            }

        }


        /// <summary>
        /// Load Assembly Types into the Current ComponentsContainer by Registered ComponentClasses contracts.
        /// </summary>
        /// <param name="containerAssembly"></param>
        /// <param name="useCustomDomainAssemblyCheck"></param>
        public virtual void LoadComponents(Assembly containerAssembly, bool useCustomDomainAssemblyCheck = true )
        {
            if (useCustomDomainAssemblyCheck && TypeCache.IsCustomDomainAssemblyCheck(containerAssembly) == false) return;

            var assemblyTypes = containerAssembly.GetTypes();
            if (assemblyTypes.IsNotWorkable()) return;
                        
            CheckAssemblyNotScannedThenAddSlot(containerAssembly);

            // assembly Types can be scanned twice or more times          
            for (int i = 0; i < assemblyTypes.Length; i++)
            {
                // update Components by Classes
                foreach (var componentClass in ComponentClasses.Values)
                {
                    if (componentClass.Contract.IsTypeMatchesWithContract(assemblyTypes[i]))
                    {
                        var newComponent = componentClass.AddComponentIfNotExist( assemblyTypes[i] );
                    }
                }

            }

        }

        /// <summary>
        /// Load Assembly Types into the Current ComponentsContainer by Registered ComponentClasses contracts.
        /// Loading Types array should belong to the one Assembly.
        /// Here we adding to Container some Types[] without successfull Check - of their assembly as TypeCache.IsCustomDomainAssemblyCheck(typesassembly)
        /// </summary>
        /// <param name="addedDomainTypes"></param>
        public virtual void LoadComponents(Type[] addedDomainTypes)
        {
            if (addedDomainTypes.IsNotWorkable()) return;
            
            // assembly can be scanned twice or more times

            for (int i = 0; i < addedDomainTypes.Length; i++)
            {
                // update Components by Classes
                foreach (var componentClass in  ComponentClasses.Values)
                {
                    if (componentClass.Contract.IsTypeMatchesWithContract(addedDomainTypes[i]))
                    {
                        // it looks like method including types,  Assembly is in excluding types list
                        //var assemblyKey = addedDomainTypes[i].Assembly.GetAssemblyNameEx();

                        CheckAssemblyNotScannedThenAddSlot(addedDomainTypes[i].Assembly);                        
                        
                        var newComponent = componentClass.AddComponentIfNotExist(addedDomainTypes[i], true);
                    }
                }
            }
          
        }




        /// <summary>
        /// Rising Init actions of each of ComponentsClasses. In this Actions each ComponentsClass analyzing all it's components and can do some prepartion to use them further.
        ///<para/> Rising can be used multiple times - but your ComponentClass InitAction should have logic about such behavior. 
        /// </summary>
        public static void InitializeComponentsClasses()
        {
            //run Init Actions for Current.ComponentClasses
            foreach (var registeredComponentClass in Current.ComponentClasses)
            {
                registeredComponentClass.Value.RiseInitIfActionExist();
            }

        }


        #endregion ----------------------------------- LOADING ASSEMBLIES TO CONTAINER --------------------------------



        #region ------------------------- BUILD CONTAINER --------------------------------


        /// <summary>
        /// Returns  Current -singleton instance of this container
        /// </summary>
        /// <returns></returns>
        public static ComponentsContainer GetSingleton()
        {
            return Current;
        }



        #endregion ------------------------------ BUILD CONTAINER --------------------------------



        ////  ---------.---------
        //// GETTING VARIATIONS:
        //// {  [1.1] Get ComponentsClasses,
        ////     [1.2] Get ComponentsClass, 
        ////   [2.1] Get Components, 
        ////   [2.2] Get Component, 
        ////     [3.1] Get Assembly Components, 
        ////     [3.2] Get Assembly Component } 
        ////  ---------.---------



        ////    [1.1] Get Components Classes
        #region ------------------------------ [1.1] GET COMPONENTS CLASSES ------------------------------


        /// <summary>
        /// Get Current.ComponentClasses dictionary with all of its components.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, ComponentDefinition> GetComponentsClasses()
        {
            return Current.ComponentClasses;
        }

        #endregion ------------------------------[1.1] GET COMPONENTS CLASSES ------------------------------


        ////    [1.2] Get Components Class by [ComponentClassKey/En]
        ////    [1.2] Get Components Class by [ComponentsClassCriterionType] 
        #region ------------------- [1.2] GET COMPONENTS CLASS by [ComponentClassKey/En], by [ComponentsClassCriterionType]------------------------

        /// <summary>
        /// Get componentsClass by [ComponentClassKey] - componentsClassKey.
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <returns></returns>
        public static ComponentDefinition GetComponentsClass(string componentsClassKey)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponentsClass), nameof(componentsClassKey));

            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return null;

            return Current.ComponentClasses[componentsClassKey];
        }

        /// <summary>
        /// Get componentsClass by [ComponentClassEn] - componentsClass.
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <returns></returns>
        public static ComponentDefinition GetComponentsClass(ComponentClassEn componentsClass)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponentsClass), nameof(componentsClass));
            
            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return null;

            return Current.ComponentClasses[componentsClass.S()];
        }


        /// <summary>
        /// Get  componentsClass by [ComponentsClassCriterionType] - componentsClassCriterionType.
        /// </summary>
        /// <param name="componentsClassCriterionType"></param>
        /// <returns></returns>
        public static ComponentDefinition GetComponentsClass(Type componentsClassCriterionType)
        {
            Validator.ATNullReferenceArgDbg(componentsClassCriterionType, Class, nameof(GetComponentsClass), nameof(componentsClassCriterionType));

            if (componentsClassCriterionType.IsNull()) return null;

            return Current.ComponentClasses.Values.FirstOrDefault(
                    cmpCls => cmpCls.Contract.ContractType == componentsClassCriterionType);

        }

        #endregion ------------------- [1.2] GET COMPONENTS CLASS by [ComponentClassKey/En], by [ComponentsClassCriterionType]------------------------


        ////    [2.1] Get Components by [ComponentClassKey/En]
        ////    [2.1] Get Components by [ComponentClassKey/En] and [ComponentItemInfo selector Func]
        ////    [2.1] Get Components by [ComponentClassKey/En] and [TComponentMetaInfo selector Func]
        ////    [2.1] Get Components by [ComponentClassKey/En] and [Type selector Func]
        #region ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] FROM CONTAINER ----------------------

        /// <summary>
        /// Get Components by [ComponentClassKey] 
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(string componentsClassKey)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponents), nameof(componentsClassKey));

            var resultList = new List<ComponentMatch>();
            if (componentsClassKey.IsNullOrEmpty()
                || Current.ComponentClasses.NotContainsKey(componentsClassKey) ) return resultList;
          
            return GetComponentsClass(componentsClassKey).Matches;
        }


        /// <summary>
        /// Get Components by [ComponentClassEn] 
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(ComponentClassEn componentsClass)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponents), nameof(componentsClass));
       
            var resultList = new List<ComponentMatch>();
            if (componentsClass != ComponentClassEn.NotDefined
                || Current.ComponentClasses.NotContainsKey(componentsClass.S())) return resultList;

            return GetComponentsClass(componentsClass).Matches;
        }

        #endregion ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] FROM CONTAINER ----------------------


        #region ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER ----------------------

        /// <summary>
        /// Get Components by [ComponentClassKey] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(string componentsClassKey, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));
            
            var resultList = new List<ComponentMatch>();
            if (componentsClassKey.IsNotWorkable() || selector.IsNull()) return resultList;
            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return resultList;
            
            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;
            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i]) == true) resultList.Add(targetComponentsClass.Matches[i]);
            }
       
            return resultList;
        }


        /// <summary>
        /// Get Components by [ComponentClassEn] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(ComponentClassEn componentsClass, Func<ComponentMatch, bool> selector)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));


            var resultList = new List<ComponentMatch>();
            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return resultList;
          
            var targetComponentsClass = GetComponentsClass(componentsClass.S());
            if (targetComponentsClass.IsNull()) return null;
            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i]) == true) resultList.Add(targetComponentsClass.Matches[i]);
            }
          
            return resultList;
        }

        #endregion ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER ----------------------


        #region ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER --------------------------

        /// <summary>
        /// Get Components by [ComponentClassKey] and [TComponentMetaInfo selector Func], where TComponentMetaInfo - component's metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"> attribute class,that also implements [IComponentItemMetaInfo] </typeparam>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List< ComponentMatch> GetComponents<TComponentMetaInfo>(string componentsClassKey, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : class, IComponentMatchMetaInfo
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));

            var resultList = new List<ComponentMatch>();
            if (componentsClassKey.IsNotWorkable() || selector.IsNull()) return resultList;
            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return resultList;
                        
            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (targetComponentsClass.Matches[i].MetaInfo.IsNotNull() 
                    && selector(targetComponentsClass.Matches[i].MetaInfo as TComponentMetaInfo) == true)
                    resultList.Add(targetComponentsClass.Matches[i]);
            }
           
            return resultList;
        }


        /// <summary>
        ///  Get Components by [ComponentClassEn] and [TComponentMetaInfo selector Func], where TComponentMetaInfo - component's metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo">attribute class,that also implements [IComponentItemMetaInfo]</typeparam>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents<TComponentMetaInfo>(ComponentClassEn componentsClass, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : class, IComponentMatchMetaInfo
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));


            var resultList = new List<ComponentMatch>();
            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return resultList;
                        
            var targetComponentsClass = GetComponentsClass(componentsClass.S());
            if (targetComponentsClass.IsNull()) return null;
            
            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (targetComponentsClass.Matches[i].MetaInfo.IsNotNull()
                    && selector(targetComponentsClass.Matches[i].MetaInfo as TComponentMetaInfo) == true)
                    resultList.Add(targetComponentsClass.Matches[i]);
            }

            return resultList;
        }

        #endregion ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER ----------------------------


        #region ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ----------------------


        /// <summary>
        /// Get Components by [ComponentClassKey] and [Type selector Func]
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(string componentsClassKey, Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));

            var resultList = new List<ComponentMatch>();

            if (componentsClassKey.IsNotWorkable() || selector.IsNull()) return resultList;
            if (Current.ComponentClasses.ContainsKey(componentsClassKey)) return resultList;

            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i].ComponentType) == true)
                    resultList.Add(targetComponentsClass.Matches[i]);
            }

            return resultList;
        }



        /// <summary>
        /// Get Components by [ComponentClassEn] and [Type selector Func]
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetComponents(ComponentClassEn componentsClass, Func<Type, bool> selector)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponents), nameof(selector));

            var resultList = new List<ComponentMatch>();
            if (Current.ComponentClasses.ContainsKey(componentsClass.S())) return resultList;

            var targetComponentsClass = GetComponentsClass(componentsClass.S());
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i].ComponentType) == true) resultList.Add(targetComponentsClass.Matches[i]);
            }

            return resultList;
        }

        #endregion ------------ [2.1] GET  COMPONENTS by [ComponentClassKey/En] and Type[Type selector Func] FROM CONTAINER ----------------------



        ////    [2.2] searching components - there is no different from what assembly -from all domain assemblies
        ////    [2.2] Get Component by [ComponentClassKey/En] and [ComponentItemInfo selector Func]
        ////    [2.2] Get Component by [ComponentClassKey/En] and [TComponentMetaInfo selector Func]
        ////    [2.2] Get Component by [ComponentClassKey/En] and [Type selector Func]
        ////    [2.2] Get Component by [Type]
        #region --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER --------------------


        /// <summary>
        /// Get Component by [ComponentClassKey] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent(string componentsClassKey, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (componentsClassKey.IsNullOrEmpty() || selector.IsNull()) return null;
            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return null;

            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i]) == true) return targetComponentsClass.Matches[i];
            }

            return null;
        }


        /// <summary>
        /// Get Component by [ComponentClassEn] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent(ComponentClassEn componentsClass, Func<ComponentMatch, bool> selector)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));
            
            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return null;

            var targetComponentsClass = GetComponentsClass(componentsClass);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i]) == true) return targetComponentsClass.Matches[i];
            }

            return null;
        }

        #endregion --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER --------------------


        #region --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER --------------------

        /// <summary>
        /// Get Component  by [ComponentClassKey] and [TComponentMetaInfo selector Func] ,where TComponentMetaInfo -component's metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent<TComponentMetaInfo>(string componentsClassKey, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : class, IComponentMatchMetaInfo
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (componentsClassKey.IsNotWorkable() || selector.IsNull()) return null;
            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return null;

            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (targetComponentsClass.Matches[i].MetaInfo.IsNotNull()
                    && selector(targetComponentsClass.Matches[i].MetaInfo as TComponentMetaInfo) == true)
                    return targetComponentsClass.Matches[i];
            }

            return null;
        }


        /// <summary>
        /// Get Component  by [ComponentClassEn] and [TComponentMetaInfo selector Func] ,where TComponentMetaInfo -component's metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent<TComponentMetaInfo>(ComponentClassEn componentsClass, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : class, IComponentMatchMetaInfo
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return null;

            var targetComponentsClass = GetComponentsClass(componentsClass);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (targetComponentsClass.Matches[i].MetaInfo.IsNotNull()
                    && selector(targetComponentsClass.Matches[i].MetaInfo as TComponentMetaInfo) == true)
                    return targetComponentsClass.Matches[i];
            }

            return null;
        }

        #endregion --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER --------------------


        #region --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ------------------


        /// <summary>
        /// Get Component by [ComponentClassKey] and [Type selector Func], where Type is component.ComponentType
        /// </summary>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent(string componentsClassKey, Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));

            if (componentsClassKey.IsNotWorkable() || selector.IsNull()) return null;
            if (Current.ComponentClasses.NotContainsKey(componentsClassKey)) return null;

            var targetComponentsClass = GetComponentsClass(componentsClassKey);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i].ComponentType) == true) return targetComponentsClass.Matches[i];
            }

            return null;
        }


        /// <summary>
        ///  Get Component by [ComponentClassEn] and [Type selector Func] , where Type is component.ComponentType
        /// </summary>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent(ComponentClassEn componentsClass, Func<Type, bool> selector)
        {
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetComponent), nameof(selector));
            
            if (Current.ComponentClasses.NotContainsKey(componentsClass.S())) return null;

            var targetComponentsClass = GetComponentsClass(componentsClass);
            if (targetComponentsClass.IsNull()) return null;

            for (int i = 0; i < targetComponentsClass.Matches.Count; i++)
            {
                if (selector(targetComponentsClass.Matches[i].ComponentType) == true) return targetComponentsClass.Matches[i];
            }

            return null;
        }

        #endregion --------------- [2.2] GET  COMPONENT by [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ------------------


        #region --------------- [2.2] GET  COMPONENT by [Type] FROM CONTAINER ------------------------

        /// <summary>
        /// Get Component by [Type].
        ///  Here we beleave that componentType - is the real class Type and it is not ( interface or some base abstract class Type, and that it is not (array or enum).
        /// </summary>
        /// <param name="componentType"></param>
        /// <returns></returns>
        public static ComponentMatch GetComponent(Type componentType)
        {
            Validator.ATNullReferenceArgDbg(componentType, Class, nameof(GetComponent), nameof(componentType));

            return GetAssemblyComponent(componentType);

        }


        #endregion ------------------ [2.2] GET  COMPONENT by [Type] FROM CONTAINER ------------------------



        ////   [3.1] - searching components - but we need components those only  exist in some assembly 
        ////   [3.1] Get AssemblyComponents by [assemblyName]
        ////   [3.1] Get AssemblyComponents by [assemblyName] and [ComponentClassKey/En]
        ////   [3.1] Get AssemblyComponents by [assemblyName] and [ComponentClassKey/En] and [ComponentItemInfo selector Func]
        ////   [3.1] Get AssemblyComponents by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func]
        ////   [3.1] Get AssemblyComponents by [assemblyName] and [ComponentClassKey/En] and [Type selector Func]
        #region ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] FROM CONTAINER ----------------------


        /// <summary>
        /// Get Components that exist-was searched in Assembly by [assemblyName]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));

            var resultList = new List<ComponentMatch>();
            if (Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values;
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
                resultList.Add(item);
            }

            return resultList;
        }

        #endregion ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] FROM CONTAINER ----------------------


        #region ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] and [ComponentClassKey/En] FROM CONTAINER ---------------------------------


        /// <summary>
        /// Get Components that exist-was searched in Assembly by [assemblyName] and [ComponentClassKey] 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName ,string componentClassKey)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentClassKey, Class, nameof(GetAssemblyComponents), nameof(componentClassKey));

            var resultList = new List<ComponentMatch>();
            if (Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values;
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
               if(item.ComponentClass.Key == componentClassKey) resultList.Add(item);
            }

            return resultList;
        }



        /// <summary>
        /// Get Components that exist-was searched in Assembly by [assemblyName] and [ComponentClassEn] 
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName, ComponentClassEn componentsClass)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponents), nameof(componentsClass));
            

            var resultList = new List<ComponentMatch>();
            if (Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values;
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
                if (item.ComponentClass.Key == componentsClass.S()) resultList.Add(item);
            }

            return resultList;
        }

        #endregion ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] and [ComponentClassKey/En] FROM CONTAINER ---------------------------------
        

        #region ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] and [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER ---------------------------------

        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassKey] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName, string componentsClassKey, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));
            
            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNullOrEmpty() || selector.IsNull()
                 || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = GetAssemblyComponents(assemblyName,componentsClassKey);
            if (classComponents.IsNotWorkable()) return resultList;

            for (int i = 0; i < classComponents.Count; i++)
            {
                if (selector(classComponents[i]) == true) resultList.Add(classComponents[i]);
            } 
                
            return resultList;
        }


        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassEn] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName, ComponentClassEn componentsClass, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));


            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
              || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClass.S());
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
                if (selector(item) == true) resultList.Add(item);
            }
            return resultList;
        }


        #endregion ------------------------------- GET ASSEMBLY  COMPONENTS  [by assemblyName and ClassKey and ComponentInfo] FROM CONTAINER ---------------------------------


        #region ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER ------------------------



        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassKey] and [TComponentMetaInfo selector Func], where TComponentMetaInfo -is component metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents<TComponentMetaInfo>(string assemblyName, string componentsClassKey, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : Attribute, IComponentMatchMetaInfo
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));
            

            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNotWorkable() || selector.IsNull()
                 || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClassKey);
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
                if (item.MetaInfo.IsNotNull() && selector(item.MetaInfo as TComponentMetaInfo) == true) resultList.Add(item);
            }

            return resultList;
        }


        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassEn] and and [TComponentMetaInfo selector Func], where TComponentMetaInfo -is component metaInfo attribute
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents<TComponentMetaInfo>(string assemblyName, ComponentClassEn componentsClass, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : Attribute, IComponentMatchMetaInfo
        {

            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));
            
            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
              || Current.ComponentsByAssemblies.ContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClass.S());
            if (classComponents.IsNotWorkable()) return resultList;

            foreach (var item in classComponents)
            {
                if (item.MetaInfo.IsNotNull() && selector(item.MetaInfo as TComponentMetaInfo) == true) resultList.Add(item);
            }

            return resultList;
        }

        #endregion ---------------- [3.1] GET ASSEMBLY COMPONENTS by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER ------------------------


        #region ---------------- [3.1] GET ASSEMBLY  COMPONENTS by [assemblyName] and [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ----------------------


        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassKey] and [Type selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName, string componentsClassKey, Func<Type, bool> selector)
        {

            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponents), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));
            
            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNotWorkable() || selector.IsNull()
                 || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClassKey);
            if ( classComponents.IsNotWorkable() ) return resultList;

            foreach (var item in classComponents)
            {
                if (selector(item.ComponentType) == true) resultList.Add(item);
            }

            return resultList;
        }



        /// <summary>
        /// Get Module Components by [assemblyName] and [ComponentClassEn] and [Type selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static List<ComponentMatch> GetAssemblyComponents(string assemblyName, ComponentClassEn componentsClass, Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponents), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponents), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponents), nameof(selector));
            
            var resultList = new List<ComponentMatch>();
            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
              || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return resultList;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClass.S());
            if ( classComponents.IsNotWorkable() )  return resultList;

            foreach (var item in classComponents)
            {
                if (selector(item.ComponentType) == true) resultList.Add(item);
            }

            return resultList;
        }

        #endregion ---------------- [3.1] GET ASSEMBLY COMPONENTS by [assemblyName] and [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ----------------------





        ////   [3.2] - searching component - but we need component that exist in the some end assembly  
        ////   [3.2] Get AssemblyComponent by [assemblyName] and [ComponentClassKey/En] and [ComponentItemInfo selector Func]
        ////   [3.2] Get AssemblyComponent by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func]
        ////   [3.2] Get AssemblyComponent by [assemblyName] and [ComponentClassKey/En] and [Type selector Func]
        ////   [3.2] Get AssemblyComponent by [Type]
        #region ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER ----------------------


        /// <summary>
        /// Get Module Component by [assemblyName] and [ComponentClassKey] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent(string assemblyName, string componentsClassKey, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));
            
            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNullOrEmpty() || selector.IsNull() 
                || Current.ComponentsByAssemblies.NotContainsKey(assemblyName) ) return null;
            
            //select all components from assembly with ClassKey
            var classComponents = GetAssemblyComponents(assemblyName, componentsClassKey);      
            if (classComponents.IsNotWorkable()) return null;
            
            for (int i = 0; i < classComponents.Count; i++)
            {
                if (selector(classComponents[i]) == true) return classComponents[i];
            }
            
            return null;
        }


        /// <summary>
        /// Get Module Component by [assemblyName] and [ComponentClassEn] and [ComponentItemInfo selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent(string assemblyName, ComponentClassEn componentsClass, Func<ComponentMatch, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));
            
            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
                || Current.ComponentsByAssemblies.ContainsKey(assemblyName)) return null;

            //select all by Class
            var classComponents = GetAssemblyComponents(assemblyName, componentsClass);
            if (classComponents.IsNotWorkable()) return null;

            foreach (var item in classComponents)
            {
                if (selector(item) == true) return item;
            }
            return null;
        }

        #endregion ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [ComponentItemInfo selector Func] FROM CONTAINER ----------------------


        #region ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER ---------------------

        /// <summary>
        /// Get Module Component by [assemblyName] and [ComponentClassKey] and [TComponentMetaInfo(component metaInfo attribute) selector Func]
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent<TComponentMetaInfo>(string assemblyName, string componentsClassKey, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : Attribute, IComponentMatchMetaInfo
        {

            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));
            
            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNullOrEmpty() || selector.IsNull()
                || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return null;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClassKey);
            if (classComponents.IsNotWorkable()) return null;
            
            foreach (var item in classComponents)
            {
                if (item.MetaInfo.IsNotNull() && selector(item.MetaInfo as TComponentMetaInfo) == true) return item;
            }

            return null;
        }



        /// <summary>
        /// Get Module Component by [assemblyName] and [ComponentClassEn] and [TComponentMetaInfo(component metaInfo attribute) selector Func]
        /// </summary>
        /// <typeparam name="TComponentMetaInfo"></typeparam>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent<TComponentMetaInfo>(string assemblyName, ComponentClassEn componentsClass, Func<TComponentMetaInfo, bool> selector)
            where TComponentMetaInfo : Attribute, IComponentMatchMetaInfo
        {

            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));

            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
               || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return null;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClass.S());
            if (classComponents.IsNotWorkable()) return null;
                        
            foreach (var item in classComponents)
            {
                if (item.MetaInfo.IsNotNull() && selector(item.MetaInfo as TComponentMetaInfo) == true) return item;
            }

            return null;
        }

        #endregion ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [TComponentMetaInfo selector Func] FROM CONTAINER ---------------------


        #region ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ----------------------


        /// <summary>
        /// Get Module Component by [assemblyName] and [ComponentClassKey] and [Type selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClassKey"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent(string assemblyName, string componentsClassKey, Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATNullReferenceArgDbg(componentsClassKey, Class, nameof(GetAssemblyComponent), nameof(componentsClassKey));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));

            if (assemblyName.IsNullOrEmpty() || componentsClassKey.IsNotWorkable() || selector.IsNull()
                || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return null;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClassKey);
            if (classComponents.IsNotWorkable()) return null;
           
            foreach (var item in classComponents)
            {
                if (selector(item.ComponentType) == true) return item;
            }

            return null;
        }


        /// <summary>
        ///  Get Module Component by [assemblyName] and [ComponentClassEn] and [Type selector Func]
        /// </summary>
        /// <param name="assemblyName"></param>
        /// <param name="componentsClass"></param>
        /// <param name="selector"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent(string assemblyName, ComponentClassEn componentsClass, Func<Type, bool> selector)
        {
            Validator.ATNullReferenceArgDbg(assemblyName, Class, nameof(GetAssemblyComponent), nameof(assemblyName));
            Validator.ATInvalidOperationDbg(componentsClass == ComponentClassEn.NotDefined, Class, nameof(GetAssemblyComponent), nameof(componentsClass));
            Validator.ATNullReferenceArgDbg(selector, Class, nameof(GetAssemblyComponent), nameof(selector));
            
            if (assemblyName.IsNullOrEmpty() || selector.IsNull()
               || Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return null;

            //select all by Class
            var classComponents = Current.ComponentsByAssemblies[assemblyName].Values.Where(cmp => cmp.MetaInfo.ComponentDefinitionKey == componentsClass.S());
            if (classComponents.IsNotWorkable()) return null;

            foreach (var item in classComponents)
            {
                if (selector(item.ComponentType) == true) return item;
            }

            return null;
        }


        #endregion ---------------- [3.2] GET ASSEMBLY COMPONENT by [assemblyName] and [ComponentClassKey/En] and [Type selector Func] FROM CONTAINER ----------------------


        #region ---------------- [3.2] GET ASSEMBLY COMPONENT by [Type] FROM CONTAINER -------------------------

        /// <summary>
        /// Get Module Component only by [Type]- Type of component - [component.ComponentType].
        /// <para/> If componentType exists in innner Loaded components, then method returns target ComponentItemInfo. 
        /// <para/> If assembly of such componentType doesn't exist in container or component with such ComponentType won't be found in assemblyDictionary then method returns null.
        /// </summary>
        /// <param name="componentType"></param>
        /// <returns></returns>
        public static ComponentMatch GetAssemblyComponent(Type componentType)
        {
            Validator.ATNullReferenceArgDbg(componentType, Class, nameof(GetAssemblyComponent), nameof(componentType));
            
            string assemblyName = componentType.Assembly.GetAssemblyNameEx();//

            if (Current.ComponentsByAssemblies.NotContainsKey(assemblyName)) return null;

            //select one component by Class
            return Current.ComponentsByAssemblies[assemblyName].Values
                .FirstOrDefault(cmp => cmp.ComponentType == componentType);

        }


        #endregion ---------------- [3.2] GET ASSEMBLY  COMPONENT by [Type] FROM CONTAINER -------------------------




        ////     Resolve Component Instance Boxed of serviceType, BindingFlags and argumnts
        ////     Resolve Component Instance Boxed Lazy of serviceType, BindingFlags and argumnts
        ////     Resolve Component Instance T of serviceType, BindingFlags and argumnts
        ////     Resolve Component Instance T Lazy of serviceType, BindingFlags and argumnts
        #region -------------------------------RESOLVE COMPONENT INSTANCE ------------------------------------ 

        /// <summary>
        /// ResolveComponentInstanceBoxed - trying to return instance of serviceType, returns value as boxed into object.
        /// It searchs Component of serviceType by condition - where [component.ComponentType==serviceType],  and then (Create by TypeActivator)/(Get Singleton instance) of serviceType.
        /// <para/> In DEBUG mode method checks that this serviceType is not (interface or abstract class or array or enum), else throw InvalidOperationException. 
        /// <para/> This container doesn't store any referencies of non singleton/runtime temporarily component instancies , or manage by LifiTime Scope of such non static instancies.
        /// </summary>
        /// <exception cref="InvalidOperationException"></exception>
        /// <returns></returns>
        public static object ResolveComponentInstanceBoxed(Type serviceType, BindingFlags ctorFilter = DefaultCtorSearchBinding, params object[] args)
        {
            Validator.ATInvalidOperationDbg(serviceType.IsInterface || serviceType.IsAbstract || serviceType.IsArray || serviceType.IsEnum
                                                    , nameof(ComponentsContainer), nameof(ResolveComponentInstanceBoxed),
                                                    "incorrect serviceType - target serviceType can't be -[ Interface or Abstract class,or Array, or Enum], where serviceType is [{0}]".Fmt(serviceType.FullName));

            var component = GetAssemblyComponent(serviceType);//
            if (component.IsNull()) return null;

            return component.ResolveInstanceBoxed();

        }



        /// <summary>
        /// ResolveComponentInstanceBoxedLazy - trying to return instance of serviceType, returns value as boxed into object.
        /// It searchs Component of serviceType by condition - where [component.ComponentType==serviceType],  and then (Create by TypeActivator)/(Get Singleton instance) of serviceType.
        /// <para/> In DEBUG mode method checks that this serviceType is not (interface or abstract class or array or enum), else throw InvalidOperationException. 
        /// <para/> This container doesn't store any referencies of non singleton/runtime temporarily component instancies , or manage by LifiTime Scope of such non static instancies.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="ctorFilter"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static object ResolveComponentInstanceBoxedLazy(Type serviceType, BindingFlags ctorFilter = DefaultCtorSearchBinding, params object[] args)
        {
            Validator.ATInvalidOperationDbg(serviceType.IsInterface || serviceType.IsAbstract || serviceType.IsArray || serviceType.IsEnum
                                                    , nameof(ComponentsContainer), nameof(ResolveComponentInstanceBoxedLazy),
                                                    "incorrect serviceType - target serviceType can't be -[ Interface or Abstract class,or Array, or Enum], where serviceType is [{0}]".Fmt(serviceType.FullName));

            var component = GetAssemblyComponent(serviceType);//
            if (component.IsNull()) return null;

            return component.ResolveInstanceBoxedLazy();

        }




        /// <summary>
        /// ResolveComponentInstanceT - trying to return instance of serviceType, returns value as boxed into object.
        /// It searchs Component of serviceType by condition - where [component.ComponentType==serviceType],  and then (Create by TypeActivator)/(Get Singleton instance) of serviceType.
        /// <para/> In DEBUG mode method checks that this serviceType is not (interface or abstract class or array or enum), else throw InvalidOperationException. 
        /// <para/> This container doesn't store any referencies of non singleton/runtime temporarily component instancies , or manage by LifiTime Scope of such non static instancies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceType"></param>
        /// <param name="ctorFilter"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T ResolveComponentInstanceT<T>(Type serviceType, BindingFlags ctorFilter = DefaultCtorSearchBinding, params object[] args)//  ComponentClassEn componentsClass,
        {
            Validator.ATInvalidOperationDbg(serviceType.IsInterface || serviceType.IsAbstract || serviceType.IsArray || serviceType.IsEnum
                                                    , nameof(ComponentsContainer), nameof(ResolveComponentInstanceBoxed),
                                                    "incorrect serviceType - target serviceType can't be -[ Interface or Abstract class,or Array, or Enum], where serviceType is [{0}]".Fmt(serviceType.FullName));

            //select all by Class
            var component = GetAssemblyComponent(serviceType);//
            if (component.IsNull()) return default(T);

            return component.ResolveInstanceTBase<T>();

        }




        /// <summary>
        /// ResolveComponentInstanceTLazy - trying to return instance of serviceType, returns value as boxed into object.
        /// It searchs Component of serviceType by condition - where [component.ComponentType==serviceType],  and then (Create by TypeActivator)/(Get Singleton instance) of serviceType.
        /// <para/> In DEBUG mode method checks that this serviceType is not (interface or abstract class or array or enum), else throw InvalidOperationException. 
        /// <para/> This container doesn't store any referencies of non singleton/runtime temporarily component instancies , or manage by LifiTime Scope of such non static instancies.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serviceType"></param>
        /// <param name="ctorFilter"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static T ResolveComponentInstanceTLazy<T>(Type serviceType, BindingFlags ctorFilter = DefaultCtorSearchBinding, params object[] args)

        {
            Validator.ATInvalidOperationDbg(serviceType.IsInterface || serviceType.IsAbstract || serviceType.IsArray || serviceType.IsEnum
                                                      , nameof(ComponentsContainer), nameof(ResolveComponentInstanceBoxed),
                                                      "incorrect serviceType - target serviceType can't be -[ Interface or Abstract class,or Array, or Enum], where serviceType is [{0}]".Fmt(serviceType.FullName));

            //select all by Class
            var component = GetAssemblyComponent(serviceType);//
            if (component.IsNull()) return default(T);

            return component.ResolveInstanceTBaseLazy<T>();
        }


        #endregion -------------------------------RESOLVE COMPONENT INSTANCE ------------------------------------





        #region --------------------------------- IDisposable ------------------------------------

        public void Dispose()
        {


        }

        #endregion --------------------------------- IDisposable ------------------------------------



#if SERVER && IIS

        #region --------------------------------- IDependencyScope, IDependencyResolver --------------------------------------


        /// <summary>
        /// Here we beleave that serviceType - is the real class Type and it is not (interface or abstract class or array or enum).
        /// <para/> If component has singleton instance,then method returns its singleton instance, else method retuns instance created by TypeActivator.
        /// <para/> Return result of GetComponentInstance(serviceType) with default BindingFlags ,and defaultCtor -with no args. 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public object GetService(Type serviceType)
        {
            Validator.ATNullReferenceArgDbg(serviceType, nameof(ComponentsContainer), nameof(GetService),
               nameof(serviceType));

            if (serviceType.IsNull()) return null;
            
            var searchedComponent = GetAssemblyComponent(serviceType);
            if (searchedComponent.IsNull()) return null;
            

            return searchedComponent.ResolveInstanceBoxedLazy();
        }

        /// <summary>
        /// Here we beleave that serviceType - is interface or some base class Type, and that it is not (array or enum).
        /// <para/> By serviceType we'll get ComponentClass with such criterionType==serviceType, then method returns all of component instancies of this ComponentClass, we use method GetComponentInstance for each of components.
        /// <para/> If componentsClass won't be found then method returns emplty List{object} - not null.  
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            Validator.ATNullReferenceArgDbg(serviceType, nameof(ComponentsContainer), nameof(GetServices),
                nameof(serviceType));

            Validator.ATInvalidOperationDbg(serviceType.IsArray || serviceType.IsEnum,
                nameof(ComponentsContainer), nameof(GetServices)
                , "Here we beleave that serviceType - is interface or some base class Type, and that it is not (array or enum). serviceType is[{0}] "
                  .Fmt(serviceType.FullName));
            
            List<object> resultItems = new List<object>();

            if (serviceType.IsNull()) return resultItems;
            
            var componentsClass = GetComponentsClass(serviceType);
            if (componentsClass.IsNull()) return resultItems;

            for (int i = 0; i < componentsClass.Matches.Count; i++)
            { resultItems.Add( GetService( componentsClass.Matches[i].ComponentType) );
            }
            
            return resultItems;
        }


        /// <summary>
        /// Wrap method that returns  Current instance of this container.
        /// </summary>
        /// <returns></returns>
        public IDependencyScope BeginScope()
        {
            return Current;
        }




        #endregion --------------------------------- IDependencyScope, IDependencyResolver --------------------------------------





        #region ----------------------------------- IAssembliesResolver --------------------------------------------


        /// <summary>
        /// Collecting AppDomain Assemblies by TypeCache API(TypeCache.GetCustomDomainAssemblies()).
        /// </summary>
        /// <returns></returns>
        public ICollection<Assembly> GetAssemblies()
        {
            return TypeCache.GetCustomDomainAssemblies();
        }


        #endregion ----------------------------------- IAssembliesResolver --------------------------------------------
        

#endif




    }
}


 



#region ------------------------------- GARBAGE ------------------------------

/// <summary>
/// All Scanned assemblies when- you used LoadComponents() methods. 
/// With the help of this list we can further make ScanPreviosAssemblies(). 
/// </summary>
/// <remarks>
/// We need this separate list of Assemblies because when we try to LoadComponents from Assembly it may do not contain any of components, and it's AssemblyName nowhere will be pointed,- so it'll be pointed here. 
/// </remarks>
//protected internal List<string> AllScannedAssemblies
//{ get; } = new List<string>();


///// <summary>
///// To add assembly component internally, by ComponentClassInfo for example
///// </summary>
///// <param name="component"></param>
////internal static void AddAssemblyComponent(ComponentItemInfo component)
////{
////    Validator.ATNullReferenceArgDbg(component, Class, nameof(AddAssemblyComponent), nameof(component));

////    // ComponentClass - group of Components - inside each class we have also   ComponentsByAssembly Dictionary
////    if (Current.ComponentsByAssemblies.NotContainsKey(component.AssemblyName))
////    {
////        Current.ComponentsByAssemblies.Add(component.AssemblyName, new Dictionary<string, ComponentItemInfo>());
////    }

////    if (Current.ComponentsByAssemblies[component.AssemblyName].ContainsKey(component.Key)) return;
////    Current.ComponentsByAssemblies[component.AssemblyName].Add(component.Key, component);
////}

#endregion ------------------------------- GARBAGE ------------------------------