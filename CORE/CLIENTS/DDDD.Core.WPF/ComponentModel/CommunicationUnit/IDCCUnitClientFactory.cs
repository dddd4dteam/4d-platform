﻿
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Patterns;
using System;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TClient"></typeparam>
    //public interface IDCCUnitClientFactory<TClient> : IDCCUnitClientFactory
    //{

    //    /// <summary>
    //    /// create Communication Client instance. In different Factories Client type is different
    //    /// </summary>
    //    /// <param name="communicationUnit"></param>
    //    /// <returns></returns>
    //    IDCCUnitClient CreateClientT(DCCUnit communicationUnit);
    //}


    /// <summary>
    /// It's meta interface that help us to declare association of that type with one of the DCCommunication Unit Scenario. 
    /// DCCUnit  is standart App Component. 
    /// </summary>
    public interface IDCCUnitClientFactory :  IFactory 
    {
        /// <summary>
        /// ScenarioKeys means Scenarios[technology+configuration], where this Factory Client can be used.
        /// <para/>Client-side  Factories Client items can be used in several Scenarious/instead of Server-side factories Clients.
        /// </summary>
        string[] ScenarioKeys{ get; }

        /// <summary>
        /// Add or use Exist Client instance
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        IDCCUnitClient AddOrUseExistClient(DCCUnit communicationUnit);

        /// <summary>
        /// Create new Client
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        IDCCUnitClient CreateClient(DCCUnit communicationUnit);
    }

}
