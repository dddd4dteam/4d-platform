﻿using DDDD.Core.Net;
using DDDD.Core.Net.ServiceModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// Uri prefixes for different types of communication Units
    /// </summary>
    public enum UriPrefixEn
    {
        /// <summary>
        /// WCF service prefix
        /// </summary>
        wcf = 2,

        /// <summary>
        /// Web Api prefix
        /// </summary>
        webapi = 4,

        /// <summary>
        /// MVC prefix
        /// </summary>
        mvc = 8,

        /// <summary>
        /// SignalR prefix
        /// </summary>
        sigr = 16,

        /// <summary>
        /// Thrift prefix
        /// </summary>
        thrift = 32,

        /// <summary>
        /// Http Handler prefix
        /// </summary>
        HH = 64,

        /// <summary>
        /// Undetermined service prefix
        /// </summary>
        usvc = 128

    }

   



    /// <summary>
    /// DCCU( Dynamic Command Communication Unit ) Serialization mode. 
    /// <para/> In some cases we even can use combination(MDCS) - to use fully standart technology(WCF) but only as transport with primitive serializing option.
    /// <para/> We can use binary serialization with internal- TSSerializer(Type Set Serializer- TSS) and
    /// <para/> ServiceStacks text serializers - [JSON,XML,CSV,JSV]  and of course 
    /// <para/> Microsoft's standart(for WCF) DataContractSerializer - MDCS(Microsoft's DataContractSerializer- 
    /// <para/> 
    /// </summary>
    /// <remarks> notes:  here we use company name 'M'(Microsoft) letter in the beginning of'MDCS'(Microsoft DataContract serializer) abbreviation, to distinct DC(DataContract serializer) from DC-concept(Dynamic Command)  only).
    /// </remarks>
    public enum DCSerializationModeEn
    {
        /// <summary>
        /// Not defined serialization Mode
        /// </summary>
        NotDefined
        ,
        /// <summary>
        /// We are going to use Microsoft's standart(for WCF) DataContractSerializer to transfer DCMessage
        /// , and Use TSS binary serialization to pack message into message.Body.
        /// </summary>
        MDCS_TSS
        ,

        /// <summary>
        /// We are going to use directly TSS-Binary, max compact data serialization format, to transfer DCMessage and its data.
        /// </summary>
        TSS

        ,

        /// <summary>
        /// We are going to use directly  NewtonSoft JSON data serialization format, to transfer DCMessage and its data 
        /// </summary>
        NSJSON
#if !WP81

        ,
        /// <summary>
        /// We are going to use Microsoft's standart(for WCF) DataContractSerializer to transfer DCMessage
        /// , and Use ServiceStack's JSON serialization to pack message into message.Body.
        /// </summary>
        MDCS_SSJSON
        ,
        /// <summary>
        /// We are going to use Microsoft's standart(for WCF) DataContractSerializer to transfer DCMessage
        /// , and Use ServiceStack's XML serialization to pack message into message.Body.
        /// </summary>
        MDCS_SSXML
        ,
        /// <summary>
        /// We are going to use Microsoft's standart(for WCF) DataContractSerializer to transfer DCMessage
        /// , and Use ServiceStack's CSV serialization to pack message into message.Body.
        /// </summary>
        MDCS_SSCSV
        ,
          
        /// <summary>
        /// We are going to use directly  ServiceStack's XML data serialization format, to transfer DCMessage and its data
        /// </summary>
        SSXML

        ,
        /// <summary>
        /// We are going to use directly  ServiceStack's JSON data serialization format, to transfer DCMessage and its data
        /// </summary>
        SSJSON
       ,
        /// <summary>
        /// We are going to use directly  ServiceStack's CSV data serialization format, to transfer DCMessage and its data
        /// </summary>
        SSCSV
        ,
        /// <summary>
        /// We are going to use directly  ServiceStack's JSON data serialization format, to transfer DCMessage and its data
        /// </summary>
        SSJSV

        ////,
        ///// <summary>
        ///// We are going to use Microsoft's standart(for WCF) DataContractSerializer to transfer DCMessage
        ///// , and Use ServiceStack's JSV(JSON+CSV) serialization to pack message into message.Body.
        ///// </summary>
        //MDCS_JSV

#endif  
      
       
      
        

    }



}
