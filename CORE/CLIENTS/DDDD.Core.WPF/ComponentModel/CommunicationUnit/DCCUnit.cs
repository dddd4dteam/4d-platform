﻿using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;



using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Threading;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Diagnostics;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Environment;
using DDDD.Core.Resources;



#if !WP81
using ServiceStack.Text;
using System.Net.Http.Headers;
//using Newtonsoft.Json;
#endif


#if SERVER && IIS

using System.Web;
using System.Net;
using ServiceStack.Text.Jsv;
using DDDD.Core.Net;


#endif



namespace DDDD.Core.ComponentModel
{

    /// <summary>
    /// DCCUnit - DC Communication Unit's - contains info about owner and target communication unit representer class. 
    /// <para/> It does the following: Contains registering info about Owner of this Unit; 
    /// <para/>                        Can manage by serializationMode. 
    /// </summary>
    public class DCCUnit
    {

        #region ---------------------------------- CTOR ---------------------------------------


        DCCUnit(Type ownerType, DCCUnitOrderAttribute registeringInfo)
        {

            InitializeItem(ownerType, registeringInfo);
        }

        static DCCUnit()
        {
            InitializeStatic();
        }



        #endregion ---------------------------------- CTOR ---------------------------------------



        #region ----------------------------- ID , REGISTRATION INFO----------------------------------


        /// <summary>
        /// Identifier of DCCommnication Unit -of this wrap instance.
        /// </summary>
        public Guid ID
        { get; } = Guid.NewGuid();


        /// <summary>
        /// Registration info that Owner wants to use for it's work- some owner ordered string - [technology-configuration-scenario]-it's info about it:string,OwnerType, DCSerializationModeEn, TargetUri .
        /// </summary>
        public DCCUnitOrderAttribute RegistrationInfo
        { get; set; }




        #endregion ----------------------------- ID , REGISTRATION INFO----------------------------------


        #region ----------------------------------- Owner Type and Owner ---------------------------------

        /// <summary>
        /// Owner Type can be setted manually by SetOwnerAndUriResultAddress(Type ownerType)
        /// </summary>
        public Type OwnerType
        {
            get; private set;
        }




        /// <summary>
        /// Owner ShortKey - ComponentItemInfo.GetShortKey(OwnerType.Name)
        /// </summary>
        public string OwnerShortKey
        {
            get { return ComponentMatch.GetShortKey(OwnerType.Name); }
        }




        string _OwnerName;
        /// <summary>
        /// Owner Name is the Type.Name without standart word part. Like [ClientReport]Manager without word [Manager]. And other standart owner words.
        /// </summary>
        public string OwnerName
        {
            get
            {
                if (_OwnerName.IsNull())
                {
                    _OwnerName = GetOwnerName(OwnerType);
                }
                return _OwnerName;
            }
        }


        static string GetOwnerName(Type ownerType)
        {
            return ownerType.Name.Replace("Manager", "");
        }


        #endregion ----------------------------------- Owner Type and Owner ---------------------------------


        #region ------------------------------- INITIALIZATION ---------------------------------

        private static void InitializeStatic()
        {
            //JsConfig.DateHandler = DateHandler.ISO8601;
            //         //JsConfig.ExcludeTypeInfo = true;
            //         JsConfig.IncludePublicFields = true;
            //         JsConfig.InitStatics();

        }



        #endregion ------------------------------- INITIALIZATION ---------------------------------


        #region --------------------------DCCUNITS CACHE Communication Scenarios ------------------------------------


        /// <summary>
        /// DC Communication unit  instance Registering Func(  in *.config Action,or programmatically add-WebAPI ). This register Func will be used on server-side to register service item. 
        /// </summary>
        static Dictionary<string, Func<DCCUnit, bool>> RegisterFactoryItemConfigByScenarioFunc
        { get; } = new Dictionary<string, Func<DCCUnit, bool>>();

        static Type DCCUnitItemRegisterConfigFuncType = typeof(Func<DCCUnit, bool>);


        /// <summary>
        /// DC Communication units Register - Dictionary. By each of the DCCUnit Scenario
        /// - for each Fabric associated with this Unit Type, we register items 
        ///    - DCCUnits where we want to know [OwnerType(Type),DCCUnit]. 
        /// </summary>
        static Dictionary<string, Dictionary<Type, DCCUnit>> DCCUnits
        { get; } = new Dictionary<string, Dictionary<Type, DCCUnit>>();




#if SERVER && IIS

        /// <summary>
        ///  Add  new scenario if its still not exist;
        ///  On adding new Scenario, if Factory type has MetaInfo with not nullable InitConfig method pointer, then this Configuration method will be invoked.
        /// </summary>
        /// <param name="scenario"></param>
        /// <returns></returns>
        public static bool TryAddOrUseExistScenario(ComponentMatch dCCUnitServerFactoryComponent)
        {

            Validator.ATNullReferenceArgDbg(dCCUnitServerFactoryComponent, nameof(DCCUnit), nameof(TryAddOrUseExistScenario), nameof(dCCUnitServerFactoryComponent));


            var scenarioWasAdded = false;
            if (dCCUnitServerFactoryComponent.IsNull()) return scenarioWasAdded;


            var metainfo = (dCCUnitServerFactoryComponent.MetaInfo as DCCUnitServerFactoryAttribute);
            string scenario = metainfo.ScenarioKey.S();

            if (DCCUnits.ContainsKey(scenario)) return scenarioWasAdded; // we don't add it here

            //Init Factory config and add Scenario Key
            if (metainfo.InitFactoryGlobalConfigMethodName.IsNotNullOrEmpty())
            {
                var methodDelegate = dCCUnitServerFactoryComponent.ComponentType.GetMethod(metainfo.InitFactoryGlobalConfigMethodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

                if (methodDelegate.IsNotNull())
                {
                    methodDelegate.Invoke(null, null);
                }
            }

            //Init Factory item's configuration by DCCUnit 
            if (metainfo.InitFactoryItemConfigMethodName.IsNotNullOrEmpty())
            {
                var initFactoryItemConfigMethodFuncMethodInfo = dCCUnitServerFactoryComponent.ComponentType.GetMethod(metainfo.InitFactoryItemConfigMethodName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (initFactoryItemConfigMethodFuncMethodInfo.IsNull())
                { scenarioWasAdded = true; return scenarioWasAdded;
                }
                var initFactoryItemConfigMethodFunc = (Func<DCCUnit, bool>)initFactoryItemConfigMethodFuncMethodInfo.CreateDelegate(DCCUnitItemRegisterConfigFuncType);

                if (initFactoryItemConfigMethodFunc.IsNotNull())
                { RegisterFactoryItemConfigByScenarioFunc.Add(scenario, initFactoryItemConfigMethodFunc);
                }

            }

            // after we inited Senario Factory then registering it's Scenario Slot - [scenario, new Dictionary<Type, DCCUnit>()]

            DCCUnits.Add(scenario, new Dictionary<Type, DCCUnit>());  ////_Scenarios.Add(scenario); 

            scenarioWasAdded = true;
            return scenarioWasAdded;
        }

#elif CLIENT

        /// <summary>
        ///  Add  new scenario if its still not exist
        /// </summary>
        /// <param name="scenario"></param>
        /// <returns></returns>
        public static bool TryAddOrUseExistScenario(ComponentMatch  dCCUnitClientFactoryComponent)
        {
           if (dCCUnitClientFactoryComponent.IsNull()) return false;

            // add scenario with   IDCCommunicationUnitServerFactory implemented Type
            var metainfo = (dCCUnitClientFactoryComponent.MetaInfo as DCCUnitClientFactoryAttribute);
            
            //string[] scenarios = metainfo.TargetDCCommunicationUnits.ToArray();// S();
            //if (_Scenarios.Contains(scenarios[0])) return false;
            
            //Init Factory config and add Scenario Key


            return true;
        }

#endif


        #endregion |--------------------------DCCUNITS CACHE Communication Scenarios -----------------------------


        #region ------------------------------- BUILD DC COMMUNICATION UNIT URI ----------------------------------------

        /// <summary>
        /// Get associated with each CommunicationUnit  uri prefix.
        /// </summary>
        /// <param name="unitKey"></param>
        /// <returns></returns>
        public static string GetCommunicationPrefix(string scenarioKey)
        {
            var scenarioKeylower = scenarioKey.ToLower();
            if (scenarioKeylower.Contains(UriPrefixEn.wcf.S()))
            {
                return UriPrefixEn.wcf.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.webapi.S()) || scenarioKeylower.Contains("webapi"))
            {
                return UriPrefixEn.webapi.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.mvc.S()))
            {
                return UriPrefixEn.mvc.S();
            }
            if (scenarioKeylower.Contains(UriPrefixEn.sigr.S()) || scenarioKeylower.Contains("signalr"))
            {
                return UriPrefixEn.sigr.S();
            }

            if (scenarioKeylower.Contains(UriPrefixEn.thrift.S()))
            {
                return UriPrefixEn.thrift.S();
            }

            return UriPrefixEn.usvc.S();

        }


        /// <summary>
        ///  Build  service-communicationUnit's  uri  .
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <param name="ownerName"></param>
        /// <returns></returns>
        public static string BuildDCCUnitUriStartAddress(DCCUnit communicationUnit)
        {
            return OperationInvoke.CallInMode(nameof(DCCUnit), nameof(BuildDCCUnitUriStartAddress)
                 , () =>
                  {
                     return HttpRoute2.BuildServiceUri(communicationUnit.RegistrationInfo.UriTemplate
                            , RouteKeysEn.Service.KVPair(communicationUnit.OwnerName)
                            , RouteKeysEn.Controller.KVPair(communicationUnit.OwnerName)
                            , RouteKeysEn.Communication.KVPair(GetCommunicationPrefix(communicationUnit.RegistrationInfo.ScenarioKey))
                            , RouteKeysEn.WebApplication.KVPair(Environment2.DefaultUrl.WebApplication)
                             // ServiceName: communicationUnit.OwnerName,
                             // CommunicationPrefix: GetCommunicationPrefix(communicationUnit.RegistrationInfo.ScenarioKey)                           
                             );
                 });
        }


        #endregion ------------------------------- BUILD DC UNIT URI ----------------------------------------


        #region -------------------------- Uri result Address-----------------------------------

        /// <summary>
        /// Result Uri Address that we can build based on UriTemplate and current HttpRouteClient.StartUrl values.
        /// <para/> on the server side this uri value will be always relative.
        /// <para/> You can change/set HttpRouteClient.DefaultStartServer and  then call UriResultAddress() to get updated value of UriResultAddress.
        /// </summary>
        public string UriResultAddress
        { get; private set; }


        void InitializeItem(Type ownerType, DCCUnitOrderAttribute registeringInfo)
        {
            OwnerType = ownerType;
            RegistrationInfo = registeringInfo;

#if !WP81
            if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSJSON
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSXML
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSCSV
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.SSJSON
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.SSXML
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.SSCSV
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.SSJSV
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.NSJSON
               )
            {
                UseTextSerialization = true;
            }
            else if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.TSS
                )
            {
                UseBinSerialization = true;
            }
#else
           

            if ( RegistrationInfo.SerializationMode == DCSerializationModeEn.NSJSON )
            {
                UseTextSerialization = true;
            }
            else if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
               || RegistrationInfo.SerializationMode == DCSerializationModeEn.TSS
                )
            {
                UseBinSerialization = true;
            }
#endif


            if (RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_TSS
#if !WP81
                || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSJSON
                || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSXML
                || RegistrationInfo.SerializationMode == DCSerializationModeEn.MDCS_SSCSV
#endif
              )
            {
                UsePackUnpackLogic = true;
            }




            //var hostEntityName = "";
            var targetUriAddress = BuildDCCUnitUriStartAddress(this);
            CorrectUriAddressByScenariosOnDifferentSides(ref targetUriAddress);

            if (targetUriAddress.IsNull())
                throw new InvalidOperationException($"Couldn't find any Registration for [{OwnerType.Name}] with DCC Scenario [{RegistrationInfo.ScenarioKey}] ");

            UriResultAddress = targetUriAddress;

            //init TSS for some service
            TssSerializer = TypeSetSerializer.AddOrUseExistTSS(OwnerShortKey);

            SerializeToStreamHandler = GetSerializeToStreamHandler();
            DeserializeFromStreamHandler = GetDeserializeFromStreamHandler();

            SerializeToStringHandler = GetSerializeToStringHandler();
            DeserializeFromStringHandler = GetDeserializeFromStringHandler();

            SerializeToByteArrayHandler = GetSerializeToByteArray();
            DeserializeFromByteArrayHandler = GetDeserializeFromByteArray();


            //Init Empty Dyagnostics Logger
            Logger = DCLogger.CreateEmptyLogger(OwnerShortKey);

        }

        void CorrectUriAddressByScenariosOnDifferentSides(ref string targetUriAddress)
        {
#if SERVER
            if (RegistrationInfo.ScenarioKey.ToLower().Contains("webapi") && targetUriAddress.StartsWith("~/"))
            {
                targetUriAddress = targetUriAddress.Replace("~/", "");
            }
#endif
        }

        #endregion -------------------------- Uri result Address-----------------------------------


        #region ---------------------- SERIALIZATION --------------------------

        /// <summary>
        /// TSS(TypeSetSerializer ). You should remember that it's [Set of Types-Domain model] should be initialized by Owner- Manager/Service Class.
        /// </summary>
        protected ITypeSetSerializer TssSerializer
        { get; set; }


        protected SD.SerializeToStream SerializeToStreamHandler = null;
        protected SD.DeserializeFromStream DeserializeFromStreamHandler = null;

        protected SD.SerializeToString SerializeToStringHandler = null;
        protected SD.DeserializeFromString DeserializeFromStringHandler = null;

        protected SD.SerializeToByteArray SerializeToByteArrayHandler = null;
        protected SD.DeserializeFromByteArray DeserializeFromByteArrayHandler = null;


        /// <summary>
        /// Here we declare that we use one of Text Serializations.
        /// </summary>
        public bool UseTextSerialization { get; private set; }

        /// <summary>
        /// Here we declare that we use Binary Serialization.
        /// </summary>
        public bool UseBinSerialization { get; private set; }


        #region---------------------------------GET/... SERIALIZE/DESERIALIZE TO/FROM STREAM ------------------------------

        /// <summary>
        ///   Handler of SerializeToStream.[TSS, XML,JSON,CSV ]
        /// </summary>
        protected SD.SerializeToStream GetSerializeToStreamHandler()
        {
            switch (RegistrationInfo.SerializationMode) //serializeMode
            {
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return TssSerializer.SerializeToStream;
                    }
#if !WP81
                case DCSerializationModeEn.MDCS_SSXML:
                    {
                        return  DDDD.Core.Serialization.XmlSerializer.SerializeToStream;
                    }
                case DCSerializationModeEn.MDCS_SSCSV:
                    {
                        return CsvSerializer.SerializeToStream;
                    }
                case DCSerializationModeEn.MDCS_SSJSON:
                    {
                        return JsonSerializer.SerializeToStream;
                    }
                case DCSerializationModeEn.SSJSON:
                    {
                        return JsonSerializer.SerializeToStream;
                    }
                case DCSerializationModeEn.SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.SerializeToStream;
                    }
                case DCSerializationModeEn.SSCSV:
                    {
                        return CsvSerializer.SerializeToStream;
                    }

#endif

                default:
                    {
                        return TssSerializer.SerializeToStream;
                    }
            }
        }


        /// <summary>
        ///  Handler of DeserializeFromStream.[TSS, XML,JSON,CSV ]
        /// </summary>       
        protected SD.DeserializeFromStream GetDeserializeFromStreamHandler()
        {
            switch (RegistrationInfo.SerializationMode) //serializeMode
            {
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return TssSerializer.DeserializeFromStream;
                    }
#if !WP81
                case DCSerializationModeEn.MDCS_SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.DeserializeFromStream;
                    }
                case DCSerializationModeEn.MDCS_SSCSV:
                    {
                        return CsvSerializer.DeserializeFromStream;
                    }
                case DCSerializationModeEn.MDCS_SSJSON:
                    {
                        return JsonSerializer.DeserializeFromStream;
                    }
                case DCSerializationModeEn.SSJSON:
                    {
                        return JsonSerializer.DeserializeFromStream;
                    }
                case DCSerializationModeEn.SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.DeserializeFromStream;
                    }
                case DCSerializationModeEn.SSCSV:
                    {
                        return CsvSerializer.DeserializeFromStream;
                    }

#endif

                default:
                    {
                        return TssSerializer.DeserializeFromStream;
                    }
            }
        }

        /// <summary>
        /// Serialize instance into Stream with current  SerializeToStreamHandler.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public async Task SerializeToStream(object instance, Stream inputStream)
        {
            try
            {
                SerializeToStreamHandler(instance, inputStream);
            }
            catch (Exception ex)
            {
            	//LOG - save message by Logging settings
            }             
        }


        /// <summary>
        /// Deserialize from inputStream as outType. 
        /// </summary>
        /// <param name="outType"></param>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public async Task<object> DeserializeFromStream(Type outType, Stream inputStream )
        {
            object result = null;
            try
            {
                result = DeserializeFromStreamHandler(outType, inputStream);
            }
            catch (Exception ex)
            {
                //LOG - save message by Logging settings
                //do not throw
            }
            return result;
        }



        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STREAM ------------------------------



        #region---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STRING------------------------------


        /// <summary>
        /// Handler of DeserializeFromStream.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public SD.SerializeToString GetSerializeToStringHandler()
        {

            switch (RegistrationInfo.SerializationMode) //serializeMode
            {
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return null;
                    }

#if !WP81


                case DCSerializationModeEn.MDCS_SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.SerializeToString;
                    }
                case DCSerializationModeEn.MDCS_SSCSV:
                    {
                        return CsvSerializer.SerializeToString;
                    }
                case DCSerializationModeEn.MDCS_SSJSON:
                    {
                        return JsonSerializer.SerializeToString;
                    }
                case DCSerializationModeEn.SSJSON:
                    {
                        return JsonSerializer.SerializeToString;
                    }
                case DCSerializationModeEn.SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.SerializeToString;
                    }
                case DCSerializationModeEn.SSCSV:
                    {
                        return CsvSerializer.SerializeToString;
                    }
                default:
                    {
                        return JsonSerializer.SerializeToString;
                    }
#elif WP81

                default:
                    {
                        return null;
                    }
#endif


            }

        }


        /// <summary>
        /// Handler of DeserializeFromStream.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <returns></returns>
        public SD.DeserializeFromString GetDeserializeFromStringHandler()
        {
            switch (RegistrationInfo.SerializationMode) //serializeMode
            {
                case DCSerializationModeEn.MDCS_TSS:
                    {
                        return null;
                    }
#if !WP81
                case DCSerializationModeEn.MDCS_SSJSON:
                    {
                        return JsonSerializer.DeserializeFromString;
                    }
                case DCSerializationModeEn.MDCS_SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.DeserializeFromString;
                    }
                case DCSerializationModeEn.MDCS_SSCSV:
                    {
                        return null;

                    }

                case DCSerializationModeEn.SSJSON:
                    {
                        return JsonSerializer.DeserializeFromString;
                    }
                case DCSerializationModeEn.SSXML:
                    {
                        return DDDD.Core.Serialization.XmlSerializer.DeserializeFromString;
                    }
                case DCSerializationModeEn.SSCSV:
                    {
                        return null;
                    }
                default:
                    {
                        return JsonSerializer.SerializeToString;
                    }
#elif WP81
                    default:
                    {
                        return null;
                    }
 

#endif


            }

        }


        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM STRING ------------------------------



        #region---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM BYTE ARRAY ------------------------------

        /// <summary>
        /// Handler of SerializeToByteArray.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <returns></returns>
        public SD.SerializeToByteArray GetSerializeToByteArray()
        {
            return (data) =>
            {
                using (var dataStream = new MemoryStream())
                {
                    SerializeToStreamHandler(data, dataStream);

                    return dataStream.ToArray();
                }

            };

        }

        /// <summary>
        /// Handler of  DeserializeFromByteArray.[TSS, XML,JSON,CSV ]
        /// </summary>
        /// <returns></returns>
        public SD.DeserializeFromByteArray GetDeserializeFromByteArray()
        {
            return (dataArray, tp) =>
            {
                using (var dataStream = new MemoryStream(dataArray))
                {
                    return DeserializeFromStreamHandler(tp, dataStream);
                }

            };
        }



        #endregion---------------------------------GET SERIALIZE/DESERIALIZE TO/FROM BYTE ARRAY ------------------------------


        #region ---------------------- HttpRequestMessage/ HttpResponse SERIALIZE/DESERIALIZE TO/FROM STREAM  --------------------------------


        const string application_json = @"application/json";
        const string application_octetStream = @"application/octet-stream";

        const string text_xml = @"text/xml";
        const string text_csv = @"text/csv";
        const string text_plain = @"text/plain";




#if SERVER && IIS

        static protected readonly MediaTypeHeaderValue ContentType_Json = new MediaTypeHeaderValue(application_json);
        static protected readonly MediaTypeHeaderValue ContentType_OctetStream = new MediaTypeHeaderValue(application_octetStream);
        static protected readonly MediaTypeHeaderValue ContentType_TextXml = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextCsv = new MediaTypeHeaderValue(text_xml);
        static protected readonly MediaTypeHeaderValue ContentType_TextPlain = new MediaTypeHeaderValue(text_xml);


        static protected readonly MediaTypeWithQualityHeaderValue JsonAccept = new MediaTypeWithQualityHeaderValue(application_json);
        static protected readonly MediaTypeWithQualityHeaderValue OctetAccept = new MediaTypeWithQualityHeaderValue(application_octetStream);


        /// <summary>
        /// Get ContentType Http Header value based on RegistrationInfo.SerializationMode.
        /// </summary>
        /// <returns></returns>
        public MediaTypeHeaderValue GetContentTypeHeaderValue()
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.MDCS_TSS:
                    { return ContentType_OctetStream; }
                case DCSerializationModeEn.MDCS_SSJSON:
                    { return ContentType_Json; }
                case DCSerializationModeEn.MDCS_SSXML:
                    { return ContentType_TextXml; }
                case DCSerializationModeEn.MDCS_SSCSV:
                    { return ContentType_TextCsv; }
                case DCSerializationModeEn.SSCSV:
                    { return ContentType_TextCsv; }
                case DCSerializationModeEn.SSJSV:
                    { return ContentType_TextPlain; }
                case DCSerializationModeEn.TSS:
                    { return ContentType_OctetStream; }
                case DCSerializationModeEn.SSXML:
                    { return ContentType_TextXml; }
                case DCSerializationModeEn.SSJSON:
                    { return ContentType_Json; }
                case DCSerializationModeEn.NSJSON:
                    { return ContentType_Json; }
                default:
                    { return ContentType_Json; }
            }
        }


        /// <summary>
        /// SERVER SIDE - DeserializeFromRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<object> DeserializeFromRequest(HttpRequestMessage request, Type dataType)
        {
            return await OperationInvoke.CallInMode(nameof(DCCUnit), nameof(DeserializeFromRequest),
               async () =>
               {
                   var contentStream = await request.Content.ReadAsStreamAsync();// as StreamContent;
                   return DeserializeFromStreamHandler(dataType, contentStream);
               });
        }


        /// <summary>
        /// SERVER SIDE -  SerializeToResponse
        /// </summary>
        /// <param name="response"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> SerializeToResponse(HttpResponseMessage response, object message)
        {
           return  OperationInvoke.CallInMode(nameof(DCCUnit), nameof(SerializeToResponse)
                ,  () =>
                {
                    var stream = new MemoryStream();
                    SerializeToStreamHandler(message, stream);
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentType = GetContentTypeHeaderValue();
                    return response;
                }
                );
        }


#elif CLIENT



        //Pack DCMessage    - To   SendToServer
        //Unpack DCMessage  - From ReceiveFromServer

        /// <summary>
        /// CLIENT SIDE - SerializeToRequest
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<HttpRequestMessage> SerializeToRequest(HttpRequestMessage request, object message)
        {
             return  OperationInvoke.CallInMode(nameof(DCCUnit), nameof(SerializeToRequest)
                    , () =>
                     {
                       var stream = new MemoryStream();
                       SerializeToStreamHandler(message, stream);
                       var streamContent = new StreamContent(stream);
                       
                       request.Content = streamContent;
                       return request;
                     });
             
        }


        /// <summary>
        /// CLIENT SIDE - DeserializeFromResponse
        /// </summary>
        /// <param name="response"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<object> DeserializeFromResponse(HttpResponseMessage response, Type dataType)
        {

            return await OperationInvoke.CallInMode( nameof(DCCUnit), nameof(DeserializeFromResponse),
                async () =>
                {
                    var contentStream = await response.Content.ReadAsStreamAsync();// as StreamContent;
                    return DeserializeFromStreamHandler(dataType, contentStream);
                });
            

        }

#endif


        #endregion ---------------------- HttpRequestMessage/ HttpResponse SERIALIZE/DESERIALIZE TO/FROM STREAM  --------------------------------


        #region ----------------------------WCF Known DCMessage ...  DataContracts ------------------------------

        static List<Type> KnownDataContracts
        { get; } = new List<Type>();

        /// <summary>
        /// We can Add new known Data Contract. All Known DataContracts we can get by   GetKnownDataContracts(). It also used in WCF service contracts - IDCService.
        /// </summary>
        /// <param name="knownContract"></param>
        public static void AddKnowDataContracts(params Type[] knownContracts)
        {
            foreach (var kContract in knownContracts)
            {
                if (KnownDataContracts.NotContains(kContract))
                {
                    KnownDataContracts.Add(kContract);
                }
            }
        }

        /// <summary>
        /// Globally known Data Contracts. It also used in WCF service contracts - IDCService.
        /// </summary>
        /// <returns></returns>
        public static List<Type> GetKnownDataContracts(ICustomAttributeProvider provider)
        {
            return KnownDataContracts;
        }

        #endregion ----------------------------WCF Known DCMessage ...  DataContracts ------------------------------


        #endregion ---------------------------- SERIALIZATION ---------------------------------


        #region --------------------------- CREATE/PACK/UNPACK  DCMESSAGE By DCUNIT SCENARIO ------------------------------


        /// <summary>
        /// If we are Scenario use Messages where we need to use Pack/Unpack logic.(it's WCF use scenarios- when we need it).
        /// </summary>
        public bool UsePackUnpackLogic
        { get; private set; }



        /// <summary>
        /// Create Empty Message. Decide what end DCmessage type we will use: DCMessage, DCMessage2, DCmessage3.
        /// </summary>
        /// <returns></returns>
        public IDCMessage CreateEmptyMessage()
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    { return DCMessage.Empty; }
                case DCSerializationModeEn.MDCS_TSS:
                    { return DCMessage.Empty; }
                case DCSerializationModeEn.NSJSON:
                    { return DCMessage2.Empty; }
#if !WP81
                case DCSerializationModeEn.MDCS_SSJSON:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.MDCS_SSXML:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.MDCS_SSCSV:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.SSXML:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.SSJSON:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.SSCSV:
                    { return DCMessage2.Empty; }
                case DCSerializationModeEn.SSJSV:
                    { return DCMessage2.Empty; }

#endif

                case DCSerializationModeEn.TSS:
                    { return DCMessage2.Empty; }
                default:
                    { return DCMessage.Empty; }
            }


        }

        /// <summary>
        /// Create IDCMessage based on the SerializationMode of current DCUnit. 
        /// </summary>
        /// <param name="shortKey"></param>
        /// <param name="currentCommandName"></param>
        /// <param name="actionParameters"></param>
        /// <returns></returns>
        public IDCMessage CreateMessage(string shortKey, string currentCommandName, params DCParameter[] actionParameters)
        {
            switch (RegistrationInfo.SerializationMode)
            {
                case DCSerializationModeEn.NotDefined:
                    { return DCMessage.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.TSS:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.MDCS_TSS:
                    { return DCMessage.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.NSJSON:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
#if !WP81
                case DCSerializationModeEn.MDCS_SSJSON:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.MDCS_SSXML:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.MDCS_SSCSV:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.SSXML:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.SSJSON:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.SSCSV:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }
                case DCSerializationModeEn.SSJSV:
                    { return DCMessage2.Create(shortKey, currentCommandName, actionParameters); }

#endif

                default:
                    { return DCMessage.Empty; }
            }
        }



        /// <summary>
        /// Unpack Message,but if it's needable. When we use WCF scenario(Not Rest)/MDxx_xxx serialization Modes we need in this operation.
        /// </summary>
        /// <param name="inputCommand"></param>
        public IDCMessage UnpackMessageIfNeeds(IDCMessage inputCommand)
        {
            try
            {
                if (UsePackUnpackLogic && inputCommand is DCMessage)
                {
                    DCMessage messageEnd = inputCommand as DCMessage;
                    messageEnd = (DCMessage)DeserializeFromByteArrayHandler(messageEnd.Body, typeof(DCMessage));

                    inputCommand = messageEnd;
                }
                return inputCommand;

            }
            catch (Exception ex)
            {
            	// TO DO LOG
            }
            return inputCommand;            

        }


        /// <summary>
        /// Pack Message,but if it's needable. When we use WCF scenario(Not Rest)/MDxx_xxx serialization Modes we need in this operation.
        /// </summary>
        /// <param name="inputCommand"></param>
        public IDCMessage PackMessageIfNeeds(IDCMessage inputCommand)
        {
            try
            {

                if (UsePackUnpackLogic && inputCommand is DCMessage)
                {
                    DCMessage messageEnd = inputCommand as DCMessage;
                    messageEnd.Body = SerializeToByteArrayHandler(messageEnd);
                    inputCommand = messageEnd;
                }
            }
            catch (Exception ex)
            {
                // TO DO LOG
            }

            return inputCommand;

        }


        public IDCMessage PackParametersIfNeeds(IDCMessage inputCommand)
        {
            var paramsToUnpack = inputCommand.GetAllParams().Where(prm => prm.UsePackUnpackLogic);

            foreach (var unpackParam in paramsToUnpack)
            {
                SerializeParameter(unpackParam);//.Serialize(this);
            }

            return inputCommand;
        }

        public IDCMessage UnpackParametersIfNeeds(IDCMessage inputCommand)
        {
            var paramsToUnpack = inputCommand.GetAllParams().Where(prm => prm.UsePackUnpackLogic);

            foreach (var unpackParam in paramsToUnpack)
            {
                DeserializeParameter(unpackParam);//  .Deserialize(this);                         
            }

            return inputCommand;

        }




        #endregion --------------------------- CREATE/PACK/UNPACK  DCMESSAGE By DCUNIT SCENARIO ------------------------------



        #region ---------------------------- SERIALIZE / PACK / UNPACK DCPARAMETERS --------------------------------- 

        /// <summary>
        /// Get CSharpTypeName string builder.
        /// </summary>
        /// <param name="targetValueType"></param>
        /// <returns></returns>
        public static string GetCSharpTypeName(Type targetValueType)
        {
            var targetTypeName = targetValueType.FullName;

            var targetAssemblyName = targetValueType.Assembly.GetAssemblyNameEx();

            return targetTypeName + "," + targetAssemblyName;
        }

#if CLIENT



         /// <summary>
        /// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communication unit with prepered serialization handler, based on it's SerializationMode</param>

        public void SerializeParameter(DCParameter parameter)
        {
            if (parameter.Value.IsNull()) return;

            var valueType = parameter.Value.GetType();
            parameter.CSharpTypeFullName = GetCSharpTypeName(valueType); //for Type Script Typed data communication.


            if (UseBinSerialization)
            {
                try
                {
                    parameter.ValueBody = SerializeToByteArrayHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            else if ( UseTextSerialization)
            {
                try
                {
                    parameter.ValueText = SerializeToStringHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true; 
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            parameter.Value = null; //nullify original data if serialization was successfull or falted
        }





        /// <summary>
        /// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
        public void DeserializeParameter(DCParameter  parameter)
        {
            var targetType = Type.GetType(parameter.CSharpTypeFullName);
            if (targetType.IsNull()) throw new InvalidOperationException("Target Type was not found by Name string [{0}]".Fmt(parameter.CSharpTypeFullName));

            if (UseBinSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueBody.IsNull() ||

                //do not deserialize if it's byte[]
                if (targetType == typeof(byte[])) { parameter.Value = parameter.ValueBody; return; }

                try
                {
                    parameter.Value =  DeserializeFromByteArrayHandler(parameter.ValueBody, targetType);
                    parameter.ValueBody = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;

                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter ),
                                    "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueBody -[{2}] | ExceptionMessage-[{3}]"
                                                .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueBody.S(), desExc.Message))
                               );
                }

            }
            else if ( UseTextSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueText.IsNull() ||

                //do not deserialize if it's string
                if (targetType == typeof(string)) { parameter.Value = parameter.ValueText; return; }


                try
                {
                    parameter.Value =  DeserializeFromStringHandler(parameter.ValueText, targetType);
                    parameter.ValueText = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;
                    Logger.ErrorDbg(
                      RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter),
                                  "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueText -[{2}] | ExceptionMessage-[{3}]"
                                              .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueText, desExc.Message))
                             );
                }

            }


        }




#elif SERVER

        /// <summary>
        /// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communication unit with prepered serialization handler, based on it's SerializationMode</param>

        public void SerializeParameter(DCParameter parameter)
        {
            if (parameter.Value.IsNull()) return;

            var valueType = parameter.Value.GetType();
            parameter.CSharpTypeFullName = GetCSharpTypeName(valueType); //for Type Script Typed data communication.


            if (UseBinSerialization)
            {
                if (valueType == typeof(byte[])) { parameter.ValueBody = (byte[])parameter.Value; return; }
                try
                {    
                    parameter.ValueBody = SerializeToByteArrayHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            else if ( UseTextSerialization)
            {
                if (valueType == typeof(string)) { parameter.ValueText = (string)parameter.Value; return; }

                try
                {
                     parameter.ValueText = SerializeToStringHandler(parameter.Value);
                }
                catch (Exception serExc)
                {
                    parameter.HasServerSerializationException = true;
                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(SerializeParameter),
                                        " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueInString-[{2}] | ExceptionMessage-[{3}]"
                                                    .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.Value.S(), serExc.Message))
                                 );
                }

            }
            parameter.Value = null; //nullify original data if serialization was successfull or falted
        }





        /// <summary>
        /// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
        /// </summary>
        /// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
        public void DeserializeParameter(DCParameter  parameter)
        {
            var targetType = Type.GetType(parameter.CSharpTypeFullName);
            if (targetType.IsNull()) throw new InvalidOperationException("Target Type was not found by Name string [{0}]".Fmt(parameter.CSharpTypeFullName));

            if (UseBinSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueBody.IsNull() ||

                //do not deserialize if it's byte[]
                if (targetType == typeof(byte[])) { parameter.Value = parameter.ValueBody; return; }

                try
                {
                    parameter.Value =  DeserializeFromByteArrayHandler(parameter.ValueBody, targetType);
                    parameter.ValueBody = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;

                    Logger.ErrorDbg(
                        RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter ),
                                    "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueBody -[{2}] | ExceptionMessage-[{3}]"
                                                .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueBody.S(), desExc.Message))
                               );
                }

            }
            else if ( UseTextSerialization)
            {
                if (parameter.HasClientSerializationException) return; // ValueText.IsNull() ||

                //do not deserialize if it's string
                if (targetType == typeof(string)) { parameter.Value = parameter.ValueText; return; }


                try
                {
                    parameter.Value =  DeserializeFromStringHandler(parameter.ValueText, targetType);
                    parameter.ValueText = null; //nullify serialization Text value
                }
                catch (Exception desExc)
                {
                    parameter.HasDeserializeException = true;
                    parameter.DeserializeExceptionMessage = desExc.Message;
                    Logger.ErrorDbg(
                      RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(DeserializeParameter),
                                  "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueText -[{2}] | ExceptionMessage-[{3}]"
                                              .Fmt(parameter.Key, parameter.CSharpTypeFullName, parameter.ValueText, desExc.Message))
                             );
                }

            }


        }



#endif



        #endregion ---------------------------- SERIALIZE / PACK / UNPACK DCPARAMETERS --------------------------------- 





        #region -------------------------- ADD NEW GET DCCUNIT, GET ADDRESS  ------------------------------


#if CLIENT

        /// <summary>
        /// Create  DCCUnit if instance doesn't exist in internal cache dictionary. Add Register DC Communication Unit - [in CommunicationUnitType dictionary- register  Communication Unit, like-[for some OwnerType,and Unit UriResultAddress] ].
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="registeringInfo"></param>
        /// <returns></returns>
        public static DCCUnit TryGetOrAddNewDCCUnit(Type owner, DCCUnitOrderAttribute registeringInfo)
        {
            try
            {
                //add if not exist - scenario slot
                if (DCCUnits.NotContainsKey(registeringInfo.ScenarioKey))
                    DCCUnits.Add(registeringInfo.ScenarioKey, new Dictionary<Type, DCCUnit>() );

                // check if we already have item in dcCUnit.CommunicationUnitKey
                if (DCCUnits[registeringInfo.ScenarioKey].ContainsKey(owner))
                    return DCCUnits[registeringInfo.ScenarioKey][owner]; // already exist

                // Create DCCUnit   -set owner and uriResultAddress
                var dccUnit = new DCCUnit(owner, registeringInfo);

                DCCUnits[registeringInfo.ScenarioKey].Add(owner, dccUnit);

                return DCCUnits[registeringInfo.ScenarioKey][owner];
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }




#elif SERVER && IIS


        /// <summary>
        /// Create  DCCUnit if instance doesn't exist in internal cache dictionary. Add Register DC Communication Unit - [in CommunicationUnitType dictionary- register  Communication Unit, like-[for some OwnerType,and Unit UriResultAddress] ].
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="registeringInfo"></param>
        /// <returns></returns>
        public static DCCUnit AddOrUseExistDCCUnit(Type owner, DCCUnitOrderAttribute registeringInfo)
        {
            try
            {   // check if we already have item in dcCUnit.CommunicationUnitKey
                if (DCCUnits[registeringInfo.ScenarioKey].ContainsKey(owner))
                    return DCCUnits[registeringInfo.ScenarioKey][owner]; // already exist

                // Create DCCUnit   -set owner and uriResultAddress
                var dccUnit = new DCCUnit(owner, registeringInfo);

                var configWasChanged = RegisterFactoryItemConfigByScenarioFunc[registeringInfo.ScenarioKey](dccUnit); //
                                                                                                                      //change webConfig if need                


                DCCUnits[registeringInfo.ScenarioKey].Add(owner, dccUnit);

                return DCCUnits[registeringInfo.ScenarioKey][owner];
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



#endif


        /// <summary>
        /// Get Registered DC Communication Unit, by some string unit type, and owner Type
        /// </summary>
        /// <param name="ownerToCheck"></param>
        /// <param name="dccUnitScenarioKey"></param>
        /// <returns></returns>
        public static DCCUnit GetDCCUnit(Type ownerToCheck, string dccUnitScenarioKey)
        {
            if (DCCUnits[dccUnitScenarioKey].NotContainsKey(ownerToCheck)) return null;
            return DCCUnits[dccUnitScenarioKey][ownerToCheck];
        }


        /// <summary>
        /// Get Registered DC Communication Unit, by some string unit type, and owner Type.
        /// </summary>
        /// <param name="dccUnitScenarioKey"></param>
        /// <param name="ownerType"></param>
        /// <returns></returns>

        public static string GetRegisteredOwnerUnitAddress(Type ownerType, string dccUnitScenarioKey)
        {
            if (DCCUnits.NotContainsKey(dccUnitScenarioKey)) return null;
            if (DCCUnits[dccUnitScenarioKey].NotContainsKey(ownerType)) return null;

            return DCCUnits[dccUnitScenarioKey][ownerType].UriResultAddress;
        }


        #endregion -------------------------- ADD NEW GET DCCUNIT, GET ADDRESS  ------------------------------



        #region ------------------------------- DIAGNOSTICS/LOGGING ------------------------------------

        /// <summary>
        /// Logger for Service Diagnostics.
        /// </summary>
        public DCLogger Logger
        { get; private set; }

        

        /// <summary>
        /// Authentication Encoding Fnc from DCManager - to encode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecureEncodeDataFunc { get; set; }
        /// <summary>
        /// Authentication Decoding Fnc from DCManager - to decode user data : UserName/password/Role/RoleGroup
        /// </summary>
        public Func<object, string> SecureDecodeDataFunc { get; set; }
       






        #endregion ------------------------------- LOGGING ------------------------------------







    }
}



#region ---------------------------------- GARBAGE --------------------------------------

///// <summary>
///// Logging output fields
///// </summary>
//List<string> LogConextProperties
//{ get; } = new List<string>();


//string BuildLogContextMessage(IDCMessage inputMessage, Exception packException)
//{
//    var targetTemplatedMessage = "";
//    // User - simply integrated authentication  
//    var userParam = inputMessage.GetParam("User");
//    if (userParam.IsNotNull()) targetTemplatedMessage +=  ("User : " + userParam.Value.S());

//    // Manager            
//    targetTemplatedMessage += ("TargetDCManager : " + inputMessage.TargetDCManager);

//    // Command  
//    targetTemplatedMessage += ("Command : " + inputMessage.Command);


//    // Place- Unpack /Pack /JustOperation
//    var placeParam = inputMessage.GetParam("Place");
//    if (placeParam.IsNotNull()) targetTemplatedMessage += ("Place : " + placeParam.Value.S());

//    // for other context properties



//    return "";
//}

///// <summary>
///// Log message Template for operation failed on Client-side. 
///// </summary>
//public string LogMessageTemplateOnClient
//{ get; private set; }


///// <summary>
///// Log message Template for operation failed on Server-side.
///// </summary>
//public string LogMessageTemplateOnServer
//{ get; private set; }

/// <summary>
/// Log message template fields. 
/// </summary>
/// <param name="logMessageTemplate"></param>
//public void SetLogMessageFields(params string[] logMessageFields)
//{

//}




/// <summary>
/// Serialize Parameter. When parameter value will be serialized to ValueBin , Value will be nullify.
/// </summary>
/// <param name="communicationUnit">communication unit with prepered serialization handler, based on it's SerializationMode</param>

//public void Serialize(DCCUnit communicationUnit)
//{
//    if (Value.IsNull()) return;

//    var valueType = Value.GetType();
//    CSharpTypeFullName = GetCSharpTypeName(valueType); //for Type Script Typed data communication.


//    if (communicationUnit.UseBinSerialization)
//    {
//        try
//        {  ValueBody = communicationUnit.SerializeToByteArrayHandler(Value);
//        }
//        catch (Exception serExc)
//        {
//            HasClientSerializationException = true;
//            Log.ErrorDbg(
//               RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(Serialize),
//                               " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | Value -[{2}] | ExceptionMessage-[{3}]"
//                                           .Fmt(Key, CSharpTypeFullName, Value.S(), serExc.Message))
//                        );
//        }
//    }
//    else if (communicationUnit.UseTextSerialization)
//    {
//        try
//        {
//            ValueText = communicationUnit.SerializeToStringHandler(Value);
//        }
//        catch (Exception serExc)
//        {
//            HasClientSerializationException = true;                    
//            Log.ErrorDbg(
//            RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(Serialize),
//                            " Serialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | Value -[{2}] | ExceptionMessage-[{3}]"
//                                        .Fmt(Key, CSharpTypeFullName, Value.S(), serExc.Message))
//                        );
//        }               
//    }

//    Value = null; //nullify original data value
//}


/// <summary>
/// Deserialize Parameter.  When parameter value will be deserialized to Value , ValueText will be nullify.
/// </summary>
/// <param name="communicationUnit">communicationUnit has Serialize/Deserialize handlers basesd on RegistrationInfo. </param>
//public void Deserialize(DCCUnit communicationUnit)
//{
//    var targetType = Type.GetType(CSharpTypeFullName);
//    if (targetType.IsNull()) throw new InvalidOperationException("Target Type was not found by Name string [{0}]".Fmt(CSharpTypeFullName));

//    if (communicationUnit.UseBinSerialization)
//    {
//        if ( HasServerSerializationException) return;// ValueBody.IsNull() ||

//        //do not deserialize if it's byte[]
//        if (targetType == typeof(byte[])) { Value = ValueBody; return; }

//        try
//        {
//            Value = communicationUnit.DeserializeFromByteArrayHandler(ValueBody, targetType);
//            ValueBody = null; //nullify serialization Text value
//        }
//        catch (Exception desExc)
//        {
//            HasDeserializeException = true;
//            DeserializeExceptionMessage = desExc.Message;
//            Log.ErrorDbg(
//              RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(Deserialize),
//                              "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueBody -[{2}] | ExceptionMessage-[{3}]"
//                                          .Fmt(Key, CSharpTypeFullName, ValueBody.S(), desExc.Message))
//                       );

//        }
//    }
//    else if (communicationUnit.UseTextSerialization)
//    {
//        if ( HasServerSerializationException) return;  //ValueText.IsNull() ||

//        //do not deserialize if it's string
//        if (targetType == typeof(string)) { Value = ValueText; return; }                

//        try
//        {   Value = communicationUnit.DeserializeFromStringHandler(ValueText, targetType);
//            ValueText = null; //nullify serialization Text value
//        }
//        catch (Exception desExc)
//        {
//            HasDeserializeException = true;
//            DeserializeExceptionMessage = desExc.Message;
//            Log.ErrorDbg(
//             RCX.ERR_InvalidOperation.Fmt(nameof(DCParameter), nameof(Deserialize),
//                             "Deserialization error with ParamKey-[{0}] | CSharpTypeFullname-[{1}] | ValueText -[{2}] | ExceptionMessage-[{3}]"
//                                         .Fmt(Key, CSharpTypeFullName, ValueText , desExc.Message))
//                      );
//        } 
//    }


//}




/// <summary>
/// Owner Key - OwnerType.FullName - TargetCommandManager value equals
/// </summary>
//public string OwnerKey
//{
//    get { return OwnerType.FullName; }
//}



//throw new NotSupportedException(RCX.ERR_NotSupportedOption.Fmt( nameof(DCCUnit),nameof(GetDeserializeFromStringHandler), 
//    " Try to choose USE SerializationMode.MDCS_TSS: -  TSS doesn't support DeserializeFromString")  );


#region --------------------------- OPERATION CODE BLOCK --------------------------------

///// <summary>
///// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
///// Here className is [nameof(DCCUnit)].
///// </summary>
///// <param name="operationMethodName"></param>
///// <param name="operationAction"></param>
//static void OperationCodeBlock(string operationMethodName, Action operationAction)
//{

//#if DEBUG
//    try
//    {
//        operationAction();
//    }
//    catch (Exception exc)
//    {
//        throw new InvalidOperationException(" ERROR in {0}.{1}(): message - [{2}]"
//                  .Fmt(nameof(DCCUnit), operationMethodName, exc.Message)
//                  );
//    }
//#else
//            operationAction();
//#endif

//}


///// <summary>
///// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
///// Here className is [nameof(DCCUnit )].
///// </summary>
///// <typeparam name="TRes"></typeparam>
///// <param name="operationMethodName"></param>
///// <param name="operationFunc"></param>
///// <returns></returns>
//static TRes OperationCodeBlock<TRes>(string operationMethodName, Func<TRes> operationFunc)
//{

//#if DEBUG
//    try
//    {
//        return operationFunc();
//    }
//    catch (Exception exc)
//    {
//        throw new InvalidOperationException(RCX.ERR_InvalidOperation
//                  .Fmt(nameof(DCCUnit), operationMethodName, exc.Message)
//                  );
//    }
//#else
//                return operationFunc();
//#endif

//}

#endregion --------------------------- OPERATION CODE BLOCK --------------------------------



//public SD.PackMessage PackMessageHandler = null;
//public SD.UnpackMessage UnpackMessageHandler = null;

//public SD.PackMessage2 PackMessageHandler2 = null;
//public SD.UnpackMessage2 UnpackMessageHandler2 = null;

#region -----------------------------GET PACK DCMESSAGE ---------------------------------


/// <summary>
/// Packing  DCMessage smartly into Body : DC(DataContarctSerializer) + one of serializers[TSS, XML,JSON,CSV,JSV]
/// </summary>
/// <returns></returns>
//        protected virtual SD.PackMessage GetPackMessageHandler()
//        {            
//            switch (RegistrationInfo.SerializationMode)
//            {
//                    case DCSerializationModeEn.MDCS_TSS:
//                    {
//                        return (message) =>
//                        {                           
//                            message.Body = TssSerializer.Serialize(message);
//                        };    
//                    }


//#if !WP81  // all other clients and server  

//                    case DCSerializationModeEn.MDCS_SSJSON:
//                    {
//                        return (message) =>
//                        {
//                            using (var streamXml = new MemoryStream())
//                            {
//                                JsonSerializer.SerializeToStream(message, streamXml);
//                                message.Body = streamXml.ToArray();
//                            }
//                        };
//                    }

//                    case DCSerializationModeEn.MDCS_SSXML:
//                    {
//                        return (message) =>
//                        {
//                            using (var streamXml = new MemoryStream())
//                            {
//                                XmlSerializer.SerializeToStream(message, streamXml);
//                                message.Body = streamXml.ToArray();
//                            }
//                        };
//                        //   return PackMessage_WithXML;
//                    }


//                   case DCSerializationModeEn.MDCS_SSCSV:
//                   {
//                        return (message) =>
//                        {
//                            using (var streamXml = new MemoryStream())
//                            {
//                              CsvSerializer.SerializeToStream(message, streamXml);
//                              message.Body = streamXml.ToArray();
//                            }
//                        };                          
//                   }                 
//#endif

//                default:
//                   {
//                        return (message) =>
//                        {
//                            message.Body = TssSerializer.Serialize(message);
//                        };
//                   }
//            }

//        }


#endregion --------------------------------GET PACK DCMESSAGE  -------------------------------


#region --------------------------------GET UNPACK DCMESSAGE  -------------------------------

//        /// <summary>
//        /// Unpacking DCMessage smartly-from [Body] by one of the serializers[TSS, XML,JSON,CSV,JSV]
//        /// </summary>      
//        protected virtual SD.UnpackMessage  GetUnpackMessageHandler()
//        {

//            switch (RegistrationInfo.SerializationMode)
//            {
//                case DCSerializationModeEn.MDCS_TSS:
//                    {
//                        return UnpackMessage_WithTSS;
//                    }
//#if !WP81
//                case DCSerializationModeEn.MDCS_SSXML:
//                    {
//                        return UnpackMessage_WithXML;
//                    }
//                case DCSerializationModeEn.MDCS_SSCSV:
//                    {
//                        return UnpackMessage_WithCSV;
//                    }
//                case DCSerializationModeEn.MDCS_SSJSON:
//                    {
//                        return UnpackMessage_WithJSON;
//                    }
//#endif
//                default:
//                    {
//                        return UnpackMessage_WithTSS;
//                    }
//            }
//        }

//        protected virtual void UnpackMessage_WithTSS(ref DCMessage message)
//        {            
//            message = TssSerializer.Deserialize<DCMessage>(message.Body);
//        }


//#if !WP81 // ServiceStack doesn't support WP81 .NET Port 
//        protected virtual void UnpackMessage_WithXML(ref DCMessage message)
//        {
//            using (var streamXml = new MemoryStream(message.Body))
//            {
//                message = XmlSerializer.DeserializeFromStream<DCMessage>(streamXml);
//            }
//        }

//        protected virtual void UnpackMessage_WithCSV(ref DCMessage message)
//        {
//            using (var streamXml = new MemoryStream(message.Body))
//            {
//                message = CsvSerializer.DeserializeFromStream<DCMessage>(streamXml);
//            }
//        }

//        protected virtual void UnpackMessage_WithJSON(ref DCMessage message)
//        {
//            using (var streamXml = new MemoryStream(message.Body))
//            {
//                message = JsonSerializer. DeserializeFromStream<DCMessage>(streamXml);
//            }
//        }

//#endif


#endregion --------------------------------GET UNPACK DCMESSAGE -------------------------------




//private static void InitializeStatic()
//{
//    JsConfig.DateHandler = DateHandler.ISO8601;

//    //#if SERVER
//    //            var enumValues = typeof(string).GetEnumValues();
//    //#elif CLIENT && SL5

//    //var enumValues = typeof(string).GetEnumValues<string>();
//    //#endif


//    //initializing registration dictionaries for each of Scenario
//    //            foreach (string unitKey in enumValues)
//    //            {
//    //                DCCUnits.Add(unitKey, new Dictionary<Type, DCCUnit>());

//    //#if SERVER && IIS
//    //                var registerFunc = GetUnitConfigRegisteringFuncByScenario(unitKey);
//    //                if (registerFunc.IsNotNull())
//    //                {
//    //                    RegisterInConfigDCCUnitFunc.Add(unitKey, registerFunc);
//    //                }
//    //#endif

//    //            }
//}




//static Func<DCCUnit,bool> GetUnitConfigRegisteringFuncByScenario(string dcCommunicationType)
//{
//  var registerInfo  =   typeof(string).GetField(dcCommunicationType.S())
//                       .GetFieldAttribute<DCCUnitServerConfigActionAttribute>().FirstOrDefault();
//       if (registerInfo.IsNull()) return null;

// var delegateFunc = (Func<DCCUnit, bool>)Delegate.CreateDelegate(DCCommunicationUnitSelectorType,
//           registerInfo.RegisterActorType.GetMethod(registerInfo.RegistringActionName, BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic));

//    return delegateFunc;   
//}


//#region ----------------------- SERIALIZATION -------------------------------------


///// <summary>
///// Server side Service TSS Serializer instance.
///// </summary>
//public LazySlim<ITypeSetSerializer> LA_Serializer
//{ get; protected set; }




///// <summary>
///// Unpacking inputCommand 
///// <para/> If inputMessage.Body is null, then more probably we use TSS EndpointBehaviorExtension for service endpoint configuration - that's incorrect use case.
///// </summary>
///// <param name="inputMessage"></param>
//protected void UnpackMessageWithTSS(ref DCMessage inputMessage)
//{
//    try
//    {//Body can be null if we use TSS EndpointBehaviorExtension - incorrect case
//        inputMessage = LA_Serializer.Value.Deserialize<DCMessage>(inputMessage.Body);
//    }
//    catch (Exception deserializeExc)
//    {
//        LogPackUnpackFaultError(inputMessage, deserializeExc, true); //true - Unpacking Excepton                
//    }
//}


///// <summary>
///// Packing command message before sent it further or back to client.
///// </summary>
///// <param name="outputMessage"></param>
//protected void PackMessageWithTSS(ref DCMessage outputMessage)
//{
//    try
//    {
//        //set diagnostics  time [out] info
//        outputMessage.SetServerSendToClientTime(DateTime.Now);
//        outputMessage.Body = LA_Serializer.Value.Serialize(outputMessage);

//        //no need to clear other properties - they aren't  serialize members -only Body.

//    }
//    catch (Exception serializeExc)
//    {
//        LogPackUnpackFaultError(outputMessage, serializeExc, false, true); //true - Packing Exception                
//    }
//}


//#endregion ----------------------- SERIALIZATION -------------------------------------



//static readonly List<string> _Scenarios = new List<string>();

///// <summary>
/////Global List of all Enabled-Registered DCCommunication Scenarios[technology+configuration]. Only Keys.
///// By default all enabled Scenario Keys declared in DCCScenarios class - it' constant fields.
///// </summary>
//public static List<string> Scenarios
//{
//    get
//    {
//        return new List<string>(_Scenarios);//to not enable list items modification
//    }
//}
#endregion ---------------------------------- GARBAGE --------------------------------------