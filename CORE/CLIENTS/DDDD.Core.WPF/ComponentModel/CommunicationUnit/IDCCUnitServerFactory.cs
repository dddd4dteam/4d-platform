﻿#if SERVER  && IIS


using DDDD.Core.Patterns;
using System;

namespace DDDD.Core.ComponentModel
{

    public interface IDCCUnitServerFactory<TService> : IDCCUnitServerFactory
    {

        /// <summary>
        /// Add or use Exist Server side comunication unit instance -WCF service, WebApi/MVC controller.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        TService AddOrUseExistServiceT(DCCUnit communicationUnit);
    }


    /// <summary>
    /// It's meta interface that help us to declare association of that type with one of the DCCommunication Unit type. 
    /// DCCUnit is standart App Component . 
    /// </summary>
    public interface IDCCUnitServerFactory : IFactory
    {
        string ScenarioKey { get; }

        /// <summary>
        /// Add or use Exist Server side comunication unit instance -WCF service, WebApi/MVC controller.
        /// </summary>
        /// <param name="communicationUnit"></param>
        /// <returns></returns>
        object AddOrUseExistService(DCCUnit communicationUnit);


    }

}



#endif
