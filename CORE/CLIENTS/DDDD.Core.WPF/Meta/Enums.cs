﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Meta
{
    // 
    // 
    // ENTITY FRAMEWORK MODELING DEIGNER/RUNTIME ANALYZING METADATA EXETNSIONS.
    // 
    //

    #region ----------------------------------- SERVICE DOMAIN LEVEL (DESIGN LEVEL IN EFDESIGNER) -------------------------------



    /// <summary>
    /// 
    /// </summary>
    public enum ServiceDataModelStructureEn
    {
        NotDefined
      ,
        CustomDataModelStructure
      ,
        StarDataModelStructure
      ,
        SnowFlakeDataModelStructure
      ,
        HierarchyDataModelStructure
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StoreEn
    {
        SQLServer
      ,
        SQLCE
      ,
        MySQL
      ,
        PostgreSQL
      ,
        MongoDB
      ,
        Cassandra
      ,
        CouchDB
      ,
        Redis
      ,
        SterlingDB
      ,
        NinjaDB
      ,
        RaptorDB
      ,
        RavenDB
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ServiceFilesOutputEn
    {
        AllClassesInSingleFile
      ,
        EachClassInSeparateFile
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MappingUseServiceLevelEn
    {
        NotDefined
      ,
        Enable
      ,
        NotEnabled
    }


    
    /// <summary>
    /// Generate Item: Localization xml- File Name Template 
    /// </summary>
    public enum GenItem_Localization_FileNameEn
    {
        NotDefined
      ,
        Service_Localization_culture_resx
      ,
        Service_Localization_culture_resw
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CommunicationServiceEn
    {
        WCFRestJsonService
      ,
        WCFTSSRestService
      ,
        HttpHandler
      ,
        WebApiJsonController
      ,
        WebApiTSSController
      ,
        MVCJsonController
      ,
        MVCTSSController
    }



    #endregion ----------------------------------- SERVICE DOMAIN LEVEL (DESIGN LEVEL IN EFDESIGNER) ----------------------------


    #region ----------------------------------- CLASS LEVEL( ENTITY LEVEL IN EFDESIGNER) --------------------------------------

    /// <summary>
    /// 
    /// </summary>
    public enum StoreObjectTypeEn 
    {
        Table
      ,
        View
      ,
        Collection
      ,
        Object
      ,
        Document
      ,
        Graph

    }

    /// <summary>
    /// 
    /// </summary>
    public enum DataKindEn
    {
        FactTable
      ,
        FactDocument
      ,
        FactCollection
      ,
        Register
      ,
        NavigationView
      ,
        RegisterView
      ,
        ImageTable
      ,
        ImageCollection
      ,
        DataStructure
      ,
        PatternComponent
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EntityInStoreNamingRuleEn
    {
        PlainEntityName
      ,
        Prefix_Entity_Suffix
      ,
        CstPrfx_SId_Entity_DataKind
      ,
        StObjTp_SId_Entity_DataKind
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StarModelParticipantEn
    {
        Target
      ,
        Associated
      ,
        Register
    }

    /// <summary>
    /// 
    /// </summary>
    public enum HierarchyModelParticipantEn
    {
        HierarchyEntity
      ,
        ItemEntity
      ,
        ItemTypeEntity
      ,
        AncestorsView
      ,
        DescndantsView
      ,
        ItemsOnLevel
    }

    /// <summary>
    /// 
    /// </summary>
    public enum MappingUseEntityLevelEn
    {
        NotDefined
      ,
        DefinedOnServiceLevel
      ,
        IgnoreForEntity
    }

    /// <summary>
    /// 
    /// </summary>
    public enum EntityClassAccessorEn
    {
        NotDefined
      ,
        Default
      ,
        Public
      ,
        Internal
    }

    /// <summary>
    /// 
    /// </summary>
    public enum CachingEn
    {
        NotDefined
      ,
        Server_Global
      ,
        Client_Global
      ,
        Client_Page
      ,
        Server_Global_Client_Page
      ,
        Server_Global_Client_Global
    }

    /// <summary>
    /// 
    /// </summary>
    public enum TranactionManagementEn
    {
        NotDefined
      ,
        TransactionScope
      ,
        EnterpriseServices
      ,
        Manual
    }



    
    /// <summary>
    /// Generate Item: DAL Class- File Name Template 
    /// </summary>
    public enum GenItem_DalClass_FileNameEn
    {
        UseContextFileForAllDALClasses
      ,
        Entity_cs
      ,
        ServiceKey_Entity_cs
      ,
        ServiceKey_Entity_Dal_cs
    }


  
    /// <summary>
    /// Generate Item: MVC Model - File Name Template
    /// </summary>
    public enum GenItem_MVCModel_FileNameEn
    {
        Entity_MVCModel_cs
      ,
        ServiceKey_Entity_MVCModel_cs      
    }


    
    /// <summary>
    /// Generate Item: Item Edit View - File Name Template 
    /// </summary>
    public enum GenItem_ItemEditView_FileNameEn
    {
        Entity_ItemEditView_cs
      ,
        ServiceKey_Entity_ItemEditView_cs
      
    }

    /// <summary>
    /// 
    /// </summary>
    public enum ItemEditViewTypeEn
    {
        XamlEntityEditView
      ,
        HtmlEntityEditView
    }


    /// <summary>
    /// Generate Item: Collection View - File Name Template 
    /// </summary>
    public enum GenItem_CollectionView_FileNameEn
    {
        Entity_CollectiontView_cs
      ,
        ServiceKey_Entity_CollectiontView_cs     
      
    }


    /// <summary>
    /// 
    /// </summary>
    public enum CollectionViewTemplateEn
    {
        XamlEntityCollectionView
      ,
        HtmlEntityCollectionView
    }


    #endregion ----------------------------------- CLASS LEVEL( ENTITY LEVEL IN EFDESIGNER) -----------------------------------


    #region ----------------------------------- PROPERTY LEVEL(PROPERTY IN EFDESIGNER) ------------------------------------------
    
    /// <summary>
    /// 
    /// </summary>
    public enum PropertyAccessorEn
    {
        None
      ,
        Default
      ,
        Public
      ,
        Protected
      ,
        Internal
      ,
        ProtectedInternal
    }
    
    /// <summary>
    /// 
    /// </summary>
    public enum PropertySetterAccessorEn
    {
        None
      ,
        Default
      ,
        Public
      ,
        Protected
      ,
        Internal
      ,
        ProtectedInternal
    }

    /// <summary>
    /// 
    /// </summary>
    public enum PropertyKindEn //(BOPropertyRoleEn)
    {
        NotDefined
      ,
        PKeyField
      ,
        FKeyField
      ,
        VOFKeyField
      ,
        DataField
      ,
        FacadeSQLDataField
      ,
        FacadeCodeDataField
    }


    #endregion ----------------------------------- PROPERTY LEVEL(PROPERTY IN EFDESIGNER) ---------------------------------------



}
