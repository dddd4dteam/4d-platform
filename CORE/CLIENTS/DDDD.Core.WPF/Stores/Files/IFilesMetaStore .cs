﻿
using System;

using DDDD.Core.Stores.Model;

namespace DDDD.Core.Stores.Files
{
    /// <summary>
    /// Interface to Store where [content element] associated with [Files Node], will be stored/getted...
    /// </summary>
    public interface IFilesMetaStore 
    {

        /// <summary>
        /// Host Store of Node Elements.  
        /// </summary>
        string HostElementsStore
        { get; }

        /// <summary>
        /// Get IContentElement by ID
        /// </summary>
        IContentElement GetElement(Guid elementID);


        /// <summary>
        /// Name-string / Description- string / ParentID - Guid/ IsCategory - bool
        /// </summary>
        Guid AddElement(string Name, string Decription, Guid? parentID, bool isCategory);
        

        /// <summary>
        /// Update Element Action
        /// </summary>
        void UpdateElement(IContentElement elementToUpdate);


        /// <summary>
        /// Check that we already contain 
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>

        bool ContainsElement(string elementName, Guid? parentID);


        /// <summary>
        /// Get element by Name and ParentID
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="parentID"></param>
        /// <returns></returns>
        IContentElement GetElement(string elementName, Guid? parentID);
         

    }


}
