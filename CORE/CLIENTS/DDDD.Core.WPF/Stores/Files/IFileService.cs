﻿using System.Collections.Generic;
using DDDD.Core.Stores.Model;
using DDDD.Core.Stores.Replication;

namespace DDDD.Core.Stores.Files
{
    public interface IFileService
    {
        /// <summary>
        /// 
        /// </summary>
        IFilesMetaStore   HostStoreService { get; }


        /// <summary>
        /// 
        /// </summary>
        List<ReplicaTarget> ReplicationTargets { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ReplicaTarget GetBestReplica();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostStoreService"></param>
        void Init(IFilesMetaStore hostStoreService);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetElement"></param>
        /// <param name="filePath"></param>
        /// <param name="replicaIndex"></param>
        /// <param name="accessMethod"></param>
        void AddFile(IContentElement targetElement, string filePath, int replicaIndex = 1, AccessMethodEn accessMethod = AccessMethodEn.LocalFileSystem); 



        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceRootPath"></param>
        void ImportSynchronize(string sourceRootPath);





        /// <summary>
        /// Backup user files and their metabase.
        /// </summary>
        /// <param name="User"></param>
        void Backup(string User);

        /// <summary>
        /// Restore user files and their metabase
        /// </summary>
        /// <param name="userFilesBackup"></param>
        /// <param name="userFilesMetaDBBackup"></param>
        void Restore(string userFilesBackup, string userFilesMetaDBBackup);


    }
}