﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using DDDD.Core.Extensions;
using DDDD.Core.Stores.Replication;
using DDDD.Core.Diagnostics;
using DDDD.Core.Stores.RaptorDB;

namespace DDDD.Core.Stores.Model
{



    public class ContentElement :IContentElement//, IHierarchyElement, IDocumentEntity
    {

        #region ------------------------ CTOR ---------------------------

        public ContentElement()
        {
            ID = Guid.NewGuid();
            Tags = new List<string>();
            ContentStrings = new List<StringContentResource>();
            NodeReplics = new Dictionary<int, NodeReplicaInfo>();
            Settings = new Dictionary<string, string>();
        }

        #endregion ------------------------ CTOR ---------------------------


        #region --------------------- IHierarchyElement ------------------------
        public Guid ID { get; set; }


        public Guid? IDParent { get; set; }
        
        #endregion --------------------- IHierarchyElement ------------------------


        public bool IsCategory { get; set; }
 
        
        public string Name { get; set; }

        
        public string DisplayName { get; set; }


        public string Description { get; set; }


        public List<string> Tags { get; set; }


        public List<StringContentResource> ContentStrings { get; set; }

        public ModificationInfo Modifications { get; set; }


        #region --------------------- NODE STORING INFO  -----------------------------

        /// <summary>
        /// Node Replications info - where files that belongs to this element contain.  
        /// </summary> 
        public Dictionary<int, NodeReplicaInfo> NodeReplics
        { get; set; } 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="replicaIndex"></param>
        /// <param name="acessMethod"></param>
        /// <returns></returns>
        public bool IsContainsReplicaWithAddress(int replicaIndex, AccessMethodEn acessMethod)
        {
            if (NodeReplics.NotContainsKey(replicaIndex)) return false;
            if (acessMethod == AccessMethodEn.LocalFileSystem && !NodeReplics[replicaIndex].ContainsLocalAddress) return false;
            if (acessMethod == AccessMethodEn.NetworkAccess && !NodeReplics[replicaIndex].ContainsNetworkAddress) return false;
            if (acessMethod == AccessMethodEn.HttpAccess && !NodeReplics[replicaIndex].ContainsHttpAddress) return false;
            return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="replicaIndex"></param>
        /// <param name="accessmethod"></param>
        /// <returns></returns>
        public string GetReplicaAdress(int replicaIndex, AccessMethodEn accessmethod)
        {
            Validator.ATInvalidOperationDbg(NodeReplics.NotContainsKey(replicaIndex), nameof(ContentElement), nameof(GetReplicaAdress)
                , " ContentElement  doesn't contain NodeReplicaInfo with index [{0}]".Fmt(replicaIndex.S()));

            switch (accessmethod)
            {
                case AccessMethodEn.LocalFileSystem:
                    return  this.NodeReplics[replicaIndex].NodeStoreLocalAddress;
                case AccessMethodEn.NetworkAccess:
                    return this.NodeReplics[replicaIndex].NodeStoreNetworkAddress;
                case AccessMethodEn.HttpAccess:
                    return this.NodeReplics[replicaIndex].NodeStoreHttpAddress;
                default:
                    return this.NodeReplics[replicaIndex].NodeStoreLocalAddress;
            }

        }



        public void Update()
        {    
           RaptorDBService.Current.Store.Save(ID, this);            
        }
        #endregion --------------------- NODE STORING INFO  -----------------------------


        #region -------------------------- SETTINGS  ----------------------------------


        /// <summary>
        /// Settings of Content Node. Separate File settings.json.
        /// </summary>
        public Dictionary<string, string> Settings
        { get; set; } 

        #endregion -------------------------- SETTINGS   ---------------------------------- 


        #region ---------------------------- MAIL REQUESTS -------------------------------

        /// <summary>
        /// Count of Mail Requests - to know more about this item, order an exemplar...
        /// </summary>
        public int MailRequestsCount
        { get; set; }

        #endregion ---------------------------- MAIL REQUESTS ------------------------------- 


        //////////////////////////////////////////////////////////////////////////
        //////////////////////////////  MODELS    ////////////////////////////////
        //////////////////////////////////////////////////////////////////////////
        

        #region ------------------------- OPENED/ONLY FOR USERS/ROLES -------------------------

        #endregion ------------------------- OPENED/ONLY FOR USERS/ROLES -------------------------


        #region ---------------------------------- LIKES -------------------------------

        /// <summary>
        /// Rating of current category 
        /// </summary>
        //public ushort LikeRating
        //{ get; set; }

        ///// <summary>
        ///// Voiced Count -only one time from one IPs. Lazy read/write from file LikeVoicesIPS
        ///// </summary>
        //public List<string> LikeVoicesIPS
        //{ get; set; } = new List<string>();

        ///// <summary>
        ///// Voices Count based on IPs count- in VoicesIPs file info, if such file exists. By default is 0 of course.
        ///// </summary>
        //public int LikeVoicesCount
        //{ get; set; } = 0;

        #endregion ---------------------------------- LIKES -------------------------------
        

        #region  ------------------------ COMMENTS ----------------------------

        #endregion  ------------------------ COMMENTS ----------------------------
        

    }


    #region ------------------- MenuItemView definition ---------------------------

    [RegisterView]
    public class MenuItemView : View<ContentElement>
    {

        public class  RowSchema : RDBSchema, IDocumentEntity
        {
            //public Guid docid { get; set; } // must exist

            public Guid ID { get; set; }

            public Guid? IDParent { get; set; }

            public bool IsCategory { get; set; }

            public string Name { get; set; }

            public string DisplayName { get; set; }

            public string Description { get; set; }

            public List<string> Tags { get; set; }// = new List<string>();




            public   IDocumentEntity GetDocumentFromView()
            {
                var element = (ContentElement)RaptorDBService.Current.Store.Fetch(ID);
                element.Name = this.Name;
                element.IsCategory = this.IsCategory;
                element.IDParent  = this.IDParent;
                element.Description = this.Description;
                element.DisplayName = this.DisplayName;
                element.Tags = this.Tags;// Modifications = this.Modifications;
                 
                return element;
            }



            public void Update()
            {
                var entity = (ContentElement)GetDocumentFromView();
                RaptorDBService.Current.Store.Save<ContentElement>(ID, entity);
            }



        }


        public MenuItemView()
        {
            this.Name = nameof(MenuItemView);
            this.Description = "A primary view for  ContentElement";
            this.isPrimaryList = true;
            this.isActive = true;
            this.BackgroundIndexing = true;
            this.Version = 1;

            this.Schema = typeof(RowSchema);
            this.NoIndexingColumns = new List<string>() { "DisplayName", "Description", "Tags" };// , "Tags", "NodeStoreInfo" "Modification" "Settings" "MailRequestsCount"};  


            this.Mapper = (api, docid, doc) =>
            {
                //if (doc.Status == 0)
                //    return;

                api.EmitObject(docid, doc);
            };
        }
    }


    #endregion ------------------- MenuItemView definition ---------------------------


    #region -------------------  ContentElementFilesNodeView definition ---------------------------

    [RegisterView]
    public class ContentElementFilesView : View<ContentElement> 
    {

        public class RowSchema : RDBSchema, IContentElement// DocumentEntity
        {
            //public Guid docid { get; set; } // must exist ?

            public Guid ID { get; set; }

            public Guid? IDParent { get; set; }

            public bool IsCategory { get; set; }

            public string Name { get; set; }
            public string Description { get; set; }

            public Dictionary<int, NodeReplicaInfo> NodeReplics { get; set; }


            public Dictionary<string, string> Settings { get; set; }


            #region -------------------- IContentElement --------------------------


            public bool IsContainsReplicaWithAddress(int replicaIndex, AccessMethodEn accessMethod)
            {
                if (NodeReplics.NotContainsKey(replicaIndex)) return false;
                if (accessMethod == AccessMethodEn.LocalFileSystem && !NodeReplics[replicaIndex].ContainsLocalAddress) return false;
                if (accessMethod == AccessMethodEn.NetworkAccess && !NodeReplics[replicaIndex].ContainsNetworkAddress) return false;
                if (accessMethod == AccessMethodEn.HttpAccess && !NodeReplics[replicaIndex].ContainsHttpAddress) return false;
                return true;
            }



            public string GetReplicaAdress(int replicaIndex, AccessMethodEn accessmethod)
            {
                Validator.ATInvalidOperationDbg(NodeReplics.NotContainsKey(replicaIndex), nameof(ContentElement), nameof(GetReplicaAdress)
               , " ContentElement  doesn't contain NodeReplicaInfo with index [{0}]".Fmt(replicaIndex.S()));

                switch (accessmethod)
                {
                    case AccessMethodEn.LocalFileSystem:
                        return this.NodeReplics[replicaIndex].NodeStoreLocalAddress;
                    case AccessMethodEn.NetworkAccess:
                        return this.NodeReplics[replicaIndex].NodeStoreNetworkAddress;
                    case AccessMethodEn.HttpAccess:
                        return this.NodeReplics[replicaIndex].NodeStoreHttpAddress;
                    default:
                        return this.NodeReplics[replicaIndex].NodeStoreLocalAddress;
                }
            }


            #endregion -------------------- IContentElement --------------------------


            #region --------------------------- IDocumentEntity ------------------------------

            public IContentElement GetDocumentFromView()
            {
                var element = (ContentElement)RaptorDBService.Current.Store.Fetch(ID);
                element.Name = this.Name;
                element.IDParent = this.IDParent;
                element.IsCategory = this.IsCategory;
                element.NodeReplics = this.NodeReplics;
                element.Settings = this.Settings;
                 //element.Modifications = this.Modifications;

                return element;
            }



            public void Update()
            {
                var entity = (ContentElement)GetDocumentFromView();
                RaptorDBService.Current.Store.Save<ContentElement>(ID, entity);
            }

            #endregion --------------------------- IDocumentEntity ------------------------------

        }


        public ContentElementFilesView()
        {
            this.Name = nameof(ContentElementFilesView);
            this.Description = "A primary view for  ContentElement";
            this.isPrimaryList = true;
            this.isActive = true;
            this.BackgroundIndexing = true;
            this.Version = 1;

            this.Schema = typeof(RowSchema);
            this.NoIndexingColumns = new List<string>() { "IsCategory", "Description", "NodeStoreInfo", "Settings" };// , "Tags", "NodeStoreInfo" "Modification" "Settings" "MailRequestsCount"};  


            this.Mapper = (api, docid, doc) =>
            {
                //if (doc.Status == 0)
                //    return;

                api.EmitObject(docid, doc);
            };
        }

    }


    #endregion ------------------- MenuItemView definition ---------------------------



}
