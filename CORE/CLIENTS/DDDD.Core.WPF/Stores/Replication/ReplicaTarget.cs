﻿using System.Collections.Generic;
using System.IO;

using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Stores.Files;


namespace DDDD.Core.Stores.Replication
{

    /// <summary>
    /// Replication Target PCNode and Store Address
    /// </summary>
    public class ReplicaTarget
    {

        #region ----------------------- CTOR-------------------------

        ReplicaTarget() { }

        #endregion ----------------------- CTOR-------------------------

        static readonly string dirDelim = Path.DirectorySeparatorChar.S();// @"\";

        /// <summary>
        /// Replica Target index
        /// </summary>
        public int Index
        { get; set; }

        /// <summary>
        /// Replication PC netBIOS name.
        /// </summary>
        public string NodePC
        { get; set; }


        public string AppRootDir
        { get; set; }


        public string HostUser
        { get; set; }

        #region ---------------------------- LOCAL ADDRESS ------------------------


        public string NodeStoreLocalAddress
        { get; set; }


        public bool ContainsLocalAddress
        {
            get
            {
                return NodeStoreLocalAddress.IsNotNullOrEmpty();
            }
        }


        #endregion  ---------------------------- LOCAL ADDRESS ------------------------
         

        #region ---------------------------- NETWORK ADDRESS ------------------------

        public string NodeStoreNetworkAddress
        { get; set; }


        public bool ContainsNetworkAddress
        {
            get
            {
                return NodeStoreNetworkAddress.IsNotNullOrEmpty();
            }
        }




        #endregion ---------------------------- NETWORK ADDRESS ------------------------


        #region ---------------------- HTTP ADDRESS ---------------------------
        public string NodeStoreHttpAddress
        { get; set; }


        public bool ContainsHttpAddress
        {
            get
            {
                return NodeStoreHttpAddress.IsNotNullOrEmpty();
            }
        }

        #endregion ---------------------- HTTP ADDRESS ---------------------------


        #region ------------------- ACCESS PRIORITY -----------------------

        /// <summary>
        /// Replication Target's current  Access method priority
        /// </summary>
        public List<AccessMethodEn> AccessPriorities
        { get; set; } = new List<AccessMethodEn>();


        /// <summary>
        /// Create Replication Target item whith LocalAddress access method priority.
        /// </summary>
        /// <returns></returns>
        public static ReplicaTarget CreateWithLocalPriority()
        {
            var replicaTarget = new ReplicaTarget();
            replicaTarget.NodePC = System.Environment.MachineName;
            replicaTarget.AppRootDir = FileService.GetAppRootData();
            replicaTarget.HostUser = FileService.AuthenticatedUser;

            replicaTarget.Index = 1;

            replicaTarget.AccessPriorities.Add(AccessMethodEn.LocalFileSystem);
            replicaTarget.AccessPriorities.Add(AccessMethodEn.NetworkAccess);

            replicaTarget.NodeStoreLocalAddress = Path.Combine(replicaTarget.AppRootDir, "content", replicaTarget.HostUser);


            return replicaTarget;
        }




        /// <summary>
        /// Create Replication Target item whith NetworkAddress  access method priority.
        /// </summary>
        /// <returns></returns>
        public static ReplicaTarget CreateWithNetworkPriority(int indexReplica, string userStoreNetworkAddress)
        {
            var replicaTarget = new ReplicaTarget();
            Validator.ATInvalidOperationDbg(indexReplica == 1, nameof(ReplicaTarget), nameof(CreateWithNetworkPriority), 
                "Replica with prioritized NetworkAddress access method can't be created with index 1. Index 1 reserved for default replica with LocalAdress access method."); 
              
            replicaTarget.NodePC = System.Environment.MachineName;
            replicaTarget.AppRootDir = FileService.GetAppRootData();
            replicaTarget.HostUser = FileService.AuthenticatedUser;

            replicaTarget.Index = indexReplica;

            replicaTarget.AccessPriorities.Add(AccessMethodEn.NetworkAccess);
            replicaTarget.AccessPriorities.Add(AccessMethodEn.LocalFileSystem);
            
            replicaTarget.NodeStoreNetworkAddress  = userStoreNetworkAddress;
             
            return replicaTarget;
        }


        #endregion ------------------- ACCESS PRIORITY -----------------------



        #region ------------------- LAST PC NODE CONNECTION,WORK STATE ----------------------

        /// <summary>
        /// Last call state - completed successfully/not.
        /// </summary>
        public PCNodeStateEn LastCallState
        { get; set; } = PCNodeStateEn.Normal;


        #endregion ------------------- LAST PC NODE CONNECTION,WORK STATE ----------------------




        public NodeReplicaInfo ToReplicaInfo()
        {
            var result = new NodeReplicaInfo();
            result.NodePC = NodePC;
            result.HostUser = HostUser;
            
            if (ContainsLocalAddress)
            {
                result.NodeStoreLocalAddress = NodeStoreLocalAddress;
            }

            if (ContainsNetworkAddress)
            {
                result.NodeStoreNetworkAddress = NodeStoreNetworkAddress;
            }

            if (ContainsLocalAddress)
            {
                result.NodeStoreHttpAddress = NodeStoreHttpAddress;
            }
            return result;

        }



    }
}
