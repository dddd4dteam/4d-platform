﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

using DDDD.Core.Extensions;
using DDDD.Core.Stores.Model;
 

using Raptor=RaptorDB;

namespace DDDD.Core.Stores.RaptorDB
{




    public class RaptorDBRepository : IRaptorDBRepository
    {

        #region ------------------- CTOR ---------------------

        internal RaptorDBRepository(Type targetEntityType )
        {
            EntityType = targetEntityType;
            HashKey = EntityType.GetHashCode();

            Store = RaptorDBService.Current.Store;
        }

        #endregion ------------------- CTOR ---------------------


        #region ---------------------- PROPERTIES ------------------------
        
        /// <summary>
        /// 
        /// </summary>
        public Type EntityType
        { get; private set; }


        /// <summary>
        /// 
        /// </summary>
        public int HashKey
        { get; private set; }


        /// <summary>
        /// RaptorDB - Entities Store 
        /// </summary>
        public Raptor.RaptorDB Store
        { get; private set; }


        HashSet<ContentItem> CachedItems
        { get; set; } = new HashSet<ContentItem>();
        

        #endregion ---------------------- PROPERTIES ------------------------
         

        #region ----------------------- CRUD/ SEARCHFOR-------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void Insert<TEntity>(TEntity entity) where TEntity : IDocumentEntity
        {
            Store.Save((entity as IDocumentEntity).ID , entity);            
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="entity"></param>
        public void Delete<TEntity>(TEntity entity) where TEntity : IDocumentEntity
        {
            Store.Delete((entity as IDocumentEntity).ID);            
        }


       


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public Raptor.Result<TViewRowSchema> GetById<TViewRowSchema>(Guid id) where TViewRowSchema : Raptor.RDBSchema, IDocumentEntity
        {
            // "product = \"prod 1\"" // string type filter
            return Store.Query<TViewRowSchema>(  (TViewRowSchema s)=>s.ID == id ); // typeof(TView).Name, "ID == {0}".Fmt(id.S()));  
                    
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <returns></returns>
        public Raptor.Result<TViewRowSchema> GetItems<TViewRowSchema>(Expression<Predicate<TViewRowSchema>> predicate, int page, int pageSize)
        {
            //Store.Query<TRowSheme>(filter, start, count) 
            //return Store.Query<TViewRowSchema>("");
            var startIndex = page * pageSize;
            return Store.Query( predicate,startIndex, pageSize  );// "Name != \"\" ");
            // throw new NotImplementedException();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public Raptor.Result<TViewRowSchema> SearchFor<TViewRowSchema>(Expression<Predicate<TViewRowSchema>> predicate) where TViewRowSchema : Raptor.RDBSchema, IDocumentEntity
        {  
            return Store.Query<TViewRowSchema>( predicate );
        }

      
        #endregion ----------------------- CRUD/ SEARCHFOR-------------------------

    }

}
