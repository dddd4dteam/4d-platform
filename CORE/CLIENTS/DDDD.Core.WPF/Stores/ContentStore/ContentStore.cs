﻿

#if SERVER && IIS 

using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;

using ImageMagick;
using ServiceStack.Text;
using  Raptor = RaptorDB;

using DDDD.Core.Collections;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;
using DDDD.Core.Stores.Model;

namespace DDDD.Core.Stores
{
      

    /// <summary>
    /// File Store based on File System. Categorized Content Items.
    /// </summary>
    public class ContentStore        
    {

        #region ------------------------------ CTOR -------------------------------

        /// <summary>
        /// Content File Store based on File System. Categorized Content Items.
        /// </summary>
        /// <param name="rootpath"></param>
        public ContentStore(string hostUser,string rootpath)
        {
            RootPath = rootpath;
            HostUser = hostUser;
            //ContentTree.AddRoot(RootPath);
            LoadUserStoreSettings();
            LastLoadedContentLevel = 0;
        }


        #endregion ------------------------------ CTOR -------------------------------


        #region --------------------------- IDENTIFICATION --------------------------------

        /// <summary>
        ///  Store Guid Identifier
        /// </summary>
        public Guid StoreID
        { get; } = Guid.NewGuid();



        /// <summary>
        ///  PC Host Key of this Node 
        /// </summary>
        public string HostNodePC
        { get; internal set; }



        /// <summary>
        /// Store of HostUser
        /// </summary>
        public string HostUser
        { get; private set; }


        /// <summary>
        /// Root Node Dir Physical Path
        /// </summary>
        public string RootPath
        { get; set; }


        #endregion --------------------------- IDENTIFICATION --------------------------------



        /// <summary>
        /// Last Loaded Content Level
        /// </summary>
        public int LastLoadedContentLevel
        { get; set; } = -1;


        ///// <summary>
        ///// Content Tree
        ///// </summary>
        //public TreeSearch<long,ContentItem> ContentTree
        //{ get; set; } = new TreeSearch<long,ContentItem>();


        /// <summary>
        /// Host Entities For Category  Nodes. When Category Entity will be created, then CategoryNode-file storing part, also will be created. 
        /// </summary>
        public List<Type>  CategoryEntities
        { get; private set; } = new List<Type>();

        /// <summary>
        /// Host Entities For Content  Nodes. When host Entity will be created, then ContentNode-file storing part, also will be created. 
        /// </summary>
        public List<Type> ContenEntities
        { get; private set; } = new List<Type>();



        /// <summary>
        /// RaptorDB - Entities DB
        /// </summary>
        public Raptor.RaptorDB EntitiesDB
        { get; set; }



        #region ------------------------ UserStoreSettings  ------------------------------- 


        public const string UserStoreSettingsJson = "userStoreSettings";

        

        /// <summary>
        /// Load UserStoreSettings - only on root Node loading. It's settings for all user belonged file store.
        /// </summary>
        /// <returns></returns>
        public void LoadUserStoreSettings()
        {
           //var rootNode = (ContentTree.Root as ContentItem<object>);
           // var userSettingsJsonFile = rootNode.MetaFiles.FirstOrDefault(fl => fl.Name.StartsWith(UserStoreSettingsJson));

           // Validator.ATNullReference(userSettingsJsonFile, nameof(ContentItem<object>), nameof(LoadUserStoreSettings), nameof(userSettingsJsonFile)
           //                                    , "File of UserStoreSettings was not found in the root Directory.");

           // Settings = JsonSerializer.DeserializeFromString<UserStoreSettings>(userSettingsJsonFile.LoadJsonText());
            
        }


        /// <summary>
        /// FileStore User Settings
        /// </summary>
        //public UserStoreSettings Settings
        //{ get; set; }




        #endregion ------------------------ UserStoreSettings  -------------------------------


        #region  ---------------------------- CONTENT TREE with PAGES LOADING  ----------------------------------

        //load sub nodes - [startindex -endindex]
        // load all subnodes
        //public void LoadSubNodes(int beginIndex, int endIndex)


        internal void LoadRootContentLevel()
        {
            //var targetNode = ContentTree.Root as ContentItem<object>;

            //load user Store Settings
            //if (targetNode.ContainsUserStoreSettingsJson())
            //{
            //    Settings = targetNode.LoadUserStoreSettings();
            //}
            //else throw new InvalidOperationException("User Settings file was not found");

            LastLoadedContentLevel = 0; //Root was Loaded successfully
        }


        /// <summary>
        /// Load all Child Nodes For [parentNode]
        /// </summary>
        /// <param name="parentNode"></param>
        public void LoadChildNodesForNode(ContentItem  parentNode, bool reScanSubDirsNow = true)
        {
            //update all subdirs at this moment and create nodes by this collection.
            //if (reScanSubDirsNow) { var dirs = parentNode.LA_SubNodesDirs.Value; }


            //foreach (var subdir in  parentNode.SubNodeDirs.Where(d=>d.DirectoryType == DirectoryTypeEn.CategoryDir) )
            //{
            //   var newNode = ContentTree.AddNodeToParent(parentNode, subdir.PhysicalPath);              
            //}
            //foreach (var subdir in parentNode.SubNodeDirs.Where(d => d.DirectoryType == DirectoryTypeEn.ContentItemDir))
            //{
            //    var newNode = ContentTree.AddNodeToParent(parentNode, subdir.PhysicalPath);                
            //}
            //only not for DataDirs
        }

        /// <summary>
        ///  Load all   Nodes of level , if it really was not loaded before. 
        /// </summary>
        /// <param name="level"></param>
        public void LoadContentOfLevel(ushort level = 1)
        {

            if (LastLoadedContentLevel == level) return; //level was already Loaded

            if (level == 0)
            {
                LoadRootContentLevel();
            }
            if (level > 0)
            {
                //var forparentNodes = ContentTree.GetNodesOnLevel((level - 1));

                //foreach (var parntNode in forparentNodes)
                //{
                //    LoadChildNodesForNode(parntNode as ContentItem<object>);
                //}
            }
            LastLoadedContentLevel = level; //level was Loaded successfully
        }


        /// <summary>
        /// Reload Child Nodes for Node - parentNode. 
        /// </summary>
        /// <param name="parentNode"></param>
        public void ReloadChildNodesForNode(ContentItem  parentNode)
        {
            //clear all childs of child -true -recursively
            //ContentTree.ClearChilds(parentNode);
            LoadChildNodesForNode(parentNode);
        }




        public void ReloadContentOfLevel(ushort level = 1)
        {

        }
        #endregion ---------------------------- CONTENT TREE with PAGES LOADING  ----------------------------------



        #region ---------------------------- ADD /REMOVE /MOVE NODE ----------------------------

        /// <summary>
        /// Add new Node to parent Node and create Directories.
        /// </summary>
        /// <param name="parentID"></param>
        /// <param name="nodeType"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        //public long AddNode(long parentID, NodeTypeEn nodeType, string name)
        //{
        //    var parentNode = ContentTree.GetNode(parentID);
        //    var newNodePhysicalPath = BuildNewNodePhysicalPath(parentNode, nodeType);
        //    var addedNode = ContentTree.AddNodeToParent(parentNode, newNodePhysicalPath);

        //    return addedNode.ID;
        //}

        //string BuildNewNodePhysicalPath(NodeM<long> parentNode, NodeTypeEn nodeType)
        //{
        //    var targetNode = (parentNode as ContentItem<object>);
        //    var lastchild = parentNode.Childs.LastOrDefault();
        //    var chNodePath = targetNode.GetNewChildNodePhysicalPath();  

        //    var newNodePhysicalPath = targetNode.PhysicalPath;
        //    if (nodeType == NodeTypeEn.Category)
        //    {
        //        newNodePhysicalPath += "CG";
        //    }
        //    else if (nodeType == NodeTypeEn.Content)
        //    {
        //        newNodePhysicalPath += "CNT";
        //    }

        //    if (lastchild.IsNotNull()) newNodePhysicalPath += (lastchild.ChildIndex + 1).S();
        //    else if (lastchild.IsNull()) newNodePhysicalPath += 1;
        //    return newNodePhysicalPath;
        //}

        /// <summary>
        /// Remove Node by it's nodeID. 
        /// </summary>
        /// <param name="ID"></param>
        //public void RemoveNode(long nodeID)
        //{
        //    var targetNode = ContentTree.GetNode(nodeID);


        //    //remove directory with all content files

        //    //remove node definition from ContentTree
        //    ContentTree.RemoveNode(nodeID);

        //    //update Parent Node

        //}



        public void MoveNode(int targetNodeID, int toParentNodeID)
        {

        }



        #endregion ---------------------------- ADD /REMOVE /MOVE NODE ----------------------------


        #region  ----------------------------  QUERIES for Entities ------------------------------------ 



        #endregion ----------------------------  QUERIES for Entities ------------------------------------




        #region  ----------------------------  QUERIES for  ContentNodes ------------------------------------ 
        // CLIENT SIDE  User queries  

        /// <summary>
        /// Get All Nodes on Level
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public List<ContentItem> GetNodes(int level)
        {
            var result = new List<ContentItem>();

            return result;
        }

        /// <summary>
        /// Get Nodes for  forNodeId
        /// </summary>
        /// <param name="forNodeId"></param>
        /// <returns></returns>
        public List<ContentItem> GetNodes(long forNodeId)
        {
            var result = new List<ContentItem>();

            return result;
        }
        #endregion ----------------------------  QUERIES for  ContentNodes ------------------------------------
          

        #region ------------------------ GET - NODE DEFINITIONS -------------------------------

        //List<INodeDefinition> GetRootNodeChildNodesDefenitions
        //List<INodeDefinition> GetNodeChildNodesDefenitions

        #endregion ------------------------ GET - NODE DEFINITIONS -------------------------------


        #region ------------------------- GET - NODE IMAGES/AUDIOS/VIDEOS/DOCUMENTS ---------------------------

        //List<string> GetNodeAbsoluteImagesUrls ( long nodeID)
        //List<string> GetNodeRelativeImagesUrls ( long nodeID)
        //List<string> GetNodeAbsoluteAudiosUrls ( long nodeID)
        //List<string> GetNodeRelativeAudiosUrls ( long nodeID)
        //List<string> GetNodeAbsoluteVideosUrls ( long nodeID)
        //List<string> GetNodeRelativeVideosUrls ( long nodeID)
        //List<string> GetNodeAbsoluteDocumentsUrls ( long nodeID)
        //List<string> GetNodeRelativeDocumentsUrls ( long nodeID)

        #endregion ------------------------- GET - NODE IMAGES/AUDIOS/VIDEOS/DOCUMENTS ---------------------------
            

        /// THUMBS - build on client automatically from Urls
 

        #region ---------------------------- ADD UPLOADED FILES TO NODE(S) -------------------------------

        /// <summary>
        /// Add file from Upload Folder to Node data files.
        /// </summary>
        /// <param name="targetNodeID"></param>
        /// <param name="uploadedFileName"></param>
        public void AddUploadedFileToNode(long targetNodeID, string uploadedFileName)
        {
            //from uploaded folder -move file to content dir -safety


        }

        /// <summary>
        /// Add files from Upload Folder to Node data files.
        /// </summary>
        /// <param name="targetNodeID"></param>
        /// <param name="uploadedFile"></param>
        public void AddUploadedFilesToNode(long targetNodeID, string[] uploadedFile)
        {
            //from uploaded folder -move file to content dir -safety


        }

        #endregion ---------------------------- ADD UPLOADED FILES TO NODE(S) -------------------------------
        

        #region --------------------- Task - check add automatically Thumbs ----------------------------


        public static void CheckAddImageThumbsAutomatically()
        {
            var someImageFilePath = "";
            
            // Resize from file
            using (MagickImage image = new MagickImage(someImageFilePath))
            {
                MagickGeometry size = new MagickGeometry(100, 100);
                // This will resize the image to a fixed size without maintaining the aspect ratio.
                // Normally an image will be resized to fit inside the specified size.
                size.IgnoreAspectRatio = true;

                image.Resize(size);
                image.CompressionMethod = CompressionMethod.LosslessJPEG;
                // Save the result
                image.Write(someImageFilePath); //   "Snakeware.100x100.png"
            }


            //Compress Lossless compress JPEG logo
            FileInfo snakewareLogo = new FileInfo("Snakeware.jpg");

            Console.WriteLine("Bytes before: " + snakewareLogo.Length);

            ImageOptimizer optimizer = new ImageOptimizer();
            optimizer.LosslessCompress(snakewareLogo);

            snakewareLogo.Refresh();
            Console.WriteLine("Bytes after:  " + snakewareLogo.Length);

        }

        #endregion --------------------- Task - check add automatically Thumbs ----------------------------

 

    }


}



#endif /// SERVER && IIS


#region ----------------------------------- GARBAGE ------------------------------------

//[System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
//sealed class FileTypeExtensionsAttribute : Attribute
//{
//    // See the attribute guidelines at 
//    //  http://go.microsoft.com/fwlink/?LinkId=85236
//    readonly string positionalString;

//    // This is a positional argument
//    public FileTypeExtensionsAttribute(string fileTypeExtensions)
//    {
//        AllFileTypeExtensions = fileTypeExtensions;


//        Extensions = AllFileTypeExtensions.Split(new char[] { ',', ';', '|' }).ToList();
//    }

//    /// <summary>
//    /// All extensions whose belong to some File Type.
//    /// </summary>
//    public string AllFileTypeExtensions
//    {
//        get; private set;
//    }

//    /// <summary>
//    /// File Type - known file Extensions after splitted by delimeter. 
//    /// </summary>
//    public List<string> Extensions
//    { get; } = new List<string>();


//}


//[FileTypeExtensions(".jpg|.png|.bmp|.jpeg")]
//[FileTypeExtensions(".mp3|.ogg|.wav")]
//[FileTypeExtensions(".3gp|.avi|.mkv")]
//[FileTypeExtensions(".doc|.xls|.xlsx|.ppt|.zip|.rar|.7z")]



#endregion ----------------------------------- GARBAGE ------------------------------------


