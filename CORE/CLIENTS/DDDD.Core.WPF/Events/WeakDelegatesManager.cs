﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDD.Core.Events
{



    /// <summary>
    /// Weak Delegate manager 
    /// </summary>
    /// <example>
    /// The following code shows the usage of the 
    /// WeakDelegatesManager .
    /// <code>
    /// private readonly WeakDelegatesManager contentRegisteredListeners = new WeakDelegatesManager();
    /// 
    /// public event EventHandler_EventArgs_ ContentRegistered
    /// {
    ///    add {     
    ///          this.contentRegisteredListeners.AddListener(value); 
    ///        }
    ///    remove 
    ///        {
    ///          this.contentRegisteredListeners.RemoveListener(value); 
    ///        }
    /// }
    /// 
    ///  ContentRegistered.Raise(newEventArg)
    /// 
    /// </code>    
    /// </example>
    public class WeakDelegatesManager
    {
        private readonly List<DelegateReference> listeners = new List<DelegateReference>();

        /// <summary>
        /// Add Listener delegate
        /// </summary>       
        public void AddListener(Delegate listener)
        {
            listeners.Add(new DelegateReference(listener, false));
        }

        /// <summary>
        /// Remove Listener 
        /// </summary>
        /// <param name="listener"></param>
        public void RemoveListener(Delegate listener)
        {
            listeners.RemoveAll(reference =>
            {
                //Remove the listener, and prune collected listeners
                Delegate target = reference.Target;
                return listener.Equals(target) || target == null;
            });
        }

        /// <summary>
        ///Remove all null-Target Listeners, and rise handlers for each of listeners as DynamicInvoke. 
        /// </summary>
        /// <param name="args"></param>
        public void Raise(params object[] args)
        {
            listeners.RemoveAll(listener => listener.Target == null);
            var pointListeners = listeners.ToList();// 
            var handlers = pointListeners.Where(listener => listener != null)
                                         .Select(listener => listener.Target)                                          
                                         .ToList();            
            for (int i = 0; i < handlers.Count; i++)
            {
                handlers[i].DynamicInvoke(args);
            }

            //foreach (Delegate handler in listeners.ToList().Select(listener => listener.Target)
            //                                               .Where(listener => listener != null) )
            //{
            //    handler.DynamicInvoke(args);
            //}
        }
    }

}
