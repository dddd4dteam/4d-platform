﻿using System;
using System.Collections.Specialized;

namespace DDDD.Core.Events.IG
{
	/// <summary>
	/// Helper class for weak handling of <c>System.Collections.Specialized.INotifyCollectionChanged.CollectionChanged</c>.
	/// </summary>
	/// <typeparam name="TInstance">The type of the instance that will recieve the events.</typeparam>
	/// <remarks>
	/// See <see cref="T: WeakEventHandler`3" /> for a sample.
	/// </remarks>
	internal class WeakCollectionChangedHandler<TInstance> : WeakEventHandler<TInstance, INotifyCollectionChanged, NotifyCollectionChangedEventArgs>
	where TInstance : class
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T: WeakCollectionChangedHandler`1" /> class.
		/// </summary>
		/// <param name="instance">The short living object that wants to recieve events from the long living <paramref name="eventSource" /> object.</param>
		/// <param name="eventSource">The long living object that raises the event.</param>
		/// <param name="onEventAction">The delegate that will be invoked when the event is raised.</param>
		public WeakCollectionChangedHandler(TInstance instance, INotifyCollectionChanged eventSource, Action<TInstance, object, NotifyCollectionChangedEventArgs> onEventAction) : base(instance, eventSource, onEventAction, (WeakEventHandler<TInstance, INotifyCollectionChanged, NotifyCollectionChangedEventArgs> weakHandler, INotifyCollectionChanged evSource) => evSource.CollectionChanged -= new NotifyCollectionChangedEventHandler(weakHandler.OnEvent))
		{
		}
	}
}
