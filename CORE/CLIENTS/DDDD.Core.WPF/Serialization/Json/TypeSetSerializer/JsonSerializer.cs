﻿using System;
using System.Net;
using System.Text;

using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

using DDDD.Core.IO;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Serialization;
using System.Xml.Serialization;

#if NET45
using System.Collections.Concurrent;
#endif


namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// Type Set Serializer2(TSS).  Serializer personally. 
    /// <para/> Common use  workflow - at first we grouping our types into some Type Set:  1 -creating TypeSetSerializer2 and 2 -AddingType[s] to it - it means that this TypeSetSerializer2 will be able to [De]/Serialise only these custom known types.
    /// <para/> We manage by TypeSetSerializer2's collections by the following static methods :
    /// <para/>             Get(...), GetDirect(...), GetSafe(...), AddUpdateTypeSetSerializer(...), AddOrUseExistTypeSetSerializer(...), RemoveSerializer(...)
    /// <para/> And to [De]/Serialise:
    /// <para/>             we use Serialize(...), SerializeTPL(...), Deserialize(...), DeserializeTPL() , ContinueSerializeTPL(...), ContinueDeserializeTPL() 
    /// </summary>
    public class JsonSerializer : ITypeSetSerializerJson
    {

        #region ----------------------------- CTOR  ---------------------------

        static JsonSerializer()
        {
            Initialize();          
        }


        #endregion ----------------------------- CTOR  ---------------------------


        #region  --------------------------------------- INITIALIZATION -----------------------------------------

        private static void Initialize()
        {
            TypeSerializeAnalyser.Init_JsonSerializeGenerators();
        }


        #endregion --------------------------------------- INITIALIZATION -----------------------------------------


        #region --------------------- CONFIG -----------------------

        public static class Config
        {
            
            public static UTF8Encoding UTF8Encoding = new UTF8Encoding();

            public static int MAX_DEPTH = 20;
                        
            /// <summary>
            /// Use the optimized fast Dataset Schema format (default = True)
            /// </summary>
            public static bool UseOptimizedDatasetSchema = true;
            
            /// <summary>
            /// Use the fast GUID format (default = True)
            /// </summary>
            public static bool UseFastGuid = true;
            /// <summary>
            /// Serialize null values to the output (default = True)
            /// </summary>
            public static bool SerializeNullValues = true;
            /// <summary>
            /// Use the UTC date format (default = True)
            /// </summary>
            public static bool UseUTCDateTime = true;
            /// <summary>
            /// Show the readonly properties of types in the output (default = False)
            /// </summary>
            public static bool ShowReadOnlyProperties = false;
            /// <summary>
            /// Use the $types extension to optimise the output json (default = True)
            /// </summary>
            public static bool UsingGlobalTypes = true;
             
            /// <summary>
            /// Anonymous types have read only properties 
            /// </summary>
            public static bool EnableAnonymousTypes = false;
            /// <summary>
            /// Enable fastJSON extensions $types, $type, $map (default = True)
            /// </summary>
            public static bool UseExtensions = true;
            /// <summary>
            /// Use escaped unicode i.e. \uXXXX format for non ASCII characters (default = True)
            /// </summary>
            public static bool UseEscapedUnicode = true;
            /// <summary>
            /// Output string key dictionaries as "k"/"v" format (default = False) 
            /// </summary>
            public static bool KVStyleStringDictionary = false;
            /// <summary>
            /// Output Enum values instead of names (default = False)
            /// </summary>
            public static bool UseValuesOfEnums = false;
            /// <summary>
            /// Ignore attributes to check for (default : XmlIgnoreAttribute, NonSerialized)
            /// </summary>
            public static List<Type> IgnoreAttributes = new List<Type> { typeof(XmlIgnoreAttribute), typeof(NonSerializedAttribute) };
            /// <summary>
            /// If you have parametric and no default constructor for you classes (default = False)
            /// 
            /// IMPORTANT NOTE : If True then all initial values within the class will be ignored and will be not set
            /// </summary>
            public static bool ParametricConstructorOverride = false;
            /// <summary>
            /// Serialize DateTime milliseconds i.e. yyyy-MM-dd HH:mm:ss.nnn (default = false)
            /// </summary>
            public static bool DateTimeMilliseconds = false;
            /// <summary>
            /// Maximum depth for circular references in inline mode (default = 20)
            /// </summary>
            public static byte SerializerMaxDepth = 20;
            /// <summary>
            /// Inline circular or already seen objects instead of replacement with $i (default = False) 
            /// </summary>
            public static bool InlineCircularReferences = false;
            /// <summary>
            /// Save property/field names as lowercase (default = false)
            /// </summary>
            public static bool SerializeToLowerCaseNames = false;

            public static void FixValues()
            {
                if (UseExtensions == false) // disable conflicting params
                {
                    UsingGlobalTypes = false;
                    InlineCircularReferences = true;
                }
                if (EnableAnonymousTypes)
                    ShowReadOnlyProperties = true;
            }


            #region ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

#if NET45 /////// --------------------------- WRITE DEBUG CODE ASSEMBLY --------------------------------///////


            /// <summary>
            /// If this flag is true, after each BuildProcessing() call, we'll write debugging code assembly.
            /// By Default it's false. But this feature can't be used in real-time parallel work, be carefull.
            /// </summary>
            internal static bool UseGenerationOfDebugCodeAssembly { get; set; }
             

#endif
            #endregion ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

        } 

        #endregion
        

        #region ---------------------------- CONST && FIELDS -----------------------
        
        const string Create = nameof(Create);
        static readonly object _locker = new object();


        private int _before;
        int _current_depth = 0;
        private Dictionary<string, int> _globalTypes = new Dictionary<string, int>();
        private Dictionary<object, int> _cirobj = new Dictionary<object, int>();

        #endregion ---------------------------- CONST && FIELDS -----------------------



        #region ---------------------------- TYPE  SERIALIZERS DICTIONARIES ---------------------------

#if NET45


        static ConcurrentDictionary<Type, ITypeSerializerJson> TypeSerializers
        { get; } = new ConcurrentDictionary<Type, ITypeSerializerJson>();




        ///// <summary>
        /////  Type Item Serializers - Dictionary[ID-ushort, ITypeserializer -Type item Serializer].
        ///// </summary>
        //static ConcurrentDictionary<ushort, ITypeSerializerJson> TypeSerializers
        //{ get; } = new ConcurrentDictionary<ushort, ITypeSerializerJson>();


        ///// <summary>
        ///// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        ///// </summary>
        //static  ConcurrentDictionary<Type, ushort> TypeSerializerIDByType
        //{ get; } = new ConcurrentDictionary<Type, ushort>();

#else
        
        /// <summary>
        /// Main Type Serializers Collection of this TYPE SET SERIALIZER
        /// </summary>
        static Dictionary<ushort, ITypeSerializerJson> TypeSerializers
        { get; } = new Dictionary<ushort, ITypeSerializerJson>();


        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        /// </summary>
        static Dictionary<Type, ushort> TypeSerializerIDByType
        { get; } = new Dictionary<Type, ushort>();

#endif
 

        #endregion  ---------------------------- TYPE SERIALIZERS DICTIONARIES ---------------------------


        #region --------------------------- StringBuilder ALLOCATE/FREE Thread static -------------------------


        [ThreadStatic]
        static StringWriter cachedStringWriter;

        internal static StringWriter Allocate()
        {
            var ret = cachedStringWriter;
            if (ret == null)
                return new StringWriter(CultureInfo.InvariantCulture);

            var sb = ret.GetStringBuilder();
            sb.Length = 0;
            cachedStringWriter = null;  //don't re-issue cached instance until it's freed
            return ret;
        }

        internal static void Free(StringWriter writer)
        {
            cachedStringWriter = writer;
        }

        internal static string ReturnAndFree(StringWriter writer)
        {
            var ret = writer.ToString();
            cachedStringWriter = writer;
            return ret;
        }

        #endregion --------------------------- StringBuilder ALLOCATE/FREE Thread static -------------------------



        #region ---------------------- GET/CONTAINS TYPE SERIALIZER BY typeID / TYPE/ DATA ------------------------


        internal static bool ContainsTypeSerializer(Type targetType)
        {
            return TypeSerializers.ContainsKey(targetType);
        }





        #endregion  ---------------------- GET/CONTAINS TYPE SERIALIZER BY typeID / TYPE/ DATA ------------------------


        #region ----------------------- ADD KNOWN TYPES && CreateTypeSerializer -------------------------


       internal static ITypeSerializerJson<T> AddKnownType<T>(
                                     TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {

            return AddKnownType(typeof(T), selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers) as ITypeSerializerJson<T>;
        }


        /// <summary>
        ///  Full  AddContract overload Creating TypeSerializer for different Contract Category value.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        internal static ITypeSerializerJson AddKnownType(Type targetType

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {
            ushort? typeID = 0;

            //We cannot add  Contract Type that already exist
            if (ContainsTypeSerializer(targetType)) return TypeSerializers[targetType];
            //if ((typeID = GetTypeSerializeIDForType(targetType)).IsNotNull()) return TypeSerializers[typeID.Value];

            _locker.LockAction( ()=> ContainsTypeSerializer(targetType)  //GetTypeSerializeIDForType(targetType).IsNotNull
            , () =>
            {
                var serializeKnownMemberTypes = TypeSerializeAnalyser.CanBeSerializedJson(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);

                //if  serializeKnownTypesTree is List<Type>  with  0 elements it means that this targetType is not serializable
                Validator.ATInvalidOperationDbg(serializeKnownMemberTypes.IsNotWorkable(), nameof(JsonSerializer), nameof(AddKnownType)
                     , "This targetType - [{2}]  Can't be Serialized".Fmt(targetType.FullName));


                for (int i = 0; i < serializeKnownMemberTypes.Count; i++)
                {
                    AddKnownTypeWithoutAnalyze(serializeKnownMemberTypes[i], selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
                }

                //after all members Types added add targetType
                var typeSerializer = TypeSerializerJsonCreator.CreateTypeSerializerJson(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
                AddTypeSerializer(typeSerializer);
            }
            );

            return TypeSerializers[targetType];
           // return TypeSerializers[typeID.Value];

        }

        internal static void AddKnownTypeWithoutAnalyze(Type targetType

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {
            ushort? typeID = 0;
            //We cannot add  Contract Type that already exist
            // if ((typeID = GetTypeSerializeIDForType(targetType)).IsNotNull()) return;
            if (ContainsTypeSerializer(targetType)) return;// TypeSerializers[targetType];


            //not exist then Create Types Serializer and Add it
            var typeSerializer =TypeSerializerJsonCreator.CreateTypeSerializerJson(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
            AddTypeSerializer(typeSerializer);

        }


        internal static void AddTypeSerializer(ITypeSerializerJson tpSerializer)
        {
#if NET45
            TypeSerializers.TryAdd(tpSerializer.TargetType, tpSerializer);
            //TypeSerializers.TryAdd(tpSerializer.TypeID, tpSerializer);
            //TypeSerializerIDByType.TryAdd(tpSerializer.TargetType, tpSerializer.TypeID);
#else
            TypeSerializers.TryAdd(tpSerializer.TargetType, tpSerializer);
            //TypeSerializers.Add(tpSerializer.TypeID, tpSerializer);
            //TypeSerializerIDByType.Add(tpSerializer.TargetType, tpSerializer.TypeID);
#endif

        }



        #endregion ----------------------- ADD KNOWN TYPES && CreateTypeSerializer -------------------------


        #region ------------------------- CAN SERIALIZE T/TYPE ----------------------------

        /// <summary>
        /// Check if current TSS instance can serialize  Type - typeof(T).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool CanSerialize<T>()
        {
            return true;
            //return CanSerialize(typeof(T));
        }


        /// <summary>
        ///  Check if current TSS instance can serialize Type of data instance - [dataItem.GetType()].
        /// </summary>
        /// <param name="dataItem"></param>
        /// <param name="throwOnNullValue"></param>
        /// <returns></returns>
        public static bool CanSerialize(object dataItem, bool throwOnNullValue)
        {
            if (dataItem == null && throwOnNullValue)
                throw new InvalidOperationException($" ERROR in {nameof(JsonSerializer)}.{nameof(CanSerialize)}() : dataItem can't be null to determine serialization possibilities ");
            else if (dataItem == null) return false;

            return true;
            //return CanSerialize(dataItem.GetType());
        }

        #endregion ------------------------- CAN SERIALIZE T/TYPE ----------------------------





        #region --------------------------------- PLAIN STYLE DE/SERIALIZE -------------------------------------

        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] Serialize(object data, bool isNullable = false)
        {
            byte[] result = null;
            using (var ms = new MemoryStream())
            {
                Serialize(ms, data, isNullable);
                result = ms.ToArray();
            }
            return result;
        }

        /// <summary>
        /// Serialize object data into stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <param name="isNullable"></param>
        public static void Serialize(Stream stream, object data, bool isNullable = false)
        {
            if (stream == null || data == null) return;

            var realType =(isNullable)? typeof(Nullable<>).MakeGenericType( data.GetType()) : data.GetType() ;

            //if (GetTypeSerializeIDForType(realType).IsNull())
            //{
            //    AddKnownType(realType);//thread-safe action
            //}

            //ushort typeID = GetTypeID(realType);
           
            //if (typeID > 0) //already contains need no to DetectAddAutoContract
            //{
            //    if (realType.Is4DPrimitiveType()|| realType.IsEnum)
            //    {
            //        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
            //        BinaryReaderWriter.Write(stream, typeID);
            //        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);
                    
            //    }
            //    else
            //    {
            //        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);                    
            //    }

            //}
            //else if (typeID == 0) return; //null value
             

        }


     


        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] SerializeToByteArray(object value)
        {
           return Serialize(value);
        }

       




        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object Deserialize(Stream stream)
        {
            if (stream == null) return null;        

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID

            ////check if typeID exist in Dictionary 
            //Validator.AssertFalse<InvalidOperationException>( 
            //                            TypeSerializers.ContainsKey(typeID)
            //                            , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
            //                            , typeID.S()
            //                            );
            //return TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);
            

            return null;
        }


        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T Deserialize<T>(Stream stream)
        {
            if (stream == null) return default(T);                     

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID
            
            ////check if typeID exist in Dictionary 
            //Validator.AssertFalse<InvalidOperationException>(
            //                            TypeSerializers.ContainsKey(typeID)
            //                            , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
            //                            , typeID.S()
            //                            );


            //return (T)TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);

            return default(T);
            //return (T)Deserialize(stream);
        }

 

        #endregion ---------------------------------PLAIN  STYLE DE/SERIALIZE -------------------------------------


        #region ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------------------ SERIALIZE TPL ------------------------------------------


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data)
        {
            return SerializeTPL(data, default(CancellationToken), TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="taskCretionOptions"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data, CancellationToken cancelToken, TaskCreationOptions taskCretionOptions, TaskScheduler scheduler)
        {         
            return Task.Factory.StartNew<byte[]>((st) =>
            {
                var inSt = st as  object;

                if (inSt.IsNull()) return null;

                return Serialize(inSt);               

            },
            data,
            cancelToken,
            taskCretionOptions,
            scheduler
            );

        }



        /// <summary>
        /// Serialize object data into binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public static Task SerializeTPL(Stream stream, object data)
        {
            var state = ThreadCallState<Stream, object>.Create(stream, data);

            return Task.Factory.StartNew((st) =>
            {
                //Item1- TSSerializer
                //Item2 - stream
                //Item3 - data
                var inSt = st as ThreadCallState<Stream, object>;

                if (inSt.Item1 == null || inSt.Item2 == null) return;

                var realType = inSt.Item2.GetType();
                
                Serialize(inSt.Item1, inSt.Item2);
            }
            ,
            state,
            TaskCreationOptions.AttachedToParent
            );

        }



#endregion ------------------------------------------ SERIALIZE TPL ------------------------------------------



#region ------------------------------------- ContinueSerializeTPL ------------------------------------------

#if NET45 || WP81
        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream  
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data)
        {
            return ContinueSerializeTPL(streamTask, data, TaskContinuationOptions.AttachedToParent);
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>     
        /// <param name="continueAsOption"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, TaskContinuationOptions continueAsOption)
        {       
            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as  object;

                if (strm == null || inSt == null) return;

                var realType = inSt.GetType();

                Serialize(strm, inSt);
            }
            , data
            , continueAsOption
            );
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options, and add CancellationToken.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler)
        {
            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as  object;

                if (strm == null || inSt == null) return;

                var realType = inSt.GetType();

                Serialize(strm, inSt);   
            }
            , data
            , cancelToken
            , continueAsOption
            , scheduler
            );
        }

#endif

#endregion ------------------------------------- ContinueSerializeTPL ------------------------------------------



#region ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------

        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Task<object> DeserializeTPL(Stream stream)
        {            
            return Task.Factory.StartNew<object>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as Stream;

                if (inSt == null) return null;
                              
                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case      

                //check if typeID exist in Dictionary 
                //Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                //    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                //    , typeID.S());
                ////return GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(inSt, true);
                return null;
                
            }
                ,
                stream
                , TaskCreationOptions.AttachedToParent
                );

        }

        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public  static  Task<T> DeserializeTPL<T>(Stream stream)
        {           
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as Stream;

                if (inSt == null) return default(T);

                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case                    

                //check if typeID exist in Dictionary 
                //Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                //    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                //    , typeID.S());

                //return (T)GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(inSt, true);

                return default(T);

            }
            ,
            stream
            , TaskCreationOptions.AttachedToParent
            );
        }



        #endregion ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------



#region ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------

#if NET45 || WP81

        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }



        /// <summary>
        ///  Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<object>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 //var serializer = st as ITypeSetSerializer2;


                 var strm = t.Result;

                 if (strm == null) return null;

                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 //Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                 //    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                 //    , typeID.S());

                 //return GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(strm, true);

                 return null;
             }
            , null
            , cancelToken // 
            , continuation
            , scheduler
            );

        }



        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL<T>(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<T>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 //var serializer = st as ITypeSetSerializer2;
                 
                 var strm = t.Result;

                 if (strm == null) return default(T);
                       
                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 //Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                 //    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                 //    , typeID.S());

                 //return (T)GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(strm, true);
                 
                 return default(T);
             }
            , null
            , cancelToken // 
            , continuation
            , scheduler
            );

        }


#endif

#endregion ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------



#region ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------

        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static Task<T> DeserializeTPL<T>(byte[] serializedData)
        {
            //var state = ThreadCallState<ITypeSetSerializer2, byte[]>.Create(Current, serializedData);
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as  byte[];

                if (inSt.IsNotWorkable()) return default(T);
                
                using (var ms = new MemoryStream(inSt))
                {
                    ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(ms);//read TypeID

                    //if (typeID == 0) // null value type and value - impossible case                    

                    //check if typeID exist in Dictionary   InvalidOperationException
                    //Validator.AssertFalse<InvalidOperationException>( ContainsTypeSerializer(typeID) // TypeSerializers.ContainsKey
                    //    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary".Fmt(typeID.S()));

                    //return ( GetTypeSerializer(typeID) as ITypeSerializer2<T>).RaiseDeserializeFromStreamTHandler( ms, true); //TypeSerializers
                    return default(T);
                     
                }
            }
            ,
             serializedData
            , TaskCreationOptions.AttachedToParent
            );

        }

#endregion ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------



#endregion ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


         




        #region --------------- DESERIALIZE Response ------------------- 

        public static T DeserializeResponse<T>(WebRequest webRequest)
        {
            T t= default(T);
            //using (WebResponse response = PclExport.Instance.GetResponse(webRequest))
            //{
            //    using (Stream responseStream = response.GetResponseStream())
            //    {
            //        t = JsonSerializer.DeserializeFromStream<T>(responseStream);
            //    }
            //}
            return t;
        }

        public static object DeserializeResponse<T>(Type type, WebRequest webRequest)
        {
            object obj = null;
            //using (WebResponse response = PclExport.Instance.GetResponse(webRequest))
            //{
            //    using (Stream responseStream = response.GetResponseStream())
            //    {
            //        obj = JsonSerializer.DeserializeFromStream(type, responseStream);
            //    }
            //}
            return obj;
        }

        public static T DeserializeResponse<T>(WebResponse webResponse)
        {
            T t= default(T);
            //using (Stream responseStream = webResponse.GetResponseStream())
            //{
            //    t = JsonSerializer.DeserializeFromStream<T>(responseStream);
            //}
            return t;
        }

        public static object DeserializeResponse(Type type, WebResponse webResponse)
        {
            object obj = null;
            //using (Stream responseStream = webResponse.GetResponseStream())
            //{
            //    obj = JsonSerializer.DeserializeFromStream(type, responseStream);
            //}
            return obj;
        }

        #endregion --------------- DESERIALIZE Response ------------------- 
        

        #region --------------- DESERIALIZE  Request -------------------

        public static T DeserializeRequest<T>(WebRequest webRequest)
        {
            T t = default(T);
            //using (WebResponse response = PclExport.Instance.GetResponse(webRequest))
            //{
            //    t = DeserializeResponse<T>(response);
            //}
            return t;
        }

        public static object DeserializeRequest(Type type, WebRequest webRequest)
        {
            object obj = null;
            //using (WebResponse response = PclExport.Instance.GetResponse(webRequest))
            //{
            //    obj = JsonSerializer.DeserializeResponse(type, response);
            //}
            return obj;
        }

        #endregion --------------- DESERIALIZE  Request -------------------
        

        #region --------------- DE/SERIALIZE TO/FROM Stream ------------------- 

        /// <summary>
        /// Serialize object data into  stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        public static void SerializeToStream(object value, Stream stream)
        {
            Serialize(stream, value);
        }


        /// <summary>
        /// Serialize {T} object data into  stream.
        /// </summary>
        /// <typeparam name="T"> type of value </typeparam>
        /// <param name="value"> value </param>
        /// <param name="stream"> stream  </param>
        public static void SerializeToStream<T>(T value, Stream stream)
        {
            Serialize(stream, value);
        }

        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object DeserializeFromStream(Type type, Stream stream)
        {
            return Deserialize(stream);
        }



        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T DeserializeFromStream<T>(Stream stream)
        {
            return Deserialize<T>(stream);
        }


        //public static T DeserializeFromStream<T>(Stream stream)
        //{
        //    T t = default(T);
        //    //using (StreamReader streamReader = new StreamReader(stream, JsonSerializer.UTF8Encoding))
        //    //{
        //    //    t = JsonSerializer.DeserializeFromString<T>(streamReader.ReadToEnd());
        //    //}
        //    return t;
        //}

        //public static object DeserializeFromStream(Type type, Stream stream)
        //{
        //    object obj = null;
        //    //using (StreamReader streamReader = new StreamReader(stream, JsonSerializer.UTF8Encoding))
        //    //{
        //    //    obj = JsonSerializer.DeserializeFromString(streamReader.ReadToEnd(), type);
        //    //}
        //    return obj;
        //}


        //public static void SerializeToStream<T>(T value, Stream stream)
        //{
        //if (value == null)
        //{
        //    return;
        //}
        //if (typeof(T) == typeof(object))
        //{
        //    JsonSerializer.SerializeToStream(value, value.GetType(), stream);
        //    return;
        //}
        //if (!typeof(T).IsAbstract() && !typeof(T).IsInterface())
        //{
        //    StreamWriter streamWriter = new StreamWriter(stream, JsonSerializer.UTF8Encoding);
        //    JsonWriter<T>.WriteRootObject(streamWriter, value);
        //    streamWriter.Flush();
        //    return;
        //}
        //JsState.IsWritingDynamic = false;
        //JsonSerializer.SerializeToStream(value, value.GetType(), stream);
        //JsState.IsWritingDynamic = true;
        //}

        public static void SerializeToStream(object value, Type type, Stream stream)
        {
            //analyze

            StreamWriter streamWriter = new StreamWriter(stream, Config.UTF8Encoding);
            
            //JsonWriter.GetWriteFn(type)(streamWriter, value);

            streamWriter.Flush();
        }

        #endregion --------------- DE/SERIALIZE TO/FROM Stream ------------------- 


        #region  --------------- DE/SERIALIZE TO/FROM String ------------------- 
        
        public static string SerializeToString<T>(T data)
        {

            if (data.IsNull() || ((object)data).IsDelegate()) //  
            {
                return null;
            }
            
            if ( typeof(T) == typeof(object))
            {
                return SerializeToString(data, data.GetType());
            }

            var typeinfo = TypeInfoEx.Get(data.GetType());

            if (ContainsTypeSerializer(typeinfo.WorkingType)  == false ) 
            {   // Analyze Can be Serialized and Add all Serialize Types that we need to serialize data 
                // thread-safe action
                AddKnownType(typeinfo.OriginalType);  
            }

            StringWriter stringWriter = Allocate();
            //TypeSerializers[typeinfo.OriginalType].RaiseSerializeToStreamBoxedHandler(data);
            return ReturnAndFree(stringWriter);
              
        }

        public static string SerializeToString(object data, Type type)
        {
            if (data.IsNull() || ((object)data).IsDelegate()) //  
            {
                return null;
            }

            StringWriter stringWriter =  Allocate();
            //TypeSerializers[typeinfo.OriginalType].RaiseSerializeToStreamBoxedHandler(data);
            return ReturnAndFree(stringWriter);            
        }


        public static T DeserializeFromString<T>(string value)
        {
            T t = default(T);
            //if (string.IsNullOrEmpty(value))
            //{
            //    return default(T);
            //}
            //return (T)JsonReader<T>.Parse(value);
            return t;
        }

        public static object DeserializeFromString(string value, Type type)
        {
            

            //if (string.IsNullOrEmpty(value))
            //{
            //    return null;
            //}
            //return JsonReader.GetParseFn(type)(value);
            return null;
        }


        #endregion  --------------- DE/SERIALIZE TO/FROM String ------------------- 


        #region --------------- DE/SERIALIZE TO TextWriter/TextReader ------------------- 

        public static void SerializeToWriter<T>(T value, TextWriter writer)
        {
            if (value == null)
            {
                return;
            }
            //if (typeof(T) == typeof(string))
            //{
            //    JsonUtils.WriteString(writer, (object)value as string);
            //    return;
            //}
            //if (typeof(T) == typeof(object))
            //{
            //    JsonSerializer.SerializeToWriter(value, value.GetType(), writer);
            //    return;
            //}
            //if (!typeof(T).IsAbstract() && !typeof(T).IsInterface())
            //{
            //    JsonWriter<T>.WriteRootObject(writer, value);
            //    return;
            //}
            //JsState.IsWritingDynamic = false;
            //JsonSerializer.SerializeToWriter(value, value.GetType(), writer);
            //JsState.IsWritingDynamic = true;

        }

        public static void SerializeToWriter(object value, Type type, TextWriter writer)
        {
            if (value == null)
            {
                return;
            }
            
            //if (type == typeof(string))
            //{
            //    JsonUtils.WriteString(writer, value as string);
            //    return;
            //}
            //JsonWriter.GetWriteFn(type)(writer, value);

        }


        public static T DeserializeFromReader<T>(TextReader reader)
        {
            return DeserializeFromString<T>(reader.ReadToEnd());
        }

        public static object DeserializeFromReader(TextReader reader, Type type)
        {
            return DeserializeFromString(reader.ReadToEnd(), type);
        }

        #endregion --------------- DE/SERIALIZE TO TextWriter/TextReader -------------------

         


    }
}


#region  -------------------------- GARBAGE -----------------------------

        // Serialize To String
        //if (typeof(T).IsAbstract() || typeof(T).IsInterface())
        //{
        //    JsState.IsWritingDynamic = true;
        //    JsState.IsWritingDynamic = false;
        //    return JsonSerializer.SerializeToString(value, value.GetType());
        //}
        //StringWriter stringWriter = StringWriterThreadStatic.Allocate();
        //if (typeof(T) != typeof(string))
        //{
        //    JsonWriter<T>.WriteRootObject(stringWriter, value);
        //}
        //else
        //{
        //    JsonUtils.WriteString(stringWriter, (object)value as string);
        //}
        //return StringWriterThreadStatic.ReturnAndFree(stringWriter);
        //return null;




//if (typeID > 0) //already contains need no to DetectAddAutoContract
//{
//    if (realType.Is4DPrimitiveType() || realType.IsEnum)
//    {
//        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
//        BinaryReaderWriter.Write(stream, typeID);
//        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);

//    }
//    else
//    {
//        TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);
//    }

//}
//else if (typeID == 0) return; //null value







//internal static bool ContainsTypeSerializer(ushort typeID)
//{
//    return TypeSerializers.ContainsKey(typeID);
//}


//internal static bool ContainsTypeSerializer(ushort typeID)
//{
//    return TypeSerializers.ContainsKey(typeID);
//}

//internal static ITypeSerializerJson GetTypeSerializer(ushort typeID)
//{
//    return TypeSerializers[typeID];//  Current.GetTypeSerializer(typeID);
//}



///// <summary>
///// Check if Serializer contains TypeSerializer for targetType then return its ID.
///// </summary>
///// <param name="targetType"></param>
///// <returns></returns>
//internal static ushort? GetTypeSerializeIDForType(Type targetType)
//{
//    if (targetType.IsNull()) return null;

//    if (TypeSerializerIDByType.ContainsKey(targetType))
//    {
//        return TypeSerializerIDByType[targetType];
//    }
//    return null;
//}


/// <summary>
///  Get Type of data object and return typeID of this Type
/// </summary>
/// <param name="data"></param>
/// <returns></returns>
//internal static ushort GetTypeID(object data)
//{
//    ushort id;

//    if (data.IsNull()) return 0;

//    var type = data.GetType();

//    Validator.AssertFalse<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(type, out id),
//                                                        string.Format("Unknown type {0}", type.FullName)
//                                                    );

//    return id;
//}


/// <summary>
/// Get  typeID of this dataType
/// </summary>
/// <param name="dataType"></param>
/// <returns></returns>
//internal static ushort GetTypeID(Type dataType)
//{
//    ushort id;

//    Validator.AssertFalseDbg<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(dataType, out id),
//                                                     "Unknown type {0}".Fmt(dataType.FullName)
//                                                    );

//    return id;
//}




/// <summary>
/// Get next TypeID for dataType. 
/// If we already  added this dataType earlier then returns 0.
/// Counting begins from 1.
/// </summary>
/// <param name="dataType"></param>
/// <returns></returns>
//internal static ushort GetNextTypeID(Type dataType)
//{
//    if (TypeSerializerIDByType.ContainsKey(dataType)) return 0;

//    return (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);
//}


/// <summary>
/// Getting  Type from runtime type ModelId
/// If type doesn't exist in Serializer.ContractsDictionry then ContractNotFoundException will be thrown
/// </summary>
/// <param name="typeID"></param>
/// <returns></returns>
//internal static Type GetTypeByTypeID(ushort typeID)
//{
//    object findedItem = TypeSerializerIDByType.Where(itm => itm.Value == typeID).FirstOrDefault();

//    Validator.AssertTrueDbg<InvalidOperationException>(findedItem == null,
//                        " Cannot find Type by runtime type ModelId - [{0}]".Fmt(typeID.S())
//                                                   );

//    return ((KeyValuePair<Type, ushort>)findedItem).Key;
//}

#endregion -------------------------- GARBAGE -----------------------------
