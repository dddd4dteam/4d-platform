﻿using System;
using System.Text;

using DDDD.Core.Extensions;
using DDDD.Core.Resources;
using DDDD.Core.Diagnostics;
using System.Collections.Generic;
using DDDD.Core.Reflection;
using System.IO;
using DDDD.Core.Collections;

namespace DDDD.Core.Serialization.Json
{
    public class Json1
    {

        #region ------------------------------ CONSTS ------------------------------


        public const char C_n = 'n';
        public const char C_u = 'u';
        public const char C_l = 'l';
        public const char C_t = 't';
        public const char C_r = 'r';
        public const char C_e = 'e';
        public const char C_f = 'f';
        public const char C_a = 'a';
        public const char C_s = 's';

        public const string NULL = "null";
        public const string TRUE = "true";
        public const string FALSE = "false";

        public const char CARET = '^';        
        public const char QUOTE = '\"';
        public const char COLON = ':';        
        public const char SCOLON = ';';//semicolon
        public const char DOTc = '.';
        public const string DOTs = ".";
        public const char COMMA = ',';
        public const char PIPE = '|';
        public const char QSTN = '?'; //Question
        public const char BSLASH = '\\';//back slash
        public const char FSLASH = '/'; // forward slash
        public const char EQUALS = '='; // 
        public const char STAR = '*';
        public const char SHARP = '#';
        public const char AT = '@';
        


        public const char OBJECT_START = '{';
        public const char OBJECT_END = '}';

        public const char ARRAY_START = '[';
        public const char ARRAY_END = ']';

        public const string EMPTY = "";
        public const string TYPEKEY = "$type";

        public const char N_1 = '1';
        public const char N_2 = '2';
        public const char N_3 = '3';
        public const char N_4 = '4';
        public const char N_5 = '5';
        public const char N_6 = '6';
        public const char N_7 = '7';
        public const char N_8 = '8';
        public const char N_9 = '9';
        public const char N_0 = '0';
        public const char EBig = 'E';   //'E' Big
        public const char eSmall = 'e'; // 'e' Small        
        public const char PLUS = '+';
        public const char DASH = '-';

        public static readonly HashSet<char> NumberStartChars = new HashSet<char>()
        { N_1, N_2, N_3, N_4, N_5, N_6, N_7, N_8, N_9, N_0,  DASH };

        public static readonly HashSet<char> NumberChars = new HashSet<char>()
        {N_1, N_2, N_3, N_4, N_5, N_6, N_7, N_8, N_9, N_0, EBig, eSmall, DOTc, PLUS, DASH };



        /// <summary>
        /// Not nullable Types that will be written in Json.StringType Value syntax.
        /// </summary>
        public static readonly HashSet<Type> StringTypes = new HashSet<Type>()
        {   Tps.T_char , Tps.T_DateTime
          ,Tps.T_TimeSpan, Tps.T_DateTimeOffset, Tps.T_Guid          
        };

        /// <summary>
        ///  Nullable Types that will be written in Json.StringType Value syntax or can have NULL value too.
        /// </summary>
        public static readonly HashSet<Type> StringNullableTypes = new HashSet<Type>()
        {
             Tps.T_string,Tps.T_Uri

             ,Tps.T_charNul , Tps.T_DateTimeNul
             ,Tps.T_TimeSpanNul, Tps.T_DateTimeOffsetNul, Tps.T_GuidNul
        };



        public const char SPACE = ' '; // whitespace
        public const char RETURN = '\r'; // return caret
        public const char NEWLINE = '\n'; // new line
        public const char TAB = '\t'; // tabulation
        public static readonly HashSet<char> FormatChars = new HashSet<char>()
        { SPACE,RETURN,NEWLINE,TAB};

        #endregion ------------------------------ CONSTS ------------------------------


        public static void Write_Quote(TextWriter writer)
        {
            writer.Write(QUOTE);
        }

        #region ----------------------- WRITE JSON OBJECT BEGIN/END -------------------------


        public static void Write_ObjectStart(TextWriter writer)
        {
            writer.Write(OBJECT_START);
        }

        public static void Write_Comma(TextWriter writer)
        {
            writer.Write(COMMA);
        }


        public static void Write_Null(TextWriter writer)
        {
            writer.Write(NULL);
        }



        public static void Write_MemberName(TextWriter writer, string member)
        {
            writer.Write(QUOTE + member + QUOTE + COLON);
        }


        public static void Write_Type(TextWriter writer, string typeAQName)
        {

            Write_MemberName(writer, TYPEKEY );
            writer.Write(typeAQName);
        }


        public static void Write_ObjectEnd(TextWriter writer)
        {
            writer.Write(OBJECT_END);
        }

        #endregion  --------------------------- WRITE JSON OBJECT BEGIN/END ----------------------------



        #region  -------------------------------  WRITE JSON ARRAY BEGIN/END --------------------------------



        public static void Write_ArrayStart(TextWriter writer)
        {
            writer.Write(ARRAY_START);
        }

        public static void Write_ArrayEnd(TextWriter writer)
        {
            writer.Write(ARRAY_END);
        }


        #endregion  -------------------------------  WRITE JSON ARRAY BEGIN/END --------------------------------





        
        #region ---------------------- EAT FORMAT SYMBOLS/ OBJECT CURLS/ ARRAY CURLS -----------------------
        
        // EatFormatSymbols
        // EatObjectCurls
        // EatArrayCurls 

        public static string EatFormatSymbols( string inputSource , ref int runIndex )
        {
            if (FormatChars.NotContains(inputSource[runIndex])) return "";

            int startIndex = runIndex;

            for (int i = runIndex + 1; i < inputSource.Length; i++)
            {
                if (FormatChars.Contains(inputSource[i])) runIndex++;
                else break;
            }

            //return format string
            return inputSource.Substring(startIndex, runIndex - startIndex + 1);            
        }


        ///// <summary>
        ///// Eat object's string  content between ['{'] ans ['}'] Curls.
        ///// </summary>
        ///// <param name="inputSource"></param>
        //public static void EatObjectCurls(ref string inputSource)
        //{
        //    if (inputSource[0] == OBJECT_START && inputSource[inputSource.Length-1] == OBJECT_END )
        //    {
        //        inputSource = inputSource.Substring(1,inputSource.Length - 2);
        //    }
        //}


        ///// <summary>
        ///// Eat array's string  content between ['['] ans [']'] Curls.
        ///// </summary>
        ///// <param name="inputSource"></param>
        //public static void EatArrayCurls(ref string inputSource)
        //{
        //    if (inputSource[0] == ARRAY_START && inputSource[inputSource.Length - 1] == ARRAY_END)
        //    {
        //        inputSource = inputSource.Substring(1, inputSource.Length - 2);
        //    }
        //}




        #endregion ---------------------- EAT FORMAT SYMBOLS/ OBJECT CURLS/ ARRAY CURLS -----------------------


        #region  ------------------------ EAT COLON/ COMMA -----------------------
        // EatColon
        // EatComma


        /// <summary>
        ///  Eat COLON symbol with SPACES BEFORE AND AFTER if they exists.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>     
        /// <returns></returns>
        public static string EatColon(string inputSource, ref int runIndex)
        {
            //return format string
            EatFormatSymbols( inputSource,ref runIndex);
                      
            Validator.ATInvalidOperation(inputSource[runIndex] != COLON, nameof(Json1), nameof(EatColon)
                        , " Unexpected symbol-[{0}] instead of COLON-[':'] symbol ".Fmt(inputSource[runIndex].S()) );

            runIndex++;
            
            return COLON.S();
        }


        /// <summary>
        /// Eat COMMA symbol with SPACES BEFORE AND AFTER if they exists. 
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string EatComma(string inputSource, ref int runIndex)
        {
            EatFormatSymbols( inputSource, ref runIndex);

            Validator.ATInvalidOperation(inputSource[runIndex] != COMMA, nameof(Json1), nameof(EatComma)
                        , " Unexpected symbol-[{0}] instead of COMMA -[','] symbol ".Fmt(inputSource[runIndex].S()));

            runIndex++;

            //eat one COMMA SYMBOL
            return COMMA.S();
        }



        #endregion ------------------------ EAT COLON/ COMMA -----------------------



        #region ---------------------- EAT VALUES: NULL/NUMBER/STRING/BOOL ---------------------------


        // EatNull        
        // EatStringValue 
        // EatBoolVal
        // EatNumberVal


        /// <summary>
        /// Eat nullval
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string EatNull(string inputSource, ref int runIndex)
        {

            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] != C_n) return EMPTY;

            if (inputSource.Length >= runIndex +3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] == C_l  &&
                       inputSource[runIndex + 3] == C_l)
            {
                runIndex += 4;
                //inputSource = inputSource.Remove(0, 4);
            }

            return NULL;
        }


        /// <summary>
        /// Eat json string Value only. Spaces/other symbols will be ignored.
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string EatStringValue( string inputSource, ref int runIndex)
        {

            EatFormatSymbols(inputSource, ref runIndex);

            int startIndex = -1; int endIndex = -1;
            var bytedString = "";

            // get start index
            for (int i = runIndex; i < inputSource.Length; i++)
            {
                if (inputSource[i] == QUOTE)
                {
                    startIndex = i; break;
                }
            }

            Validator.ATInvalidOperationDbg( (startIndex == -1), nameof(Json1), nameof(EatStringValue)
                                              , " String Value even was not started");

            // get end index            
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == QUOTE)
                {
                    endIndex = i; break;
                }
            }

            Validator.ATInvalidOperationDbg((endIndex == -1), nameof(Json1), nameof(EatStringValue)
                                                , " String Value was not Closed Correctly");

            // check end Index was found  . here we need to use inclusive eating
            if ( endIndex > 0  && endIndex > startIndex )
            {
                runIndex = endIndex + 1;                
                bytedString = inputSource.Substring(startIndex, (endIndex - startIndex + 1));                
            }

            return bytedString;
        }

        public static string EatStringContentOnlyValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            int startIndex = -1; int endIndex = -1;
            var bytedString = "";

            // get start index
            for (int i = runIndex; i < inputSource.Length; i++)
            {
                if (inputSource[i] == QUOTE)
                {
                    startIndex = i; break;
                }
            }

            Validator.ATInvalidOperationDbg((startIndex == -1), nameof(Json1), nameof(EatStringValue)
                                              , " String Value even was not started");

            // get end index            
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == QUOTE)
                {
                    endIndex = i; break;
                }
            }

            Validator.ATInvalidOperationDbg((endIndex == -1), nameof(Json1), nameof(EatStringValue)
                                                , " String Value was not Closed Correctly");

            // check end Index was found  . here we need to use inclusive eating
            if (endIndex > 0 && endIndex > startIndex)
            {
                runIndex = endIndex + 1;
                bytedString = inputSource.Substring(startIndex+1, (endIndex - startIndex - 1 ));
            }

            return bytedString;
        }

        public static string EatMemberName(string inputSource, ref int runIndex)
        {
            return EatStringContentOnlyValue(inputSource, ref runIndex);
        }


        /// <summary>
        /// Eat Boolean Value
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>
        public static string EatBoolValue( string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] == C_t )
            {
                if (inputSource.Length >= 4 &&
                       inputSource[runIndex + 1] == C_r  &&
                       inputSource[runIndex + 2] == C_u &&
                       inputSource[runIndex + 3] == C_e )
                {
                    runIndex += 4;                     
                    return TRUE;
                }
            }

            if (inputSource[0] == C_f)
            {
                if (inputSource.Length >= 5 &&
                       inputSource[1] == C_a &&
                       inputSource[2] == C_l &&
                       inputSource[3] == C_s &&
                       inputSource[3] == C_e 
                       )
                {
                    runIndex += 5;                    
                    return FALSE;
                }
            }

            return "";
        }


        /// <summary>
        /// Eat Number Value
        /// </summary>
        /// <param name="inputSource"></param>
        /// <param name="runIndex"></param>
        /// <returns></returns>

        public static string EatNumberValue( string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (NumberStartChars.NotContains(inputSource[runIndex])) return "";

            int startIndex = runIndex;
            for (int i = runIndex+1; i < inputSource.Length; i++)
            {
                if (NumberChars.Contains(inputSource[i])) runIndex++;
                else break;
            }
                        
            string bytedString = inputSource.Substring(startIndex, (runIndex - startIndex + 1) );            

            return bytedString;
        }
         

        #endregion ---------------------- EAT VALUES: NULL/NUMBER/STRING/BOOL ---------------------------



        #region ----------------------- EAT OBJECT/ OBJECT MEMBERS --------------------------

        // EatObjectVal 
        // EatObjects_BoolMember
        // EatObjects_NumberMember - 
        // EatObjects_StringMember - can be null too
        // EatObjects_ObjectMember
         

        public static string EatObjectValue(string inputSource , ref int runIndex)
        {

            EatFormatSymbols(inputSource, ref runIndex);

            string bytedString = null;
            int startIndex = 0; int endIndex = 0;

            // get begin index
            for (int i = 0; i < inputSource.Length; i++)
            {
                if (inputSource[i] == OBJECT_START)
                {
                    startIndex = i; break;
                }
            }

            // get end index
            int deep = 1;
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == OBJECT_START) { deep++; continue; }

                if (inputSource[i] == OBJECT_END && deep == 1)
                { endIndex = i; break; }
                else if (inputSource[i] == OBJECT_END) deep--;
            }

            // check end Index was found 
            if (endIndex > 0 && endIndex > startIndex)
            {
                var charCount = (endIndex - startIndex + 1);
                bytedString = inputSource.Substring(startIndex, charCount);
            }

            return bytedString;
        }


        public static string EatObjectSTART(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);
            if (inputSource[runIndex] == OBJECT_START) { runIndex++; return OBJECT_START.S(); }
            else return EMPTY;
        }

        public static string EatObjectEND(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);
            if (inputSource[runIndex] == OBJECT_END) { runIndex++; return OBJECT_END.S(); }
            else return EMPTY;
        }

        public static string EatObjectContentOnlyValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            string bytedString = null;
            int startIndex = 0; int endIndex = 0;

            // get begin index
            for (int i = 0; i < inputSource.Length; i++)
            {
                if (inputSource[i] == OBJECT_START)
                {
                    startIndex = i; break;
                }
            }

            // get end index
            int deep = 1;
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == OBJECT_START) { deep++; continue; }

                if (inputSource[i] == OBJECT_END && deep == 1)
                { endIndex = i; break; }
                else if (inputSource[i] == OBJECT_END) deep--;
            }

            // check end Index was found 
            if (endIndex > 0 && endIndex > startIndex)
            {
                var charCount = (endIndex - startIndex - 1);
                bytedString = inputSource.Substring(startIndex + 1, charCount);
            }

            return bytedString;
        }


        public static string EatObjects_BoolMember (string inputSource, string member , ref int runIndex)
        {
            var memberReaded = EatStringContentOnlyValue(inputSource, ref runIndex);

            Validator.ATInvalidOperationDbg((memberReaded != (QUOTE + member + QUOTE)), nameof(Json1), nameof(EatObjects_BoolMember)
                     , "Member Name mismatch Readed[{0}] but Waited[{1}]".Fmt(memberReaded, member));

            EatColon(inputSource,ref runIndex);

            var boolValue = EatBoolValue(inputSource, ref runIndex);

            return boolValue;

        }

        #endregion ----------------------- EAT OBJECT/ OBJECT MEMBERS --------------------------
         

        #region --------------------------- EAT ARRAY ----------------------------

        // EatArray
        // EatArrays_Element - for different Dimensions
        //  -EatArrays_BoolElement
        //  -EatArrays_NumberElement
        //  -EatArrays_StringElement
        //  -EatArrays_ObjectElement

        public static string EatArrayValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            string bytedString = null;
            int startIndex = 0; int endIndex = 0;

            // get begin index
            for (int i = 0; i < inputSource.Length; i++)
            {
                if (inputSource[i] == ARRAY_START)
                {
                    startIndex = i; break;
                }
            }

            // get end index
            int deep = 1;
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == ARRAY_START) { deep++; continue; }

                if (inputSource[i] == ARRAY_END && deep == 1)
                { endIndex = i; break; }
                else if (inputSource[i] == ARRAY_END) deep--;
            }

            // check end Index was found 
            if (endIndex > 0 && endIndex > startIndex)
            {
                var charCount = (endIndex - startIndex + 1);
                bytedString = inputSource.Substring(startIndex, charCount);
            }

            return bytedString;
        }

        public static string EatArrayContentOnlyValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            string bytedString = null;
            int startIndex = 0; int endIndex = 0;

            // get begin index
            for (int i = 0; i < inputSource.Length; i++)
            {
                if (inputSource[i] == ARRAY_START)
                {
                    startIndex = i; break;
                }
            }

            // get end index
            int deep = 1;
            for (int i = (startIndex + 1); i < inputSource.Length; i++)
            {
                if (inputSource[i] == ARRAY_START) { deep++; continue; }

                if (inputSource[i] == ARRAY_END && deep == 1)
                { endIndex = i; break; }
                else if (inputSource[i] == ARRAY_END) deep--;
            }

            // check end Index was found 
            if (endIndex > 0 && endIndex > startIndex)
            {
                var charCount = (endIndex - startIndex -1);
                bytedString = inputSource.Substring(startIndex + 1, charCount);
            }

            return bytedString;
        }


        public static string EatArraySTART(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);
            if (inputSource[runIndex] == ARRAY_START) { runIndex++; return ARRAY_START.S(); }
            else return EMPTY;
        }

        public static string EatArrayEND(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);
            if (inputSource[runIndex] ==  ARRAY_END) { runIndex++; return  ARRAY_END.S(); }
            else return EMPTY;
        }

        #endregion --------------------------- EAT ARRAY ----------------------------



        #region ---------------------- EAT UNKNOWN JSON TYPE ----------------------------- 

        public static KeyValuePair<JsonValueTypeEn, string> EatUnknownJsonTypeValue(string inputSource, ref int runIndex)
        {
            var result = new KeyValuePair<JsonValueTypeEn, string>();

            string eatValue = null; int runIndexStart = runIndex;
            // NULL
            // BOOL 
            // NUMBER
            // STRING
            // OBJECT
            // ARRAY
            if ((eatValue = EatNull(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.Null, eatValue);
            }
            else runIndex = runIndexStart;
            if ((eatValue = EatBoolValue(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.Boolean, eatValue);
            }
            else runIndex = runIndexStart;
            if ((eatValue = EatNumberValue(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.Number, eatValue);
            }
            else runIndex = runIndexStart;

            if ((eatValue = EatStringValue(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.String, eatValue);
            }
            else runIndex = runIndexStart;

            if ((eatValue = EatObjectValue(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.Object, eatValue);
            }
            else runIndex = runIndexStart;

            if ((eatValue = EatArrayValue(inputSource, ref runIndex)) != EMPTY)
            {
                return new KeyValuePair<JsonValueTypeEn, string>(JsonValueTypeEn.Array, eatValue);
            }
            else runIndex = runIndexStart;

            return result;
        }

        #endregion --------------------- EAT UNKNOWN JSON TYPE -----------------------------

        #region -------------------------- EAT SPLIT ARRAY ELEMENTS -------------------------

        // EatSplitArrayElements
        // EatSplit2DArrayElements
        // EatSplit3DArrayElements
        // EatSplit4DArrayElements

        public static List<string> EatSplitArrayElements(string inputSource, JsonValueTypeEn jsonValueType)
        {
            var result = new List<string>(); var runIndex = 0;

            EatArraySTART(inputSource, ref runIndex);

            switch (jsonValueType)
            {
                case JsonValueTypeEn.NotDefined:
                    return result;
                case (JsonValueTypeEn.Boolean| JsonValueTypeEn.Null) :
                    {
                        while (runIndex < inputSource.Length-1)
                        {
                            var resultItem = EatNullOrBoolValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;
                    }
                case JsonValueTypeEn.Boolean:
                    {
                        while (runIndex < inputSource.Length - 1)
                        {
                            var resultItem = EatBoolValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;
                    }
                case (JsonValueTypeEn.Number | JsonValueTypeEn.Null):
                    {
                        while (runIndex < inputSource.Length - 1)
                        {
                            var resultItem = EatNullOrNumberValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;
                    }
                case JsonValueTypeEn.String| JsonValueTypeEn.Null :
                    {
                        while (runIndex < inputSource.Length - 1)
                        {
                            var resultItem = EatNullOrStringContentOnlyValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;
                    }                    
                case JsonValueTypeEn.Object| JsonValueTypeEn.Null:
                    {
                        while (runIndex < inputSource.Length-1)
                        {
                            var resultItem = EatNullOrObjectValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;
                    }
                case JsonValueTypeEn.Array| JsonValueTypeEn.Null :
                    {
                        while (runIndex < inputSource.Length - 1)
                        {
                            var resultItem = EatNullOrArrayValue(inputSource, ref runIndex);
                            //if resultItem != NULL throw invalidOperationExeption
                            result.Add(resultItem);
                            EatComma(inputSource, ref runIndex);
                        }
                        break;                        
                    }
                default:    return result;
            }

            EatArrayEND(inputSource, ref runIndex);

            return result;
        }

        public static List<KeyValuePair<JsonValueTypeEn, string> > EatSplitArrayOfUnknownElements(string inputSource, JsonValueTypeEn jsonValueType)
        {
            var result = new List<KeyValuePair<JsonValueTypeEn, string>>(); var runIndex = 0;

            EatArraySTART(inputSource, ref runIndex);
            
            while (runIndex < inputSource.Length - 1)
            {
                var resultItem = EatUnknownJsonTypeValue(inputSource, ref runIndex);
                //if resultItem != NULL throw invalidOperationExeption
                result.Add(resultItem);
                EatComma(inputSource, ref runIndex);
            }                
            
            EatArrayEND(inputSource, ref runIndex);

            return result;
        }


        public static List2D<string> EatSplit2DArrayElements(string inputSource,JsonValueTypeEn jsonValueType)
        {
            var result = new List2D<string>();
            var runIndex = 0;

            switch (jsonValueType)
            {
                case JsonValueTypeEn.NotDefined:
                    return result;
                case (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null):
                    {
                        
                        break;
                    }
                case JsonValueTypeEn.Boolean:
                    { 
                        break;
                    }
                case (JsonValueTypeEn.Number | JsonValueTypeEn.Null):
                    {
                       
                        break;
                    }
                case JsonValueTypeEn.String | JsonValueTypeEn.Null:
                    {
                        
                        break;
                    }
                case JsonValueTypeEn.Object | JsonValueTypeEn.Null:
                    {
                        
                        break;
                    }
                case JsonValueTypeEn.Array | JsonValueTypeEn.Null:
                    {
                         
                        break;
                    }
                default: return result;
            }

            return result;
        }


        public static List3D<string> EatSplit3DArrayElements(string inputSource, JsonValueTypeEn jsonValueType)
        {
            var result = new List3D<string>();
            switch (jsonValueType)
            {
                case JsonValueTypeEn.NotDefined:
                    return result;
                case (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null):
                    {

                        break;
                    }
                case JsonValueTypeEn.Boolean:
                    {
                        break;
                    }
                case (JsonValueTypeEn.Number | JsonValueTypeEn.Null):
                    {

                        break;
                    }
                case JsonValueTypeEn.String | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                case JsonValueTypeEn.Object | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                case JsonValueTypeEn.Array | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                default: return result;
            }



            return result;
        }


        public static List4D<string> EatSplit4DArrayElements(string inputSource, JsonValueTypeEn jsonValueType)
        {
            var result = new List4D<string>();
            switch (jsonValueType)
            {
                case JsonValueTypeEn.NotDefined:
                    return result;
                case (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null):
                    {

                        break;
                    }
                case JsonValueTypeEn.Boolean:
                    {
                        break;
                    }
                case (JsonValueTypeEn.Number | JsonValueTypeEn.Null):
                    {

                        break;
                    }
                case JsonValueTypeEn.String | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                case JsonValueTypeEn.Object | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                case JsonValueTypeEn.Array | JsonValueTypeEn.Null:
                    {

                        break;
                    }
                default: return result;
            }


            return result;
        }



        #endregion -------------------------- EAT SPLIT ARRAY ELEMENTS -------------------------




        #region ----------------- EAT NULL OR BOOL/NUMBER/STRING/OBJECT/ARRAY -------------------


        public static string EatNullOrBoolValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] == C_n )
            {// and n-ull
                if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] ==  C_l &&
                       inputSource[runIndex + 3] == C_l)
                {
                    runIndex += 4;
                    return NULL;                    
                }
                throw new InvalidOperationException("Error in  Json1..EatNullOrBoolValue() : Incorrect null value string  ");
            }
            else
            {            
               return EatBoolValue(inputSource, ref runIndex);
            }
                       
        }

        public static string EatNullOrNumberValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] == C_n)
            {// and n-ull
                if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] == C_l &&
                       inputSource[runIndex + 3] == C_l)
                {
                    runIndex += 4;
                    return NULL;
                }
                throw new InvalidOperationException("Error in  Json1.EatNullOrNumberValue() : Incorrect null value string  ");
            }
            else
            {
                return EatNumberValue(inputSource, ref runIndex);
            }
        }


        public static string EatNullOrStringContentOnlyValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] == C_n)
            {// and n-ull
                if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] == C_l &&
                       inputSource[runIndex + 3] == C_l)
                {
                    runIndex += 4;
                    return NULL;
                }
                throw new InvalidOperationException("Error in  Json1..EatNullOrStringValue() : Incorrect null value string  ");
            }
            else
            {
                return EatStringContentOnlyValue(inputSource, ref runIndex);

            }
        }

        public static string EatNullOrObjectValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] ==  C_n)
            {// and n-ull
                if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] == C_l &&
                       inputSource[runIndex + 3] == C_l )
                {
                    runIndex += 4;
                    return NULL;
                }

                throw new InvalidOperationException(
                    "Error in [{0}].[{1}]()  : Incorrect null value string  ".Fmt(nameof(Json1), nameof(EatNullOrObjectValue) ) );
            }
            else
            {
                return EatObjectValue(inputSource, ref runIndex);
            }
        }

        public static string EatNullOrArrayValue(string inputSource, ref int runIndex)
        {
            EatFormatSymbols(inputSource, ref runIndex);

            if (inputSource[runIndex] == C_n)
            {// and n-ull
                if (inputSource.Length >= runIndex + 3 &&
                       inputSource[runIndex + 1] == C_u &&
                       inputSource[runIndex + 2] == C_l &&
                       inputSource[runIndex + 3] == C_l)
                {
                    runIndex += 4;
                    return NULL;
                }
                throw new InvalidOperationException(
                    "Error in [{0}].[{1}]()  : Incorrect null value string  ".Fmt(nameof(Json1),nameof(EatNullOrArrayValue) )  );
            }
            else
            {
                return EatArrayValue(inputSource, ref runIndex);
            }
        }


        #endregion ----------------- EAT NULL OR BOOL/NUMBER/STRING/OBJECT/ARRAY -------------------





        #region ------------------- JSON VALUE TYPE -------------------------

        /// <summary>
        /// Get JsonValueType(ECMA-404 Standart JSON Value Type) for targetType
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static JsonValueTypeEn GetJsonValueType(TypeInfoEx targetType)
        {
            var result = JsonValueTypeEn.NotDefined;

            if (targetType.OriginalType == Tps.T_boolNul )
                result = (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null);

            else if (targetType.OriginalType == Tps.T_bool)
                result = JsonValueTypeEn.Boolean;

            else if (StringTypes.Contains(targetType.OriginalType))
                result = (JsonValueTypeEn.String );

            else if (StringNullableTypes.Contains(targetType.OriginalType))
                result = (JsonValueTypeEn.String|JsonValueTypeEn.Null);

            else if (TypeInfoEx.NumberNullableTypes.Contains(targetType.OriginalType))
                result = (JsonValueTypeEn.Number | JsonValueTypeEn.Null);

            else if (TypeInfoEx.NumberNotNullableTypes.Contains(targetType.OriginalType))
                result = JsonValueTypeEn.Number;

            else if (targetType.OriginalType.IsArray
                //|| targetType.OriginalType == Tps.T_byteArray
                //|| targetType.OriginalType == Tps.T_BitArray
                || Tps.T_IEnumerable.IsAssignableFrom(targetType.OriginalType)
                )
                result = (JsonValueTypeEn.Array | JsonValueTypeEn.Null);

            else if (targetType.OriginalType.IsValueType
              ) return (JsonValueTypeEn.Object);

            else if (targetType.OriginalType == Tps.T_object)
                return (JsonValueTypeEn.Unknown);

            else if (  (targetType.OriginalType.IsClass && !targetType.OriginalType.IsAbstract)
                     || (targetType.OriginalType.IsValueType && targetType.IsNullabeType)
                ) return (JsonValueTypeEn.Object | JsonValueTypeEn.Null);
            
            else if ( (targetType.OriginalType.IsClass && targetType.OriginalType.IsAbstract)
                     || targetType.OriginalType.IsInterface
                ) return JsonValueTypeEn.Unknown;
            

            else // for interndfaces // abstract classes 
                result = JsonValueTypeEn.NotDefined;

            return result;

        }



        public static SDJson.EatValueByJsonValueType GetEatValueHandlerByType(JsonValueTypeEn jsonValueType)
        {

            if (jsonValueType == (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null))
            {
                return EatNullOrBoolValue;
            }
            else if (jsonValueType == JsonValueTypeEn.Boolean)
            {
                return EatBoolValue;
            }
            else if (jsonValueType == (JsonValueTypeEn.Number | JsonValueTypeEn.Null))
            {
                return EatNullOrNumberValue;
            }
            else if (jsonValueType == JsonValueTypeEn.Number)
            {
                return EatNumberValue;
            }
            else if (jsonValueType == (JsonValueTypeEn.String | JsonValueTypeEn.Null))
            {
                return EatNullOrStringContentOnlyValue;
            }
            else if (jsonValueType == JsonValueTypeEn.String)
            {
                return EatStringContentOnlyValue;
            }
            else if (jsonValueType == (JsonValueTypeEn.Object | JsonValueTypeEn.Null))
            {
                return EatNullOrObjectValue;
            }
            else if (jsonValueType ==JsonValueTypeEn.Object )
            {
                return EatObjectValue;
            }
            else if (jsonValueType == (JsonValueTypeEn.Array | JsonValueTypeEn.Null))
            {
                return EatNullOrArrayValue;
            }
            else if (jsonValueType == (JsonValueTypeEn.Unknown))
            {   //Unknown do not have ready EatyMethod
                //return EatUnknownJsonTypeValue;
                return null;
            }

            return null;
        }



        #endregion -------------------  JSON VALUE TYPE -------------------------




    }
}
