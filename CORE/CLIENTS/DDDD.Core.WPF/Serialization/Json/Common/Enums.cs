﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Json value that contains in Json ecma-404 standart
    /// </summary>
    [Flags]
    public enum JsonValueTypeEn
    {

        NotDefined = 2
        ,
         /// <summary>
         /// null const value
         /// </summary>
         Null = 4
        ,
        /// <summary>
        ///  true and false keywords
        /// </summary>
        Boolean = 8
        ,
        /// <summary>
        /// Number cahrs, plus minus, dot of float point value, e and E symbols of float point value 
        /// </summary>
        Number = 16
        ,
          /// <summary>
          /// string value between  two \"  symbols.
          /// </summary>
        String = 32
        ,
          /// <summary>
          /// value of object between two { and } curls
          /// </summary>
        Object = 64
        ,
        /// <summary>
        /// value of an array- between two or more [ and ] curls 
        /// </summary>
        Array = 128
        ,
        /// <summary>
        /// Created for Polymorphism Aspect of object Language, when  value System.Type can be System.object/abstract class/or interface.. 
        /// </summary>
        Unknown = 256
    }

   
}
