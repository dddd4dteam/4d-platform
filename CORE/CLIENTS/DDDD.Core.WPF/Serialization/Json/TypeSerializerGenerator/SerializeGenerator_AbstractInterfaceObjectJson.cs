﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;

using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;
using System.ComponentModel;

namespace DDDD.Core.Serialization.Json
{
    /// <summary>
    /// Type Serialize Generator for interfaces types and object type.
    /// </summary>
    internal class SerializeGenerator_AbstractInterfaceObjectJson : ITypeSerializerGeneratorJson
    {


        private readonly static object locker = new object();
      


        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_Interfaces_Object:
        ///     - all interfaces and object type;
        ///     -interfaces can't be IList or IDictionary
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            // abstract classes , all inherited enable classes?  
            return TypeSerializeAnalyser.CanHandle_AbstractObjectInterfaceType(type);           
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        public static object DeserializeFromStringJsonUnknownValue(string inputSource, ref int runIndex)
        {
            var itemUnknownValue = Json1.EatUnknownJsonTypeValue(inputSource, ref runIndex);
            int objectIndex = 0;
            

            switch (itemUnknownValue.Key)
            {

                case (JsonValueTypeEn.Boolean | JsonValueTypeEn.Null):
                {
                    if (itemUnknownValue.Value == Json1.NULL) return null;
                    else  return bool.Parse(itemUnknownValue.Value);                   
                   
                }
                case JsonValueTypeEn.Boolean:
                {
                    return bool.Parse(itemUnknownValue.Value);
                }
                case  (JsonValueTypeEn.Number | JsonValueTypeEn.Null):
                {
                        if (itemUnknownValue.Value == Json1.NULL) return null;                        
                        if (itemUnknownValue.Value.Contains(Json1.DOTs)  )
                        {                           
                           return double.Parse(itemUnknownValue.Value);
                        }
                        else
                            return int.Parse(itemUnknownValue.Value);
                    }
                case  JsonValueTypeEn.Number:
                {
                        if (itemUnknownValue.Value.Contains(Json1.DOTs))
                        {
                            return double.Parse(itemUnknownValue.Value);
                        }
                        else
                            return int.Parse(itemUnknownValue.Value);
                }
                case  (JsonValueTypeEn.String | JsonValueTypeEn.Null):
                {
                        if (itemUnknownValue.Value == Json1.NULL) return null;
                        else return itemUnknownValue.Value;                       
                }
                case JsonValueTypeEn.String:
                {
                        return itemUnknownValue.Value;
                }
                case  (JsonValueTypeEn.Object | JsonValueTypeEn.Null):
                {
                    if (itemUnknownValue.Value == Json1.NULL) return null;
                        return DeserializeFromStringJsonObjectValue(inputSource, ref objectIndex);
                       // return null; // here $type redirection TO DO
                }
                case JsonValueTypeEn.Object:
                {
                        return DeserializeFromStringJsonObjectValue(inputSource, ref objectIndex);
                        //return null; // here $type redirection TO DO
                 }
                case (JsonValueTypeEn.Array | JsonValueTypeEn.Null):
                {
                        if (itemUnknownValue.Value == Json1.NULL) return null;
                        return DeserializeFromStringJsonArrayValue(inputSource, ref objectIndex);

                        //return null; // here $type redirection TO DO
                }
                case JsonValueTypeEn.Unknown:
                {
                        throw new InvalidOperationException("ERROR : Object's JsonValue Type was not Determined");
                }
                default:
                    throw new InvalidOperationException("ERROR : Object's JsonValue Type was not Determined");
            }

            
        }



        public static object DeserializeFromStringJsonObjectValue(string inputSource, ref int runIndex)
        {

            //$type member             
            var memberKey = Json1.EatStringContentOnlyValue(inputSource, ref runIndex);

            Validator.ATInvalidOperation((memberKey != Json1.TYPEKEY), nameof(Json1), nameof(DeserializeFromStringJsonObjectValue)
                                        , " Can't find $type field. ");

            Json1.EatColon(inputSource, ref runIndex);
            var typeMemberValue = Json1.EatStringContentOnlyValue(inputSource, ref runIndex);
            var objectTypeInf = TypeInfoEx.Get(typeMemberValue);
            Validator.ATInvalidOperation((objectTypeInf.State == TypeFoundStateEn.NotFounded), nameof(Json1), nameof(DeserializeFromStringJsonObjectValue)
                                        , " Type with Name [{0}] was not found ".Fmt(typeMemberValue));

            //redirect to founded type serializer
            var result = JsonSerializer.GetTypeSerializer(objectTypeInf.OriginalType)
                .RaiseDeserializeFromStringBoxedHandler(inputSource, ref runIndex);

            Json1.EatComma(inputSource, ref runIndex);

            return result;

        }


        public static object DeserializeFromStringJsonArrayValue(string inputSource, ref int runIndex)
        {
            return null;
        }

        #region ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------



        public static void Write_object_ToString<T>(TextWriter writer, T instanceValue)
        {
            // switch to real type
            if (instanceValue == null) {  writer.Write( Json1.NULL); return; }

            var valuetype = instanceValue.GetType();


            /// locked code: for auto detecting and adding Contracts 
            locker.LockAction(() => (!JsonSerializer.ContainsTypeSerializer(valuetype))
            , () =>
            {
                JsonSerializer.AddKnownType(valuetype);
            }
            );

            JsonSerializer.GetTypeSerializer(valuetype).RaiseSerializeToStringBoxedHandler(writer, instanceValue, needToWriteTypeAQName: true);

        }




        public static void Write_Interface_ToStream<T>(ITypeSerializerJson<T> typeSerializer, Stream stream, T instanceValue)
        {
            //switch to real type
            //if (instanceValue == null) { BinaryReaderWriter.WriteT_uint16(stream, (ushort)0); return; }

            ////for auto detecting and adding Contracts -Collection Contact 
            //var valuetype = instanceValue.GetType();
            //if (typeof(IEnumerable).IsAssignableFrom(valuetype) )//can't be enum -so need not to check here
            //{
            //    JsonSerializer.AddKnownType(valuetype);            
            //}

            ////var realTypeID = ;// GetTypeID(instanceValue.GetType());
            //BinaryReaderWriter.Write(stream, typeSerializer.TypeID);
            //typeSerializer.RaiseElementSerializeToStreamBoxedHandler(typeSerializer.TypeID, stream, instanceValue); // realTypeID
        }


        public static object Read_Interface_FromStream<T>(ITypeSerializerJson<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            //if (readedTypeID == false)
            //{
            //    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
            //    if (typeId == 0U) //is null
            //    {
            //        return null;//just the same as return null  for reference types
            //    }

            //    if (typeId != typeSerializer.TypeID)
            //    {
            //      return typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeId, stream, true);//yes we red your typeID
            //    }
            //}

            //readed and not null - can be only object() - we won't switch somewhere else
            return default(T);
        }


        public static void Write_object_ToStream<T>(ITypeSerializerJson<T> typeSerializer, Stream stream, T instanceValue)
        {
            //switch to real type
            //if (instanceValue == null) { BinaryReaderWriter.Write(stream, (ushort)0); return; }
            
            ////for auto detecting and adding Contracts 
            //var valuetype = instanceValue.GetType();
            //if (valuetype.IsEnum || typeof(IEnumerable).IsAssignableFrom(valuetype))
            //{
            //    //typeProcessor.SetSerializer.DetectAddAutoContract(valuetype);
            //    //typeProcessor.SetSerializer.BuildTypeProcessor ();
            //}
            
            ////var realTypeID = typeProcessor.GetTypeID(instanceValue.GetType());
            //BinaryReaderWriter.Write(stream, typeSerializer.TypeID ); // realTypeID
            //typeSerializer.RaiseElementSerializeToStreamBoxedHandler( typeSerializer.TypeID , stream, instanceValue);
        }

       
       


        public static object Read_object_FromStream<T>(ITypeSerializerJson<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            //if (readedTypeID == false)
            //{
            //    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
            //    if (typeId == 0U) //is null
            //    {
            //        return null;//just the same as return null  for reference types
            //    }

            //    if (typeId != typeSerializer.TypeID)
            //    {
            //     return typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeId, stream, true);//yes we red your typeID
            //    }
            //}

            //readed and not null - can be only object() - we won't switch somewhere else
            return default(T);
        }


        #endregion ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------




        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer) //
        {
            try
            {
                //algorithm:            
                // void  Write_object_ToString<T>(  TextWriter writer, T instanceValue)
                var typeInfo = TypeInfoEx.Get(typeof(T));                              
                var parameterWriter = Expression.Parameter(Tps.T_TextWriter, "writer");
                var parameterInputData = Expression.Parameter(typeInfo.OriginalType , "data");
                var parameterNeedToWriteTypeAQName = Expression.Parameter(Tps.T_bool, "needToWriteTypeAQName");


                var methodWrite_Object_ToString = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                        .GetMethod(nameof(Write_object_ToString))
                                                        .MakeGenericMethod(typeInfo.OriginalType);


                return Expression.Lambda<SDJson.SerializeToStringT<T>>(
                                                       Expression.Call(  methodWrite_Object_ToString
                                                                       , parameterWriter
                                                                       , parameterInputData
                                                                       )
                                                        , typeInfo.TypeDebugCodeConventionName + "SerializeToString_Write"
                                                       , new[] { parameterWriter, parameterInputData, parameterNeedToWriteTypeAQName }
                                                       );                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateSerialize_WriteExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }


        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() 
        {
            try
            {
               
                var parameterInpurSourceString = Expression.Parameter(Tps.T_string, "inputSource");
                var parameterRunIndexRef = Expression.Parameter(Tps.T_int.MakeByRefType(), "runIndexByRef");
               
                var methodDeseriializeFromString = typeof(SerializeGenerator_AbstractInterfaceObjectJson )
                                                        .GetMethod(nameof(DeserializeFromStringJsonUnknownValue));
                
                var deserializeCall = Expression.Call(methodDeseriializeFromString                                    
                                    , parameterInpurSourceString
                                    , parameterRunIndexRef);
                

                return Expression.Lambda<SDJson.DeserializeFromStringT<T>>
                                                (deserializeCall
                                                   , "DeserializeFromStringT"
                                                   , new[] { parameterInpurSourceString, parameterRunIndexRef }
                                                );

            }
            catch (System.Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateDeserializeFromString_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
          
        }








        #region -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            try
            {
                //algorithm:            
                // void Write_Interface_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)
                // void Write_object_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                if (typeof(T).IsInterface)
                {

                    var methodWrite_Interface_ToStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                 .GetMethod(nameof(Write_Interface_ToStream))
                                                                                 .MakeGenericMethod(tpSerializer.TargetType);


                    return Expression.Lambda<SDJson.SerializeToStreamT<T>>(
                                                           Expression.Call(
                                                             //typeProcessorInstance
                                                             methodWrite_Interface_ToStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                            , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                           , new[] { parameterStream, parameterInputData }
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {

                    var methodWrite_object_ToStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                  .GetMethod(nameof(Write_object_ToStream))
                                                                                  .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SDJson.SerializeToStreamT<T>>(
                                                           Expression.Call(
                                                             methodWrite_object_ToStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                           , new[] { parameterStream, parameterInputData }
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateSerialize_WriteExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }

          
        }


        /// <summary>
        ///  Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamT<T>> GenerateDeserialize_ReadExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            try
            {
                //algorithm:
                // object Read_Interface_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
                // object Read_object_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                if (typeof(T).IsInterface)
                {

                    var methodRead_Interface_FromStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                    .GetMethod(nameof(Read_Interface_FromStream))
                                                                                    .MakeGenericMethod(tpSerializer.TargetType);

                    return Expression.Lambda<SDJson.DeserializeFromStreamT<T>>(
                                                           Expression.Call(
                                                             methodRead_Interface_FromStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] { parameterStream, parameterReadedTypeID }
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {


                    var methodRead_object_FromStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                     .GetMethod(nameof(Read_object_FromStream))
                                                                                     .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SDJson.DeserializeFromStreamT<T>>(
                                                           Expression.Call(
                                                             methodRead_object_FromStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                            , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] { parameterStream, parameterReadedTypeID }
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateDeserialize_ReadExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }

          
        }

        
        #endregion -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:            
                // void Write_Interface_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)
                // void Write_object_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");


                if (typeof(T).IsInterface)
                {

                    var methodWrite_Interface_ToStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                 .GetMethod( nameof(Write_Interface_ToStream) )
                                                                                 .MakeGenericMethod(tpSerializer.TargetType);


                    return Expression.Lambda<SDJson.SerializeToStreamTDbg<T>>(
                                                           Expression.Call(
                        //typeProcessorInstance
                                                             methodWrite_Interface_ToStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , parameterStream
                                                           , parameterInputData
                                                           , parameterTypeProcessor
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {

                    var methodWrite_object_ToStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                                .GetMethod( nameof(Write_object_ToStream) )
                                                                                .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SDJson.SerializeToStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodWrite_object_ToStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , parameterStream
                                                           , parameterInputData
                                                           , parameterTypeProcessor
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateSerialize_WriteExpressionDbg)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpProcessor = null)
        {
            try
            {
                //algorithm:
                // object Read_Interface_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
                // object Read_object_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");



                if (typeof(T).IsInterface)
                {

                    var methodRead_Interface_FromStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                .GetMethod(nameof( Read_Interface_FromStream) )
                                                                .MakeGenericMethod(tpProcessor.TargetType);

                    return Expression.Lambda<SDJson.DeserializeFromStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodRead_Interface_FromStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           , parameterTypeProcessor
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {


                    var methodRead_object_FromStream = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                                    .GetMethod(nameof(Read_object_FromStream) )
                                                                    .MakeGenericMethod(tpProcessor.TargetType);

                    return Expression.Lambda<SDJson.DeserializeFromStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodRead_object_FromStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           , parameterTypeProcessor
                                                           );
                }

                return null;

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }




        public Expression<SDJson.SerializeToStringTDbg<T>> GenerateSerializeToString_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:            
                // void  Write_object_ToString<T>(  TextWriter writer, T instanceValue)
                var typeInfo = TypeInfoEx.Get(typeof(T));
                var parameterWriter = Expression.Parameter(Tps.T_TextWriter, "writer");
                var parameterInputData = Expression.Parameter(typeInfo.OriginalType, "data");
                var parameterNeedToWriteTypeAQName = Expression.Parameter(Tps.T_bool, "needToWriteTypeAQName");
                var parameterTypeSerializer = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeSerializer");


                var methodWrite_Object_ToString = typeof(SerializeGenerator_AbstractInterfaceObjectJson)
                                                        .GetMethod(nameof(Write_object_ToString))
                                                        .MakeGenericMethod(typeInfo.OriginalType);


                return Expression.Lambda<SDJson.SerializeToStringTDbg<T>>(
                                                       Expression.Call(methodWrite_Object_ToString
                                                                       , parameterWriter
                                                                       , parameterInputData
                                                                       )
                                                        , typeInfo.TypeDebugCodeConventionName + "SerializeToString_Write"
                                                       , new[] { parameterWriter
                                                               , parameterInputData
                                                               , parameterNeedToWriteTypeAQName
                                                               , parameterTypeSerializer }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateSerialize_WriteExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            } 
        }


        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



        #region  -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------

        public Expression<SDJson.DeserializeFromStringTDbg<T>> GenerateDeserializeFromString_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpserializer = null)
        {
            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_AbstractInterfaceObjectJson)}.{nameof(GenerateDeserializeFromString_ReadExpressionDbg)}() : Message -[NEEDS TO DO SOON]  ");
        }

        #endregion -----------------------------  GENERATE DEBUG DESERIALIZEFROMSTRING READ EXPRESSION ---------------------------




#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [interfaces and object] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            return new List<Type>() { Tps.T_object };      
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>      
        public void LoadDefaultTypeSerializers()
        {

            var serializeToStringHandler = GenerateSerializeToString_WriteExpression<object>(null).Compile();
            var deserializeFromStringHandler = GenerateDeserializeFromString_ReadExpression<object>().Compile();

            //object and its default 
            JsonSerializer.AddKnownType<object>()
               .SetSerializeToStringTHandlers(serializeToStringHandler, deserializeFromStringHandler);
                                                
        }
        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}
