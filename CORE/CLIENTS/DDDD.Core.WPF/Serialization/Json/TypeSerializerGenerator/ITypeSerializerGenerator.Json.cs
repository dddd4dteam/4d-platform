﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// Type Serialize Generator interface. 
    /// <para/> Type Serialize Generator can be use in 3 scenarios:
    /// <para/> - 1) when it generates Serialize/Deserialize Expressions(handlers) for some type;
    /// <para/> - 2) when it add final Serialize/Deserialize Expressions(handlers) for some so called [Default Types];
    /// <para/> - 3)  combination of [1] and [2] scenarios. 
    /// <para/> TypeProcessor will use TypeSerializeGenerator if it's Contract satisfy the criterion that described in CanHandle().
    /// <para/> When TypeSerializeGenerator developing to use in [1] scenario, and work in NET4.5 ,  it should realize Debug Expressions(handlers).
    /// <para/> The difference between  Debug Expressions and Work Expressions: 
    /// <para/>    Debug  Expressions - generate static code(for TypeProcessor), 
    /// <para/>    but Work Expressions generate runtime  instance(TypeProcessor) code. 
    /// </summary>
    public interface ITypeSerializerGeneratorJson
    {

        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator. Default Formatting is Binary.
        /// </summary>
        FormattingEn Formatting { get; }



        /// <summary>
        /// Can Handle [type] criterion for  TypeSerializeGenerator
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        bool CanHandle(Type type);


        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>(); //ITypeSerializerJson<T> tpSerializer


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>(); //ITypeSerializerJson<T> tpSerializer







        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        Expression<SDJson.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer);


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        Expression<SDJson.DeserializeFromStreamT<T>> GenerateDeserialize_ReadExpression<T>(ITypeSerializerJson<T> tpSerializer);

        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        Expression<SDJson.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null);


        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        Expression<SDJson.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null);

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#endif

        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        ///<para/>  So if this Type Serialize Generator has such types (and their handlers), 
        ///<para/>  and wants to add them to the Serializer on its(generator) activation,
        ///<para/>  CanLoadDefaultTypeSerializers should be true. 
        ///<para/>  If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///<para/>     - then Serializer will Load its [Default Types] by LoadDefaultContractHandlers().
        ///<para/>     - Also it means that we can get the List of all [Default Types] by GetDefaultContractList()  
        /// </summary>
        bool CanLoadDefaultTypeSerializers { get; }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        List<Type> GetDefaultContractList();


        /// <summary>
        /// Loading default types handlers into TYPE SET SERIALIZER - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="tpSetSerializer"></param>
        void LoadDefaultTypeSerializers();


        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------
        
    }

}
