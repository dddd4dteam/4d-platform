﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;

using DDDD.Core.IO;

namespace DDDD.Core.Serialization.Json
{

    /// <summary>
    /// Type Serialize Generator of enum-s.
    /// </summary>
    internal class SerializeGenerator_EnumJson : ITypeSerializerGeneratorJson
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        ///   Can Handle  criterion for SerializeGenerator_Enum:
        ///     - type is enum;         
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
           return type.IsEnum;            
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------

        public static T  EnumToObject<T>(object value)//
        {
            return  (T) Enum.ToObject(typeof(T), value);
        }

        public static TEnum FromTtoEnumT<TEnum, Tin>(Tin value)//
        {
            return (TEnum)Enum.ToObject(typeof(TEnum), value);
        }





        public Expression<SDJson.SerializeToStringT<T>> GenerateSerializeToString_WriteExpression<T>() //ITypeSerializerJson<T> tpSerializer
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_EnumJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }


        public Expression<SDJson.DeserializeFromStringT<T>> GenerateDeserializeFromString_ReadExpression<T>() //ITypeSerializerJson<T> tpSerializer
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_EnumJson)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }




        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SDJson.SerializeToStreamT<T> > GenerateSerialize_WriteExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            try
            {
                //algorithm:
                //Get enum underlying type - primitive, use that primitive to read and write enum data

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                //1 nullable Enum type      - primitive type already exist      
                //2 not nullable Enum type  - primitive type already exist

                var methodWritePrimitive = BinaryReaderWriter.GetWritePrimitiveMethod(tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);

                return Expression.Lambda<SDJson.SerializeToStreamT<T>>(
                                                       Expression.Call(
                                                         // typeProcessorInstance ,
                                                         methodWritePrimitive
                                                       , parameterStream
                                                       , Expression.Convert(parameterInputData, tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType)
                                                       )
                                                       , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Serialize_Write"
                                                       , new[] { parameterStream, parameterInputData }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_EnumJson)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }

        }


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializerJson<T> tpSerializer)
        {
            //algorithm:
            //Get enum underlying type - primitive, use that primitive to read and write enum data
            // Enum.ToObject(    typeof(SomeByteTypologyEn),    enumValueInByte    );
            try
            {
                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                //1 nullable Enum type      - primitive type already exist      
                //2 not nullable Enum type  - primitive type already exist

                var methodReadPrimitive = BinaryReaderWriter.GetReadPrimitiveMethod(tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);

                var methodEnumToObject = GetType().GetMethod(nameof(FromTtoEnumT)).MakeGenericMethod(tpSerializer.TargetType, tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);

                return Expression.Lambda<SDJson.DeserializeFromStreamT<T>>(

                                                    Expression.Call(
                                                                methodEnumToObject,
                                                                        Expression.Call
                                                                        (
                                                                        methodReadPrimitive
                                                                        , parameterStream
                                                                        , parameterReadedTypeID
                                                                        )
                                                                    )

                                                                    , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                                    , new[] { parameterStream, parameterReadedTypeID }

                                                                   );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_EnumJson)}.{nameof(GenerateDeserialize_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }
            
        }



        


        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------



#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {

                //algorithm:
                //Get enum underlying type - primitive, use that primitive to read and write enum data               
                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");


                //1 nullable Enum type      - primitive type already exist      
                //2 not nullable Enum type  - primitive type already exist

                var methodWritePrimitive = BinaryReaderWriter.GetWritePrimitiveMethod(tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);

                return Expression.Lambda<SDJson.SerializeToStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodWritePrimitive
                                                       , parameterStream
                                                       , Expression.Convert(parameterInputData, tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType)
                                                       ),
                                                       parameterStream
                                                      , parameterInputData
                                                      , parameterTypeProcessor
                                                       );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_EnumJson)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SDJson.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializerJson<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                //Get enum underlying type - primitive, use that primitive to read and write enum data

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializerJson<T>), "typeProcessor");

                //Get enum underlying type - primitive, use that primitive to read and write enum data



                //1 nullable Enum type      - primitive type already exist      
                //2 not nullable Enum type  - primitive type already exist

                var methodReadPrimitive = BinaryReaderWriter.GetReadPrimitiveMethod(tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);
                //var methodEnumToObject = typeof(Enum).GetMethod(nameof(Enum.ToObject), new[] { typeof(Type), typeof(object) });
                var methodEnumToObject = GetType().GetMethod(nameof(FromTtoEnumT)).MakeGenericMethod(tpSerializer.TargetType, tpSerializer.LA_TargetTypeEx.Value.EnumUnderlyingType);



                return Expression.Lambda<SDJson.DeserializeFromStreamTDbg<T> >(

                                                             Expression.Call(
                                                              methodEnumToObject,
                                                              Expression.Constant(tpSerializer.TargetType),
                                                              Expression.Call
                                                                (
                                                                methodReadPrimitive
                                                                , parameterStream
                                                                , parameterReadedTypeID
                                                                )
                                                                           )
                                                                    , parameterStream
                                                                    , parameterReadedTypeID
                                                                    , parameterTypeProcessor
                                                                   );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_EnumJson)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif



        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [Enums] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;
            }
        }

        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers( )
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_EnumJson)} - can't have the Final List of Supported Types");
        }

        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_EnumJson)} - can't have the Final List of Supported Types");
        }


        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------





    }
}
