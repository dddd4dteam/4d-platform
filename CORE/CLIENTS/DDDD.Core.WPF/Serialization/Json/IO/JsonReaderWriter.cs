﻿/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Collections;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;


namespace DDDD.Core.Serialization.Json 
{



    /// <summary>
    /// Reader/Writer for Primitive types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    /// <para/>                          DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    /// <para/>                          DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// <para/>   Reflection             TypeInfoEx 
    /// </summary>
    public static class JsonReaderWriter
    {
        #region ------------------------------- CTOR ----------------------------------

        static JsonReaderWriter()
        {
            InitPrimitiveReadingWritingMethods();
        }

        #endregion ------------------------------- CTOR ----------------------------------

        #region --------------------- NULLABLE READ/WRITE WRAPS --------------------------


        
        static void WriteNullableAction<T>(TextWriter writer, T? value, Action<TextWriter, T?> action) where T : struct
        {
            if (!value.HasValue)
            {
                writer.Write("null");                
            }
            else
            {        
                action(writer, value);
            }
        }

        static object ReadNullableAction<T>(String stringValue, Func<string, T?> function) where T : struct
        {
          
            if (stringValue  == "null") //Has No Value
            {
                return null; //      
            }
            else //Has  value 
            {
                return function(stringValue); 
            }
        }



        static T? ReadNullableTAction<T>(String stringValue, Func<string, T?> function) where T : struct
        {
            if (stringValue == "null") //Has No Value
            {
                return null; //      
            }
            else //Has  value 
            {
                return function(stringValue);
            }
        }






        #endregion --------------------- NULLABLE READ/WRITE WRAPS --------------------------



        #region ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------

        static Dictionary<Type, MethodInfo> WritingPrimitiveMethod = new Dictionary<Type, MethodInfo>();
        static Dictionary<Type, MethodInfo> ReadingPrimitiveMethod = new Dictionary<Type, MethodInfo>();



        static void InitPrimitiveReadingWritingMethods()
        {
            OperationInvoke.CallInMode(nameof(JsonReaderWriter), nameof(InitPrimitiveReadingWritingMethods)
                , () =>
                {
                    foreach (var primitiveType in TypeInfoEx.PrimitiveTypes)
                    {
                        //writing method 

                        //
                        Type realType = primitiveType.GetWorkingTypeFromNullableType();
                        bool IsNullableType = primitiveType.IsNullable();

                        string writeMethodName = "WriteT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");
                        var writePrimitiveMethod = typeof(JsonReaderWriter).GetMethod(writeMethodName, new[] { typeof(Stream), primitiveType });
                        WritingPrimitiveMethod.Add(primitiveType, writePrimitiveMethod);



                        string readMethodName = "ReadT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");

                        var readPrimitiveMethod = typeof(JsonReaderWriter).GetMethod(readMethodName, BindingFlags.Static | BindingFlags.Public);

                        ReadingPrimitiveMethod.Add(primitiveType, readPrimitiveMethod);
                    }

                });

        }

        public static MethodInfo GetWritePrimitiveMethod(Type primitiveType)
        {
            return WritingPrimitiveMethod[primitiveType];
        }

        public static MethodInfo GetReadPrimitiveMethod(Type primitiveType)
        {
            return ReadingPrimitiveMethod[primitiveType];
        }

        //public static List<Type> GetRegisteredPrimitives()
        //{
        //return TypeInfoEx.PrimitiveTypes;
        //}

        //public static bool IsPrimitiveType(Type type)
        //{
        //    return TypeInfoEx.PrimitiveTypes.Contains(type);
        //}

        #endregion ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------


        #region ---------------------------- READ/Write short-int16 ----------------------------------
            
        public static void Write(TextWriter writer, short value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int16(TextWriter writer, short value)
        {
            writer.Write(value.S());
        }

        public static object Read_int16(string itemValue)
        {
            return short.Parse(itemValue);
        }
        public static short ReadT_int16(string itemValue)
        {
            return short.Parse(itemValue);
        }

        #endregion ---------------------------- READ/Write short-int16----------------------------------


        #region ---------------------------- READ/Write ushort-uint16 ----------------------------------


        public static void Write(TextWriter writer, ushort value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint16(TextWriter writer, ushort value)
        {
            writer.Write(value.S());
        }

        public static object Read_uint16(string itemValue)
        {
            return short.Parse(itemValue);
        }
        public static ushort ReadT_uint16(string itemValue)
        {
            return ushort.Parse(itemValue);
        }


        #endregion ---------------------------- READ/Write ushort-uint16 ----------------------------------


        #region ---------------------------- READ/Write int32----------------------------------


        public static void Write(TextWriter writer, int value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int32(TextWriter writer, int value)
        {
            writer.Write(value.S());
        }

        public static object Read_int32(string itemValue)
        {
            return int.Parse(itemValue);
        }
        public static int ReadT_int32(string itemValue)
        {
            return int.Parse(itemValue);
        }


        #endregion ---------------------------- READ/Write int32----------------------------------


        #region ---------------------------- READ/Write uint32----------------------------------
        

        public static void Write(TextWriter writer, uint value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint32(TextWriter writer, uint value)
        {
            writer.Write(value.S());
        }

        public static object Read_uint32(string itemValue)
        {
            return uint.Parse(itemValue);
        }
        public static uint ReadT_uint32(string itemValue)
        {
            return uint.Parse(itemValue);
        }



        #endregion ---------------------------- READ/Write uint32----------------------------------


        #region ---------------------------- READ/Write long-int64 ----------------------------------
         

        public static void Write(TextWriter writer, long value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_int64(TextWriter writer, long value)
        {
            writer.Write(value.S());
        }

        public static object Read_int64(string itemValue)
        {
            return long.Parse(itemValue);
        }
        public static long ReadT_int64(string itemValue)
        {
            return long.Parse(itemValue);
        }
        #endregion ---------------------------- READ/Write long-int64 ----------------------------------


        #region ---------------------------- READ/Write ulong-uint64 ----------------------------------


        public static void Write(TextWriter writer, ulong value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_uint64(TextWriter writer, ulong value)
        {
            writer.Write(value.S());
        }

        public static object Read_uint64(string itemValue)
        {
            return ulong.Parse(itemValue);
        }
        public static ulong ReadT_uint64(string itemValue)
        {
            return ulong.Parse(itemValue);
        }

        #endregion ---------------------------- READ/Write ulong-uint64----------------------------------


        #region ---------------------------- READ/Write byte----------------------------------
        

        public static void Write(TextWriter writer, byte value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_byte(TextWriter writer, byte value)
        {
            writer.Write(value.S());
        }

        public static object Read_byte(string itemValue)
        {
            return byte.Parse(itemValue);
        }
        public static byte ReadT_byte(string itemValue)
        {
            return byte.Parse(itemValue);
        }


        #endregion ---------------------------- READ/Write byte----------------------------------


        #region ---------------------------- READ/Write sbyte----------------------------------


        public static void Write(TextWriter writer, sbyte value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_sbyte(TextWriter writer, sbyte value)
        {
            writer.Write(value.S());
        }

        public static object Read_sbyte(string itemValue)
        {
            return sbyte.Parse(itemValue);
        }
        public static sbyte ReadT_sbyte(string itemValue)
        {
            return sbyte.Parse(itemValue);
        }

        #endregion ---------------------------- READ/Write sbyte----------------------------------


        #region ---------------------------- READ/Write char----------------------------------
        

        public static void Write(TextWriter writer, char value)
        {
            writer.Write(value.S());//  TO DO ??? if S()
        }

        public static void WriteT_char(TextWriter writer, char value)
        {
            writer.Write(value.S());
        }

        public static object Read_char(string itemValue)
        {
            return char. Parse(itemValue);
        }
        public static char ReadT_char(string itemValue)
        {
            return  char.Parse(itemValue);
        }
        

        #endregion ---------------------------- READ/Write char----------------------------------


        #region ---------------------------- READ/Write bool----------------------------------
        
        public static void Write(TextWriter writer, bool value)
        {
            writer.Write( value ? "true" : "false");            
        }
                
        public static void WriteT_bool(TextWriter writer, bool value)
        {
            writer.Write( value ? "true" : "false");
        }

        public static object Read_bool(string boolValue) 
        {
            return   bool.Parse(boolValue);
        }
        public static bool ReadT_bool( string boolValue) 
        {
            return bool.Parse(boolValue);
        }


        #endregion ---------------------------- READ/Write bool----------------------------------


        #region ---------------------------- READ/Write DateTime----------------------------------


        public static void Write(TextWriter writer, DateTime value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_DateTime(TextWriter writer, DateTime value)
        {
            writer.Write(value.S());
        }

        public static object Read_DateTime(string itemValue)
        {
            return DateTime.Parse(itemValue);
        }
        public static DateTime ReadT_DateTime(string itemValue)
        {
            return DateTime.Parse(itemValue);
        }

        #endregion ---------------------------- READ/Write  DateTime----------------------------------


        #region ------------------------------ READ/WRITE deciaml -----------------------------------
        

        public static void Write(TextWriter writer, decimal value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_decimal(TextWriter writer, decimal value)
        {
            writer.Write(value.S());
        }

        public static object Read_decimal(string itemValue)
        {
            return decimal.Parse(itemValue);
        }
        public static decimal ReadT_decimal(string itemValue)
        {
            return decimal.Parse(itemValue);
        }


        #endregion ------------------------------ READ/WRITE deciaml -----------------------------------


        #region ---------------------------- READ/Write  TimeSpan ----------------------------------


        public static void Write(TextWriter writer, TimeSpan value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_TimeSpan(TextWriter writer, TimeSpan value)
        {
            writer.Write(value.S());
        }

        public static object Read_TimeSpan(string itemValue)
        {
            return TimeSpan.Parse(itemValue);
        }
        public static TimeSpan ReadT_TimeSpan(string itemValue)
        {
            return TimeSpan.Parse(itemValue);
        }

        #endregion ---------------------------- READ/Write  TimeSpan ----------------------------------


        #region ---------------------------- READ/Write Guid -----------------------------------


        public static void Write(TextWriter writer, Guid value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_Guid(TextWriter writer, Guid value)
        {
            writer.Write(value.S());
        }

        public static object Read_Guid(string itemValue)
        {
            return Guid.Parse(itemValue);
        }
        public static Guid ReadT_Guid(string itemValue)
        {
            return Guid.Parse(itemValue);
        }


        #endregion ---------------------------- READ/Write Guid -----------------------------------


        #region -------------------------------READ/Write Uri -----------------------------------


        public static void Write(TextWriter writer, Uri value)
        {
            writer.Write(value.OriginalString);
        }

        public static void WriteT_Uri(TextWriter writer, Uri value)
        {
            writer.Write(value.OriginalString);
        }

        public static object Read_Uri(string itemValue)
        {
            return new  Uri(itemValue);
        }
        public static Uri ReadT_Uri(string itemValue)
        {
            return new Uri(itemValue);
        }

        #endregion -------------------------------READ/Write Uri -----------------------------------


        #region ---------------------------- READ/Write BitArray -----------------------------------


        public static void Write(TextWriter writer, BitArray value)
        {
             writer.Write("[");
            //Copy to bool array            
            bool[] bits = new bool[value.Length];
            value.CopyTo(bits, 0);

            for (int i = 0; i < bits.Length; i++)
            {
                writer.Write(bits[i] == true ? "1" : "0"); // "true" : "false");
                if (i < bits.Length - 1) writer.Write(",");                
            }
            
            writer.Write("]");            
        }

        public static void WriteT_BitArray(TextWriter writer, BitArray value)
        {
            writer.Write("[");
            //Copy to bool array            
            bool[] bits = new bool[value.Length];
            value.CopyTo(bits, 0);

            for (int i = 0; i < bits.Length; i++)
            {
                writer.Write(bits[i] == true ? "1" : "0"); // "true" : "false");
                if (i < bits.Length - 1) writer.Write(",");
            }

            writer.Write("]");
        }

        public static object Read_BitArray(string itemValue)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            var itemsStringOnly = itemValue.Substring(1, itemValue.Length - 1);
            var itemsInstrings = itemsStringOnly.SplitNoEntries();

            var result = new BitArray(itemsInstrings.Count);
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = ( itemsInstrings[i] == "1" ? true : false ); //  bool.Parse(itemsInstrings[i]);                
            }
            return result;
        }

        public static BitArray ReadT_BitArray(string itemValue)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            var itemsStringOnly = itemValue.Substring(1, itemValue.Length - 1);
            var itemsInstrings = itemsStringOnly.SplitNoEntries();

            var result = new BitArray(itemsInstrings.Count);
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = (itemsInstrings[i] == "1" ? true : false); //  bool.Parse(itemsInstrings[i]);                
            }
            return result;
        }
        

        #endregion ---------------------------- READ/Write BitArray -----------------------------------


        #region  -------------------------- READ/Write TypeInfoEx -----------------------------


        public static void Write(TextWriter writer, TypeInfoEx value)
        {
            if (value.State == TypeFoundStateEn.NotFounded) throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");

            writer.Write(value.OriginalTypeAQNameString);
        }

        public static void WriteT_TypeInfoEx(TextWriter writer, TypeInfoEx value)
        {
            if (value.State == TypeFoundStateEn.NotFounded) throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");

            writer.Write(value.OriginalTypeAQNameString);
        }

        public static object Read_TypeInfoEx(string itemValue)
        {
            return TypeInfoEx.Get( itemValue);
        }
        public static TypeInfoEx ReadT_TypeInfoEx(string itemValue)
        {
            return TypeInfoEx.Get(itemValue);
        }



        #endregion -------------------------- READ/Write TypeInfoEx -----------------------------

#if SERVER && UNSAFE

        #region ---------------------------- READ/Write  double----------------------------------


        public static unsafe void Write(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }

        public static unsafe void WriteT_double(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }


        public static unsafe object Read_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }

        public static unsafe double ReadT_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }


        #endregion ---------------------------- READ/Write  double----------------------------------


        #region ---------------------------- READ/Write  float----------------------------------


        public static unsafe void Write(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }

        public static unsafe void WriteT_float(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }


        public static unsafe object Read_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        public static unsafe float ReadT_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        #endregion ---------------------------- READ/Write  float----------------------------------


#else

        #region ---------------------------- READ/Write double----------------------------------


        //public static void Write(TextWriter writer, double value)
        //{
        //    ulong vulong = (ulong)BitConverter.DoubleToInt64Bits(value);
        //    writer.Write(vulong.S());
        //}

        //public static void WriteT_double(TextWriter writer, double value)
        //{
        //    ulong vulong = (ulong)BitConverter.DoubleToInt64Bits(value);
        //    writer.Write(vulong.S());
        //}

        //public static object Read_double(string itemValue)
        //{
        //    ulong v = ulong.Parse(itemValue);
        //    return BitConverter.Int64BitsToDouble((long)v);            
        //}

        //public static double ReadT_double(string itemValue)
        //{
        //    ulong v = ulong.Parse(itemValue);
        //    return BitConverter.Int64BitsToDouble((long)v);
        //}


        public static void Write(TextWriter writer, double value)
        {           
            writer.Write(value.S());
        }

        public static void WriteT_double(TextWriter writer, double value)
        {         
            writer.Write(value.S());
        }

        public static object Read_double(string itemValue)
        {
            return double.Parse(itemValue);
        }

        public static double ReadT_double(string itemValue)
        {
            return double.Parse(itemValue);
        }



        #endregion ---------------------------- READ/Write double----------------------------------


        #region ---------------------------- READ/Write float----------------------------------
        

        public static void Write(TextWriter writer, float value)
        {
            writer.Write(value.S());
        }

        public static void WriteT_float(TextWriter writer, float value)
        {
            writer.Write(value.S());
        }

        public static object Read_float(string itemValue)
        {
            return float.Parse(itemValue);
        }

        public static float ReadT_float(string itemValue)
        {
            return float.Parse(itemValue);
        }



        #endregion ---------------------------- READ/Write float----------------------------------

#endif



        #region ---------------------------- READ/Write short?-int16----------------------------------


        public static void Write(TextWriter writer, short? value)
        {

            WriteNullableAction<short>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int16(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_int16_nullable(TextWriter writer, short? value)
        {

            WriteNullableAction<short>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int16(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_int16_nullable(string itemValue )
        {

            return ReadNullableAction<short>(itemValue, (str) =>
            {
                return (short)Read_int16(str); //(short)               
            }
                                             );

        }

        public static short? ReadT_int16_nullable(string itemValue)
        {

            return ReadNullableTAction<short>(itemValue, (str) =>
            {
                return ReadT_int16(str); //(short)               
            }
                                             ); 
        }



        #endregion ---------------------------- READ/Write short?----------------------------------


        #region ---------------------------- READ/Write ushort?-ushort ----------------------------------



        public static void Write(TextWriter writer, ushort? value)
        {

            WriteNullableAction<ushort>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint16(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_uint16_nullable(TextWriter writer, ushort? value)
        {

            WriteNullableAction<ushort>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint16(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint16_nullable(string itemValue)
        {

            return ReadNullableAction<ushort>(itemValue, (str) =>
            {
                return (ushort)Read_uint16(str);               
            }
                                             );

        }

        public static ushort? ReadT_uint16_nullable(string itemValue)
        {

            return ReadNullableTAction<ushort>(itemValue, (str) =>
            {
                return ReadT_uint16(str);             
            }
                                             );
        }


        #endregion ---------------------------- READ/Write ushort?-ushort ----------------------------------


        #region ---------------------------- READ/Write int?-int32----------------------------------
        

        public static void Write(TextWriter writer, int? value)
        {

            WriteNullableAction<int>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int32(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_int32_nullable(TextWriter writer, int? value)
        {

            WriteNullableAction<int>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int32(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_int32_nullable(string itemValue)
        {

            return ReadNullableAction<int>(itemValue, (str) =>
            {
                return (int)Read_int32(str);              
            }
                                             );

        }

        public static int? ReadT_int32_nullable(string itemValue)
        {

            return ReadNullableTAction<int>(itemValue, (str) =>
            {
                return ReadT_int32(str);                
            }
                                             );
        }


        #endregion ---------------------------- READ/Write int?-int32 ----------------------------------


        #region ---------------------------- READ/Write uint?-uint32 ----------------------------------



        public static void Write(TextWriter writer, uint? value)
        {

            WriteNullableAction<uint>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint32(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_uint32_nullable(TextWriter writer, uint? value)
        {

            WriteNullableAction<uint>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint32(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint32_nullable(string itemValue)
        {

            return ReadNullableAction<uint>(itemValue, (str) =>
            {
                return (uint)Read_uint32(str);
            }
                                             );

        }

        public static uint? ReadT_uint32_nullable(string itemValue)
        {

            return ReadNullableTAction<uint>(itemValue, (str) =>
            {
                return ReadT_uint32(str);
            }
                                             );
        }






        #endregion ---------------------------- READ/Write uint?-uint32 ----------------------------------


        #region ---------------------------- READ/Write long?-int64----------------------------------





        public static void Write(TextWriter writer, long? value)
        {

            WriteNullableAction<long>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int64(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_int64_nullable(TextWriter writer, long? value)
        {

            WriteNullableAction<long>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_int64(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_int64_nullable(string itemValue)
        {

            return ReadNullableAction<long>(itemValue, (str) =>
            {
                return (long)Read_int64(str);
            }
                                             );

        }

        public static long? ReadT_int64_nullable(string itemValue)
        {

            return ReadNullableTAction<long>(itemValue, (str) =>
            {
                return ReadT_int64(str);
            }
                                             );
        }






        #endregion ---------------------------- READ/Write long?-int64----------------------------------


        #region ---------------------------- READ/Write ulong?-uint64----------------------------------
        

        public static void Write(TextWriter writer, ulong? value)
        {

            WriteNullableAction<ulong>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint64(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_uint64_nullable(TextWriter writer, ulong? value)
        {

            WriteNullableAction<ulong>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_uint64(txtwrtr, vl.Value); }
                             );

        }


        public static object Read_uint64_nullable(string itemValue)
        {

            return ReadNullableAction<ulong>(itemValue, (str) =>
            {
                return (ulong)Read_uint64(str);
            }
                                             );

        }

        public static ulong? ReadT_uint64_nullable(string itemValue)
        {

            return ReadNullableTAction<ulong>(itemValue, (str) =>
            {
                return ReadT_uint64(str);
            }
                                             );
        }

        #endregion ---------------------------- READ/Write ulong?-uint64 ----------------------------------


        #region ------------------------------ READ/WRITE deciaml? -----------------------------------


        public static void Write(TextWriter writer, decimal? value)
        {

            WriteNullableAction<decimal>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_decimal(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_decimal_nullable(TextWriter writer, decimal? value)
        {

            WriteNullableAction<decimal>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_decimal(txtwrtr, vl.Value); }
                             );

        }



        public static object Read_decimal_nullable(string itemValue)
        {

            return ReadNullableAction<decimal>(itemValue, (str) =>
            {
                return (decimal)Read_decimal(str);
            }
                                             );

        }

        public static decimal? ReadT_decimal_nullable(string itemValue)
        {

            return ReadNullableTAction<decimal>(itemValue, (str) =>
            {
                return ReadT_decimal(str);
            }
                                             );
        }




        #endregion ------------------------------ READ/WRITE deciaml? -----------------------------------


        #region ---------------------------- READ/Write byte?----------------------------------



        public static void Write(TextWriter writer, byte? value)
        {

            WriteNullableAction<byte>( writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_byte( txtwrtr, vl.Value ); }
                             );

        }


        public static void WriteT_byte_nullable(TextWriter writer, byte? value)
        {

            WriteNullableAction<byte>( writer, value,
                                ( txtwrtr, vl) =>
                                { WriteT_byte( txtwrtr, vl.Value); }
                             );
        }



        public static object Read_byte_nullable(string itemValue)
        {
            return ReadNullableAction<byte>(itemValue, (str) =>
            {
                return (byte)Read_byte(str);
            }
                                             );
        }


        public static byte? ReadT_byte_nullable(string itemValue)
        {

            return ReadNullableTAction<byte>(itemValue, (str) =>
            {
                return ReadT_byte(str);
            }
                                             );
        }






        #endregion ---------------------------- READ/Write byte?----------------------------------


        #region ---------------------------- READ/Write sbyte?----------------------------------



        public static void Write(TextWriter writer, sbyte? value)
        {

            WriteNullableAction<sbyte>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_sbyte(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_sbyte_nullable(TextWriter writer, sbyte? value)
        {

            WriteNullableAction<sbyte>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_sbyte(txtwrtr, vl.Value); }
                             );
        }



        public static object Read_sbyte_nullable(string itemValue)
        {
            return ReadNullableAction<sbyte>(itemValue, (str) =>
            {
                return (sbyte)Read_sbyte(str);
            }
                                             );
        }


        public static sbyte? ReadT_sbyte_nullable(string itemValue)
        {

            return ReadNullableTAction<sbyte>(itemValue, (str) =>
            {
                return ReadT_sbyte(str);
            }
                                             );
        }


        #endregion ---------------------------- READ/Write sbyte?----------------------------------


        #region ---------------------------- READ/Write char?----------------------------------


        public static void Write(TextWriter writer, char? value)
        {

            WriteNullableAction<char>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_char(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_char_nullable(TextWriter writer, char? value)
        {

            WriteNullableAction<char>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_char(txtwrtr, vl.Value); }
                             );
        }



        public static object Read_char_nullable(string itemValue)
        {
            return ReadNullableAction<char>(itemValue, (str) =>
            {
                return (char)Read_char(str);
            }
                                             );
        }


        public static char? ReadT_char_nullable(string itemValue)
        {

            return ReadNullableTAction<char> ( itemValue, (str) =>
            {
                return ReadT_char(str);
            }
                                             );

        }


        #endregion ---------------------------- READ/Write char?----------------------------------


        #region ---------------------------- READ/Write bool?----------------------------------


        public static void Write(TextWriter writer, bool? value)
        {

            WriteNullableAction<bool>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_bool(txtwrtr, vl.Value); }
                             );

        }


        public static void WriteT_bool_nullable(TextWriter writer, bool? value)
        {

            WriteNullableAction<bool>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_bool(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_bool_nullable(string itemValue)
        {
            return ReadNullableAction<bool>(itemValue, (str) =>
            {
                return (bool)Read_bool(str);
            }
                                             );
        }


        public static bool? ReadT_bool_nullable(string itemValue)
        {

            return ReadNullableTAction<bool>(itemValue, (str) =>
            {
                return ReadT_bool(str);
            }
                                             );

        }




        #endregion ---------------------------- READ/Write bool?----------------------------------


        #region ---------------------------- READ/Write DateTime?----------------------------------


        public static void Write(TextWriter writer, DateTime? value)
        {
            WriteNullableAction<DateTime>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_DateTime(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_DateTime_nullable(TextWriter writer, DateTime? value)
        {
            WriteNullableAction<DateTime>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_DateTime(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_DateTime_nullable(string itemValue)
        {
            return ReadNullableAction<DateTime>(itemValue, (str) =>
            {
                return (DateTime)Read_DateTime(str);
            }
                                             );
        }


        public static DateTime? ReadT_DateTime_nullable(string itemValue)
        {
            return ReadNullableTAction<DateTime>(itemValue, (str) =>
            {
                return ReadT_DateTime(str);
            }
                                             );
        }



        #endregion ---------------------------- READ/Write  DateTime?----------------------------------


        #region ---------------------------- READ/Write  TimeSpan? ----------------------------------


        public static void Write(TextWriter writer, TimeSpan? value)
        {
            WriteNullableAction<TimeSpan>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_TimeSpan(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_TimeSpan_nullable(TextWriter writer, TimeSpan? value)
        {
            WriteNullableAction<TimeSpan>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_TimeSpan(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_TimeSpan_nullable(string itemValue)
        {
            return ReadNullableAction<TimeSpan>(itemValue, (str) =>
            {
                return (TimeSpan)Read_TimeSpan(str);
            }
                                             );
        }


        public static TimeSpan? ReadT_TimeSpan_nullable(string itemValue)
        {
            return ReadNullableTAction<TimeSpan>(itemValue, (str) =>
            {
                return ReadT_TimeSpan(str);
            }
                                             );
        }

        #endregion ---------------------------- READ/Write  TimeSpan? ----------------------------------


        #region ---------------------------- READ/Write Guid? -----------------------------------

        
        public static void Write(TextWriter writer, Guid? value)
        {
            WriteNullableAction<Guid>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_Guid(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_Guid_nullable(TextWriter writer, Guid? value)
        {
            WriteNullableAction<Guid>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_Guid(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_Guid_nullable(string itemValue)
        {
            return ReadNullableAction<Guid>(itemValue, (str) =>
            {
                return (Guid)Read_Guid(str);
            }
                                             );
        }


        public static Guid? ReadT_Guid_nullable(string itemValue)
        {
            return ReadNullableTAction<Guid>(itemValue, (str) =>
            {
                return ReadT_Guid(str);
            }
                                             );
        }


        #endregion ---------------------------- READ/Write Guid? -----------------------------------



#if UNSAFE


        #region ---------------------------- READ/Write  float? ----------------------------------


        public static unsafe void Write(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }

      
        public static unsafe void WriteT_float_nullable(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }





        public static unsafe object Read_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        public static unsafe float? ReadT_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        #endregion ---------------------------- READ/Write  float? ----------------------------------


        #region ---------------------------- READ/Write  double? ----------------------------------


        public static unsafe void Write(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe void WriteT_double_nullable(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe object Read_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        public static unsafe double? ReadT_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        #endregion ---------------------------- READ/Write  double? ----------------------------------

#elif !UNSAFE

        #region ---------------------------- READ/Write double?----------------------------------
        

        public static void Write(TextWriter writer, double? value)
        {
            WriteNullableAction<double>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_double(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_double_nullable(TextWriter writer, double? value)
        {
            WriteNullableAction<double>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_double(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_double_nullable(string itemValue)
        {
            return ReadNullableAction<double>(itemValue, (str) =>
            {
                return (double)Read_double(str);
            }
                                             );
        }


        public static double? ReadT_double_nullable(string itemValue)
        {
            return ReadNullableTAction<double>(itemValue, (str) =>
            {
                return ReadT_double(str);
            }
                                             );
        }


        #endregion ---------------------------- READ/Write double?----------------------------------


        #region ---------------------------- READ/Write float?----------------------------------


        public static void Write(TextWriter writer, float? value)
        {
            WriteNullableAction<float>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_float(txtwrtr, vl.Value); }
                             );
        }


        public static void WriteT_float_nullable(TextWriter writer, float? value)
        {
            WriteNullableAction<float>(writer, value,
                                (txtwrtr, vl) =>
                                { WriteT_float(txtwrtr, vl.Value); }
                             );
        }


        public static object Read_float_nullable(string itemValue)
        {
            return ReadNullableAction<float>(itemValue, (str) =>
            {
                return (float)Read_float(str);
            }
                                             );
        }


        public static float? ReadT_float_nullable(string itemValue)
        {
            return ReadNullableTAction<float>(itemValue, (str) =>
            {
                return ReadT_float(str);
            }
                                             );
        }


        #endregion ---------------------------- READ/Write float?----------------------------------

#endif




#if (NET45 || SL5 || WP81)  //&& !UNSAFE  //  string

        #region ---------------------------- READ/Write string ----------------------------------


        public static void Write(TextWriter writer, string value)
        {
            writer.Write("\"");
               writer.Write(value); 
            writer.Write("\"");
        }

        public static void WriteT_string(TextWriter writer, string value)
        {
            writer.Write("\"");
                writer.Write(value);
            writer.Write("\"");
        }

        public static object Read_string(string itemValue)
        {
            return itemValue.Replace("\"","");
        }

        public static string ReadT_string(string itemValue)
        {
            return itemValue.Replace("\"", "");
        }
        
        #endregion ---------------------------- READ/Write string ----------------------------------

#elif UNSAFE
        #region ---------------------------- READ/Write string ----------------------------------

        [ThreadStatic]
        static StringHelper s_stringHelper;

        public unsafe static void Write(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
        

         public unsafe static void WriteT_string(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
      

        public static object Read_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }


        
        public static string ReadT_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }

        #endregion ---------------------------- READ/Write string ----------------------------------
#endif


        #region ---------------------------- READ/Write byte[] ----------------------------------
        

        public static void Write(TextWriter writer, byte[] value)
        {
            writer.Write("[");
           
            for (int i = 0; i < value.Length; i++)
            {
                writer.Write(value[i].S() ); // "true" : "false");
                if (i < (value.Length - 1) ) writer.Write(",");
            }

            writer.Write("]");
        }

        public static void WriteT_byteArray(TextWriter writer, byte[] value)
        {
            writer.Write("[");

            for (int i = 0; i < value.Length; i++)
            {
                writer.Write(value[i].S()); // "true" : "false");
                if (i < (value.Length - 1)) writer.Write(",");
            }

            writer.Write("]");
        }

        public static object Read_byteArray(string itemValue)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            var itemsStringOnly = itemValue.Substring(1, itemValue.Length - 1);
            var itemsInstrings = itemsStringOnly.SplitNoEntries();

            var result = new byte[itemsInstrings.Count];
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = (byte.Parse(itemsInstrings[i]) ); 
            }
            return result;
        }

        public static byte[] ReadT_byteArray(string itemValue)
        {
            // value must be like  - "[1,1,0,1,0,0]"
            var itemsStringOnly = itemValue.Substring(1, itemValue.Length - 1);
            var itemsInstrings = itemsStringOnly.SplitNoEntries();

            var result = new byte[itemsInstrings.Count];
            for (int i = 0; i < itemsInstrings.Count; i++)
            {
                result[i] = (byte.Parse(itemsInstrings[i]));
            }
            return result;
        } 

         
        #endregion ---------------------------- READ/Write byte[] ----------------------------------







    }

}





#region --------------------------- GARBAGE ----------------------------------



//static void ReadWithBufferedStream()
//{
//    using (var ms = new MemoryStream())
//    {
//        BufferedStreamX bs = new BufferedStreamX(ms);

//    }

//}



#region ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------

//static uint EncodeZigZag32(int n)
//{
//    return (uint)((n << 1) ^ (n >> 31));
//}

//static ulong EncodeZigZag64(long n)
//{
//    return (ulong)((n << 1) ^ (n >> 63));
//}

//static int DecodeZigZag32(uint n)
//{
//    return (int)(n >> 1) ^ -(int)(n & 1);
//}

//static long DecodeZigZag64(ulong n)
//{
//    return (long)(n >> 1) ^ -(long)(n & 1);
//}

//static uint ReadVarint32(Stream stream)
//{
//    int result = 0;
//    int offset = 0;

//    for (; offset < 32; offset += 7)
//    {
//        int b = stream.ReadByte();
//        if (b == -1)
//            throw new EndOfStreamException();

//        result |= (b & 0x7f) << offset;

//        if ((b & 0x80) == 0)
//            return (uint)result;
//    }

//    throw new InvalidOperationException();
//}

//static void WriteVarint32(Stream stream, uint value)
//{
//    for (; value >= 0x80u; value >>= 7)
//        stream.WriteByte((byte)(value | 0x80u));

//    stream.WriteByte((byte)value);
//}

//static ulong ReadVarint64(Stream stream)
//{
//    long result = 0;
//    int offset = 0;

//    for (; offset < 64; offset += 7)
//    {
//        int b = stream.ReadByte();
//        if (b == -1)
//            throw new EndOfStreamException();

//        result |= ((long)(b & 0x7f)) << offset;

//        if ((b & 0x80) == 0)
//            return (ulong)result;
//    }

//    throw new InvalidOperationException();
//}

//static void WriteVarint64(Stream stream, ulong value)
//{
//    for (; value >= 0x80u; value >>= 7)
//        stream.WriteByte((byte)(value | 0x80u));

//    stream.WriteByte((byte)value);
//}



#endregion ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------




#endregion --------------------------- GARBAGE ----------------------------------