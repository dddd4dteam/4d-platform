﻿using System;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;

using DDDD.Core.IO;
using DDDD.Core.Threading;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Diagnostics;


namespace DDDD.Core.Serialization.Json
{
    


    public class TypeSerializerJsonCreator
    {
        const string Create = nameof(Create);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
       public static ITypeSerializerJson CreateTypeSerializerJson(Type targetType
                                                    //, ITypeSetSerializer2 typeSetSerializer
                                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                                    , BindingFlags fieldsBinding = BindingFlags.Default
                                                    , List<string> onlyMembers = null
                                                    , List<string> ignoreMembers = null
                                                    )
        {
            var createMethod = typeof(TypeSerializerJson<>).MakeGenericType(targetType).GetMethod(Create);
            return createMethod.Invoke(null, new object[] { targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers }) as ITypeSerializerJson;
        }

    }



    /// <summary>
    /// Type Serializer - serializer for {T} 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TypeSerializerJson<T> : ITypeSerializerJson<T>
    {

        #region ------------------------------- CTOR ---------------------------------

        TypeSerializerJson()//ushort typeID   ITypeSetSerializer2 setSerializer
        {
            Initialize( ); //  setSerializer
        }
        
        #endregion ------------------------------- CTOR ---------------------------------


        const BindingFlags DefaultMembersBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
        const BindingFlags DefaultMembersBindingNoBase = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.DeclaredOnly;

        private static readonly object _locker = new object();

        #region ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------

        /// <summary>
        /// Serialization TypeID
        /// </summary>
        public ushort TypeID { get; private set; }

 

        public LazyAct<RuntimeIDs> LA_RuntimeIDs
        { get; private set; }
            
         


        /// <summary>
        /// Updating TypeSerializer2 sub Types IDs -  EnumUnderlyingTypeID, ArrayElementTypeID ,  Arg1TypeID, Arg2TypeID - we getting IDs from current TypeSetserializer IDs Dictionary.
        /// </summary>
        public void InitLazyRuntimeIDs()
        {
            LA_RuntimeIDs = LazyAct<RuntimeIDs>.Create(
                (args)=>
                {                 
                        var runtimeIDs = new RuntimeIDs();
                        //TypeInfoEx targetTypeEx = args[0] as TypeInfoEx;
                        //TypeSetSerializer setSerializer = args[1] as TypeSetSerializer;

                        if (LA_TargetTypeEx.Value.ArrayElementType != null)
                        {
                            //runtimeIDs.ArrayElementTypeID = TypeSetSerializer2.GetTypeID(LA_TargetTypeEx.Value.ArrayElementType);
                        }
                        if (LA_TargetTypeEx.Value.EnumUnderlyingType != null)
                        {
                            //runtimeIDs.EnumUnderlyingTypeID = TypeSetSerializer2.GetTypeID(LA_TargetTypeEx.Value.EnumUnderlyingType);
                        }
                        if (LA_TargetTypeEx.Value.Arg1Type != null)
                        {
                            //runtimeIDs.Arg1TypeID = TypeSetSerializer2.GetTypeID(LA_TargetTypeEx.Value.Arg1Type);
                        }
                        if (LA_TargetTypeEx.Value.Arg2Type != null)
                        {
                            //runtimeIDs.Arg2TypeID = TypeSetSerializer2.GetTypeID(LA_TargetTypeEx.Value.Arg2Type);
                        }

                        return runtimeIDs;     
                }
                , null
                , null
                ); 

        }

        #endregion ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------


        /// <summary>
        /// Target Type is typeof(T) 
        /// </summary>
        public Type TargetType { get; private set; }

        /// <summary>
        ///Lazy Target Type's extended info. 
        /// </summary>
        public LazyAct<TypeInfoEx> LA_TargetTypeEx
        { get; private set; }


        /// <summary>
        ///Initing Lazy Target Type's extended info. 
        /// </summary>
        public void InitLazyTargetTypeExtendedInfo()
        {
            LA_TargetTypeEx = LazyAct<TypeInfoEx>.Create((args) =>
            {
#if DEBUG
                try
                {
                    return TypeInfoEx.Get(TargetType);
                }
                catch (Exception exc)
                {
                    throw  new InvalidOperationException($"ERROR IN {nameof(TypeSerializerJson<T>)}.{nameof(InitLazyTargetTypeExtendedInfo)}() WHERE T[{typeof(T).FullName}] - error on TypeInfoEx Get . Exception message [{exc.Message}]", exc);
                }
#elif RELEASE
                return TypeInfoEx.Get(TargetType);
#endif

            }
            , null
            , null
            );
        }



        /// <summary>
        /// Type Members Accessor of TargetType
        /// </summary>
        public ITypeAccessor Accessor { get { return AccessorT; } }


        /// <summary>
        /// Type Members Accessor of {T} type
        /// </summary>
        public ITypeAccessor<T> AccessorT { get; private set; }

          



        /// <summary>
        /// Selector defines strategy of Type.Members collecting. It's predefined BindingFlags combinations and other custom strategy options -like IgnoreMembers, OnlyMembers and Custom BindingFlags. 
        /// </summary>
        public TypeMemberSelectorEn Selector { get; private set; } //Default Member-Selector for TypeSerializer2 Members selectioning

        /// <summary>
        /// Ignore Members- means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer2.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        public List<string> IgnoreMembers { get; private set; }


        /// <summary>
        /// Only Members- means other that all other members will be gathered into Members of current TypeSerializer2.
        /// <para/>  Used when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        public List<string> OnlyMembers { get; private set; }


         

#region -------------------------------- CREATION -----------------------------------


        private void Initialize()
        {
            //SetSerializer = TypeSetSerializer2.Current;// setSerializer;
            TargetType = typeof(T);
            //TypeID = JsonSerializer.GenerateNewTypeID(typeof(T));
            InitLazyTargetTypeExtendedInfo();
            InitLazyRuntimeIDs();
        }


        static ITypeSerializerJson CreateInternal(Type contract)// , ITypeSetSerializer2 setSerialier
        {
            var typeSerializer = typeof(TypeSerializerJson<>).MakeGenericType(contract);
            return TypeActivator.CreateInstanceTBase<ITypeSerializerJson>(typeSerializer);
          
        }


        /// <summary>
        /// Creation of TypeSerializer2 for  T-contract   with  T as first  arg(not generic arg)
        /// </summary>
        /// <param name="currentType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public static ITypeSerializerJson Create(Type currentType
                                              //, ITypeSetSerializer setSerialier
                                              , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                              , BindingFlags propertiesBinding = BindingFlags.Default
                                              , BindingFlags fieldsBinding = BindingFlags.Default
                                              , List<string> onlyMembers = null
                                              , List<string> ignoreMembers = null
                                            )
        {
            var typeSerializer = CreateInternal(currentType) as TypeSerializerJson<T>;

            //TypeAccessor need only for ComplexStructure Types / but not for Collection  Types
            var members = TypeSerializeAnalyser.SelectMembers<T>(selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
            typeSerializer.AccessorT = TypeAccessor<T>.Create(true, members.ToArray());

            typeSerializer.TypeSerializeGenerator = TypeSerializeAnalyser.GetJsonTypeSerializerGeneratorForType(currentType); // JsonSerializer.GetTypeSerializerGeneratorByType

            // Expression_SerializeToStreamTHandler
            typeSerializer.LA_Expression_SerializeToStreamTHandler = LazyAct<Expression<SDJson.SerializeToStreamT<T>>>.Create(typeSerializer.CreateExpression_SerializeToStreamT, typeSerializer,null);
            // SerializeToStreamTHandler
            typeSerializer.LA_SerializeToStreamTHandler = LazyAct<SDJson.SerializeToStreamT<T>>.Create(typeSerializer.Create_SerializeToStreamT, typeSerializer, null);

            // Expression_DeserializeFromStreamTHandler
            typeSerializer.LA_Expression_DeserializeFromStreamTHandler = LazyAct<Expression<SDJson.DeserializeFromStreamT<T>>>.Create(typeSerializer.CreateExpression_DeserializeFromStreamT, typeSerializer, null);
            // DeserializeFromStreamTHandler
            typeSerializer.LA_DeserializeFromStreamTHandler = LazyAct<SDJson.DeserializeFromStreamT<T>>.Create(typeSerializer.Create_DeserializeFromStreamT, typeSerializer, null);
               
            return typeSerializer;
        }





#endregion -------------------------------- CREATION -----------------------------------


#region ----------------------------- RUNTIME  type - IDs ----------------------------

      



        //public bool IsElement_EnumOrPrimitive(ushort elementID)
        //{
        //    var targetTypeInfoEx = TypeSetSerializer2.GetTypeSerializer(elementID).LA_TargetTypeEx.Value;
        //    return (targetTypeInfoEx.Is4DPrimitive || targetTypeInfoEx.EnumUnderlyingType != null); // is primitive or enum type
            
        //}





        /// <summary>
        ///  Getting data object's Type runtime ModelId - its TypeSerializer2 ModelId. 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        //public  ushort GetElementTypeID(object element)
        //{
        //    if (element.IsNull())
        //    {
        //        return (ushort)0;
        //    }
        //    else
        //    {
        //        return TypeSetSerializer2.GetTypeID(element.GetType());
        //    }
        //}


        /// <summary>
        /// 2 STEP After Contract setted- On  SetSerializer.BuildTypeProcessor ().  
        /// <para/>   Collecting  Contract subTypes IDs - runtime IDs. 
        /// <para/>  Now all available Contracts of the user Service Domain already should be added into dictionary.
        /// <para/>  If subtype won't exist in SetSerializer.ContractDictionary exception wil be thrown. 
        /// <para/>Also if Contract is enum        - init EnumUnderlyingTypeID;
        /// <para/>     if Contract is array       - init ArrayElementTypeID;
        /// <para/>     if Contract is IList       - init Arg1TypeID;
        /// <para/>     if Contract is IDictionary - init Arg1TypeID, Arg2TypeID
        /// <para/> This method can be called only after HostSerializer setted to this TypeSerializer2.
        /// </summary>
        //internal void AnalyzeRuntimeContractTypeIDs()
        //{
        //    //ContractMemberAnalyzeException
        //    Validator.NotNullDbg<InvalidOperationException>(SetSerializer == null, nameof(AnalyzeRuntimeContractTypeIDs), " SetSerializer can't be null");

        //    if (Accessor.TAccess.IsEnum)
        //    {
        //        EnumUnderlyingTypeID = GetTypeID(Accessor.TAccessEx.EnumUnderlyingType);
        //    }
        //    else if (Accessor.TAccess.IsArray)
        //    {
        //        ArrayElementTypeID = GetTypeID(Accessor.TAccessEx.ArrayElementType);
        //    }



        //    else if (Accessor.TAccess.IsImplement_List()) //   typeof(IList).IsAssignableFrom(Accessor.TAccess) && Accessor.TAccess.IsGenericType
        //    {
        //        Arg1TypeID = GetTypeID(Accessor.TAccessEx.Arg1Type);
        //    }
        //    else if (Accessor.TAccess.IsImplement_Dictionary()) //     typeof(IDictionary).IsAssignableFrom(Accessor.TAccess) && Accessor.TAccess.IsGenericType
        //    {
        //        Arg1TypeID = GetTypeID(Accessor.TAccessEx.Arg1Type);
        //        Arg2TypeID = GetTypeID(Accessor.TAccessEx.Arg2Type);
        //    }

        //}



        #endregion ----------------------------- RUNTIME  type - IDs ----------------------------



        #region ------------------------- GENERATE / SET DIRECTLY  SERIALIZE/DESERIALIZE HANDLERS ------------------------



        /// <summary>
        /// Serialize Handlers Generator for  Contract
        /// </summary>
        public ITypeSerializerGeneratorJson TypeSerializeGenerator
        {
            get;
            private set;
        }

      

#endregion------------------------- GENERATE / SET DIRECTLY  SERIALIZE/DESERIALIZE HANDLERS ------------------------



#region -------------------------------- Object.HashCode ------------------------------

        /// <summary>
        /// Overrided object.GetHashCode.This instance HashCode is Accessor.TAccess.GetHashCode().
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Accessor.TAccess.GetHashCode();
        }


        #endregion -------------------------------- Object.HashCode ------------------------------







        #region ------------------------------------ SERIALIZE/DESRIALIZE  HANDLERS --------------------------------


        ///// 
        ///  SerializeToStringTHandler
        ///// 
        public LazyAct<Expression<SDJson.SerializeToStringT<T>>> LA_Expression_SerializeToStringTHandler { get; private set; }
        public LazyAct<SDJson.SerializeToStringT<T>> LA_SerializeToStringTHandler { get; private set; }


        Expression<SDJson.SerializeToStringT<T>> CreateExpression_SerializeToStringT(object[] args)
        {
            return TypeSerializeGenerator.GenerateSerializeToString_WriteExpression<T>();
        }

        SDJson.SerializeToStringT<T> Create_SerializeToStringT(object[] args)
        {
            return LA_Expression_SerializeToStringTHandler.Value.Compile();
        }




        ///// 
        ///  DeserializeFromStringTHandler
        ///// 
        public LazyAct<Expression<SDJson.DeserializeFromStringT<T>>> LA_Expression_DeserializeFromStringTHandler { get; private set; }
        public LazyAct<SDJson.DeserializeFromStringT<T>> LA_DeserializeFromStringTHandler { get; private set; }


        Expression<SDJson.DeserializeFromStringT<T>> CreateExpression_DeserializeToStringT(object[] args)
        {
            return TypeSerializeGenerator.GenerateDeserializeFromString_ReadExpression<T>();
        }

        SDJson.DeserializeFromStringT<T> Create_DeserializeFromStringT(object[] args)
        {
            return LA_Expression_DeserializeFromStringTHandler.Value.Compile();
        }













        ///// 
        ///  SerializeToStreamTHandler
            ///// 

        public LazyAct<Expression<SDJson.SerializeToStreamT<T>>> LA_Expression_SerializeToStreamTHandler { get; private set; }
        public LazyAct<SDJson.SerializeToStreamT<T>> LA_SerializeToStreamTHandler { get; private set; }

        Expression<SDJson.SerializeToStreamT<T>> CreateExpression_SerializeToStreamT(object[] args)
        { return TypeSerializeGenerator.GenerateSerialize_WriteExpression(this);
        }

        SDJson.SerializeToStreamT<T> Create_SerializeToStreamT(object[] args)
        { return LA_Expression_SerializeToStreamTHandler.Value.Compile();
        }



        /////
        /// DeserializeFromStreamTHandler
        /////

        public LazyAct<Expression<SDJson.DeserializeFromStreamT<T>>> LA_Expression_DeserializeFromStreamTHandler { get; private set; }
        public LazyAct<SDJson.DeserializeFromStreamT<T>> LA_DeserializeFromStreamTHandler { get; private set; }

      

        Expression<SDJson.DeserializeFromStreamT<T>> CreateExpression_DeserializeFromStreamT(object[] args)
        { return TypeSerializeGenerator.GenerateDeserialize_ReadExpression(this);
        }

        SDJson.DeserializeFromStreamT<T> Create_DeserializeFromStreamT(object[] args)
        { return LA_Expression_DeserializeFromStreamTHandler.Value.Compile();
        }
                

        public object RaiseDeserializeFromStreamBoxedHandler(Stream ms, bool readedTypeID = false)
        {
            return LA_DeserializeFromStreamTHandler.Value(ms, readedTypeID);            
        }

        public T RaiseDeserializeFromStreamTHandler(Stream ms, bool readedTypeID = false)
        {
            return LA_DeserializeFromStreamTHandler.Value(ms, readedTypeID);//, ref  value  
        }


        public void RaiseSerializeToStreamBoxedHandler(Stream ms, object itemValue)
        {
            LA_SerializeToStreamTHandler.Value(ms, (T)itemValue); //temporary we need separate handler for BoxedDelegate
        }

        public void RaiseSerializeToStreamTHandler(Stream ms, T itemValue)
        {
            LA_SerializeToStreamTHandler.Value(ms, itemValue);
        }


     

#endregion------------------------------------ SERIALIZE/DESRIALIZE  HANDLERS --------------------------------





        // ELEMENT - concept determines collection/array items or  structure's fields/properties
#region -------------------------- SERIALIZE /DESERIALZIE ELEMENT - SWITCH  HANDLERS --------------------------------


        /// <summary>
        /// Raising Element Serialize into stream to object value
        /// </summary>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        public void RaiseElementSerializeToStreamBoxedHandler(ushort elemTypeID, Stream ms, object itemValue)
        {
            //GetElementTypeSerializer(elemTypeID)
            //    .RaiseSerializeToStreamBoxedHandler(ms, itemValue);            /// ??? Boxed or T Hadler will be better
        }



        /// <summary>
        /// Raising Element Deserialize from stream to object value
        /// </summary>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        public object RaiseElementDeserializeFromStreamBoxedHandler(ushort elemTypeID, Stream ms, bool readedTypeID)
        {
            return null;
            //return GetElementTypeSerializer(elemTypeID)
            //       .RaiseDeserializeFromStreamBoxedHandler(ms, readedTypeID);
        }





        /// <summary>
        /// Raising Element Serialize into stream from TElement value
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        public void RaiseElementSerializeToStreamTHandler<TElement>(ushort elemTypeID, Stream ms, TElement itemValue)
        {
#if DEBUG
            var targetType = TargetType;
           
#endif

            //(GetElementTypeSerializer(elemTypeID) as ITypeSerializer2<TElement>)
            //    .RaiseSerializeToStreamTHandler(ms, itemValue);
        }

        /// <summary>
        /// Raising Element Deserialize from stream to TElement value
        /// </summary>
        /// <param name="typeID"></param>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        public TElement RaiseElementDeserializeFromStreamTHandler<TElement>(ushort elemTypeID, Stream ms, bool readedTypeID = false)
        {
#if DEBUG
            var targetType = TargetType;
            var elementType = typeof(TElement);

#endif

            //    return (GetElementTypeSerializer(elemTypeID) as ITypeSerializer2<TElement>)
            //        .RaiseDeserializeFromStreamTHandler(ms, readedTypeID);
            return default(TElement);
        }






#endregion  -------------------------- SERIALIZE /DESERIALZIE ELEMENT - SWITCH  HANDLERS --------------------------------





#region -------------------------------- READ WRITE ARRAY DEFENITINS ----------------------------------------

        public T ReadCollectionTypeID_CreateValue(Stream ms, out int collectionLength, bool readedTypeID = false) //Lists , dictionaries
        {

            if (readedTypeID == false) //need to read typeID 
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(ms);
                if (typeId == 0U) //is null
                {
                    collectionLength = -1;
                    return default(T);//just the same as return null  for reference types
                }
            }
            
            collectionLength = (int)BinaryReaderWriter.Read_int32(ms);//read dimension-length

            
            return LA_CreateDefaultValue.Value(new object[] { collectionLength } );
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="collectionLength"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        public T ReadObservableCollectionTypeID_CreateValue(Stream ms, out int collectionLength, bool readedTypeID = false) // observableCollections
        {

            if (readedTypeID == false) //need to read typeID 
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(ms);
                if (typeId == 0U) //is null
                {
                    collectionLength = -1;
                    return default(T);//just the same as return null  for reference types
                }
            }

            collectionLength = (int)BinaryReaderWriter.Read_int32(ms);//read dimension-length

            return LA_CreateDefaultValue.Value(null);
        }




        public T ReadArrayTypeID_CreateValue(Stream ms, int knownrank, bool readedTypeID = false)
        {

            if (readedTypeID == false) //need to read typeID 
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(ms);
                if (typeId == 0U) //is null
                {
                    return default(T);//just the same as return null  for reference types
                }
            }

            //read rank - needn't we have pattern where rank is known            

            //reading array dimensions and creating It
            int[] dimensionsLengths = new int[knownrank];

            //reading  dimensions and  totalLength- to count up of   [linear element read times ]           
            for (int i = 0; i < knownrank; ++i)
            {
                dimensionsLengths[i] = (int)BinaryReaderWriter.Read_int32(ms);//read dimension-length               
            }

            return LA_CreateDefaultValue.Value(dimensionsLengths.ToObjectArray());
        }



        #endregion -------------------------------- READ WRITE ARRAY DEFENITINS ----------------------------------------



#region ---------------------------------Utils:  TypeID & TypeSerializer2 SWITCH  -----------------------------------




        /// <summary>
        /// Write ushort typeID( of T) to Stream - For Nullable T- struct, or classes.
        /// <para/> If data == null  write  TypeID = 0.  If data != null write TypeID
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="data"></param>
        public  void Write_T_TypeID(Stream ms, T data)
        {
            if (data == null)
            {
                BinaryReaderWriter.WriteT_uint16(ms, 0);
            }
            else
            {
                BinaryReaderWriter.WriteT_uint16(ms, TypeID);
            }
        }


        /// <summary>
        /// Write ushort typeID( of T) to Stream - For Nullable T- struct, or classes.
        /// <para/> If data == null  write  TypeID = 0( null value const);  If data != null write typeID
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="data"></param>
        /// <param name="typeID"></param>
        public  void Write_X_TypeID(Stream ms, object data, ushort typeID)
        {
            if (data == null)
            {
                BinaryReaderWriter.WriteT_uint16(ms, 0);
            }
            else
            {
                BinaryReaderWriter.WriteT_uint16(ms, typeID);
            }
        }



        /// <summary>
        /// Dictionary Key can't be null  even if TKey is interface or class Type - so it's simple writing correct typeID into Stream
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="typeID"></param>
        public  void Write_DictKeyID(Stream ms, ushort typeID)
        {
            BinaryReaderWriter.WriteT_uint16(ms, typeID);
        }



        /// <summary>
        /// ReadTypeID_CreateValue from Stream. If we need to create collection with length we adding first dimensions parameter
        /// If we need to create [array of (1-4) dimension] we adding [1-4] values to dimesions
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <param name="dimensions"></param>
        /// <returns></returns>
        public  T ReadTypeID_CreateValue(Stream ms, bool readedTypeID = false, params int[] dimensions)
        {
            T defValue = default(T);

            if (readedTypeID == false) //need to read typeID 
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(ms);
                if (typeId == 0U) //is null
                {
                    return default(T);//just the same as return null  for reference types
                }
            }

            //create New Instance as it is not 0- not null             
            if (dimensions.Length == 0)
            { defValue = LA_CreateDefaultValue.Value(null);
            }
            else if (dimensions.Length == 1)
            { defValue = LA_CreateDefaultValue.Value(new object[] { dimensions[0] });
            }
            else if (dimensions.Length == 2)
            { defValue = LA_CreateDefaultValue.Value(new object[] { dimensions[0], dimensions[1] });
            }
            else if (dimensions.Length == 3)
            { defValue = LA_CreateDefaultValue.Value(new object[] { dimensions[0], dimensions[1], dimensions[2] });
            }
            else if (dimensions.Length == 4)
            { defValue = LA_CreateDefaultValue.Value(new object[] { dimensions[0], dimensions[1], dimensions[2], dimensions[3] });
            }

            return defValue;

        }


        /// <summary>
        /// Lazy Func to create default value based on type kind: 1 -class; for enum; for struct; 2 - IDictionary, IList 3 - Array with [1-4] dimension
        /// <para/> When we get LA_CreateDefaultValue.Value first time - we choosing and precompiling fitted for T  DefaultValueFunc.          
        /// </summary>
        public LazyAct<Func<object[], T>> LA_CreateDefaultValue
        { get; } = LazyAct<Func<object[], T>>.Create(CreateDefaultvalueFuncT, null, null);

        /// <summary>
        /// Choose -what default value func fitted by this type and precompile it
        /// </summary>
        /// <returns></returns>
        static Func<object[], T> CreateDefaultvalueFuncT(object[] args)
        {
            var seriaizeCollectionType = typeof(T).GetBaseCollectionType();// GetTypeInfo();//

            if (seriaizeCollectionType == CollectionTypeEn.NotCollection)
            {
                // for class; for enum; for struct
                return TypeActivator.PrecompileAsNewExpressionT<T>();
            }
            else if ( seriaizeCollectionType == CollectionTypeEn.ObservableCollection )
            {
                // for ObservableCollection types
                return TypeActivator.PrecompileAsNewExpressionT<T>();
            }
            else if (seriaizeCollectionType == CollectionTypeEn.IDictionary
                    || seriaizeCollectionType == CollectionTypeEn.IList)//for collection types
            {
                return TypeActivator.PrecompileAsNewWithLengthExpressionT<T>();
            }
            else if (seriaizeCollectionType == CollectionTypeEn.Array)
            {
                return TypeActivator.PrecompileAsNewArrayExpressionT<T>();//array dimensions
            }

            return null;
        }

      


#endregion ---------------------------------Utils:  TypeID & TypeSerializer2 SWITCH  -----------------------------------
        


#region ------------------------------------- WRITE MEMBER TO STREAM HELPERS -----------------------------------------

        /// <summary>
        /// Write  [0] if [null] to Stream. Returns: TRUE VALUE (IS NULL)  -  then we logically   return/ or continue.
        /// <para/> If realTypeID is not  [null] but 4DPrimitive or Enum type we also write this realTypeID to Stream. Returns: FALSE VALUE (IS NOT NULL).
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="realTypeID"></param>
        /// <returns></returns>
        public bool WriteRealTypeID_4Null_4PrimitiveOrEnum (Stream  stream, ushort realTypeID)
        {
            if (realTypeID == 0) // NULL
            {
                BinaryReaderWriter.WriteT_uint16(stream, realTypeID); return true; // yes null - return / continue
            }
            //if (IsElement_EnumOrPrimitive(realTypeID))
            //{
            //    BinaryReaderWriter.WriteT_uint16(stream, realTypeID);
            //} //// still need to write ID even if it's 4DPrimitive or Enum- because in their usual handlers they doesn't write their ids

            return false; // NOT NULL 
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TMember"></typeparam>
        /// <param name="typeID"></param>
        /// <param name="member"></param>
        /// <param name="stream"></param>
        /// <param name="instanceValue"></param>
        public void WriteMemberToStream_NeedToSureInDataType<TMember>(ushort typeID, string member, Stream stream, T instanceValue)
        {

            var tType = typeof(T);
            var tMemberType = typeof(TMember);
            var memberName = member;


            //OperationInvoke.CallInMode(nameof(TypeSerializer2<T>), nameof(WriteMemberToStream_NeedToSureInDataType)
            //    , () =>
            //    {  
            //        //for not sealed class, interface , nullable structs - 1 - types category           
            //        var memberValue = AccessorT.GetMember<TMember>(member, instanceValue);

            //        //New - Always write member.Name before member.typeID and member.value        [1/3/2017 A1]
            //        BinaryReaderWriter.WriteT_string(stream,member);


            //        var realTypeID = GetElementTypeID(memberValue); //correct member type id before transfer to element serialize
            //        if (WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, realTypeID)) return;  // end - value was Null              

            //        // custom structs and custom classes will write their ID in their private handlers
            //        RaiseElementSerializeToStreamBoxedHandler(realTypeID, stream, memberValue);

            //    }
            //    , errorDetailMessage: " DETAILS-- T:[{0}] | MemberKey:[{1}] | TMember:[{2}]  | ".Fmt(tType.FullName, member, tMemberType.FullName)
            //    );



        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TMember"></typeparam>
        /// <param name="typeID"></param>
        /// <param name="member"></param>
        /// <param name="ms"></param>
        /// <param name="instanceValue"></param>
        public void WriteMemberToStream_NeedNotToSureInDataType<TMember>(ushort typeID, string member, Stream ms, T instanceValue)
        {
            //only  for Diagnostics
            var tType = typeof(T);
            var tMemberType = typeof(TMember);
            var memberName = member;

            //OperationInvoke.CallInMode(nameof(TypeSerializer2<T>), nameof(WriteMemberToStream_NeedNotToSureInDataType)
            //    , () =>
            //     {

            //         //New - Always write member.Name before member.typeID and member.value        [1/3/2017 A1]
            //         BinaryReaderWriter.WriteT_string(ms, member);

            //         //for sealed classes or structs - 2 -types category

            //         // even it TMember is sealed class  - the typeID need not to be changed - so we don't need write here  null ID - it'll be done in Element TypeSerializer2 
            //         RaiseElementSerializeToStreamTHandler(typeID, ms, AccessorT.GetMember<TMember>(member, instanceValue));
            //     }
            //     , errorDetailMessage : " DETAILS-- T:[{0}] | MemberKey:[{1}] | TMember:[{2}]  | ".Fmt(tType.FullName, member, tMemberType.FullName)
            //    ); 



        }

        #endregion ------------------------------------- WRITE MEMBER TO STREAM HELPERS -----------------------------------------


        #region ----------------------------------- READ MEMBER FROM STREAM HELPERS-------------------------------------


        private void CheckNextMemberIsCurrentAndSkipIfNot(string memberKey, Stream ms)
        {
            var nextMember = BinaryReaderWriter.ReadT_string(ms);
            if (nextMember == memberKey) return;//the same member order on send-side and receive-side



            //not the same nextMember as memberKey
            var nextTypeID = BinaryReaderWriter.ReadT_uint16(ms);


            if (nextTypeID == 0U) //is null    then stop read  nextMember logic  and go to next one after this next 
            {
            }
            //else if (nextTypeID > 0  && JsonSerializer.ContainsTypeSerializer(nextTypeID) == true) //if not null and typeSeriualizer contained
            //{
            //    var memberValue = RaiseElementDeserializeFromStreamBoxedHandler(nextTypeID, ms, readedTypeID: true);                
            //}
            //else if (nextTypeID > 0 && TypeSetSerializer2.ContainsTypeSerializer(nextTypeID)==false)
            //{
            //    throw new InvalidDataException("Unknown TypeSerializer2 ID on next member reading - [{0}]".Fmt(nextTypeID.S()));
            //}

            // NNED TO ADD IN FUTURE:
            // Types mismatch case - when the same typeID but reallly types different  

            //check and read  next member if stream not finished
            if (ms.Position != ms.Length)
            {
                CheckNextMemberIsCurrentAndSkipIfNot(memberKey, ms);
            }

        }




        public void ReadMemberFromStream_NeedToRereadDataType<TMember>(ushort typeID, string memberKey, Stream ms, ref T instanceValue, bool readedTypeID = false) //, Int32 length = 0 
        {

#if DEBUG
            //only  for Diagnostics
            var tType = typeof(T);
            var tMemberType = typeof(TMember);
            var memberName = memberKey;
            try
            {
                //New - Always  read member and if next is not the same as memberKey skip it,if  typeSerializerID isnot known then throw exception
                CheckNextMemberIsCurrentAndSkipIfNot(memberKey,ms);
                               

                typeID = BinaryReaderWriter.ReadT_uint16(ms);
                if (typeID == 0U) //is null
                {
                    AccessorT.SetMember(memberKey, ref instanceValue, (TMember)(object)null); return;
                }

                TMember memberValue = (TMember)RaiseElementDeserializeFromStreamBoxedHandler(typeID, ms, readedTypeID = true);
                AccessorT.SetMember(memberKey, ref instanceValue, memberValue);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializerJson<T>)}.{nameof(ReadMemberFromStream_NeedToRereadDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{memberKey}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);
            }
#elif RELEASE
            //New - Always write member.Name before member.typeID and member.value        [1/3/2017 A1]
             CheckNextMemberIsCurrentAndSkipIfNot(memberKey,ms);

            typeID = BinaryReaderWriter.ReadT_uint16(ms);
            if (typeID == 0U) //is null
            {
                AccessorT.SetMember(memberKey, ref instanceValue, (TMember)(object)null);     return;                
            }
           
            TMember memberValue = (TMember)RaiseElementDeserializeFromStreamBoxedHandler(typeID, ms, readedTypeID = true);                        
            AccessorT.SetMember(memberKey, ref instanceValue, memberValue);
#endif

        }


        public void ReadMemberFromStream_NeedNotToRereadDataType<TMember>(ushort typeID, string memberKey, Stream ms, ref T instanceValue, bool readedTypeID = false) //, Int32 length = 0 
        {

#if DEBUG
            //only  for Diagnostics
            var tType = typeof(T);
            var tMemberType = typeof(TMember);
            var memberName = memberKey;
            try
            {
                //New - Always write member.Name before member.typeID and member.value        [1/3/2017 A1]
                CheckNextMemberIsCurrentAndSkipIfNot(memberKey, ms);

                //for sealed classes or structs - 2 -types category
                //typeID - static constant           
                TMember memberValue = RaiseElementDeserializeFromStreamTHandler<TMember>(typeID, ms, readedTypeID);
                AccessorT.SetMember(memberKey, ref instanceValue, memberValue);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializerJson<T>)}.{nameof(ReadMemberFromStream_NeedNotToRereadDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{memberKey}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);
            }
#elif RELEASE
            //New - Always write member.Name before member.typeID and member.value        [1/3/2017 A1]
             CheckNextMemberIsCurrentAndSkipIfNot(memberKey,ms);

            //typeID - static constant           
            TMember memberValue = RaiseElementDeserializeFromStreamTHandler<TMember>(typeID, ms, readedTypeID);                        
            AccessorT.SetMember(memberKey, ref instanceValue, memberValue);
#endif



        }




        #endregion ----------------------------------- READ MEMBER FROM STREAM HELPERS-------------------------------------



        #region ---------------------------------- SET SERIALIZETOSTREAMT HANDLERS ----------------------------------------

        /// <summary>
        /// Setting -or changing Lazy{T}.Values of serializeToStreamTHandler   and deserializeFromStreamTHandler
        /// </summary>
        /// <param name="serializeToStreamTHandler"></param>
        /// <param name="deserializeFromStreamTHandler"></param>
        public void SetSerializeToStreamTHandlers(SDJson.SerializeToStreamT<T> serializeToStreamTHandler, SDJson.DeserializeFromStreamT<T> deserializeFromStreamTHandler)
        {
            LA_SerializeToStreamTHandler.ResetValueDirectly(serializeToStreamTHandler);
            LA_DeserializeFromStreamTHandler.ResetValueDirectly(deserializeFromStreamTHandler);
        }


        public void SetSerializeToStreamTHandlers(Delegate serializeToStreamTHandler, Delegate deserializeFromStreamTHandler)
        {
            // static fields by default souldn't be included into de/serialization
            Validator.AssertTrue<InvalidOperationException>(
                              serializeToStreamTHandler.GetType() != typeof(SDJson.SerializeToStreamT<T>),
                               $"SDJson.SerializeToStreamT Delegate Type mismatch for T-[{typeof(T).FullName}] ."
                               , typeof(T));
            
            Validator.AssertTrue<InvalidOperationException>(
                              deserializeFromStreamTHandler.GetType() != typeof(SDJson.DeserializeFromStreamT<T>),
                               $"SDJson.DeserializeFromStreamT Delegate Type mismatch for T-[{typeof(T).FullName}] ."
                               , typeof(T));

            SetSerializeToStreamTHandlers(serializeToStreamTHandler as SDJson.SerializeToStreamT<T>, deserializeFromStreamTHandler as SDJson.DeserializeFromStreamT<T>);
        }

       

#endregion ----------------------------------  SET SERIALIZETOSTREAMT HANDLERS  ----------------------------------------




#if NET45 /////// ----------------------- GENERATE DEBUG CODE -----------------------////////

#region ----------------------- GENERATE DEBUG CODE -----------------------



        public void BuildDynamicTypeAccessor(TypeBuilder tpBiulder)
        {
            throw new NotImplementedException();
        }

        public void SetSerializeToStringTHandlers(SDJson.SerializeToStringT<T> serializeToStringTHandler, SDJson.DeserializeFromStringT<T> deserializeFromStringTHandler)
        {
            LA_SerializeToStringTHandler.ResetValueDirectly(serializeToStringTHandler);
            LA_DeserializeFromStringTHandler.ResetValueDirectly(deserializeFromStringTHandler);
        }
        


        #endregion ----------------------- GENERATE DEBUG CODE -----------------------

#endif


    }
}



#region ------------------------------ GARBAGE -------------------------------



/// <summary>
/// Getting TypeSerializer2 from SetSerializer by typeID
/// </summary>
/// <typeparam name="TMember"></typeparam>
/// <param name="typeID"></param>
/// <returns></returns>
//internal protected ITypeSerializerJson<TMember> GetElementTypeSerializer<TMember>(ushort typeID)
//{
//    return JsonSerializer.GetTypeSerializer(typeID) as ITypeSerializerJson<TMember>;
//}

///// <summary>
///// Getting TypeSerializer2 from SetSerializer by typeID 
///// </summary>
///// <param name="typeID"></param>
///// <returns></returns>
//internal protected ITypeSerializerJson GetElementTypeSerializer(ushort typeID)
//{
//    return JsonSerializer.GetTypeSerializer(typeID);
//}

#region ----------------------------- MEMBERS SELECTION -------------------------------

///// <summary>
///// Get Binding by Type Members Selector 
///// </summary>
///// <param name="policy"></param>
///// <returns></returns>
//public static BindingFlags GetSelectorBinding(TypeMemberSelectorEn membersSelector)
//{

//    switch (membersSelector)
//    {
//        case TypeMemberSelectorEn.NotDefined:
//            {
//                return BindingFlags.Default;
//            }

//        case TypeMemberSelectorEn.Default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.DefaultNoBase:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public | BindingFlags.NonPublic |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.PublicOnly:
//            {
//                return (BindingFlags.Instance |
//                            BindingFlags.Public |
//                            BindingFlags.DeclaredOnly);
//            }
//        case TypeMemberSelectorEn.OnlyMembers:
//            {
//                return (BindingFlags.Instance |
//                        BindingFlags.Public | BindingFlags.NonPublic);
//            }
//        case TypeMemberSelectorEn.Custom:
//            {
//                return (BindingFlags.Default);
//            }
//        default:
//            {
//                return (BindingFlags.Instance |
//                             BindingFlags.Public | BindingFlags.NonPublic);
//            }
//    }

//}



//static List<string> SelectMembers<TTarget>(TypeMemberSelectorEn selector
//                        , BindingFlags propertiesBinding = BindingFlags.Default
//                        , BindingFlags fieldsBinding = BindingFlags.Default
//                        , List<string> onlyMembers = null
//                        , List<string> ignoreMembers = null)
//{
//    var resultMembers = new List<string>();
//    var targetType = typeof(TTarget).GetWorkingTypeFromNullableType();
//    //var targetType = typeof(TTarget);

//    var properties = new List<string>();
//    var fields = new List<string>();


//    if (targetType.IsICollectionType() || targetType.IsInterface || targetType == typeof(object) || targetType.Is4DPrimitiveType())
//    {
//        // not collecting members for this type kinds 
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.NotDefined)
//    {
//        throw new InvalidOperationException($"TypeAcessor's members selector can't be undefined in TypeserializerT[{targetType.FullName}] ");
//    }
//    else if (selector == TypeMemberSelectorEn.Default)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                      .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                      .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection 
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.DefaultNoBase)
//    {
//        properties.AddRange(targetType.GetProperties(DefaultMembersBindingNoBase).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBindingNoBase).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        // not use Ignore Members - only in Custom Selection
//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.PublicOnly)
//    {
//        properties.AddRange(targetType.GetProperties(BindingFlags.Public).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(BindingFlags.Public).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);
//    }
//    else if (selector == TypeMemberSelectorEn.OnlyMembers)
//    {
//        //check all of elements exists
//        properties.AddRange(targetType.GetProperties(DefaultMembersBinding).Where(prp => prp.IsWritable().Value)
//                                        .Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(DefaultMembersBinding).Where(fld => fld.IsWritable().Value)
//                                        .Select(fld => fld.Name));

//        //only member  should be more than 0
//        if (onlyMembers == null || onlyMembers.Count == 0)
//        { throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers, then only members property can't be null or with [0] Count value  "); }

//        //check all of elements  exist in Properties or Fields
//        foreach (var item in onlyMembers)
//        {
//            if (properties.Contains(item) || fields.Contains(item))
//            {
//                throw new InvalidOperationException($" if TypeAcessor's members selector  == TypeMemberSelectorEn.OnlyMembers,  only member item [{item}]  was not found in Members of T[{targetType.Name}]");
//            }
//        }

//        resultMembers.AddRange(onlyMembers); // but will processing only members                
//    }
//    else if (selector == TypeMemberSelectorEn.Custom)
//    {
//        //only  custom binding should be as input parameter
//        //ignore items
//        properties.AddRange(targetType.GetProperties(fieldsBinding).Select(prp => prp.Name));
//        fields.AddRange(targetType.GetFields(propertiesBinding).Select(fld => fld.Name));

//        foreach (var ignoreItem in ignoreMembers)
//        {
//            if (properties.Contains(ignoreItem)) { properties.Remove(ignoreItem); }
//            if (fields.Contains(ignoreItem)) { fields.Remove(ignoreItem); }
//        }

//        resultMembers.AddRange(properties);
//        resultMembers.AddRange(fields);

//    }

//    //remove autofields <>k_BackingField
//    if (resultMembers.Count > 0)
//    {
//        resultMembers = resultMembers.Where(itm => !itm.Contains(">k__BackingField")).ToList();
//    }

//    //remove members which is read only // or instantiated once at creation




//    return resultMembers;
//}

#endregion ----------------------------- MEMBERS SELECTION -------------------------------




//////////////////////////////////////////////////////////////////////////

//#if DEBUG  //only  for Diagnostics

//            try
//            {
//                //for not sealed class, interface , nullable structs - 1 - types category           
//                var memberValue = AccessorT.GetMember<TMember>(member, instanceValue);                

//                var realTypeID = GetElementTypeID(memberValue); //correct member type id before transfer to element serialize
//                if (WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, realTypeID)) return;  // end - value was Null              

//                // custom structs and custom classes will write their ID in their private handlers
//                RaiseElementSerializeToStreamBoxedHandler(realTypeID, stream, memberValue);                
//            }
//            catch (Exception exc)
//            {
//                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializer2<T>)}.{nameof(WriteMemberToStream_NeedToSureInDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{member}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);                
//            }

//#elif RELEASE
//                //for not sealed class, interface , nullable structs - 1- types category           
//                var memberValue = AccessorT.GetMember<TMember>(member, instanceValue);

//                var realTypeID = GetElementTypeID(memberValue); //correct member type id before transfer to element serialize
//                if (WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, realTypeID)) return;  // end - value was Null              

//                RaiseElementSerializeToStreamBoxedHandler(realTypeID, stream, memberValue);

//#endif





//#if DEBUG
//            //only  for Diagnostics
//            var tType = typeof(T);
//            var tMemberType = typeof(TMember);
//            var memberName = member;
//            try
//            {
//                //for sealed classes or structs - 2 -types category

//                // even it TMember is sealed class  - the typeID need not to be changed - so we don't need write here  null ID - it'll be done in Element TypeSerializer2 
//                RaiseElementSerializeToStreamTHandler(typeID, ms, AccessorT.GetMember<TMember>(member, instanceValue));
//            }
//            catch (Exception exc)
//            {
//                throw new InvalidOperationException($"ERROR in {nameof(TypeSerializer2<T>)}.{nameof(WriteMemberToStream_NeedNotToSureInDataType)}(): - DETAILS-- T:[{tType.FullName}] | MemberKey:[{member}] | TMember:[{tMemberType.FullName}] | Exception Message -- [{exc.Message}]", exc);                
//            }

//#elif RELEASE

//             //for sealed classes or structs - 2 -types category

//             // even it TMember is sealed class  - the typeID need not to be changed - so we don't need write here  null ID - it'll be done in Element TypeSerializer2 
//             RaiseElementSerializeToStreamTHandler(typeID, ms, AccessorT.GetMember<TMember>(member, instanceValue));
//#endif








#region -------------------------------- COLLECTING AUTOITEMS  FROM PROPERTIES & FIELDS  --------------------------------------


///// <summary>
///// If FieldType is interface or enum we Add them 
///// </summary>
///// <param name="fields"></param>
///// <param name="typeSerializer"></param>
//private   void CollectAutoItemsFromFields(FieldInfo[] fields)  // ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var fld in fields)
//    {
//        //Type compareType = Tools.GetWorkingTypeFromNullableType(fld.FieldType);

//        ushort? typeId = 0;
//        //if ((typeId = JsonSerializer.ContainsTypeSerializer(fld.FieldType)) != null) continue;

//        //if (fld.FieldType.IsAutoAdding()) { TypeSetSerializer2.AddKnownType(fld.FieldType); }

//        if (typeof(IEnumerable).IsAssignableFrom(fld.FieldType)) continue;

//        //если это все же класс или структура, но только не инам -то он уже должен быть в словаре Контрактов иначе это ошибка
//        if ((fld.FieldType.IsClass || fld.FieldType.IsValueType) && !fld.FieldType.IsEnum)
//        {
//            Validator.NotNullDbg(typeId,
//                contextType: typeof(TypeSerializerJson<>), contextOperation: nameof(CollectAutoItemsFromFields),
//                contextPoint: $"Member Analyze error: Service Serializer Dictionary doesn't contain FieldType  [{fld.FieldType}]. Look at the contract [{ TargetType.FullName}] in field [{fld.Name}]");
//        }
//    }
//}


///// <summary>
///// If PropertyType is interface or enum we Add them
///// </summary>
///// <param name="properties"></param>     
//private  void CollectAutoItemsFromProperties(PropertyInfo[] properties)  //  ITypeSetSerializer HostSerializer, Type Contract)
//{
//    //analyze Member Types 
//    foreach (var prp in properties)
//    {
//        ushort? typeId;
//        //if ((typeId = JsonSerializer.ContainsTypeSerializer(prp.PropertyType)) != null) continue;

//        //if (prp.PropertyType.IsAutoAdding()) TypeSetSerializer2.AddKnownType(prp.PropertyType);     // typeSerializer.SetSerializer. 

//        if (typeof(IEnumerable).IsAssignableFrom(prp.PropertyType)) continue;

//        //если это все же класс или структура, но не инам - то они уже должен быть в словаре Контрактов иначе это ошибка
//        if ((prp.PropertyType.IsClass || prp.PropertyType.IsValueType) && !prp.PropertyType.IsEnum)
//        {
//            Validator.NotNullDbg(typeId,
//                contextType: typeof(TypeSerializerJson<>), contextOperation: nameof(CollectAutoItemsFromProperties),
//                contextPoint: $"Member Analyze error:  Serializer Dictionary doesn't contain PropertyType  [{prp.PropertyType.FullName}]. Look at the contract [{TargetType.FullName}] in property [{prp.Name}]");
//        }

//    }
//}


#endregion -------------------------------- COLLECTING AUTOITEMS  FROM PROPERTIES & FIELDS  --------------------------------------


///// <summary>
///// Init SerializeGenerator Handlers for Contract  
///// </summary>
///// <param name="serializeGenerator"></param>
//internal void SetSerializeGenerator(ITypeSerializerGenerator serializeGenerator)
//{
//    TypeSerializeGenerator = serializeGenerator;
//}

#endregion ------------------------------ GARBAGE -------------------------------

