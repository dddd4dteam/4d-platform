﻿using DDDD.Core.Reflection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

#if NET45
using System.Collections.Concurrent;
#endif

namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Type Set Serializer
    /// </summary>
    public interface ITypeSetSerializer : ISterlingSerializer
    {

#if NET45 && DEBUG

        /// <summary>
        /// Get convention readable string  value of current TypeSetSerializer Key-identifier.
        /// </summary>
        /// <returns></returns>
        string GetTSSConventionIdentityKey();

#endif




#if NET45
        /// <summary>
        /// Type Item Serializers 
        /// </summary>
        ConcurrentDictionary<ushort, ITypeSerializer> TypeSerializers { get; }

        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID
        /// </summary>
        ConcurrentDictionary<Type, ushort> TypeSerializerIDByType { get; }
#else

        /// <summary>
        /// Type Item Serializers
        /// </summary>
        Dictionary<ushort,ITypeSerializer> TypeSerializers { get; }

        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID
        /// </summary>
        Dictionary<Type, ushort> TypeSerializerIDByType {get;}
#endif

        ushort GenerateNewTypeID(Type dataType);
        
        ushort GetTypeID(object dataItem);

        ushort GetTypeID(Type dataType);

        ushort? GetIfContainsContract(Type fieldType);
        

        ITypeSerializer AddKnownType(Type tp);
        

        ITypeSerializer<T> AddKnownType<T>();
        

        ITypeSerializer AddKnownType(Type targetType       
                                       
                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null
            
                                    );


        void AddKnownTypesTPL(params Type[] knownTypes);


        /// <summary>
        /// Serialize data object to Stream. If data is nullable struct you should set isNullable to true or InvalidOperationException will be thrown. 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <param name="isNullable"></param>
        void Serialize(Stream stream, object data, bool isNullable = false);

        /// <summary>
        /// Serialize data object to byte Array. If data is nullable struct you should set isNullable to true or InvalidOperationException will be thrown. 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isNullable"></param>
        /// <returns></returns>
        byte[] Serialize(object data, bool isNullable = false);

        /// <summary>
        /// Serialize data object into Stream.
        /// </summary>
        /// <param name="value"></param>  
        /// <param name="stream"></param>
        void SerializeToStream(object value,  Stream stream);
         

        /// <summary>
        /// Serialize {T} data object into Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="stream"></param>
        void SerializeToStream<T>(T value, Stream stream);


        /// <summary>
        /// Serialize data object to byte Array.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        byte[] SerializeToByteArray(object value);  




        /// <summary>
        ///  Deserialize  data object  from  stream.  
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        object Deserialize(Stream stream);

        /// <summary>
        /// Deserialize   {T} data object  from  stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        T Deserialize<T>(Stream stream);

        /// <summary>
        /// Deserialize   {T} data object  from   byte Array.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        T Deserialize<T>(byte[] serializedData);

        /// <summary>
        /// Deserialize    data object  from   byte Array.
        /// </summary>
        /// <param name="serializedData"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        object Deserialize(byte[] serializedData, Type  targetType);


        /// <summary>
        /// Deserialize  data object  from Stream.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        object DeserializeFromStream( Type type, Stream stream);

        /// <summary>
        /// Deserialize  data object  from Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        T DeserializeFromStream<T>(Stream stream);



        /// <summary>
        /// Deserialize  data object  from  ByteArray.
        /// </summary>
        /// <param name="dataArray"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        object DeserializeFromByteArray(byte[] dataArray, Type targetType);


#if NET45 || WP81 

        /// <summary>
        /// Serialize data object to  byte Array using TPL.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<byte[]> SerializeTPL(object data);

        /// <summary>
        /// Serialize data object into stream using TPL.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        Task SerializeTPL(Stream stream, object data);// serialize data into Stream

        /// <summary>
        /// Serialize data object into byte array using TPL with cancellation option, creation option and TaskScheduler.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="taskCretionOptions"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        Task<byte[]> SerializeTPL(object data, CancellationToken cancelToken, TaskCreationOptions taskCretionOptions, TaskScheduler scheduler);


        /// <summary>
        /// Deserialize data object from stream using TPL.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        Task<object> DeserializeTPL(Stream stream);

        /// <summary>
        /// Deserialize {T} data object from stream using TPL.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        Task<T> DeserializeTPL<T>(Stream stream);

        /// <summary>
        /// Deserialize {T} data object from byte Array using TPL.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        Task<T> DeserializeTPL<T>(byte[] serializedData);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        Task ContinueSerializeTPL(Task<Stream> streamTask, object data); //continue and serialize data into Stream
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        Task<object> ContinueDeserializeTPL(Task<Stream> streamTask);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        Task<object> ContinueDeserializeTPL(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler);

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask);
        
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler);

#endif

    }
}

