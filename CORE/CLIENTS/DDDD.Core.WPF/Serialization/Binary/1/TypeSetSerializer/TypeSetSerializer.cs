﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

using DDDD.Core.IO;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Resources;

#if NET45
using System.Collections.Concurrent; 
#endif


namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Type Set Serializer(TSS).  Serializer personally. 
    /// <para/> Common use  workflow - at first we grouping our types into some Type Set:  1 -creating TypeSetSerializer and 2 -AddingType[s] to it - it means that this TypeSetSerializer will be able to [De]/Serialise only these custom known types.
    /// <para/> We manage by TypeSetSerializer's collections by the following static methods :
    /// <para/>             Get(...), GetDirect(...), GetSafe(...), AddUpdateTypeSetSerializer(...), AddOrUseExistTypeSetSerializer(...), RemoveSerializer(...)
    /// <para/> And to [De]/Serialise:
    /// <para/>             we use Serialize(...), SerializeTPL(...), Deserialize(...), DeserializeTPL() , ContinueSerializeTPL(...), ContinueDeserializeTPL() 
    /// </summary>
    public class TypeSetSerializer : ITypeSetSerializer
    {

        #region ----------------------------- CTOR---------------------------

        TypeSetSerializer()
        {
            throw new InvalidOperationException($" TypeSetSerializer Instance Creation is Impossible in such way: Reflected private ctor without any identity parameter value");              
        }

        TypeSetSerializer(int setIndex)
        {
            SetIdentity(setIndex);           
        }


        TypeSetSerializer(string setKeyName)
        {
            SetIdentity(setKeyName);           
        }

        TypeSetSerializer(Guid setGuidKey)
        {
            SetIdentity(setGuidKey);           
        }


        #endregion ----------------------------- CTOR---------------------------

        
        #region ---------------------------- FIELDS -----------------------
            
        static readonly object _locker = new object();

        #endregion ---------------------------- FIELDS -----------------------
        

        #region  --------------------------------------- INITIALIZATION -----------------------------------------


        private void Initialize()
        {

            LoadDefaultTypeSerializersFromSerializeGenerators(this);
#if NET45
            UseGenerationOfDebugCodeAssembly = false;
#endif
                  
        }


        #endregion --------------------------------------- INITIALIZATION -----------------------------------------


        #region ----------------------------- IDENTITY --------------------------

        /// <summary>
        /// Type Set Serializer Identification - by Key/ by Guid / by index
        /// </summary>
        public Identification Identity { get; private set; } = new Identification();

        void SetIdentity(string key) { Identity.KeyName = key; }

        void SetIdentity(int index) { Identity.Index = index; }

        void SetIdentity(Guid id) { Identity.ID = id; }



        /// <summary>
        /// String value of serializer instance identity key
        /// </summary>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public string GetTSSConventionIdentityKey()
        {
            if (Identity != null)
            {
                if (Identity.IdentityType == IdentityTypeEn.IdentityWitKeyName)
                {
                    return "TSSKey_" + Identity.KeyName;
                }
                else if (Identity.IdentityType == IdentityTypeEn.IdentityWithIndex)
                {
                    return "TSSKey_" + Identity.Index.S();
                }
                else if (Identity.IdentityType == IdentityTypeEn.IdentityWithID)
                {
                    return "TSSKey_" + Identity.ID.S();
                }

            }

            return "TSSKey_";
        }



        #endregion ----------------------------- IDENTITY --------------------------



        #region  ------------------------- TYPE SET  SERIALIZERS ----------------------------------

#if NET45

        static ConcurrentDictionary<Guid, ITypeSetSerializer> TypeSetsByID
        { get; } = new ConcurrentDictionary<Guid, ITypeSetSerializer>();

        //static readonly object _lockerKey = new object();
        static ConcurrentDictionary<String, ITypeSetSerializer> TypeSetsByKey
        { get; } = new ConcurrentDictionary<string, ITypeSetSerializer>();

        //static readonly object _lockerIndex = new object();
        static ConcurrentDictionary<int, ITypeSetSerializer> TypeSetsByIndex
        { get; } = new ConcurrentDictionary<int, ITypeSetSerializer>();
#else
        
        static Dictionary<Guid, ITypeSetSerializer> TypeSetsByID
        { get; } = new Dictionary<Guid, ITypeSetSerializer>();

        //static readonly object _lockerKey = new object();
        static Dictionary<String, ITypeSetSerializer> TypeSetsByKey
        { get; } = new Dictionary<string, ITypeSetSerializer>();

        //static readonly object _lockerIndex = new object();
        static Dictionary<int, ITypeSetSerializer> TypeSetsByIndex
        { get; } = new Dictionary<int, ITypeSetSerializer>();
#endif


        #endregion ------------------------- TYPE SET  SERIALIZERS ----------------------------------


        #region ---------------------------- TYPE  SERIALIZERS DICTIONARIES ---------------------------

#if NET45

        /// <summary>
        ///  Type Item Serializers - Dictionary[ID-ushort, ITypeserializer -Type item Serializer].
        /// </summary>
        public ConcurrentDictionary<ushort, ITypeSerializer> TypeSerializers
        { get; } = new ConcurrentDictionary<ushort, ITypeSerializer>();


        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        /// </summary>
        public ConcurrentDictionary<Type, ushort> TypeSerializerIDByType
        { get; } = new ConcurrentDictionary<Type, ushort>();

#else
        
        /// <summary>
        /// Main Type Serializers Collection of this TYPE SET SERIALIZER
        /// </summary>
        public Dictionary<ushort, ITypeSerializer> TypeSerializers
        { get; } = new Dictionary<ushort, ITypeSerializer>();


        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        /// </summary>
        public Dictionary<Type, ushort> TypeSerializerIDByType
        { get; } = new Dictionary<Type, ushort>();

#endif





#endregion  ---------------------------- TYPE SERIALIZERS DICTIONARIES ---------------------------




        /// <summary>
        /// Generate new Type ID
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public ushort GenerateNewTypeID(Type dataType)
        {
            if (TypeSerializerIDByType.ContainsKey(dataType)) return 0;

            return (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);            
        }




#region --------------------------------- ADDING KNOWN TYPES -----------------------------------
        
        /// <summary>
        /// Add Contract to Type Set Serializer
        /// </summary>
        /// <param name="tp"></param>
        /// <returns></returns>
        public ITypeSerializer AddKnownType(Type tp)
        {
            return AddKnownType(tp, selector: TypeMemberSelectorEn.Default);
        }

        /// <summary>
        /// Add Contract to Type Set Serializer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public ITypeSerializer<T> AddKnownType<T>()
        {
            return AddKnownType(typeof(T)) as ITypeSerializer<T>;
        }
         

        /// <summary>
        /// 
        /// </summary>
        /// <param name="knownTypes"></param>
        public void AddKnownTypesTPL(params Type[] knownTypes)
        {
            lock (_locker)
            {
                foreach (var knownType in knownTypes)
                {
                    AddKnownType(knownType);
                }
            }             
        }

        /// <summary>
        ///  Full  AddContract overload Creating TypeSerializer for different Contract Category value.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        public ITypeSerializer AddKnownType(Type targetType

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {
            ushort? typeID = 0;
            //We cannot add  Contract Type that already exist
            if ((typeID = GetIfContainsContract(targetType)) != null) return TypeSerializers[typeID.Value];


            lock (_locker)
            { // LOCK

                if ((typeID = GetIfContainsContract(targetType)) != null) return TypeSerializers[typeID.Value];

                typeID = (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);

                if ( true)//targetType.IsAutoAdding() 
                {
                    // check that contract is really Auto Adding Contract
                    // enums/IList/IDictionary/Array and interfaces

                    // cannot be other than real AutoAdding Category Contract
                    if(targetType.IsNullable())
                    {
                        var typeSerializer = CreateTypeSerializer(targetType, this);
                        AddItemToTypeSetDictionaries(typeSerializer);
                        
                        //1 case concrete serialise /deserialize handlers - as primitives for example   - but initing- by handlers only after Added or Craeted of TypeSerializer 
                        //2 case auto adding custom nullable struct 

                    }
                    //если интерфейс либо Инам  которого все еще нет то дабавить обработчики для контракта и добавить сразу запустить их
                    else if (targetType.IsInterface)
                    {
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType, this);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression


                    }
                    else if (targetType.IsEnum)
                    {

                        var typeSerializer = CreateTypeSerializer(targetType, this);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression

                    }
                    else if (targetType.IsArray)
                    {
                        //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType, this);
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need to Generate  Handlers Expression                           

                    }
                    else if (typeof(IList).IsAssignableFrom(targetType))
                    {
                        //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType, this);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression
                                             

                    }
                    else if (typeof(IDictionary).IsAssignableFrom(targetType))
                    {
                        //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        DetectAddAutoContractArgTypesFromCollection(targetType);//detect Key or Value Contract Arg

                        var typeSerializer = CreateTypeSerializer(targetType, this);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);                         
                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression
                    } 
                }
                else if ( targetType.Is4DPrimitiveType()  )
                {
                    // default contract adding: have  serializeHandler & deserializeHandler- need no to generate expressions                        
                    // can be Primitive contract                      

                    var typeSerializer = CreateTypeSerializer(targetType, this);//typeID.Value
                    // PRIMITIVE DEFAULT HANDLERS
                    //contractProcessor.SetDefaultSerializeHandler(serializeHandler);
                    //contractProcessor.SetDefaultDeserializeHandler(deserializeHandler);

                    AddItemToTypeSetDictionaries(typeSerializer);

                    //need no Policy and BuildMembers; need no Generate  Handlers Expression

                   

                    // break;
                }
                else 
                { //custom Contract  
                    //Validator.AssertTrue<InvalidOperationException>( targetType.IsAutoAdding()                                      
                    //                  , "Contract Add error: we cannot add [{0}] type, because it was classified as Auto Adding Category contract"
                    //                  , targetType);


                    //mismath between OnlyMembersPolicy and  empty OnlyMembers array  
                    Validator.AssertTrue<InvalidOperationException>(
                                      (selector == TypeMemberSelectorEn.OnlyMembers && onlyMembers != null && onlyMembers.Count == 0),
                                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_NEED_ITEM_ERR"
                                                );


                    //mismath between not empty OnlyMembers array and incorrect selector
                    Validator.AssertTrue<InvalidOperationException>(
                                      (onlyMembers!= null && onlyMembers.Count > 0 && selector != TypeMemberSelectorEn.OnlyMembers),
                                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_ITEMS_INCORRECT_POLICY_ERR"
                                                );


                    //политика не поддерживает кастомный Биндинг для свойств или для полей
                    Validator.AssertTrue<InvalidOperationException>(
                         (propertiesBinding != BindingFlags.Default || fieldsBinding != BindingFlags.Default)
                      && (selector != TypeMemberSelectorEn.OnlyMembers && selector != TypeMemberSelectorEn.Custom),
                             "Contract Add error: Current Processing Policy doesn't support custom Bindings for Properties or Fields."
                           + "Only ONLY_MEMBER and CUSTOM Porocessing policies support this feature. Look  at the  contract{0}"
                                       , targetType);


                    ////Статические поля  не должны быть включены в Биндинг или ошибка
                    Validator.AssertTrue<InvalidOperationException>(
                                       propertiesBinding.HasFlag(BindingFlags.Static) || fieldsBinding.HasFlag(BindingFlags.Static),
                                       "Member Analyze error: Your Custom Binding cannot contains Static flag. Look  at the  contract{0}."
                                       , targetType);

                    
                    var typeSerializer = CreateTypeSerializer(targetType, this, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);//typeID.Value
                    AddItemToTypeSetDictionaries(typeSerializer);

                    //collect MEmberTypes, and Check -if we have AutoAdding types within them
                    foreach (var memberType in typeSerializer.Accessor.CollectMemberTypes())
                    {
                        DetectAddAutoContract(memberType);
                    }
                    


                    // if  type  - CustomStruct- is ValueType, we'll add it's nullable brother type- CustomStruct?.                        
                    if (targetType.IsValueType && !targetType.IsNullable() && !targetType.IsEnum)
                    {
                        var contractNullableTwinBrother = typeof(Nullable<>).MakeGenericType(targetType);

                        AddKnownType(
                              contractNullableTwinBrother                                
                             , selector
                             , propertiesBinding
                             , fieldsBinding
                             , onlyMembers
                            );

                    }

                    //collect auto items - list

                    //break;
                }

                //Updating TypeSerializer sub Types IDs -  EnumUnderlyingTypeID, ArrayElementTypeID ,  Arg1TypeID, Arg2TypeID - we getting IDs from current TypeSetserializer IDs Dictionary.
                //TypeSerializers[typeID.Value].UpdateSubTypesIDs();

                return TypeSerializers[typeID.Value];
            } // LOCK

        }



        /// <summary>
        /// CreateTypeSerializer() - it is Wrap of TypeSerializer{T} Creation on Adding new Contract
        /// </summary>
        /// <param name="currentType"></param>
        /// <param name="typeSetSerializer"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        static ITypeSerializer CreateTypeSerializer(Type currentType
                                                    , ITypeSetSerializer typeSetSerializer
                                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                                    , BindingFlags fieldsBinding = BindingFlags.Default
                                                    , List<string> onlyMembers = null
                                                    , List<string> ignoreMembers = null
                                                    )
        {
            var createMethod = typeof(TypeSerializer<>).MakeGenericType(currentType).GetMethod("Create");
            return createMethod.Invoke(null, new object[] { currentType, typeSetSerializer, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers }) as ITypeSerializer;
        }



        internal void AddItemToTypeSetDictionaries(ITypeSerializer tpSerializer)
        {
#if NET45
            TypeSerializers.TryAdd(tpSerializer.TypeID, tpSerializer);
            TypeSerializerIDByType.TryAdd(tpSerializer.TargetType, tpSerializer.TypeID);
#else
            TypeSerializers.Add(tpSerializer.TypeID, tpSerializer);
            TypeSerializerIDByType.Add(tpSerializer.TargetType, tpSerializer.TypeID);
#endif

        }

#endregion --------------------------------- ADDING KNOWN TYPES -----------------------------------
                

#region ---------------------------------------- DETECT ADD AUTOCONTARACTS -------------------------------------------

        /// <summary>
        /// Auto add contract cases: Nullable struct | (Array) | (IList) | (IDictionary) | (Interface) | (Boxed Enum)
        /// </summary>       
        /// <param name="realTypeContract"></param>       
        public void DetectAddAutoContract(Type realTypeContract)
        {
            ushort? typeId = GetIfContainsContract(realTypeContract);
            if (typeId != null) return; //already contains

            lock (_locker)
            {
                if ((typeId = GetIfContainsContract(realTypeContract)) != null) return;

                //collection types with additional item types  
                if ( realTypeContract.IsICollectionType()  || realTypeContract.IsInterface )
                {
                    //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                    //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                    //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                    //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                    DetectAddAutoContractArgTypesFromCollection(realTypeContract);

                    AddKnownType(realTypeContract); //add as [Auto Adding Category Contract]

                }
                else if (realTypeContract.IsEnum  || realTypeContract.IsNullable())
                {

                    AddKnownType(realTypeContract); //add as [Auto Adding Category Contract]

                }


            }

        }



        /// <summary>
        /// Detecting if we need to add as [AUTO ADDING Category Contract] one/two of arg-types from collection Contract.
        /// </summary>
        /// <param name="collectionContract"></param>
        internal void DetectAddAutoContractArgTypesFromCollection(Type collectionContract)
        {
            Type[] argTypes = Type.EmptyTypes;

            if (collectionContract.IsArray)
            {
                argTypes = new[] { collectionContract.GetElementType() };
            }
            else if (collectionContract.IsGenericType)
            {
                argTypes = collectionContract.GetGenericArguments();
            }

            if (argTypes == null) return;//error?? 

            foreach (var itemArgType in argTypes)
            {
                ushort? itemArgTypeId = GetIfContainsContract(itemArgType);
                if (itemArgTypeId != null) continue;//already contains

                if (itemArgType.IsInterface)
                {

                    AddKnownType(itemArgType,   selector: TypeMemberSelectorEn.Default   );

                }
                else if (itemArgType.IsEnum)
                {
                    AddKnownType(itemArgType,   selector: TypeMemberSelectorEn.Default   );

                }
                else
                {
                    Validator.AssertTrue<InvalidOperationException>(itemArgTypeId == 0,
                        "DetectAddAutoContractArgTypesFromCollection(): Searching In Collection [{0}] for arg type [{1}] : Result - NOT DETERMINED. So TypeSerializer  for such Contract  unable to be load to use from Serializer Contract Dictionary."
                       , collectionContract, itemArgType);

                }

            }

        }




#endregion ---------------------------------------- DETECT ADD AUTOCONTARACTS -------------------------------------------


#region ------------------------------ GET RUNTIME TYPE ModelId -------------------------------


        /// <summary>
        ///  Get Type of data object and return typeID of this Type
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ushort GetTypeID(object data)
        {
            ushort id;

            if (data == null)
                return 0;

            var type = data.GetType();

            Validator.AssertFalse<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(type, out id),
                                                                string.Format("Unknown type {0}", type.FullName)
                                                            );

            return id;
        }


        /// <summary>
        /// Get  typeID of this dataType
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        public ushort GetTypeID(Type dataType)
        {
            ushort id;

            Validator.AssertFalse<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(dataType, out id),
                                                                string.Format("Unknown type {0}", dataType.FullName)
                                                            );

            return id;
        }


        /// <summary>
        /// Getting  Type from runtime type ModelId
        /// If type doesn't exist in Serializer.ContractsDictionry then ContractNotFoundException will be thrown
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        public Type GetTypeByTypeID(ushort typeID)
        {
            object findedItem = TypeSerializerIDByType.Where(itm => itm.Value == typeID).FirstOrDefault();

            Validator.AssertTrue<InvalidOperationException>(findedItem == null,
                                                             " Cannot find Type by runtime type ModelId - [{0}]",
                                                             typeID.S()
                                                           );

            return ((KeyValuePair<Type, ushort>)findedItem).Key;


        }


#endregion ------------------------------ GET RUNTIME TYPE ModelId -------------------------------        


#region --------------------------------- PLAIN STYLE DE/SERIALIZE -------------------------------------

        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public byte[] Serialize(object data, bool isNullable = false)
        {
            byte[] result = null;
            using (var ms = new MemoryStream())
            {
                Serialize(ms, data, isNullable);
                result = ms.ToArray();
            }
            return result;
        }

        /// <summary>
        /// Serialize object data into stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public void Serialize(Stream stream, object data, bool isNullable = false)
        {
            if (stream == null || data == null) return;

            var realType =(isNullable)? typeof(Nullable<>).MakeGenericType( data.GetType()) : data.GetType() ;

            ushort? mayBeTypeID = GetIfContainsContract(realType);
            if (mayBeTypeID == null)
            {
                DetectAddAutoContract(realType);
               
                ushort typeID = GetTypeID(realType);// now ModelId must be here  or  GetTypeID will throw if not found typeProcessor for realType

                if (realType.Is4DPrimitiveType()  || realType.IsEnum)
                {
                    //write typeID  here only for primitives - because primitives don't write itself TypeID directly
                    BinaryReaderWriter.Write(stream, typeID);
                    TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);                    
                }
                else
                {
                    TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);
                }
            }
            else if (mayBeTypeID > 0) //already contains need no to DetectAddAutoContract
            {
                if (realType.Is4DPrimitiveType()|| realType.IsEnum)
                {
                    //write typeID  here only for primitives - because primitives don't write itself TypeID directly
                    BinaryReaderWriter.Write(stream, mayBeTypeID.Value);
                    TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(stream, data);
                    
                }
                else
                {
                    TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(stream, data);                    
                }

            }
            else if (mayBeTypeID == 0) return; //null value


        }


        /// <summary>
        /// Serialize object data into  stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        public void SerializeToStream(object value,  Stream stream)
        {
            Serialize( stream , value );
        }


        /// <summary>
        /// Serialize {T} object data into  stream.
        /// </summary>
        /// <typeparam name="T"> type of value </typeparam>
        /// <param name="value"> value </param>
        /// <param name="stream"> stream  </param>
        public void SerializeToStream<T>(T value, Stream stream)
        {
            Serialize( stream , value );
        }



        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public byte[] SerializeToByteArray(object value)
        {
           return Serialize(value);
        }

       




        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public object Deserialize(Stream stream)
        {
            if (stream == null) return null;        

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID
                        
            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>( 
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);

        }


        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public T Deserialize<T>(Stream stream)
        {
            if (stream == null) return default(T);                     

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID
            
            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>(
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return (T)TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);


            //return (T)Deserialize(stream);
        }


        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public T Deserialize<T>(byte[] serializedData)
        {
            T result = default(T);
            using (var ms = new MemoryStream(serializedData))
            {
                result = Deserialize<T>(ms);
            }
            return result;
        }


        /// <summary>
        /// Deserialize  to object  from byte[].
        /// </summary>
        /// <param name="serializedData"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public object Deserialize(byte[] serializedData, Type targetType)
        {            
            using (var ms = new MemoryStream(serializedData))
            {
               return Deserialize(ms);
            }            
        }


        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public object DeserializeFromStream(Type type, Stream stream)
        {
           return Deserialize(stream);
        }

      

        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public T DeserializeFromStream<T>(Stream stream)
        {
            return Deserialize<T>(stream);
        }


        /// <summary>
        /// Deserialize  to object  from byte[].
        /// </summary>
        /// <param name="dataArray"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public object DeserializeFromByteArray(byte[] dataArray, Type targetType)
        {
            return Deserialize(dataArray, targetType);
        }

        #endregion ---------------------------------PLAIN  STYLE DE/SERIALIZE -------------------------------------


        #region ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------------------ SERIALIZE TPL ------------------------------------------


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<byte[]> SerializeTPL(object data)
        {
            return SerializeTPL(data, default(CancellationToken), TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }

        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task<byte[]> SerializeTPL(object data, CancellationToken cancelToken, TaskCreationOptions taskCretionOptions, TaskScheduler scheduler)
        {
            var state = ThreadCallState<ITypeSetSerializer, object>.Create(this, data);

            return Task.Factory.StartNew<byte[]>((st) =>
            {
                var inSt = st as ThreadCallState<ITypeSetSerializer, object>;

                if (inSt.Item2 == null) return null;

                return Serialize(inSt.Item2);               

            },
            state,
            cancelToken,
            taskCretionOptions,
            scheduler
            );

        }



        /// <summary>
        /// Serialize object data into binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public Task SerializeTPL(Stream stream, object data)
        {
            var state = ThreadCallState<ITypeSetSerializer, Stream, object>.Create(this, stream, data);

            return Task.Factory.StartNew((st) =>
            {
                //Item1- TSSerializer
                //Item2 - stream
                //Item3 - data
                var inSt = st as ThreadCallState<ITypeSetSerializer, Stream, object>;

                if (inSt.Item2 == null || inSt.Item3 == null) return;

                var realType = inSt.Item3.GetType();
                
                Serialize(inSt.Item2, inSt.Item2);
            }
            ,
            state,
            TaskCreationOptions.AttachedToParent
            );

        }



#endregion ------------------------------------------ SERIALIZE TPL ------------------------------------------



#region ------------------------------------- ContinueSerializeTPL ------------------------------------------

#if NET45 || WP81
        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream  
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public Task ContinueSerializeTPL(Task<Stream> streamTask, object data)
        {
            return ContinueSerializeTPL(streamTask, data, TaskContinuationOptions.AttachedToParent);
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options, and add CancellationToken.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <returns></returns>
        public Task ContinueSerializeTPL(Task<Stream> streamTask, object data, TaskContinuationOptions continueAsOption)
        {
            var state = ThreadCallState<ITypeSetSerializer, object>.Create(this, data);

            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as ThreadCallState<ITypeSetSerializer, object>;

                if (strm == null || inSt.Item2 == null) return;

                var realType = inSt.Item2.GetType();

                inSt.Item1.Serialize(strm, inSt.Item2);              
         
            }
            , state
            , continueAsOption
            );
        }


        public Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler)
        {
            var state = ThreadCallState<ITypeSetSerializer, object>.Create(this, data);

            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as ThreadCallState<ITypeSetSerializer, object>;

                if (strm == null || inSt.Item2 == null) return;

                var realType = inSt.Item2.GetType();

                inSt.Item1.Serialize(strm, inSt.Item2);
            }
            , state
            , cancelToken
            , continueAsOption
            , scheduler
            );
        }

#endif

#endregion ------------------------------------- ContinueSerializeTPL ------------------------------------------



#region ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------

        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public Task<object> DeserializeTPL(Stream stream)
        {

            var state = ThreadCallState<ITypeSetSerializer, Stream>.Create(this, stream);
            return Task.Factory.StartNew<object>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as ThreadCallState<ITypeSetSerializer, Stream>;

                if (inSt.Item2 == null) return null;
                              
                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt.Item2);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case      

                //check if typeID exist in Dictionary 
                Validator.AssertFalse<InvalidOperationException>(inSt.Item1.TypeSerializers.ContainsKey(typeID)
                    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                    , typeID.S());

                return inSt.Item1.TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(inSt.Item2, true);
            }
                ,
                state
                , TaskCreationOptions.AttachedToParent
                );

        }

        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public Task<T> DeserializeTPL<T>(Stream stream)
        {
            var state = ThreadCallState<ITypeSetSerializer, Stream>.Create(this, stream);
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as ThreadCallState<ITypeSetSerializer, Stream>;

                if (inSt.Item2 == null) return default(T);

                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt.Item2);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case                    

                //check if typeID exist in Dictionary 
                Validator.AssertFalse<InvalidOperationException>(inSt.Item1.TypeSerializers.ContainsKey(typeID)
                    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                    , typeID.S());

                return (T)inSt.Item1.TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(inSt.Item2, true);

            }
            ,
            state
            , TaskCreationOptions.AttachedToParent
            );
        }



#endregion ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------



#region ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------

#if NET45 || WP81

        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public Task<object> ContinueDeserializeTPL(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }



        /// <summary>
        ///  Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public Task<object> ContinueDeserializeTPL(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<object>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 var serializer = st as ITypeSetSerializer;


                 var strm = t.Result;

                 if (strm == null) return null;

                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 Validator.AssertFalse<InvalidOperationException>(serializer.TypeSerializers.ContainsKey(typeID)
                     , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                     , typeID.S());

                 return serializer.TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(strm, true);
             }
            , this
            , cancelToken // 
            , continuation
            , scheduler
            );

        }



        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL<T>(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<T>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 var serializer = st as ITypeSetSerializer;
                 
                 var strm = t.Result;

                 if (strm == null) return default(T);
                       
                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 Validator.AssertFalse<InvalidOperationException>(serializer.TypeSerializers.ContainsKey(typeID)
                     , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                     , typeID.S());

                 return (T)serializer.TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(strm, true);
             }
            , this
            , cancelToken // 
            , continuation
            , scheduler
            );

        }


#endif

#endregion ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------



#region ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------

        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public Task<T> DeserializeTPL<T>(byte[] serializedData)
        {
            var state = ThreadCallState<ITypeSetSerializer, byte[]>.Create(this, serializedData);
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as ThreadCallState<ITypeSetSerializer, byte[]>;

                if (inSt.Item2 == null) return default(T);

               


                using (var ms = new MemoryStream(inSt.Item2))
                {
                    ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(ms);//read TypeID

                    //if (typeID == 0) // null value type and value - impossible case                    

                    //check if typeID exist in Dictionary   InvalidOperationException
                    Validator.AssertFalse<InvalidOperationException>(inSt.Item1.TypeSerializers.ContainsKey(typeID)
                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary".Fmt(typeID.S()));

                    return (inSt.Item1.TypeSerializers[typeID] as ITypeSerializer<T>).RaiseDeserializeFromStreamTHandler( ms, true);
                }

            }
            ,
            state
            , TaskCreationOptions.AttachedToParent
            );

        }

#endregion ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------



#endregion ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


#region ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

#if NET45 /////// --------------------------- WRITE DEBUG CODE ASSEMBLY --------------------------------///////


        /// <summary>
        /// If this flag is true, after each BuildProcessing() call, we'll write debugging code assembly.
        /// By Default it's false. But this feature can't be used in real-time parallel work, be carefull.
        /// </summary>
        public bool UseGenerationOfDebugCodeAssembly { get; set; }

      


#endif
#endregion ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

        






#region ----------------------------------------- GET [TYPE SET SERIALIZER] NOT SAFE ---------------------------------------

        /// <summary>
        /// Get  serializer directly from Guid_Serializers_Dicitionary.
        /// Has check about Key existing and throw appropriate exception if Key doesn't exist
        /// This method stil is not thread safe       
        /// </summary>
        /// <param name="ModelId"></param>
        /// <returns></returns> 
        public static ITypeSetSerializer Get(Guid ID)
        {
            ITypeSetSerializer result = null;

            Validator.AssertTrue<InvalidOperationException>(
                               !TypeSetsByID.TryGetValue(ID, out result),
                               "Serialzer with such ModelId doesn't exist.");

            return result;
        }



        /// <summary>
        ///  Get  serializer directly from String_Serializers_Dicitionary.
        ///  Has check about Key existing and throw appropriate exception if Key doesn't exist
        ///  This method stil is not thread safe       
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static ITypeSetSerializer Get(string Key)
        {
            ITypeSetSerializer result = null;
            Validator.AssertTrue<InvalidOperationException>(
                               !TypeSetsByKey.TryGetValue(Key, out result),
                                "Serialzer with such Key doesn't exist.");

            return result;
        }


        /// <summary>
        /// Get  serializer directly from Int32_Serializers_Dicitionary.
        /// Has check about Key existing and throw appropriate exception if Key doesn't exist        
        /// This method still is not thread safe
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns>
        public static ITypeSetSerializer Get(int Index)
        {
            ITypeSetSerializer result = null;
            Validator.AssertTrue<InvalidOperationException>(
                                !TypeSetsByIndex.TryGetValue(Index, out result),
                                "Serialzer with such Index doesn't exist.");

            return result;
        }








        /// <summary>
        /// Get  serializer directly from Guid_Serializers_Dicitionary.
        /// No check about Key existing or not- remember that.
        /// That's not thread safe method but the most fastest way to access serializer. 
        /// </summary>
        /// <param name="ModelId"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetDirect(Guid ID)
        {
            return TypeSetsByID[ID];
        }


        /// <summary>
        /// Get  serializer directly from String_Serializers_Dicitionary.
        /// No check about Key existing or not- remember that.
        /// That's not thread safe method but the most fastest way to access serializer.  
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetDirect(string Key)
        {
            return TypeSetsByKey[Key];
        }


        /// <summary>
        /// Get  serializer directly from String_Serializers_Dicitionary.
        /// No check about Key existing or not- remember that.
        /// That's not thread safe method but the most fastest way to access serializer.  
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetDirect(int Index)
        {
            return TypeSetsByIndex[Index];
        }




#endregion----------------------------------------- GET [TYPE SET SERIALIZER] NOT SAFE ---------------------------------------


#region ------------------------------------------- GET SAFE [TYPE SET SERIALIZER] ---------------------------------------

        /// <summary>
        /// Get  serializer  from Guid_Serializers_Dicitionary thread safety.
        /// Contains Check about Key existing or not- remember that. 
        /// </summary>
        /// <param name="ModelId"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetSafe(Guid ID)
        {
            ITypeSetSerializer result = null;
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                                   !TypeSetsByID.TryGetValue(ID, out result),
                                   "Serialzer with such ModelId doesn't exist.");
            }
            return result;
        }


        /// <summary>
        /// Get  serializer  from String_Serializers_Dicitionary thread safety.
        /// Contains Check about Key existing or not- remember that. 
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetSafe(string Key)
        {
            ITypeSetSerializer result = null;
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                                   !TypeSetsByKey.TryGetValue(Key, out result),
                                   "Serialzer with such Key doesn't exist.");
            }
            return result;
        }


        /// <summary>
        /// Get  serializer  from Int32_Serializers_Dicitionary thread safety.
        /// Contains Check about Key existing or not- remember that.  
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns>
        public static ITypeSetSerializer GetSafe(int Index)
        {
            ITypeSetSerializer result = null;
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                                !TypeSetsByIndex.TryGetValue(Index, out result),
                                "Serialzer with such Index doesn't exist.");

            }
            return result;
        }





#endregion ------------------------------------------- GET SAFE [TYPE SET SERIALIZER]  ---------------------------------------


#region ------------------------------------ REMOVE [TYPE SET SERIALIZER] --------------------------------------


        /// <summary>
        ///  Remove  existed ITypeSetSerializer by ModelId from Guid_Serializers_Dicitionary.
        ///  Contains check that item with such ModelId already exist then it'll be removed/otherwise  TSS rise  Exception 
        ///  It is thread safe  action - with locking Guid_Serializers_Dicitionary.        
        /// </summary>
        /// <param name="ModelId"></param> 
        public static void RemoveSerializer(Guid ID)
        {

#if NET45
            //_locker.LockAction( )

            lock(_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                              !TypeSetsByID.ContainsKey(ID),
                              "Serialzer with such ModelId doesn't exist.");                
            }

            ITypeSetSerializer tssSerializer = null;
            TypeSetsByID.TryRemove(ID, out tssSerializer);

#else
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                              !TypeSetsByID.ContainsKey(ID),
                              "Serialzer with such ModelId doesn't exist.");
                TypeSetsByID.Remove(ID);
            }
#endif



        }


        /// <summary>
        ///  Remove  existed ITypeSetSerializer by ModelId from String_Serializers_Dicitionary.
        ///  Contains check that item with such ModelId already exist then it'll be removed/otherwise  TSS rise  Exception 
        ///  It is thread safe  action - with locking String_Serializers_Dicitionary.        
        /// </summary>
        /// <param name="Key"></param>
        public static void RemoveSerializer(string Key)
        {
#if NET45
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                             !TypeSetsByKey.ContainsKey(Key),
                             "Serialzer with such Key doesn't exist.");             
            }

            ITypeSetSerializer tssSerializer = null;
            TypeSetsByKey.TryRemove(Key, out tssSerializer);            

#else
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                             !TypeSetsByKey.ContainsKey(Key),
                             "Serialzer with such Key doesn't exist.");
                TypeSetsByKey.Remove(Key);
            }
#endif


        }


        /// <summary>
        ///  Remove  existed ITypeSetSerializer by ModelId from Int32_Serializers_Dicitionary.
        ///  Contains check that item with such ModelId already exist then it'll be removed/otherwise  TSS rise  Exception 
        ///  It is thread safe  action - with locking Int32_Serializers_Dicitionary.        
        /// </summary>
        /// <param name="Index"></param> 
        public static void RemoveSerializer(int Index)
        {

#if NET45
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                                !TypeSetsByIndex.ContainsKey(Index),
                                "Serialzer with such Index doesn't exist.");               
            }

            ITypeSetSerializer tssSerializer = null;
            TypeSetsByIndex.TryRemove(Index, out tssSerializer);

#else
            lock (_locker)
            {
                Validator.AssertTrue<InvalidOperationException>(
                                !TypeSetsByIndex.ContainsKey(Index),
                                "Serialzer with such Index doesn't exist.");
                TypeSetsByIndex.Remove(Index);
            }
#endif

        }




        #endregion ------------------------------------ REMOVE [TYPE SET SERIALIZER] --------------------------------------


        #region -------------------------------------- CREATE TSS LAZY THREAD SAFETY INITIALIZED --------------------------------

        /// <summary>
        /// Lazy ThreadSafety Creating and Initializing new  ITypeSetSerializer by Guid-ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        static ITypeSetSerializer CreateTSSInstanceByID(Guid id)
        {
            return LazyAct<ITypeSetSerializer>.Create(
                (args) =>
                {
                    var ID = (Guid)args[0];
                    var newSerializer = new TypeSetSerializer(ID);
                    newSerializer.Initialize();
                    return newSerializer;
                }
                , _locker,
                new object[] { id }
                ).Value;
        }

        /// <summary>
        /// Lazy ThreadSafety Creating and Initializing new  ITypeSetSerializer by String-KEY
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        static ITypeSetSerializer CreateTSSInstanceByKey(string key)
        {
            return LazyAct<ITypeSetSerializer>.Create(
                (args) =>
                {
                    var Key = (string)args[0];
                    var newSerializer = new TypeSetSerializer(Key);
                    newSerializer.Initialize();
                    return newSerializer;
                }
                , _locker,
                new object[] { key}
                ).Value;
        }


        /// <summary>
        /// Lazy ThreadSafety Creating and Initializing new  ITypeSetSerializer by Int-Index
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        static ITypeSetSerializer CreateTSSInstanceByIndex(int index)
        {
            return LazyAct<ITypeSetSerializer>.Create(
                (args) =>
                {
                    var Index = (int)args[0];
                    var newSerializer = new TypeSetSerializer(Index);

                    newSerializer.Initialize(); // initialize

                    return newSerializer;
                }
                , _locker,
                new object[] { index }
                ).Value;
        }


        #endregion -------------------------------------- CREATE TSS LAZY THREAD SAFETY INITIALIZED --------------------------------
        

        #region ---------------------------------- ADD/UPDATE [TYPE SET SERIALIZER] ------------------------------------

        /// <summary>
        ///  Add  new ServiceSerializer with ModelId to Guid_Serializers_Dicitionary.
        /// <para/>  If item with such ModelId already exist then it'll be removed first and new item with the same ModelId will be added
        /// <para/>  It is thread safe  action - with locking Guid_Serializers_Dicitionary. 
        /// <para/>  It may be used in some scenarios when you want to reload service serializer's dictionary after recreating serializer.
        /// </summary>
        /// <param name="ModelId"></param>
        /// <returns></returns>
        public static ITypeSetSerializer AddUpdateTSS(Guid ID)
        {
#if NET45                          
            ITypeSetSerializer tssSerializer = null;
            TypeSetsByID. TryRemove(ID, out tssSerializer);                                  
              
            var newSerializer = TypeSetsByID.AddOrUpdate(ID, CreateTSSInstanceByID(ID), (id,tss)=> { return tss; } );
            return newSerializer;            
#else
            lock (_locker)
            {
                if (TypeSetsByID.ContainsKey(ID)) { TypeSetsByID.Remove(ID); }

                var newSerializer = CreateTSSInstanceByID(ID);
                             
                TypeSetsByID.Add(ID, newSerializer);
                return newSerializer;
            }

#endif

        }


        /// <summary>
        ///  Add new ServiceSerializer with Key to String_Serializers_Dicitionary.
        /// <para/> If item with such Key already exist then it'll be removed first and new item with the same Key will be added
        /// <para/> It is thread safe  action - with locking String_Serializers_Dicitionary. 
        /// <para/> It may be used in some scenarios when you want to reload service serializer's dictionary after recreating serializer.
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static ITypeSetSerializer AddUpdateTSS(String Key)
        {
#if NET45
            ITypeSetSerializer removingTSS = null;
            TypeSetsByKey.TryRemove(Key, out removingTSS);

            var newSerializer = TypeSetsByKey.AddOrUpdate(Key, CreateTSSInstanceByKey(Key), (key, tss) => { return tss; });
            return newSerializer;

#else
            
            lock (_locker)
            {
                if (TypeSetsByKey.ContainsKey(Key)) { TypeSetsByKey.Remove(Key); }

                TypeSetsByKey.Add(Key, CreateTSSInstanceByKey(Key));
                return TypeSetsByKey[Key];
            }          
#endif
        }


        /// <summary>
        ///  Add new ServiceSerializer with Index to Index_Serializers_Dicitionary.
        /// <para/> If item with such Index already exist then it'll be removed first and new item with the same Index will be added
        /// <para/> It is thread safe  action - with locking Index_Serializers_Dicitionary. 
        /// <para/> It may be used in some scenarios when you want to reload service serializer's dictionary after recreating serializer.     
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static ITypeSetSerializer AddUpdateTSS(int index)
        {
#if NET45

            ITypeSetSerializer removingTSS = null;
            TypeSetsByIndex.TryRemove(index, out removingTSS);

            return TypeSetsByIndex.AddOrUpdate(index, CreateTSSInstanceByIndex(index), (indx, tss) => { return tss; });          

#else
            lock (_locker)
            {
                if (TypeSetsByIndex.ContainsKey(index)) { TypeSetsByIndex.Remove(index); }

                TypeSetsByIndex.Add(index, CreateTSSInstanceByIndex(index));
                return TypeSetsByIndex[index];
            }
#endif
            
        }


        #endregion ---------------------------------- ADD/UPDATE[TYPE SET SERIALIZER]------------------------------------


        #region -----------------------------------ADD OR USE EXIST [TYPE SET SERIALIZER] ----------------------------------

        /// <summary>
        /// Add new Type Set Serializer(Set of Types of service Model) with ModelId key to Guid_Serializers_Dicitionary only if it still wasn't there.
        /// <para/> Then return newly created or existed Serializer instance by ModelId key.
        /// <para/> It is thread safe  action - with locking Guid_Serializers_Dicitionary.
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public static ITypeSetSerializer AddOrUseExistTSS(Guid ID)
        {
#if NET45
            return TypeSetsByID.GetOrAdd(ID, CreateTSSInstanceByID(ID));            
#else
            lock (_locker)
            {               
                if (! TypeSetsByID.ContainsKey(ID))
                {  
                    TypeSetsByID.Add(ID, CreateTSSInstanceByID(ID));
                    return TypeSetsByID[ID];
                }                
                else return TypeSetsByID[ID];
            }
#endif         
        }


        /// <summary>
        /// Add new ServiceSerializer with String key to String_Serializers_Dicitionary only if it still wasn't there.
        /// <para/> Then return newly created or existed Serializer instance by Key.
        /// <para/> It is thread safe  action - with locking String_Serializers_Dicitionary.
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        public static ITypeSetSerializer AddOrUseExistTSS(string Key)
        {

#if NET45
            return TypeSetsByKey.GetOrAdd(Key, CreateTSSInstanceByKey(Key));
#else
            lock (_locker)
            {
                if (!TypeSetsByKey.ContainsKey(Key))
                {
                    TypeSetsByKey.Add(Key, CreateTSSInstanceByKey(Key));
                    return TypeSetsByKey[Key];
                }
                else return TypeSetsByKey[Key];
            }
#endif

        }


        /// <summary>
        /// Add new ServiceSerializer with String key to Index_Serializers_Dicitionary only if it still wasn't there.
        /// <para/> Then return newly created or existed Serializer instance by Index.
        /// <para/> It is thread safe  action - with locking Index_Serializers_Dicitionary.
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns> 
        public static ITypeSetSerializer AddOrUseExistTSS(int index)
        {
#if NET45
            return TypeSetsByIndex.GetOrAdd(index, CreateTSSInstanceByIndex(index));
#else
            lock (_locker)
            {
                if (!TypeSetsByIndex.ContainsKey(index))
                {
                    TypeSetsByIndex.Add(index, CreateTSSInstanceByIndex(index));
                    return TypeSetsByIndex[index];
                }
                else return TypeSetsByIndex[index];
            }
#endif                     
        }

        
#endregion -----------------------------------ADD OR USE EXIST [TYPE SET SERIALIZER] ----------------------------------
        

        #region  -----------------------------------DE/SERIALIZE GENERATORS -------------------------------------------


        /// <summary>
        /// It means first  Generator that  can  generate type Handlers will be used. 
        /// <para/> i.e. last added custom generator will be used first to generate type Handlers from it's Type Kind -because it'll be first item from top of the stack.
        /// </summary>
        internal static LazyAct<List<ITypeSerializerGenerator> > LA_SerializeGenerators = LazyAct< List<ITypeSerializerGenerator> >.Create( 
            (args)=>
            {
                //Loading all existed type De/Serialzie Generators 
                List<ITypeSerializerGenerator> generatorsResult = new List<ITypeSerializerGenerator>();

                var generatorTypes = typeof(ITypeSerializerGenerator).Assembly.GetTypes()
                          .Where(tp => typeof(ITypeSerializerGenerator).IsAssignableFrom(tp) && tp.IsClass && !tp.IsAbstract).ToList();

                if (generatorTypes.Count == 0) return generatorsResult;

                foreach (var genType in generatorTypes)
                {
                    var generator = (ITypeSerializerGenerator)TypeActivator.CreateInstanceBoxed(genType);
                    generatorsResult.Add(generator);
                }
                return generatorsResult;
            }
            , null//locker
            , null //args
            );

      

        
        internal static void LoadDefaultTypeSerializersFromSerializeGenerators(ITypeSetSerializer serializer)
        {
            string currentGeneratorName = "";
            try
            {
                foreach (var genrtr in LA_SerializeGenerators.Value)
                {
                    currentGeneratorName = genrtr.GetType().FullName;
                    if (genrtr.CanLoadDefaultTypeSerializers)
                    {
                        genrtr.LoadDefaultTypeSerializers(serializer);
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(
                    $" {nameof(TypeSetSerializer)}.{nameof(LoadDefaultTypeSerializersFromSerializeGenerators)}(): ERROR on loading Default TypeSerializers from  [{currentGeneratorName}] - Message:[{exc.Message}] ");
            }            
        }




        /// <summary>
        /// Get Type Serialize Generator by targetType 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static ITypeSerializerGenerator GetTypeSerializerGeneratorByType(Type targetType)
        {
            return LA_SerializeGenerators.Value.Where(sg => sg.CanHandle(targetType)).FirstOrDefault();
        }


       

        /// <summary>
        ///  Check if Contract exist in Serializer Dictionary. 
        /// </summary>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        public ushort? GetIfContainsContract(Type type)
        {
            if (type == null) return null;

            if (TypeSerializerIDByType.ContainsKey(type))
            {
                return TypeSerializerIDByType[type];
            }
            return null;
        }





        /// <summary>
        ///  Check if current TSS instance can serialize  Type - targetType.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public bool CanSerialize(Type targetType)
        {
            // 1 if ICollection  -Array  get ArrayElementType 
            // 2 if ICollection  -List get   Arg1Type 
            // 3 if ICollection  -Dictionary get  Arg1Type + Arg2Type            
            // 4 simple Test- Contains in Dictionary

            if (targetType.IsArray)
            {
                return CanSerialize(targetType.GetElementType());
            }
            else if (TypeInfoEx.IsImplement_List(targetType))
            {
                return CanSerialize(targetType.GetGenericArguments()[0]);
            }
            else if (TypeInfoEx.IsImplement_Dictionary(targetType))
            {
                return (CanSerialize(targetType.GetGenericArguments()[0])
                     && CanSerialize(targetType.GetGenericArguments()[1]));
            }

            else if (true) { return true; }// targetType.IsAutoAdding()rtue

            // it's nullable or usual struct or class type - all of them should already contained in TypeSerializerIDByType
            else return TypeSerializerIDByType.ContainsKey(targetType);
            
        }


        /// <summary>
        /// Check if current TSS instance can serialize  Type - typeof(T).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public bool CanSerialize<T>()
        {
            return CanSerialize(typeof(T));
        }


        /// <summary>
        ///  Check if current TSS instance can serialize Type of data instance - [dataItem.GetType()].
        /// </summary>
        /// <param name="dataItem"></param>
        /// <param name="throwOnNullValue"></param>
        /// <returns></returns>
        public bool CanSerialize(object dataItem, bool throwOnNullValue )
        {
            if (dataItem == null && throwOnNullValue) throw new InvalidOperationException($" ERROR in {nameof(TypeSetSerializer)}.{nameof(CanSerialize)}() : dataItem can't be null to determine serialization possibilities ");
            else if (dataItem == null) return false;

            return CanSerialize(dataItem.GetType());
        }
         



#endregion -----------------------------------DE/SERIALIZE GENERATORS -------------------------------------------
        

#region ------------------------------------ STERLING INTEGRATION SERIALIZATION OVERLOADS -------------------------------------     
        
        //NOT IMPLEMENTED 

        public void Serialize(object target, BinaryWriter writer)
        {
            throw new NotImplementedException(RCX.ERR_NotImplementedOperation.Fmt(nameof(TypeSetSerializer), nameof(Serialize)
                   , "overload with BinaryWriter not implemented yet."));
        }


        public object Deserialize(Type type, BinaryReader reader)
        {
            throw new NotImplementedException( RCX.ERR_NotImplementedOperation.Fmt(nameof(TypeSetSerializer), nameof(Deserialize)
                , "overload with BinaryReader  not implemented yet.")  );
        }

        public T Deserialize<T>(BinaryReader reader)
        {
            throw new NotImplementedException(RCX.ERR_NotImplementedOperation.Fmt(nameof(TypeSetSerializer), nameof(Deserialize)
            , "overload with BinaryReader not implemented yet."));
        }





        #endregion ------------------------------------ STERLING INTEGRATION SERIALIZATION OVERLOADS -------------------------------------






    }
}





#region --------------------------------- GARBAGE  ----------------------------------


///// <summary>
///// Check if Contract exist in Serializer Dictionary. 
///// </summary>
///// <param name="contract"></param>
///// <returns></returns>
//public ushort? GetContainsContract(Type type)
//{
//    if (type == null) return null;

//    if (TypeSerializerIDByType.ContainsKey(type))
//    {
//        return TypeSerializerIDByType[type];
//    }
//    return null;

//}

//ushort? mayBeTypeID = inSt.Item1.GetIfContainsContract(realType);
//if (mayBeTypeID == null)
//{
//    if (realType.IsAutoAdding()) inSt.Item1.AddKnownType(realType);                    

//    ushort typeID = inSt.Item1.GetTypeID(inSt.Item2);// now ModelId must be here  or  GetTypeID will throw if not found typeProcessor for realType

//    if (realType.Is4DPrimitiveType()  || realType.IsEnum)
//    {
//        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
//        BinaryReaderWriter.Write(strm, typeID);
//        inSt.Item1.TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(strm, inSt.Item2);
//    }
//    else
//    {
//        inSt.Item1.TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(strm, inSt.Item2);
//    }
//}
//else if (mayBeTypeID > 0) //already contains need no to DetectAddAutoContract
//{
//    if (realType.Is4DPrimitiveType() || realType.IsEnum)
//    {
//        //write typeID  here only for primitives - because primitives don't write itself TypeID directly
//        BinaryReaderWriter.Write(strm, mayBeTypeID.Value);
//        inSt.Item1.TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(strm, inSt.Item2);
//    }
//    else
//    {
//        inSt.Item1.TypeSerializers[mayBeTypeID.Value].RaiseSerializeToStreamBoxedHandler(strm, inSt.Item2);
//    }

//}
//else if (mayBeTypeID == 0) return; //null value




//Check if need and prepare TypeProcessors for Service Domain Contracts
//if (UseAutoPrebuild && NeedBuildTypeProcessor)
//{
//    BuildTypeProcessor();
//}



///// <summary>
/////  AddContract overload for - contract that should belong to [Custom Adding Category Contract].
/////  In CustomAdding Category  - we want to add contract that can have: 
/////         can set Constructor test options;
/////         custom Policy to build members; 
/////         custom fields set ;
/////         custom properties set ; 
/////          - thats all. 
/////  If contract will belong to Tools.IsAutoAddingContractCategory or Tools.IsPrimitiveType then ContractAddException will be thrown  
///// </summary>
///// <typeparam name="T"></typeparam>
///// <param name="selector"></param>
///// <param name="fieldsBinding"></param>
///// <param name="propertiesBinding"></param>
///// <param name="onlyMembers"></param>
///// <param name="ignoreMembers"></param>
///// <returns></returns>
//public ushort AddContract<T>(
//                            TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
//                           , BindingFlags fieldsBinding = BindingFlags.Default
//                           , BindingFlags propertiesBinding = BindingFlags.Default
//                            , List<string> onlyMembers = null
//                           , List<string> ignoreMembers = null
//                            )
//{
//    return AddContract(typeof(T), null, null
//                        , selector
//                        , fieldsBinding, propertiesBinding, onlyMembers, ignoreMembers
//                        );
//}




//this.Identity = new Identification() { SvcIndex = SvcIndex};

//if (!IsInitialized)
//{   
//    //if (!IsInitialized)
//    //{


//    //this.Identity = new Identification() { IdentityType = IdentityTypeEn.IdentityWitSvcKeyName, SvcID = Guid.NewGuid(), Version = new DNVersion() {  SvcKeyName = SvcKeyName };
//    //}

//    // ServiceDomainModel = new DModel();
//    //this.Identity = new Identification() { SvcID = SvcGuidKey };
//}

//Check if need and prepare TypeSerializer  for Service Domain Contracts
//if (inSt.Item1.UseAutoPrebuild) { if (inSt.Item1.NeedBuildTypeProcessor) { inSt.Item1.BuildTypeProcessor(); } }

/// <summary>
/// 
/// </summary>
//internal static List<ITypeSerializerGenerator> LoadSerializeGenerators()
//{

//}

//internal static List<ITypeSerializerGenerator> GetSerializeGenerators()
//{
//    return SerializeGenerators.Where(sr => sr.GetType() != typeof(SerializeGenerator_Primitives)).ToList();
//}


/// <summary>
/// Choose generator that will generate Handlers for TypeSerializer.Contract
/// </summary>
/// <param name="typeProcessor"></param>
//internal static void SetTypeSerializerGeneratorToTypeSerializer(ITypeSerializer typeSerializer)
//{
//    var generator = SerializeGenerators
//                            .Where(sgen =>
//                                   sgen.CanHandle(typeSerializer.Accessor.TAccess))
//                            .FirstOrDefault();

//    Validator.AssertTrue<ArgumentNullException>((generator == null), $"TypeSerializerGenerator was not found for contract [{typeSerializer.Accessor.TAccess.FullName}]");


//    typeSerializer. SetSerializerGenerator(generator);

//}

//public void AddAutoContract(Type fieldType)
//{
//    throw new NotImplementedException();
//}

#region ---------------------------------- EXPORT TSS to DOMAINMODEL ---------------------------------------

///// <summary>
///// Exporting TSSerializer instancies by IdentityStringKeys ( it means only from [SerializersByKey TSS dictionary] ),
/////  into DomainModels and serialize them 
///// -to prepare them to the transmitting. 
///// </summary>
///// <param name="IdentityStringKeys"></param>
///// <returns></returns>
//public static byte[] ExportTSSToDomainModels(params String[] IdentityStringKeys)
//{
//    //List<DModel> into byte[]
//    return null;
//}

///// <summary>
///// Exporting TSSerializer instancies by IdentityStringKeys ( it means only from [SerializersByKey TSS dictionary] ),
/////  into DomainModels and serialize them 
///// -to prepare them to the transmitting. 
///// </summary>
///// <param name="IdentityIndexKeys"></param>
///// <returns></returns> 
//public static byte[] ExportTSSToDomainModels(params int[] IdentityIndexKeys)
//{
//    //List<DModel> into byte[]
//    return null;
//}

///// <summary>
///// Exporting TSSerializer instancies by IdentityStringKeys ( it means only from [SerializersByKey TSS dictionary] ),
/////  into DomainModels and serialize them 
///// -to prepare them to the transmitting. 
///// </summary>
///// <param name="IdentityIDKeys"></param>
///// <returns></returns> 
//public static byte[] ExportTSSToDomainModels(params Guid[] IdentityIDKeys)
//{
//    //List<DModel> into byte[]
//    return null;
//}

///// <summary>
///// Exporting TSSerializer instancies by IdentityStringKeys ( it means only from [SerializersByKey TSS dictionary] ),
/////  into DomainModels and serialize them 
///// -to prepare them to the transmitting. 
///// </summary>
///// <param name="IdentityKeys"></param>
///// <returns></returns> 
//public static byte[] ExportTSSToDomainModels(params KeyValuePair<IdentityTypeEn, String>[] IdentityKeys)
//{
//    //List<DModel> into byte[]
//    return null;
//}



#endregion ---------------------------------- SAVE-EXPORT TSS to DOMAINMODEL ---------------------------------------



///// <summary>
/////  Full  AddContract overload Creating TypeSerializer for different Contract Category value.  
///// </summary>
///// <param name="contract"> some Type that can be  Processed by this Serializer </param>
///// <param name="category"></param>
///// <param name="serializeHandler"></param>
///// <param name="deserializeHandler"></param>
///// <param name="CanTestDefaultConstructorOption"></param>
///// <param name="DefaultConstructorCleanerOption"></param>
///// <param name="NeedToBeAnalyseAndBuild"></param>
///// <param name="NeedToBeGenerateDeSerializeHandlers"></param>
///// <param name="selector"></param>
///// <param name="propertiesBinding"></param>
///// <param name="fieldsBinding"></param>
///// <param name="OnlyMembers"></param>
///// <returns></returns>
//private ushort AddContract(Type contract

//                            //Delegate serializeHandler = null,
//                            //Delegate deserializeHandler = null
//                            , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

//                            , BindingFlags propertiesBinding = BindingFlags.Default
//                            , BindingFlags fieldsBinding = BindingFlags.Default

//                            , List<string> onlyMembers = null
//                            , List<string> ignoreMembers = null


//                            )
//{
//    ushort? typeID = 0;
//    //We cannot add  Contract Type that already exist
//    if ((typeID = GetContainsContract(contract)) != null) return typeID.Value;


//    lock (_locker)
//    { // LOCK

//        if ((typeID = GetContainsContract(contract)) != null) return typeID.Value;

//        typeID = (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);

//        if (contract.IsAutoAdding())
//        {
//            // check that contract is really Auto Adding Contract
//            // enums/IList/IDictionary/Array and interfaces

//            // cannot be other tan real AutoAdding Category Contract

//            if (contract.IsArray)
//            {
//                //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
//                DetectAddAutoContractArgTypesFromCollection(contract);

//                var typeSerializer = CreateTypeSerializer(contract, this);
//                AddItemToTypeSetDictionaries(typeSerializer);

//                //Handlers is null and need to be generated                           
//                //contractProcessor.Category = category;
//                //contractProcessor.SetHostSerializer(this);
//                //array has no SetCtor
//                //contractProcessor.CanTestDefaultCtorOption = false;
//                //contractProcessor.DefaultCtorCleanerOption = false;

//                //contractProcessor.NeedToBeAnalyseAndBuild = false;//false
//                //contractProcessor.NeedToGenerateDeSerializeHandlers = true;

//                //need no Policy and BuildMembers; but need to Generate  Handlers Expression                           

//            }
//            else if (typeof(IList).IsAssignableFrom(contract))
//            {
//                //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//                DetectAddAutoContractArgTypesFromCollection(contract);

//                var typeSerializer = CreateTypeSerializer(contract, this);//typeID.Value
//                AddItemToTypeSetDictionaries(typeSerializer);

//                //var contractProcessor = TypeSerializer.Create(contract, typeID.Value);
//                //Handlers is null and need to be generated                           
//                //contractProcessor.Category = category;
//                //contractProcessor.SetHostSerializer(this);
//                //contractProcessor.SetCtor();
//                //contractProcessor.CanTestDefaultCtorOption = false;
//                //contractProcessor.DefaultCtorCleanerOption = false;

//                //contractProcessor.NeedToBeAnalyseAndBuild = false;// false;
//                //contractProcessor.NeedToGenerateDeSerializeHandlers = true;

//                //need no Policy and BuildMembers; but need  Generate  Handlers Expression



//            }
//            else if (typeof(IDictionary).IsAssignableFrom(contract))
//            {
//                //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
//                DetectAddAutoContractArgTypesFromCollection(contract);//detect Key or Value Contract Arg

//                var typeSerializer = CreateTypeSerializer(contract, this);//typeID.Value
//                AddItemToTypeSetDictionaries(typeSerializer);

//                //Handlers is null and need to be generated                           
//                //contractProcessor.Category = category;
//                //contractProcessor.SetHostSerializer(this);
//                //contractProcessor.SetCtor();
//                //contractProcessor.CanTestDefaultCtorOption = false;
//                //contractProcessor.DefaultCtorCleanerOption = false;

//                //contractProcessor.NeedToBeAnalyseAndBuild = false;// false;
//                //contractProcessor.NeedToGenerateDeSerializeHandlers = true;

//                //need no Policy and BuildMembers; but need  Generate  Handlers Expression



//            }
//            //если интерфейс либо Инам  которого все еще нет то дабавить обработчики для контракта и добавить сразу запустить их
//            else if (contract.IsInterface)
//            {
//                DetectAddAutoContractArgTypesFromCollection(contract);

//                var typeSerializer = CreateTypeSerializer(contract, this);//typeID.Value
//                AddItemToTypeSetDictionaries(typeSerializer);

//                //Handlers is null and need to be generated                           
//                //contractProcessor.Category = category;
//                //contractProcessor.SetHostSerializer(this); //has no SetCtor
//                //contractProcessor.CanTestDefaultCtorOption = false;
//                //contractProcessor.DefaultCtorCleanerOption = false;

//                //contractProcessor.NeedToBeAnalyseAndBuild = false;// false;
//                //contractProcessor.NeedToGenerateDeSerializeHandlers = true;

//                //need no Policy and BuildMembers; but need  Generate  Handlers Expression


//            }
//            else if (contract.IsEnum)
//            {

//                var typeSerializer = CreateTypeSerializer(contract, this);//typeID.Value
//                AddItemToTypeSetDictionaries(typeSerializer);

//                //Handlers is null and need to be generated                           
//                //contractProcessor.Category = category;
//                //contractProcessor.SetHostSerializer(this);
//                //contractProcessor.SetCtor();
//                ////contractProcessor.CanTestDefaultCtorOption = false;
//                //contractProcessor.DefaultCtorCleanerOption = false;

//                //contractProcessor.NeedToBeAnalyseAndBuild = false;
//                //contractProcessor.NeedToGenerateDeSerializeHandlers = true;

//                //need no Policy and BuildMembers; but need  Generate  Handlers Expression



//            }

//            //break;

//        }
//        else if (contract.Is4DPrimitiveType())
//        {
//            // default contract adding: have  serializeHandler & deserializeHandler- need no to generate expressions                        
//            // can be Primitive contract                      


//            var typeSerializer = CreateTypeSerializer(contract, this);//typeID.Value
//                                                                      // PRIMITIVE DEFAULT HANDLERS
//                                                                      //contractProcessor.SetDefaultSerializeHandler(serializeHandler);
//                                                                      //contractProcessor.SetDefaultDeserializeHandler(deserializeHandler);

//            AddItemToTypeSetDictionaries(typeSerializer);

//            //contractProcessor.Category = category;
//            //contractProcessor.SetHostSerializer(this);
//            //contractProcessor.SetCtor();


//            //contractProcessor.CanTestDefaultCtorOption = CanTestDefaultConstructorOption;
//            //contractProcessor.DefaultCtorCleanerOption = DefaultConstructorCleanerOption;

//            //contractProcessor.NeedToBeAnalyseAndBuild = false;
//            //contractProcessor.NeedToGenerateDeSerializeHandlers = false;
//            //need no Policy and BuildMembers; need no Generate  Handlers Expression



//            // break;
//        }
//        else
//        { //custom Contract


//            Validator.AssertTrue<InvalidOperationException>(contract.IsAutoAdding()
//                              , "Contract Add error: we cannot add [{0}] type, because it was classified as Auto Adding Category contract"
//                              , contract);


//            //mismath between OnlyMembersPolicy and  empty OnlyMembers array  
//            Validator.AssertTrue<InvalidOperationException>(
//                              (selector == TypeMemberSelectorEn.OnlyMembers && onlyMembers.Count == 0),
//                               "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_NEED_ITEM_ERR"
//                                        );


//            //mismath between not empty OnlyMembers array and incorrect selector
//            Validator.AssertTrue<InvalidOperationException>(
//                              (onlyMembers.Count > 0 && selector != TypeMemberSelectorEn.OnlyMembers),
//                               "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_ITEMS_INCORRECT_POLICY_ERR"
//                                        );


//            //политика не поддерживает кастомный Биндинг для свойств или для полей
//            Validator.AssertTrue<InvalidOperationException>(
//                 (propertiesBinding != BindingFlags.Default || fieldsBinding != BindingFlags.Default)
//              && (selector != TypeMemberSelectorEn.OnlyMembers && selector != TypeMemberSelectorEn.Custom),
//                     "Contract Add error: Current Processing Policy doesn't support custom Bindings for Properties or Fields."
//                   + "Only ONLY_MEMBER and CUSTOM Porocessing policies support this feature. Look  at the  contract{0}"
//                               , contract);


//            ////Статические поля  не должны быть включены в Биндинг или ошибка
//            Validator.AssertTrue<InvalidOperationException>(
//                               propertiesBinding.HasFlag(BindingFlags.Static) || fieldsBinding.HasFlag(BindingFlags.Static),
//                               "Member Analyze error: Your Custom Binding cannot contains Static flag. Look  at the  contract{0}."
//                               , contract);


//            var typeSerializer = CreateTypeSerializer(contract, this, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);//typeID.Value

//            AddItemToTypeSetDictionaries(typeSerializer);

//            //var contractProcessing = TypeSerializer.Create(contract, typeID.Value, selector);
//            //contractProcessing.Category = category;//


//            //contractProcessing.SetHostSerializer(this);
//            //contractProcessing.SetCtor();

//            //contractProcessing.NeedToBeAnalyseAndBuild = NeedToBeAnalyseAndBuild;
//            //if (NeedToBeAnalyseAndBuild == true) NeedBuildTypeProcessor = true;//change state of TSSerializer to NeedToBuildTypeProcessors 

//            //contractProcessing.NeedToGenerateDeSerializeHandlers = NeedToBeGenerateDeSerializeHandlers;


//            //contractProcessing.CanTestDefaultCtorOption = Tools.DetermineEnableContractCtorTest(contract, CanTestDefaultConstructorOption);
//            //contractProcessing.DefaultCtorCleanerOption = (CanTestDefaultConstructorOption == true && DefaultConstructorCleanerOption == true);



//            // if  type  - CustomStruct- is ValueType, we'll add it's nullable brother type- CustomStruct?.                        
//            if (contract.IsValueType && !contract.IsNullable() && !contract.IsEnum)
//            {
//                var contractNullableTwinBrother = typeof(Nullable<>).MakeGenericType(contract);

//                AddContract(
//                      contractNullableTwinBrother
//                     //,
//                     //, ContractCategoryEn.CustomContract // nullable struct  for custom struct- 
//                     //, null, null // no handlers-  need to Generate Handlers expressions                     

//                     //, CanTestDefaultConstructorOption, DefaultConstructorCleanerOption
//                     //, NeedToBeAnalyseAndBuild, NeedToBeGenerateDeSerializeHandlers

//                     , selector
//                     , propertiesBinding
//                     , fieldsBinding
//                     , onlyMembers
//                    );

//            }


//            //break;
//        }




//        return typeID.Value;
//    } // LOCK

//}



//static Func<Type, ushort, TypeMemberSelectorEn, List<string>, List<string>, ITypeSerializer> PrecompileCreateTypeSerializer()
//{
//    var createMethod = typeof(TypeSerializer<>).MakeGenericType(tp).GetMethod("Create");
//    var currentTypeParam = Expression.Parameter(typeof(Type), "currentType");
//    var idParam = Expression.Parameter(typeof(ushort), "typeID");
//    var selectorParam = Expression.Parameter(typeof(TypeMemberSelectorEn), "selector");
//    var onlyMembersParam = Expression.Parameter(typeof(List<string>), "onlyMembers");
//    var ignoreMembersParam = Expression.Parameter(typeof(List<string>), "ignoreMembers");

//    // TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
//    // , List< string > onlyMembers = null
//    // , List<string> ignoreMembers = null


//    return Expression.Lambda<Func<Type, ushort, TypeMemberSelectorEn, List<string>, List<string>, ITypeSerializer>>(
//                      Expression.Call(createMethod, currentTypeParam, idParam, selectorParam, onlyMembersParam, ignoreMembersParam),
//                      currentTypeParam, idParam, selectorParam, onlyMembersParam, ignoreMembersParam
//        ).Compile();

//}


//static Func<Type, ushort, TypeMemberSelectorEn, List<string>, List<string>, ITypeSerializer> createTypeSerializerFunc = PrecompileCreateTypeSerializer();




//switch (category)
//               {
//                   case ContractCategoryEn.DefaultAddingContract:
//                       {

//                       }

//                   case ContractCategoryEn.AutoAddingContract:
//                       {

//                       }

//                   case ContractCategoryEn.CustomContract:
//                       {

//                       }

//               }//switch end



#region --------------------------- ADDING CONTRACT -------------------------------

///// <summary>
///// Add Contracts Set/Array at once to TSSerializer 
///// </summary>
///// <param name="Contracts"></param>
///// <returns></returns>
//public Task<bool> AddContractsTPL(params Type[] Contracts)
//{
//    var state = ThreadCallState<ITypeSetSerializer, Type[]>.Create(this, Contracts);
//    return Task.Factory.StartNew<bool>(
//        () =>
//        {
//            foreach (var cntrct in Contracts)
//            {
//                if (Tools.IsAutoAddingContractCategory(cntrct))
//                {    //Add as AutoAdding Contract
//                    AddContract(cntrct);
//                }
//                else //Add as Custom Adding Contract
//                    AddContract(cntrct, CanTestDefaultConstructorOption: true,
//                        DefaultConstructorCleanerOption: false,
//                        NeedToBeAnalyseAndBuild: true,//we beleave that all [Custom Adding Contracts] need to be analysed
//                        selector: TypeMemberSelectorEn.DefaultPolicy,
//                        fieldsBinding: BindingFlags.Default, propertiesBinding: BindingFlags.Default);

//            }
//            return true;
//        }
//        , TaskCreationOptions.AttachedToParent
//        );
//}
#endregion --------------------------- ADDING CONTRACT -------------------------------


#region -------------------------------------  BUILD TypeSerializer --------------------------------------


//internal void BuildNeedToGenerateDeSerializeHandlersTypeProcessors()
//{
//    //2 Stage - Generating De/Serialize Handlers in TypeProcessors that really need it
//    foreach (var tpProcessor in NeedToGenerateDeSerializeHandlersTypeProcessors)
//    {
//        tpProcessor.AnalyzeRuntimeContractTypeIDs();//Runtime Contract sub types IDs

//        TSSV3.SetTypeSerializeGeneratorToTypeProcessor(tpProcessor);
//        tpProcessor.GenerateTypeSerializeHandler();
//        tpProcessor.GenerateTypeDeserializeHandler();
//        tpProcessor.NeedToGenerateDeSerializeHandlers = false;
//    }
//}


/// <summary>
/// Build Processing Info for all  Service Domain Contracts, which still wasn't builded 
/// </summary>
//        public void BuildTypeProcessor()
//        {
//            lock (this)
//            {
//                // 1 Stage Check if ServiceSerializerstate really need to be Build
//                if (NeedBuildTypeProcessor)
//                {
//                    //1 Stage - Analyse members, Add Auto Contracts, Build Get/Set Delegate
//                    Dictionary<Type, ITypeSerializer> autoContractsCommon = new Dictionary<Type, ITypeSerializer>();

//                    //т.к. контракты могут автоматически добавляться в словарь как автоопределенные поэтому мы должны использовать отдельную копию словаря для цикла foreach
//                    //var preProcessingsByContract = ProcessingsByContract.Where(itm => !itm.Value.IsDefault && itm.Value.HasBuiltTypeProcessor  == false).ToList();

//                    while (NeedToAnalyzeBuildTypeProcessors.Count > 0)
//                    {
//                        var typeProcessorToBuild = NeedToAnalyzeBuildTypeProcessors.First();

//                        //если пользователь добавляет Абстрактный класс-он не проходит тестиорвание    /Интерфейс/Инам - автоопределяются вообще
//                        //то все же не тестировать их
//                        if (typeProcessorToBuild.Contract.IsInterface || typeProcessorToBuild.Contract.IsAbstract)
//                        {
//                            typeProcessorToBuild.NeedToBeAnalyseAndBuild = false;
//                            continue;
//                        }

//                        //// creating def value delegate  for class/struct/enum
//                        //typeProcessorToBuild.SetCtor();

//                        if (typeProcessorToBuild.Contract.IsEnum)
//                        {
//                            typeProcessorToBuild.NeedToBeAnalyseAndBuild = false;
//                            continue;
//                        }

//                        //CtorFunc Test if need. Not for enum                     
//                        typeProcessorToBuild.TryCtorTest();

//                        // analyze members and their types and build processing info. If member type is interface or enum - create TypeSerializer  automatically for it. 
//                        // Collect all autoTypeProcessor  into Dictionary 
//                        if (!(typeProcessorToBuild. Category == ContractCategoryEn.AutoAddingContract))
//                        {
//                            //Only for Custom Contracts not for auto Added Contracts
//                            typeProcessorToBuild.BuildMembersTypeProcessor();// Detecting and Add Automatically Auto-Contracts        
//                        }

//                        typeProcessorToBuild.NeedToBeAnalyseAndBuild = false;
//                    }

//                    NeedBuildTypeProcessor = false;
//                }

//                //2 Building TypeProcessors- that need now that their serialize/deserialize handlers will be created.   
//                BuildNeedToGenerateDeSerializeHandlersTypeProcessors();

//                ////3 DModel
//                //BuildSerializerDomainModel();

//#if NET45 && DEBUG   
//                if (UseGenerationOfDebugCodeAssembly == true)
//                {
//                    //in DEBUG it'll be called
//                    //DebugCodeGenerator.GenerateDebugAssemblyWithHandlers(this);
//                }
//#endif

//            }

//        }



////bool _NeedBuildTypeProcessor  = false;
///// <summary>
///// One or more contracts in dictionary need to analyze, test and build their TypeSerializer.  
///// Always changes to true after new Contract will be added. 
///// </summary>
////public bool NeedBuildTypeProcessor = false;
////{
////    get;

////    private set;
////    //{
////    //    return (ProcessingsByShortID.Values.Where(itm => itm.NeedToBeAnalyseAndBuild == true).Count() > 0);
////    //}
////}


////NeedToBuildAccessor - TypeAccessorState
////NeedToBeGenerated   - TypeSerializerState

////public List<ITypeSerializer> Get_NeedToAnalyzeBuildTypeSerializers()
////{
////    return TypeSerializers.Values
////                    .Where(itm => (!(itm.Accessor.TAccessEx.IsAutoAdding) && itm.State == TypeSerializerStateEn.NeedToGenerate ) )
////                    .ToList();

////}

///// <summary>
///// TypeProcessors that need to be Analysed and Builded 
///// </summary>
//public List<ITypeSerializer> NeedToAnalyzeBuildTypeProcessors
//{
//    get
//    {
//    }
//}


///// <summary>
///// it's TypeProcessors that need to generate serialize/deserialize handlers  for it's custom contracts
///// </summary>
//public List<TypeSerializer> NeedToGenerateDeSerializeHandlersTypeProcessors
//{
//    get
//    {
//        return ProcessingsByShortID.Values
//             .Where(itm => itm.NeedToGenerateDeSerializeHandlers == true)
//            .ToList();
//    }
//}



///// <summary>
///// If true -then check NeedBuildTypeProcessor  and Building  processing info/
///// If false- then won't check NeedBuildTypeProcessor  and won't Build Processing info
///// </summary>
//public bool UseAutoPrebuild
//{
//    get;
//    set;
//}


#endregion -------------------------------------  BUILD TypeSerializer --------------------------------------


#region -------------------------- IDENTIFICATION ----------------------------------

///// <summary>
///// Service Domain Model.  
///// </summary>
//public DModel ServiceDomainModel
//{
//    get;
//    private set;
//}


///// <summary>
///// TSSerializerV2 identification. When you create serializer by TSSContainer you use StringKey/IntKey/GuidKey/EnumKEy. 
///// TSSIdentity save one value of these keys  inside  this property struct.
///// Also these keys used to save or load serializer DomainDataModel -configuration for 
///// </summary>
//public Identification Identity
//{
//    get;
//    private set;
//}





#endregion -------------------------- IDENTIFICATION ----------------------------------


/// <summary>
/// Contracts deserialize handlers - store dictionary for all contract deserialization handlers
/// Analyzer should collect info about Item : it's signature type(base/end), and if this type is nullable  
/// </summary>
//internal protected Dictionary<ushort, ITypeSerializer> TypeSerializersByShortID;

/// <summary>
/// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
/// </summary>
//internal protected Dictionary<Type, ushort> TypeSerializerShortIDByType;

#region ------------------------- DEFAULT  DOMAINMODEL SERIALIZER ------------------------------

///// <summary>
///// Default TSSerializer that can serialize/deserialize DomainModels.
///// </summary> 
//public static ITypeSetSerializer GetDomainModelSerializer()
//{

//    if (!SerializersByKey.ContainsKey(DomainModelSerializerKey))
//    {
//        lock (_lockerHub)
//        {
//            if (!SerializersByKey.ContainsKey(DomainModelSerializerKey))
//            {
//                var serializer = AddOrUseExistSerializer(DomainModelSerializerKey);

//                serializer.AddKnownType<DContract>();
//                serializer.AddKnownType<DContractGroup>();
//                serializer.AddKnownType<DModel>();
//                serializer.AddKnownType<Identification>();
//                serializer.AddKnownType<DNVersion>();
//                serializer.AddKnownType<List<DModel>>();
//                serializer.BuildTypeProcessor();
//            }
//        }
//    }

//    return GetDirect(DomainModelSerializerKey);
//}





#endregion ------------------------- DEFAULT  DOMAINMODELMODELS SERIALIZER ------------------------------


#region --------------------------------  LOAD TSS FROM DOMAINMODEL ------------------------------------


///// <summary>
///// Loading into TSS new TSSerializer instance by Creating new TSSerializer instance by DModel
///// </summary>
///// <param name="domainModel"></param>
//public static void LoadTSSFromDomainModel(DModel domainModel)
//{
//    Validator.AssertFalse<InvalidOperationException>(domainModel != null
//                                             , "AddSerializer(): domainModel can't be null"
//                                          );

//    lock (_lockerHub)
//    {
//        //1  determine  this side Contract by received AssemblyQualifiedName( with the Help of  MappingAdapter)  
//        foreach (var grp in domainModel.Groups)
//        {
//            foreach (var dContract in grp.Contracts)
//            {
//                dContract.Contract = TypeAQName.MappingAdapter.GetCustomType(dContract.AQName);  //DefaultMapping.G GetCusto  dContract.
//                Validator.AssertTrue<ContractNotFoundException>(dContract.Contract == null,
//                                                                " Contract[{0}] was not able to determine ", dContract.AQName
//                                                               );
//            }
//        }


//        //2 check if TSS with such key exist -then delete that instance.
//        //3 create new TSS by its IdentityKey.Key           
//        ITypeSetSerializer serializerByDomainModel = null;
//        if (domainModel.Identification.IdentityType == IdentityTypeEn.IdentityWitSvcKeyName)
//        {
//            var searchingSerializer = TSSV3.SerializersByKey.Where(k => k.Key == domainModel.Identification.SvcKeyName).FirstOrDefault();
//            if (searchingSerializer.Value != null) TSSV3.SerializersByKey.Remove(searchingSerializer.Key);

//            TSSV3.AddOrUseExistSerializer(domainModel.Identification.SvcKeyName);
//            serializerByDomainModel = TSSV3.GetDirect(domainModel.Identification.SvcKeyName);
//        }
//        else if (domainModel.Identification.IdentityType == IdentityTypeEn.IdentityWithSvcIndex)
//        {
//            var searchingSerializer = TSSV3.SerializersByIndex.Where(k => k.Key == domainModel.Identification.SvcIndex).FirstOrDefault();
//            if (searchingSerializer.Value != null) TSSV3.SerializersByIndex.Remove(searchingSerializer.Key);

//            TSSV3.AddOrUseExistSerializer(domainModel.Identification.SvcIndex);
//            serializerByDomainModel = TSSV3.GetDirect(domainModel.Identification.SvcIndex);
//        }
//        else if (domainModel.Identification.IdentityType == IdentityTypeEn.IdentityWithSvcID)
//        {
//            var searchingSerializer = TSSV3.SerializersByID.Where(k => k.Key == domainModel.Identification.SvcID).FirstOrDefault();
//            if (searchingSerializer.Value != null) TSSV3.SerializersByID.Remove(searchingSerializer.Key);

//            TSSV3.AddOrUseExistSerializer(domainModel.Identification.SvcID);
//            serializerByDomainModel = TSSV3.GetDirect(domainModel.Identification.SvcID);
//        }

//        Validator.AssertTrue<InvalidOperationException>(serializerByDomainModel == null,
//                                                              "  Can't create TSSserializer by DModel with IdentityKey[{0}]"
//                                                              , domainModel.Identification.GetSvcIdentity()
//                                                             );


//        //4 add all Contracts, 
//        foreach (var grp in domainModel.Groups)
//        {
//            foreach (var dContract in grp.Contracts)
//            {
//                if (Tools.IsAutoAddingContractCategory(dContract.Contract))
//                {
//                    serializerByDomainModel.AddKnownType(dContract.Contract);
//                }

//                else
//                {
//                    serializerByDomainModel.AddKnownType(dContract.Contract,
//                                                        CanTestDefaultConstructorOption: true,
//                                                        DefaultConstructorCleanerOption: typeof(IDisposable).IsAssignableFrom(dContract.Contract)
//                                                        );
//                }
//            }

//        }


//        //5 Build processing Info
//        serializerByDomainModel.BuildTypeProcessor();
//    }

//}


///// <summary>
///// Loading into TSS new TSSerializer instances by Creating new TSSerializer instances by each of DModel
///// </summary>
///// <param name="domainModels"></param> 
//public static void LoadTSSFromDomainModels(List<DModel> domainModels)
//{
//    Validator.AssertFalse<InvalidOperationException>(domainModels != null,
//             "AddTSSerialzers(): domainModels can't be null");


//    foreach (var dmModel in domainModels)
//    {
//        LoadTSSFromDomainModel(dmModel);
//    }

//}



///// <summary>
///// Loading into TSS new TSSerializer instances from  a few DomainModels from DomainModelsData by Creating new TSSerializer instances by each of DModel.
///// </summary>
///// <param name="DomainModelsData"></param>
//public static void LoadTSSFromDomainModels(byte[] DomainModelsData)
//{
//    //from List<DModel> into TSSInstances
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    var domainModels = domainModelSerializer.Deserialize<List<DModel>>(DomainModelsData);

//    LoadTSSFromDomainModels(domainModels);


//}



///// <summary>
///// Loading into TSS new TSSerializer instance from one DModel from sourceData and then Creating new TSSerializer instance by this DModel.
///// </summary>
///// <param name="sourceData"></param>
//public static void LoadTSSFromDomainModel(byte[] sourceData)
//{
//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    var domainModel = domainModelSerializer.Deserialize<DModel>(sourceData);

//    LoadTSSFromDomainModel(domainModel);



//}



///// <summary>
///// Loading into TSS new TSSerializer instance from one DModel from sourceData and then Creating new TSSerializer instance by this DModel.
///// </summary>
///// <param name="data"></param>
//public static void LoadTSSFromDomainModel(FileStream sourceData)
//{
//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    var domainModel = domainModelSerializer.Deserialize<DModel>(sourceData);

//    LoadTSSFromDomainModel(domainModel);

//}



///// <summary>
///// Loading into TSS new TSSerializer instance from one DModel from sourceData and then Creating new TSSerializer instance by this DModel.
///// </summary>
///// <param name="sourceData"></param>
//public static void LoadTSSFromDomainModel(MemoryStream sourceData)
//{
//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    var domainModel = domainModelSerializer.Deserialize<DModel>(sourceData);

//    LoadTSSFromDomainModel(domainModel);



//}




#if NET45

///// <summary>
///// Loading into TSS new TSSerializer instance from one DModel from File by sourceFileName and then Creating new TSSerializer instance by this DModel.
///// </summary>
///// <param name="sourceFile"></param>
//public static void LoadTSSFromDomainModel(String sourceFileName)
//{
//    Validator.AssertFalse<InvalidOperationException>(File.Exists(sourceFileName),
//                                            "LoadDomainModel(): file  [{0}] doesn't  exist",
//                                            sourceFileName);

//    using (var fs = new FileStream(sourceFileName, FileMode.Open))
//    {
//        LoadTSSFromDomainModel(fs);
//    }

//}

#endif



#endregion --------------------------------  LOAD TSS FROM DOMAINMODEL ------------------------------------


#region ------------------------------- SAVE DModel -----------------------------
//DModel - always will be rebuilded in BuildTypeProcessor () 


/// <summary>
/// Get - EXPORT  DModel from current Processing/dictionary
/// </summary>
/// <returns></returns>
//public DModel GetDomainModel()
//{
//    //collect DModel from current Runtime ProcessingById dictionary
//    var domainModel = new DModel();
//    domainModel.ModelId = Guid.NewGuid();
//    //domainModel.Identification = Identity;


//    //var collected SerializeGenerators = TSSV3.GetDomainModelSerializeGenerators();

//    foreach (var serGenrtr in collectedSerializeGenerators)
//    {
//        var dmGroup = domainModel.GetOrAddGroup(serGenrtr.GetType().Name);
//        dmGroup.SerializeGenerator = serGenrtr.GetType();
//        dmGroup.SerializeGeneratorFullName = serGenrtr.GetType().FullName;


//        var generatorTypeProcessors = this.ProcessingsByShortID.Values
//                                        .Where(tp => tp.TypeSerializeGenerator != null
//                                                   && tp.TypeSerializeGenerator.GetType() == serGenrtr.GetType()
//                                                   && (!typeof(Nullable<>).IsAssignableFrom(tp.Contract))
//                                                   )
//                                       .ToList();

//        foreach (var tpProcessor in generatorTypeProcessors)
//        {
//            dmGroup.AddKnownType(tpProcessor);
//        }
//    }

//    return domainModel;
//}






///// <summary>
///// Saving Domain Contract Model as byte[]  to the FileStream.
///// </summary>
///// <returns></returns>
//public void SaveDomainModel(FileStream saveStream)
//{
//    //collect DModel from current Runtime ProcessingById dictionary
//    var domainModel = GetDomainModel();

//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    domainModelSerializer.Serialize(saveStream, domainModel);

//}


///// <summary>
///// Saving Domain Contract Model as byte[]  to the File by FileWithPath  Stream
///// </summary>
///// <param name="FileName">full path with file name and its extension</param>
//public void SaveDomainModel(String FileName)
//{
//    var directory = Path.GetDirectoryName(FileName);// .GetDirectoryName
//    if (!Directory.Exists(directory)) //throw new DirectoryNotFoundException(directory);
//        Directory.CreateDirectory(directory);

//    if (File.Exists(FileName))
//    {
//        File.Delete(FileName);
//    }

//    using (var flStream = new FileStream(FileName, FileMode.CreateNew))
//    {
//        SaveDomainModel(flStream);
//    }
//}

/// <summary>
/// Saving Domain Contract Model as byte[]  to the MemoryStream
/// </summary>
/// <param name="saveStream"></param>
//public void SaveDomainModel(MemoryStream saveStream)
//{
//    //collect DModel from current Runtime ProcessingById dictionary
//    var domainModel = GetDomainModel();

//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    domainModelSerializer.Serialize(saveStream, domainModel);
//}

//public Task<byte[]> SaveDomainModelTPL()
//{
//    //collect DModel from current Runtime ProcessingById dictionary
//    var domainModel = GetDomainModel();

//    //serialize to filestream 
//    var domainModelSerializer = TSSV3.GetDomainModelSerializer();
//    return domainModelSerializer.SerializeTPL(domainModel);

//}


#endregion -------------------------------  SAVE DModel -----------------------------


//#region -------------------------- CTOR ------------------------

//static TSSV3()
//{
//    SerializersByID = new Dictionary<Guid, ITypeSetSerializer>();
//    SerializersByKey = new Dictionary<string, ITypeSetSerializer>();
//    SerializersByIndex = new Dictionary<int, ITypeSetSerializer>();

//    LoadSerializeGenerators();

//    InitPackageDiagnosticsExceptions();
//    Mapper = MappingAdapter.Create();

//}

//#endregion -------------------------- CTOR ------------------------


//static void InitPackageDiagnosticsExceptions()
//{

//    Validator.PrebuildException<ContractAddException>();
//    Validator.PrebuildException<ContractCtorTestException>();
//    Validator.PrebuildException<InvalidOperationException>();
//    Validator.PrebuildException<ContractSerializeException>();
//    Validator.PrebuildException<ContractMemberAnalyzeException>();
//    Validator.PrebuildException<ContractNotFoundException>();
//    Validator.PrebuildException<InvalidOperationException>();
//    Validator.PrebuildException<IOException>();


//}


///// <summary>
/////  AddContract overload:  determine one of two category of T-Contract{ [Auto Adding Category Contract] or [Custom Adding Category Contract]} and add T as it need.
///// [Custom Adding Category Contract] -  use ( CanTestDefaultConstructorOption: true, DefaultConstructorCleanerOption: false, DefalutPolicy and Fields /Properties Bindings) values.  
///// </summary>
///// <typeparam name="T"></typeparam>
///// <returns></returns>
//public ushort AddContract<T>(bool NeedToBeAnalyseAndBuild = true)
//{
//    if (typeof(T).IsAutoAdding())
//    {
//        return AddContract(typeof(T));
//    }
//    else
//        return AddContract<T>();
//}


//static ITypeSerializer CreateTypeSerializer(Type type, ushort iD)
//{
//    var tSerializerType = typeof(TypeSerializer<>).MakeGenericType(type);
//    return TypeActivator.CreateInstanceTBase<ITypeSerializer>(tSerializerType, iD);
//}





///// <summary>
/////  Custom way to insert additional Serialize Generator to the serializer.
///// </summary>
///// <typeparam name="TSerializerGenerator"></typeparam>
//public static void AddLoadSerializeGenerator<TSerializerGenerator>()
//    where TSerializerGenerator : ITypeSerializerGenerator
//{
//    lock (_locker)
//    {
//        var serializeGenerator = TypeActivator.CreateInstanceT<TSerializerGenerator>();
//        //if (serializeGenerator.CanLoadDefaultContractHandlers) - that's for TYPE SET SERiALIZER SEPARATE instance
//        //{
//        //    serializeGenerator.LoadDefaultContractHandlers(serializer);//load all Default Types from Generator                   
//        //}

//        LA_SerializeGenerators.Value.Add(serializeGenerator);

//    }
//}


#endregion --------------------------------- GARBAGE ----------------------------------

