﻿using System;
using System.IO;
using System.Linq.Expressions;
using DDDD.Core.Threading;
using DDDD.Core.Reflection;


namespace DDDD.Core.Serialization
{


    /// <summary>
    /// Type Serializer Generic interface 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ITypeSerializer<T> : ITypeSerializer
    {      
        
        /// <summary>
        ///Type Accessor{T} 
        /// </summary>
        ITypeAccessor<T> AccessorT { get; }       
        
        /// <summary>
        /// Lazy Func to create default value based on type kind: 1 -class; for enum; for struct; 2 - IDictionary, IList 3 - Array with [1-4] dimension
        /// <para/> When we get LA_CreateDefaultValue.Value first time - we choosing and precompiling fitted for T  DefaultValueFunc.
        /// </summary>
        LazyAct<Func<object[], T>> LA_CreateDefaultValue { get; }
        
        //LazySlim<Func<T>> LA_CreateDefaultValue { get; }

        
        
        LazyAct<Expression<SD.SerializeToStreamT<T>>> LA_Expression_SerializeToStreamTHandler { get;  }
        LazyAct<SD.SerializeToStreamT<T>> LA_SerializeToStreamTHandler { get; }



        LazyAct<Expression<SD.DeserializeFromStreamT<T>>> LA_Expression_DeserializeFromStreamTHandler { get; }
        LazyAct<SD.DeserializeFromStreamT<T>> LA_DeserializeFromStreamTHandler { get;  }



        T ReadObservableCollectionTypeID_CreateValue(Stream ms, out int collectionLength, bool readedTypeID = false); // observableCollections
        
        T ReadCollectionTypeID_CreateValue(Stream ms, out int collectionLength, bool readedTypeID = false); //Lists , dictionaries

        T ReadArrayTypeID_CreateValue(Stream ms, int knownrank, bool readedTypeID = false); // , params int[] dimensions

        T ReadTypeID_CreateValue(Stream ms, bool readedTypeID = false, params int[] dimensions);


        /// <summary>
        /// Write ushort typeID( of T) to Stream - For Nullable T- struct, or classes.
        /// <para/> If data == null  write  TypeID = 0.  If data != null write TypeID
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="data"></param>
        void Write_T_TypeID(Stream ms, T data);


        /// <summary>
        /// Raise Serialize To Stream {T} value Handler.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        void RaiseSerializeToStreamTHandler(Stream ms, T itemValue);


        /// <summary>
        /// Raise Deserialize From Stream, returns {T} Handler.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        T RaiseDeserializeFromStreamTHandler(Stream ms, bool readedTypeID = false);


        /// <summary>
        /// Set Serialize To Stream {T} Handlers
        /// </summary>
        /// <param name="serializeToStreamTHandler"></param>
        /// <param name="deserializeFromStreamBoxedHandler"></param>
        void SetSerializeToStreamTHandlers(SD.SerializeToStreamT<T> serializeToStreamTHandler, SD.DeserializeFromStreamT<T> deserializeFromStreamBoxedHandler);               
       
        
    }
}




#region ----------------------------------- GARBAGE -------------------------------------

//T GetMember<TMember>(string member, T instanceValue);
//void SetMember<TMember>(string memberKey, ref T instanceValue, TMember memberValue);
//TElement RaiseDeserializeFromStreamTHandler<TElement>(Stream ms, bool readedTypeID = false);
// void RaiseSerializeToStreamTHandler<TElement>(Stream ms, TElement itemValue);

//LazySlim<SD.SerializeToStreamT<T>>         SerializeToStreamTHandler   { get; }
//LazySlim<SD.DeserializeFromStreamT<T>>     DeserializeFromStreamTHandler    { get;  }
//LazySlim<SD.SerializeToBinWriterT<T>>      SerializeToBinWriterTHandler   { get;  }
//LazySlim<SD.DeserializeFromBinReaderT<T>>  DeserializeFromBinReaderTHandler   {  get;  }

#endregion ----------------------------------- GARBAGE -------------------------------------