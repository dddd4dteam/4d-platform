﻿
using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection.Emit;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;

namespace DDDD.Core.Serialization
{
    /// <summary>
    /// TypeSerializerT  RuntimeIDs of-  {T} Type[TypeID], and its sub types [{EnumUnderlyingTypeID, ArrayElementTypeID, Arg1TypeID, Arg2TypeID}]
    /// </summary>
    public class RuntimeIDs
    {
        /// <summary>
        /// Enum Underlying Type ID in current SetSerializer context
        /// </summary>
        public ushort EnumUnderlyingTypeID { get; internal set; }

        /// <summary>
        /// Array Element Type ID in current SetSerializer context
        /// </summary>
        public ushort ArrayElementTypeID { get; internal set; }

        /// <summary>
        /// List_Arg1_  or ObservableCollection_Arg1_ or  Dictionary_Arg1     Type ID in current SetSerializer context
        /// </summary>
        public ushort Arg1TypeID { get; internal set; }

        /// <summary>
        /// Dictionary_Arg2     Type ID in current SetSerializer context
        /// </summary>
        public ushort Arg2TypeID { get; internal set; }

    }
    


    /// <summary>
    ///  Custom Type serializer - i.e. the purpose of this concept is possibility to build and run [serialize/deserialize] handlers
    /// </summary>
    public interface ITypeSerializer
    {

        #region -------------------------------- TypeSetSerializer INTEGRATION ------------------------------

        /// <summary>
        /// TYPE SET Serializer
        /// </summary>
        ITypeSetSerializer SetSerializer { get; }

        void SetSerializeToStreamTHandlers(Delegate serializeToStreamTHandler, Delegate deserializeFromStreamTHandler);

        #endregion -------------------------------- TypeSetSerializer INTEGRATION ------------------------------


        /// <summary>
        /// Target Type is typeof(T) in ITypeSerializer{T} . Really is reference to [Accessor.TAccess] property path
        /// </summary>
        Type TargetType { get; }

        /// <summary>
        /// Initing Lazy Target Type's extended info.
        /// </summary>
        void InitLazyTargetTypeExtendedInfo();


        /// <summary>
        ///Lazy Target Type's extended info. 
        /// </summary>
        LazyAct<TypeInfoEx> LA_TargetTypeEx { get; }

        /// <summary>
        /// Type Accessor 
        /// </summary>
        ITypeAccessor Accessor    { get; }

        /// <summary>
        /// Type Serializer Generator - generate  De/Serialize Func for current TargetType. 
        /// </summary>
        ITypeSerializerGenerator TypeSerializeGenerator { get; }

        #region ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------


        /// <summary>
        /// Type ID  - serialization ID in TYPE SET Dictionary
        /// </summary>
        ushort TypeID                    { get; }


        /// <summary>
        /// Get current data item type's ID from current TypeSetSerializer 
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        ushort GetElementTypeID(object element);


        /// <summary>
        /// If Element with such elementID  is 4DPrimitive or Enum Type
        /// </summary>
        /// <param name="elementID"></param>
        /// <returns></returns>
        bool IsElement_EnumOrPrimitive(ushort elementID);


        #endregion ------------------------------ TYPE & Sub-Types IDs  in current SetSerializer context------------------------------------



        #region ----------------------------RUNTIME - Sub-Types IDs  in current SetSerializer context ----------------------------------------

        /// <summary>
        /// Runtime TargetType's Sub-Type IDs. 
        /// </summary>
        LazyAct<RuntimeIDs> LA_RuntimeIDs { get; }


        /// <summary>
        /// Initing TypeSerializer sub Types IDs -  EnumUnderlyingTypeID, ArrayElementTypeID ,  Arg1TypeID, Arg2TypeID - we getting IDs from current TypeSetserializer IDs Dictionary.
        ///  on the Serialize moment only / not on AddKnownType moment.
        /// </summary>
        void InitLazyRuntimeIDs();

        #endregion ----------------------------RUNTIME - Sub-Types IDs  in current SetSerializer context ----------------------------------------


        /// <summary>
        /// Write  [0] if [null] to Stream. Returns: TRUE VALUE (IS NULL)  -  then we logically   return/ or continue.
        /// <para/> If realTypeID is not  [null] but 4DPrimitive or Enum type we also write this realTypeID to Stream. Returns: FALSE VALUE (IS NOT NULL).
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="realTypeID"></param>
        /// <returns></returns>
        bool WriteRealTypeID_4Null_4PrimitiveOrEnum(Stream stream, ushort realTypeID);

        /// <summary>
        /// Write ushort typeID( of T) to Stream - For Nullable T- struct, or classes.
        /// <para/> If data == null  write  TypeID = 0( null value const);  If data != null write typeID
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="data"></param>
        /// <param name="typeID"></param>
        void Write_X_TypeID(Stream ms, object data, ushort typeID);


        /// <summary>
        /// Dictionary Key can't be null  even if TKey is interface or class Type - so it's simple writing correct typeID into Stream
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="typeID"></param>
        void Write_DictKeyID(Stream ms, ushort typeID);
        

        /// customization options for TAccessor members: IgnoreMembers , OnlyMembers, Selector - when they will be changed then TAccessor will be rebuilded
        #region --------------------------------- CUSTOMIZATION of ACCESSOR MEMBERS ----------------------------------------------

        /// <summary>
        /// Ignore Members- means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        List<string> IgnoreMembers { get; }

        /// <summary>
        /// Only Members - means that all Type.Members will be gathered by Selector Binding option, except Ignored Members in current TypeSerializer.
        /// <para/> We can use it when Selector has TypeMemberSelectorEn.Custom  value.
        /// </summary>
        List<string> OnlyMembers { get; }


        /// <summary>
        /// Selector defines strategy of Type.Members collecting. It's predefined BindingFlags combinations and other custom strategy options -like IgnoreMembers, OnlyMembers and Custom BindingFlags. 
        /// </summary>
        TypeMemberSelectorEn Selector { get; }

        #endregion --------------------------------- CUSTOMIZATION of ACCESSOR MEMBERS ----------------------------------------------








        //////////////////
        ////    ----- OWN SERIALIZATION - ONLY BOXED Handlers( others in ITypeSerializer{T})
        //////////////////

        /// <summary>
        /// Type's real boxed serializing  handler.
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        void RaiseSerializeToStreamBoxedHandler(Stream ms, object itemValue);

        /// <summary>        
        /// T Type's real deserializing  handler, then return T as object
        /// </summary>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        object RaiseDeserializeFromStreamBoxedHandler(Stream ms, bool readedTypeID = false);

      

        //////////////////
        ////    ----- ELEMENT SERIALIZATION - ONLY BOXED Handlers( others in ITypeSerializer{T})
        //////////////////

        /// <summary>
        /// Type's element serializing - transition to the TypeSerializer of TElement and call it's boxed serialize handler.
        /// </summary>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        void RaiseElementSerializeToStreamBoxedHandler(ushort elemTypeID,Stream ms, object itemValue);

        /// <summary>
        /// Type's element deserializing - transition to the TypeSerializer of TElement and call it's boxed deserialize handler.
        /// </summary>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        object RaiseElementDeserializeFromStreamBoxedHandler(ushort elemTypeID, Stream ms, bool readedTypeID = false);

        /// <summary>
        /// Type's element serializing - transition to the TypeSerializer of TElement and call it's {T} serialize handler.
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="itemValue"></param>
        void RaiseElementSerializeToStreamTHandler<TElement>(ushort elemTypeID, Stream ms, TElement itemValue);


        /// <summary>
        ///  Type's element deserializing - transition to the TypeSerializer of TElement and call it's {T} deserialize handler. 
        /// </summary>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="elemTypeID"></param>
        /// <param name="ms"></param>
        /// <param name="readedTypeID"></param>
        /// <returns></returns>
        TElement RaiseElementDeserializeFromStreamTHandler<TElement>(ushort elemTypeID, Stream ms, bool readedTypeID = false);

        
    


#if NET45 || NET46

        void BuildDynamicTypeAccessor(TypeBuilder tpBiulder);      

#endif



    }


}


#region ----------------------------------- GARBAGE -----------------------------

//object CreateDefaultValue(int? collectionLengt = 0);
//void SetSerializeToStreamHandlers<T>(SD.SerializeToStreamT<T> serializeToStreamTHandler, SD.DeserializeFromStream deserializeFromStreamBoxedHandler);       
//void SetStreamBoxedHandlers(Delegate serializeToStreamBoxedHandler, Delegate deserializeFromStreamBoxedHandler);

///// <summary>
///// Set Serializer expressions generator - we are talking about SERIALIZE/DESERIALIZE expressions for target Accessor.TAccess
///// </summary>
///// <param name="generator"></param>
//void SetSerializerGenerator(ITypeSerializerGenerator generator);

//TypeSerializerStateEn State { get; }
//ITypeSerializerGenerator SerializerGenerator { get; }

#endregion ----------------------------------- GARBAGE -----------------------------

