﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

namespace DDDD.Core.Serialization
{


    /// <summary>
    /// Serialize Generator for Primitive types. All of them are [DefaultContract]. The Primitives are the following types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// </summary>
    internal class SerializeGenerator_Primitives : ITypeSerializerGenerator
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        ///   Can Handle  criterion for SerializeGenerator_Primitives:           
        ///    Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
        ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
        ///    Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
        ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
        ///    Classes:               string, byte[], Uri, BitArray
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return type.Is4DPrimitiveType();
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        public Expression<SD.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializer<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        public Expression<SD.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializer<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }
       

      

        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SD.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SD.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// <para/> So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        ///  CanLoadDefaultContractHandlers should be true. 
        /// <para/> If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [4DPrimitive ] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;        
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {   return TypeInfoEx.PrimitiveTypes; //BinaryReaderWriter.GetRegisteredPrimitives();
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers(ITypeSetSerializer tpSetSerializer)
        {
           
            ////NOT NULLABLE PrimitiveReaderWriter    
            tpSetSerializer.AddKnownType<int>().SetSerializeToStreamTHandlers( BinaryReaderWriter.WriteT_int32, BinaryReaderWriter.ReadT_int32);
            tpSetSerializer.AddKnownType<uint>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint32, BinaryReaderWriter.ReadT_uint32);
            tpSetSerializer.AddKnownType<long>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_long, BinaryReaderWriter.ReadT_int64);
            tpSetSerializer.AddKnownType<ulong>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_ulong, BinaryReaderWriter.ReadT_uint64);
            tpSetSerializer.AddKnownType<short>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int16, BinaryReaderWriter.ReadT_int16);
            tpSetSerializer.AddKnownType<ushort>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint16, BinaryReaderWriter.ReadT_uint16);
            tpSetSerializer.AddKnownType<byte>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte, BinaryReaderWriter.ReadT_byte);
            tpSetSerializer.AddKnownType<sbyte>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_sbyte, BinaryReaderWriter.ReadT_sbyte);
            tpSetSerializer.AddKnownType<char>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_char, BinaryReaderWriter.ReadT_char);
            tpSetSerializer.AddKnownType<bool>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_bool, BinaryReaderWriter.ReadT_bool);
            tpSetSerializer.AddKnownType<DateTime>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_DateTime, BinaryReaderWriter.ReadT_DateTime);
            tpSetSerializer.AddKnownType<TimeSpan>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TimeSpan, BinaryReaderWriter.ReadT_TimeSpan);
            tpSetSerializer.AddKnownType<Guid>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Guid, BinaryReaderWriter.ReadT_Guid);
            tpSetSerializer.AddKnownType<float>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_float, BinaryReaderWriter.ReadT_float);
            tpSetSerializer.AddKnownType<double>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_double, BinaryReaderWriter.ReadT_double);
            tpSetSerializer.AddKnownType<decimal>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_decimal, BinaryReaderWriter.ReadT_decimal);

           

            tpSetSerializer.AddKnownType<int?>().SetSerializeToStreamTHandlers(  BinaryReaderWriter.WriteT_int32_nullable, BinaryReaderWriter.ReadT_int32_nullable);
            tpSetSerializer.AddKnownType<uint?>().SetSerializeToStreamTHandlers( BinaryReaderWriter.WriteT_uint32_nullable, BinaryReaderWriter.ReadT_uint32_nullable);
            tpSetSerializer.AddKnownType<long?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int64_nullable, BinaryReaderWriter.ReadT_int64_nullable);
            tpSetSerializer.AddKnownType<ulong?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint64_nullable, BinaryReaderWriter.ReadT_uint64_nullable);
            tpSetSerializer.AddKnownType<short?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int16_nullable, BinaryReaderWriter.ReadT_int16_nullable);
            tpSetSerializer.AddKnownType<ushort?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint16_nullable, BinaryReaderWriter.ReadT_uint16_nullable);
            tpSetSerializer.AddKnownType<byte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte_nullable, BinaryReaderWriter.ReadT_byte_nullable);
            tpSetSerializer.AddKnownType<byte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte_nullable, BinaryReaderWriter.ReadT_byte_nullable);
            tpSetSerializer.AddKnownType<sbyte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_sbyte_nullable, BinaryReaderWriter.ReadT_sbyte_nullable);
            tpSetSerializer.AddKnownType<char?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_char_nullable, BinaryReaderWriter.ReadT_char_nullable);
            tpSetSerializer.AddKnownType<bool?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_bool_nullable, BinaryReaderWriter.ReadT_bool_nullable);
            tpSetSerializer.AddKnownType<DateTime?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_DateTime_nullable, BinaryReaderWriter.ReadT_DateTime_nullable);
            tpSetSerializer.AddKnownType<TimeSpan?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TimeSpan_nullable, BinaryReaderWriter.ReadT_TimeSpan_nullable);
            tpSetSerializer.AddKnownType<Guid?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Guid_nullable, BinaryReaderWriter.ReadT_Guid_nullable);
            tpSetSerializer.AddKnownType<float?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_float_nullable, BinaryReaderWriter.ReadT_float_nullable);
            tpSetSerializer.AddKnownType<double?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_double_nullable, BinaryReaderWriter.ReadT_double_nullable);
            tpSetSerializer.AddKnownType<decimal?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_decimal_nullable, BinaryReaderWriter.ReadT_decimal_nullable);


            
            ////primitive classes : string , byte[]
            tpSetSerializer.AddKnownType<string>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_string, BinaryReaderWriter.ReadT_string);
            tpSetSerializer.AddKnownType<byte[]>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byteArray, BinaryReaderWriter.ReadT_byteArray);
            tpSetSerializer.AddKnownType<Uri>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Uri, BinaryReaderWriter.ReadT_Uri);
            tpSetSerializer.AddKnownType<BitArray>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_BitArray, BinaryReaderWriter.ReadT_BitArray);

            // 4 Reflection  Classes - TypeInfo
            tpSetSerializer.AddKnownType<TypeInfoEx>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TypeInfoEx, BinaryReaderWriter.ReadT_TypeInfoEx);

        }



        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

    }

}




#region ------------------------------------ GARBAGE ---------------------------------


////NOT NULLABLE PrimitiveReaderWriter    
//serializer.AddKnownType<int>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int32);
//serializer.AddKnownType<uint>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_uint32);
//serializer.AddKnownType<long>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int64);
//serializer.AddKnownType<ulong>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_uint64);
//serializer.AddKnownType<short>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int16);
//serializer.AddKnownType<ushort>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_uint16);
//serializer.AddKnownType<byte>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_byte);
//serializer.AddKnownType<sbyte>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_sbyte);
//serializer.AddKnownType<char>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_char);
//serializer.AddKnownType<bool>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_bool);
//serializer.AddKnownType<DateTime>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_DateTime);
//serializer.AddKnownType<TimeSpan>(BinaryReaderWriter.Write, BinaryReaderWriter.Read_TimeSpan);
//serializer.AddKnownType<Guid>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_Guid);
//serializer.AddKnownType<float>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_float);
//serializer.AddKnownType<double>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_double);
//serializer.AddKnownType<decimal>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_decimal);

////NULLABLE PrimitiveReaderWriter                
//serializer.AddKnownType<int?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int32_nullable);
//serializer.AddKnownType<uint?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_uint32_nullable);
//serializer.AddKnownType<long?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int64_nullable);
//serializer.AddKnownType<ulong?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_uint64_nullable);
//serializer.AddKnownType<short?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_int16_nullable);
//serializer.AddKnownType<ushort?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_ushort_nullable);
//serializer.AddKnownType<byte?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_byte_nullable);
//serializer.AddKnownType<sbyte?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_sbyte_nullable);
//serializer.AddKnownType<char?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_char_nullable);
//serializer.AddKnownType<bool?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_bool_nullable);
//serializer.AddKnownType<DateTime?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_DateTime_nullable);
//serializer.AddKnownType<TimeSpan?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_TimeSpan_nullable);
//serializer.AddKnownType<Guid?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_Guid_nullable);
//serializer.AddKnownType<float?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_float_nullable);
//serializer.AddKnownType<double?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_double_nullable);
//serializer.AddKnownType<decimal?>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_decimal_nullable);


////primitive classes : string , byte[]
//serializer.AddKnownType<string>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_string);
//serializer.AddKnownType<byte[]>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_byteArray);
//serializer.AddKnownType<Uri>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_Uri);
//serializer.AddKnownType<BitArray>( BinaryReaderWriter.Write, BinaryReaderWriter.Read_BitArray);


#endregion ------------------------------------ GARBAGE ---------------------------------