﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Reflection;
using DDDD.Core.Extensions;

namespace DDDD.Core.Serialization
{
    /// <summary>
    /// Most commonly used TypeSerializeGenerator that generate Serialize/Deserialize expressions for:
    ///     - non generic custom Class or Struct;
    ///     - Nullable struct TypeSerializer will be added automatically on adding original Struct. So you shouldn't add it manually;
    ///     - types that  are not contained in [Default Contract] List of SerializeGenerator_Primitives generator;
    ///     - types  wich category are not Auto Adding Contract category.     
    /// </summary>
    internal class SerializeGenerator_ComplexType : ITypeSerializerGenerator
    {


        #region ------------------------------------- CAN HANDLE --------------------------------------
        
        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_ComplexStructure:
        ///     - non generic custom Class or Struct;
        ///     - Nullable struct TypeSerializer will be added automatically on adding original Struct. So you shouldn't add it manually;
        ///     - types that are not contained in [Default Contract] List of SerializeGenerator_Primitives generator;
        ///     - types  wich category are not Auto Adding Contract category.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            if (type.IsNullable() && type.IsValueType) return true;

            return (
                 (type.IsClass || type.IsValueType)  // abstract also can be added
                 && !type.Is4DPrimitiveType()        // 
                 && !type.IsGenericType //  custom generic + not collection based contract isn't operable  Nullable<>
                // && ! type.IsAutoAdding() 
                 && ! type.IsImplementInterfaceByTypeNameOnly(  typeof(IEnumerable) ) // other enumerable that Auto Adding COntract doesn't support also can't be handled               
               );

        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region ------------------------------------- GENERATE  SERIALIZE-  WRITE  EXPRESSIONS  ---------------------------



        /// <summary>
        /// Creating Expression with Write TypeID  of some T.  Per T instance operation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="typeSerializerInstance"></param>
        /// <param name="parameterStream"></param>
        /// <param name="parameterInputData"></param>
        /// <returns></returns>
        static Expression Generate_Write_T_TypeID<T>(ConstantExpression typeSerializerInstance, ParameterExpression parameterStream, ParameterExpression parameterInputData)
        {
            //1. Write typeID - Write_T_TypeID(Stream ms)
            var methodWrite_T_TypeID = typeof(TypeSerializer<T>).GetMethod(nameof(TypeSerializer<T>.Write_T_TypeID), new[] { typeof(Stream), typeof(T) });
            if (parameterInputData == null)
            {
                return Expression.Call(typeSerializerInstance,
                                    methodWrite_T_TypeID,
                                    parameterStream,
                                    Expression.Constant(0)
                                    );
            }
            else
            {
                return Expression.Call(typeSerializerInstance,
                                    methodWrite_T_TypeID,
                                    parameterStream,
                                    parameterInputData);
            }



        }


        static Expression Generate_Write_T_TypeID<T>(ParameterExpression typeSerializerInstanceParam, ParameterExpression parameterStream, ParameterExpression parameterInputData)
        {
            //1. Write typeID - Write_T_TypeID(Stream ms)
            var methodWrite_T_TypeID = typeof(TypeSerializer<T>).GetMethod(nameof(TypeSerializer<T>.Write_T_TypeID), new[] { typeof(Stream), typeof(T) });
            if (parameterInputData == null)
            {
                return Expression.Call(typeSerializerInstanceParam,
                                    methodWrite_T_TypeID,
                                    parameterStream
                                    );
            }
            else
            {
                return Expression.Call(typeSerializerInstanceParam,
                          methodWrite_T_TypeID,
                          parameterStream,
                          parameterInputData);
            }



        }






        /// <summary>
        /// Selecting   WriteMemberToStream_NeedToSureInDataType  or WriteMemberToStream_NeedNotToSureInDataType  method  to create WriteMember Code inside  Expression  . Per Member operation.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <param name="memberItem"></param>
        /// <param name="parameterStream"></param>
        /// <param name="parameterInputData"></param>
        /// <param name="typeSerializerInstanceParam"></param>
        /// <returns></returns>
        static Expression Generate_WriteMemberCode<T>(ITypeSerializer<T> tpSerializer, MemberInfo memberItem
                                                      , ParameterExpression parameterStream, ParameterExpression parameterInputData, ParameterExpression typeSerializerInstanceParam)
        {

            var memberType = memberItem.GetMemberType();
            var parameter_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(memberType)); //can throw if type won't find out
            var parameter_MemberName = Expression.Constant(memberItem.Name); //


            MethodInfo methodWriteMemberToStream = null;
            if (memberType.IsNeedToSureInDataType())
            {
                methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
                                                     .MakeGenericMethod(memberType);//FieldType
            }
            else
            {
                methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
                                                     .MakeGenericMethod(memberType);//FieldType
            }


            return Expression.Call(typeSerializerInstanceParam,
                                                     methodWriteMemberToStream,
                                                     parameter_memberTypeID,
                                                     parameter_MemberName,
                                                     parameterStream,
                                                     parameterInputData
                                 );

        }



        static Expression Generate_WriteMemberCode<T>(ITypeSerializer<T> tpSerializer, MemberInfo memberItem
                                                     , ParameterExpression parameterStream, ParameterExpression parameterInputData, ConstantExpression typeSerializerInstance)
        {
            var memberType = memberItem.GetMemberType();
            var parameter_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(memberType));   //can throw if type won't find out// MemberType
            var parameter_MemberName = Expression.Constant(memberItem.Name);// Name of field

            MethodInfo methodWriteMemberToStream = null;
            if (memberType.IsNeedToSureInDataType())
            {
                methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
                                                     .MakeGenericMethod(memberType);//FieldType
            }
            else
            {
                methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
                                                     .MakeGenericMethod(memberType);//FieldType
            }


            return Expression.Call(typeSerializerInstance,
                                       methodWriteMemberToStream,
                                       parameter_memberTypeID,
                                       parameter_MemberName,
                                       parameterStream,
                                       parameterInputData
                                  );

        }




        /// <summary>
        ///  Expressions with All T.Members Write Code
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        static List<Expression> Generate_WriteMembersCode<T>(ITypeSerializer<T> tpSerializer,
                                ParameterExpression parameterStream, ParameterExpression parameterInputData, ConstantExpression typeSerializerInstance)
        {
            List<Expression> operators_Serialize_T = new List<Expression>();

            foreach (var membrItem in tpSerializer.AccessorT.Members)
            {
                operators_Serialize_T.Add(Generate_WriteMemberCode(tpSerializer, membrItem.Value, parameterStream, parameterInputData, typeSerializerInstance));
            }

            return operators_Serialize_T;
        }


        static List<Expression> Generate_WriteMembersCode<T>(ITypeSerializer<T> tpSerializer,
                               ParameterExpression parameterStream, ParameterExpression parameterInputData, ParameterExpression typeSerializerInstance)
        {
            List<Expression> operators_Serialize_T = new List<Expression>();

            foreach (var membrItem in tpSerializer.AccessorT.Members)
            {
                operators_Serialize_T.Add(Generate_WriteMemberCode(tpSerializer, membrItem.Value, parameterStream, parameterInputData, typeSerializerInstance));
            }

            return operators_Serialize_T;
        }


        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializer<T> tpSerializer)
        {
            try
            {
                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && classes  - with  If(instance != null){}

                // write T typeID - void Write_T_TypeID(Stream ms)-for 1 image: Not nullable struct |  
                //                  void Write_T_TypeID(Stream ms, T data) - for 2 image: nullable structs && classes
                // write members - void TypeSerializer<T>.void WriteMemberToStream<TMember>(ushort typeID, string member, Stream ms, T instanceValue)


                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var typeSerializerInstance = Expression.Constant(tpSerializer);

                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                if (tpSerializer.TargetType.IsValueType && !tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType) //&& !tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType
                {
                    List<Expression> operators_Serialize_T = new List<Expression>();

                    //1. Write typeID - Write_T_TypeID(Stream ms)
                    operators_Serialize_T.Add(Generate_Write_T_TypeID<T>(typeSerializerInstance, parameterStream, parameterInputData));

                    //2. write members:
                    operators_Serialize_T.AddRange(Generate_WriteMembersCode(tpSerializer, parameterStream, parameterInputData, typeSerializerInstance));


                    return Expression.Lambda<SD.SerializeToStreamT<T>>(
                                                    Expression.Block(operators_Serialize_T)
                                                    , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Serialize_Write"
                                                    , new[] { parameterStream, parameterInputData }
                                                    );

                }
                //// 2 image -  nullable structs - not needed
                //if (tpSerializer.TargetType.IsValueType && tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType)
                //{
                //}

                //// 3 image
                else  //// 3 image: for nullable structs && classes  - with  If(instance != null){}
                {
                    List<Expression> operators_InstanceSerialize_T = new List<Expression>();
                    List<Expression> operators_MembersSerialize_T = new List<Expression>();

                    //1. Write typeID - Write_T_TypeID(Stream ms)                    
                    operators_InstanceSerialize_T.Add(Generate_Write_T_TypeID<T>(typeSerializerInstance, parameterStream, parameterInputData));

                    //2. write members:
                    operators_MembersSerialize_T.AddRange(Generate_WriteMembersCode(tpSerializer, parameterStream, parameterInputData, typeSerializerInstance));

                    //  only when members was found
                    if (operators_MembersSerialize_T.Count > 0)
                    {
                        ////3. if(instanceVal == null) return instanceVal; but if not equal null serialize members              
                        operators_InstanceSerialize_T.Add(Expression.IfThen(
                                                                Expression.NotEqual(parameterInputData, Expression.Constant(null)),
                                                                Expression.Block(operators_MembersSerialize_T)
                                                                           )
                                                          );
                    }

                    return Expression.Lambda<SD.SerializeToStreamT<T>>
                                                    (Expression.Block(operators_InstanceSerialize_T)
                                                       , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Serialize_Write"
                                                       , new[] { parameterStream, parameterInputData }
                                                    );

                }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ComplexType)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  " , exc);
            }
        }



        #endregion ------------------------------------- GENERATE  SERIALIZE-  WRITE  EXPRESSIONS  ---------------------------


        #region ------------------------------------- GENERATE  DESERIALIZE-  READ  EXPRESSIONS  ---------------------------


        static Expression Generate_AssignLocalVariableByReadTypeIDCreateValue(  ParameterExpression variable_InstanceVal
                                                                              , ConstantExpression param_typeProcessorInstance
                                                                              ,  MethodInfo method_ReadTypeID_CreateValue
                                                                              , ParameterExpression parameter_Stream
                                                                              , ParameterExpression parameter_ReadedTypeID
                                                                            )               
        {
            return Expression.Assign(variable_InstanceVal,
                                            Expression.Call(
                                            param_typeProcessorInstance,
                                            method_ReadTypeID_CreateValue,
                                            parameter_Stream,
                                            parameter_ReadedTypeID,
                                            Expression.Constant(new int[] { })//length to create T  0
                                            )
                                     );
        }

        static Expression Generate_AssignLocalVariableByReadTypeIDCreateValue(ParameterExpression variable_InstanceVal
                                                                            , ParameterExpression param_typeProcessorInstance
                                                                            , MethodInfo method_ReadTypeID_CreateValue
                                                                            , ParameterExpression parameter_Stream
                                                                            , ParameterExpression parameter_ReadedTypeID
                                                                          )
        {
            return Expression.Assign(variable_InstanceVal,
                                            Expression.Call(
                                            param_typeProcessorInstance,
                                            method_ReadTypeID_CreateValue,
                                            parameter_Stream,
                                            parameter_ReadedTypeID,
                                            Expression.Constant(new int[] { })//length to create T  0
                                            )
                                     );
        }




        static Expression Generate_ReadMemberCode<T>(ITypeSerializer<T> tpSerializer
                                                     , MemberInfo memberItem
                                                     , ParameterExpression variable_InstanceVal
                                                     , ConstantExpression param_typeProcessorInstance
                                                     , ParameterExpression parameterStream                                                     
                                                     )
        {
           
            //2.  ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, ref T instanceValue, bool readedTypeID = false)
            var memberType = memberItem.GetMemberType();

            MethodInfo method_ReadMemberFromStream = null;
            if (memberType.IsNeedToSureInDataType())
            {
                method_ReadMemberFromStream = tpSerializer.GetType()
                                                          .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream_NeedToRereadDataType)) //  "ReadMemberFromStream"
                                                          .MakeGenericMethod(memberType);
            }
            else
            {
                method_ReadMemberFromStream = tpSerializer.GetType()
                                                          .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream_NeedNotToRereadDataType)) //  "ReadMemberFromStream"
                                                          .MakeGenericMethod(memberType);
            }
            

            // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
            var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(memberType));

            return Expression.Call(    param_typeProcessorInstance  ,
                                        method_ReadMemberFromStream  ,
                                        variable_memberTypeID ,
                                        Expression.Constant( memberItem.Name ), //Key == Name
                                        parameterStream ,
                                        variable_InstanceVal ,
                                        Expression.Constant(false) //   constant_memberReadedTypeID     //readedTypeID                                                                       
                                    );

        }


        static Expression Generate_ReadMemberCode<T>(ITypeSerializer<T> tpSerializer
                                                     , MemberInfo memberItem
                                                     , ParameterExpression variable_InstanceVal
                                                     , ParameterExpression param_typeProcessorInstance
                                                     , ParameterExpression parameterStream
                                                     
                                                     )
        {            
            //2.  ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, ref T instanceValue, bool readedTypeID = false)
            var memberType = memberItem.GetMemberType();

            MethodInfo method_ReadMemberFromStream = null;
            if (memberType.IsNeedToSureInDataType())
            {
                method_ReadMemberFromStream = tpSerializer.GetType()
                                                          .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream_NeedToRereadDataType)) //  "ReadMemberFromStream"
                                                          .MakeGenericMethod(memberType);
            }
            else
            {
                method_ReadMemberFromStream = tpSerializer.GetType()
                                                          .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream_NeedNotToRereadDataType)) //  "ReadMemberFromStream"
                                                          .MakeGenericMethod(memberType);
            }

            // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
            var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(memberType));

            return Expression.Call(param_typeProcessorInstance,
                                        method_ReadMemberFromStream,
                                        variable_memberTypeID,
                                        Expression.Constant(memberItem.Name), //Key == Name
                                        parameterStream,
                                        variable_InstanceVal,
                                        Expression.Constant(false) //   constant_memberReadedTypeID     //readedTypeID                                                                       
                                    );
        }




        static void Generate_Switch_IfTypeContainsMembersToRead(  List<Expression> operators_InstanceDeserialize
                                                                , List<Expression> operators_MembersDeserialize
                                                                , ParameterExpression variable_InstanceVal
                                                               )
        {
            if (operators_MembersDeserialize.Count > 0)
            {
                ////3. if(instanceVal == null) return instanceVal;               
                operators_InstanceDeserialize.Add(Expression.IfThen(
                                                Expression.NotEqual(variable_InstanceVal, Expression.Constant(null)),
                                                Expression.Block(operators_MembersDeserialize)
                                                                   )
                                                 );

            }
        }


        /// <summary>
        /// Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.DeserializeFromStreamT<T>> GenerateDeserialize_ReadExpression<T>(ITypeSerializer<T> tpSerializer)
        {
            try
            {
                //Deserialize Expression signature:   object Deserialize(Stream stream, bool readedTypeID = false);

                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && /classes  - with  If(not null value){}
                //
                // 3 image: for classes 

                // ReadTypeID   and CreateValue if typeID not 0 - not null 
                // if(typeID == 0 && class/ struct?)  -  return Null
                //Read fields      -- if we wont find GetTypeID it will throw exception - so the member typeID is strong
                //Read Properties  -- if we wont find GetTypeID it will throw exception - so the member typeID is strong

                // T instanceVal = T ReadTypeID_CreateValue(Stream ms,bool readedTypeID = false ) //, Int32 length = 0 
                // if(instanceVal != null)
                // foreach(field; property) {void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, Int32 length = 0, bool readedTypeID = false) }
                // return instanceVal;

                var param_Stream = Expression.Parameter(typeof(Stream), "targetStream");
                var param_ReadedTypeID = Expression.Parameter(typeof(bool), "readedTypeID");
                var param_typeProcessorInstance = Expression.Constant(tpSerializer);// r(typeof(TypeSerializer<T>), "typeprocessorInstance");

                //1.  T instanceVal = T ReadTypeID_CreateValue(Stream ms, bool readedTypeID = false, Int32 length = 0,  ) 
                var variable_InstanceVal = Expression.Variable(tpSerializer.TargetType, "instanceVal");
                var method_ReadTypeID_CreateValue = typeof(TypeSerializer<T>)
                                                    .GetMethod(nameof(TypeSerializer<T>.ReadTypeID_CreateValue));
                //var constant_Length = Expression.Constant(0);// typeId -it will be getted from dictionary as instance dictionary  runtime builded const
                var constant_memberReadedTypeID = Expression.Constant(false);


                // 1 image: for Not Nullable/NotNullable  structs - valueTypes    - without  If(value != null){} 
                if (tpSerializer.TargetType.IsValueType && !tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType)//
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();
                    // 1 
                    operators_InstanceDeserialize.Add( Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance , method_ReadTypeID_CreateValue, param_Stream , param_ReadedTypeID) );
                    // 2 
                    variable_InstanceVal.Type.MakeByRefType();


                    // 3
                    ////for each field 
                    foreach (var member in tpSerializer.AccessorT.Members)
                    {
                        operators_InstanceDeserialize.Add( Generate_ReadMemberCode(tpSerializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream)  );
                    }
                                      
                    // 
                    operators_InstanceDeserialize.Add(Expression.Convert(variable_InstanceVal, tpSerializer.TargetType));

                    return Expression.Lambda<SD.DeserializeFromStreamT<T>>(
                                                     Expression.Block(tpSerializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                       , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                       , new[] { param_Stream, param_ReadedTypeID }

                                                     );

                }
                ////////// 2 image  structs -  Nullable
                else if (tpSerializer.TargetType.IsValueType && tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType)  //2 image:   nullable structs but not classes
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();
                    List<Expression> operators_MembersDeserialize = new List<Expression>();

                    operators_InstanceDeserialize.Add(Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance, method_ReadTypeID_CreateValue, param_Stream, param_ReadedTypeID));

                    variable_InstanceVal.Type.MakeByRefType();

                    //Variable.Value property --?

                    ////for each members
                    foreach (var member in tpSerializer.AccessorT.Members)
                    {
                        operators_MembersDeserialize.Add(Generate_ReadMemberCode(tpSerializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream));
                    }

                    //  only when members was found
                    Generate_Switch_IfTypeContainsMembersToRead(operators_InstanceDeserialize, operators_MembersDeserialize, variable_InstanceVal);

                    //return 
                    operators_InstanceDeserialize.Add(Expression.Convert(variable_InstanceVal, tpSerializer.TargetType));

                    return Expression.Lambda<SD.DeserializeFromStreamT<T>>(
                                                     Expression.Block(tpSerializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                     , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                     , new[] { param_Stream, param_ReadedTypeID }
                                                     );

                }

                ///// 3 image:   classes
                else //////  3 image:   classes 
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();
                    List<Expression> operators_MembersDeserialize = new List<Expression>();

                    operators_InstanceDeserialize.Add(Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance, method_ReadTypeID_CreateValue, param_Stream, param_ReadedTypeID));
                                    
                    //variable_InstanceVal.Type;//.MakeByRefType() - not need for classes
                                        
                    ////for each members
                    foreach (var member in tpSerializer.AccessorT.Members)
                    {
                        operators_MembersDeserialize.Add(Generate_ReadMemberCode(tpSerializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream));
                    }

                    //  only when members was found
                    Generate_Switch_IfTypeContainsMembersToRead(operators_InstanceDeserialize, operators_MembersDeserialize, variable_InstanceVal);

                    //return 
                    operators_InstanceDeserialize.Add(  Expression.Convert(variable_InstanceVal, tpSerializer.TargetType));

                    return Expression.Lambda<SD.DeserializeFromStreamT<T>>(                                                     
                                                     Expression.Block(tpSerializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                     , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                     , new[] { param_Stream, param_ReadedTypeID }
                                                     );

                }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($" ERROR in {nameof(SerializeGenerator_ComplexType)}.{nameof(GenerateDeserialize_ReadExpression)}() :   Message -[{exc.Message}]  ", exc);
            }

        }


        #endregion ------------------------------------- GENERATE  DESERIALIZE-  READ  EXPRESSIONS  ---------------------------
        

#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE DEBUG SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer<T> tpSerializer = null)
        {
            try
            {
                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && classes  - with  If(instance != null){}

                // write T typeID - void Write_T_TypeID(Stream ms)-for 1 image: Not nullable struct |  
                //                  void Write_T_TypeID(Stream ms, T data) - for 2 image: nullable structs && classes
                // write members - void TypeSerializer<T>.void WriteMemberToStream<TMember>(ushort typeID, string member, Stream ms, T instanceValue)


                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");
                var typeSerializerInstanceParam = Expression.Parameter(typeof(TypeSerializer<T>), "typeSeriazerInstance"); //real code  Expression.Const(tpProcessor)

                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                if (tpSerializer.TargetType.IsValueType && !tpSerializer.LA_TargetTypeEx.Value.IsNullabeValueType) //  Tools.IsNullableType(tpSerializer.TargetType)
                {
                    List<Expression> operators_Serialize_T = new List<Expression>();

                    //1. Write typeID - Write_T_TypeID(Stream ms)                   
                    operators_Serialize_T.Add( Generate_Write_T_TypeID<T>(typeSerializerInstanceParam, parameterStream,  null)    );

                    //2. write members:
                    operators_Serialize_T.AddRange(  Generate_WriteMembersCode(tpSerializer, parameterStream, parameterInputData, typeSerializerInstanceParam)   );
                    
                    return Expression.Lambda< SD.SerializeToStreamTDbg<T> >( // real code TypeSerializer<T>.Serialize
                                                    Expression.Block(operators_Serialize_T)
                                                    , parameterStream
                                                    , parameterInputData
                                                    , typeSerializerInstanceParam
                                                    );
                }
                ///////2 image Nullable Structs - empty here now

                ///////3 image  
                else  // 3 image: for classes 
                {

                    List<Expression> operators_InstanceSerialize_T = new List<Expression>();
                    List<Expression> operators_MembersSerialize_T = new List<Expression>();


                    //1. Write typeID - Write_T_TypeID(Stream ms)
                 
                    operators_InstanceSerialize_T.Add(  Generate_Write_T_TypeID<T>(typeSerializerInstanceParam, parameterStream, parameterInputData) );

                    //2. write members:
                    operators_MembersSerialize_T.AddRange(  Generate_WriteMembersCode( tpSerializer, parameterStream,parameterInputData, typeSerializerInstanceParam)  );

                    //  only when members was found
                    if (operators_MembersSerialize_T.Count > 0)
                    {
                        ////3. if(instanceVal == null) return instanceVal;               
                        operators_InstanceSerialize_T.Add(Expression.IfThen(
                                                        Expression.NotEqual(parameterInputData, Expression.Constant(null)),
                                                        Expression.Block(operators_MembersSerialize_T)
                                                                           )
                                                         );
                    }

                    return Expression.Lambda<SD.SerializeToStreamTDbg<T>>( // real code TypeSerializer<T>.Serialize
                                                    Expression.Block(operators_InstanceSerialize_T)
                                                    , parameterStream
                                                    , parameterInputData
                                                    , typeSerializerInstanceParam
                                                    );
                }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ComplexType)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }






        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpserializer"></param>
        /// <returns></returns>
        public Expression< SD.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer<T> tpserializer = null)
        {
            try
            {
                //Deserialize Expression signature:   object Deserialize(Stream stream, bool readedTypeID = false);

                //algorithm:
                // 1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                // 
                // 2 image: for nullable structs && /classes  - with  If(instance != null){}


                // ReadTypeID   and CreateValue if typeID not 0 - not null 
                // if(typeID == 0 && class/ struct?)  -  return Null
                //Read fields      -- if we wont find GetTypeID it will throw exception - so the member typeID is strong
                //Read Properties  -- if we wont find GetTypeID it will throw exception - so the member typeID is strong

                // T instanceVal = T ReadTypeID_CreateValue(Stream ms,bool readedTypeID = false ) //, Int32 length = 0 
                // if(instanceVal != null)
                // foreach(field; property) {void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, Int32 length = 0, bool readedTypeID = false) }
                // return instanceVal;

                var param_Stream = Expression.Parameter(typeof(Stream), "targetStream");
                var param_ReadedTypeID = Expression.Parameter(typeof(bool), "readedTypeID");
                var param_typeProcessorInstance = Expression.Parameter(typeof(TypeSerializer<T>), "typeprocessorInstance");



                //1.  T instanceVal = T ReadTypeID_CreateValue(Stream ms, bool readedTypeID = false, Int32 length = 0,  ) 
                var variable_InstanceVal = Expression.Variable(tpserializer.TargetType, "instanceVal");
                var method_ReadTypeID_CreateValue = typeof(TypeSerializer<T>)
                                                         .GetMethod(nameof(TypeSerializer<T>.ReadTypeID_CreateValue) );
                var constant_memberReadedTypeID = Expression.Constant(false);


                ///  1 image: for Not Nullable  structs - valueTypes    - without  If(value != null){} 
                if (tpserializer.TargetType.IsValueType && !tpserializer.LA_TargetTypeEx.Value.IsNullabeValueType)
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();

                    operators_InstanceDeserialize.Add( Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance, method_ReadTypeID_CreateValue, param_Stream, param_ReadedTypeID)  );
                    
                    variable_InstanceVal.Type.MakeByRefType();
                    
                    foreach (var member in tpserializer.AccessorT.Members)
                    {
                        operators_InstanceDeserialize.Add( Generate_ReadMemberCode(tpserializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream) ) ;
                    }
                    
                    operators_InstanceDeserialize.Add(Expression.Convert(variable_InstanceVal, tpserializer.TargetType));

                    return Expression.Lambda< SD.DeserializeFromStreamTDbg<T> >(
                                                     Expression.Block(tpserializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                     , tpserializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                     , new[] { param_Stream, param_ReadedTypeID, param_typeProcessorInstance }
                                                   
                                                     );
                }

                //2 image for Nullable structs
                ////////// 2 image  structs -  Nullable
                else if (tpserializer.TargetType.IsValueType && tpserializer.LA_TargetTypeEx.Value.IsNullabeValueType)  //2 image:   nullable structs but not classes
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();
                    List<Expression> operators_MembersDeserialize = new List<Expression>();

                    operators_InstanceDeserialize.Add(Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance, method_ReadTypeID_CreateValue, param_Stream, param_ReadedTypeID));

                    variable_InstanceVal.Type.MakeByRefType();

                    //Variable.Value property --?

                    ////for each members
                    foreach (var member in tpserializer.AccessorT.Members)
                    {
                        operators_MembersDeserialize.Add(Generate_ReadMemberCode(tpserializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream));
                    }

                    //  only when members was found
                    Generate_Switch_IfTypeContainsMembersToRead(operators_InstanceDeserialize, operators_MembersDeserialize, variable_InstanceVal);

                    //return 
                    operators_InstanceDeserialize.Add(Expression.Convert(variable_InstanceVal, tpserializer.TargetType));

                    return Expression.Lambda<SD.DeserializeFromStreamTDbg<T>>(
                                                     Expression.Block(tpserializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                     , tpserializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                     , new[] { param_Stream, param_ReadedTypeID , param_typeProcessorInstance }
                                                   
                                                     );

                }

                else //3 image: for classes  - with  If(instance != null){}
                {
                    List<Expression> operators_InstanceDeserialize = new List<Expression>();
                    List<Expression> operators_MembersDeserialize = new List<Expression>();
                    

                    operators_InstanceDeserialize.Add( Generate_AssignLocalVariableByReadTypeIDCreateValue(variable_InstanceVal, param_typeProcessorInstance, method_ReadTypeID_CreateValue, param_Stream, param_ReadedTypeID) );
                                   

                    variable_InstanceVal.Type.MakeByRefType();


                    ////for each member
                    foreach (var member in tpserializer.AccessorT.Members)
                    {
                        operators_InstanceDeserialize.Add( Generate_ReadMemberCode(tpserializer, member.Value, variable_InstanceVal, param_typeProcessorInstance, param_Stream));
                    }
                    
                    //  only when members was found
                    if (operators_MembersDeserialize.Count > 0)
                    {
                        ////3. if(instanceVal == null) return instanceVal;               
                        operators_InstanceDeserialize.Add(Expression.IfThen(
                                                        Expression.NotEqual(variable_InstanceVal, Expression.Constant(null)),
                                                        Expression.Block(operators_MembersDeserialize)
                                                                           )
                                                         );

                    }

                    //return
                    operators_InstanceDeserialize.Add(Expression.Convert(variable_InstanceVal, tpserializer.TargetType));

                    return Expression.Lambda< SD.DeserializeFromStreamTDbg<T> >(
                                                     Expression.Block(tpserializer.TargetType, new[] { variable_InstanceVal },
                                                                                              operators_InstanceDeserialize)
                                                     , tpserializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                     , new[] { param_Stream, param_ReadedTypeID, param_typeProcessorInstance }
                                                  
                                                     );

                }

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_ComplexType)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif






        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }


        /// <summary>
        ///  Data Formatting of  this TypeSerializerGenerator - for [ComplexStructure] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary; 
            }
        }



        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ComplexType)} - can't have the Final List of Supported Types");
        }
              

        /// <summary>
        /// Loading default contracts handlers into TYPE SET SERIALIZER - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers(ITypeSetSerializer serializer)
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_ComplexType)} - doesn't have the Final List of Supported Types");
        }
               


        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------




    }
}










#region ------------------------------------ GARBAGE -----------------------------------------


//for each field                     
//                    foreach (var fld in tpSerializer.AccessorT.Fields)
//                    {
//                        var parameter_fieldTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType())); //can throw if type won't find out
//var parameter_FieldName = Expression.Constant(fld.Key); //key == Name

//MethodInfo methodWriteMemberToStream = null;
//                        if (fld.Value.GetMemberType().IsNeedToSureInDataType())
//                        {
//                            methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
//                                                                 .MakeGenericMethod(fld.Value.GetMemberType());//FieldType
//                        }
//                        else
//                        {
//                            methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
//                                                                 .MakeGenericMethod(fld.Value.GetMemberType());//FieldType
//                        }


//                        operators_MembersSerialize_T.Add(Expression.Call(typeSerializerInstanceParam,
//                                                                 methodWriteMemberToStream,
//                                                                 parameter_fieldTypeID,
//                                                                 parameter_FieldName,
//                                                                 parameterStream,
//                                                                 parameterInputData
//                                                                )
//                                             );

//                    }

//                    //for each property
//                    foreach (var prpty in tpSerializer.AccessorT.Properties)
//                    {
//                        var parameter_propertyTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType()));  //can throw if type won't find out
//var parameter_propertyName = Expression.Constant(prpty.Key);


//MethodInfo methodWriteMemberToStream = null;
//                        if (prpty.Value.GetMemberType().IsNeedToSureInDataType())
//                        {
//                            methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
//                                                                 .MakeGenericMethod(prpty.Value.GetMemberType());//PropertyType
//                        }
//                        else
//                        {
//                            methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
//                                                                 .MakeGenericMethod(prpty.Value.GetMemberType());//PropertyType
//                        }

//                        operators_MembersSerialize_T.Add(Expression.Call(typeSerializerInstanceParam,
//                                                                 methodWriteMemberToStream,
//                                                                 parameter_propertyTypeID,
//                                                                 parameter_propertyName,
//                                                                 parameterStream,
//                                                                 parameterInputData
//                                                                )
//                                             );

//                    }




//2. write members:
////for each field                     
//foreach (var fld in tpSerializer.AccessorT.Fields)
//{
//    var parameter_fieldTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType() ) ); //can throw if type won't find out
//    var parameter_FieldName = Expression.Constant(fld.Key); //


//    MethodInfo methodWriteMemberToStream = null;
//    if (fld.Value.GetMemberType().IsNeedToSureInDataType())
//    {
//        methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
//                                             .MakeGenericMethod(fld.Value.GetMemberType());//FieldType
//    }
//    else
//    {
//        methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
//                                             .MakeGenericMethod(fld.Value.GetMemberType());//FieldType
//    }


//    operators_Serialize_T.Add(Expression.Call(typeSerializerInstanceParam,
//                                             methodWriteMemberToStream,
//                                             parameter_fieldTypeID,
//                                             parameter_FieldName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}

////foreach Property
//foreach (var prpty in tpSerializer.AccessorT.Properties)
//{
//    var parameter_propertyTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType())); //can throw if type won't find out
//    var parameter_propertyName = Expression.Constant(prpty.Key);     // Key == Name 

//    MethodInfo methodWriteMemberToStream = null;
//    if (prpty.Value.GetMemberType().IsNeedToSureInDataType())
//    {
//        methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedToSureInDataType))
//                                             .MakeGenericMethod(prpty.Value.GetMemberType());//PropertyType
//    }
//    else
//    {
//        methodWriteMemberToStream = tpSerializer.GetType().GetMethod(nameof(TypeSerializer<T>.WriteMemberToStream_NeedNotToSureInDataType))
//                                             .MakeGenericMethod(prpty.Value.GetMemberType());//PropertyType
//    }


//    operators_Serialize_T.Add(Expression.Call(typeSerializerInstanceParam,
//                                             methodWriteMemberToStream,
//                                             parameter_propertyTypeID,
//                                             parameter_propertyName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}




//for each field 
//foreach (var fld in tpSerializer.AccessorT.Fields)
//{
//    var parameter_fieldTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType()));  //can throw if type won't find out
//    var parameter_FieldName = Expression.Constant(fld.Key);

//    MethodInfo methodWriteMemberToStream = Generate_GetWriteMemberActualMethod(fld.Value.GetMemberType(), tpSerializer);

//    operators_MembersSerialize_T.Add(Expression.Call(typeSerializerInstance,
//                                             methodWriteMemberToStream,
//                                             parameter_fieldTypeID,
//                                             parameter_FieldName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}

////for each property
//foreach (var prpty in tpSerializer.AccessorT.Properties)
//{
//    var parameter_propertyTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType() ));  //can throw if type won't find out
//    var parameter_propertyName = Expression.Constant(prpty.Key);

//    MethodInfo methodWriteMemberToStream = Generate_GetWriteMemberActualMethod(prpty.Value.GetMemberType(), tpSerializer);

//    operators_MembersSerialize_T.Add(Expression.Call(typeSerializerInstance,
//                                             methodWriteMemberToStream,
//                                             parameter_propertyTypeID,
//                                             parameter_propertyName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}



//for each field                     
//foreach (var fld in tpSerializer.AccessorT.Fields)
//{
//    var parameter_fieldTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType()));   //can throw if type won't find out//FieldType
//    var parameter_FieldName = Expression.Constant(fld.Key);// Name of field

//    MethodInfo methodWriteMemberToStream = Generate_GetWriteMemberActualMethod(fld.Value.GetMemberType(), tpSerializer);

//    operators_Serialize_T.Add(Expression.Call(typeSerializerInstance,
//                                             methodWriteMemberToStream,
//                                             parameter_fieldTypeID,
//                                             parameter_FieldName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}

////for each property
//foreach (var prpty in tpSerializer.AccessorT.Properties)
//{
//    var parameter_propertyTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType() ) );  //can throw if type won't find out
//    var parameter_propertyName = Expression.Constant(prpty.Key); // Name of property

//    MethodInfo methodWriteMemberToStream = Generate_GetWriteMemberActualMethod(prpty.Value.GetMemberType(), tpSerializer);

//    operators_Serialize_T.Add(Expression.Call(typeSerializerInstance,
//                                             methodWriteMemberToStream,
//                                             parameter_propertyTypeID,
//                                             parameter_propertyName,
//                                             parameterStream,
//                                             parameterInputData
//                                            )
//                         );

//}


//public Expression<TypeSerializer<T>.Serialize> GenerateSerialize_WriterMethod<T>(TypeSerializer<T> tpProcessor)
//        {

//            try
//            {
//                //ProcessingsByShortID[typeID].RaiseGenericSerializeHandler(stream, data); 



//                //write id     to stream 
//                //write fields to stream

//                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
//                var parameterInputData = Expression.Parameter(tpSerializer.TargetType, "data");

//                List<Expression> calls_MemberWrite = new List<Expression>();


//                //for each field                 
//                foreach (var fld in tpProcessor.Fields.Values)
//                {
//                    if (fld.FieldType.IsPrimitive) //for primitives 
//                    {
//                        var GetMemberByAccessorMethod = GetType().GetMethod("GetMember", new[] { typeof(string), tpSerializer.TargetType });

//                        Expression callGetMemberValueExpression = Expression.Call(Expression.Constant(this),
//                                                                                  GetMemberByAccessorMethod,
//                                                                                   Expression.Constant(fld.Name),
//                                                                                   parameterInputData
//                                                                                  );

//                        var writePrimitiveMethod = typeof(PrimitiveReaderWriter).GetMethod("WritePrimitive", new[] { typeof(Stream), fld.FieldType });

//                        calls_MemberWrite.Add(
//                                                Expression.Call(
//                                                writePrimitiveMethod,
//                                                parameterStream,
//                                                Expression.Convert(callGetMemberValueExpression, fld.FieldType))
//                                             );
//                    }
//                    else // for no primitive field type - custom type serialize generator
//                    {

//                    }

//                }

//                //for each property                 
//                foreach (var prpty in tpProcessor.Properties.Values)
//                {
//                    if (prpty.PropertyType.IsPrimitive) //for primitives 
//                    {
//                        var GetMemberByAccessorMethod = GetType().GetMethod("GetMember", new[] { typeof(string), tpSerializer.TargetType });

//                        Expression callGetMemberValueExpression = Expression.Call(Expression.Constant(this),
//                                                                                   GetMemberByAccessorMethod,
//                                                                                   Expression.Constant(prpty.Name),
//                                                                                   parameterInputData
//                                                                                  );

//                        var writePrimitiveMethod = typeof(PrimitiveReaderWriter).GetMethod("WritePrimitive", new[] { typeof(Stream), prpty.PropertyType });

//                        calls_MemberWrite.Add(
//                                                Expression.Call(
//                                                writePrimitiveMethod,
//                                                parameterStream,
//                                                Expression.Convert(callGetMemberValueExpression, prpty.PropertyType))
//                                             );
//                    }
//                    else // for no primitive property type - custom type serialize generator
//                    {
//                        //
//                        //NetSerializerDebug.SerializerSwitch(stream, (object)value.m_abstractMsg);
//                        //void SerializeInternal(Stream stream, object data)
//                        var SerializerSwitchMethod = tpProcessor.HostSerializer.GetType().GetMethod("SerializeInternal", new[] { typeof(Stream), typeof(object) });

//                        var callSwithForNonPrimitive = Expression.Call(Expression.Constant(tpProcessor.HostSerializer)
//                                                                       , SerializerSwitchMethod
//                                                                       , parameterStream
//                                                                       , parameterInputData
//                                                                       );
//                    }
//                }
//                return Expression.Lambda<TypeSerializer<T>.Serialize>(
//                                                Expression.Block(calls_MemberWrite)
//                                                , parameterStream
//                                                , parameterInputData
//                                                );
//            }
//            catch (Exception)
//            {
//                throw;
//            }
//        }



////   reading Members Code 
//foreach (var fld in tpSerializer.AccessorT.Fields)
//                    {
//                        //2.  ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, ref T instanceValue, bool readedTypeID = false)

//                        var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                                       .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream)) //  "ReadMemberFromStream"
//                                                                       .MakeGenericMethod(fld.Value.GetMemberType());

//// memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType()));

//operators_InstanceDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                                   method_ReadMemberFromStream,
//                                                                   variable_memberTypeID,
//                                                                   Expression.Constant(fld.Key), //Key == Name
//                                                                   param_Stream,
//                                                                   variable_InstanceVal,
//                                                                   constant_memberReadedTypeID     //readedTypeID                                                                       
//                                                                 )
//                                                         );

//                    }


//                    //for each property

//                    foreach (var prpty in tpSerializer.AccessorT.Properties)
//                    {
//                        //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false, Int32 length = 0 )

//                        var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                                      .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream))
//                                                                      .MakeGenericMethod(prpty.Value.GetMemberType());

//// memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType()));

//operators_InstanceDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                                   method_ReadMemberFromStream,
//                                                                   variable_memberTypeID,
//                                                                   Expression.Constant(prpty.Key),
//                                                                   param_Stream,
//                                                                   variable_InstanceVal,
//                                                                   constant_memberReadedTypeID     //readedTypeID

//                                                                 )
//                                                         );

//                    }


// Add Assign of local variable by ReadedTypeID  and CreatedValue 
//    Expression.Assign(variable_InstanceVal,
//                                                Expression.Call(
//                                                param_typeProcessorInstance,
//                                                method_ReadTypeID_CreateValue,
//                                                param_Stream,
//                                                param_ReadedTypeID,
//                                                Expression.Constant(new int[] { })//length to create T  0
//                                                )
//                                              )
//                                 );

// 
//  //foreach (var fld in tpSerializer.AccessorT.Fields)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false, Int32 length = 0 )

//    var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                  .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream))
//                                                  .MakeGenericMethod(fld.Value.GetMemberType());

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType()));

//    operators_MembersDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                method_ReadMemberFromStream,
//                                                variable_memberTypeID,
//                                                Expression.Constant(fld.Key), //Key == Name
//                                                param_Stream,
//                                                variable_InstanceVal, //ValueProperty,
//                                                constant_memberReadedTypeID     //readedTypeID
//                                                                                // constant_Length // length
//                                                )
//                                    );

//}


////for each property
//foreach (var prpty in tpSerializer.AccessorT.Properties)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false)
//    var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                  .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream))
//                                                  .MakeGenericMethod(prpty.Value.GetMemberType());

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType()));

//    operators_MembersDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                     method_ReadMemberFromStream,
//                                                     variable_memberTypeID,
//                                                     Expression.Constant(prpty.Key), //Key == Name
//                                                     param_Stream,
//                                                     variable_InstanceVal,   //ValueProperty,   //
//                                                     constant_memberReadedTypeID     //readedTypeID,     //readedTypeID
//                                                                                     //constant_Length         // length
//                                                    )
//                                    );

//}


//var ValueProperty = Expression.Property(variable_InstanceVal, "Value");
//ValueProperty.Type.MakeByRefType();


// deserialize 3 image - for classes
//foreach (var fld in tpSerializer.AccessorT.Fields)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false, Int32 length = 0 )

//    var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                  .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream))
//                                                  .MakeGenericMethod(fld.Value.GetMemberType());

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(fld.Value.GetMemberType()));

//    operators_MembersDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                method_ReadMemberFromStream,
//                                                variable_memberTypeID,
//                                                Expression.Constant(fld.Key),
//                                                param_Stream,
//                                                variable_InstanceVal,
//                                                constant_memberReadedTypeID     //readedTypeID
//                                                                                // constant_Length // length
//                                                )
//                                );

//}


////for each property
//foreach (var prpty in tpSerializer.AccessorT.Properties)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false)
//    var method_ReadMemberFromStream = tpSerializer.GetType()
//                                                  .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream))
//                                                  .MakeGenericMethod(prpty.Value.GetMemberType());

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpSerializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType()));

//    operators_MembersDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                     method_ReadMemberFromStream,
//                                                     variable_memberTypeID,
//                                                     Expression.Constant(prpty.Key),
//                                                     param_Stream,
//                                                     variable_InstanceVal,
//                                                     constant_memberReadedTypeID     //readedTypeID,     //readedTypeID
//                                                                                     //constant_Length         // length
//                                                    )
//                                    );

//}




//if (operators_MembersDeserialize.Count > 0)
//{
//    ////3. if(instanceVal == null) return instanceVal;               
//    operators_InstanceDeserialize.Add(Expression.IfThen(
//                                    Expression.NotEqual(variable_InstanceVal, Expression.Constant(null)),
//                                    Expression.Block(operators_MembersDeserialize)
//                                                       )
//                                     );

//}


// Deserialize in Debug
//////for each field  
//foreach (var fld in tpserializer.AccessorT.Fields)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false, Int32 length = 0 )

//    var method_ReadMemberFromStream = typeof(TypeSerializer<T>)
//                                            .GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream) )
//                                            .MakeGenericMethod(fld.Value.GetMemberType() );

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpserializer.SetSerializer.GetTypeID(fld.Value.GetMemberType() ) );

//    operators_InstanceDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                method_ReadMemberFromStream,
//                                                variable_memberTypeID,
//                                                Expression.Constant(fld.Key), // key == Name
//                                                param_Stream,
//                                                variable_InstanceVal,
//                                                constant_memberReadedTypeID     //readedTypeID

//                                                )
//                                );

//}


////for each property                   
//foreach (var prpty in tpserializer.AccessorT.Properties)
//{
//    //2.  void ReadMemberFromStream<TMember>(ushort typeID, string memberKey, Stream ms, T instanceValue, bool readedTypeID = false, Int32 length = 0 )                        

//    var method_ReadMemberFromStream = typeof(TypeSerializer<T>).GetMethod(nameof(TypeSerializer<T>.ReadMemberFromStream)) //readmmber
//                                                                .MakeGenericMethod(prpty.Value.GetMemberType());

//    // memberType typeID -  will be changed inside generator to  typeProcessor .GetTypeID value 
//    var variable_memberTypeID = Expression.Constant(tpserializer.SetSerializer.GetTypeID(prpty.Value.GetMemberType() ) );

//    operators_InstanceDeserialize.Add(Expression.Call(param_typeProcessorInstance,
//                                                method_ReadMemberFromStream,
//                                                variable_memberTypeID,
//                                                Expression.Constant(prpty.Key),
//                                                param_Stream,
//                                                variable_InstanceVal,
//                                                constant_memberReadedTypeID     //readedTypeID

//                                                )
//                                );

//}


#endregion ------------------------------------ GARBAGE -----------------------------------------