﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using DDDD.Core.IO;

namespace DDDD.Core.Serialization
{
    /// <summary>
    /// Type Serialize Generator for interfaces types and object type.
    /// </summary>
    internal class SerializeGenerator_Interfaces_Object2: ITypeSerializerGenerator2
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_Interfaces_Object:
        ///     - all interfaces and object type;
        ///     -interfaces can't be IList or IDictionary
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return (    ( type.IsInterface 
                      && !typeof(IList).IsAssignableFrom(type)
                      && !typeof(IDictionary).IsAssignableFrom(type) 
                        )
                      || type == typeof(object));            
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------
        

        #region ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------
        // 


        public static void Write_Interface_ToStream<T>(ITypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //switch to real type
            if (instanceValue == null) { BinaryReaderWriter.WriteT_uint16(stream, (ushort)0); return; }

            //for auto detecting and adding Contracts -Collection Contact 
            var valuetype = instanceValue.GetType();
            if (typeof(IEnumerable).IsAssignableFrom(valuetype) )//can't be enum -so need not to check here
            {
                TypeSetSerializer2.AddKnownType(valuetype);            
            }

            //var realTypeID = ;// GetTypeID(instanceValue.GetType());
            BinaryReaderWriter.Write(stream, typeSerializer.TypeID);
            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(typeSerializer.TypeID, stream, instanceValue); // realTypeID
        }


        public static object Read_Interface_FromStream<T>(ITypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            if (readedTypeID == false)
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
                if (typeId == 0U) //is null
                {
                    return null;//just the same as return null  for reference types
                }

                if (typeId != typeSerializer.TypeID)
                {
                  return typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeId, stream, true);//yes we red your typeID
                }
            }

            //readed and not null - can be only object() - we won't switch somewhere else
            return default(T);
        }


        public static void Write_object_ToStream<T>(ITypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //switch to real type
            if (instanceValue == null) { BinaryReaderWriter.Write(stream, (ushort)0); return; }
            
            //for auto detecting and adding Contracts 
            var valuetype = instanceValue.GetType();
            if (valuetype.IsEnum || typeof(IEnumerable).IsAssignableFrom(valuetype))
            {
                //typeProcessor.SetSerializer.DetectAddAutoContract(valuetype);
                //typeProcessor.SetSerializer.BuildTypeProcessor ();
            }
            
            //var realTypeID = typeProcessor.GetTypeID(instanceValue.GetType());
            BinaryReaderWriter.Write(stream, typeSerializer.TypeID ); // realTypeID
            typeSerializer.RaiseElementSerializeToStreamBoxedHandler( typeSerializer.TypeID , stream, instanceValue);
        }


        public static object Read_object_FromStream<T>(ITypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            if (readedTypeID == false)
            {
                ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);  //read typeID if 0 return null
                if (typeId == 0U) //is null
                {
                    return null;//just the same as return null  for reference types
                }

                if (typeId != typeSerializer.TypeID)
                {
                 return typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(typeId, stream, true);//yes we red your typeID
                }
            }

            //readed and not null - can be only object() - we won't switch somewhere else
            return default(T);
        }


        #endregion ------------------------------------  READ /WRITE  INTERFACE , OBJECT HANDLERS ------------------------------------



        #region -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD2.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializer2<T> tpSerializer)
        {
            try
            {
                //algorithm:            
                // void Write_Interface_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)
                // void Write_object_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                if (typeof(T).IsInterface)
                {

                    var methodWrite_Interface_ToStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                 .GetMethod(nameof(Write_Interface_ToStream))
                                                                                 .MakeGenericMethod(tpSerializer.TargetType);


                    return Expression.Lambda<SD2.SerializeToStreamT<T>>(
                                                           Expression.Call(
                                                             //typeProcessorInstance
                                                             methodWrite_Interface_ToStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                            , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                           , new[] { parameterStream, parameterInputData }
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {

                    var methodWrite_object_ToStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                  .GetMethod(nameof(Write_object_ToStream))
                                                                                  .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SD2.SerializeToStreamT<T>>(
                                                           Expression.Call(
                                                             methodWrite_object_ToStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Serialize_Write"
                                                           , new[] { parameterStream, parameterInputData }
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_Interfaces_Object2)}.{nameof(GenerateSerialize_WriteExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }

          
        }


        /// <summary>
        ///  Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD2.DeserializeFromStreamT<T>> GenerateDeserialize_ReadExpression<T>(ITypeSerializer2<T> tpSerializer)
        {
            try
            {
                //algorithm:
                // object Read_Interface_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
                // object Read_object_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                if (typeof(T).IsInterface)
                {

                    var methodRead_Interface_FromStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                    .GetMethod(nameof(Read_Interface_FromStream))
                                                                                    .MakeGenericMethod(tpSerializer.TargetType);

                    return Expression.Lambda<SD2.DeserializeFromStreamT<T>>(
                                                           Expression.Call(
                                                             methodRead_Interface_FromStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] { parameterStream, parameterReadedTypeID }
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {


                    var methodRead_object_FromStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                     .GetMethod(nameof(Read_object_FromStream))
                                                                                     .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SD2.DeserializeFromStreamT<T>>(
                                                           Expression.Call(
                                                             methodRead_object_FromStream
                                                           , typeProcessorInstance
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                            , tpSerializer.AccessorT.TAccessEx.TypeDebugCodeConventionName + "Deserialize_Read"
                                                           , new[] { parameterStream, parameterReadedTypeID }
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_Interfaces_Object2)}.{nameof(GenerateDeserialize_ReadExpression)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }

          
        }

        
        #endregion -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression<SD2.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer2<T> tpSerializer = null)
        {
            try
            {
                //algorithm:            
                // void Write_Interface_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)
                // void Write_object_ToStream<T>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer2<T>), "typeProcessor");


                if (typeof(T).IsInterface)
                {

                    var methodWrite_Interface_ToStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                 .GetMethod( nameof(Write_Interface_ToStream) )
                                                                                 .MakeGenericMethod(tpSerializer.TargetType);


                    return Expression.Lambda<SD2.SerializeToStreamTDbg<T>>(
                                                           Expression.Call(
                        //typeProcessorInstance
                                                             methodWrite_Interface_ToStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , parameterStream
                                                           , parameterInputData
                                                           , parameterTypeProcessor
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {

                    var methodWrite_object_ToStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                                .GetMethod( nameof(Write_object_ToStream) )
                                                                                .MakeGenericMethod(tpSerializer.TargetType);
                    return Expression.Lambda<SD2.SerializeToStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodWrite_object_ToStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterInputData
                                                           )
                                                           , parameterStream
                                                           , parameterInputData
                                                           , parameterTypeProcessor
                                                           );
                }

                return null;
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_Interfaces_Object2)}.{nameof(GenerateSerialize_WriteExpressionDbg)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SD2.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer2<T> tpProcessor = null)
        {
            try
            {
                //algorithm:
                // object Read_Interface_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
                // object Read_object_FromStream<T>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer2<T>), "typeProcessor");



                if (typeof(T).IsInterface)
                {

                    var methodRead_Interface_FromStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                .GetMethod(nameof( Read_Interface_FromStream) )
                                                                .MakeGenericMethod(tpProcessor.TargetType);

                    return Expression.Lambda<SD2.DeserializeFromStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodRead_Interface_FromStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           , parameterTypeProcessor
                                                           );
                }
                else if (typeof(T) == typeof(object))
                {


                    var methodRead_object_FromStream = typeof(SerializeGenerator_Interfaces_Object2)
                                                                    .GetMethod(nameof(Read_object_FromStream) )
                                                                    .MakeGenericMethod(tpProcessor.TargetType);

                    return Expression.Lambda<SD2.DeserializeFromStreamTDbg<T>>(
                                                           Expression.Call(
                                                             methodRead_object_FromStream
                                                           , parameterTypeProcessor
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           )
                                                           , parameterStream
                                                           , parameterReadedTypeID
                                                           , parameterTypeProcessor
                                                           );
                }

                return null;

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(SerializeGenerator_Interfaces_Object2)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}()  ERROR : Message -[{exc.Message}]  ", exc);
            }
        }
        

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [interfaces and object] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            return new List<Type>() { typeof(object) };      
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>      
        public void LoadDefaultTypeSerializers()
        {
            //object and its default 
            var tpSerializer = TypeSetSerializer2.AddKnownType<object>();  //ITypeSetSerializer serializer
            tpSerializer.SetSerializeToStreamTHandlers(
                                                GenerateSerialize_WriteExpression(tpSerializer).Compile() ,
                                                GenerateDeserialize_ReadExpression(tpSerializer).Compile()
                                                );
        }
        

        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}
