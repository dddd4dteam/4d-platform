﻿using System;
using System.IO;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DDDD.Core.IO;
using DDDD.Core.Extensions;

namespace DDDD.Core.Serialization
{
    /// <summary>
    ///Type Serialize Generator for classes which implement IDictionary or IDictionary{,}
    /// </summary>
    internal class SerializeGenerator_Dictionary2 : ITypeSerializerGenerator2
    {
        
        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        /// Can Handle  criterion for SerializeGenerator_IDictionary:
        ///     - all classes that based on IDictionary;        
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return typeof(IDictionary).IsAssignableFrom(type);            
        }

        #endregion ------------------------------------- CAN HANDLE --------------------------------------


        // Dictionary Key can't be null - WriteDictKey - doesn't contains null check

        //   Dictionary        [ NeedKey (+)    (impossible) ]    [ NeedValue (+)     NullVal (+)  ]  
        //   Dictionary        [ NeedNotKey(-)  (impossible) ]    [ NeedValue (+)     NullVal (+)  ]  
        //   Dictionary        [ NeedKey (+)    (impossible) ]    [ NeedNotValue(-)   NullVal (-)  ]  
        //   Dictionary        [ NeedNotKey(-)  (impossible) ]    [ NeedNotValue(-)   NullVal (-)  ]   


        #region -------------------------- Dictionary NeedKey NeedValue  WRITE/READ ---------------------------------

        static void WriteDictionary_NeedKeyNeedValue_ToStream<T, TKey, TValue>(ITypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //write T TypeID - if (instance == null) then we write 0
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;
            
            var dictionary = (IDictionary<TKey, TValue>)instanceValue;
                      
            // write IDictionary length  
            BinaryReaderWriter.Write(stream, dictionary.Count);            

            // write all IDictionary elements to Stream
            foreach (var KeyValueItem in dictionary)
            {
                // element Key - Need to Get RealTypeID 
                ushort elementKeyTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Key);
                if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementKeyTypeID)) continue;  // end - value was Null
                
                // write element  Key value
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementKeyTypeID, stream, KeyValueItem.Key);


                //element Value - Need to get RealTypeID
                ushort elementValueTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Value);
                if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementValueTypeID)) continue;  // end - value was Null

                // write element  Value value
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementValueTypeID, stream, KeyValueItem.Value);

            }
        }
                
        static T ReadDictionary_NeedKeyNeedValue_FromStream<T, TKey, TValue>(ITypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
                      
            int collectionLength = -1;
            T result = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return result;//null value
            var dictionary = (IDictionary<TKey, TValue>)result;


            for (int i = 0; i < collectionLength; i++)
            {               
                //element Key - Need to get RealTypeID
                ushort elementKeyRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);                                
                //read element Key value
                var KEY_val = (TKey)typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementKeyRealTypeId, stream, true);  //true

                //element Value - Need to get RealTypeID
                ushort elementValueRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);                
                //element Value need in Null Value check
                if (elementValueRealTypeId == 0)
                {  dictionary.Add(KEY_val, (TValue)(object)null);
                }
                else
                {   //read element Value value
                    var VALUE_val = (TValue)typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementValueRealTypeId, stream, true);//true
                    dictionary.Add(KEY_val, VALUE_val);
                }
            }

            return result;

        }

        #endregion -------------------------- Dictionary NeedKey NeedValue  WRITE/READ ---------------------------------


        #region -------------------------- Dictionary NeedNotKey NeedValue  WRITE/READ ---------------------------------

        static void WriteDictionary_NeedNotKeyNeedValue_ToStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //write T TypeID - if (instance == null) then we write 0
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            IDictionary<TKey, TValue> dictionaryInstance = (IDictionary<TKey, TValue>)instanceValue;

            // write IDictionary length  
            BinaryReaderWriter.Write(stream, dictionaryInstance.Count);

            // write all IDictionary elements to Stream
            foreach (var KeyValueItem in dictionaryInstance)
            {
                // element Key - NeedNot to Get RealTypeID                 
                // write element  body
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID, stream, KeyValueItem.Key);

                //element Value - Need to get RealTypeID
                ushort elementValueTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Value);
                if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementValueTypeID)) continue;  // end - value was Null
                                
                // write element  body
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementValueTypeID, stream, KeyValueItem.Value);
            }
        }

        static T ReadDictionary_NeedNotKeyNeedValue_FromStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T result = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return result;//null value
            var dictionary = (IDictionary<TKey, TValue>)result;

            for (int i = 0; i < collectionLength; i++)
            {
                //element Key - NeedNot to get RealTypeID                
                //read element Key value
                var KEY_val = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TKey>(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID, stream);  

                //element Value - Need to get RealTypeID
                ushort elementValueRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
                //element Value need in Null Value check
                if (elementValueRealTypeId == 0)
                {
                    dictionary.Add(KEY_val, (TValue)(object)null);
                }
                else
                {   //read element Value value
                    var VALUE_val = (TValue)typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementValueRealTypeId, stream, true);
                    dictionary.Add(KEY_val, VALUE_val);
                }
            }

            return result;
        }

        #endregion -------------------------- Dictionary NeedNotKey NeedValue  WRITE/READ ---------------------------------


        #region -------------------------- Dictionary NeedKey NeedNotValue  WRITE/READ ---------------------------------

        static void WriteDictionary_NeedKeyNeedNotValue_ToStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //write T TypeID - if (instance == null) then we write 0
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            IDictionary<TKey, TValue> dictionaryInstance = (IDictionary<TKey, TValue>)instanceValue;

            // write IDictionary length  
            BinaryReaderWriter.Write(stream, dictionaryInstance.Count);

            // write all IDictionary elements to Stream
            foreach (var KeyValueItem in dictionaryInstance)
            {
                // element Key - Need to Get RealTypeID                 
                ushort elementKeyTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Key);
                if (typeSerializer.WriteRealTypeID_4Null_4PrimitiveOrEnum(stream, elementKeyTypeID)) continue;  // end - value was Null

                // write element  Key
                typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementKeyTypeID, stream, KeyValueItem.Key);

                //element Value - NeedNot to Get RealTypeID
                // write element  Value
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg2TypeID, stream, KeyValueItem.Value);
            }
        }

        static T ReadDictionary_NeedKeyNeedNotValue_FromStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        { 
            int collectionLength = -1;
            T result = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return result;//null value

            ///now cast to IList instance
            var dictionary = (IDictionary<TKey, TValue>)result;

            for (int i = 0; i < collectionLength; i++)
            {
                //element Key - Need to get RealTypeID
                ushort elementKeyRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
                //read element Key value
                var KEY_val = (TKey)typeSerializer.RaiseElementDeserializeFromStreamBoxedHandler(elementKeyRealTypeId, stream, true);  //true
                
                //element Value - NeedNot to get RealTypeID
                var VALUE_val = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TValue>( typeSerializer.LA_RuntimeIDs.Value.Arg2TypeID, stream);
                dictionary.Add(KEY_val, VALUE_val);
            }

            return result;
        }

        #endregion -------------------------- Dictionary NeedKey NeedNotValue  WRITE/READ ---------------------------------
        

        #region -------------------------- Dictionary NeedNotKey NeedNotValue  WRITE/READ ---------------------------------

        static void WriteDictionary_NeedNotKeyNeedNotValue_ToStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, T instanceValue)
        {
            //write T TypeID - if (instance == null) then we write 0
            typeSerializer.Write_T_TypeID(stream, instanceValue);
            if (instanceValue == null) return;

            IDictionary<TKey, TValue> dictionaryInstance = (IDictionary<TKey, TValue>)instanceValue;

            // write IDictionary length  
            BinaryReaderWriter.Write(stream, dictionaryInstance.Count);

            // write all IDictionary elements to Stream
            foreach (var KeyValueItem in dictionaryInstance)
            {
                // element Key - NeedNot to Get RealTypeID                 
                // write element  body
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID, stream, KeyValueItem.Key);

                //element Value - NeedNot to get RealTypeID
                // write element  body
                typeSerializer.RaiseElementSerializeToStreamTHandler(typeSerializer.LA_RuntimeIDs.Value.Arg2TypeID, stream, KeyValueItem.Value);
            }
        }

        static T ReadDictionary_NeedNotKeyNeedNotValue_FromStream<T, TKey, TValue>(TypeSerializer2<T> typeSerializer, Stream stream, bool readedTypeID = false)
        {
            int collectionLength = -1;
            T result = typeSerializer.ReadCollectionTypeID_CreateValue(stream, out collectionLength, readedTypeID);
            if (collectionLength == -1) return result;//null value

            ///now cast to IList instance
            var dictionary = (IDictionary<TKey, TValue>)result;

            for (int i = 0; i < collectionLength; i++)
            {
                //element Key - NeedNot to get RealTypeID                
                //read element Key value
                var KEY_val = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TKey>(typeSerializer.LA_RuntimeIDs.Value.Arg1TypeID, stream);  

                //element Value - NeedNot to get RealTypeID                
                //element Value need in Null Value check
                //read element Value value
                var VALUE_val = typeSerializer.RaiseElementDeserializeFromStreamTHandler<TValue>( typeSerializer.LA_RuntimeIDs.Value.Arg2TypeID, stream);
                dictionary.Add(KEY_val, VALUE_val);
                
            }

            return result;

        }

        #endregion -------------------------- Dictionary NeedNotKey NeedNotValue  WRITE/READ ---------------------------------




        static MethodInfo Get_WriteDictionaryMethod<T>(ITypeSerializer2<T> tpSerializer)
        {
            if ( tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
                && tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(WriteDictionary_NeedKeyNeedValue_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );
            }
            else if ( !tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
                && tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(WriteDictionary_NeedNotKeyNeedValue_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }
            else if (tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
               && !tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(WriteDictionary_NeedKeyNeedNotValue_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }
            else if (!tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
               && !tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(WriteDictionary_NeedNotKeyNeedNotValue_ToStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(Get_WriteDictionaryMethod)}() - such Dicitionary<TKey,TValue> Type  is not supported by this serializer [{tpSerializer.TargetType.FullName}] ");

        }


        static MethodInfo Get_ReadDictionaryMethod<T>(ITypeSerializer2<T> tpSerializer)
        {
            if (tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
                && tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(ReadDictionary_NeedKeyNeedValue_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );
            }
            else if (!tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
                && tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(ReadDictionary_NeedNotKeyNeedValue_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }
            else if (tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
               && !tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(ReadDictionary_NeedKeyNeedNotValue_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }
            else if (!tpSerializer.LA_TargetTypeEx.Value.Arg1Type.IsNeedToSureInDataType()
               && !tpSerializer.LA_TargetTypeEx.Value.Arg2Type.IsNeedToSureInDataType())
            {
                return typeof(SerializeGenerator_Dictionary2).GetMethod(nameof(ReadDictionary_NeedNotKeyNeedNotValue_FromStream), BindingFlags.Static | BindingFlags.NonPublic)
                                                            .MakeGenericMethod(tpSerializer.TargetType
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg1Type
                                                                              , tpSerializer.LA_TargetTypeEx.Value.Arg2Type
                                                                              );

            }

            throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(Get_ReadDictionaryMethod)}() - such Dicitionary<TKey,TValue> Type  is not supported by this serializer [{tpSerializer.TargetType.FullName}] ");

        }
        


        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate code expression of serialize-write handler, for all the types that  satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD2.SerializeToStreamT<T> > GenerateSerialize_WriteExpression<T>(ITypeSerializer2<T> tpSerializer)
        {
            //algorithm:
            // void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

            try
            {

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var typeProcessorInstance = Expression.Constant(tpSerializer);


                var methodWrite_IDictionary_ToStream = Get_WriteDictionaryMethod(tpSerializer);

                return Expression.Lambda<SD2.SerializeToStreamT<T>>(
                                                       Expression.Call(
                                                         methodWrite_IDictionary_ToStream
                                                       , typeProcessorInstance
                                                       , parameterStream
                                                       , parameterInputData
                                                       )
                                                       , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Serialize_Write"
                                                       , new[] { parameterStream, parameterInputData }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(GenerateSerialize_WriteExpression)}() : Message -[{exc.Message}]  ", exc);
            }
        }


        /// <summary>
        ///  Generate code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD2.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializer2<T> tpSerializer)
        {
            // algorithm:          
            // T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

            try
            {
                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var typeProcessorInstance = Expression.Constant(tpSerializer);
                
                var methodRead_IDictionary_FromSteam = Get_ReadDictionaryMethod(tpSerializer);

                return Expression.Lambda<SD2.DeserializeFromStreamT<T>>(
                                                       Expression.Call(
                                                         methodRead_IDictionary_FromSteam
                                                       , typeProcessorInstance
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , tpSerializer.LA_TargetTypeEx.Value.TypeDebugCodeConventionName + "Deserialize_Read"
                                                       , new[] { parameterStream, parameterReadedTypeID }
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(GenerateDeserialize_ReadExpression)}() : Message -[{exc.Message}]  ", exc);
            }

          
        }




        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD2.SerializeToStreamTDbg<T> > GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer2<T> tpSerializer = null)
        {
            try
            {
                // void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, T instanceValue)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterInputData = Expression.Parameter(typeof(T), "data");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeProcessor");


                 var methodWrite_IDictionary_ToStream = Get_WriteDictionaryMethod(tpSerializer);
                
                return Expression.Lambda< SD2.SerializeToStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodWrite_IDictionary_ToStream
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterInputData
                                                       )
                                                       , parameterStream
                                                       , parameterInputData,
                                                         parameterTypeProcessor
                                                       );
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(GenerateSerialize_WriteExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle(). 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpSerializer"></param>
        /// <returns></returns>
        public Expression< SD2.DeserializeFromStreamTDbg<T> > GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer2<T> tpSerializer = null)
        {
            try
            {                
                //algorithm:          
                //  T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)

                var parameterStream = Expression.Parameter(typeof(Stream), "targetStream");
                var parameterReadedTypeID = Expression.Parameter(typeof(bool), "ReadedTypeID");
                var parameterTypeProcessor = Expression.Parameter(typeof(TypeSerializer<T>), "typeProcessor");


                var methodRead_IDictionary_FromSteam = Get_ReadDictionaryMethod(tpSerializer);

                return Expression.Lambda< SD2.DeserializeFromStreamTDbg<T> >(
                                                       Expression.Call(
                                                         methodRead_IDictionary_FromSteam
                                                       , parameterTypeProcessor
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       )
                                                       , parameterStream
                                                       , parameterReadedTypeID
                                                       , parameterTypeProcessor
                                                       );

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"ERROR in {nameof(SerializeGenerator_Dictionary2)}.{nameof(GenerateDeserialize_ReadExpressionDbg)}() : Message -[{exc.Message}]  ", exc);
            }

        }
        
        
        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        /// CanLoadDefaultContractHandlers should be true. 
        /// If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return false; } }

        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [IDictionary] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;//  throw new NotImplementedException();
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_Dictionary2)} - can't have the Final List of Supported Types");
        }

        /// <summary>
        /// Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers()
        {
            throw new NotSupportedException($"{nameof(SerializeGenerator_Dictionary2)} - can't have the Final List of Supported Types");
        }

      
        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------


    }
}


#region ---------------------------------------- GARBAGE ------------------------------------

////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T); //just the same as return null  for reference types
//    }
//}

////read dictionary Length 
//int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length
////Creating new Instance
//T result = typeSerializer.LA_CreateDefaultValue.Value(new object[] { collectionLength });
////now get interface IDictionary<TKey, TValue> instance
//var dictionary = (IDictionary<TKey, TValue>)result;


////read TypeID
//if (readedTypeID == false) //need to read typeID 
//{
//    ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//    if (typeId == 0U) //is null
//    {
//        return default(T); //just the same as return null  for reference types
//    }
//}

////read dictionary Length 
//int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length
////Creating new Instance
//T result = typeSerializer.LA_CreateDefaultValue.Value(new object[] { collectionLength });
////now get interface IDictionary<TKey, TValue> instance
//var dictionary = (IDictionary<TKey, TValue>)result;




#region  ---------------------------------OLD   READ / WRITE IDICTIONARY HANDLERS ------------------------------------------

//public static void WriteIDictionaryToStream<T, TKey, TValue>(TypeSerializer<T> typeSerializer, Stream stream, T instanceValue)
//{
//    //write Length
//    typeSerializer.Write_T_TypeID(stream, instanceValue);
//    if (instanceValue == null) return;

//    IDictionary<TKey, TValue> dictionaryInstance = (IDictionary<TKey, TValue>)instanceValue;

//    // write IDictionary length - List Length
//    BinaryReaderWriter.Write(stream, dictionaryInstance.Count);

//    // write all IList elements to Stream
//    foreach (var KeyValueItem in dictionaryInstance)
//    {
//        // write Item.Key Value
//        ushort elementKeyTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Key);
//        BinaryReaderWriter.Write(stream, elementKeyTypeID);

//        //null Key element -hypotetically
//        Validator.AssertTrue<InvalidOperationException>(elementKeyTypeID == 0,
//                                                           "Writing Dictionary Key typeID cannot be null i.e. == 0 value");

//        // write element  body
//        typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementKeyTypeID, stream, KeyValueItem.Key);

//        // write Item.Value Value
//        ushort elementValueTypeID = typeSerializer.GetElementTypeID(KeyValueItem.Value);
//        BinaryReaderWriter.Write(stream, elementValueTypeID);
//        if (elementValueTypeID != 0)
//        {
//            // write element  body
//            typeSerializer.RaiseElementSerializeToStreamBoxedHandler(elementValueTypeID, stream, KeyValueItem.Value);

//        }

//    }


//}


//public static T ReadIDictionaryFromStream<T, TKey, TValue>(TypeSerializer<T> typeProcessor, Stream stream, bool readedTypeID = false)
//{
//    //read TypeID
//    if (readedTypeID == false) //need to read typeID 
//    {
//        ushort typeId = (ushort)BinaryReaderWriter.Read_uint16(stream);
//        if (typeId == 0U) //is null
//        {
//            return default(T);//just the same as return null  for reference types
//        }
//    }

//    //read dictionary Length and creating It
//    int collectionLength = (int)BinaryReaderWriter.Read_int32(stream); //read dictionary Length

//    //now create IDictionary instance
//    var dictionary = (IDictionary<TKey, TValue>)TypeActivator.CreateInstanceT<T>(TypeActivator.DefaultCtorSearchBinding, collectionLength);// //typeProcessor.AccessorT.CreateInstanceT(collectionLength);


//    for (int i = 0; i < collectionLength; i++)
//    {
//        //read Key
//        //var KEY_val = RaiseMemberDeserializeSwitch<TKey>(Arg1TypeID, stream, false);
//        //reading element RealTypeID
//        ushort elementKeyRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        Validator.AssertTrue<InvalidOperationException>(elementKeyRealTypeId == 0,
//                                                           "Reading Dictionary Key typeID cannot be null i.e. == 0 value");


//        //read element Key value
//        var KEY_val = typeProcessor.RaiseElementDeserializeFromStreamTHandler<TKey>(elementKeyRealTypeId, stream);  //true


//        //read Value
//        //var VALUE_val = RaiseMemberDeserializeSwitch<TValue>(Arg2TypeID, stream, false);
//        ushort elementValueRealTypeId = (ushort)BinaryReaderWriter.Read_uint16(stream);

//        //null value element
//        if (elementValueRealTypeId == 0)
//        {
//            dictionary.Add(KEY_val, (TValue)(object)null);
//        }
//        else
//        {
//            //read element Value value
//            var VALUE_val = typeProcessor.RaiseElementDeserializeFromStreamTHandler<TValue>(elementValueRealTypeId, stream);//true
//            dictionary.Add(KEY_val, VALUE_val);
//        }

//    }


//    return (T)dictionary;
//}


#endregion   ---------------------------------OLD   READ / WRITE IDICTIONARY HANDLERS ------------------------------------------

#endregion---------------------------------------- GARBAGE ------------------------------------
