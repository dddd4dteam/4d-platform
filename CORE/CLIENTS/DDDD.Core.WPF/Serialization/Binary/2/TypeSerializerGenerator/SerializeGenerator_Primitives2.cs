﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using DDDD.Core.IO;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;

namespace DDDD.Core.Serialization
{


    /// <summary>
    /// Serialize Generator for Primitive types. All of them are [DefaultContract]. The Primitives are the following types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// </summary>
    internal class SerializeGenerator_Primitives2 : ITypeSerializerGenerator2
    {

        #region ------------------------------------- CAN HANDLE --------------------------------------

        /// <summary>
        ///   Can Handle  criterion for SerializeGenerator_Primitives:           
        ///    Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
        ///                           DateTime,  TimeSpan, Guid,  float, double, decimal        
        ///    Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
        ///                           DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
        ///    Classes:               string, byte[], Uri, BitArray
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public bool CanHandle(Type type)
        {
            return type.Is4DPrimitiveType();
        }


        #endregion ------------------------------------- CAN HANDLE --------------------------------------



        #region  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

        public Expression<SD2.SerializeToStreamT<T>> GenerateSerialize_WriteExpression<T>(ITypeSerializer2<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives2)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        public Expression<SD2.DeserializeFromStreamT<T> > GenerateDeserialize_ReadExpression<T>(ITypeSerializer2<T> tpProcessor)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives2)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }
       

        #endregion  -----------------------------  GENERATE  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------


#if NET45 && DEBUG ///////  -------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ------------- ///////

        #region  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------
        
        /// <summary>
        /// Generate Debug code expression of serialize-write handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SD2.SerializeToStreamTDbg<T>> GenerateSerialize_WriteExpressionDbg<T>(ITypeSerializer2<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives2)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        /// <summary>
        /// Generate Debug code expression of deserialize-read handler, for all the types that satisfy the criterion defined in CanHandle().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="tpProcessor"></param>
        /// <returns></returns>
        public Expression<SD2.DeserializeFromStreamTDbg<T>> GenerateDeserialize_ReadExpressionDbg<T>(ITypeSerializer2<T> tpProcessor = null)
        {
            throw new NotSupportedException($"This TypeSerializerGenerator - [{nameof(SerializeGenerator_Primitives2)}] doesn't support  runtime  Serialize/Deserialize  expressions generation");
        }

        #endregion  -----------------------------  GENERATE  DEBUG  SERIALIZE/DESRIALIZE  WRITE/READ  EXPRESSIONS  ---------------------------

#endif


        #region -------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

        /// <summary>
        /// It means that we try to understand - If we can get so called [Default List] for those we need no generate expression but already has workable De/Serialize handlers.
        /// <para/> So if this Type Serialize Generator has such contracts (and their handlers), 
        /// and wants to add them to the Serializer on its(generator) activation,
        ///  CanLoadDefaultContractHandlers should be true. 
        /// <para/> If TypeSerializeGenerator.CanLoadDefaultContractHandlers is true:
        ///     - then Serializer will Load its [Default Contracts] by LoadDefaultContractHandlers().
        ///     - Also it means that we can get the List of all [Default Contracts] by GetDefaultContractList()
        /// </summary>
        public bool CanLoadDefaultTypeSerializers { get { return true; } }


        /// <summary>
        /// Data Formatting of  this TypeSerializerGenerator - for [4DPrimitive ] types. Current Formatting is Binary.
        /// </summary>
        public FormattingEn Formatting
        {
            get
            {
                return FormattingEn.Binary;        
            }
        }


        /// <summary>
        /// Get the final list of supported types from generator, for whom it can generate De/Serialize handlers
        /// </summary>
        /// <returns></returns>
        public List<Type> GetDefaultContractList()
        {   return TypeInfoEx.PrimitiveTypes; //BinaryReaderWriter.GetRegisteredPrimitives();
        }


        /// <summary>
        ///  Loading default contracts handlers - they will be added just in the same time as Generator activated for Serializer.
        /// </summary>
        /// <param name="serializer"></param>
        public void LoadDefaultTypeSerializers( )
        {

            ////NOT NULLABLE PrimitiveReaderWriter    
            TypeSetSerializer2.AddKnownType<int>().SetSerializeToStreamTHandlers( BinaryReaderWriter.WriteT_int32, BinaryReaderWriter.ReadT_int32);
            TypeSetSerializer2.AddKnownType<uint>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint32, BinaryReaderWriter.ReadT_uint32);
            TypeSetSerializer2.AddKnownType<long>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_long, BinaryReaderWriter.ReadT_int64);
            TypeSetSerializer2.AddKnownType<ulong>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_ulong, BinaryReaderWriter.ReadT_uint64);
            TypeSetSerializer2.AddKnownType<short>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int16, BinaryReaderWriter.ReadT_int16);
            TypeSetSerializer2.AddKnownType<ushort>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint16, BinaryReaderWriter.ReadT_uint16);
            TypeSetSerializer2.AddKnownType<byte>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte, BinaryReaderWriter.ReadT_byte);
            TypeSetSerializer2.AddKnownType<sbyte>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_sbyte, BinaryReaderWriter.ReadT_sbyte);
            TypeSetSerializer2.AddKnownType<char>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_char, BinaryReaderWriter.ReadT_char);
            TypeSetSerializer2.AddKnownType<bool>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_bool, BinaryReaderWriter.ReadT_bool);
            TypeSetSerializer2.AddKnownType<DateTime>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_DateTime, BinaryReaderWriter.ReadT_DateTime);
            TypeSetSerializer2.AddKnownType<TimeSpan>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TimeSpan, BinaryReaderWriter.ReadT_TimeSpan);
            TypeSetSerializer2.AddKnownType<Guid>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Guid, BinaryReaderWriter.ReadT_Guid);
            TypeSetSerializer2.AddKnownType<float>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_float, BinaryReaderWriter.ReadT_float);
            TypeSetSerializer2.AddKnownType<double>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_double, BinaryReaderWriter.ReadT_double);
            TypeSetSerializer2.AddKnownType<decimal>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_decimal, BinaryReaderWriter.ReadT_decimal);



            TypeSetSerializer2.AddKnownType<int?>().SetSerializeToStreamTHandlers(  BinaryReaderWriter.WriteT_int32_nullable, BinaryReaderWriter.ReadT_int32_nullable);
            TypeSetSerializer2.AddKnownType<uint?>().SetSerializeToStreamTHandlers( BinaryReaderWriter.WriteT_uint32_nullable, BinaryReaderWriter.ReadT_uint32_nullable);
            TypeSetSerializer2.AddKnownType<long?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int64_nullable, BinaryReaderWriter.ReadT_int64_nullable);
            TypeSetSerializer2.AddKnownType<ulong?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint64_nullable, BinaryReaderWriter.ReadT_uint64_nullable);
            TypeSetSerializer2.AddKnownType<short?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_int16_nullable, BinaryReaderWriter.ReadT_int16_nullable);
            TypeSetSerializer2.AddKnownType<ushort?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_uint16_nullable, BinaryReaderWriter.ReadT_uint16_nullable);
            TypeSetSerializer2.AddKnownType<byte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte_nullable, BinaryReaderWriter.ReadT_byte_nullable);
            TypeSetSerializer2.AddKnownType<byte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byte_nullable, BinaryReaderWriter.ReadT_byte_nullable);
            TypeSetSerializer2.AddKnownType<sbyte?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_sbyte_nullable, BinaryReaderWriter.ReadT_sbyte_nullable);
            TypeSetSerializer2.AddKnownType<char?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_char_nullable, BinaryReaderWriter.ReadT_char_nullable);
            TypeSetSerializer2.AddKnownType<bool?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_bool_nullable, BinaryReaderWriter.ReadT_bool_nullable);
            TypeSetSerializer2.AddKnownType<DateTime?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_DateTime_nullable, BinaryReaderWriter.ReadT_DateTime_nullable);
            TypeSetSerializer2.AddKnownType<TimeSpan?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TimeSpan_nullable, BinaryReaderWriter.ReadT_TimeSpan_nullable);
            TypeSetSerializer2.AddKnownType<Guid?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Guid_nullable, BinaryReaderWriter.ReadT_Guid_nullable);
            TypeSetSerializer2.AddKnownType<float?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_float_nullable, BinaryReaderWriter.ReadT_float_nullable);
            TypeSetSerializer2.AddKnownType<double?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_double_nullable, BinaryReaderWriter.ReadT_double_nullable);
            TypeSetSerializer2.AddKnownType<decimal?>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_decimal_nullable, BinaryReaderWriter.ReadT_decimal_nullable);



            ////primitive classes : string , byte[]
            TypeSetSerializer2.AddKnownType<string>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_string, BinaryReaderWriter.ReadT_string);
            TypeSetSerializer2.AddKnownType<byte[]>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_byteArray, BinaryReaderWriter.ReadT_byteArray);
            TypeSetSerializer2.AddKnownType<Uri>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_Uri, BinaryReaderWriter.ReadT_Uri);
            TypeSetSerializer2.AddKnownType<BitArray>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_BitArray, BinaryReaderWriter.ReadT_BitArray);

            // 4 Reflection  Classes - TypeInfo
            TypeSetSerializer2.AddKnownType<TypeInfoEx>().SetSerializeToStreamTHandlers(BinaryReaderWriter.WriteT_TypeInfoEx, BinaryReaderWriter.ReadT_TypeInfoEx);


        }





        #endregion-------------------------------- DEFAULT CONTRACT LIST && HANDLERS  -------------------------------------

    }

}


