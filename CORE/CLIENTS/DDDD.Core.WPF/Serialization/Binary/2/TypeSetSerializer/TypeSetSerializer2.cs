﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Threading.Tasks;
using System.Collections;
using System.Threading;

using DDDD.Core.IO;
using DDDD.Core.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Threading;
using DDDD.Core.Resources;
using DDDD.Core.Patterns;

#if NET45
using System.Collections.Concurrent;
#endif


namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Type Set Serializer2(TSS).  Serializer personally. 
    /// <para/> Common use  workflow - at first we grouping our types into some Type Set:  1 -creating TypeSetSerializer2 and 2 -AddingType[s] to it - it means that this TypeSetSerializer2 will be able to [De]/Serialise only these custom known types.
    /// <para/> We manage by TypeSetSerializer2's collections by the following static methods :
    /// <para/>             Get(...), GetDirect(...), GetSafe(...), AddUpdateTypeSetSerializer(...), AddOrUseExistTypeSetSerializer(...), RemoveSerializer(...)
    /// <para/> And to [De]/Serialise:
    /// <para/>             we use Serialize(...), SerializeTPL(...), Deserialize(...), DeserializeTPL() , ContinueSerializeTPL(...), ContinueDeserializeTPL() 
    /// </summary>
    public class TypeSetSerializer2 :   ITypeSetSerializer2
    {

        #region ----------------------------- CTOR  ---------------------------

        TypeSetSerializer2()
        {
            Initialize();          
        }
 

        #endregion ----------------------------- CTOR  ---------------------------


        #region ---------------------------- FIELDS -----------------------

        static readonly object _locker = new object();

        #endregion ---------------------------- FIELDS -----------------------
        

        #region  --------------------------------------- INITIALIZATION -----------------------------------------
        
        private void Initialize()
        {

            //LoadDefaultTypeSerializersFromSerializeGenerators(this);
#if NET45
            //UseGenerationOfDebugCodeAssembly = false;
#endif
                  
        }


        #endregion --------------------------------------- INITIALIZATION -----------------------------------------
                       

        #region ---------------------------- TYPE  SERIALIZERS DICTIONARIES ---------------------------

#if NET45

        /// <summary>
        ///  Type Item Serializers - Dictionary[ID-ushort, ITypeserializer -Type item Serializer].
        /// </summary>
        static  ConcurrentDictionary<ushort, ITypeSerializer2> TypeSerializers
        { get; } = new ConcurrentDictionary<ushort, ITypeSerializer2>();


        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        /// </summary>
        static  ConcurrentDictionary<Type, ushort> TypeSerializerIDByType
        { get; } = new ConcurrentDictionary<Type, ushort>();

#else
        
        /// <summary>
        /// Main Type Serializers Collection of this TYPE SET SERIALIZER
        /// </summary>
        static Dictionary<ushort, ITypeSerializer2> TypeSerializers
        { get; } = new Dictionary<ushort, ITypeSerializer2>();


        /// <summary>
        /// Dictionary[Type | short] to get typeID by Type and  get Type by typeID 
        /// </summary>
        static Dictionary<Type, ushort> TypeSerializerIDByType
        { get; } = new Dictionary<Type, ushort>();

#endif





#endregion  ---------------------------- TYPE SERIALIZERS DICTIONARIES ---------------------------




        /// <summary>
        /// Generate new Type ID
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        internal static ushort GenerateNewTypeID(Type dataType)
        {
            if (TypeSerializerIDByType.ContainsKey(dataType)) return 0;

            return (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);            
        }




        #region --------------------------------- ADDING KNOWN TYPES -----------------------------------



        /// <summary>
        ///  Full  AddContract overload Creating TypeSerializer for different Contract Category value.
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="selector"></param>
        /// <param name="propertiesBinding"></param>
        /// <param name="fieldsBinding"></param>
        /// <param name="onlyMembers"></param>
        /// <param name="ignoreMembers"></param>
        /// <returns></returns>
        internal static ITypeSerializer2 AddKnownType(Type targetType

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {
            ushort? typeID = 0;
            //We cannot add  Contract Type that already exist
            if ((typeID = GetIfContainsContract(targetType)) != null) return  TypeSerializers[typeID.Value];

            _locker.LockAction(GetIfContainsContract(targetType).IsNotNull
            , () =>
            {
                //recursion can present inside this call if member is also still not known custom Type
                AddKnownTypeInternal(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);
            }
            );
            return  TypeSerializers[typeID.Value];

        }


        static void  AddKnownTypeInternal(Type targetType

                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default

                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                    , BindingFlags fieldsBinding = BindingFlags.Default

                                    , List<string> onlyMembers = null
                                    , List<string> ignoreMembers = null

                                    )
        {

          
            
               // if (  GetIfContainsContract(targetType).IsNotNull() ) return;// already exist

               ushort? typeID = 0;
                typeID = (ushort)((TypeSerializers.Count == 0) ? 1 : TypeSerializers.Count + 1);

                //if (targetType.IsAutoAdding())
                //{
                    // check that contract is really Auto Adding Contract
                    // enums/IList/IDictionary/Array and interfaces

                    // cannot be other than real AutoAdding Category Contract
                    if (targetType.IsNullable())
                    {
                        var typeSerializer = CreateTypeSerializer(targetType);
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //1 case concrete serialise /deserialize handlers - as primitives for example   - but initing- by handlers only after Added or Craeted of TypeSerializer 
                        //2 case auto adding custom nullable struct 

                    }
                // if this Type is  /Interface/Enum /Array that still not exist then add it with args checking and ADDDING TOO if also NOT EXIST
                 if (targetType.IsInterface)
                    {
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression


                    }
                    else if (targetType.IsEnum)
                    {

                        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression

                    }
                    else if (targetType.IsArray)
                    {
                        //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType);
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need to Generate  Handlers Expression                           

                    }
                    else if (typeof(IList).IsAssignableFrom(targetType))
                    {
                        //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        DetectAddAutoContractArgTypesFromCollection(targetType);

                        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);

                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression


                    }
                    else if (typeof(IDictionary).IsAssignableFrom(targetType))
                    {
                        //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        DetectAddAutoContractArgTypesFromCollection(targetType);//detect Key or Value Contract Arg

                        var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
                        AddItemToTypeSetDictionaries(typeSerializer);
                        //need no Policy and BuildMembers; but need  Generate  Handlers Expression
                    }
                }
                else if (targetType.Is4DPrimitiveType())
                {
                    // default contract adding: have  serializeHandler & deserializeHandler- need no to generate expressions                        
                    // can be Primitive contract                      

                    var typeSerializer = CreateTypeSerializer(targetType);//typeID.Value
                    // PRIMITIVE DEFAULT HANDLERS
                    //contractProcessor.SetDefaultSerializeHandler(serializeHandler);
                    //contractProcessor.SetDefaultDeserializeHandler(deserializeHandler);

                    AddItemToTypeSetDictionaries(typeSerializer);

                    //need no Policy and BuildMembers; need no Generate  Handlers Expression

                    // break;
                }
                else
                { //custom Contract  
                   

                    //mismath between OnlyMembersPolicy and  empty OnlyMembers array  
                    Validator.AssertTrue<InvalidOperationException>(
                                      (selector == TypeMemberSelectorEn.OnlyMembers && onlyMembers != null && onlyMembers.Count == 0),
                                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_NEED_ITEM_ERR"
                                                );


                    //mismath between not empty OnlyMembers array and incorrect selector
                    Validator.AssertTrue<InvalidOperationException>(
                                      (onlyMembers != null && onlyMembers.Count > 0 && selector != TypeMemberSelectorEn.OnlyMembers),
                                       "TSSResources.ServiceSerializer_ADD_CONTRACT_ONLY_MEMBERS_ITEMS_INCORRECT_POLICY_ERR"
                                                );


                    //policy of member selector mismatch with TypeMemberSelectorEn 
                    Validator.AssertTrue<InvalidOperationException>(
                             (propertiesBinding != BindingFlags.Default || fieldsBinding != BindingFlags.Default)
                          && (selector != TypeMemberSelectorEn.OnlyMembers && selector != TypeMemberSelectorEn.Custom),
                                 "Contract Add error: Current Processing Policy doesn't support custom Bindings for Properties or Fields."
                               + "Only ONLY_MEMBER and CUSTOM Porocessing policies support this feature. Look  at the  contract{0}"
                                           , targetType);


                    // By default static members mustn't be included into serialization
                    Validator.AssertTrue<InvalidOperationException>(
                                       propertiesBinding.HasFlag(BindingFlags.Static) || fieldsBinding.HasFlag(BindingFlags.Static),
                                       "Member Analyze error: Your Custom Binding cannot contains Static flag. Look  at the  contract{0}."
                                       , targetType);


                    var typeSerializer = CreateTypeSerializer(targetType, selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers);//typeID.Value
                    AddItemToTypeSetDictionaries(typeSerializer);

                    //collect MEmberTypes, and Check -if we have AutoAdding types within them
                    foreach (var memberType in typeSerializer.Accessor.CollectMemberTypes())
                    {
                        DetectAddAutoContractFull(memberType);
                    }



                    // if  type  - CustomStruct- is ValueType, we'll add it's nullable brother type- CustomStruct?.                        
                    if (targetType.IsValueType && targetType.IsNotNullable() && !targetType.IsEnum)
                    {
                        var contractNullableTwinBrother = typeof(Nullable<>).MakeGenericType(targetType);

                        AddKnownTypeInternal(
                              contractNullableTwinBrother
                             , selector
                             , propertiesBinding
                             , fieldsBinding
                             , onlyMembers
                            );

                    }

                    //collect auto items - list   break;

                   
                }

               
               
            
        }


        /// <summary>
        /// Add Contract to Type Set Serializer
        /// </summary>
        /// <param name="tp"></param>
        /// <returns></returns>
        internal static ITypeSerializer2 AddKnownType(Type tp)
        {
            return AddKnownType(tp, selector: TypeMemberSelectorEn.Default);
        }

        /// <summary>
        /// Add Contract to Type Set Serializer
        /// </summary>
        /// <typeparam name="T"></typeparam>
        internal static ITypeSerializer2<T> AddKnownType<T>()
        {
            return AddKnownType(typeof(T)) as ITypeSerializer2<T>;
        }



        /// <summary>
        /// Add several Known Types
        /// </summary>
        /// <param name="knownTypes"></param>
        internal static void AddKnownTypesTPL(params Type[] knownTypes)
        {
            for (int i = 0; i < knownTypes.Length; i++)
            {
                AddKnownType(knownTypes[i]);
            }

        }



       
        static ITypeSerializer2 CreateTypeSerializer(Type targetType
                                                    //, ITypeSetSerializer2 typeSetSerializer
                                                    , TypeMemberSelectorEn selector = TypeMemberSelectorEn.Default
                                                    , BindingFlags propertiesBinding = BindingFlags.Default
                                                    , BindingFlags fieldsBinding = BindingFlags.Default
                                                    , List<string> onlyMembers = null
                                                    , List<string> ignoreMembers = null
                                                    )
        {
            var createMethod = typeof(TypeSerializer2<>).MakeGenericType(targetType).GetMethod("Create");
            return createMethod.Invoke(null, new object[] { targetType,  selector, propertiesBinding, fieldsBinding, onlyMembers, ignoreMembers }) as ITypeSerializer2;
        }



        internal static  void AddItemToTypeSetDictionaries(ITypeSerializer2 tpSerializer)
        {
#if NET45
           TypeSerializers.TryAdd(tpSerializer.TypeID, tpSerializer);
           TypeSerializerIDByType.TryAdd(tpSerializer.TargetType, tpSerializer.TypeID);
#else
            TypeSerializers.Add(tpSerializer.TypeID, tpSerializer);
            TypeSerializerIDByType.Add(tpSerializer.TargetType, tpSerializer.TypeID);
#endif

        }

        #endregion --------------------------------- ADDING KNOWN TYPES -----------------------------------


        #region ---------------------------------------- DETECT ADD AUTOCONTARACTS -------------------------------------------

        static void DetectAddAutoContractFull(Type realTypeContract)
        {
            if (realTypeContract.Is4DPrimitiveType()) return;
             

            _locker.LockAction(() => GetIfContainsContract(realTypeContract).IsNotNull()
              , () =>
              {
                  //collection types with additional item types  
                  if (realTypeContract.IsICollectionType() || realTypeContract.IsInterface)
                  {
                      //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                      //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                      //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                      //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                      DetectAddAutoContractArgTypesFromCollection(realTypeContract);

                      AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]

                  }
                  else if (realTypeContract.IsEnum || realTypeContract.IsNullable())
                  {
                      AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]
                  }                  
                  else if (realTypeContract.IsClass || realTypeContract.IsValueType)
                  { // new as  Analyzed New Type
                      AddKnownTypeInternal(realTypeContract);
                  }
              }
              );
        }

        /// <summary>
        /// Auto add contract cases: Nullable struct | (Array) | (IList) | (IDictionary) | (Interface) | (Boxed Enum)
        /// </summary>       
        /// <param name="realTypeContract"></param>       
        static void DetectAddAutoContract(Type realTypeContract)
        {            
            _locker.LockAction(    GetIfContainsContract(realTypeContract).IsNotNull
                ,()=>
                {
                    //collection types with additional item types  
                    if (realTypeContract.IsICollectionType() || realTypeContract.IsInterface)
                    {
                        //if array - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                        //if IList - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception
                        //if IDictionary - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        //if Interface - arg type must contains in HostSerializer.ContractsDictionary/ else throw exception 
                        DetectAddAutoContractArgTypesFromCollection(realTypeContract);

                        AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]

                    }
                    else if (realTypeContract.IsEnum || realTypeContract.IsNullable())
                    {
                        AddKnownTypeInternal(realTypeContract); //add as [Auto Adding Category Contract]
                    }
                }
                );
           

        }



        /// <summary>
        /// Detecting if we need to add as [AUTO ADDING Category Contract] one/two of arg-types from collection Contract.
        /// </summary>
        /// <param name="collectionContract"></param>
         static void DetectAddAutoContractArgTypesFromCollection(Type collectionContract)
        {
            Type[] argTypes = Type.EmptyTypes;

            if (collectionContract.IsArray)
            {
                argTypes = new[] { collectionContract.GetElementType() };
            }
            else if (collectionContract.IsGenericType)
            {
                argTypes = collectionContract.GetGenericArguments();
            }

            if (argTypes == null) return;//error?? 

            foreach (var itemArgType in argTypes)
            {
                ushort? itemArgTypeId = GetIfContainsContract(itemArgType);
                if (itemArgTypeId != null) continue;//already contains

                if (itemArgType.IsInterface)
                {

                    AddKnownTypeInternal(itemArgType,   selector: TypeMemberSelectorEn.Default   );

                }
                else if (itemArgType.IsEnum)
                {
                    AddKnownTypeInternal(itemArgType,   selector: TypeMemberSelectorEn.Default   );

                }
                else
                {
                    Validator.AssertTrue<InvalidOperationException>(itemArgTypeId == 0,
                        "DetectAddAutoContractArgTypesFromCollection(): Searching In Collection [{0}] for arg type [{1}] : Result - NOT DETERMINED. So TypeSerializer  for such Contract  unable to be load to use from Serializer Contract Dictionary."
                       , collectionContract, itemArgType);

                }

            }

        }
        

#endregion ---------------------------------------- DETECT ADD AUTOCONTARACTS -------------------------------------------


        #region ------------------------------ GET RUNTIME TYPE ModelId -------------------------------


        /// <summary>
        ///  Get Type of data object and return typeID of this Type
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        internal static ushort GetTypeID(object data)
        {
            ushort id;

            if (data.IsNull())  return 0;

            var type = data.GetType();

            Validator.AssertFalse<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(type, out id),
                                                                string.Format("Unknown type {0}", type.FullName)
                                                            );

            return id;
        }


        /// <summary>
        /// Get  typeID of this dataType
        /// </summary>
        /// <param name="dataType"></param>
        /// <returns></returns>
        internal static ushort GetTypeID(Type dataType)
        {
            ushort id;

            Validator.AssertFalse<InvalidOperationException>(TypeSerializerIDByType.TryGetValue(dataType, out id),
                                                                string.Format("Unknown type {0}", dataType.FullName)
                                                            );

            return id;
        }


        /// <summary>
        /// Getting  Type from runtime type ModelId
        /// If type doesn't exist in Serializer.ContractsDictionry then ContractNotFoundException will be thrown
        /// </summary>
        /// <param name="typeID"></param>
        /// <returns></returns>
        internal static Type GetTypeByTypeID(ushort typeID)
        {
            object findedItem = TypeSerializerIDByType.Where(itm => itm.Value == typeID).FirstOrDefault();

            Validator.AssertTrue<InvalidOperationException>(findedItem == null,
                                                             " Cannot find Type by runtime type ModelId - [{0}]",
                                                             typeID.S()
                                                           );

            return ((KeyValuePair<Type, ushort>)findedItem).Key;


        }


#endregion ------------------------------ GET RUNTIME TYPE ModelId -------------------------------        


        #region --------------------------------- PLAIN STYLE DE/SERIALIZE -------------------------------------

        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static byte[] Serialize(object data, bool isNullable = false)
        {
            byte[] result = null;
            using (var ms = new MemoryStream())
            {
                Serialize(ms, data, isNullable);
                result = ms.ToArray();
            }
            return result;
        }

        /// <summary>
        /// Serialize object data into stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        /// <param name="isNullable"></param>
        public static void Serialize(Stream stream, object data, bool isNullable = false)
        {
            if (stream == null || data == null) return;

            var realType =(isNullable)? typeof(Nullable<>).MakeGenericType( data.GetType()) : data.GetType() ;

            if (GetIfContainsContract(realType).IsNull())
            {
                AddKnownType(realType);//thread-safe action
            }

            ushort typeID = GetTypeID(realType);
           
            if (typeID > 0) //already contains need no to DetectAddAutoContract
            {
                if (realType.Is4DPrimitiveType()|| realType.IsEnum)
                {
                    //write typeID  here only for primitives - because primitives don't write itself TypeID directly
                    BinaryReaderWriter.Write(stream, typeID);
                    TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);
                    
                }
                else
                {
                    TypeSerializers[typeID].RaiseSerializeToStreamBoxedHandler(stream, data);                    
                }

            }
            else if (typeID == 0) return; //null value
             

        }


        /// <summary>
        /// Serialize object data into  stream.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        public static void SerializeToStream(object value,  Stream stream)
        {
            Serialize( stream , value );
        }


        /// <summary>
        /// Serialize {T} object data into  stream.
        /// </summary>
        /// <typeparam name="T"> type of value </typeparam>
        /// <param name="value"> value </param>
        /// <param name="stream"> stream  </param>
        public static void SerializeToStream<T>(T value, Stream stream)
        {
            Serialize( stream , value );
        }



        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static byte[] SerializeToByteArray(object value)
        {
           return Serialize(value);
        }

       




        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object Deserialize(Stream stream)
        {
            if (stream == null) return null;        

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID
                        
            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>( 
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);

        }


        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T Deserialize<T>(Stream stream)
        {
            if (stream == null) return default(T);                     

            ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(stream);//read TypeID
            
            //check if typeID exist in Dictionary 
            Validator.AssertFalse<InvalidOperationException>(
                                        TypeSerializers.ContainsKey(typeID)
                                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                                        , typeID.S()
                                        );


            return (T)TypeSerializers[typeID].RaiseDeserializeFromStreamBoxedHandler(stream, true);


            //return (T)Deserialize(stream);
        }


        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static T Deserialize<T>(byte[] serializedData)
        {
            T result = default(T);
            using (var ms = new MemoryStream(serializedData))
            {
                result = Deserialize<T>(ms);
            }
            return result;
        }


        /// <summary>
        /// Deserialize  to object  from byte[].
        /// </summary>
        /// <param name="serializedData"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static object Deserialize(byte[] serializedData, Type targetType)
        {            
            using (var ms = new MemoryStream(serializedData))
            {
               return Deserialize(ms);
            }            
        }


        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static object DeserializeFromStream(Type type, Stream stream)
        {
           return Deserialize(stream);
        }

      

        /// <summary>
        ///  Deserialize from Stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static T DeserializeFromStream<T>(Stream stream)
        {
            return Deserialize<T>(stream);
        }


        /// <summary>
        /// Deserialize  to object  from byte[].
        /// </summary>
        /// <param name="dataArray"></param>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static object DeserializeFromByteArray(byte[] dataArray, Type targetType)
        {
            return Deserialize(dataArray, targetType);
        }

        #endregion ---------------------------------PLAIN  STYLE DE/SERIALIZE -------------------------------------


        #region ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------------------ SERIALIZE TPL ------------------------------------------


        /// <summary>
        ///  Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data)
        {
            return SerializeTPL(data, default(CancellationToken), TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Serialize object data into byte[].
        /// </summary>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="taskCretionOptions"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<byte[]> SerializeTPL(object data, CancellationToken cancelToken, TaskCreationOptions taskCretionOptions, TaskScheduler scheduler)
        {         
            return Task.Factory.StartNew<byte[]>((st) =>
            {
                var inSt = st as  object;

                if (inSt.IsNull()) return null;

                return Serialize(inSt);               

            },
            data,
            cancelToken,
            taskCretionOptions,
            scheduler
            );

        }



        /// <summary>
        /// Serialize object data into binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="data"></param>
        public static Task SerializeTPL(Stream stream, object data)
        {
            var state = ThreadCallState<Stream, object>.Create(stream, data);

            return Task.Factory.StartNew((st) =>
            {
                //Item1- TSSerializer
                //Item2 - stream
                //Item3 - data
                var inSt = st as ThreadCallState<Stream, object>;

                if (inSt.Item1 == null || inSt.Item2 == null) return;

                var realType = inSt.Item2.GetType();
                
                Serialize(inSt.Item1, inSt.Item2);
            }
            ,
            state,
            TaskCreationOptions.AttachedToParent
            );

        }



#endregion ------------------------------------------ SERIALIZE TPL ------------------------------------------



#region ------------------------------------- ContinueSerializeTPL ------------------------------------------

#if NET45 || WP81
        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream  
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data)
        {
            return ContinueSerializeTPL(streamTask, data, TaskContinuationOptions.AttachedToParent);
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>     
        /// <param name="continueAsOption"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, TaskContinuationOptions continueAsOption)
        {       
            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as  object;

                if (strm == null || inSt == null) return;

                var realType = inSt.GetType();

                Serialize(strm, inSt);
            }
            , data
            , continueAsOption
            );
        }

        /// <summary>
        /// Continue Task_Stream_ and serialize   data object into this Stream.  We may change continuation Options, and add CancellationToken.
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="data"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continueAsOption"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task ContinueSerializeTPL(Task<Stream> streamTask, object data, CancellationToken cancelToken, TaskContinuationOptions continueAsOption, TaskScheduler scheduler)
        {
            return streamTask.ContinueWith((t, st) =>
            {
                // //Item1- TSSerializer
                //Item2 -data
                var strm = t.Result;
                var inSt = st as  object;

                if (strm == null || inSt == null) return;

                var realType = inSt.GetType();

                Serialize(strm, inSt);   
            }
            , data
            , cancelToken
            , continueAsOption
            , scheduler
            );
        }

#endif

#endregion ------------------------------------- ContinueSerializeTPL ------------------------------------------



#region ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------

        /// <summary>
        /// Deserialize to object  from binary stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Task<object> DeserializeTPL(Stream stream)
        {            
            return Task.Factory.StartNew<object>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as Stream;

                if (inSt == null) return null;
                              
                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case      

                //check if typeID exist in Dictionary 
                Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                    , typeID.S());

                return GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(inSt, true);
            }
                ,
                stream
                , TaskCreationOptions.AttachedToParent
                );

        }

        /// <summary>
        /// Deserialize  to T-object  from binary stream.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stream"></param>
        /// <returns></returns>
        public  static  Task<T> DeserializeTPL<T>(Stream stream)
        {           
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as Stream;

                if (inSt == null) return default(T);

                ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(inSt);//read TypeID

                //if (typeID == 0) // null value type and value - impossible case                    

                //check if typeID exist in Dictionary 
                Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                    , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                    , typeID.S());

                return (T)GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(inSt, true);

            }
            ,
            stream
            , TaskCreationOptions.AttachedToParent
            );
        }



        #endregion ------------------------------------- DESERIALIZE TPL  FROM STREAM -----------------------------------------



#region ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------

#if NET45 || WP81

        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }



        /// <summary>
        ///  Continue Deserialize from Task_Stream_
        /// </summary>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<object> ContinueDeserializeTPL(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<object>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 //var serializer = st as ITypeSetSerializer2;


                 var strm = t.Result;

                 if (strm == null) return null;

                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                     , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                     , typeID.S());

                 return GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(strm, true);
             }
            , null
            , cancelToken // 
            , continuation
            , scheduler
            );

        }



        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask)
        {
            return ContinueDeserializeTPL<T>(streamTask, default(CancellationToken), TaskContinuationOptions.AttachedToParent, TaskScheduler.Default);
        }


        /// <summary>
        /// Continue Deserialize from Task_Stream_
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="streamTask"></param>
        /// <param name="cancelToken"></param>
        /// <param name="continuation"></param>
        /// <param name="scheduler"></param>
        /// <returns></returns>
        public static Task<T> ContinueDeserializeTPL<T>(Task<Stream> streamTask, CancellationToken cancelToken, TaskContinuationOptions continuation, TaskScheduler scheduler)
        {
            return
             streamTask.ContinueWith<T>((t, st) =>
             {
                 //Item1 - TSSerializer
                 //Item2 - Stream
                 //var serializer = st as ITypeSetSerializer2;
                 
                 var strm = t.Result;

                 if (strm == null) return default(T);
                       
                 ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(strm);//read TypeID

                 //if (typeID == 0) // null value type and value - impossible case      

                 //check if typeID exist in Dictionary 
                 Validator.AssertFalse<InvalidOperationException>(ContainsTypeSerializer(typeID)
                     , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary"
                     , typeID.S());

                 return (T)GetTypeSerializer(typeID).RaiseDeserializeFromStreamBoxedHandler(strm, true);
             }
            , null
            , cancelToken // 
            , continuation
            , scheduler
            );

        }


#endif

#endregion ----------------------------------------- CONTINUE DESERIALIZE FROM STREAM TASK TPL ---------------------------------------------



#region ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------

        /// <summary>
        ///  Deserialize  to T-object  from byte[].
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedData"></param>
        /// <returns></returns>
        public static Task<T> DeserializeTPL<T>(byte[] serializedData)
        {
            //var state = ThreadCallState<ITypeSetSerializer2, byte[]>.Create(Current, serializedData);
            return Task.Factory.StartNew<T>((st) =>
            {
                //Item1 - TSSerializer
                //Item2 - Stream
                var inSt = st as  byte[];

                if (inSt.IsNotWorkable()) return default(T);
                
                using (var ms = new MemoryStream(inSt))
                {
                    ushort typeID = (ushort)BinaryReaderWriter.Read_uint16(ms);//read TypeID

                    //if (typeID == 0) // null value type and value - impossible case                    

                    //check if typeID exist in Dictionary   InvalidOperationException
                    Validator.AssertFalse<InvalidOperationException>( ContainsTypeSerializer(typeID) // TypeSerializers.ContainsKey
                        , "Deserialize error: typeID [{0}]  was not found in Processings Dictionary".Fmt(typeID.S()));

                    return ( GetTypeSerializer(typeID) as ITypeSerializer2<T>).RaiseDeserializeFromStreamTHandler( ms, true); //TypeSerializers
                }
            }
            ,
             serializedData
            , TaskCreationOptions.AttachedToParent
            );

        }

#endregion ----------------------------------- DESERIALIZE FROM BYTE[] -----------------------------------------



#endregion ---------------------------------TPL  DE/SERIALIZE DEFENITION ------------------------------


        #region ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------

#if NET45 /////// --------------------------- WRITE DEBUG CODE ASSEMBLY --------------------------------///////


        /// <summary>
        /// If this flag is true, after each BuildProcessing() call, we'll write debugging code assembly.
        /// By Default it's false. But this feature can't be used in real-time parallel work, be carefull.
        /// </summary>
        public static bool UseGenerationOfDebugCodeAssembly { get; set; }

      


#endif
#endregion ------------------------------- WRITE DEBUG CODE ASSEMBLY ----------------------------------
        

        #region  -----------------------------------TYPESERIALIZER GENERATORS -------------------------------------------


        /// <summary>
        /// It means first  Generator that  can  generate type Handlers will be used. 
        /// <para/> i.e. last added custom generator will be used first to generate type Handlers from it's Type Kind -because it'll be first item from top of the stack.
        /// </summary>
        static LazyAct<List<ITypeSerializerGenerator2> > LA_SerializeGenerators = LazyAct< List<ITypeSerializerGenerator2> >.Create( 
            (args)=>
            {
                //Loading all existed type De/Serialzie Generators 
                List<ITypeSerializerGenerator2> generatorsResult = new List<ITypeSerializerGenerator2>();

                var generatorTypes = typeof(ITypeSerializerGenerator2).Assembly.GetTypes()
                          .Where(tp => typeof(ITypeSerializerGenerator2).IsAssignableFrom(tp) && tp.IsClass && !tp.IsAbstract).ToList();

                if (generatorTypes.Count == 0) return generatorsResult;

                foreach (var genType in generatorTypes)
                {
                    var generator = TypeActivator.CreateInstanceTBaseLazy<ITypeSerializerGenerator2>(genType);
                    generatorsResult.Add(generator);
                }
                return generatorsResult;
            }
            , null//locker
            , null //args
            );




        internal static void LoadDefaultTypeSerializersFromSerializeGenerators(ITypeSetSerializer2 serializer)
        {
            string currentGeneratorName = "";
            try
            {
                foreach (var genrtr in LA_SerializeGenerators.Value)
                {
                    currentGeneratorName = genrtr.GetType().FullName;
                    if (genrtr.CanLoadDefaultTypeSerializers)
                    {
                        genrtr.LoadDefaultTypeSerializers();
                    }
                }
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(
                    $" {nameof(TypeSetSerializer2)}.{nameof(LoadDefaultTypeSerializersFromSerializeGenerators)}(): ERROR on loading Default TypeSerializers from  [{currentGeneratorName}] - Message:[{exc.Message}] ");
            }            
        }




        /// <summary>
        /// Get Type Serialize Generator by targetType 
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        internal static ITypeSerializerGenerator2 GetTypeSerializerGeneratorByType(Type targetType)
        {
            return LA_SerializeGenerators.Value.Where(sg => sg.CanHandle(targetType)).FirstOrDefault();
        }


        /// <summary>
        ///  Check if Contract exist in Serializer Dictionary. 
        /// </summary>
        /// <param name="fieldType"></param>
        /// <returns></returns>
        internal static ushort? GetIfContainsContract(Type type)
        {
            if (type == null) return null;

            if (TypeSerializerIDByType.ContainsKey(type))
            {
                return TypeSerializerIDByType[type];
            }
            return null;
        }
        

        /// <summary>
        ///  Check if current TSS instance can serialize  Type - targetType.
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool CanSerialize(Type targetType)
        {
            // 1 if ICollection  -Array  get ArrayElementType 
            // 2 if ICollection  -List get   Arg1Type 
            // 3 if ICollection  -Dictionary get  Arg1Type + Arg2Type            
            // 4 simple Test- Contains in Dictionary

            if (targetType.IsArray)
            {
                return CanSerialize(targetType.GetElementType());
            }
            else if (TypeInfoEx.IsImplement_List(targetType))
            {
                return CanSerialize(targetType.GetGenericArguments()[0]);
            }
            else if (TypeInfoEx.IsImplement_Dictionary(targetType))
            {
                return (CanSerialize(targetType.GetGenericArguments()[0])
                     && CanSerialize(targetType.GetGenericArguments()[1]));
            }

            else if (targetType.IsAutoAdding()) { return true; }

            // it's nullable or usual struct or class type - all of them should already contained in TypeSerializerIDByType
            else return TypeSerializerIDByType.ContainsKey(targetType);
            
        }


        /// <summary>
        /// Check if current TSS instance can serialize  Type - typeof(T).
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool CanSerialize<T>()
        {
            return CanSerialize(typeof(T));
        }


        /// <summary>
        ///  Check if current TSS instance can serialize Type of data instance - [dataItem.GetType()].
        /// </summary>
        /// <param name="dataItem"></param>
        /// <param name="throwOnNullValue"></param>
        /// <returns></returns>
        public static bool CanSerialize(object dataItem, bool throwOnNullValue )
        {
            if (dataItem == null && throwOnNullValue)
                throw new InvalidOperationException($" ERROR in {nameof(TypeSetSerializer2)}.{nameof(CanSerialize)}() : dataItem can't be null to determine serialization possibilities ");
            else if (dataItem == null) return false;

            return CanSerialize(dataItem.GetType());
        }


        #endregion -----------------------------------TYPESERIALIZER GENERATORS -------------------------------------------


        #region ------------------------ ITypeSetSerialier2 ------------------------------


      

        internal static bool ContainsTypeSerializer(ushort typeID)
        {
            return TypeSerializers.ContainsKey(typeID);
        }

        internal static ITypeSerializer2 GetTypeSerializer(ushort typeID)
        {
            return TypeSerializers[typeID];//    Current.GetTypeSerializer(typeID);
        }


        #endregion ------------------------ ITypeSetSerialier2 ------------------------------




    }
}



