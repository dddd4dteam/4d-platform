﻿using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.Net;
using System;
using System.IO;

namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Serialization Delegates Defenitions 
    /// </summary>
    public static class SD2
    {


#if NET45 && DEBUG

        //Debug Serialize:   Action<Stream, T, TypeProcessor<T>>
        //Debug Deserialize: Func<Stream, bool, TypeProcessor<T>, object>

        //public delegate void DebugSerialize<T>(Stream stream, T instance, ITypeSerializer<T> tpSerializer);
        //public delegate object DebugDeserialize<T>(Stream stream, bool readedTypeID = false, ITypeSerializer<T> tpSerializer = null);        


        #region --------------------------------- [DEBUG] -  T BASED DELEGATES   ----------------------------------

        //[Conditional("DEBUG")]
        public delegate void SerializeToStreamTDbg<T>(Stream stream, T instance, ITypeSerializer2<T> tpSerializer);
        public delegate T DeserializeFromStreamTDbg<T>(Stream stream, bool readedTypeID = false, ITypeSerializer2<T> tpSerializer = null);


        public delegate void SerializeToBinWriterTDbg<T>(BinaryWriter bWriter, T instance, ITypeSerializer2<T> tpSerializer);
        public delegate T DeserializeFromBinReaderTDbg<T>(BinaryReader bReader, bool readedTypeID = false, ITypeSerializer2<T> tpSerializer= null);


        #endregion --------------------------------- [DEBUG] -  T BASED DELEGATES   ----------------------------------


#endif



        #region --------------------------  OBJECT BASED DELEGATES ---------------------------------------

        public delegate void SerializeToStream(object instance, Stream stream);
        public delegate object DeserializeFromStream(Type type, Stream stream); // Stream stream, bool readedTypeID = false

        public delegate string SerializeToString( object instance );
        public delegate object DeserializeFromString(string serializedData, Type type );

        public delegate byte[] SerializeToByteArray(object instance);
        public delegate object DeserializeFromByteArray(byte[] serializedData, Type type );
        
        public delegate void SerializeToBinWriter(BinaryWriter bWriter, object instance);
        public delegate object DeserializeFromBinReader(BinaryReader bReader, bool readedTypeID = false);
        
        #endregion --------------------------  OBJECT BASED DELEGATES ---------------------------------------



        #region  ---------------------------------   T BASED DELEGATES ----------------------------------

        public delegate void SerializeToStreamT<T>(Stream stream, T instance);
        public delegate T DeserializeFromStreamT<T>(Stream stream, bool readedTypeID = false);
        

        public delegate void SerializeToBinWriterT<T>(BinaryWriter bWriter, T instance);
        public delegate T DeserializeFromBinReaderT<T>(BinaryReader bReader, bool readedTypeID = false);


        #endregion ---------------------------------   T BASED DELEGATES ----------------------------------



        #region ----------------------------- DCMessage BASED DELEGATES --------------------------------------
        
        public delegate void SerializeMsgToStream(Stream stream, DCMessage message);

        public delegate DCMessage DeserializeMsgFromStream(Stream stream, bool readedTypeID = false);
        
        #endregion ----------------------------- DCMessage BASED DELEGATES --------------------------------------


        //public delegate void PackMessage(DCMessage message);
        //public delegate void UnpackMessage(ref DCMessage message);


        //public delegate void PackMessage2(DCMessage2 message);
        //public delegate void UnpackMessage2(ref DCMessage2 message);



    }
}
