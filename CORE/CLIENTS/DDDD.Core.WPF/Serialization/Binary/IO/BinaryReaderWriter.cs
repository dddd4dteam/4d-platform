﻿/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Collections;

using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;

namespace DDDD.Core.IO
{




    /// <summary>
    /// Reader/Writer for Primitive types: 
    /// <para/>   Not Nullable structs:  int,  uint,  long,  ulong,  short,  ushort,  byte,  sbyte,  char,  bool,  
    /// <para/>                          DateTime,  TimeSpan, Guid,  float, double, decimal        
    /// <para/>   Nullable structs:      int?, uint?, long?, ulong? short?, ushort?,  byte?, sbyte?, char?, bool?, 
    /// <para/>                          DateTime?, TimeSpan?, Guid?, float?, double?, decimal?    
    /// <para/>   Classes:               string, byte[], Uri, BitArray   
    /// <para/>   Reflection             TypeInfoEx 
    /// </summary>
    public static class BinaryReaderWriter
    {
        #region ------------------------------- CTOR ----------------------------------

        static BinaryReaderWriter()
        {
            InitPrimitiveReadingWritingMethods();
        }

        #endregion ------------------------------- CTOR ----------------------------------


        #region ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------

        static uint EncodeZigZag32(int n)
        {
            return (uint)((n << 1) ^ (n >> 31));
        }

        static ulong EncodeZigZag64(long n)
        {
            return (ulong)((n << 1) ^ (n >> 63));
        }

        static int DecodeZigZag32(uint n)
        {
            return (int)(n >> 1) ^ -(int)(n & 1);
        }

        static long DecodeZigZag64(ulong n)
        {
            return (long)(n >> 1) ^ -(long)(n & 1);
        }

        static uint ReadVarint32(Stream stream)
        {
            int result = 0;
            int offset = 0;

            for (; offset < 32; offset += 7)
            {
                int b = stream.ReadByte();
                if (b == -1)
                    throw new EndOfStreamException();

                result |= (b & 0x7f) << offset;

                if ((b & 0x80) == 0)
                    return (uint)result;
            }

            throw new InvalidOperationException();
        }

        static void WriteVarint32(Stream stream, uint value)
        {
            for (; value >= 0x80u; value >>= 7)
                stream.WriteByte((byte)(value | 0x80u));

            stream.WriteByte((byte)value);
        }

        static ulong ReadVarint64(Stream stream)
        {
            long result = 0;
            int offset = 0;

            for (; offset < 64; offset += 7)
            {
                int b = stream.ReadByte();
                if (b == -1)
                    throw new EndOfStreamException();

                result |= ((long)(b & 0x7f)) << offset;

                if ((b & 0x80) == 0)
                    return (ulong)result;
            }

            throw new InvalidOperationException();
        }

        static void WriteVarint64(Stream stream, ulong value)
        {
            for (; value >= 0x80u; value >>= 7)
                stream.WriteByte((byte)(value | 0x80u));

            stream.WriteByte((byte)value);
        }



        #endregion ----------------------------- ATOMIC BYTE  OPERATIONS ------------------------------


        #region --------------------- NULLABLE READ/WRITE WRAPS --------------------------

        static void WriteNullableAction<T>(Stream stream, T? value, Action<Stream, T?> action) where T : struct
        {
            if (!value.HasValue)
            {
                Write(stream, false); //HasValue == false
            }
            else
            {
                Write(stream, true); //HasValue

                action(stream, value);
            }
        }

        static object ReadNullableAction<T>(Stream stream, Func<Stream, T?> function) where T : struct
        {
            bool HasValue = (bool)Read_bool(stream);//, readedTypeID); //HasValue == false

            if (HasValue == true) //HasValue
            {
                return function(stream);  //read T value and return it             
            }
            else //Has not value return null;
            {
                return null;
            }
        }

        static T? ReadNullableTAction<T>(Stream stream, Func<Stream, T?> function) where T : struct
        {
            bool HasValue = (bool)Read_bool(stream);//, readedTypeID); //HasValue == false

            if (HasValue == true) //HasValue
            {
                return function(stream);  //read T value and return it             
            }
            else //Has not value return null;
            {
                return null;
            }
        }


        #endregion --------------------- NULLABLE READ/WRITE WRAPS --------------------------



        #region ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------

        static Dictionary<Type, MethodInfo> WritingPrimitiveMethod = new Dictionary<Type, MethodInfo>();
        static Dictionary<Type, MethodInfo> ReadingPrimitiveMethod = new Dictionary<Type, MethodInfo>();

   

        static void InitPrimitiveReadingWritingMethods()
        {
            OperationInvoke.CallInMode(nameof(BinaryReaderWriter), nameof(InitPrimitiveReadingWritingMethods)
                ,() =>
                {
                    foreach (var primitiveType in TypeInfoEx.PrimitiveTypes)
                    {
                        //writing method 

                        //
                        Type realType = primitiveType.GetWorkingTypeFromNullableType();
                        bool IsNullableType = primitiveType.IsNullable();

                        string writeMethodName = "WriteT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");
                        var writePrimitiveMethod = typeof(BinaryReaderWriter).GetMethod(writeMethodName, new[] { typeof(Stream), primitiveType });
                        WritingPrimitiveMethod.Add(primitiveType, writePrimitiveMethod);



                        string readMethodName = "ReadT" + "_" + realType.Name.ToLower()
                            + ((IsNullableType) ? "_nullable" : "");

                        var readPrimitiveMethod = typeof(BinaryReaderWriter).GetMethod(readMethodName, BindingFlags.Static | BindingFlags.Public);

                        ReadingPrimitiveMethod.Add(primitiveType, readPrimitiveMethod);
                    }

                });           

        }

        public static MethodInfo GetWritePrimitiveMethod(Type primitiveType)
        {
            return WritingPrimitiveMethod[primitiveType];
        }

        public static MethodInfo GetReadPrimitiveMethod(Type primitiveType)
        {
            return ReadingPrimitiveMethod[primitiveType];
        }

        //public static List<Type> GetRegisteredPrimitives()
        //{
        //return TypeInfoEx.PrimitiveTypes;
        //}

        //public static bool IsPrimitiveType(Type type)
        //{
        //    return TypeInfoEx.PrimitiveTypes.Contains(type);
        //}

        #endregion ----------------------------- READING / WRITING PRIMITIVES DICTIONARIES ------------------------------------


        #region ---------------------------- READ/Write short-int16 ----------------------------------


        public static void Write(Stream stream, short value)
        {
            WriteVarint32(stream, EncodeZigZag32(value));
        }


        public static void WriteT_int16(Stream stream, short value)
        {
            WriteVarint32(stream, EncodeZigZag32(value));
        }



        public static object Read_int16(Stream stream, bool readedTypeID = false)
        {
            return (short)DecodeZigZag32(ReadVarint32(stream)); //(short)
        }

        public static short ReadT_int16(Stream stream, bool readedTypeID = false)
        {
            return (short)DecodeZigZag32(ReadVarint32(stream)); //(short)
        }

        #endregion ---------------------------- READ/Write short-int16----------------------------------


        #region ---------------------------- READ/Write ushort-uint16 ----------------------------------


        public static void Write(Stream stream, ushort value)
        {
            WriteVarint32(stream, value);
        }

        public static void WriteT_uint16(Stream stream, ushort value)
        {
            WriteVarint32(stream, value);
        }


        public static object Read_uint16(Stream stream, bool readedTypeID = false)
        {
            return (ushort)ReadVarint32(stream);
        }

        public static ushort ReadT_uint16(Stream stream, bool readedTypeID = false)
        {
            return (ushort)ReadVarint32(stream);
        }

        #endregion ---------------------------- READ/Write ushort-uint16 ----------------------------------


        #region ---------------------------- READ/Write int32----------------------------------

        public static void Write(Stream stream, int value)
        {
            WriteVarint32(stream, EncodeZigZag32(value));
        }


        public static void WriteT_int32(Stream stream, int value)
        {
            WriteVarint32(stream, EncodeZigZag32(value));
        }


        public static object Read_int32(Stream stream, bool readedTypeID = false)
        {
            return DecodeZigZag32(ReadVarint32(stream));
        }

        public static int ReadT_int32(Stream stream, bool readedTypeID = false)
        {
            return DecodeZigZag32(ReadVarint32(stream));
        }
        #endregion ---------------------------- READ/Write int32----------------------------------


        #region ---------------------------- READ/Write uint32----------------------------------

        public static void Write(Stream stream, uint value)
        {
            WriteVarint32(stream, value);
        }


        public static void WriteT_uint32(Stream stream, uint value)
        {
            WriteVarint32(stream, value);
        }

        public static object Read_uint32(Stream stream, bool readedTypeID = false)
        {
            return ReadVarint32(stream);
        }


        public static uint ReadT_uint32(Stream stream, bool readedTypeID = false)
        {
            return ReadVarint32(stream);
        }


        #endregion ---------------------------- READ/Write uint32----------------------------------


        #region ---------------------------- READ/Write long-int64 ----------------------------------

        public static void Write(Stream stream, long value)
        {
            WriteVarint64(stream, EncodeZigZag64(value));
        }

        public static void WriteT_long(Stream stream, long value)
        {
            WriteVarint64(stream, EncodeZigZag64(value));
        }

        public static object Read_int64(Stream stream, bool readedTypeID = false)
        {
            return DecodeZigZag64(ReadVarint64(stream));
        }

        public static long ReadT_int64(Stream stream, bool readedTypeID = false)
        {
            return DecodeZigZag64(ReadVarint64(stream));
        }


        #endregion ---------------------------- READ/Write long-int64 ----------------------------------


        #region ---------------------------- READ/Write ulong-uint64 ----------------------------------

        public static void Write(Stream stream, ulong value)
        {
            WriteVarint64(stream, value);
        }


        public static void WriteT_ulong(Stream stream, ulong value)
        {
            WriteVarint64(stream, value);
        }


        public static object Read_uint64(Stream stream, bool readedTypeID = false)
        {
            return ReadVarint64(stream);
        }

        public static ulong ReadT_uint64(Stream stream, bool readedTypeID = false)
        {
            return ReadVarint64(stream);
        }

        #endregion ---------------------------- READ/Write ulong-uint64----------------------------------


        #region ---------------------------- READ/Write byte----------------------------------

        public static void Write(Stream stream, byte value)
        {
            stream.WriteByte(value);
        }


        public static void WriteT_byte(Stream stream, byte value)
        {
            stream.WriteByte(value);
        }



        public static object Read_byte(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (byte)stream.ReadByte();
        }

        public static byte ReadT_byte(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (byte)stream.ReadByte();
        }


        #endregion ---------------------------- READ/Write byte----------------------------------


        #region ---------------------------- READ/Write sbyte----------------------------------

        public static void Write(Stream stream, sbyte value)
        {
            stream.WriteByte((byte)value);
        }


        public static void WriteT_sbyte(Stream stream, sbyte value)
        {
            stream.WriteByte((byte)value);
        }


        public static object Read_sbyte(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (sbyte)stream.ReadByte();
        }

        public static sbyte ReadT_sbyte(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (sbyte)stream.ReadByte();
        }
        #endregion ---------------------------- READ/Write sbyte----------------------------------


        #region ---------------------------- READ/Write char----------------------------------

        public static void Write(Stream stream, char value)
        {
            WriteVarint32(stream, value);
        }

        public static void WriteT_char(Stream stream, char value)
        {
            WriteVarint32(stream, value);
        }

        public static object Read_char(Stream stream, bool readedTypeID = false)
        {
            return (char)ReadVarint32(stream);
        }

        public static char ReadT_char(Stream stream, bool readedTypeID = false)
        {
            return (char)ReadVarint32(stream);
        }

        #endregion ---------------------------- READ/Write char----------------------------------


        #region ---------------------------- READ/Write bool----------------------------------

        public static void Write(Stream stream, bool value)
        {
            stream.WriteByte(value ? (byte)1 : (byte)0);
        }


        public static void WriteT_bool(Stream stream, bool value)
        {
            stream.WriteByte(value ? (byte)1 : (byte)0);
        }


        public static object Read_bool(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (stream.ReadByte() != 0);//(bool)
        }

        public static bool ReadT_bool(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return (stream.ReadByte() != 0);//(bool)
        }

        #endregion ---------------------------- READ/Write bool----------------------------------


        #region ---------------------------- READ/Write DateTime----------------------------------

        public static void Write(Stream stream, DateTime value)
        {
            long v = value.Ticks;//ToBinary();
            Write(stream, v);
        }



        public static void WriteT_DateTime(Stream stream, DateTime value)
        {
            long v = value.Ticks;//ToBinary();
            Write(stream, v);
        }



        public static object Read_DateTime(Stream stream, bool readedTypeID = false)
        {
            long v = (long)Read_int64(stream);// default(long); 

            return DateTime.FromBinary(v);
        }


        public static DateTime ReadT_DateTime(Stream stream, bool readedTypeID = false)
        {
            long v = (long)Read_int64(stream);// default(long); 

            return DateTime.FromBinary(v);
        }

        #endregion ---------------------------- READ/Write  DateTime----------------------------------


        #region ------------------------------ READ/WRITE deciaml -----------------------------------

        public static void Write(Stream stream, decimal value)
        {
            int[] parts = Decimal.GetBits(value);
            foreach (int item in parts)
            {
                Write(stream, item);                //STask.Write(item);
            }

        }


        public static void WriteT_decimal(Stream stream, decimal value)
        {
            int[] parts = decimal.GetBits(value);
            foreach (int item in parts)
            {
                Write(stream, item);                //STask.Write(item);
            }

        }

        public static object Read_decimal(Stream stream, bool readedTypeID = false)
        {

            int[] parts = new int[4];
            for (int i = 0; i < 4; ++i)
            {
                parts[i] = (int)Read_int32(stream);
            }
            bool isNegative = ((parts[3] >> 31) & 0x00000001) == 1;
            byte scale = (byte)((parts[3] >> 16) & 0x000000FF);
            return new Decimal(parts[0], parts[1], parts[2], isNegative, scale);
        }


        public static decimal ReadT_decimal(Stream stream, bool readedTypeID = false)
        {

            int[] parts = new int[4];
            for (int i = 0; i < 4; ++i)
            {
                parts[i] = (int)Read_int32(stream);
            }
            bool isNegative = ((parts[3] >> 31) & 0x00000001) == 1;
            byte scale = (byte)((parts[3] >> 16) & 0x000000FF);
            return new Decimal(parts[0], parts[1], parts[2], isNegative, scale);
        }

        #endregion ------------------------------ READ/WRITE deciaml -----------------------------------


        #region ---------------------------- READ/Write  TimeSpan ----------------------------------

        public static void Write(Stream stream, TimeSpan value)
        {
            Write(stream, value.Ticks);
        }


        public static void WriteT_TimeSpan(Stream stream, TimeSpan value)
        {
            Write(stream, value.Ticks);
        }


        public static object Read_TimeSpan(Stream stream, bool readedTypeID = false)
        {
            return new TimeSpan((long)Read_int64(stream));
        }

        public static TimeSpan ReadT_TimeSpan(Stream stream, bool readedTypeID = false)
        {
            return new TimeSpan((long)Read_int64(stream));
        }

        #endregion ---------------------------- READ/Write  TimeSpan ----------------------------------


        #region ---------------------------- READ/Write Guid -----------------------------------

        public static void Write(Stream stream, Guid value)
        {
            Write(stream, value.ToByteArray());
        }

        public static void WriteT_Guid(Stream stream, Guid value)
        {
            Write(stream, value.ToByteArray());
        }


        public static object Read_Guid(Stream stream, bool readedTypeID = false)
        {
            return new Guid((byte[])Read_byteArray(stream));
        }

        public static Guid ReadT_Guid(Stream stream, bool readedTypeID = false)
        {
            return new Guid((byte[])Read_byteArray(stream));
        }

        #endregion ---------------------------- READ/Write Guid -----------------------------------


        #region -------------------------------READ/Write Uri -----------------------------------

        public static void Write(Stream stream, Uri value)
        {
            Write(stream, value.OriginalString);
        }


        public static void WriteT_Uri(Stream stream, Uri value)
        {
            Write(stream, value.OriginalString);
        }

        public static object Read_Uri(Stream stream, bool readedTypeID = false)
        {
            return new Uri((string)Read_string(stream));
        }

        public static Uri ReadT_Uri(Stream stream, bool readedTypeID = false)
        {
            return new Uri((string)Read_string(stream));
        }

        #endregion -------------------------------READ/Write Uri -----------------------------------


        #region ---------------------------- READ/Write BitArray -----------------------------------

        public static void Write(Stream stream, BitArray value)
        {

            var bitArray = value as BitArray;

            //Write the length
            Write(stream, bitArray.Length);

            //Copy to bool array            
            bool[] bits = new bool[bitArray.Length];
            bitArray.CopyTo(bits, 0);

            foreach (bool bit in bits)
            {
                Write(stream, bit);
            }

        }


        public static void WriteT_BitArray(Stream stream, BitArray value)
        {
            var bitArray = value as BitArray;

            //Write the length
            Write(stream, bitArray.Length);

            //Copy to bool array            
            bool[] bits = new bool[bitArray.Length];
            bitArray.CopyTo(bits, 0);

            foreach (bool bit in bits)
            {
                Write(stream, bit);
            }
        }


        public static object Read_BitArray(Stream stream, bool readedTypeID = false)
        {   //Read the length
            int totalLength = (int)Read_int32(stream);


            //reading bits
            bool[] boolArray = new bool[totalLength];

            for (int i = 0; i < totalLength; i++)
                boolArray[i] = (bool)Read_bool(stream);


            return new BitArray(boolArray);
        }

        public static BitArray ReadT_BitArray(Stream stream, bool readedTypeID = false)
        {   //Read the length
            int totalLength = (int)Read_int32(stream);


            //reading bits
            bool[] boolArray = new bool[totalLength];

            for (int i = 0; i < totalLength; i++)
                boolArray[i] = (bool)Read_bool(stream);


            return new BitArray(boolArray);
        }

        #endregion ---------------------------- READ/Write BitArray -----------------------------------


        #region  -------------------------- READ/Write TypeInfoEx -----------------------------


        public static void WriteT_TypeInfoEx(Stream stream, TypeInfoEx value)
        {
            if (value.State == TypeFoundStateEn.NotFounded) throw new InvalidDataException("TypeInfoEx  error state - targetType was not found. So it can't be serialized");
            
            Write(stream, value.OriginalTypeAQNameString);            
             
        }

        public static TypeInfoEx ReadT_TypeInfoEx(Stream stream, bool readedTypeID = false)
        {   //Read the length
            string typeAQName =  ReadT_string(stream);  
            return TypeInfoEx.Get(typeAQName);
        }

        #endregion -------------------------- READ/Write TypeInfoEx -----------------------------

#if SERVER && UNSAFE

        #region ---------------------------- READ/Write  double----------------------------------


        public static unsafe void Write(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }

        public static unsafe void WriteT_double(Stream stream, double value)
        {
            ulong v = *(ulong*)(&value);
            WriteVarint64(stream, v);
        }


        public static unsafe object Read_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }

        public static unsafe double ReadT_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return *(double*)(&v);
        }


        #endregion ---------------------------- READ/Write  double----------------------------------


        #region ---------------------------- READ/Write  float----------------------------------


        public static unsafe void Write(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }

        public static unsafe void WriteT_float(Stream stream, float value)
        {
            uint v = *(uint*)(&value);
            WriteVarint32(stream, v);
        }


        public static unsafe object Read_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        public static unsafe float ReadT_float(Stream stream, bool readedTypeID = false)
        {
            uint v = ReadVarint32(stream);
            return *(float*)(&v);
        }

        #endregion ---------------------------- READ/Write  float----------------------------------


#else

        #region ---------------------------- READ/Write double----------------------------------

        public static void Write(Stream stream, double value)
        {
            ulong v = (ulong)BitConverter.DoubleToInt64Bits(value);
            WriteVarint64(stream, v);
        }

        public static void WriteT_double(Stream stream, double value)
        {
            ulong v = (ulong)BitConverter.DoubleToInt64Bits(value);
            WriteVarint64(stream, v);
        }


        public static object Read_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return BitConverter.Int64BitsToDouble((long)v);
        }

        public static double ReadT_double(Stream stream, bool readedTypeID = false)
        {
            ulong v = ReadVarint64(stream);
            return BitConverter.Int64BitsToDouble((long)v);
        }

        #endregion ---------------------------- READ/Write double----------------------------------


        #region ---------------------------- READ/Write float----------------------------------

        public static void Write(Stream stream, float value)
        {
            Write(stream, (double)value);
        }

        public static void WriteT_float(Stream stream, float value)
        {
            Write(stream, (double)value);
        }

        public static object Read_float(Stream stream, bool readedTypeID = false)
        {
            double v = (double)Read_double(stream);// default(double);

            return (float)v;
        }

        public static float ReadT_float(Stream stream, bool readedTypeID = false)
        {
            double v = (double)Read_double(stream);// default(double);

            return (float)v;
        }
        #endregion ---------------------------- READ/Write float----------------------------------

#endif



        #region ---------------------------- READ/Write short?-int16----------------------------------


        public static void Write(Stream stream, short? value)
        {

            WriteNullableAction<short>(stream, value,
                                (st, vl) =>
                                { WriteVarint32(st, EncodeZigZag32(vl.Value)); }
                             );

        }


        public static void WriteT_int16_nullable(Stream stream, short? value)
        {

            WriteNullableAction<short>(stream, value,
                                (st, vl) =>
                                { WriteVarint32(st, EncodeZigZag32(vl.Value)); }
                             );

        }


        public static object Read_int16_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<short>(stream, (strm) =>
            {
                return (short)DecodeZigZag32(ReadVarint32(strm)); //(short)               
            }
                                            );

        }

        public static short? ReadT_int16_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<short>(stream, (strm) =>
            {
                return (short)DecodeZigZag32(ReadVarint32(strm)); //(short)               
            }
                                            );

        }

        #endregion ---------------------------- READ/Write short?----------------------------------


        #region ---------------------------- READ/Write ushort?-ushort ----------------------------------


        public static void Write(Stream stream, ushort? value)
        {
            WriteNullableAction<ushort>(stream, value,
                                (st, vl) =>
                                { WriteVarint32(st, vl.Value); }
                             );
        }


        public static void WriteT_uint16_nullable(Stream stream, ushort? value)
        {
            WriteNullableAction<ushort>(stream, value,
                                (st, vl) =>
                                { WriteVarint32(st, vl.Value); }
                             );
        }


        public static object Read_uint16_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<ushort>(stream, (strm) =>
            {
                return (ushort)ReadVarint32(strm);
            }
                                            );

        }

        public static ushort? ReadT_uint16_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<ushort>(stream, (strm) =>
            {
                return (ushort)ReadVarint32(strm);
            }
                                            );

        }

        #endregion ---------------------------- READ/Write ushort?-ushort ----------------------------------


        #region ---------------------------- READ/Write int?-int32----------------------------------

        public static void Write(Stream stream, int? value)
        {
            WriteNullableAction<int>(stream, value,
                                       (st, vl) =>
                                       { WriteVarint32(st, EncodeZigZag32(vl.Value)); }
                                    );

        }


        public static void WriteT_int32_nullable(Stream stream, int? value)
        {
            WriteNullableAction<int>(stream, value,
                                       (st, vl) =>
                                       { WriteVarint32(st, EncodeZigZag32(vl.Value)); }
                                    );

        }

        public static object Read_int32_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<int>(stream, (strm) =>
            {
                return DecodeZigZag32(ReadVarint32(strm));
            }
                                          );
        }

        public static int? ReadT_int32_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<int>(stream, (strm) =>
            {
                return DecodeZigZag32(ReadVarint32(strm));
            }
                                          );
        }


        #endregion ---------------------------- READ/Write int?-int32 ----------------------------------


        #region ---------------------------- READ/Write uint?-uint32 ----------------------------------

        public static void Write(Stream stream, uint? value)
        {
            WriteNullableAction<uint>(stream, value,
                                    (st, vl) =>
                                    { WriteVarint32(st, vl.Value); }
                                 );
        }
               


        public static void WriteT_uint32_nullable(Stream stream, uint? value)
        {
            WriteNullableAction<uint>(stream, value,
                                    (st, vl) =>
                                    { WriteVarint32(st, vl.Value); }
                                 );
        }



        public static object Read_uint32_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<uint>(stream, (strm) =>
            {
                return ReadVarint32(strm);
            }
                                          );


        }

        public static uint? ReadT_uint32_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<uint>(stream, (strm) =>
            {
                return ReadVarint32(strm);
            }
                                          );


        }

        #endregion ---------------------------- READ/Write uint?-uint32 ----------------------------------


        #region ---------------------------- READ/Write long?-int64----------------------------------

        public static void Write(Stream stream, long? value)
        {
            WriteNullableAction<long>(stream, value,
                                   (st, vl) =>
                                   { WriteVarint64(st, EncodeZigZag64(vl.Value)); }
                                );

        }


        public static void WriteT_int64_nullable(Stream stream, long? value)
        {
            WriteNullableAction<long>(stream, value,
                                   (st, vl) =>
                                   { WriteVarint64(st, EncodeZigZag64(vl.Value)); }
                                );

        }


        public static object Read_int64_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<long>(stream, (strm) =>
            {
                return DecodeZigZag64(ReadVarint64(strm));
            }
                                         );
        }

        public static long? ReadT_int64_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<long>(stream, (strm) =>
            {
                return DecodeZigZag64(ReadVarint64(strm));
            }
                                         );

        }


        #endregion ---------------------------- READ/Write long?-int64----------------------------------


        #region ---------------------------- READ/Write ulong?-uint64----------------------------------

        public static void Write(Stream stream, ulong? value)
        {
            WriteNullableAction<ulong>(stream, value,
                                  (st, vl) =>
                                  { WriteVarint64(st, vl.Value); ; }
                               );

        }

        public static void WriteT_uint64_nullable(Stream stream, ulong? value)
        {
            WriteNullableAction<ulong>(stream, value,
                                  (st, vl) =>
                                  { WriteVarint64(st, vl.Value); ; }
                               );

        }

        public static object Read_uint64_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<ulong>(stream, (strm) =>
            {
                return ReadVarint64(strm);
            }
                                             );

        }

        public static ulong? ReadT_uint64_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<ulong>(stream, (strm) =>
            {
                return ReadVarint64(strm);
            }
                                             );

        }

        #endregion ---------------------------- READ/Write ulong?-uint64 ----------------------------------


        #region ------------------------------ READ/WRITE deciaml? -----------------------------------

        public static void Write(Stream stream, decimal? value)
        {
            WriteNullableAction<decimal>(stream, value,
                              (st, vl) =>
                              {
                                  int[] parts = Decimal.GetBits(vl.Value);
                                  foreach (int item in parts)
                                  {
                                      Write(st, item);                //STask.Write(item);
                                  }
                              }
                           );


        }


        public static void WriteT_decimal_nullable(Stream stream, decimal? value)
        {
            WriteNullableAction<decimal>(stream, value,
                              (st, vl) =>
                              {
                                  int[] parts = Decimal.GetBits(vl.Value);
                                  foreach (int item in parts)
                                  {
                                      Write(st, item);                //STask.Write(item);
                                  }
                              }
                           );
        }



        public static object Read_decimal_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<decimal>(stream,
                                                (strm) =>
                                                {
                                                    int[] parts = new int[4];
                                                    for (int i = 0; i < 4; ++i)
                                                    {
                                                        parts[i] = (int)Read_int32(strm);
                                                    }
                                                    bool isNegative = ((parts[3] >> 31) & 0x00000001) == 1;
                                                    byte scale = (byte)((parts[3] >> 16) & 0x000000FF);
                                                    return new Decimal(parts[0], parts[1], parts[2], isNegative, scale);
                                                }
                                              );

        }

        public static decimal? ReadT_decimal_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<decimal>(stream,
                                                (strm) =>
                                                {
                                                    int[] parts = new int[4];
                                                    for (int i = 0; i < 4; ++i)
                                                    {
                                                        parts[i] = (int)Read_int32(strm);
                                                    }
                                                    bool isNegative = ((parts[3] >> 31) & 0x00000001) == 1;
                                                    byte scale = (byte)((parts[3] >> 16) & 0x000000FF);
                                                    return new Decimal(parts[0], parts[1], parts[2], isNegative, scale);
                                                }
                                              );

        }

        #endregion ------------------------------ READ/WRITE deciaml? -----------------------------------


        #region ---------------------------- READ/Write byte?----------------------------------

        public static void Write(Stream stream, byte? value)
        {
            WriteNullableAction<byte>(stream, value,
                               (st, vl) =>
                               { st.WriteByte(vl.Value); }
                            );
        }


        public static void WriteT_byte_nullable(Stream stream, byte? value)
        {
            WriteNullableAction<byte>(stream, value,
                               (st, vl) =>
                               { st.WriteByte(vl.Value); }
                            );
        }


        public static object Read_byte_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableAction<byte>(stream, (strm) =>
            {
                return (byte)strm.ReadByte();
            }
                                              );



        }

        public static byte? ReadT_byte_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableTAction<byte>(stream, (strm) =>
            {
                return (byte)strm.ReadByte();
            }
                                              );



        }
        #endregion ---------------------------- READ/Write byte?----------------------------------


        #region ---------------------------- READ/Write sbyte?----------------------------------

        public static void Write(Stream stream, sbyte? value)
        {

            WriteNullableAction<sbyte>(stream, value,
                              (st, vl) =>
                              { st.WriteByte((byte)vl.Value); }
                           );

        }


        public static void WriteT_sbyte_nullable(Stream stream, sbyte? value)
        {

            WriteNullableAction<sbyte>(stream, value,
                              (st, vl) =>
                              { st.WriteByte((byte)vl.Value); }
                           );

        }


        public static object Read_sbyte_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableAction<sbyte>(stream, (strm) =>
            {
                return (sbyte)strm.ReadByte();
            }
                                            );

        }

        public static sbyte? ReadT_sbyte_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableTAction<sbyte>(stream, (strm) =>
            {
                return (sbyte)strm.ReadByte();
            }
                                            );

        }

        #endregion ---------------------------- READ/Write sbyte?----------------------------------


        #region ---------------------------- READ/Write char?----------------------------------

        public static void Write(Stream stream, char? value)
        {

            WriteNullableAction<char>(stream, value,
                              (st, vl) =>
                              { WriteVarint32(st, vl.Value); }
                           );
        }



        public static void WriteT_char_nullable(Stream stream, char? value)
        {

            WriteNullableAction<char>(stream, value,
                              (st, vl) =>
                              { WriteVarint32(st, vl.Value); }
                           );
        }

        public static object Read_char_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<char>(stream, (strm) =>
            {
                return (char)ReadVarint32(strm);
            }
                                           );

        }

        public static char? ReadT_char_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<char>(stream, (strm) =>
            {
                return (char)ReadVarint32(strm);
            }
                                           );

        }
        #endregion ---------------------------- READ/Write char?----------------------------------


        #region ---------------------------- READ/Write bool?----------------------------------

        public static void Write(Stream stream, bool? value)
        {

            WriteNullableAction<bool>(stream, value,
                           (st, vl) =>
                           { st.WriteByte(vl.Value ? (byte)1 : (byte)0); }
                        );

        }


        public static void WriteT_bool_nullable(Stream stream, bool? value)
        {

            WriteNullableAction<bool>(stream, value,
                           (st, vl) =>
                           { st.WriteByte(vl.Value ? (byte)1 : (byte)0); }
                        );

        }


        public static object Read_bool_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableAction<bool>(stream, (strm) =>
            {
                return (bool)(strm.ReadByte() != 0);
            }
                                          );

        }

        public static bool? ReadT_bool_nullable(Stream stream, bool readedTypeID = false) //set method accessor
        {
            return ReadNullableTAction<bool>(stream, (strm) =>
            {
                return (bool)(strm.ReadByte() != 0);
            }
                                          );

        }


        #endregion ---------------------------- READ/Write bool?----------------------------------


        #region ---------------------------- READ/Write DateTime?----------------------------------

        public static void Write(Stream stream, DateTime? value)
        {
            WriteNullableAction<DateTime>(stream, value,
                           (st, vl) =>
                           {
                               long v = ((DateTime)vl.Value).Ticks;//ToBinary();
                               Write(st, v);
                           }
                        );


        }


        public static void WriteT_DateTime_nullable(Stream stream, DateTime? value)
        {
            WriteNullableAction<DateTime>(stream, value,
                           (st, vl) =>
                           {
                               long v = ((DateTime)vl.Value).Ticks;//ToBinary();
                               Write(st, v);
                           }
                        );


        }


        public static object Read_DateTime_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<DateTime>(stream, (strm) =>
            {
                long v = (long)Read_int64(strm);
                return DateTime.FromBinary(v);
            }
                                               );


        }


        public static DateTime? ReadT_DateTime_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<DateTime>(stream, (strm) =>
            {
                long v = (long)Read_int64(strm);
                return DateTime.FromBinary(v);
            }
                                               );


        }
        #endregion ---------------------------- READ/Write  DateTime?----------------------------------


        #region ---------------------------- READ/Write  TimeSpan? ----------------------------------

        public static void Write(Stream stream, TimeSpan? value)
        {
            WriteNullableAction<TimeSpan>(stream, value,
                          (st, vl) =>
                          {
                              Write(st, vl.Value.Ticks);
                          }
                       );

        }

        public static void WriteT_TimeSpan_nullable(Stream stream, TimeSpan? value)
        {
            WriteNullableAction<TimeSpan>(stream, value,
                          (st, vl) =>
                          {
                              Write(st, vl.Value.Ticks);
                          }
                       );

        }


        public static object Read_TimeSpan_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<TimeSpan>(stream,
                                                (strm) =>
                                                {
                                                    return new TimeSpan((long)Read_int64(strm));
                                                }
                                               );
        }

        public static TimeSpan? ReadT_TimeSpan_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<TimeSpan>(stream,
                                                (strm) =>
                                                {
                                                    return new TimeSpan((long)Read_int64(strm));
                                                }
                                               );
        }

        #endregion ---------------------------- READ/Write  TimeSpan? ----------------------------------


        #region ---------------------------- READ/Write Guid? -----------------------------------

        public static void Write(Stream stream, Guid? value)
        {
            WriteNullableAction<Guid>(stream, value,
                         (st, vl) =>
                         {
                             Write(st, vl.Value.ToByteArray());
                         }
                      );
        }

        public static void WriteT_Guid_nullable(Stream stream, Guid? value)
        {
            WriteNullableAction<Guid>(stream, value,
                         (st, vl) =>
                         {
                             Write(st, vl.Value.ToByteArray());
                         }
                      );
        }



        public static object Read_Guid_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<Guid>(stream,
                                              (strm) =>
                                              {
                                                  return new Guid((byte[])Read_byteArray(strm));
                                              }
                                            );

        }

        public static Guid? ReadT_Guid_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<Guid>(stream,
                                              (strm) =>
                                              {
                                                  return new Guid((byte[])Read_byteArray(strm));
                                              }
                                            );

        }

        #endregion ---------------------------- READ/Write Guid? -----------------------------------



#if UNSAFE


        #region ---------------------------- READ/Write  float? ----------------------------------


        public static unsafe void Write(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }

      
        public static unsafe void WriteT_float_nullable(Stream stream, float? value)
        {

            WriteNullableAction<float>(stream, value,
                        (st, vl) =>
                        {
                            float realvalue = vl.Value;
                            uint v = *(uint*)(&realvalue);
                            WriteVarint32(st, v);
                        }
                     );             

        }





        public static unsafe object Read_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        public static unsafe float? ReadT_float_nullable(Stream stream, bool readedTypeID = false)
        {

            return ReadNullableTAction<float>(stream, (strm) =>
                                            {
                                                uint v = ReadVarint32(strm);
                                                return *(float*)(&v);                
                                            }
                                            );  
            
        }

        #endregion ---------------------------- READ/Write  float? ----------------------------------


        #region ---------------------------- READ/Write  double? ----------------------------------


        public static unsafe void Write(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe void WriteT_double_nullable(Stream stream, double? value)
        {
            
            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            double realValue = vl.Value;
                            ulong v = *(ulong*)(&realValue);
                            WriteVarint64(st, v);
                        }
                     );    

        }


        public static unsafe object Read_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        public static unsafe double? ReadT_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<double>(stream, (strm) =>
                                            {
                                                ulong v = ReadVarint64(strm);
                                                return *(double*)(&v);
                                            }
                                            );  

           
        }

        #endregion ---------------------------- READ/Write  double? ----------------------------------

#elif !UNSAFE

        #region ---------------------------- READ/Write double?----------------------------------

        public static void Write(Stream stream, double? value)
        {
            WriteNullableAction<double>(stream, value,
                          (st, vl) =>
                          {
                              ulong v = (ulong)BitConverter.DoubleToInt64Bits(vl.Value);
                              WriteVarint64(st, v);
                          }
                       );

        }

        public static void WriteT_double_nullable(Stream stream, double? value)
        {
            WriteNullableAction<double>(stream, value,
                          (st, vl) =>
                          {
                              ulong v = (ulong)BitConverter.DoubleToInt64Bits(vl.Value);
                              WriteVarint64(st, v);
                          }
                       );

        }



        public static object Read_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<double>(stream, (strm) =>
            {
                ulong v = ReadVarint64(strm);
                return BitConverter.Int64BitsToDouble((long)v);
            }
                                           );
        }

        public static double? ReadT_double_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<double>(stream, (strm) =>
            {
                ulong v = ReadVarint64(strm);
                return BitConverter.Int64BitsToDouble((long)v);
            }
                                           );
        }

        #endregion ---------------------------- READ/Write double?----------------------------------


        #region ---------------------------- READ/Write float?----------------------------------

        public static void Write(Stream stream, float? value)
        {

            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            Write(st, (double)vl.Value);
                        }
                     );

        }

        public static void WriteT_float_nullable(Stream stream, float? value)
        {

            WriteNullableAction<double>(stream, value,
                        (st, vl) =>
                        {
                            Write(st, (double)vl.Value);
                        }
                     );

        }

        public static object Read_float_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableAction<float>(stream, (strm) =>
            {
                double v = (double)Read_double(strm);// default(double);                                                
                return (float)v;
            }
                                             );

        }

        public static float? ReadT_float_nullable(Stream stream, bool readedTypeID = false)
        {
            return ReadNullableTAction<float>(stream, (strm) =>
            {
                double v = (double)Read_double(strm);// default(double);                                                
                return (float)v;
            }
                                             );

        }

        #endregion ---------------------------- READ/Write float?----------------------------------

#endif




#if (NET45 || SL5 || WP81)  //&& !UNSAFE  //  string

        #region ---------------------------- READ/Write string ----------------------------------

        public static void Write(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }

            var encoding = new UTF8Encoding(false, true);

            int len = encoding.GetByteCount(value);

            Write(stream, (uint)len + 1);

            var buf = new byte[len];

            encoding.GetBytes(value, 0, value.Length, buf, 0);

            stream.Write(buf, 0, len);
        }


        public static void WriteT_string(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }

            var encoding = new UTF8Encoding(false, true);

            int len = encoding.GetByteCount(value);

            Write(stream, (uint)len + 1);

            var buf = new byte[len];

            encoding.GetBytes(value, 0, value.Length, buf, 0);

            stream.Write(buf, 0, len);
        }


        public static object Read_string(Stream stream, bool readedTypeID = false)
        {
            uint len = (uint)Read_uint32(stream);// default(uint);


            if (len == 0)
            {
                return null;
                //return;
            }
            else if (len == 1)
            {
                return string.Empty;
                //return;
            }

            len -= 1;

            var encoding = new UTF8Encoding(false, true);

            var buf = new byte[len];

            int l = 0;

            while (l < len)
            {
                int r = stream.Read(buf, l, (int)len - l);
                if (r == 0)
                    throw new EndOfStreamException();
                l += r;
            }

            return encoding.GetString(buf, 0, buf.Length); //for SL5 or  !UNSAFE
        }



        public static string ReadT_string(Stream stream, bool readedTypeID = false)
        {
            uint len = (uint)Read_uint32(stream);// default(uint);


            if (len == 0)
            {
                return null;
                //return;
            }
            else if (len == 1)
            {
                return string.Empty;
                //return;
            }

            len -= 1;

            var encoding = new UTF8Encoding(false, true);

            var buf = new byte[len];

            int l = 0;

            while (l < len)
            {
                int r = stream.Read(buf, l, (int)len - l);
                if (r == 0)
                    throw new EndOfStreamException();
                l += r;
            }

            return encoding.GetString(buf, 0, buf.Length); //for SL5 or  !UNSAFE
        }

        #endregion ---------------------------- READ/Write string ----------------------------------

#elif UNSAFE
        #region ---------------------------- READ/Write string ----------------------------------

        [ThreadStatic]
        static StringHelper s_stringHelper;

        public unsafe static void Write(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
        

         public unsafe static void WriteT_string(Stream stream, string value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }
            else if (value.Length == 0)
            {
                Write(stream, (uint)1);
                return;
            }

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var encoder = helper.Encoder;
            var buf = helper.ByteBuffer;

            int totalChars = value.Length;
            int totalBytes;

            fixed (char* ptr = value)
                totalBytes = encoder.GetByteCount(ptr, totalChars, true);

            Write(stream, (uint)totalBytes + 1);
            Write(stream, (uint)totalChars);

            int p = 0;
            bool completed = false;

            while (completed == false)
            {
                int charsConverted;
                int bytesConverted;

                fixed (char* src = value)
                fixed (byte* dst = buf)
                {
                    encoder.Convert(src + p, totalChars - p, dst, buf.Length, true,
                        out charsConverted, out bytesConverted, out completed);
                }

                stream.Write(buf, 0, bytesConverted);

                p += charsConverted;
            }
        }
      

        public static object Read_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }


        
        public static string ReadT_string(Stream stream, bool readedTypeID = false)
        {
            uint totalBytes = (uint)ReadPrimitive_uint(stream);            

            if (totalBytes == 0)
            {
                return null;               
            }
            else if (totalBytes == 1)
            {
               return string.Empty;              
            }

            totalBytes -= 1;

            uint totalChars = (uint)ReadPrimitive_uint(stream);            

            var helper = s_stringHelper;
            if (helper == null)
                s_stringHelper = helper = new StringHelper();

            var decoder = helper.Decoder;
            var buf = helper.ByteBuffer;
            char[] chars;
            if (totalChars <= StringHelper.CHARBUFFERLEN)
                chars = helper.CharBuffer;
            else
                chars = new char[totalChars];

            int streamBytesLeft = (int)totalBytes;

            int cp = 0;

            while (streamBytesLeft > 0)
            {
                int bytesInBuffer = stream.Read(buf, 0, Math.Min(buf.Length, streamBytesLeft));
                if (bytesInBuffer == 0)
                    throw new EndOfStreamException();

                streamBytesLeft -= bytesInBuffer;
                bool flush = streamBytesLeft == 0 ? true : false;

                bool completed = false;

                int p = 0;

                while (completed == false)
                {
                    int charsConverted;
                    int bytesConverted;

                    decoder.Convert(buf, p, bytesInBuffer - p,
                        chars, cp, (int)totalChars - cp,
                        flush,
                        out bytesConverted, out charsConverted, out completed);

                    p += bytesConverted;
                    cp += charsConverted;
                }
            }

            return new string(chars, 0, (int)totalChars);
        }

        #endregion ---------------------------- READ/Write string ----------------------------------
#endif


        #region ---------------------------- READ/Write byte[] ----------------------------------

        public static void Write(Stream stream, byte[] value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }

            Write(stream, (uint)value.Length + 1);

            stream.Write(value, 0, value.Length);
        }

        public static void WriteT_byteArray(Stream stream, byte[] value)
        {
            if (value == null)
            {
                Write(stream, (uint)0);
                return;
            }

            Write(stream, (uint)value.Length + 1);

            stream.Write(value, 0, value.Length);
        }


        static readonly byte[] s_emptyByteArray = new byte[0];


        public static object Read_byteArray(Stream stream, bool readedTypeID = false)
        {
            uint len = (uint)Read_uint32(stream);

            if (len == 0)
            {
                return null;
            }
            else if (len == 1)
            {
                return s_emptyByteArray;
            }

            len -= 1;

            var reslt = new byte[len];
            int l = 0;

            while (l < len)
            {
                int r = stream.Read(reslt, l, (int)len - l);
                if (r == 0)
                    throw new EndOfStreamException("PrimitiveReaderWriter:: ReadPrimitive_byteArray() - End of Stream error");
                l += r;
            }

            return reslt;
        }


        public static byte[] ReadT_byteArray(Stream stream, bool readedTypeID = false)
        {
            uint len = (uint)Read_uint32(stream);

            if (len == 0)
            {
                return null;
            }
            else if (len == 1)
            {
                return s_emptyByteArray;
            }

            len -= 1;

            var reslt = new byte[len];
            int l = 0;

            while (l < len)
            {
                int r = stream.Read(reslt, l, (int)len - l);
                if (r == 0)
                    throw new EndOfStreamException("PrimitiveReaderWriter:: ReadPrimitive_byteArray() - End of Stream error");
                l += r;
            }

            return reslt;
        }
        #endregion ---------------------------- READ/Write byte[] ----------------------------------




        static void ReadWithBufferedStream()
        {
            using (var ms = new MemoryStream())
            {
                BufferedStreamX bs = new BufferedStreamX(ms);

            }

        }





    }
}



#region ------------------------------- GARBAGE ----------------------------------




/// <summary>
/// Writes any buffered data (if possible) to the underlying stream.
/// </summary>
/// <param name="writer">The writer to flush</param>
/// <remarks>It is not always possible to fully flush, since some sequences
/// may require values to be back-filled into the byte-stream.</remarks>
//internal static void Flush(ProtoWriter writer)
//{
//    if (writer.flushLock == 0 && writer.ioIndex != 0)
//    {
//        writer.dest.Write(writer.ioBuffer, 0, writer.ioIndex);
//        writer.ioIndex = 0;
//    }
//}


//internal void CheckDepthFlushlock()
//{
//    if (depth != 0 || flushLock != 0) throw new InvalidOperationException("The writer is in an incomplete state");
//}

//private static void DemandSpace(int required, Stream writer)
//{
//    // check for enough space
//    if ((writer.ioBuffer.Length - writer.ioIndex) < required)
//    {
//        if (writer.flushLock == 0)
//        {
//            Flush(writer); // try emptying the buffer
//            if ((writer.ioBuffer.Length - writer.ioIndex) >= required) return;
//        }
//        // either can't empty the buffer, or that didn't help; need more space
//        BufferPool.ResizeAndFlushLeft(ref writer.ioBuffer, required + writer.ioIndex, 0, writer.ioIndex);
//    }
//}




//    static List<Type> ListOfPrimitiveTypes = new List<Type>()
//    {
//   /////primitive structs 
//            // NOT NULLABLE PrimitiveReaderWriter
//            typeof(int),
//            typeof(uint),
//            typeof(long),
//            typeof(ulong),   
//            typeof(short),
//            typeof(ushort),
//            typeof(byte), 				
//            typeof(sbyte),
//            typeof(char),
//typeof(bool),
//typeof(DateTime),
//	typeof(TimeSpan),                
//            typeof(Guid),
//            typeof(float),
//            typeof(double),
//            typeof(decimal),


//            //NULLABLE PrimitiveReaderWriter                
//            typeof(int?),    
//            typeof(uint?),
//            typeof(long?),
//            typeof(ulong?),
//            typeof(short?),
//            typeof(ushort?),                
//            typeof(byte?),
//            typeof(sbyte?),
//            typeof(char?),
//            typeof(bool?),
//typeof(DateTime?),
//            typeof(TimeSpan?), 
//            typeof(Guid?),
//            typeof(float?),
//            typeof(double?),
//            typeof(decimal?),


//            /////primitive classes : string , byte[]
//            typeof(string),				
//typeof(byte[]),
//            typeof(Uri),
//            typeof(BitArray)
// };


//public static void ReadPrimitive(Stream stream, ref double value)
//{
//    ulong v = ReadVarint64(stream);
//    value = BitConverter.Int64BitsToDouble((long)v);
//}

#endregion ------------------------------- GARBAGE ----------------------------------