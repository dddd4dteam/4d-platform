﻿using System;


namespace DDDD.Core.Serialization
{

    /// <summary>
    /// Just because [Sustem.NonSerialized]   can be used only for fields we need another attribute for properties and fields.
    /// <para/>This attribute-help us to define field/property as member that won't be  de/serialized.
    /// <para/>IgnoreMemberAttribute class here exist only as Example attribute. 
    /// <para/>You need not to add reference to this assembly to use just right this attribute.
    /// You can copy this class into your library with your local namespace- and use that class, 
    /// there is no difference.
    /// <para/>There is no difference because the ignore member criterion is Attribute.Name.Contains("IgnoreMember")s.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public sealed class IgnoreMemberAttribute : Attribute
    {
    }
}
