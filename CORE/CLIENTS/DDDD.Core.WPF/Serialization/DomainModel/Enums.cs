﻿namespace DDDD.Core.Domain
{

    /// <summary>
    /// Domain Node Version Target : { Domain Model , Domain Contract }
    /// </summary>
    public enum DNVersionTargetEn
    {
        /// <summary>
        /// Domain Model as Target for DNVersion
        /// </summary>
        DModel
        ,
        
        /// <summary>
        /// Domain Contract as Target for DNVersion
        /// </summary>
        DContract
    }


   
}
