﻿
//#if NOT_RELEASED //


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.Core.Diagnostics;
using DDDD.Core.Reflection;
using DDDD.Core.Serialization;

namespace DDDD.Core.Serialization.Domain
{



    /// <summary>
    /// Domain Model Contract - contract in Service Domain Data Model.
    /// <para/> We can create DContract based on: 1 Type instance, 2 On the Type.FullName value.  
    /// </summary>
    /// <remarks> WE can group set of DomainContracts into DomainContractgroup.  
    /// And DomainConractGroup can be a composite item in DomainModel.Groups. </remarks>   
    public class DContract : INotifyPropertyChanged
    {

#region --------------------------------- CTOR ----------------------------------------

        /// <summary>
        /// Closed private ctor to orient  to use Create method
        /// </summary>
        DContract()
        {
        }

#endregion --------------------------------- CTOR ----------------------------------------


#region -------------------------------- PROPERTIES ----------------------------------

        /// <summary>
        /// Full Assembly Qualified name of Type of current Domain Contract.
        /// </summary>
        public string TypeAQNameValue
        {
            get;
            private set;
        }

        /// <summary>
        /// TypeEx - DContract TypeinfoEx 
        /// </summary>
        public TypeInfoEx TypeInfo
        {
            get
            {
                return TypeInfoEx.Get(TypeAQNameValue);
            }            
        }




        [IgnoreMember]
        String _GroupName = null;
        
        /// <summary>
        /// DContract  parent Group Name. Every contract includes into Domain Model only inside it's Group of DomainContracts- DomainContractGroup.        
        /// </summary>       
        public String GroupName
        {
            get
            {
                return _GroupName;
            }
            set
            {
                _GroupName = value;
                OnPropertyChanged(() => GroupName);
            }
        }
               
       
        
#endregion --------------------------------------- PROPERTIES --------------------------------------------





#region --------------------------DCONTRACT SERIALIZE GENERATOR ASSOCITION-----------------------------

        [IgnoreMember]
        Type _SerializeGenerator = null;

        /// <summary>
        /// Type of Serialize Generator that associated with this Domain Contract Group
        /// </summary>
        [IgnoreMember]
        public Type SerializeGenerator
        {
            get
            {
                return _SerializeGenerator;
            }
            set
            {
                _SerializeGenerator = value;
                OnPropertyChanged(() => SerializeGenerator);
            }
        }


        [IgnoreMember]
        String _SerializeGeneratorFullName = null;
        /// <summary>
        ///  Type.FullName of Type Serialize Generator that associated with this Domain Contract Group.
        /// </summary>
        public String SerializeGeneratorFullName
        {
            get
            {
                return _SerializeGeneratorFullName;
            }
            set
            {
                _SerializeGeneratorFullName = value;
                OnPropertyChanged(() => SerializeGeneratorFullName);
            }
        }


#endregion --------------------------DCONTRACT SERIALIZE GENERATOR ASSOCITION-----------------------------


#region ----------------------------------DCONTRACT. MEMBERS -----------------------------

        /// <summary>
        /// Members of the Domain contract- Properties and Fields(not Delegates or Events) 
        /// </summary>
        public Dictionary<String, DContract> Members
        {
            get; set;
        }

#endregion ----------------------------------DCONTRACT. MEMBERS -----------------------------


#region ---------------------  ACCESS MEMBERS - NONSERIALIZABLE -------------------------------

        /// <summary>
        /// Fields of current (T)-Type that we get after Type members Analyzing with current Policy value.  
        /// </summary>
        [IgnoreMember]
        internal protected Dictionary<string, FieldInfo> Fields
        {
            get;
            set;
        }

        /// <summary> 
        /// Reflection Binding of (T)-Type type fields. Binding creating based on the  (TypeProcessingPolicy). 
        ///<para/> But fields Binding can be customly setted when you choose TypeProcessingPolicyEn.CustomPolicy.
        ///<para/> CustomPolicy value of TypeProcessingPolicy require manual BindingFlags  setting.
        ///<para/> Whole possible binding flags that will be  used in combination are : BindingFlags.Instance ; BindingFlags.Public ; BindingFlags.NonPublic ; BindingFlags.DeclaredOnly
        ///<para/> and it means that other flags can't be used
        /// </summary>   
        [IgnoreMember]
        internal protected BindingFlags FieldsBinding
        {
            get;
            set;
        }


        /// <summary>
        /// Reflection Binding of (T)-Type type properties. Binding creating based on the  (TypeProcessingPolicy). 
        ///<para/> But properties Binding can be customly setted when you choose TypeProcessingPolicyEn.CustomPolicy.
        ///<para/>  CustomPolicy value of TypeProcessingPolicy require manual BindingFlags  setting.
        ///<para/> Whole possible binding flags that will be  used in combination are : BindingFlags.Instance ; BindingFlags.Public ; BindingFlags.NonPublic ; BindingFlags.DeclaredOnly
        ///<para/> and it means that other flags can't be used
        /// </summary>  
        [IgnoreMember]
        internal protected Dictionary<string, PropertyInfo> Properties
        {
            get;
            set;
        }

        /// <summary> 
        /// Reflection Binding of (T)-type properties. Binding creating based on the  (TypeProcessingPolicy). 
        ///<para/> But properties Binding can be custom setted when you choose TypeProcessingPolicyEn.CustomPolicy.
        ///<para/> This TypeProcessingPolicy require manual BindingFlags  setting.
        ///<para/> Whole possible binding flags that will be  used in combination are : BindingFlags.Instance ; BindingFlags.Public ; BindingFlags.NonPublic ; BindingFlags.DeclaredOnly
        ///<para/>  and it means that other flags can't be used
        /// </summary> 
        [IgnoreMember]
        internal protected BindingFlags PropertiesBinding
        {
            get;
            set;
        }
        
#endregion ---------------------  ACCESS MEMBERS - NONSERIALIZABLE -------------------------------
        



#region --------------------------------SELECTOR && ONLYMEMBERS && IGNOREMEMBERS - NONSERIALIZABLE ------------------------------
        
        /// <summary>
        /// Type Members Selector BiningFlag composition
        /// </summary>       
        public TypeMemberSelectorEn Selector
        {
            get;
            internal protected set;
        }

        /// <summary>
        /// Ignore Members of current Type when  Selector setted as OnlyMembers
        /// </summary>        
        public List<string> IgnoreMembers
        {
            get;
            private set;
        }


        /// <summary>
        ///  OnlyMembers array will be used if you use ONLY_MEMBERS Selector
        /// </summary>        
        public List<String> OnlyMembers
        {
            get;
            private set;
        }

        
#endregion--------------------------------SELECTOR && ONLYMEMBERS && IGNOREMEMBERS - NONSERIALIZABLE ------------------------------



        

#region ------------------------------- METHODS----------------------------------

        /// <summary>
        ///  Correctly Creation of Domain Contract.
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="contractType"></param>      
        /// <returns></returns>
        public static DContract Create(Type contractType) // bool pHasSubtypes = false
        {
            if (contractType == null)
                throw new InvalidOperationException( nameof(DContract.Create) + "():  contractType cannot be null."); 
            
            DContract contract = new DContract();
            contract.TypeAQNameValue = contractType.AssemblyQualifiedName;
            //var testBackTypeDetermining = contract.TypeInfo.TpAQName.FoundType.Value;
            contract.IgnoreMembers = new List<string>();

            contract.OnlyMembers = new List<string>();

            contract.Selector = TypeMemberSelectorEn.Default;

            //contract.FuncModelName = GroupName;
            //contract.GroupName = contract.TypeInfo.;// GroupName;
            
            //if (GroupName.IsNullOrEmpty() )
            //{


            //}
            //contract.TypeInfo = new TypeInfoEx(contractType);
            

            return contract;
        }



        public static DContract Create<T>() // bool pHasSubtypes = false
        {
            return Create(typeof(T));
        }



#endregion ------------------------------- METHODS----------------------------------




        /// <summary>
        /// Check that all only Members was found in Fields or Properties
        /// </summary>
        /// <param name="processingItem"></param>
        public void CheckAllOnlyMembersFounded()
        {
            if (Selector == TypeMemberSelectorEn.OnlyMembers)
            {
                //  Для политики OnlyMembers проверить что все  элементы найдены или же выдать
                //4 Обратная проверка - что все OnlyMembers найдены среди полей и свойств

                foreach (var onlyItem in OnlyMembers)
                {
                    //либо среди свойств либо среди полей, если нет то выдать сообщение
                    Validator.AssertFalse<InvalidOperationException>(Properties.ContainsKey(onlyItem) ||
                                                                     Fields.ContainsKey(onlyItem),
                          String.Format("Member Analyze error: Only member was not found. Look at the contract {0} and member{0}",
                                                                     TypeInfo.OriginalType, onlyItem)
                                                                    );
                }


            }
        }

      
           





#region ------------------------------- INotifyPropertyChanged  ------------------------------------

        /// <summary>
        /// Notification event -  will be raised when some property change its value
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        ///  Raise  PropertyChanged -Notification event - for some Property
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property"></param>
        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property of DContract");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }

#endregion -------------------------------  INotifyPropertyChanged  ------------------------------------


#region -------------------------DCONTRACT SERIALIZER ---------------------------------





        //static ITypeSerializer GetContractSerializer(Type contract)
        //{



        //    return null;
        //}



#endregion -------------------------DCONTRACT SERIALIZER ---------------------------------



#region -------------------------- DContract version -----------------------------------

        //PreviosContractVersion DContract
        //CurrentVersion DContract
        //Properties PropertySet

#endregion -------------------------- DContract version -----------------------------------




    }


}


//#endif  // NOT RELEASED YET


#region ------------------------------------------------- GARBAGE ---------------------------------------------------


///// <summary>
///// If TypeMapper was able to determine  Type  for thi DomainContract by String Names. 
///// Contract can be null after it'll be sent to the client before TypeMapper will run Type determination procedure.
///// If  IsTypeDetected == false  then we should call TryGetContract() to try to Determine  Contract's type.
///// </summary>                
//public bool IsTypeDetected
//{
//    get
//    {
//        return false;
//    }
//    //(TypeInfo != null); }
//}



//bool? _IsTypeWasNotDetermined = null;

///// <summary>
///// This property is actual when Contract's .Net Type is null:
/////   - if value  null - then we still wasn't try to determine the .Net type,
/////   - if true - yes .NET type was determined successfully,
/////   - if false - it was unsuccessfull trying to determine the Contract's .Net Type. 
///// </summary>
//public bool? IsTypeWasNotDetermined
//{
//    get
//    {
//        return _IsTypeWasNotDetermined;
//    }
//    set
//    {
//        _IsTypeWasNotDetermined = value;
//    }
//}





///// <summary>
///// задание типа контракта - на клиенте, только когда контракт не определен.
///// </summary>
///// <param name="contractType"></param>
//public void TrySetContractWithCheck(Type contractType)
//{
//    //Попробовать найти контракт на клиенте, на основе строковых имен полученных с сервера дл данного контракта
//    //contract 
//    //parentcontract

//}


//[IgnoreMember]
//String _DomainModelName = null;



///// <summary>
///// We  are talking about DDDD.Core.TypePrimitivesEn  - primitive types enum classification:
///// - true -  Contract's Type exist in DDDD.Core.TypePrimitivesEn enum
///// - false - Contract's Type doesn't exist in DDDD.Core.TypePrimitivesEn enum
///// </summary>
//public bool IsPrimitive
//{
//    get;
//    set;
//}

//[IgnoreMember]
//Type _Contract = null;
///// <summary>
///// Contract's .NET Type.
///// Will be setted only once- on creation. 
///// </summary> 
//[IgnoreMember]
//public Type Contract
//{
//    get
//    {
//        return _Contract;
//    }
//    set
//    {
//        _Contract = value;

//        if (_Contract != null)
//        {
//            _ContractName = Contract.Name;
//        }
//        if (_Contract != null)
//        {
//            _FullName = Contract.FullName;
//        }
//        if (_Contract != null)
//        {
//            _AQName = Contract.AssemblyQualifiedName;
//        }


//        OnPropertyChanged(() => Contract);
//    }
//}



//[IgnoreMember]        
//String _AssemblyName = null;

///// <summary>
/////Contract's  Type.Assembly.FullName.
/////
///// </summary>        
//public String AssemblyName
//{
//    get
//    {
//        if (_AssemblyName == null && Contract != null)
//        {
//            _AssemblyName = Contract.Assembly.FullName;
//        }

//        return _AssemblyName;
//    }
//    set
//    {
//        _AssemblyName = value;
//        OnPropertyChanged(() => AssemblyName);
//    }
//}





//[IgnoreMember]
//String _ContractName = null;

///// <summary>
///// Contract's Type.Name  . 
///// Will be setted only once- on creation.
///// </summary>
//public String ContractName
//{
//    get
//    {
//        return _ContractName;
//    }
//    set
//    {
//        _ContractName = value;
//        OnPropertyChanged(() => ContractName);
//    }
//}



//[IgnoreMember]
//String _FullName = null;
///// <summary>
/////Contract's Type.FullName.
///// </summary>       
//public String FullName
//{
//    get
//    {

//        return _FullName;
//    }
//    set
//    {
//        _FullName = value;
//        OnPropertyChanged(() => FullName);
//    }
//}


//[IgnoreMember]//isn't serializable - AQName property is enough for it
//String _AQName = null;
///// <summary>
///// Contract's Type.AssemblyQualifiedName.
///// </summary>       
//public String AQName
//{
//    get
//    {
//        return _AQName;
//    }
//    set
//    {
//        _AQName = value;
//        OnPropertyChanged(() => AQName);
//    }
//}





///// <summary>
//       /// DContract  Id that is compatible with NinjaDB store
//       /// </summary>
//       public Guid ContractId { get; set; }


//       ushort _RuntimeID = 0;

//       /// <summary>
//       /// ModelId that TypeProcessor_Contract_ will get in runtime after Serializer will build its TypeProcessor's Processing Dictionary ( call BuildTypeProcessor () ) 
//       /// </summary>
//       public ushort RuntimeID
//       {
//           get
//           {
//               return _RuntimeID;
//           }
//           set
//           {
//               _RuntimeID = value;
//               OnPropertyChanged(() => RuntimeID);
//           }
//       }




//contract.FuncModelName = GroupName;           

// dContract.AssemblyCrossName = DomainModel.GetCrossAssemblyName(dContract.AssemblyName);
//contract.AssemblyCrossName = DomainModel.GetCrossAssemblyName(contract.AssemblyName);

//public static FuncModelContract Create(String FuncModelName, String GroupName, String pContractFullName, String pAssemblyName = null, bool pNotUseInRuntimeModel = false, bool pHasSubtypes = false, Type pParentType = null)
//{
//    FuncModelContract contract = new FuncModelContract();
//    contract.FuncModelName = FuncModelName;
//    contract.GroupName = GroupName;
//    contract.FullName = pContractFullName;
//    contract.AssemblyName = pAssemblyName;
//    contract.ContractName = pContractFullName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

//    contract.NotUseInRuntimeModel = pNotUseInRuntimeModel;
//    contract.HasSubtypes = pHasSubtypes;
//    contract.ParentContract = pParentType;

//    return contract;
//}



//public static FuncModelContract Create(String FuncModelName, String GroupName, 
//    String pContractFullName, String pAssemblyName = null,
//    bool pNotUseInRuntimeModel = false, bool pHasSubtypes = false, 
//    String pParentTypeFullName = null, String pParentTypeAssemblyName = null)
//{
//    FuncModelContract contract = new FuncModelContract();
//    contract.FuncModelName = FuncModelName;
//    contract.GroupName = GroupName;
//    contract.FullName = pContractFullName;
//    contract.AssemblyName = pAssemblyName;
//    contract.ContractName = pContractFullName.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault();

//    contract.NotUseInRuntimeModel = pNotUseInRuntimeModel;
//    contract.HasSubtypes = pHasSubtypes;
//    contract.ParentFullName = pParentTypeFullName;
//    contract.ParentAssemblyName = pParentTypeAssemblyName;

//    return contract;
//}



//[IgnoreMember]
//String _AssemblyCrossName = null;
///// <summary>
///// Название сборки - независимое от  фреймворка сайда - для получения смежной сборки клиентских контрактов 
///// </summary>        
//public String AssemblyCrossName
//{
//    get
//    {
//        return _AssemblyCrossName;
//    }
//    set
//    {
//        _AssemblyCrossName = value;
//        OnPropertyChanged(() => AssemblyCrossName);
//    }
//}


//public DateTime? CreateDate
//{
//    get; set;            
//}

//public bool IsDeleted
//{
//    get; set;
//}

//public bool IsDirty
//{
//    get; set;
//}

//public DateTime? UpdateDate
//{
//    get; set;
//}

//[IgnoreMember]
//Type _ParentContract = null;
///// <summary>
///// У данного класса есть свой Родительcкий -Базовый класс.
///// </summary>


//public Type ParentContract
//{
//    get
//    {
//        return _ParentContract;
//    }
//    set
//    {
//        _ParentContract = value;
//        OnPropertyChanged(() => ParentContract);
//    }
//}

// [IgnoreMember]
//String _ParentFullName = null;
///// <summary>
///// Полное имя контракта. Задается тотлько при созддании.
///// </summary>        
//public String ParentFullName
//{
//    get
//    {
//        if (ParentContract != null)
//        {
//            return ParentContract.FullName;
//        }

//        return _ParentFullName;
//    }
//    set { 
//        _ParentFullName = value;
//        OnPropertyChanged(() => ParentFullName);
//    }
//}


// [IgnoreMember]
//String _ParentAssemblyName = null;
///// <summary>
///// Название сборки контракта 
///// </summary>        
//public String ParentAssemblyName
//{
//    get
//    {
//        if (ParentContract != null)
//        {
//            _ParentAssemblyName = ParentContract.Assembly.FullName;                    
//        }

//        return _ParentAssemblyName;
//    }
//    set {
//        _ParentAssemblyName = value;
//        OnPropertyChanged(() => ParentAssemblyName);
//    }
//}


//[IgnoreMember]
//String _ParentAssemblyCrossName = null;
///// <summary>
///// Название сборки - независимое от  фреймворка сайда - для получения смежной сборки клиентских контрактов 
///// </summary>        
//public String ParentAssemblyCrossName
//{
//    get
//    {
//        return _ParentAssemblyCrossName;
//    }
//    set
//    {
//        _ParentAssemblyCrossName = value;
//        OnPropertyChanged(() => ParentAssemblyCrossName);
//    }
//}

#endregion ------------------------------------------------- GARBAGE ---------------------------------------------------