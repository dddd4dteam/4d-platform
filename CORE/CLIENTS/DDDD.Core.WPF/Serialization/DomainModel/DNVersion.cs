﻿using System;

namespace DDDD.Core.Serialization.Domain
{

    /// <summary>
    /// Domain Node Version where NodeType: { Domain Model, Domain Contract}
    /// </summary>
    public struct DNVersion
    {
        public static readonly DNVersion MAXValue = new DNVersion() { MajorVersion = 255, MinorVersion = 255, SubVersion = ushort.MaxValue };

        public static readonly DNVersion MINValue = new DNVersion() { MajorVersion = 0, MinorVersion = 0, SubVersion = 0 };
        
        /// <summary>
        /// Version 1 index - Major Index
        /// </summary>
        public byte MajorVersion;

        /// <summary>
        /// Version 2 index - Minor Index
        /// </summary>
        public byte MinorVersion;

        /// <summary>
        /// Version 3 index - That may be build version or some else detailed and quickly changing version value
        /// </summary>
        public ushort SubVersion;

    }



}
