﻿using System;


namespace DDDD.Core.Serialization
{
 
    /// <summary>
    ///  Serializer for NinjaDB
    /// </summary>
    public interface INinjaDBSerializer
    {
        byte[] Serialize(object objectToSerialize, Type typeOfTheObject);

        object Deserialize(byte[] serializedBytes, Type typeOfTheObject);

        byte[] Serialize<T>(T objectToSerialize);

        T Deserialize<T>(byte[] serializedBytes);
    }


}
