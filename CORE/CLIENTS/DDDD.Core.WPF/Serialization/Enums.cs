﻿namespace DDDD.Core.Serialization
{


    /// <summary>
    /// Data Formatting Setting for Serializer.
    /// </summary>
    public enum FormattingEn
    {
        /// <summary>
        /// Binary formatting of data when serializing.
        /// </summary>
        Binary
       ,

        /// <summary>
        /// Json formatting of data when serializing.
        /// </summary>
        Json        
        , 

        /// <summary>
        /// Xml formatting of data when serializing.
        /// </summary>
        Xml
        ,

        /// <summary>
        /// CSV formatting of data. 
        /// </summary>
        Csv        
        ,

        /// <summary>
        /// JSV formatting of data. JSV is the format of data developed by ServiceStack.
        /// </summary>
        Jsv

    }


    /// <summary>
    /// Identity Type - information flag that helps us to determine - what is the Idetification type  of this ITypeSetSerializer instance.
    /// <para/> Identity key may be one of the followings: KeyName with [String] value 
    /// <para/>                                           , ID with [Guid] value
    /// <para/>                                           , Index with [Int32] value    
    /// </summary>
    public enum IdentityTypeEn
    {
        /// <summary>
        /// Not Defined  Identity Key Type
        /// </summary>
        NotDefined,

        /// <summary>
        /// When we use Identity Key property - KeyName with [String] value
        /// </summary>
        IdentityWitKeyName,

        /// <summary>
        /// When we use Identity Key property - ID with [Guid] value
        /// </summary>
        IdentityWithID,

        /// <summary>
        /// When we use Identity Key property -Index with [Int32] value
        /// </summary>
        IdentityWithIndex
    }




}


#region ------------------------------ GARBAGE --------------------------------------


/// <summary>
/// Contract Category - information flag that help us to determine  ADD_CONTRACT task of Contract.
/// Categories of contract can be the followings: 
///     Default adding Contract - all contracts that will be added with DEFAULT HANDLERS(No need to generate handlers from Expressions)    
///       Now that's only types from SerializeGenerator_Primitives-  4D[PRIMITIVE TYPES].
///       So ADD_CONTRACT task of [DefaultAdding] ContractCategory is internal and only for [PRIMITIVE TYPES]. 
///          TypeProcessor that belong to this this category has : 
///                     always  NeedToBeAnalyseAndBuild = false; 
///                     always   NeedToGenerateDeSerializeHandlers = false |||       
///     Auto adding Contract -  if contract is enum, or some Collection Kind, or interface-  it need no to implicitly AddContract call.
///       Such contract will be detecting and auto adding to Serializer.ContractDictionary  on Building Members of Contract.
///       So ADD_CONTRACT task of [AutoAdding] ContractCategory exist for IList/IDictionary/Enum/Interface types.   
///          TypeProcessor that belong to this this category has : 
///                     always  NeedToBeAnalyseAndBuild = false; 
///                     always  NeedToGenerateDeSerializeHandlers = true; |||
///                                                              
///     Custom adding Contract - all custom structs or class that is not  AutoAddingContract - enum, some of collection Kind, interface;
///       So ADD_CONTRACT task of [CustomAdding] ContractCategory exist for  any  other types- custom contracts( then two task above)
///          TypeProcessor that belong to this this category has : 
///                     custom  NeedToBeAnalyseAndBuild = custom - here we can customize TypeProcessingPolicy to select Members set;
///                     custom  NeedToGenerateDeSerializeHandlers = custom;   
/// </summary>
//public enum ContractCategoryEn
//{
//    /// <summary>
//    ///     Default adding Contract - all contracts that will be added with DEFAULT HANDLERS(No need to generate handlers from Expressions)
//    ///        Now that's only types from SerializeGenerator_Primitives-  so called [PRIMITIVE TYPES].
//    ///        TypeProcessor that belong to this this category has : 
//    ///                     always  NeedToBeAnalyseAndBuild = false; 
//    ///                     always   NeedToGenerateDeSerializeHandlers = false;
//    /// </summary>
//    DefaultAddingContract,

//    /// <summary>
//    /// Auto adding Contract -  if contract is enum, or some Collection Kind, or interface-  it need no to implicitly AddContract call.
//    ///     Such contract will be detecting and auto adding to Serializer.ContractDictionary  on Building Members of Contract.
//    ///     So ADD_CONTRACT task of [AutoAdding] ContractCategory exist for IList/IDictionary/Enum/Interface types. 
//    ///        TypeProcessor that belong to this this category has : 
//    ///                     always  NeedToBeAnalyseAndBuild = false; 
//    ///                     always   NeedToGenerateDeSerializeHandlers = true;
//    /// </summary>
//    AutoAddingContract,

//    /// <summary>
//    /// Custom adding Contract - all custom structs or class that is not  AutoAddingContract - enum, some of collection Kind, interface; 
//    ///    So ADD_CONTRACT task of [CustomAdding] ContractCategory exist for  any  other types- custom contracts( then two task above)
//    ///     TypeProcessor that belong to this this category has : 
//    ///                     custom  NeedToBeAnalyseAndBuild = custom - here we can customize TypeProcessingPolicy to select Members set;
//    ///                     custom  NeedToGenerateDeSerializeHandlers = custom;
//    ///                                                           
//    /// </summary>
//    CustomContract

//}


///// <summary>
///// 
///// </summary>
//public enum TypeSerializerStateEn
//{       
//    // NeedToGenerate
//    NeedToGenerate
//    // Generated

//   , Generated
//}


//NeedToBuildAccessor - TypeAccessorStateEn
//NeedToBeGenerated   - TypeSerializerStateEn

#endregion ------------------------------ GARBAGE --------------------------------------

