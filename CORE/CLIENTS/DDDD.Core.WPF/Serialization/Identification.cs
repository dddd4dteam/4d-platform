﻿using System;
using DDDD.Core.Extensions;

namespace DDDD.Core.Serialization
{


    /// <summary>
    /// Identification Keys of some Item
    /// </summary>
    public class Identification
    {

        internal Identification()
        {
            _ID = Guid.Empty;
            _Index = -1;
            _KeyName = null;
        }

        /// <summary>
        /// Identity Type info - that tell us the type of Identity Key: String- [KeyName];Guid - [ID]; Int32 - [Index]
        /// </summary>
        public IdentityTypeEn IdentityType
        {
            get;
            private set;
        }


        string _KeyName;
        /// <summary>
        /// Identity Key by String value
        /// </summary>
        public String KeyName
        {
            get { return _KeyName; }
            internal set
            {
                _KeyName = value;
                IdentityType = IdentityTypeEn.IdentityWitKeyName;
            }
        }

        Guid _ID = Guid.Empty;
        /// <summary>
        /// Identity Key by Guid value
        /// </summary>
        public Guid ID
        {
            get { return _ID; }
            internal set
            {
                _ID = value;
                IdentityType = IdentityTypeEn.IdentityWithID;
            }
        }



        Int32 _Index = -1;

        /// <summary>
        /// Identity Key by Int32 value
        /// </summary>
        public Int32 Index
        {
            get { return _Index; }
            internal set
            {
                _Index = value;
                IdentityType = IdentityTypeEn.IdentityWithIndex;
            }
        }





        /// <summary>
        /// Get one of 4 keys that is not default value
        /// </summary>
        public String GetIdentity()
        {
            if (!String.IsNullOrEmpty(KeyName))
            {
                return KeyName;
            }
            else if (ID != Guid.Empty)
            {
                return ID.S();
            }
            else if (Index != -1)
            {
                return Index.S();
            }
            else
            {
                return null;
            }
        }
        
    }
    

}





#region ------------------------ GARBAGE ----------------------------

///// <summary>
///// Domain Model Version. For a while this property not used
///// </summary>
//public DNVersion Version
//{
//    get;
//    set;
//}

#endregion ------------------------ GARBAGE ----------------------------
