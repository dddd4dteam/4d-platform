﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.IO.Memory
{

    public static class MemoryStreamFactory
    {
        public static RecyclableMemoryStreamManager RecyclableInstance = new RecyclableMemoryStreamManager();

        public static bool UseRecyclableMemoryStream { get; set; }

        public static MemoryStream GetStream()
        {
            if (!MemoryStreamFactory.UseRecyclableMemoryStream)
                return new MemoryStream();
            return MemoryStreamFactory.RecyclableInstance.GetStream();
        }

        public static MemoryStream GetStream(int capacity)
        {
            if (!MemoryStreamFactory.UseRecyclableMemoryStream)
                return new MemoryStream(capacity);
            return MemoryStreamFactory.RecyclableInstance.GetStream(typeof(MemoryStreamFactory).Name, capacity);
        }

        public static MemoryStream GetStream(byte[] bytes)
        {
            if (!MemoryStreamFactory.UseRecyclableMemoryStream)
                return new MemoryStream(bytes);
            return MemoryStreamFactory.RecyclableInstance.GetStream(typeof(MemoryStreamFactory).Name, bytes, 0, bytes.Length);
        }

        public static MemoryStream GetStream(byte[] bytes, int index, int count)
        {
            if (!MemoryStreamFactory.UseRecyclableMemoryStream)
                return new MemoryStream(bytes, index, count);
            return MemoryStreamFactory.RecyclableInstance.GetStream(typeof(MemoryStreamFactory).Name, bytes, index, count);
        }
    }

}

