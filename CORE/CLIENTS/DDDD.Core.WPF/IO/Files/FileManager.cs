﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using DDDD.Core.Patterns;

namespace DDDD.Core.IO.Files
{


    /// <summary>
    ///     This class is used to assist with manager the isolated storage references, allowing
    ///     for nested requests to use the same isolated storage reference
    /// </summary>
    public class FileManager:Singleton<FileManager>
    {

        #region -------------- CTOR/ SINGETON INSTANCE ----------------------
        FileManager()
        { }

        static FileManager Current
        {
            get { return LA_Current.Value; }
        }

        #endregion -------------- CTOR/ SINGETON INSTANCE ----------------------


        private static readonly List<string> _paths = new List<string>();
        private static readonly List<string> _files = new List<string>();
                
        /// <summary>
        ///     Gets an isolated storage reader
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>The reader</returns>
        public BinaryReader GetReader(string path)
        {
            return new BinaryReader(File.OpenRead(path));            
        }

        /// <summary>
        ///     Get an isolated storage writer
        /// </summary>
        /// <param name="path">The path</param>
        /// <returns>The writer</returns>
        public BinaryWriter GetWriter(string path)
        {
            var stream = File.Open(path, FileMode.Create, FileAccess.Write);
            return new BinaryWriter(stream);
           
        }

        /// <summary>
        ///     Delete a file based on its path
        /// </summary>
        /// <param name="path">The path</param>
        public void Delete(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                if (_files.Contains(path))
                {
                    _paths.Remove(path);
                }
            }
        }
       
        /// <summary>
        ///     Ensure that a directory exists
        /// </summary>
        /// <param name="path">the path</param>
        public void EnsureDirectory(string path)
        {            
            if (path.EndsWith("/"))
            {
                path = path.Substring(0, path.Length - 1);
            }

            if (!_paths.Contains(path))
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                _paths.Add(path);
            }
           
        }

        /// <summary>
        ///     Check to see if a file exists
        /// </summary>
        /// <param name="path">The path to the file</param>
        /// <returns>True if it exists</returns>
        public bool FileExists(string path)
        {
            if (_files.Contains(path))
                return true;

            if (File.Exists(path))
            {
                _files.Add(path);
                return true;
            }
            return false;

        }

        /// <summary>
        /// Purge a directory and everything beneath it
        /// </summary>
        /// <param name="path">The path</param>
        public void Purge(string path)
        {
            _Purge(path, true);
        }

        /// <summary>
        /// Purge a directory and everything beneath it
        /// </summary>
        /// <param name="path">The path</param>
        /// <param name="clear">A value indicating whether the internal lists should be cleared</param>
        private static void _Purge(string path, bool clear)
        {
            if (clear)
            {
                _paths.Clear();
                _files.Clear();
            }


            // already purged!
            if (!Directory.Exists(path))
            {
                return;
            }

            // clear the sub directories
            var directory = new DirectoryInfo(path);
#if NET45 || SL5

            foreach (var dir in directory.EnumerateDirectories())
#elif WP81
            foreach (var dir in directory.GetDirectories())
#endif 
            {
                _Purge(Path.Combine(path, dir.FullName), false);
            }


            // clear the files - don't use a where clause because we want to get closer to the delete operation
            // with the filter
            foreach (var filePath in 
#if NET45 || SL5               
                directory.EnumerateFiles()
#elif WP81
                directory.GetFiles()
#endif 
                .Select(file => file.FullName))
            {
                File.Delete(filePath);
            }

            var dirPath = path.TrimEnd('\\', '/');
            if (!string.IsNullOrEmpty(dirPath) && Directory.Exists(dirPath))
            {
                Directory.Delete(dirPath);
            }
        }        
        

        public static void PurgeAll(String PurgePath)
        {            
            Current.Purge(PurgePath);
        }
    }
}
