﻿using System;
using System.Net;
using System.Text;
using System.Collections.Generic;
using System.ComponentModel;



namespace DDDD.BCL
{


    /// <summary>
    /// UI Progress Notification
    /// </summary>
    [Flags]
    public enum ProgressNotifyTypeEn
    {
        
        /// <summary>
        ///  1 on StageNotify
        /// </summary>
        StageNotify =1, 
                
        /// <summary>
        ///  2 on StateNotify  
        /// </summary>
        StateNotify= 47,
                
        /// <summary>
        ///  3 on FileNotify - FileName / DataLength;
        /// </summary>
        FileNotify=9,
        
        /// <summary>
        ///  4 on PercantegeNotify
        /// </summary>
        PercentageNotify=238,
                
        /// <summary>
        ///  5 on WholePercantageNotify 
        /// </summary>
        WholePercentageNotify=17,

        /// <summary>
        ///  6 on TimerTimeNotify 
        /// </summary>
        WaitingTimeNotify=574,
        
        /// <summary>
        /// Elapsed(прошедшее) time notification
        /// </summary>
        ElapsedTimeNotify=29,

        /// <summary>
        /// remainig(оставшееся) time notification
        /// </summary>
        RemainTimeNotify=793,

        /// <summary>
        ///  7 on ErrorNotify
        /// </summary>
        ErrorNotify=851        
                

        //combinations


    }





    /// <summary>
    ///  Структура которая информирует о текущем прогрессе. Учитывает несколько этапов проведения запросов 
    /// </summary>    
    public partial struct ProgressInfoTp
    {     
        
        /// <summary>
        /// 
        /// </summary>
        public String ErrorMessage
        {
            get;
            private set;
        }
        


        /// <summary>
        /// 
        /// </summary>
        public String FileName
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public String Stage
        {
            get
            {
                return GetCurrentStage();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public String State
        {
            get
            {
               return GetCurrentState();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Int32 RetryNumber
        {
            get;
            set;
        }
                
        
        /// <summary>
        /// 
        /// </summary>
        public DateTime WaitStartTime
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public DateTime StartTime
        {
            get;
            private set;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //public ProgressReporter Reporter
        //{
        //    get;
        //    private set;
        //}


        public static ProgressInfoTp Empty()
        {
            return new ProgressInfoTp();
        }



        #region  ---------------------------------- CALCULATED PROPERTIES -------------------------------

        private DateTime CurrentTime
        {    get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan ElapsedTime
        {
            get { return CurrentTime - StartTime; } // DateTime.Now

        }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan WaitedTime
        {
            get { return CurrentTime - WaitStartTime; } //DateTime.Now
        }

        /// <summary>
        /// Still Don't know how ?????
        /// </summary>
        public TimeSpan RemainingTime
        {
            get;
            private set;
        }
        
        #endregion ---------------------------------- CALCULATED PROPERTIES -------------------------------
    


        #region  ------------------------------------- METHODS ----------------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetCurrentState()
        {
            if (String.IsNullOrEmpty(States)) return "";

            // Localization for Stage Keys if need
            String[] states = States.Split(new char[] { '.', ',', ':', ';', '|', '/', '\\' });

            //1 not existed index
            if (states.IsValidIndex(CurrentStateIndex) == false)
            { return ""; }
            else
            { //2 existed  Stage by index 
                return states[CurrentStateIndex];
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetCurrentStage()
        {
            if (String.IsNullOrEmpty(Stages)) return "";

            // Localization for Stage Keys if need
            String[] stages = Stages.Split(new char[] { '.', ',', ':', ';', '|', '/', '\\' });

            //1 not existed index
            if (stages.IsValidIndex(CurrentStageIndex) == false)
            { return ""; }
            else
            { //2 existed  Stage by index 
                return stages[CurrentStageIndex];
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public void CalculateWholePercentCompleted()
        {
            // Localization for Stage Keys if need
            String[] stages = Stages.Split(new char[] { '.', ',', ':', ';', '|', '/', '\\' });


            PercentWholeCompleted = (CurrentStageIndex + 1 / stages.Length) * 100;
        }


        public void SetTimes(DateTime pStartTime, DateTime pWaitStartTime )
        {
            StartTime = pStartTime;
            WaitStartTime = pWaitStartTime;

            CurrentTime = DateTime.Now;
        }



        //public void SetReporter(ProgressReporter reporter )
        //{
        //    //Reporter = reporter;

        //}


        public static ProgressInfoTp Create(   ProgressModelEn progressModel = ProgressModelEn.LocalProcessProgress                                               
                                                
                                            )
        {
             ProgressInfoTp progress = new ProgressInfoTp();

            return progress;
        }



        /// <summary>
        /// Creating Progress Info  
        /// </summary>
        /// <returns></returns>
        public static ProgressInfoTp Create(         
                                                     ProgressModelEn progressModel = ProgressModelEn.LocalProcessProgress,
                                                     
                                                     String FileName = null,
                                                     String ErrorMessage = null,

                                                     StageModeEn StageMode = StageModeEn.SingleStage,
                                                     String Stages = null,
                                                     Int32 StageIndex = 0,

                                                     StateModeEn StateMode = StateModeEn.SingleState,
                                                     String States = null,
                                                     Int32 StateIndex = 0,

                                                     long DataLength = 0,
                                                     long ProcessedDataLength = 0,

                                                     float PercentStage = 0,

                                                     bool UsePercentWhole = false,
                                                     float WholePercent = 0

                                                                                                          
                                                     
                                            )
        {

            ProgressInfoTp progress = new ProgressInfoTp();
            progress.StageMode = StageMode;
        
            //  
            if (DataLength > 0)
            {
                progress.BytesLength = DataLength;
            }

            // 
            if (ProcessedDataLength > 0)
            {
                progress.BytesProcessed = ProcessedDataLength;
            }

            // 
            if (progress.BytesLength > 0 && progress.BytesProcessed > 0)
            {
                progress.PercentStageCompleted = ((float)progress.BytesProcessed / (float)progress.BytesLength); // * 100
            }

            if (String.IsNullOrEmpty(FileName) )
            {
                progress.FileName = FileName;
            }
            
            if (String.IsNullOrEmpty(ErrorMessage))
            {
                progress.ErrorMessage = ErrorMessage;
            }
            
            //Current Stage Index  - It may be different from 0 for StageModeEn.MultipleStages
            progress.CurrentStageIndex = StageIndex;
            //Stages
            if (String.IsNullOrEmpty(Stages) == false)
            {
                progress.Stages = Stages;
            }
            else
            {
                progress.Stages = "Default Process";
            }
            // by Index getting Stage or print All Stages
            

            //Current State Index  - It may be different from 0 for StateModeEn.MultipleStates
            progress.CurrentStateIndex = StateIndex;
            //States 
            if (String.IsNullOrEmpty(States) == false)
            {
                progress.States = States;
            }
            else
            {
                progress.States = " State:  Processing Data";
            }
            // by Index getting State or print All States


            //progress.PercentWholeCompleted = 
            if (UsePercentWhole && WholePercent != 0)
            {
                progress.PercentWholeCompleted = WholePercent;
            }
            else if (UsePercentWhole && WholePercent == 0)
            {
                progress.CalculateWholePercentCompleted();
            }
            

            //progress.StartTime = StartTime;
            //progress.WaitStartTime = WaitStartTime;

            
            return progress;
        }



        


        #region  -------------------------------- PROGRESS ------------------------------------

        /// <summary>
        ///  Getting Progress 
        /// </summary>
        /// <param name="states"></param>
        /// <param name="UseLocalization"></param>
        /// <returns></returns>
        public static String BuildStatesString(IEnumerable<String> states,
                                                String Dlmr = "&",  //States Delimeter
                                                bool UseLocalization = false)
        {
            StringBuilder strBuilder = new StringBuilder();

            bool firstIteration = true;
            foreach (var item in states)
            {
                if (firstIteration)
                {
                    strBuilder.Append(item);
                    firstIteration = false;
                }
                else strBuilder.Append(Dlmr + item);
            }
            return strBuilder.ToString();
        }


        /// <summary>
        ///  Parse  States from String
        /// </summary>
        /// <param name="states"></param>
        /// <param name="UseLocalization"></param>
        /// <returns></returns>
        public static String[] ParseStates(String states,
                                         String Dlmr = "&",  //States Delimeter
                                         bool UseLocalization = false)
        {
            return states.Split(new char[] { '&' }); ;
        }




        /// <summary>
        /// Build  Stages from String
        /// </summary>
        /// <param name="states">          </param>
        /// <param name="UseLocalization"> </param>
        /// <returns></returns>
        public static String BuildStagesString(IEnumerable<String> stages,
                                                String Dlmr = "&",  //States Delimeter
                                                bool UseLocalization = false)
        {
            StringBuilder strBuilder = new StringBuilder();

            bool firstIteration = true;
            foreach (var item in stages)
            {
                if (firstIteration)
                {
                    strBuilder.Append(item);
                    firstIteration = false;
                }
                else strBuilder.Append(Dlmr + item);
            }
            return strBuilder.ToString();
        }
        


        /// <summary>
        /// Parse  Stages from String
        /// </summary>
        /// <param name="states"></param>
        /// <param name="UseLocalization"></param>
        /// <returns></returns>
        public static String[] ParseStages( String stages,
                                            String Dlmr = "&",  //States Delimeter
                                            bool UseLocalization = false)
        {
            return stages.Split(new char[] { '&' }); 
        }
        

        #endregion -------------------------------- PROGRESS ------------------------------------
        


        
        #endregion ------------------------------------- METHODS ---------------------------------------



    }


}
