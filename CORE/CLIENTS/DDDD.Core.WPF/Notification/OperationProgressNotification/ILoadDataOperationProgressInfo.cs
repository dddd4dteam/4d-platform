﻿using System;

namespace DDDD.Core.Notification
{
    /// <summary>
    /// Base Loading Progress Notification on Download Task or Upload Task
    /// </summary>
    public interface ILoadDataOperationProgressInfo
    {
        double TransferRate { get; }

        DateTime TransferStartTime { get; }

        void ReCalculateTransferRate();

        void Reset();
    }
}
