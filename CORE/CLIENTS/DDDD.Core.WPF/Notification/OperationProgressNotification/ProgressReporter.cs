﻿using Microsoft;
using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;


namespace DDDD.Core.Notification
{



    /// <summary>
    /// Generic UI Progress Change Action Reporter - reports UI to change indicators based on your background Task/Thread events.
    /// </summary>
#if CLIENT && (SL5 || WP81)
     public class ProgressReporter<TUIActionsEnum> : Microsoft.Progress<IOperationProgressInfo<TUIActionsEnum>>
#elif NET45 || WPF
    public class ProgressReporter<TUIActionsEnum> : Progress<IOperationProgressInfo<TUIActionsEnum>>
#endif

    {

        #region  ----------------- CTOR ------------------------

        public ProgressReporter()
        {
            InitProgressFlagCommands();

            //set UI ProgressChanged event- OnReport Handler 
            ProgressChanged += DefaultOnReportHandler;
        }

        #endregion ----------------- CTOR ------------------------


        #region ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------


        IOperationProgressInfo<TUIActionsEnum> _Progress;

        /// <summary>
        ///By default it is making by base ProgressReporter
        /// </summary>
        public IOperationProgressInfo<TUIActionsEnum> Progress
        {
            get { return _Progress; }
            private set
            {
                _Progress = value;

                OnReport(_Progress);//Reporting 

            }

        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------

        /// <summary>
        /// Custom command Actions 
        /// </summary>
        Dictionary<Enum, Action<IOperationProgressInfo<TUIActionsEnum>>> CmdActions = new Dictionary<Enum, Action<IOperationProgressInfo<TUIActionsEnum>>>();

        void InitProgressFlagCommands()
        {
            foreach (var enumValue in Enum.GetValues(typeof(TUIActionsEnum)))
            {
                //switching Order to check Flags  - by enum value

                CmdActions.Add(enumValue as Enum, null);
            }
        }


        /// <summary>
        /// Check ProgressChange  EnumCommands by Enum.HasFlag(cmd) and call registered Action if it exist.
        /// The Order of commands enumeration belong to enum members declaration order.
        /// Commands calls will be combined as it'll be found in your enum-flags-value combination.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="progressArg"></param>
        protected virtual void DefaultOnReportHandler(object sender, IOperationProgressInfo<TUIActionsEnum> progressArg)
        {
            foreach (var commandItem in Enum.GetValues(typeof(TUIActionsEnum)))
            {
                if ((progressArg.UIActions as Enum).HasFlag(commandItem as Enum))
                {
                    if (CmdActions.ContainsKey(commandItem as Enum) && CmdActions[commandItem as Enum] != null)
                    {
                        CmdActions[commandItem as Enum](progressArg);
                    }
                }
            }
        }


        /// <summary>
        /// Custom ProgressChange EnumCommand action in UI for TCmdEnum Cmd command. 
        /// Commands calls can be combined as it'll be found in your enum-flags-value combination.
        /// </summary>
        /// <param name="Cmd"></param>
        /// <param name="someUIProgressActionOnCmd"></param>
        public void RegisterCommandAction(TUIActionsEnum Cmd, Action<IOperationProgressInfo<TUIActionsEnum>> someUIProgressActionOnCmd)
        {
            CmdActions[Cmd as Enum] = someUIProgressActionOnCmd;
        }


        /// <summary>
        ///  Notify UI with  ProgressInfoTp - [progress action info] struct 
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyProgress(IOperationProgressInfo<TUIActionsEnum> progress)
        {
            Progress = progress;
        }


    }
 
   
   

    

}
