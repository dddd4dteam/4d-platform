﻿using System;
using System.Collections.Generic;
using System.Net;

namespace DDDD.BCL
{

    /// <summary>
    /// On DevAppID new value Setting Notification Model: 
    /// Only Before setting newValue Notification
    /// Only After setting newValue Notification
    /// Before and After setting newValue Notification
    /// </summary>
    public enum NotifyEventModelEn
    {
        NotDefined,
        /// <summary>
        /// Only Before setting newValue Notification 
        /// </summary>
        BeforeChangedOnly, // Changing events firing

        /// <summary>
        /// Only After setting newValue Notification 
        /// </summary>
        AfterChangedOnly,

        /// <summary>
        /// Before and After setting newValue Notification 
        /// </summary>
        BeforeAfterChanged 
    }



    /// <summary>
    /// 
    /// </summary>
    public enum NotifyEventEn
    {
     
        /// <summary>
        /// Before setting newValue  Notification Event
        /// </summary>
        BeforeEvent,

        /// <summary>
        /// After setting newValue  Notification Event
        /// </summary>
        AfterEvent
    }

        


    /// <summary>
    /// 
    /// </summary>
    public class StateNotifier
    {
        #region --------------------- CTOR ------------------------

        public StateNotifier(object Target)
        {
            PropertyNotifyModels = new Dictionary<String, NotifyEventModelEn>();
            EventHandlers = new Dictionary<String, ElasticEvent>();

            //Starting += StateMachine_Starting;
        }
        
	    #endregion --------------------- CTOR ------------------------
        

        #region ---------------- EVENTS -------------------------- 
        
        public event EventHandler Starting;
        public event EventHandler Started;
        public event EventHandler Initialized;
        public event EventHandler Closing;
        public event EventHandler Closed;

        #endregion ---------------- EVENTS --------------------------


        #region  ------------------------------ PROPERTIES -----------------------------
       
        /// <summary>
        /// 
        /// </summary>
        public object Target //WeakReference
        {
            get;
            private set;
        }


        /// <summary>
        /// 
        /// </summary>
        public ElasticType TargetElasticType //WeakReference
        {
            get{

                return Target.GetType().ToElastic();
            }            
        }
        
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, NotifyEventModelEn> PropertyNotifyModels
        {
            get;
           private set;
        }
                    

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, ElasticEvent> EventHandlers
        {
            get;
            set;
        }


#endregion ------------------------------ PROPERTIES -----------------------------
        

        /// <summary>
        ///  Getting Event Key
        /// </summary>
        /// <param name="DevAppID"></param>
        /// <param name="StateValue"></param>
        /// <param name="eventNotifyDetail"></param>
        /// <param name="Delimeter"></param>
        /// <returns></returns>
        private String GetEventKey(String Property, object StateValue, NotifyEventEn eventNotifyDetail, String Delimeter = "=")
        {
            return
                String.Format("{0}{1}{2}.{3}", Property, Delimeter, StateValue.ToString(), eventNotifyDetail.ToString());
        }


        /// <summary>
        /// State DevAppID as Enum  Scenario  
        /// </summary>
        /// <typeparam name="TArg"></typeparam>
        /// <param name="DevAppID"></param>
        /// <param name="StateValue"></param>
        /// <param name="notifyModel"></param>
        /// <param name="beforeEventAndHandler"></param>
        /// <param name="AfterEventAndHandler"></param>
        public void RegisterStateHandlers<TArg>(String Property, Enum StateValue, NotifyEventModelEn notifyModel,                                                                               
                                                                 Tuple<String, EventHandler<TArg> > beforeEventAndHandler = null ,
                                                                 Tuple<String, EventHandler<TArg> > afterEventAndHandler = null)
            where TArg : EventArgs
        {


           //1 Add handler to Event -   Delegate.Combine(EventBefore,) EventBefore.
           //2 
            var TTarget = Target.GetType().ToElastic();

            // Attaching  Handlers to Events
            bool HasBeforeEvent = false;
            bool HasAfterEvent = false;
            
            // Saving items to the private State Call Table
            if (beforeEventAndHandler != null)
            {
                TTarget.Event(beforeEventAndHandler.Item1).Add(beforeEventAndHandler.Item2);
                EventHandlers.Add(
                    GetEventKey(Property, StateValue,NotifyEventEn.BeforeEvent),               
                    TTarget.Event(beforeEventAndHandler.Item1));
                HasBeforeEvent = true;
            }

            if (afterEventAndHandler != null)
            {
                TTarget.Event(afterEventAndHandler.Item1).Add(afterEventAndHandler.Item2);
                EventHandlers.Add(
                    GetEventKey(Property, StateValue, NotifyEventEn.AfterEvent),
                    TTarget.Event(afterEventAndHandler.Item1));
                HasAfterEvent = true;
            }
            
            // Register DevAppID Event Notification Model
            if (HasBeforeEvent && HasAfterEvent == false)
            {  this.PropertyNotifyModels.Add(Property, NotifyEventModelEn.BeforeChangedOnly);
            }
            else if (HasBeforeEvent == false && HasAfterEvent)
            {  this.PropertyNotifyModels.Add(Property, NotifyEventModelEn.AfterChangedOnly);
            }
            else if (HasBeforeEvent && HasAfterEvent)
            {  this.PropertyNotifyModels.Add(Property, NotifyEventModelEn.BeforeAfterChanged);
            }
            
        }

                

        /// <summary>
        /// Process State Changing
        /// </summary>
        /// <param name="DevAppID"></param>
        /// <param name="StateNewValue"></param>
        private void ProcessChangingValue(String Property, object StateNewValue )
        {
            NotifyEventModelEn notifyModel = PropertyNotifyModels[Property];
            switch (notifyModel)
            {
                case NotifyEventModelEn.BeforeChangedOnly:
                    {
                        EventHandlers[GetEventKey(Property, StateNewValue, NotifyEventEn.BeforeEvent)].Raise(new object[] { });

                        TargetElasticType.Property(Property).Set(StateNewValue);
                        break;
                    }
                case NotifyEventModelEn.AfterChangedOnly:
                    {
                        TargetElasticType.Property(Property).Set(StateNewValue);

                        EventHandlers[GetEventKey(Property, StateNewValue, NotifyEventEn.AfterEvent)].Raise(new object[] { });

                        break;
                    }
                case NotifyEventModelEn.BeforeAfterChanged:
                    {
                        EventHandlers[GetEventKey(Property, StateNewValue, NotifyEventEn.BeforeEvent)].Raise(new object[] { });

                        TargetElasticType.Property(Property).Set(StateNewValue);

                        EventHandlers[GetEventKey(Property, StateNewValue, NotifyEventEn.AfterEvent)].Raise(new object[] { });

                        break;
                    }

                default:
                    {
                        TargetElasticType.Property(Property).Set(StateNewValue);

                        break;
                    }

            } 
        }
                

        /// <summary>
        ///  Public member that is used in property setters to process DevAppID changing
        /// </summary>
        /// <param name="DevAppID"></param>
        /// <param name="newValue"></param>
        /// <param name="oldValue"></param>
        public void OnPropertyChanged(String Property, Enum newValue, Enum oldValue)
        {
            if (oldValue != newValue 
                && GetPropertyNotifyModel(Property) != NotifyEventModelEn.NotDefined)

                ProcessChangingValue(Property, newValue);
        }
        

        /// <summary>
        /// DevAppID notify Model
        /// </summary>
        /// <param name="PropertyName"></param>
        /// <returns></returns>
        public NotifyEventModelEn GetPropertyNotifyModel(String PropertyName)
        {
            if (PropertyNotifyModels.ContainsKey(PropertyName))
            {
                return PropertyNotifyModels[PropertyName]; 
            }

            return NotifyEventModelEn.NotDefined;            
        }



    }
}
