using System.Collections.Generic;
using System.IO;
using SevenZSharp.Engines;

namespace SevenZSharp.Decoders
{
   /// <summary>
   /// 
   /// </summary>
   public class DelegationDecoder : CompressionDecoder, IDelegationMethod
   {
      private readonly Dictionary<CompressionFormat, ICompressionEngine> m_engines;

      /// <summary>
      /// 
      /// </summary>
      /// <param name="engines"></param>
      public DelegationDecoder(Dictionary<CompressionFormat, ICompressionEngine> engines)
      {
         m_engines = engines;
      }

      #region IDelegationMethod Members

      /// <summary>
      /// 
      /// </summary>
      public Dictionary<CompressionFormat, ICompressionEngine> Engines
      {
         get { return m_engines; }
      }

      #endregion

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inStream"></param>
      /// <param name="outStream"></param>
      public override void DecodeSingleFile(FileStream inStream, FileStream outStream)
      {
         Engines[CompressionFormat.Unknown].Decoder.DecodeSingleFile(inStream, outStream);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inFile"></param>
      /// <param name="outDirectory"></param>
      public override void DecodeIntoDirectory(string inFile, string outDirectory)
      {
         DelegationMethod.GetCompressionEngine(this, inFile).Decoder.DecodeIntoDirectory(inFile, outDirectory);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inFile"></param>
      /// <param name="outFile"></param>
      public override void DecodeSingleFile(string inFile, string outFile)
      {
         DelegationMethod.GetCompressionEngine(this, inFile).Decoder.DecodeSingleFile(inFile, outFile);
      }
   }
}