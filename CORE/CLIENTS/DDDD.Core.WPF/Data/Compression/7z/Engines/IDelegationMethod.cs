using System;
using System.Collections.Generic;

namespace SevenZSharp.Engines
{
   /// <summary>
   /// 
   /// </summary>
   public interface IDelegationMethod
   {
      /// <summary>
      /// 
      /// </summary>
      Dictionary<CompressionFormat, ICompressionEngine> Engines { get; }
   }

   /// <summary>
   /// 
   /// </summary>
   public static class DelegationMethod
   {
      /// <summary>
      /// 
      /// </summary>
      /// <returns></returns>
      public static ICompressionEngine GetCompressionEngine(IDelegationMethod method, string file)
      {
         CompressionFormat format = CompressionEngine.InferCompressionFormat(file);
         return GetCompressionEngine(method, format);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="method"></param>
      /// <param name="format"></param>
      /// <returns></returns>
      public static ICompressionEngine GetCompressionEngine(IDelegationMethod method, CompressionFormat format)
      {
         ICompressionEngine result;
         if (!method.Engines.TryGetValue(format, out result))
         {
            result = method.Engines[CompressionFormat.Unknown];
         }
         return result;
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="method"></param>
      public static void ThrowImpossibleException(IDelegationMethod method)
      {
         throw new NotImplementedException(
            string.Format(
               "A {0} must by supplied information to understand the {1} (such as a method with a file name); otherwise, either the default {1} must be specified on the {3} or a specific {2} must be used.",
               method.GetType().Name, typeof (CompressionFormat).Name, typeof (ICompressionEngine).Name,
               typeof (DelegationEngine).Name));
      }
   }
}