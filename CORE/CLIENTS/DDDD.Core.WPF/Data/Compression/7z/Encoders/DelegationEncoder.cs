using System.Collections.Generic;
using System.IO;
using SevenZSharp.Engines;

namespace SevenZSharp.Encoders
{
   /// <summary>
   /// 
   /// </summary>
   public class DelegationEncoder : CompressionEncoder, IDelegationMethod
   {
      private readonly Dictionary<CompressionFormat, ICompressionEngine> m_engines;

      /// <summary>
      /// 
      /// </summary>
      /// <param name="engines"></param>
      public DelegationEncoder(Dictionary<CompressionFormat, ICompressionEngine> engines)
      {
         m_engines = engines;
      }

      #region IDelegationMethod Members

      /// <summary>
      /// 
      /// </summary>
      public Dictionary<CompressionFormat, ICompressionEngine> Engines
      {
         get { return m_engines; }
      }

      #endregion

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inStream"></param>
      /// <param name="outStream"></param>
      public override void EncodeSingleFile(FileStream inStream, FileStream outStream)
      {
         Engines[CompressionFormat.Unknown].Encoder.EncodeSingleFile(inStream, outStream);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inDirectory"></param>
      /// <param name="outFile"></param>
      public override void EncodeFromDirectory(string inDirectory, string outFile)
      {
         DelegationMethod.GetCompressionEngine(this, outFile).Encoder.EncodeFromDirectory(inDirectory, outFile);
      }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="inFile"></param>
      /// <param name="outFile"></param>
      public override void EncodeSingleFile(string inFile, string outFile)
      {
         DelegationMethod.GetCompressionEngine(this, outFile).Encoder.EncodeSingleFile(inFile, outFile);
      }
   }
}