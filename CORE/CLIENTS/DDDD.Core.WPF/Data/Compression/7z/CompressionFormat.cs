namespace SevenZSharp
{
   /// <summary>
   /// Well-known compression extension (e.g. 7z, ZIP, RAR, etc.)
   /// </summary>
   public enum CompressionFormat
   {
      /// <summary>
      /// 
      /// </summary>
      Unknown = 0,

      /// <summary>
      /// 
      /// </summary>
      SevenZ,

      /// <summary>
      /// 
      /// </summary>
      Zip,

      /// <summary>
      /// 
      /// </summary>
      Cab,

      /// <summary>
      /// 
      /// </summary>
      Rar,

      /// <summary>
      /// 
      /// </summary>
      Arj,

      /// <summary>
      /// 
      /// </summary>
      Lzh,

      /// <summary>
      /// 
      /// </summary>
      Chm,

      /// <summary>
      /// 
      /// </summary>
      Gzip,

      /// <summary>
      /// 
      /// </summary>
      Bzip2,

      /// <summary>
      /// 
      /// </summary>
      Z,

      /// <summary>
      /// 
      /// </summary>
      Tar,

      /// <summary>
      /// 
      /// </summary>
      Cpio,

      /// <summary>
      /// 
      /// </summary>
      Rpm,

      /// <summary>
      /// 
      /// </summary>
      Deb,

      /// <summary>
      /// 
      /// </summary>
      Split,

      /// <summary>
      /// 
      /// </summary>
      Iso,

      /// <summary>
      /// 
      /// </summary>
      Nsis
   }
}