using System.IO;

namespace SevenZSharp
{
   /// <summary>
   /// 
   /// </summary>
   public interface ICompressionEncoder : ICompressionMethod
   {
      /// <summary>
      /// Encodes a single file. <paramref name="inStream"/> is the non-encoded
      /// file which will be encoded and placed into <paramref name="outStream"/>.
      /// If <paramref name="outStream"/> does not exist, it will be created.
      /// </summary>
      /// <param name="inStream">The in stream.</param>
      /// <param name="outStream">The out stream.</param>
      void EncodeSingleFile(FileStream inStream, FileStream outStream);

      /// <summary>
      /// Encodes a single file. <paramref name="inFile"/> is the path to the non-encoded
      /// file which will be encoded and placed into <paramref name="outFile"/>.
      /// If <paramref name="outFile"/> does not exist, it will be created.
      /// </summary>
      /// <param name="inFile">The in file.</param>
      /// <param name="outFile">The out file.</param>
      void EncodeSingleFile(string inFile, string outFile);

      /// <summary>
      /// Recursively encodes all files in the specified <paramref name="inDirectory"/> and
      /// writes the archive into the specified <paramref name="outFile"/>. If <paramref name="outFile"/>
      /// does not exist, it will be created.
      /// 
      /// Supports: 7z (.7z), ZIP (.zip), GZIP (.gz), BZIP2 (.bz2) and TAR (.tar)
      /// </summary>
      /// <param name="inDirectory">The in directory.</param>
      /// <param name="outFile">The out file.</param>
      void EncodeFromDirectory(string inDirectory, string outFile);
   }

   /// <summary>
   /// 
   /// </summary>
   public abstract class CompressionEncoder : ICompressionEncoder
   {
      #region ICompressionEncoder Members

      /// <summary>
      /// Encodes a single file. <paramref name="inStream"/> is the non-encoded
      /// file which will be encoded and placed into <paramref name="outStream"/>.
      /// If <paramref name="outStream"/> does not exist, it will be created.
      /// </summary>
      /// <param name="inStream">The in stream.</param>
      /// <param name="outStream">The out stream.</param>
      public abstract void EncodeSingleFile(FileStream inStream, FileStream outStream);

      /// <summary>
      /// Encodes a single file. <paramref name="inFile"/> is the path to the non-encoded
      /// file which will be encoded and placed into <paramref name="outFile"/>.
      /// If <paramref name="outFile"/> does not exist, it will be created.
      /// </summary>
      /// <param name="inFile">The in file.</param>
      /// <param name="outFile">The out file.</param>
      public virtual void EncodeSingleFile(string inFile, string outFile)
      {
         using (FileStream inStream = new FileStream(inFile, FileMode.Open, FileAccess.Read))
         {
            using (FileStream outStream = new FileStream(outFile, FileMode.OpenOrCreate, FileAccess.Write))
            {
               EncodeSingleFile(inStream, outStream);
            }
         }
      }

      /// <summary>
      /// Recursively encodes all files in the specified <paramref name="inDirectory"/> and
      /// writes the archive into the specified <paramref name="outFile"/>. If <paramref name="outFile"/>
      /// does not exist, it will be created.
      /// </summary>
      /// <param name="inDirectory">The in directory.</param>
      /// <param name="outFile">The out file.</param>
      public abstract void EncodeFromDirectory(string inDirectory, string outFile);

      #endregion
   }
}