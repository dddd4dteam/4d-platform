﻿using System;

using DDDD.Core.Extensions;
using DDDD.Core.Resources;
using DDDD.Core.ComponentModel;

namespace DDDD.Core.Diagnostics
{


    /// <summary>
    /// Operation Invoke - is  Development primitive, focused on custom Action, and presented as wrap method. With the help of this method we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.    
    /// <para/>            If you set LogOnError - then logging will be also activated  on errors
    /// </summary>
    public static class OperationInvoke
    {

        #region ---------------------- DEFAULT APP LOGGER ---------------------------


        static DCLogger _Logger = null;
        static DCLogger Logger
        {
            get
            {
                if (_Logger.IsNull())
                {
                    _Logger = DCLogger.AddOrUseExist_EmptyLogger(DCLogger.DefaultLogKey);  //standart primary    File targed Logger for Application.
                }
                return _Logger;
            }
        }


        #endregion ---------------------- DEFAULT APP LOGGER ---------------------------


        /// <summary>
        /// In [DEBUG] mode, if you set LogOnError to true , then Logging will be activated for errors.  
        /// </summary>
        public static bool LogOnError
        { get; set; } = true;





        /// <summary>        
        /// Calling custom Operation Code Block In DEBUG Mode - here we have try/Catch construction with [operationAction], or In RELEASE Mode -here we have direct call of [operationAction].
        /// </summary>        
        /// <param name="operationMethodName"></param>
        /// <param name="operationAction"></param>
        /// <param name="className"></param>
        public static void CallInMode(string className,  string operationMethodName, Action operationAction, string errorDetailMessage = null)
        {

#if DEBUG
            try
            {
                operationAction();
            }
            catch (Exception exc)
            {
                if (LogOnError)
                { Logger.Error(exc.Message);  }

                throw new InvalidOperationException(RCX.ERR_InvalidOperation
                          .Fmt(className, operationMethodName, errorDetailMessage + " Exception Message :[ " + exc.Message + "]")
                          );
            }
#else
            operationAction();
#endif
        }


        /// <summary>
        /// Calling custom Operation Code Block In DEBUG Mode - here we have try/Catch construction with [operationAction], or In RELEASE Mode -here we have direct call of [operationAction].
        /// </summary>
        /// <param name="className"></param>
        /// <param name="operationMethodName"></param>
        /// <param name="operationAction"></param>
        /// <param name="exceptionAction"></param>
        /// <param name="errorDetailMessage"></param>
        public static void CallInMode(string className, string operationMethodName, Action operationAction, Action exceptionAction, string errorDetailMessage = null)
        {

#if DEBUG
            try
            {
                operationAction();
            }
            catch (Exception exc)
            {
                exceptionAction();

                if (LogOnError)
                { Logger.Error(exc.Message); }

                throw new InvalidOperationException(RCX.ERR_InvalidOperation
                          .Fmt(className, operationMethodName, errorDetailMessage + " Exception Message :[ " + exc.Message + "]")
                          );
            }
#else
            operationAction();
#endif
        }




        /// <summary>
        /// Calling custom Operation Code Block In DEBUG Mode - here we have try/Catch construction with [operationAction], or In RELEASE Mode -here we have direct call of [operationAction].
        /// </summary>
        /// <typeparam name="TRes"></typeparam>
        /// <param name="operationMethodName"></param>
        /// <param name="operationFunc"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static TRes CallInMode<TRes>(string className, string operationMethodName, Func<TRes> operationFunc, string errorDetailMessage = null, bool throwOnError = true)
        {

#if DEBUG
            try
            {
                return operationFunc();
            }
            catch (Exception exc)
            {
                if (LogOnError)
                { Logger.Error(exc.Message); }

               if (throwOnError)
               {   throw new InvalidOperationException(RCX.ERR_InvalidOperation
                          .Fmt(className, operationMethodName, errorDetailMessage + " Exception Message :[ " + exc.Message + "]")
                          );
               }
               return default(TRes);
            }
#else
            return operationFunc();
#endif

        }



        #region ----------------------- CALL WITH [USE OR NOT LOCK] WITH DEBUG/RELEASE MODE TOO -------------------------------


        /// <summary>
        /// Call operation with option  [useThreadSafetyWay], also with DEBUG or RELEASE mode.
        /// </summary>
        /// <param name="className"></param>
        /// <param name="operationMethodName"></param>
        /// <param name="useThreadSafetyWay"></param>
        /// <param name="locker"></param>
        /// <param name="checkLockingFunc"></param>
        /// <param name="operationAction"></param>
        /// <param name="exceptionAction"></param>
        /// <param name="errorDetailMessage"></param>
        public static void CallWithLockAndModes(string className, string operationMethodName ,bool useThreadSafetyWay, object locker, Func<bool> checkLockingFunc, Action operationAction,   Action exceptionAction = null, string errorDetailMessage = null)
        {

#if DEBUG
            try
            {
                if (useThreadSafetyWay == true )
                {
                    locker.LockAction(checkLockingFunc, operationAction); 
                }
                else
                {   if (checkLockingFunc() == true )
                    {
                        operationAction();
                    }
               }
                
            }
            catch (Exception exc)
            {
                exceptionAction();

                if (LogOnError)
                { Logger.Error(exc.Message); }

                throw new InvalidOperationException(RCX.ERR_InvalidOperation
                          .Fmt(className, operationMethodName, errorDetailMessage + " Exception Message :[ " + exc.Message + "]")
                          );
            }
#else
                if (  useThreadSafetyWay == true )
                {
                    locker.LockAction(checkLockingFunc, operationAction); 
                }
                else
                {   if (checkLockingFunc() == true )
                    {
                        operationAction();
                    }
               }
#endif
        }





        #endregion----------------------- CALL WITH [USE OR NOT LOCK] WITH DEBUG/RELEASE MODE TOO -------------------------------







#if CLIENT && SL5

        /// <summary>
        /// Calling custom Operation Code Block only if - App has Elevated Permission - check.
        /// </summary>
        /// <typeparam name="TRes"></typeparam>
        /// <param name="className"></param>
        /// <param name="operationMethodName"></param>
        /// <param name="operationFunc"></param>
        /// <returns></returns>
        public static TRes CallWithEPCheckMode<TRes>(string className, string operationMethodName, Func<TRes> operationFunc, string errorDetailMessage = null)
        {          
            Validator.ATInvalidOperationDbg( (System.Windows.Application.Current.HasElevatedPermissions == false) ,
                className, operationMethodName, " This  operation can be done only with Elevated Permissions");

            //Validator.ATInvalidOperationDbg((System.Windows.Application.Current.IsRunningOutOfBrowser == false),
            //   className, operationMethodName, " This  operation can be done only in OutOfBrowserMode ");

#if DEBUG
            try
            {
                return operationFunc();
            }
            catch (Exception exc)
            {
                if (LogOnError)
                { Logger.Error(exc.Message); }

                throw new InvalidOperationException(RCX.ERR_InvalidOperation
                          .Fmt(className, operationMethodName, errorDetailMessage + " Exception Message :[ " + exc.Message + "]")
                          );
            }
#else
            return operationFunc();
#endif
        }


#endif // CLIENT && SL5




    }
}
