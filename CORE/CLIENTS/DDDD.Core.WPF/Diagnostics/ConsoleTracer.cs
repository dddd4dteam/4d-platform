﻿using System;
using System.Diagnostics;
using DDDD.Core.Extensions;
using DDDD.Core.Resources;

namespace DDDD.Core.Diagnostics
{

#if NET45 

    /// <summary>
    /// Tracer - this class use the System.Diagnostics.Trace class options with some additions:
    /// <para/>  1 Feature -  Tracer can use  MessageInspector for messaging based on OperationContext. 
    /// <para/>  2 Feature -  Tracer contains methods that can be called only in DEBUG Conditionals.
    /// </summary>
    public class ConsoleTracer
    {
        #region ----------------------------------- CTOR ---------------------------------

        static ConsoleTracer()
        {
            
        }

        #endregion ----------------------------------- CTOR ---------------------------------


        #region ------------------------------------- FIELDS ---------------------------------

        static readonly object _locker = new object();
        static readonly ConsoleTraceListener Listener = new ConsoleTraceListener();

        #endregion ------------------------------------- FIELDS ---------------------------------
        

        
        /// <summary>
        /// Tracing line into the Console.
        /// Template: [ TRACE INFO : Context- [{0}] | ...custom message...]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="operationName"></param>
        /// <param name="templateMessage"></param>
        /// <param name="templateReplacemens"></param>
        [Conditional("DEBUG")]
        public static void TraceInfoDbg<T>(string operationName, string templateMessage, params  string[] templateReplacemens)
        {

            lock (_locker)
            {
                var operationContext = MessageInspector.GetOperationContext<T>(operationName);
                var InfoMessage = " TRACE INFO : Context- [{0}] | ".FmtEx(operationContext.Item2, operationContext.Item2);

                InfoMessage += templateMessage.Fmt(templateReplacemens);
                                
                Listener.WriteLine(InfoMessage);

            }
        }

        /// <summary>
        /// Tracing line into the Console.
        /// Template: [ TRACE ERROR : Context- [{0}] | ...custom message...]        
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="operationName"></param>
        /// <param name="templateMessage"></param>
        /// <param name="templateReplacemens"></param>
        [Conditional("DEBUG")]
        public static void TraceErrorDbg<T>(String operationName, String templateMessage, params  string[] templateReplacemens)
        {
            lock (_locker)
            {
                var operationContext = MessageInspector.GetOperationContext<T>(operationName);
                var InfoMessage = @" TRACE ERROR: Context- [{0}] | ".FmtEx(operationContext.Item2, operationContext.Item2);

                InfoMessage += templateMessage.Fmt(templateReplacemens);
                
                Listener.WriteLine(InfoMessage);

            }
        }
        


        #region -------------------------------- Assert IN DEBUG ------------------------------------
        
        /// <summary>
        /// Checks for a condition , and then write true or false inside message. Only in DEBUG  mode.
        /// Template: [Context -[{0}] || ChekName- [{1}] ||Check Result- [{2}]  ]
        /// </summary>     
        /// <param name="condition"></param>
        /// <param name="args"></param>
        [Conditional("DEBUG")]
        public static void AssertDbg<T>( bool condition, String operationName, String CheckName)           
        {
            lock (_locker)
            {
                var operationContext = MessageInspector.GetOperationContext<T>(operationName);
                var template = MessageInspector.GetMessageTemplate(operationContext);

                /// ResKey : Tracer_AssertDbg |||  Context -[{0}] || ChekName- [{1}] || Check Result- [{2}]  ||
                ///                 + Trace Output Options - DateTime;TimeStamp...

                Listener.WriteLine(template.Fmt(operationContext.Item2, CheckName, condition.S()));                
            }
        
        }


        /// <summary>
        ///  Checks for a condition , and then write true or false inside message. Only in DEBUG  mode.
        ///  Default Template: [ Context -[{0}] || ChekName- [{1}] ||Check Result- [{2} ]  "        
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="condition"></param>
        /// <param name="operationName"></param>
        /// <param name="CheckName"></param>
        [Conditional("DEBUG")]
        public static void AssertDbg<TDelegate>(bool condition, TDelegate operationName, String CheckName)
        {
            lock (_locker)
            {
                var operationContext = MessageInspector.GetOperationContext<TDelegate>(operationName);
                var template = MessageInspector.GetMessageTemplate(operationContext);

                /// ResKey : Tracer_AssertDbg |||  Context -[{0}] || ChekName- [{1}] || Check Result:[{2}]  ||
                ///                 + Trace Output Options - DateTime;TimeStamp...

                Listener.WriteLine( 
                                    template.Fmt(operationContext.Item2, CheckName, condition.S())
                                  );

            }

        }


        #endregion --------------------------------Assert IN DEBUG ------------------------------------



      
    }


   
#endif 




}
