﻿using System;
using System.Diagnostics;
using DDDD.Core.Resources;
using DDDD.Core.Extensions;
using DDDD.Core.Exceptions;
using DDDD.Core.Reflection;



namespace DDDD.Core.Diagnostics
{


    


    /// <summary>
    /// Validator for custom condition checks with custom Exception types. 
    /// <para/> Validator let us to raise condition checks not only in DEBUG mode.
    /// <para/> Methods with name like this - [methodName]dbg - with 'dbg' suffix will be called only in DEBUG Conditionals.
    /// <para/> Has methods overload which let us to use of Localized Messages- with the help of ResourceLoader   
    /// </summary>
    public class Validator
     {

        // logger = new Log();

        #region -------------------------------- CTOR --------------------------------

         static Validator()
         {

         }

         #endregion -------------------------------- CTOR --------------------------------


         #region ---------------------------- PROPERTIES --------------------------

         /// <summary>
         /// This flag tells to Validator - trace ERROR by ConsoleTracer on some validation check when exception will be thrown, or not. 
         /// </summary>
         public static bool UseValidationErrorTracing
         {
             get;
             internal set;
         }

         /// <summary>
         /// This flag tells to Validator - trace INFO by ConsoleTracer on some validation check when Condition will be successfully, or not.
         /// </summary>
         public static bool UseValidationSuccessTracing
         {
             get;
             internal set;
         }

        #endregion ---------------------------- PROPERTIES --------------------------



        #region --------------------------- OPERATION CODE BLOCK --------------------------------
        
        /// <summary>
        /// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
        /// </summary>
        /// <param name="className"></param>
        /// <param name="operationMethodName"></param>
        /// <param name="operationAction"></param>
        public static void OperationCodeBlock(string className, string operationMethodName, Action operationAction)
        {

#if DEBUG
            try
            {
                operationAction();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(" ERROR in {0}.{1}(): message - [{2}]"
                          .Fmt(className, operationMethodName, exc.Message)
                          );
            }
#else
            testAction();
#endif

        }



        /// <summary>
        /// Operation Code Block Wrap method. With this method help we can build Try/Catch in DEBUG mode and  do not use Try/Catch,but call action directly in RELEASE  mode.
        /// Here className is [nameof(TypeActivator)].
        /// </summary>
        /// <typeparam name="TRes"></typeparam>
        /// <param name="className"></param>
        /// <param name="operationMethodName"></param>
        /// <param name="operationFunc"></param>
        /// <returns></returns>
        public static TRes OperationCodeBlock<TRes>(string className, string operationMethodName, Func<TRes> operationFunc)
        {

#if DEBUG
            try
            {
                return operationFunc();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(" ERROR in {0}.{1}(): message - [{2}]"
                          .Fmt(className, operationMethodName, exc.Message)
                          );
            }
#else
                return operationFunc();
#endif

        }



        #endregion --------------------------- OPERATION CODE BLOCK --------------------------------


        #region ------------------------- EXCEPTIONS THROWING --------------------------

        /// <summary>
        /// Creating Exception by cached ctor delegate. 
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="args"></param>
        /// <returns></returns>   
        public static Exception CreateException<TException>(params object[] args)
            where TException : Exception
        {
            return TypeActivator.CreateInstanceT<TException>(TypeActivator.DefaultCtorSearchBinding, args);
        }

        #endregion ------------------------- EXCEPTIONS THROWING --------------------------


        #region --------------------------------- ASSERT FALSE --------------------------------------


        /// <summary>
        /// Checks for a condition; if the condition is false, displays the specified message.
        /// </summary>
        /// <typeparam name="TException"> The type of exception that invokes condition check.</typeparam>
        /// <param name="condition">The conditional expression to test.</param>
        /// <param name="userMessage">TException ctor  Message parameter.</param>
        public static void AssertFalse<TException>(bool condition, string userMessage)
             where TException : Exception
         {
             if (condition)
                 return;
             var args = new object[] { userMessage };
             throw ExceptionsService.CreateException<TException>(args);
         }

         /// <summary>
         /// Checks for a condition; if the condition is false, displays the specified message.
         /// </summary>
         /// <typeparam name="TException">The type of exception that invokes condition check.</typeparam>
         /// <param name="condition">The conditional expression to test.</param>
         /// <param name="args">TException ctor parameters.</param>
         public static void AssertFalse<TException>(bool condition, params object[] args)
             where TException : Exception
         {
             if (condition)
                 return;
             throw ExceptionsService.CreateException<TException>(args);
         }

         #endregion --------------------------------- ASSERT FALSE --------------------------------------


         #region ---------------------------------- ASSERT FALSE DEBUG ONLY --------------------------------


         /// <summary>
         /// Checks for a condition; if the condition is false, displays the specified message. Only in DEBUG  mode.
         /// </summary>
         /// <typeparam name="TException"></typeparam>
         /// <param name="condition"></param>
         /// <param name="userMessage"></param>
         [Conditional("DEBUG")]
         public static void AssertFalseDbg<TException>(bool condition, string userMessage)
             where TException : Exception
         {

             if (condition)
                 return;
             var args = new object[] { userMessage };
             throw ExceptionsService.CreateException<TException>(args);
         }


         /// <summary>
         ///  Checks for a condition; if the condition is false, displays the specified message. Only in DEBUG  mode.
         /// </summary>
         /// <typeparam name="TException"></typeparam>
         /// <param name="condition"></param>
         /// <param name="args"></param>
         [Conditional("DEBUG")]
         public static void AssertFalseDbg<TException>(bool condition, params object[] args)
             where TException : Exception
         {
             if (condition)
                 return;
             throw ExceptionsService.CreateException<TException>(args);
         }



         #endregion ---------------------------------- ASSERT FALSE DEBUG ONLY --------------------------------


         #region ------------------------------- ASSERT TRUE ---------------------------------


         /// <summary>
         ///  Checks for a condition; if the condition is true, displays the specified message.
         /// </summary>
         /// <typeparam name="TException"></typeparam>
         /// <param name="condition"></param>
         /// <param name="userMessage"></param>     
         public static void AssertTrue<TException>(bool condition, string userMessage)
             where TException : Exception
         {
             if (condition)
             {
                 var args = new object[] { userMessage };
                 throw ExceptionsService.CreateException<TException>(args);
             }
         }

         /// <summary>
         /// Checks for a condition; If the condition is true, throw Exception with args parameters.
         /// </summary>
         /// <typeparam name="TException"></typeparam>
         /// <param name="condition"></param>
         /// <param name="args"></param>
         public static void AssertTrue<TException>(bool condition, params object[] args)
             where TException : Exception
         {
             if (condition)
                 throw ExceptionsService.CreateException<TException>(args);
         }


        /// <summary>
        /// Checks for a condition; if the condition is true, throw Exception with args parameters.
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="condition"></param>
        /// <param name="args"></param>
        public static void AssertTrue<TException>(bool condition, string className, string operation, string customMessage)
            where TException : Exception
        {
            if (condition)
                throw ExceptionsService.CreateException<TException>(RCX.ERR_SomeErrorCustomMsg.Fmt(className,operation,customMessage)   );
        }


        #endregion ------------------------------- ASSERT TRUE ---------------------------------


        #region --------------------------------- ASSERT TRUE DEBUG ------------------------------


        /// <summary>
        /// Checks for a condition; if the condition is true, displays the specified message. 
        /// Works in DEBUG MODE ONLY. 
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="condition"></param>
        /// <param name="userMessage"></param>
        [Conditional("DEBUG")]
         public static void AssertTrueDbg<TException>(bool condition, string userMessage)
             where TException : Exception
         {
             if (condition)
             {
                 var args = new object[] { userMessage };
                 throw ExceptionsService.CreateException<TException>(args);
             }
         }




        /// <summary>
        /// Checks for a condition; if the condition is true, throw Exception with args parameters.
        /// Works in DEBUG MODE ONLY. 
        /// </summary>
        /// <typeparam name="TException"></typeparam>
        /// <param name="condition"></param>
        /// <param name="args"></param>
        [Conditional("DEBUG")]
         public static void AssertTrueDbg<TException>(bool condition, params object[] args)
            where TException : Exception
         {
             if (condition)
                 throw ExceptionsService.CreateException<TException>(args);
         }

         #endregion --------------------------------- ASSERT TRUE DEBUG ------------------------------
         

         #region ------------------------------ TRACING MESSAGES IN DEBUG --------------------------------
         

         /// <summary>
         /// TraceModeEn - Trace Messaging Output target
         /// </summary>
         public enum TraceModeEn
         {   
             /// <summary>
             /// Use System.Console as output message target
             /// </summary>
             Console             
             , 
             /// <summary>
             /// Use System.Windows.MessageBox as output message target
             /// </summary>
             MessageBox
         }


         /// <summary>
         /// Trace Mode for Trace messaging : 
         ///            Console - tracing messages to the Console
         ///            , MessageBox - tracing messages throw the MessageBox Window.
         /// </summary>
         public static TraceModeEn TraceMode = TraceModeEn.Console;
         

     
         /// <summary>
         /// Trace some warning message based on the TraceMode: to the Console or by the MessageBox window.
         /// It will be done only in DEBUG mode.
         /// </summary>
         /// <param name="userMessage"></param>
         [Conditional("DEBUG")]
         public static void TraceDbg(string userMessage)             
         {
             if (TraceMode == TraceModeEn.Console)
             {
                 Console.WriteLine(userMessage);       
             }
             else 
             {
                 System.Windows.MessageBox.Show(userMessage);
             }
         }

           /// <summary>
         /// Trace some warning message into Console if Condition is true,and it will be done only in DEBUG mode.
         /// </summary>
         /// <param name="condition"></param>
         /// <param name="userMessage"></param>
         [Conditional("DEBUG")]
         public static void TraceTrueDbg(bool condition, string userMessage)
         {
            if (!condition) return;//false condition

            //if condition is true - trace message to target output
            TraceDbg(userMessage);
                                       
         } 


         /// <summary>
         /// Trace some warning message into Console if Condition is false,and it will be done only in DEBUG mode.
         /// </summary>
         /// <param name="condition"></param>
         /// <param name="userMessage"></param>
         [Conditional("DEBUG")]
         public static void TraceFalseDbg(bool condition, string userMessage)
         {
             if (condition) return; // true condition

             //if condition is false - trace message to target output 
             TraceDbg(userMessage);
          
         }

        #endregion ------------------------------ TRACING MESSAGES IN DEBUG --------------------------------



        #region --------------------ASSERT NULLREFERENCE ARG, INVALIDOPERATION IN DEBUG ------------------------------


        /// <summary>
        ///  AT(Assert True ). If  nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException. 
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// Works in DEBUG MODE ONLY. 
        /// </summary>
        /// <param name="nullChekingItem"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        [Conditional("DEBUG")]
        public static void ATNullReferenceArgDbg(object nullChekingItem, string className, string methodName, string parameterName)
        {
            if (nullChekingItem.IsNull())
            {
                throw new NullReferenceException(RCX.ERR_NullReferenceValue.Fmt(className, methodName, parameterName));
            }
        }

        /// <summary>
        /// AT(Assert True ).ATNullReference - Check If nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException.   
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// Works in RELEASE MODE TOO. 
        /// </summary>
        /// <param name="nullChekingItem"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        public static void ATNullReference(object nullChekingItem, string className, string methodName, string parameterName)
        {
            if (nullChekingItem.IsNull())
            {
                throw new NullReferenceException(RCX.ERR_NullReferenceValue.Fmt(className, methodName, parameterName ) );
            }
        }


        /// <summary>
        /// AT(Assert True ).ATNullReference - Check If nullChekingItem IS NULL -(TRUE that nullChekingItem.IsNull()) then throw NullReferenceException.  
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// Works in RELEASE MODE TOO. 
        /// </summary>
        /// <param name="nullChekingItem"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        /// <param name="customMessage"></param>
        public static void ATNullReferenceCustomMsg(object nullChekingItem, string className, string methodName, string parameterName, string customMessage)
        {
            if (nullChekingItem.IsNull())
            {
                throw new NullReferenceException(RCX.ERR_NullReferenceValueCustomMsg.Fmt(className, methodName, parameterName, customMessage));
            }
        }



        /// <summary>
        /// AT(Assert True ).If condition is TRUE - then throw InvalidOperationException. Works only in DEBUG mode.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): invalid operation - [{2}].".
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="userMessage"></param>
        [Conditional("DEBUG")]
        public static void ATInvalidOperationDbg(bool condition, string className, string methodName, string userMessage)
        {
            if (condition)
            {
                throw new InvalidOperationException(RCX.ERR_InvalidOperation.Fmt(className, methodName, userMessage));
            }
        }



        #endregion --------------------ASSERT NULLREFERENCE ARG, INVALIDOPERATION IN DEBUG ------------------------------


        #region ------------------------------- ASSERT NULLOREMPTY STRING ARG --------------------------------------
        /// <summary>
        /// AT(Assert True ).ATNullOrEmptyArgDbg - chech If string argument is null or empty - if(TRUE that stringChekingItem.IsNullOrEmpty()) then throw ArgumentException.  
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// Works in DEBUG MODE ONLY. 
        /// </summary>
        /// <param name="stringChekingItem"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>
        [Conditional("DEBUG")]
        public static void ATNullOrEmptyArgDbg(string stringChekingItem, string className, string methodName, string parameterName)
        {
            if (stringChekingItem.IsNullOrEmpty())
            {
                throw new  ArgumentException(RCX.ERR_NullOrEmptyStringValue.Fmt(className, methodName, parameterName));
            }
        }

        /// <summary>
        /// AT(Assert True ).ATNullOrEmptyArg - chech If string argument is null or empty - if(TRUE that stringChekingItem.IsNullOrEmpty()) then throw ArgumentException.
        /// Message Template will be formatted as : "ERROR in {0}.{1}(): argument [{2}] value cannot be null.".
        /// Works in RELEASE MODE TOO.
        /// </summary>
        /// <param name="stringChekingItem"></param>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="parameterName"></param>     
        public static void ATNullOrEmptyArg(string stringChekingItem, string className, string methodName, string parameterName)
        {
            if (stringChekingItem.IsNullOrEmpty())
            {
                throw new ArgumentException(RCX.ERR_NullOrEmptyStringValue.Fmt(className, methodName, parameterName ));
            }
        }


        #endregion ------------------------------- ASSERT NULLOREMPTY STRING ARG --------------------------------------


        #region --------------------------------- KNOWN CONDITION CHECKS ---------------------------------------

         const string Validator_NotNullDbg_Class = " Error In [{0}] Operation : Argument [{1}]  Cannot have null  reference ";
         const string Validator_NotNullDbg_NullableStruct = " Error In [{0}] Operation : Argument [{1}]  Cannot have null  value ";
         const string Validator_NotNullOrEmptyStringDbg = " Error In [{0}] Operation : string Argument [{1}]  Cannot have null or emplty value ";
         const string Validator_NotNullOrWhiteSpaceStringDbg = " Error In [{0}] Operation : string Argument [{1}]  Cannot have null or whitespace value ";



        #region ------------------------------------- NotNull Check IN DEBUG ------------------------------------

        // ResKey : Validator_NotNullDbg : Error in context -[{0}] - checkValue Cannot be Null
        // ResKey : Validator_NotNullDbg : Error in context -[{0}] - Value Cannot be Null
        // Template: {Key- "Validator_NotNullDbg" ; Value- "Error in context -[{0}] - Value Cannot be Null" } 

        /// <summary>
        /// NotNull validation Check - check that object checkValue is not null. Works only in DEBUG mode.
        /// If value is null then throw NullReferenceException.          
        /// For Message we use only custom user Message - without getting template from Default ResourceManager. 
        /// Message - custom  Message template with templateReplacements parameters.
        /// If templateReplacements.Length == 0 then formatting won't be done.
        /// </summary>
        /// <param name="checkValue"></param>
        /// <param name="Message"></param>
        /// <param name="templateReplacements"></param>
        [Conditional("DEBUG")]
         public static void NotNullCustMsgDbg(object checkValue, String Message, params string[] templateReplacements)
         {
            
             AssertTrueDbg<NullReferenceException>((checkValue == null),                  
                                                    Message.Fmt(templateReplacements));
         }



         
         /// <summary>
         /// NotNull validation Check - check that object checkValue is not null. Works only in DEBUG mode.
         /// If value is null then throw NullReferenceException.
         /// To get Message we use template from Default ResourceManager by Validator class.
         /// Message template Key: [T-Validator] + [Operation Name - Operation].                        
         /// Message Template will be formatted with  OperationContext.
         /// </summary>
         /// <param name="checkValue"></param>
         /// <param name="operation"></param>         
         [Conditional("DEBUG")]
         internal static void NotNullDbg(object checkValue, string operation)
         {
             var operationContext = MessageInspector.GetOperationContext<Validator>(operation, point: null);
            
             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);

             AssertTrueDbg<NullReferenceException>( (checkValue == null),
                 msgTemplate.FmtEx(Context: operationContext.Item2, inparams: operationContext.Item2));
         }





        /// <summary>
        /// NotNull validation Check - check that object checkValue is not null. Works only in DEBUG mode.
        /// If value is null then throw NullReferenceException.
        /// To get Message we use template from Default ResourceManager by Validator class.
        /// Message template Key : [T-ContextType] + [Operation Name - Operation] + [Point].  
        /// Message Template will be formatted with  templateReplacements parameters.
        /// If templateReplacements.Length == 0 then formatting won't be done.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="checkValue"></param>
        /// <param name="operation"></param>
        /// <param name="point"></param>
        /// <param name="templateReplacements"></param>
        [Conditional("DEBUG")]
         public static void NotNullDbg<T>(object checkValue, string operation, string point = null, params string[] templateReplacements)
         {
             var operationContext = MessageInspector.GetOperationContext<T>(operation, point);
                         
             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);
                         
             AssertTrueDbg<NullReferenceException>((checkValue == null),
                 msgTemplate.FmtEx(Context: operationContext.Item2, inparams: templateReplacements));                                                    
         }


        /// <summary>
        /// NotNull validation Check - check that object checkValue is not null. Works only in DEBUG mode.
        /// If value is null then throw NullReferenceException.
        /// To get Message we use template from Default ResourceManager by Validator class.
        /// Message template Key : [T-ContextType] + [Operation Name - Operation] + [Point].  
        /// Message Template will be formatted with  templateReplacements parameters.
        /// If templateReplacements.Length == 0 then formatting won't be done.
        /// </summary>
        /// <param name="checkValue"></param>
        /// <param name="contextType"></param>
        /// <param name="contextOperation"></param>
        /// <param name="contextPoint"></param>
        /// <param name="templateReplacements"></param>
        [Conditional("DEBUG")]
         public static void NotNullDbg(object checkValue, Type contextType, String contextOperation, String contextPoint = null, params string[] templateReplacements)
         {
             //if checkValue == null throw error
             //if contextType == null throw error
             //if contextOperation == null throw error

             var operationContext = MessageInspector.GetOperationContext(contextType, contextOperation, contextPoint);

             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);


             AssertTrueDbg<NullReferenceException>((checkValue == null),
                 msgTemplate.FmtEx(Context: operationContext.Item2, inparams: templateReplacements));

         }


       
         /// <summary>
         /// Check that object value is not null. If value is null then throw NullReferenceException.
         /// Message template Key : [TDelegate.ContextType] + [TDelegate.Operation] + [Point].  
         /// Message Template will be formatted with  templateReplacements parameters.
         /// If templateReplacements.Length == 0 then formatting won't be done.
         /// </summary>
         /// <typeparam name="TDelegate"></typeparam>
         /// <param name="checkValue"></param>
         /// <param name="Operation"></param>
         /// <param name="Point"></param>
         /// <param name="templateReplacements"></param>
         [Conditional("DEBUG")]
         public static void NotNullDbg<TDelegate>(object checkValue, TDelegate operation, String point = null, params string[] templateReplacements)
         {
             var operationContext = MessageInspector.GetOperationContext<TDelegate>(operation, point);

             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);
                      
             AssertTrueDbg<NullReferenceException>((checkValue == null),
                msgTemplate.FmtEx(Context: operationContext.Item2, inparams: templateReplacements));
             
         }


         #endregion ------------------------------------- NotNull Check IN DEBUG ------------------------------------




         #region -------------------------------- NotNullOrEmpty Check IN DEBUG -------------------------------------

         /// <summary>
         /// Check that string value is not null or empty
         /// </summary>
         /// <param name="value"></param>
         //[Conditional("DEBUG")]
         //public static void NotNullOrEmptyDbg(string value, String OperationName, String VariableName)
         //{
         //    AssertTrueDbg<ArgumentNullException>(string.IsNullOrEmpty(value),
         //                                          ErrTemplate_NotNullOrEmptyStringDbg.Fmt(OperationName, VariableName) );
         //}



         /// ResKey : Validator_NotNullOrEmptyDbg : Error in context -[{0}] - checkValue Cannot be Null
         /// ResKey : Validator_NotNullDbg : Error in context -[{0}] - Value Cannot be Null
         /// Template: {Key- "Validator_NotNullDbg" ; Value- "Error in context -[{0}] - Value Cannot be Null" } 


         /// <summary>
         /// NotNullOrEmpty string validation Check - check that string checkValue is not null. Works only in DEBUG mode.
         /// If value is null then throw NullReferenceException.          
         /// For Message we use only custom user Message - without getting template from Default ResourceManager. 
         /// Message - custom  Message template with templateReplacements parameters.
         /// If templateReplacements.Length == 0 then formatting won't be done.
         /// </summary>
         /// <param name="checkValue"></param>
         /// <param name="Message"></param>
         [Conditional("DEBUG")]
         public static void NotNullOrEmptyCustMsgDbg(string checkValue, String Message, params string[] templateReplacements)
         {

             AssertTrueDbg<ArgumentNullException>(string.IsNullOrEmpty(checkValue),
                                                    Message.Fmt(templateReplacements));
         }




         /// <summary>
         /// NotNullOrEmpty string validation Check - check that string checkValue is not null. Works only in DEBUG mode.
         /// If value is null then throw NullReferenceException.
         /// To get Message we use template from Default ResourceManager by Validator class.
         /// Message template Key: [T-Validator] + [Operation Name - Operation].                        
         /// Message Template will be formatted with  OperationContext.
         /// </summary>
         /// <param name="checkValue"></param>
         /// <param name="Operation"></param>
         /// <param name="Point"></param>
         [Conditional("DEBUG")]
         internal static void NotNullOrEmptyDbg(string checkValue, String operation)
         {
             var operationContext = MessageInspector.GetOperationContext<Validator>(operation, point: null);

             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);

             AssertTrueDbg<ArgumentNullException>(string.IsNullOrEmpty(checkValue),
                 msgTemplate.FmtEx(Context: operationContext.Item2, inparams: operationContext.Item2));
         }





         /// <summary>
         /// NotNullOrEmpty string validation Check - check that string checkValue is not null. Works only in DEBUG mode.
         /// If value is null then throw NullReferenceException.
         /// To get Message we use template from Default ResourceManager by Validator class.
         /// Message template Key : [T-ContextType] + [Operation Name - Operation] + [Point].  
         /// Message Template will be formatted with  templateReplacements parameters.
         /// If templateReplacements.Length == 0 then formatting won't be done.
         /// </summary>
         /// <typeparam name="T"></typeparam>
         /// <param name="checkValue"></param>
         /// <param name="Operation"></param>
         /// <param name="Point"></param>
         /// <param name="templateParts"></param>
         [Conditional("DEBUG")]
         public static void NotNullOrEmptyDbg<T>(string checkValue, String operation, String point = null, params string[] templateReplacements)
         {
             var operationContext = MessageInspector.GetOperationContext<T>(operation, point);

             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);


             AssertTrueDbg<ArgumentNullException>(string.IsNullOrEmpty(checkValue),
                 msgTemplate.FmtEx(Context: operationContext.Item2, inparams: templateReplacements));

         }




         /// <summary>
         /// NotNullOrEmpty string validation Check - check that string checkValue is not null. Works only in DEBUG mode.
         /// Message template Key : [TDelegate.ContextType] + [TDelegate.Operation] + [Point].  
         /// Message Template will be formatted with  templateReplacements parameters.
         /// If templateReplacements.Length == 0 then formatting won't be done.
         /// </summary>
         /// <typeparam name="TDelegate"></typeparam>
         /// <param name="checkValue"></param>
         /// <param name="Operation"></param>
         /// <param name="Point"></param>
         /// <param name="templateReplacements"></param>
         [Conditional("DEBUG")]
         public static void NotNullOrEmptyDbg<TDelegate>(string checkValue, TDelegate operation, String point = null, params string[] templateReplacements)
         {
             var operationContext = MessageInspector.GetOperationContext<TDelegate>(operation, point);

             //culture based template
             string msgTemplate = MessageInspector.GetMessageTemplate(operationContext);

             AssertTrueDbg<ArgumentNullException>(string.IsNullOrEmpty(checkValue),
                msgTemplate.FmtEx(Context: operationContext.Item2, inparams: templateReplacements));

         }



         #endregion  -------------------------------- NotNullOrEmpty Check IN DEBUG -------------------------------------




         #region --------------------------------- NotNullOrWhiteSpace Check IN DEBUG ------------------------------------


         ///// <summary>
         ///// Check that string value is not null or whitespace
         ///// </summary>
         ///// <param name="value"></param>
         ///// <param name="OperationName"></param>
         ///// <param name="VariableName"></param>
         //[Conditional("DEBUG")]
         //public static void NotNullOrWhiteSpaceDbg(string value, String OperationName, String VariableName)
         //{
         //    AssertTrueDbg<ArgumentNullException>(string.IsNullOrWhiteSpace(value),
         //                                         ErrTemplate_NotNullOrWhiteSpaceStringDbg.Fmt(OperationName, VariableName));
         //}

         #endregion --------------------------------- NotNullOrWhiteSpace Check IN DEBUG ------------------------------------


         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsNotNullOrEmptyArray(string paramName, Array paramValue)
         //{
         //    if ((paramValue == null) || (paramValue.Length == 0))
         //    {
         //        var error = string.Format("Argument '{0}' cannot be null or an empty array", ObjectToStringHelper.ToString(paramName));
         //        Log.Error(error);
         //        throw new ArgumentException(error, paramName);
         //    }
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsNotOutOfRange<T>(string paramName, T paramValue, T minimumValue, T maximumValue, Func<T, T, T, bool> validation)
         //{
         //    IsNotNull("validation", validation);
         //    if (!validation(paramValue, minimumValue, maximumValue))
         //    {
         //        var error = string.Format("Argument '{0}' should be between {1} and {2}", ObjectToStringHelper.ToString(paramName), minimumValue, maximumValue);
         //        Log.Error(error);
         //        throw new ArgumentOutOfRangeException(paramName, error);
         //    }
         //}
         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsNotOutOfRange<T>(string paramName, T paramValue, T minimumValue, T maximumValue)
         //    where T : IComparable
         //{
         //    IsNotOutOfRange(paramName, paramValue, minimumValue, maximumValue,
         //        (innerParamValue, innerMinimumValue, innerMaximumValue) => innerParamValue.CompareTo(innerMinimumValue) >= 0 && innerParamValue.CompareTo(innerMaximumValue) <= 0);
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMinimal<T>(string paramName, T paramValue, T minimumValue, Func<T, T, bool> validation)
         //{
         //    IsNotNull("validation", validation);

         //    if (!validation(paramValue, minimumValue))
         //    {
         //        var error = string.Format("Argument '{0}' should be minimal {1}", ObjectToStringHelper.ToString(paramName), minimumValue);
         //        Log.Error(error);
         //        throw new ArgumentOutOfRangeException(paramName, error);
         //    }
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMinimal<T>(string paramName, T paramValue, T minimumValue)
         //    where T : IComparable
         //{
         //    IsMinimal(paramName, paramValue, minimumValue,
         //        (innerParamValue, innerMinimumValue) => innerParamValue.CompareTo(innerMinimumValue) >= 0);
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMaximum<T>(string paramName, T paramValue, T maximumValue, Func<T, T, bool> validation)
         //{
         //    if (!validation(paramValue, maximumValue))
         //    {
         //        var error = string.Format("Argument '{0}' should be at maximum {1}", ObjectToStringHelper.ToString(paramName), maximumValue);
         //        Log.Error(error);
         //        throw new ArgumentOutOfRangeException(paramName, error);
         //    }
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMaximum<T>(string paramName, T paramValue, T maximumValue)
         //    where T : IComparable
         //{
         //    IsMaximum(paramName, paramValue, maximumValue,
         //        (innerParamValue, innerMaximumValue) => innerParamValue.CompareTo(innerMaximumValue) <= 0);
         //}
                  
         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMatch(string paramName, string paramValue, string pattern, RegexOptions regexOptions = RegexOptions.None)
         //{
         //    Argument.IsNotNull("paramValue", paramValue);
         //    Argument.IsNotNull("pattern", pattern);

         //    if (!Regex.IsMatch(paramValue, pattern, regexOptions))
         //    {
         //        var error = string.Format("Argument '{0}' doesn't match with pattern '{1}'", paramName, pattern);
         //        Log.Error(error);
         //        throw new ArgumentException(error, paramName);
         //    }
         //}

         //[DebuggerNonUserCode, DebuggerStepThrough]
         //public static void IsMatch(string paramName, string paramValue, string pattern, RegexOptions regexOptions = RegexOptions.None)
         //{
         //    Argument.IsNotNull("paramValue", paramValue);
         //    Argument.IsNotNull("pattern", pattern);

         //    if (!Regex.IsMatch(paramValue, pattern, regexOptions))
         //    {
         //        var error = string.Format("Argument '{0}' doesn't match with pattern '{1}'", paramName, pattern);
         //        Log.Error(error);
         //        throw new ArgumentException(error, paramName);
         //    }
         //}
               
         //public static void IsSupported(bool isSupported, string errorFormat, params object[] args)
         //{
         //    Argument.IsNotNullOrWhitespace("errorFormat", errorFormat);

         //    if (!isSupported)
         //    {
         //        var error = string.Format(errorFormat, args);
         //        Log.Error(error);
         //        throw new NotSupportedException(error);
         //    }
         //}

         #endregion --------------------------------- KNOWN CONDITION CHECKS ---------------------------------------



     }
 }

