using System;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An <see cref="T:System.Exception" /> that is thrown when a <see cref="T:Infragistics.Persistence.PersistenceGroup" />'s controls don't match up with the stream that it's supposed to load from.
	/// </summary>
	public class InvalidPersistenceGroupException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="T:Infragistics.Persistence.InvalidPersistenceGroupException" /> class.
		/// </summary>
		public InvalidPersistenceGroupException() : this(SR.GetString("InvalidPersistenceGroupException"))
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Infragistics.Persistence.InvalidPersistenceGroupException" /> class.
		/// </summary>
		/// <param propertyName="message">The message that should be displayed.</param>
		public InvalidPersistenceGroupException(string message) : this(message, null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Infragistics.Persistence.InvalidPersistenceGroupException" /> class.
		/// </summary>
		/// <param propertyName="message">The message that should be displayed.</param>
		/// <param propertyName="innerException">An inner exception.</param>
		public InvalidPersistenceGroupException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}