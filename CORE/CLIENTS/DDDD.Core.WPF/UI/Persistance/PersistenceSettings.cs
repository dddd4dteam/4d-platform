using Infragistics.Persistence.Primitives;
using System;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object that contains the settings for how an object should save and load properties. 
	/// </summary>
	public class PersistenceSettings : DependencyObject
	{
		private PropertyPersistenceInfoBaseCollection _propertySettings;

		private PropertyPersistenceInfoBaseCollection _ignorePropertySettings;

		private PersistenceEvents _events;

		/// <summary>
		/// Gets/Sets the events that will be raised while saving and loading this <see cref="T:Infragistics.Persistence.PersistenceSettings" />
		/// </summary>
		public PersistenceEvents Events
		{
			get
			{
				if (this._events == null)
				{
					this._events = new PersistenceEvents();
				}
				return this._events;
			}
			set
			{
				this._events = value;
			}
		}

		/// <summary>
		/// Gets a collection of PropertyPersistenceInfoBase objects that shouldn't be saved/loaded. 
		/// </summary>
		/// <remarks>
		/// Properties that are identified here will not be saved/loaded. 
		/// </remarks>
		public PropertyPersistenceInfoBaseCollection IgnorePropertySettings
		{
			get
			{
				if (this._ignorePropertySettings == null)
				{
					this._ignorePropertySettings = new PropertyPersistenceInfoBaseCollection();
				}
				return this._ignorePropertySettings;
			}
		}

		/// <summary>
		/// Gets/sets which properties should be loaded. 
		/// </summary>
		public PersistenceOption LoadPersistenceOptions
		{
			get;
			set;
		}

		/// <summary>
		/// Gets a collection of PropertyPersistenceInfoBase objects to be saved/loaded.
		/// </summary>
		/// <remarks>
		/// When <see cref="P:Infragistics.Persistence.PersistenceSettings.SavePersistenceOptions" /> or <see cref="P:Infragistics.Persistence.PersistenceSettings.LoadPersistenceOptions" /> are set to OnlySpecified, just properties 
		/// that are identified here will be saved/loaded. 
		/// Otherwise this collection will be used to identify how a property should be saved/loaded.
		/// </remarks>
		public PropertyPersistenceInfoBaseCollection PropertySettings
		{
			get
			{
				if (this._propertySettings == null)
				{
					this._propertySettings = new PropertyPersistenceInfoBaseCollection();
				}
				return this._propertySettings;
			}
		}

		/// <summary>
		/// Gets/sets which properties should be saved. 
		/// </summary>
		public PersistenceOption SavePersistenceOptions
		{
			get;
			set;
		}

		public PersistenceSettings()
		{
		}
	}
}