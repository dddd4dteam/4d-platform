using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to group multiple <see cref="T:System.Windows.DependencyObject" />s so that they maybe Saved and Loaded at the same time.
	/// </summary>
	public class PersistenceGroup
	{
		private List<DependencyObject> _registeredObjects;

		private ReadOnlyCollection<DependencyObject> _objects;

		private PersistenceEvents _events;

		/// <summary>
		/// Gets/Sets the events that will be raised while saving and loading this <see cref="T:Infragistics.Persistence.PersistenceGroup" />
		/// </summary>
		public PersistenceEvents Events
		{
			get
			{
				if (this._events == null)
				{
					this._events = new PersistenceEvents();
				}
				return this._events;
			}
			set
			{
				this._events = value;
			}
		}

		/// <summary>
		/// Gets a ReadOnly collection of <see cref="T:System.Windows.DependencyObject" />s that this <see cref="T:Infragistics.Persistence.PersistenceGroup" /> will save or load.
		/// </summary>
		public ReadOnlyCollection<DependencyObject> RegisteredObjects
		{
			get
			{
				return this._objects;
			}
		}

		/// <summary>
		/// Creates a new instances of the <see cref="T:Infragistics.Persistence.PersistenceGroup" /> class.
		/// </summary>
		public PersistenceGroup()
		{
			this._registeredObjects = new List<DependencyObject>();
			this._objects = new ReadOnlyCollection<DependencyObject>(this._registeredObjects);
		}

		internal void RegisterObject(DependencyObject obj)
		{
			this._registeredObjects.Add(obj);
		}

		internal void UnregisterObject(DependencyObject obj)
		{
			this._registeredObjects.Remove(obj);
		}
	}
}