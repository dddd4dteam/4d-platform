using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to pass information of a property being loaded. 
	/// </summary>
	public class LoadPropertyPersistenceEventArgs : PropertyPersistenceEventArgs
	{
		/// <summary>
		/// Gets/sets whether the property was loaded in the event, and thus shouldn't try to be loaded by the framework.
		/// </summary>
		public bool Handled
		{
			get;
			set;
		}

		/// <summary>
		/// Gets an identifier that can be used to identify an property being loaded. 
		/// </summary>
		/// <remarks>
		/// If specified, during loading the framework will attempt to load an object from the Application.Current.Resources using this Identifier. 
		/// Otherwise, it may be used by the end developer to identify a specific property in an a event. 
		/// </remarks>
		public string Identifier
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets/sets the value that should be used to restore the property. 
		/// </summary>
		/// <remarks>
		/// When set, this value will be attempted to be used to load the property.
		/// </remarks>
		public object LoadedValue
		{
			get;
			set;
		}

		/// <summary>
		/// Gets the object to whom the property belongs. 
		/// </summary>
		public object Owner
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets a string representation of the property that was being stored. 
		/// </summary>
		public string SavedValue
		{
			get;
			private set;
		}

		/// <summary>
		/// Gets the value of the property, if the property was a Value type.
		/// </summary>
		public object Value
		{
			get;
			private set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T:Infragistics.Persistence.LoadPropertyPersistenceEventArgs" /> object. 
		/// </summary>
		/// <param name="rootOwner"></param>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <param name="savedValue"></param>
		/// <param name="id"></param>
		/// <param name="owner"></param>
		/// <param name="value"></param>
		public LoadPropertyPersistenceEventArgs(DependencyObject rootOwner, System.Reflection.PropertyInfo pi, string propertyPath, string savedValue, string id, object owner, object value) : base(rootOwner, pi, propertyPath)
		{
			this.SavedValue = savedValue;
			this.Identifier = id;
			this.Owner = owner;
			this.Value = value;
		}
	}
}