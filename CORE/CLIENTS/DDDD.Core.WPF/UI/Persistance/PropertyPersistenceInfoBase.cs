using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence
{
	/// <summary>
	/// A base object that can be used to identify a Property.
	/// </summary>
	public abstract class PropertyPersistenceInfoBase
	{
		/// <summary>
		/// Gets/sets an identifier that can be used to identify an property being loaded. 
		/// </summary>
		/// <remarks>
		/// If specified, during loading the framework will attempt to load an object from the Application.Current.Resources using this Identifier. 
		/// Otherwise, it may be used by the end developer to identify a specific property in an a event. 
		/// </remarks>
		public string Identifier
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a string representation of the value for a property that will be used to store the property.
		/// </summary>
		public string SaveValue
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the a TypeConverter that can be used to Convert an object to and from a string.
		/// </summary>
		public System.ComponentModel.TypeConverter TypeConverter
		{
			get;
			set;
		}

		protected PropertyPersistenceInfoBase()
		{
		}

		/// <summary>
		/// When overridden on a derived class, this method should be used to determine if the specified property matches
		/// the criteria given.
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <returns></returns>
		public abstract bool DoesPropertyMeetCriteria(PropertyInfo pi, string propertyPath);

		/// <summary>
		/// A method that when overridden on a derived class, can be used to load a object from a string.
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public virtual object LoadObjectFromString(PropertyInfo pi, string propertyPath, string value)
		{
			return null;
		}

		/// <summary>
		/// A method that when overridden on a derived class, can be used to build a string version of the value that should be saved.
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public virtual string SaveValueToString(PropertyInfo pi, string propertyPath, object value)
		{
			return null;
		}
	}
}