using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace Infragistics.Persistence
{
	/// <summary>
	/// A Collection of <see cref="T:Infragistics.Persistence.PropertyPersistenceInfoBase" /> objects. 
	/// </summary>
	public class PropertyPersistenceInfoBaseCollection : ObservableCollection<PropertyPersistenceInfoBase>
	{
		public PropertyPersistenceInfoBaseCollection()
		{
		}

		/// <summary>
		/// Loops through all <see cref="T:Infragistics.Persistence.PropertyPersistenceInfoBase" /> objects in the collection, and calls <see cref="M:Infragistics.Persistence.PropertyPersistenceInfoBase.DoesPropertyMeetCriteria(System.Reflection.PropertyInfo,System.String)" /> on them. 
		/// The first one found will be returned.  Otherwise null is returned. 
		/// </summary>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <returns></returns>
		public PropertyPersistenceInfoBase GetPropertyPersistenceInfo(PropertyInfo pi, string propertyPath)
		{
			PropertyPersistenceInfoBase propertyPersistenceInfoBase;
			using (IEnumerator<PropertyPersistenceInfoBase> enumerator = base.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					PropertyPersistenceInfoBase current = enumerator.Current;
					if (!current.DoesPropertyMeetCriteria(pi, propertyPath))
					{
						continue;
					}
					propertyPersistenceInfoBase = current;
					return propertyPersistenceInfoBase;
				}
				return null;
			}
			return propertyPersistenceInfoBase;
		}
	}
}