using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object that acts as a serializable container for storing information of an object. 
	/// </summary>
	public class PropertyData
	{
		/// <summary>
		/// Gets/sets the lookup key for the type of property this object is. 
		/// </summary>
		public int AssemblyTypeKey
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the AssemblyQualifiedName of a TypeConverter that can be used to Convert a string back to an object.
		/// </summary>
		public string ConverterTypeName
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the custom storage.
		/// </summary>
		public string CustomStorage
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets an identifier that can be used to identify an property being loaded. 
		/// </summary>
		/// <remarks>
		/// If specified, during loading the framework will attempt to load an object from the Application.Current.Resources using this Identifier. 
		/// Otherwise, it may be used by the end developer to identify a specific property in an a event. 
		/// </remarks>
		public string Identifier
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a collection of string keys that should be used to repopulate a collection.
		/// </summary>
		public Collection<string> LookUpKeys
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a list of property information for all subproperties of an object.
		/// </summary>
		public Collection<PropertyDataInfo> Properties
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a string representation of a property.
		/// </summary>
		public string StringValue
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the actual value of a property. 
		/// </summary>
		/// <remarks>
		/// This property is only used for properties that are Value types.
		/// </remarks>
		public object Value
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes the members of the <see cref="T:Infragistics.Persistence.Primitives.PropertyData" />
		/// </summary>
		public PropertyData()
		{
			this.LookUpKeys = new Collection<string>();
			this.Properties = new Collection<PropertyDataInfo>();
		}
	}
}