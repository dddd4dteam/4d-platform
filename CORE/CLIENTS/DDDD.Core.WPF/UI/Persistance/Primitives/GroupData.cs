using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store all the information to serialize or deserialize a group of objects. 
	/// </summary>
	public class GroupData
	{
		/// <summary>
		/// Gets/sets a list of all the elements that are being stored.
		/// </summary>
		public Collection<ElementData> Elements
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a list of all the Types that are being stored.
		/// </summary>
		public Collection<TypeDataPair> TypeStore
		{
			get;
			set;
		}

		public GroupData()
		{
		}
	}
}