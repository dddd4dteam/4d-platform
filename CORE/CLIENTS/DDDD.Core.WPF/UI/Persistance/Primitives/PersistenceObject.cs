using System;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store the root level of serialziation. 
	/// </summary>
	public class PersistenceObject
	{
		/// <summary>
		/// A list of AssemblyQualifiedNames that make up all value types that are stored by their actual values instead of a string representation.
		/// </summary>
		/// <remarks>
		/// This list of types is neccessary to deserialize the <see cref="P:Infragistics.Persistence.Primitives.PersistenceObject.Xml" /> defined.
		/// </remarks>
		public Collection<string> Types
		{
			get;
			set;
		}

		/// <summary>
		/// The xml that contains all the information for serializing and deserializing an object. 
		/// </summary>
		public string Xml
		{
			get;
			set;
		}

		public PersistenceObject()
		{
		}
	}
}