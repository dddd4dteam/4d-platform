using System;
using System.Runtime.CompilerServices;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An object used to store <see cref="T:Infragistics.Persistence.Primitives.PropertyData" /> with its lookup key in a format that can be serialized.
	/// </summary>
	public class PropertyDataPair
	{
		/// <summary>
		/// Gets/sets the data being stored.
		/// </summary>
		public PropertyData Data
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the unique key that identifies the <see cref="T:Infragistics.Persistence.Primitives.PropertyData" />
		/// </summary>
		public int LookupKey
		{
			get;
			set;
		}

		public PropertyDataPair()
		{
		}
	}
}