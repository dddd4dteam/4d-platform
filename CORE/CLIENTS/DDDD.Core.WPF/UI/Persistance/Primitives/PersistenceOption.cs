using System;

namespace Infragistics.Persistence.Primitives
{
	/// <summary>
	/// An enum that specifies how something should be persisted.
	/// </summary>
	public enum PersistenceOption
	{
		/// <summary>
		/// All properties should be saved or loaded, except for the properties that list properties marked to be ignored.
		/// </summary>
		AllButIgnored,
		/// <summary>
		/// Only the list of properties that are specified should be saved or loaded. 
		/// </summary>
		OnlySpecified
	}
}