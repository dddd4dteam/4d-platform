using System;
using System.Threading;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object that contains the events used for Saving and Loading the persistence of <see cref="T:System.Windows.DependencyObject" />s.
	/// </summary>
	public class PersistenceEvents
	{
		public PersistenceEvents()
		{
		}

		internal void OnLoadPropertyPersistence(LoadPropertyPersistenceEventArgs args)
		{
			if (this.LoadPropertyPersistence != null)
			{
				this.LoadPropertyPersistence(this, args);
			}
		}

		internal void OnPersistenceLoaded(PersistenceLoadedEventArgs args)
		{
			if (this.PersistenceLoaded != null)
			{
				this.PersistenceLoaded(this, args);
			}
		}

		internal void OnPersistenceSaved(PersistenceSavedEventArgs args)
		{
			if (this.PersistenceSaved != null)
			{
				this.PersistenceSaved(this, args);
			}
		}

		internal void OnSavePropertyPersistence(SavePropertyPersistenceEventArgs args)
		{
			if (this.SavePropertyPersistence != null)
			{
				this.SavePropertyPersistence(this, args);
			}
		}

		/// <summary>
		/// Occurs before a property is loaded.
		/// </summary>
		public event EventHandler<LoadPropertyPersistenceEventArgs> LoadPropertyPersistence;

		/// <summary>
		/// Occurs after all properties are loaded. 
		/// </summary>
		public event EventHandler<PersistenceLoadedEventArgs> PersistenceLoaded;

		/// <summary>
		/// Ocurrs afert all properties are saved. 
		/// </summary>
		public event EventHandler<PersistenceSavedEventArgs> PersistenceSaved;

		/// <summary>
		/// Occurs before a property is saved. 
		/// </summary>
		public event EventHandler<SavePropertyPersistenceEventArgs> SavePropertyPersistence;
	}
}