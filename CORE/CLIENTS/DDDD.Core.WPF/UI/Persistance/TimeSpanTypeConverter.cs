using System;
using System.ComponentModel;
using System.Globalization;

namespace Infragistics.Persistence
{
	/// <summary>
	/// A <see cref="T:System.ComponentModel.TypeConverter" /> that converts from TimeSpan to string, and string to TimeSpan.
	/// </summary>
	public class TimeSpanTypeConverter : TypeConverter
	{
		public TimeSpanTypeConverter()
		{
		}

		/// <summary>
		/// Returns true if the sourceType is of type string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="sourceType"></param>
		/// <returns></returns>
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string);
		}

		/// <summary>
		/// Returns true if the destinationType is of type string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string);
		}

		/// <summary>
		/// Converts a string into a TimeSpan.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			string str = (string)value;
			if (str == null || str.Length <= 0)
			{
				return ' ';
			}
			return TimeSpan.Parse(str);
		}

		/// <summary>
		/// Converts a TimeSpan into a string.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="culture"></param>
		/// <param name="value"></param>
		/// <param name="destinationType"></param>
		/// <returns></returns>
		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (value == null)
			{
				return null;
			}
			return ((TimeSpan)value).ToString();
		}
	}
}