using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Windows;

namespace Infragistics.Persistence
{
	/// <summary>
	/// An object used to pass information of a property being saved. 
	/// </summary>
	public class SavePropertyPersistenceEventArgs : PropertyPersistenceEventArgs
	{
		/// <summary>
		/// Get/sets whether the property shouldn't be stored. 
		/// </summary>
		public bool Cancel
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets an identifier that can be used to identify an property being loaded. 
		/// </summary>
		/// <remarks>
		/// If specified, during loading the framework will attempt to load an object from the Application.Current.Resources using this Identifier. 
		/// Otherwise, it may be used by the end developer to identify a specific property in an a event. 
		/// </remarks>
		public string Identifier
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets a string representation of the <see cref="P:Infragistics.Persistence.SavePropertyPersistenceEventArgs.Value" /> that will be used to store the property.
		/// </summary>
		public string SaveValue
		{
			get;
			set;
		}

		/// <summary>
		/// Gets/sets the actual value of the property being stored. 
		/// </summary>
		public object Value
		{
			get;
			set;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="T:Infragistics.Persistence.SavePropertyPersistenceEventArgs" /> object. 
		/// </summary>
		/// <param name="rootOwner"></param>
		/// <param name="pi"></param>
		/// <param name="propertyPath"></param>
		/// <param name="value"></param>
		public SavePropertyPersistenceEventArgs(DependencyObject rootOwner, System.Reflection.PropertyInfo pi, string propertyPath, object value) : base(rootOwner, pi, propertyPath)
		{
			this.Value = value;
		}
	}
}