﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

using DDDD.Core.UI.Resources.IG;
using DDDD.Core.Extensions;

namespace DDDD.Core.UI.IG
{
    /// <summary>
    /// Static class with attached properties to help with cross platform development with WPF and Silverlight.
    /// </summary>
    public class XamlHelper : DependencyObject
    {


        #region ----------------------- CTOR --------------------------
        
        static XamlHelper()
        {
            CanContentScrollProperty = DependencyProperty.RegisterAttached(nameof(CanContentScrollProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(false));
            FocusableProperty = DependencyProperty.RegisterAttached(nameof(FocusableProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(true));
            StaysOpenProperty = DependencyProperty.RegisterAttached(nameof(StaysOpenProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(true));
            AllowsTransparencyProperty = DependencyProperty.RegisterAttached(nameof(AllowsTransparencyProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(true));
            IsExcludedFromWashProperty = DependencyProperty.RegisterAttached(nameof(IsExcludedFromWashProperty).RP(), typeof(bool), typeof(XamlHelper), new PropertyMetadata(false));
            WashGroupProperty = DependencyProperty.RegisterAttached(nameof(WashGroupProperty).RP(), typeof(bool), typeof(XamlHelper), new PropertyMetadata(false));
            SnapsToDevicePixelsProperty = DependencyProperty.RegisterAttached(nameof(SnapsToDevicePixelsProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(false));
            TabNavigationProperty = DependencyPropertyUtilities.RegisterAttached(nameof(TabNavigationProperty).RP(), typeof(KeyboardNavigationMode), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(KeyboardNavigationMode.Local, new PropertyChangedCallback(XamlHelper.OnTabNavigationChanged)));
            UpdateBindingOnTextPropertyChangedProperty = DependencyPropertyUtilities.RegisterAttached(nameof(UpdateBindingOnTextPropertyChangedProperty).RP(), typeof(bool), typeof(XamlHelper), DependencyPropertyUtilities.CreateMetadata(false, new PropertyChangedCallback(XamlHelper.OnUpdateBindingOnTextPropertyChanged)));
        }

        private XamlHelper()
        {
        }

        #endregion ----------------------- CTOR --------------------------


        /// <summary>
        /// Identifies the CanContentScroll attached dependency property
        /// </summary>
        public readonly static DependencyProperty CanContentScrollProperty;

        /// <summary>
        /// Identifies the Focusable attached dependency property
        /// </summary>
        public readonly static DependencyProperty FocusableProperty;

        /// <summary>
        /// Identifies the WPF Popup StaysOpen attached dependency property
        /// </summary>
        public readonly static DependencyProperty StaysOpenProperty;

        /// <summary>
        /// Identifies the WPF Popup AllowsTransparency attached dependency property
        /// </summary>
        public readonly static DependencyProperty AllowsTransparencyProperty;

        /// <summary>
        /// Identifies the ResourceWasher IsExcludedFromWash attached dependency property
        /// </summary>
        public readonly static DependencyProperty IsExcludedFromWashProperty;

        /// <summary>
        /// Identifies the ResourceWasher WashGroup attached dependency property
        /// </summary>
        public readonly static DependencyProperty WashGroupProperty;

        /// <summary>
        /// Identifies the SnapsToDevicePixels attached dependency property
        /// </summary>
        public readonly static DependencyProperty SnapsToDevicePixelsProperty;

        /// <summary>
        /// Identifies the TabNavigation attached dependency property
        /// </summary>
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetTabNavigation(System.Windows.DependencyObject)" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetTabNavigation(System.Windows.DependencyObject,System.Windows.Input.KeyboardNavigationMode)" />
        public readonly static DependencyProperty TabNavigationProperty;

        /// <summary>
        /// Identifies the UpdateBindingOnTextPropertyChanged attached dependency property which can be set on a TextBox
        /// to control whether changes to the TextBox's Text property forces an immediate update of the binding Target the same way
        /// that an UpdateSourceTrigger = PropertyChanged works in WPF.
        /// </summary>
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetUpdateBindingOnTextPropertyChanged(System.Windows.DependencyObject)" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetUpdateBindingOnTextPropertyChanged(System.Windows.DependencyObject,System.Boolean)" />
        public readonly static DependencyProperty UpdateBindingOnTextPropertyChangedProperty;


        /// <summary>
        /// Used to change the AllowsTransparency value of a popup in WPF.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        public static bool GetAllowsTransparency(DependencyObject d)
        {
            return (bool)d.GetValue(XamlHelper.AllowsTransparencyProperty);
        }

        /// <summary>
        /// Used to change the CanContentScroll value of a ScrollViewer in WPF.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.CanContentScrollProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetCanContentScroll(System.Windows.DependencyObject,System.Boolean)" />
        public static bool GetCanContentScroll(DependencyObject d)
        {
            return (bool)d.GetValue(XamlHelper.CanContentScrollProperty);
        }

        /// <summary>
        /// Used to change the Focusable value of a UIElement in WPF.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.FocusableProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetFocusable(System.Windows.DependencyObject,System.Boolean)" />
        public static bool GetFocusable(DependencyObject d)
        {
            return (bool)d.GetValue(XamlHelper.FocusableProperty);
        }

        /// <summary>
        /// Gets the value of the IsExcludedFromWash attached property.
        /// </summary>
        /// <param name="element">The object whose value is to be returned.</param>
        /// <returns></returns>
        public static bool GetIsExcludedFromWash(DependencyObject element)
        {
            return ResourceWasher.GetIsExcludedFromWash(element);
        }

        /// <summary>
        /// Used to change the SnapsToDevicePixels value of a UIElement in WPF.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.SnapsToDevicePixelsProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetSnapsToDevicePixels(System.Windows.DependencyObject,System.Boolean)" />
        public static bool GetSnapsToDevicePixels(DependencyObject d)
        {
            return (bool)d.GetValue(SnapsToDevicePixelsProperty);
        }

        /// <summary>
        /// Used to change the StaysOpen value of a Popup in WPF.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        public static bool GetStaysOpen(DependencyObject d)
        {
            return (bool)d.GetValue(StaysOpenProperty);
        }

        /// <summary>
        /// Gets the value of the attached TabNavigation DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.TabNavigationProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetTabNavigation(System.Windows.DependencyObject,System.Windows.Input.KeyboardNavigationMode)" />
        public static KeyboardNavigationMode GetTabNavigation(DependencyObject d)
        {
            return (KeyboardNavigationMode)d.GetValue(XamlHelper.TabNavigationProperty);
        }

        /// <summary>
        /// Gets the value of the attached UpdateBindingOnTextPropertyChanged DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be returned</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.UpdateBindingOnTextPropertyChangedProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.SetUpdateBindingOnTextPropertyChanged(System.Windows.DependencyObject,System.Boolean)" />
        public static bool GetUpdateBindingOnTextPropertyChanged(DependencyObject d)
        {
            return (bool)d.GetValue(UpdateBindingOnTextPropertyChangedProperty);
        }

        /// <summary>
        /// Gets the value of the WashGroup attached property.
        /// </summary>
        /// <param name="element">The object whose value is to be returned.</param>
        /// <returns></returns>
        public static string GetWashGroup(DependencyObject element)
        {
            return ResourceWasher.GetWashGroup(element);
        }
      /// <summary>
        /// Sets the value of the attached AllowsTransparency DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        public static void SetAllowsTransparency(DependencyObject d, bool value)
        {
            d.SetValue(AllowsTransparencyProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached CanContentScroll DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.CanContentScrollProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetCanContentScroll(System.Windows.DependencyObject)" />
        public static void SetCanContentScroll(DependencyObject d, bool value)
        {
            d.SetValue(CanContentScrollProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached Focusable DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.FocusableProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetFocusable(System.Windows.DependencyObject)" />
        public static void SetFocusable(DependencyObject d, bool value)
        {
            d.SetValue(FocusableProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached IsExcludedFromWash property.
        /// </summary>
        /// <param name="element">The object whose value is to be modified.</param>
        /// <param name="value">The new value</param>
        public static void SetIsExcludedFromWash(DependencyObject element, bool value)
        {
            ResourceWasher.SetIsExcludedFromWash(element, value);
        }

        /// <summary>
        /// Sets the value of the attached SnapsToDevicePixels DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.SnapsToDevicePixelsProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetSnapsToDevicePixels(System.Windows.DependencyObject)" />
        public static void SetSnapsToDevicePixels(DependencyObject d, bool value)
        {
            d.SetValue(SnapsToDevicePixelsProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached StaysOpen DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        public static void SetStaysOpen(DependencyObject d, bool value)
        {
            d.SetValue(StaysOpenProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached TabNavigation DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.TabNavigationProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetTabNavigation(System.Windows.DependencyObject)" />
        public static void SetTabNavigation(DependencyObject d, KeyboardNavigationMode value)
        {
            d.SetValue(TabNavigationProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached UpdateBindingOnTextPropertyChanged DependencyProperty.
        /// </summary>
        /// <param name="d">The object whose value is to be modified</param>
        /// <param name="value">The new value</param>
        /// <seealso cref="F:Controls.Primitives.XamlHelper.UpdateBindingOnTextPropertyChangedProperty" />
        /// <seealso cref="M:Controls.Primitives.XamlHelper.GetUpdateBindingOnTextPropertyChanged(System.Windows.DependencyObject)" />
        public static void SetUpdateBindingOnTextPropertyChanged(DependencyObject d, bool value)
        {
            d.SetValue(UpdateBindingOnTextPropertyChangedProperty, value);
        }

        /// <summary>
        /// Sets the value of the attached WashGroup property.
        /// </summary>
        /// <param name="element">The object whose value is to be modified.</param>
        /// <param name="value">The new value</param>
        public static void SetWashGroup(DependencyObject element, string value)
        {
            ResourceWasher.SetWashGroup(element, value);
        }

        


        private static void OnTabNavigationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Control newValue = d as Control;
            if (newValue != null)
            {
#if CLIENT && SL5
               newValue.TabNavigation = (KeyboardNavigationMode)e.NewValue;   //  [12/27/2016 A1]
#endif 
            }
        }

        private static void OnUpdateBindingOnTextPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TextBox)
            {
                TextBoxTextChangedListener textBoxTextChangedListener = new TextBoxTextChangedListener((TextBox)d);
            }
        }





    }


}
