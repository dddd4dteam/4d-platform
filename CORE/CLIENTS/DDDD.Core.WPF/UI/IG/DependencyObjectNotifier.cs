﻿using System;
using System.ComponentModel;
using System.Windows;

namespace DDDD.Core.UI.IG
{
    
    
    /// <summary>
    /// Abstract base class for objects that implement INotifyPropertyChanged and derive from DependencyObject
    /// </summary>
    public abstract class DependencyObjectNotifier : DependencyObject, INotifyPropertyChanged
    {
      
        /// <summary>
        /// Initializes a new instance of the <see cref="T:DependencyObjectNotifier" /> class
        /// </summary>
        protected DependencyObjectNotifier()
        {
        }


        private PropertyChangedEventHandler _propertyChangedHandler;


        /// <summary>
        /// Raised when a property has changed
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                this._propertyChangedHandler = Delegate.Combine(this._propertyChangedHandler, value) as PropertyChangedEventHandler;
            }
            remove
            {
                this._propertyChangedHandler = Delegate.Remove(this._propertyChangedHandler, value) as PropertyChangedEventHandler;
            }
        }



        /// <summary>
        /// Used to raise the <see cref="E:DependencyObjectNotifier.PropertyChanged" /> event for the specified property name.
        /// </summary>
        /// <param name="propertyName">The name of the property that has changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChangedEventHandler = this._propertyChangedHandler;
            if (propertyChangedEventHandler != null)
            {
                propertyChangedEventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Raises the PropertyChanged event
        /// </summary>
        /// <param name="propertyName">The name of the property</param>
        [EditorBrowsable(EditorBrowsableState.Never)]
        protected void RaisePropertyChangedEvent(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
        }

     
    }
}
