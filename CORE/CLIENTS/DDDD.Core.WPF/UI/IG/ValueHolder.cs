﻿using System.Windows;

namespace DDDD.Core.UI.IG
{
  
    
    /// <summary>
    /// Simple dependency object with a single Value dependency property
    /// </summary>
    internal class ValueHolder : DependencyObject
    {

        #region -------------------- CTOR ------------------------

        static ValueHolder()
        {
            ValueHolder.ValueProperty = DependencyProperty.Register("Value", typeof(object), typeof(ValueHolder), null);
        }

        public ValueHolder()
        {
        }

        #endregion -------------------- CTOR ------------------------

        public readonly static DependencyProperty ValueProperty;

        public object Value
        {
            get
            {
                return base.GetValue(ValueHolder.ValueProperty);
            }
            set
            {
                base.SetValue(ValueHolder.ValueProperty, value);
            }
        }

    }


}
