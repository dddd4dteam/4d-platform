﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.IsolatedStorage;
using System.Xml;
using System.Xml.Serialization;

using SXml = ServiceStack.Text;

namespace DDDD.Core.UI.Themes
{


    /// <summary>
    /// Collection of PackageFileTp items
    /// <summary>
    [XmlType(Namespace = "http://tempuri.org/PackageFiles.xsd")]
    [XmlRoot("PackageFiles", Namespace = "http://tempuri.org/PackageFiles.xsd", IsNullable = false)]
    public partial class PackageFilesCollectionTp
    {

        private List<PackageFileTp> packageFileField;

        /// <summary>
        /// PackageFilesCollectionTp class constructor
        /// </summary>
        public PackageFilesCollectionTp()
        {
            this.packageFileField = new List<PackageFileTp>();
        }

        [XmlElement("PackageFile", Order = 0)]
        public List<PackageFileTp> PackageFiles
        {
            get
            {
                return this.packageFileField;
            }
            set
            {
                this.packageFileField = value;
            }
        }





        #region ------------------- SerializeToXml/DeserializeFromXml ---------------------

        /// <summary>
        /// Serialize this instance to Xml string.
        /// </summary>
        /// <returns></returns>
        public virtual string SerializeToXml()
        {
            return SXml.XmlSerializer.SerializeToString(this);
        }

        /// <summary>
        /// Deserialize [xml] string to out [obj] as  PackageFilesCollectionTp.
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static bool DeserializeFromXml(string xml, out PackageFilesCollectionTp obj, out Exception exception)
        {
            exception = null;
            obj = default(PackageFilesCollectionTp);
            try
            {

                obj = SXml.XmlSerializer.DeserializeFromString<PackageFilesCollectionTp>(xml); //  Deserialize(xml);                
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }


        /// <summary>
        /// Deserialize [xml] string and return value as PackageFilesCollectionTp.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static PackageFilesCollectionTp DeserializeFromXml(string xml)
        {
            try
            {
                return SXml.XmlSerializer.DeserializeFromString<PackageFilesCollectionTp>(xml); //  Deserialize(xml);                                
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        /// <summary>
        /// Deserialize [xml] string and return value as PackageFilesCollectionTp.
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool DeserializeFromXml(string xml, out PackageFilesCollectionTp obj)
        {
            Exception exception = null;
            return DeserializeFromXml(xml, out obj, out exception);
        }


        #endregion  ------------------- SerializeToXml/DeserializeFromXml ---------------------



        #region ------------------ SAVETOXMLFILE / LOADFROMXMLFILE -----------------------

        /// <summary>
        /// Save this instance: serialize into xml and  then  save by [filename] to isolated storage. 
        /// </summary>
        /// <param name="fileName"></param>
        public virtual void SaveToXmlFile(string fileName)
        {
            StreamWriter streamWriter = null;
            IsolatedStorageFile isoFile = null;
            IsolatedStorageFileStream isoStream = null;
            try
            {
                isoFile = IsolatedStorageFile.GetUserStoreForApplication();
                isoStream = new IsolatedStorageFileStream(fileName, FileMode.Create, isoFile);
                streamWriter = new StreamWriter(isoStream);
                string xmlString = SerializeToXml();
                FileInfo xmlFile = new FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
                isoStream.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
                if ((isoFile != null))
                {
                    isoFile.Dispose();
                }
                if ((isoStream != null))
                {
                    isoStream.Dispose();
                }
            }
        }


        /// <summary>
        /// Save this instance: serialize into xml and  then  save by [filename] to isolated storage.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public virtual bool SaveToXmlFile(string fileName, out Exception exception)
        {
            exception = null;
            try
            {
                SaveToXmlFile(fileName);
                return true;
            }
            catch (Exception e)
            {
                exception = e;
                return false;
            }
        }


        /// <summary>
        /// Load instance into out [obj] as PackageFilesCollectionTp from data in [filename] from Isolated Storage.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="obj"></param>
        /// <param name="exception"></param>
        /// <returns></returns>
        public static bool LoadFromXmlFile(string fileName, out PackageFilesCollectionTp obj, out Exception exception)
        {
            exception = null;
            obj = default(PackageFilesCollectionTp);
            try
            {
                obj = LoadFromXmlFile(fileName);
                return true;
            }
            catch (Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        /// <summary>
        /// Load instance into out [obj] as PackageFilesCollectionTp from data in [filename] from Isolated Storage.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool LoadFromXmlFile(string fileName, out PackageFilesCollectionTp obj)
        {
            Exception exception = null;
            return LoadFromXmlFile(fileName, out obj, out exception);
        }


        /// <summary>
        ///  Load - return PackageFilesCollectionTp  instance from data in [filename] from Isolated Storage.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static PackageFilesCollectionTp LoadFromXmlFile(string fileName)
        {
            IsolatedStorageFile isoFile = null;
            IsolatedStorageFileStream isoStream = null;
            StreamReader sr = null;
            try
            {
                isoFile = IsolatedStorageFile.GetUserStoreForApplication();
                isoStream = new IsolatedStorageFileStream(fileName, FileMode.Open, isoFile);
                sr = new StreamReader(isoStream);
                string xmlString = sr.ReadToEnd();
                isoStream.Close();
                sr.Close();
                return DeserializeFromXml(xmlString);
            }
            finally
            {
                if ((isoFile != null))
                {
                    isoFile.Dispose();
                }
                if ((isoStream != null))
                {
                    isoStream.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }


        #endregion ------------------ SAVETOXMLFILE / LOADFROMXMLFILE -----------------------



        #region ------------------- Cloning -------------------------
        /// <summary>
        /// Create a clone of this PackageFilesCollectionTp object
        /// </summary>
        public virtual PackageFilesCollectionTp Clone()
        {
            return ((PackageFilesCollectionTp)(this.MemberwiseClone()));
        }

        #endregion ------------------- Cloning -------------------------
    }

}
