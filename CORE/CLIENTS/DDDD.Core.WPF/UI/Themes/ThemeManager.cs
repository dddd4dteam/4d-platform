﻿
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

using DDDD.Core.Extensions;
using DDDD.Core.Resources;

namespace DDDD.Core.UI.Themes
{

    /// <summary>
    /// XAML UI - Theme Manager
    /// </summary>
    public static class ThemeManager
    {
        // Target Operations:
        // AddIGTheme(this ResourceDictionary dictionary String AssemblySourceName)
        // AddIGTheme(this ResourceDictionary dictionary String ThemeName, String AssemblySourceName, List<String>) 
        // AddIGTheme(this ResourceDictionary dictionary String ThemeName, String AssemblySourceName, List<Type>) 
        // AddToolkitTheme(String ThemeName) 

        #region  ----------------------------- CTOR ---------------------------------


#if CLIENT && SL5
        
        static ThemeManager()
        {
           //GThemeNeededResources =   //Gety GetIGThemeNeededResources();

           GlobalThemeDictionary = Application.Current.Resources;

           CurrentTheme = new ThemeLoadInfo();

        }    

#elif CLIENT && WPF

#endif


        #endregion ----------------------------- CTOR ----------------------------------


        #region  ------------------------------- FIELDS ----------------------------------

        #region  ------------------------------ DEFAULT PARAM VALUES for THEME LOADING METHODS ---------------------------------

        public const string DefThemeName = "Metro";

        /// <summary>
        ///  Default Param: Mask Need Params :  {0}-[ThemeName]; 
        /// </summary>        
        public const string DefThemeAssemblyMask = "Core.Controls.Theming.{0}.SL5";

        /// <summary>
        /// Default Param:  Mask Need Params :  {0}-[ThemeAssemblyName]; {1}-[Folder]; {2}-[ResDictionaryName]
        /// </summary>
        public const string DefThemeResDictionaryMask = "/{0};component/{1}/{2}";

        /// <summary>
        /// Default Param: All ResDictionaries in  Theme  -  Register- Convention based Name
        /// </summary>
        public const string DefThemeDescriptionFile = "ThemeDescription.xml";

        /// <summary>
        /// Default Param: Mask Need Params : {0}-[ThemeAssemblyName]; 
        /// </summary>
        public const string DefThemeDescriptionFileMask = "/{0};component/ThemeDescription.xml";


        #endregion ------------------------------ DEFAULT PARAM VALUES for THEME LOADING METHODS ---------------------------------


        #endregion ------------------------------- FIELDS ----------------------------------
        

        #region  ----------------------------- PROPERTIES -------------------------------

        /// <summary>
        /// Current Platform Theme resource dictionaries
        ///  For SL5 it is - Application.Current.Resources
        /// </summary>
        public static ResourceDictionary GlobalThemeDictionary
        {
            get; private set;
        }



        /// <summary>
        /// Текущая тема
        /// </summary>
        public static ThemeLoadInfo CurrentTheme
        {
            get;
            internal set; //notification???
        }



        #endregion ----------------------------- PROPERTIES -------------------------------
        

        #region  ------------------------------------ITSELF METHODS --------------------------------------



        /// <summary>
        /// Словари стилей которые будут загружаться по умолчанию для каждой темы  если не указан другой конкретный список 
        /// </summary>
        /// <returns></returns>
        public static List<PackageFileTp> DefaultToLoadPackageStyles()
        {
            List<PackageFileTp> DefaultThemeDictionaries = new List<PackageFileTp>();


            //Folder - Infragistics:      
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Theme.Colors.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Styles.Shared.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Styles.SL.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamDialogWindow.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamMenu.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamRibbon.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamRibbonBrushes.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamRibbonPartStyles.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamDialogWindow.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamRibbonSB.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamSchedule.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamCalendar.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("Infragistics", "Metro.xamMaskedInput.xaml"));

            //Folder - SLTolkit:            
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.LabelTextBlock.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ScrollBars.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ScrollViewer.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.Tooltip.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.Validation.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ChildWindow.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.AppPanel.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.DataGrid.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.DataPager.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.NumericUpDown.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.FilterBoxControl.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.BusyIndicators.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ToolbarControl.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ComboBox.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ListBox.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.TabControl.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.Expander.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.Accordion.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.ExchangeControls.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.SLToolkit.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.TimePicker.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.BlacklightControls.xaml"));
            DefaultThemeDictionaries.Add(new PackageFileTp("SLToolkit", "Metro.FlowMenuControls.xaml"));

            //<PackageFile Folder="SLToolkit" Name="Metro.BlacklightControls"/>
            //<PackageFile Folder="SLToolkit" Name="Metro.FlowMenuControls.xaml"/>


            return DefaultThemeDictionaries;

        }




        /// <summary>
        /// Перегрузить тему 
        /// </summary>
        /// <param name="ThemeName"></param>
        /// <param name="ThemeAssemblyMask"></param>
        /// <param name="ThemeDescriptionFile"></param>
        /// <param name="ThemeDescriptionFileMask"></param>
        /// <param name="ThemeResDictionaryMask"></param>
        /// <param name="OnlyNeedableToLoadItems"></param>
        public static void ReloadTheme(
                                         String ThemeName = ThemeManager.DefThemeName,
                                         String ThemeAssemblyMask = ThemeManager.DefThemeAssemblyMask,

                                         String ThemeDescriptionFile = ThemeManager.DefThemeDescriptionFile,
                                         String ThemeDescriptionFileMask = ThemeManager.DefThemeDescriptionFileMask,

                                         String ThemeResDictionaryMask = ThemeManager.DefThemeResDictionaryMask,
                                         List<PackageFileTp> OnlyNeedableToLoadItems = null
                                      )
        {
            GlobalThemeDictionary.ReloadTheme(ThemeName,
                                              ThemeAssemblyMask, ThemeDescriptionFile,
                                              ThemeDescriptionFileMask,
                                              ThemeResDictionaryMask,
                                              OnlyNeedableToLoadItems);
        }



        /// <summary>
        /// Перегрузить Стиль Контрола до подходящего ему по типу, текущего, который у нас в текущем глобальном словаре темы
        /// </summary>
        /// <param name="control"></param>
        public static void ResetThemeToCurrent(Control control, String StyleName = null)
        {
            Contract.Requires(control != null, "ThemeManager.ResetThemeToCurrent(): control for who we try to find and apply style cannot be null ");

            if (String.IsNullOrEmpty(StyleName) == false &&
                GlobalThemeDictionary.Contains(StyleName))
            {
                control.Style = GlobalThemeDictionary[StyleName] as Style;
            }
            else if (GlobalThemeDictionary.Contains(control.GetSLToolkitStyleName()))
            {
                control.Style = GlobalThemeDictionary[control.GetSLToolkitStyleName()] as Style;
            }
        }


        /// <summary>
        /// Название стиля контрола- билд строки названия
        /// </summary>
        /// <param name="contrl"></param>
        /// <returns></returns>
        private static String GetSLToolkitStyleName(this Control contrl)
        {
            return contrl.GetType().Name + "Style";
        }


        #endregion ------------------------------------ITSELF METHODS --------------------------------------


        #region  -------------------------------- EXTENSION METHODS ---------------------------------------


        /// <summary>
        ///  Reload UI Theme 
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="ThemeName"></param>
        /// <param name="ThemeAssemblyMask"></param>
        /// <param name="ThemeDescriptionFile"></param>
        /// <param name="ThemeDescriptionFileMask"></param>
        /// <param name="ThemeResDictionaryMask"></param>
        /// <param name="OnlyNeedableToLoadItems"></param>
        public static void ReloadTheme(this ResourceDictionary dictionary,
                                         String ThemeName = ThemeManager.DefThemeName,
                                         String ThemeAssemblyMask = ThemeManager.DefThemeAssemblyMask,

                                         String ThemeDescriptionFile = ThemeManager.DefThemeDescriptionFile,
                                         String ThemeDescriptionFileMask = ThemeManager.DefThemeDescriptionFileMask,

                                         String ThemeResDictionaryMask = ThemeManager.DefThemeResDictionaryMask,
                                         List<PackageFileTp> OnlyNeedableToLoadItems = null
                                      )
        {
            Contract.Requires(dictionary != null, "ThemeManager.ReloadTheme(): dictionary can not be null ");


            //DefThemeAssemblyMask
            String AssemblyName = String.Format(ThemeAssemblyMask, ThemeName);
            String PathToDescriptor = String.Format(ThemeDescriptionFileMask, AssemblyName);

            PackageFilesCollectionTp themeFiles = new PackageFilesCollectionTp();
            String xmlContent = ResourceLoader.LoadStringRes(new Uri(PathToDescriptor, UriKind.Relative));
            themeFiles = PackageFilesCollectionTp.DeserializeFromXml(xmlContent);


            // Creating new Theme    
            CurrentTheme.ClearTheme();
            CurrentTheme.ThemeName = ThemeName;
            CurrentTheme.ThemeAssembly = AssemblyName;


            // PackageFiles   
            foreach (var packfile in themeFiles.PackageFiles)
            {
                //список элементов темы создатся для всех файлов но не все будут загружаться

                ThemeItem newThemeItem = new ThemeItem();
                newThemeItem.ResourceFile = packfile;

                //Load needable Resource
                string ThemeResDictionaryName = String.Format(ThemeResDictionaryMask, AssemblyName, packfile.Folder, packfile.Name);
                newThemeItem.ResDictionaryUri = new Uri(ThemeResDictionaryName, UriKind.Relative);

                //если задан список только необходимых то файл должен находится в этом списке и только тогда он подгружается
                if (OnlyNeedableToLoadItems != null && OnlyNeedableToLoadItems.Where(pf => pf.Name == packfile.Name).FirstOrDefault() != null)
                {
                    newThemeItem.IsNeedableItem = true;
                    //если еще не загружен то только тогда мы его загружаем
                    if (dictionary.GetResourceDictionaryIndex(newThemeItem.ResDictionaryUri.OriginalString) == -1) //т.е. если его еще нет то подгрузить-слить его(Merge)
                    {
                        try
                        {
                            dictionary.ReplaceResourceDictionary(newThemeItem.ResDictionaryUri);
                            newThemeItem.IsLoadedItem = true;
                        }
                        catch (System.Exception)
                        {   //чтоб видеть более адекватное сообщение об ошибке
                            Contract.Assert(newThemeItem.IsLoadedItem == true,
                                           String.Format(" ThemeManager.ReloadTheme():  ResourceDictionary with Uri- [{0}] couldn't be Applied. " +
                                                          " Check that style formed Correctly", newThemeItem.ResDictionaryUri.OriginalString));
                        }
                    }
                }


                CurrentTheme.Items.Add(newThemeItem);
            }//foreach


            foreach (var item in CurrentTheme.Items)
            {
                //Проверка загрузки всех - только необходимых элементов темы
                if (item.IsNeedableItem == true)
                {
                    Contract.Assert(item.IsLoadedItem == true,
                    String.Format(@" ThemeManager.ReloadTheme(): Не найден Необходимый элемент [{0}]; в сборке [{1}]; выбранная тема [{2}] " +
                                    " Check that Resource exist in assembly or its property BuildAction = Resource", item.ResourceFile.Name, AssemblyName, ThemeName));
                }


            }




        }








        #endregion -------------------------------- EXTENSION METHODS ---------------------------------------
        

    }
}
