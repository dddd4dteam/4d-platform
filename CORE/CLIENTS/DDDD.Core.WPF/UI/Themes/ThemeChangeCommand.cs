﻿using System;
using System.Windows.Input;

namespace DDDD.Core.UI.Themes
{


    /// <summary>
    /// 
    /// </summary>
    public class ThemeChangeCommand : ICommand
    {
        #region ICommand Members

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;



        /// <summary>
        /// 
        /// </summary>
        /// <param name="parameter"></param>
        public void Execute(object parameter)
        {
            string themeName = parameter as String;//  "MetroLightTouch";
            ThemeManager.ReloadTheme(themeName);


            if (CanExecuteChanged != null)
                CanExecuteChanged(this, new EventArgs());
        }


        #endregion
    }
}
