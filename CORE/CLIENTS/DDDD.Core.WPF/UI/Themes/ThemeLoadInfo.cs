﻿using System.Collections.Generic;

namespace DDDD.Core.UI.Themes
{
    /// <summary>
    /// Information about loaded UI theme: about loaded Theme components. 
    /// </summary>
    public class ThemeLoadInfo
    {

        #region  --------------------- CTOR -----------------------

        public ThemeLoadInfo()
        {
            Items = new List<ThemeItem>();
        }

        #endregion --------------------- CTOR -----------------------


        #region  ------------------- PROPERTIES ------------------------

        /// <summary>
        /// Theme Name
        /// </summary>
        public string ThemeName
        {
            get;
            internal set;
        }


        /// <summary>
        /// Theme Assembly - assembly where from component styles was loaded        /// 
        /// </summary>
        public string ThemeAssembly
        {
            get;
            internal set;
        }


        /// <summary>
        /// Theme Elements
        /// </summary>
        public List<ThemeItem> Items
        {
            get;
            internal set;
        }

        #endregion ------------------- PROPERTIES ------------------------


        public void ClearTheme()
        {
            Items.Clear();
            ThemeAssembly = null;
            ThemeName = null;
        }

    }
}
