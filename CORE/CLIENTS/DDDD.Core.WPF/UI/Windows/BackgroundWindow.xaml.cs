﻿using System.Windows;

namespace DDDD.Core.UI.Windows
{
    /// <summary>
    /// BackgroundWindow that can be used to run App on the Background Process.
    /// </summary>
    public partial class BackgroundWindow : Window 
    { 
        public BackgroundWindow()
        {
            InitializeComponent(); 
        }
    }
}
