﻿namespace DDDD.Core.UI.Resources.IG
{
	/// <summary>
	/// Determines the method used to wash colors. 
	/// </summary>
	/// <seealso cref="T:ResourceWasher" />
	/// <seealso cref="P:ResourceWasher.WashMode" />
	public enum WashMode
	{
		/// <summary>
		/// Blends each of the RGB color values with the corresponding wash color values. 
		/// </summary>
		SoftLightBlend,
		/// <summary>
		/// Replaces the hue and saturation values with the corresponding values from the wash color but retains the brightness value.
		/// </summary>
		HueSaturationReplacement
	}
}
