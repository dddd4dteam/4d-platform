﻿

using System.ComponentModel;
using System.Windows.Media;

using DDDD.Core.Converters;

namespace DDDD.Core.UI.Resources.IG
{


    /// <summary>
    /// Defines the color to use to wash a group of resources
    /// </summary>
    /// <seealso cref="T:WashGroupCollection" />
    /// <seealso cref="T:ResourceWasher" />
    /// <seealso cref="F:ResourceWasher.WashGroupProperty" />
    /// <seealso cref="P:ResourceWasher.WashGroups" />
    public class WashGroup
    {
        private string _name;

        private Color? _washColor = null;

        private float _hue;

        private float _saturation;

        private WashMode? _washMode = null;

        internal float Hue
        {
            get
            {
                return this._hue;
            }
        }

        /// <summary>
        /// Gets/sets the name of the group.
        /// </summary>
        /// <remarks>
        /// <para class="note">
        /// <b>Note:</b> Any brushes found in the source dictionary whose <see cref="F:ResourceWasher.WashGroupProperty" /> attached property mathes this name will be washed with the specified WashColor.
        /// </para>
        /// </remarks>
        /// <seealso cref="M:ResourceWasher.SetWashGroup(System.Windows.DependencyObject,System.String)" />
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        [DefaultValue(null)]
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        internal float Saturation
        {
            get
            {
                return this._saturation;
            }
        }

        /// <summary>
        /// Gets/sets the color to use to wash the resources in this group.
        /// </summary>
        /// <remarks>
        /// <para class="body">The color to use to wash any resources whose <see cref="F:ResourceWasher.WashGroupProperty" /> matches this group's <see cref="P:WashGroup.Name" />.</para>
        /// <para class="note"><b>Note:</b> if this property is left to its default value of <b>Colors.Transparent</b> then the resources in this group will not be cloned and washed. Instead they will be copied over without cloning or modification.</para>
        /// </remarks>
        /// <seealso cref="M:ResourceWasher.SetWashGroup(System.Windows.DependencyObject,System.String)" />
        /// <seealso cref="P:ResourceWasher.WashGroups" />
        public Color? WashColor
        {
            get
            {
                return this._washColor;
            }
            set
            {
                this._washColor = value;
                Color? nullable = value;
                Color color = (nullable.HasValue ? nullable.GetValueOrDefault() : Colors.Transparent);
                this._hue = ResourceWasher.GetHue(color);
                this._saturation = ResourceWasher.GetSaturation(color);
            }
        }

        /// <summary>
        /// Gets/sets the method used to wash colors in the resources in the SourceDictionary in this group.
        /// </summary>
        /// <seealso cref="P:ResourceWasher.WashMode" />
        [TypeConverter(typeof(NullableEnumTypeConverter<WashMode>))]
        public WashMode? WashMode
        {
            get
            {
                return this._washMode;
            }
            set
            {
                this._washMode = value;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public WashGroup()
        {
        }

        /// <summary>
        /// Resets the <see cref="P:WashGroup.WashColor" /> property to its default state
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashColor()
        {
            this._washColor = null;
        }

        /// <summary>
        /// Resets the <see cref="P:WashGroup.WashMode" /> property to its default state
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public void ResetWashMode()
        {
            this._washMode = null;
        }

        /// <summary>
        /// Determines if the <see cref="P:WashGroup.WashColor" /> property needs to be serialized.
        /// </summary>
        /// <returns>True if the property should be serialized</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashColor()
        {
            return this._washColor.HasValue;
        }

        /// <summary>
        /// Determines if the <see cref="P:WashGroup.WashMode" /> property needs to be serialized.
        /// </summary>
        /// <returns>True if the property should be serialized</returns>
        [EditorBrowsable(EditorBrowsableState.Never)]
        public bool ShouldSerializeWashMode()
        {
            return this._washMode.HasValue;
        }
    }
}
