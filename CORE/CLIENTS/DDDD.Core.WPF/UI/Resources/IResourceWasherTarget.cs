﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.UI.Resources.IG
{

    internal interface IResourceWasherTarget
    {
        ResourceWasher ResourceWasher
        {
            get;
            set;
        }
    }
}
