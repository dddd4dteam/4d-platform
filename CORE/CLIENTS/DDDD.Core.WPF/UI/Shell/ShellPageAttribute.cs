﻿using System;
using DDDD.Core.ComponentModel;
using DDDD.Core.Extensions;


namespace DDDD.Core.UI
{

    /// <summary>
    /// Shell UI Control  based  on  System.Windows.Window   control  class. 
    /// Usually it's  SL5 , WP81  client.
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = false)]
    public sealed class ShellPageAttribute : Attribute , IComponentMatchMetaInfo
    {

        #region -------------------------------- CTOR -------------------------------

        // This is a positional argument
        public ShellPageAttribute(string shellKey)
        {
            ShellKey = shellKey;
        }

        #endregion -------------------------------- CTOR -------------------------------
        

        #region ---------------------- IComponentItemMetaInfo --------------------------

        /// <summary>
        /// Component Class Key 
        /// </summary>
        public string ComponentDefinitionKey
        {
            get
            {
                return ComponentClassEn.Pages.S();
            }
        }

        #endregion ---------------------- IComponentItemMetaInfo --------------------------


        /// <summary>
        /// Filter key that can differ one ShellControl from another.
        /// </summary>
        public string ShellKey
        { get; set; }
        

    }


}
