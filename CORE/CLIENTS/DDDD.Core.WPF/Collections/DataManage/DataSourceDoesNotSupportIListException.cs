﻿using System;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An <see cref="T:System.Exception" /> that is thrown when the GetEnumerator method of the IEnumerable returns null for the <see cref="T: DataManagerBase" />.
    /// </summary>
    public class DataSourceDoesNotSupportIListException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataSourceDoesNotSupportIListException" /> class.
        /// </summary>
        public DataSourceDoesNotSupportIListException() : base( "DataSourceDoesNotSupportIListException")// SR.GetString  -  [12/14/2016 A1]
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataSourceDoesNotSupportIListException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        public DataSourceDoesNotSupportIListException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataSourceDoesNotSupportIListException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        /// <param name="innerException">An inner exception.</param>
        public DataSourceDoesNotSupportIListException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
