﻿using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;

using System;
using System.Linq;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// A nongeneric abstract class representing a summary on an object.
    /// </summary>
    public abstract class SummaryContext
    {

        #region --------------------- CTOR ------------------------
        protected SummaryContext()
        {
        }

        #endregion --------------------- CTOR ------------------------


        /// <summary>
        /// Gets the name of the property on the data object that will be summed on.
        /// </summary>
        public string FieldName
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the LinqSummaryOperator associated with this <see cref="T: SummaryContext" />.
        /// </summary>
        public LinqSummaryOperator LinqSummary
        {
            get;
            protected set;
        }



        /// <summary>
        /// Executes a LINQ based Average summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected abstract object Average(IQueryable query);

        /// <summary>
        /// Executes a LINQ based Count summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected abstract int Count(IQueryable query);

        /// <summary>
        /// Creates a SummaryContext instanced typed to the object type of the data being processed.
        /// </summary>
        /// <param name="cachedTypeInfo">The <see cref="T: CachedTypedInfo" /> object which has the type info for this method.</param>
        /// <param name="propertyName">The field data type that will be processed on.</param>
        /// <param name="linqSummary">The LINQ statement which will be used.</param>
        /// <returns></returns>		
        public static SummaryContext CreateGenericSummary(CachedTypedInfo cachedTypeInfo, string propertyName, LinqSummaryOperator linqSummary)
        {
            Type cachedType = cachedTypeInfo.CachedType;
            Type type = DataManagerBase.ResolvePropertyTypeFromPropertyName(propertyName, cachedTypeInfo);
            Type type1 = typeof(SummaryContext<,>);
            Type[] typeArray = new Type[] { cachedType, type };
            Type type2 = type1.MakeGenericType(typeArray);
            object[] objArray = new object[] { propertyName, linqSummary, cachedTypeInfo };
            return TypeActivator.CreateInstanceTBaseLazy<SummaryContext>(type2, TypeActivator.DefaultCtorSearchBinding, objArray);//(SummaryContext)Activator.CreateInstance(type2, objArray);
        }

        /// <summary>
        /// Performs a LINQ based summary on the inputted query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object Execute(IQueryable query)
        {
            object obj = null;
            switch (this.LinqSummary)
            {
                case LinqSummaryOperator.Count:
                    {
                        obj = this.Count(query);
                        break;
                    }
                case LinqSummaryOperator.Minimum:
                    {
                        obj = this.Minimum(query);
                        break;
                    }
                case LinqSummaryOperator.Maximum:
                    {
                        obj = this.Maximum(query);
                        break;
                    }
                case LinqSummaryOperator.Sum:
                    {
                        obj = this.Sum(query);
                        break;
                    }
                case LinqSummaryOperator.Average:
                    {
                        obj = this.Average(query);
                        break;
                    }
            }
            return obj;
        }

        /// <summary>
        /// Executes a LINQ based Maximum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected abstract object Maximum(IQueryable query);

        /// <summary>
        /// Executes a LINQ based Minimum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected abstract object Minimum(IQueryable query);

        /// <summary>
        /// Executes a LINQ based Sum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected abstract object Sum(IQueryable query);
    }
}
