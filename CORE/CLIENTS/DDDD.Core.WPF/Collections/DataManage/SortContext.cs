﻿
using System;
using System.Linq;
using System.Windows.Data;
using DDDD.Core.Caching.IG;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{

    public abstract class SortContext
    {


        #region -------------------- PROPERTIES -----------------------------

        public string SortPropertyName { get; protected set; }

        public bool SortAscending { get; protected set; }

        public Type PropertyType { get; protected set; }

        public Type DataType { get; protected set; }

        public bool CaseSensitiveSort { get; protected set; }

        #endregion -------------------- PROPERTIES -----------------------------


        public static SortContext CreateGenericSort(CachedTypedInfo cachedTypeInfo, string propertyName, bool sortAscending, bool isCaseSensitiveSort, object comparer)
        {
            return TypeActivator.CreateInstanceTBaseLazy<SortContext>( //(SortContext)Activator.CreateInstance(
                typeof(SortContext<,>).MakeGenericType(cachedTypeInfo.CachedType, DataManagerBase.ResolvePropertyTypeFromPropertyName(propertyName, cachedTypeInfo))
                , TypeActivator.DefaultCtorSearchBinding
                , new object[5]
                  {
                    (object) propertyName,
                    (object) sortAscending,
                    (object) isCaseSensitiveSort,
                    comparer,
                    (object) cachedTypeInfo
                  }
            );
        }

        public static SortContext CreateGenericSort(CachedTypedInfo cachedTypeInfo, bool sortAscending, object comparer, IValueConverter converter, object converterParam)
        {
            Type cachedType = cachedTypeInfo.CachedType;
            return    TypeActivator.CreateInstanceTBaseLazy<SortContext>(   //(SortContext)Activator.CreateInstance(                  
                typeof(SortContext<,>).MakeGenericType(cachedType, cachedType)
                ,TypeActivator.DefaultCtorSearchBinding
                , new object[5]
                 {
                    (object) sortAscending,
                    comparer,
                    (object) converter,
                    converterParam,
                    (object) cachedTypeInfo
                }
             );
        }

        public abstract IOrderedQueryable<T> Sort<T>(IQueryable<T> query);

        public abstract IOrderedQueryable<T> AppendSort<T>(IOrderedQueryable<T> query);

    }
}
