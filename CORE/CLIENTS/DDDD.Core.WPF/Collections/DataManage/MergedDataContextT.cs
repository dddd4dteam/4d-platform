﻿
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Data;

using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;
using DDDD.Core.Collections.Merging.IG;
using DDDD.Core.Collections.Compare.IG;

namespace DDDD.Core.Collections.DataManage.IG
{


    /// <summary>
    /// An object that encapsulates the Merging functionality used by the <see cref="T: DataManagerBase" />
    /// </summary>
    /// <typeparam name="T">The data object that the field belongs to.</typeparam>
    /// <typeparam name="TColumnType">The type of data that should be merged by.</typeparam>
    public class MergedDataContext<T, TColumnType> : MergedDataContext
    {

        #region --------------------------- CTOR -------------------------------

        /// <summary>
        /// Creates a new instance of the <see cref="T: MergedDataContext" />.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="comparer"></param>
        /// <param name="typedInfo"></param>
        public MergedDataContext(string propertyName, object comparer, CachedTypedInfo typedInfo)
        {
            base.PropertyName = propertyName;
            this.Comparer = comparer as IEqualityComparer<TColumnType>;
            this._unboundColumn = false;
            this.TypedInfo = typedInfo;
        }

        /// <summary>
        /// Creates a new instance of the <see cref="T: MergedDataContext" />.
        /// </summary>
        /// <param name="comparer"></param>
        /// <param name="converter"></param>
        /// <param name="converterParam"></param>
        /// <param name="typedInfo"></param>
        public MergedDataContext(object comparer, IValueConverter converter, object converterParam, CachedTypedInfo typedInfo)
        {
            this._unboundColumn = true;
            this.Comparer = comparer as IEqualityComparer<TColumnType>;
            this.Converter = converter;
            this.ConverterParameter = converterParam;
            this.TypedInfo = typedInfo;
        }


        #endregion --------------------------- CTOR -------------------------------



        private bool _unboundColumn;

        private MergedDataContext _nextMergeDataContext;

        private List<MergedDataContext> _mdcs;

        private Dictionary<T, MergedRowInfo> _rowsDictionary;

        private List<MergedRowInfo> _rows;

        /// <summary>
        /// Gets the Comparer that will be used to perform the merging.
        /// </summary>
        public IEqualityComparer<TColumnType> Comparer
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the <see cref="T:System.Windows.Data.IValueConverter" /> which will be used to evaluate the merge.
        /// </summary>
        protected IValueConverter Converter
        {
            get;
            private set;
        }

        /// <summary>
        /// The parameter applied to the <see cref="P: MergedDataContext`2.Converter" />.
        /// </summary>
        protected object ConverterParameter
        {
            get;
            private set;
        }

        /// <summary>
        /// A collection of summaries that should be applied to each subset of merged groupings.
        /// </summary>
        protected SummaryDefinitionCollection Summaries
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/Sets the TypedInfo for the object.
        /// </summary>
        protected CachedTypedInfo TypedInfo
        {
            get;
            set;
        }

        /// <summary>
        /// Takes the given MergedColumnInfo, and determines if needs to create a new MergedRowInfo or use and existing one, then appends itself to that RowInfo
        /// </summary>
        /// <param name="data"></param>
        /// <param name="mci"></param>
        /// <param name="index"></param>
        /// <param name="lastIndex"></param>
        /// <param name="mergedObject"></param>
        /// <returns></returns>
        public MergedRowInfo AppendColumnInfoToRowInfo(T data, MergedColumnInfo mci, int index, int lastIndex, object mergedObject)
        {
            MergedRowInfo item = null;
            if (!this._rowsDictionary.ContainsKey(data))
            {
                MergedRowInfo mergedRowInfo = new MergedRowInfo()
                {
                    Data = data,
                    MergedGroups = new List<MergedColumnInfo>()
                };
                item = mergedRowInfo;
                this._rowsDictionary.Add(data, item);
                this._rows.Add(item);
            }
            else
            {
                item = this._rowsDictionary[data];
            }
            if (index == lastIndex)
            {
                if (index != 0)
                {
                    item.IsLastRowInGroup.Add(mergedObject, true);
                    item.IsFirstRowInGroup.Add(mergedObject, false);
                }
                else
                {
                    item.IsLastRowInGroup.Add(mergedObject, true);
                    item.IsFirstRowInGroup.Add(mergedObject, true);
                }
            }
            else if (index != 0)
            {
                item.IsFirstRowInGroup.Add(mergedObject, false);
                item.IsLastRowInGroup.Add(mergedObject, false);
            }
            else
            {
                item.IsFirstRowInGroup.Add(mergedObject, true);
                item.IsLastRowInGroup.Add(mergedObject, false);
            }
            item.MergedGroups.Add(mci);
            return item;
        }

        /// <summary>
        /// Merges the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="mdcs"></param>
        /// <param name="summaries"></param>
        /// <returns></returns>
        public override IList Merge(IQueryable query, List<MergedDataContext> mdcs, SummaryDefinitionCollection summaries)
        {
            this._rows.Clear();
            this._rowsDictionary.Clear();
            this.Summaries = summaries;
            return this.Merge(query, mdcs, new MergeDelegate<object, IEnumerable, MergedDataContext, object, MergedColumnInfo, object>(this.OnGrouping), null);
        }

        /// <summary>
        /// Merges the specified <see cref="T:System.Linq.IQueryable" /> by the property this data represents.
        /// </summary>
        /// <param name="iquery"></param>
        /// <param name="mdcs"></param>
        /// <param name="method"></param>
        /// <param name="parentMCI"></param>
        /// <returns></returns>
        protected internal override IList Merge(IQueryable iquery, List<MergedDataContext> mdcs, MergeDelegate<object, IEnumerable, MergedDataContext, object, MergedColumnInfo, object> method, MergedColumnInfo parentMCI)
        {
            IQueryable<T> ts = iquery as IQueryable<T>;
            if (ts != null)
            {
                this._mdcs = mdcs;
                int num = mdcs.IndexOf(this);
                if (num == mdcs.Count - 1)
                {
                    this._nextMergeDataContext = null;
                }
                else
                {
                    this._nextMergeDataContext = mdcs[num + 1];
                }
                ParameterExpression parameterExpression = Expression.Parameter(typeof(T), "param");
                Expression expression = null;
                if (!this._unboundColumn)
                {
                    TColumnType tColumnType = default(TColumnType);
                    expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(base.PropertyName, parameterExpression, this.TypedInfo, typeof(TColumnType), tColumnType);
                }
                else
                {
                    expression = parameterExpression;
                }
                if (!typeof(T).IsValueType)
                {
                    Expression expression1 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(T)));
                    Expression expression2 = Expression.Constant(default(TColumnType), typeof(TColumnType));
                    expression = Expression.Condition(expression1, expression2, expression);
                }
                ParameterExpression[] parameterExpressionArray = new ParameterExpression[] { parameterExpression };
                Expression<Func<T, TColumnType>> expression3 = Expression.Lambda(expression, parameterExpressionArray) as Expression<Func<T, TColumnType>>;
                IEqualityComparer<TColumnType> comparer = this.Comparer;
                if (comparer == null && this._unboundColumn)
                {
                    comparer = new ValObjComparer<TColumnType>(this.Converter, this.ConverterParameter);
                }
                IEnumerable<object> objs = ts.AsEnumerable<T>().GroupBy<T, TColumnType, object>(expression3.Compile(), (TColumnType key, IEnumerable<T> list) => method(key, list, this._nextMergeDataContext, this.MergedObject, parentMCI), comparer);
                objs.ToList<object>();
            }
            return this._rows;
        }

        /// <summary>
        /// Invoked by the GroupBy query, this will recurse through all MergeDataContexts and Invoke their merge method.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="list"></param>
        /// <param name="nextMergeDataContext"></param>
        /// <param name="mergedObject"></param>
        /// <param name="parentMCI"></param>
        /// <returns></returns>
        public object OnGrouping(object key, IEnumerable list, MergedDataContext nextMergeDataContext, object mergedObject, MergedColumnInfo parentMCI)
        {
            IEnumerable<T> ts = list as IEnumerable<T>;
            MergedColumnInfo mergedColumnInfo = new MergedColumnInfo()
            {
                Key = key,
                Children = ts.ToList<T>(),
                DataType = typeof(T),
                Summaries = this.Summaries,
                ParentMergedColumnInfo = parentMCI,
                MergingObject = mergedObject,
                TypedInfo = this.TypedInfo
            };
            MergedColumnInfo mergedColumnInfo1 = mergedColumnInfo;
            int num = ts.Count<T>() - 1;
            ts.Select<T, MergedRowInfo>((T comic, int index) => this.AppendColumnInfoToRowInfo(comic, mergedColumnInfo1, index, num, mergedObject)).ToList<MergedRowInfo>();
            if (nextMergeDataContext != null)
            {
                nextMergeDataContext.Merge(list.AsQueryable(), this._mdcs, new MergeDelegate<object, IEnumerable, MergedDataContext, object, MergedColumnInfo, object>(this.OnGrouping), mergedColumnInfo1);
            }
            return mergedColumnInfo1;
        }
    }
}
