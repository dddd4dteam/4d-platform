﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Condition.IG;
using DDDD.Core.Reflection;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// A <see cref="T: FilterContext" /> object typed to a particular object.
    /// </summary>
    /// <typeparam name="TDataObject">The type of the object that will be processed against.</typeparam>
    public class FilterContext<TDataObject> : FilterContext
    {
        /// <summary>
        /// Gets the info describing the Object.Equals method.
        /// </summary>
        protected readonly static MethodInfo EqualsMethod;

        static FilterContext()
        {
            Type type = typeof(object);
            Type[] typeArray = new Type[] { typeof(object) };
            FilterContext<TDataObject>.EqualsMethod = type.GetMethod("Equals", typeArray);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: FilterContext" /> class.
        /// </summary>
        /// <param name="fieldDataType"></param>
        /// <param name="caseSensitive"></param>
        /// <param name="fromDateColumn"></param>
        /// <param name="cti"></param>
        public FilterContext(Type fieldDataType, bool caseSensitive, bool fromDateColumn,  CachedTypedInfo cti)
        {
            base.CaseSensitive = caseSensitive;
            base.FieldDataType = fieldDataType;
            base.FromDateColumn = fromDateColumn;
            base.CachedTypedInfo = cti;
        }

        internal override Expression And(Expression left, Expression right)
        {
            return this.AndAlsoExpression<TDataObject>((Expression<Func<TDataObject, bool>>)left, (Expression<Func<TDataObject, bool>>)right);
        }

        /// <summary>
        /// Combines two <see cref="T:System.Linq.Expressions.Expression" /> objects with a AND Expression.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which is the combination of the two inputs.</returns>
        protected internal Expression<Func<TDataObject, bool>> AndAlsoExpression<TDataObjectType>(Expression<Func<TDataObject, bool>> left, Expression<Func<TDataObject, bool>> right)
        {
            InvocationExpression invocationExpression = Expression.Invoke(right, left.Parameters.Cast<Expression>());
            Expression expression = Expression.AndAlso(left.Body, invocationExpression);
            return Expression.Lambda<Func<TDataObject, bool>>(expression, left.Parameters);
        }

        /// <summary>
        /// Returns an <see cref="T:System.Linq.Expressions.Expression" /> for a case insensitive string operation.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal Expression CreateCaseInsensitiveStringExpression(string fieldName, ComparisonOperator op, object value)
        {
            string empty = string.Empty;
            string str = value as string;
            if (str != null)
            {
                empty = str;
            }
            Expression expression = null;
            ComparisonOperator comparisonOperator = op;
            switch (comparisonOperator)
            {
                case ComparisonOperator.Equals:
                    {
                        expression = this.CreateEqualsCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.NotEquals:
                    {
                        expression = this.CreateNotEqualsCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.GreaterThan:
                    {
                        expression = this.CreateStringGreaterThanCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.GreaterThanOrEqual:
                    {
                        expression = this.CreateStringGreaterThanOrEqualCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.LessThan:
                    {
                        expression = this.CreateStringLessThanCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.LessThanOrEqual:
                    {
                        expression = this.CreateStringLessThanOrEqualCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.StartsWith:
                    {
                        expression = this.CreateStartsWithCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotStartWith:
                    {
                        expression = this.CreateDoesNotStartWithCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.EndsWith:
                    {
                        expression = this.CreateEndsWithCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotEndWith:
                    {
                        expression = this.CreateDoesNotEndWithCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.Contains:
                    {
                        expression = this.CreateContainsCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotContain:
                    {
                        expression = this.CreateDoesNotContainCaseInsensitiveExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                default:
                    {
                        if (comparisonOperator == ComparisonOperator.InOperand)
                        {
                            List<string> strs = FilterContext<TDataObject>.ParseAndNormalizeValue(value, false);
                            expression = this.CreateStringInOperandCaseInsensitiveExpression<TDataObject>(fieldName, strs);
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
            }
            return expression;
        }

        /// <summary>
        /// Returns an <see cref="T:System.Linq.Expressions.Expression" /> for a case sensitive string operation.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal Expression CreateCaseSensitiveStringExpression(string fieldName, ComparisonOperator op, object value)
        {
            string empty = string.Empty;
            string str = value as string;
            if (str != null)
            {
                empty = str;
            }
            Expression expression = null;
            ComparisonOperator comparisonOperator = op;
            switch (comparisonOperator)
            {
                case ComparisonOperator.Equals:
                    {
                        expression = this.CreateEqualsExpression<TDataObject>(fieldName, value);
                        break;
                    }
                case ComparisonOperator.NotEquals:
                    {
                        expression = this.CreateNotEqualsExpression<TDataObject>(fieldName, value);
                        break;
                    }
                case ComparisonOperator.GreaterThan:
                    {
                        expression = this.CreateStringGreaterThanExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.GreaterThanOrEqual:
                    {
                        expression = this.CreateStringGreaterThanOrEqualExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.LessThan:
                    {
                        expression = this.CreateStringLessThanExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.LessThanOrEqual:
                    {
                        expression = this.CreateStringLessThanOrEqualExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.StartsWith:
                    {
                        expression = this.CreateStartsWithExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotStartWith:
                    {
                        expression = this.CreateDoesNotStartWithExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.EndsWith:
                    {
                        expression = this.CreateEndsWithExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotEndWith:
                    {
                        expression = this.CreateDoesNotEndWithExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.Contains:
                    {
                        expression = this.CreateContainsExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                case ComparisonOperator.DoesNotContain:
                    {
                        expression = this.CreateDoesNotContainExpression<TDataObject>(fieldName, empty);
                        break;
                    }
                default:
                    {
                        if (comparisonOperator == ComparisonOperator.InOperand)
                        {
                            List<string> strs = FilterContext<TDataObject>.ParseAndNormalizeValue(value, true);
                            expression = this.CreateInOperandExpression<TDataObject>(fieldName, strs);
                            break;
                        }
                        else
                        {
                            break;
                        }
                    }
            }
            return expression;
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive contains against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateContainsCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) >= 0);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive contains against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateContainsExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.Contains(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for date ranges.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="includeStartDate"></param>
        /// <param name="excludedEndDate"></param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDateRangeExpression<TDataObjectType>(string fieldName, DateTime includeStartDate, DateTime excludedEndDate)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObjectType), "parameter");
            Expression expression = null;
            Expression expression1 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression = Expression.Constant(includeStartDate, base.FieldDataType);
            Expression expression2 = Expression.GreaterThanOrEqual(expression1, expression);
            expression = Expression.Constant(excludedEndDate, base.FieldDataType);
            Expression expression3 = Expression.LessThan(expression1, expression);
            Expression expression4 = Expression.AndAlso(expression2, expression3);
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression5 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression4 = Expression.Condition(expression5, Expression.Constant(false), expression4);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression4, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for equality.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDateTimeTimeExcludedEqualsExpression<TDataObjectType>(string fieldName, object value)
        {
            Expression expression;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObjectType), "parameter");
            Expression expression1 = null;
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression1 = Expression.Constant(value, base.FieldDataType);
            if (value == null)
            {
                expression = Expression.Equal(expression2, expression1);
            }
            else
            {
                Expression expression3 = Expression.GreaterThanOrEqual(expression2, expression1);
                DateTime dateTime = ((DateTime)value).AddDays(1);
                expression1 = Expression.Constant(dateTime, base.FieldDataType);
                Expression expression4 = Expression.LessThan(expression2, expression1);
                expression = Expression.AndAlso(expression3, expression4);
            }
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression5 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression = Expression.Condition(expression5, Expression.Constant(false), expression);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive does not contain against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotContainCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || x.IndexOf(value, StringComparison.CurrentCultureIgnoreCase) == -1);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive does not contain against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotContainExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.Contains(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive does not ends with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotEndWithCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.EndsWith(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive does not ends with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotEndWithExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.EndsWith(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive does not starts with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotStartWithCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.StartsWith(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive does not starts with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateDoesNotStartWithExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.StartsWith(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive ends with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateEndsWithCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.EndsWith(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive ends with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateEndsWithExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.EndsWith(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive equals against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateEqualsCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return this.CreateStringExpression<TDataObject>(fieldName, (string x) => string.IsNullOrEmpty(x));
            }
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.Equals(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for equality.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateEqualsExpression<TDataObjectType>(string fieldName, object value)
        {
            Expression expression;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObjectType), "parameter");
            Expression expression1 = null;
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression1 = Expression.Constant(value, base.FieldDataType);
            if (base.FromDateColumn && value != null)
            {
                Expression expression3 = Expression.GreaterThanOrEqual(expression2, expression1);
                DateTime dateTime = ((DateTime)value).AddDays(1);
                expression1 = Expression.Constant(dateTime, base.FieldDataType);
                Expression expression4 = Expression.LessThan(expression2, expression1);
                expression = Expression.AndAlso(expression3, expression4);
            }
            else if (expression2.Type.IsValueType)
            {
                expression = Expression.Equal(expression2, expression1);
            }
            else
            {
                Expression expression5 = Expression.Equal(expression2, Expression.Constant(null, expression2.Type));
                BinaryExpression binaryExpression = Expression.Equal(expression2, expression1);
                MethodInfo equalsMethod = FilterContext<TDataObject>.EqualsMethod;
                Expression[] expressionArray = new Expression[] { expression1 };
                expression = Expression.Condition(expression5, binaryExpression, Expression.Call(expression2, equalsMethod, expressionArray));
            }
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression6 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression = Expression.Condition(expression6, Expression.Constant(false), expression);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> based on all the terms in the <see cref="T: ConditionCollection" />.
        /// </summary>
        /// <param name="conditionGroup"></param>
        /// <returns></returns>
        public override Expression CreateExpression(ConditionCollection conditionGroup)
        {
            Expression<Func<TDataObject, bool>> currentExpression = null;
            Expression<Func<TDataObject, bool>> expression = null;
            if (conditionGroup.Count > 0)
            {
                currentExpression = (Expression<Func<TDataObject, bool>>)conditionGroup[0].GetCurrentExpression(this);
                for (int i = 1; i < conditionGroup.Count; i++)
                {
                    expression = (Expression<Func<TDataObject, bool>>)conditionGroup[i].GetCurrentExpression(this);
                    if (currentExpression == null)
                    {
                        currentExpression = expression;
                    }
                    else if (expression != null)
                    {
                        currentExpression = (conditionGroup.LogicalOperator != LogicalOperator.Or ? this.AndAlsoExpression<TDataObject>(currentExpression, expression) : this.OrElseExpression<TDataObject>(currentExpression, expression));
                    }
                }
            }
            return currentExpression;
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> from the <see cref="T: RecordFilterCollection" />
        /// </summary>
        /// <param name="conditionGroup"></param>
        /// <returns></returns>
        public override Expression CreateExpression(RecordFilterCollection conditionGroup)
        {
            Expression<Func<TDataObject, bool>> currentExpression = null;
            Expression<Func<TDataObject, bool>> expression = null;
            if (conditionGroup.Count > 0)
            {
                currentExpression = (Expression<Func<TDataObject, bool>>)conditionGroup[0].GetCurrentExpression();
                for (int i = 1; i < conditionGroup.Count; i++)
                {
                    expression = (Expression<Func<TDataObject, bool>>)conditionGroup[i].GetCurrentExpression();
                    if (currentExpression == null)
                    {
                        currentExpression = expression;
                    }
                    else if (expression != null)
                    {
                        currentExpression = (conditionGroup.LogicalOperator != LogicalOperator.Or ? this.AndAlsoExpression<TDataObject>(currentExpression, expression) : this.OrElseExpression<TDataObject>(currentExpression, expression));
                    }
                }
            }
            return currentExpression;
        }

        /// <summary>
        /// Creates an expression based on the <see cref="T: ComparisonOperator" />.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override Expression CreateExpression(string fieldName, ComparisonOperator op, object value)
        {
            if (base.FieldDataType != typeof(string))
            {
                return this.CreateObjectExpression(fieldName, op, value);
            }
            if (base.CaseSensitive)
            {
                return this.CreateCaseSensitiveStringExpression(fieldName, op, value);
            }
            return this.CreateCaseInsensitiveStringExpression(fieldName, op, value);
        }

        /// <summary>
        /// Creates a new <see cref="T:System.Linq.Expressions.Expression" />
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        public override Expression CreateExpression(string fieldName, ComparisonOperator op, object value, bool caseSensitive)
        {
            if (base.FieldDataType != typeof(string))
            {
                return this.CreateObjectExpression(fieldName, op, value);
            }
            if (caseSensitive)
            {
                return this.CreateCaseSensitiveStringExpression(fieldName, op, value);
            }
            return this.CreateCaseInsensitiveStringExpression(fieldName, op, value);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for greater than the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateGreaterThanExpression<TDataObjectType>(string fieldName, object value)
        {
            Expression expression;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression1 = null;
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression1 = Expression.Constant(value, base.FieldDataType);
            if (!base.FromDateColumn || value == null)
            {
                expression = Expression.GreaterThan(expression2, expression1);
            }
            else
            {
                DateTime dateTime = ((DateTime)value).AddDays(1);
                expression1 = Expression.Constant(dateTime, base.FieldDataType);
                expression = Expression.GreaterThanOrEqual(expression2, expression1);
            }
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression3 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression = Expression.Condition(expression3, Expression.Constant(false), expression);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for greater than or equal the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateGreaterThanOrEqualsExpression<TDataObjectType>(string fieldName, object value)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression = null;
            Expression expression1 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression = Expression.Constant(value, base.FieldDataType);
            Expression expression2 = Expression.GreaterThanOrEqual(expression1, expression);
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression3 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression2 = Expression.Condition(expression3, Expression.Constant(false), expression2);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression2, new ParameterExpression[] { parameterExpression });
        }



        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive inequality against a list of values.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="values">The IList of values that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateInOperandExpression<TDataObjectType>(string fieldName, IList values)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TDataObjectType), "parameter");
            Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, paramExpression, this.CachedTypedInfo, this.FieldDataType, this.GetDefaultValue());
            return Expression.Lambda<Func<TDataObject, bool>>(Expression.Invoke(
                (Expression<Func<object, bool>>)
                (x => x != (object)null && values != (object)null && values.Contains(x) || x == (object)null && values != (object)null && values.Contains((object)null))
                , new Expression[1] { (Expression)Expression.Convert(expression, typeof(object)) })
                , new ParameterExpression[1] { paramExpression });
        }




        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for last Month.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLastMonthExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            DateTime dateTime1 = DateTime.Today.AddMonths(-1);
            DateTime dateTime2 = new DateTime(dateTime1.Year, dateTime1.Month, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime2, dateTime);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for last Quarter.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLastQuarterExpression<TDataObjectType>(string fieldName)
        {
            FilterContext<TDataObject>.QuarterTracking previousQuarterInfo = this.GetPreviousQuarterInfo(DateTime.Today);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, previousQuarterInfo.StartDate, previousQuarterInfo.EndDate);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for last week.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLastWeekExpression<TDataObjectType>(string fieldName)
        {
            DateTime today = DateTime.Today;
            DateTime dateTime = DateTime.Today;
            DateTime dateTime1 = today.AddDays((double)(-(int)dateTime.DayOfWeek + (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
            dateTime1 = new DateTime(dateTime1.Year, dateTime1.Month, dateTime1.Day);
            dateTime1 = dateTime1.AddDays(-7);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime1, dateTime1.AddDays(7));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for last Year.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLastYearExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = new DateTime(DateTime.Today.Year, 1, 1);
            DateTime today = DateTime.Today;
            DateTime dateTime1 = new DateTime(today.Year - 1, 1, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime1, dateTime);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for less than the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLessThanExpression<TDataObjectType>(string fieldName, object value)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression = null;
            Expression expression1 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression = Expression.Constant(value, base.FieldDataType);
            Expression expression2 = Expression.LessThan(expression1, expression);
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression3 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression2 = Expression.Condition(expression3, Expression.Constant(false), expression2);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression2, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for less than or equal the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateLessThanOrEqualsExpression<TDataObjectType>(string fieldName, object value)
        {
            Expression expression;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression1 = null;
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression1 = Expression.Constant(value, base.FieldDataType);
            if (!base.FromDateColumn || value == null)
            {
                expression = Expression.LessThanOrEqual(expression2, expression1);
            }
            else
            {
                DateTime dateTime = ((DateTime)value).AddDays(1);
                expression1 = Expression.Constant(dateTime, base.FieldDataType);
                expression = Expression.LessThan(expression2, expression1);
            }
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression3 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression = Expression.Condition(expression3, Expression.Constant(false), expression);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression, new ParameterExpression[] { parameterExpression });
        }


        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that will evaluate an object's DateTime field for a particular month value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="month">The int month value which will be filterd for.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateMonthExpression<TDataObjectType>(string fieldName, int month)
        {
            Expression expression1;
            if ((object)this.FieldDataType == (object)typeof(DateTime))
                expression1 = (Expression<Func<DateTime, bool>>)(x => x.Month == month);
            else
                expression1 = (Expression<Func<DateTime, bool>>)(x => x != new DateTime?() && ((DateTime)x).Month == month);
            ParameterExpression paramExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, paramExpression, this.CachedTypedInfo, this.FieldDataType, this.GetDefaultValue());
            Expression expression3 = (Expression)Expression.Invoke(expression1, new Expression[1]
            {
        expression2
            });
            if (!typeof(TDataObject).IsValueType)
                expression3 = (Expression)Expression.Condition((Expression)Expression.Equal((Expression)paramExpression, (Expression)Expression.Constant((object)null, typeof(TDataObject))), (Expression)Expression.Constant((object)false), expression3);
            return Expression.Lambda<Func<TDataObject, bool>>(expression3, new ParameterExpression[1]
            {
        paramExpression
            });
        }





        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for next Month.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNextMonthExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = DateTime.Today.AddMonths(1);
            DateTime dateTime1 = new DateTime(dateTime.Year, dateTime.Month, 1);
            dateTime = dateTime.AddMonths(1);
            DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime1, dateTime2);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for next Quarter.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNextQuarterExpression<TDataObjectType>(string fieldName)
        {
            FilterContext<TDataObject>.QuarterTracking nextQuarterInfo = this.GetNextQuarterInfo(DateTime.Today);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, nextQuarterInfo.StartDate, nextQuarterInfo.EndDate);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for next week.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNextWeekExpression<TDataObjectType>(string fieldName)
        {
            DateTime today = DateTime.Today;
            DateTime dateTime = DateTime.Today;
            DateTime dateTime1 = today.AddDays((double)(-(int)dateTime.DayOfWeek + (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
            dateTime1 = new DateTime(dateTime1.Year, dateTime1.Month, dateTime1.Day);
            dateTime1 = dateTime1.AddDays(7);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime1, dateTime1.AddDays(7));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for next Year.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNextYearExpression<TDataObjectType>(string fieldName)
        {
            DateTime today = DateTime.Today;
            DateTime dateTime = new DateTime(today.Year + 1, 1, 1);
            DateTime today1 = DateTime.Today;
            DateTime dateTime1 = new DateTime(today1.Year + 2, 1, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime, dateTime1);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive inequality against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNotEqualsCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return this.CreateStringExpression<TDataObject>(fieldName, (string x) => !string.IsNullOrEmpty(x));
            }
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || !x.Equals(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for inequality.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateNotEqualsExpression<TDataObjectType>(string fieldName, object value)
        {
            Expression expression;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObjectType), "parameter");
            Expression expression1 = null;
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            expression1 = Expression.Constant(value, base.FieldDataType);
            if (base.FromDateColumn && value != null)
            {
                Expression expression3 = Expression.GreaterThanOrEqual(expression2, expression1);
                DateTime dateTime = ((DateTime)value).AddDays(1);
                expression1 = Expression.Constant(dateTime, base.FieldDataType);
                Expression expression4 = Expression.LessThan(expression2, expression1);
                expression = Expression.AndAlso(expression3, expression4);
                expression = Expression.Not(expression);
            }
            else if (expression2.Type.IsValueType)
            {
                expression = Expression.NotEqual(expression2, expression1);
            }
            else
            {
                Expression expression5 = Expression.Equal(expression2, Expression.Constant(null, expression2.Type));
                BinaryExpression binaryExpression = Expression.NotEqual(expression2, expression1);
                MethodInfo equalsMethod = FilterContext<TDataObject>.EqualsMethod;
                Expression[] expressionArray = new Expression[] { expression1 };
                expression = Expression.Condition(expression5, binaryExpression, Expression.Not(Expression.Call(expression2, equalsMethod, expressionArray)));
            }
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression6 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression = Expression.Condition(expression6, Expression.Constant(false), expression);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Returns an <see cref="T:System.Linq.Expressions.Expression" /> for a object operation.
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        protected internal Expression CreateObjectExpression(string fieldName, ComparisonOperator op, object value)
        {
            Expression dateExpression = null;
            IList lists = null;
            if (value is IList)
            {
                lists = value as IList;
            }
            switch (op)
            {
                case ComparisonOperator.Equals:
                    {
                        dateExpression = this.CreateEqualsExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.NotEquals:
                    {
                        dateExpression = this.CreateNotEqualsExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.GreaterThan:
                    {
                        dateExpression = this.CreateGreaterThanExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.GreaterThanOrEqual:
                    {
                        dateExpression = this.CreateGreaterThanOrEqualsExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.LessThan:
                    {
                        dateExpression = this.CreateLessThanExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.LessThanOrEqual:
                    {
                        dateExpression = this.CreateLessThanOrEqualsExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.StartsWith:
                case ComparisonOperator.DoesNotStartWith:
                case ComparisonOperator.EndsWith:
                case ComparisonOperator.DoesNotEndWith:
                case ComparisonOperator.Contains:
                case ComparisonOperator.DoesNotContain:
                    {
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeAfter:
                    {
                        dateExpression = this.CreateGreaterThanExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeBefore:
                    {
                        dateExpression = this.CreateLessThanExpression<TDataObject>(fieldName, value);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeToday:
                    {
                        dateExpression = this.CreateDateTimeTimeExcludedEqualsExpression<TDataObject>(fieldName, DateTime.Today);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeTomorrow:
                    {
                        DateTime today = DateTime.Today;
                        dateExpression = this.CreateDateTimeTimeExcludedEqualsExpression<TDataObject>(fieldName, today.AddDays(1));
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeYesterday:
                    {
                        DateTime dateTime = DateTime.Today;
                        dateExpression = this.CreateDateTimeTimeExcludedEqualsExpression<TDataObject>(fieldName, dateTime.AddDays(-1));
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeThisWeek:
                    {
                        dateExpression = this.CreateThisWeekExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeNextWeek:
                    {
                        dateExpression = this.CreateNextWeekExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeLastWeek:
                    {
                        dateExpression = this.CreateLastWeekExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeThisMonth:
                    {
                        dateExpression = this.CreateThisMonthExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeLastMonth:
                    {
                        dateExpression = this.CreateLastMonthExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeNextMonth:
                    {
                        dateExpression = this.CreateNextMonthExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeThisYear:
                    {
                        dateExpression = this.CreateThisYearExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeLastYear:
                    {
                        dateExpression = this.CreateLastYearExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeNextYear:
                    {
                        dateExpression = this.CreateNextYearExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeYearToDate:
                    {
                        dateExpression = this.CreateYearToDateExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeLastQuarter:
                    {
                        dateExpression = this.CreateLastQuarterExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeThisQuarter:
                    {
                        dateExpression = this.CreateThisQuarterExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeNextQuarter:
                    {
                        dateExpression = this.CreateNextQuarterExpression<TDataObject>(fieldName);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeJanuary:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 1);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeFebruary:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 2);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeMarch:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 3);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeApril:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 4);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeMay:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 5);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeJune:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 6);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeJuly:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 7);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeAugust:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 8);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeSeptember:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 9);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeOctober:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 10);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeNovember:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 11);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeDecember:
                    {
                        dateExpression = this.CreateMonthExpression<TDataObject>(fieldName, 12);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeQuarter1:
                    {
                        dateExpression = this.CreateQuarterYearIndependentExpression<TDataObject>(fieldName, 1);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeQuarter2:
                    {
                        dateExpression = this.CreateQuarterYearIndependentExpression<TDataObject>(fieldName, 2);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeQuarter3:
                    {
                        dateExpression = this.CreateQuarterYearIndependentExpression<TDataObject>(fieldName, 3);
                        return dateExpression;
                    }
                case ComparisonOperator.DateTimeQuarter4:
                    {
                        dateExpression = this.CreateQuarterYearIndependentExpression<TDataObject>(fieldName, 4);
                        return dateExpression;
                    }
                case ComparisonOperator.InOperand:
                    {
                        dateExpression = this.CreateInOperandExpression<TDataObject>(fieldName, lists);
                        return dateExpression;
                    }
                default:
                    {
                        return dateExpression;
                    }
            }
        }



        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that will evaluate an object's DateTime field for a particular quarter.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName"></param>
        /// <param name="quarter"></param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateQuarterYearIndependentExpression<TDataObjectType>(string fieldName, int quarter)
        {
            Expression expression1;
            if ((object)this.FieldDataType == (object)typeof(DateTime))
                // CHECKED HERE   [12/14/2016 A1]   
                expression1 = (Expression<Func<DateTime, bool>>)(x => this.IsDateValueInQuarter(x, quarter));
            else
                // CHECKED HERE  [12/14/2016 A1]   
                expression1 = (Expression<Func<DateTime, bool>>)(x => x != new DateTime?() && this.IsDateValueInQuarter(x, quarter));
            ParameterExpression paramExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression2 = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, paramExpression, this.CachedTypedInfo, this.FieldDataType, this.GetDefaultValue());
            Expression expression3 = (Expression)Expression.Invoke(expression1, new Expression[1]
            {
                expression2
            });
            if (!typeof(TDataObject).IsValueType)
                expression3 = (Expression)Expression.Condition((Expression)Expression.Equal((Expression)paramExpression, (Expression)Expression.Constant((object)null, typeof(TDataObject))), (Expression)Expression.Constant((object)false), expression3);
            return Expression.Lambda<Func<TDataObject, bool>>(expression3, new ParameterExpression[1]
            {
                paramExpression
            });
        }






        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive starts with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStartsWithCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.StartsWith(value, StringComparison.CurrentCultureIgnoreCase));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive starts with against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStartsWithExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && x.StartsWith(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for empty strings values.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringEmptyExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => string.IsNullOrEmpty(x));
        }

        /// <summary>
        /// Builds an expression for analyzing string
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="booleanStringExpression">A <![CDATA[Expression<Func<string, bool>>]]> which will analyze a string.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringExpression<TDataObjectType>(string fieldName, Expression<Func<string, bool>> booleanStringExpression)
        {
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TDataObject), "parameter");
            Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(fieldName, parameterExpression, base.CachedTypedInfo, base.FieldDataType, this.GetDefaultValue());
            Expression[] expressionArray = new Expression[] { expression };
            Expression expression1 = Expression.Invoke(booleanStringExpression, expressionArray);
            if (!typeof(TDataObject).IsValueType)
            {
                Expression expression2 = Expression.Equal(parameterExpression, Expression.Constant(null, typeof(TDataObject)));
                expression1 = Expression.Condition(expression2, Expression.Constant(false), expression1);
            }
            return Expression.Lambda<Func<TDataObject, bool>>(expression1, new ParameterExpression[] { parameterExpression });
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive greater than against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringGreaterThanCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && 1 == StringComparer.CurrentCultureIgnoreCase.Compare(x, value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive greater than against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringGreaterThanExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && 1 == x.CompareTo(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive greater than or equal against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringGreaterThanOrEqualCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && -1 != StringComparer.CurrentCultureIgnoreCase.Compare(x, value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive greater than or equal against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringGreaterThanOrEqualExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && -1 != x.CompareTo(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive inequality against a list of values.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The IList of values that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringInOperandCaseInsensitiveExpression<TDataObjectType>(string fieldName, IList value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x != null && value != null && value.Contains(x.ToLower()) || x == null && value != null && value.Contains(null));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive less than against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringLessThanCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || -1 == StringComparer.CurrentCultureIgnoreCase.Compare(x, value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive less than against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringLessThanExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || -1 == x.CompareTo(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case insensitive less than or equal against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringLessThanOrEqualCaseInsensitiveExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || 1 != StringComparer.CurrentCultureIgnoreCase.Compare(x, value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for case sensitive less than or equal against the inputted value.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringLessThanOrEqualExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => x == null || 1 != x.CompareTo(value));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for nonempty strings values.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <param name="value">The value that will be analyzed against.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateStringNotEmptyExpression<TDataObjectType>(string fieldName, string value)
        {
            return this.CreateStringExpression<TDataObject>(fieldName, (string x) => !string.IsNullOrEmpty(x));
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for this Month.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateThisMonthExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
            DateTime dateTime1 = DateTime.Today.AddMonths(1);
            DateTime dateTime2 = new DateTime(dateTime1.Year, dateTime1.Month, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime, dateTime2);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for this Quarter.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateThisQuarterExpression<TDataObjectType>(string fieldName)
        {
            FilterContext<TDataObject>.QuarterTracking currentQuarterInfo = this.GetCurrentQuarterInfo(DateTime.Today);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, currentQuarterInfo.StartDate, currentQuarterInfo.EndDate);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for this week.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateThisWeekExpression<TDataObjectType>(string fieldName)
        {
            DateTime today = DateTime.Today;
            DateTime dateTime = DateTime.Today;
            DateTime dateTime1 = today.AddDays((double)(-(int)dateTime.DayOfWeek + (int)CultureInfo.CurrentCulture.DateTimeFormat.FirstDayOfWeek));
            dateTime1 = new DateTime(dateTime1.Year, dateTime1.Month, dateTime1.Day);
            DateTime dateTime2 = dateTime1.AddDays(7);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime1, dateTime2);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for this Year.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateThisYearExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = new DateTime(DateTime.Today.Year, 1, 1);
            DateTime today = DateTime.Today;
            DateTime dateTime1 = new DateTime(today.Year + 1, 1, 1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime, dateTime1);
        }

        /// <summary>
        /// Creates an <see cref="T:System.Linq.Expressions.Expression" /> that evaluates for year to date.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="fieldName">The property that will be evaluated on.</param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which will evaluate for the condition.</returns>
        protected internal virtual Expression<Func<TDataObject, bool>> CreateYearToDateExpression<TDataObjectType>(string fieldName)
        {
            DateTime dateTime = new DateTime(DateTime.Today.Year, 1, 1);
            DateTime dateTime1 = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day);
            dateTime1 = dateTime1.AddDays(1);
            return this.CreateDateRangeExpression<TDataObject>(fieldName, dateTime, dateTime1);
        }

        /// <summary>
        /// Combines two <see cref="T:System.Linq.Expressions.Expression" /> objects with a XOR Expression.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which is the combination of the two inputs.</returns>
        protected internal Expression<Func<TDataObject, bool>> ExclusiveOrExpression<TDataObjectType>(Expression<Func<TDataObject, bool>> left, Expression<Func<TDataObject, bool>> right)
        {
            InvocationExpression invocationExpression = Expression.Invoke(right, left.Parameters.Cast<Expression>());
            Expression expression = Expression.ExclusiveOr(left.Body, invocationExpression);
            return Expression.Lambda<Func<TDataObject, bool>>(expression, left.Parameters);
        }

        internal FilterContext<TDataObject>.QuarterTracking GetCurrentQuarterInfo(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;
            FilterContext<TDataObject>.QuarterTracking quarterTracking = new FilterContext<TDataObject>.QuarterTracking();
            if (month <= 3)
            {
                quarterTracking.StartDate = new DateTime(year, 1, 1);
                quarterTracking.EndDate = new DateTime(year, 4, 1);
            }
            else if (month <= 6)
            {
                quarterTracking.StartDate = new DateTime(year, 4, 1);
                quarterTracking.EndDate = new DateTime(year, 7, 1);
            }
            else if (month > 9)
            {
                quarterTracking.StartDate = new DateTime(year, 10, 1);
                quarterTracking.EndDate = new DateTime(year + 1, 1, 1);
            }
            else
            {
                quarterTracking.StartDate = new DateTime(year, 7, 1);
                quarterTracking.EndDate = new DateTime(year, 10, 1);
            }
            return quarterTracking;
        }

        private object GetDefaultValue()
        {
            if (!base.FieldDataType.IsValueType)
            {
                return null;
            }
            return TypeActivator.CreateInstanceBoxedLazy(base.FieldDataType); //Activator.CreateInstance(base.FieldDataType);
        }

        internal FilterContext<TDataObject>.QuarterTracking GetNextQuarterInfo(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;
            FilterContext<TDataObject>.QuarterTracking quarterTracking = new FilterContext<TDataObject>.QuarterTracking();
            if (month <= 3)
            {
                quarterTracking.StartDate = new DateTime(year, 4, 1);
                quarterTracking.EndDate = new DateTime(year, 7, 1);
            }
            else if (month <= 6)
            {
                quarterTracking.StartDate = new DateTime(year, 7, 1);
                quarterTracking.EndDate = new DateTime(year, 10, 1);
            }
            else if (month > 9)
            {
                quarterTracking.StartDate = new DateTime(year + 1, 1, 1);
                quarterTracking.EndDate = new DateTime(year + 1, 4, 1);
            }
            else
            {
                quarterTracking.StartDate = new DateTime(year, 10, 1);
                quarterTracking.EndDate = new DateTime(year + 1, 1, 1);
            }
            return quarterTracking;
        }

        internal FilterContext<TDataObject>.QuarterTracking GetPreviousQuarterInfo(DateTime date)
        {
            int month = date.Month;
            int year = date.Year;
            FilterContext<TDataObject>.QuarterTracking quarterTracking = new FilterContext<TDataObject>.QuarterTracking();
            if (month <= 3)
            {
                quarterTracking.StartDate = new DateTime(year - 1, 10, 1);
                quarterTracking.EndDate = new DateTime(year, 1, 1);
            }
            else if (month <= 6)
            {
                quarterTracking.StartDate = new DateTime(year, 1, 1);
                quarterTracking.EndDate = new DateTime(year, 4, 1);
            }
            else if (month > 9)
            {
                quarterTracking.StartDate = new DateTime(year, 7, 1);
                quarterTracking.EndDate = new DateTime(year, 10, 1);
            }
            else
            {
                quarterTracking.StartDate = new DateTime(year, 4, 1);
                quarterTracking.EndDate = new DateTime(year, 7, 1);
            }
            return quarterTracking;
        }

        internal bool IsDateValueInQuarter(DateTime value, int quarter)
        {
            int num = 1;
            int num1 = 1;
            int num2 = 1;
            int num3 = 1;
            switch (quarter)
            {
                case 1:
                    {
                        num = 1;
                        num1 = 4;
                        int year = value.Year;
                        num2 = year;
                        num3 = year;
                        break;
                    }
                case 2:
                    {
                        num = 4;
                        num1 = 7;
                        int year1 = value.Year;
                        num2 = year1;
                        num3 = year1;
                        break;
                    }
                case 3:
                    {
                        num = 7;
                        num1 = 10;
                        int year2 = value.Year;
                        num2 = year2;
                        num3 = year2;
                        break;
                    }
                case 4:
                    {
                        num = 10;
                        num1 = 1;
                        int year3 = value.Year;
                        num2 = year3;
                        num3 = year3;
                        num3++;
                        break;
                    }
            }
            DateTime dateTime = new DateTime(num2, num, 1);
            DateTime dateTime1 = new DateTime(num3, num1, 1);
            if (value < dateTime)
            {
                return false;
            }
            return value < dateTime1;
        }

        internal override Expression Or(Expression left, Expression right)
        {
            return this.OrElseExpression<TDataObject>((Expression<Func<TDataObject, bool>>)left, (Expression<Func<TDataObject, bool>>)right);
        }

        /// <summary>
        /// Combines two <see cref="T:System.Linq.Expressions.Expression" /> objects with a OR Expression.
        /// </summary>
        /// <typeparam name="TDataObjectType">The object type that the <see cref="T:System.Linq.Expressions.Expression" /> will be applied over.</typeparam>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns>An <see cref="T:System.Linq.Expressions.Expression" /> which is the combination of the two inputs.</returns>
        protected internal Expression<Func<TDataObject, bool>> OrElseExpression<TDataObjectType>(Expression<Func<TDataObject, bool>> left, Expression<Func<TDataObject, bool>> right)
        {
            InvocationExpression invocationExpression = Expression.Invoke(right, left.Parameters.Cast<Expression>());
            Expression expression = Expression.OrElse(left.Body, invocationExpression);
            return Expression.Lambda<Func<TDataObject, bool>>(expression, left.Parameters);
        }

        private static List<string> ParseAndNormalizeValue(object value, bool isCaseSensitive)
        {
            List<string> strs = new List<string>();
            if (value is IList)
            {
                foreach (string str in value as IList)
                {
                    if (str == null)
                    {
                        strs.Add(null);
                    }
                    else
                    {
                        strs.Add((isCaseSensitive ? str : str.ToLower()));
                    }
                }
                if (strs.Contains(null))
                {
                    strs.Add(string.Empty);
                }
            }
            return strs;
        }

        internal class QuarterTracking
        {
            public DateTime EndDate
            {
                get;
                set;
            }

            public DateTime StartDate
            {
                get;
                set;
            }

            public QuarterTracking()
            {
            }
        }
    }
}



