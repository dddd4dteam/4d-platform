﻿
using System;

using DDDD.Core.Collections.Condition.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Interface that describes and object that can contain other IFilterCondition objects and will generate an expression for all of them.
    /// </summary>
    public interface IGroupFilterConditions : IExpressConditions
    {
        /// <summary>
        /// The <see cref="P: IGroupFilterConditions.LogicalOperator" /> which will be used to combine all the Conditions listed by this group.
        /// </summary>
         LogicalOperator LogicalOperator
        {
            get;
        }

        /// <summary>
        /// Event raised when an Item in the Collection is changed.
        /// </summary>
        event EventHandler<EventArgs> CollectionItemChanged;
    }
}
