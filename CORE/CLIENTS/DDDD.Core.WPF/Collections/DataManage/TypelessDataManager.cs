﻿using System.Collections;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Wraps an IEnumerable to get items while using IList or IQueryable to improve performance if available.
    /// This particular DataManager doesn't support Sorting, Filtering, or Paging.
    /// </summary>
    /// <remarks>
    /// Currently this DataManager is only used if the underlying data is an Anonymous type.
    /// </remarks>
    public class TypelessDataManager : DataManagerBase
    {
        public TypelessDataManager()
        {
        }

        /// <summary>
        /// Sets the <see cref="P: DataManagerBase.DataSource" /> while registering for change notification.
        /// </summary>
        /// <param name="source">The IEnumerable to set the DataSource to.</param>
        protected override void SetDataSource(IEnumerable source)
        {
            base.OriginalDataSource = source;
            base.SetDataSource(source);
        }
    }
}
