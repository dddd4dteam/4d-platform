﻿
using System;
using System.Collections;
using System.Linq;



using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An object that represents a particular field that the data has been merged by. 
    /// </summary>
    public class MergedColumnInfo
    {
        private  SummaryResultCollection _summaryResultCollection;

        /// <summary>
        /// Gets the collection of child rows that belong to this grouping, who all share the same value for the field as the Key
        /// </summary>
        public IList Children
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the Type of the underling data row.
        /// </summary>
        public Type DataType
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the unique key that this particualr field grouping represents.
        /// </summary>
        public object Key
        {
            get;
            internal set;
        }

        /// <summary>
        /// The object used to create this merging.
        /// </summary>
        public object MergingObject
        {
            get;
            set;
        }

        /// <summary>
        /// The MergedColumnInfo who this grouping falls under, null if its the root. 
        /// </summary>
        public MergedColumnInfo ParentMergedColumnInfo
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets a list of the summaries that should be applied to Children.
        /// </summary>
        public SummaryDefinitionCollection Summaries
        {
            get;
            internal set;
        }

        /// <summary>
        /// When this collection is acccessed it will lazily perform the summaries for this particular MergedColumnInfo based on the children.
        /// </summary>
        public  SummaryResultCollection SummaryResultCollection
        {
            get
            {
                if (this._summaryResultCollection == null)
                {
                    if (this.Summaries.Count <= 0)
                    {
                        return new  SummaryResultCollection();
                    }
                    this._summaryResultCollection = new  SummaryResultCollection();
                    IQueryable queryables = this.Children.AsQueryable();
                    foreach (SummaryDefinition summary in this.Summaries)
                    {
                        ISupportLinqSummaries summaryCalculator = summary.SummaryOperand.SummaryCalculator as ISupportLinqSummaries;
                        if (summaryCalculator == null)
                        {
                            SynchronousSummaryCalculator synchronousSummaryCalculator = summary.SummaryOperand.SummaryCalculator as SynchronousSummaryCalculator;
                            if (synchronousSummaryCalculator == null)
                            {
                                continue;
                            }
                            this._summaryResultCollection.Add(new SummaryResult(summary, synchronousSummaryCalculator.Summarize(queryables, summary.ColumnKey)));
                        }
                        else
                        {
                            SummaryContext summaryContext = SummaryContext.CreateGenericSummary(this.TypedInfo, summary.ColumnKey, summaryCalculator.SummaryType);
                            summaryCalculator.SummaryContext = summaryContext;
                            this._summaryResultCollection.Add(new SummaryResult(summary, summaryContext.Execute(queryables)));
                        }
                    }
                }
                return this._summaryResultCollection;
            }
        }

        /// <summary>
        /// Gets/Sets the TypedInfo for the object.
        /// </summary>
        public CachedTypedInfo TypedInfo
        {
            get;
            set;
        }

        public MergedColumnInfo()
        {
        }
    }
}
