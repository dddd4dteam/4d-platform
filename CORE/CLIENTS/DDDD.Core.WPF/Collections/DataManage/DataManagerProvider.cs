﻿using System;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// An abstract class, that provides information to create a custom DataManager.
    /// </summary>
    public abstract class DataManagerProvider
	{
		protected DataManagerProvider()
		{
		}

		/// <summary>
		/// Returns the type of the DataManager that it represents, so that it can be created indirectly.
		/// </summary>
		/// <returns></returns>
		public abstract Type ResolveDataManagerType();
	}
}
