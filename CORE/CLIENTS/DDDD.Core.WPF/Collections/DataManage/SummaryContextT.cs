﻿
using System;
using System.Linq;
using System.Linq.Expressions;
using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Summary.IG;

namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// A generic class representing a summary on an object.
    /// </summary>
    /// <typeparam name="TObjectType">The type of the object which will be summed on.</typeparam>
    /// <typeparam name="TColumnType">The type of the field that will be summed on.</typeparam>
    public class SummaryContext<TObjectType, TColumnType> : SummaryContext
    {
        /// <summary>
        /// The CachedTypedInfo for the opeartion
        /// </summary>
        protected  CachedTypedInfo CachedTypedInfo
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: SummaryContext" />
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="linqSummary"></param>
        /// <param name="cti"></param>
        public SummaryContext(string propertyName, LinqSummaryOperator linqSummary,  CachedTypedInfo cti)
        {
            base.FieldName = propertyName;
            base.LinqSummary = linqSummary;
            this.CachedTypedInfo = cti;
        }

        /// <summary>
        /// Executes a LINQ based Average summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected override object Average(IQueryable query)
        {
            object obj;
            Type type = typeof(TObjectType);
            Type type1 = typeof(TColumnType);
            ParameterExpression parameterExpression = Expression.Parameter(type, "parameter");
            TColumnType tColumnType = default(TColumnType);
            Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(base.FieldName, parameterExpression, this.CachedTypedInfo, typeof(TColumnType), tColumnType);
            try
            {
                if (type1 == typeof(int))
                {
                    IQueryable<TObjectType> tObjectTypes = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray));
                }
                else if (type1 == typeof(int?))
                {
                    IQueryable<TObjectType> tObjectTypes1 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray1 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes1.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray1));
                }
                else if (type1 == typeof(long))
                {
                    IQueryable<TObjectType> tObjectTypes2 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray2 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes2.Average<TObjectType>(Expression.Lambda<Func<TObjectType, long>>(expression, parameterExpressionArray2));
                }
                else if (type1 == typeof(long?))
                {
                    IQueryable<TObjectType> tObjectTypes3 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray3 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes3.Average<TObjectType>(Expression.Lambda<Func<TObjectType, long?>>(expression, parameterExpressionArray3));
                }
                else if (type1 == typeof(double))
                {
                    IQueryable<TObjectType> tObjectTypes4 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray4 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes4.Average<TObjectType>(Expression.Lambda<Func<TObjectType, double>>(expression, parameterExpressionArray4));
                }
                else if (type1 == typeof(double?))
                {
                    IQueryable<TObjectType> tObjectTypes5 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray5 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes5.Average<TObjectType>(Expression.Lambda<Func<TObjectType, double?>>(expression, parameterExpressionArray5));
                }
                else if (type1 == typeof(decimal))
                {
                    IQueryable<TObjectType> tObjectTypes6 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray6 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes6.Average<TObjectType>(Expression.Lambda<Func<TObjectType, decimal>>(expression, parameterExpressionArray6));
                }
                else if (type1 == typeof(decimal?))
                {
                    IQueryable<TObjectType> tObjectTypes7 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray7 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes7.Average<TObjectType>(Expression.Lambda<Func<TObjectType, decimal?>>(expression, parameterExpressionArray7));
                }
                else if (type1 == typeof(float))
                {
                    IQueryable<TObjectType> tObjectTypes8 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray8 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes8.Average<TObjectType>(Expression.Lambda<Func<TObjectType, float>>(expression, parameterExpressionArray8));
                }
                else if (type1 == typeof(float?))
                {
                    IQueryable<TObjectType> tObjectTypes9 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray9 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes9.Average<TObjectType>(Expression.Lambda<Func<TObjectType, float?>>(expression, parameterExpressionArray9));
                }
                else if (type1 == typeof(byte))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes10 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray10 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes10.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray10));
                }
                else if (type1 == typeof(byte?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes11 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray11 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes11.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray11));
                }
                else if (type1 == typeof(sbyte))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes12 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray12 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes12.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray12));
                }
                else if (type1 == typeof(sbyte?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes13 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray13 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes13.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray13));
                }
                else if (type1 == typeof(uint))
                {
                    expression = Expression.Convert(expression, typeof(long));
                    IQueryable<TObjectType> tObjectTypes14 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray14 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes14.Average<TObjectType>(Expression.Lambda<Func<TObjectType, long>>(expression, parameterExpressionArray14));
                }
                else if (type1 == typeof(uint?))
                {
                    expression = Expression.Convert(expression, typeof(long?));
                    IQueryable<TObjectType> tObjectTypes15 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray15 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes15.Average<TObjectType>(Expression.Lambda<Func<TObjectType, long?>>(expression, parameterExpressionArray15));
                }
                else if (type1 == typeof(short))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes16 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray16 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes16.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray16));
                }
                else if (type1 == typeof(short?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes17 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray17 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes17.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray17));
                }
                else if (type1 == typeof(ushort))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes18 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray18 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes18.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray18));
                }
                else if (type1 == typeof(ushort?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes19 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray19 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes19.Average<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray19));
                }
                else if (type1 == typeof(ulong))
                {
                    expression = Expression.Convert(expression, typeof(decimal));
                    IQueryable<TObjectType> tObjectTypes20 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray20 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes20.Average<TObjectType>(Expression.Lambda<Func<TObjectType, decimal>>(expression, parameterExpressionArray20));
                }
                else if (type1 != typeof(ulong?))
                {
                    return null;
                }
                else
                {
                    expression = Expression.Convert(expression, typeof(decimal?));
                    IQueryable<TObjectType> tObjectTypes21 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray21 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes21.Average<TObjectType>(Expression.Lambda<Func<TObjectType, decimal?>>(expression, parameterExpressionArray21));
                }
            }
            catch (InvalidOperationException invalidOperationException)
            {
                obj = null;
            }
            return obj;
        }

        /// <summary>
        /// Executes a LINQ based Count summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected override int Count(IQueryable query)
        {
            return query.Cast<TObjectType>().Count<TObjectType>();
        }

        /// <summary>
        /// Executes a LINQ based Maximum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected override object Maximum(IQueryable query)
        {
            ParameterExpression paramExpression  = Expression.Parameter(typeof(TObjectType),"p1");
            Expression<Func<TObjectType, TColumnType>> selector = Expression.Lambda<Func<TObjectType, TColumnType>>
                (DataManagerBase.BuildPropertyExpressionFromPropertyName(this.FieldName, paramExpression, this.CachedTypedInfo, typeof(TColumnType), (object)default(TColumnType)), new ParameterExpression[1]
            {
        paramExpression
            });
            try
            {
                return (object)Queryable.Cast<TObjectType>(query).Max<TObjectType, TColumnType>(selector);
            }
            catch (InvalidOperationException ex)
            {
                return (object)null;
            } 
        }

        /// <summary>
        /// Executes a LINQ based Minimum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected override object Minimum(IQueryable query)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TObjectType), "p1");
            Expression<Func<TObjectType, TColumnType>> selector = Expression.Lambda<Func<TObjectType, TColumnType>>(DataManagerBase.BuildPropertyExpressionFromPropertyName(this.FieldName, paramExpression, this.CachedTypedInfo, typeof(TColumnType), (object)default(TColumnType)), new ParameterExpression[1]
            {
                paramExpression
            });
            try
            {
                return (object)Queryable.Cast<TObjectType>(query).Min<TObjectType, TColumnType>(selector);
            }
            catch (InvalidOperationException ex)
            {
                return (object)null;
            }
        }

        /// <summary>
        /// Executes a LINQ based Sum summary.
        /// </summary>
        /// <param name="query">The IQueryable to execute the summary against.</param>
        /// <returns>The value of the summation.</returns>
        protected override object Sum(IQueryable query)
        {
            object obj;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(TObjectType), "parameter");
            TColumnType tColumnType = default(TColumnType);
            Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(base.FieldName, parameterExpression, this.CachedTypedInfo, typeof(TColumnType), tColumnType);
            Type type = typeof(TColumnType);
            try
            {
                if (type == typeof(int))
                {
                    IQueryable<TObjectType> tObjectTypes = query.Cast<TObjectType>();
                    UnaryExpression unaryExpression = Expression.Convert(expression, typeof(double));
                    ParameterExpression[] parameterExpressionArray = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double>>(unaryExpression, parameterExpressionArray));
                }
                else if (type == typeof(int?))
                {
                    IQueryable<TObjectType> tObjectTypes1 = query.Cast<TObjectType>();
                    UnaryExpression unaryExpression1 = Expression.Convert(expression, typeof(double?));
                    ParameterExpression[] parameterExpressionArray1 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes1.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double?>>(unaryExpression1, parameterExpressionArray1));
                }
                else if (type == typeof(long))
                {
                    IQueryable<TObjectType> tObjectTypes2 = query.Cast<TObjectType>();
                    UnaryExpression unaryExpression2 = Expression.Convert(expression, typeof(double));
                    ParameterExpression[] parameterExpressionArray2 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes2.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double>>(unaryExpression2, parameterExpressionArray2));
                }
                else if (type == typeof(long?))
                {
                    IQueryable<TObjectType> tObjectTypes3 = query.Cast<TObjectType>();
                    UnaryExpression unaryExpression3 = Expression.Convert(expression, typeof(double?));
                    ParameterExpression[] parameterExpressionArray3 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes3.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double?>>(unaryExpression3, parameterExpressionArray3));
                }
                else if (type == typeof(double))
                {
                    IQueryable<TObjectType> tObjectTypes4 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray4 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes4.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double>>(expression, parameterExpressionArray4));
                }
                else if (type == typeof(double?))
                {
                    IQueryable<TObjectType> tObjectTypes5 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray5 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes5.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, double?>>(expression, parameterExpressionArray5));
                }
                else if (type == typeof(decimal))
                {
                    IQueryable<TObjectType> tObjectTypes6 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray6 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes6.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, decimal>>(expression, parameterExpressionArray6));
                }
                else if (type == typeof(decimal?))
                {
                    IQueryable<TObjectType> tObjectTypes7 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray7 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes7.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, decimal?>>(expression, parameterExpressionArray7));
                }
                else if (type == typeof(float))
                {
                    IQueryable<TObjectType> tObjectTypes8 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray8 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes8.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, float>>(expression, parameterExpressionArray8));
                }
                else if (type == typeof(float?))
                {
                    IQueryable<TObjectType> tObjectTypes9 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray9 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes9.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, float?>>(expression, parameterExpressionArray9));
                }
                else if (type == typeof(byte))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes10 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray10 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes10.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray10));
                }
                else if (type == typeof(byte?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes11 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray11 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes11.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray11));
                }
                else if (type == typeof(sbyte))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes12 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray12 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes12.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray12));
                }
                else if (type == typeof(sbyte?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes13 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray13 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes13.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray13));
                }
                else if (type == typeof(uint))
                {
                    expression = Expression.Convert(expression, typeof(long));
                    IQueryable<TObjectType> tObjectTypes14 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray14 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes14.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, long>>(expression, parameterExpressionArray14));
                }
                else if (type == typeof(uint?))
                {
                    expression = Expression.Convert(expression, typeof(long?));
                    IQueryable<TObjectType> tObjectTypes15 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray15 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes15.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, long?>>(expression, parameterExpressionArray15));
                }
                else if (type == typeof(short))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes16 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray16 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes16.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray16));
                }
                else if (type == typeof(short?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes17 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray17 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes17.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray17));
                }
                else if (type == typeof(ushort))
                {
                    expression = Expression.Convert(expression, typeof(int));
                    IQueryable<TObjectType> tObjectTypes18 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray18 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes18.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int>>(expression, parameterExpressionArray18));
                }
                else if (type == typeof(ushort?))
                {
                    expression = Expression.Convert(expression, typeof(int?));
                    IQueryable<TObjectType> tObjectTypes19 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray19 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes19.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, int?>>(expression, parameterExpressionArray19));
                }
                else if (type == typeof(ulong))
                {
                    expression = Expression.Convert(expression, typeof(decimal));
                    IQueryable<TObjectType> tObjectTypes20 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray20 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes20.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, decimal>>(expression, parameterExpressionArray20));
                }
                else if (type != typeof(ulong?))
                {
                    return null;
                }
                else
                {
                    expression = Expression.Convert(expression, typeof(decimal?));
                    IQueryable<TObjectType> tObjectTypes21 = query.Cast<TObjectType>();
                    ParameterExpression[] parameterExpressionArray21 = new ParameterExpression[] { parameterExpression };
                    obj = tObjectTypes21.Sum<TObjectType>(Expression.Lambda<Func<TObjectType, decimal?>>(expression, parameterExpressionArray21));
                }
            }
            catch (InvalidOperationException invalidOperationException)
            {
                obj = null;
            }
            return obj;
        }
    }
}
