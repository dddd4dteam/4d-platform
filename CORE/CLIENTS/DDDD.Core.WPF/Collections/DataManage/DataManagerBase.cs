﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;


using DDDD.Core.Reflection.IG;

using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.IG;
using DDDD.Core.Events.IG;
using DDDD.Core.Collections.Summary.IG;
using DDDD.Core.Collections.Format.IG;
using DDDD.Core.Collections.Compare.IG;
using DDDD.Core.Reflection;


namespace DDDD.Core.Collections.DataManage.IG
{
    /// <summary>
    /// Wraps an IEnumerable to get items while using IList or IQueryable to improve performance if available.
    /// </summary>
    public abstract class DataManagerBase : INotifyCollectionChanged
    {
        private bool _enablePaging;

        private IEnumerable _dataSource;

        private IEnumerable _originalDataSource;

        private ObservableCollection<SortContext> _currentSort;

        private RecordFilterCollection _currentFilter;

        private int _pageSize = -1;

        private int _currentPage = 1;

        private int _recordCount = -1;

        private int _totalRecordCount = -1;

        private int _pageCount;

        private IList _sortedFilterDataSource;

        private SummaryExecution _summaryExecution;

        private Type _cachedType;

        private Type _cachedCollectionType;

        private IPagedCollectionView _pagedCollection;

        private ICollectionView _collectionView;

        private IList _list;

        private IEditableCollectionView _editableCollectionView;

        private IFilteredCollectionView _filteredCollectionView;

        private bool _suspendInvalidateDataSource;

        private SummaryDefinitionCollection _currentSummaries;

        private FormattingRuleCollection<IRule> _conditionalFormatRules;

        private bool _allowCollectionViewOverrides = true;

        private List<MergedDataContext> _mergeDataContexts;

        private bool _ignoreCurrentChanging;

        private WeakCollectionChangedHandler<DataManagerBase> _weakDataSourceCollectionChanged;

        private WeakEventHandler<DataManagerBase, ICollectionView, EventArgs> _weakCollectionViewCurrentChanged;

        private bool _shouldUnwrapCollectionView;

        private bool _preferEnumerableItemType;

        private CachedTypedInfo _cachedTypedInfo;

        /// <summary>
        /// Controls whether the DataSource will be updated when calling ApplyClientDataManipulations
        /// </summary>
        private bool _updateDataSource = true;

        /// <summary>
        /// Gets/Sets whether this <see cref="T:DataManagerBase" /> is allowed to set properties for sorting and grouping if DataSource is an ICollectionView
        /// </summary>
        public bool AllowCollectionViewOverrides
        {
            get
            {
                return this._allowCollectionViewOverrides;
            }
            set
            {
                this._allowCollectionViewOverrides = value;
            }
        }

        /// <summary>
        /// A cached version of CollectionType.
        /// </summary>
        public Type CachedCollectionType
        {
            get
            {
                if (this._cachedCollectionType == null)
                {
                    this._cachedCollectionType = this.CollectionType;
                }
                return this._cachedCollectionType;
            }
            set
            {
                this._cachedCollectionType = value;
            }
        }

        /// <summary>
        /// Gets/Sets a cached version of the <see cref="P:DataManagerBase.DataType" />.
        /// </summary>
        public Type CachedType
        {
            get
            {
                if (this._cachedType == null)
                {
                    this._cachedType = this.DataType;
                }
                return this._cachedType;
            }
            set
            {
                this._cachedType = value;
            }
        }

        /// <summary>
        /// Gets/Sets a cached version of the <see cref="P:DataManagerBase.DataType" />.
        /// </summary>
        public CachedTypedInfo CachedTypedInfo
        {
            get
            {
                if (this._cachedTypedInfo != null)
                {
                    return this._cachedTypedInfo;
                }
                CachedTypedInfo cachedTypedInfo = new CachedTypedInfo()
                {
                    CachedType = this.CachedType
                };
                if (typeof(ICustomTypeProvider).IsAssignableFrom(this.CachedType) && this.DataSource != null)
                {
                    IEnumerator enumerator = this.DataSource.GetEnumerator();
                    if (enumerator.MoveNext())
                    {
                        object current = enumerator.Current;
                        if (current != null && DataManagerBase.GetCustomOrCLRType(current) != typeof(object))
                        {
                            Type customOrCLRType = DataManagerBase.GetCustomOrCLRType(current);
                            List<PropertyInfo> list = customOrCLRType.GetProperties().Except<PropertyInfo>(this.CachedType.GetProperties()).ToList<PropertyInfo>();
                            if (list.Count > 0)
                            {
                                CustomTypePropertyCollection customTypePropertyCollection = new CustomTypePropertyCollection();
                                foreach (PropertyInfo propertyInfo in list)
                                {
                                    customTypePropertyCollection.Add(new CustomTypeProperty(propertyInfo));
                                }
                                cachedTypedInfo.CustomTypeProperties = customTypePropertyCollection;
                            }
                        }
                    }
                }
                return cachedTypedInfo;
            }
            internal set
            {
                this._cachedTypedInfo = value;
            }
        }

        /// <summary>
        /// The <see cref="T:System.Type" /> that the collection is designed to hold.
        /// </summary>
        protected Type CollectionType
        {
            get
            {
                Type type = null;
                if (this.DataSource != null)
                {
                    type = DataManagerBase.ResolveCollectionType(this.OriginalDataSource);
                }
                return type;
            }
        }

        /// <summary>
        /// Gets / sets the collection of conditional formatting rules which will be requesting data.
        /// </summary>
        public FormattingRuleCollection<IRule> ConditionalFormattingRules
        {
            get
            {
                return this._conditionalFormatRules;
            }
            set
            {
                this._conditionalFormatRules = value;
                this.ClearCachedDataSource(false);
            }
        }

        /// <summary>
        /// Gets / sets the index of the page of data which should be retrieved from the manager.
        /// </summary>
        public int CurrentPage
        {
            get
            {
                if (this._currentPage > this.PageCount)
                {
                    return 1;
                }
                return this._currentPage;
            }
            set
            {
                if (value != this._currentPage)
                {
                    this._currentPage = value;
                    this._sortedFilterDataSource = null;
                    this.IsSortedFilteredDataSourceCalculated = false;
                    this.InvalidateSortedFilterdDataSource();
                }
            }
        }

        /// <summary>
        /// Gets or sets the IEnumerable that this <see cref="T:DataManagerBase" /> manages.
        /// </summary>
        public IEnumerable DataSource
        {
            get
            {
                return this.GetDataSource();
            }
            set
            {
                this.SetDataSource(value);
            }
        }

        /// <summary>
        /// Returns type of data that this IEnumerable represents. 
        /// </summary>
        protected Type DataType
        {
            get
            {
                Type type = null;
                if (this.DataSource != null)
                {
                    type = DataManagerBase.ResolveItemType(this.DataSource, this._shouldUnwrapCollectionView, this._preferEnumerableItemType);
                }
                return type;
            }
        }

        /// <summary>
        /// Prevents operations from happening when the control is in an inconsistant state.
        /// </summary>
        protected internal bool Defer
        {
            get;
            set;
        }

        /// <summary>
        /// Gets / sets whether paging should be used by the manager.
        /// </summary>
        public bool EnablePaging
        {
            get
            {
                return this._enablePaging;
            }
            set
            {
                if (this._enablePaging != value)
                {
                    this._enablePaging = value;
                    this.ClearCachedDataSource(false);
                }
            }
        }

        /// <summary>
        /// Gets / sets the <see cref="T:RecordFilterCollection" /> which will be applied to the records during the databinding.
        /// </summary>
        public RecordFilterCollection Filters
        {
            get
            {
                return this._currentFilter;
            }
            set
            {
                this.DetachFilterEvents();
                this._currentFilter = value;
                if ((this.ICollectionViewData == null ? true : !this.ICollectionViewData.CanGroup) || this.GroupByObject == null)
                {
                    this.ClearCachedDataSource(false);
                }
                if (this._currentFilter != null)
                {
                    this._currentFilter.CollectionChanged += new NotifyCollectionChangedEventHandler(this.CurrentFilter_CollectionChanged);
                    this._currentFilter.CollectionItemChanged += new EventHandler<EventArgs>(this.CurrentFilter_CollectionItemChanged);
                }
            }
        }

        /// <summary>
        /// Gets/Sets the object in which the data that this <see cref="T:DataManagerBase" /> represents, should be grouped by.
        /// </summary>
        public GroupByContext GroupByObject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/Sets the sort direction that should be applied to the field that the underlying data has been grouped by.
        /// </summary>
        public bool GroupBySortAscending
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/Sets the CurrentSort that will be applied when the data is Grouped by a particular field.
        /// </summary>
        public SortContext GroupBySortContext
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the underlying data source as an <see cref="T:System.ComponentModel.ICollectionView" />. If the datasource isn't an ICollectionView, null is returned.
        /// </summary>
        protected ICollectionView ICollectionViewData
        {
            get
            {
                return this._collectionView;
            }
        }

        /// <summary>
        /// Gets the underlying data source as an <see cref="T:System.ComponentModel.IEditableCollectionView" />. If the datasource isn't an IEditableCollectionView, null is returned.
        /// </summary>
        protected IEditableCollectionView IEditableCollectionViewData
        {
            get
            {
                return this._editableCollectionView;
            }
        }

        /// <summary>
        /// Gets the underlying data source as an <see cref="T:IFilteredCollectionView" />. If the datasource isn't an IFilteredCollectionView, null is returned.
        /// </summary>
        protected IFilteredCollectionView IFilteredCollectionViewData
        {
            get
            {
                return this._filteredCollectionView;
            }
        }

        /// <summary>
        /// Gets the underlying data source as an <see cref="T:System.Collections.IList" />. If the datasource isn't an IList, null is returned.
        /// </summary>
        protected IList IListData
        {
            get
            {
                return this._list;
            }
        }

        /// <summary>
        /// Gets the underlying data source as an <see cref="T:System.ComponentModel.IPagedCollectionView" />. If the datasource isn't an IPagedCollectionView, null is returned.
        /// </summary>
        internal IPagedCollectionView IPagedCollectionViewData
        {
            get
            {
                return this._pagedCollection;
            }
        }

        internal bool IsSorted
        {
            get;
            set;
        }

        /// <summary>
        /// Gets/ sets whether the the SortedFilteredDataSource needs to be recalculated.
        /// </summary>
        protected bool IsSortedFilteredDataSourceCalculated
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the list of <see cref="T:MergedDataContext" /> objects, that the manager should be displaying the data as. 
        /// </summary>
        public List<MergedDataContext> MergeDataContexts
        {
            get
            {
                if (this._mergeDataContexts == null)
                {
                    this._mergeDataContexts = new List<MergedDataContext>();
                }
                return this._mergeDataContexts;
            }
        }

        /// <summary>
        /// Gets or sets the IEnumerable that this <see cref="T:DataManagerBase" /> manages without converting types.
        /// </summary>
        public IEnumerable OriginalDataSource
        {
            get
            {
                return this._originalDataSource;
            }
            set
            {
                this._originalDataSource = value;
                this._pagedCollection = value as IPagedCollectionView;
                this._collectionView = value as ICollectionView;
                this._list = value as IList;
                this._editableCollectionView = value as IEditableCollectionView;
                this._filteredCollectionView = value as IFilteredCollectionView;
            }
        }

        /// <summary>
        /// Gets the total number of pages available in the data source based on page size.  If 
        /// <see cref="P:DataManagerBase.EnablePaging" /> is false, this will report 1. 
        /// </summary>
        public int PageCount
        {
            get
            {
                if (this.IPagedCollectionViewData != null && this.IPagedCollectionViewData.TotalItemCount != -1)
                {
                    this._pageCount = (int)Math.Ceiling((double)this.IPagedCollectionViewData.TotalItemCount / (double)this.IPagedCollectionViewData.PageSize);
                }
                return this._pageCount;
            }
            set
            {
                this._pageCount = value;
            }
        }

        /// <summary>
        /// Gets / sets how many records constitute a page of data
        /// </summary>
        public int PageSize
        {
            get
            {
                return this._pageSize;
            }
            set
            {
                if (this._pageSize != value)
                {
                    this._pageSize = value;
                    if (this.EnablePaging)
                    {
                        this.ClearCachedDataSource(false);
                    }
                    if (this.IPagedCollectionViewData != null && this.IPagedCollectionViewData.PageSize != this.PageSize)
                    {
                        this.IPagedCollectionViewData.PageSize = this.PageSize;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the number of records that can be currently displayed.
        /// </summary>
        /// <remarks>
        /// This takes into account filtering, paging and grouping. 
        /// </remarks>
        public virtual int RecordCount
        {
            get
            {
                int num = 0;
                if (this._dataSource != null)
                {
                    if (this.IPagedCollectionViewData != null && this.IPagedCollectionViewData.TotalItemCount != -1)
                    {
                        if (this._recordCount == -1)
                        {
                            this._recordCount = this.ResolveCount();
                        }
                    }
                    else if (this._recordCount == -1)
                    {
                        this._recordCount = this.TotalRecordCount;
                    }
                    num = this._recordCount;
                }
                return num;
            }
            protected set
            {
                this._recordCount = value;
            }
        }

        /// <summary>
        /// Gets an observable collection of sorts, from primary sort to final sort.
        /// Sorts after the first are only applied if all the previous sorts were equal.
        /// </summary>
        public Collection<SortContext> Sort
        {
            get
            {
                if (this._currentSort == null)
                {
                    this._currentSort = new ObservableCollection<SortContext>();
                }
                return this._currentSort;
            }
        }

        /// <summary>
        /// Gets/sets a cached list of sorted items.
        /// </summary>		
        public IList SortedFilteredDataSource
        {
            get
            {
                this.InvalidateSortedFilterdDataSource();
                return this._sortedFilterDataSource;
            }
        }

        internal virtual IList SortedFilteredList
        {
            get
            {
                return this.SortedFilteredDataSource;
            }
        }

        /// <summary>
        /// Gets / sets the <see cref="T:SummaryDefinitionCollection" /> which will be applied to the records during the databinding.
        /// </summary>
        public SummaryDefinitionCollection Summaries
        {
            get
            {
                return this._currentSummaries;
            }
            protected internal set
            {
                this.SummariesDirty = true;
                this._currentSummaries = value;
                this.ClearCachedDataSource(false);
            }
        }

        internal bool SummariesDirty
        {
            get;
            set;
        }

        /// <summary>
        /// Gets / sets the <see cref="P:DataManagerBase.SummaryExecution" /> which will determine where the summaries should be calculated by default.
        /// </summary>
        public SummaryExecution SummaryExecution
        {
            get
            {
                return this._summaryExecution;
            }
            set
            {
                if (this._summaryExecution != value)
                {
                    this._summaryExecution = value;
                    this.ClearCachedDataSource(true);
                }
            }
        }

        /// <summary>
        /// The collection of <see cref="T:SummaryResult" /> objects that will be populated by the <see cref="T:DataManagerBase" />.
        /// </summary>
        public SummaryResultCollection SummaryResultCollection
        {
            get;
            protected internal set;
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="P:DataManagerBase.DataSource" /> raises change notification events.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the DataSource supports change notification; otherwise, <c>false</c>.
        /// </value>
        internal bool SupportsChangeNotification
        {
            get
            {
                return this.DataSource is INotifyCollectionChanged;
            }
        }

        /// <summary>
        /// Gets/ sets whether data manipulations such as Sorting are supported on this particular data manager.
        /// </summary>
        public bool SupportsDataManipulations
        {
            get;
            set;
        }

        /// <summary>
        /// Gets whether editing is supported by the collection.
        /// </summary>
        public bool SupportsEditing
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets / sets if the DataManager should be prevented from invaliating it's cached data stores, so that 
        /// multiple actions can be built up and executed at one time.
        /// </summary>
        public bool SuspendInvalidateDataSource
        {
            get
            {
                return this._suspendInvalidateDataSource;
            }
            set
            {
                if (this._suspendInvalidateDataSource != value)
                {
                    this._suspendInvalidateDataSource = value;
                    if (!value)
                    {
                        this.InvalidateSortedFilterdDataSource();
                    }
                }
            }
        }

        /// <summary>
        /// Gets the total number of records available from the datasource.
        /// </summary>
        /// <remarks>
        /// This excludes filtering, paging and grouping. 
        /// </remarks>
        public virtual int TotalRecordCount
        {
            get
            {
                if (this._totalRecordCount == -1)
                {
                    int totalItemCount = 0;
                    if (this.IPagedCollectionViewData == null || this.IPagedCollectionViewData.TotalItemCount == -1)
                    {
                        totalItemCount = (this.IListData == null ? this.ResolveCount() : this.IListData.Count);
                    }
                    else
                    {
                        totalItemCount = this.IPagedCollectionViewData.TotalItemCount;
                    }
                    this._totalRecordCount = totalItemCount;
                }
                return this._totalRecordCount;
            }
            protected set
            {
                this._totalRecordCount = value;
            }
        }

        internal bool UpdateDataSource
        {
            get
            {
                return this._updateDataSource;
            }
            set
            {
                this._updateDataSource = value;
            }
        }

        protected DataManagerBase()
        {
        }

        private static Expression AddNullRefCheckToExpression(Expression newExpression, Expression expression)
        {
            if (expression != null && !expression.Type.IsValueType)
            {
                Expression expression1 = Expression.Equal(expression, Expression.Constant(null, expression.Type));
                Expression expression2 = DataManagerBase.ExpressionTypeGenerationBase.CreateGenericExpressionConstant(newExpression.Type);
                newExpression = Expression.Condition(expression1, expression2, newExpression);
            }
            return newExpression;
        }

        /// <summary>
        /// Adds inputted object to the datasource
        /// </summary>
        /// <param name="value"></param>
        public void AddRecord(object value)
        {
            if (this.IEditableCollectionViewData == null || !this.IEditableCollectionViewData.CanAddNew)
            {
                if (this.IListData == null)
                {
                    throw new DataSourceDoesNotSupportIListException();
                }
                this.IListData.Add(value);
                this.ClearCachedDataSource(true);
            }
            else
            {
                this._ignoreCurrentChanging = true;
                if (!DataManagerBase.CopyValues(this.IEditableCollectionViewData.AddNew(), value))
                {
                    this.IEditableCollectionViewData.CancelNew();
                }
                else
                {
                    this.IEditableCollectionViewData.CommitNew();
                }
                this._ignoreCurrentChanging = false;
            }
            this.OnDataUpdated();
        }

        /// <summary>
        /// Builds a <see cref="T:System.Linq.Expressions.Expression" /> for complex property names such as Address.Street1 or Items[FirstName]
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="paramExpression"></param>
        /// <returns></returns>
        public static Expression BuildPropertyExpressionFromPropertyName(string propertyName, ParameterExpression paramExpression)
        {
            return DataManagerBase.BuildPropertyExpressionFromPropertyName(propertyName, paramExpression, null, null, null);
        }

        /// <summary>
        /// Builds a <see cref="T:System.Linq.Expressions.Expression" /> for complex property names such as Address.Street1 or Items[FirstName]
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="paramExpression"></param>
        /// <param name="cachedTypedInfo" />
        /// <param name="propertyType" />
        /// <param name="defaultValue" />
        /// <param name="isDataTypePrimitive">Set this to true if data object is primitive and there are no property names to extract the data from.
        /// The propertyName is expected to be null or empty to return a valid expression.</param>
        /// <returns></returns>
        public static Expression BuildPropertyExpressionFromPropertyName(string propertyName, ParameterExpression paramExpression, CachedTypedInfo cachedTypedInfo, Type propertyType, object defaultValue)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                return paramExpression;
            }
            if (!propertyName.Contains(".") && !propertyName.Contains("["))
            {
                if (cachedTypedInfo != null && cachedTypedInfo.CustomTypeProperties != null && cachedTypedInfo.CustomTypeProperties.Count > 0)
                {
                    CustomTypeProperty item = cachedTypedInfo.CustomTypeProperties[propertyName];
                    if (item != null)
                    {
                        Type type = item.PropertyInfo.GetType();
                        Type[] typeArray = new Type[] { typeof(object), typeof(object[]) };
                        MethodInfo method = type.GetMethod("GetValue", typeArray);
                        Expression expression = Expression.Call(Expression.Constant(item.PropertyInfo), method, paramExpression, Expression.Constant(new object[0]));
                        Expression expression1 = Expression.Convert(expression, propertyType);
                        Expression expression2 = Expression.Equal(expression, Expression.Constant(DBNull.Value, typeof(object)));
                        if (!item.PropertyType.IsValueType)
                        {
                            Expression expression3 = Expression.Equal(expression, Expression.Constant(null, typeof(object)));
                            expression2 = Expression.Or(expression2, expression3);
                        }
                        Expression expression4 = null;
                        if (defaultValue == null)
                        {
                            expression4 = Expression.Default(propertyType);
                        }
                        else
                        {
                            expression4 = Expression.Constant(defaultValue, propertyType);
                        }
                        return Expression.Condition(expression2, expression4, expression1);
                    }
                }
                Type type1 = paramExpression.Type;
                if (type1.IsInterface)
                {
                    PropertyInfo property = type1.GetProperty(propertyName);
                    if (property == null)
                    {
                        Type[] interfaces = cachedTypedInfo.CachedType.GetInterfaces();
                        for (int i = 0; i < (int)interfaces.Length; i++)
                        {
                            property = interfaces[i].GetProperty(propertyName);
                            if (property != null)
                            {
                                break;
                            }
                        }
                    }
                    if (property != null)
                    {
                        return Expression.Property(paramExpression, property);
                    }
                }
                return Expression.Property(paramExpression, propertyName);
            }
            Expression expression5 = null;
            string str = "";
            int length = propertyName.Length;
            for (int j = 0; j < length; j++)
            {
                char chr = propertyName[j];
                char chr1 = chr;
                if (chr1 == '.')
                {
                    if (str.Length > 0)
                    {
                        if (expression5 != null)
                        {
                            expression5 = DataManagerBase.AddNullRefCheckToExpression(DataManagerBase.ResolvePropertyExpression(expression5, str), expression5);
                        }
                        else
                        {
                            expression5 = DataManagerBase.ResolvePropertyExpression(paramExpression, str);
                        }
                    }
                    str = "";
                }
                else if (chr1 == '[')
                {
                    if (expression5 != null)
                    {
                        expression5 = DataManagerBase.AddNullRefCheckToExpression(DataManagerBase.ResolvePropertyExpression(expression5, str), expression5);
                    }
                    else if (str.Length <= 0)
                    {
                        expression5 = paramExpression;
                    }
                    else
                    {
                        expression5 = DataManagerBase.ResolvePropertyExpression(paramExpression, str);
                    }
                    int num = j + 1;
                    j = propertyName.IndexOf(']', num);
                    List<object> objs = new List<object>();
                    PropertyInfo propertyInfo = DataManagerBase.ResolveIndexerPropertyInfo(propertyName, num, j, expression5.Type, objs);
                    if (propertyInfo == null)
                    {
                        MethodInfo methodInfo = DataManagerBase.ResolveIndexerMethodInfo(expression5.Type, objs);
                        if (methodInfo == null)
                        {
                            throw new Exception(string.Format(  "InvalidPropertyPathException" , propertyName)); //SR.GetString( - [12/15/2016 A1]
        }
                        Expression[] expressionArray = new Expression[objs.Count];
                        for (int k = 0; k < objs.Count; k++)
                        {
                            expressionArray[k] = Expression.Constant(objs[k]);
                        }
                        expression5 = Expression.Call(expression5, methodInfo, expressionArray);
                    }
                    else
                    {
                        Expression[] expressionArray1 = new Expression[objs.Count];
                        for (int l = 0; l < objs.Count; l++)
                        {
                            expressionArray1[l] = Expression.Constant(objs[l]);
                        }
                        Expression expression6 = Expression.Equal(expression5, Expression.Constant(null, expression5.Type));
                        Expression expression7 = Expression.MakeIndex(expression5, propertyInfo, expressionArray1);
                        Expression expression8 = DataManagerBase.ExpressionTypeGenerationBase.CreateGenericExpressionConstant(expression7.Type);
                        expression5 = Expression.Condition(expression6, expression8, expression7);
                        if (propertyType != null && propertyType != propertyInfo.PropertyType && propertyInfo.PropertyType.IsAssignableFrom(propertyType))
                        {
                            expression5 = Expression.Convert(expression5, propertyType);
                        }
                    }
                    str = "";
                }
                else
                {
                    str = string.Concat(str, chr);
                }
            }
            if (str.Length > 0)
            {
                expression5 = DataManagerBase.AddNullRefCheckToExpression(DataManagerBase.ResolvePropertyExpression(expression5, str), expression5);
            }
            return expression5;
        }

        /// <summary>
        /// Wraps the IEditableCollectionVie.CancelEdit method
        /// </summary>
        /// <returns>Returns true if it's an IEditableCollectionView</returns>
        public bool CancelEdit()
        {
            if (this.IEditableCollectionViewData == null || !this.IEditableCollectionViewData.CanCancelEdit)
            {
                return false;
            }
            this.IEditableCollectionViewData.CancelEdit();
            return true;
        }

        /// <summary>
        /// Clears any cached information that the manager keeps.
        /// </summary>
        protected virtual void ClearCachedDataSource(bool invalidateTotalRowCount)
        {
            this._recordCount = -1;
            this._sortedFilterDataSource = null;
            this.IsSortedFilteredDataSourceCalculated = false;
            if (invalidateTotalRowCount)
            {
                this._totalRecordCount = -1;
            }
            this.InvalidateSortedFilterdDataSource();
        }

        /// <summary>
        /// Wraps the IEditableCollectionVie.CommitEdit method
        /// </summary>
        /// <returns>Returns true if it's an IEditableCollectionView</returns>
        public bool CommitEdit()
        {
            if (this.IEditableCollectionViewData == null)
            {
                return false;
            }
            this.IEditableCollectionViewData.CommitEdit();
            return true;
        }

        private static bool CopyValues(object newObj, object obj)
        {
            Type type = obj.GetType();
            if (newObj.GetType() != type)
            {
                return false;
            }
            PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty);
            for (int i = 0; i < (int)properties.Length; i++)
            {
                PropertyInfo propertyInfo = properties[i];
                if (propertyInfo.GetSetMethod() != null && (int)propertyInfo.GetIndexParameters().Length <= 0)
                {
                    object value = propertyInfo.GetValue(newObj, null);
                    object value1 = propertyInfo.GetValue(obj, null);
                    if (value1 != value && (value == null || value1 == null || !value1.Equals(value)))
                    {
                        propertyInfo.SetValue(newObj, propertyInfo.GetValue(obj, null), null);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Creates a generic data manager of the type of the first object in the source.
        /// </summary>
        /// <param name="dataSource">The source that the created manager should manage.</param>
        /// <returns>A new DataManagerBase.</returns>
        public static DataManagerBase CreateDataManager(IEnumerable dataSource)
        {
            return DataManagerBase.CreateDataManager(dataSource, null);
        }

        /// <returns></returns>
        /// <summary>
        /// Creates a generic data manager of the type of the first object in the source.
        /// </summary>
        /// <param name="dataSource">The source that the created manager should manage.</param>
        /// <param name="provider">The <see cref="T:DataManagerProvider" /> that will be used to Generate a DataManager.</param>
        /// <returns>A new DataManagerBase.</returns>
        public static DataManagerBase CreateDataManager(IEnumerable dataSource, DataManagerProvider provider)
        {
            return DataManagerBase.CreateDataManager(dataSource, provider, true, false, false);
        }

        internal static DataManagerBase CreateDataManager(IEnumerable dataSource, DataManagerProvider provider, bool unwrapCollectionView, bool preferEnumerableItemType, bool allowObjectItemType)
        {
            DataManagerBase dataManagerBase;
            try
            {
                DataManagerBase dataManagerBase1 = null;
                Type type = DataManagerBase.ResolveItemType(dataSource, unwrapCollectionView, preferEnumerableItemType);
                Type type1 = typeof(DataManager<>);
                if (provider != null)
                {
                    type1 = provider.ResolveDataManagerType();
                }
                if (type != null && (type != typeof(object) || allowObjectItemType))
                {
                    bool flag = true;
                    if (type.FullName == "System.ServiceModel.DomainServices.Client.Entity")
                    {
                        IEditableCollectionView editableCollectionView = dataSource as IEditableCollectionView;
                        if (editableCollectionView == null || !editableCollectionView.CanAddNew)
                        {
                            flag = false;
                        }
                        else
                        {
                            ICollectionView collectionViews = dataSource as ICollectionView;
                            if (collectionViews != null)
                            {
                                try
                                {
                                    object obj = editableCollectionView.AddNew();
                                    if (obj != null)
                                    {
                                        type = obj.GetType();
                                    }
                                    editableCollectionView.CancelNew();
                                }
                                catch (ArgumentNullException argumentNullException)
                                {
                                }
                                collectionViews.Refresh();
                            }
                        }
                    }
                    if (flag)
                    {
                        Type type2 = type1.MakeGenericType(new Type[] { type });
                        dataManagerBase1 = TypeActivator.CreateInstanceTBaseLazy<DataManagerBase>(type2);  // Activator.CreateInstance(type2, new object[0]) as DataManagerBase;
                        dataManagerBase1.DataSource = dataSource;
                        dataManagerBase1.SupportsDataManipulations = true;
                        dataManagerBase1._shouldUnwrapCollectionView = unwrapCollectionView;
                        dataManagerBase1._preferEnumerableItemType = preferEnumerableItemType;
                        if (dataManagerBase1.GetDataProperties().Count<DataField>() == 0 && dataManagerBase1.TotalRecordCount == 0)
                        {
                            dataManagerBase1.DataSource = null;
                            dataManagerBase = null;
                            return dataManagerBase;
                        }
                    }
                }
                dataManagerBase = dataManagerBase1;
            }
            catch (NotImplementedException notImplementedException)
            {
                dataManagerBase = null;
            }
            catch (MemberAccessException memberAccessException)
            {
                TypelessDataManager typelessDataManager = new TypelessDataManager()
                {
                    DataSource = dataSource,
                    SupportsDataManipulations = true
                };
                dataManagerBase = typelessDataManager;
            }
            return dataManagerBase;
        }

        private void CurrentFilter_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.ClearCachedDataSource(false);
        }

        private void CurrentFilter_CollectionItemChanged(object sender, EventArgs e)
        {
            this.ClearCachedDataSource(false);
        }

        private void CurrentSummaries_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.ClearCachedDataSource(false);
        }

        private void DataSource_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.OnDataSourceCollectionChanged(e);
        }

        private void DetachFilterEvents()
        {
            if (this._currentFilter != null)
            {
                this._currentFilter.CollectionChanged -= new NotifyCollectionChangedEventHandler(this.CurrentFilter_CollectionChanged);
                this._currentFilter.CollectionItemChanged -= new EventHandler<EventArgs>(this.CurrentFilter_CollectionItemChanged);
            }
        }

        /// <summary>
        /// Clears the collection changed handlers used by the DataManager
        /// </summary>        
        public void DetachWeekReferences()
        {
            if (this._weakDataSourceCollectionChanged != null)
            {
                this._weakDataSourceCollectionChanged.Detach();
                this._weakDataSourceCollectionChanged = null;
            }
            if (this._weakCollectionViewCurrentChanged != null)
            {
                this._weakCollectionViewCurrentChanged.Detach();
                this._weakCollectionViewCurrentChanged = null;
            }
        }

        /// <summary>
        /// Wraps the IEditableCollectionVie.EditItem method
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Returns true if it's an IEditableCollectionView</returns>
        public bool EditItem(object item)
        {
            if (this.IEditableCollectionViewData == null)
            {
                return false;
            }
            this.IEditableCollectionViewData.EditItem(item);
            return true;
        }

        /// <summary>
        /// Filters a list of items using the filter operands applied on the <see cref="T:DataManagerBase" />.
        /// </summary>
        /// <param name="items">The items.</param>
        /// <returns>Filtered list or null if filtering cannot be applied.</returns>
        /// <remarks>
        /// This method is added solely for the purpose of fast adding of an item in sorted/filtered list.
        /// </remarks>
        internal virtual IList FilterItems(IList items)
        {
            return null;
        }

        /// <summary>
        /// Filters an IEnumerable using provided filter items.
        /// </summary>
        /// <param name="items">Items to filter.</param>
        /// <param name="filtersToUse">Filters to use during fitlering.</param>
        /// <returns>Filtered list or null if filtering cannot be applied.</returns>
        internal virtual IList FilterItems(IEnumerable items, RecordFilterCollection filtersToUse)
        {
            return null;
        }

        /// <summary>
        /// Creates a <see cref="T:DataField" /> object, which contains information about a specific property.
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        protected virtual DataField GenerateDataField(PropertyInfo info)
        {
            DataField dataField = new DataField(info.Name, info.PropertyType);
            object[] customAttributes = info.GetCustomAttributes(true);
            if (customAttributes != null)
            {
                object[] objArray = customAttributes;
                int num = 0;
                while (num < (int)objArray.Length)
                {
                    Attribute attribute = (Attribute)objArray[num];
                    Type type = attribute.GetType();
                    if (!type.Name.Contains("DisplayAttribute"))
                    {
                        num++;
                    }
                    else
                    {
                        MethodInfo method = type.GetMethod("GetAutoGenerateField");
                        if (method == null)
                        {
                            break;
                        }
                        bool? nullable = (bool?)method.Invoke(attribute, null);
                        if (!nullable.HasValue)
                        {
                            break;
                        }
                        dataField.AutoGenerate = nullable.Value;
                        break;
                    }
                }
            }
            return dataField;
        }

        /// <summary>
        /// Creates a new object with of <see cref="P:DataManagerBase.DataType" /> type.
        /// </summary>
        /// <returns></returns>
        public object GenerateNewObject()
        {
            object obj = null;
            if (this.IEditableCollectionViewData == null || !this.IEditableCollectionViewData.CanAddNew)
            {
                obj = this.OnNewObjectGeneration();
                if (obj != null)
                {
                    if (!this.CachedType.IsAssignableFrom(obj.GetType()))
                    {
                        throw new DataObjectTypeMismatchException();
                    }
                    return obj;
                }
                try
                {
                    if (this.CachedType != null)
                    {
                        obj = TypeActivator.CreateInstanceBoxedLazy(CachedType); //Activator.CreateInstance(this.CachedType);
                    }
                }
                catch (MissingMemberException missingMemberException1)
                {
                    MissingMemberException missingMemberException = missingMemberException1;
                    if (this.CachedType == null)
                    {
                        throw new RequireEmptyConstructorException("RequireEmptyConstructorException", missingMemberException); //SR.GetString( //  [12/15/2016 A1]
                    }
                    throw new RequireEmptyConstructorException(string.Format("RequireEmptyConstructorExceptionWithType"  , this.CachedType.FullName), missingMemberException); //SR.GetString( //  [12/15/2016 A1]
                }
                return obj;
            }
            object currentItem = null;
            if (this.ICollectionViewData != null)
            {
                currentItem = this.ICollectionViewData.CurrentItem;
            }
            this._ignoreCurrentChanging = true;
            obj = this.IEditableCollectionViewData.AddNew();
            this.IEditableCollectionViewData.CancelNew();
            this._ignoreCurrentChanging = false;
            if (this.ICollectionViewData != null)
            {
                this.ICollectionViewData.MoveCurrentTo(currentItem);
                if (this.ICollectionViewData.IsEmpty)
                {
                    this.ICollectionViewData.Refresh();
                }
            }
            return obj;
        }

        internal static Type GetCustomOrCLRType(object instance)
        {
            ICustomTypeProvider customTypeProvider = instance as ICustomTypeProvider;
            if (customTypeProvider == null)
            {
                return instance.GetType();
            }
            return customTypeProvider.GetCustomType() ?? instance.GetType();
        }

        /// <summary>
        /// Returns an IEnumerable of <see cref="T:DataField" />'s that describe the different fields in this object.
        /// </summary>
        /// <returns>An IEnumerable of the DataField's for all the properties of the object.</returns>
        public IEnumerable<DataField> GetDataProperties()
        {
            return DataManagerBase.ResolvePropertiesForCachedType(this.CachedTypedInfo, this);
        }

        /// <summary>
        /// Gets the <see cref="P:DataManagerBase.DataSource" /> associated with this <see cref="T:DataManagerBase" />.
        /// </summary>
        protected virtual IEnumerable GetDataSource()
        {
            return this._dataSource;
        }

        public static object GetDefaultValue(Type t)
        {
            if (!t.IsValueType)
            {
                return null;
            }
            return TypeActivator.CreateInstanceBoxedLazy(t);  // Activator.CreateInstance(t);
        }

        /// <summary>
        /// Returns an object in the data source at a given index, after applying the sort and filter.
        /// </summary>
        /// <param name="recordIndex">The index of the item to find.</param>
        /// <returns>The object at that index.</returns>
        public object GetRecord(int recordIndex)
        {
            if (this.GetDataSource() == null)
            {
                return null;
            }
            this.UpdateDataSource = false;
            if (this.SortedFilteredDataSource != null)
            {
                this.UpdateDataSource = true;
                return this.SortedFilteredDataSource[recordIndex];
            }
            this.UpdateDataSource = true;
            if (this.IListData == null)
            {
                return this.ResolveRecord(recordIndex);
            }
            return this.IListData[recordIndex];
        }

        private void ICollectionViewData_CurrentChanged(object sender, EventArgs e)
        {
            if (!this._ignoreCurrentChanging)
            {
                this.OnCurrentItemChanged(this.ICollectionViewData.CurrentItem);
            }
        }

        /// <summary>
        /// Adds inputted object to the datasource at the given index
        /// </summary>
        /// <param name="index"></param>
        /// <param name="value"></param>
        public void InsertRecord(int index, object value)
        {
            if (this.IListData == null)
            {
                throw new DataSourceDoesNotSupportIListException();
            }
            this.IListData.Insert(index, value);
            this.ClearCachedDataSource(true);
            this.OnDataUpdated();
        }

        private void InvalidateSortedFilterdDataSource()
        {
            if (!this.SuspendInvalidateDataSource && !this.IsSortedFilteredDataSourceCalculated && this._dataSource != null)
            {
                this.ResolveFilteredSortedPagedDataSource();
                this.IsSortedFilteredDataSourceCalculated = true;
            }
        }

        /// <summary>
        /// Raises the CollectionChanged event.
        /// </summary>
        /// <param name="e">Data about the collection being changed.</param>
        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (this.CollectionChanged != null)
            {
                this.CollectionChanged(this, e);
            }
        }

        /// <summary>
        /// Raises the <see cref="E:DataManagerBase.CurrentItemChanged" /> event.
        /// </summary>
        /// <param name="item"></param>
        protected virtual void OnCurrentItemChanged(object item)
        {
            if (this.CurrentItemChanged != null)
            {
                this.CurrentItemChanged(this, new CurrentItemEventArgs()
                {
                    Item = item
                });
            }
        }

        /// <summary>
        /// Triggered when the underlying data source's data is changed.
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnDataSourceCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            int? nullable;
            bool flag = true;
            if ((this.Summaries == null ? true : !this.Summaries.Any<SummaryDefinition>()) && this._sortedFilterDataSource != null)
            {
                if (e.Action == NotifyCollectionChangedAction.Add)
                {
                    IList lists = this.FilterItems(e.NewItems);
                    if (lists != null && lists.Count != 0)
                    {
                        flag = false;
                        foreach (object obj in lists)
                        {
                            int? nullable1 = this.ResolveIndexForInsertOrDelete(obj, true);
                            if (!nullable1.HasValue)
                            {
                                flag = true;
                            }
                            else
                            {
                                int? nullable2 = nullable1;
                                if ((nullable2.GetValueOrDefault() >= 0 ? false : nullable2.HasValue))
                                {
                                    int? nullable3 = nullable1;
                                    if (nullable3.HasValue)
                                    {
                                        nullable = new int?(~nullable3.GetValueOrDefault());
                                    }
                                    else
                                    {
                                        nullable = null;
                                    }
                                    nullable1 = nullable;
                                }
                                if (!this.IsSorted)
                                {
                                    this._sortedFilterDataSource.Add(obj);
                                }
                                else
                                {
                                    this._sortedFilterDataSource.Insert(nullable1.Value, obj);
                                }
                                this.TotalRecordCount = -1;
                                if (this.UpdateCachedDataManipulations(obj, true))
                                {
                                    continue;
                                }
                                this.SetSortedFilteredDataSource(this._sortedFilterDataSource);
                            }
                        }
                    }
                }
                else if (e.Action == NotifyCollectionChangedAction.Remove)
                {
                    foreach (object oldItem in e.OldItems)
                    {
                        int? nullable4 = this.ResolveIndexForInsertOrDelete(oldItem, false);
                        if (!nullable4.HasValue)
                        {
                            flag = !this.UpdateCachedDataManipulations(oldItem, false);
                        }
                        else
                        {
                            int? nullable5 = nullable4;
                            if ((nullable5.GetValueOrDefault() < 0 ? false : nullable5.HasValue))
                            {
                                this._sortedFilterDataSource.RemoveAt(nullable4.Value);
                                this.TotalRecordCount = -1;
                                this.SetSortedFilteredDataSource(this._sortedFilterDataSource);
                            }
                            this.UpdateCachedDataManipulations(oldItem, false);
                            flag = false;
                        }
                    }
                }
            }
            if (flag)
            {
                this.ClearCachedDataSource(true);
            }
            this.OnCollectionChanged(e);
            this.OnDataUpdated();
        }

        /// <summary>
        /// Raises the <see cref="E:DataManagerBase.DataUpdated" /> event.
        /// </summary>
        protected virtual void OnDataUpdated()
        {
            if (this.DataUpdated != null)
            {
                this.DataUpdated(this, new EventArgs());
            }
        }

        /// <summary>
        /// Raises the <see cref="E:DataManagerBase.NewObjectGeneration" /> event.
        /// </summary>
        /// <returns></returns>
        protected internal virtual object OnNewObjectGeneration()
        {
            HandleableObjectGenerationEventArgs handleableObjectGenerationEventArg = new HandleableObjectGenerationEventArgs()
            {
                CollectionType = this.CollectionType,
                ObjectType = this.CachedType,
                Handled = false
            };
            if (this.NewObjectGeneration != null)
            {
                this.NewObjectGeneration(this, handleableObjectGenerationEventArg);
            }
            if (!handleableObjectGenerationEventArg.Handled)
            {
                return null;
            }
            return handleableObjectGenerationEventArg.NewObject;
        }

        /// <summary>
        /// Raises the <see cref="E:DataManagerBase.ResolvingData" /> event.
        /// </summary>
        /// <param name="args"></param>
        protected internal virtual void OnResolvingData(DataAcquisitionEventArgs args)
        {
            if (this.ResolvingData != null)
            {
                this.ResolvingData(this, args);
            }
        }

        /// <summary>
        /// Reevaluates the summaries for the ItemsSource bound to this <see cref="T:DataManagerBase" />.
        /// </summary>
        public virtual void RefreshSummaries()
        {
        }

        /// <summary>
        /// Removes a record from the datasource.
        /// </summary>
        /// <param name="value"></param>
        /// <returns>True if the record was removed, false otherwise.</returns>
        public bool RemoveRecord(object value)
        {
            if (this.IEditableCollectionViewData == null)
            {
                if (this.IListData == null)
                {
                    throw new DataSourceDoesNotSupportIListException();
                }
                this.IListData.Remove(value);
                this.ClearCachedDataSource(true);
            }
            else if (this.IEditableCollectionViewData.CanRemove)
            {
                this.IEditableCollectionViewData.Remove(value);
            }
            this.OnDataUpdated();
            return true;
        }

        /// <summary>
        /// Clears out any stored information on the previous DataSource.
        /// </summary>
        public virtual void Reset()
        {
            this.Sort.Clear();
            this.ClearCachedDataSource(true);
            this._cachedType = null;
            this._cachedCollectionType = null;
            this._cachedTypedInfo = null;
            if (this.SummaryResultCollection != null)
            {
                this.SummaryResultCollection.Clear();
            }
            this.DetachFilterEvents();
            this._currentFilter = null;
        }

        /// <summary>
        /// Resolves the underlying type of the item that the specified collection contains.
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static Type ResolveCollectionType(IEnumerable dataSource)
        {
            Type type = null;
            IEnumerator enumerator = dataSource.GetEnumerator();
            if (enumerator == null)
            {
                throw new InvalidEnumeratorException("InvalidEnumeratorException");   //SR.GetString( //  [12/15/2016 A1]
            }
            Type type1 = enumerator.GetType();
            if (type1.IsGenericType)
            {
                Type[] genericArguments = type1.GetGenericArguments();
                if ((int)genericArguments.Length > 0)
                {
                    type = genericArguments[0];
                }
            }
            return type;
        }

        /// <summary>
        /// Determines the size of the collection by walking through the DataSource.
        /// </summary>
        /// <returns></returns>
        protected virtual int ResolveCount()
        {
            int num = 0;
            foreach (object dataSource in this.GetDataSource())
            {
                num++;
            }
            return num;
        }

        /// <summary>
        /// Uses the existing paging, sorting, and filtering information to build a cached object for the
        /// DataManagerBase to use.
        /// </summary>
        protected virtual void ResolveFilteredSortedPagedDataSource()
        {
        }

        private static object ResolveFromPropertyPath(string propertyPath, object data, bool returnValue)
        {
            if (!propertyPath.Contains(".") && !propertyPath.Contains("["))
            {
                string[] strArrays = propertyPath.Split(new char[] { '.' });
                object value = data;
                PropertyInfo propertyInfo = null;
                Type type = data.GetType();
                string[] strArrays1 = strArrays;
                for (int i = 0; i < (int)strArrays1.Length; i++)
                {
                    string str = strArrays1[i];
                    data = value;
                    PropertyInfo[] properties = type.GetProperties();
                    int num = 0;
                    while (num < (int)properties.Length)
                    {
                        PropertyInfo propertyInfo1 = properties[num];
                        if (string.Compare(propertyInfo1.Name, str, StringComparison.Ordinal) != 0 || (int)propertyInfo1.GetIndexParameters().Length != 0)
                        {
                            num++;
                        }
                        else
                        {
                            propertyInfo = propertyInfo1;
                            break;
                        }
                    }
                    if (propertyInfo == null)
                    {
                        break;
                    }
                    value = propertyInfo.GetValue(value, null);
                    if (value == null)
                    {
                        break;
                    }
                    type = propertyInfo.PropertyType;
                }
                if (returnValue)
                {
                    return value;
                }
                return propertyInfo;
            }
            Type propertyType = data.GetType();
            string str1 = "";
            object obj = data;
            int length = propertyPath.Length;
            PropertyInfo propertyInfo2 = null;
            for (int j = 0; j < length; j++)
            {
                char chr = propertyPath[j];
                char chr1 = chr;
                if (chr1 != '.')
                {
                    if (chr1 == '[')
                    {
                        if (str1.Length > 0)
                        {
                            propertyInfo2 = DataManagerBase.ResolveProperty(propertyType, str1);
                            if (propertyInfo2 == null)
                            {
                                return null;
                            }
                            propertyType = propertyInfo2.PropertyType;
                            obj = propertyInfo2.GetValue(obj, null);
                            if (obj == null)
                            {
                                return null;
                            }
                        }
                        str1 = "";
                        int num1 = j + 1;
                        j = propertyPath.IndexOf(']', num1);
                        List<object> objs = new List<object>();
                        propertyInfo2 = DataManagerBase.ResolveIndexerPropertyInfo(propertyPath, num1, j, propertyType, objs);
                        if (propertyInfo2 != null)
                        {
                            propertyType = propertyInfo2.PropertyType;
                            obj = propertyInfo2.GetValue(obj, objs.ToArray());
                        }
                        else
                        {
                            MethodInfo methodInfo = DataManagerBase.ResolveIndexerMethodInfo(propertyType, objs);
                            if (methodInfo == null)
                            {
                                return null;
                            }
                            propertyType = methodInfo.ReturnType;
                            obj = methodInfo.Invoke(obj, objs.ToArray());
                        }
                        if (obj == null)
                        {
                            return null;
                        }
                        str1 = "";
                    }
                    else
                    {
                        str1 = string.Concat(str1, chr);
                    }
                }
                else if (str1.Length > 0)
                {
                    propertyInfo2 = DataManagerBase.ResolveProperty(propertyType, str1);
                    if (propertyInfo2 == null)
                    {
                        return null;
                    }
                    propertyType = propertyInfo2.PropertyType;
                    obj = propertyInfo2.GetValue(obj, null);
                    if (obj == null)
                    {
                        return null;
                    }
                    str1 = "";
                }
            }
            if (str1.Length > 0)
            {
                propertyInfo2 = DataManagerBase.ResolveProperty(propertyType, str1);
                if (propertyInfo2 == null)
                {
                    return null;
                }
                propertyType = propertyInfo2.PropertyType;
                obj = propertyInfo2.GetValue(obj, null);
            }
            if (returnValue)
            {
                return obj;
            }
            return propertyInfo2;
        }

        private static MethodInfo ResolveIndexerMethodInfo(Type type, List<object> indexerParams)
        {
            MethodInfo method = null;
            MethodInfo[] methods = type.GetMethods();
            if ((int)methods.Length != 1)
            {
                MethodInfo[] methodInfoArray = methods;
                for (int i = 0; i < (int)methodInfoArray.Length; i++)
                {
                    MethodInfo methodInfo = methodInfoArray[i];
                    if (methodInfo.Name == "Get")
                    {
                        bool flag = false;
                        ParameterInfo[] parameters = methodInfo.GetParameters();
                        if (parameters != null && (int)parameters.Length == indexerParams.Count)
                        {
                            flag = true;
                            int num = 0;
                            while (num < indexerParams.Count)
                            {
                                if (parameters[num].ParameterType == indexerParams[num].GetType())
                                {
                                    num++;
                                }
                                else
                                {
                                    flag = false;
                                    break;
                                }
                            }
                        }
                        if (flag)
                        {
                            method = methodInfo;
                            break;
                        }
                    }
                }
            }
            else
            {
                method = type.GetMethod("Get");
            }
            return method;
        }

        private static PropertyInfo ResolveIndexerPropertyInfo(string propertyName, int startIndex, int endIndex, Type type, List<object> indexerParams)
        {
            if (endIndex != -1)
            {
                string str = propertyName.Substring(startIndex, endIndex - startIndex);
                string[] strArrays = str.ToString().Split(new char[] { ',' });
                for (int i = 0; i < (int)strArrays.Length; i++)
                {
                    string str1 = strArrays[i];
                    object obj = str1;
                    int num = -1;
                    if (int.TryParse(str1, out num))
                    {
                        obj = num;
                    }
                    indexerParams.Add(obj);
                }
            }
            PropertyInfo propertyInfo = null;
            string memberName = "Item";
            object[] customAttributes = type.GetCustomAttributes(typeof(DefaultMemberAttribute), true);
            if ((int)customAttributes.Length > 0)
            {
                DefaultMemberAttribute defaultMemberAttribute = customAttributes[0] as DefaultMemberAttribute;
                if (defaultMemberAttribute != null)
                {
                    memberName = defaultMemberAttribute.MemberName;
                }
            }
            PropertyInfo[] properties = type.GetProperties();
            for (int j = 0; j < (int)properties.Length; j++)
            {
                PropertyInfo propertyInfo1 = properties[j];
                if (propertyInfo1.Name == memberName)
                {
                    bool flag = false;
                    ParameterInfo[] indexParameters = propertyInfo1.GetIndexParameters();
                    if (indexParameters != null && (int)indexParameters.Length == indexerParams.Count)
                    {
                        flag = true;
                        int num1 = 0;
                        while (num1 < indexerParams.Count)
                        {
                            if (indexParameters[num1].ParameterType == indexerParams[num1].GetType())
                            {
                                num1++;
                            }
                            else
                            {
                                flag = false;
                                break;
                            }
                        }
                    }
                    if (flag)
                    {
                        propertyInfo = propertyInfo1;
                        break;
                    }
                }
            }
            return propertyInfo;
        }

        /// <summary>
        /// Resolves the index for insert or delete or return null if the Filtered DataSource is not supproeted.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="isAdding"></param>
        /// <returns></returns>
        internal virtual int? ResolveIndexForInsertOrDelete(object data, bool isAdding)
        {
            return new int?(this.ResolveIndexForRecord(data));
        }

        /// <summary>
        /// Looks through the filtered DataSource for the index of the item specified. 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int ResolveIndexForRecord(object data)
        {
            int num;
            if (this._sortedFilterDataSource != null)
            {
                return this._sortedFilterDataSource.IndexOf(data);
            }
            if (this.IListData != null)
            {
                return this.IListData.IndexOf(data);
            }
            int num1 = -1;
            IEnumerator enumerator = this.OriginalDataSource.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    object current = enumerator.Current;
                    num1++;
                    if (!object.Equals(current, data))
                    {
                        continue;
                    }
                    num = num1;
                    return num;
                }
                return -1;
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            return num;
        }

        private static Type ResolveInterfaceForProperty(Type type, string propertyName)
        {
            if (type.IsInterface)
            {
                List<Type> types = new List<Type>();
                Queue<Type> types1 = new Queue<Type>();
                types.Add(type);
                types1.Enqueue(type);
                while (types1.Count > 0)
                {
                    Type type1 = types1.Dequeue();
                    if (type1.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public) != null)
                    {
                        return type1;
                    }
                    Type[] interfaces = type1.GetInterfaces();
                    for (int i = 0; i < (int)interfaces.Length; i++)
                    {
                        Type type2 = interfaces[i];
                        if (!types.Contains(type2))
                        {
                            types.Add(type2);
                            types1.Enqueue(type2);
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Resolves the underlying type of the item that the specified collection contains.
        /// </summary>
        /// <param name="dataSource"></param>
        /// <returns></returns>
        public static Type ResolveItemType(IEnumerable dataSource)
        {
            return DataManagerBase.ResolveItemType(dataSource, false, false);
        }

        internal static Type ResolveItemType(IEnumerable dataSource, bool unwrapCollectionView, bool preferEnumerableItemType)
        {
            Type type = null;
            Type genericArguments = null;
            Type type1 = null;
            IEnumerator enumerator = dataSource.GetEnumerator();
            if (enumerator == null)
            {
                throw new InvalidEnumeratorException("InvalidEnumeratorException"); //SR.GetString( //  [12/15/2016 A1]
            }
            if (enumerator.MoveNext())
            {
                object current = enumerator.Current;
                if (current != null)
                {
                    type = current.GetType();
                }
            }
            bool flag = false;
            Type type2 = dataSource.GetType();
            if (typeof(IDictionary).IsAssignableFrom(type2))
            {
                flag = true;
            }
            else if (type2.IsGenericType && type2.GetGenericTypeDefinition() == typeof(IDictionary<,>))
            {
                flag = true;
            }
            else if (((IEnumerable<Type>)type2.GetInterfaces()).Any<Type>((Type i) =>
            {
                if (!i.IsGenericType)
                {
                    return false;
                }
                return i.GetGenericTypeDefinition() == typeof(IDictionary<,>);
            }))
            {
                flag = true;
            }
            if (!flag)
            {
                bool flag1 = false;
                Type[] interfaces = type2.GetInterfaces();
                int num = 0;
                while (num < (int)interfaces.Length)
                {
                    Type type3 = interfaces[num];
                    if (!type3.IsGenericType || type3.GetGenericTypeDefinition() != typeof(IEnumerable<>))
                    {
                        num++;
                    }
                    else
                    {
                        genericArguments = type3.GetGenericArguments()[0];
                        flag1 = true;
                        break;
                    }
                }
                if (!flag1)
                {
                    Type[] typeArray = enumerator.GetType().GetInterfaces();
                    int num1 = 0;
                    while (num1 < (int)typeArray.Length)
                    {
                        Type type4 = typeArray[num1];
                        if (!type4.IsGenericType || type4.GetGenericTypeDefinition() != typeof(IEnumerator<>))
                        {
                            num1++;
                        }
                        else
                        {
                            genericArguments = type4.GetGenericArguments()[0];
                            break;
                        }
                    }
                }
                if (!flag1 && unwrapCollectionView)
                {
                    ICollectionView collectionViews = dataSource as ICollectionView;
                    if (collectionViews != null && collectionViews.SourceCollection != null)
                    {
                        return DataManagerBase.ResolveItemType(collectionViews.SourceCollection, unwrapCollectionView, preferEnumerableItemType);
                    }
                }
            }
            if (preferEnumerableItemType && genericArguments != null)
            {
                type1 = genericArguments;
            }
            else if (type != null && genericArguments == null)
            {
                type1 = type;
            }
            else if (type != null || genericArguments == null)
            {
                IEnumerable<DataField> dataFields = DataManagerBase.ResolvePropertiesForItemType(genericArguments, null, 0);
                IEnumerable<DataField> dataFields1 = DataManagerBase.ResolvePropertiesForItemType(type, null, 0);
                if (dataFields != null && !dataFields.Any<DataField>())
                {
                    type1 = type;
                }
                else if (dataFields1 == null || dataFields1.Any<DataField>())
                {
                    type1 = (genericArguments == null || genericArguments == typeof(object) || !(genericArguments.FullName != "System.Windows.Ria.Entity") || !(genericArguments.FullName != "System.ServiceModel.DomainServices.Client.Entity") ? type : genericArguments);
                }
                else
                {
                    type1 = genericArguments;
                }
            }
            else
            {
                type1 = genericArguments;
            }
            return type1;
        }

        internal static PropertyInfo[] ResolveProperties(Type type)
        {
            List<PropertyInfo> propertyInfos = new List<PropertyInfo>();
            if (!type.IsInterface)
            {
                PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                propertyInfos.AddRange(
                    from i in (IEnumerable<PropertyInfo>)properties
                    where (int)i.GetIndexParameters().Length == 0
                    select i);
            }
            else
            {
                List<Type> types = new List<Type>();
                Queue<Type> types1 = new Queue<Type>();
                types.Add(type);
                types1.Enqueue(type);
                while (types1.Count > 0)
                {
                    Type type1 = types1.Dequeue();
                    Type[] interfaces = type1.GetInterfaces();
                    for (int num = 0; num < (int)interfaces.Length; num++)
                    {
                        Type type2 = interfaces[num];
                        if (!types.Contains(type2))
                        {
                            types.Add(type2);
                            types1.Enqueue(type2);
                        }
                    }
                    PropertyInfo[] propertyInfoArray = type1.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                    propertyInfos.InsertRange(0, ((IEnumerable<PropertyInfo>)propertyInfoArray).Where<PropertyInfo>((PropertyInfo i) =>
                    {
                        if (propertyInfos.Contains(i))
                        {
                            return false;
                        }
                        return (int)i.GetIndexParameters().Length == 0;
                    }));
                }
            }
            return propertyInfos.ToArray();
        }

        private static IEnumerable<DataField> ResolvePropertiesForCachedType(CachedTypedInfo typeInfo, DataManagerBase manager)
        {
            if (typeInfo == null)
            {
                return null;
            }
            int count = 0;
            bool flag = (typeInfo.CustomTypeProperties == null ? false : typeInfo.CustomTypeProperties.Any<CustomTypeProperty>());
            if (flag)
            {
                count = typeInfo.CustomTypeProperties.Count;
            }
            IEnumerable<DataField> dataFields = DataManagerBase.ResolvePropertiesForItemType(typeInfo.CachedType, manager, count);
            List<DataField> dataFields1 = (dataFields != null ? dataFields.ToList<DataField>() : new List<DataField>());
            if (flag)
            {
                int num = count;
                foreach (CustomTypeProperty customTypeProperty in typeInfo.CustomTypeProperties)
                {
                    if (manager == null)
                    {
                        dataFields1.Add(new DataField(customTypeProperty.PropertyName, customTypeProperty.PropertyType));
                    }
                    else
                    {
                        DataField dataField = manager.GenerateDataField(customTypeProperty.PropertyInfo);
                        if (dataField != null)
                        {
                            dataFields1.Add(dataField);
                            if (dataField.Order == 2147483647)
                            {
                                DataField order = dataField;
                                order.Order = order.Order - num;
                            }
                        }
                    }
                    num--;
                }
                dataFields1.Sort(new DataFieldComparer());
            }
            return dataFields1;
        }

        private static IEnumerable<DataField> ResolvePropertiesForItemType(Type itemType, DataManagerBase manager, int indexShift = 0)
        {
            if (itemType == null)
            {
                return null;
            }
            List<DataField> dataFields = new List<DataField>();
            PropertyInfo[] propertyInfoArray = DataManagerBase.ResolveProperties(itemType);
            int length = (int)propertyInfoArray.Length;
            PropertyInfo[] propertyInfoArray1 = propertyInfoArray;
            for (int i = 0; i < (int)propertyInfoArray1.Length; i++)
            {
                PropertyInfo propertyInfo = propertyInfoArray1[i];
                if (manager == null)
                {
                    dataFields.Add(new DataField(propertyInfo.Name, propertyInfo.PropertyType));
                }
                else
                {
                    DataField dataField = manager.GenerateDataField(propertyInfo);
                    if (dataField != null)
                    {
                        dataFields.Add(dataField);
                        if (dataField.Order == 2147483647)
                        {
                            DataField order = dataField;
                            order.Order = order.Order - (length + indexShift);
                        }
                    }
                }
                length--;
            }
            dataFields.Sort(new DataFieldComparer());
            return dataFields;
        }

        internal static PropertyInfo ResolveProperty(Type type, string propertyName)
        {
            if (!type.IsInterface)
            {
                return type.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
            }
            List<Type> types = new List<Type>();
            Queue<Type> types1 = new Queue<Type>();
            types.Add(type);
            types1.Enqueue(type);
            while (types1.Count > 0)
            {
                Type type1 = types1.Dequeue();
                PropertyInfo property = type1.GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
                if (property != null)
                {
                    return property;
                }
                Type[] interfaces = type1.GetInterfaces();
                for (int i = 0; i < (int)interfaces.Length; i++)
                {
                    Type type2 = interfaces[i];
                    if (!types.Contains(type2))
                    {
                        types.Add(type2);
                        types1.Enqueue(type2);
                    }
                }
            }
            return null;
        }

        private static MemberExpression ResolvePropertyExpression(Expression expression, string propertyName)
        {
            if (expression.Type.IsInterface)
            {
                Type type = DataManagerBase.ResolveInterfaceForProperty(expression.Type, propertyName);
                if (type != expression.Type)
                {
                    return Expression.Property(Expression.Convert(expression, type), propertyName);
                }
            }
            return Expression.Property(expression, propertyName);
        }

        /// <summary>
        /// Walks through the property tree of an object to resolve the propretyInfo 
        /// </summary>
        /// <param name="propertyPath"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static PropertyInfo ResolvePropertyInfoFromPropertyPath(string propertyPath, object data)
        {
            return DataManagerBase.ResolveFromPropertyPath(propertyPath, data, false) as PropertyInfo;
        }

        /// <summary>
        /// Resolves the type of a property for complex properties such as Address.Stree1.
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="rootType"></param>
        /// <returns></returns>
        public static Type ResolvePropertyTypeFromPropertyName(string propertyName, CachedTypedInfo rootType)
        {
            if (propertyName.Contains("["))
            {
                Expression expression = DataManagerBase.BuildPropertyExpressionFromPropertyName(propertyName, Expression.Parameter(rootType.CachedType, "param"));
                return expression.Type;
            }
            if (rootType.CustomTypeProperties != null)
            {
                CustomTypePropertyCollection customTypeProperties = rootType.CustomTypeProperties;
                if (customTypeProperties.Count > 0)
                {
                    CustomTypeProperty customTypeProperty = (
                        from i in customTypeProperties
                        where i.PropertyName.Equals(propertyName)
                        select i).FirstOrDefault<CustomTypeProperty>();
                    if (customTypeProperty != null)
                    {
                        return customTypeProperty.PropertyType;
                    }
                }
            }
            string[] strArrays = propertyName.Split(new char[] { '.' });
            Type propertyType = DataManagerBase.ResolveProperty(rootType.CachedType, strArrays[0]).PropertyType;
            for (int num = 1; num < (int)strArrays.Length; num++)
            {
                PropertyInfo propertyInfo = DataManagerBase.ResolveProperty(propertyType, strArrays[num]);
                propertyType = propertyInfo.PropertyType;
            }
            return propertyType;
        }

        /// <summary>
        /// Resolve the specified record at a given index. 
        /// </summary>
        /// <param name="recordIndex"></param>
        /// <returns></returns>
        /// <remarks> This method only gets called, if the data source isn't of type IList.</remarks>
        protected virtual object ResolveRecord(int recordIndex)
        {
            object obj;
            int num = 0;
            IEnumerator enumerator = this.GetDataSource().GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    object current = enumerator.Current;
                    if (num != recordIndex)
                    {
                        num++;
                    }
                    else
                    {
                        obj = current;
                        return obj;
                    }
                }
                return null;
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            return obj;
        }

        /// <summary>
        /// Walks the property tree of an object to resolve properties such as Address.Street1 or Items[FirstName] 
        /// </summary>
        /// <param name="propertyPath"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static object ResolveValueFromPropertyPath(string propertyPath, object data)
        {
            return DataManagerBase.ResolveFromPropertyPath(propertyPath, data, true);
        }

        /// <summary>
        /// Sets the <see cref="P:DataManagerBase.DataSource" /> while registering for change notification.
        /// </summary>
        /// <param name="source">The IEnumerable to set the DataSource to.</param>
        protected virtual void SetDataSource(IEnumerable source)
        {
            if (source == this._dataSource)
            {
                return;
            }
            if (this._weakDataSourceCollectionChanged != null)
            {
                this._weakDataSourceCollectionChanged.Detach();
                this._weakDataSourceCollectionChanged = null;
            }
            if (this._weakCollectionViewCurrentChanged != null)
            {
                this._weakCollectionViewCurrentChanged.Detach();
                this._weakCollectionViewCurrentChanged = null;
            }
            IEnumerable enumerable = this._dataSource;
            this._dataSource = source;
            if (enumerable != null)
            {
                this.ClearCachedDataSource(true);
            }
            if (source == null)
            {
                this.Reset();
                return;
            }
            this.CachedType = this.DataType;
            this.CachedCollectionType = this.CollectionType;
            INotifyCollectionChanged originalDataSource = this.OriginalDataSource as INotifyCollectionChanged;
            if (originalDataSource != null)
            {
                this._weakDataSourceCollectionChanged = new WeakCollectionChangedHandler<DataManagerBase>(this, originalDataSource, (DataManagerBase instance, object s, NotifyCollectionChangedEventArgs e) => instance.DataSource_CollectionChanged(s, e));
                originalDataSource.CollectionChanged += new NotifyCollectionChangedEventHandler(this._weakDataSourceCollectionChanged.OnEvent);
            }
            if (this.ICollectionViewData != null)
            {
                this._weakCollectionViewCurrentChanged = new WeakEventHandler<DataManagerBase, ICollectionView, EventArgs>(this, this.ICollectionViewData, (DataManagerBase instance, object s, EventArgs e) => instance.ICollectionViewData_CurrentChanged(s, e), (WeakEventHandler<DataManagerBase, ICollectionView, EventArgs> weakHandler, ICollectionView eventSource) => eventSource.CurrentChanged -= new EventHandler(weakHandler.OnEvent));
                this.ICollectionViewData.CurrentChanged += new EventHandler(this._weakCollectionViewCurrentChanged.OnEvent);
            }
        }

        /// <summary>
        /// Used to update the sorted, filtered, paged, and grouped data source.
        /// </summary>
        /// <param name="source"></param>
        protected virtual void SetSortedFilteredDataSource(IList source)
        {
            this._sortedFilterDataSource = source;
            if (source != null)
            {
                this.RecordCount = source.Count;
            }
        }

        /// <summary>
        /// Updates the cached data manipulations.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="isAdding">if set to <c>true</c> the item will be added to the cached lists, otherwise removed.</param>
        /// <returns><c>true</c> if the cache was updated, otherwise <c>false</c>.</returns>
        internal virtual bool UpdateCachedDataManipulations(object item, bool isAdding)
        {
            return false;
        }

        /// <summary>
        /// Moves the <see cref="T:System.ComponentModel.ICollectionView" /> current item pointer to the inputted item.
        /// </summary>
        /// <param name="item"></param>
        public void UpdateCurrentItem(object item)
        {
            if (this.ICollectionViewData != null)
            {
                this._ignoreCurrentChanging = true;
                this.ICollectionViewData.MoveCurrentTo(item);
                this._ignoreCurrentChanging = false;
            }
        }

        /// <summary>
        /// Clears the underlying cached data, and triggeres all data operations to be applied again.
        /// </summary>
        public void UpdateData()
        {
            this.ClearCachedDataSource(false);
        }

        /// <summary>
        /// Occurs when the data source has changed and it implements <see cref="T:System.Collections.Specialized.INotifyCollectionChanged" />.
        /// </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary>
        /// Event raised when the currentItem changes.
        /// </summary>
        public event EventHandler<CurrentItemEventArgs> CurrentItemChanged;

        /// <summary>
        /// Event raised when the underlying data changes.
        /// </summary>
        public event EventHandler<EventArgs> DataUpdated;

        /// <summary>
        /// Event raised when the <see cref="T:DataManagerBase" /> is attempting to create a new instance of the <see cref="P:DataManagerBase.CachedType" /> object.
        /// </summary>
        public event EventHandler<HandleableObjectGenerationEventArgs> NewObjectGeneration;

        /// <summary>
        /// Event raised when paging, filtering, sorting, or groupby actions are changed.
        /// </summary>
        public event EventHandler<DataAcquisitionEventArgs> ResolvingData;

        private class ExpressionTypeGeneration<T> : DataManagerBase.ExpressionTypeGenerationBase
        {
            public ExpressionTypeGeneration()
            {
            }

            protected override Expression GenerateConstantExpression()
            {
                return Expression.Constant(default(T), typeof(T));
            }
        }

        private abstract class ExpressionTypeGenerationBase
        {
            protected ExpressionTypeGenerationBase()
            {
            }

            public static Expression CreateGenericExpressionConstant(Type type)
            {
                Type type1 = typeof(ExpressionTypeGeneration<>).MakeGenericType(new Type[] { type });
                ExpressionTypeGenerationBase expressionTypeGenerationBase
                    = TypeActivator.CreateInstanceTBaseLazy<ExpressionTypeGenerationBase>(type1);  //(ExpressionTypeGenerationBase)   Activator.CreateInstance(type1, new object[0]);
                return expressionTypeGenerationBase.GenerateConstantExpression();
            }

            protected abstract Expression GenerateConstantExpression();
        }
    }
}
