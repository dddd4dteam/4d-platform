﻿
using System;
using System.ComponentModel;

using DDDD.Core.Caching.IG;
using DDDD.Core.Collections.Condition.IG;



namespace DDDD.Core.Collections.DataManage.IG
{
	/// <summary>
	/// The IRecordFilter interface is used to define objects which will contain a collection of conditions which will build up filters.
	/// </summary>
	public interface IRecordFilter : IExpressConditions, INotifyPropertyChanged
	{
		/// <summary>
		/// A Collection of Conditions which will be applied to the object.
		/// </summary>
		ConditionCollection Conditions
		{
			get;
		}

		/// <summary>
		/// The property on the underlying data object which the filter will be applied to.
		/// </summary>
		string FieldName
		{
			get;
		}

		/// <summary>
		/// The Type of the property that is being evaluated.
		/// </summary>
		Type FieldType
		{
			get;
		}

		/// <summary>
		/// The Type of the object that the filter will be applied to.
		/// </summary>
		Type ObjectType
		{
			get;
		}

		/// <summary>
		/// The Type of the object along with any PropertyDescriptors.
		/// </summary>
		CachedTypedInfo ObjectTypedInfo
		{
			get;
		}
	}
}
