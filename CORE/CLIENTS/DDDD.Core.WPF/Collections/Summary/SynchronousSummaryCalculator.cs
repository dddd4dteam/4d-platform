﻿using System.Linq;

namespace DDDD.Core.Collections.Summary.IG
{

    /// <summary>
    /// A summary that will be executed during the normal databinding in process.
    /// </summary>
    public abstract class SynchronousSummaryCalculator : SummaryCalculatorBase
    {
        protected SynchronousSummaryCalculator()
        {
        }

        /// <summary>
        /// Calculates the summary information from the records provided by the query.
        /// </summary>
        /// <param name="data">The LINQ that provides the data which is currently available.</param>
        /// <param name="fieldKey">The name of the field being acted on.</param>
        /// <returns></returns>
        public abstract object Summarize(IQueryable data, string fieldKey);
    }


}
