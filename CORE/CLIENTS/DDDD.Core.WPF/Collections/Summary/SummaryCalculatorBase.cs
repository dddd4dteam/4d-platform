﻿namespace DDDD.Core.Collections.Summary.IG
{
    /// <summary>
    /// Provides a base class for SummaryCalculators for the Summary framework.
    /// </summary>
    public abstract class SummaryCalculatorBase
    {
        /// <summary>
        /// Gets the <see cref="P: SummaryCalculatorBase.SummaryExecution" />, indicating when the summary will be applied.  
        /// </summary>
        /// <remarks>
        /// When overridden, this can be used to indicate when an individual summary should be evaluated.   Depending 
        /// on when the summary is executed the final result of the evaluation can change.
        /// </remarks>
        public virtual  SummaryExecution? SummaryExecution
        {
            get
            {
                return null;
            }
        }

        protected SummaryCalculatorBase()
        {
        }
    }
}
