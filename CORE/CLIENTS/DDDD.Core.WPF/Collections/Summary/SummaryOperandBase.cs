﻿using System.Windows;
 
using DDDD.Core.UI.IG;

namespace DDDD.Core.Collections.Summary.IG
{
    /// <summary>
    /// A base class for operands that will be used in the Summary framework which contains information 
    /// which is needed for display.
    /// </summary>
    public abstract class SummaryOperandBase : DependencyObjectNotifier
    {

        #region  ------------------------ CTOR -------------------------


        static SummaryOperandBase()
        {
            SummaryOperandBase.SelectionDisplayLabelProperty = DependencyProperty.Register("SelectionDisplayLabel", typeof(string), typeof(SummaryOperandBase), new PropertyMetadata(new PropertyChangedCallback(SummaryOperandBase.SelectionDisplayLabelChanged)));
            SummaryOperandBase.RowDisplayLabelProperty = DependencyProperty.Register("RowDisplayLabel", typeof(string), typeof(SummaryOperandBase), new PropertyMetadata(new PropertyChangedCallback(SummaryOperandBase.RowDisplayLabelChanged)));
            SummaryOperandBase.FormatStringProperty = DependencyProperty.Register("FormatString", typeof(string), typeof(SummaryOperandBase), new PropertyMetadata(new PropertyChangedCallback(SummaryOperandBase.FormatStringChanged)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: SummaryOperandBase" /> class.
        /// </summary>
        protected SummaryOperandBase()
        {
        }

        #endregion ------------------------ CTOR -------------------------


        private bool _isApplied;

        /// <summary>
        /// Identifies the <see cref="P: SummaryOperandBase.SelectionDisplayLabel" /> dependency property. 
        /// </summary>
        public readonly static DependencyProperty SelectionDisplayLabelProperty;

        /// <summary>
        /// Identifies the <see cref="P: SummaryOperandBase.RowDisplayLabel" /> dependency property. 
        /// </summary>
        public readonly static DependencyProperty RowDisplayLabelProperty;

        /// <summary>
        /// Identifies the <see cref="P: SummaryOperandBase.FormatString" /> dependency property. 
        /// </summary>
        public readonly static DependencyProperty FormatStringProperty;

        /// <summary>
        /// Get's the default text that will be displayed in a SummaryRow when this <see cref="T: SummaryOperandBase" /> is selected.
        /// </summary>
        protected abstract string DefaultRowDisplayLabel
        {
            get;
        }

        /// <summary>
        /// Get's the default text that will be displayed in the drop down for this <see cref="T: SummaryOperandBase" />
        /// </summary>
        protected abstract string DefaultSelectionDisplayLabel
        {
            get;
        }

        /// <summary>
        /// Gets/Sets the format string that will be applied the value of this summary. 
        /// </summary>
        public string FormatString
        {
            get
            {
                return (string)base.GetValue(SummaryOperandBase.FormatStringProperty);
            }
            set
            {
                base.SetValue(SummaryOperandBase.FormatStringProperty, value);
            }
        }

        /// <summary>
        /// Gets / sets if the summary should processed for this summary operand.
        /// </summary>
        public virtual bool IsApplied
        {
            get
            {
                return this._isApplied;
            }
            set
            {
                if (this._isApplied != value)
                {
                    this._isApplied = value;
                    this.OnPropertyChanged("IsApplied");
                }
            }
        }

        /// <summary>
        /// Gets the operator that will be associated with this operand.
        /// </summary>						
        public virtual  LinqSummaryOperator? LinqSummaryOperator
        {
            get
            {
                return null;
            }
        }

        /// <summary>
        /// Gets / sets the string that will be displayed in the SummaryRow when this <see cref="T: SummaryOperandBase" /> is selected.
        /// </summary>
        public string RowDisplayLabel
        {
            get
            {
                return (string)base.GetValue(SummaryOperandBase.RowDisplayLabelProperty);
            }
            set
            {
                base.SetValue(SummaryOperandBase.RowDisplayLabelProperty, value);
            }
        }

        /// <summary>
        /// Gets the value that will be displayed in the SummaryRow.
        /// </summary>
        public string RowDisplayLabelResolved
        {
            get
            {
                if (string.IsNullOrEmpty(this.RowDisplayLabel))
                {
                    return this.DefaultRowDisplayLabel;
                }
                return this.RowDisplayLabel;
            }
        }

        /// <summary>
        /// Gets the string that will be displayed in the drop down for this <see cref="T: SummaryOperandBase" />
        /// </summary>
        public string SelectionDisplayLabel
        {
            get
            {
                return (string)base.GetValue(SummaryOperandBase.SelectionDisplayLabelProperty);
            }
            set
            {
                base.SetValue(SummaryOperandBase.SelectionDisplayLabelProperty, value);
            }
        }

        /// <summary>
        /// Gets the value that will be displayed in the SummaryRow.
        /// </summary>
        public string SelectionDisplayLabelResolved
        {
            get
            {
                if (string.IsNullOrEmpty(this.SelectionDisplayLabel))
                {
                    return this.DefaultSelectionDisplayLabel;
                }
                return this.SelectionDisplayLabel;
            }
        }

        /// <summary>
        /// Gets the <see cref="P: SummaryOperandBase.SummaryCalculator" /> which will be used to calculate the summary.
        /// </summary>
        public virtual SummaryCalculatorBase SummaryCalculator
        {
            get
            {
                return null;
            }
        }


        private static void FormatStringChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SummaryOperandBase)obj).OnPropertyChanged("FormatString");
        }

        private static void RowDisplayLabelChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SummaryOperandBase)obj).OnPropertyChanged("RowDisplayLabel");
        }

        private static void SelectionDisplayLabelChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            ((SummaryOperandBase)obj).OnPropertyChanged("SelectionDisplayLabel");
        }
    }
}
