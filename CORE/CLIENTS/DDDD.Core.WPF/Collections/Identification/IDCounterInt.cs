﻿namespace DDDD.Core.Collections.Identification
{

    /// <summary>
    /// Counter of Nodes ID's  each next ID must not be presented earlier. Id type doesn't have constraints-it can be int/long/guid. 
    /// </summary>
    public interface IDCounter<TKey>
    {
        /// <summary>
        /// Get next  ID value from IDCounter 
        /// </summary>
        /// <returns></returns>
        TKey GetNext();


        /// <summary>
        /// Check that checkNewIDIsLast - is the lastest ID value, and Get next ID value from IDCounter.
        /// </summary>
        /// <param name="checkNewIDIsLast"></param>
        /// <returns></returns>
        TKey GetNext(TKey checkNewIDIsLast);


        /// <summary>
        /// Update last ID. 
        /// </summary>
        /// <param name="checkNewIDIsLast"></param>
        void UpdateLastID(TKey checkNewIDIsLast);

    }






    /// <summary>
    /// ID Counter for Nodes, integer.
    /// </summary>
    public class IDCounterInt:IDCounter<int>
    {
         int LastID = 0;

        /// <summary>
        /// Get  next ID value based on internal counter -LastID;
        /// </summary>
        /// <returns></returns>
        public  int GetNext()
        {
            LastID++;
            return LastID;
        }

        /// <summary>
        /// Get  next ID value based on internal counter -LastID;
        /// </summary>
        /// <param name="checkNewIDIsLast"></param>
        /// <returns></returns>
        public int GetNext(int checkNewIDIsLast)
        {
            if (checkNewIDIsLast > LastID) { LastID = checkNewIDIsLast + 1; return LastID; }
            else
            { LastID++; return LastID; }
        }

        /// <summary>
        /// Check and Update LastID Value if newValue is more or equal to LastID;
        /// </summary>
        /// <param name="checkNewIDIsLast"></param>
        public void UpdateLastID(int checkNewIDIsLast)
        {
            if (checkNewIDIsLast > (LastID)) { LastID = checkNewIDIsLast+1; }
        }

    }

}
