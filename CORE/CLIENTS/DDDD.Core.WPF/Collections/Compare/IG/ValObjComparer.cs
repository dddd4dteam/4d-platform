﻿using System;
using System.Collections.Generic;
using System.Windows.Data;

namespace DDDD.Core.Collections.Compare.IG
{
    internal class ValObjComparer<T> : IComparer<T>, IEqualityComparer<T>
    {
        private ValObj tbx = new ValObj();
        private ValObj tby = new ValObj();

        private IValueConverter Converter { get; set; }

        private object ConverterParameter { get; set; }

        protected internal ValObjComparer(IValueConverter converter, object converterParam)
        {
            this.Converter = converter;
            this.ConverterParameter = converterParam;
        }

        public int Compare(T x, T y)
        {
            this.tbx.SetBinding(ValObj.ValueProperty, new Binding()
            {
                Converter = this.Converter,
                ConverterParameter = this.ConverterParameter,
                Source = (object)x
            });
            this.tby.SetBinding(ValObj.ValueProperty, new Binding()
            {
                Converter = this.Converter,
                ConverterParameter = this.ConverterParameter,
                Source = (object)y
            });
            return string.Compare(this.tbx.Value, this.tby.Value, StringComparison.CurrentCulture);
        }

        bool IEqualityComparer<T>.Equals(T x, T y)
        {
            return this.Compare(x, y) == 0;
        }

        int IEqualityComparer<T>.GetHashCode(T obj)
        {
            this.tbx.SetBinding(ValObj.ValueProperty, new Binding()
            {
                Converter = this.Converter,
                ConverterParameter = this.ConverterParameter,
                Source = (object)obj
            });
            if (this.tbx.Value == null)
                return 0;
            return this.tbx.Value.GetHashCode();
        }
    }
}
