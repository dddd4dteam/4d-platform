﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Linq.Expressions;
using System.Reflection.Emit;
using DDDD.Core.Extensions;

namespace DDDD.Core.Collections.Compare
{



    /// <summary>
    ///Order - sorting order direction.  DESC -descending and ASC- ascending ordering direction- string const values.
    /// </summary>
    public struct Order
    {
        public const string DESC = nameof(DESC);

        public const string ASC = nameof(ASC);
    }


    /// <summary>
    /// IComparer interface- represents Comparer instance. 
    /// One Comparer contains-associates with one Comparison{ Type, OrderMembers } - that differentiates to others by Type and Order Members. 
    /// Interface used to create common Cache with different Comparers - like DynamicComparer{T}.  
    /// </summary>
    public interface IComparer
    {

        int ID { get; }


        Type ComparingType { get; }


        string ComparisonKey { get; }


        string OrderByKey { get; }


        KeyValuePair<string, string>[] CompareMembers  { get; }
        

        int Compare(object x, object y);
    }




    /// <summary>
    /// Dynamic Sort Comparer of  two T type instancies. Also it contains caching logic for different Comparisons.
    /// </summary>
    public class DynamicComparer<T>: IComparer, IComparer<T>
    {

        #region ---------------------------------- CTOR ---------------------------------------

        DynamicComparer(KeyValuePair<string, string>[] sortCompareMembers)
        {
            Initialize(sortCompareMembers);           
        }
        

        #endregion ---------------------------------- CTOR ---------------------------------------

        #region -------------------------- INITIALIZE ---------------------------------


        
        private void Initialize(KeyValuePair<string, string>[] sortCompareMembers)
        {
#if DEBUG
            try
            {
                ComparingType = typeof(T);
                CheckSortMembers(ComparingType, sortCompareMembers);//check sortMembers or throw exception 

                CompareMembers = sortCompareMembers;

                // unique ComparisonKey : 1- What is ComparingType, 2 -What members needs to be ordering- OrderMembers
                ComparisonKey = BuildSortComparisonKey(ComparingType, sortCompareMembers);
                ID = ComparisonKey.GetHashCode();

                // OrderByKey - we simply need to remove 1part of SortComparisonKey
                OrderByKey = ComparisonKey.Replace(ComparingType.FullName + "|", "");

                CompareFuncExpression = BuildCompareExpression(this);
                ComparisonBeginFunc = CompareFuncExpression.Compile();
                
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException("[{0}].[{1}] ERROR in Initialization DEBUG: {2}".Fmt(nameof(DynamicComparer<T>), nameof(Initialize), exc.Message));
            }
#else 
                ComparingType = typeof(T);
                CheckSortMembers(ComparingType, sortCompareMembers);//check sortMembers or throw exception 

                CompareMembers = sortCompareMembers;

                // unique ComparisonKey : 1- What is ComparingType, 2 -What members needs to be ordering- OrderMembers
                ComparisonKey = BuildSortComparisonKey(ComparingType, sortCompareMembers);
                ID = ComparisonKey.GetHashCode();

                // OrderByKey - we simply need to remove 1part of SortComparisonKey
                OrderByKey = ComparisonKey.Replace(ComparingType.FullName + "|", "");

                CompareFuncExpression = BuildCompareExpression(this);
                CompareFunc = CompareFuncExpression.Compile();
#endif
            
        }

        #endregion -------------------------- INITIALIZE ---------------------------------

        #region --------------------------- FIELDS ---------------------------------
        
        /// <summary>
        /// innner static object to use in [lock(locker){Action}] construction
        /// </summary>
        static readonly object locker = new object();

        #endregion --------------------------- FIELDS ---------------------------------
        
        #region ----------------------------INSTANCE PROPERTIES -----------------------------

        /// <summary>
        /// Target Sorting Type 
        /// </summary>
        public Type ComparingType
        { get; private set; }

        /// <summary>
        /// Unique Sort Comparison Key
        /// </summary>
        public string ComparisonKey
        { get; private set; }

        /// <summary>
        /// OrderBy Key - string value
        /// </summary>
        public string OrderByKey
        { get; private set; }

        /// <summary>
        /// ID - is  hash code of SortComparisonKey string will is unique
        /// </summary>
        public int ID
        { get; private  set; }


        /// <summary>
        /// Sorting Compare Members
        /// </summary>
        public KeyValuePair<string, string>[] CompareMembers
        { get; private set; } 


        /// <summary>
        /// Expression of builded by Linq.Expressions dynamic compare Function.
        /// </summary>
        public Expression<Comparison<T>> CompareFuncExpression
        { get; private set; }

        /// <summary>
        /// BeginFunc equal CompareFuncExpression.Compile()
        /// </summary>
        Comparison<T> ComparisonBeginFunc
        { get;  set; }

        


        #endregion ----------------------------INSTANCE PROPERTIES -----------------------------

        #region ---------------------------- GLOBAL PROPERTIES ----------------------------------

        /// <summary>
        /// Cached DynamicComparers
        /// </summary>
        static Dictionary<int, IComparer> CachedComparers
        { get; } = new Dictionary<int, IComparer>();

        #endregion ---------------------------- GLOBAL PROPERTIES ----------------------------------



        /// <summary>
        /// Create DynamicComparer instance for some specific Comparison with sortCompareMembers, or get existed instance from inner cache, with thread safety way.
        /// <para/> Here we use syntax of KeyValuePair{string, string} to declare [order by] members and their directions as: [string - order member name,string - 'DESC'/'ASC' order direction const value ] syntax.
        /// </summary>  
        /// <param name="sortCompareMembers"></param>
        /// <returns></returns>
        public static DynamicComparer<T> AddOrUseExists(params KeyValuePair<string, string>[] sortCompareMembers)
        {
            Type sortingType = typeof(T);
            var ComparisonKey = BuildSortComparisonKey(sortingType, sortCompareMembers);
            var comparisonID = ComparisonKey.GetHashCode();


            locker.LockAction(()=>CachedComparers.NotContainsKey(comparisonID),
                               () =>
                               {
                                    var sortCompareItem = new DynamicComparer<T>(sortCompareMembers);
                                    CachedComparers.Add(sortCompareItem.ID, sortCompareItem);
                               }
                               );


            return CachedComparers[comparisonID] as DynamicComparer<T>;
            
        }



        /// <summary>
        /// Check sorting members before creating SortComparison. 
        ///   Checking 1 - that each of sortMembers exist in sortingType members.        
        ///            2 - that each of sortMembers.MemberTypes implement interface IComparable
        /// </summary>
        /// <param name="sortingType"></param>
        /// <param name="sortMembers"></param>
        protected  static void CheckSortMembers(Type sortingType, KeyValuePair<string,string>[] sortMembers)
        {
            if (sortMembers.IsNotWorkable())
                throw new InvalidOperationException("[{0}].[{1}] - PARAMETER ERROR:  sorting members can't be null or empty"
                                                    .Fmt(nameof(DynamicComparer<T>), nameof(DynamicComparer<T>.CheckSortMembers)));


            if (!sortingType.IsPublic)
                throw new InvalidOperationException( "Type \"{0}\" is not public.".Fmt( sortingType.FullName) );
            
            foreach (var membr in sortMembers)
            {
                MemberInfo memberInfo = sortingType.GetMemberInfo(membr.Key);                

                if (memberInfo == null)
                    throw new InvalidOperationException( "No public member named \"{0}\" was found.".Fmt(membr.Key));

                var memberType = memberInfo.GetMemberType();
                
                if (!memberType.IsImplementInterface<IComparable>())
                    throw new InvalidOperationException( "The type \"{1}\" of the property \"{0}\" does not implement IComparable."
                                                .Fmt( memberType.Name, memberType.FullName ) );
            }
        }




        /// <summary>
        /// Build SortComparisonKey - unique string Key 
        /// </summary>
        /// <param name="sortingType"></param>
        /// <param name="sortCompareMembers"></param>
        /// <returns></returns>
        protected static string BuildSortComparisonKey(Type sortingType, KeyValuePair<string, string>[] sortCompareMembers)
        {
            var sortComparisonKeyCurrent = sortingType.FullName + "|";

            for (int i = 0; i < sortCompareMembers.Length; i++)
            {
                // add member sorting 
                sortComparisonKeyCurrent += sortCompareMembers[i].Key;

                // add order direction for member 
                if (sortCompareMembers[i].Value.IsNotNullOrEmpty())
                {
                    sortComparisonKeyCurrent += " " + sortCompareMembers[i].Value.ElementAt(i);
                }

                //add delimeter if it's not ending item 
                if ((i + 1) < sortCompareMembers.Length)
                {
                    sortComparisonKeyCurrent += ",";
                }
            }

            return sortComparisonKeyCurrent;
        }


        #region ------------------------------ BUILDING SORT COMPARISON EXPRESSION --------------------------------

        ////
        //// Examples
        //// 
        //public static int Compare(Product xItem, Product yItem) - with Dynamic Method
        //{
        //    int num = 0;
        //    if (num == 0)
        //    {
        //        num = -xItem.get_Id().CompareTo(yItem.get_Id());
        //        if (num == 0)
        //            num = xItem.get_Name().CompareTo(yItem.get_Name());
        //    }
        //    return num;
        //}

        //public static int Compare(Product xItem, Product yItem) - - with Expressions
        //{
        //    int num = 0;
        //    if (num == 0)
        //        num = -xItem.get_Id().CompareTo((object)yItem.get_Id());
        //    if (num == 0)
        //        num = xItem.get_Name().CompareTo((object)yItem.get_Name());
        //    return num;
        //}



        /// <summary>
        /// Generate [int Compare(T x, T y)] operation  code  in Linq.Expression, based on some sortComparison( DynamicComparer{T} instance).
        /// </summary>
        /// <param name="sortComparison"></param>
        /// <returns></returns>
        public static Expression<Comparison<T>> BuildCompareExpression(DynamicComparer<T> sortComparison)
        {
            //we follow to the condition - typeof(T) == sortComparison.SortingType

            var param_X = Expression.Parameter(sortComparison.ComparingType, "xItem");
            var param_Y = Expression.Parameter(sortComparison.ComparingType, "yItem");
            var comparedResult = Expression.Variable(typeof(int), "comparedResult");
            var IfStandartConditionExpr = Expression.Equal(comparedResult, Expression.Constant(0));
                  

            //instructions 
            List<Expression> compareInstructionsBlockExpr = new List<Expression>();
            
            // set comparedResult to 0 
            compareInstructionsBlockExpr.Add( Expression.Assign(comparedResult, Expression.Constant(0)) );

            //foreach of members
            foreach (var compareItem in sortComparison.CompareMembers)
            {
                var member = sortComparison.ComparingType.GetMemberInfo(compareItem.Key);
                var memberType = member.GetMemberType();
                var memberCompareToMethod = memberType.GetMethod("CompareTo", new Type[] { typeof(object) });
              
                
                //get CompareTo
                if (member.IsProperty()) // if member is property
                {
                    if (compareItem.Value == Order.DESC)
                    {
                        compareInstructionsBlockExpr.Add(
                                           Expression.IfThen(
                                            IfStandartConditionExpr
                                          , Expression.Assign(comparedResult,
                                                    
                                                Expression.Negate(         
                                                    Expression.Call(
                                                                  Expression.Property(param_X, member.Name), memberCompareToMethod
                                                                  , Expression.Convert(Expression.Property(param_Y, member.Name), typeof(object)))
                                                                )
                                                             )
                                                        )

                                                   );

                    }
                    else
                    {
                        compareInstructionsBlockExpr.Add(
                                           Expression.IfThen(
                                            IfStandartConditionExpr
                                          , Expression.Assign(comparedResult,
                                                              Expression.Call(
                                                                  Expression.Property(param_X, member.Name), memberCompareToMethod
                                                                  , Expression.Convert(Expression.Property(param_Y, member.Name), typeof(object)))
                                                             )
                                                        )

                                                   );
                    }                 


                }

                if(member.IsField()) // for fields
                {
                    if (compareItem.Value == Order.DESC)
                    {
                        compareInstructionsBlockExpr.Add(
                                           Expression.IfThen(
                                             IfStandartConditionExpr
                                            , Expression.Assign(comparedResult,

                                                Expression.Negate(
                                                    Expression.Call(
                                                                   Expression.Field(param_X, member.Name), memberCompareToMethod
                                                                   , Expression.Convert(Expression.Field(param_Y, member.Name), typeof(object)))
                                                                  )
                                                               )
                                         )
                                      );
                    }
                    else
                    {
                        compareInstructionsBlockExpr.Add(
                                           Expression.IfThen(
                                             IfStandartConditionExpr
                                            , Expression.Assign(comparedResult,
                                                        Expression.Call(
                                                                   Expression.Field(param_X, member.Name), memberCompareToMethod
                                                                   , Expression.Convert(Expression.Field(param_Y, member.Name), typeof(object)))
                                           )
                                         )
                                      );

                    }                       
                }
            }

            //to return int
           
            compareInstructionsBlockExpr.Add(comparedResult);

            var compareBlock = Expression.Block( typeof(int)                                            
                                            ,variables: new[] { comparedResult }
                                            ,expressions: compareInstructionsBlockExpr
                                           );
            
            return Expression.Lambda<Comparison<T>>(compareBlock
                                                       , "Compare_{0}_".Fmt(sortComparison.ID.S())
                                                       , new[] { param_X, param_Y });
            
        }
        
        /// <summary>
        /// Generate [int Compare(T x, T y)] operation  code into ILGenerator, based on some sortComparison( DynamicComparer{T} instance).
        /// </summary>
        /// <param name="ilGen"></param>
        public static void BuildCompareIntoILGenerator(ILGenerator ilGen, DynamicComparer<T> sortComparison)
        {
            #region Generate IL for dynamic method           

            Label lbl = ilGen.DefineLabel(); // Declare and define a label that we can jump to.
            ilGen.DeclareLocal(typeof(int)); // Declare a local variable for storing result.
            Dictionary<Type, LocalBuilder> localVariables = new Dictionary<Type, LocalBuilder>();

            ilGen.Emit(OpCodes.Ldc_I4_0); // Push 0 onto the eval stack.
            ilGen.Emit(OpCodes.Stloc_0); // Store the eval stack item in the local variable @ position 0.

            foreach (var compareItem in sortComparison.CompareMembers) // For each of the properties we want to check inject the following il.
            {
                var member = sortComparison.ComparingType.GetMemberInfo(compareItem.Key);
                var memberType = member.GetMemberType();
                var memberCompareToMethod = memberType.GetMethod("CompareTo", new Type[] { typeof(object) });

                if (member.IsProperty()) // if it's property
                {
                    PropertyInfo propertyInfo = member as PropertyInfo;
                    ilGen.Emit(OpCodes.Ldloc_0); // Load local variable at position 0.
                    ilGen.Emit(OpCodes.Brtrue_S, lbl); // Is the local variable in the evaluation stack equal to 0. If not jump to the label we just defined.
                    ilGen.Emit(OpCodes.Ldarg_0); // Load argument at position 0.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.GetGetMethod(), null); // Get "Name" property value.

                    if (propertyInfo.PropertyType.IsValueType) // If the type is a valuetype then we need to inject the following IL.
                    {
                        if (!localVariables.ContainsKey(propertyInfo.PropertyType)) // Do we have a local variable for this type? If not, add one.
                            localVariables.Add(propertyInfo.PropertyType, ilGen.DeclareLocal(propertyInfo.PropertyType)); // Adds a local variable of type x.

                        int localIndex = localVariables[propertyInfo.PropertyType].LocalIndex; // This local variable is for handling value types of type x.

                        ilGen.Emit(OpCodes.Stloc, localIndex); // Store the value in the local var at position x.
                        ilGen.Emit(OpCodes.Ldloca_S, localIndex); // Load the address of the value into the stack. 
                    }

                    ilGen.Emit(OpCodes.Ldarg_1); // Load argument at position 1.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.GetGetMethod(), null); // Get "propertyName" property value.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.PropertyType.GetMethod("CompareTo", new Type[] { propertyInfo.PropertyType }), null); // Compare the top 2 items in the evaluation stack and push the return value into the eval stack.

                    if (compareItem.Value == Order.DESC) // If the sort should be descending we need to flip the result of the comparison.
                        ilGen.Emit(OpCodes.Neg); // Negates the item in the eval stack.

                    ilGen.Emit(OpCodes.Stloc_0); // Store the result in the local variable.
                }// END - if member is property
                else //if member is field
                {

                    //TO DO

                }// END - if member is field
                               
            }

            ilGen.MarkLabel(lbl); // This is the spot where the label we created earlier should be added.
            ilGen.Emit(OpCodes.Ldloc_0); // Load the local var into the eval stack.
            ilGen.Emit(OpCodes.Ret); // Return the value.
            
            #endregion
        }

        /// <summary>
        /// Generate DynamicMethod with Compare operation.
        /// </summary>
        /// <param name="ilGen"></param>
        public static DynamicMethod BuildCompareDMethod( DynamicComparer<T> sortComparison)
        {
            #region Generate IL for dynamic method           

            var dynCompareMethod = new DynamicMethod("Compare_{0}_".Fmt(sortComparison.ID.S())
                                                    , typeof(int)
                                                    , new Type[] { typeof(object), typeof(object) });
            var ilGen = dynCompareMethod.GetILGenerator();

            Label lbl = ilGen.DefineLabel(); // Declare and define a label that we can jump to.
            ilGen.DeclareLocal(typeof(int)); // Declare a local variable for storing result.
            Dictionary<Type, LocalBuilder> localVariables = new Dictionary<Type, LocalBuilder>();

            ilGen.Emit(OpCodes.Ldc_I4_0); // Push 0 onto the eval stack.
            ilGen.Emit(OpCodes.Stloc_0); // Store the eval stack item in the local variable @ position 0.
                      

            foreach (var compareItem in sortComparison.CompareMembers) // For each of the properties we want to check inject the following il.
            {
                var member = sortComparison.ComparingType.GetMemberInfo(compareItem.Key);
                var memberType = member.GetMemberType();
                var memberCompareToMethod = memberType.GetMethod("CompareTo", new Type[] { typeof(object) });// .a BindingFlags.Instance | BindingFlags.Public);

                if (member.IsProperty()) // if it's property
                {
                    PropertyInfo propertyInfo = member as PropertyInfo;
                    ilGen.Emit(OpCodes.Ldloc_0); // Load local variable at position 0.
                    ilGen.Emit(OpCodes.Brtrue_S, lbl); // Is the local variable in the evaluation stack equal to 0. If not jump to the label we just defined.
                    ilGen.Emit(OpCodes.Ldarg_0); // Load argument at position 0.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.GetGetMethod(), null); // Get "Name" property value.

                    if (propertyInfo.PropertyType.IsValueType) // If the type is a value type then we need to inject the following IL.
                    {
                        if (!localVariables.ContainsKey(propertyInfo.PropertyType)) // Do we have a local variable for this type? If not, add one.
                            localVariables.Add(propertyInfo.PropertyType, ilGen.DeclareLocal(propertyInfo.PropertyType)); // Adds a local variable of type x.

                        int localIndex = localVariables[propertyInfo.PropertyType].LocalIndex; // This local variable is for handling value types of type x.

                        ilGen.Emit(OpCodes.Stloc, localIndex); // Store the value in the local var at position x.
                        ilGen.Emit(OpCodes.Ldloca_S, localIndex); // Load the address of the value into the stack. 
                    }

                    ilGen.Emit(OpCodes.Ldarg_1); // Load argument at position 1.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.GetGetMethod(), null); // Get "propertyName" property value.
                    ilGen.EmitCall(OpCodes.Callvirt, propertyInfo.PropertyType.GetMethod("CompareTo", new Type[] { propertyInfo.PropertyType }), null); // Compare the top 2 items in the evaluation stack and push the return value into the eval stack.

                    if (compareItem.Value == Order.DESC) // If the sort should be descending we need to flip the result of the comparison.
                        ilGen.Emit(OpCodes.Neg); // Negates the item in the eval stack.

                    ilGen.Emit(OpCodes.Stloc_0); // Store the result in the local variable.
                }// END - if member is property
                else //if member is field
                {
                    //TO DO

                }// END - if member is field              
            }

            ilGen.MarkLabel(lbl); // This is the spot where the label we created earlier should be added.
            ilGen.Emit(OpCodes.Ldloc_0); // Load the local var into the eval stack.
            ilGen.Emit(OpCodes.Ret); // Return the value.

            return dynCompareMethod;

            #endregion
        }



        #endregion ------------------------------ BUILDING SORT COMPARISON EXPRESSION --------------------------------


        #region ---------------------------------- COMPARE ----------------------------------

        /// <summary>
        /// Comparing two T-Type items. 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(T x, T y)
        {
            if (x == null && y == null)
                return 0;
            else if (x == null)
                return -1;
            else if (y == null)
                return 1;

            return ComparisonBeginFunc.Invoke(x, y);
        }

        /// <summary>
        /// Comparing two T-Type items. 
        /// X and Y params will be converted as [(T)x, (T)y] - so if x or y type isn't T Type then exception will be thrown.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public int Compare(object x, object y)
        {
            if (x == null && y == null)
                return 0;
            else if (x == null)
                return -1;
            else if (y == null)
                return 1;

            return ComparisonBeginFunc.Invoke((T)x, (T)y);
        }

        #endregion ---------------------------------- COMPARE ----------------------------------
        

    }

   
}



#region -------------------------------------GARBAGE --------------------------------------------




#endregion -------------------------------------GARBAGE --------------------------------------------