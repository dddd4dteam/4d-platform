﻿
using System.Collections.Generic;
using System.Collections.ObjectModel;

using DDDD.Core.Collections.IG;

namespace DDDD.Core.Collections.Format.IG
{
    /// <summary>
	/// A collection of IRule objects which need a chance to gather data during the databinding process.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class FormattingRuleCollection<T> : CollectionBase<T>
    where T : IRule
    {
        public FormattingRuleCollection()
        {
        }

        /// <summary>
        /// Returns a collection of rules which need to be processed at the given stage.
        /// </summary>
        /// <param name="stage"></param>
        /// <returns></returns>
        public ReadOnlyCollection<IRule> GetRulesForStage(EvaluationStage stage)
        {
            List<IRule> rules = new List<IRule>();
            foreach (T item in this.Items)
            {
                IRule rule = item;
                if (rule.RuleExecution != stage)
                {
                    continue;
                }
                rules.Add(rule);
            }
            return new ReadOnlyCollection<IRule>(rules);
        }
    }
}
