﻿using System;

namespace DDDD.Core.Collections.Virtual.DeferredItem
{
	/// <summary>
	/// An object that VirtualCollection DeferItemRequests method returns.
	/// </summary>
	/// <typeparam name="T">The type of elements in the collection</typeparam>
	internal class DeferredItemRequest<T> : IDisposable
	{
		private VirtualCollection<T> _virtualCollection;

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Collections.DeferredItemRequest`1" /> class.
		/// </summary>
		/// <param name="source">The VirtualCollection that creates this instance.</param>
		public DeferredItemRequest(VirtualCollection<T> source)
		{
			this._virtualCollection = source;
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			this._virtualCollection.StopDeferItemRequests();
			this._virtualCollection = null;
			GC.SuppressFinalize(this);
		}
	}
}
