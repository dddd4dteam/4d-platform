﻿
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Media;

using DDDD.Core.Converters;
using DDDD.Core.Data;

namespace DDDD.Core.Collections.Virtual.Utils
{
    /// <summary>
    /// Contains static helper methods.
    /// </summary>
    public class CoreUtilities
    {
        /// <summary>
        /// For internal use
        /// </summary>
        protected CoreUtilities()
        {
        }


        #region  ------------------------ ANTIRECURSION -------------------------

        [ThreadStatic]
        private static CoreUtilities.AntirecursionUtility g_antirecursionUtility;

        internal static CoreUtilities.AntirecursionUtility Antirecursion
        {
            get
            {
                if (CoreUtilities.g_antirecursionUtility == null)
                {
                    CoreUtilities.g_antirecursionUtility = new CoreUtilities.AntirecursionUtility();
                }
                return CoreUtilities.g_antirecursionUtility;
            }
        }

        /// <summary>
        /// Used instead of anti-recursion flag to prevent an action from being taken recursively.
        /// </summary>
        internal class AntirecursionUtility
        {
            private Dictionary<CoreUtilities.AntirecursionUtility.ID, int> _map;

            private Dictionary<CoreUtilities.AntirecursionUtility.ID, int> Map
            {
                get
                {
                    if (this._map == null)
                    {
                        this._map = new Dictionary<CoreUtilities.AntirecursionUtility.ID, int>();
                    }
                    return this._map;
                }
            }

            public AntirecursionUtility()
            {
            }

            public bool Enter(object item, object flagId, bool disallowMultipleEntries)
            {
                int num;
                CoreUtilities.AntirecursionUtility.ID d = new CoreUtilities.AntirecursionUtility.ID(item, flagId);
                if (!this.Map.TryGetValue(d, out num))
                {
                    num = 0;
                }
                else if (disallowMultipleEntries)
                {
                    return false;
                }
                this.Map[d] = 1 + num;
                return true;
            }

            public void Exit(object item, object flagId)
            {
                int num;
                CoreUtilities.AntirecursionUtility.ID d = new CoreUtilities.AntirecursionUtility.ID(item, flagId);
                if (this._map != null && this._map.TryGetValue(d, out num))
                {
                    num--;
                    if (num <= 0)
                    {
                        this._map.Remove(d);
                        return;
                    }
                    this._map[d] = num;
                }
            }

            public bool InProgress(object item, object flagId)
            {
                if (this._map == null)
                {
                    return false;
                }
                return this._map.ContainsKey(new CoreUtilities.AntirecursionUtility.ID(item, flagId));
            }

            private struct ID
            {
                internal object _item;

                internal object _flagId;

                internal ID(object item, object flagId)
                {
                    this._item = item;
                    this._flagId = flagId;
                }

                public override bool Equals(object obj)
                {
                    if (obj is CoreUtilities.AntirecursionUtility.ID)
                    {
                        CoreUtilities.AntirecursionUtility.ID d = (CoreUtilities.AntirecursionUtility.ID)obj;
                        if (d._item == this._item && d._flagId == this._flagId)
                        {
                            return true;
                        }
                    }
                    return false;
                }

                internal static int GetHashCode(object o)
                {
                    if (o == null)
                    {
                        return 0;
                    }
                    return o.GetHashCode();
                }

                public override int GetHashCode()
                {
                    return CoreUtilities.AntirecursionUtility.ID.GetHashCode(this._item) ^ CoreUtilities.AntirecursionUtility.ID.GetHashCode(this._flagId);
                }
            }
        }

        #endregion ------------------------ ANTIRECURSION -------------------------


        [ThreadStatic]
        private static Type lastNonConvertibleDataType;

        [ThreadStatic]
        private static Type _lastInfoObjectType;

        [ThreadStatic]
        private static Type _lastInfoConvertToType;

        [ThreadStatic]
        private static TypeConverterInfo _lastTypeConverterInfo;

      

        internal static bool AreClose(Point pt1, Point pt2)
        {
            if (!CoreUtilities.AreClose(pt1.X, pt2.X))
            {
                return false;
            }
            return CoreUtilities.AreClose(pt1.Y, pt2.Y);
        }

        internal static bool AreClose(Size x, Size y)
        {
            if (!CoreUtilities.AreClose(x.Width, y.Width))
            {
                return false;
            }
            return CoreUtilities.AreClose(x.Height, y.Height);
        }

        internal static bool AreClose(double value1, double value2)
        {
            if (value1 == value2)
            {
                return true;
            }
            return Math.Abs(value1 - value2) < 1E-10;
        }

        internal static bool AreEqual<T>(IList<T> items1, IList<T> items2, IEqualityComparer<T> comparer = null)
        {
            if (items1 == items2)
            {
                return true;
            }
            if (items1 == null || items2 == null)
            {
                return false;
            }
            if (items1.Count != items2.Count)
            {
                return false;
            }
            if (comparer == null)
            {
                comparer = EqualityComparer<T>.Default;
            }
            int num = 0;
            int count = items1.Count;
            while (num < count)
            {
                if (!comparer.Equals(items1[num], items2[num]))
                {
                    return false;
                }
                num++;
            }
            return true;
        }

        internal static int BinarySearch(IList<DateRange> ranges, DateTime date)
        {
            Func<IList<DateRange>, int, DateRange> item = (IList<DateRange> list, int index) => ranges[index];
            return CoreUtilities.BinarySearchDateRange<DateRange>(ranges, item, date, ranges.Count);
        }

        internal static int BinarySearchDateRange<T>(IList<T> rangeProviders, Func<IList<T>, int, DateRange> rangeProviderCallback, DateTime date, int count)
        {
            int num = 0;
            int num1 = count - 1;
            int num2 = 0;
            while (num <= num1)
            {
                num2 = (num + num1) / 2;
                DateRange dateRange = rangeProviderCallback(rangeProviders, num2);
                if (dateRange.Start <= date)
                {
                    if (dateRange.End > date)
                    {
                        return num2;
                    }
                    num = num2 + 1;
                }
                else
                {
                    num1 = num2 - 1;
                }
            }
            return ~num;
        }



        /// <summary>
        /// Helper method for performing a binary search of a sorted list
        /// </summary>
        /// <typeparam name="T">The type of items in the list</typeparam>
        /// <param name="list">The sorted list to be searched</param>
        /// <param name="item">The item to look for</param>
        /// <param name="comparer">The comparer used to perform the search</param>
        /// <param name="ignoreItem">False for a standard binary search. True if the <paramref name="item" /> should 
        /// be ignored in the search. This is useful where the item's sort criteria may have changed to find out where 
        /// the item should be moved to.</param>
        /// <returns>The index where the item in the list. If the item does not exist then it returns the bitwise 
        /// complement. In the case where <paramref name="ignoreItem" /> is true, the index of the item will be returned 
        /// if it exists at the correct location otherwise the bitwise complement of where it should be. Note the complement 
        /// may need to be decremented if doing a move and the item is before the new index.</returns>
        internal static int BinarySearch<T>(IList<T> list, T item, IComparer<T> comparer, bool ignoreItem)
        {
            EqualityComparer<T> @default;
            if (comparer == null)
            {
                comparer = Comparer<T>.Default;
            }
            int num = 0;
            int count = list.Count - 1;
            int num1 = 0;
            if (!ignoreItem)
            {
                @default = null;
            }
            else
            {
                @default = EqualityComparer<T>.Default;
            }
            EqualityComparer<T> equalityComparer = @default;
            while (num <= count)
            {
                num1 = (num + count) / 2;
                T t = list[num1];
                if (equalityComparer != null && equalityComparer.Equals(item, t))
                {
                    if (num1 <= num)
                    {
                        if (num1 >= count)
                        {
                            return num1;
                        }
                        int num2 = num1 + 1;
                        num1 = num2;
                        t = list[num2];
                    }
                    else
                    {
                        int num3 = num1 - 1;
                        num1 = num3;
                        t = list[num3];
                    }
                }
                int num4 = comparer.Compare(t, item);
                if (num4 <= 0)
                {
                    if (num4 >= 0)
                    {
                        return num1;
                    }
                    num = num1 + 1;
                }
                else
                {
                    count = num1 - 1;
                }
            }
            if (equalityComparer != null && num < list.Count - 1 && equalityComparer.Equals(item, list[num]))
            {
                return num;
            }
            return ~num;
        }

        /// <summary>
        /// Helper method for performing a binary search where the search criteria is a separate piece of data
        /// </summary>
        /// <typeparam name="TItem"></typeparam>
        /// <typeparam name="TComparison"></typeparam>
        /// <param name="items"></param>
        /// <param name="comparer"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static int BinarySearch<TItem, TComparison>(IList<TItem> items, Func<TItem, TComparison, int> comparer, TComparison value)
        {
            int num = 0;
            int count = items.Count - 1;
            int num1 = 0;
            while (num <= count)
            {
                num1 = (num + count) / 2;
                int num2 = comparer(items[num1], value);
                if (num2 <= 0)
                {
                    if (num2 >= 0)
                    {
                        return num1;
                    }
                    num = num1 + 1;
                }
                else
                {
                    count = num1 - 1;
                }
            }
            return ~num;
        }

        /// <summary>
        /// Creates a Uri for a resource embedded in an assembly
        /// </summary>
        /// <param name="assembly">The assembly that contains the resource</param>
        /// <param name="resourcePath">The relative path to the resource (e.g. "Looks/Onyx.xaml")</param>
        /// <returns>A Uri to the resource.</returns>
        public static Uri BuildEmbeddedResourceUri(Assembly assembly, string resourcePath)
        {
            if (assembly == null)
            {
                throw new ArgumentNullException("assembly");
            }
            if (resourcePath == null)
            {
                throw new ArgumentNullException("resourcePath");
            }
            string fullName = assembly.FullName;
            fullName = fullName.Substring(0, fullName.IndexOf(','));
            StringBuilder stringBuilder = new StringBuilder(fullName.Length + 15 + resourcePath.Length);
            stringBuilder.Append('/');
            stringBuilder.Append(fullName);
            stringBuilder.Append(";component/");
            stringBuilder.Append(resourcePath);
            return new Uri(stringBuilder.ToString(), UriKind.RelativeOrAbsolute);
        }

        internal static double Clamp(double value, double min, double max)
        {
            if (value < min)
            {
                return min;
            }
            if (value > max)
            {
                return max;
            }
            return value;
        }

        internal static bool Contains<T>(IEnumerable<T> e, T item)
        {
            bool flag;
            IList<T> ts = e as IList<T>;
            if (ts != null)
            {
                return ts.IndexOf(item) >= 0;
            }
            IList lists = e as IList;
            if (lists != null)
            {
                return lists.IndexOf(item) >= 0;
            }
            EqualityComparer<T> @default = EqualityComparer<T>.Default;
            using (IEnumerator<T> enumerator = e.GetEnumerator())
            {
                while (enumerator.MoveNext())
                {
                    if (!@default.Equals(enumerator.Current, item))
                    {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
                return false;
            }
            return flag;
        }

        internal static bool Contains(IEnumerable e, object item)
        {
            bool flag;
            IList lists = e as IList;
            if (lists != null)
            {
                return lists.IndexOf(item) >= 0;
            }
            IEnumerator enumerator = e.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    if (enumerator.Current != item)
                    {
                        continue;
                    }
                    flag = true;
                    return flag;
                }
                return false;
            }
            finally
            {
                IDisposable disposable = enumerator as IDisposable;
                if (disposable != null)
                {
                    disposable.Dispose();
                }
            }
            return flag;
        }

        /// <summary>
        /// Converts 'valueToConvert' to an object of the type 'convertToType'. If it cannot perform the conversion
        /// it returns null. It makes use of any formatting information provided passed in.
        /// </summary>
        /// <returns>Converted value, null if unsuccessful.</returns>
        public static object ConvertDataValue(object valueToConvert, Type convertToType, IFormatProvider formatProvider, string format)
        {
            return CoreUtilities.ConvertDataValue(valueToConvert, convertToType, formatProvider, format, true);
        }

      


        /// <summary>
        /// Converts 'valueToConvert' to an object of the type 'convertToType'. If it cannot perform the conversion
        /// it returns null. It makes use of any formatting information provided passed in.
        /// </summary>
        /// <returns>Converted value, null if unsuccessful.</returns>
        internal static object ConvertDataValue(object valueToConvert, Type convertToType, IFormatProvider formatProvider, string format, bool useTypeConverters)
        {
            object obj1 = (object)null;
            Type underlyingType1 = CoreUtilities.GetUnderlyingType(convertToType);
            if (CoreUtilities.IsNull(valueToConvert))
                obj1 = valueToConvert;
            else if ((object)underlyingType1 != null)
            {
                if ((object)valueToConvert.GetType() == (object)underlyingType1 || underlyingType1.IsInstanceOfType(valueToConvert))
                    return valueToConvert;
                if (obj1 == null)
                {
                     TypeConverterInfo typeConverterInfo = useTypeConverters ? CoreUtilities.GetTypeConverterInfo(valueToConvert.GetType(), convertToType) : ( TypeConverterInfo)null;
                    if (typeConverterInfo != null)
                    {
                        if ((object)typeof(string) == (object)underlyingType1)
                        {
                            try
                            {
                                obj1 = (object)CoreUtilities.ToString(valueToConvert, format, formatProvider, true);
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                        for (int index = 0; index <= 1; ++index)
                        {
                            if (obj1 == null)
                            {
                                if (index == 0)
                                {
                                    if (typeConverterInfo.useConvertFromTypeConverterFirst)
                                        goto label_16;
                                }
                                if (1 == index)
                                {
                                    if (typeConverterInfo.useConvertFromTypeConverterFirst)
                                        goto label_20;
                                }
                                else
                                    goto label_20;
                                label_16:
                                try
                                {
                                    if (typeConverterInfo.convertFromTypeConverter != null)
                                    {
                                        if (typeConverterInfo.convertFromTypeConverter.CanConvertFrom(valueToConvert.GetType()))
                                            obj1 = typeConverterInfo.convertFromTypeConverter.ConvertFrom((ITypeDescriptorContext)null, formatProvider as CultureInfo, valueToConvert);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                            label_20:
                            if (obj1 == null)
                            {
                                if (index == 0)
                                {
                                    if (!typeConverterInfo.useConvertFromTypeConverterFirst)
                                        goto label_25;
                                }
                                if (1 == index)
                                {
                                    if (!typeConverterInfo.useConvertFromTypeConverterFirst)
                                        continue;
                                }
                                else
                                    continue;
                                label_25:
                                try
                                {
                                    if (typeConverterInfo.convertToTypeConverter != null)
                                    {
                                        if (typeConverterInfo.convertToTypeConverter.CanConvertTo(underlyingType1))
                                            obj1 = typeConverterInfo.convertToTypeConverter.ConvertTo((ITypeDescriptorContext)null, formatProvider as CultureInfo, valueToConvert, underlyingType1);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                    }
                }
                if (obj1 == null)
                {
                    if ((object)typeof(Uri) == (object)underlyingType1)
                    {
                        try
                        {
                            obj1 = (object)new Uri(valueToConvert.ToString());
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                if (obj1 == null)
                {
                    if ((object)typeof(string) == (object)underlyingType1)
                    {
                        try
                        {
                            obj1 = (object)CoreUtilities.ToString(valueToConvert, format, formatProvider, false);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                bool isEnum = underlyingType1.IsEnum;
                if (obj1 == null && isEnum)
                {
                    Type underlyingType2 = Enum.GetUnderlyingType(underlyingType1);
                    object obj2 = valueToConvert;
                    if ((object)underlyingType2 != (object)valueToConvert.GetType())
                        obj2 = CoreUtilities.ConvertDataValue(valueToConvert, underlyingType2, formatProvider, format) ?? valueToConvert;
                    try
                    {
                        if (Enum.IsDefined(underlyingType1, obj2))
                        {
                            if (valueToConvert is string)
                            {
                                try
                                {
                                    obj1 = Enum.Parse(underlyingType1, (string)valueToConvert, false);
                                }
                                catch
                                {
                                }
                            }
                            if (obj1 == null)
                                obj1 = Enum.ToObject(underlyingType1, valueToConvert);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
                if (obj1 == null && valueToConvert is IConvertible)
                {
                    string str = valueToConvert as string;
                    if ((object)CoreUtilities.lastNonConvertibleDataType != (object)underlyingType1 || str == null)
                    {
                        if (obj1 == null)
                        {
                            try
                            {
                                obj1 = Convert.ChangeType(valueToConvert, underlyingType1, formatProvider);
                            }
                            catch (Exception ex)
                            {
                                if (str != null)
                                {
                                    if (!typeof(IConvertible).IsAssignableFrom(underlyingType1))
                                        CoreUtilities.lastNonConvertibleDataType = underlyingType1;
                                }
                            }
                        }
                        if (obj1 != null)
                            ;
                    }
                }
                if (obj1 == null)
                {
                    if ((object)typeof(string) == (object)underlyingType1)
                    {
                        try
                        {
                            if (obj1 == null)
                                obj1 = (object)valueToConvert.ToString();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            return obj1;
        }


        internal static NotifyCollectionChangedEventArgs CreateAddRemoveNCCArgs(bool add, IList addRemoveMultiItems, int index)
        {
            NotifyCollectionChangedAction notifyCollectionChangedAction = (add ? NotifyCollectionChangedAction.Add : NotifyCollectionChangedAction.Remove);
            if (1 != addRemoveMultiItems.Count)
            {
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            }
            return new NotifyCollectionChangedEventArgs(notifyCollectionChangedAction, addRemoveMultiItems[0], index);
        }

        /// <summary>
        /// Creates an IComparer that wraps the specified comparison.
        /// </summary>
        /// <typeparam name="T">The type of object being compared</typeparam>
        /// <param name="comparison">The comparison delegate to use when comparing elements.</param>
        /// <returns>A new IComparer&lt;T&gt; that uses the specified <paramref name="comparison" /> to perform the compare.</returns>
        public static IComparer<T> CreateComparer<T>(Comparison<T> comparison)
        {
            return new CoreUtilities.ComparisonComparer<T>(comparison);
        }

        /// <summary>
        /// If the specified comparer is IComparer&lt;T&gt; then returns it otherwise creates an 
        /// IComparer&lt;T&gt; that wraps the specified IComparer.
        /// </summary>
        /// <typeparam name="T">The type of object being compared</typeparam>
        /// <param name="comparer">Non-generic comparer.</param>
        /// <returns>A new IComparer&lt;T&gt; that uses the specified IComparer to perform the comparison.</returns>
        public static IComparer<T> CreateComparer<T>(IComparer comparer)
        {
            return comparer as IComparer<T> ?? (IComparer<T>)new CoreUtilities.ComparerWrapper<T>(comparer);


            //object comparerWrapper = comparer as IComparer<T>;
            //if (comparerWrapper == null)
            //{
            //    comparerWrapper = new CoreUtilities.ComparerWrapper<T>(comparer);                     - [12/14/2016 A1]
            //}
            //return comparerWrapper;
        }

        internal static NotifyCollectionChangedEventArgs CreateReplaceNCCArgs(IList oldItems, IList newItems, int index)
        {
            if (1 != oldItems.Count || 1 != newItems.Count)
            {
                return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
            }
            return new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, newItems[0], oldItems[0], index);
        }

        internal static void DebuggerLog(int level, string category, string message)
        {
            Debugger.Log(level, category, message);
        }

        internal static void EnsureIsANumber(double number)
        {
            if (double.IsNaN(number))
            {
                throw new ArgumentOutOfRangeException("number", "The value cannot be 'double.NaN'.");
            }
        }

        internal static int GetCount(IEnumerable e)
        {
            return CoreUtilities.GetCount(e, false);
        }

       internal static int GetCount(IEnumerable e, bool nonOptimized)
        {
            if (!nonOptimized)
            {
                ICollection collections = e as ICollection;
                if (collections != null)
                {
                    return collections.Count;
                }
            }
            int num = 0;
            if (e != null)
            {
                foreach (object obj in e)
                {
                    num++;
                }
            }
            return num;
        }

       internal static List<T> GetEnumValues<T>()
        where T : struct
        {
            return (
                from f in typeof(T).GetFields(BindingFlags.Static | BindingFlags.Public)
                where f.IsLiteral
                select (T)f.GetValue(null)).ToList<T>();
        }

       internal static T GetFirstItem<T>(IEnumerable<T> e)
        {
            T current;
            T t;
            if (e != null)
            {
                using (IEnumerator<T> enumerator = e.GetEnumerator())
                {
                    if (enumerator.MoveNext())
                    {
                        current = enumerator.Current;
                    }
                    else
                    {
                        t = default(T);
                        return t;
                    }
                }
                return current;
            }
            t = default(T);
            return t;
        }

       internal static T GetFirstItem<T>(IEnumerable e, bool optimized)
        {
            T current;
            T t;
            if (e != null)
            {
                if (optimized)
                {
                    IList<T> ts = e as IList<T>;
                    if (ts != null)
                    {
                        if (ts.Count <= 0)
                        {
                            return default(T);
                        }
                        return ts[0];
                    }
                    IList lists = e as IList;
                    if (lists != null)
                    {
                        if (lists.Count <= 0)
                        {
                            return default(T);
                        }
                        return (T)lists[0];
                    }
                }
                IEnumerator enumerator = e.GetEnumerator();
                try
                {
                    if (enumerator.MoveNext())
                    {
                        current = (T)enumerator.Current;
                    }
                    else
                    {
                        t = default(T);
                        return t;
                    }
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                return current;
            }
            t = default(T);
            return t;
        }

       internal static int GetIndexOf(IEnumerable e, object item)
        {
            int num;
            IList lists = e as IList;
            if (lists != null)
            {
                return lists.IndexOf(item);
            }
            int num1 = 0;
            if (e != null)
            {
                IEnumerator enumerator = e.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        if (enumerator.Current != item)
                        {
                            num1++;
                        }
                        else
                        {
                            num = num1;
                            return num;
                        }
                    }
                    return -1;
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                return num;
            }
            return -1;
        }

       internal static int GetIndexOf<T>(IEnumerable<T> e, T item)
        {
            int num;
            IList<T> ts = e as IList<T>;
            if (ts != null)
            {
                return ts.IndexOf(item);
            }
            IList lists = e as IList;
            if (lists != null)
            {
                return lists.IndexOf(item);
            }
            int num1 = 0;
            if (e != null)
            {
                EqualityComparer<T> @default = EqualityComparer<T>.Default;
                using (IEnumerator<T> enumerator = e.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        if (!@default.Equals(enumerator.Current, item))
                        {
                            num1++;
                        }
                        else
                        {
                            num = num1;
                            return num;
                        }
                    }
                    return -1;
                }
                return num;
            }
            return -1;
        }

       internal static object GetItemAt(IEnumerable e, int index)
        {
            object obj;
            IList lists = e as IList;
            if (lists != null)
            {
                return lists[index];
            }
            if (e != null)
            {
                IEnumerator enumerator = e.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        object current = enumerator.Current;
                        int num = index;
                        index = num - 1;
                        if (num != 0)
                        {
                            continue;
                        }
                        obj = current;
                        return obj;
                    }
                    return null;
                }
                finally
                {
                    IDisposable disposable = enumerator as IDisposable;
                    if (disposable != null)
                    {
                        disposable.Dispose();
                    }
                }
                return obj;
            }
            return null;
        }

       internal static bool GetItemAt<T>(IEnumerable<T> e, int index, out T item)
       {
            bool flag;
            item = default(T);
            IList<T> ts = e as IList<T>;
            if (ts != null)
            {
                if (index >= ts.Count)
                {
                    return false;
                }
                item = ts[index];
                return true;
            }
            IList lists = e as IList;
            if (lists != null)
            {
                if (index >= lists.Count)
                {
                    return false;
                }
                item = (T)lists[index];
                return true;
            }
            if (e != null)
            {
                using (IEnumerator<T> enumerator = e.GetEnumerator())
                {
                    while (enumerator.MoveNext())
                    {
                        T current = enumerator.Current;
                        int num = index;
                        index = num - 1;
                        if (num != 0)
                        {
                            continue;
                        }
                        item = current;
                        flag = true;
                        return flag;
                    }
                    return false;
                }
                return flag;
            }
            return false;
        }

        /// <summary>
        /// Gets items from the list which must be an IList or IList&lt;T&gt;.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">Must be an IList or IList&lt;T&gt;.</param>
        /// <param name="startIndex">Start of the range.</param>
        /// <param name="count">Number of items to return.</param>
        /// <returns>Array of items.</returns>
        internal static T[] GetItemsInRange<T>(IEnumerable list, int startIndex, int count)
        {
            T[] item = new T[count];
            IList<T> ts = list as IList<T>;
            if (ts == null)
            {
                IList lists = list as IList;
                if (lists == null)
                {
                    throw new InvalidOperationException("Source must be an IList<T> or IList.");
                }
                for (int i = 0; i < count; i++)
                {
                    item[i] = (T)lists[startIndex + i];
                }
            }
            else
            {
                for (int j = 0; j < count; j++)
                {
                    item[j] = ts[startIndex + j];
                }
            }
            return item;
        }

        internal static TypeConverterInfo GetTypeConverterInfo(Type objectType, Type convertToType)
        {
            if (CoreUtilities._lastInfoObjectType == objectType && CoreUtilities._lastInfoConvertToType == convertToType)
            {
                return CoreUtilities._lastTypeConverterInfo;
            }
            TypeConverter typeConverter = null;
            TypeConverter typeConverter1 = null;
            if (typeConverter == null && typeConverter1 == null)
            {
                return null;
            }
            bool isPrimitive = objectType.IsPrimitive;
            TypeConverterInfo typeConverterInfo = new TypeConverterInfo(typeConverter1, typeConverter, (objectType == null ? false : !isPrimitive));
            CoreUtilities._lastInfoObjectType = objectType;
            CoreUtilities._lastInfoConvertToType = convertToType;
            CoreUtilities._lastTypeConverterInfo = typeConverterInfo;
            return typeConverterInfo;
        }

      

        /// <summary>
        /// Wraps the 'get' of the Target property in a try/catch to prevent unhandled exceptions
        /// </summary>
        /// <param name="weakReference">The WeakRefernce holding the target.</param>
        /// <returns>The Target or null if an exception was thrown.</returns>
        public static object GetWeakReferenceTargetSafe(WeakReference weakReference)
        {
            object target;
            if (weakReference != null)
            {
                try
                {
                    target = weakReference.Target;
                }
                catch (Exception exception)
                {
                    return null;
                }
                return target;
            }
            return null;
        }

        internal static bool GreaterThan(double x, double y)
        {
            if (x <= y)
            {
                return false;
            }
            return !CoreUtilities.AreClose(x, y);
        }

        internal static bool GreaterThanOrClose(double x, double y)
        {
            if (x >= y)
            {
                return true;
            }
            return CoreUtilities.AreClose(x, y);
        }

        internal static bool HasItems(IEnumerable e)
        {
            bool flag;
            if (e != null)
            {
                if (e is ICollection)
                {
                    return ((ICollection)e).Count > 0;
                }
                if (e != null)
                {
                    IEnumerator enumerator = e.GetEnumerator();
                    try
                    {
                        if (enumerator.MoveNext())
                        {
                            object current = enumerator.Current;
                            flag = true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    finally
                    {
                        IDisposable disposable = enumerator as IDisposable;
                        if (disposable != null)
                        {
                            disposable.Dispose();
                        }
                    }
                    return flag;
                }
            }
            return false;
        }

        internal static bool IntersectsWith(Rect rect1, Rect rect2)
        {
            Rect rect = rect1;
            rect.Intersect(rect2);
            if (rect.IsEmpty)
            {
                return false;
            }
            if (rect.Y == rect1.Bottom || rect.Y == rect2.Bottom)
            {
                return false;
            }
            if (rect.X != rect1.Right && rect.X != rect2.Right)
            {
                return true;
            }
            return false;
        }

        internal static bool IsDebuggerLogging()
        {
            if (Debugger.IsAttached)
            {
                return Debugger.IsLogging();
            }
            return false;
        }

        private static bool IsEnumFlags(Type enumType)
        {
            object[] customAttributes = enumType.GetCustomAttributes(typeof(FlagsAttribute), true);
            if (customAttributes == null)
            {
                return false;
            }
            return (int)customAttributes.Length > 0;
        }

        /// <summary>
        /// Checks if the type is a known type (to Infragistics controls).
        /// </summary>
        internal static bool IsKnownType(Type type)
        {
            if (type == null)
            {
                return false;
            }
            type = CoreUtilities.GetUnderlyingType(type);
            if (!type.IsPrimitive && typeof(decimal) != type && typeof(string) != type && typeof(DateTime) != type && typeof(DayOfWeek) != type && typeof(Color) != type && !typeof(Brush).IsAssignableFrom(type))
            {
                return false;
            }
            return true;
        }



        /// <summary>
        /// Takes a Type and returns the underlying (non-nullable) type, if the Type is nullable. If the specified type is not nullable, then the passed-in type is returned. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Type GetUnderlyingType(Type type)
        {
            if (type == null) { return null; }

            Type underlyingType = Nullable.GetUnderlyingType(type);

            if (underlyingType != null)
            {
                type = underlyingType;
            }
            return type;
        }





        internal static bool IsNull(object val)
        {
            if (val == null)
            {
                return true;
            }
            return DBNull.Value == val;
        }

        /// <summary>
        /// Returns true if the specified val is null, DBNull, empty string, or DependencyProperty.UnsetValue.
        /// </summary>
        /// <param name="val">Value to test</param>
        internal static bool IsValueEmpty(object val)
        {
            if (CoreUtilities.IsNull(val) || string.Empty == val as string)
            {
                return true;
            }
            return DependencyProperty.UnsetValue == val;
        }

        internal static bool LessThan(double x, double y)
        {
            if (x >= y)
            {
                return false;
            }
            return !CoreUtilities.AreClose(x, y);
        }

        internal static bool LessThanOrClose(double x, double y)
        {
            if (x <= y)
            {
                return true;
            }
            return CoreUtilities.AreClose(x, y);
        }

        internal static void Normalize(ref DateTime start, ref DateTime end)
        {
            if (end < start)
            {
                DateTime dateTime = start;
                start = end;
                end = dateTime;
            }
        }

        internal static void RaiseReadOnlyCollectionException()
        {
            throw new NotSupportedException("Collection is read-only.");
        }

        /// <summary>
        /// Reads all the data from the stream starting from its current position.
        /// </summary>
        internal static byte[] ReadData(Stream stream)
        {
            MemoryStream memoryStream = new MemoryStream();
            stream.CopyTo(memoryStream);
            byte[] array = memoryStream.ToArray();
            memoryStream.Dispose();
            return array;
        }

        /// <summary>
        /// Removes all occurrences of itemToRemove from list.
        /// </summary>
        /// <param name="list">List whose items should be removed</param>
        /// <param name="itemToRemove">The value of the items that should be removed</param>
        public static void RemoveAll<T>(List<T> list, T itemToRemove)
        {
            int num = CoreUtilities.RemoveAllHelper<T>(list, itemToRemove);
            list.RemoveRange(num, list.Count - num);
        }

        /// <summary>
        /// Removes all occurrences of itemToRemove from list.
        /// </summary>
        /// <param name="list">List whose items should be removed</param>
        /// <param name="itemToRemove">The value of the items that should be removed</param>
        public static void RemoveAll<T>(IList<T> list, T itemToRemove)
        {
            int num = CoreUtilities.RemoveAllHelper<T>(list, itemToRemove);
            for (int i = list.Count - 1; i >= num; i--)
            {
                list.RemoveAt(i);
            }
        }

        private static int RemoveAllHelper<T>(IList<T> list, T itemToRemove)
        {
            int item = 0;
            int count = list.Count;
            EqualityComparer<T> @default = EqualityComparer<T>.Default;
            for (int i = 0; i < count; i++)
            {
                if (@default.Equals(list[i], itemToRemove))
                {
                    item++;
                }
                else if (item != 0)
                {
                    list[i - item] = list[i];
                }
            }
            return count - item;
        }

        internal static double RoundToIntegralValue(double value)
        {
            if (double.IsNaN(value) || double.IsInfinity(value))
            {
                return value;
            }
            if (value < 0)
            {
                double num = Math.Ceiling(value);
                if (value - num > -0.5)
                {
                    return num;
                }
                return num - 1;
            }
            double num1 = Math.Floor(value);
            if (value - num1 < 0.5)
            {
                return num1;
            }
            return num1 + 1;
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. It requires allocation of an array equal in size to the array to be sorted.
        /// Merge sort should be used if the operation of comparing items is expensive.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <param name="comparer">Object used to compare the items during the sort</param>
        public static void SortMerge(object[] array, IComparer comparer)
        {
            CoreUtilities.SortMerge(array, null, comparer);
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. Optionally you can pass in a temporary array equal (or greater) in size to arr. 
        /// The method will make use of that array instead of allocating one. If null is passed in, 
        /// then it will allocate one. Merge sort should be used if the operation of comparing items 
        /// is expensive.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <param name="tempArray">Null or a temporary array equal (or greater) in size to arr.</param>
        /// <param name="comparer">Object used to compare the items during the sort</param>
        public static void SortMerge(object[] array, object[] tempArray, IComparer comparer)
        {
            CoreUtilities.SortMerge(array, tempArray, comparer, 0, (int)array.Length - 1);
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. Optionally you can pass in a temporary array equal (or greater) in size to arr. 
        /// The method will make use of that array instead of allocating one. If null is passed in, 
        /// then it will allocate one. Merge sort should be used if the operation of comparing items 
        /// is expensive.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <param name="tempArray">Null or a temporary array equal (or greater) in size to arr.</param>
        /// <param name="comparer">Object used to compare the items during the sort</param>
        /// <param name="si">Start index in the array.</param>
        /// <param name="ei">End index in the array.</param>
        public static void SortMerge(object[] array, object[] tempArray, IComparer comparer, int si, int ei)
        {
            if (array == null)
            {
                throw new ArgumentNullException("array");
            }
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }
            if (tempArray != null)
            {
                Array.Copy(array, tempArray, (int)array.Length);
            }
            else
            {
                tempArray = (object[])array.Clone();
            }
            CoreUtilities.SortMergeHelper<object>(array, tempArray, new CoreUtilities.ComparerWrapper<object>(comparer), si, ei);
        }

        /// <summary>
        /// Sorts the passed in list based on the passed in comparer using a modified merge-sort
        /// algorithm. 
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <param name="comparer">The comparer (must not be null).</param>
        public static void SortMergeGeneric<T>(List<T> list, IComparer<T> comparer)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }
            T[] array = list.ToArray();
            CoreUtilities.SortMergeGeneric<T>(array, null, comparer);
            list.Clear();
            list.AddRange(array);
        }

        /// <summary>
        /// Sorts the passed in list based on the passed in comparer using a modified merge-sort
        /// algorithm. 
        /// </summary>
        /// <param name="list">The list to be sorted.</param>
        /// <param name="comparer">The comparer (must not be null).</param>
        /// <param name="startIndex">Start index in the array. Items between the specified start index and end index will be sorted. Other items will be left as they are.</param>
        /// <param name="endIndex">End index in the array. Items between the specified start index and end index will be sorted. Other items will be left as they are.</param>
        public static void SortMergeGeneric<T>(List<T> list, IComparer<T> comparer, int startIndex, int endIndex)
        {
            if (list == null)
            {
                throw new ArgumentNullException("list");
            }
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }
            T[] tArray = new T[1 + endIndex - startIndex];
            list.CopyTo(startIndex, tArray, 0, (int)tArray.Length);
            CoreUtilities.SortMergeGeneric<T>(tArray, null, comparer);
            for (int i = 0; i < (int)tArray.Length; i++)
            {
                list[startIndex + i] = tArray[i];
            }
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. It requires allocation of an array equal in size to the array to be sorted.
        /// Merge sort should be used if the operation of comparing items is expensive.
        /// </summary>
        /// <param name="array">The array to be sorted</param>
        /// <param name="comparer">The comparer to use for the sort</param>
        public static void SortMergeGeneric<T>(T[] array, IComparer<T> comparer)
        {
            CoreUtilities.SortMergeGeneric<T>(array, null, comparer);
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. Optionally you can pass in a temporary array equal (or greater) in size to arr. 
        /// The method will make use of that array instead of allocating one. If null is passed in, 
        /// then it will allocate one. Merge sort should be used if the operation of comparing items 
        /// is expensive.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <param name="tempArray">Null or a temporary array equal (or greater) in size to <paramref name="array" />.</param>
        /// <param name="comparer">Comparer.</param>
        public static void SortMergeGeneric<T>(T[] array, T[] tempArray, IComparer<T> comparer)
        {
            CoreUtilities.SortMergeGeneric<T>(array, tempArray, comparer, 0, (int)array.Length - 1);
        }

        /// <summary>
        /// Sorts the passed in array based on the passed in comparer using a modified merge-sort
        /// algorithm. Optionally you can pass in a temporary array equal (or greater) in size to arr. 
        /// The method will make use of that array instead of allocating one. If null is passed in, 
        /// then it will allocate one. Merge sort should be used if the operation of comparing items 
        /// is expensive.
        /// </summary>
        /// <param name="array">Array to be sorted.</param>
        /// <param name="tempArray">Null or a temporary array equal (or greater) in size to <paramref name="array" />.</param>
        /// <param name="comparer">Comparer.</param>
        /// <param name="startIndex">Start index in the array.</param>
        /// <param name="endIndex">End index in the array.</param>
        public static void SortMergeGeneric<T>(T[] array, T[] tempArray, IComparer<T> comparer, int startIndex, int endIndex)
        {
            if (array == null)
            {
                throw new ArgumentNullException("arr");
            }
            if (comparer == null)
            {
                throw new ArgumentNullException("comparer");
            }
            if (tempArray != null)
            {
                Array.Copy(array, tempArray, (int)array.Length);
            }
            else
            {
                tempArray = (T[])array.Clone();
            }
            CoreUtilities.SortMergeHelper<T>(array, tempArray, comparer, startIndex, endIndex);
        }

        private static void SortMergeHelper<T>(T[] arr, T[] tmpArr, IComparer<T> comparer, int startIndex, int endIndex)
        {
            int i;
            int j;
            T t = default(T);
            T t1 = default(T);
            if (endIndex - startIndex < 6)
            {
                for (i = 1 + startIndex; i <= endIndex; i++)
                {
                    t = arr[i];
                    for (j = i; j > startIndex; j--)
                    {
                        t1 = arr[j - 1];
                        if (comparer.Compare(t, t1) >= 0)
                        {
                            break;
                        }
                        arr[j] = t1;
                    }
                    if (i != j)
                    {
                        arr[j] = t;
                    }
                }
                return;
            }
            int num = (startIndex + endIndex) / 2;
            CoreUtilities.SortMergeHelper<T>(tmpArr, arr, comparer, startIndex, num);
            CoreUtilities.SortMergeHelper<T>(tmpArr, arr, comparer, 1 + num, endIndex);
            i = startIndex;
            j = 1 + num;
            for (int k = startIndex; k <= endIndex; k++)
            {
                if (i <= num)
                {
                    t = tmpArr[i];
                }
                if (j <= endIndex)
                {
                    t1 = tmpArr[j];
                }
                if (j > endIndex || i <= num && comparer.Compare(t, t1) <= 0)
                {
                    arr[k] = t;
                    i++;
                }
                else
                {
                    arr[k] = t1;
                    j++;
                }
            }
        }

        /// <summary>
        /// Swaps values in a list at specified indexes.
        /// </summary>
        /// <typeparam name="T">Type of elements in the list.</typeparam>
        /// <param name="arr">The list to modify</param>
        /// <param name="x">Index location 1</param>
        /// <param name="y">Index location 2</param>
        public static void Swap<T>(IList<T> arr, int x, int y)
        {
            T item = arr[x];
            arr[x] = arr[y];
            arr[y] = item;
        }

        /// <summary>
        /// Swaps the values of the specified 
        /// </summary>
        /// <typeparam name="T">The type of variable to be swapped</typeparam>
        /// <param name="value1">The member to be updated with the value of <paramref name="value2" /></param>
        /// <param name="value2">The member to be updated with the value of <paramref name="value1" /></param>
        internal static void Swap<T>(ref T value1, ref T value2)
        {
            T t = value2;
            value2 = value1;
            value1 = t;
        }

        internal static char ToLower(char c, CultureInfo culture)
        {
            return char.ToLower(c, culture);
        }

        internal static string ToString(object value, string format, IFormatProvider provider, bool ignoreFormatIfEmpty)
        {
            string str = null;
            if (provider != null)
            {
                ICustomFormatter customFormatter = provider.GetFormat(typeof(ICustomFormatter)) as ICustomFormatter;
                if (customFormatter != null)
                {
                    str = customFormatter.Format(format, value, provider);
                }
            }
            if (str == null && value is IFormattable && (!ignoreFormatIfEmpty || !string.IsNullOrEmpty(format)))
            {
                str = ((IFormattable)value).ToString(format, provider);
            }
            return str;
        }

        internal static char ToUpper(char c, CultureInfo culture)
        {
            return char.ToUpper(c, culture);
        }

        /// <summary>
        /// Traverses the enumerator. This may be used to force allocation of lazily allocated list items.
        /// </summary>
        /// <param name="e"></param>
        internal static void Traverse(IEnumerable e)
        {
            foreach (object obj in e)
            {
            }
        }

        internal static void ValidateEnum(Type enumType, object enumVal)
        {
            if (!Enum.IsDefined(enumType, enumVal) && !CoreUtilities.IsEnumFlags(enumType))
            {
                throw new ArgumentException("Invalid enum value");
            }
        }

        internal static void ValidateEnum(string argumentName, Type enumType, object enumVal)
        {
            if (!Enum.IsDefined(enumType, enumVal) && !CoreUtilities.IsEnumFlags(enumType))
            {
                throw new ArgumentException("Invalid enum value");
            }
        }

        internal static void ValidateIsNotInfinity(double value)
        {
            if (double.IsInfinity(value))
            {
                throw new ArgumentException( "ERROR : LE_ValueCannotBeInfinity"); // SR.GetString(  -  [12/14/2016 A1]
            }
        }

        internal static void ValidateIsNotNan(double value)
        {
            if (double.IsNaN(value))
            {
                throw new ArgumentException(" ERROR : LE_ValueCannotBeNan");  // SR.GetString(  -  [12/14/2016 A1]
            }
        }

        internal static void ValidateIsNotNegative(double value)
        {
            if (value < 0)
            {
                throw new ArgumentException("ERROR : LE_ValueCannotBeNegative"); //  SR.GetString(  -  [12/14/2016 A1]
            }
        }

        internal static void ValidateIsNotNegative(double value, string paramName)
        {
            if (value < 0)
            {
                throw new ArgumentException(paramName, "ERROR : LE_ValueCannotBeNegative");// SR.GetString(  -  [12/14/2016 A1]
            }
        }

        internal static void ValidateIsPositive(int value)
        {
            if (value <= 0)
            {
                throw new ArgumentException( "ERROR : LE_ValueMustBePositiveIntegral" ); // SR.GetString  - [12/14/2016 A1]
            }
        }

        internal static void ValidateIsPositive(int value, string paramName)
        {
            if (value <= 0)
            {
                throw new ArgumentException(paramName, "ERROR : LE_ValueMustBePositiveIntegral"); //   //  [12/14/2016 A1]
            }
        }

        internal static void ValidateIsPositive(double value)
        {
            if (value <= 0)
            {
                throw new ArgumentException(  " ERROR : LE_ValueMustBePositive"); //  - [12/14/2016 A1]
            }
        }

        internal static void ValidateIsPositive(double value, string paramName)
        {
            if (value <= 0)
            {
                throw new ArgumentException(paramName,  "ERROR : LE_ValueMustBePositive");//   - [12/14/2016 A1]
            }
        }

        internal static void ValidateNotEmpty(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException();
            }
        }

        internal static void ValidateNotEmpty(string value, string paramName)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new ArgumentException(paramName);
            }
        }

        internal static void ValidateNotNull(object value)
        {
            if (value == null)
            {
                throw new ArgumentNullException();
            }
        }

        internal static void ValidateNotNull(object value, string paramName)
        {
            if (value == null)
            {
                throw new ArgumentNullException(paramName);
            }
        }
  
        private class ComparerWrapper<T> : IComparer<T>
        {
            private IComparer comparer;

            public ComparerWrapper(IComparer comparer)
            {
                this.comparer = comparer;
            }

            public int Compare(T x, T y)
            {
                return this.comparer.Compare(x, y);
            }
        }

        private sealed class ComparisonComparer<T> : IComparer<T>
        {
            private Comparison<T> _c;

            internal ComparisonComparer(Comparison<T> comparison)
            {
                CoreUtilities.ValidateNotNull(comparison, "comparison");
                this._c = comparison;
            }

            public int Compare(T x, T y)
            {
                return this._c(x, y);
            }
        }

        /// <summary>
        /// A comparer that compares converted values.
        /// </summary>
        /// <typeparam name="T">Type of objects being sorted.</typeparam>
        /// <typeparam name="Z">The type of the value of the object that will be compared.</typeparam>
        internal class ConverterComparer<T, Z> : IComparer<T>
        {
            private Converter<T, Z> _converter;

            private IComparer<Z> _comparer;

            public ConverterComparer(Converter<T, Z> converter, Comparison<Z> comparison) : this(converter, CoreUtilities.CreateComparer<Z>(comparison))
            {
            }

            public ConverterComparer(Converter<T, Z> converter, IComparer<Z> comparer)
            {
                this._converter = converter;
                this._comparer = comparer;
            }

            public int Compare(T i, T j)
            {
                return this._comparer.Compare(this._converter(i), this._converter(j));
            }
        }

       
       

       

    }
}
