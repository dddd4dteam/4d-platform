﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Collections.Virtual
{

    /// <summary>
    /// An IEnumerator object that VirtualCollection returns.
    /// </summary>
    /// <typeparam name="T">The type of elements in the VirtualCollection</typeparam>
    internal class VirtualCollectionEnumerator<T> : IEnumerator<T>, IDisposable, IEnumerator
    {

        #region ------------------------- CTOR -----------------------

        public VirtualCollectionEnumerator(VirtualCollection<T> collection)
        {
            this._virtualCollection = collection;
            this._firstIndex = 0;
            this._index = this._firstIndex - 1;
            this._lastIndex = this._index + collection.Count;
        }


        #endregion ------------------------- CTOR -----------------------


        private readonly VirtualCollection<T> _virtualCollection;

        private readonly int _lastIndex;

        private readonly int _firstIndex;

        private int _index;

        private object _current;

        /// <summary>
        /// Gets the current element in the collection.
        /// </summary>
        /// <value></value>
        /// <returns>The current element in the collection.</returns>
        /// <exception cref="T:System.InvalidOperationException">The enumerator is positioned before the first element of the collection or after the last element.-or- The collection was modified after the enumerator was created.</exception>
        public object Current
        {
            get
            {
                if (this._index < this._firstIndex || this._index > this._lastIndex)
                {
                    throw new InvalidOperationException("Call the Enumerator.Reset before using");
                }
                return this._current;
            }
        }

        T IEnumerator<T>.Current
        {
            get
            {
                return (T)this.Current;
            }
        }


        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Advances the enumerator to the next element of the collection.
        /// </summary>
        /// <returns>
        /// true if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.
        /// </returns>
        /// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
        public bool MoveNext()
        {
            if (this._index >= this._lastIndex)
            {
                if (this._index == this._lastIndex)
                {
                    VirtualCollectionEnumerator<T> virtualCollectionEnumerator = this;
                    virtualCollectionEnumerator._index = virtualCollectionEnumerator._index + 1;
                }
                return false;
            }
            VirtualCollectionEnumerator<T> virtualCollectionEnumerator1 = this;
            virtualCollectionEnumerator1._index = virtualCollectionEnumerator1._index + 1;
            this._current = this._virtualCollection[this._index];
            return true;
        }

        /// <summary>
        /// Sets the enumerator to its initial position, which is before the first element in the collection.
        /// </summary>
        /// <exception cref="T:System.InvalidOperationException">The collection was modified after the enumerator was created. </exception>
        public void Reset()
        {
            this._index = -1;
        }
    }
}
