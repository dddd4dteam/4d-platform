﻿using System.Globalization;

namespace DDDD.Core.Collections.Virtual.Requests
{

    /// <summary>
    /// A class used to describe data requested by the VirtualCollection
    /// </summary>
    internal class RequestedDataDescription
    {
        /// <summary>
        /// Gets the key that is used to access this data request.
        /// </summary>
        public string Key
        {
            get
            {
                return this.StartIndex.ToString(CultureInfo.InvariantCulture);
            }
        }

        /// <summary>
        /// Gets the index of the page that is requested.
        /// </summary>
        public int PageIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the number of requested items.
        /// </summary>
        public int Size
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the index of first element that is requested.
        /// </summary>
        public int StartIndex
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.RequestedDataDescription" /> class.
        /// </summary>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="startIndex">The start index.</param>
        /// <param name="size">The number of requested items.</param>
        public RequestedDataDescription(int pageIndex, int startIndex, int size)
        {
            this.StartIndex = startIndex;
            this.Size = size;
            this.PageIndex = pageIndex;
        }
    }
}
