﻿using System.Collections.Generic;

namespace DDDD.Core.Collections.Concurrent
{

    public interface IKV<T, V>
    {
        bool TryGetValue(T key, out V val);
        int Count();
        IEnumerator<KeyValuePair<T, V>> GetEnumerator();
        void Add(T key, V value);
        T[] Keys();
        bool ContainsKey(T key);
        bool Remove(T key);
        void Clear();
        V GetValue(T key);
        // safesortedlist only
        //V GetValue(int index);
        //T GetKey(int index);
    }


    /// <summary>
    /// Safe Dictionary - it use Sync locking on each operation.
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    public class SafeDictionary<TKey, TValue> : IKV<TKey, TValue>
    {
        #region ------------------------- CTOR -----------------------

        public SafeDictionary(int capacity)
        {
            _Dictionary = new Dictionary<TKey, TValue>(capacity);
        }

        public SafeDictionary()
        {
            _Dictionary = new Dictionary<TKey, TValue>();
        }

        #endregion ------------------------- CTOR -----------------------


        private readonly object syncRoot = new object();
        private readonly Dictionary<TKey, TValue> _Dictionary;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue this[TKey key]
        {
            get
            {
                lock (syncRoot)
                    return _Dictionary[key];
            }
            set
            {
                lock (syncRoot)
                    _Dictionary[key] = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            lock (syncRoot) return _Dictionary.Count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool TryGetValue(TKey key, out TValue value)
        {
            lock (syncRoot)
                return _Dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return ((ICollection<KeyValuePair<TKey, TValue>>)_Dictionary).GetEnumerator();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(TKey key, TValue value)
        {
            lock (syncRoot)
            {
                if (_Dictionary.ContainsKey(key) == false)
                    _Dictionary.Add(key, value);
                else
                    _Dictionary[key] = value;
            }
        }

        /// <summary>
        /// Keys 
        /// </summary>
        /// <returns></returns>
        public TKey[] Keys()
        {
            lock (syncRoot)
            {
                TKey[] keys = new TKey[_Dictionary.Keys.Count];
                _Dictionary.Keys.CopyTo(keys, 0);
                return keys;
            }
        }

        /// <summary>
        /// Remove item by TKey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(TKey key)
        {
            if (key == null)   return true;

            lock (syncRoot)
            {
                return _Dictionary.Remove(key);
            }
        }

        /// <summary>
        /// Clear Dictionary
        /// </summary>
        public void Clear()
        {
            lock (syncRoot)
                _Dictionary.Clear();
        }

        /// <summary>
        /// Contains Key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            lock (syncRoot) return _Dictionary.ContainsKey(key);
        }


        /// <summary>
        /// Contains Value
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool ContainsValue(TValue value)
        {
            lock (syncRoot) return _Dictionary.ContainsValue(value);
        }


        /// <summary>
        /// Get Value by TKey
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue GetValue(TKey key)
        {
            lock (syncRoot)
                return _Dictionary[key];
        }
    }



}
