﻿namespace DDDD.Core.Collections.Merging.IG
{
    /// <summary>
    /// Custom deletage used to pass information in the MergedDataContext.
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <typeparam name="T3"></typeparam>
    /// <typeparam name="T4"></typeparam>
    /// <typeparam name="T5"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="in1"></param>
    /// <param name="in2"></param>
    /// <param name="in3"></param>
    /// <param name="in4"></param>
    /// <param name="in5"></param>
    /// <returns></returns>
    public delegate TResult MergeDelegate<T1, T2, T3, T4, T5, TResult>(T1 in1, T2 in2, T3 in3, T4 in4, T5 in5);
}
