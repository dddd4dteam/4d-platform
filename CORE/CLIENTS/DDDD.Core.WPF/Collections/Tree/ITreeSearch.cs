﻿using DDDD.Core.Collections.Identification;
using System;
using System.Collections.Generic;

namespace DDDD.Core.Collections.Tree
{


     



    /// <summary>
    /// Interface for ITreeSearch
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    public interface ITreeSearch<TKey>  //<T> where T : NodeM, new()
    {
        IDCounter<TKey> IDCounter { get; }


        Node<TKey> Root { get; }


        Node<TKey> AddRoot(params object[] args);


        Node<TKey> AddNodeToParent(Node<TKey> parentNode, object[] args);


        bool ContainsNode(TKey id);

        List<Node<TKey>> GetAllSiblings(Node<TKey> fromNode);

        List<Node<TKey>> GetAllSiblingsAndSelf(Node<TKey> fromNode);

        List<Node<TKey>> GetAncestors(Node<TKey> fromNode);

        List<Node<TKey>> GetAncestorsAndSelf(Node<TKey> fromNode);

        List<Node<TKey>> GetDescendents(Node<TKey> fromNode);

        List<Node<TKey>> GetDescendentsAndSelf(Node<TKey> fromNode);

        Node<TKey> GetFirstLeftSibling(Node<TKey> fromNode);

        Node<TKey> GetFirstRightSibling(Node<TKey> fromNode);

       

        List<Node<TKey>> GetNodesOnLevel(int deepLevel);

        List<Node<TKey>> GetSiblingsOnLeft(Node<TKey> fromNode);

        List<Node<TKey>> GetSiblingsOnRight(Node<TKey> fromNode);

        bool NotContainsNode(TKey id);

        void RemoveNode(Node<TKey> removeNode);

        void RemoveNode(TKey removeNodeId);



        Node<TKey> GetNode(Func<Node<TKey>, bool> nodeContentSelector);
        Node<TKey> GetNode(TKey nodeID);

        List<Node<TKey>> SearchFor(Func<Node<TKey>, bool> nodeContentSelector);


    }




}