﻿namespace DDDD.Core.Collections
{

    public class StringNode : NamedNode<StringNode, string>
    {
        public new string Value
        {
            get { return base.Value; }
            set { base.Value = value; }
        }

        public StringNode(string node) : base(node) { }

        public StringNode AddFirst(string value)
        {
            return AddFirst(new StringNode(value));
        }

        public StringNode AddLast(string value)
        {
            return AddLast(new StringNode(value));
        }

        public StringNode AddNext(string value)
        {
            return AddNext(new StringNode(value));
        }

        public StringNode AddPrevious(string value)
        {
            return AddPrevious(new StringNode(value));
        }

    }

}
