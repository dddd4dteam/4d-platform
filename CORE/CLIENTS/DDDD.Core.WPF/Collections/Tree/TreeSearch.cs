﻿using System;
using System.Linq;
using System.Collections.Generic;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.Diagnostics;
using DDDD.Core.Collections.Identification;

namespace DDDD.Core.Collections.Tree
{


    /// <summary>
    /// Node - Element class for TreeSearch class. 
    /// </summary>  
    public class Node<TKey>
    {

        #region ---------------------------- CTOR / CREATE ---------------------------------
        public Node()
        { 
        } 



        /// <summary>
        /// Create new Node instance of T,where T:NodeM, with args that T needs. 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="hostTree"></param>
        /// <param name="args"></param>
        /// <param name="xIndex"></param>
        /// <param name="yIndex"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public static T Create<T>(ITreeSearch<TKey> hostTree , object[] args, int xIndex, int yIndex, Node<TKey> parent = null)
            where T: Node<TKey>
        {
            var newNode = TypeActivator.CreateInstanceT<T>(TypeActivator.DefaultCtorSearchBinding, args); // new T();
            newNode.HostTree = hostTree;
            newNode.XIndex = xIndex;
            newNode.YIndex = yIndex;

            //set parent and add this node to its childs
            newNode.Parent = parent;
            if (  parent.IsNotNull()  ) //parent Exists
            {
                newNode.ChildIndex = parent.Childs.Count;
                parent.Childs.Add(newNode);
            }
            else
            {
                newNode.ChildIndex = -1;  //no parent 
            } 
            return newNode;
        }

        /// <summary>
        /// Some node Initing  after it was created. 
        /// </summary>
        public virtual void Init()
        {
            // init ID in derived class
            ID = HostTree.IDCounter.GetNext();

        }

        #endregion ---------------------------- CTOR / CREATE ---------------------------------


        #region  ----------------------------- IDENTIFICATION ---------------------------------


        /// <summary>
        ///ID - NodeM Identificator. 
        /// </summary>
        public TKey ID { get; protected set; } 

        //public Guid IDguid { get; } = Guid.NewGuid();

        /// <summary>
        /// Host TreeSearch - tree who this node belongs. 
        /// </summary>
        public ITreeSearch<TKey> HostTree { get; protected set; }
         


        #endregion ----------------------------- IDENTIFICATION ---------------------------------


        #region ------------------------- COORDS X, Y ----------------------------

        /// <summary>
        /// Depth or YIndex. 0 -is Root element index.
        /// </summary>
        public int  YIndex { get; set; }

        /// <summary>
        /// Depth or XIndex. 0 -is Root element index.
        /// </summary>
        public int XIndex { get; set; }

        #endregion ------------------------- COORDS ----------------------------


        #region -------------------ROOT/ PARENT/ CHILDS -------------------------

        /// <summary>
        /// Is This Node -Root Node - with coords (X=0;Y=0)
        /// </summary>
        public bool IsRootNode
        {
            get { return (XIndex == 0 && YIndex == 0); }
        }

        /// <summary>
        /// Parent Node for current Node. 
        /// </summary>
        public Node<TKey>  Parent { get; private set; }

        /// <summary>
        /// Parent Node ID.
        /// </summary>
        public TKey ParentID
        {   get{ return Parent.ID; }   }


        /// <summary>
        /// Node Child Elements.
        /// </summary>
        public List<Node<TKey>> Childs
        { get; private set; } = new List<Node<TKey>>();

        /// <summary>
        /// Node Child index
        /// </summary>
        public int ChildIndex
        { get; private set; }


        #endregion ------------------- ROOT/ PARENT/ CHILDS -------------------------




        internal T AddChild<T>( int xIndex, object[] args) where T : Node<TKey> 
        {
            var newNode = Create<T>(HostTree, args,  xIndex, (YIndex+1), this);
            newNode.Init();
            return newNode;
        }



        #region ----------------------------- GETHASCODE/ EQUALS / == / != ---------------------------------

        //public override int GetHashCode()
        //{
        //    var rand = new Random(100000000);
        //    var xAddon = rand.Next(100000000);
        //    var yAddon = rand.Next(100000000);
        //    if (Parent.IsNotNull())
        //    {
        //        return (XIndex+xAddon)  ^ (YIndex+yAddon) ^ Parent.ID;
        //    }
        //    return (XIndex + xAddon) ^ (YIndex + yAddon) ^ 37924;
        //}

        public override bool Equals(object obj)
        {
            // If parameter is null return false.
            if (obj.IsNull() || (obj as Node<TKey>).IsNull() ) return false;
              
            // If parameter cannot be cast to Point return false.
            Node<TKey> p = obj as Node<TKey>;

            //(ID == p.ID) &&
            // Return true if the fields match:
            return (XIndex == p.XIndex) && (YIndex == p.YIndex) &&  IsRootNode == p.IsRootNode && (Parent.IsNotNull() == p.Parent.IsNotNull()); //&& IDguid == p.IDguid
        }

        public bool Equals(Node<TKey> p)
        {
            // If parameter is null return false:
            if (p.IsNull()) { return false; }

            //(ID == p.ID) &&
            // Return true if the fields match:
            return (XIndex == p.XIndex) && (YIndex == p.YIndex) && IsRootNode == p.IsRootNode && (Parent.IsNotNull() == p.Parent.IsNotNull()); //&& IDguid == p.IDguid
        }

        public static bool operator ==(Node<TKey> a, Node<TKey> b)
        {
            // If both are null, or both are same instance, return true.
            if (ReferenceEquals(a, b)) return true;

            // If one is null, but not both, return false.
            if (a.IsNull() || b.IsNull()) return false;

            // Return true if the fields match:
            return a.Equals(b); // ID == b.ID
        }

        public static bool operator !=(Node<TKey> a, Node<TKey> b)
        {
            return !(a == b);
        }

        #endregion ---------------------------- GETHASCODE/ EQUALS / == / != ---------------------------------


        #region --------------------------- TRAVERSING METHDS --------------------------------

        /// <summary>
        /// Get current Node Ancestors
        /// </summary>
        /// <returns></returns>
        public List<Node<TKey>> GetAncestors()
        {
            return HostTree.GetAncestors(this);
        }

        /// <summary>
        /// Get current Node Descendents
        /// </summary>
        /// <returns></returns>
        public List<Node<TKey>> GetDescendents()
        {
            return HostTree.GetDescendents(this);
        }

        /// <summary>
        ///  Get First Left Sibling of current Node
        /// </summary>
        /// <returns></returns>
        public Node<TKey> GetFirstLeftSibling()
        {           
           return HostTree.GetFirstLeftSibling(this);
        }

        /// <summary>
        /// Get First Right Sibling of current Node
        /// </summary>
        /// <returns></returns>
        public Node<TKey> GetFirstRightSibling()
        {
            return HostTree.GetFirstRightSibling(this);
        }

        /// <summary>
        /// Get all Siblings On Left side from current Node
        /// </summary>
        /// <returns></returns>
        public List<Node<TKey>> GetSiblingsOnLeft()
        {
            return HostTree.GetSiblingsOnLeft(this);
        }

        /// <summary>
        /// Get all Siblings On Right side from current Node
        /// </summary>
        /// <returns></returns>
        public List<Node<TKey>> GetSiblingsOnRight()
        {
            return HostTree.GetSiblingsOnRight(this);
        }

        /// <summary>
        /// Get all Siblings of current Node
        /// </summary>
        /// <returns></returns>
        public List<Node<TKey>> GetAllSiblings( )
        {
             return HostTree.GetAllSiblings(this);
        }


        #endregion --------------------------- TRAVERSING METHODS --------------------------------




    }




    /// <summary>
    /// TreeSearch has  - 2 dimensional node matrix collection, and plain collection of all elements - on internal storing plan. 
    /// <para/> So it has standart tree traversing methods, 
    /// and also we can quickly search linearly without recursion with the help of plain collection. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TreeSearch<TKey,T> : ITreeSearch<TKey> 
        where T: Node<TKey>
    {

        #region --------------------------- CTOR -----------------------------
        /// <summary>
        /// TreeSearch - tree  and linear collection - to simplyfy search, in one structure.
        /// </summary>
        public TreeSearch()
        {
            
        }

        #endregion --------------------------- CTOR -----------------------------

        private static readonly object syncRoot = new object();

        #region --------------------- Store COLECTIONS Elements/ NodesMatrix/  EmptyCells ---------------------

        /// [1- 65535] - Deep- max Deep Level; 0 - Root Deep Level; 
        /// Each Level has its own List of Nodes
        /// Each node know its : Parent

        readonly Dictionary<TKey, Node<TKey>> Elements = new Dictionary<TKey, Node<TKey>>();
        readonly Dictionary<int, List<Node<TKey>>> NodesMatrix = new Dictionary<int, List<Node<TKey>>>();
        readonly Dictionary<int, List<int>> EmptyCells = new Dictionary<int, List<int>>(); // yindex, List<> -xIndexes that is empty now.

        #endregion --------------------- Store COLECTIONS Elements/ NodesMatrix/  EmptyCells ---------------------


        /// <summary>
        ///  Node ID generator -each node has ID that was not presented earlier in tree.
        /// </summary>
        public IDCounter<TKey> IDCounter
        { get; private set; }

        /// <summary>
        /// Set Node ID generator 
        /// </summary>
        /// <param name="treeIDCounter"></param>
        public void SetIdentityCounter(IDCounter<TKey>  treeIDCounter)
        {
            IDCounter = treeIDCounter;
        }


        #region     ---------------------------- ROOT NODE -------------------------------

        /// <summary>
        /// Root node always creating in new TreeSearch() ctor call.
        /// <para/> Root node Deep level-Yindex - is 0.
        /// </summary>
        public Node<TKey> Root
        {
            get; private set;           
        }
        
        /// <summary>
        ///  Craete and Add Root Node.
        /// </summary>
        /// <param name="args"></param>
        public Node<TKey> AddRoot(params object[] args )
        {
           Validator.ATInvalidOperationDbg(Root.IsNotNull(), nameof(TreeSearch<TKey,T>), nameof(AddRoot), "Root can't be inited twice- you should recreate you FileStore to get other Root Folder.");  
           Root = Node<TKey>.Create<T>(this, args, 0, 0); //no Parent Node attaching
           Root.Init();
           SaveNodeInTree(Root);
            return Root;
        }


        #endregion ---------------------------- ROOT NODE --------------------------------


        #region -------------------------- ADDNODE/ GETNODE /REMOVENODE/ CONTAINSNODE ----------------------------

 

        /// <summary>
        /// Add new Node with newData value to the child collection of parentNode.
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="newData"></param>
        public Node<TKey> AddNodeToParent(Node<TKey> parentNode, params object[] args)
        {
            if (parentNode.IsNull()) throw new InvalidOperationException("[parentNode] arg can't be null");
             
            var yIndex =    parentNode.YIndex+1;
            var xIndex = GetXIndexForNewNode(yIndex); 

            var newNode =  parentNode.AddChild<T>( xIndex, args);//Create newNode, Init newNode 
            SaveNodeInTree(newNode);
            
            return newNode;
        }


        void SaveNodeInTree(Node<TKey> newNode)
        {
            //saving in tree matrix and plain collection
            Elements.Add(newNode.ID, newNode);

            if (NodesMatrix.NotContainsKey(newNode.YIndex)) NodesMatrix.Add(newNode.YIndex, new List<Node<TKey>>());
            if (NodesMatrix[newNode.YIndex].Count < newNode.XIndex)
                NodesMatrix[newNode.YIndex][newNode.XIndex] = newNode;
            else NodesMatrix[newNode.YIndex].Add(newNode);

        }






        int GetXIndexForNewNode(int yIndex)
        {
            int xIndex = 0;
            //1 try- add to empty cell
            if (EmptyCells.ContainsKey(yIndex) && EmptyCells[yIndex].Count > 0)
            {   //get first empty cell- to place new Node there
                xIndex = EmptyCells[yIndex][0];
                EmptyCells[yIndex].RemoveAt(0);
            }
            else
            { // 2 - simply new Item on ylevel
                if (NodesMatrix.NotContainsKey(yIndex)) NodesMatrix.Add(yIndex, new List<Node<TKey>>());
                xIndex = NodesMatrix[yIndex].Count;
            }
            return xIndex;
        }


       


        /// <summary>
        /// Remove some Node by some ID
        /// </summary>
        /// <param name="removeNodeId"></param>
        public void RemoveNode(TKey  removeNodeId)
        {
            if ( NotContainsNode(removeNodeId)) return;
             
            var removingElement = Elements[removeNodeId];

            //remove from parent Node if it exists  in removingNode
            if (removingElement.Parent.IsNotNull())
            { removingElement.Parent.Childs.RemoveAt(removingElement.ChildIndex);
            }

            //remove from 2d Node-Matrix
            NodesMatrix[removingElement.YIndex][removingElement.XIndex] = null;

            //EmptyCells -register new Empty cell
            if (EmptyCells.NotContainsKey(removingElement.YIndex) ) EmptyCells.Add(removingElement.YIndex, new List<int>());
            EmptyCells[removingElement.YIndex].Add(removingElement.XIndex);

            //plain collection removing
            Elements.Remove(removeNodeId);  
        }




        /// <summary>
        /// Remove some Node
        /// </summary>
        /// <param name="removeNode"></param>
        public void RemoveNode(Node<TKey> removeNode)
        {
            RemoveNode(removeNode.ID);
        }

        /// <summary>
        /// Contains Node by its ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool ContainsNode(TKey id) => Elements.ContainsKey(id);

        /// <summary>
        /// We want sure that Element with such id doesn't contain in tree.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool NotContainsNode(TKey id) { return !ContainsNode(id);  }

        #endregion -------------------------- ADDNODE/ GETNODE /REMOVENODE/ CONTAINSNODE ----------------------------


        #region --------------------------- TRAVERSING METHDS --------------------------------

        /// <summary>
        /// Get Node ancestors
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetAncestors(Node<TKey> fromNode)
        {
            var ancestors = new List<Node<TKey>>();
             var previosAnc = fromNode.Parent ;
            for (int i = fromNode.YIndex-1; i >= 0 ; i--)
            {
                ancestors.Add(previosAnc);
                previosAnc = previosAnc.Parent;
                if (previosAnc.Parent.IsRootNode) break; 
            }  

            return fromNode.GetAncestors();
        }


        /// <summary>
        /// Get Node Ancestors + itself
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetAncestorsAndSelf(Node<TKey> fromNode)
        {
            var result = GetAncestors(fromNode);
            result.Add(fromNode);
            return result;
        }

        /// <summary>
        /// Get Node Descendents
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public  List<Node<TKey>> GetDescendents(Node<TKey> fromNode)
        {
            return fromNode.GetDescendents();
        }

        /// <summary>
        /// Get Node Descendents + itself
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetDescendentsAndSelf(Node<TKey> fromNode)
        {
            var result = fromNode.GetDescendents();
            result.Add(fromNode);
            return result;
        }
        

        /// <summary>
        /// Get All Siblings for current Node
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetAllSiblings(Node<TKey> fromNode)
        {
            if (fromNode.Parent.IsNull()) return new List<Node<TKey>>();
            
            var result = fromNode.Parent.Childs;
            result.RemoveAt(fromNode.ChildIndex);
            return result; 
        }


        /// <summary>
        /// Get All Siblings + itself 
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetAllSiblingsAndSelf(Node<TKey> fromNode)
        {
            if (fromNode.Parent.IsNull()) return new List<Node<TKey>>();
            return fromNode.Parent.Childs; 
        }

        /// <summary>
        /// Get  First Left Sibling  for current Node
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public Node<TKey> GetFirstLeftSibling(Node<TKey> fromNode)
        {
            if (fromNode.Parent.IsNull()) return null;
            if (fromNode.ChildIndex == 0) return null;

            var leftindex = fromNode.ChildIndex - 1;

            return fromNode.Parent.Childs[leftindex];
            
        }

        /// <summary>
        /// Get  First Right Sibling  for current Node
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public Node<TKey> GetFirstRightSibling(Node<TKey> fromNode)
        {
            if (fromNode.Parent.IsNull()) return null;
            if (fromNode.ChildIndex == (fromNode.Parent.Childs.Count -1) ) return null;
            var rightIndex = fromNode.ChildIndex + 1;

            return fromNode.Parent.Childs[rightIndex]; 
        }


        /// <summary>
        /// Get all Siblings on Left for current Node
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetSiblingsOnLeft(Node<TKey> fromNode)
        {
            var result = new List<Node<TKey>>();
            if (fromNode.Parent.IsNull()) return result;
            if (fromNode.ChildIndex == 0) return result;

            for (int i = 0; i < fromNode.ChildIndex; i++)
            {
                result.Add(fromNode.Parent.Childs[i]);
            }

            return result;

        }

        /// <summary>
        /// Get all Siblings on Right for current Node
        /// </summary>
        /// <param name="fromNode"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetSiblingsOnRight(Node<TKey> fromNode)
        {

            var result = new List<Node<TKey>>();
            if (fromNode.Parent.IsNull()) return result;
            if (fromNode.ChildIndex == fromNode.Parent.Childs.Count-1) return result;

            for (int i = (fromNode.ChildIndex+1); i < fromNode.Parent.Childs.Count; i++)
            {
                result.Add(fromNode.Parent.Childs[i]);
            }

            return result;
        }

        /// <summary>
        /// Get all nodes on some Deep Level-Yindex.
        /// </summary>
        /// <param name="deepLevel"></param>
        /// <returns></returns>
        public List<Node<TKey>> GetNodesOnLevel(int deepLevel)
        {
            return NodesMatrix[deepLevel];
        }

        internal void ClearChilds(Node<TKey> parentNode)
        {
            if (parentNode.IsNull() || parentNode.Childs.Count == 0) return;


            for (int i = 0; i < parentNode.Childs.Count; i++)
            {
                if (parentNode.Childs[i].Childs.Count> 0)
                { ClearChilds(parentNode.Childs[i]);       }

                RemoveNode(parentNode.Childs[i].ID);
            }  
        }



        /// <summary>
        /// Get first successfully matched node by such nodeContentSelector.  
        /// </summary>
        /// <param name="nodeContentSelector"></param>
        /// <returns></returns>
        public Node<TKey> GetNode(Func<Node<TKey>, bool> nodeContentSelector)
        {
            foreach (var item in Elements)
            {
                if (nodeContentSelector(item.Value)) return item.Value;
            }
            return null;//if all search nodes was false 
        }



        /// <summary>
        /// Get Node by its ID.
        /// </summary>
        /// <param name="nodeID"></param>
        /// <returns></returns>
        public Node<TKey> GetNode(TKey nodeID)
        {
            if (Elements.ContainsKey(nodeID)) return Elements[nodeID];

            return null;
        }


        /// <summary>
        /// Search For one or Several nodes that match with your nodeContentSelector predicate.
        /// </summary>
        /// <param name="nodeContentSelector"></param>
        /// <returns></returns>
        public List<Node<TKey>> SearchFor(Func<Node<TKey>, bool> nodeContentSelector)
        {

            var result = new List<Node<TKey>>();
     
            foreach (var item in Elements)
            {
                if (nodeContentSelector(item.Value))   result.Add(item.Value);
            }

            return result;

        }

        #endregion --------------------------- TRAVERSING METHODS --------------------------------


    }

}


#region ------------------------------ GARBAGE ------------------------------------

//public void SaveNodeInIDBasedCollection(NodeM targetNode, int lastNodeID)
//{

//    if (Elements.ContainsKey(lastNodeID)) Elements.Remove(lastNodeID);             
//    Elements.Add(targetNode.ID , targetNode);

//}


//SaveRootNode(); //attaching into tree matrix  and plain collections
//NodeM SaveRootNode()
//{
//    Elements.Add(Root.ID, Root);

//    if (NodesMatrix.NotContainsKey(Root.YIndex)) NodesMatrix.Add(Root.YIndex, new List<NodeM>());
//    if (NodesMatrix[Root.YIndex].Count < Root.XIndex)
//        NodesMatrix[Root.YIndex][Root.XIndex] = Root;
//    else NodesMatrix[Root.YIndex].Add(Root);
//    return Root;
//}



/// <summary>
/// Update Root node  Data by rootdataItem.
/// </summary>
/// <param name="rootData"></param>
//public void UpdateRoot(T rootdataItem)
//{
//    //Root.Data = rootdataItem;
//}

/// <summary>
/// Custom Content Data 
/// </summary>
//public T Data { get; internal set; }



#endregion ------------------------------ GARBAGE ------------------------------------
