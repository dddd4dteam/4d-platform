﻿using System;

namespace  DDDD.Core.Collections.IG
{

    /// <summary>
    /// An <see cref="T:System.Exception" /> that is thrown when the GetEnumerator method of the IEnumerable returns null for the <see cref="T: DataManagerBase" />.
    /// </summary>
    public class InvalidEnumeratorException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T: InvalidEnumeratorException" /> class.
        /// </summary>
        public InvalidEnumeratorException()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: InvalidEnumeratorException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        public InvalidEnumeratorException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: InvalidEnumeratorException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        /// <param name="innerException">An inner exception.</param>
        public InvalidEnumeratorException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }


}
