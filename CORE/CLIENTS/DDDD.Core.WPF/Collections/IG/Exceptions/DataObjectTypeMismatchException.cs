﻿using System;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// An <see cref="T:System.Exception" /> that is thrown when the GetEnumerator method of the IEnumerable returns null for the <see cref="T: DataManagerBase" />.
    /// </summary>
    public class DataObjectTypeMismatchException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataObjectTypeMismatchException" /> class.
        /// </summary>
        public DataObjectTypeMismatchException() : base("DataObjectTypeMismatchException") // SR.GetString(   -  [12/14/2016 A1]            
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataObjectTypeMismatchException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        public DataObjectTypeMismatchException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T: DataObjectTypeMismatchException" /> class.
        /// </summary>
        /// <param name="message">The message that should be displayed.</param>
        /// <param name="innerException">An inner exception.</param>
        public DataObjectTypeMismatchException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
