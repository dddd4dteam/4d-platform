﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace DDDD.Core.Collections.IG
{

    internal class AggregateEnumerable<T> : IEnumerable<T>, IEnumerable
    {
        private IEnumerable<IEnumerable<T>> _enumerables;

        public AggregateEnumerable(params IEnumerable<T>[] enumerables) : this((IEnumerable<IEnumerable<T>>)enumerables)
        {
        }

        public AggregateEnumerable(IEnumerable<IEnumerable<T>> enumerables)
        {
            this._enumerables = enumerables;
        }

        public IEnumerator GetEnumerator()
        {
            return new  AggregateEnumerable<T>.Enumerator(this._enumerables);
        }

        IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator()
        {
            return new  AggregateEnumerable<T>.Enumerator(this._enumerables);
        }

        public class Enumerator : IEnumerator<T>, IDisposable, IEnumerator
        {
            private IEnumerator<IEnumerable<T>> _enumerables;

            private IEnumerator<T> _current;

            public T Current
            {
                get
                {
                    if (this._current == null)
                    {
                        return default(T);
                    }
                    return this._current.Current;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public Enumerator(params IEnumerable<T>[] enumerables) : this((IEnumerable<IEnumerable<T>>)enumerables)
            {
            }

            public Enumerator(IEnumerable<IEnumerable<T>> enumerables)
            {
                IEnumerator<IEnumerable<T>> enumerator;
                if (enumerables != null)
                {
                    enumerator = enumerables.GetEnumerator();
                }
                else
                {
                    enumerator = null;
                }
                this._enumerables = enumerator;
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (this._current != null && this._current.MoveNext())
                {
                    return true;
                }
                while (this._enumerables != null && this._enumerables.MoveNext())
                {
                    IEnumerable<T> current = this._enumerables.Current;
                    if (current == null)
                    {
                        continue;
                    }
                    this._current = current.GetEnumerator();
                    if (!this._current.MoveNext())
                    {
                        continue;
                    }
                    return true;
                }
                return false;
            }

            public void Reset()
            {
                if (this._enumerables != null)
                {
                    this._enumerables.Reset();
                }
                this._current = null;
            }
        }
    }


}
