﻿using System;

namespace DDDD.Core.Collections.IG
{
    /// <summary>
    /// Event arguments for the event <see cref="E:Collections.VirtualCollection`1.CommitNewItem" />
    /// </summary>
    /// <typeparam name="T">The type of new item.</typeparam>
    /// <seealso cref="E:Collections.VirtualCollection`1.CommitNewItem" />
    public class CommitNewItemEventArgs<T> : EventArgs
    {
        /// <summary>
        /// Gets the new item to be commited.
        /// </summary>
        public T NewItem
        {
            get;
            private set;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:Collections.CommitNewItemEventArgs`1" /> class.
        /// </summary>
        /// <param name="item">The new item.</param>
        public CommitNewItemEventArgs(T item)
        {
            this.NewItem = item;
        }
    }
}
