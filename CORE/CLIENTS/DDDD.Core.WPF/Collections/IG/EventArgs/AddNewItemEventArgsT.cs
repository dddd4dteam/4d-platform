﻿using System;
using System.Reflection;

namespace DDDD.Core.Collections.IG
{
	/// <summary>
	/// Event arguments for the event <see cref="E:Collections.VirtualCollection`1.AddNewItem" />
	/// </summary>
	/// <typeparam name="T">The type of new item.</typeparam>
	/// <seealso cref="E:Collections.VirtualCollection`1.AddNewItem" />
	public class AddNewItemEventArgs<T> : EventArgs
	{
		/// <summary>
		/// Gets or sets the default value of the item.
		/// </summary>
		public T DefaultValue
		{
			get;
			set;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="T:Collections.AddNewItemEventArgs`1" /> class.
		/// </summary>
		public AddNewItemEventArgs()
		{
			ConstructorInfo constructor = typeof(T).GetConstructor(Type.EmptyTypes);
			if (constructor != null && (typeof(T).IsPublic || typeof(T).IsNestedPublic))
			{
				this.DefaultValue = (T)constructor.Invoke(null);
			}
		}
	}
}
