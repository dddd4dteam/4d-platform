﻿using System;

namespace DDDD.Core.Collections.IG
{
    public class CurrentItemEventArgs : EventArgs
    {
        public object Item { get; protected internal set; }
    }
}
