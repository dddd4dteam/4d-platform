﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Composition;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Composition.Hosting.Core;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;

namespace DDDD.Core.ComponentModel.IOC
{
    public class MefDependencyResolver : MefDependencyScope, IDependencyResolver
    {
        readonly System.Composition.ExportFactory<CompositionContext> _requestScopeFactory;

        public MefDependencyResolver(CompositionHost rootCompositionScope)
            : base(new Export<CompositionContext>(rootCompositionScope, rootCompositionScope.Dispose))
        {
            if (rootCompositionScope == null)
                throw new ArgumentNullException("rootCompositionScope");

            var factoryContract = new CompositionContract(typeof(System.Composition.ExportFactory<CompositionContext>), null, new Dictionary<string, object>
            {
                { "SharingBoundaryNames", new[] { "HttpRequest" } }
            });

            _requestScopeFactory = (System.Composition.ExportFactory<CompositionContext>)rootCompositionScope.GetExport(factoryContract);
        }

        public IDependencyScope BeginScope()
        {
            return new MefDependencyScope(_requestScopeFactory.CreateExport());
        }

        public static IDependencyResolver CreateWithDefaultConventions(Assembly[] appAssemblies)
        {
            var conventions = new ConventionBuilder();

            conventions.ForTypesDerivedFrom<IHttpController>()
                .Export();

            conventions.ForTypesMatching(t => t.Namespace != null && t.Namespace.EndsWith(".Parts"))
                .Export()
                .ExportInterfaces();

            var container = new ContainerConfiguration()
                .WithAssemblies(appAssemblies, conventions)
                .CreateContainer();

            return new MefDependencyResolver(container);
        }
    }
}
