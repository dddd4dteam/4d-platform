﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;

namespace DDDD.Core.ComponentModel.IOC
{
    public class MefDependencyScope : IDependencyScope
    {
        readonly Export<CompositionContext> _compositionScope;

        public MefDependencyScope(Export<CompositionContext> compositionScope)
        {
            if (compositionScope == null)
                throw new ArgumentNullException("compositionScope");

            _compositionScope = compositionScope;
        }

        public object GetService(Type serviceType)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            object result;
            CompositionScope.TryGetExport(serviceType, null, out result);
            return result;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (serviceType == null)
                throw new ArgumentNullException("serviceType");

            return CompositionScope.GetExports(serviceType, null);
        }

        protected CompositionContext CompositionScope
        {
            get { return _compositionScope.Value; }
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
                _compositionScope.Dispose();
        }
    }

}
