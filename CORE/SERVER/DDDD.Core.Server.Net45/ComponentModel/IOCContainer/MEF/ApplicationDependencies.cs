﻿

using System;
using System.Collections.Generic;
using System.Composition;
using System.Composition.Convention;
using System.Composition.Hosting;
using System.Composition.Hosting.Core;
using System.Reflection;
using System.Web.Http.Controllers;
using System.Web.Http.Dependencies;


namespace DDDD.Core.IOC.MEF
{








    //    [Export]
    //    public  class AppDependenciesResolverMEF
    //    {
    //    }


    //    [Export]
    //    public class SystemComposition : ISystemComposition
    //    {
    //        public SystemComposition() { }


    #region  -------------------------- COMMAND MANAGERS -----------------------------
    //        /// <summary>
    //        /// Менеджеры Команд
    //        /// </summary>
    //        [ImportMany(typeof(ICommandManager), AllowRecomposition = true)]
    //        public IList<Lazy<ICommandManager, ICommandManagerMetaData>> commandManagers = new List<Lazy<ICommandManager, ICommandManagerMetaData>>();
    //        //{
    //        //    get{ if(commandManagers == null) commandManagers = new List<Lazy<ICommandManager, ICommandManagerMetaData>>();  
    //        //       return commandManagers; }
    //        //    set { commandManagers = value; }
    //        //}

    //        /// <summary>
    //        /// Менеджеры
    //        /// </summary>
    //        public List<ICommandManager> CommandManagers
    //        {
    //            get { return commandManagers.Select(tl => tl.Value).ToList(); }
    //            set { }
    //        }


    //        /// <summary>
    //        /// Получить Менеджера Комманд
    //        /// </summary>
    //        /// <param name="CommandManager"></param>
    //        /// <returns></returns>
    //        public ICommandManager GetCommmandManager(CommandManager_RegisterEn CommandManager)
    //        {
    //            try
    //            {

    //                return commandManagers.Where(cmdmngr => cmdmngr.Metadata.CommandManager == CommandManager).Select(cmdmngr => cmdmngr.Value).FirstOrDefault();
    //            }
    //            catch (Exception)
    //            {
    //                throw new Exception("");
    //            }
    //        }

    #endregion -------------------------- END COMMAND MANAGERS -----------------------------


    #region  -------------------------- SERVICES COMMAND MANAGERS -----------------------------

    //        /// <summary>
    //        /// Менеджеры Команд
    //        /// </summary>
    //        [ImportMany(typeof(IServiceCommandManager), AllowRecomposition = true)]
    //        public IList<Lazy<IServiceCommandManager, IServiceCommandManagerMetaData>> serviceCommandManagers = new List<Lazy<IServiceCommandManager, IServiceCommandManagerMetaData>>();
    //        //{
    //        //    get{ if(commandManagers == null) commandManagers = new List<Lazy<ICommandManager, ICommandManagerMetaData>>();  
    //        //       return commandManagers; }
    //        //    set { commandManagers = value; }
    //        //}

    //        /// <summary>
    //        /// Менеджеры
    //        /// </summary>
    //        public List<IServiceCommandManager> ServiceCommandManagers
    //        {
    //            get { return serviceCommandManagers.Select(tl => tl.Value).ToList(); }
    //            set { }
    //        }


    //        /// <summary>
    //        /// Получить Менеджера Комманд
    //        /// </summary>
    //        /// <param name="CommandManager"></param>
    //        /// <returns></returns>
    //        public IServiceCommandManager GetServiceCommmandManager(ServiceModelManager_RegisterEn Service)
    //        {
    //            try
    //            {
    //                return serviceCommandManagers.Where(svccmdmngr => svccmdmngr.Metadata.ServiceModel == Service).Select(svccmdmngr => svccmdmngr.Value).FirstOrDefault();
    //            }
    //            catch (Exception)
    //            {
    //                throw new Exception("");
    //            }
    //        }

    #endregion -------------------------- END COMMAND MANAGERS -----------------------------


    #region  ----------------------------- BEGIN SERVICE MODEL MANAGERS -----------------------------

    //        /// <summary>
    //        /// Менеджеры Моделей Сервисов
    //        /// </summary>
    //        [ImportMany(typeof(IServiceModelManager), AllowRecomposition = true)]
    //        public IList<Lazy<IServiceModelManager, IServiceModelManagerMetaData>> serviceModelManagers = new List<Lazy<IServiceModelManager, IServiceModelManagerMetaData>>();
    //        //{
    //        //    get{
    //        //        if (serviceModelManagers == null) serviceModelManagers = new List<Lazy<IServiceModelManager, IServiceModelManagerMetaData>>();
    //        //        return serviceModelManagers;
    //        //       }
    //        //    set { serviceModelManagers = value; }
    //        //}


    //        /// <summary>
    //        /// Менеджеры Моделей Сервисов
    //        /// </summary>
    //        public List<IServiceModelManager> ServiceModelManagers
    //        {
    //            get { return serviceModelManagers.Select(tl => tl.Value).ToList(); }
    //        }


    //        /// <summary>
    //        /// Получить Менеджера Моделей Сервисов
    //        /// </summary>
    //        /// <param name="Service"></param>
    //        /// <returns></returns> 
    //        public IServiceModelManager GetServiceModelManager(ServiceModelManager_RegisterEn Service)
    //        {
    //            try
    //            {
    //                return serviceModelManagers.Where(svcmgr => svcmgr.Metadata.ServiceModel == Service).Select(svcmgr => svcmgr.Value).FirstOrDefault(); ;
    //            }
    //            catch (Exception)
    //            {
    //                throw new Exception("GetServiceModelManager throw some Exception");
    //            }
    //        }

    #endregion ----------------------------- END SERVICE MODEL MANAGERS -----------------------------


    #region ---------------------- INFRASTRUCTURE SERVICES --------------------------

    //        /// <summary>
    //        /// Сервисы Инфраструктуры
    //        /// </summary>
    //        [ImportMany(typeof(IInfrastructureService), AllowRecomposition = true)]
    //        public IList<Lazy<IInfrastructureService, IInfrastructureServiceMetaData>> infrastructureServices = new List<Lazy<IInfrastructureService, IInfrastructureServiceMetaData>>();
    //        //{
    //        //    get
    //        //    {
    //        //        if (infrastructureServices == null) infrastructureServices = new List<Lazy<IInfrastructureService, IInfrastructureServiceMetaData>>();
    //        //        return infrastructureServices;
    //        //    }
    //        //    set { infrastructureServices = value; }
    //        //}



    //        /// <summary>
    //        /// Менеджеры Моделей Сервисов
    //        /// </summary>
    //        public List<IInfrastructureService> InfrastructureServices
    //        {
    //            get { return infrastructureServices.Select(tl => tl.Value).ToList(); }
    //        }



    //        /// <summary>
    //        /// Получить  Сервис Инфраструктуры 
    //        /// </summary>
    //        /// <param name="Service"></param>
    //        /// <returns></returns> 
    //        public IInfrastructureService GetInfrastructureService(InfrastructureService_RegisterEn InfrastructureService)
    //        {
    //            try
    //            {
    //                return infrastructureServices.Where(infrasvcmgr => infrasvcmgr.Metadata.InfrastructureServiceManager == InfrastructureService).Select(svcmgr => svcmgr.Value).FirstOrDefault(); ;
    //            }
    //            catch (Exception)
    //            {
    //                throw new Exception("");
    //            }
    //        }

    #endregion ---------------------- INFRASTRUCTURE SERVICES --------------------------
    

    //        #region ---------------------- Server Modules PARTS --------------------------
    //        /// <summary>
    //        /// Сервисы Инфраструктуры
    //        /// </summary>
    //        [ImportMany(typeof(IModuleExt), AllowRecomposition = true)]
    //        public IList<Lazy<IModuleExt, IModuleExtMetaData>> serverModules = new List<Lazy<IModuleExt, IModuleExtMetaData>>();



    //        /// <summary>
    //        /// Менеджеры Моделей Сервисов
    //        /// </summary>
    //        public List<IModuleExt> Modules
    //        {
    //            get { return serverModules.Select(tl => tl.Value).ToList(); }
    //        }


    //#if SERVER
    //        /// <summary>
    //        /// Получить  Сервис Инфраструктуры 
    //        /// </summary>
    //        /// <param name="Service"></param>
    //        /// <returns></returns> 
    //        public IModuleExt GetModule(ServiceModelManager_RegisterEn crModule)
    //        {
    //            try
    //            {
    //                return serverModules.Where(mdl => mdl.Metadata.ModuleService == crModule).Select(mdl => mdl.Value).FirstOrDefault();
    //            }
    //            catch (Exception)
    //            {
    //                throw new Exception("");
    //            }
    //        }
    //#endif

    //      #endregion ---------------------- Server Modules PARTS --------------------------



    //        //------------------- Клиентский Коллекции композиции -----------------------

    //#if CLIENT && SL5
    

    #region ---------------------- WCF Services ---------------------------
    
    //        ///// <summary>
    //        ///// WCF сервисы коммуникаций 
    //        ///// </summary>
    //        //[ImportMany(typeof(ICommandWcfService), AllowRecomposition = true)]
    //        //public IList<Lazy<ICommandWcfService, ICommandWcfServiceMetaData>> wcfServices = new List<Lazy<ICommandWcfService, ICommandWcfServiceMetaData>>();



    //        ///// <summary>
    //        ///// Сервисы Wcf для работы с командами
    //        ///// </summary>
    //        //public List<ICommandWcfService> WcfServices
    //        //{
    //        //    get { return wcfServices.Select(tl => tl.Value).ToList(); }
    //        //}



    //        ///// <summary>
    //        ///// Получить  Сервис WCF/ Но ключ старый тот же что и у менеджеров моделей сервисов
    //        ///// </summary>
    //        ///// <param name="Service"></param>
    //        ///// <returns></returns> 
    //        //public ICommandWcfService GetWcfServiceProxy(ServiceModelManager_RegisterEn ServiceCustomer)
    //        //{
    //        //    try
    //        //    {
    //        //        return wcfServices.Where(wcfSvc => wcfSvc.Metadata.ServiceModelCustomer == ServiceCustomer).Select(svcmgr => svcmgr.Value).FirstOrDefault();
    //        //    }
    //        //    catch (Exception exc)
    //        //    {
    //        //        throw exc;
    //        //    }
    //        //}


    //        ////GetWcfServiceProxy

    #endregion ---------------------- WCF Services ---------------------------



    //    /// <summary>
    //    /// Менеджеры Модулей -приват
    //    /// </summary>
    //    [ImportMany(typeof(IServiceModelManager), AllowRecomposition = true)]
    //    public IList<Lazy<IServiceModelManager, IServiceModelManagerMetaData>> moduleManagers  = new List<Lazy<IServiceModelManager, IServiceModelManagerMetaData>>();
    
    //    //Add    XAP
    //    //Remove XAP
    //#else  //---------------  SERVER ONLY --------------------




    //        ///----------------- Identification Elements -------------------


    //        [Import(AllowDefault = true)]
    //        public ICRMIdentificationProvider IdentificationProvider
    //        { get; set; }



    //#endif //---------------  SERVER ONLY --------------------






    //    }


}
