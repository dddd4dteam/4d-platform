﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics.Contracts;
using DDDD.Core.HttpRoutes;


namespace DDDD.Core.Extensions
{

    /// <summary>
    /// String class extensions
    /// </summary>
    public static class HttpStringExtensions
    {



        public static KeyValuePair<RouteKeysEn, TVal> KVPair<TVal>(this RouteKeysEn routeKey, TVal value)
        {
            return new KeyValuePair<RouteKeysEn, TVal>(routeKey, value);
        }


    }
}
