﻿using DDDD.Core.Data.DA2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRM.DAL
{
    public  partial class S1_GeographyModel
     {
    //          case   HierarchyCommandManager.ComponentsEn.ItemEntity:
    //                       return "V_S1_GeographyItem_Vo";
    //                case HierarchyCommandManager.ComponentsEn.HierarchyEntity:
    //                    return "crs_S1_GeographyHierarchy";
    //               case HierarchyCommandManager.ComponentsEn.ItemTypeEntity:
    //                  return "crs_S1_GeographyItemType_Register";
    //               case HierarchyCommandManager.ComponentsEn.AncestorsView:
    //                  return "V_S1_GeographyItemAncestor";
    //               case HierarchyCommandManager.ComponentsEn.DescndantsView:
    //                  return "V_S1_GeographyItemDescendant";
    //               case HierarchyCommandManager.ComponentsEn.ItemsOnLevel:
    //                  return "V_S1_GeographyItemOnLevel";
                
                   



		protected override Dictionary<string, Type> Components
		{ get; set; } = new Dictionary<string, Type>()
		{
             { HierarchyServiceModel.Component.ItemEntity,typeof(V_S1_GeographyItem_Vo) } // typeof(crs_S1_GeographyItem)
            ,{ HierarchyServiceModel.Component. HierarchyEntity,  typeof(crs_S1_GeographyHierarchy) }
            ,{ HierarchyServiceModel.Component. ItemTypeEntity,  typeof(crs_S1_GeographyItemType_Register) }
            ,{ HierarchyServiceModel.Component. AncestorsView,  typeof(V_S1_GeographyItemAncestor) }
            ,{ HierarchyServiceModel.Component. DescndantsView,  typeof(V_S1_GeographyItemDescendant)}
            ,{ HierarchyServiceModel.Component. ItemsOnLevel,  typeof(V_S1_GeographyItemOnLevel) }
              
		};
	}
}
