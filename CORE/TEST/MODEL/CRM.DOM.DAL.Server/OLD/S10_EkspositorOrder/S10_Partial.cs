﻿
namespace CRM.DAL
{
    public enum ConstructionTypeEn
    {
        /// <summary>
        /// Плиточный (1)
        /// </summary>
        Tiles = 1,

        /// <summary>
        /// Санфаянс (2)
        /// </summary>
        SanitaryFaience = 2,

        /// <summary>
        /// Мебель (3)
        /// </summary>
        Furniture = 3,

        /// <summary>
        /// Ванны (4)
        /// </summary>
        Bathtub = 4,

        /// <summary>
        /// Образцы 3D (5)
        /// </summary>
        Samples3D = 5

    }

    public partial class V_S10_Ekspositor
    {
        public ConstructionTypeEn ConstructionTypeEn
        {
            get { return (ConstructionTypeEn)ID_ConstructionType; }
            set { ID_ConstructionType = (int)value; }
        }
    }

    public partial class V_S10_EkspositorOrder_Vo
    {
        public ConstructionTypeEn ConstructionTypeEn
        {
            get { return (ConstructionTypeEn)ID_ConstructionType; }
            set { ID_ConstructionType = (int)value; }
        }
    }
}
