﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Composition.DataServices;
using System.Resources;
using Core.Commanding;
using Core.Meta.Systems;
using CRM.DAL;
using Core;
//using CRM.DOM.DAL;

namespace CRM.DOM.Services
{
    public partial class S15_MonitoringModelManager
    {

        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return typeof (V_S14_Product_Vo); } }
      

        /// <summary>
        /// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn
        {
            get
            {
                return new List<ServiceModelManager_RegisterEn>()
                {
                    ServiceModelManager_RegisterEn.S12_Units,
                    ServiceModelManager_RegisterEn.S13_Grupa,
                    ServiceModelManager_RegisterEn.S2_Assortment,
                    ServiceModelManager_RegisterEn.S14_Product,
                    ServiceModelManager_RegisterEn.S9_Meeting,
                };


            }
        }
    }
}