﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Resources;
using CRM.DAL;
using Core;
using Core.Composition.DataServices;
using Core.Commanding;
using Core.Meta.Systems;
using Core.CommandManager;
using Core.Meta.BO;
using Core.BoModel;

namespace CRM.DOM.Services
{

    [ServiceModelManager(ServiceModelManager_RegisterEn.S1_Geography, SrviceTypeEn.HierarchyStructureServiceType)]
    public partial class S1_GeographyModelManager : IHierarchyServiceModelManager
    {

        public S1_GeographyModelManager() { }


        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return null; } }


        /// <summary>
        /// Сервис 
        /// </summary>
        public  ServiceModelManager_RegisterEn ServiceModel 
        {
            get { return GetType().GetTypeAttribute<ServiceModelManagerAttribute>().FirstOrDefault().ServiceModel; }
        }


        /// <summary>
        /// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn { get { return new List<ServiceModelManager_RegisterEn>(); } }



        private ResourceManager resourceManager = new ResourceManager("CRM.DOM.DAL.S1_Geography.S1_Geography", typeof(S1_GeographyModelManager).Assembly);
        /// <summary>
        /// Менеджер ресурсов данного сервиса
        /// </summary>
        public ResourceManager ResourceManager 
        {
            get { return resourceManager; }
        }

        /// <summary>
        /// Получить Все Контракты Модели Сервиса(объекты в роли DataTables здесь присутствуют - т.е. КОнтракты со всеми ролями, по Даум разделам(Сущностей, Объектов Сруктур)  )
        /// </summary>
        /// <returns></returns>
        public List<Type> GetBOContracts()
        {
            try
            {
                
              return   new List<Type>()
              {
                  typeof(crs_S1_GeographyHierarchy),
                  typeof(crs_S1_GeographyItem),
                  typeof(crs_S1_GeographyItemType_Register),
                  typeof(V_S1_GeographyItem_Vo),
                  typeof(V_S1_GeographyItemAncestor),
                  typeof(V_S1_GeographyItemDescendant),
                  typeof(V_S1_GeographyItemOnLevel)                ///PLACE CODE HERE

              };

            }
            catch (Exception)
            {                
                throw;
            }
        }


        /// <summary>
        /// Вернуть Тип Компонента в даном Сервисе исходя из маппинга. 
        /// </summary>
        /// <param name="StructureComponent"></param>
        /// <returns></returns>
        public Type GetDataStructureComponentType(Enum StructureComponent)
        {
            try
            {
                HierarchyCommandManager.ComponentsEn component = (HierarchyCommandManager.ComponentsEn)StructureComponent;
                foreach (var item in GetBOContracts())
	            {
                    DataStructureItemAttribute criterion =  item.GetTypeAttribute<DataStructureItemAttribute>().FirstOrDefault();
                    if (criterion != null && ((HierarchyCommandManager.ComponentsEn)Enum.Parse(typeof(HierarchyCommandManager.ComponentsEn), criterion.StructureMappingKey, false)) == component)
                        return item;        
	            }

                return null;
            }
            catch (Exception)
            {   
                throw;
            }
        }


        /// <summary>
        /// Получить только Клиентские контракты необходимые для Коммуникационного сервиса( объекты в роли DataTables здесь исключаются)
        /// </summary>
        /// <returns></returns>
       public List<Type> GetClientContracts()
        {
            try
            {
                 return   new List<Type>()
                 {
                     ///PLACE CODE HERE
                  typeof(crs_S1_GeographyHierarchy),
                  typeof(crs_S1_GeographyItem),
                  typeof(crs_S1_GeographyItemType_Register),
                  typeof(V_S1_GeographyItem_Vo)
                  //typeof(V_S1_GeographyItemAncestor),
                  //typeof(V_S1_GeographyItemDescendant)   ///PLACE CODE HERE

                };
            }
            catch (Exception)
            {
                
                throw;
            }
        }




        /// <summary>
        /// Получить компонент структуры  данных от даного сервиса 
        /// </summary>
        /// <param name="StructureComponent"></param>
        /// <returns></returns>
        public String GetDataStructureComponent(Enum StructureComponent)
        {
            try
            {
                HierarchyCommandManager.ComponentsEn component = (HierarchyCommandManager.ComponentsEn)StructureComponent;
                switch (component)
                {
                    case HierarchyCommandManager.ComponentsEn.ItemEntity:
                        {
                            return "V_S1_GeographyItem_Vo";
                        }
                    case HierarchyCommandManager.ComponentsEn.HierarchyEntity:
                        {
                            return "crs_S1_GeographyHierarchy";
                        }
                    case HierarchyCommandManager.ComponentsEn.ItemTypeEntity:
                        {
                            return "crs_S1_GeographyItemType_Register";
                        }
                    case HierarchyCommandManager.ComponentsEn.AncestorsView:
                        {
                            return "V_S1_GeographyItemAncestor";
                        }
                    case HierarchyCommandManager.ComponentsEn.DescndantsView:
                        {
                            return "V_S1_GeographyItemDescendant";
                        }
                    case HierarchyCommandManager.ComponentsEn.ItemsOnLevel:
                        {
                            return "V_S1_GeographyItemOnLevel";
                        }
                    default:
                        {
                            break;
                        }
                }

                return "";
            }
            catch (Exception)
            {                
                throw;
            }
        }


        /// <summary>
        /// Является ли данный сервис Сервисом для Предопределенной структуры. 
        /// Если тип Структуры  не равен  CustomStructureServiceType то да.
        /// Указывается в атрибуте данного сервиса 
        /// </summary>
        public bool IsSpecifiedDataStructureService
        {
            get
            {
                return (!IsServiceType(ServiceTypeEn.CustomStructureServiceType));
            }
        }



        /// <summary>
        /// Это сервис какого типа - ПОльзовательский или под какой то структурой
        /// </summary>
        /// <param name="serviceStructureType"></param>
        /// <returns></returns>
        public bool IsServiceType(ServiceTypeEn serviceStructureType)
        {
            try
            {
                return (GetType().GetTypeAttribute<ServiceModelManagerAttribute>().FirstOrDefault().ServiceType == serviceStructureType);
            }
            catch (Exception)
            {
                throw new Exception(" Cannot get current Service Type  ");
            }
        }



        /// <summary>
        /// Получить типы объектов сервиса Которые имеют Роли  boRole
        /// </summary>
        /// <param name="boRole"></param>
        /// <returns></returns>
        public List<Type> GetObjectsByRole(BORoleEn boRole)
        {
            try
            {
                List<Type> AllBObjectTypes =
#if CLIENT && SL5
				GetClientContracts();
#elif SERVER // ------- Server
             GetBOContracts();
#endif
                return AllBObjectTypes.Where(tp => tp.GetTypeAttribute<BORoleAttribute>().BoConatainRole(boRole)).ToList();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        //public Type GetDataStructureComponentType(Enum StructureComponent)
        //{
        //    throw new NotImplementedException();
        //}


        //public List<Type> GetBOContracts()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
