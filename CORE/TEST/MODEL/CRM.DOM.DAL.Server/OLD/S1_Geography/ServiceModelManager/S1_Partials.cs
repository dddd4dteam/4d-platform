﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.BoModel;
using Core.Meta.BO;
using Core.Composition.DataServices;
using System.Runtime.Serialization;

namespace CRM.DAL
{

    [DataStructureItemAttribute("HierarchyEntity")]
    public partial class crs_S1_GeographyHierarchy
    {    }
    

    [DataStructureItemAttribute("ItemEntity")]
    public partial class V_S1_GeographyItem_Vo :  IHierarchicalItem
    {   
        /// <summary>
        /// Уровень - служебная информация
        /// </summary>
        [DataMember]public int Level { get; set; }
    }
    

    [DataStructureItemAttribute("ItemTypeEntity")]
    public partial class crs_S1_GeographyItemType_Register
    {   }
    

    [DataStructureItemAttribute("AncestorsView")]
    public partial class V_S1_GeographyItemAncestor 
    {   }
    

    [DataStructureItemAttribute("DescndantsView")]
    public partial class V_S1_GeographyItemDescendant 
    {   }

    [DataStructureItemAttribute("ItemsOnLevel")]
    public partial class V_S1_GeographyItemOnLevel : IHierarchicalView
    {
        public IBObject ToEntityItem()
        {
            return new V_S1_GeographyItem_Vo()
            {
                Level = (Level == null) ? 0 : Level.Value,
                Id_GeographyItem = this.Id_GeographyItem,
                Id_GeographyParentItem = this.Id_GeographyParentItem,
                Id_GeographyItemType = this.Id_GeographyItemType,
                GeographyItem = this.GeographyItem,
                Prefix = this.Prefix,
                Description = this.Description,
                Population = this.Population
            };
        }

        int IHierarchicalView.Level
        {
            get { return (int)Level; }
        }

    }


    //crs_S1_GeographyHierarchy
    //--crs_S1_GeographyItem
    //crs_S1_GeographyItemType_Register
    //V_S1_GeographyItem_Vo
    //V_S1_GeographyItemAncestor
    //V_S1_GeographyItemDescendant


}
