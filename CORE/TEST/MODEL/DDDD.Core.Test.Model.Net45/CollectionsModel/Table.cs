﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.CollectionsModel
{
    /// <summary>
    /// Data field
    /// </summary>
    //[Serializable]
    


    /// <summary>
    /// Data table
    /// </summary>
    public class Table
    {

        #region Properties
        public int ID { get; set; }


        string _name = null;
        public string Name
        {
            get { return _name; }
            set { _name = value != null ? value.ToLower() : value; }
        }

        public string Description { get; set; }


        FieldCollection _fields = null;
        public FieldCollection Fields { get { return _fields ?? (_fields = new FieldCollection()); } set { _fields = value; } }
        #endregion


    }
}
