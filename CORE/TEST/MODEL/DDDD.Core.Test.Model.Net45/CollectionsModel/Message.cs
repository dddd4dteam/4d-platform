﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.SDS.Tests.Model
{
    public class TestContract
    {
        public string Str { get; set; }
        public int Int { get; set; }
        public Guid UUID { get; set; }
    }

    public class Message
    {
        public ulong ID { get; set; }
        public DateTime CreateDate { get; set; }
        public string Data { get; set; }
        public TestContract test { get; set; }
        public List<TestContract> Tests { get; set; }
    }
}
