﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.CollectionsModel
{
  

    public class Field
    {
        string _name = null;

        #region Properties
        public int ID { get; set; }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value != null ? value.ToLower() : value;
            }
        }

        public string Description { get; set; }

        public DbType DataType { get; set; }

        public uint Length { get; set; }
        
        #endregion
    }


}
