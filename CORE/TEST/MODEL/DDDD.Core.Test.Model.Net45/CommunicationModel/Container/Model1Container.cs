﻿

using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.CommunicationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.Communication
{
    public class Model1Container
    {

        #region ------------------------- CONST ----------------------------------

        const Int32 CommandsTestCountDefault = 7;// 20000; 
        const Int32 ProductsInCommandTestDefault = 20;

        #endregion ------------------------- CONST ----------------------------------


        #region  ---------------------------- FIELDS ---------------------------------

        // public static Dictionary<Int32, object> DATA = new Dictionary<Int32, object>();
        // public static Dictionary<Int32, object> compareDeserDATA = new Dictionary<Int32, object>();


        public static byte[] SerializedDATA
        {
            get;
            set;
        }


        public static List<CommandContainer2> DATACommands
        {
            get;
            set;
        }


        public static List<CommandContainer2> CompareDesererializeDATA
        {
            get;
            set;
        }


        #endregion ---------------------------- FIELDS ---------------------------------


        #region --------------------------- GENERATE UTILS -----------------------------
        
        static DateTime RandomDay()
        {
            DateTime start = new DateTime(1995, 1, 1);
            Random gen = new Random();

            int range = (DateTime.Today - start).Days;
            return start.AddDays(gen.Next(range));
        }

        #endregion --------------------------- GENERATE UTILS -----------------------------
        


        static Product GenerateProduct(Int32 ID, decimal price = 0, String category = null, String name = null)
        {
            return new Product
            {
                Id = ID,
                Category = String.IsNullOrEmpty(category) ? "Autos" : category,
                //Price = ( price == 0 ) ? new Random().Next(30000,2000000) :  price,
                Name = String.IsNullOrEmpty(name) ? "AutoName_" + ID.ToString() : name
            };
        }


        static IEnumerable<Product> GenerateNProducts(Int32 Count)
        {
            Product[] resultProducts = new Product[Count];
            for (Int32 i = 1; i <= Count; i++)
            {
                resultProducts[i] = GenerateProduct(i);
            }
            return resultProducts;
        }


        public static IEnumerable<CommandContainer2> GenerateArrayNCommands1(Int32 CommandsCount = CommandsTestCountDefault, Int32 ProductsInCommand = ProductsInCommandTestDefault)
        {
            DATACommands = new List<CommandContainer2>();

            for (Int32 i = 1; i <= CommandsCount; i++)
            {
                DATACommands.Add(new CommandContainer2());
                DATACommands[i - 1].Products = new List<object>();
                for (int m = 0; m < ProductsInCommand; m++)
                {
                    DATACommands[i - 1].Products.Add(GenerateProduct(m));
                }
            }

            return DATACommands;

        }


    }
}
