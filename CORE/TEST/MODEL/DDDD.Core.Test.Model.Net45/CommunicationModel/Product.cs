﻿
using DDDD.Core.Model.CommunicationModel;
using System;
using System.Collections.Generic;

namespace DDDD.TSS.Tests.Model.CommunicationModel
{



    public struct ProductMark
    {
        public string Copyrighter;

        public string Mark;

        public int YearCreated;
    }


    public class Product : ContractBase, IContractBase
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public decimal? Price { get; set; }

        public uint Length { get; set; }

        public int PublicIntField;
        private string v1;
        private int v2;

       

        public ProductMark Mark { get; set; }

        public List<string> PartsNames { get; set; }

        public List<ProductMark> PartsProductMarks { get; set; }

        public bool IsReadOnly
        {
            get { return false; }
        }

    }


    public class Product2
    {

        public ProductMark? Mark { get; set; }
    }
    

    public class Product3
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public ProductMark? Mark { get; set; }

        public List<string> PartsNames { get; set; }

        public List<ProductMark> PartsProductMarks { get; set; }
    }


    public class Product4
    {

        /**
         * CSharpTypeFullName of this contract
        */
        public static string TypeFullName { get; } = "DDDD.TSS.Tests.Model.CommunicationModel.Product4";



        public int productId { get; set; }
        public string productName { get; set; }
        public string productCode { get; set; }
        public DateTime releaseDate { get; set; }
        public double price { get; set; }
        public string description { get; set; }
        public double starRating { get; set; }

        public string imageUrl { get; set; }
    }

    public struct ProductStruct :  IContractBase
    {
        
        public int Id { get; set; }
      
        public string Name { get; set; }
      
        public string Category { get; set; }

        public decimal? Price { get; set; }    

        public uint Length { get; set; }

        public int PublicIntField;

        public ProductMark Mark { get; set; }

        public List<string> PartsNames { get; set; }

        public List<ProductMark> PartsProductMarks { get; set; }
       

        public bool IsReadOnly
        {
            get { return false; }
        }


    }


}
