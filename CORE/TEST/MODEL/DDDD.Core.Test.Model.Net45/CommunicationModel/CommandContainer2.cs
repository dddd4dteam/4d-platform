﻿using DDDD.Core.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.CommunicationModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.CommunicationModel
{

    /// <summary>
    /// Command COntainer 2 version - for KellermanSoftware .Serializer Test
    /// </summary>
    public partial class CommandContainer2
    {
        /// <summary>
        /// 
        /// </summary>
        public CommandContainer2()
        {
            TimeStamp = DateTime.Now;
        }

        public DateTime TimeStamp { get; set; }


        /// <summary>
        /// List item as we can see is object - not specially typed as ContractBase to store Products
        /// </summary>
        public List<object> Products { get; set; }


        // [ProtoMember(2, DataFormat = DataFormat.Group)]
        public List<ContractBase> Products2 { get; set; }


        // [ProtoMember(3)]
        public Product Product { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        public ContractBase Product2 { get; set; }


    }
}
