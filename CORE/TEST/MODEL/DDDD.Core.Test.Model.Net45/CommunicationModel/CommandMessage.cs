﻿using DDDD.Core.Tests.Model.Ignore;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DDDD.Core.Tests.Model.CommunicationModel
{


    /// <summary>
    /// CommandMessage - Message  to target CommandManeger  about to do somethig based on [CommandName + Parameters], then store result again inside that CommandMessage, 
    /// or safe fault  error, and after all return the same Message back to client
    /// CommandMessage is also the DataContract, with only one property -Body.
    /// </summary>
    [DataContract] // DataContract is only stub for default serializer behavior
    public  class CommandMessage 
    {

        #region ---------------------------- CTOR ---------------------------------

        public CommandMessage()
        {
           
            ProcessingSuccessMessage = String.Empty;

            ID = Guid.NewGuid();
            Parameters = new Dictionary<string, object>();
            
        }

        #endregion ---------------------------- CTOR ---------------------------------
        
        
        #region -------------------------- IDENTIFICATION ------------------------------

        /// <summary>
        /// Command  ModelId
        /// </summary>
        public Guid ID 
        { get; 
          private set; 
        }

        #endregion-------------------------- IDENTIFICATION ------------------------------

     
        #region ------------------------ COMMUNICATION  TRANSMISSION INFO---------------------

        /// <summary>
        /// Web API/MVC  Controller Name
        /// </summary>
        public String Controller 
        { 
            get;
            internal protected set; 
        }

        /// <summary>
        /// WCF DynamicService  Key
        /// </summary>
        public String ServiceKey 
        {
            get;
            internal protected set; 
        }


        /// <summary>
        /// To Save Source SvcService Namespace
        /// </summary>
        public String ServiceNamespace
        {
            get;
            internal protected set;
        }

      



        #endregion ------------------------ COMMUNICATION TRANSMISSION INFO---------------------


        #region ----------------------TARGET COMMAND RECEIVER-COMMAND MANAGER --------------------------

        /// <summary>
        /// Targeting Processing Manager, like CRUDManager...
        /// </summary>
        public String TargetProcessingManager { get; set; }

        /// <summary>
        /// CommandName  to CommandController
        /// </summary>
        public String Command { get; set; }


        /// <summary>
        /// Flexible SDS can serialize complex parameter classes/structs and then we box them into the Dictionary, each by  its Key 
        /// </summary>
        public Dictionary<String, object> Parameters { get; set; }


        #endregion ----------------------TARGETCOMMAND RECEIVER-COMMAND MANAGER --------------------------


        #region -------------------------- RESULT  &  ERROR INFO ----------------------------


        /// <summary>
        /// This property will transmitt after command will'be packed  from client to server 
        /// </summary>
        [IgnoreMember]
        [DataMember]
        public byte[] Body
        {
            get;
            set;
        }


        /// <summary>
        /// Flexible SDS serializer could deserialize complex objects from single object
        /// </summary>
        public object Result { get; set; }
              

        /// <summary>
        /// Message on successful result (if you wish)
        /// </summary>
        public String ProcessingSuccessMessage { get; set; }
        

        /// <summary>
        /// Message on Fault
        /// </summary>
        public String ProcessingFaultMessage { get; set; }


        /// <summary>
        /// Internal System Error Code  
        /// </summary>
        public short ErrorCode { get; set; }



        /// <summary>
        /// Server( Command Controller)  should set this message if error happened  before send CommandMessage back.
        /// After such CommandMessage will be received by client we'll show full error message only in [Debug] Conditional  mode.
        /// In release mode client will see only common message - like [SERVER ERROR HAPPENED] 
        /// </summary>
        public bool HasServerErrorHappened
        {
            get
            {
                return !String.IsNullOrEmpty(ProcessingFaultMessage);
            }
        }

        /// <summary>
        /// By default Successfull Message is empty string, that means that Command is succsessful  - until server won't set an ErrorMessage.
        /// So if we have Empty String or valid Succesfull message -  command will be means as successful.  
        /// </summary>
        public bool HasCommandSuccessfullyCompleted
        {
            get
            {
                return (   ProcessingSuccessMessage != null
                    && (ProcessingSuccessMessage == "" || ProcessingSuccessMessage.Length > 0) );
            }
        }

        #endregion -------------------------- RESULT  &  ERROR INFO ----------------------------
        

    }

}
