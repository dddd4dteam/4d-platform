﻿using CRM.DAL;

using System;
using System.Collections.Generic;
using DDDD.Core.Extensions;


#if SERVER
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
#endif


namespace DDDD.Core.Test.Web.Models
{

    /// <summary>
    /// Client Report Information Container
    /// </summary>
    public class ClientReportInfoContainer
    {

        static ClientReportInfoContainer()
        {
#if SERVER && TEST
            Load_ClientReportInfoFromDB(new Guid("7670C94C-E82C-408E-A6E9-08F0CA7A757F"));
#endif
        }

        #region  ------------------------SQL COMMON QUERIES -------------------------------
        /*
            --
            -- TEST COMMON CLIENT POINT REPORT INFO QUERIES
            --  

            DECLARE  @id_Client uniqueIdentifier; 
            SET @id_Client = '7670C94C-E82C-408E-A6E9-08F0CA7A757F'    

             --  1 Get Client Images     Раздел «Фото магазина»   
            select * from V_S8_ClientReportImages
            where id_client = @id_Client

             --  2 Get Client Info	     Раздел «Общая информация»   
            select * from V_S8_ClientReportInfo 
            where id_client = @id_Client

             --  3 Get Client  Trade Equipment   Раздел «Торговое Оборудование»	
            select * from V_S8_ClientReportEkspositor
            where id_client = @id_Client

            --  4 Get Client  Assortment   Раздел «Ассортимент»	
            select * from V_S8_ClientReportGoodsCategory
            where id_client = @id_Client

            -- 5 Get Client Rivals          Раздел «Конкуренты»
            select * from V_S8_ClientReportRival
            where id_client = @id_Client

            -- 6 Get Client Answers        Раздел «Анкеты»
            select * from V_S8_ClientReportAnswers
            where id_client = @id_Client


        */

        #endregion ------------------------SQL COMMON QUERIES -------------------------------


        #region ------------------------ TRANSFER TYPES REGISTER -----------------------------

        /// <summary>
        /// Transferring Types
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> TransferTypes()
        {
            return new[] { typeof(ClientReportInfoContainer)
                         , typeof(V_S8_ClientReportImages)
                         , typeof(V_S8_ClientReportInfo)
                         , typeof(V_S8_ClientReportEkspositor)
                         , typeof(V_S8_ClientReportGoodsCategory)
                         , typeof(V_S8_ClientReportRival)
                         , typeof(V_S8_ClientReportAnswers) };            
        }


        #endregion ------------------------ TRANSFER TYPES REGISTER -----------------------------


        #region ----------------------------- INTERNAL REPORT ITEM DATA STRUCTURE ---------------------------
         // internal 
         // Rep_1_ClientImages
         // Rep_2_ClientInfo
         // Rep_3_ClientEkspositor
         // Rep_4_ClientGoodsCategory
         // Rep_5_ClientRivals
         // Rep_6_ClientAnswers
         List<V_S8_ClientReportImages> Rep_1_ClientImages
        { get; set; } = new List<V_S8_ClientReportImages>();

         List<V_S8_ClientReportInfo> Rep_2_ClientInfo
        { get; set; } = new List<V_S8_ClientReportInfo>();
           
         List<V_S8_ClientReportEkspositor> Rep_3_ClientEkspositors
        { get; set; } = new List<V_S8_ClientReportEkspositor>();

         List<V_S8_ClientReportGoodsCategory> Rep_4_ClientGoodsCategories
        { get; set; } = new List<V_S8_ClientReportGoodsCategory>();

         List<V_S8_ClientReportRival> Rep_5_ClientRivals
        { get; set; } = new List<V_S8_ClientReportRival>();

         List<V_S8_ClientReportAnswers> Rep_6_ClientAnswers
        { get; set; } = new List<V_S8_ClientReportAnswers>();


        #endregion  ----------------------------- INTERNAL REPORT ITEM DATA STRUCTURE ---------------------------



        /// <summary>
        /// Client Report's Object Context, static instance that store data for One V_S8_Client always.
        /// </summary>
        public static ClientReportInfoContainer ReportItem
        { get; private set; } = new ClientReportInfoContainer(); //default empty Client Item reportInfo



        #region -------------------------PUBLIC REPORT OBJECT CONTEXT PROPERTIES --------------------------------


        /// <summary>
        /// 1 Get Client Images	           Раздел «Фото магазина»
        /// </summary>
        public static List<V_S8_ClientReportImages> ReportItem_Rep_1_ClientImages
        {  get { return ReportItem.Rep_1_ClientImages; } 
        }


        /// <summary>
        /// 2 Get Client Info	           Раздел «Общая информация»  
        /// </summary>
        public static List<V_S8_ClientReportInfo> ReportItem_Rep_2_ClientInfo
        {  get{ return ReportItem.Rep_2_ClientInfo; }
        }
        /// <summary>
        /// 3 Get Client  Trade Equipment  Раздел «Торговое Оборудование»
        /// </summary>
        public static List<V_S8_ClientReportEkspositor>  ReportItem_Rep_3_ClientEkspositors
        { get { return ReportItem.Rep_3_ClientEkspositors; }
        }

        /// <summary>
        /// 4 Get Client  Assortment       Раздел «Ассортимент»
        /// </summary>
        public static List<V_S8_ClientReportGoodsCategory>  ReportItem_Rep_4_ClientGoodsCategory
        { get { return ReportItem.Rep_4_ClientGoodsCategories; }
        }

        /// <summary>
        /// 5  Get Client Rivals           Раздел «Конкуренты»
        /// </summary>
        public static List<V_S8_ClientReportRival> ReportItem_Rep_5_ClientRivals
        { get { return ReportItem.Rep_5_ClientRivals; }
        }

        /// <summary>
        /// 6 Get Client Answers           Раздел «Анкеты»
        /// </summary>
        public static List<V_S8_ClientReportAnswers> ReportItem_Rep_6_ClientAnswers
        { get { return ReportItem.Rep_6_ClientAnswers; }
        }
        

#endregion ------------------------- REPORT OBJECT CONTEXT BINDING  --------------------------------


    


#if SERVER && NET45 // BEGIN: SERVER SIDE ONLY PROPERTIES & LOAD LOGIC


        /// <summary>
        ///  Target Client to collect all report data about it.
        /// </summary>
        public static string Current_IdClient
        { get; private set; }

#region ------------------------ SERVER SQL QUERIES -----------------------


        class Query //won't be serialized 
        {

            /// <summary>
            ///  Database Variable for report Queries
            /// </summary>
            public static string QueryDBName
            { get; } = "CRM_Cersanit2_test3";



            /// <summary>
            /// SQL Connection to CRM Database
            /// </summary>
            public static string ConnectionString
            { get; } = $"Data Source=LC-OLAPTEST;Initial Catalog={QueryDBName};User ID=sa;Password=cersanit";



            /// <summary>
            ///   
            /// </summary>
            /// <returns></returns>
            internal static DbManager CreateDBManager()
            {
                try
                {
                    return new DbManager(new SqlDataProvider(), ConnectionString);
                }
                catch (Exception exc)
                {
                    throw new InvalidOperationException($"ERROR IN {nameof(Query) }.{nameof(CreateDBManager)}() - Exception Message [{exc.Message}] ", exc);
                }
            }

            internal static string QuerySelectAllByID<TTable>(string IdPropertyName, string Id_GuidValue)
            {
                return $" SELECT * FROM [{QueryDBName}].[dbo].[{ typeof(TTable).Name}]  qt"
                       + $" WHERE qt.{IdPropertyName} = '{Id_GuidValue}' ";
            }


            /// <summary>
            /// Select collection of TTable as command inside Transaction 
            /// </summary>
            /// <typeparam name="TTable"></typeparam>
            /// <param name="selectQuery"></param>
            /// <param name="dbmngr"></param>
            /// <returns></returns>       
            internal static List<TTable> SelectCollection<TTable>( DbManager dbmngr, string idProperty, string idValue)
            {
                try
                {
                    var query = QuerySelectAllByID<TTable>(IdPropertyName: idProperty, Id_GuidValue: idValue);
                    return dbmngr.SetCommand(query).ExecuteList(typeof(TTable)).ToListOfT<TTable>();
                }
                catch (Exception exc)
                {
                    throw new InvalidOperationException($"ERROR in {nameof(ClientReportInfoContainer)}.{nameof(SelectCollection)}() - Exception Message [{exc.Message}] ", exc);
                }
            }


            /// <summary>
            /// Select item of TTable as command inside Transaction 
            /// </summary>
            /// <typeparam name="TTable"></typeparam>
            /// <param name="selectQuery"></param>
            /// <param name="dbmngr"></param>
            /// <returns></returns>
           internal static TTable SelectItem<TTable>(string selectQuery, DbManager dbmngr)
           {
                try
                {
                    return dbmngr.SetCommand(selectQuery).ExecuteObject<TTable>();
                }
                catch (Exception exc)
                {
                    throw new InvalidOperationException($"ERROR in {nameof(ClientReportInfoContainer)}.{nameof(SelectItem)}() - Exception Message [{exc.Message}] ", exc);
                }
            }          
                





        }
        

#endregion ------------------------ SERVER SQL QUERIES -----------------------

        /// <summary>
        /// ON TEST MODE - we Load ReportItem, in static Ctor, when we simple get this property.
        /// </summary>
        public static string StubProperty { get; set; }


        public static void Load_ClientReportInfoFromDB( Guid id_Client )
        {
            using (var dbManager = Query.CreateDBManager())
            {
                if (id_Client == Guid.Empty) throw new InvalidOperationException($"ERROR in {nameof(ClientReportInfoContainer)}.{nameof(Load_ClientReportInfoFromDB)}(): - param Id_Client can't be null or empty");
                Current_IdClient = id_Client.Str();

                /// 1 Get Client Info          Раздел «Фото магазина»
                ReportItem.Rep_1_ClientImages = Query.SelectCollection<V_S8_ClientReportImages>( dbManager, nameof(V_S8_ClientReportImages.id_client), Current_IdClient);

                // 2 Get Client Images          Раздел «Фото магазина»              
                ReportItem.Rep_2_ClientInfo = Query.SelectCollection<V_S8_ClientReportInfo>(dbManager, nameof(V_S8_ClientReportInfo.Id_Client), Current_IdClient);

                // 3 Get Client  Trade Equipment   Раздел «Торговое Оборудование»	
                ReportItem.Rep_3_ClientEkspositors = Query.SelectCollection<V_S8_ClientReportEkspositor>(dbManager, nameof(V_S8_ClientReportEkspositor.Id_client), Current_IdClient);

                // 4 Get Client GoodsCategory   Раздел «Товарные группы»
                ReportItem.Rep_4_ClientGoodsCategories= Query.SelectCollection<V_S8_ClientReportGoodsCategory>(dbManager, nameof(V_S8_ClientReportGoodsCategory.ID_Client), Current_IdClient);

                // 5 Get Client Rivals          Раздел «Конкуренты»
                ReportItem.Rep_5_ClientRivals = Query.SelectCollection<V_S8_ClientReportRival>(dbManager, nameof(V_S8_ClientReportRival.ID_Client), Current_IdClient);

                // 6 Get Client Answers        Раздел «Анкеты»
                ReportItem.Rep_6_ClientAnswers = Query.SelectCollection<V_S8_ClientReportAnswers>(dbManager, nameof(V_S8_ClientReportAnswers.ID_Client), Current_IdClient);

            } 
        }


#endif  //END: SERVER SIDE ONLY PROPERTIES & LOAD LOGIC



    }
}
