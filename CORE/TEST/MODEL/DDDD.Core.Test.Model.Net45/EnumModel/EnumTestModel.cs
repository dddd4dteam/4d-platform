﻿using System;
using System.Collections.Generic;

namespace DDDD.TSS.Tests.Model.EnumModel
{

    public enum SomeTypologyEn
    {
        NotDefined=0,
        Peoples=1,
        Animals=2,
        Birds=3,
        Reptiles=4,
        Plants=5
    }



    public enum SomeByteTypologyEn:byte
    {
        NotDefined = 0,
        Peoples = 1,
        Animals = 2,
        Birds = 3,
        Reptiles = 4,
        Plants = 5
    }


    public enum NotKnownEnumEn
    {
        NotDefined = 0,
        Peoples = 1,
        Animals = 2,
        Birds = 3,
        Reptiles = 4,
        Plants = 5
    }



    
    public class EnumTestClass
    {
        #region ------------------------- CTOR --------------------------

        public  EnumTestClass()
        {
            AllTypologyItemsPublic = new List<SomeTypologyEn>();
            
            BoxedNotKnownEnumPublicProperty = NotKnownEnumEn.Peoples;


            byte enumValueInByte = 3;

            var enumValue = Enum.ToObject(
                             typeof(SomeByteTypologyEn),
                             enumValueInByte
                            );

        }

        #endregion ------------------------- CTOR --------------------------


        #region ------------------------- PRIVATE MEMBERS---------------------------

        SomeTypologyEn? EnumFieldPrivate;

        public void SetEnumFieldPrivate(SomeTypologyEn anotherType)
        {
            EnumFieldPrivate  = anotherType;
            anotherType = EnumFieldPrivate.Value;
        }


        List<SomeTypologyEn> AllTypologyItemsPrivate
        {
            get;
            set;
        }


        public void AddNewTypologyItemPrivate(SomeTypologyEn newTypologyItem)
        {
            if (AllTypologyItemsPrivate == null) AllTypologyItemsPrivate = new List<SomeTypologyEn>();
            AllTypologyItemsPrivate.Add(newTypologyItem);
        }

        #endregion ------------------------- PRIVATE MEMBERS---------------------------


        #region -------------------------- PUBLIC MEMBERS -----------------------------

        public SomeTypologyEn EnumFieldPublic;

        public List<SomeTypologyEn> AllTypologyItemsPublic
        {
            get;
            set;
        }



        public object BoxedNotKnownEnumPublicProperty
        {
            get;
            set;
        }

        #endregion ------------------------- PUBLIC MEMBERS -----------------------------
        

    }
}
