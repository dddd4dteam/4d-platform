﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.StreamMessagesModel
{


    // MessageBase
    //sealed class U8Message : MessageBase
    //sealed class S16Message : MessageBase
    //sealed class S32Message : MessageBase
    //struct MyStruct1
    //struct MyStruct2
    //sealed class StructMessage : MessageBase
    //sealed class S64Message : MessageBase
    //enum MyEnum
    //sealed class PrimitivesMessage : MessageBase
    //sealed class BoxedPrimitivesMessage : MessageBase
    //sealed class ByteArrayMessage : MessageBase
    //sealed class IntArrayMessage : MessageBase
    //sealed class StringMessage : MessageBase
    //abstract class SimpleClassBase
    //sealed class SimpleClass : SimpleClassBase
    //interface IMyTest
    //sealed class SimpleClass2 : IMyTest
    //sealed class DictionaryMessage : MessageBase
    //sealed class ComplexMessage : MessageBase
    //sealed class CustomSerializersMessage : MessageBase
    
    
 public   abstract class MessageBase
    {
        public abstract void Compare(MessageBase msg);

        protected static Random s_rand = new Random(123);

        public static void ResetSeed()
        {
            s_rand = new Random(123);
        }

        protected static void A(bool b)
        {
            if (!b)
                throw new Exception();
        }

        public static MessageBase[] CreateMessages(Type type, int numMessages)
        {
            var arr = new MessageBase[numMessages];

            for (int i = 0; i < numMessages; ++i)
                arr[i] = (MessageBase)Activator.CreateInstance(type, s_rand);

            return arr;
        }

        static byte[] r64buf = new byte[8];
        protected static long GetRandomInt64(Random random)
        {
            // XXX produces quite big numbers
            random.NextBytes(r64buf);
            return BitConverter.ToInt64(r64buf, 0);
        }
    }



 public sealed class U8Message : MessageBase
    {
        
        byte m_val;

        public U8Message()
        {
        }

        public U8Message(Random r)
        {
            m_val = (byte)r.Next();
        }

        public override void Compare(MessageBase msg)
        {
            var m = (U8Message)msg;
            A(m_val == m.m_val);
        }
    }



 public sealed class S16Message : MessageBase
    {
        
        short m_val;

        public S16Message()
        {
        }

        public S16Message(Random r)
        {
            m_val = (short)r.Next();
        }

        public override void Compare(MessageBase msg)
        {
            var m = (S16Message)msg;
            A(m_val == m.m_val);
        }
    }



 public sealed class S32Message : MessageBase
    {
        
        int m_val;

        public S32Message()
        {
        }

        public S32Message(Random r)
        {
            m_val = (int)r.Next();
        }

        public override void Compare(MessageBase msg)
        {
            var m = (S32Message)msg;
            A(m_val == m.m_val);
        }
    }



 public struct MyStruct1
    {
        
        public byte m_byte;
        
        public int m_int;
        
        public long m_long;
    }



 public struct MyStruct2
    {
        
        public string m_string;
        
        public int m_int;
    }



 public sealed class StructMessage : MessageBase
    {
        
        MyStruct1 m_struct1;

        
        MyStruct2 m_struct2;

        public StructMessage()
        {
        }

        public StructMessage(Random r)
        {
            m_struct1.m_byte = (byte)r.Next();
            m_struct1.m_int = r.Next();
            m_struct1.m_long = (long)r.Next() + (long)r.Next();

            m_struct2.m_string = new string((char)r.Next((int)'a', (int)'z'), r.Next(0, 20));
            m_struct2.m_int = r.Next();
        }

        public override void Compare(MessageBase msg)
        {
            var m = (StructMessage)msg;
            A(m_struct1.Equals(m.m_struct1));

            A(m_struct2.Equals(m.m_struct2));
        }
    }



 public sealed class S64Message : MessageBase
    {
        
        long m_val;

        public S64Message()
        {
        }

        public S64Message(Random r)
        {
            m_val = GetRandomInt64(r);
        }

        public override void Compare(MessageBase msg)
        {
            var m = (S64Message)msg;
            A(m_val == m.m_val);
        }
    }
    


 public enum MyEnum
    {
        Zero = 0,
        One,
        Two,
        Three,
        Four,
        Five,
    }



 public sealed class PrimitivesMessage : MessageBase
    {
        
        bool m_bool;

        
        byte m_byte;
        
        sbyte m_sbyte;
        
        char m_char;
        
        ushort m_ushort;
       
        short m_short;
       
        uint m_uint;
        
        int m_int;
        
        ulong m_ulong;
        
        long m_long;

        
        float m_single;
        
        double m_double;

        
        MyEnum m_enum;

        
        DateTime m_date;

        public PrimitivesMessage()
        {
        }

        public PrimitivesMessage(Random r)
        {
            m_char = r.Next().ToString()[0];
            m_sbyte = (sbyte)r.Next();
            m_bool = (r.Next() & 1) == 1;
            m_byte = (byte)r.Next();

            m_uint = (uint)r.Next();
            m_int = (int)r.Next();
            m_ulong = (ulong)r.Next();
            m_long = (long)r.Next();
            m_short = (short)r.Next();


            m_int = r.Next();
            m_double = r.NextDouble();
            m_enum = (MyEnum)r.Next(0, 6);
            m_date = DateTime.Now;
            m_ushort = (ushort)r.Next();
           
            //m_single = (float)r.NextDouble();            
            //byte[] utf8 = System.Text.Encoding.Unicode.GetBytes(r.Next().ToString());
           
          
        }

        public override void Compare(MessageBase msg)
        {
            var m = (PrimitivesMessage)msg;

            A(m_bool == m.m_bool);

            A(m_byte == m.m_byte);
            A(m_sbyte == m.m_sbyte);
            A(m_char == m.m_char);
            A(m_ushort == m.m_ushort);
            A(m_short == m.m_short);
            A(m_uint == m.m_uint);
            A(m_int == m.m_int);
            A(m_ulong == m.m_ulong);
            A(m_long == m.m_long);

            A(m_single == m.m_single);
            A(m_double == m.m_double);

            A(m_enum == m.m_enum);

            A(m_date == m.m_date);
        }
    }


 public sealed class BoxedPrimitivesMessage : MessageBase
    {
        object m_bool;

        object m_byte;
        object m_int;
        object m_long;

        object m_enum;

        public BoxedPrimitivesMessage()
        {
        }

        public BoxedPrimitivesMessage(Random r)
        {
            m_bool = (r.Next() & 1) == 1;
            m_byte = (byte)r.Next();
            m_int = (int)r.Next();
            m_long = (long)r.Next();

            m_int = r.Next();

            m_enum = (MyEnum)r.Next(0, 6);
        }

        public override void Compare(MessageBase msg)
        {
            var m = (BoxedPrimitivesMessage)msg;

            A((bool)m_bool == (bool)m.m_bool);

            A((byte)m_byte == (byte)m.m_byte);
            A((int)m_int == (int)m.m_int);
            A((long)m_long == (long)m.m_long);

            A((MyEnum)m_enum == (MyEnum)m.m_enum);
        }
    }
    

 public sealed class ByteArrayMessage : MessageBase
    {
        
        byte[] m_byteArr;

        public ByteArrayMessage()
        {
        }

        public ByteArrayMessage(Random r)
        {
            int len = r.Next(100000);

            if (len == 0)
            {
                m_byteArr = null;
            }
            else
            {
                m_byteArr = new byte[len - 1];
                for (int i = 0; i < m_byteArr.Length; ++i)
                    m_byteArr[i] = (byte)i;
            }
        }

        public override void Compare(MessageBase msg)
        {
            var m = (ByteArrayMessage)msg;

            if (m_byteArr == null)
            {
                A(m_byteArr == m.m_byteArr);
            }
            else
            {
                for (int i = 0; i < m_byteArr.Length; ++i)
                    A(m_byteArr[i] == m.m_byteArr[i]);
            }
        }
    }



 public sealed class IntArrayMessage : MessageBase
    {
        
        int[] m_intArr;

        public IntArrayMessage()
        {
        }

        public IntArrayMessage(Random r)
        {
            int len = r.Next(100000);

            if (len == 0)
            {
                m_intArr = null;
            }
            else
            {
                m_intArr = new int[len - 1];
                for (int i = 0; i < m_intArr.Length; ++i)
                    m_intArr[i] = r.Next();
            }
        }

        public override void Compare(MessageBase msg)
        {
            var m = (IntArrayMessage)msg;

            if (m_intArr == null)
            {
                A(m_intArr == m.m_intArr);
            }
            else
            {
                for (int i = 0; i < m_intArr.Length; ++i)
                    A(m_intArr[i] == m.m_intArr[i]);
            }
        }
    }



 public sealed class StringMessage : MessageBase
    {
        
        string m_string;

        public StringMessage()
        {
        }

        public StringMessage(Random r)
        {
            int len = r.Next(100);

            if (len == 0)
                m_string = null;
            else
                //m_string = new string((char)r.Next(0xD7FF), len - 1);
                m_string = new string((char)r.Next((int)'a', (int)'z'), len - 1);
        }

        public override void Compare(MessageBase msg)
        {
            var m = (StringMessage)msg;

            A(m_string == m.m_string);
        }
    }




 public abstract class SimpleClassBase
    {
        
        int m_val;

        protected SimpleClassBase()
        {
        }

        protected SimpleClassBase(Random r)
        {
            m_val = r.Next();
        }

        public void Compare(SimpleClassBase other)
        {
            if (m_val != other.m_val)
                throw new Exception();
        }
    }



 public sealed class SimpleClass : SimpleClassBase
    {
        
        long m_val;

        public SimpleClass()
        {
        }

        public SimpleClass(Random r)
            : base(r)
        {
            m_val = (long)r.Next();
        }

        public void Compare(SimpleClass other)
        {
            if (m_val != other.m_val)
                throw new Exception();

            base.Compare(other);
        }
    }



 public interface IMyTest
    {
    }



 public sealed class SimpleClass2 : IMyTest
    {
        
        long m_val;

        public SimpleClass2()
        {
        }

        public SimpleClass2(Random r)
        {
            m_val = (long)r.Next();
        }

        public void Compare(SimpleClass2 other)
        {
            if (m_val != other.m_val)
                throw new Exception();
        }
    }



 public sealed class DictionaryMessage : MessageBase
    {
        
        Dictionary<int, int> m_intMap;

        
        Dictionary<string, SimpleClass2> m_obMap;

        public DictionaryMessage()
        {
        }

        public DictionaryMessage(Random r)
        {
            var len = r.Next(0, 1000);//0
            if (len > 0)
            {
                m_intMap = new Dictionary<int, int>(len);
                for (int i = 0; i < len; ++i)
                    m_intMap[r.Next()] = r.Next();
            }

            len = r.Next(0, 100);//0
            if (len > 0)
            {
                m_obMap = new Dictionary<string, SimpleClass2>();
                for (int i = 0; i < len; ++i)
                {
                    var str = i.ToString();
                    m_obMap[str] = new SimpleClass2(r);
                }
            }
        }

        public override void Compare(MessageBase msg)
        {
            var m = (DictionaryMessage)msg;

            if (m_intMap == null)
                A(m_intMap == m.m_intMap);
            else
            {
                A(m_intMap.Count == m.m_intMap.Count);
                foreach (var kvp in m_intMap)
                    A(kvp.Value == m.m_intMap[kvp.Key]);
            }

            if (m_obMap == null)
                A(m_obMap == m.m_obMap);
            else
            {
                A(m_obMap.Count == m.m_obMap.Count);
                foreach (var kvp in m_obMap)
                    kvp.Value.Compare(m.m_obMap[kvp.Key]);
            }
        }
    }



 public sealed class ComplexMessage : MessageBase
    {
        
        S16Message m_msg;

        
        SimpleClass m_sealedClass;

        
        SimpleClassBase m_abstractMsg;

        
        IMyTest m_ifaceMsg;

        public ComplexMessage()
        {
        }

        public ComplexMessage(Random r)
        {
            if (r.Next(100) == 0)
                m_msg = null;
            else
                m_msg = new S16Message(r);

            if (r.Next(100) == 0)
                m_sealedClass = null;
            else
                m_sealedClass = new SimpleClass(r);

            if (r.Next(100) == 0)
                m_abstractMsg = null;
            else
                m_abstractMsg = new SimpleClass(r);

            if (r.Next(100) == 0)
                m_ifaceMsg = null;
            else
                m_ifaceMsg = new SimpleClass2(r);
        }

        public override void Compare(MessageBase msg)
        {
            var m = (ComplexMessage)msg;

            if (m_msg == null)
                A(m_msg == m.m_msg);
            else
                m_msg.Compare(m.m_msg);

            if (m_sealedClass == null)
                A(m_sealedClass == m.m_sealedClass);
            else
                m_sealedClass.Compare(m.m_sealedClass);

            if (m_abstractMsg == null)
                A(m_abstractMsg == m.m_abstractMsg);
            else
                ((SimpleClass)m_abstractMsg).Compare((SimpleClass)m.m_abstractMsg);

            if (m_ifaceMsg == null)
                A(m_ifaceMsg == m.m_ifaceMsg);
            else
                ((SimpleClass2)m_ifaceMsg).Compare((SimpleClass2)m.m_ifaceMsg);
        }
    }



 public sealed class CustomSerializersMessage : MessageBase
    {
        
        int[, ,] m_int3Arr;

        public CustomSerializersMessage()
        {
        }

        public CustomSerializersMessage(Random r)
        {
            int lx = r.Next(100) + 1;
            int ly = r.Next(70) + 1;
            int lz = r.Next(40) + 1;

            m_int3Arr = new int[lz, ly, lx];

            for (int z = 0; z < lz; ++z)
                for (int y = 0; y < ly; ++y)
                    for (int x = 0; x < lx; ++x)
                        m_int3Arr[z, y, x] = r.Next();
        }

        public override void Compare(MessageBase msg)
        {
            var m = (CustomSerializersMessage)msg;

            int lz = m_int3Arr.GetLength(0);
            int ly = m_int3Arr.GetLength(1);
            int lx = m_int3Arr.GetLength(2);

            for (int z = 0; z < lz; ++z)
                for (int y = 0; y < ly; ++y)
                    for (int x = 0; x < lx; ++x)
                        A(m_int3Arr[z, y, x] == m.m_int3Arr[z, y, x]);
        }
    }
}
