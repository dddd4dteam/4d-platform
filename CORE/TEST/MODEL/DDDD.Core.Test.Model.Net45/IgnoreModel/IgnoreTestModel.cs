﻿
using DDDD.Core.Tests.Model.CommunicationModel;
using System;

namespace DDDD.Core.Tests.Model.Ignore
{
    public class IgnoreMemberClass
    {
        public IgnoreMemberClass()
        {
            PublicIgnoredProperty="DefaultValue";
        }
        /// <summary>
        /// Private member that  is exposed as storing field /instead of excluded property PublicIgnoredProperty
        /// </summary>
        String PrivateNotIgnoredStringField;


        public void SetPrivateNotIgnoredStringField(String newValue)
        {
            PrivateNotIgnoredStringField = newValue;
        }


        /// <summary>
        /// Ignored Property by Ignore attribute declared in [DDDD.Core.Test.Model.Net45]  assembly, not in core lib.
        /// Attribute with such name can be declared everywhere and used to ignore some  member. 
        /// Another way is  Only_MEMBERS policy using - to include and exclude members you need, 
        /// even for types that exist in external second party assemblies .
        /// </summary>
        [IgnoreMember]
        public String PublicIgnoredProperty
        { 
          get; 
          set; 
        }


        /// <summary>
        /// false - not changed and is default-  very well -  PublicIgnoredProperty was ignored from processing.
        /// true- PublicIgnoredProperty was processed it s error
        /// </summary>
        /// <param name="otherInstance"></param>
        /// <returns></returns>
        public bool IsPublicIgnoredPropertyNotDefault()
        {
            return PublicIgnoredProperty != "DefaultValue";
        }

    }
}
