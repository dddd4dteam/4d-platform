﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.ReflectionModel
{



    public struct RProductMark
    {
        public String Copyrighter;
        public String Mark;
    }




    public class RProductSimple
    {
        public RProductMark Mark { get; set; }

        public RProductMark? MarkNullable { get; set; }
    }



    public class RProduct 
    {

        public RProduct()
        {

            PartsProductMarks
                = new List<RProductMark>() 
                                {
                                    new RProductMark() { Mark="Calibry1", Copyrighter="Writer1"}
                                    ,new RProductMark() { Mark="Calibry155", Copyrighter="Writer2"}
                                };
        }


       
        public int Id { get; set; }

       
        public string Name { get; set; }

     
        public string Category { get; set; }

     
        public decimal? Price { get; set; }


        public uint Length { get; set; }


        public Int32 PublicIntField;

        public RProductMark Mark { get; set; }

        public RProductMark? MarkNullable { get; set; }

        public List<String> PartsNames { get; set; }

        public List<RProductMark> PartsProductMarks { get; set; }
               

        public bool IsReadOnly
        {
            get { return false; }
        }


    }

    public struct RProductStruct 
    {
        
        public int Id { get; set; }

        
        public string Name { get; set; }

        
        public string Category { get; set; }

        
        public decimal? Price { get; set; }
        
        
        public uint Length { get; set; }
        

        public Int32 PublicIntField;


        public RProductMark Mark { get; set; }


        public List<String> PartsNames { get; set; }


        public List<RProductMark> PartsProductMarks { get; set; }
        

        public bool IsReadOnly
        {
            get { return false; }
        }
        
    }


    public interface IBaseContract
    {
        string BaseStringPropertyWithSetPublic { get; set; }
    }

    public enum PointKindEn
    {
       NotDefined, Point, Circle, Line, Ellipse
    }

    public abstract class BaseContract : IBaseContract
    {

        protected string BaseStringField;

        string _BaseStringPropertyNoSet = "value1";

        protected readonly string BaseFieldsNoSettable = "Value of field";

        string BaseStringPropertyNoSet
        {
            get { return _BaseStringPropertyNoSet; }
        }

        string BaseStringPropertyWithSet
        {
            get;
            set;// { return _BaseStringPropertyNoSet; }
        }


        public string BaseStringPropertyWithSetPublic
        {
            get;
            set;
        }


        protected string BaseStringPropertyWithSetProtected
        {
            get;
            set;
        }

    }


    public class TestReflectionContract : BaseContract
    {
        bool _PropertyNoSetAccesor = true;
        public bool PropertyNoSetAccesor
        {
            get { return _PropertyNoSetAccesor; }
            //set;
        }

        public bool PropertyWithSetAccesor
        {
            get;
            set;
        }

        public bool PropertyWithSetAccesor2
        {
            get;
            set;
        }

        const String ConstField = "Const Value1";

        readonly string FieldsNoSettable = "Valueof field";

        string FieldsWithSettable = "Valueof field";

        static String StaticProperty
        {
            get;
            set;
        }


    }


    public struct SubStructType
    {
        Int32 PrivateIntField;
        string PrivateStringField;

        public string PublicStringField;
        public int PublicProperty
        {
            get;
            set;
        }
    }


    public struct SomeStructType
    {
        Int32 PrivateIntField;
        
        public Int32 GetPrivateIntField()
        { return PrivateIntField; }


        string PrivateStringField;

        public string PublicStringField;
        public int PublicIntProperty
        {
            get;
            set;
        }

        SubStructType PrivateSubStructField;
    }


    public class SomeClassType
    {
        Int32 PrivateField;

        public int PublicProperty
        {
            get;
            set;
        }


        List<SomeStructType> PrivatePropertyListOfSomeStructType
        { get; set; }

    }



    public class ContractMemberAnalyzeException : Exception
    {

        internal ContractMemberAnalyzeException() { }

        internal ContractMemberAnalyzeException(string message) : base(message) { }

        internal ContractMemberAnalyzeException(String Message, String Member) :
            base(String.Format(Message, Member)) { }


        internal ContractMemberAnalyzeException(String Message, Type Contract) :
            base(String.Format(Message, Contract.FullName)) { }

        internal ContractMemberAnalyzeException(String Message, String Member, Type MemberType) :
            base(String.Format(Message, Member, MemberType.FullName)) { }


        internal ContractMemberAnalyzeException(String Message, Type Contract, String MemberName) :
            base(String.Format(Message, Contract.FullName, MemberName)) { }


        internal ContractMemberAnalyzeException(string message, Exception inner) : base(message, inner) { }


        //protected ContractMemberAnalyzeException(
        //  System.Runtime.Serialization.SerializationInfo info,
        //  System.Runtime.Serialization.StreamingContext context)
        //    : base(info, context) { }
    }


}
