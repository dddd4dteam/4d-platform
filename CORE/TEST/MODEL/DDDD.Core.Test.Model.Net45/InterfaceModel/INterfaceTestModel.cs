﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.Tests.Model.InterfaceModel
{
    public interface IStringQuality
    {
        String StringValue { get; }
    }


    public interface IIntQuality
    {
        Int32 IntValue { get; }
    }


    public class InterfacesTestContract1 : IStringQuality, IIntQuality
    {

        public string StringValue
        {
            get; private set;
        }


        public int IntValue
        {
            get; private set;
        }




    }


}
