﻿
using DDDD.Core.Model.CommunicationModel;
using System;
using System.Collections.Generic;

namespace DDDD.TSS.Tests.Model.Polimorphism 
{

    public interface IGudjet
    {
        int ID { get; set; }
    }



    public struct GudjetStruct: IGudjet
    {
        public int ID
        { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }        

        public int YearCreated;// field to get/set

    }

    public class GudjetBase : IGudjet
    {
        public int ID
        { get; set; }
    }


    public class Gudjet1 : GudjetBase
    {  
        public string Name { get; set; }

        public string Category { get; set; }

        public int YearCreated;// field to get/set
                      
    }

    public class Gudjet2 : GudjetBase
    {
        public string Name { get; set; }
        
        public int Length    { get; set; }

    }

    public class Gudjet3 : GudjetBase
    {
        public string Name { get; set; }

        public string Color { get; set; }
    }


     
     
     
     


}
