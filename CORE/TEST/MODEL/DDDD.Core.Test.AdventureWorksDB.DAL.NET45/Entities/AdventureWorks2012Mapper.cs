using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using NHibernate.Mapping.ByCode;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.Entities
{
	static partial class AdventureWorks2012Mapper
	{
		internal static void MapToModel(ModelMapper mapper)
		{

			// Default mappings for ScrapReason
			mapper.Class<ScrapReason>(
				mp =>
				{
					mp.Table("ScrapReason");

					mp.Id(
						x => x.ScrapReasonID,
						map =>
						{
							map.Column("ScrapReasonID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ScrapReason_Name");
							});

					mp.Bag(
						x => x.WorkOrder,
						map => map.Key(m => m.Column("ScrapReasonID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(WorkOrder));
								}));
				});

			// Default mappings for Shift
			mapper.Class<Shift>(
				mp =>
				{
					mp.Table("Shift");

					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartTime,
						map =>
						{
							map.Column("StartTime");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndTime,
						map =>
						{
							map.Column("EndTime");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Shift_Name");
							});

					mp.Property(
						x => x.StartTime,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Shift_StartTime_EndTime");
							});

					mp.Property(
						x => x.EndTime,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Shift_StartTime_EndTime");
							});

					mp.Id(
						x => x.ShiftID,
						map =>
						{
							map.Column("ShiftID");
							map.Generator(Generators.Foreign<Shift>(a => a.EmployeeDepartmentHistory));
						});					
					mp.OneToOne(m => m.EmployeeDepartmentHistory,
						map => map.Constrained(true));
				});

			// Default mappings for ProductCategory
			mapper.Class<ProductCategory>(
				mp =>
				{
					mp.Table("ProductCategory");

					mp.Id(
						x => x.ProductCategoryID,
						map =>
						{
							map.Column("ProductCategoryID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductCategory_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductCategory_rowguid");
							});

					mp.Bag(
						x => x.ProductSubcategory,
						map => map.Key(m => m.Column("ProductCategoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(ProductSubcategory));
								}));
				});

			// Default mappings for ShipMethod
			mapper.Class<ShipMethod>(
				mp =>
				{
					mp.Table("ShipMethod");

					mp.Id(
						x => x.ShipMethodID,
						map =>
						{
							map.Column("ShipMethodID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipBase,
						map =>
						{
							map.Column("ShipBase");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipRate,
						map =>
						{
							map.Column("ShipRate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ShipMethod_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ShipMethod_rowguid");
							});

					mp.Bag(
						x => x.PurchaseOrderHeader,
						map => map.Key(m => m.Column("ShipMethodID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(PurchaseOrderHeader));
								}));
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("ShipMethodID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
				});

			// Default mappings for ProductCostHistory
			mapper.Class<ProductCostHistory>(
				mp =>
				{
					mp.Table("ProductCostHistory");

					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.StandardCost,
						map =>
						{
							map.Column("StandardCost");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductCostHistory>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
				});

			// Default mappings for ProductDescription
			mapper.Class<ProductDescription>(
				mp =>
				{
					mp.Table("ProductDescription");

					mp.Id(
						x => x.ProductDescriptionID,
						map =>
						{
							map.Column("ProductDescriptionID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Description,
						map =>
						{
							map.Column("Description");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductDescription_rowguid");
							});

					mp.OneToOne(m => m.ProductModelProductDescriptionCulture,
								map =>
									{ });
				});

			// Default mappings for ShoppingCartItem
			mapper.Class<ShoppingCartItem>(
				mp =>
				{
					mp.Table("ShoppingCartItem");

					mp.Id(
						x => x.ShoppingCartItemID,
						map =>
						{
							map.Column("ShoppingCartItemID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ShoppingCartID,
						map =>
						{
							map.Column("ShoppingCartID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Quantity,
						map =>
						{
							map.Column("Quantity");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.DateCreated,
						map =>
						{
							map.Column("DateCreated");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.ShoppingCartID,
						map => map.Index("IX_ShoppingCartItem_ShoppingCartID_ProductID"));


					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for ProductDocument
			mapper.Class<ProductDocument>(
				mp =>
				{
					mp.Table("ProductDocument");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductDocument>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
					mp.Id(
						x => x.DocumentNode,
						map =>
						{
							map.Column("DocumentNode");
							map.Generator(Generators.Foreign<ProductDocument>(a => a.Document));
						});					
					mp.OneToOne(m => m.Document,
						map => map.Constrained(true));
				});

			// Default mappings for DatabaseLog
			mapper.Class<DatabaseLog>(
				mp =>
				{
					mp.Table("DatabaseLog");

					mp.Id(
						x => x.DatabaseLogID,
						map =>
						{
							map.Column("DatabaseLogID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.PostTime,
						map =>
						{
							map.Column("PostTime");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.DatabaseUser,
						map =>
						{
							map.Column("DatabaseUser");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Event,
						map =>
						{
							map.Column("Event");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Schema,
						map =>
						{
							map.Column("Schema");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Object,
						map =>
						{
							map.Column("Object");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TSQL,
						map =>
						{
							map.Column("TSQL");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.XmlEvent,
						map =>
						{
							map.Column("XmlEvent");
							map.NotNullable(true);
						});



				});

			// Default mappings for ProductInventory
			mapper.Class<ProductInventory>(
				mp =>
				{
					mp.Table("ProductInventory");

					mp.Property(
						x => x.Shelf,
						map =>
						{
							map.Column("Shelf");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Bin,
						map =>
						{
							map.Column("Bin");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Quantity,
						map =>
						{
							map.Column("Quantity");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductInventory>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
					mp.Id(
						x => x.LocationID,
						map =>
						{
							map.Column("LocationID");
							map.Generator(Generators.Foreign<ProductInventory>(a => a.Location));
						});					
					mp.OneToOne(m => m.Location,
						map => map.Constrained(true));
				});

			// Default mappings for SpecialOffer
			mapper.Class<SpecialOffer>(
				mp =>
				{
					mp.Table("SpecialOffer");

					mp.Id(
						x => x.SpecialOfferID,
						map =>
						{
							map.Column("SpecialOfferID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Description,
						map =>
						{
							map.Column("Description");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.DiscountPct,
						map =>
						{
							map.Column("DiscountPct");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Type,
						map =>
						{
							map.Column("Type");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Category,
						map =>
						{
							map.Column("Category");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartDate,
						map =>
						{
							map.Column("StartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MinQty,
						map =>
						{
							map.Column("MinQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MaxQty,
						map =>
						{
							map.Column("MaxQty");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SpecialOffer_rowguid");
							});

					mp.OneToOne(m => m.SpecialOfferProduct,
								map =>
									{ });
				});

			// Default mappings for ErrorLog
			mapper.Class<ErrorLog>(
				mp =>
				{
					mp.Table("ErrorLog");

					mp.Id(
						x => x.ErrorLogID,
						map =>
						{
							map.Column("ErrorLogID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ErrorTime,
						map =>
						{
							map.Column("ErrorTime");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.UserName,
						map =>
						{
							map.Column("UserName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ErrorNumber,
						map =>
						{
							map.Column("ErrorNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ErrorSeverity,
						map =>
						{
							map.Column("ErrorSeverity");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ErrorState,
						map =>
						{
							map.Column("ErrorState");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ErrorProcedure,
						map =>
						{
							map.Column("ErrorProcedure");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ErrorLine,
						map =>
						{
							map.Column("ErrorLine");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ErrorMessage,
						map =>
						{
							map.Column("ErrorMessage");
							map.NotNullable(true);
						});



				});

			// Default mappings for ProductListPriceHistory
			mapper.Class<ProductListPriceHistory>(
				mp =>
				{
					mp.Table("ProductListPriceHistory");

					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ListPrice,
						map =>
						{
							map.Column("ListPrice");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductListPriceHistory>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
				});

			// Default mappings for Address
			mapper.Class<Address>(
				mp =>
				{
					mp.Table("Address");

					mp.Id(
						x => x.AddressID,
						map =>
						{
							map.Column("AddressID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceID,
						map =>
						{
							map.Column("StateProvinceID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SpatialLocation,
						map =>
						{
							map.Column("SpatialLocation");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Address_rowguid");
							});

					mp.Property(
						x => x.AddressLine1,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("IX_Address_AddressLine1_AddressLine2_City_StateProvinceID_PostalCode");
							});

					mp.Property(
						x => x.AddressLine2,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("IX_Address_AddressLine1_AddressLine2_City_StateProvinceID_PostalCode");
							});

					mp.Property(
						x => x.City,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("IX_Address_AddressLine1_AddressLine2_City_StateProvinceID_PostalCode");
							});

					mp.Property(
						x => x.PostalCode,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("IX_Address_AddressLine1_AddressLine2_City_StateProvinceID_PostalCode");
							});

					mp.ManyToOne<StateProvince>(
						x => x.StateProvince,
						map =>
						{
							map.Column("StateProvinceID");
							map.Class(typeof(StateProvince));
							map.ForeignKey("StateProvinceID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.OneToOne(m => m.BusinessEntityAddress,
								map =>
									{ });
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("BillToAddressID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
					mp.Bag(
						x => x.SalesOrderHeader_,
						map => map.Key(m => m.Column("ShipToAddressID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
				});

			// Default mappings for SpecialOfferProduct
			mapper.Class<SpecialOfferProduct>(
				mp =>
				{
					mp.Table("SpecialOfferProduct");

					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SpecialOfferProduct_rowguid");
							});

					mp.Bag(
						x => x.SalesOrderDetail,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderDetail));
								}));
					mp.Id(
						x => x.SpecialOfferID,
						map =>
						{
							map.Column("SpecialOfferID");
							map.Generator(Generators.Foreign<SpecialOfferProduct>(a => a.SpecialOffer));
						});					
					mp.OneToOne(m => m.SpecialOffer,
						map => map.Constrained(true));
					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<SpecialOfferProduct>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
				});

			// Default mappings for ProductModel
			mapper.Class<ProductModel>(
				mp =>
				{
					mp.Table("ProductModel");

					mp.Id(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CatalogDescription,
						map =>
						{
							map.Column("CatalogDescription");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Instructions,
						map =>
						{
							map.Column("Instructions");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.CatalogDescription,
						map => map.Index("PXML_ProductModel_CatalogDescription"));

					mp.Property(
						x => x.Instructions,
						map => map.Index("PXML_ProductModel_Instructions"));


					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductModel_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductModel_rowguid");
							});

					mp.Bag(
						x => x.Product,
						map => map.Key(m => m.Column("ProductModelID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Product));
								}));
					mp.OneToOne(m => m.ProductModelIllustration,
								map =>
									{ });
					mp.OneToOne(m => m.ProductModelProductDescriptionCulture,
								map =>
									{ });
				});

			// Default mappings for AddressType
			mapper.Class<AddressType>(
				mp =>
				{
					mp.Table("AddressType");

					mp.Id(
						x => x.AddressTypeID,
						map =>
						{
							map.Column("AddressTypeID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_AddressType_rowguid");
							});

					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_AddressType_Name");
							});

					mp.OneToOne(m => m.BusinessEntityAddress,
								map =>
									{ });
				});

			// Default mappings for StateProvince
			mapper.Class<StateProvince>(
				mp =>
				{
					mp.Table("StateProvince");

					mp.Id(
						x => x.StateProvinceID,
						map =>
						{
							map.Column("StateProvinceID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.StateProvinceCode,
						map =>
						{
							map.Column("StateProvinceCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionCode,
						map =>
						{
							map.Column("CountryRegionCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.IsOnlyStateProvinceFlag,
						map =>
						{
							map.Column("IsOnlyStateProvinceFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_StateProvince_Name");
							});

					mp.Property(
						x => x.StateProvinceCode,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_StateProvince_StateProvinceCode_CountryRegionCode");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_StateProvince_rowguid");
							});

					mp.Bag(
						x => x.Address,
						map => map.Key(m => m.Column("StateProvinceID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Address));
								}));
					mp.Bag(
						x => x.SalesTaxRate,
						map => map.Key(m => m.Column("StateProvinceID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesTaxRate));
								}));
					mp.ManyToOne<CountryRegion>(
						x => x.CountryRegion,
						map =>
						{
							map.Column("CountryRegionCode");
							map.Class(typeof(CountryRegion));
							map.ForeignKey("CountryRegionCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<SalesTerritory>(
						x => x.SalesTerritory,
						map =>
						{
							map.Column("TerritoryID");
							map.Class(typeof(SalesTerritory));
							map.ForeignKey("TerritoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for ProductModelIllustration
			mapper.Class<ProductModelIllustration>(
				mp =>
				{
					mp.Table("ProductModelIllustration");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.Generator(Generators.Foreign<ProductModelIllustration>(a => a.ProductModel));
						});					
					mp.OneToOne(m => m.ProductModel,
						map => map.Constrained(true));
					mp.Id(
						x => x.IllustrationID,
						map =>
						{
							map.Column("IllustrationID");
							map.Generator(Generators.Foreign<ProductModelIllustration>(a => a.Illustration));
						});					
					mp.OneToOne(m => m.Illustration,
						map => map.Constrained(true));
				});

			// Default mappings for AWBuildVersion
			mapper.Class<AWBuildVersion>(
				mp =>
				{
					mp.Table("AWBuildVersion");

					mp.Id(
						x => x.SystemInformationID,
						map =>
						{
							map.Column("SystemInformationID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.Database_Version,
						map =>
						{
							map.Column("Database Version");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.VersionDate,
						map =>
						{
							map.Column("VersionDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for ProductModelProductDescriptionCulture
			mapper.Class<ProductModelProductDescriptionCulture>(
				mp =>
				{
					mp.Table("ProductModelProductDescriptionCulture");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.Generator(Generators.Foreign<ProductModelProductDescriptionCulture>(a => a.ProductModel));
						});					
					mp.OneToOne(m => m.ProductModel,
						map => map.Constrained(true));
					mp.Id(
						x => x.ProductDescriptionID,
						map =>
						{
							map.Column("ProductDescriptionID");
							map.Generator(Generators.Foreign<ProductModelProductDescriptionCulture>(a => a.ProductDescription));
						});					
					mp.OneToOne(m => m.ProductDescription,
						map => map.Constrained(true));
					mp.Id(
						x => x.CultureID,
						map =>
						{
							map.Column("CultureID");
							map.Generator(Generators.Foreign<ProductModelProductDescriptionCulture>(a => a.Culture));
						});					
					mp.OneToOne(m => m.Culture,
						map => map.Constrained(true));
				});

			// Default mappings for BillOfMaterials
			mapper.Class<BillOfMaterials>(
				mp =>
				{
					mp.Table("BillOfMaterials");

					mp.Id(
						x => x.BillOfMaterialsID,
						map =>
						{
							map.Column("BillOfMaterialsID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ProductAssemblyID,
						map =>
						{
							map.Column("ProductAssemblyID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ComponentID,
						map =>
						{
							map.Column("ComponentID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartDate,
						map =>
						{
							map.Column("StartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.UnitMeasureCode,
						map =>
						{
							map.Column("UnitMeasureCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.BOMLevel,
						map =>
						{
							map.Column("BOMLevel");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PerAssemblyQty,
						map =>
						{
							map.Column("PerAssemblyQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.StartDate,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_BillOfMaterials_ProductAssemblyID_ComponentID_StartDate");
							});

					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductAssemblyID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Product>(
						x => x.Product_,
						map =>
						{
							map.Column("ComponentID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<UnitMeasure>(
						x => x.UnitMeasure,
						map =>
						{
							map.Column("UnitMeasureCode");
							map.Class(typeof(UnitMeasure));
							map.ForeignKey("UnitMeasureCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for Store
			mapper.Class<Store>(
				mp =>
				{
					mp.Table("Store");

					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesPersonID,
						map =>
						{
							map.Column("SalesPersonID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Demographics,
						map =>
						{
							map.Column("Demographics");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.Demographics,
						map => map.Index("PXML_Store_Demographics"));


					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Store_rowguid");
							});

					mp.Bag(
						x => x.Customer,
						map => map.Key(m => m.Column("StoreID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Customer));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Store>(a => a.BusinessEntity));
						});					
					mp.OneToOne(m => m.BusinessEntity,
						map => map.Constrained(true));
					mp.ManyToOne<SalesPerson>(
						x => x.SalesPerson,
						map =>
						{
							map.Column("SalesPersonID");
							map.Class(typeof(SalesPerson));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for ProductPhoto
			mapper.Class<ProductPhoto>(
				mp =>
				{
					mp.Table("ProductPhoto");

					mp.Id(
						x => x.ProductPhotoID,
						map =>
						{
							map.Column("ProductPhotoID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ThumbNailPhoto,
						map =>
						{
							map.Column("ThumbNailPhoto");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ThumbnailPhotoFileName,
						map =>
						{
							map.Column("ThumbnailPhotoFileName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LargePhoto,
						map =>
						{
							map.Column("LargePhoto");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LargePhotoFileName,
						map =>
						{
							map.Column("LargePhotoFileName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.OneToOne(m => m.ProductProductPhoto,
								map =>
									{ });
				});

			// Default mappings for ProductProductPhoto
			mapper.Class<ProductProductPhoto>(
				mp =>
				{
					mp.Table("ProductProductPhoto");

					mp.Property(
						x => x.Primary,
						map =>
						{
							map.Column("Primary");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductProductPhoto>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
					mp.Id(
						x => x.ProductPhotoID,
						map =>
						{
							map.Column("ProductPhotoID");
							map.Generator(Generators.Foreign<ProductProductPhoto>(a => a.ProductPhoto));
						});					
					mp.OneToOne(m => m.ProductPhoto,
						map => map.Constrained(true));
				});

			// Default mappings for TransactionHistory
			mapper.Class<TransactionHistory>(
				mp =>
				{
					mp.Table("TransactionHistory");

					mp.Id(
						x => x.TransactionID,
						map =>
						{
							map.Column("TransactionID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReferenceOrderID,
						map =>
						{
							map.Column("ReferenceOrderID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReferenceOrderLineID,
						map =>
						{
							map.Column("ReferenceOrderLineID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TransactionDate,
						map =>
						{
							map.Column("TransactionDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TransactionType,
						map =>
						{
							map.Column("TransactionType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Quantity,
						map =>
						{
							map.Column("Quantity");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ActualCost,
						map =>
						{
							map.Column("ActualCost");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.ReferenceOrderID,
						map => map.Index("IX_TransactionHistory_ReferenceOrderID_ReferenceOrderLineID"));

					mp.Property(
						x => x.ReferenceOrderLineID,
						map => map.Index("IX_TransactionHistory_ReferenceOrderID_ReferenceOrderLineID"));


					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for ProductReview
			mapper.Class<ProductReview>(
				mp =>
				{
					mp.Table("ProductReview");

					mp.Id(
						x => x.ProductReviewID,
						map =>
						{
							map.Column("ProductReviewID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReviewerName,
						map =>
						{
							map.Column("ReviewerName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReviewDate,
						map =>
						{
							map.Column("ReviewDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Rating,
						map =>
						{
							map.Column("Rating");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Comments,
						map =>
						{
							map.Column("Comments");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.ReviewerName,
						map => map.Index("IX_ProductReview_ProductID_Name"));

					mp.Property(
						x => x.Comments,
						map => map.Index("IX_ProductReview_ProductID_Name"));


					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for BusinessEntity
			mapper.Class<BusinessEntity>(
				mp =>
				{
					mp.Table("BusinessEntity");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_BusinessEntity_rowguid");
							});

					mp.OneToOne(m => m.BusinessEntityAddress,
								map =>
									{ });
					mp.OneToOne(m => m.BusinessEntityContact,
								map =>
									{ });
					mp.OneToOne(m => m.Person,
								map =>
									{ });
					mp.OneToOne(m => m.Store,
								map =>
									{ });
					mp.OneToOne(m => m.Vendor,
								map =>
									{ });
				});

			// Default mappings for TransactionHistoryArchive
			mapper.Class<TransactionHistoryArchive>(
				mp =>
				{
					mp.Table("TransactionHistoryArchive");

					mp.Id(
						x => x.TransactionID,
						map =>
						{
							map.Column("TransactionID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReferenceOrderID,
						map =>
						{
							map.Column("ReferenceOrderID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReferenceOrderLineID,
						map =>
						{
							map.Column("ReferenceOrderLineID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TransactionDate,
						map =>
						{
							map.Column("TransactionDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TransactionType,
						map =>
						{
							map.Column("TransactionType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Quantity,
						map =>
						{
							map.Column("Quantity");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ActualCost,
						map =>
						{
							map.Column("ActualCost");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.ProductID,
						map => map.Index("IX_TransactionHistoryArchive_ProductID"));

					mp.Property(
						x => x.ReferenceOrderID,
						map => map.Index("IX_TransactionHistoryArchive_ReferenceOrderID_ReferenceOrderLineID"));

					mp.Property(
						x => x.ReferenceOrderLineID,
						map => map.Index("IX_TransactionHistoryArchive_ReferenceOrderID_ReferenceOrderLineID"));


				});

			// Default mappings for ProductSubcategory
			mapper.Class<ProductSubcategory>(
				mp =>
				{
					mp.Table("ProductSubcategory");

					mp.Id(
						x => x.ProductSubcategoryID,
						map =>
						{
							map.Column("ProductSubcategoryID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ProductCategoryID,
						map =>
						{
							map.Column("ProductCategoryID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductSubcategory_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ProductSubcategory_rowguid");
							});

					mp.Bag(
						x => x.Product,
						map => map.Key(m => m.Column("ProductSubcategoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Product));
								}));
					mp.ManyToOne<ProductCategory>(
						x => x.ProductCategory,
						map =>
						{
							map.Column("ProductCategoryID");
							map.Class(typeof(ProductCategory));
							map.ForeignKey("ProductCategoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for BusinessEntityAddress
			mapper.Class<BusinessEntityAddress>(
				mp =>
				{
					mp.Table("BusinessEntityAddress");

					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_BusinessEntityAddress_rowguid");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<BusinessEntityAddress>(a => a.BusinessEntity));
						});					
					mp.OneToOne(m => m.BusinessEntity,
						map => map.Constrained(true));
					mp.Id(
						x => x.AddressID,
						map =>
						{
							map.Column("AddressID");
							map.Generator(Generators.Foreign<BusinessEntityAddress>(a => a.Address));
						});					
					mp.OneToOne(m => m.Address,
						map => map.Constrained(true));
					mp.Id(
						x => x.AddressTypeID,
						map =>
						{
							map.Column("AddressTypeID");
							map.Generator(Generators.Foreign<BusinessEntityAddress>(a => a.AddressType));
						});					
					mp.OneToOne(m => m.AddressType,
						map => map.Constrained(true));
				});

			// Default mappings for ProductVendor
			mapper.Class<ProductVendor>(
				mp =>
				{
					mp.Table("ProductVendor");

					mp.Property(
						x => x.AverageLeadTime,
						map =>
						{
							map.Column("AverageLeadTime");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StandardPrice,
						map =>
						{
							map.Column("StandardPrice");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.LastReceiptCost,
						map =>
						{
							map.Column("LastReceiptCost");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastReceiptDate,
						map =>
						{
							map.Column("LastReceiptDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.MinOrderQty,
						map =>
						{
							map.Column("MinOrderQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MaxOrderQty,
						map =>
						{
							map.Column("MaxOrderQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OnOrderQty,
						map =>
						{
							map.Column("OnOrderQty");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.UnitMeasureCode,
						map =>
						{
							map.Column("UnitMeasureCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Foreign<ProductVendor>(a => a.Product));
						});					
					mp.OneToOne(m => m.Product,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<ProductVendor>(a => a.Vendor));
						});					
					mp.OneToOne(m => m.Vendor,
						map => map.Constrained(true));
					mp.ManyToOne<UnitMeasure>(
						x => x.UnitMeasure,
						map =>
						{
							map.Column("UnitMeasureCode");
							map.Class(typeof(UnitMeasure));
							map.ForeignKey("UnitMeasureCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for BusinessEntityContact
			mapper.Class<BusinessEntityContact>(
				mp =>
				{
					mp.Table("BusinessEntityContact");

					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_BusinessEntityContact_rowguid");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<BusinessEntityContact>(a => a.BusinessEntity));
						});					
					mp.OneToOne(m => m.BusinessEntity,
						map => map.Constrained(true));
					mp.Id(
						x => x.PersonID,
						map =>
						{
							map.Column("PersonID");
							map.Generator(Generators.Foreign<BusinessEntityContact>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
					mp.Id(
						x => x.ContactTypeID,
						map =>
						{
							map.Column("ContactTypeID");
							map.Generator(Generators.Foreign<BusinessEntityContact>(a => a.ContactType));
						});					
					mp.OneToOne(m => m.ContactType,
						map => map.Constrained(true));
				});

			// Default mappings for UnitMeasure
			mapper.Class<UnitMeasure>(
				mp =>
				{
					mp.Table("UnitMeasure");

					mp.Id(
						x => x.UnitMeasureCode,
						map =>
						{
							map.Column("UnitMeasureCode");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_UnitMeasure_Name");
							});

					mp.Bag(
						x => x.BillOfMaterials,
						map => map.Key(m => m.Column("UnitMeasureCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(BillOfMaterials));
								}));
					mp.Bag(
						x => x.Product,
						map => map.Key(m => m.Column("SizeUnitMeasureCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Product));
								}));
					mp.Bag(
						x => x.Product_,
						map => map.Key(m => m.Column("WeightUnitMeasureCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Product));
								}));
					mp.Bag(
						x => x.ProductVendor,
						map => map.Key(m => m.Column("UnitMeasureCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(ProductVendor));
								}));
				});

			// Default mappings for Vendor
			mapper.Class<Vendor>(
				mp =>
				{
					mp.Table("Vendor");

					mp.Property(
						x => x.AccountNumber,
						map =>
						{
							map.Column("AccountNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CreditRating,
						map =>
						{
							map.Column("CreditRating");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PreferredVendorStatus,
						map =>
						{
							map.Column("PreferredVendorStatus");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ActiveFlag,
						map =>
						{
							map.Column("ActiveFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PurchasingWebServiceURL,
						map =>
						{
							map.Column("PurchasingWebServiceURL");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.AccountNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Vendor_AccountNumber");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Vendor>(a => a.ProductVendor));
						});					
					mp.OneToOne(m => m.ProductVendor,
						map => map.Constrained(true));
					mp.Bag(
						x => x.PurchaseOrderHeader,
						map => map.Key(m => m.Column("VendorID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(PurchaseOrderHeader));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Vendor>(a => a.BusinessEntity));
						});					
					mp.OneToOne(m => m.BusinessEntity,
						map => map.Constrained(true));
				});

			// Default mappings for ContactType
			mapper.Class<ContactType>(
				mp =>
				{
					mp.Table("ContactType");

					mp.Id(
						x => x.ContactTypeID,
						map =>
						{
							map.Column("ContactTypeID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_ContactType_Name");
							});

					mp.OneToOne(m => m.BusinessEntityContact,
								map =>
									{ });
				});

			// Default mappings for CountryRegionCurrency
			mapper.Class<CountryRegionCurrency>(
				mp =>
				{
					mp.Table("CountryRegionCurrency");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.CountryRegionCode,
						map =>
						{
							map.Column("CountryRegionCode");
							map.Generator(Generators.Foreign<CountryRegionCurrency>(a => a.CountryRegion));
						});					
					mp.OneToOne(m => m.CountryRegion,
						map => map.Constrained(true));
					mp.Id(
						x => x.CurrencyCode,
						map =>
						{
							map.Column("CurrencyCode");
							map.Generator(Generators.Foreign<CountryRegionCurrency>(a => a.Currency));
						});					
					mp.OneToOne(m => m.Currency,
						map => map.Constrained(true));
				});

			// Default mappings for CountryRegion
			mapper.Class<CountryRegion>(
				mp =>
				{
					mp.Table("CountryRegion");

					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_CountryRegion_Name");
							});

					mp.Id(
						x => x.CountryRegionCode,
						map =>
						{
							map.Column("CountryRegionCode");
							map.Generator(Generators.Foreign<CountryRegion>(a => a.CountryRegionCurrency));
						});					
					mp.OneToOne(m => m.CountryRegionCurrency,
						map => map.Constrained(true));
					mp.Bag(
						x => x.SalesTerritory,
						map => map.Key(m => m.Column("CountryRegionCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesTerritory));
								}));
					mp.Bag(
						x => x.StateProvince,
						map => map.Key(m => m.Column("CountryRegionCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(StateProvince));
								}));
				});

			// Default mappings for WorkOrder
			mapper.Class<WorkOrder>(
				mp =>
				{
					mp.Table("WorkOrder");

					mp.Id(
						x => x.WorkOrderID,
						map =>
						{
							map.Column("WorkOrderID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OrderQty,
						map =>
						{
							map.Column("OrderQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StockedQty,
						map =>
						{
							map.Column("StockedQty");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ScrappedQty,
						map =>
						{
							map.Column("ScrappedQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartDate,
						map =>
						{
							map.Column("StartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.DueDate,
						map =>
						{
							map.Column("DueDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ScrapReasonID,
						map =>
						{
							map.Column("ScrapReasonID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<ScrapReason>(
						x => x.ScrapReason,
						map =>
						{
							map.Column("ScrapReasonID");
							map.Class(typeof(ScrapReason));
							map.ForeignKey("ScrapReasonID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.OneToOne(m => m.WorkOrderRouting,
								map =>
									{ });
				});

			// Default mappings for PurchaseOrderDetail
			mapper.Class<PurchaseOrderDetail>(
				mp =>
				{
					mp.Table("PurchaseOrderDetail");

					mp.Id(
						x => x.PurchaseOrderDetailID,
						map =>
						{
							map.Column("PurchaseOrderDetailID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.DueDate,
						map =>
						{
							map.Column("DueDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OrderQty,
						map =>
						{
							map.Column("OrderQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.UnitPrice,
						map =>
						{
							map.Column("UnitPrice");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.LineTotal,
						map =>
						{
							map.Column("LineTotal");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ReceivedQty,
						map =>
						{
							map.Column("ReceivedQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.RejectedQty,
						map =>
						{
							map.Column("RejectedQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StockedQty,
						map =>
						{
							map.Column("StockedQty");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.PurchaseOrderID,
						map =>
						{
							map.Column("PurchaseOrderID");
							map.Generator(Generators.Foreign<PurchaseOrderDetail>(a => a.PurchaseOrderHeader));
						});					
					mp.OneToOne(m => m.PurchaseOrderHeader,
						map => map.Constrained(true));
					mp.ManyToOne<Product>(
						x => x.Product,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(Product));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for CreditCard
			mapper.Class<CreditCard>(
				mp =>
				{
					mp.Table("CreditCard");

					mp.Id(
						x => x.CreditCardID,
						map =>
						{
							map.Column("CreditCardID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.CardType,
						map =>
						{
							map.Column("CardType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CardNumber,
						map =>
						{
							map.Column("CardNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ExpMonth,
						map =>
						{
							map.Column("ExpMonth");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ExpYear,
						map =>
						{
							map.Column("ExpYear");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.CardNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_CreditCard_CardNumber");
							});

					mp.OneToOne(m => m.PersonCreditCard,
								map =>
									{ });
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("CreditCardID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
				});

			// Default mappings for Culture
			mapper.Class<Culture>(
				mp =>
				{
					mp.Table("Culture");

					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Culture_Name");
							});

					mp.Id(
						x => x.CultureID,
						map =>
						{
							map.Column("CultureID");
							map.Generator(Generators.Foreign<Culture>(a => a.ProductModelProductDescriptionCulture));
						});					
					mp.OneToOne(m => m.ProductModelProductDescriptionCulture,
						map => map.Constrained(true));
				});

			// Default mappings for WorkOrderRouting
			mapper.Class<WorkOrderRouting>(
				mp =>
				{
					mp.Table("WorkOrderRouting");

					mp.Property(
						x => x.LocationID,
						map =>
						{
							map.Column("LocationID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ScheduledStartDate,
						map =>
						{
							map.Column("ScheduledStartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ScheduledEndDate,
						map =>
						{
							map.Column("ScheduledEndDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ActualStartDate,
						map =>
						{
							map.Column("ActualStartDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ActualEndDate,
						map =>
						{
							map.Column("ActualEndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ActualResourceHrs,
						map =>
						{
							map.Column("ActualResourceHrs");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PlannedCost,
						map =>
						{
							map.Column("PlannedCost");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ActualCost,
						map =>
						{
							map.Column("ActualCost");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.WorkOrderID,
						map =>
						{
							map.Column("WorkOrderID");
							map.Generator(Generators.Foreign<WorkOrderRouting>(a => a.WorkOrder));
						});					
					mp.OneToOne(m => m.WorkOrder,
						map => map.Constrained(true));
					mp.ManyToOne<Location>(
						x => x.Location,
						map =>
						{
							map.Column("LocationID");
							map.Class(typeof(Location));
							map.ForeignKey("LocationID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for Currency
			mapper.Class<Currency>(
				mp =>
				{
					mp.Table("Currency");

					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Currency_Name");
							});

					mp.Id(
						x => x.CurrencyCode,
						map =>
						{
							map.Column("CurrencyCode");
							map.Generator(Generators.Foreign<Currency>(a => a.CountryRegionCurrency));
						});					
					mp.OneToOne(m => m.CountryRegionCurrency,
						map => map.Constrained(true));
					mp.Bag(
						x => x.CurrencyRate,
						map => map.Key(m => m.Column("FromCurrencyCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(CurrencyRate));
								}));
					mp.Bag(
						x => x.CurrencyRate_,
						map => map.Key(m => m.Column("ToCurrencyCode")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(CurrencyRate));
								}));
				});

			// Default mappings for PurchaseOrderHeader
			mapper.Class<PurchaseOrderHeader>(
				mp =>
				{
					mp.Table("PurchaseOrderHeader");

					mp.Id(
						x => x.PurchaseOrderID,
						map =>
						{
							map.Column("PurchaseOrderID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.RevisionNumber,
						map =>
						{
							map.Column("RevisionNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Status,
						map =>
						{
							map.Column("Status");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EmployeeID,
						map =>
						{
							map.Column("EmployeeID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.VendorID,
						map =>
						{
							map.Column("VendorID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipMethodID,
						map =>
						{
							map.Column("ShipMethodID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OrderDate,
						map =>
						{
							map.Column("OrderDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipDate,
						map =>
						{
							map.Column("ShipDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SubTotal,
						map =>
						{
							map.Column("SubTotal");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TaxAmt,
						map =>
						{
							map.Column("TaxAmt");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Freight,
						map =>
						{
							map.Column("Freight");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TotalDue,
						map =>
						{
							map.Column("TotalDue");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.OneToOne(m => m.PurchaseOrderDetail,
								map =>
									{ });
					mp.ManyToOne<Employee>(
						x => x.Employee,
						map =>
						{
							map.Column("EmployeeID");
							map.Class(typeof(Employee));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Vendor>(
						x => x.Vendor,
						map =>
						{
							map.Column("VendorID");
							map.Class(typeof(Vendor));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<ShipMethod>(
						x => x.ShipMethod,
						map =>
						{
							map.Column("ShipMethodID");
							map.Class(typeof(ShipMethod));
							map.ForeignKey("ShipMethodID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for CurrencyRate
			mapper.Class<CurrencyRate>(
				mp =>
				{
					mp.Table("CurrencyRate");

					mp.Id(
						x => x.CurrencyRateID,
						map =>
						{
							map.Column("CurrencyRateID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.CurrencyRateDate,
						map =>
						{
							map.Column("CurrencyRateDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.FromCurrencyCode,
						map =>
						{
							map.Column("FromCurrencyCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ToCurrencyCode,
						map =>
						{
							map.Column("ToCurrencyCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AverageRate,
						map =>
						{
							map.Column("AverageRate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndOfDayRate,
						map =>
						{
							map.Column("EndOfDayRate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.CurrencyRateDate,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_CurrencyRate_CurrencyRateDate_FromCurrencyCode_ToCurrencyCode");
							});

					mp.ManyToOne<Currency>(
						x => x.Currency,
						map =>
						{
							map.Column("FromCurrencyCode");
							map.Class(typeof(Currency));
							map.ForeignKey("CurrencyCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Currency>(
						x => x.Currency_,
						map =>
						{
							map.Column("ToCurrencyCode");
							map.Class(typeof(Currency));
							map.ForeignKey("CurrencyCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("CurrencyRateID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
				});

			// Default mappings for Customer
			mapper.Class<Customer>(
				mp =>
				{
					mp.Table("Customer");

					mp.Id(
						x => x.CustomerID,
						map =>
						{
							map.Column("CustomerID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.PersonID,
						map =>
						{
							map.Column("PersonID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.StoreID,
						map =>
						{
							map.Column("StoreID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.AccountNumber,
						map =>
						{
							map.Column("AccountNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Customer_rowguid");
							});

					mp.Property(
						x => x.AccountNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Customer_AccountNumber");
							});

					mp.ManyToOne<Person>(
						x => x.Person,
						map =>
						{
							map.Column("PersonID");
							map.Class(typeof(Person));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Store>(
						x => x.Store,
						map =>
						{
							map.Column("StoreID");
							map.Class(typeof(Store));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<SalesTerritory>(
						x => x.SalesTerritory,
						map =>
						{
							map.Column("TerritoryID");
							map.Class(typeof(SalesTerritory));
							map.ForeignKey("TerritoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("CustomerID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
				});

			// Default mappings for Department
			mapper.Class<Department>(
				mp =>
				{
					mp.Table("Department");

					mp.Id(
						x => x.DepartmentID,
						map =>
						{
							map.Column("DepartmentID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.GroupName,
						map =>
						{
							map.Column("GroupName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Department_Name");
							});

					mp.OneToOne(m => m.EmployeeDepartmentHistory,
								map =>
									{ });
				});

			// Default mappings for Document
			mapper.Class<Document>(
				mp =>
				{
					mp.Table("Document");

					mp.Property(
						x => x.DocumentLevel,
						map =>
						{
							map.Column("DocumentLevel");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Owner,
						map =>
						{
							map.Column("Owner");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.FolderFlag,
						map =>
						{
							map.Column("FolderFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.FileName,
						map =>
						{
							map.Column("FileName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.FileExtension,
						map =>
						{
							map.Column("FileExtension");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Revision,
						map =>
						{
							map.Column("Revision");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ChangeNumber,
						map =>
						{
							map.Column("ChangeNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Status,
						map =>
						{
							map.Column("Status");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.DocumentSummary,
						map =>
						{
							map.Column("DocumentSummary");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Document_,
						map =>
						{
							map.Column("Document");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.FileName,
						map => map.Index("IX_Document_FileName_Revision"));

					mp.Property(
						x => x.Revision,
						map => map.Index("IX_Document_FileName_Revision"));


					mp.Property(
						x => x.DocumentLevel,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Document_DocumentLevel_DocumentNode");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Document_rowguid");
							});

					mp.ManyToOne<Employee>(
						x => x.Employee,
						map =>
						{
							map.Column("Owner");
							map.Class(typeof(Employee));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.Id(
						x => x.DocumentNode,
						map =>
						{
							map.Column("DocumentNode");
							map.Generator(Generators.Foreign<Document>(a => a.ProductDocument));
						});					
					mp.OneToOne(m => m.ProductDocument,
						map => map.Constrained(true));
				});

			// Default mappings for SalesOrderDetail
			mapper.Class<SalesOrderDetail>(
				mp =>
				{
					mp.Table("SalesOrderDetail");

					mp.Id(
						x => x.SalesOrderDetailID,
						map =>
						{
							map.Column("SalesOrderDetailID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.CarrierTrackingNumber,
						map =>
						{
							map.Column("CarrierTrackingNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.OrderQty,
						map =>
						{
							map.Column("OrderQty");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SpecialOfferID,
						map =>
						{
							map.Column("SpecialOfferID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.UnitPrice,
						map =>
						{
							map.Column("UnitPrice");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.UnitPriceDiscount,
						map =>
						{
							map.Column("UnitPriceDiscount");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.LineTotal,
						map =>
						{
							map.Column("LineTotal");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesOrderDetail_rowguid");
							});

					mp.Id(
						x => x.SalesOrderID,
						map =>
						{
							map.Column("SalesOrderID");
							map.Generator(Generators.Foreign<SalesOrderDetail>(a => a.SalesOrderHeader));
						});					
					mp.OneToOne(m => m.SalesOrderHeader,
						map => map.Constrained(true));
					mp.ManyToOne<SpecialOfferProduct>(
						x => x.SpecialOfferProduct,
						map =>
						{
							map.Column("ProductID");
							map.Class(typeof(SpecialOfferProduct));
							map.ForeignKey("ProductID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for EmailAddress
			mapper.Class<EmailAddress>(
				mp =>
				{
					mp.Table("EmailAddress");

					mp.Id(
						x => x.EmailAddressID,
						map =>
						{
							map.Column("EmailAddressID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.EmailAddress_,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.EmailAddress_,
						map => map.Index("IX_EmailAddress_EmailAddress"));


					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<EmailAddress>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
				});

			// Default mappings for Employee
			mapper.Class<Employee>(
				mp =>
				{
					mp.Table("Employee");

					mp.Property(
						x => x.NationalIDNumber,
						map =>
						{
							map.Column("NationalIDNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.LoginID,
						map =>
						{
							map.Column("LoginID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OrganizationNode,
						map =>
						{
							map.Column("OrganizationNode");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.OrganizationLevel,
						map =>
						{
							map.Column("OrganizationLevel");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.JobTitle,
						map =>
						{
							map.Column("JobTitle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.BirthDate,
						map =>
						{
							map.Column("BirthDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MaritalStatus,
						map =>
						{
							map.Column("MaritalStatus");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Gender,
						map =>
						{
							map.Column("Gender");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.HireDate,
						map =>
						{
							map.Column("HireDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalariedFlag,
						map =>
						{
							map.Column("SalariedFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.VacationHours,
						map =>
						{
							map.Column("VacationHours");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SickLeaveHours,
						map =>
						{
							map.Column("SickLeaveHours");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CurrentFlag,
						map =>
						{
							map.Column("CurrentFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.OrganizationLevel,
						map => map.Index("IX_Employee_OrganizationLevel_OrganizationNode"));

					mp.Property(
						x => x.OrganizationNode,
						map => map.Index("IX_Employee_OrganizationLevel_OrganizationNode"));


					mp.Property(
						x => x.LoginID,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Employee_LoginID");
							});

					mp.Property(
						x => x.NationalIDNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Employee_NationalIDNumber");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Employee_rowguid");
							});

					mp.Bag(
						x => x.Document,
						map => map.Key(m => m.Column("Owner")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Document));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Employee>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Employee>(a => a.EmployeeDepartmentHistory));
						});					
					mp.OneToOne(m => m.EmployeeDepartmentHistory,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Employee>(a => a.EmployeePayHistory));
						});					
					mp.OneToOne(m => m.EmployeePayHistory,
						map => map.Constrained(true));
					mp.Bag(
						x => x.JobCandidate,
						map => map.Key(m => m.Column("BusinessEntityID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(JobCandidate));
								}));
					mp.Bag(
						x => x.PurchaseOrderHeader,
						map => map.Key(m => m.Column("EmployeeID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(PurchaseOrderHeader));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Employee>(a => a.SalesPerson));
						});					
					mp.OneToOne(m => m.SalesPerson,
						map => map.Constrained(true));
				});

			// Default mappings for SalesOrderHeader
			mapper.Class<SalesOrderHeader>(
				mp =>
				{
					mp.Table("SalesOrderHeader");

					mp.Id(
						x => x.SalesOrderID,
						map =>
						{
							map.Column("SalesOrderID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.RevisionNumber,
						map =>
						{
							map.Column("RevisionNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OrderDate,
						map =>
						{
							map.Column("OrderDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.DueDate,
						map =>
						{
							map.Column("DueDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipDate,
						map =>
						{
							map.Column("ShipDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Status,
						map =>
						{
							map.Column("Status");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.OnlineOrderFlag,
						map =>
						{
							map.Column("OnlineOrderFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesOrderNumber,
						map =>
						{
							map.Column("SalesOrderNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PurchaseOrderNumber,
						map =>
						{
							map.Column("PurchaseOrderNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.AccountNumber,
						map =>
						{
							map.Column("AccountNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.CustomerID,
						map =>
						{
							map.Column("CustomerID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesPersonID,
						map =>
						{
							map.Column("SalesPersonID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.BillToAddressID,
						map =>
						{
							map.Column("BillToAddressID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipToAddressID,
						map =>
						{
							map.Column("ShipToAddressID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ShipMethodID,
						map =>
						{
							map.Column("ShipMethodID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CreditCardID,
						map =>
						{
							map.Column("CreditCardID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.CreditCardApprovalCode,
						map =>
						{
							map.Column("CreditCardApprovalCode");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.CurrencyRateID,
						map =>
						{
							map.Column("CurrencyRateID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SubTotal,
						map =>
						{
							map.Column("SubTotal");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TaxAmt,
						map =>
						{
							map.Column("TaxAmt");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Freight,
						map =>
						{
							map.Column("Freight");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TotalDue,
						map =>
						{
							map.Column("TotalDue");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Comment,
						map =>
						{
							map.Column("Comment");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesOrderHeader_rowguid");
							});

					mp.Property(
						x => x.SalesOrderNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesOrderHeader_SalesOrderNumber");
							});

					mp.OneToOne(m => m.SalesOrderDetail,
								map =>
									{ });
					mp.ManyToOne<Customer>(
						x => x.Customer,
						map =>
						{
							map.Column("CustomerID");
							map.Class(typeof(Customer));
							map.ForeignKey("CustomerID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<SalesPerson>(
						x => x.SalesPerson,
						map =>
						{
							map.Column("SalesPersonID");
							map.Class(typeof(SalesPerson));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<SalesTerritory>(
						x => x.SalesTerritory,
						map =>
						{
							map.Column("TerritoryID");
							map.Class(typeof(SalesTerritory));
							map.ForeignKey("TerritoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Address>(
						x => x.Address,
						map =>
						{
							map.Column("BillToAddressID");
							map.Class(typeof(Address));
							map.ForeignKey("AddressID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<Address>(
						x => x.Address_,
						map =>
						{
							map.Column("ShipToAddressID");
							map.Class(typeof(Address));
							map.ForeignKey("AddressID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<ShipMethod>(
						x => x.ShipMethod,
						map =>
						{
							map.Column("ShipMethodID");
							map.Class(typeof(ShipMethod));
							map.ForeignKey("ShipMethodID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<CreditCard>(
						x => x.CreditCard,
						map =>
						{
							map.Column("CreditCardID");
							map.Class(typeof(CreditCard));
							map.ForeignKey("CreditCardID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<CurrencyRate>(
						x => x.CurrencyRate,
						map =>
						{
							map.Column("CurrencyRateID");
							map.Class(typeof(CurrencyRate));
							map.ForeignKey("CurrencyRateID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.OneToOne(m => m.SalesOrderHeaderSalesReason,
								map =>
									{ });
				});

			// Default mappings for EmployeeDepartmentHistory
			mapper.Class<EmployeeDepartmentHistory>(
				mp =>
				{
					mp.Table("EmployeeDepartmentHistory");

					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<EmployeeDepartmentHistory>(a => a.Employee));
						});					
					mp.OneToOne(m => m.Employee,
						map => map.Constrained(true));
					mp.Id(
						x => x.DepartmentID,
						map =>
						{
							map.Column("DepartmentID");
							map.Generator(Generators.Foreign<EmployeeDepartmentHistory>(a => a.Department));
						});					
					mp.OneToOne(m => m.Department,
						map => map.Constrained(true));
					mp.Id(
						x => x.ShiftID,
						map =>
						{
							map.Column("ShiftID");
							map.Generator(Generators.Foreign<EmployeeDepartmentHistory>(a => a.Shift));
						});					
					mp.OneToOne(m => m.Shift,
						map => map.Constrained(true));
				});

			// Default mappings for EmployeePayHistory
			mapper.Class<EmployeePayHistory>(
				mp =>
				{
					mp.Table("EmployeePayHistory");

					mp.Property(
						x => x.Rate,
						map =>
						{
							map.Column("Rate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PayFrequency,
						map =>
						{
							map.Column("PayFrequency");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<EmployeePayHistory>(a => a.Employee));
						});					
					mp.OneToOne(m => m.Employee,
						map => map.Constrained(true));
				});

			// Default mappings for SalesOrderHeaderSalesReason
			mapper.Class<SalesOrderHeaderSalesReason>(
				mp =>
				{
					mp.Table("SalesOrderHeaderSalesReason");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.SalesOrderID,
						map =>
						{
							map.Column("SalesOrderID");
							map.Generator(Generators.Foreign<SalesOrderHeaderSalesReason>(a => a.SalesOrderHeader));
						});					
					mp.OneToOne(m => m.SalesOrderHeader,
						map => map.Constrained(true));
					mp.Id(
						x => x.SalesReasonID,
						map =>
						{
							map.Column("SalesReasonID");
							map.Generator(Generators.Foreign<SalesOrderHeaderSalesReason>(a => a.SalesReason));
						});					
					mp.OneToOne(m => m.SalesReason,
						map => map.Constrained(true));
				});

			// Default mappings for SalesPerson
			mapper.Class<SalesPerson>(
				mp =>
				{
					mp.Table("SalesPerson");

					mp.Property(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SalesQuota,
						map =>
						{
							map.Column("SalesQuota");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Bonus,
						map =>
						{
							map.Column("Bonus");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CommissionPct,
						map =>
						{
							map.Column("CommissionPct");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesYTD,
						map =>
						{
							map.Column("SalesYTD");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesLastYear,
						map =>
						{
							map.Column("SalesLastYear");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesPerson_rowguid");
							});

					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("SalesPersonID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<SalesPerson>(a => a.Employee));
						});					
					mp.OneToOne(m => m.Employee,
						map => map.Constrained(true));
					mp.ManyToOne<SalesTerritory>(
						x => x.SalesTerritory,
						map =>
						{
							map.Column("TerritoryID");
							map.Class(typeof(SalesTerritory));
							map.ForeignKey("TerritoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<SalesPerson>(a => a.SalesPersonQuotaHistory));
						});					
					mp.OneToOne(m => m.SalesPersonQuotaHistory,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<SalesPerson>(a => a.SalesTerritoryHistory));
						});					
					mp.OneToOne(m => m.SalesTerritoryHistory,
						map => map.Constrained(true));
					mp.Bag(
						x => x.Store,
						map => map.Key(m => m.Column("SalesPersonID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Store));
								}));
				});

			// Default mappings for Illustration
			mapper.Class<Illustration>(
				mp =>
				{
					mp.Table("Illustration");

					mp.Id(
						x => x.IllustrationID,
						map =>
						{
							map.Column("IllustrationID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Diagram,
						map =>
						{
							map.Column("Diagram");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.OneToOne(m => m.ProductModelIllustration,
								map =>
									{ });
				});

			// Default mappings for JobCandidate
			mapper.Class<JobCandidate>(
				mp =>
				{
					mp.Table("JobCandidate");

					mp.Id(
						x => x.JobCandidateID,
						map =>
						{
							map.Column("JobCandidateID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Resume,
						map =>
						{
							map.Column("Resume");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.ManyToOne<Employee>(
						x => x.Employee,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Class(typeof(Employee));
							map.ForeignKey("BusinessEntityID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for Location
			mapper.Class<Location>(
				mp =>
				{
					mp.Table("Location");

					mp.Id(
						x => x.LocationID,
						map =>
						{
							map.Column("LocationID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CostRate,
						map =>
						{
							map.Column("CostRate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Availability,
						map =>
						{
							map.Column("Availability");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Location_Name");
							});

					mp.OneToOne(m => m.ProductInventory,
								map =>
									{ });
					mp.Bag(
						x => x.WorkOrderRouting,
						map => map.Key(m => m.Column("LocationID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(WorkOrderRouting));
								}));
				});

			// Default mappings for Password
			mapper.Class<Password>(
				mp =>
				{
					mp.Table("Password");

					mp.Property(
						x => x.PasswordHash,
						map =>
						{
							map.Column("PasswordHash");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PasswordSalt,
						map =>
						{
							map.Column("PasswordSalt");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Password>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
				});

			// Default mappings for SalesPersonQuotaHistory
			mapper.Class<SalesPersonQuotaHistory>(
				mp =>
				{
					mp.Table("SalesPersonQuotaHistory");

					mp.Property(
						x => x.SalesQuota,
						map =>
						{
							map.Column("SalesQuota");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesPersonQuotaHistory_rowguid");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<SalesPersonQuotaHistory>(a => a.SalesPerson));
						});					
					mp.OneToOne(m => m.SalesPerson,
						map => map.Constrained(true));
				});

			// Default mappings for Person
			mapper.Class<Person>(
				mp =>
				{
					mp.Table("Person");

					mp.Property(
						x => x.PersonType,
						map =>
						{
							map.Column("PersonType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.NameStyle,
						map =>
						{
							map.Column("NameStyle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AdditionalContactInfo,
						map =>
						{
							map.Column("AdditionalContactInfo");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Demographics,
						map =>
						{
							map.Column("Demographics");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});


					mp.Property(
						x => x.LastName,
						map => map.Index("IX_Person_LastName_FirstName_MiddleName"));

					mp.Property(
						x => x.FirstName,
						map => map.Index("IX_Person_LastName_FirstName_MiddleName"));

					mp.Property(
						x => x.MiddleName,
						map => map.Index("IX_Person_LastName_FirstName_MiddleName"));

					mp.Property(
						x => x.AdditionalContactInfo,
						map => map.Index("PXML_Person_AddContact"));

					mp.Property(
						x => x.Demographics,
						map => map.Index("XMLVALUE_Person_Demographics"));


					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Person_rowguid");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.BusinessEntityContact));
						});					
					mp.OneToOne(m => m.BusinessEntityContact,
						map => map.Constrained(true));
					mp.Bag(
						x => x.Customer,
						map => map.Key(m => m.Column("PersonID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Customer));
								}));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.EmailAddress));
						});					
					mp.OneToOne(m => m.EmailAddress,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.Employee));
						});					
					mp.OneToOne(m => m.Employee,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.Password));
						});					
					mp.OneToOne(m => m.Password,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.BusinessEntity));
						});					
					mp.OneToOne(m => m.BusinessEntity,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.PersonCreditCard));
						});					
					mp.OneToOne(m => m.PersonCreditCard,
						map => map.Constrained(true));
					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<Person>(a => a.PersonPhone));
						});					
					mp.OneToOne(m => m.PersonPhone,
						map => map.Constrained(true));
				});

			// Default mappings for SalesReason
			mapper.Class<SalesReason>(
				mp =>
				{
					mp.Table("SalesReason");

					mp.Id(
						x => x.SalesReasonID,
						map =>
						{
							map.Column("SalesReasonID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReasonType,
						map =>
						{
							map.Column("ReasonType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.OneToOne(m => m.SalesOrderHeaderSalesReason,
								map =>
									{ });
				});

			// Default mappings for SalesTaxRate
			mapper.Class<SalesTaxRate>(
				mp =>
				{
					mp.Table("SalesTaxRate");

					mp.Id(
						x => x.SalesTaxRateID,
						map =>
						{
							map.Column("SalesTaxRateID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.StateProvinceID,
						map =>
						{
							map.Column("StateProvinceID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TaxType,
						map =>
						{
							map.Column("TaxType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TaxRate,
						map =>
						{
							map.Column("TaxRate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.TaxType,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesTaxRate_StateProvinceID_TaxType");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesTaxRate_rowguid");
							});

					mp.ManyToOne<StateProvince>(
						x => x.StateProvince,
						map =>
						{
							map.Column("StateProvinceID");
							map.Class(typeof(StateProvince));
							map.ForeignKey("StateProvinceID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
				});

			// Default mappings for PersonCreditCard
			mapper.Class<PersonCreditCard>(
				mp =>
				{
					mp.Table("PersonCreditCard");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<PersonCreditCard>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
					mp.Id(
						x => x.CreditCardID,
						map =>
						{
							map.Column("CreditCardID");
							map.Generator(Generators.Foreign<PersonCreditCard>(a => a.CreditCard));
						});					
					mp.OneToOne(m => m.CreditCard,
						map => map.Constrained(true));
				});

			// Default mappings for PersonPhone
			mapper.Class<PersonPhone>(
				mp =>
				{
					mp.Table("PersonPhone");

					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<PersonPhone>(a => a.Person));
						});					
					mp.OneToOne(m => m.Person,
						map => map.Constrained(true));
					mp.Id(
						x => x.PhoneNumberTypeID,
						map =>
						{
							map.Column("PhoneNumberTypeID");
							map.Generator(Generators.Foreign<PersonPhone>(a => a.PhoneNumberType));
						});					
					mp.OneToOne(m => m.PhoneNumberType,
						map => map.Constrained(true));
				});

			// Default mappings for SalesTerritory
			mapper.Class<SalesTerritory>(
				mp =>
				{
					mp.Table("SalesTerritory");

					mp.Id(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionCode,
						map =>
						{
							map.Column("CountryRegionCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Group,
						map =>
						{
							map.Column("Group");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesYTD,
						map =>
						{
							map.Column("SalesYTD");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesLastYear,
						map =>
						{
							map.Column("SalesLastYear");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CostYTD,
						map =>
						{
							map.Column("CostYTD");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CostLastYear,
						map =>
						{
							map.Column("CostLastYear");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesTerritory_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesTerritory_rowguid");
							});

					mp.Bag(
						x => x.Customer,
						map => map.Key(m => m.Column("TerritoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(Customer));
								}));
					mp.Bag(
						x => x.SalesOrderHeader,
						map => map.Key(m => m.Column("TerritoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesOrderHeader));
								}));
					mp.Bag(
						x => x.SalesPerson,
						map => map.Key(m => m.Column("TerritoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(SalesPerson));
								}));
					mp.ManyToOne<CountryRegion>(
						x => x.CountryRegion,
						map =>
						{
							map.Column("CountryRegionCode");
							map.Class(typeof(CountryRegion));
							map.ForeignKey("CountryRegionCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.OneToOne(m => m.SalesTerritoryHistory,
								map =>
									{ });
					mp.Bag(
						x => x.StateProvince,
						map => map.Key(m => m.Column("TerritoryID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(StateProvince));
								}));
				});

			// Default mappings for PhoneNumberType
			mapper.Class<PhoneNumberType>(
				mp =>
				{
					mp.Table("PhoneNumberType");

					mp.Id(
						x => x.PhoneNumberTypeID,
						map =>
						{
							map.Column("PhoneNumberTypeID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.OneToOne(m => m.PersonPhone,
								map =>
									{ });
				});

			// Default mappings for Product
			mapper.Class<Product>(
				mp =>
				{
					mp.Table("Product");

					mp.Id(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductNumber,
						map =>
						{
							map.Column("ProductNumber");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MakeFlag,
						map =>
						{
							map.Column("MakeFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.FinishedGoodsFlag,
						map =>
						{
							map.Column("FinishedGoodsFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Color,
						map =>
						{
							map.Column("Color");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SafetyStockLevel,
						map =>
						{
							map.Column("SafetyStockLevel");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ReorderPoint,
						map =>
						{
							map.Column("ReorderPoint");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StandardCost,
						map =>
						{
							map.Column("StandardCost");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ListPrice,
						map =>
						{
							map.Column("ListPrice");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Size,
						map =>
						{
							map.Column("Size");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SizeUnitMeasureCode,
						map =>
						{
							map.Column("SizeUnitMeasureCode");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.WeightUnitMeasureCode,
						map =>
						{
							map.Column("WeightUnitMeasureCode");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Weight,
						map =>
						{
							map.Column("Weight");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.DaysToManufacture,
						map =>
						{
							map.Column("DaysToManufacture");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductLine,
						map =>
						{
							map.Column("ProductLine");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Class,
						map =>
						{
							map.Column("Class");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Style,
						map =>
						{
							map.Column("Style");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ProductSubcategoryID,
						map =>
						{
							map.Column("ProductSubcategoryID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SellStartDate,
						map =>
						{
							map.Column("SellStartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SellEndDate,
						map =>
						{
							map.Column("SellEndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.DiscontinuedDate,
						map =>
						{
							map.Column("DiscontinuedDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.ProductNumber,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Product_ProductNumber");
							});

					mp.Property(
						x => x.Name,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Product_Name");
							});

					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_Product_rowguid");
							});

					mp.Bag(
						x => x.BillOfMaterials,
						map => map.Key(m => m.Column("ProductAssemblyID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(BillOfMaterials));
								}));
					mp.Bag(
						x => x.BillOfMaterials_,
						map => map.Key(m => m.Column("ComponentID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(BillOfMaterials));
								}));
					mp.ManyToOne<UnitMeasure>(
						x => x.UnitMeasure,
						map =>
						{
							map.Column("SizeUnitMeasureCode");
							map.Class(typeof(UnitMeasure));
							map.ForeignKey("UnitMeasureCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<UnitMeasure>(
						x => x.UnitMeasure_,
						map =>
						{
							map.Column("WeightUnitMeasureCode");
							map.Class(typeof(UnitMeasure));
							map.ForeignKey("UnitMeasureCode");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<ProductSubcategory>(
						x => x.ProductSubcategory,
						map =>
						{
							map.Column("ProductSubcategoryID");
							map.Class(typeof(ProductSubcategory));
							map.ForeignKey("ProductSubcategoryID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.ManyToOne<ProductModel>(
						x => x.ProductModel,
						map =>
						{
							map.Column("ProductModelID");
							map.Class(typeof(ProductModel));
							map.ForeignKey("ProductModelID");
							map.Lazy(LazyRelation.Proxy);
							map.Access(Accessor.Property);
						});
					mp.OneToOne(m => m.ProductCostHistory,
								map =>
									{ });
					mp.OneToOne(m => m.ProductDocument,
								map =>
									{ });
					mp.OneToOne(m => m.ProductInventory,
								map =>
									{ });
					mp.OneToOne(m => m.ProductListPriceHistory,
								map =>
									{ });
					mp.OneToOne(m => m.ProductProductPhoto,
								map =>
									{ });
					mp.Bag(
						x => x.ProductReview,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(ProductReview));
								}));
					mp.OneToOne(m => m.ProductVendor,
								map =>
									{ });
					mp.Bag(
						x => x.PurchaseOrderDetail,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(PurchaseOrderDetail));
								}));
					mp.Bag(
						x => x.ShoppingCartItem,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(ShoppingCartItem));
								}));
					mp.OneToOne(m => m.SpecialOfferProduct,
								map =>
									{ });
					mp.Bag(
						x => x.TransactionHistory,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(TransactionHistory));
								}));
					mp.Bag(
						x => x.WorkOrder,
						map => map.Key(m => m.Column("ProductID")),
						r => r.OneToMany(
							m =>
								{
									m.NotFound(NotFoundMode.Exception); // or NotFoundMode.Ignore
									m.Class(typeof(WorkOrder));
								}));
				});

			// Default mappings for SalesTerritoryHistory
			mapper.Class<SalesTerritoryHistory>(
				mp =>
				{
					mp.Table("SalesTerritoryHistory");

					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



					mp.Property(
						x => x.rowguid,
						map =>
							{
								map.Unique(true);
								map.UniqueKey("AK_SalesTerritoryHistory_rowguid");
							});

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Foreign<SalesTerritoryHistory>(a => a.SalesPerson));
						});					
					mp.OneToOne(m => m.SalesPerson,
						map => map.Constrained(true));
					mp.Id(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.Generator(Generators.Foreign<SalesTerritoryHistory>(a => a.SalesTerritory));
						});					
					mp.OneToOne(m => m.SalesTerritory,
						map => map.Constrained(true));
				});

			// Default mappings for vEmployee
			mapper.Class<vwvEmployee>(
				mp =>
				{
					mp.Table("vEmployee");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.JobTitle,
						map =>
						{
							map.Column("JobTitle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PhoneNumber,
						map =>
						{
							map.Column("PhoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumberType,
						map =>
						{
							map.Column("PhoneNumberType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AdditionalContactInfo,
						map =>
						{
							map.Column("AdditionalContactInfo");
							map.NotNullable(false);
						});



				});

			// Default mappings for vEmployeeDepartment
			mapper.Class<vwvEmployeeDepartment>(
				mp =>
				{
					mp.Table("vEmployeeDepartment");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.JobTitle,
						map =>
						{
							map.Column("JobTitle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Department,
						map =>
						{
							map.Column("Department");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.GroupName,
						map =>
						{
							map.Column("GroupName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartDate,
						map =>
						{
							map.Column("StartDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for vEmployeeDepartmentHistory
			mapper.Class<vwvEmployeeDepartmentHistory>(
				mp =>
				{
					mp.Table("vEmployeeDepartmentHistory");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Shift,
						map =>
						{
							map.Column("Shift");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Department,
						map =>
						{
							map.Column("Department");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.GroupName,
						map =>
						{
							map.Column("GroupName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StartDate,
						map =>
						{
							map.Column("StartDate");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.EndDate,
						map =>
						{
							map.Column("EndDate");
							map.NotNullable(false);
						});



				});

			// Default mappings for vJobCandidate
			mapper.Class<vwvJobCandidate>(
				mp =>
				{
					mp.Table("vJobCandidate");

					mp.Id(
						x => x.JobCandidateID,
						map =>
						{
							map.Column("JobCandidateID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Skills,
						map =>
						{
							map.Column("Skills");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EMail,
						map =>
						{
							map.Column("EMail");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.WebSite,
						map =>
						{
							map.Column("WebSite");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for vJobCandidateEducation
			mapper.Class<vwvJobCandidateEducation>(
				mp =>
				{
					mp.Table("vJobCandidateEducation");

					mp.Id(
						x => x.JobCandidateID,
						map =>
						{
							map.Column("JobCandidateID");
							map.Generator(Generators.Native);
						});



				});

			// Default mappings for vJobCandidateEmployment
			mapper.Class<vwvJobCandidateEmployment>(
				mp =>
				{
					mp.Table("vJobCandidateEmployment");

					mp.Id(
						x => x.JobCandidateID,
						map =>
						{
							map.Column("JobCandidateID");
							map.Generator(Generators.Native);
						});



				});

			// Default mappings for vAdditionalContactInfo
			mapper.Class<vwvAdditionalContactInfo>(
				mp =>
				{
					mp.Table("vAdditionalContactInfo");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TelephoneNumber,
						map =>
						{
							map.Column("TelephoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TelephoneSpecialInstructions,
						map =>
						{
							map.Column("TelephoneSpecialInstructions");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Street,
						map =>
						{
							map.Column("Street");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.StateProvince,
						map =>
						{
							map.Column("StateProvince");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.CountryRegion,
						map =>
						{
							map.Column("CountryRegion");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.HomeAddressSpecialInstructions,
						map =>
						{
							map.Column("HomeAddressSpecialInstructions");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EMailAddress,
						map =>
						{
							map.Column("EMailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EMailSpecialInstructions,
						map =>
						{
							map.Column("EMailSpecialInstructions");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EMailTelephoneNumber,
						map =>
						{
							map.Column("EMailTelephoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for vStateProvinceCountryRegion
			mapper.Class<vwvStateProvinceCountryRegion>(
				mp =>
				{
					mp.Table("vStateProvinceCountryRegion");

					mp.Id(
						x => x.StateProvinceID,
						map =>
						{
							map.Column("StateProvinceID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.StateProvinceCode,
						map =>
						{
							map.Column("StateProvinceCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.IsOnlyStateProvinceFlag,
						map =>
						{
							map.Column("IsOnlyStateProvinceFlag");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TerritoryID,
						map =>
						{
							map.Column("TerritoryID");
							map.NotNullable(true);
						});
					mp.Id(
						x => x.CountryRegionCode,
						map =>
						{
							map.Column("CountryRegionCode");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});



				});

			// Default mappings for vProductAndDescription
			mapper.Class<vwvProductAndDescription>(
				mp =>
				{
					mp.Table("vProductAndDescription");

					mp.Property(
						x => x.ProductID,
						map =>
						{
							map.Column("ProductID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ProductModel,
						map =>
						{
							map.Column("ProductModel");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CultureID,
						map =>
						{
							map.Column("CultureID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Description,
						map =>
						{
							map.Column("Description");
							map.NotNullable(true);
						});



				});

			// Default mappings for vProductModelCatalogDescription
			mapper.Class<vwvProductModelCatalogDescription>(
				mp =>
				{
					mp.Table("vProductModelCatalogDescription");

					mp.Id(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Summary,
						map =>
						{
							map.Column("Summary");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Manufacturer,
						map =>
						{
							map.Column("Manufacturer");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Copyright,
						map =>
						{
							map.Column("Copyright");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ProductURL,
						map =>
						{
							map.Column("ProductURL");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.WarrantyPeriod,
						map =>
						{
							map.Column("WarrantyPeriod");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.WarrantyDescription,
						map =>
						{
							map.Column("WarrantyDescription");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.NoOfYears,
						map =>
						{
							map.Column("NoOfYears");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.MaintenanceDescription,
						map =>
						{
							map.Column("MaintenanceDescription");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Wheel,
						map =>
						{
							map.Column("Wheel");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Saddle,
						map =>
						{
							map.Column("Saddle");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Pedal,
						map =>
						{
							map.Column("Pedal");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.BikeFrame,
						map =>
						{
							map.Column("BikeFrame");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Crankset,
						map =>
						{
							map.Column("Crankset");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PictureAngle,
						map =>
						{
							map.Column("PictureAngle");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PictureSize,
						map =>
						{
							map.Column("PictureSize");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ProductPhotoID,
						map =>
						{
							map.Column("ProductPhotoID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Material,
						map =>
						{
							map.Column("Material");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Color,
						map =>
						{
							map.Column("Color");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.ProductLine,
						map =>
						{
							map.Column("ProductLine");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Style,
						map =>
						{
							map.Column("Style");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.RiderExperience,
						map =>
						{
							map.Column("RiderExperience");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for vProductModelInstructions
			mapper.Class<vwvProductModelInstructions>(
				mp =>
				{
					mp.Table("vProductModelInstructions");

					mp.Id(
						x => x.ProductModelID,
						map =>
						{
							map.Column("ProductModelID");
							map.Generator(Generators.Native);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Instructions,
						map =>
						{
							map.Column("Instructions");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LocationID,
						map =>
						{
							map.Column("LocationID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SetupHours,
						map =>
						{
							map.Column("SetupHours");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.MachineHours,
						map =>
						{
							map.Column("MachineHours");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LaborHours,
						map =>
						{
							map.Column("LaborHours");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LotSize,
						map =>
						{
							map.Column("LotSize");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Step,
						map =>
						{
							map.Column("Step");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.rowguid,
						map =>
						{
							map.Column("rowguid");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ModifiedDate,
						map =>
						{
							map.Column("ModifiedDate");
							map.NotNullable(true);
						});



				});

			// Default mappings for vVendorWithAddresses
			mapper.Class<vwvVendorWithAddresses>(
				mp =>
				{
					mp.Table("vVendorWithAddresses");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressType,
						map =>
						{
							map.Column("AddressType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});



				});

			// Default mappings for vVendorWithContacts
			mapper.Class<vwvVendorWithContacts>(
				mp =>
				{
					mp.Table("vVendorWithContacts");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ContactType,
						map =>
						{
							map.Column("ContactType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumber,
						map =>
						{
							map.Column("PhoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumberType,
						map =>
						{
							map.Column("PhoneNumberType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});



				});

			// Default mappings for vIndividualCustomer
			mapper.Class<vwvIndividualCustomer>(
				mp =>
				{
					mp.Table("vIndividualCustomer");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumber,
						map =>
						{
							map.Column("PhoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumberType,
						map =>
						{
							map.Column("PhoneNumberType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressType,
						map =>
						{
							map.Column("AddressType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Demographics,
						map =>
						{
							map.Column("Demographics");
							map.NotNullable(false);
						});



				});

			// Default mappings for vPersonDemographics
			mapper.Class<vwvPersonDemographics>(
				mp =>
				{
					mp.Table("vPersonDemographics");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.TotalPurchaseYTD,
						map =>
						{
							map.Column("TotalPurchaseYTD");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.DateFirstPurchase,
						map =>
						{
							map.Column("DateFirstPurchase");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.BirthDate,
						map =>
						{
							map.Column("BirthDate");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.MaritalStatus,
						map =>
						{
							map.Column("MaritalStatus");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.YearlyIncome,
						map =>
						{
							map.Column("YearlyIncome");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Gender,
						map =>
						{
							map.Column("Gender");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TotalChildren,
						map =>
						{
							map.Column("TotalChildren");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.NumberChildrenAtHome,
						map =>
						{
							map.Column("NumberChildrenAtHome");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Education,
						map =>
						{
							map.Column("Education");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Occupation,
						map =>
						{
							map.Column("Occupation");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.HomeOwnerFlag,
						map =>
						{
							map.Column("HomeOwnerFlag");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.NumberCarsOwned,
						map =>
						{
							map.Column("NumberCarsOwned");
							map.NotNullable(false);
						});



				});

			// Default mappings for vSalesPerson
			mapper.Class<vwvSalesPerson>(
				mp =>
				{
					mp.Table("vSalesPerson");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.JobTitle,
						map =>
						{
							map.Column("JobTitle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PhoneNumber,
						map =>
						{
							map.Column("PhoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumberType,
						map =>
						{
							map.Column("PhoneNumberType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.TerritoryName,
						map =>
						{
							map.Column("TerritoryName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.TerritoryGroup,
						map =>
						{
							map.Column("TerritoryGroup");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SalesQuota,
						map =>
						{
							map.Column("SalesQuota");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SalesYTD,
						map =>
						{
							map.Column("SalesYTD");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesLastYear,
						map =>
						{
							map.Column("SalesLastYear");
							map.NotNullable(true);
						});



				});

			// Default mappings for vSalesPersonSalesByFiscalYears
			mapper.Class<vwvSalesPersonSalesByFiscalYears>(
				mp =>
				{
					mp.Table("vSalesPersonSalesByFiscalYears");

					mp.Property(
						x => x.SalesPersonID,
						map =>
						{
							map.Column("SalesPersonID");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FullName,
						map =>
						{
							map.Column("FullName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.JobTitle,
						map =>
						{
							map.Column("JobTitle");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.SalesTerritory,
						map =>
						{
							map.Column("SalesTerritory");
							map.NotNullable(true);
						});
					mp.Property(
						x => x._2002,
						map =>
						{
							map.Column("2002");
							map.NotNullable(false);
						});
					mp.Property(
						x => x._2003,
						map =>
						{
							map.Column("2003");
							map.NotNullable(false);
						});
					mp.Property(
						x => x._2004,
						map =>
						{
							map.Column("2004");
							map.NotNullable(false);
						});



				});

			// Default mappings for vStoreWithAddresses
			mapper.Class<vwvStoreWithAddresses>(
				mp =>
				{
					mp.Table("vStoreWithAddresses");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressType,
						map =>
						{
							map.Column("AddressType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine1,
						map =>
						{
							map.Column("AddressLine1");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AddressLine2,
						map =>
						{
							map.Column("AddressLine2");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.City,
						map =>
						{
							map.Column("City");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.StateProvinceName,
						map =>
						{
							map.Column("StateProvinceName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.PostalCode,
						map =>
						{
							map.Column("PostalCode");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.CountryRegionName,
						map =>
						{
							map.Column("CountryRegionName");
							map.NotNullable(true);
						});



				});

			// Default mappings for vStoreWithContacts
			mapper.Class<vwvStoreWithContacts>(
				mp =>
				{
					mp.Table("vStoreWithContacts");

					mp.Property(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.ContactType,
						map =>
						{
							map.Column("ContactType");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Title,
						map =>
						{
							map.Column("Title");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.FirstName,
						map =>
						{
							map.Column("FirstName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.MiddleName,
						map =>
						{
							map.Column("MiddleName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.LastName,
						map =>
						{
							map.Column("LastName");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.Suffix,
						map =>
						{
							map.Column("Suffix");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumber,
						map =>
						{
							map.Column("PhoneNumber");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.PhoneNumberType,
						map =>
						{
							map.Column("PhoneNumberType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailAddress,
						map =>
						{
							map.Column("EmailAddress");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.EmailPromotion,
						map =>
						{
							map.Column("EmailPromotion");
							map.NotNullable(true);
						});



				});

			// Default mappings for vStoreWithDemographics
			mapper.Class<vwvStoreWithDemographics>(
				mp =>
				{
					mp.Table("vStoreWithDemographics");

					mp.Id(
						x => x.BusinessEntityID,
						map =>
						{
							map.Column("BusinessEntityID");
							map.Generator(Generators.Assigned);
						});
					mp.Property(
						x => x.Name,
						map =>
						{
							map.Column("Name");
							map.NotNullable(true);
						});
					mp.Property(
						x => x.AnnualSales,
						map =>
						{
							map.Column("AnnualSales");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.AnnualRevenue,
						map =>
						{
							map.Column("AnnualRevenue");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.BankName,
						map =>
						{
							map.Column("BankName");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.BusinessType,
						map =>
						{
							map.Column("BusinessType");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.YearOpened,
						map =>
						{
							map.Column("YearOpened");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Specialty,
						map =>
						{
							map.Column("Specialty");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.SquareFeet,
						map =>
						{
							map.Column("SquareFeet");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Brands,
						map =>
						{
							map.Column("Brands");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.Internet,
						map =>
						{
							map.Column("Internet");
							map.NotNullable(false);
						});
					mp.Property(
						x => x.NumberEmployees,
						map =>
						{
							map.Column("NumberEmployees");
							map.NotNullable(false);
						});



				});

		}
	}
}
