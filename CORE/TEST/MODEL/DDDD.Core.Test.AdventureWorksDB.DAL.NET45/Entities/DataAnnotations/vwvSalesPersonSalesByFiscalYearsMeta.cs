using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vSalesPersonSalesByFiscalYears. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvSalesPersonSalesByFiscalYearsMeta
	{

		[DisplayName("")]
		public Int32? SalesPersonID { get; set; }

		[DisplayName("")]
		public String FullName { get; set; }

		[DisplayName("")]
		public String JobTitle { get; set; }

		[DisplayName("")]
		public String SalesTerritory { get; set; }

		[DisplayName("")]
		public Decimal? _2002 { get; set; }

		[DisplayName("")]
		public Decimal? _2003 { get; set; }

		[DisplayName("")]
		public Decimal? _2004 { get; set; }

	}
}
