using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for TransactionHistory. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class TransactionHistoryMeta
	{

		[DisplayName("")]
		public Int32 TransactionID { get; set; }

		[DisplayName("")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public Int32 ReferenceOrderID { get; set; }

		[DisplayName("")]
		public Int32 ReferenceOrderLineID { get; set; }

		[DisplayName("")]
		public DateTime TransactionDate { get; set; }

		[DisplayName("")]
		public String TransactionType { get; set; }

		[DisplayName("")]
		public Int32 Quantity { get; set; }

		[DisplayName("")]
		public Decimal ActualCost { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
