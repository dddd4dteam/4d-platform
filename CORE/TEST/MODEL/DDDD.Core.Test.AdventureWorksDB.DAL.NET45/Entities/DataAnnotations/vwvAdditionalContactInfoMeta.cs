using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vAdditionalContactInfo. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvAdditionalContactInfoMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String FirstName { get; set; }

		[DisplayName("")]
		public String MiddleName { get; set; }

		[DisplayName("")]
		public String LastName { get; set; }

		[DisplayName("")]
		public String TelephoneNumber { get; set; }

		[DisplayName("")]
		public String TelephoneSpecialInstructions { get; set; }

		[DisplayName("")]
		public String Street { get; set; }

		[DisplayName("")]
		public String City { get; set; }

		[DisplayName("")]
		public String StateProvince { get; set; }

		[DisplayName("")]
		public String PostalCode { get; set; }

		[DisplayName("")]
		public String CountryRegion { get; set; }

		[DisplayName("")]
		public String HomeAddressSpecialInstructions { get; set; }

		[DisplayName("")]
		public String EMailAddress { get; set; }

		[DisplayName("")]
		public String EMailSpecialInstructions { get; set; }

		[DisplayName("")]
		public String EMailTelephoneNumber { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
