using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for PurchaseOrderDetail. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class PurchaseOrderDetailMeta
	{

		[Display(Name = "")]
		public Int32 PurchaseOrderID { get; set; }

		[Display(Name = "")]
		public Int32 PurchaseOrderDetailID { get; set; }

		[DisplayName("")]
		public DateTime DueDate { get; set; }

		[DisplayName("")]
		public Int16 OrderQty { get; set; }

		[DisplayName("")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public Decimal UnitPrice { get; set; }

		[DisplayName("")]
		public Decimal? LineTotal { get; set; }

		[DisplayName("")]
		public Decimal ReceivedQty { get; set; }

		[DisplayName("")]
		public Decimal RejectedQty { get; set; }

		[DisplayName("")]
		public Decimal? StockedQty { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
