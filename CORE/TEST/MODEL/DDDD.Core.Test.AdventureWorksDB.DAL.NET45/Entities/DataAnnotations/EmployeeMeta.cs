using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Employee. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class EmployeeMeta
	{

		[Display(Name = "")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String NationalIDNumber { get; set; }

		[DisplayName("")]
		public String LoginID { get; set; }

		//[DisplayName("")]
		//public Microsoft.SqlServer.Types.SqlHierarchyId? OrganizationNode { get; set; }

		[DisplayName("")]
		public Int16? OrganizationLevel { get; set; }

		[DisplayName("")]
		public String JobTitle { get; set; }

		[DisplayName("")]
		public DateTime BirthDate { get; set; }

		[DisplayName("")]
		public String MaritalStatus { get; set; }

		[DisplayName("")]
		public String Gender { get; set; }

		[DisplayName("")]
		public DateTime HireDate { get; set; }

		[DisplayName("")]
		public Boolean SalariedFlag { get; set; }

		[DisplayName("")]
		public Int16 VacationHours { get; set; }

		[DisplayName("")]
		public Int16 SickLeaveHours { get; set; }

		[DisplayName("")]
		public Boolean CurrentFlag { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
