using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vJobCandidateEducation. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvJobCandidateEducationMeta
	{

		[DisplayName("")]
		public Int32 JobCandidateID { get; set; }

	}
}
