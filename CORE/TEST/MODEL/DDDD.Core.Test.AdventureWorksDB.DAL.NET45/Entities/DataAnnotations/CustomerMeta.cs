using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Customer. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class CustomerMeta
	{

		[DisplayName("")]
		public Int32 CustomerID { get; set; }

		[DisplayName("")]
		public Int32? PersonID { get; set; }

		[DisplayName("")]
		public Int32? StoreID { get; set; }

		[DisplayName("")]
		public Int32? TerritoryID { get; set; }

		[DisplayName("")]
		public String AccountNumber { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
