using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductPhoto. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductPhotoMeta
	{

		[Display(Name = "")]
		public Int32 ProductPhotoID { get; set; }

		[DisplayName("")]
		public Byte[] ThumbNailPhoto { get; set; }

		[DisplayName("")]
		public String ThumbnailPhotoFileName { get; set; }

		[DisplayName("")]
		public Byte[] LargePhoto { get; set; }

		[DisplayName("")]
		public String LargePhotoFileName { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
