using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for AWBuildVersion. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class AWBuildVersionMeta
	{

		[DisplayName("Primary key for AWBuildVersion records.")]
		public Byte SystemInformationID { get; set; }

		[DisplayName("Version number of the database in 9.yy.mm.dd.00 format.")]
		public String Database_Version { get; set; }

		[DisplayName("Date and time the record was last updated.")]
		public DateTime VersionDate { get; set; }

		[DisplayName("Date and time the record was last updated.")]
		public DateTime ModifiedDate { get; set; }

	}
}
