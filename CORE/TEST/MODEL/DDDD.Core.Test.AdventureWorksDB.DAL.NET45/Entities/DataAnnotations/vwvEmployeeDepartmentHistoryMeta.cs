using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vEmployeeDepartmentHistory. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvEmployeeDepartmentHistoryMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String Title { get; set; }

		[DisplayName("")]
		public String FirstName { get; set; }

		[DisplayName("")]
		public String MiddleName { get; set; }

		[DisplayName("")]
		public String LastName { get; set; }

		[DisplayName("")]
		public String Suffix { get; set; }

		[DisplayName("")]
		public String Shift { get; set; }

		[DisplayName("")]
		public String Department { get; set; }

		[DisplayName("")]
		public String GroupName { get; set; }

		[DisplayName("")]
		public DateTime StartDate { get; set; }

		[DisplayName("")]
		public DateTime? EndDate { get; set; }

	}
}
