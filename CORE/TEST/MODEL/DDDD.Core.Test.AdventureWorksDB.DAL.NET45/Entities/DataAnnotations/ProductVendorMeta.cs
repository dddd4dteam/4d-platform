using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductVendor. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductVendorMeta
	{

		[Display(Name = "")]
		public Int32 ProductID { get; set; }

		[Display(Name = "")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public Int32 AverageLeadTime { get; set; }

		[DisplayName("")]
		public Decimal StandardPrice { get; set; }

		[DisplayName("")]
		public Decimal? LastReceiptCost { get; set; }

		[DisplayName("")]
		public DateTime? LastReceiptDate { get; set; }

		[DisplayName("")]
		public Int32 MinOrderQty { get; set; }

		[DisplayName("")]
		public Int32 MaxOrderQty { get; set; }

		[DisplayName("")]
		public Int32? OnOrderQty { get; set; }

		[DisplayName("")]
		public String UnitMeasureCode { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
