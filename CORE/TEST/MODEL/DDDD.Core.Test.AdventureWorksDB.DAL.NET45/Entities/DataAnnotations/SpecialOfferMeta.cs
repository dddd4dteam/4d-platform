using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for SpecialOffer. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class SpecialOfferMeta
	{

		[Display(Name = "")]
		public Int32 SpecialOfferID { get; set; }

		[DisplayName("")]
		public String Description { get; set; }

		[DisplayName("")]
		public Decimal DiscountPct { get; set; }

		[DisplayName("")]
		public String Type { get; set; }

		[DisplayName("")]
		public String Category { get; set; }

		[DisplayName("")]
		public DateTime StartDate { get; set; }

		[DisplayName("")]
		public DateTime EndDate { get; set; }

		[DisplayName("")]
		public Int32 MinQty { get; set; }

		[DisplayName("")]
		public Int32? MaxQty { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
