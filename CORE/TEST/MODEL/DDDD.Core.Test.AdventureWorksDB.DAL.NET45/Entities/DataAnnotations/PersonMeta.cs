using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Person. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class PersonMeta
	{

		[Display(Name = "")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String PersonType { get; set; }

		[DisplayName("")]
		public Boolean NameStyle { get; set; }

		[DisplayName("")]
		public String Title { get; set; }

		[DisplayName("")]
		public String FirstName { get; set; }

		[DisplayName("")]
		public String MiddleName { get; set; }

		[DisplayName("")]
		public String LastName { get; set; }

		[DisplayName("")]
		public String Suffix { get; set; }

		[DisplayName("")]
		public Int32 EmailPromotion { get; set; }

		[DisplayName("")]
		public String AdditionalContactInfo { get; set; }

		[DisplayName("")]
		public String Demographics { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
