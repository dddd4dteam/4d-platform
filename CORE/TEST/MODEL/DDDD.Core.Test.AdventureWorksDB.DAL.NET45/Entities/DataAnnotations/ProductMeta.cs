using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Product. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductMeta
	{

		[Display(Name = "")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public String ProductNumber { get; set; }

		[DisplayName("")]
		public Boolean MakeFlag { get; set; }

		[DisplayName("")]
		public Boolean FinishedGoodsFlag { get; set; }

		[DisplayName("")]
		public String Color { get; set; }

		[DisplayName("")]
		public Int16 SafetyStockLevel { get; set; }

		[DisplayName("")]
		public Int16 ReorderPoint { get; set; }

		[DisplayName("")]
		public Decimal StandardCost { get; set; }

		[DisplayName("")]
		public Decimal ListPrice { get; set; }

		[DisplayName("")]
		public String Size { get; set; }

		[DisplayName("")]
		public String SizeUnitMeasureCode { get; set; }

		[DisplayName("")]
		public String WeightUnitMeasureCode { get; set; }

		[DisplayName("")]
		public Decimal? Weight { get; set; }

		[DisplayName("")]
		public Int32 DaysToManufacture { get; set; }

		[DisplayName("")]
		public String ProductLine { get; set; }

		[DisplayName("")]
		public String Class { get; set; }

		[DisplayName("")]
		public String Style { get; set; }

		[DisplayName("")]
		public Int32? ProductSubcategoryID { get; set; }

		[DisplayName("")]
		public Int32? ProductModelID { get; set; }

		[DisplayName("")]
		public DateTime SellStartDate { get; set; }

		[DisplayName("")]
		public DateTime? SellEndDate { get; set; }

		[DisplayName("")]
		public DateTime? DiscontinuedDate { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
