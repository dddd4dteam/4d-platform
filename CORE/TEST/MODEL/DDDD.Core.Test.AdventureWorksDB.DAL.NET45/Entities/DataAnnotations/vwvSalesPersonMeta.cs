using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vSalesPerson. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvSalesPersonMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String Title { get; set; }

		[DisplayName("")]
		public String FirstName { get; set; }

		[DisplayName("")]
		public String MiddleName { get; set; }

		[DisplayName("")]
		public String LastName { get; set; }

		[DisplayName("")]
		public String Suffix { get; set; }

		[DisplayName("")]
		public String JobTitle { get; set; }

		[DisplayName("")]
		public String PhoneNumber { get; set; }

		[DisplayName("")]
		public String PhoneNumberType { get; set; }

		[DisplayName("")]
		public String EmailAddress { get; set; }

		[DisplayName("")]
		public Int32 EmailPromotion { get; set; }

		[DisplayName("")]
		public String AddressLine1 { get; set; }

		[DisplayName("")]
		public String AddressLine2 { get; set; }

		[DisplayName("")]
		public String City { get; set; }

		[DisplayName("")]
		public String StateProvinceName { get; set; }

		[DisplayName("")]
		public String PostalCode { get; set; }

		[DisplayName("")]
		public String CountryRegionName { get; set; }

		[DisplayName("")]
		public String TerritoryName { get; set; }

		[DisplayName("")]
		public String TerritoryGroup { get; set; }

		[DisplayName("")]
		public Decimal? SalesQuota { get; set; }

		[DisplayName("")]
		public Decimal SalesYTD { get; set; }

		[DisplayName("")]
		public Decimal SalesLastYear { get; set; }

	}
}
