using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ShipMethod. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ShipMethodMeta
	{

		[DisplayName("")]
		public Int32 ShipMethodID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public Decimal ShipBase { get; set; }

		[DisplayName("")]
		public Decimal ShipRate { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
