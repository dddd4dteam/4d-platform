using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for SalesOrderDetail. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class SalesOrderDetailMeta
	{

		[Display(Name = "")]
		public Int32 SalesOrderID { get; set; }

		[Display(Name = "")]
		public Int32 SalesOrderDetailID { get; set; }

		[DisplayName("")]
		public String CarrierTrackingNumber { get; set; }

		[DisplayName("")]
		public Int16 OrderQty { get; set; }

		[DisplayName("")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public Int32 SpecialOfferID { get; set; }

		[DisplayName("")]
		public Decimal UnitPrice { get; set; }

		[DisplayName("")]
		public Decimal UnitPriceDiscount { get; set; }

		[DisplayName("")]
		public Decimal? LineTotal { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
