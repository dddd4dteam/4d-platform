using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductCostHistory. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductCostHistoryMeta
	{

		[Display(Name = "")]
		public Int32 ProductID { get; set; }

		[Display(Name = "")]
		public DateTime StartDate { get; set; }

		[DisplayName("")]
		public DateTime? EndDate { get; set; }

		[DisplayName("")]
		public Decimal StandardCost { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
