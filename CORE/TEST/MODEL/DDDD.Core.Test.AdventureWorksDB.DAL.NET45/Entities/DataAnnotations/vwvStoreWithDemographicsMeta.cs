using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vStoreWithDemographics. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvStoreWithDemographicsMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public Decimal? AnnualSales { get; set; }

		[DisplayName("")]
		public Decimal? AnnualRevenue { get; set; }

		[DisplayName("")]
		public String BankName { get; set; }

		[DisplayName("")]
		public String BusinessType { get; set; }

		[DisplayName("")]
		public Int32? YearOpened { get; set; }

		[DisplayName("")]
		public String Specialty { get; set; }

		[DisplayName("")]
		public Int32? SquareFeet { get; set; }

		[DisplayName("")]
		public String Brands { get; set; }

		[DisplayName("")]
		public String Internet { get; set; }

		[DisplayName("")]
		public Int32? NumberEmployees { get; set; }

	}
}
