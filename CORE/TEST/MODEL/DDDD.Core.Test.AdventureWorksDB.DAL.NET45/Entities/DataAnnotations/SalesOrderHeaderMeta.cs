using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for SalesOrderHeader. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class SalesOrderHeaderMeta
	{

		[Display(Name = "")]
		public Int32 SalesOrderID { get; set; }

		[DisplayName("")]
		public Byte RevisionNumber { get; set; }

		[DisplayName("")]
		public DateTime OrderDate { get; set; }

		[DisplayName("")]
		public DateTime DueDate { get; set; }

		[DisplayName("")]
		public DateTime? ShipDate { get; set; }

		[DisplayName("")]
		public Byte Status { get; set; }

		[DisplayName("")]
		public Boolean OnlineOrderFlag { get; set; }

		[DisplayName("")]
		public String SalesOrderNumber { get; set; }

		[DisplayName("")]
		public String PurchaseOrderNumber { get; set; }

		[DisplayName("")]
		public String AccountNumber { get; set; }

		[DisplayName("")]
		public Int32 CustomerID { get; set; }

		[DisplayName("")]
		public Int32? SalesPersonID { get; set; }

		[DisplayName("")]
		public Int32? TerritoryID { get; set; }

		[DisplayName("")]
		public Int32 BillToAddressID { get; set; }

		[DisplayName("")]
		public Int32 ShipToAddressID { get; set; }

		[DisplayName("")]
		public Int32 ShipMethodID { get; set; }

		[DisplayName("")]
		public Int32? CreditCardID { get; set; }

		[DisplayName("")]
		public String CreditCardApprovalCode { get; set; }

		[DisplayName("")]
		public Int32? CurrencyRateID { get; set; }

		[DisplayName("")]
		public Decimal SubTotal { get; set; }

		[DisplayName("")]
		public Decimal TaxAmt { get; set; }

		[DisplayName("")]
		public Decimal Freight { get; set; }

		[DisplayName("")]
		public Decimal? TotalDue { get; set; }

		[DisplayName("")]
		public String Comment { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
