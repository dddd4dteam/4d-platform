using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductInventory. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductInventoryMeta
	{

		[Display(Name = "")]
		public Int32 ProductID { get; set; }

		[Display(Name = "")]
		public Int16 LocationID { get; set; }

		[DisplayName("")]
		public String Shelf { get; set; }

		[DisplayName("")]
		public Byte Bin { get; set; }

		[DisplayName("")]
		public Int16 Quantity { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
