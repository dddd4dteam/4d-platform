using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vPersonDemographics. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvPersonDemographicsMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public Decimal? TotalPurchaseYTD { get; set; }

		[DisplayName("")]
		public DateTime? DateFirstPurchase { get; set; }

		[DisplayName("")]
		public DateTime? BirthDate { get; set; }

		[DisplayName("")]
		public String MaritalStatus { get; set; }

		[DisplayName("")]
		public String YearlyIncome { get; set; }

		[DisplayName("")]
		public String Gender { get; set; }

		[DisplayName("")]
		public Int32? TotalChildren { get; set; }

		[DisplayName("")]
		public Int32? NumberChildrenAtHome { get; set; }

		[DisplayName("")]
		public String Education { get; set; }

		[DisplayName("")]
		public String Occupation { get; set; }

		[DisplayName("")]
		public Boolean? HomeOwnerFlag { get; set; }

		[DisplayName("")]
		public Int32? NumberCarsOwned { get; set; }

	}
}
