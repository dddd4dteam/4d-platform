using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for CreditCard. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class CreditCardMeta
	{

		[Display(Name = "")]
		public Int32 CreditCardID { get; set; }

		[DisplayName("")]
		public String CardType { get; set; }

		[DisplayName("")]
		public String CardNumber { get; set; }

		[DisplayName("")]
		public Byte ExpMonth { get; set; }

		[DisplayName("")]
		public Int16 ExpYear { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
