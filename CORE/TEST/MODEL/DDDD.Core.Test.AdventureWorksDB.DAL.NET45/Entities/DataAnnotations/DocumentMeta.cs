using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Document. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class DocumentMeta
	{

		//[Display(Name = "")]
		//public Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }

		[DisplayName("")]
		public Int16? DocumentLevel { get; set; }

		[DisplayName("")]
		public String Title { get; set; }

		[DisplayName("")]
		public Int32 Owner { get; set; }

		[DisplayName("")]
		public Boolean FolderFlag { get; set; }

		[DisplayName("")]
		public String FileName { get; set; }

		[DisplayName("")]
		public String FileExtension { get; set; }

		[DisplayName("")]
		public String Revision { get; set; }

		[DisplayName("")]
		public Int32 ChangeNumber { get; set; }

		[DisplayName("")]
		public Byte Status { get; set; }

		[DisplayName("")]
		public String DocumentSummary { get; set; }

		[DisplayName("")]
		public Byte[] Document_ { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
