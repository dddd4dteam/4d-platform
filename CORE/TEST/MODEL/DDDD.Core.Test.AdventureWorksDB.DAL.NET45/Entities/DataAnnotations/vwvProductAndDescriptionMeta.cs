using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vProductAndDescription. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvProductAndDescriptionMeta
	{

		[DisplayName("")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public String ProductModel { get; set; }

		[DisplayName("")]
		public String CultureID { get; set; }

		[DisplayName("")]
		public String Description { get; set; }

	}
}
