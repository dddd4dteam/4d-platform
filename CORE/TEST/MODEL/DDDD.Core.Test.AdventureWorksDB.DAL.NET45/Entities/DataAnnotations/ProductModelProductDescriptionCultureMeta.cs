using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductModelProductDescriptionCulture. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductModelProductDescriptionCultureMeta
	{

		[Display(Name = "")]
		public Int32 ProductModelID { get; set; }

		[Display(Name = "")]
		public Int32 ProductDescriptionID { get; set; }

		[Display(Name = "")]
		public String CultureID { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
