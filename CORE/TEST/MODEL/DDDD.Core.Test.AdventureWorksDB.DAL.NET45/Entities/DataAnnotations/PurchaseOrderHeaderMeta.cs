using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for PurchaseOrderHeader. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class PurchaseOrderHeaderMeta
	{

		[Display(Name = "")]
		public Int32 PurchaseOrderID { get; set; }

		[DisplayName("")]
		public Byte RevisionNumber { get; set; }

		[DisplayName("")]
		public Byte Status { get; set; }

		[DisplayName("")]
		public Int32 EmployeeID { get; set; }

		[DisplayName("")]
		public Int32 VendorID { get; set; }

		[DisplayName("")]
		public Int32 ShipMethodID { get; set; }

		[DisplayName("")]
		public DateTime OrderDate { get; set; }

		[DisplayName("")]
		public DateTime? ShipDate { get; set; }

		[DisplayName("")]
		public Decimal SubTotal { get; set; }

		[DisplayName("")]
		public Decimal TaxAmt { get; set; }

		[DisplayName("")]
		public Decimal Freight { get; set; }

		[DisplayName("")]
		public Decimal? TotalDue { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
