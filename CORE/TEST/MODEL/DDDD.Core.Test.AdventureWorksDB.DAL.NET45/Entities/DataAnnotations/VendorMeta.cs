using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for Vendor. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class VendorMeta
	{

		[Display(Name = "")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String AccountNumber { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public Byte CreditRating { get; set; }

		[DisplayName("")]
		public Boolean PreferredVendorStatus { get; set; }

		[DisplayName("")]
		public Boolean ActiveFlag { get; set; }

		[DisplayName("")]
		public String PurchasingWebServiceURL { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
