using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for BillOfMaterials. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class BillOfMaterialsMeta
	{

		[DisplayName("")]
		public Int32 BillOfMaterialsID { get; set; }

		[DisplayName("")]
		public Int32? ProductAssemblyID { get; set; }

		[DisplayName("")]
		public Int32 ComponentID { get; set; }

		[DisplayName("")]
		public DateTime StartDate { get; set; }

		[DisplayName("")]
		public DateTime? EndDate { get; set; }

		[DisplayName("")]
		public String UnitMeasureCode { get; set; }

		[DisplayName("")]
		public Int16 BOMLevel { get; set; }

		[DisplayName("")]
		public Decimal PerAssemblyQty { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
