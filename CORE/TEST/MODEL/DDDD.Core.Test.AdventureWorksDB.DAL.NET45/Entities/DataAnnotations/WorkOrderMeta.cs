using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for WorkOrder. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class WorkOrderMeta
	{

		[Display(Name = "")]
		public Int32 WorkOrderID { get; set; }

		[DisplayName("")]
		public Int32 ProductID { get; set; }

		[DisplayName("")]
		public Int32 OrderQty { get; set; }

		[DisplayName("")]
		public Int32? StockedQty { get; set; }

		[DisplayName("")]
		public Int16 ScrappedQty { get; set; }

		[DisplayName("")]
		public DateTime StartDate { get; set; }

		[DisplayName("")]
		public DateTime? EndDate { get; set; }

		[DisplayName("")]
		public DateTime DueDate { get; set; }

		[DisplayName("")]
		public Int16? ScrapReasonID { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
