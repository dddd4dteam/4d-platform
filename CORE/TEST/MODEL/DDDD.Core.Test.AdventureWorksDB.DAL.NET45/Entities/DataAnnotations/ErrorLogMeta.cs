using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{


	/// <summary>
	/// Data annotations attributes for ErrorLog. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ErrorLogMeta
	{

		[DisplayName("Primary key for ErrorLog records.")]
		public Int32 ErrorLogID { get; set; }

		[DisplayName("The date and time at which the error occurred.")]
		public DateTime ErrorTime { get; set; }

		[DisplayName("The user who executed the batch in which the error occurred.")]
		public String UserName { get; set; }

		[DisplayName("The error number of the error that occurred.")]
		public Int32 ErrorNumber { get; set; }

		[DisplayName("The severity of the error that occurred.")]
		public Int32? ErrorSeverity { get; set; }

		[DisplayName("The state number of the error that occurred.")]
		public Int32? ErrorState { get; set; }

		[DisplayName("The name of the stored procedure or trigger where the error occurred.")]
		public String ErrorProcedure { get; set; }

		[DisplayName("The line number at which the error occurred.")]
		public Int32? ErrorLine { get; set; }

		[DisplayName("The message text of the error that occurred.")]
		public String ErrorMessage { get; set; }

	}
}
