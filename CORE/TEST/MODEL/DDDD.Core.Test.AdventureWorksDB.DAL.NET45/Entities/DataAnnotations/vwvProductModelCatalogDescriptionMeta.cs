using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vProductModelCatalogDescription. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvProductModelCatalogDescriptionMeta
	{

		[DisplayName("")]
		public Int32 ProductModelID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public String Summary { get; set; }

		[DisplayName("")]
		public String Manufacturer { get; set; }

		[DisplayName("")]
		public String Copyright { get; set; }

		[DisplayName("")]
		public String ProductURL { get; set; }

		[DisplayName("")]
		public String WarrantyPeriod { get; set; }

		[DisplayName("")]
		public String WarrantyDescription { get; set; }

		[DisplayName("")]
		public String NoOfYears { get; set; }

		[DisplayName("")]
		public String MaintenanceDescription { get; set; }

		[DisplayName("")]
		public String Wheel { get; set; }

		[DisplayName("")]
		public String Saddle { get; set; }

		[DisplayName("")]
		public String Pedal { get; set; }

		[DisplayName("")]
		public String BikeFrame { get; set; }

		[DisplayName("")]
		public String Crankset { get; set; }

		[DisplayName("")]
		public String PictureAngle { get; set; }

		[DisplayName("")]
		public String PictureSize { get; set; }

		[DisplayName("")]
		public String ProductPhotoID { get; set; }

		[DisplayName("")]
		public String Material { get; set; }

		[DisplayName("")]
		public String Color { get; set; }

		[DisplayName("")]
		public String ProductLine { get; set; }

		[DisplayName("")]
		public String Style { get; set; }

		[DisplayName("")]
		public String RiderExperience { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
