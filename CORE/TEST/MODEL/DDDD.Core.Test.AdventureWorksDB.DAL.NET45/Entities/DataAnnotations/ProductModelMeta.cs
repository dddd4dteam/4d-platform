using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for ProductModel. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class ProductModelMeta
	{

		[Display(Name = "")]
		public Int32 ProductModelID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public String CatalogDescription { get; set; }

		[DisplayName("")]
		public String Instructions { get; set; }

		[DisplayName("")]
		public System.Guid rowguid { get; set; }

		[DisplayName("")]
		public DateTime ModifiedDate { get; set; }

	}
}
