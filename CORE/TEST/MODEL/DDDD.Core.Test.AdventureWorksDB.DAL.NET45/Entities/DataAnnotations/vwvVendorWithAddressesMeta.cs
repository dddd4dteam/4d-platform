using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.DataAnnotations
{
	/// <summary>
	/// Data annotations attributes for vVendorWithAddresses. 
	/// This class won't update by generator. Delete this file to get updated fields.
	/// </summary>
	public class vwvVendorWithAddressesMeta
	{

		[DisplayName("")]
		public Int32 BusinessEntityID { get; set; }

		[DisplayName("")]
		public String Name { get; set; }

		[DisplayName("")]
		public String AddressType { get; set; }

		[DisplayName("")]
		public String AddressLine1 { get; set; }

		[DisplayName("")]
		public String AddressLine2 { get; set; }

		[DisplayName("")]
		public String City { get; set; }

		[DisplayName("")]
		public String StateProvinceName { get; set; }

		[DisplayName("")]
		public String PostalCode { get; set; }

		[DisplayName("")]
		public String CountryRegionName { get; set; }

	}
}
