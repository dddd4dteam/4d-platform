using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.Entities
{

	[DataContract]
	public partial class ScrapReason : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 ScrapReasonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// WorkOrder - ScrapReasonID
		/// </summary>
		[DisplayName("WorkOrder")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<WorkOrder> WorkOrder { get; set; }

	}



	[DataContract]
	public partial class Shift : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte ShiftID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.TimeSpan StartTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.TimeSpan EndTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// EmployeeDepartmentHistory - ShiftID
		/// </summary>
		[DisplayName("EmployeeDepartmentHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual EmployeeDepartmentHistory EmployeeDepartmentHistory { get; set; }

	}

	[DataContract]
	public partial class ProductCategory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductCategoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductSubcategory - ProductCategoryID
		/// </summary>
		[DisplayName("ProductSubcategory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<ProductSubcategory> ProductSubcategory { get; set; }

	}

	[DataContract]
	public partial class ShipMethod : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ShipMethodID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ShipBase { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ShipRate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// PurchaseOrderHeader - ShipMethodID
		/// </summary>
		[DisplayName("PurchaseOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<PurchaseOrderHeader> PurchaseOrderHeader { get; set; }

		/// <summary>
		/// SalesOrderHeader - ShipMethodID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

	}

	[DataContract]
	public partial class ProductCostHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal StandardCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class ProductDescription : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductDescriptionID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Description { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductModelProductDescriptionCulture - ProductDescriptionID
		/// </summary>
		[DisplayName("ProductModelProductDescriptionCulture")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModelProductDescriptionCulture ProductModelProductDescriptionCulture { get; set; }

	}

	[DataContract]
	public partial class ShoppingCartItem : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ShoppingCartItemID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ShoppingCartID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 Quantity { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime DateCreated { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class ProductDocument : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// Document - DocumentNode
		/// </summary>
		[DisplayName("Document")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Document Document { get; set; }

	}

	[DataContract]
	public partial class DatabaseLog : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// Primary key for DatabaseLog records.
		/// </summary>
		[DisplayName("Primary key for DatabaseLog records.")]
		[DataMember]
		public virtual Int32 DatabaseLogID { get; set; }

		/// <summary>
		/// The date and time the DDL change occurred.
		/// </summary>
		[DisplayName("The date and time the DDL change occurred.")]
		[DataMember]
		public virtual DateTime PostTime { get; set; }

		/// <summary>
		/// The user who implemented the DDL change.
		/// </summary>
		[DisplayName("The user who implemented the DDL change.")]
		[DataMember]
		public virtual String DatabaseUser { get; set; }

		/// <summary>
		/// The type of DDL statement that was executed.
		/// </summary>
		[DisplayName("The type of DDL statement that was executed.")]
		[DataMember]
		public virtual String Event { get; set; }

		/// <summary>
		/// The schema to which the changed object belongs.
		/// </summary>
		[DisplayName("The schema to which the changed object belongs.")]
		[DataMember]
		public virtual String Schema { get; set; }

		/// <summary>
		/// The object that was changed by the DDL statment.
		/// </summary>
		[DisplayName("The object that was changed by the DDL statment.")]
		[DataMember]
		public virtual String Object { get; set; }

		/// <summary>
		/// The exact Transact-SQL statement that was executed.
		/// </summary>
		[DisplayName("The exact Transact-SQL statement that was executed.")]
		[DataMember]
		public virtual String TSQL { get; set; }

		/// <summary>
		/// The raw XML data generated by database trigger.
		/// </summary>
		[DisplayName("The raw XML data generated by database trigger.")]
		[DataMember]
		public virtual String XmlEvent { get; set; }


	}

	[DataContract]
	public partial class ProductInventory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 LocationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Shelf { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte Bin { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 Quantity { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// Location - LocationID
		/// </summary>
		[DisplayName("Location")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Location Location { get; set; }

	}

	[DataContract]
	public partial class SpecialOffer : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SpecialOfferID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Description { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal DiscountPct { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Type { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Category { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 MinQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? MaxQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SpecialOfferProduct - SpecialOfferID
		/// </summary>
		[DisplayName("SpecialOfferProduct")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SpecialOfferProduct SpecialOfferProduct { get; set; }

	}

	[DataContract]
	public partial class ErrorLog : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// Primary key for ErrorLog records.
		/// </summary>
		[DisplayName("Primary key for ErrorLog records.")]
		[DataMember]
		public virtual Int32 ErrorLogID { get; set; }

		/// <summary>
		/// The date and time at which the error occurred.
		/// </summary>
		[DisplayName("The date and time at which the error occurred.")]
		[DataMember]
		public virtual DateTime ErrorTime { get; set; }

		/// <summary>
		/// The user who executed the batch in which the error occurred.
		/// </summary>
		[DisplayName("The user who executed the batch in which the error occurred.")]
		[DataMember]
		public virtual String UserName { get; set; }

		/// <summary>
		/// The error number of the error that occurred.
		/// </summary>
		[DisplayName("The error number of the error that occurred.")]
		[DataMember]
		public virtual Int32 ErrorNumber { get; set; }

		/// <summary>
		/// The severity of the error that occurred.
		/// </summary>
		[DisplayName("The severity of the error that occurred.")]
		[DataMember]
		public virtual Int32? ErrorSeverity { get; set; }

		/// <summary>
		/// The state number of the error that occurred.
		/// </summary>
		[DisplayName("The state number of the error that occurred.")]
		[DataMember]
		public virtual Int32? ErrorState { get; set; }

		/// <summary>
		/// The name of the stored procedure or trigger where the error occurred.
		/// </summary>
		[DisplayName("The name of the stored procedure or trigger where the error occurred.")]
		[DataMember]
		public virtual String ErrorProcedure { get; set; }

		/// <summary>
		/// The line number at which the error occurred.
		/// </summary>
		[DisplayName("The line number at which the error occurred.")]
		[DataMember]
		public virtual Int32? ErrorLine { get; set; }

		/// <summary>
		/// The message text of the error that occurred.
		/// </summary>
		[DisplayName("The message text of the error that occurred.")]
		[DataMember]
		public virtual String ErrorMessage { get; set; }


	}

	[DataContract]
	public partial class ProductListPriceHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ListPrice { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class Address : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 AddressID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 StateProvinceID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Microsoft.SqlServer.Types.SqlGeography SpatialLocation { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// StateProvince - StateProvinceID
		/// </summary>
		[DisplayName("StateProvince")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual StateProvince StateProvince { get; set; }

		/// <summary>
		/// BusinessEntityAddress - AddressID
		/// </summary>
		[DisplayName("BusinessEntityAddress")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityAddress BusinessEntityAddress { get; set; }

		/// <summary>
		/// SalesOrderHeader - BillToAddressID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

		/// <summary>
		/// SalesOrderHeader - ShipToAddressID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader_ { get; set; }

	}

	[DataContract]
	public partial class SpecialOfferProduct : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SpecialOfferID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderDetail - ProductID
		/// </summary>
		[DisplayName("SalesOrderDetail")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderDetail> SalesOrderDetail { get; set; }

		/// <summary>
		/// SpecialOffer - SpecialOfferID
		/// </summary>
		[DisplayName("SpecialOffer")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SpecialOffer SpecialOffer { get; set; }

		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class ProductModel : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CatalogDescription { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Instructions { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductModelID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Product> Product { get; set; }

		/// <summary>
		/// ProductModelIllustration - ProductModelID
		/// </summary>
		[DisplayName("ProductModelIllustration")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModelIllustration ProductModelIllustration { get; set; }

		/// <summary>
		/// ProductModelProductDescriptionCulture - ProductModelID
		/// </summary>
		[DisplayName("ProductModelProductDescriptionCulture")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModelProductDescriptionCulture ProductModelProductDescriptionCulture { get; set; }

	}

	[DataContract]
	public partial class AddressType : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 AddressTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntityAddress - AddressTypeID
		/// </summary>
		[DisplayName("BusinessEntityAddress")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityAddress BusinessEntityAddress { get; set; }

	}

	[DataContract]
	public partial class StateProvince : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 StateProvinceID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean IsOnlyStateProvinceFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Address - StateProvinceID
		/// </summary>
		[DisplayName("Address")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Address> Address { get; set; }

		/// <summary>
		/// SalesTaxRate - StateProvinceID
		/// </summary>
		[DisplayName("SalesTaxRate")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesTaxRate> SalesTaxRate { get; set; }

		/// <summary>
		/// CountryRegion - CountryRegionCode
		/// </summary>
		[DisplayName("CountryRegion")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CountryRegion CountryRegion { get; set; }

		/// <summary>
		/// SalesTerritory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritory SalesTerritory { get; set; }

	}

	[DataContract]
	public partial class ProductModelIllustration : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 IllustrationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductModel - ProductModelID
		/// </summary>
		[DisplayName("ProductModel")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModel ProductModel { get; set; }

		/// <summary>
		/// Illustration - IllustrationID
		/// </summary>
		[DisplayName("Illustration")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Illustration Illustration { get; set; }

	}

	[DataContract]
	public partial class AWBuildVersion : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// Primary key for AWBuildVersion records.
		/// </summary>
		[DisplayName("Primary key for AWBuildVersion records.")]
		[DataMember]
		public virtual Byte SystemInformationID { get; set; }

		/// <summary>
		/// Version number of the database in 9.yy.mm.dd.00 format.
		/// </summary>
		[DisplayName("Version number of the database in 9.yy.mm.dd.00 format.")]
		[DataMember]
		public virtual String Database_Version { get; set; }

		/// <summary>
		/// Date and time the record was last updated.
		/// </summary>
		[DisplayName("Date and time the record was last updated.")]
		[DataMember]
		public virtual DateTime VersionDate { get; set; }

		/// <summary>
		/// Date and time the record was last updated.
		/// </summary>
		[DisplayName("Date and time the record was last updated.")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class ProductModelProductDescriptionCulture : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductDescriptionID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CultureID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductModel - ProductModelID
		/// </summary>
		[DisplayName("ProductModel")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModel ProductModel { get; set; }

		/// <summary>
		/// ProductDescription - ProductDescriptionID
		/// </summary>
		[DisplayName("ProductDescription")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductDescription ProductDescription { get; set; }

		/// <summary>
		/// Culture - CultureID
		/// </summary>
		[DisplayName("Culture")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Culture Culture { get; set; }

	}

	[DataContract]
	public partial class BillOfMaterials : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BillOfMaterialsID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? ProductAssemblyID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ComponentID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String UnitMeasureCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 BOMLevel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal PerAssemblyQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductAssemblyID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// Product - ComponentID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product_ { get; set; }

		/// <summary>
		/// UnitMeasure - UnitMeasureCode
		/// </summary>
		[DisplayName("UnitMeasure")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual UnitMeasure UnitMeasure { get; set; }

	}

	[DataContract]
	public partial class Store : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? SalesPersonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Demographics { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Customer - StoreID
		/// </summary>
		[DisplayName("Customer")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Customer> Customer { get; set; }

		/// <summary>
		/// BusinessEntity - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntity")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntity BusinessEntity { get; set; }

		/// <summary>
		/// SalesPerson - SalesPersonID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPerson SalesPerson { get; set; }

	}

	[DataContract]
	public partial class ProductPhoto : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductPhotoID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte[] ThumbNailPhoto { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ThumbnailPhotoFileName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte[] LargePhoto { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LargePhotoFileName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductProductPhoto - ProductPhotoID
		/// </summary>
		[DisplayName("ProductProductPhoto")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductProductPhoto ProductProductPhoto { get; set; }

	}

	[DataContract]
	public partial class ProductProductPhoto : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductPhotoID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean Primary { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// ProductPhoto - ProductPhotoID
		/// </summary>
		[DisplayName("ProductPhoto")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductPhoto ProductPhoto { get; set; }

	}

	[DataContract]
	public partial class TransactionHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TransactionID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ReferenceOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ReferenceOrderLineID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime TransactionDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TransactionType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 Quantity { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ActualCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class ProductReview : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductReviewID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ReviewerName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ReviewDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 Rating { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Comments { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class BusinessEntity : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntityAddress - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntityAddress")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityAddress BusinessEntityAddress { get; set; }

		/// <summary>
		/// BusinessEntityContact - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntityContact")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityContact BusinessEntityContact { get; set; }

		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// Store - BusinessEntityID
		/// </summary>
		[DisplayName("Store")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Store Store { get; set; }

		/// <summary>
		/// Vendor - BusinessEntityID
		/// </summary>
		[DisplayName("Vendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Vendor Vendor { get; set; }

	}

	[DataContract]
	public partial class TransactionHistoryArchive : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TransactionID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ReferenceOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ReferenceOrderLineID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime TransactionDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TransactionType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 Quantity { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ActualCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class ProductSubcategory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductSubcategoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductCategoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductSubcategoryID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Product> Product { get; set; }

		/// <summary>
		/// ProductCategory - ProductCategoryID
		/// </summary>
		[DisplayName("ProductCategory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductCategory ProductCategory { get; set; }

	}

	[DataContract]
	public partial class BusinessEntityAddress : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 AddressID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 AddressTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntity - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntity")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntity BusinessEntity { get; set; }

		/// <summary>
		/// Address - AddressID
		/// </summary>
		[DisplayName("Address")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Address Address { get; set; }

		/// <summary>
		/// AddressType - AddressTypeID
		/// </summary>
		[DisplayName("AddressType")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual AddressType AddressType { get; set; }

	}

	[DataContract]
	public partial class ProductVendor : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 AverageLeadTime { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal StandardPrice { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? LastReceiptCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? LastReceiptDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 MinOrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 MaxOrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? OnOrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String UnitMeasureCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// Vendor - BusinessEntityID
		/// </summary>
		[DisplayName("Vendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Vendor Vendor { get; set; }

		/// <summary>
		/// UnitMeasure - UnitMeasureCode
		/// </summary>
		[DisplayName("UnitMeasure")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual UnitMeasure UnitMeasure { get; set; }

	}

	[DataContract]
	public partial class BusinessEntityContact : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PersonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ContactTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntity - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntity")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntity BusinessEntity { get; set; }

		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// ContactType - ContactTypeID
		/// </summary>
		[DisplayName("ContactType")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ContactType ContactType { get; set; }

	}

	[DataContract]
	public partial class UnitMeasure : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String UnitMeasureCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BillOfMaterials - UnitMeasureCode
		/// </summary>
		[DisplayName("BillOfMaterials")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<BillOfMaterials> BillOfMaterials { get; set; }

		/// <summary>
		/// Product - SizeUnitMeasureCode
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Product> Product { get; set; }

		/// <summary>
		/// Product - WeightUnitMeasureCode
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Product> Product_ { get; set; }

		/// <summary>
		/// ProductVendor - UnitMeasureCode
		/// </summary>
		[DisplayName("ProductVendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<ProductVendor> ProductVendor { get; set; }

	}

	[DataContract]
	public partial class Vendor : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AccountNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte CreditRating { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean PreferredVendorStatus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean ActiveFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PurchasingWebServiceURL { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductVendor - BusinessEntityID
		/// </summary>
		[DisplayName("ProductVendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductVendor ProductVendor { get; set; }

		/// <summary>
		/// PurchaseOrderHeader - VendorID
		/// </summary>
		[DisplayName("PurchaseOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<PurchaseOrderHeader> PurchaseOrderHeader { get; set; }

		/// <summary>
		/// BusinessEntity - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntity")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntity BusinessEntity { get; set; }

	}

	[DataContract]
	public partial class ContactType : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ContactTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntityContact - ContactTypeID
		/// </summary>
		[DisplayName("BusinessEntityContact")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityContact BusinessEntityContact { get; set; }

	}

	[DataContract]
	public partial class CountryRegionCurrency : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CurrencyCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// CountryRegion - CountryRegionCode
		/// </summary>
		[DisplayName("CountryRegion")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CountryRegion CountryRegion { get; set; }

		/// <summary>
		/// Currency - CurrencyCode
		/// </summary>
		[DisplayName("Currency")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Currency Currency { get; set; }

	}

	[DataContract]
	public partial class CountryRegion : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// CountryRegionCurrency - CountryRegionCode
		/// </summary>
		[DisplayName("CountryRegionCurrency")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CountryRegionCurrency CountryRegionCurrency { get; set; }

		/// <summary>
		/// SalesTerritory - CountryRegionCode
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesTerritory> SalesTerritory { get; set; }

		/// <summary>
		/// StateProvince - CountryRegionCode
		/// </summary>
		[DisplayName("StateProvince")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<StateProvince> StateProvince { get; set; }

	}

	[DataContract]
	public partial class WorkOrder : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 WorkOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 OrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? StockedQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 ScrappedQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime DueDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16? ScrapReasonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

		/// <summary>
		/// ScrapReason - ScrapReasonID
		/// </summary>
		[DisplayName("ScrapReason")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ScrapReason ScrapReason { get; set; }

		/// <summary>
		/// WorkOrderRouting - WorkOrderID
		/// </summary>
		[DisplayName("WorkOrderRouting")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual WorkOrderRouting WorkOrderRouting { get; set; }

	}

	[DataContract]
	public partial class PurchaseOrderDetail : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PurchaseOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PurchaseOrderDetailID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime DueDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 OrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal UnitPrice { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? LineTotal { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ReceivedQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal RejectedQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? StockedQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// PurchaseOrderHeader - PurchaseOrderID
		/// </summary>
		[DisplayName("PurchaseOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PurchaseOrderHeader PurchaseOrderHeader { get; set; }

		/// <summary>
		/// Product - ProductID
		/// </summary>
		[DisplayName("Product")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Product Product { get; set; }

	}

	[DataContract]
	public partial class CreditCard : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 CreditCardID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CardType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CardNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte ExpMonth { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 ExpYear { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// PersonCreditCard - CreditCardID
		/// </summary>
		[DisplayName("PersonCreditCard")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PersonCreditCard PersonCreditCard { get; set; }

		/// <summary>
		/// SalesOrderHeader - CreditCardID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

	}

	[DataContract]
	public partial class Culture : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CultureID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductModelProductDescriptionCulture - CultureID
		/// </summary>
		[DisplayName("ProductModelProductDescriptionCulture")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModelProductDescriptionCulture ProductModelProductDescriptionCulture { get; set; }

	}

	[DataContract]
	public partial class WorkOrderRouting : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 WorkOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 OperationSequence { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 LocationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ScheduledStartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ScheduledEndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? ActualStartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? ActualEndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? ActualResourceHrs { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal PlannedCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? ActualCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// WorkOrder - WorkOrderID
		/// </summary>
		[DisplayName("WorkOrder")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual WorkOrder WorkOrder { get; set; }

		/// <summary>
		/// Location - LocationID
		/// </summary>
		[DisplayName("Location")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Location Location { get; set; }

	}

	[DataContract]
	public partial class Currency : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CurrencyCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// CountryRegionCurrency - CurrencyCode
		/// </summary>
		[DisplayName("CountryRegionCurrency")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CountryRegionCurrency CountryRegionCurrency { get; set; }

		/// <summary>
		/// CurrencyRate - FromCurrencyCode
		/// </summary>
		[DisplayName("CurrencyRate")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<CurrencyRate> CurrencyRate { get; set; }

		/// <summary>
		/// CurrencyRate - ToCurrencyCode
		/// </summary>
		[DisplayName("CurrencyRate")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<CurrencyRate> CurrencyRate_ { get; set; }

	}

	[DataContract]
	public partial class PurchaseOrderHeader : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PurchaseOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte RevisionNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmployeeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 VendorID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ShipMethodID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime OrderDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? ShipDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SubTotal { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal TaxAmt { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal Freight { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? TotalDue { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// PurchaseOrderDetail - PurchaseOrderID
		/// </summary>
		[DisplayName("PurchaseOrderDetail")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PurchaseOrderDetail PurchaseOrderDetail { get; set; }

		/// <summary>
		/// Employee - EmployeeID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

		/// <summary>
		/// Vendor - VendorID
		/// </summary>
		[DisplayName("Vendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Vendor Vendor { get; set; }

		/// <summary>
		/// ShipMethod - ShipMethodID
		/// </summary>
		[DisplayName("ShipMethod")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ShipMethod ShipMethod { get; set; }

	}

	[DataContract]
	public partial class CurrencyRate : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 CurrencyRateID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime CurrencyRateDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FromCurrencyCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ToCurrencyCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal AverageRate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal EndOfDayRate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Currency - FromCurrencyCode
		/// </summary>
		[DisplayName("Currency")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Currency Currency { get; set; }

		/// <summary>
		/// Currency - ToCurrencyCode
		/// </summary>
		[DisplayName("Currency")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Currency Currency_ { get; set; }

		/// <summary>
		/// SalesOrderHeader - CurrencyRateID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

	}

	[DataContract]
	public partial class Customer : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 CustomerID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? PersonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? StoreID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AccountNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Person - PersonID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// Store - StoreID
		/// </summary>
		[DisplayName("Store")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Store Store { get; set; }

		/// <summary>
		/// SalesTerritory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritory SalesTerritory { get; set; }

		/// <summary>
		/// SalesOrderHeader - CustomerID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

	}

	[DataContract]
	public partial class Department : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 DepartmentID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String GroupName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// EmployeeDepartmentHistory - DepartmentID
		/// </summary>
		[DisplayName("EmployeeDepartmentHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual EmployeeDepartmentHistory EmployeeDepartmentHistory { get; set; }

	}

	[DataContract]
	public partial class Document : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Microsoft.SqlServer.Types.SqlHierarchyId DocumentNode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16? DocumentLevel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 Owner { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean FolderFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FileName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FileExtension { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Revision { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ChangeNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String DocumentSummary { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte[] Document_ { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Employee - Owner
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

		/// <summary>
		/// ProductDocument - DocumentNode
		/// </summary>
		[DisplayName("ProductDocument")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductDocument ProductDocument { get; set; }

	}

	[DataContract]
	public partial class SalesOrderDetail : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesOrderDetailID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CarrierTrackingNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 OrderQty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SpecialOfferID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal UnitPrice { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal UnitPriceDiscount { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? LineTotal { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderHeader - SalesOrderID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesOrderHeader SalesOrderHeader { get; set; }

		/// <summary>
		/// SpecialOfferProduct - ProductID
		/// </summary>
		[DisplayName("SpecialOfferProduct")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SpecialOfferProduct SpecialOfferProduct { get; set; }

	}

	[DataContract]
	public partial class EmailAddress : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailAddressID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress_ { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

	}

	[DataContract]
	public partial class Employee : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String NationalIDNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LoginID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Microsoft.SqlServer.Types.SqlHierarchyId? OrganizationNode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16? OrganizationLevel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String JobTitle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime BirthDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MaritalStatus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Gender { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime HireDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean SalariedFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 VacationHours { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 SickLeaveHours { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean CurrentFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Document - Owner
		/// </summary>
		[DisplayName("Document")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Document> Document { get; set; }

		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// EmployeeDepartmentHistory - BusinessEntityID
		/// </summary>
		[DisplayName("EmployeeDepartmentHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual EmployeeDepartmentHistory EmployeeDepartmentHistory { get; set; }

		/// <summary>
		/// EmployeePayHistory - BusinessEntityID
		/// </summary>
		[DisplayName("EmployeePayHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual EmployeePayHistory EmployeePayHistory { get; set; }

		/// <summary>
		/// JobCandidate - BusinessEntityID
		/// </summary>
		[DisplayName("JobCandidate")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<JobCandidate> JobCandidate { get; set; }

		/// <summary>
		/// PurchaseOrderHeader - EmployeeID
		/// </summary>
		[DisplayName("PurchaseOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<PurchaseOrderHeader> PurchaseOrderHeader { get; set; }

		/// <summary>
		/// SalesPerson - BusinessEntityID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPerson SalesPerson { get; set; }

	}

	[DataContract]
	public partial class SalesOrderHeader : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte RevisionNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime OrderDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime DueDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? ShipDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte Status { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean OnlineOrderFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String SalesOrderNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PurchaseOrderNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AccountNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 CustomerID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? SalesPersonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BillToAddressID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ShipToAddressID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ShipMethodID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? CreditCardID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CreditCardApprovalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? CurrencyRateID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SubTotal { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal TaxAmt { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal Freight { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? TotalDue { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Comment { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderDetail - SalesOrderID
		/// </summary>
		[DisplayName("SalesOrderDetail")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesOrderDetail SalesOrderDetail { get; set; }

		/// <summary>
		/// Customer - CustomerID
		/// </summary>
		[DisplayName("Customer")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Customer Customer { get; set; }

		/// <summary>
		/// SalesPerson - SalesPersonID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPerson SalesPerson { get; set; }

		/// <summary>
		/// SalesTerritory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritory SalesTerritory { get; set; }

		/// <summary>
		/// Address - BillToAddressID
		/// </summary>
		[DisplayName("Address")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Address Address { get; set; }

		/// <summary>
		/// Address - ShipToAddressID
		/// </summary>
		[DisplayName("Address")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Address Address_ { get; set; }

		/// <summary>
		/// ShipMethod - ShipMethodID
		/// </summary>
		[DisplayName("ShipMethod")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ShipMethod ShipMethod { get; set; }

		/// <summary>
		/// CreditCard - CreditCardID
		/// </summary>
		[DisplayName("CreditCard")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CreditCard CreditCard { get; set; }

		/// <summary>
		/// CurrencyRate - CurrencyRateID
		/// </summary>
		[DisplayName("CurrencyRate")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CurrencyRate CurrencyRate { get; set; }

		/// <summary>
		/// SalesOrderHeaderSalesReason - SalesOrderID
		/// </summary>
		[DisplayName("SalesOrderHeaderSalesReason")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesOrderHeaderSalesReason SalesOrderHeaderSalesReason { get; set; }

	}

	[DataContract]
	public partial class EmployeeDepartmentHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 DepartmentID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte ShiftID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Employee - BusinessEntityID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

		/// <summary>
		/// Department - DepartmentID
		/// </summary>
		[DisplayName("Department")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Department Department { get; set; }

		/// <summary>
		/// Shift - ShiftID
		/// </summary>
		[DisplayName("Shift")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Shift Shift { get; set; }

	}

	[DataContract]
	public partial class EmployeePayHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime RateChangeDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal Rate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte PayFrequency { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Employee - BusinessEntityID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

	}

	[DataContract]
	public partial class SalesOrderHeaderSalesReason : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesOrderID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesReasonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderHeader - SalesOrderID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesOrderHeader SalesOrderHeader { get; set; }

		/// <summary>
		/// SalesReason - SalesReasonID
		/// </summary>
		[DisplayName("SalesReason")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesReason SalesReason { get; set; }

	}

	[DataContract]
	public partial class SalesPerson : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? SalesQuota { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal Bonus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal CommissionPct { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesYTD { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesLastYear { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderHeader - SalesPersonID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

		/// <summary>
		/// Employee - BusinessEntityID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

		/// <summary>
		/// SalesTerritory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritory SalesTerritory { get; set; }

		/// <summary>
		/// SalesPersonQuotaHistory - BusinessEntityID
		/// </summary>
		[DisplayName("SalesPersonQuotaHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPersonQuotaHistory SalesPersonQuotaHistory { get; set; }

		/// <summary>
		/// SalesTerritoryHistory - BusinessEntityID
		/// </summary>
		[DisplayName("SalesTerritoryHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritoryHistory SalesTerritoryHistory { get; set; }

		/// <summary>
		/// Store - SalesPersonID
		/// </summary>
		[DisplayName("Store")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Store> Store { get; set; }

	}

	[DataContract]
	public partial class Illustration : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 IllustrationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Diagram { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductModelIllustration - IllustrationID
		/// </summary>
		[DisplayName("ProductModelIllustration")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModelIllustration ProductModelIllustration { get; set; }

	}

	[DataContract]
	public partial class JobCandidate : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 JobCandidateID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Resume { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Employee - BusinessEntityID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

	}

	[DataContract]
	public partial class Location : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 LocationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal CostRate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal Availability { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// ProductInventory - LocationID
		/// </summary>
		[DisplayName("ProductInventory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductInventory ProductInventory { get; set; }

		/// <summary>
		/// WorkOrderRouting - LocationID
		/// </summary>
		[DisplayName("WorkOrderRouting")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<WorkOrderRouting> WorkOrderRouting { get; set; }

	}

	[DataContract]
	public partial class Password : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PasswordHash { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PasswordSalt { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

	}

	[DataContract]
	public partial class SalesPersonQuotaHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime QuotaDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesQuota { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesPerson - BusinessEntityID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPerson SalesPerson { get; set; }

	}

	[DataContract]
	public partial class Person : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PersonType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean NameStyle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AdditionalContactInfo { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Demographics { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BusinessEntityContact - PersonID
		/// </summary>
		[DisplayName("BusinessEntityContact")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntityContact BusinessEntityContact { get; set; }

		/// <summary>
		/// Customer - PersonID
		/// </summary>
		[DisplayName("Customer")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Customer> Customer { get; set; }

		/// <summary>
		/// EmailAddress - BusinessEntityID
		/// </summary>
		[DisplayName("EmailAddress")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual EmailAddress EmailAddress { get; set; }

		/// <summary>
		/// Employee - BusinessEntityID
		/// </summary>
		[DisplayName("Employee")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Employee Employee { get; set; }

		/// <summary>
		/// Password - BusinessEntityID
		/// </summary>
		[DisplayName("Password")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Password Password { get; set; }

		/// <summary>
		/// BusinessEntity - BusinessEntityID
		/// </summary>
		[DisplayName("BusinessEntity")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual BusinessEntity BusinessEntity { get; set; }

		/// <summary>
		/// PersonCreditCard - BusinessEntityID
		/// </summary>
		[DisplayName("PersonCreditCard")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PersonCreditCard PersonCreditCard { get; set; }

		/// <summary>
		/// PersonPhone - BusinessEntityID
		/// </summary>
		[DisplayName("PersonPhone")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PersonPhone PersonPhone { get; set; }

	}

	[DataContract]
	public partial class SalesReason : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesReasonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ReasonType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesOrderHeaderSalesReason - SalesReasonID
		/// </summary>
		[DisplayName("SalesOrderHeaderSalesReason")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesOrderHeaderSalesReason SalesOrderHeaderSalesReason { get; set; }

	}

	[DataContract]
	public partial class SalesTaxRate : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 SalesTaxRateID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 StateProvinceID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Byte TaxType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal TaxRate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// StateProvince - StateProvinceID
		/// </summary>
		[DisplayName("StateProvince")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual StateProvince StateProvince { get; set; }

	}

	[DataContract]
	public partial class PersonCreditCard : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 CreditCardID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// CreditCard - CreditCardID
		/// </summary>
		[DisplayName("CreditCard")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CreditCard CreditCard { get; set; }

	}

	[DataContract]
	public partial class PersonPhone : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PhoneNumberTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Person - BusinessEntityID
		/// </summary>
		[DisplayName("Person")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual Person Person { get; set; }

		/// <summary>
		/// PhoneNumberType - PhoneNumberTypeID
		/// </summary>
		[DisplayName("PhoneNumberType")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PhoneNumberType PhoneNumberType { get; set; }

	}

	[DataContract]
	public partial class SalesTerritory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Group { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesYTD { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesLastYear { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal CostYTD { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal CostLastYear { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// Customer - TerritoryID
		/// </summary>
		[DisplayName("Customer")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<Customer> Customer { get; set; }

		/// <summary>
		/// SalesOrderHeader - TerritoryID
		/// </summary>
		[DisplayName("SalesOrderHeader")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesOrderHeader> SalesOrderHeader { get; set; }

		/// <summary>
		/// SalesPerson - TerritoryID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<SalesPerson> SalesPerson { get; set; }

		/// <summary>
		/// CountryRegion - CountryRegionCode
		/// </summary>
		[DisplayName("CountryRegion")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual CountryRegion CountryRegion { get; set; }

		/// <summary>
		/// SalesTerritoryHistory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritoryHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritoryHistory SalesTerritoryHistory { get; set; }

		/// <summary>
		/// StateProvince - TerritoryID
		/// </summary>
		[DisplayName("StateProvince")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<StateProvince> StateProvince { get; set; }

	}

	[DataContract]
	public partial class PhoneNumberType : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 PhoneNumberTypeID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// PersonPhone - PhoneNumberTypeID
		/// </summary>
		[DisplayName("PersonPhone")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual PersonPhone PersonPhone { get; set; }

	}

	[DataContract]
	public partial class Product : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean MakeFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean FinishedGoodsFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Color { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 SafetyStockLevel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int16 ReorderPoint { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal StandardCost { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal ListPrice { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Size { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String SizeUnitMeasureCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String WeightUnitMeasureCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? Weight { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 DaysToManufacture { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductLine { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Class { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Style { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? ProductSubcategoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime SellStartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? SellEndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? DiscontinuedDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// BillOfMaterials - ProductAssemblyID
		/// </summary>
		[DisplayName("BillOfMaterials")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<BillOfMaterials> BillOfMaterials { get; set; }

		/// <summary>
		/// BillOfMaterials - ComponentID
		/// </summary>
		[DisplayName("BillOfMaterials")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<BillOfMaterials> BillOfMaterials_ { get; set; }

		/// <summary>
		/// UnitMeasure - SizeUnitMeasureCode
		/// </summary>
		[DisplayName("UnitMeasure")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual UnitMeasure UnitMeasure { get; set; }

		/// <summary>
		/// UnitMeasure - WeightUnitMeasureCode
		/// </summary>
		[DisplayName("UnitMeasure")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual UnitMeasure UnitMeasure_ { get; set; }

		/// <summary>
		/// ProductSubcategory - ProductSubcategoryID
		/// </summary>
		[DisplayName("ProductSubcategory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductSubcategory ProductSubcategory { get; set; }

		/// <summary>
		/// ProductModel - ProductModelID
		/// </summary>
		[DisplayName("ProductModel")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductModel ProductModel { get; set; }

		/// <summary>
		/// ProductCostHistory - ProductID
		/// </summary>
		[DisplayName("ProductCostHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductCostHistory ProductCostHistory { get; set; }

		/// <summary>
		/// ProductDocument - ProductID
		/// </summary>
		[DisplayName("ProductDocument")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductDocument ProductDocument { get; set; }

		/// <summary>
		/// ProductInventory - ProductID
		/// </summary>
		[DisplayName("ProductInventory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductInventory ProductInventory { get; set; }

		/// <summary>
		/// ProductListPriceHistory - ProductID
		/// </summary>
		[DisplayName("ProductListPriceHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductListPriceHistory ProductListPriceHistory { get; set; }

		/// <summary>
		/// ProductProductPhoto - ProductID
		/// </summary>
		[DisplayName("ProductProductPhoto")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductProductPhoto ProductProductPhoto { get; set; }

		/// <summary>
		/// ProductReview - ProductID
		/// </summary>
		[DisplayName("ProductReview")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<ProductReview> ProductReview { get; set; }

		/// <summary>
		/// ProductVendor - ProductID
		/// </summary>
		[DisplayName("ProductVendor")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual ProductVendor ProductVendor { get; set; }

		/// <summary>
		/// PurchaseOrderDetail - ProductID
		/// </summary>
		[DisplayName("PurchaseOrderDetail")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }

		/// <summary>
		/// ShoppingCartItem - ProductID
		/// </summary>
		[DisplayName("ShoppingCartItem")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<ShoppingCartItem> ShoppingCartItem { get; set; }

		/// <summary>
		/// SpecialOfferProduct - ProductID
		/// </summary>
		[DisplayName("SpecialOfferProduct")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SpecialOfferProduct SpecialOfferProduct { get; set; }

		/// <summary>
		/// TransactionHistory - ProductID
		/// </summary>
		[DisplayName("TransactionHistory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<TransactionHistory> TransactionHistory { get; set; }

		/// <summary>
		/// WorkOrder - ProductID
		/// </summary>
		[DisplayName("WorkOrder")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual IList<WorkOrder> WorkOrder { get; set; }

	}

	[DataContract]
	public partial class SalesTerritoryHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


		/// <summary>
		/// SalesPerson - BusinessEntityID
		/// </summary>
		[DisplayName("SalesPerson")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesPerson SalesPerson { get; set; }

		/// <summary>
		/// SalesTerritory - TerritoryID
		/// </summary>
		[DisplayName("SalesTerritory")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		public virtual SalesTerritory SalesTerritory { get; set; }

	}

	[DataContract]
	public partial class vwvEmployee : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String JobTitle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumberType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AdditionalContactInfo { get; set; }


	}

	[DataContract]
	public partial class vwvEmployeeDepartment : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String JobTitle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Department { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String GroupName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }


	}

	[DataContract]
	public partial class vwvEmployeeDepartmentHistory : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Shift { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Department { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String GroupName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime StartDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? EndDate { get; set; }


	}

	[DataContract]
	public partial class vwvJobCandidate : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 JobCandidateID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Skills { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EMail { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String WebSite { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class vwvJobCandidateEducation : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 JobCandidateID { get; set; }


	}

	[DataContract]
	public partial class vwvJobCandidateEmployment : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 JobCandidateID { get; set; }


	}

	[DataContract]
	public partial class vwvAdditionalContactInfo : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TelephoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TelephoneSpecialInstructions { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Street { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvince { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String HomeAddressSpecialInstructions { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EMailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EMailSpecialInstructions { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EMailTelephoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class vwvStateProvinceCountryRegion : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 StateProvinceID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean IsOnlyStateProvinceFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 TerritoryID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }


	}

	[DataContract]
	public partial class vwvProductAndDescription : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductModel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CultureID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Description { get; set; }


	}

	[DataContract]
	public partial class vwvProductModelCatalogDescription : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Summary { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Manufacturer { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Copyright { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductURL { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String WarrantyPeriod { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String WarrantyDescription { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String NoOfYears { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MaintenanceDescription { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Wheel { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Saddle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Pedal { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String BikeFrame { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Crankset { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PictureAngle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PictureSize { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductPhotoID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Material { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Color { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ProductLine { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Style { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String RiderExperience { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class vwvProductModelInstructions : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 ProductModelID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Instructions { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? LocationID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? SetupHours { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? MachineHours { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? LaborHours { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? LotSize { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Step { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual System.Guid rowguid { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime ModifiedDate { get; set; }


	}

	[DataContract]
	public partial class vwvVendorWithAddresses : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }


	}

	[DataContract]
	public partial class vwvVendorWithContacts : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ContactType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumberType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }


	}

	[DataContract]
	public partial class vwvIndividualCustomer : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumberType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Demographics { get; set; }


	}

	[DataContract]
	public partial class vwvPersonDemographics : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? TotalPurchaseYTD { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? DateFirstPurchase { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual DateTime? BirthDate { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MaritalStatus { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String YearlyIncome { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Gender { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? TotalChildren { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? NumberChildrenAtHome { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Education { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Occupation { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Boolean? HomeOwnerFlag { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? NumberCarsOwned { get; set; }


	}

	[DataContract]
	public partial class vwvSalesPerson : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String JobTitle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumberType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TerritoryName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String TerritoryGroup { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? SalesQuota { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesYTD { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal SalesLastYear { get; set; }


	}

	[DataContract]
	public partial class vwvSalesPersonSalesByFiscalYears : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? SalesPersonID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FullName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String JobTitle { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String SalesTerritory { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? _2002 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? _2003 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? _2004 { get; set; }


	}

	[DataContract]
	public partial class vwvStoreWithAddresses : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine1 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String AddressLine2 { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String City { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String StateProvinceName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PostalCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String CountryRegionName { get; set; }


	}

	[DataContract]
	public partial class vwvStoreWithContacts : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String ContactType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Title { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String FirstName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String MiddleName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String LastName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Suffix { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumber { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String PhoneNumberType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String EmailAddress { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 EmailPromotion { get; set; }


	}

	[DataContract]
	public partial class vwvStoreWithDemographics : AdventureWorks2012BaseEntity
	{

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32 BusinessEntityID { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Name { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? AnnualSales { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Decimal? AnnualRevenue { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String BankName { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String BusinessType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? YearOpened { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Specialty { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? SquareFeet { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Brands { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual String Internet { get; set; }

		/// <summary>
		/// 
		/// </summary>
		[DisplayName("")]
		[DataMember]
		public virtual Int32? NumberEmployees { get; set; }


	}

}
