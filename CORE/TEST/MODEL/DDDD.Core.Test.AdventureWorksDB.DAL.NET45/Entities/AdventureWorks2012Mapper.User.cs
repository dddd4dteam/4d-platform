using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using NHibernate.Mapping.ByCode;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.Entities
{
	/// <summary>
	/// This partial class won't be toched by the generator.
	/// </summary>
	static partial class AdventureWorks2012Mapper
	{
		internal static void CustomMapToModel(ModelMapper mapper)
		{
			// put your custom mappings here
		}
	}
}
