using System;
using System.Configuration;
using System.Data;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHCfg  = NHibernate.Cfg;
using DDDD.Core.Test.DAL.AdventureWorks2012.Base;
using DDDD.Core.Test.DAL.AdventureWorks2012.Entities;

namespace DDDD.Core.Test.DAL.AdventureWorks2012.Entities
{
	/// <summary>
	/// AdventureWorks2012 NHibernate helper methods. This is suitable for windows-apps.
	/// </summary>
	public static class AdventureWorks2012Session
	{
		private static ISessionFactory _sessionFactory;

		private static ISessionFactory SessionFactory
		{
			get
			{
				if (_sessionFactory == null)
				{
					var configuration = GetConfig();
					var mapping = GetMappings();
					configuration.AddDeserializedMapping(mapping, "NHSchema_AdventureWorks2012");
					_sessionFactory = configuration.BuildSessionFactory();
				}
				return _sessionFactory;
			}
		}

		private static HbmMapping GetMappings()
		{
			var mapper = new ModelMapper();
			AdventureWorks2012Mapper.MapToModel(mapper);
			AdventureWorks2012Mapper.CustomMapToModel(mapper);

			var mapping = mapper.CompileMappingFor(
				new[]
					{
						typeof(ScrapReason),
						typeof(Shift),
						typeof(ProductCategory),
						typeof(ShipMethod),
						typeof(ProductCostHistory),
						typeof(ProductDescription),
						typeof(ShoppingCartItem),
						typeof(ProductDocument),
						typeof(DatabaseLog),
						typeof(ProductInventory),
						typeof(SpecialOffer),
						typeof(ErrorLog),
						typeof(ProductListPriceHistory),
						typeof(Address),
						typeof(SpecialOfferProduct),
						typeof(ProductModel),
						typeof(AddressType),
						typeof(StateProvince),
						typeof(ProductModelIllustration),
						typeof(AWBuildVersion),
						typeof(ProductModelProductDescriptionCulture),
						typeof(BillOfMaterials),
						typeof(Store),
						typeof(ProductPhoto),
						typeof(ProductProductPhoto),
						typeof(TransactionHistory),
						typeof(ProductReview),
						typeof(BusinessEntity),
						typeof(TransactionHistoryArchive),
						typeof(ProductSubcategory),
						typeof(BusinessEntityAddress),
						typeof(ProductVendor),
						typeof(BusinessEntityContact),
						typeof(UnitMeasure),
						typeof(Vendor),
						typeof(ContactType),
						typeof(CountryRegionCurrency),
						typeof(CountryRegion),
						typeof(WorkOrder),
						typeof(PurchaseOrderDetail),
						typeof(CreditCard),
						typeof(Culture),
						typeof(WorkOrderRouting),
						typeof(Currency),
						typeof(PurchaseOrderHeader),
						typeof(CurrencyRate),
						typeof(Customer),
						typeof(Department),
						typeof(Document),
						typeof(SalesOrderDetail),
						typeof(EmailAddress),
						typeof(Employee),
						typeof(SalesOrderHeader),
						typeof(EmployeeDepartmentHistory),
						typeof(EmployeePayHistory),
						typeof(SalesOrderHeaderSalesReason),
						typeof(SalesPerson),
						typeof(Illustration),
						typeof(JobCandidate),
						typeof(Location),
						typeof(Password),
						typeof(SalesPersonQuotaHistory),
						typeof(Person),
						typeof(SalesReason),
						typeof(SalesTaxRate),
						typeof(PersonCreditCard),
						typeof(PersonPhone),
						typeof(SalesTerritory),
						typeof(PhoneNumberType),
						typeof(Product),
						typeof(SalesTerritoryHistory),
						typeof(vwvEmployee),
						typeof(vwvEmployeeDepartment),
						typeof(vwvEmployeeDepartmentHistory),
						typeof(vwvJobCandidate),
						typeof(vwvJobCandidateEducation),
						typeof(vwvJobCandidateEmployment),
						typeof(vwvAdditionalContactInfo),
						typeof(vwvStateProvinceCountryRegion),
						typeof(vwvProductAndDescription),
						typeof(vwvProductModelCatalogDescription),
						typeof(vwvProductModelInstructions),
						typeof(vwvVendorWithAddresses),
						typeof(vwvVendorWithContacts),
						typeof(vwvIndividualCustomer),
						typeof(vwvPersonDemographics),
						typeof(vwvSalesPerson),
						typeof(vwvSalesPersonSalesByFiscalYears),
						typeof(vwvStoreWithAddresses),
						typeof(vwvStoreWithContacts),
						typeof(vwvStoreWithDemographics)
					});
			//Output XML mappings
			//Console.WriteLine(mapping.AsString());
			return mapping;
		}

		private static NHCfg.Configuration GetConfig()
		{
			var configure = new NHCfg.Configuration();
			configure.SessionFactoryName("SessionFactory_AdventureWorks2012");

			configure.DataBaseIntegration(
				db =>
					{
						db.ConnectionProvider<DriverConnectionProvider>();
						db.Dialect<MsSql2008Dialect>();
						db.Driver<Sql2008ClientDriver>();
						db.KeywordsAutoImport = Hbm2DDLKeyWords.AutoQuote;
						db.IsolationLevel = IsolationLevel.ReadCommitted;
						db.ConnectionString = ConfigurationManager.ConnectionStrings["AdventureWorks2012ConnectionString"].ConnectionString;
						db.Timeout = 15;

						////for testing ...
						//db.LogFormattedSql = true;
						//db.LogSqlInConsole = true;
					});
			return configure;
		}

		/// <summary>
		/// Create a database connection and open a ISession on it
		/// </summary>
		public static ISession OpenSession()
		{
			return SessionFactory.OpenSession();
		}

		/// <summary>
		/// Create a database connection and open a ISession on it
		/// </summary>
		public static ISession OpenSession(FlushMode flushMode)
		{
			var session = SessionFactory.OpenSession();
			session.FlushMode = flushMode;
			return session;
		}

		/// <summary>
		/// Open a ISession on the given connection
		/// </summary>
		public static ISession OpenSession(IDbConnection conn)
		{
			return SessionFactory.OpenSession(conn);
		}

		/// <summary>
		/// Open a ISession on the given connection
		/// </summary>
		public static ISession OpenSession(IDbConnection conn, FlushMode flushMode)
		{
			var session = SessionFactory.OpenSession(conn);
			session.FlushMode = flushMode;
			return session;
		}

		/// <summary>
		/// Get a new stateless session.
		/// </summary>
		public static IStatelessSession OpenStatelessSession()
		{
			return SessionFactory.OpenStatelessSession();
		}

		/// <summary>
		/// Get a new stateless session for the given ADO.NET connection.
		/// </summary>
		public static IStatelessSession OpenStatelessSession(IDbConnection conn)
		{
			return SessionFactory.OpenStatelessSession(conn);
		}
	}
}
