﻿
 
 

 

/// <reference path="Enums.ts" />

declare module DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging {
	interface CommandMessage {
		Body: number[];
		Command: string;
		Controller: string;
		ErrorCode: number;
		HasOperationSuccessfullyCompleted: boolean;
		HasServerErrorHappened: boolean;
		ID: System.Guid;
		Parameters: System.Collections.Generic.KeyValuePair<string, any>[];
		ProcessingFaultMessage: string;
		ProcessingSuccessMessage: string;
		Result: any;
		ResultJsonTypeName: string;
		ServiceKey: string;
		ServiceNamespace: string;
		TargetCommandManager: string;
		TargetCommandManagerID: number;
	}
	interface DCMessage extends DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging.CommandMessage {
		ClientRecievedResultFromServerTime: Date;
		ClientSendTime: Date;
		Empty: DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging.DCMessage;
		OperationProgressPercent: number;
		OperationProgressState: string;
		ServerReceivedFromClientTime: Date;
		ServerSendToClientTime: Date;
		UseOperationProgress: boolean;
	}
}
declare module System {
	interface Guid {
	}
}
declare module System.Collections.Generic {
	interface KeyValuePair<TKey, TValue> {
		Key: TKey;
		Value: TValue;
	}
}


