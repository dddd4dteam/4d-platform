﻿using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using TypeLite;


namespace DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging
{
    /// <summary>
    /// CommandMessage - Message  to target CommandManeger  about to do somethig based on [CommandName + Parameters], then store result again inside that CommandMessage, 
    /// or safe fault  error, and after all return the same Message back to client
    /// CommandMessage is also the DataContract, with only one property -Body.
    /// </summary>
    [DataContract] // DataContract is only stub for default serializer behavior
    [TsClass]    
    public class CommandMessage
    {


        #region ---------------------------- CTOR ---------------------------------

        public CommandMessage()
        {

            ProcessingSuccessMessage = string.Empty;

            ID = Guid.NewGuid();
            Parameters = new Dictionary<string, object>();

        }

        #endregion ---------------------------- CTOR ---------------------------------



        #region -------------------------- IDENTIFICATION ------------------------------

        /// <summary>
        /// Command  ModelId
        /// </summary>
        public Guid ID { get; private set; }

        #endregion-------------------------- IDENTIFICATION ------------------------------



        #region ------------------------ COMMUNICATION  TRANSMISSION INFO---------------------

        /// <summary>
        /// Web API/MVC  Controller Name. 
        ///<para/> If we sending command to WCF service - then we should set ServiceKey. If we sending Command to   WebAPI/MVC controller we should set  this property- Controller.
        /// </summary>
        public string Controller { get; protected internal set; }


        /// <summary>
        /// WCF DynamicService  Key
        ///<para/>  If we sending Command to   WebAPI/MVC controller we should set property Controller. If we sending command to WCF service, then we should set this property -ServiceKey.
        /// </summary>
        public string ServiceKey { get; protected internal set; }


        /// <summary>
        /// To Save Source SvcService Namespace. 
        /// </summary>
        public string ServiceNamespace { get; protected internal set; }


        #endregion ------------------------ COMMUNICATION TRANSMISSION INFO---------------------



        #region ----------------------TARGET COMMAND RECEIVER-COMMAND MANAGER --------------------------

        /// <summary>
        /// Targeting BL or infrastructure operations( like CRUDManager) responsible Command Manager's [Key-TargetCommandManager]. 
        /// Where [ TargetCommandManager = [Type.FullName] = Key ].
        /// </summary>
        ///[IgnoreMember]
        [DataMember]
        public String TargetCommandManager { get; set; }



        /// <summary>
        /// Targeting BL or infrastructure operations( like CRUDManager) responsible Command Manager's [ID-TargetCommandManagerID]. 
        /// Where [ TargetCommandManagerID = [Type.FullName.GethashCode] = ID_hash ].
        /// </summary>
        [DataMember]
        public int TargetCommandManagerID { get; set; }

        /// <summary>
        /// CommandName  to CommandController
        /// </summary>
        ///[IgnoreMember]
        //[DataMember]
        public String Command { get; set; }


        /// <summary>
        /// Flexible TSS can serialize complex parameter classes/structs and then we box them into the Dictionary, each by  its Key 
        /// </summary>
        public Dictionary<string, object> Parameters { get; set; }


        #endregion ----------------------TARGETCOMMAND RECEIVER-COMMAND MANAGER --------------------------



        #region -------------------------- RESULT  &  ERROR INFO ----------------------------


        /// <summary>
        /// This property will contains packed DCMessage by secondary Serializer(like TSS or some of ServiceStack's serialiers) from client to server. 
        /// </summary>
        [IgnoreMember]
        [DataMember]
        [XmlIgnore]
        public byte[] Body { get; set; }


        /// <summary>
        ///Result can be almost of any Type. Constrains Here is Serialization mode and serializer you'll use here.
        /// Flexible TSS serializer could deserialize complex objects from single object
        /// </summary>
        public object Result { get; set; }

        /// <summary>
        /// ResultJsonTypeName - helps to determine instance Type stored in Result property. 
        /// </summary>
        public string ResultJsonTypeName { get; set; }


        /// <summary>
        /// Message on successful result (if you wish)
        /// </summary>
        public string ProcessingSuccessMessage { get; set; }


        /// <summary>
        /// Message on operation Fault
        /// </summary>
        public string ProcessingFaultMessage { get; set; }


        /// <summary>
        /// Internal System Error Code  
        /// </summary>
        public short ErrorCode { get; set; }



        /// <summary>
        /// Server( Command Controller)  should set this message if error happened  before send CommandMessage back.
        /// After such CommandMessage will be received by client we'll show full error message only in [Debug] Conditional  mode.
        /// In release mode client will see only common message - like [SERVER ERROR HAPPENED] 
        /// </summary>
        public bool HasServerErrorHappened
        {
            get
            {
                return !ProcessingFaultMessage.IsNullOrEmpty();
            }
        }

        /// <summary>
        /// By default Successfull Message is empty string, that means that Command is succsessful  - until server won't set an ErrorMessage.
        /// So if we have Empty String or valid Succesfull message -  command will be means as successful.  
        /// </summary>
        public bool HasOperationSuccessfullyCompleted
        {
            get
            {
                return !ProcessingSuccessMessage.IsNullOrEmpty();
            }
        }

        #endregion -------------------------- RESULT  &  ERROR INFO ----------------------------


    }

}
