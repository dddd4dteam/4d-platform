﻿using System;
using DDDD.Core.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using TypeLite;


namespace DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging
{
    #region --------------------------------------- CUSTOM MESSAGE CONTRACT --------------------------------------

    /// <summary>
    /// Dynamic Command Message. It's also the example of custom CommandMessage.
    /// </summary>
    [DataContract]
    [TsClass]    
    public class DCMessage : CommandMessage
    {

        #region ------------------------------ CTOR ---------------------------------

#if SL5 || WP81Sl

        public DCMessage()
            : base()
        {

        }

#elif WPF

        protected DCMessage()
            : base()
        {

        }

#endif
        #endregion ------------------------------ CTOR ---------------------------------


        public static DCMessage Empty
        {
            get { return default(DCMessage); }
        }


        #region -------------------------------- TIME DIAGNOSTICS -------------------------------

        /// <summary>
        /// Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientSendTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        public DateTime ServerReceivedFromClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the server sending processed command to client( server time )
        /// </summary>
        public DateTime ServerSendToClientTime
        {
            get;
            private set;
        }


        /// <summary>
        /// Time point - when the client received processed command from server( client time synchronized with server )  
        /// </summary>
        public DateTime ClientRecievedResultFromServerTime
        {
            get;
            private set;
        }

        #endregion --------------------------------TIME DIAGNOSTICS -------------------------------


        #region -------------------------------- OPERATION PROGRESS --------------------------------

        /// <summary>
        /// Operation Progress in Percents
        /// </summary>
        public int OperationProgressPercent
        {
            get;
            private set;
        }


        /// <summary>
        /// Operation Progress State -string value
        /// </summary>
        public string OperationProgressState
        {
            get;
            private set;
        }

        /// <summary>
        /// If we use Operation Progress info properties - OperationProgressPercent and OperationProgressState
        /// </summary>
        public bool UseOperationProgress
        {
            get
            {
                return (OperationProgressPercent >= 0 || OperationProgressState.IsNotNullOrEmpty());
            }
        }
        #endregion -------------------------------- OPERATION PROGRESS --------------------------------




        /// <summary>
        /// Create new Command with parameters - in case when we use WebAPI/MVC controller
        /// </summary>
        /// <param name="controller"></param>
        /// <param name="targetCommandManager"></param>
        /// <param name="command"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static DCMessage Create(String controller, string targetCommandManager, String command, params KeyValuePair<String, object>[] commandParameters)
        {
            return new DCMessage()
            {
                Controller = controller,
                TargetCommandManager = targetCommandManager,
                Command = command,
                Parameters = commandParameters.ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
            };

        }


        /// <summary>
        /// /// Create new Command with parameters  - in case when we use WCF Services controller
        /// </summary>
        /// <param name="serviceKey"></param>
        /// <param name="serviceNamespace"></param>
        /// <param name="targetCommandManager"></param>
        /// <param name="command"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static DCMessage Create(String serviceKey, String serviceNamespace, String targetCommandManager, String command, params KeyValuePair<String, object>[] commandParameters)
        {
            return new DCMessage()
            {
                ServiceKey = serviceKey,
                ServiceNamespace = serviceNamespace,
                TargetCommandManager = targetCommandManager,
                Command = command,
                Parameters = commandParameters.ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
            };
        }


        /// <summary>
        /// Create new Command  with parameters
        /// </summary>
        /// <param name="targetCommandManager"></param>
        /// <param name="command"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        public static DCMessage Create(String targetCommandManager, int targetCommandManagerID, String command, params KeyValuePair<String, object>[] commandParameters)
        {
            return new DCMessage()
            {
                TargetCommandManager = targetCommandManager
                ,
                TargetCommandManagerID = targetCommandManagerID
                ,
                Command = command
                ,
                Parameters = commandParameters.ToDictionary(kvp => kvp.Key, kvp => kvp.Value)
            };
        }



        /// <summary>
        ///  Set  ServiceKey when using  WCF Service contract. 
        /// </summary>
        /// <param name="serviceKey"></param>
        public void SetServiceKey(String serviceKey)
        {
            ServiceKey = serviceKey;
        }


        /// <summary>
        ///  Set  ServiceNamespace when using  WCF Service contract. 
        /// </summary>
        /// <param name="serviceNamespace"></param>
        public void SetServiceNamespace(String serviceNamespace)
        {
            ServiceNamespace = serviceNamespace;
        }


        /// <summary>
        /// Set  ClientSendTime when - Time point - when the client sending command to server( client time synchronized with server )  
        /// </summary>
        /// <param name="clientSendTime"></param>
        public void SetClientSendTime(DateTime clientSendTime)
        {
            ClientSendTime = clientSendTime;
        }

        /// <summary>
        ///Set  ServerReceivedFromClientTime when - Time point - when the server receiving  command, before processing it( server time ) 
        /// </summary>
        /// <param name="serverReceivedFromClientTime"></param>
        public void SetServerReceivedFromClientTime(DateTime serverReceivedFromClientTime)
        {
            ServerReceivedFromClientTime = serverReceivedFromClientTime;
        }

        /// <summary>
        /// Set  ServerSendToClientTime when - Time point - when the server sending processed command to client( server time )
        /// </summary>
        /// <param name="serverSendToClientTime"></param>
        public void SetServerSendToClientTime(DateTime serverSendToClientTime)
        {
            ServerSendToClientTime = serverSendToClientTime;
        }

        /// <summary>
        /// Set  ClientRecievedResultFromServerTime when - Time point - when the client received processed command from server( client time synchronized with server ) 
        /// </summary>
        /// <param name="clientRecievedResultFromServerTime"></param>
        public void SetClientRecievedResultFromServerTime(DateTime clientRecievedResultFromServerTime)
        {
            ClientRecievedResultFromServerTime = clientRecievedResultFromServerTime;
        }


    }


    #endregion --------------------------------------- CUSTOM MESSAGE CONTRACT --------------------------------------

}
