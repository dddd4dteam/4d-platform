﻿class Greeter
{
    element: HTMLElement;
    span: HTMLElement;
    timerToken: number;

    constructor(element: HTMLElement) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }

    start() {
        this.timerToken = setInterval(() => this.span.innerHTML = new Date().toUTCString(), 500);
    }

    stop() {
        clearTimeout(this.timerToken);
    }

    static  dateReviver(key, value) {
    var a;
    if (typeof value === 'string') {
        a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
        if (a) {
            return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                +a[5], +a[6]));
        }
    }
    return value;
};

     static ReviveDateTime(key: any, value: any): any {
        if (typeof value === 'string') {
            let a = /\/Date\((\d*)\)\//.exec(value);
            if (a) {
                return new Date(+a[1]);
            }
        }

        return value;
    }
    
 static  JsonTest() {

    var dateTimeVal = new Date().toUTCString();//(1983, 12, 4, 15, 33, 55, 777); 
    console.log('DateTime : ' + dateTimeVal);
    console.log('DateTime now: ' + dateTimeVal.toString()); //DateTime now: [1485461020284] 
    var dateTimeValJson = JSON.stringify(dateTimeVal);
    console.log('DateTime Json : ' + dateTimeValJson); //DateTime Json:  


    var dateJson2 = '\"9999-12-31T20:59:59.999Z\"';
    var dateTimeParsed = JSON.parse(dateJson2, Greeter.dateReviver);// ReviveDateTime);
     

}




}





window.onload = () => {
    var el = document.getElementById('content');
    
    var greeter = new Greeter(el);
    greeter.start();

    var el_btn_TestJson = document.getElementById('btn_TestJson');
    el_btn_TestJson.onclick = Greeter.JsonTest;


};