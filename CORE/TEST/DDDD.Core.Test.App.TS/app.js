var Greeter = (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    Greeter.dateReviver = function (key, value) {
        var a;
        if (typeof value === 'string') {
            a = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
            if (a) {
                return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4], +a[5], +a[6]));
            }
        }
        return value;
    };
    ;
    Greeter.ReviveDateTime = function (key, value) {
        if (typeof value === 'string') {
            var a = /\/Date\((\d*)\)\//.exec(value);
            if (a) {
                return new Date(+a[1]);
            }
        }
        return value;
    };
    Greeter.JsonTest = function () {
        var dateTimeVal = new Date().toUTCString(); //(1983, 12, 4, 15, 33, 55, 777); 
        console.log('DateTime : ' + dateTimeVal);
        console.log('DateTime now: ' + dateTimeVal.toString()); //DateTime now: [1485461020284] 
        var dateTimeValJson = JSON.stringify(dateTimeVal);
        console.log('DateTime Json : ' + dateTimeValJson); //DateTime Json:  
        var dateJson2 = '\"9999-12-31T20:59:59.999Z\"';
        var dateTimeParsed = JSON.parse(dateJson2, Greeter.dateReviver); // ReviveDateTime);
    };
    return Greeter;
}());
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
    var el_btn_TestJson = document.getElementById('btn_TestJson');
    el_btn_TestJson.onclick = Greeter.JsonTest;
};
//# sourceMappingURL=app.js.map