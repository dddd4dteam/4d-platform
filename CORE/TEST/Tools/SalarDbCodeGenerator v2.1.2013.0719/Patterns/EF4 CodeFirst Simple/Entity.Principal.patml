﻿<?xml version="1.0" encoding="utf-8" ?>
<PatternFile>
	<Name>Database Entity Objects</Name>
	<Description>Entity objects pattern</Description>
	<Options
		Group="Database Model"
		AppliesTo="TablesAndViewsAll"
		Overwrite="true"
		FilePath="Entities\Principal\EntityObjects.cs"
		Language="C#"
		/>
	<BaseContent>
		<![CDATA[using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using [:Namespace:].Base;

namespace [:Namespace:].Entities
{
[#TablesAndViewsContent#]
}
]]>
	</BaseContent>

	<PatternContent Name="TablesAndViewsContent" ConditionKeyMode="TablesAndViewsAll">
		<BaseContent>[:InnerContent:]</BaseContent>
		<Condition Key="TheReplacement">
			<![CDATA[
	[DataContract]
	[Table("[:TableNameDb:]")]
	public partial class [:TableName:] : [:DatabaseName:]BaseEntity
	{
[#FieldsProperties#]
[#ForeignKeys#]
[#IndexConstraintKeys#]
[#UniqueConstraintKeys#]
	}
]]>
		</Condition>

		<ConditionContents>
			<PatternContent Name="ForeignKeys" ConditionKeyMode="FieldsForeignKeyAll">
				<Condition Key="MultiplicityOne">
					<![CDATA[
		/// <summary>
		/// [:ForeignTableNameDb:] - [:LocalFieldNameDb:]
		/// </summary>
		[Display(Name = "[:ForeignTableNameDb:]")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		[ForeignKey("[:LocalFieldName:]")]
		public virtual [:ForeignTableName:] [:ForeignTableNameAsField:] { get; set; }
]]>
				</Condition>
				<Condition Key="MultiplicityMany">
					<![CDATA[
		/// <summary>
		/// [:ForeignTableNameDb:] - [:ForeignFieldNameDb:]
		/// </summary>
		[Display(Name = "[:ForeignTableNameDb:]")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		[ForeignKey("[:ForeignFieldName:]")]
		public virtual IList<[:ForeignTableName:]> [:ForeignTableNameAsField:] { get; set; }
]]>
				</Condition>
				<Condition Key="MultiplicityOneToOnePrimary">
					<![CDATA[
		/// <summary>
		/// [:ForeignTableNameDb:] - [:LocalFieldNameDb:]
		/// </summary>
		[Display(Name = "[:ForeignTableNameDb:]")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		[ForeignKey("[:LocalFieldName:]")]
		public virtual [:ForeignTableName:] [:ForeignTableNameAsField:] { get; set; }
]]>
				</Condition>
				<Condition Key="MultiplicityOneToOneForeign">
					<![CDATA[
		/// <summary>
		/// [:ForeignTableNameDb:] - [:ForeignFieldNameDb:]
		/// </summary>
		[Display(Name = "[:ForeignTableNameDb:]")]
		[XmlIgnore]
		[SoapIgnore]
		[Browsable(false)]
		[ForeignKey("[:LocalFieldName:]")]
		public virtual [:ForeignTableName:] [:ForeignTableNameAsField:] { get; set; }
]]>
				</Condition>
			</PatternContent>

			<PatternContent Name="FieldsProperties" ConditionKeyMode="FieldsKeyTypeAll">
				<Condition Key="OneToOnePrimaryKey">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("[:FieldNameDb:]", Order = [:FieldOrdinalValue:])]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="OneToOneForeignKey">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Key, ForeignKey("[#OneToOneForeignKey_NavigationKey#]")]
		[Column("[:FieldNameDb:]", Order = [:FieldOrdinalValue:])]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="AutoInrcementPrimaryKey">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("[:FieldNameDb:]", Order = [:FieldOrdinalValue:])]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="AutoInrcement">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="AutoIncNativeNullable">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="AutoIncNullableType">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:]? [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="PrimaryKey">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Key]
		[Column("[:FieldNameDb:]", Order = [:FieldOrdinalValue:])]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="NormalField">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="NativeNullable">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:] [:FieldName:] { get; set; }
]]>
				</Condition>
				<Condition Key="NullableType">
					<![CDATA[
		/// <summary>
		/// [:FieldDescription:]
		/// </summary>
		[Display(Name = "[:FieldDescription:]")]
		[DataMember]
		[Column("[:FieldNameDb:]")]
		public [:FieldDotNetType:]? [:FieldName:] { get; set; }
]]>
				</Condition>
				<ConditionContents>
					<PatternContent Name="OneToOneForeignKey_NavigationKey" ConditionKeyMode="FieldsForeignKeyAll">
						<Condition Key="MultiplicityOne">
							<![CDATA[]]>
						</Condition>
						<Condition Key="MultiplicityMany">
							<![CDATA[]]>
						</Condition>
						<Condition Key="MultiplicityOneToOnePrimary">
							<![CDATA[]]>
						</Condition>
						<Condition Key="MultiplicityOneToOneForeign">
							<![CDATA[[:ForeignTableNameAsField:]]]>
						</Condition>
					</PatternContent>
				</ConditionContents>
			</PatternContent>

			<PatternContent Name="IndexConstraintKeys" ConditionKeyMode="TableIndexConstraint">
				<Condition Key="NormalKey"></Condition>
			</PatternContent>

			<PatternContent Name="UniqueConstraintKeys" ConditionKeyMode="TableUniqueConstraint">
				<Condition Key="NormalKey"></Condition>
			</PatternContent>
		</ConditionContents>
	</PatternContent>
</PatternFile>
