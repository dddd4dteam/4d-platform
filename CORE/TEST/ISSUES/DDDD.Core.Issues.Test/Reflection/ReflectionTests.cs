﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using System.Linq.Expressions;

namespace DDDD.Core.Issues.Test.Reflection
{
    /// <summary>
    /// Summary description for ReflectionTests
    /// </summary>
    [TestClass]
    public class ReflectionTests
    {
        public ReflectionTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void  CoreReflection_GetTypeInfoEx_Test()
        {
           
            try
            {

                var typeinfo = TypeInfoEx.Get(typeof(object));
                var shortTypeAQNamw = typeinfo.TpAQName.GetTypeAQNameShortVariant();//
            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void CoreReflection_TypeCache_Test()
        {
            try
            {
                var typeinfo = TypeCache.Current;
            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void CoreReflection_TypeActivator_PrecompileArrayTest()
        {
            try
            {
                var targetType = typeof(string[]);               

                var arrayDefaultCtor = targetType.GetConstructors().First();
                var paramtrs = arrayDefaultCtor.GetParameters();
                var firstPrm = paramtrs.FirstOrDefault();

                int arrayRank = arrayDefaultCtor.GetParameters().Count();//targetType.GetArrayRank(); 

                ParameterExpression param1 = Expression.Parameter(typeof(int), "param1");

                Expression<Func<int, string[]>> expr =
                    Expression.Lambda<Func<int, string[]>>(
                            Expression.New(arrayDefaultCtor,param1),
                            param1
                            );

                var arFunc = expr.Compile();

                var arrInstance = (string[])arFunc(5);

                arrInstance[0] = "One";
                arrInstance[1] = "Two";
                arrInstance[2] = "Three";
                arrInstance[3] = "Four";
                arrInstance[4] = "Five";


                var arrayFunc = TypeActivator.PrecompileAsNewArrayExpressionT<string[]>();
                var typeinfo = arrayFunc(new object[] {1} );

            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }




        }

}
