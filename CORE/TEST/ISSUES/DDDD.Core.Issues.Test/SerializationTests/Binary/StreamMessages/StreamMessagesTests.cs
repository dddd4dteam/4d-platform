﻿//using DDDD.Core.Test;

using System;

using DDDD.Core.Serialization;
using DDDD.Core.TestUtils;
//using DDDD.Core.Tests.Model.StreamMessagesModel;
using DDDD.Core.Serialization.Binary;
using DDDD.Core.Tests.Model.StreamMessagesModel;

#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;
#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.TestUtils;
#endif



namespace DDDD.Core.Tests.StreamMessagesTests
{
    [TestClass]
    public class StreamMessagesTests
    {

        static class TestConfig
        {
            internal static int U8Message_Iterations = 200000;
            internal static int S16Message_Iterations = 20000;
            internal static int S32Message_Iterations = 20000;
            internal static int S64Message_Iterations = 20000;
            internal static int PrimitivesMessage_Iterations = 10000;
            internal static int DictionaryMessage_Iterations = 5000;
            internal static int StringMessage_Iterations = 2000;
            internal static int StructMessage_Iterations = 10000;
            internal static int BoxedPrimitivesMessage_Iterations = 2000;
            internal static int ByteArrayMessage_Iterations = 3000;
            internal static int IntArrayMessage_Iterations = 100;
            internal static int CustomSerializersMessage_Iterations = 800;

        }
               
        
       



        //static bool s_runProtoBufTests = false;
        static bool s_quickRun = false;
                
        static void Warmup()
        {
            var msgs = new MessageBase[] { new S16Message(), new ComplexMessage(), new IntArrayMessage() };

            IMemStreamTest t;

            t = new MemStreamTest();
            t.Prepare(msgs.Length);
            t.Serialize(msgs);
            t.Deserialize();
           
        }



        static void RunTests(Type msgType, int numMessages)
        {
            if (s_quickRun)
                numMessages = 50;

            Console.WriteLine("== {0} {1} ==", numMessages, msgType.Name);            

            var msgs = MessageBase.CreateMessages(msgType, numMessages);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            MemStreamTest.Test(new MemStreamTest(), msgs);

#if NET45
            NetTest.Test(new NetTest(), msgs);       
      
#endif


        }
        
      
        
    

        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_U8Messages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<U8Message>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();
                BinarySerializer.AddKnownType<MyEnum>();


                Warmup();

                RunTests(typeof(U8Message),TestConfig.U8Message_Iterations );             

            }
            catch (Exception exc)
            {                
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_S16Messages()
        {
            try
            {
                // var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(S16Message), TestConfig.S16Message_Iterations);
               
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_S32Messages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<S32Message>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(S32Message), TestConfig.S32Message_Iterations);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_S64Messages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<S64Message>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(S64Message), TestConfig.S64Message_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_StructMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<StructMessage>();
                BinarySerializer.AddKnownType<MyStruct1>();
                BinarySerializer.AddKnownType<MyStruct2>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(StructMessage), TestConfig.StructMessage_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_StringMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<StringMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(StringMessage), TestConfig.StringMessage_Iterations);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_IntArrayMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<IntArrayMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                
                Warmup();

                RunTests(typeof(IntArrayMessage), TestConfig.IntArrayMessage_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }




        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_ByteArrayMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<ByteArrayMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();


                Warmup();

                RunTests(typeof(ByteArrayMessage), TestConfig.ByteArrayMessage_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_CustomSerializersMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<CustomSerializersMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                Warmup();

                RunTests(typeof(CustomSerializersMessage), TestConfig.CustomSerializersMessage_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }







        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_PrimitivesMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<PrimitivesMessage>();
                BinarySerializer.AddKnownType<MyStruct1>();
                BinarySerializer.AddKnownType<MyStruct2>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClass2>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<IntArrayMessage>();

                var newPrimitiveMessage = new PrimitivesMessage();
                var data = BinarySerializer.Serialize(newPrimitiveMessage);
                var deserlzdValue = BinarySerializer.Deserialize<PrimitivesMessage>(data);


                Warmup();

                RunTests(typeof(PrimitivesMessage),TestConfig.PrimitivesMessage_Iterations );

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


       



        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_BoxedPrimitivesMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<BoxedPrimitivesMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();
                BinarySerializer.AddKnownType<MyEnum>(); // unknown Enum- can't be determined in boxed field


                Warmup();

                RunTests(typeof(BoxedPrimitivesMessage), TestConfig.BoxedPrimitivesMessage_Iterations);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_StreamMessages_DictionaryMessages()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<DictionaryMessage>();
                BinarySerializer.AddKnownType<S16Message>();
                BinarySerializer.AddKnownType<ComplexMessage>();
                BinarySerializer.AddKnownType<SimpleClass>();
                BinarySerializer.AddKnownType<SimpleClass2>();
                BinarySerializer.AddKnownType<SimpleClassBase>();
                BinarySerializer.AddKnownType<IntArrayMessage>();


                Warmup();

                RunTests(typeof(DictionaryMessage), TestConfig.DictionaryMessage_Iterations);
            }
            catch (Exception)
            {
                throw;
            }
        }
        


    }
}
