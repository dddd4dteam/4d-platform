﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using DDDD.Core.Test;


using DDDD.Core.TestUtils;

using DDDD.Core.Serialization;
using DDDD.Core.Reflection;
using DDDD.TSS.Tests.Model.CommunicationModel;

using DDDD.Core.Tests.Model.StreamMessagesModel;
using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Serialization.Binary;



#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;


#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.Core.TestUtils;
#endif


namespace DDDD.Core.Tests.InterfaceTests
{
    [TestClass]
    public class InterfaceTests
    {


        [TestMethod]
        public void Test_4DCore_Serialization_ObjectInterface_BoxedPrimitivesOnlyMessage()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<BoxedPrimitivesMessage>();
                BinarySerializer.AddKnownType<MyEnum>(); // can't be unknown if we trying to box it


                var BoxedMessage = new BoxedPrimitivesMessage(new Random());


                var szdData = BinarySerializer.Serialize(BoxedMessage);
                var deserzdBoxedMessage = BinarySerializer.Deserialize<BoxedPrimitivesMessage>(szdData);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_ObjectInterface_ListWithBaseInterface()
        {
            try
            {

                IContractBase productToSerialize = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary"// Price=12500,
                };

                List<IContractBase> listofProducts = new List<IContractBase>();
                listofProducts.Add(productToSerialize);

                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                BinarySerializer.AddKnownType<Product>();
                BinarySerializer.AddKnownType<ProductMark>();


                byte[] serializedData = BinarySerializer.Serialize(listofProducts);

                List<IContractBase> deserializedList = BinarySerializer.Deserialize<List<IContractBase>>(serializedData);

            }
            catch (Exception exc)
            {
                throw exc;
            }


        }



    }
}
