﻿

using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;


namespace DDDD.Core.Issues.Test.Serialization.JSON
{
    [TestClass]
    public class PrimitivesTests
    {

        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_BuildingDebugProcessingCode()
        {

            //default types- primitives
            //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   
            
            //var serializer = JsonSerializer.MAX_DEPTH .SerializeToString() // TypeSetSerializer.AddOrUseExistTSS(0);

            //serializer.AddKnownType<>
            //serializer.BuildTypeProcessor ();

        }




        [TestMethod]
        public void Test_4DCoreJson_4DPrimitivesSerialization_PrimitivesTest()
        {
            try
            {
                //default types- primitives
                //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   


                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                //serializer.BuildTypeProcessor ();

                //bool
                var boolVariable = true;// serializer.DefaultValues.Add(typeof(bool), default(bool));
                var boolVariableSerialized = D4.JsonSerializer.SerializeToString(boolVariable);
                var boolVariableDeserialized = D4.JsonSerializer.DeserializeFromString<bool>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", boolVariable, boolVariableDeserialized);
                //Assert.That(boolVariable, NUnit.Framework.Is.Not.DeepEqualTo(boolVariableDeserialized));

                //byte
                var byteVariable = byte.MaxValue;// serializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = D4.JsonSerializer.SerializeToString(byteVariable);
                var byteVariableDeserialized = D4.JsonSerializer.DeserializeFromString<byte>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Byte", byteVariable, byteVariableDeserialized);


                //char
                var charVariable = char.MaxValue; //serializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = D4.JsonSerializer.SerializeToString(charVariable);
                var charVariableDeserialized = D4.JsonSerializer.DeserializeFromString<char>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Char", charVariable, charVariableDeserialized);


                //decimal
                var decimalVariable = decimal.MaxValue; //serializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = D4.JsonSerializer.SerializeToString(decimalVariable);
                var decimalVariableDeserialized = D4.JsonSerializer.DeserializeFromString<decimal>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - decimal", decimalVariable, decimalVariableDeserialized);

                //double
                var doubleVariable = double.MaxValue;// serializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = D4.JsonSerializer.SerializeToString(doubleVariable);
                var doubleVariableDeserialized = D4.JsonSerializer.DeserializeFromString<double>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - double", byteVariable, byteVariableDeserialized);


                //float
                var floatVariable = float.MaxValue;// serializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = D4.JsonSerializer.SerializeToString(floatVariable);
                var floatVariableDeserialized = D4.JsonSerializer.DeserializeFromString<float>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - float", floatVariable, floatVariableDeserialized);


                //int
                var intVariable = int.MaxValue;// serializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = D4.JsonSerializer.SerializeToString(intVariable);
                var intVariableDeserialized = D4.JsonSerializer.DeserializeFromString<int>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - int", intVariable, intVariableDeserialized);


                //long
                var longVariable = long.MaxValue;// serializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = D4.JsonSerializer.SerializeToString(longVariable);
                var longVariableDeserialized = D4.JsonSerializer.DeserializeFromString<long>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - long", longVariable, longVariableDeserialized);


                //sbyte
                var sbyteVariable = sbyte.MaxValue;// serializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = D4.JsonSerializer.SerializeToString(sbyteVariable);
                var sbyteVariableDeserialized = D4.JsonSerializer.DeserializeFromString<sbyte>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - sbyte", sbyteVariable, sbyteVariableDeserialized);


                //short
                var shortVariable = short.MaxValue;// serializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = D4.JsonSerializer.SerializeToString(shortVariable);
                var shortVariableDeserialized = D4.JsonSerializer.DeserializeFromString<short>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - short", shortVariable, shortVariableDeserialized);


                //string
                var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                var stringVariableSerialized = D4.JsonSerializer.SerializeToString(stringVariable);
                var stringVariableDeserialized = D4.JsonSerializer.DeserializeFromString<string>(stringVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - string", stringVariable, stringVariableDeserialized);


                //uint
                var uintVariable = uint.MaxValue;// serializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = D4.JsonSerializer.SerializeToString(uintVariable);
                var uintVariableDeserialized = D4.JsonSerializer.DeserializeFromString<uint>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - uint", uintVariable, uintVariableDeserialized);


                //ulong
                var ulongVariable = ulong.MaxValue;// serializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = D4.JsonSerializer.SerializeToString(ulongVariable);
                var ulongVariableDeserialized = D4.JsonSerializer.DeserializeFromString<ulong>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", ulongVariable, ulongVariableDeserialized);


                //ushort
                var ushortVariable = ushort.MaxValue;// serializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = D4.JsonSerializer.SerializeToString(ushortVariable);
                var ushortVariableDeserialized = D4.JsonSerializer.DeserializeFromString<ushort>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - ushort", ushortVariable, ushortVariableDeserialized);


                //DateTime
               //JsonSerializer.Config.DateTimeMilliseconds = true;


                var datetimeVariable = DateTime.MaxValue;// serializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var dateLikeFromBrowser = DateTime.FromBinary(1485461020284);

                var datetimeVariableSerialized = D4.JsonSerializer.SerializeToString(datetimeVariable);
                var datetimeVariableDeserialized = D4.JsonSerializer.DeserializeFromString<DateTime>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - datetime", datetimeVariable, datetimeVariableDeserialized);


                //TimeSpan
                var timespanVariable = TimeSpan.MaxValue;// serializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = D4.JsonSerializer.SerializeToString(timespanVariable);
                var timespanVariableDeserialized = D4.JsonSerializer.DeserializeFromString<TimeSpan>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - timeSpan", timespanVariable, timespanVariableDeserialized);


                //Guid
                var guidVariable = Guid.Empty;// serializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = D4.JsonSerializer.SerializeToString(guidVariable);
                var guidVariableDeserialized = D4.JsonSerializer.DeserializeFromString<Guid>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Guid", guidVariable, guidVariableDeserialized);


                //Uri
                var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. serializer.DefaultValues.Add(typeof(Uri), default(Uri));
                var uriVariableSerialized = D4.JsonSerializer.SerializeToString(uriVariable);
                var uriVariableDeserialized = D4.JsonSerializer.DeserializeFromString<Uri>(uriVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Uri", uriVariable, uriVariableDeserialized);


                //BitArray
                var bitAArrayVariable = new BitArray(new bool[] { true, true, false, true, false, true });
                var bitAArrayVariableSerialized = D4.JsonSerializer.SerializeToString(bitAArrayVariable);
                var bitAArrayVariableDeserialized = D4.JsonSerializer.DeserializeFromString<BitArray>(bitAArrayVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - BitArray", bitAArrayVariable, bitAArrayVariableDeserialized);


                //Tps.T_DateTime
                //Tps.T_TimeSpan
                //Tps.T_DateTimeOffset
                //Tps.T_Guid
                //Tps.T_Uri
                //Tps.T_byteArray
                //Tps.T_BitArray



            }
            catch (Exception exc)
            {
                throw exc;
            }

        }


        [TestMethod]
        public void Test_4DCoreJson_4DPrimitivesSerialization_NullablePrimitives_Test()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);


                //bool
                bool? boolNullableVariable = true;// = null;
                var boolVariableSerialized = D4.JsonSerializer.SerializeToString(boolNullableVariable);
                var boolVariableDeserialized = D4.JsonSerializer.DeserializeFromString<bool?>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - bool?", boolNullableVariable, boolVariableDeserialized);


                //byte
                byte? byteVariable = byte.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = D4.JsonSerializer.SerializeToString(byteVariable);
                var byteVariableDeserialized = D4.JsonSerializer.DeserializeFromString<byte?>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - byte?", byteVariable, byteVariableDeserialized);


                //char
                char? charVariable = char.MaxValue; //st.JsonSerializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = D4.JsonSerializer.SerializeToString(charVariable);
                var charVariableDeserialized = D4.JsonSerializer.DeserializeFromString<char?>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - char?", charVariable, charVariableDeserialized);


                //decimal
                decimal? decimalVariable = decimal.MaxValue; //st.JsonSerializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = D4.JsonSerializer.SerializeToString(decimalVariable);
                var decimalVariableDeserialized = D4.JsonSerializer.DeserializeFromString<decimal?>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - decimal?", decimalVariable, decimalVariableDeserialized);


                //double
                double? doubleVariable = double.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = D4.JsonSerializer.SerializeToString(doubleVariable);
                var doubleVariableDeserialized = D4.JsonSerializer.DeserializeFromString<double?>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - double?", doubleVariable, doubleVariableDeserialized);


                //float
                float? floatVariable = float.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = D4.JsonSerializer.SerializeToString(floatVariable);
                var floatVariableDeserialized = D4.JsonSerializer.DeserializeFromString<float?>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - float?", floatVariable, floatVariableDeserialized);


                //int
                int? intVariable = int.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = D4.JsonSerializer.SerializeToString(intVariable);
                var intVariableDeserialized = D4.JsonSerializer.DeserializeFromString<int?>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - int?", intVariable, intVariableDeserialized);


                //long
                long? longVariable = long.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = D4.JsonSerializer.SerializeToString(longVariable);
                var longVariableDeserialized = D4.JsonSerializer.DeserializeFromString<long?>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - long?", longVariable, longVariableDeserialized);


                //sbyte
                sbyte? sbyteVariable = sbyte.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = D4.JsonSerializer.SerializeToString(sbyteVariable);
                var sbyteVariableDeserialized = D4.JsonSerializer.DeserializeFromString<sbyte?>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - sbyte?", sbyteVariable, sbyteVariableDeserialized);


                //short
                short? shortVariable = short.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = D4.JsonSerializer.SerializeToString(shortVariable);
                var shortVariableDeserialized = D4.JsonSerializer.DeserializeFromString<short?>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - short?", shortVariable, shortVariableDeserialized);


                //uint
                uint? uintVariable = uint.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = D4.JsonSerializer.SerializeToString(uintVariable);
                var uintVariableDeserialized = D4.JsonSerializer.DeserializeFromString<uint?>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - uint?", uintVariable, uintVariableDeserialized);


                //ulong
                ulong? ulongVariable = ulong.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = D4.JsonSerializer.SerializeToString(ulongVariable);
                var ulongVariableDeserialized = D4.JsonSerializer.DeserializeFromString<ulong?>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ulong?", ulongVariable, ulongVariableDeserialized);


                //ushort
                ushort? ushortVariable = ushort.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = D4.JsonSerializer.SerializeToString(ushortVariable);
                var ushortVariableDeserialized = D4.JsonSerializer.DeserializeFromString<ushort?>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ushort?", ushortVariable, ushortVariableDeserialized);


                //DateTime
                DateTime? datetimeVariable = DateTime.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var datetimeVariableSerialized = D4.JsonSerializer.SerializeToString(datetimeVariable);
                var datetimeVariableDeserialized = D4.JsonSerializer.DeserializeFromString<DateTime?>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - DateTime?", datetimeVariable, datetimeVariableDeserialized);


                //TimeSpan
                TimeSpan? timespanVariable = TimeSpan.MaxValue;// st.JsonSerializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = D4.JsonSerializer.SerializeToString(timespanVariable);
                var timespanVariableDeserialized = D4.JsonSerializer.DeserializeFromString<TimeSpan?>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - TimeSpan?", timespanVariable, timespanVariableDeserialized);


                //Guid
                Guid? guidVariable = Guid.Empty;// st.JsonSerializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = D4.JsonSerializer.SerializeToString(guidVariable);
                var guidVariableDeserialized = D4.JsonSerializer.DeserializeFromString<Guid?>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - Guid?", guidVariable, guidVariableDeserialized);


                ////Uri
                //var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. st.JsonSerializer.DefaultValues.Add(typeof(Uri), default(Uri));
                //var uriVariableSerialized = serializer.Serialize(uriVariable);
                //var uriVariableDeserialized = serializer.Deserialize<Uri>(uriVariableSerialized);


                ////string
                //var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                //var stringVariableSerialized = serializer.Serialize(stringVariable);
                //var stringVariableDeserialized = serializer.Deserialize<string>(stringVariableSerialized);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }





    }







}





