﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;
using System.Diagnostics.Contracts;
using DDDD.Core.Tests.Model.CollectionsModel;

namespace DDDD.Core.Issues.Test.Serialization.JSON
{
    [TestClass]
    public class CollectionTests
    {


        [TestMethod]
        public void Test_4DCore_SerializationJSON_Item_ProductMark()
        {
            try
            {
                var productItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 };
             

                var serializedByFJ = FJ.JSON.ToJSON(productItem);
                var deserializedByFJ = FJ.JSON.ToObject<ProductMark>( serializedByFJ );


                var serializedByD4 = D4.JsonSerializer.SerializeToString(productItem);
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<ProductMark>(serializedByD4);

                

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_SerializationJSON_Collections_ListOfProductMark()
        {
            try
            {

                var productItems = new List<ProductMark>()
                {
                      new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 }
                    , new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                };


                //var typInfEx = Tps.T_object.GetTypeInfoEx();
                //var typeShortName = typInfEx.OriginalTypeAQNameShort;

                //var type2Ex = productItems.GetType().GetTypeInfoEx();
                //var typeShortName2 = type2Ex.TpAQName.LA_CSharpStyleName.Value;
                //var typeShortName3 = type2Ex.OriginalTypeNameTS;

                //var type2 = productItems.GetType();
                //var typeName2 = type2.Name;

                var serializedByD4 = D4.JsonSerializer.SerializeToString(productItems);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<ProductMark>>(serializedByD4);



                var serializedByFJ = FJ.JSON.ToJSON(productItems);
                var deserializedByFJ = FJ.JSON.ToObject<List<ProductMark>>(serializedByFJ);



            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_SerializationJSON_Formatting_ListOfProductMark()
        {
            try
            {
                var productItems = new List<ProductMark>()
                {
                      new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 }
                    , new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                };

                var filePath = @"c:\Temp\jsonList.json";
             
                D4.JsonSerializer.SerializeToFile(productItems, filePath);
                


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }





        [TestMethod]
        public void Test_4DCore_SerializationJSON_Collections_ListOfObject()
        {
            try
            {
                var lisdtData = new List<object>()
                {
                       123
                     , null
                     , 4444.56567
                     , false
                     , "string VALUE"
                     , DateTime.Now
                     //, Guid.NewGuid()
                     //, new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 }                     
                     //, null
                     //, new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                     //, 333
                };
                 

                var serializedByD4 = D4.JsonSerializer.SerializeToString(lisdtData);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<object>>(serializedByD4);


                var serializedByFJ = FJ.JSON.ToJSON(lisdtData);
                var deserializedByFJ = FJ.JSON.ToObject<List<object>>(serializedByFJ);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_Dictionary_Test()
        {
            var dict = new Dictionary<string, string>()
            {  {"odin","odin" }
               , {"dva","dva" }
               ,{"try","try" }
            };

            var El3 = dict.ElementAt(1);

            try
            {
                var ifaces = Tps.T_object.GetInterfaces().Length;


                var dictin = new Dictionary<string, Type>()
                    {

                        #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------

                        { Tps.T_Int32.Name, Tps.T_Int32 }
                        ,
                        { Tps.T_UInt32.Name, Tps.T_UInt32 }
                        ,
                        { Tps.T_Int64.Name, Tps.T_Int64 }
                        ,
                        { Tps.T_UInt64.Name , Tps.T_UInt64 }
                        ,
                        { Tps.T_Int16.Name , Tps.T_Int16 }
                        ,
                        { Tps.T_UInt16.Name , Tps.T_UInt16 }
                        ,
                        { Tps.T_byte.Name  ,  Tps.T_byte }
                        ,
                        { Tps.T_sbyte.Name ,Tps.T_sbyte }
                        ,
                        {Tps.T_char.Name,  Tps.T_char}
                        ,
                        { Tps.T_bool.Name , Tps.T_bool }
                        ,
                        {Tps.T_DateTime.Name, Tps.T_DateTime }
                        ,
                        {Tps.T_TimeSpan.Name, Tps.T_TimeSpan }
                        ,
                        { Tps.T_DateTimeOffset.Name, Tps.T_DateTimeOffset }
                        ,
                        { Tps.T_Guid.Name, Tps.T_Guid }
                        ,
                        {Tps.T_float.Name , Tps.T_float}
                        ,
                        {Tps.T_Double.Name, Tps.T_Double}
                        ,
                        {Tps.T_decimal.Name, Tps.T_decimal }

                        #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


                        #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

                        //,
                        //{Tps.T_Int32Nul.Name, Tps.T_Int32Nul }
                        ////,
                        ////{Tps.T_UInt32Nul.Name, Tps.T_UInt32Nul }
                        ////,
                        ////{Tps.T_Int64Nul.Name, Tps.T_Int64Nul }
                        ////,
                        ////{Tps.T_UInt64Nul.Name, Tps.T_UInt64Nul }
                        ////,
                        ////{Tps.T_Int16Nul.Name, Tps.T_Int16Nul }
                        ////,
                        ////{Tps.T_UInt16Nul.Name, Tps.T_UInt16Nul }
                        ////,
                        ////{Tps.T_byteNul.Name, Tps.T_byteNul }
                        ////,
                        ////{Tps.T_sbyteNul.Name,  Tps.T_sbyteNul}
                        ////,
                        ////{Tps.T_charNul.Name, Tps.T_charNul }
                        ////,
                        ////{Tps.T_boolNul.Name,Tps.T_boolNul }
                        ////,
                        ////{Tps.T_DateTimeNul.Name,Tps.T_DateTimeNul }
                        ////,
                        ////{Tps.T_TimeSpanNul.Name ,Tps.T_TimeSpanNul }
                        ////,
                        ////{Tps.T_GuidNul.Name ,Tps.T_GuidNul }
                        ////,
                        ////{Tps.T_floatNul.Name ,Tps.T_floatNul }
                        ////,
                        ////{Tps.T_DoubleNul.Name,Tps.T_DoubleNul }
                        ////,
                        ////{Tps.T_decimalNul.Name,Tps.T_decimalNul}                                    //,
                        //{Tps.T_Int32Nul.Name, Tps.T_Int32Nul }
                        ////,
                        ////{Tps.T_UInt32Nul.Name, Tps.T_UInt32Nul }
                        ////,
                        ////{Tps.T_Int64Nul.Name, Tps.T_Int64Nul }
                        ////,
                        ////{Tps.T_UInt64Nul.Name, Tps.T_UInt64Nul }
                        ////,
                        ////{Tps.T_Int16Nul.Name, Tps.T_Int16Nul }
                        ////,
                        ////{Tps.T_UInt16Nul.Name, Tps.T_UInt16Nul }
                        ////,
                        ////{Tps.T_byteNul.Name, Tps.T_byteNul }
                        ////,
                        ////{Tps.T_sbyteNul.Name,  Tps.T_sbyteNul}
                        ////,
                        ////{Tps.T_charNul.Name, Tps.T_charNul }
                        ////,
                        ////{Tps.T_boolNul.Name,Tps.T_boolNul }
                        ////,
                        ////{Tps.T_DateTimeNul.Name,Tps.T_DateTimeNul }
                        ////,
                        ////{Tps.T_TimeSpanNul.Name ,Tps.T_TimeSpanNul }
                        ////,
                        ////{Tps.T_GuidNul.Name ,Tps.T_GuidNul }
                        ////,
                        ////{Tps.T_floatNul.Name ,Tps.T_floatNul }
                        ////,
                        ////{Tps.T_DoubleNul.Name,Tps.T_DoubleNul }
                        ////,
                        ////{Tps.T_decimalNul.Name,Tps.T_decimalNul}            

                        #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


                        #region ----------------------------------------  3  Classes:  ---------------------------------------------
                        ,
                        {Tps.T_string.Name,Tps.T_string }
                        ,
                        {Tps.T_byteArray.Name,Tps.T_byteArray }
                        ,
                        {Tps.T_Uri.Name,Tps.T_Uri }
                        ,
                        {Tps.T_BitArray.Name,Tps.T_BitArray }           

                        #endregion---------------------------------------- 3  Classes:  ---------------------------------------------

                    };


                TryAddType(dictin, Tps.T_UInt32Nul);
                TryAddType(dictin, Tps.T_Int64Nul);
                TryAddType(dictin, Tps.T_UInt64Nul);
                TryAddType(dictin, Tps.T_Int16Nul);
                TryAddType(dictin, Tps.T_UInt16Nul);
                TryAddType(dictin, Tps.T_byteNul);
                TryAddType(dictin, Tps.T_sbyteNul);
                TryAddType(dictin, Tps.T_charNul);
                TryAddType(dictin, Tps.T_boolNul);
                TryAddType(dictin, Tps.T_DateTimeNul);
                TryAddType(dictin, Tps.T_TimeSpanNul);
                TryAddType(dictin, Tps.T_GuidNul);
                TryAddType(dictin, Tps.T_floatNul);
                TryAddType(dictin, Tps.T_DoubleNul);
                TryAddType(dictin, Tps.T_decimalNul);


            }
            catch (Exception exc)
            {
                throw exc;
            }            

        }


        static void TryAddType(Dictionary<string,Type> tpDict, Type newType)
        {
            if (tpDict.ContainsKey(newType.Name))
            {
                throw new IndexOutOfRangeException("Already Exist Key:"
                            + newType.Name);
            }
            else
            {
                tpDict.Add(newType.Name, newType);
            }
        }


        [TestMethod]
        public void Test_4DCore_SerializationJSON_Collections_DictionaryIntString()
        {
            try
            {
                var dict1 = new Dictionary<int, string>()
                {
                    { 1,"One"}
                    ,{ 2,"Two"}
                    ,{ 3,"Three"}
                    ,{ 4,"Four"}
                    ,{ 5,"Five"}
                };
                    
                //var serializedByFJ = FJ.JSON.ToJSON(dict1);
                // "[{\"k\":1,\"v\":\"One\"},{\"k\":2,\"v\":\"Two\"},{\"k\":3,\"v\":\"Three\"},{\"k\":4,\"v\":\"Four\"},{\"k\":5,\"v\":\"Five\"}]"
                //var deserializedByFJ = FJ.JSON.ToObject<Dictionary<int, string>>(serializedByFJ);


                var serializedByD4 = D4.JsonSerializer.SerializeToString(dict1);
                // "[{\"k\":1,\"v\":\"One\"},{\"k\":2,\"v\":\"Two\"},{\"k\":3,\"v\":\"Three\"},{\"k\":4,\"v\":\"Four\"},{\"k\":5,\"v\":\"Five\"}]"
                var deserializedByD4 = D4.JsonSerializer.DeserializeFromString<Dictionary<int, string>>(serializedByD4);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_SerializationJSON_Collections_ArrayString()
        {
            try
            {
                var array1 = new string[5];
                array1[0] = "One";
                array1[1] = "Two";
                array1[2] = "Three";
                array1[3] = "Four";
                array1[4] = "Five";


                var serializedByD4 = D4.JsonSerializer.SerializeToString(array1);                
                var deserializedByD4 = D4.JsonSerializer.DeserializeFromString<string[]>(serializedByD4);


            }
            catch (Exception exc)
            {
                throw exc;
            }

        }




    }
}
