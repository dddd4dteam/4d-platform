﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;


namespace DDDD.Core.Issues.Test.Serialization.JSON
{
    [TestClass]
    public class InterfaceTests
    {


        [TestMethod]
        public void Test_4DCore_Serialization_Json_Object_Tests()
        {
            try
            {
                object boxedEnumItem = SomeTypologyEn.Birds;


                var serializedByFJ = FJ.JSON.ToJSON(boxedEnumItem);
                //   "\"Birds\""    "\"Birds\""  
                var objectBack = FJ.JSON.ToObject<SomeTypologyEn>(serializedByFJ);

                // D$ serializer
                var serializedByD4 = D4.JsonSerializer.SerializeToString(boxedEnumItem);
                var deserializedByD4 = D4.JsonSerializer.DeserializeFromString<SomeTypologyEn>(serializedByD4);
                                             
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Json_NullableType1_Test()
        {
            try
            {
                FJ.JSON.Parameters.UsingGlobalTypes = false;

                //enum 
                SomeTypologyEn? enumItem = null;
                var serializedByFJ1 = FJ.JSON.ToJSON(enumItem);
                enumItem =  SomeTypologyEn.Peoples;
                serializedByFJ1 = FJ.JSON.ToJSON(enumItem);
                // 

                //int
                int? intItem = null;
                var serializedByFJ2 = FJ.JSON.ToJSON(intItem);
                intItem = 345345;
                serializedByFJ2 = FJ.JSON.ToJSON(intItem);
                // 


                //struct
                ProductMark? structItem = null;
                var serializedByFJ3 = FJ.JSON.ToJSON(structItem);
                structItem = new ProductMark() { Copyrighter="cvbcvbcv" } ;
                serializedByFJ3 = FJ.JSON.ToJSON(structItem);
                // "{\"$type\":\"DDDD.TSS.Tests.Model.CommunicationModel.ProductMark, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\",\"Copyrighter\":\"cvbcvbcv\",\"Mark\":null,\"YearCreated\":0}"

                //class
                Product classItem = null;
                var serializedByFJ4 = FJ.JSON.ToJSON(classItem);
                classItem  = new Product() { Name= "Product7" };
                serializedByFJ4 = FJ.JSON.ToJSON(classItem);
                // "{\"$type\":\"DDDD.TSS.Tests.Model.CommunicationModel.Product, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\",\"Id\":0,\"Name\":\"Product7\",\"Category\":null,\"Price\":null,\"Length\":0,\"Mark\":{\"$type\":\"DDDD.TSS.Tests.Model.CommunicationModel.ProductMark, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\",\"Copyrighter\":null,\"Mark\":null,\"YearCreated\":0},\"PartsNames\":null,\"PartsProductMarks\":null,\"PublicIntField\":0}"


               

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Reflection_TypeInfoEx_ID_Test()
        {
            //list
            var tpInf = typeof(List<ProductMark>).GetTypeInfoEx();
            var ftm = tpInf.TpAQName.FullTypeName;
            //

            try
            {
                var typeName = "DDDD.TSS.Tests.Model.CommunicationModel.ProductMark";
                var tpInfEx = Type.GetType(typeName);
                //TypeInfoEx.
            }
            catch (Exception exc2)
            {

                throw exc2;
            }
 
        }





    }
}
