﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.Contracts;
using System.Collections.Generic;
using DDDD.Core.App;

namespace DDDD.Core.Issues.Test
{
    [TestClass]
    public class Test_4DCore_Issues_Tests
    {


        static void TryCatchItself()
        {
            int i = 0;
            int zero = 0;
            try
            {
                i = 456 / zero;
            }
            catch ( Exception ex)
            {
                throw ex; 
            }
            finally
            {
                i++;
            }


        }




        static void TrycatchWrap()
        {
            try
            {
                TryCatchItself();
            }
            catch (System.Exception ex)
            {
            	
            }
            
        }


        [TestMethod]
        public void Test_Issues_TryCatchFinallyWithCatchThrow()
        {
            TrycatchWrap();
        }



        public static int GetHashCode(string somestring)
        {

#if FEATURE_RANDOMIZED_STRING_HASHING
            if(HashHelpers.s_UseRandomizedStringHashing)
            {
                return InternalMarvin32HashString(this, this.Length, 0);
            }
#endif // FEATURE_RANDOMIZED_STRING_HASHING

            unsafe
            {
                fixed (char* src = somestring)
                {
                    Contract.Assert(src[somestring.Length] == '\0', "src[this.Length] == '\\0'");
                    Contract.Assert(((int)src) % 4 == 0, "Managed string should start at 4 bytes boundary");

#if WIN32
                    int hash1 = (5381<<16) + 5381;
#else
                    int hash1 = 5381;
#endif
                    int hash2 = hash1;

#if WIN32
                    // 32 bit machines.
                    int* pint = (int *)src;
                    int len = somestring.Length;
                    while (len > 2)
                    {
                        var pint0 = pint[0];
                        var pint1 = pint[1];
                        hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                        hash2 = ((hash2 << 5) + hash2 + (hash2 >> 27)) ^ pint[1];
                        pint += 2;
                        len  -= 4;
                    }

                    if (len > 0)
                    {
                        hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                    }
#else
                    int c;
                    char* s = src;
                    while ((c = s[0]) != 0)
                    {
                        hash1 = ((hash1 << 5) + hash1) ^ c;
                        c = s[1];
                        if (c == 0)
                            break;
                        hash2 = ((hash2 << 5) + hash2) ^ c;
                        s += 2;
                    }
#endif
#if DEBUG
                    // We want to ensure we can change our hash function daily.
                    // This is perfectly fine as long as you don't persist the
                    // value from GetHashCode to disk or count on String A 
                    // hashing before string B.  Those are bugs in your code.
                    //hash1 ^= ThisAssembly.DailyBuildNumber;
#endif
                    return hash1 + (hash2 * 1566083941);
                }
            }
        }


        [TestMethod]
        public void Test_Issues_AppBootstrapper_LoadBackgroundAppComponents_Test()
        {
            AppBootstrapper.Current.LoadBackgroundAppComponents(
              "ComputerUseTimeManager"
              , null
              , null, null,
               null
              );
        }


        [TestMethod]
        public void Test_Issues_ArrayTypesNames()
        {
           Console.WriteLine(typeof(List<string>).FullName);

            try
            {
                var tName1 = "System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]";

                var tName2 = "System.Nullable`1[[System.Int32]]";

                var getType1 = Type.GetType(tName1);
                var getType2 = Type.GetType(tName2);



            }
            catch (System.Exception ex)
            {
            	    
            }
        }


        [TestMethod]
        public void Test_Issues_NameofResultForGenericClass()
        {
            var methodName = nameof(GenericClass<object>.GetTypeName);
            var inttest = 32;
        }

        [TestMethod]
        public void Test_string_HashCodeUtil()
        {
            var somestring = "Acme Product Management";

            var hashLocal = GetHashCode(somestring);

            var hashOrigin = somestring.GetHashCode();

            if (hashLocal == hashOrigin)
            {
                Console.WriteLine($"Teststring[{somestring}].Local hash func result [IS THE SAME] as Origin hash func result : Local[{hashLocal}] | Origin[{hashOrigin}]");
            }
            else Console.WriteLine($"Teststring[{somestring}].Local hash func result [IS NOT THE SAME] as Origin hash func result : Local[{hashLocal}] | Origin[{hashOrigin}]");

        }



    }
}
