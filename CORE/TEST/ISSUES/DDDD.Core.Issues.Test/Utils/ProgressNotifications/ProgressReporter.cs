﻿using System;
using System.Collections.Generic;

namespace DDDD.Core.Test.SL5
{



    public interface IProgessChangeCommand<TCmdEnum>
    {
        TCmdEnum Command { get; }

        string Message { get; }

        String DetailMessage { get; }
    }

    /// <summary>
    ///  ProgressInfoTp- [progress action info] struct that will be transmitted from back-Task-Thread into UI-Thread 
    /// </summary>    
    public struct TestProgressInfoTp : IProgessChangeCommand<TestsProgressCommandsEn>
    {

        /// <summary>
        /// Progress Indication Command
        /// </summary>
        public TestsProgressCommandsEn Command
        {
            get;
            private set;
        }


        /// <summary>
        /// Progress Indication Message
        /// </summary>
        public string Message
        {
            get;
            private set;
        }


        /// <summary>
        /// Progress Detail Message
        /// </summary>
        public String DetailMessage
        {
            get;
            private set;
        }


        /// <summary>
        /// Create  ProgressInfoTp- [progress action info] struct that will be transmitted from back-Thread into UI-Thread  
        /// </summary>
        /// <param name="commandToShowProgress"></param>
        /// <returns></returns>
        public static TestProgressInfoTp ProgressAction(TestsProgressCommandsEn commandToShowProgress, String message = null, String detailMessage = null)
        {
            return new TestProgressInfoTp() { Command = commandToShowProgress, Message = message, DetailMessage = detailMessage };
        }
        
    }


    /// <summary>
    /// Generic UI Progress Change Action Reporter - reports UI to change indicators from your back-Task-Thread
    /// </summary>
    public class ProgressReporter<TCmdEnum> : Progress<IProgessChangeCommand<TCmdEnum>>
    {
    
        #region  ----------------- CTOR ------------------------

        public ProgressReporter()
        {
            InitProgressFlagCommands();
            
            //set UI ProgressChanged event- OnReport Handler 
            base.ProgressChanged += DefaultOnReportHandler;
        }
              
        #endregion ----------------- CTOR ------------------------


        #region ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------


        IProgessChangeCommand<TCmdEnum> _Progress;

        /// <summary>
        ///By default it is making by base ProgressReporter
        /// </summary>
        public IProgessChangeCommand<TCmdEnum> Progress
        {
            get { return _Progress; }
            private set
            {
                _Progress = value;                

                OnReport(_Progress);//Reporting 
               
            }

        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: Progress------------------------------

        /// <summary>
        /// Custom command Actions 
        /// </summary>
        Dictionary<Enum, Action<IProgessChangeCommand<TCmdEnum>>> CmdActions = new Dictionary<Enum, Action<IProgessChangeCommand<TCmdEnum>> >();
        
        void InitProgressFlagCommands()
        { 
            foreach (var enumValue in Enum.GetValues(typeof(TCmdEnum)))
	        {
                //switching Order to check Flags  - by enum value

                CmdActions.Add( enumValue as Enum, null);
	        }            
        }


        /// <summary>
        /// Check ProgressChange  EnumCommands by Enum.HasFlag(cmd) and call registered Action if it exist.
        /// The Order of commands enumeration belong to enum members declaration order.
        /// Commands calls will be combined as it'l be found in your enum-flags-value combination.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="progressArg"></param>
        protected virtual void DefaultOnReportHandler(object sender, IProgessChangeCommand<TCmdEnum> progressArg )
        {
            foreach (var commandItem in  Enum.GetValues( typeof(TCmdEnum)))
            {
                if ( (progressArg.Command as Enum).HasFlag(commandItem as Enum ) )
                {
                    if (CmdActions.ContainsKey(commandItem as Enum) && CmdActions[commandItem as Enum] != null)
                    {
                        CmdActions[commandItem as Enum](progressArg);
                    }
                }                    
            }                        
        }
        
        
        /// <summary>
        /// Custom ProgressChange EnumCommand action in UI for TCmdEnum Cmd command. 
        /// Commands calls can be combined as it'l be found in your enum-flags-value combination.
        /// </summary>
        /// <param name="Cmd"></param>
        /// <param name="someUIProgressActionOnCmd"></param>
        public void RegisterCommandAction(TCmdEnum Cmd, Action<IProgessChangeCommand<TCmdEnum>> someUIProgressActionOnCmd)
        {
            CmdActions[Cmd as Enum] = someUIProgressActionOnCmd;            
        }


        /// <summary>
        ///  Notify UI with  ProgressInfoTp - [progress action info] struct 
        /// </summary>
        /// <param name="progress"></param>
        public void NotifyProgress(IProgessChangeCommand<TCmdEnum> progress)
        {
            this.Progress = progress;
        }


    }
 
   




}
