﻿using System;



namespace DDDD.Core.Test.SL5
{

    /// <summary>
    /// Progress Indication Commands .You can use it as flags too
    /// </summary>
    [Flags]
    public enum TestsProgressCommandsEn
    {
        /// <summary>
        /// Show BusyIndicator and Start Busy command
        /// </summary>
        StartProcessIndication= 1,

        /// <summary>
        /// BusyIndicator Content message
        /// </summary>
        ProgressMessage=2,

        /// <summary>
        /// TextBox Text Message
        /// </summary>
        DetailMessage=4,

        /// <summary>
        ///Clear all TextBox Text Messages that was there before
        /// </summary>
        ClearDetailMessages=8,

        /// <summary>
        ///Hide BusyIndicator and Stop Busy command
        /// </summary>
        StopProcessIndication=16
    }



}

