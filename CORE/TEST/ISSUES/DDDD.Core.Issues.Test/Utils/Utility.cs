﻿

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
//using KellermanSoftware.CompareNetObjects;

using DDDD.Core.Diagnostics;
using DDDD.Core.Reflection;
using DDDD.Core.Reflection.Compare;

#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.Test.SL5;
using System.Windows;

#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
#endif




namespace DDDD.Core.TestUtils
{
   public class Utility
	{
		
		/// <summary>
		/// Compare two objects based on Policy that we used in serialization
		/// </summary>
		/// <param name="TestName"></param>
		/// <param name="originalItem"></param>
		/// <param name="deserializedItem"></param>
		/// <param name="policy"></param>
		internal static void CompareSerializeResult(String TestName
            , object originalItem
            , object deserializedItem
            , TypeMemberSelectorEn policy
            , bool CompareChildren= true )
		{
			Validator.AssertTrue<InvalidOperationException>(originalItem == null, "CompareSerializeResult():  Original Item can't be null");
			Validator.AssertTrue<InvalidOperationException>(deserializedItem == null, "CompareSerializeResult():  Original deserializedItem can't be null");
			
			// compare This is the comparison class                    
			


			if (originalItem.GetType() == typeof(BitArray)) //compare
			{
				//compareLogic.Config.MembersToIgnore = new List<String>() { "_version" };
			}
					   
			var result = CompareService.IsEqualData(
												originalItem
                                                ,deserializedItem
                                                ,policy
                                                );

			Validator.AssertFalse<InvalidOperationException>(result.Count == 0,
				string.Format(" Test: [{0}]  -  Serialize / Deserialize Items are not equal ", TestName));

		}



		internal static void CompareSerializeResult(String TestName, object originalItem, object deserializedItem)
		{
			CompareSerializeResult(TestName, originalItem, deserializedItem, TypeMemberSelectorEn.DefaultNoBase);
		}

	   


#if SL5

		internal static Task StartFullTests(TestsProgressReporter Reporter)
		{
			return Task.Factory.StartNew((state) =>
			{

				var reporter = (state as TestsProgressReporter);
				   
				var result = new StringBuilder();
				var assemmbly = Application.Current.GetType().Assembly;
				
				//1 test classes searching
				var testClasses = assemmbly
				   .GetTypes()
				   .Where(t => t.GetCustomAttributes(typeof(TestClassAttribute), false).Length > 0)
				   .ToArray();

				if (testClasses == null || testClasses.Count() == 0)
				{   
					//Write Test Classes was not Found
					string NoTestClassesFoundMessage = " There was no  found  Test Classes in Aplication.Current.Assembly types :(:(:( ";                    
					reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																	NoTestClassesFoundMessage, "\n\n\n" + NoTestClassesFoundMessage)
										   );
					return;
				}


				reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ClearDetailMessages));
				reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.StartProcessIndication | TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																	"Start Testing", "-----------Starting  TESTS -----------")
									   );


				int cntAll = 0;
				int cntPassed = 0;
				foreach (var t in testClasses)
				{
					var obj = Activator.CreateInstance(t);

					foreach (var mi in
						t.GetMethods()
						.Where(m => m.GetCustomAttributes(typeof(TestMethodAttribute), false).Length > 0 && m.GetParameters().Length == 0)
					)
					{
						result.Append("Executing " + t.Name + "." + mi.Name + "... ");

						cntAll++;
						mi.Invoke(obj, null);
						cntPassed++;
						result.AppendLine("OK");
					}
				}

				result.AppendLine("Done. " + cntPassed + "(" + cntAll + ")");
        
				reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.StopProcessIndication | TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
								   );

				
			   },
			   Reporter                 
			   )
			   .ContinueWith( (t) =>
			   {

				   var rpt = t.AsyncState as TestsProgressReporter;
				   if ((t.IsFaulted || t.IsCanceled)
					   && t.Exception != null && t.Exception.InnerExceptions != null)
				   {
					   foreach (var iexc in t.Exception.InnerExceptions)
					   {
						   string iexcMessage = String.Format(@" Error Message : [{0}] ; \n  StackTrace [{1}] ", iexc.Message, iexc.StackTrace);

						   rpt.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.DetailMessage, null, iexcMessage));
					   }

					   rpt.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.StopProcessIndication | TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																		"TESTS FAILED", "TESTS FAILED"));
				   }


			   });
		}



		/// <summary>
		/// Only Some Test Class methods testing
		/// </summary>
		/// <param name="Reporter"></param>
		/// <param name="ClassName"></param>
		/// <param name="useCleanDetailCommandOnStart"></param>
		/// <returns></returns>
		internal static Task StartClassTests(TestsProgressReporter Reporter, String ClassName, bool useCleanDetailCommandOnStart = true)
		{
			return Task.Factory.StartNew((state) =>
			{
				var reporter = (state as TestsProgressReporter);

				if (useCleanDetailCommandOnStart)
				{ reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ClearDetailMessages));
				}
							

				var result = new StringBuilder();
				var assemmbly = Application.Current.GetType().Assembly;
			   
				//1 test classes searching
				var testClasses = assemmbly
				   .GetTypes()
				   .Where(t => t.GetCustomAttributes(typeof(TestClassAttribute), false).Length > 0)
				   .ToArray();  
			   
				if (testClasses == null || testClasses.Count() == 0) 
				{
					string NoTestClassesFoundMessage = " There was no  found  Test Classes in Aplication.Current.Assembly types :(:(:( ";
					//Write Test Classes was not Found
					reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																	NoTestClassesFoundMessage, "\n\n\n" + NoTestClassesFoundMessage)
										   );                    
					return;                     
				}
				
				//2 target class searching 
				var targetTestClass = testClasses.Where(tcl => tcl.Name == ClassName).FirstOrDefault();
				if (targetTestClass == null )
				{
					string TargetClassNotFoundMessage = String.Format("Class [{0}] was not found to Test :( ", ClassName);
					//Write  Target Test Class was not Found                   
					reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																	TargetClassNotFoundMessage, "\n\n\n" + TargetClassNotFoundMessage)
										   );
					return;
				}

				//3 Starting Class Tests
				string StartClassTestMessage = String.Format("Start Class [{0}] Testing ...", ClassName);
				reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																	StartClassTestMessage, "\n\n\n" + StartClassTestMessage)
										);


				int cntAll = 0;
				int cntPassed = 0;
				foreach (var t in testClasses)
				{
					var obj = Activator.CreateInstance(t);

					foreach (var mi in
						t.GetMethods()
						.Where(m => m.GetCustomAttributes(typeof(TestMethodAttribute), false).Length > 0 && m.GetParameters().Length == 0)
					)
					{
						result.Append("Executing " + t.Name + "." + mi.Name + "... ");

						cntAll++;
						mi.Invoke(obj, null);
						cntPassed++;
						result.AppendLine(" OK ");
					}
				}


				result.AppendLine("Done. " + cntPassed + "(" + cntAll + ")");


				reporter.NotifyProgress(TestProgressInfoTp.ProgressAction(TestsProgressCommandsEn.StopProcessIndication | TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																   "Stop Testing", result.ToString() + "\n -----------FINISHED TESTS SUCCESFULLY -----------")
								  );

			},
			   Reporter
			   )
			   .ContinueWith((t) =>
			   {

				   var reporter = t.AsyncState as TestsProgressReporter;
				   if ((t.IsFaulted || t.IsCanceled)
					   && t.Exception != null && t.Exception.InnerExceptions != null)
				   {
					   foreach (var iexc in t.Exception.InnerExceptions)
					   {
						   string iexcMessage = String.Format(@" Error Message : [{0}] ; \n  StackTrace [{1}] ", iexc.Message, iexc.StackTrace);

						   reporter.NotifyProgress(TestProgressInfoTp.ProgressAction( TestsProgressCommandsEn.DetailMessage, null, iexcMessage));
					   }

					   reporter.NotifyProgress(TestProgressInfoTp.ProgressAction( TestsProgressCommandsEn.StopProcessIndication | TestsProgressCommandsEn.ProgressMessage | TestsProgressCommandsEn.DetailMessage,
																		"TESTS FAILED", "TESTS FAILED"));
				   }


			   });
		}


#endif

	 


	}
}
