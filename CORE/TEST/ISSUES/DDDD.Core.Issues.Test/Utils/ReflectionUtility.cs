﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.BCL.TestUtils
{
    



    public class ReflectionUtility<ObjectType>
    {



        internal static Action<object, object> CreateSetMemberAction( MemberInfo memberInfo)
        {
            Type ParamType;
            if (memberInfo is PropertyInfo)
                ParamType = ((PropertyInfo)memberInfo).PropertyType;
            else if (memberInfo is FieldInfo)
                ParamType = ((FieldInfo)memberInfo).FieldType;
            else
                throw new Exception("Can only create set methods for properties and fields.");

            Type[] ParamTypes = new Type[] { typeof(object), typeof(object) };    //.MakeByRefType()

            string SetMethodName = "_Set" + ((memberInfo is FieldInfo) ? "Field" : "IsSharedDomainModelGroup") + "_" + memberInfo.Name;

            DynamicMethod setter = new DynamicMethod(
                SetMethodName
                ,typeof(void)
                ,new [] { typeof(object), typeof(object) }
#if !SL5 && !WP81
                ,memberInfo.ReflectedType.Module
                ,true        
#endif
        );

            ILGenerator generator = setter.GetILGenerator();
            generator.Emit(OpCodes.Ldarg_0);
            //generator.Emit(OpCodes.Ldind_Ref);
            generator.Emit(OpCodes.Castclass, typeof(ObjectType)); // instanceType

            if (memberInfo.DeclaringType.IsValueType)
            {
#if UNSAFE_IL
        generator.Emit(OpCodes.Unbox, memberInfo.DeclaringType);
#else
                generator.DeclareLocal(memberInfo.DeclaringType);//.MakeByRefType()
                generator.Emit(OpCodes.Unbox, memberInfo.DeclaringType);   //Unbox_Any
                generator.Emit(OpCodes.Stloc_0);
                generator.Emit(OpCodes.Ldloc_0);
#endif // UNSAFE_IL
            }

            generator.Emit(OpCodes.Ldarg_1);
            if (ParamType.IsValueType)
                generator.Emit(OpCodes.Unbox_Any, ParamType);

            if (memberInfo is PropertyInfo)
                generator.Emit(OpCodes.Callvirt, ((PropertyInfo)memberInfo).GetSetMethod());
            else if (memberInfo is FieldInfo)
                generator.Emit(OpCodes.Stfld, (FieldInfo)memberInfo);

            if (memberInfo.DeclaringType.IsValueType)
            {
#if !UNSAFE_IL
                generator.Emit(OpCodes.Ldarg_0);
                generator.Emit(OpCodes.Ldloc_0);
                generator.Emit(OpCodes.Ldobj, memberInfo.DeclaringType);
                generator.Emit(OpCodes.Box, memberInfo.DeclaringType);
                generator.Emit(OpCodes.Stind_Ref);
#endif // UNSAFE_IL
            }
            generator.Emit(OpCodes.Ret);

            return (Action<object, object>)setter.CreateDelegate(typeof(Action<object, object>));
        }


    }
}
