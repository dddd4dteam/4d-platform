﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.App;

namespace DDDD.Core.Issues.Test.IOC
{
    public class BOBase
    {
        public BOBase()
        {
            if (StatVal == null)
            {
                StatVal = GetType();
            }
        }
        public   static Type StatVal = null;


    }

    public class BOBaseSon : BOBase
    {
       public BOBaseSon() : base() 
        {
             

        }

       public new static Type StatVal = null;

    }


    /// <summary>
    /// 4D  IOC  COntainer - App Bootstrapping with ConponentsContainer testing
    /// </summary>
    [TestClass]
    public class IOCUnitTests
    {
        public IOCUnitTests()
        {
            //
            // TODO: добавьте здесь логику конструктора
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Получает или устанавливает контекст теста, в котором предоставляются
        ///сведения о текущем тестовом запуске и обеспечивается его функциональность.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Дополнительные атрибуты тестирования
        //
        // При написании тестов можно использовать следующие дополнительные атрибуты:
        //
        // ClassInitialize используется для выполнения кода до запуска первого теста в классе
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // ClassCleanup используется для выполнения кода после завершения работы всех тестов в классе
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // TestInitialize используется для выполнения кода перед запуском каждого теста 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // TestCleanup используется для выполнения кода после завершения каждого теста
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Test_Issues_AppBootstrapper_LoadBackgroundAppComponents_Test()
        {
            AppBootstrapper.Current.LoadBackgroundAppComponents(
              "ComputerUseTimeManager"
              , null
              , null
              , null
              , null
              );
        }

        [TestMethod]
        public void Test_StaticNewMember_Test()
        {
            var bobase = new BOBase();            
            var bobaseSon = new BOBaseSon();
            var tp1 = BOBase.StatVal;
            var tp2 = BOBaseSon.StatVal;

        }


    }
}
