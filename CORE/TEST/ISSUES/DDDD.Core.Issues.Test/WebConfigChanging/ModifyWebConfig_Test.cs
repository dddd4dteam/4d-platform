﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using System.ServiceModel.Configuration;
using DDDD.Core.Net.ServiceModel;
using System.Web.Configuration;

namespace DDDD.Core.Issues.Test.WebConfigChanging
{
    /// <summary>
    /// Summary description for ModifyWebConfig_Test
    /// </summary>
    [TestClass]
    public class ModifyWebConfig_Test
    {
        public ModifyWebConfig_Test()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        public object ConfigurationManager { get; private set; }

        static string FilePath = "";


        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            System.Configuration.Configuration wConfig = WebConfigurationManager.OpenWebConfiguration("~");
            ServiceModelSectionGroup wServiceSection = ServiceModelSectionGroup.GetSectionGroup(wConfig);
            //  <add name="ddddtss"
            //type = "DDDD.Core.Net.ServiceModel.TSSBehaviorExtension, DDDD.Core.Net45.v.2.0.1.0, Version=2.0.1.0, Culture=neutral, PublicKeyToken=e364950485695b69" />
            if ( !wServiceSection.Extensions.BehaviorExtensions.ContainsKey("ddddtss") )
            {
                var extTss = new ExtensionElement("ddddtss", typeof(TSSBehaviorExtension).AssemblyQualifiedName);
                wServiceSection.Extensions.BehaviorExtensions.Add(extTss);
            }

            ClientSection wClientSection = wServiceSection.Client;
            //wClientSection.Endpoints[0].Address = < your address >;
            wConfig.Save();


        }
    }
}
