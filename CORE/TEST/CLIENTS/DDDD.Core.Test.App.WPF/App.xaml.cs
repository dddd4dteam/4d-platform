﻿
using DDDD.Core.App;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace DDDD.Core.Test.Net45.WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);


            AppBootstrapper.Current.LoadAppComponents(
                null,  
                null, 
                null, 
                null );
            
        }
    }
}
