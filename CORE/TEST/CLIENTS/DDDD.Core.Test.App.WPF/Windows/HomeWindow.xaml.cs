﻿using DDDD.Core.Test.Net45.WPF.Pages;
using DDDD.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDDD.Core.Test.Net45.WPF.Windows
{
    /// <summary>
    /// Interaction logic for HomeWindow.xaml
    /// </summary>
    [ShellWindow("Shell1")]
    public partial class HomeWindow : Window
    {
        public HomeWindow()
        {
            InitializeComponent();
        }

               
        private void btn_WritableBitmap_Click(object sender, RoutedEventArgs e)
        {
            frm_NavigationFrame.Navigate(new WritableBitmapExamplePage());
            

        }

        private void btn_DCSClient_example_Click(object sender, RoutedEventArgs e)
        {
            frm_NavigationFrame.Navigate(new  DCSClientTestExamplePage());
        }
    }
}
