﻿using System;
using System.Linq;
using System.Timers;
using System.Management;
 
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using DDDD.Core.Threading;
using DDDD.Core.Extensions;
using Microsoft.Win32;
using System.Diagnostics;
using DDDD.Core.Environment;
using DDDD.Core.UI.Windows;

namespace DDDD.Core.Test.App.WinService 
{
    public class ComputerTimeUseManager 
    {
        #region -------------------------------- CTOR ---------------------------------------


        static ComputerTimeUseManager()
        {
            Init();
        }

        #endregion -------------------------------- CTOR ---------------------------------------

        #region  -------------------------------- INIT ------------------------------------  
        static void Init()
        {
            ConditionCheckTimer = new Timer(ConditionCheckTimeRange.TotalMilliseconds);
            ConditionCheckTimer.AutoReset = true;
            ConditionCheckTimer.Elapsed += ConditionCheckTimer_Elapsed;

        }

        #endregion -------------------------------- INIT ------------------------------------


       


        #region -------------------------------- PROPERTIES ------------------------------------
        static Timer ConditionCheckTimer
        { get; set; }


        /// <summary>
        /// Checking range
        /// </summary>
        public static TimeSpan ConditionCheckTimeRange
        { get; set; } = new TimeSpan(0, 3, 0);// 3 minutes betweeen cheking


        /// <summary>
        /// Enable to use Time range Start-Begin border. By default it's 00:00.    
        /// </summary>
        public static TimeSpan EnableTime_StartRangeBorder
        { get; set; } = new TimeSpan(00, 00, 00);


        /// <summary>
        /// Enable to use Time-Range Stop-End border. By default it's 05:00. 
        /// </summary>
        public static TimeSpan EnableTime_StopRangeBorder
        { get; set; } = new TimeSpan(5, 00, 00);


        /// <summary>
        /// 
        /// </summary>
        static bool IsMessageAlreadyShowned
        { get; set; }



        #endregion -------------------------------- PROPERTIES ------------------------------------



        public static void Start()
        {
            //CheckAndAddToAutorun();
            Environment2.CheckExistAddUpdateToOSStartup(Registry.CurrentUser);

            ConditionCheckTimer.Start();

        }


        static void ShutdownPC()
        {
            Process.Start("Shutdown", "-s -t 10");
        }

        #region ------------------------------ TIMER && CHECK CURRENT TIME LOGIC -------------------------------------
        private static async void ConditionCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            await CheckAvailableTimeAndShutdownIfNeed();
        }

        public static bool IfThisTimeIsAvailable()
        {
            var currrentDayTime = DateTime.Now.TimeOfDay;

            if (currrentDayTime > EnableTime_StartRangeBorder && currrentDayTime < EnableTime_StopRangeBorder)
            { // we need to shtdown PC
                return false;
            }
            return true;
        }


        async static Task CheckAvailableTimeAndShutdownIfNeed()
        {
            if (IfThisTimeIsAvailable() == false && IsMessageAlreadyShowned == false)
            {

                UIInvoker.InvokeAsynchronously(
                    () =>
                    {
                        var mesageWindow = new MessageWithTimerWindow("Пришло время выключать компьютер. Пора спать.", "Осталось до выключения: ");
                        mesageWindow.ShowDialog();

                        ShutdownPC();
                    }
                    );
                IsMessageAlreadyShowned = true;
            }
        }


        #endregion ------------------------------ TIMER && CHECK CURRENT TIME LOGIC -------------------------------------

        #region ------------------------------ ADD TO AUTORUN --------------------------------------


        //private static void CheckAndAddToAutorun()
        //{

        //    if (RegistryCheckKeyExistPS("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", AutoRunProgramName) == false)
        //    {
        //        var key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
        //        key.SetValue(AutoRunProgramName,Environment2.EntryAssemblyLocation );
        //    }
        //    else
        //    {
        //        //if already exist - then compare values if is the same then break? else update to currentValue
        //        var key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
        //        var valueInKey = key.GetValue(AutoRunProgramName);
        //        if ( (valueInKey as string)  != Environment2.EntryAssemblyLocation)
        //        {
        //            key.SetValue(AutoRunProgramName, Environment2.EntryAssemblyLocation);
        //        }                 
        //    }

        //}



        ///// <summary>
        ///// Check Win Registry Key exist
        ///// </summary>
        ///// <param name="regPath"></param>
        ///// <returns></returns>
        //public static bool RegistryCheckKeyExistPS(string regPath, string checkStringPathValue)
        //{
        //    try
        //    {

        //        var readScriptResult = Registry.CurrentUser.OpenSubKey(regPath);
        //        if (readScriptResult.IsNull()) return false;

        //        var paramsInRegkey = readScriptResult.GetValueNames();
        //        return (paramsInRegkey.IsNotNull() && paramsInRegkey.Contains(checkStringPathValue));

        //    }
        //    catch (Exception exc)
        //    {
        //        // log - notEXIST
        //        return false;
        //    }
        //}


        #endregion ------------------------------ ADD TO AUTORUN --------------------------------------





    }
}