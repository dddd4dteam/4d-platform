﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DDDD.Core.Test.App.WinService.Windows
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class BackgroundWindow : Window, INotifyPropertyChanged
    {

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion


        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }



        #region ------------------NOTIFIABLE MODEL ITEM: MessageText------------------------------

        // ItemName - MessageText  -  Ex: Client 
        // Contract - string  -  Ex: V_S8_Client_Vo

        string _MessageText = "";

        /// <summary>
        /// Description
        /// </summary>
        public string MessageText
        {
            get { return _MessageText; }
            set
            {
                _MessageText = value; //Field changing
                OnPropertyChanged(() => MessageText);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: MessageText------------------------------
        


        public BackgroundWindow()
        {
            InitializeComponent();

             Visibility = Visibility.Hidden;
            ComputerTimeUseManager.Start();
            
        }
    }
}
