﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using DDDD.Core.Extensions;

namespace DDDD.Core.Test.App.WinService.Windows
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class MessageWindow : Window, INotifyPropertyChanged
    {

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion


        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid property");
            }

            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }



        #region ------------------NOTIFIABLE MODEL ITEM: MessageText------------------------------

        // ItemName - MessageText  -  Ex: Client 
        // Contract - string  -  Ex: V_S8_Client_Vo

        string _MessageText = "";

        /// <summary>
        /// Description
        /// </summary>
        public string MessageText
        {
            get { return _MessageText; }
            set
            {
                _MessageText = value; //Field changing
                OnPropertyChanged(() => MessageText);
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: MessageText------------------------------

        DispatcherTimer CloseTimer
        { get; set; }



        #region ------------------NOTIFIABLE MODEL ITEM: CloseTimeText ------------------------------

        const string timeShowTemplate = "{0} (Min): {1} (Sec)";
        static int secondsToClose = 180;


        // ItemName - CloseTimeText   -  Ex: Client 
        // Contract - string  -  Ex: V_S8_Client_Vo
        string _CloseTimeText = timeShowTemplate.Fmt( 3.S(), (00).S());
        

        //3 (Min) : 00 (Sec)
        /// <summary>
        /// Time to close this window automatically
        /// </summary>
        public string CloseTimeText 
        {
            get { return _CloseTimeText ; }
            set
            {
                _CloseTimeText  = value; //Field changing
                OnPropertyChanged(() => CloseTimeText );
            }
        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: CloseTimeText ------------------------------


        public MessageWindow(string showMessageText)
        {
            InitializeComponent();
            DataContext = this;
            MessageText = showMessageText;

            CloseTimer = new DispatcherTimer(DispatcherPriority.Normal);
            CloseTimer.Interval = new TimeSpan(0, 0, 1);
            CloseTimer.Tick += CloseTimer_Tick;
            CloseTimer.Start();

        }

        private void CloseTimer_Tick(object sender, EventArgs e)
        {
            secondsToClose--;
            var timeNow = new TimeSpan(0,0,secondsToClose);
            var minsToClose = timeNow.Minutes;
            var secsToClose = timeNow.Seconds;
            CloseTimeText = timeShowTemplate.Fmt(minsToClose.S(), secsToClose.S());

            if (minsToClose == 0 && secsToClose == 0)
            {
                CloseTimer.Stop();
                CloseTimer.Tick -= CloseTimer_Tick;
                this.Close();
            }
        }
    }
}
