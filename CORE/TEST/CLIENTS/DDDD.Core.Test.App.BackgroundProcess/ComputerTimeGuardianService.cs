﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using DDDD.Core.UI.Windows;
 

namespace DDDD.Core.Test.App.WinService
{
    public partial class ComputerTimeGuardianService : ServiceBase
    {
        public ComputerTimeGuardianService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {

            var mesageWindow = new MessageWithTimerWindow("Времени до выключения осталось совсем чуть чуть.", "Осталось времени перед выключением: ");
            mesageWindow.ShowDialog();

        }

        protected override void OnStop()
        {
        }
    }
}
