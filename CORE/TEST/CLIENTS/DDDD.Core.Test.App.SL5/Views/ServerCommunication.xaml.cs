﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Navigation;


using System.ServiceModel;


using System.ServiceModel.Channels;
//using DDDD.Core.Test.CommandManager;
using DDDD.Core.Net.ServiceModel;
//using Castle.DynamicProxy;
//using Castle.DynamicProxy.Serialization;
//using Castle.DynamicProxy.Generators;
using System.Reflection.Emit;
//using Autofac;
using DDDD.BCL.Test.Web.DCManagers;
using DDDD.Core.HttpRoutes;
using DDDD.Core.App;
using System.Reflection;
using DDDD.Core.Net;
//using DDDD.BCL.Test.SL5.DynamicClientFullTest;

namespace DDDD.Core.Test.SL5
{
    public partial class ServerCommunication : Page
    {
        public ServerCommunication()
        {
            InitializeComponent();
        }

        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            // http://localhost:61272/DCSModuleService.svc
                     

        }

        private async void btn_LoadClientReport_Click(object sender, RoutedEventArgs e)
        {

            // SET @id_Client = '7670C94C-E82C-408E-A6E9-08F0CA7A757F'-- Code_Client - 14468
            var idClient = new Guid( "7670C94C-E82C-408E-A6E9-08F0CA7A757F");
            //await ClientReport_CommandManager.LoadClientReportData_Client(idClient);



        }









        private async void  btn_TestDynamicCreatedServiceClient_Click(object sender, RoutedEventArgs e)
        {
            HttpRoute2.StartUrl.BaseUrlSegmentsCountFromLocalPath = 0;

            var webApplication = HttpRoute2.StartUrl.WebApplication;
            //
            await  ClientReportManager.Current.LoadNewClientReport( DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());



        }


        #region ---------------------- DC MANAGER SCENARIO 2 ------------------------


        private async void btn_LoadClientReport_Click_1(object sender, RoutedEventArgs e)
        {
            // var getCurrentProperty = typeof(AppComponentsLoadManager).GetProperty("Current", BindingFlags.Static | BindingFlags.Public | BindingFlags.FlattenHierarchy);

            var result = await ClientReportManager.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }


        private async void btn_LoadClientReport2_Click(object sender, RoutedEventArgs e)
        {
            var result = await ClientReportManager2.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }


        private async void btn_LoadClientReport3_Click(object sender, RoutedEventArgs e)
        {
            var result = await ClientReportManager3.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }


        #endregion ---------------------- DC MANAGER SCENARIO 2 ------------------------



        #region ---------------------- DC MANAGER SCENARIO 2 ------------------------

        private async void btn2_LoadClientReport_Click(object sender, RoutedEventArgs e)
        {
            var result = await ClientReportManager.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());

        }

        private async void btn2_LoadClientReport2_Click(object sender, RoutedEventArgs e)
        {
            var result = await ClientReportManager2.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }

        private async void btn2_LoadClientReport3_Click(object sender, RoutedEventArgs e)
        {
            var result = await ClientReportManager3.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());

        }
        #endregion ---------------------- DC MANAGER SCENARIO 2 ------------------------

    }
}






//public static Type WcfServiceContract
//{ get; } = typeof(IClientReportService);


//static Binding DCSClientBinding
//{ get; } = DynamicCommandServiceClient2.CreateCustomBinding(sendTimeoutMins: 2, sendTimeoutSecs: 30, receiveTimeoutMins: 2, receiveTimeoutSecs: 30);


//static DynamicCommandServiceClient2 DCSClient
//{ get; } = GetDCSClient();


//static DynamicCommandServiceClient2 GetDCSClient()
//{
//    return DynamicCommandServiceClient2.AddOrUseExistSvcClient(
//            NewServiceKey:"TestClients"
//          , NewServiceNamespace:"contoso.com"
//          //,
//          //  WcfServiceContract
//          , serviceBinding:  DCSClientBinding
//          //, useTSSSerialization: true
//          ////, KnownTypesLoaderFunc: null 
//          //, routeTemplate: HttpRoutes.HttpRouteTemplateEn.HttpRoute_BaseurlWebappSubfldrService
//          //, ServiceFileName: "ClientReportService"
//          //, WebApplication: "DDDD.Core.Test.Web", Subfolder: "Services" //, Server: null, Port: null,
//         );
//}


////DynamicCommandServiceClient.AddOrUseExistSvcClient
//InterfaceProxyWithTargetInterfaceGenerator generator = new InterfaceProxyWithTargetInterfaceGenerator( new ModuleScope( savePhysicalAssembly: false), typeof(IDynamicCommandClientBase));

//var proxyGenerator = new ProxyGenerator();
//proxyGenerator. CreateInterfaceProxyWithTargetInterface<IDynamicCommandClientBase>()

//var options = new ProxyGenerationOptions
//{
//  //  BaseTypeForInterfaceProxy = typeof(IDynamicCommandClientBase)                 
//};
//var proxy = generator.GenerateCode(typeof(IDynamicCommandClientBase), Type.EmptyTypes,  options);
//var methodGenerator = new MethodGenerator();

//var newProxy = SerializeAndDeserialize(proxy);

//IClientChannel allowing the Abort and Close methods to be called.

//var cb = new ContainerBuilder();

//cb.RegisterType();

//cb.Register(c => new ChannelFactory(new BasicHttpBinding(), new EndpointAddress(TestServiceAddress)))
//.SingleInstance();

//cb.Register(c => c.Resolve > ().CreateChannel())
//.InterceptTransparentProxy(typeof(IClientChannel))
//.InterceptedBy(typeof(TestServiceInterceptor))
//.UseWcfSafeRelease();
