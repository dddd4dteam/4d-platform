﻿
//using DDDD.BCL;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Resources;
using System.Windows.Shapes;
//using Microsoft.Threading.Tasks;

namespace DDDD.Core.Test.SL5
{

    //public class TestsProgressReporter : ProgressReporter<TestsProgressCommandsEn>
    //{        
    //}


    public class TestReflectionClass
    {

        private String PrivateProperty
        {
            get;
            set;
        }
    }



    public struct TestReflectionStruct
    {

        private String PrivateProperty
        {
            get;
            set;
        }

        public String PublicProperty
        {
            get;
            set;
        }


    }


    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();

            //Reporter = new TestsProgressReporter();
            RegisterActionsOnProgressChangedCommand();//registering custom progress change on UI command actions 
            
            bsind_TestProcessIndicator.IsBusy = false;
            bsind_TestProcessIndicator.Visibility = System.Windows.Visibility.Collapsed;
        }


        /// <summary>
        /// UI Progress Change Action Reporter - reports UI to change indicators from your back-Task-Thread
        /// </summary>
        //public TestsProgressReporter Reporter
        //{
        //    get;
        //    private set;
        //}




        void RegisterActionsOnProgressChangedCommand( )
        {
            //Reporter.RegisterCommandAction(TestsProgressCommandsEn.StartProcessIndication,
            //    (prs) =>
            //    {
            //        bsind_TestProcessIndicator.Visibility = System.Windows.Visibility.Visible;
            //        bsind_TestProcessIndicator.IsBusy = true;
            //    }
            //    );
            //Reporter.RegisterCommandAction(TestsProgressCommandsEn.StopProcessIndication,
            //   (prs) =>
            //   {
            //       bsind_TestProcessIndicator.Visibility = System.Windows.Visibility.Collapsed;
            //       bsind_TestProcessIndicator.IsBusy = false;
            //   }
            //   );
            //Reporter.RegisterCommandAction(TestsProgressCommandsEn.ProgressMessage,
            //   (prs) =>
            //   {
            //       bsind_TestProcessIndicator.Content = prs.Message; 
            //   }
            //   );
            //Reporter.RegisterCommandAction(TestsProgressCommandsEn.ClearDetailMessages,
            //   (prs) =>
            //   {
            //       txt_Out.Text = "";
            //   }
            //   );
            //Reporter.RegisterCommandAction(TestsProgressCommandsEn.DetailMessage,
            //   (prs) =>
            //   {
            //       txt_Out.Text += prs.DetailMessage;
            //   }
            //   );
        }
                


        // Executes when the user navigates to this page.
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
             
        }




        
        private void btn_Test_Click(object sender, RoutedEventArgs e)
        {                      
            //Utility.StartFullTests(Reporter);
        }
    }
}