﻿using DDDD.Core.ServiceModel;
using System;
using System.Net;
using System.ServiceModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DDDD.Core.Net.ServiceModel
{



    //[ServiceContract(Name = ClientReportServiceIdentifier.ServiceKey, Namespace = ClientReportServiceIdentifier.ServiceNamespace)]
    //public interface IClientReportService
    //{
    //    /// <summary>
    //    /// Send Command Message to Server:
    //    /// <para/> Build  DCommandMessage by your client,on client side by your CommandManagersHub infrastructure.
    //    /// <para/> Set diagnostics timestamps on sending to server points.
    //    /// <para/> Send  DCommandMessage to Server.
    //    /// </summary>
    //    /// <param name="ClientCommand">Client side DCommandMessage</param>
    //    /// <param name="callback">Action on server return processed DCommandMessage</param>
    //    /// <param name="asyncState">Thread State object</param>
    //    /// <returns></returns>
    //    [OperationContract(AsyncPattern = true, Action = "contoso.crm.com/IClientReportService/ExecuteCommand", ReplyAction = "contoso.crm.com/IClientReportService/ExecuteCommandResponse")]
    //    [ServiceKnownType(nameof(ClientReportServiceIdentifier.GetClientReportKnownTypes), typeof(ClientReportServiceIdentifier))]
    //    IAsyncResult BeginExecuteCommand(DCommandMessage ClientCommand, AsyncCallback callback, object asyncState);

    //    DCommandMessage EndExecuteCommand(IAsyncResult result);
    //}


    public static class DCSConstants
    {
        public const string InitServiceName = nameof(InitServiceName);

        public const string InitServiceNamespace = nameof(InitServiceNamespace);

    }


    //static class DynamicKnownTypesManager
    [ServiceContract(Name = DCSConstants.InitServiceName, Namespace = DCSConstants.InitServiceNamespace)]
    public interface IDCClientBase
    {
        IAsyncResult BeginExecuteCommand(DCMessage ClientCommand, AsyncCallback callback, object asyncState);
        DCMessage EndExecuteCommand(IAsyncResult result);
    }









}
