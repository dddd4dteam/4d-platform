﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.BCL.Test.SL5.DynamicClientFullTest
{
    public class ServicesInitializer
    {
        public override void Start()
        {
            foreach (Type serviceType in this.ServiceTypeProvider.Types)
            {
                var binding = this.CreateBinding();
                var address = this.CreateEndpointAddress(serviceType);
                var endpointAddress = new EndpointAddress(address);

                var proxyCreatorType = this.MakeGenericType(serviceType);
                var proxyCreator = this.GetProxyCreator(proxyCreatorType, binding, endpointAddress);

                Type type = serviceType;
                this.kernel.Bind(serviceType).ToMethod(
                    ctx => this.proxyGenerator.CreateInterfaceProxyWithoutTarget(type, new[] { typeof(IContextChannel), }, this.CreateInterceptor(proxyCreator, type)));
            }
        }

        public override void Shutdown()
        {
            foreach (IChannelFactory channelFactory in this.proxyInstanceCreatorCache.Values)
            {
                channelFactory.Close();
            }
        }

        protected virtual IInterceptor CreateInterceptor(IChannelFactory proxyCreator, Type serviceType)
        {
            dynamic channelFactory = proxyCreator;
            return new ClientProxyInterceptor(() => (ICommunicationObject)channelFactory.CreateChannel(), serviceType);
        }

        protected virtual IChannelFactory CreateProxyInstanceCreator(Type genericProxyCreatorType, Binding binding, EndpointAddress endpointAddress)
        {
            IChannelFactory proxyInstanceCreator = (IChannelFactory)Activator.CreateInstance(genericProxyCreatorType, binding, endpointAddress);
            return proxyInstanceCreator;
        }

        private IChannelFactory GetProxyCreator(Type genericProxyCreatorType, Binding binding, EndpointAddress endpointAddress)
        {
            IChannelFactory proxyInstanceCreator;
            if (!this.proxyInstanceCreatorCache.TryGetValue(genericProxyCreatorType, out proxyInstanceCreator))
            {
                proxyInstanceCreator = this.CreateProxyInstanceCreator(genericProxyCreatorType, binding, endpointAddress);

                this.proxyInstanceCreatorCache.Add(genericProxyCreatorType, proxyInstanceCreator);
            }
            return proxyInstanceCreator;
        }

        private Type MakeGenericType(Type serviceType)
        {
            if (!serviceType.IsInterface)
            {
                throw new InvalidOperationException("Type returned from proxy type provider must not be concrete type!");
            }

            Type genericProxyType;
            if (!this.genericTypeCache.TryGetValue(serviceType, out genericProxyType))
            {
                genericProxyType = typeof(ChannelFactory<>).MakeGenericType(new[] { serviceType });

                this.genericTypeCache.Add(serviceType, genericProxyType);
            }

            return genericProxyType;
        }
    }
}
