﻿using Castle.DynamicProxy.Contributors;
using Castle.DynamicProxy.Generators;
using Castle.DynamicProxy.Generators.Emitters;
using DDDD.Core.Diagnostics;
using DDDD.Core.HttpRoutes;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Serialization;
using DDDD.Core.ServiceModel;
using DDDD.Core.Threading;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.NetworkInformation;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.BCL.Test.SL5.DynamicClientFullTest
{
    /// <summary>
    /// WCF Service  Client.
    /// DynamicCommandServiceClient2 also Allows to us unified TPL calls for every command or command loops.
    /// It also support UI Notification about Progress based on TPL  IProgress<T> pattern.      
    /// By default use/workflow you can Add/Remove these  Clients  by DynamicCommandServiceHub.
    /// If you want to change DCommandMessage Execution:  for your custom MessageContract/for another Notifications - you can create your own derived from this class. 
    /// That's the wrap on WCF  System.ServiceModel.ChannelFactory<TChannel> whose target - to let us using [Type ServiceContractType] - not as generic arg and in more flexible way.
    /// </summary>
    public class DynamicCommandServiceClient2 : IDisposable
    {

        #region  ----------------------------------------- CTOR -------------------------------------------

#if CLIENT && (SL5 || WP81)



        /// <summary>
        /// Create new  DynamicCommandServiceClient2 with using custom binding and endpoint manually.  Only for internal use by Hub/ or protected
        /// </summary>
        /// <param name="SvcContract">WCF Service Contract Type</param>
        /// <param name="serviceBinding"> Binding that this Serice support on Server </param>
        /// <param name="useTSSSerializer">switch to use  2 serialization mode - TSSSerializer + DataContractSerializer</param>
        /// <param name="KnownTypesLoaderFunc">Func that returns known types-contract that will init serialization</param>
        /// <param name="routeTemplate">Http adress building template. You can choose from predefined templates set of HttpRouteTemplateEn</param>
        /// <param name="ServiceFileName">Http adress Template Part -[service]. Without [*.svc]  extension. </param>
        /// <param name="Server">Http adress Template Part -[server]</param>
        /// <param name="Port">Http adress Template Part -[port]</param>
        /// <param name="WebApplication">Http adress Template Part -[WebApplication]</param>
        /// <param name="Subfolder">Http adress Template Part -[Subfolder]</param>
        protected internal DynamicCommandServiceClient2(Type SvcContract, Binding serviceBinding,
                                              bool useTSSSerializer = false,
                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null,
                                              HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlService,
                                              String ServiceFileName = null, String Server = null, String Port = null, String WebApplication = null, String Subfolder = null)
        {

            Initialize(SvcContract, serviceBinding

                        , new EndpointAddress(HttpRouteClient.BuildServiceUri(routeTemplate, ServiceFileName, Server, Port,
                                                                                    WebApplication, Subfolder)
                                      )
                         , KnownTypesLoaderFunc,
                         useTSSSerializer
                       );

        }


        /// <summary>
        /// Create new  DynamicCommandServiceClient2 with using endpoint configuration.  Only for internal use by Hub/ or protected
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <param name="useTSSSerializer"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        protected internal DynamicCommandServiceClient2(Type SvcContract, String endpointConfigurationName,
                                              bool useTSSSerializer = false,
                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
                                             )
        {

            Initialize(SvcContract //serviceBinding                        
                        , endpointConfigurationName
                        , KnownTypesLoaderFunc
                        , useTSSSerializer
                       );

        }

#elif CLIENT && WPF


        /// <summary>
        /// Create new  DynamicCommandServiceClient2 with using custom binding and endpoint manually.  Only for internal use by Hub/ or protected
        /// </summary>
        /// <param name="SvcContract"> WCF Service Contract Type</param>
        /// <param name="serviceBinding"> Binding that this Serice support on Server </param>
        /// <param name="useTSSSerializer"> switch to use  2 serialization mode - SDSerializer + DataContractSerializer </param>
        /// <param name="useTSSDirectSerialization">switch to use  3 serialization mode -  Direct SDSerializer(XmlSDS) </param>
        /// <param name="KnownTypesLoaderFunc">Func that returns known types-contract that will init serialization</param>
        /// <param name="routeTemplate">Http adress building template. You can choose from predefined templates set of HttpRouteTemplateEn</param>
        /// <param name="ServiceFileName">Http adress Template Part -[service]. Without [*.svc]  extension.</param>
        /// <param name="Server">Http adress Template Part -[server]</param>
        /// <param name="Port">Http adress Template Part -[port]</param>
        /// <param name="WebApplication">Http adress Template Part -[WebApplication]</param>
        /// <param name="Subfolder">Http adress Template Part -[Subfolder]</param>
        protected internal DynamicCommandServiceClient2(Type SvcContract, Binding serviceBinding,
                                              bool useTSSSerializer = false,
                                              bool useTSSDirectSerialization = false,
                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null,
                                              HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlService,
                                              string ServiceFileName = null, string Server = null, string Port = null, string WebApplication = null, String Subfolder = null)
        {

            Initialize(SvcContract,
                         serviceBinding
                        , new EndpointAddress(HttpRouteClient.BuildServiceUri(routeTemplate, ServiceFileName, Server, Port,
                                                                                    WebApplication, Subfolder)
                                      )
                        , KnownTypesLoaderFunc
                        , useTSSSerializer
                        , useTSSDirectSerialization
                       );

        }



        /// <summary>
        /// Create new  DynamicCommandServiceClient2 with using endpoint configuration.  Only for internal use by Hub/ or protected
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <param name="useTSSSerializer"></param>
        /// <param name="useTSSDirectSerialization"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        protected internal DynamicCommandServiceClient2(Type SvcContract, string endpointConfigurationName,
                                              bool useTSSSerializer = false,
                                              bool useTSSDirectSerialization = false,
                                              Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
                                             )
        {

            Initialize(SvcContract
                        , endpointConfigurationName
                        , KnownTypesLoaderFunc
                        , useTSSSerializer
                        , useTSSDirectSerialization
                       );

        }




#endif


        #endregion ----------------------------------------- CTOR -------------------------------------------


        #region ------------------------------------------ IDisposable ------------------------------------------

        public void Dispose()
        {
            //remove SDSerializer from SDShub
            TypeSetSerializer.RemoveSerializer(ServiceKey);

            if (Factory.State != CommunicationState.Closed)
            {
                Factory.Close();
            }

        }

        #endregion ------------------------------------------ IDisposable ------------------------------------------




        #region --------------------------------- SERVICE MODEL -----------------------------------

        static Dictionary<string, DynamicCommandServiceClient2> dynamicCommandClientServices = new Dictionary<string, DynamicCommandServiceClient2>();


        #region --------------------------------- SERVICE CONTRACT CHECKS -----------------------------------


        static bool CheckIsSvcContract(Type serviceType)
        {
            var scontract = serviceType.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
            Validator.AssertTrue<InvalidCastException>(scontract == null,
                String.Format("serviceType - [{0}]  that you are going to use as ServiceContract  must have valid ServiceContractAttribute ", serviceType.FullName));

            return true;
        }


        static bool CheckIsSvcContractSupportDynamicCommands(Type serviceType)
        {

#if SL5 || WP81

            var mthdBeginExist = serviceType.GetMethod("BeginExecuteCommand");
            var mthdEndExist = serviceType.GetMethod("EndExecuteCommand");

            Validator.AssertFalse<InvalidCastException>((mthdBeginExist != null && mthdEndExist != null),
                string.Format("serviceType - [{0}] it doesnt have one or both methods BeginExecuteCommand and EndExecuteComand ", serviceType.FullName));

#elif WPF
            var mthdExist = serviceType.GetMethod("ExecuteCommand");

            Validator.AssertFalse<InvalidCastException>((mthdExist != null),
               string.Format("serviceType - [{0}] it doesnt have method ExecuteCommand", serviceType.FullName));

#endif

            return true;
        }


        #endregion --------------------------------- SERVICE CONTRACT CHECKS -----------------------------------




#if CLIENT && (SL5 || WP81)



        /// <summary>
        /// Add new  WCF DynamicCommandServiceClient2 service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="serviceBinding"></param>
        /// <param name="useTSSSerialization"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        /// <param name="routeTemplate"></param>
        /// <param name="ServiceFileName"> {serviceFileName}.svc -it's used in Uri Building </param>
        /// <param name="Server"></param>
        /// <param name="Port"></param>
        /// <param name="WebApplication"></param>
        /// <param name="Subfolder"></param>
        /// <param name="ActionName"></param>
        /// <returns></returns>
        public static DynamicCommandServiceClient2 AddOrUseExistSvcClient(Type SvcContract,
                                            Binding serviceBinding,
                                            bool useTSSSerialization = false,
                                            Func<IEnumerable<Type>> KnownTypesLoaderFunc = null,
                                            HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlSubfldrService,
                                            string ServiceFileName = null, string Server = null, string Port = null, string WebApplication = null, string Subfolder = null, string ActionName = null)
        {
            CheckIsSvcContract(SvcContract);

            var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
                                         .FirstOrDefault() as ServiceContractAttribute;
            string ServiceKey = svccontract.Name;

            //return if client already exist
            if (dynamicCommandClientServices.ContainsKey(ServiceKey)) return dynamicCommandClientServices[ServiceKey];

            //if not exist check for methods ExecuteCommand
            CheckIsSvcContractSupportDynamicCommands(SvcContract);

            if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
            {
                lock (_locker)
                {
                    if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
                    {

                        dynamicCommandClientServices.Add(ServiceKey,
                                                        new DynamicCommandServiceClient2(
                                                              SvcContract
                                                             , serviceBinding
                                                             , useTSSSerialization                 //useTSSSerializer on dataTransmission
                                                             , KnownTypesLoaderFunc //load now KnownTypes 
                                                             , routeTemplate
                                                             , ServiceFileName, Server, Port, WebApplication, Subfolder));

                        return dynamicCommandClientServices[ServiceKey];
                    }
                }
            }

            return dynamicCommandClientServices[ServiceKey];
        }



        /// <summary>
        /// Add new  WCF DynamicCommandServiceClient2 service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name key. 
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <param name="useTSSSerialization"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        /// <returns></returns>
        public static DynamicCommandServiceClient2 AddOrUseExistSvcClient(Type SvcContract,
                                            string endpointConfigurationName,
                                            bool useTSSSerialization = false,
                                            Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
                                          )
        {
            CheckIsSvcContract(SvcContract);

            var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
                                         .FirstOrDefault() as ServiceContractAttribute;
            string ServiceKey = svccontract.Name;

            //return if client already exist
            if (dynamicCommandClientServices.ContainsKey(ServiceKey)) return dynamicCommandClientServices[ServiceKey];

            //if not exist check for methods ExecuteCommand
            CheckIsSvcContractSupportDynamicCommands(SvcContract);

            if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
            {
                lock (_locker)
                {
                    if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
                    {

                        dynamicCommandClientServices.Add(ServiceKey,
                                                        new DynamicCommandServiceClient2(
                                                              SvcContract
                                                             , endpointConfigurationName
                                                             , useTSSSerialization                 // useTSSSerializer on dataTransmission
                                                             , KnownTypesLoaderFunc //load now KnownTypes 
                                                             ));

                        return dynamicCommandClientServices[ServiceKey];
                    }
                }
            }

            return dynamicCommandClientServices[ServiceKey];

        }





        public static DynamicCommandServiceClient2 AddOrUseExistSvcClient(
                                            string NewServiceKey
                                          , string NewServiceNamespace
                                          ,  Binding serviceBinding
                                          ,  bool useTSSSerialization = false
                                          ,  Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
                                          ,  HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlSubfldrService
                                          ,  string ServiceFileName = null, string Server = null, string Port = null, string WebApplication = null, string Subfolder = null, string ActionName = null
                                          )
        {

            var startSvcContract = typeof(IDynamicCommandClientBase);

            CheckIsSvcContract( startSvcContract);

            var svccontract = startSvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
                                         .FirstOrDefault() as ServiceContractAttribute;
            string ServiceKey = NewServiceKey;

            //return if client already exist
            if (dynamicCommandClientServices.ContainsKey(ServiceKey)) return dynamicCommandClientServices[ServiceKey];

            //if not exist check for methods ExecuteCommand
            CheckIsSvcContractSupportDynamicCommands(startSvcContract);

            if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
            {
                lock (_locker)
                {
                    if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
                    {

                        dynamicCommandClientServices.Add(ServiceKey,
                                                        new DynamicCommandServiceClient2(
                                                              startSvcContract
                                                             , null
                                                             , useTSSSerialization                 // useTSSSerializer on dataTransmission
                                                             , KnownTypesLoaderFunc //load now KnownTypes 
                                                             ));

                        return dynamicCommandClientServices[ServiceKey];
                    }
                }
            }

            return dynamicCommandClientServices[ServiceKey];

        }


#elif CLIENT && WPF





        /// <summary>
        /// Add new  WCF DynamicCommandServiceClient2 service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name of SvcContract type as KEY. 
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="serviceBinding"></param>
        /// <param name="useTSSSerialization"></param>
        /// <param name="useTSSDirectSerialization"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        /// <param name="routeTemplate"></param>
        /// <param name="ServiceFileName"> {serviceFileName}.svc -it's used in Uri Building </param>
        /// <param name="Server"></param>
        /// <param name="Port"></param>
        /// <param name="WebApplication"></param>
        /// <param name="Subfolder"></param>
        /// <param name="ActionName"></param>
        /// <returns></returns>
        public static DynamicCommandServiceClient2 AddOrUseExistSvcClient(Type SvcContract,
                                            Binding serviceBinding,
                                            bool useTSSSerialization = false,
                                            bool useTSSDirectSerialization = false,
                                            Func<IEnumerable<Type>> KnownTypesLoaderFunc = null,
                                           HttpRouteTemplateEn routeTemplate = HttpRouteTemplateEn.HttpRoute_BaseurlSubfldrService,
                                           String ServiceFileName = null, String Server = null, String Port = null, String WebApplication = null, String Subfolder = null, String ActionName = null)
        {
            CheckIsSvcContract(SvcContract);

            var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
                                         .FirstOrDefault() as ServiceContractAttribute;
            string ServiceKey = svccontract.Name;

            //return if client already exist
            if (dynamicCommandClientServices.ContainsKey(ServiceKey)) return dynamicCommandClientServices[ServiceKey];

            //if not exist check for methods ExecuteCommand
            CheckIsSvcContractSupportDynamicCommands(SvcContract);

            if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
            {
                lock (_locker)
                {
                    if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
                    {

                        dynamicCommandClientServices.Add(ServiceKey,
                                                        new DynamicCommandServiceClient2(
                                                              SvcContract
                                                             , serviceBinding
                                                             , useTSSSerialization                   //2 mode seriaization
                                                             , useTSSDirectSerialization            //3 mode serialization    
                                                             , KnownTypesLoaderFunc //load now KnownTypes 
                                                             , routeTemplate
                                                             , ServiceFileName, Server, Port, WebApplication, Subfolder));

                        return dynamicCommandClientServices[ServiceKey];
                    }
                }
            }

            return dynamicCommandClientServices[ServiceKey];

        }



        /// <summary>
        /// Add new  WCF DynamicCommandServiceClient2 service into internal static Dictionary.
        /// <para/>  If client with such ServiceName already exist it'll be returned without new instance creation.
        /// <para/>  Also you can get this client next time by its ServiceContract.Name of SvcContract type as KEY. 
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>
        /// <param name="useTSSSerialization"></param>
        /// <param name="useTSSDirectSerialization"></param>
        /// <param name="KnownTypesLoaderFunc"></param>
        /// <returns></returns>
        public static DynamicCommandServiceClient2 AddOrUseExistSvcClient(Type SvcContract,
                                            String endpointConfigurationName,
                                            bool useTSSSerialization = false,
                                            bool useTSSDirectSerialization = false,
                                            Func<IEnumerable<Type>> KnownTypesLoaderFunc = null
                                          )
        {
            CheckIsSvcContract(SvcContract);

            var svccontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false)
                                         .FirstOrDefault() as ServiceContractAttribute;
            string ServiceKey = svccontract.Name;

            //return if client already exist
            if (dynamicCommandClientServices.ContainsKey(ServiceKey)) return dynamicCommandClientServices[ServiceKey];

            //if not exist check for methods ExecuteCommand
            CheckIsSvcContractSupportDynamicCommands(SvcContract);

            if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
            {
                lock (_locker)
                {
                    if (!dynamicCommandClientServices.ContainsKey(ServiceKey))
                    {

                        dynamicCommandClientServices.Add(ServiceKey,
                                                        new DynamicCommandServiceClient2(
                                                              SvcContract
                                                             , endpointConfigurationName
                                                             , useTSSSerialization               //2 mode seriaization
                                                             , useTSSDirectSerialization        //3 mode seriaization                                                             
                                                             , KnownTypesLoaderFunc //load now KnownTypes 
                                                             ));

                        return dynamicCommandClientServices[ServiceKey];
                    }
                }
            }

            return dynamicCommandClientServices[ServiceKey];

        }
#endif





        /// <summary>
        /// Remove  Service client from Hub by its service name 
        /// </summary>
        /// <param name="ServiceName">Name that you use on Creating this service</param>
        public static void RemoveServiceClient(String ServiceName)
        {
            if (String.IsNullOrEmpty(ServiceName)) return;

            if (!dynamicCommandClientServices.ContainsKey(ServiceName)) return;

            lock (_locker)
            {
                if (dynamicCommandClientServices.ContainsKey(ServiceName))
                    dynamicCommandClientServices.Remove(ServiceName);
            }

        }



#if CLIENT  && (WPF || SL5)

        /// <summary>
        /// Creating Custom Binding for WCF DynamicCommandServiceClient2 service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="transferMode"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , TransferMode transferMode = TransferMode.StreamedResponse
                                                 , Int32 maxBufferSize = Int32.MaxValue, Int32 maxReceivedMessageSize = Int32.MaxValue
                                                , Int32 sendTimeoutMins = 0, Int32 sendTimeoutSecs = 30
                                                , Int32 receiveTimeoutMins = 0, Int32 receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,
            });


            CustomBinding binding = new CustomBinding(elements);


            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);// 
            return binding;
        }

#elif CLIENT && WP81

        /// <summary>
        /// Creating Custom Binding for WCF DynamicCommandServiceClient2 service
        /// </summary>
        /// <param name="UseBinaryMessageEncoding"></param>
        /// <param name="maxBufferSize"></param>
        /// <param name="maxReceivedMessageSize"></param>
        /// <param name="sendTimeoutMins"></param>
        /// <param name="sendTimeoutSecs"></param>
        /// <param name="receiveTimeoutMins"></param>
        /// <param name="receiveTimeoutSecs"></param>
        /// <returns></returns>
        public static Binding CreateCustomBinding(bool UseBinaryMessageEncoding = true
                                                 , Int32 maxBufferSize = Int32.MaxValue, Int32 maxReceivedMessageSize = Int32.MaxValue
                                                , Int32 sendTimeoutMins = 0, Int32 sendTimeoutSecs = 30
                                                , Int32 receiveTimeoutMins = 0, Int32 receiveTimeoutSecs = 30)
        {
            var elements = new List<BindingElement>();

            if (UseBinaryMessageEncoding)
                elements.Add(new BinaryMessageEncodingBindingElement());

            elements.Add(new HttpTransportBindingElement()
            {
                //TransferMode = transferMode,
                MaxBufferSize = maxBufferSize,
                MaxReceivedMessageSize = maxReceivedMessageSize,
            });


            CustomBinding binding = new CustomBinding(elements);


            binding.SendTimeout = new TimeSpan(0, sendTimeoutMins, sendTimeoutSecs);// 
            binding.ReceiveTimeout = new TimeSpan(0, receiveTimeoutMins, receiveTimeoutSecs);// 
            return binding;
        }
      
#endif

        #endregion --------------------------------- SERVICE MODEL -----------------------------------



        #region --------------------------------------- NESTED TYPES ----------------------------------------

        /// <summary>
        /// Constant values of DynamicCommandServiceClient2  infrastructure
        /// </summary>
        protected internal static class DSConst
        {

            public const string LoadKnownContractsMethodName = "LoadKnownContracts";

            public const string DSOperation_ExecuteCommand = "ExecuteCommand";

            public const string Server_Default_Error_Message = "Server error happened.";

#if CLIENT && (SL5 || WP81)

            public const string Method_BeginExecuteCommand = "BeginExecuteCommand";


            public const string Method_EndExecuteCommand = "EndExecuteCommand";


            public static Type BeginExecuteCommandFuncType = typeof(Func<DCMessage, AsyncCallback, object, IAsyncResult>);


            public static Type EndExecuteCommandFuncType = typeof(Func<IAsyncResult, DCMessage>);




            public static Func<DCMessage, AsyncCallback, object, IAsyncResult> BuildBeginExecuteCommandFunc(ICommunicationObject channel)
            {
                try
                {
                    return channel.GetType().GetMethod(Method_BeginExecuteCommand)
                                 .CreateDelegate(BeginExecuteCommandFuncType, channel)
                                  as Func<DCMessage, AsyncCallback, object, IAsyncResult>;


                }
                catch (Exception)
                {
                    throw;
                }
            }


            public static Func<IAsyncResult, DCMessage> BuildEndExecuteCommandFunc(ICommunicationObject channel)
            {
                try
                {
                    return

                        channel.GetType().GetMethod(Method_EndExecuteCommand)
                                .CreateDelegate(EndExecuteCommandFuncType, channel)
                                  as Func<IAsyncResult, DCMessage>;

                }
                catch (Exception)
                {
                    throw;
                }
            }
#endif

#if CLIENT && WPF

            public const string Method_ExecuteCommand = "ExecuteCommand";


            public const string Method_ExecuteCommandAsync = "ExecuteCommandAsync";


            public static Type ExecuteCommandFuncType = typeof(Func<DCommandMessage, DCommandMessage>);


            public static Type ExecuteCommandAsyncFuncType = typeof(Func<DCommandMessage, Task<DCommandMessage>>);


#endif



            public static Func<Binding, EndpointAddress, ChannelFactory> GetCreateChannelFactoryDelegate(Type factoryContract)
            {
                try
                {
                    var ctor = factoryContract.GetConstructor(new Type[] { typeof(Binding), typeof(EndpointAddress) });

                    ParameterExpression paramBinding = Expression.Parameter(typeof(Binding));
                    ParameterExpression paramEndpoint = Expression.Parameter(typeof(EndpointAddress));

                    var expr = Expression.Lambda<Func<Binding, EndpointAddress, ChannelFactory>>(
                                                 Expression.Convert(
                                                            Expression.New(ctor, paramBinding, paramEndpoint), typeof(ChannelFactory)),

                                            paramBinding, paramEndpoint);


                    return expr.Compile();
                }
                catch (Exception)
                {
                    throw;
                }
            }

            public static Func<Binding, EndpointAddress, ChannelFactory> GetCreateChannelFactoryFromConfigDelegate(Type factoryContract)
            {
                try
                {
                    var ctor = factoryContract.GetConstructor(new Type[] { typeof(String) });

                    ParameterExpression paramConfigName = Expression.Parameter(typeof(String));


                    var expr = Expression.Lambda<Func<Binding, EndpointAddress, ChannelFactory>>(
                                                 Expression.Convert(
                                                            Expression.New(ctor, paramConfigName), typeof(ChannelFactory)),

                                            paramConfigName);


                    return expr.Compile();
                }
                catch (Exception)
                {
                    throw;
                }
            }





            /// <summary>
            /// Creating delegate of Channel  of this Client's Factory instance
            /// </summary>
            /// <param name="FactoryInstance"></param>
            /// <returns></returns>
            public static Func<ICommunicationObject> GetCreateChannelDelegate(object FactoryInstance)
            {
                try
                {
                    var tp = FactoryInstance.GetType();

                    MethodInfo createChannel = tp.GetMethod("CreateChannel", BindingFlags.Public | BindingFlags.Instance, null, Type.EmptyTypes, null);

                    ConstantExpression instance = Expression.Constant(FactoryInstance, typeof(ChannelFactory));
                    //ParameterExpression instance = Expression. Parameter(typeof(ChannelFactory), "instance");

                    Expression block = Expression.Block(
                                                          Expression.Convert(
                                                                 Expression.Call(
                                                                        Expression.Convert(instance, tp), createChannel)
                                                                            , typeof(ICommunicationObject)
                                                                            )
                                                       //, instance
                                                       );

                    var expr = Expression.Lambda<Func<ICommunicationObject>>(block); //ChannelFactory,

                    return expr.Compile();
                }
                catch (Exception)
                {
                    throw;
                }

            }

        }

        #endregion --------------------------------------- NESTED TYPES ----------------------------------------



        #region ------------------------------------- FIELDS ----------------------------------------

        private static readonly object _locker = new object();

        /// <summary>
        /// Create  ChannelFactory of <ServiceContract> 
        /// </summary>
        protected Func<Binding, EndpointAddress, ChannelFactory> CreateChannelFactoryDelegate;//ChannelFactory

        /// <summary>
        /// Create New Channel from current Factory Function  
        /// </summary>
        protected Func<ICommunicationObject> CreateChannelDelegate; //Chanel   ChannelFactory,


        #endregion ------------------------------------- FIELDS ----------------------------------------



        #region --------------------------------------  INITIALIZE ---------------------------------------

#if CLIENT && (SL5 || WP81)


        /// <summary>
        /// Initializing new DynamicCommandServiceClient
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="serviceBinding"></param>
        /// <param name="serviceAddress"></param>
        /// <param name="KnowTypesLoaderFunc"></param>
        /// <param name="useTSSSerializer"></param>
        void Initialize(Type SvcContract,
                         System.ServiceModel.Channels.Binding serviceBinding,
                         EndpointAddress serviceAddress,
                         Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
                         bool useTSSSerializer = false
                        )
        {
            try
            {
                ServiceContract = SvcContract;

                var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
                ServiceKey = scontract.Name;
                ServiceNamespace = scontract.Namespace;

                ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);



                ServiceBinding = serviceBinding;

                ServiceAddress = serviceAddress;

                CreateChannelFactoryDelegate = DSConst.GetCreateChannelFactoryDelegate(ServiceChannelFactoryContract);

                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

                DCSEndpoint = GetDSEndpoint(Factory);

                CreateChannelDelegate = DSConst.GetCreateChannelDelegate(Factory);

                //addKnownTypes from CustomLoader to ExecuteCommand Operation and set SDSSerialization if enable
                LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer);


            }
            catch (Exception)
            {
                throw;
            }
        }



        /// <summary>
        /// Initializing new DynamicCommandServiceClient2 from Configuration
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>        
        /// <param name="KnowTypesLoaderFunc"></param>
        /// <param name="useTSSSerializer"></param>
        void Initialize(Type SvcContract,
                       String endpointConfigurationName,
                       Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
                       bool useTSSSerializer = false
                      )
        {
            try
            {
                ServiceContract = SvcContract;

                var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
                ServiceKey = scontract.Name;
                ServiceNamespace = scontract.Namespace;

                ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

                CreateChannelFactoryDelegate = DSConst.GetCreateChannelFactoryFromConfigDelegate(ServiceChannelFactoryContract);

                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

                ServiceBinding = Factory.Endpoint.Binding;// serviceBinding;

                ServiceAddress = Factory.Endpoint.Address;// serviceAddress;

                DCSEndpoint = GetDSEndpoint(Factory);

                 //DCSEndpoint.Binding = new Binding().


                CreateChannelDelegate = DSConst.GetCreateChannelDelegate(Factory);

                //addKnownTypes from CustomLoader to ExecuteCommand Operation and set SDSSerialization if enable
                LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer);


            }
            catch (Exception)
            {
                throw;
            }
        }


        void Initialize(
                      string NewServiceKey
                     , string NewNamespace
                      ,Type SvcContract
                      ,String endpointConfigurationName,
                      Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
                      bool useTSSSerializer = false
                     )
        {
            try
            {
                ServiceContract = SvcContract;

                var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
                ServiceKey = NewServiceKey;
                ServiceNamespace = NewNamespace;

                EmitTargetClientContract(NewServiceKey, NewNamespace);

                ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

                CreateChannelFactoryDelegate = DSConst.GetCreateChannelFactoryFromConfigDelegate(ServiceChannelFactoryContract);

                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

                ServiceBinding = Factory.Endpoint.Binding;// serviceBinding;

                ServiceAddress = Factory.Endpoint.Address;// serviceAddress;

                DCSEndpoint = GetDSEndpoint(Factory);

                //DCSEndpoint.Binding = new Binding().


                CreateChannelDelegate = DSConst.GetCreateChannelDelegate(Factory);

                //addKnownTypes from CustomLoader to ExecuteCommand Operation and set SDSSerialization if enable
                LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer);


            }
            catch (Exception)
            {
                throw;
            }
        }

        private void EmitTargetClientContract(string newServiceKey, string newNamespace)
        {
            InterfaceProxyTargetContributor methGenerator = new InterfaceProxyTargetContributor( typeof(IDynamicCommandClientBase), false, new NamingScope() { }  );
            
            
           
        }



#elif CLIENT && WPF


        /// <summary>
        /// Initializing new DynamicCommandServiceClient
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="serviceBinding"></param>
        /// <param name="serviceAddress"></param>
        /// <param name="KnowTypesLoaderFunc"></param>
        /// <param name="useTSSSerializer"></param>
        /// <param name="useTSSDirectSerialization"></param>
        void Initialize(Type SvcContract,
                         Binding serviceBinding,
                         EndpointAddress serviceAddress,
                         Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
                         bool useTSSSerializer = false,
                         bool useTSSDirectSerialization = false
                        )
        {
            try
            {
                ServiceContract = SvcContract;

                var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
                ServiceKey = scontract.Name;
                ServiceNamespace = scontract.Namespace;

                ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

                ServiceBinding = serviceBinding;

                ServiceAddress = serviceAddress;

                CreateChannelFactoryDelegate = DSConst.GetCreateChannelFactoryDelegate(ServiceChannelFactoryContract);

                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);

                DCSEndpoint = GetDSEndpoint(Factory);

                CreateChannelDelegate = DSConst.GetCreateChannelDelegate( Factory); //Factory.CreateChannel

                //addKnownTypes from CustomLoader to ExecuteCommand Operation and set SDSSerialization if enable
                LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer, useTSSDirectSerialization);


            }
            catch (Exception)
            {
                throw;
            }
        }



        /// <summary>
        /// Initializing new DynamicCommandServiceClient2 from Configuration
        /// </summary>
        /// <param name="SvcContract"></param>
        /// <param name="endpointConfigurationName"></param>        
        /// <param name="KnowTypesLoaderFunc"></param>
        /// <param name="useTSSSerializer"></param>
        /// <param name="useTSSDirectSerialization"></param>
        void Initialize(Type SvcContract,
                       String endpointConfigurationName,
                       Func<IEnumerable<Type>> KnowTypesLoaderFunc = null,
                       bool useTSSSerializer = false,
                       bool useTSSDirectSerialization = false
                      )
        {
            try
            {
                ServiceContract = SvcContract;

                var scontract = SvcContract.GetCustomAttributes(typeof(ServiceContractAttribute), false).FirstOrDefault() as ServiceContractAttribute;
                ServiceKey = scontract.Name;
                ServiceNamespace = scontract.Namespace;

                ServiceChannelFactoryContract = typeof(ChannelFactory<>).MakeGenericType(ServiceContract);

                CreateChannelFactoryDelegate = DSConst.GetCreateChannelFactoryFromConfigDelegate(ServiceChannelFactoryContract);

                Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);
                //var itm = Factory as ChannelFactory<>;

                ServiceBinding = Factory.Endpoint.Binding;// serviceBinding;

                ServiceAddress = Factory.Endpoint.Address;// serviceAddress;

                DCSEndpoint = GetDSEndpoint(Factory);

                CreateChannelDelegate = DSConst.GetCreateChannelDelegate(Factory);

                //addKnownTypes from CustomLoader to ExecuteCommand Operation and set SDSSerialization if enable
                LoadKnowTypesFromKnowTypesLoader(KnowTypesLoaderFunc, useTSSSerializer, useTSSDirectSerialization);


            }
            catch (Exception)
            {
                throw;
            }
        }

#endif





        /// <summary>
        /// Simply return by reflection ServiceEndpoint from Factory Instance
        /// </summary>
        /// <param name="factory"></param>
        /// <returns></returns>
        protected static ServiceEndpoint GetDSEndpoint(ChannelFactory factory)
        {
            try
            {
                var props = factory.GetType().GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);
                var endpProp = props.Where(prp => prp.Name == "Endpoint").FirstOrDefault();

                return (ServiceEndpoint)endpProp.GetValue(factory, BindingFlags.NonPublic, null, null, null);

            }
            catch (Exception)
            {
                throw;
            }
        }



        #endregion -------------------------------------  INITIALIZE ---------------------------------------



        #region  ---------------------------------- PROPERTIES --------------------------------------


        /// <summary>
        /// ServiceContract of current DynamicCommandServiceClient2 
        /// </summary>
        public Type ServiceContract { get; protected set; }

        /// <summary>
        /// Service Name from ServiceContract attribute. By this key -   
        /// 1- this DynamicCommandServiceClient2 exist in DynamicCommandServiceHub - it means that you can get it back from DynamicCommandServiceHub by this key
        /// 2 SDSerializer will be added for this Client if  we  [UseSDSerializer].
        /// </summary>
        public String ServiceKey { get; protected set; }

        /// <summary>
        /// Namespace of  ServiceContract interface
        /// </summary>
        public String ServiceNamespace { get; protected set; }

        /// <summary>
        /// ChannelFactory<ServiceContract> type
        /// </summary>
        public Type ServiceChannelFactoryContract { get; protected set; }

        /// <summary>
        ///  ChannelFactory of <ServiceContract>   of current DynamicCommandServiceClient
        /// </summary>
        protected ChannelFactory Factory { get; set; }




        /// <summary>
        /// Service Binding that you add on current Client creation
        /// </summary>
        public System.ServiceModel.Channels.Binding ServiceBinding { get; protected set; }


        /// <summary>
        /// Current  Client Endpoint Address
        /// </summary>
        public EndpointAddress ServiceAddress { get; protected set; }


        /// <summary>
        /// Current  Client Endpoint 
        /// </summary>
        protected ServiceEndpoint DCSEndpoint { get; set; }

        /// <summary>
        /// ExecuteCommand Operation Description of from ServiceContract
        /// </summary>
        protected OperationDescription ExecuteCommandOperation
        {
            get
            {
                return DCSEndpoint.Contract.Operations.Where(op => op.Name == DSConst.DSOperation_ExecuteCommand).FirstOrDefault();
            }
        }






        /// <summary>
        /// Client's SDSerializer.
        /// Only if flag UseSDSerializer  is  true  then we"ll create  ClientsSDSerializer.
        /// ClientsSDSerializer will be stored  in SDSHub until you delete it by hands( it may cause error in next call time, don't do it),
        /// or when you remove this Client from DynamicCommandServiceHub
        /// </summary>
        public ITypeSetSerializer TSSerializer { get; protected set; }



        #endregion ---------------------------------- PROPERTIES --------------------------------------



        #region ---------------------------------- EMIT HELPERS ---------------------------------------


        private static Func<TOut> CreateCtorDelegate<TOut>(ConstructorInfo ctor)
        {

            Expression<Func<TOut>> expr =
                Expression.Lambda<Func<TOut>>(
                        Expression.New(ctor),
                        null
                        );

            return expr.Compile();
        }

        private static Func<TParam1, TOut> CreateCtorDelegate<TOut, TParam1>(ConstructorInfo ctor)
        {
            ParameterExpression param1 = Expression.Parameter(typeof(TParam1), "param1");

            Expression<Func<TParam1, TOut>> expr =
                Expression.Lambda<Func<TParam1, TOut>>(
                        Expression.New(ctor),
                        param1
                        );

            return expr.Compile();
        }

        private static Func<TParam1, TParam2, TOut> CreateCtorDelegate<TParam1, TParam2, TOut>(ConstructorInfo ctor)
        {
            ParameterExpression param1 = Expression.Parameter(typeof(TParam1), "param1");
            ParameterExpression param2 = Expression.Parameter(typeof(TParam2), "param2");

            Expression<Func<TParam1, TParam2, TOut>> expr =
                Expression.Lambda<Func<TParam1, TParam2, TOut>>(
                        Expression.New(ctor),
                        param1, param2
                        );

            return expr.Compile();
        }




        private static Func<TOut> CreateMethodCallOnInstanceDelegate<TOut>(object instance, string MethodName, BindingFlags binding = BindingFlags.Default)
        {
            try
            {
                //Func<ICommunicationObject, Delegate>
                if (instance == null) throw new ArgumentNullException("instance");


                var tp = instance.GetType();// channelContract;

                MethodInfo methodEnd = tp.GetMethod(MethodName, binding);
                if (methodEnd == null) throw new ArgumentNullException(MethodName);

                // Delegate.CreateDelegate(delegateType, mi)
                var delegateout = (Func<TOut>)Delegate.CreateDelegate(typeof(Func<TOut>), methodEnd);

                return delegateout;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion ---------------------------------- EMIT HELPERS ---------------------------------------


        /// <summary>
        /// You can change Binding Timeouts if it's needable.
        /// </summary>
        /// <param name="SendTimeoutMins"></param>
        /// <param name="SendTimeoutSeconds"></param>
        /// <param name="ReceiveTimeOutMins"></param>
        /// <param name="ReceiveTimeoutSeconds"></param>
        public void ChangeTimeout(Int32 SendTimeoutMins = 0, Int32 SendTimeoutSeconds = 30, Int32 ReceiveTimeOutMins = 0, Int32 ReceiveTimeoutSeconds = 30)
        {
            ServiceBinding.SendTimeout = new TimeSpan(0, SendTimeoutMins, SendTimeoutSeconds);
            ServiceBinding.ReceiveTimeout = new TimeSpan(0, ReceiveTimeOutMins, ReceiveTimeoutSeconds);
        }


        #region ----------------------------- SERIALIZATION INTEGRATION  -----------------------------------------







        /// <summary>
        /// You can add known types by this method.It's the same as well known ServiceKnownContract type declaration feature in WCF.
        /// </summary>
        /// <param name="KnownContracts"></param>
        public void AddKnownTypesToTSSSerializer(params Type[] KnownContracts)
        {
            if (KnownContracts == null) return;

            foreach (var knownType in KnownContracts)
            {
                ExecuteCommandOperation.KnownTypes.Add(knownType);

                if (UseTSSerializer)
                {
                    TSSerializer.AddKnownType(knownType); // auto detecting -auto adding contract or custom complex struct type                    
                }
            }
        }


#if CLIENT && (SL5 || WP81)

        /// <summary>
        /// There are 3 mode of using TSSSerializer(any client) : 
        /// <para/>    1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
        /// <para/>    2 mode - integrated into DCommandMessage data transmitting- here  we first pack DCommandMessage data into its Body property,
        ///     and then use DataContractSerialization as usual to serialize DCommandMessage as usual DataContract, and mirrored sequence on the server.
        ///     To use that mode set UseTSSSerializer=true on service creation 
        /// <para/>    3 mode - direct TSS serialization(only WPF client)
        /// 
        /// <para/> About modes : 1 and  2 and 3 mode exclude each other         
        /// <para/> This flag, in true, show to us that we use 2 mode- integrated into DCommandMessage data transmitting- TSSSerializer + DataContractSerializer.
        /// On the client(any) you need to  set UseTSSSerializer= true on service client creation.
        /// </summary>
        public bool UseTSSerializer
        {
            get;
            protected set;
        }


        /// <summary>
        /// AddKnownTypes from custom KnowTypesLoader On  Client  Initializing
        /// </summary>
        /// <param name="knownTypesLoaderFunc"></param>
        void LoadKnowTypesFromKnowTypesLoader(Func<IEnumerable<Type>> knownTypesLoaderFunc, bool useTSSSerializer)
        {
            UseTSSerializer = useTSSSerializer;

            if (!ExecuteCommandOperation.KnownTypes.Contains(typeof(DCMessage)))
            {
                ExecuteCommandOperation.KnownTypes.Add(typeof(DCMessage));
            }


            if (knownTypesLoaderFunc != null)
            {
                foreach (var knownType in knownTypesLoaderFunc())
                {
                    ExecuteCommandOperation.KnownTypes.Add(knownType);
                }
            }


            if (UseTSSerializer)
            {
                TSSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(ServiceKey);
                //ClientsSDSerializer.Mapping.UseConventionsProcessing = true;
                //ClientsSDSerializer.Mapping.UseTypeFullNameOnlyCompareMapping = true;

                foreach (var knownType in ExecuteCommandOperation.KnownTypes)
                {
                    TSSerializer.AddKnownType(knownType);
                }
            }

        }


#elif (CLIENT && WPF) || SERVER

        /// <summary>
        /// There are 3 mode of using TSSSerializer(any client) : 
        /// <para/>    1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
        /// <para/>    2 mode - integrated into DCommandMessage data transmitting- here  we first pack DCommandMessage data into its Body property,
        ///     and then use DataContractSerialization as usual to serialize DCommandMessage as usual DataContract, and mirrored sequence on the server.
        ///     To use that mode set UseTSSSerializer=true on service creation 
        /// <para/>    3 mode - direct TSS serialization(only WPF client)
        /// 
        /// <para/> About modes : 1 and  2 and 3 mode exclude each other         
        /// <para/> This flag, in true, show to us that we use 2 mode- integrated into DCommandMessage data transmitting- TSSSerializer + DataContractSerializer.
        /// On the client(any) you need to  set UseTSSSerializer= true on service client creation.
        /// </summary>
        public bool UseTSSerializer
        {
            get;
            protected set;
        }


        /// <summary>
        /// There are 3 mode of using TSSSerializer(only for WPF client, other clients have only two ) : 
        /// <para/>     1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
        /// <para/>     2 mode - integrated into DCommandMessage data transmitting- here  we first pack DCommandMessage data into its Body property,
        ///     and then use DataContractSerialization as usual to serialize DCommandMessage as usual DataContract, and mirrored sequence on the server.
        ///     To use that mode set UseTSSSerializer=true on service creation 
        /// <para/>     3 mode - direct TSS serialization -  when we only use TSSSerializer without DataContractSerializer. 
        ///     It means also that we use TSSSerializationBehavior on the client(WPF) and on the Server.
        ///     On the client(WPF) you need to call SetTSSDirectSerializationBehavior() or set UseTSSDirectSerialization= true on service client creation.
        /// 
        ///<para/>  About modes : 1 and  2 and 3 mode exclude each other         
        ///<para/>  This flag, in true, show to us that we use 3 mode- direct SDS serialization.
        /// </summary>
        public bool UseTSSDirectSerialization
        {
            get;
            protected set;
        }


        /// <summary>
        /// AddKnownTypes from custom KnowTypesLoader On  Client  Initializing
        /// </summary>
        /// <param name="knownTypesLoaderFunc"></param>
        /// <param name="useTSSerializer"></param>
        /// <param name="useTSSDirectSerialization"></param>
        void LoadKnowTypesFromKnowTypesLoader(Func<IEnumerable<Type>> knownTypesLoaderFunc, bool useTSSerializer, bool useTSSDirectSerialization)
        {
            UseTSSerializer = useTSSerializer;
            UseTSSDirectSerialization = useTSSDirectSerialization;

            if (!ExecuteCommandOperation.KnownTypes.Contains(typeof(DCommandMessage)))
            {
                ExecuteCommandOperation.KnownTypes.Add(typeof(DCommandMessage));
            }

            if (knownTypesLoaderFunc != null)
            {
                foreach (var knownType in knownTypesLoaderFunc())
                {
                    ExecuteCommandOperation.KnownTypes.Add(knownType);
                }
            }

            if (UseTSSerializer || UseTSSDirectSerialization)
            {
                TSSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(ServiceKey);
                //SDSerializer.Mapping.UseConventionsProcessing = true;
                //SDSerializer.Mapping.UseTypeFullNameOnlyCompareMapping = true;

                foreach (var knownType in ExecuteCommandOperation.KnownTypes)
                {
                    TSSerializer.AddKnownType(knownType); // auto adding contract or Custom Complex structure contract                    
                }
                
                //TSSerializer.BuildProcessingInfo();
            }

            if (UseTSSDirectSerialization)
            {
                SetTSSDirectSerializationBehavior();
            }

        }






        /// <summary>
        /// There are 3 mode of using TSSSerializer(only for WPFclient , other clients have only two ) : 
        ///     1 mode - when we prefer to use original DataContractSerializer - then UseTSSSerializer=false(it's original state) and UseTSSDirectSerialization=false(it's original state)
        ///     2 mode - integrated into DCommandMessage data transmitting- here  we first pack DCommandMessage data into its Body property,
        ///     and then use DataContractSerialization as usual to serialize DCommandMessage as usual DataContract, and mirrored sequence on the server.
        ///     To use that mode set UseTSSSerializer=true on service creation 
        ///     3 mode - direct TSS serialization -  when we only use TSSSerializer without DataContractSerializer. 
        ///     It means also that we use TSSSerializationBehavior on the client(WPF) and on the Server.
        ///     On the client(WPF) you need to call SetTSSDirectSerializationBehavior() or set UseTSSDirectSerialization= true on service creation.
        ///          
        /// To set working mode into 3 mode- direct TSS serialization - call this method
        /// </summary>
        protected void SetTSSDirectSerializationBehavior()
        {

            var dataContractBehaviors = ExecuteCommandOperation.Behaviors.Where(bhvr => bhvr.GetType().Name.Contains("DataContractSerializerOperation"));

            TSSOperationBehavior newSdsBehavior = new TSSOperationBehavior(ExecuteCommandOperation);

            if (dataContractBehaviors != null && dataContractBehaviors.Count() > 0)
            {
                while (dataContractBehaviors.Count() > 0)
                {
                    var bhvr = dataContractBehaviors.First();
                    if (bhvr is DataContractSerializerOperationBehavior)
                    {
                        newSdsBehavior.MaxItemsInObjectGraph = (bhvr as DataContractSerializerOperationBehavior).MaxItemsInObjectGraph;
                    }
                    ExecuteCommandOperation.Behaviors.Remove(bhvr);
                }
            }


            ExecuteCommandOperation.Behaviors.Add(newSdsBehavior);

            UseTSSDirectSerialization = true;
        }

#endif



        #endregion ----------------------------- SERIALIZATION INTEGRATION  -----------------------------------------


        #region -------------------------------------- EXECUTE COMMAND -----------------------------------------

#if CLIENT && WPF




        /// <summary>
        ///  Executing some Command Message to Server Command Manager using TPL
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ResultUICallback"> Action that you'll  do on result OK </param>
        /// <param name="TriggerFaultAferAction">Custom trigger Action. Will call always - if fault or not</param>
        /// <param name="reporter"> Progress reporter to notify client  UI  about command State or progress</param>
        /// <returns>Task  let us use async call with ExecuteCommand</returns>
        public virtual Task ExecuteCommand(DCommandMessage command,
                                         Action<DCommandMessage> ResultUICallback,
                                         Action<Task<DCommandMessage>> TriggerFaultAferAction = null,
                                         DCProgressReporter reporter = null
                                        )
        {


            // Packing command and Delegates into new Server Call State-into ThreadCallState
            ThreadCallState<DCommandMessage,            //CallServiceKey + Action=Const(ExecuteCommand)  + command  Arguments 
                                Action<DCommandMessage>,      //uicallback Action  
                                Action<Task<DCommandMessage>>, //triggerFaultAferAction      
                                DCProgressReporter
                                >                      //timeout behavior                                                 
            newTaskState = ThreadCallState<DCommandMessage, Action<DCommandMessage>, Action<Task<DCommandMessage>>,
                                DCProgressReporter>
                                .Create(UIInvoker.UIDispatcher, command, ResultUICallback, TriggerFaultAferAction, reporter);



            return Task.Factory.StartNew((st) =>
            {
                ThreadCallState<DCommandMessage, Action<DCommandMessage>, Action<Task<DCommandMessage>>, DCProgressReporter> insideTS =
           st as ThreadCallState<DCommandMessage, Action<DCommandMessage>, Action<Task<DCommandMessage>>, DCProgressReporter>;

                //set ServiceKey of Current Service - it'll  be transmit this command
                insideTS.Item1.SetServiceKey(ServiceKey);
                insideTS.Item1.SetServiceNamespace(ServiceNamespace);


                if (!NetworkInterface.GetIsNetworkAvailable())
                {
                    // no network connection
                    // throw new NotSupportedException("Network doesn't support workable connection now to make service call");
                    NotifyNetworkDoesnWorkAndStopCommandProcessing(insideTS.Item1, insideTS.Item4);
                    return insideTS.Item1;
                }

                var channel = CreateChannelDelegate(); //Factory
                //var ExecCommandFunc = DSConst.BuildExecuteCommandFunc(channel);


                // indication - Starting if newtwork enable start
                NotifyStartCommandProcessing(insideTS.Item1, insideTS.Item4);

                SetDiagnosticsOnClientSendTimeDbg(insideTS.Item1);

                if (UseTSSDirectSerialization == false)
                {
                    PackInputMessageIfUseTSSSerialize(insideTS.Item1);
                }


                return (DCommandMessage)channel.GetType()
                                           .GetMethod(DSConst.Method_ExecuteCommand)
                                           .Invoke(channel, new object[] { command });


            },
                 newTaskState,
                 TaskCreationOptions.AttachedToParent)
                 .ContinueWith<DCommandMessage>(
                 (t) =>
                 {
                     ThreadCallState<DCommandMessage, Action<DCommandMessage>, Action<Task<DCommandMessage>>, DCProgressReporter> insideTSout =
                        t.AsyncState as ThreadCallState<DCommandMessage, Action<DCommandMessage>, Action<Task<DCommandMessage>>, DCProgressReporter>;


                     // updating command container result if Task Ran to Completion
                     if (t.Status == TaskStatus.RanToCompletion)
                     {
                         //updating DCommandMessage by received from server Result Message 
                         if (UseTSSDirectSerialization == false)
                         {
                             insideTSout.Item1 = UnpackOutputMessageIfUseTSSSerialize(t.Result);
                         }
                         else
                         {
                             insideTSout.Item1 = t.Result;
                         }

                         SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(insideTSout.Item1);

                         NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

                         NotifyAboutCommandSuccessResultProgress(insideTSout.Item1, insideTSout.Item4);


                         // ui result callback  
                         if (insideTSout.Item2 != null)
                         {
                             //not very safe way to call delegate on
                             UIInvoker.InvokeAsynchronously(() =>
                              {
                                  insideTSout.Item2(insideTSout.Item1);
                              });
                         }

                         //continue TriggerFaultAferAction - Will be Called if it exist
                         if (insideTSout.Item3 != null)
                         { insideTSout.Item3(t); }


                     }
                     else if (t.Status == TaskStatus.Faulted)
                     {
                         //RanToCompletion                  
                         //check if FActory not Faulted -if Faulted then recreate it
                         CheckRecreateFactoryIfFaulted();

                         NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

                         NotifyAboutCommandTaskFailureExceptionsProgress(t.Exception, insideTSout.Item1, insideTSout.Item4);

                         //continue TriggerFaultAferAction - Will be Called if it exist
                         if (insideTSout.Item3 != null)
                         { insideTSout.Item3(t); }
                     }

                     return insideTSout.Item1;
                 }
                 );

        }









#elif CLIENT && (SL5 || WP81)

        /// <summary>
        ///  Executing some Command Message to Server Command Manager using TPL
        /// </summary>
        /// <param name="command"></param>
        /// <param name="ResultUICallback"> Action that you'll  do on result OK </param>
        /// <param name="TriggerFaultAferAction"></param>
        /// <param name="reporter"></param>
        /// <returns></returns>
        public virtual async Task ExecuteCommand(DCMessage command,
                                         Action<DCMessage> ResultUICallback,
                                         Action<Task<DCMessage>> TriggerFaultAferAction = null,
                                         DCProgressReporter reporter = null
                                        )
        {


            // Packing command and Delegates into new Server Call State-into ThreadCallState
            ThreadCallState<DCMessage,            //CallServiceKey + Action=Const(ExecuteCommand)  + command  Arguments 
                                Action<DCMessage>,      //uicallback Action  
                                Action<Task<DCMessage>>, //triggerFaultAferAction      
                                DCProgressReporter
                                >                      //timeout behavior                                                 
            newTaskState = ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>,
                                DCProgressReporter>
                                .Create(UIInvoker.UIDispatcher, command, ResultUICallback, TriggerFaultAferAction, reporter);


            //set ServiceKey of Current Service - it'll  be transmit this command
            command.SetServiceKey(ServiceKey);
            command.SetServiceNamespace(ServiceNamespace);



            //creating Task of Server CALL to process Command
            Task<DCMessage> querytask = new Task<DCMessage>((st) =>
            {

                ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTS =
                st as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;

                if (!NetworkInterface.GetIsNetworkAvailable())
                {
                    // no network connection
                    // throw new NotSupportedException("Network doesn't support workable connection now to make service call");
                    NotifyNetworkDoesnWorkAndStopCommandProcessing(insideTS.Item1, insideTS.Item4);
                    return insideTS.Item1;
                }


                var channel = CreateChannelDelegate(); //Factory
                var beginExecCommandFunc = DSConst.BuildBeginExecuteCommandFunc(channel);
                var endExecCommandFunc = DSConst.BuildEndExecuteCommandFunc(channel);


                // indication - Starting if newtwork enable start
                NotifyStartCommandProcessing(insideTS.Item1, insideTS.Item4);

                SetDiagnosticsOnClientSendTimeDbg(insideTS.Item1);

                PackInputMessageIfUseTSSSerialize(insideTS.Item1);

                Task<DCMessage> resltTask = Task<DCMessage>.Factory.FromAsync(
                                      beginExecCommandFunc,  // channel.BeginExecuteCommand,     
                                      endExecCommandFunc,    // channel.EndExecuteCommand,         
                                      command,
                                      null
                                     );


                return resltTask.Result;
            },
            newTaskState,
            TaskCreationOptions.AttachedToParent
            );


            //running our created Task asynchronously
            await TaskEx.Run(() =>
            {
                //starting Task 
                querytask.Start();

                return querytask.ContinueWith
                ((t) => /// on RESULTS CONTINUE - MAY BE SUCCESSFUL/ MAY BE FAULT
                {


                    ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter> insideTSout =
                        t.AsyncState as ThreadCallState<DCMessage, Action<DCMessage>, Action<Task<DCMessage>>, DCProgressReporter>;


                    // updating command container result if Task Ran to Completion
                    if (t.Status == TaskStatus.RanToCompletion)
                    {
                        // updating DCommandMessage by received from server Result Message 
                        var returnedFromServerDCommand = UnpackOutputMessageIfUseTSSSerialize(t.Result);
                        insideTSout.Item1.Result = returnedFromServerDCommand.Result;
                        insideTSout.Item1.SetServerReceivedFromClientTime(returnedFromServerDCommand.ServerReceivedFromClientTime);
                        insideTSout.Item1.SetServerSendToClientTime(returnedFromServerDCommand.ServerSendToClientTime);
                        insideTSout.Item1.ProcessingSuccessMessage = returnedFromServerDCommand.ProcessingSuccessMessage;
                        insideTSout.Item1.ProcessingFaultMessage = returnedFromServerDCommand.ProcessingFaultMessage;

                        SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(insideTSout.Item1);

                        NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);

                        NotifyAboutCommandSuccessResultProgress(insideTSout.Item1, insideTSout.Item4);


                        // ui result callback  
                        if (insideTSout.Item2 != null)
                        {
                            //not very safe way to call delegate on
                            UIInvoker.InvokeAsynchronously(() =>
                            {
                                insideTSout.Item2(insideTSout.Item1);

                            });
                        }

                        //continue TriggerFaultAferAction - Will be Called if it exist
                        if (insideTSout.Item3 != null)
                        { insideTSout.Item3(t); }


                    }
                    else if (t.Status == TaskStatus.Faulted)
                    {
                        //RanToCompletion                  
                        //check if FActory not Faulted -if Faulted then recreate it
                        CheckRecreateFactoryIfFaulted();


                        NotifyAboutCommandFailureResultProgress(insideTSout.Item1, insideTSout.Item4);


                        NotifyAboutCommandTaskFailureExceptionsProgress(t.Exception, insideTSout.Item1, insideTSout.Item4);

                        //continue TriggerFaultAferAction - Will be Called if it exist
                        if (insideTSout.Item3 != null)
                        { insideTSout.Item3(t); }
                    }

                    return insideTSout.Item1; //need to process Faulted case 

                });

            });


        }


#endif



        /// <summary>
        /// If Factory will be Fault  then we can Recrete new and call next command by this Service
        /// </summary>
        void CheckRecreateFactoryIfFaulted()
        {
            if (Factory != null && Factory.State == CommunicationState.Faulted)
            {
                lock (_locker)
                {
                    if (Factory != null && Factory.State == CommunicationState.Faulted)
                    {
                        Factory.Abort();
                        Factory = null;
                        Factory = CreateChannelFactoryDelegate(ServiceBinding, ServiceAddress);
                    }
                }
            }
        }


        #endregion -------------------------------------- EXECUTE COMMAND -----------------------------------------


        #region ------------------------------- TSSSerialier  PACK/UNPACK  INPUT AND OUTPUT MESSAGES ---------------------------

        DCMessage UnpackOutputMessageIfUseTSSSerialize(DCMessage outputMessage)
        {
            if (outputMessage == null) return DCMessage.Create(null, null);// ();// null;

            if (UseTSSerializer)//unpack result command if we use SDSerialization
            {
                if (outputMessage.Body == null) return DCMessage.Create(null, null);// it s error
                //var unpackedMessage =
                using (var strm = new MemoryStream(outputMessage.Body))
                {
                    return (DCMessage)TSSerializer.Deserialize(strm);
                }
            }
            else
            {
                return outputMessage;
            }
        }


        void PackInputMessageIfUseTSSSerialize(DCMessage inputMessage)
        {
            if (inputMessage == null) return;// new DCommandMessage();// null;

            if (UseTSSerializer)//unpack result command if we use SDSerialization
            {
                using (var stream = new MemoryStream())
                {
                    TSSerializer.Serialize(stream, inputMessage);
                    inputMessage.Body = stream.ToArray();
                }
            }
        }


        #endregion ------------------------------- TSSSerialier  PACK/UNPACK  INPUT AND OUTPUT MESSAGES ---------------------------


        #region --------------------------------------- NOTIFICATION UI Client about COMMAND PROGRESS  ----------------------------------------


        /// <summary>
        /// Notifying UI Client that Network is  currently unavailable
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        void NotifyNetworkDoesnWorkAndStopCommandProcessing(DCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;

            // no network connection
            // throw new NotSupportedException("Network doesn't support workable connection now to make service call");

            reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.StopCommandIndication, command.ID));
            reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.DetailMessage, command.ID, null,
                                                     "Network doesn't work. Command cannot be processed")
                                                  );


        }



        /// <summary>
        /// Notifying UI Client that we Starting DCommandMessage Processing 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        void NotifyStartCommandProcessing(DCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;

            reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.ClearDetailMessages, command.ID));
            reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.StartCommandIndication | DCProgressEn.ProgressMessage | DCProgressEn.DetailMessage
                                             , command.ID
                                             , "Starting  Command...."
                                             , String.Format("to  Controller:[{1}],   Name :[{0}]", command.Controller, command.Command)
                                                                                 )
                                         );
        }



        /// <summary>
        /// Notifying UI Client About received DCommandMessage result
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        void NotifyAboutCommandSuccessResultProgress(DCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;


            if (command.HasCommandSuccessfullyCompleted)// SuccessfullyCompleted
            {
                reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.StopCommandIndication, command.ID));

                if (command.ProcessingSuccessMessage != String.Empty) //some real successfully message
                {
                    reporter.NotifyProgress(DCProgressInfoTp.ProgressAction(DCProgressEn.DetailMessage, command.ID,
                                                                                        null,
                                                                                        command.ProcessingSuccessMessage));
                }

            }
        }



        /// <summary>
        /// Notifying UI Client - about command Failure results on server , if it's so - command.HasServerErrorHappened
        /// </summary>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        void NotifyAboutCommandFailureResultProgress(DCMessage command, DCProgressReporter reporter)
        {
            if (command == null || reporter == null) return;
            var ServerDefaultErrorMessage = DSConst.Server_Default_Error_Message;

            //checking results from server  and show Progress Messages 
            if (command.HasServerErrorHappened)// Server can say about some error 
            {
#if  DEBUG                  //Debug Message about error
                reporter.NotifyProgress(
                                    DCProgressInfoTp.ProgressAction(DCProgressEn.ServerErrorMessage, command.ID, null, null, command.ProcessingFaultMessage)
                                                );





#endif
                //if not debug - still show to the client some common  phrase like [SERVER ERROR HAPPENED]
                reporter.NotifyProgress(
                                    DCProgressInfoTp.ProgressAction(DCProgressEn.ServerErrorMessage, command.ID, null, null, ServerDefaultErrorMessage)
                                                );
            }
        }



        /// <summary>
        ///  Notifying UI Client - about Communication Exceptions that happened during a try to call WCF Service
        /// </summary>
        /// <param name="aggexc"></param>
        /// <param name="command"></param>
        /// <param name="reporter"></param>
        [Conditional("DEBUG")]
        void NotifyAboutCommandTaskFailureExceptionsProgress(AggregateException aggexc, DCMessage command, DCProgressReporter reporter)
        {
            if (aggexc == null || aggexc.InnerExceptions == null || aggexc.InnerExceptions.Count == 0) return;
            if (reporter == null) return;

            //var DefaultErrorMessage = "Unknown Command execution error happened.";

            foreach (var excItem in aggexc.InnerExceptions)
            {
                // TODO CONCATENATE MESSAGES
                reporter.NotifyProgress(
                                    DCProgressInfoTp.ProgressAction(DCProgressEn.DetailMessage, command.ID, null, null, excItem.Message)
                                                );
            }


        }


        #endregion ---------------------------------------  NOTIFICATIONS COMMAND PROGRESS  ----------------------------------------


        #region ---------------------------------------- DIAGNOSTICS POINTS -----------------------------------------


        public DateTime ClientSendTimeStamp { get; set; }
        public DateTime ClientRecievedResultFromServerTimeStamp { get; set; }


        [Conditional("DEBUG")]
        void SetDiagnosticsOnClientSendTimeDbg(DCMessage command)
        {
            command.SetClientSendTime(DateTime.Now);
        }


        [Conditional("DEBUG")]
        void SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(DCMessage command)
        {
            command.SetClientRecievedResultFromServerTime(DateTime.Now);
        }


        //public DateTime ServerReceivedFromClientTime { get; set; }
        //public DateTime ServerSendToClientTime { get; set; }


        #endregion ---------------------------------------- DIAGNOSTICS POINTS -----------------------------------------




    }

}
