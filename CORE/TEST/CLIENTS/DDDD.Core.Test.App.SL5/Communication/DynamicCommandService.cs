﻿using DDDD.SDS.Tests.Model.CommunicationModel;
using DDDD.SDS.Tests.ServiceModel;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;


namespace DDDD.SDS.Test.SL5.Communication
{

    #region  -------------------------- SERVICE KNOWN TYPES(both for SERVER & CLIENT)  --------------------------------

    public static class DynamicCommandServiceModelKnownTypesHelper
    {

        /// <summary>
        /// This class has the method named GetKnownTypes that returns a generic IEnumerable. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetDynamicCommandServiceKnownTypes(ICustomAttributeProvider provider)
        {
            List<Type> serviceContracts = new List<Type>();
            serviceContracts.Add(typeof(CommandMessage));
            return serviceContracts;
        }

    }

    #endregion  -------------------------- SERVICE KNOWN TYPES(both for SERVER & CLIENT)  --------------------------------


    /// <summary>
    /// ServiceHost Contract
    /// </summary>
    //[GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    //[ServiceContractAttribute(Name = "IDynamicCommandService", Namespace = "cersanit.crm.com", ConfigurationName = "CommandTestModule.IDynamicCommandService")]
    //public interface IDynamicCommandService
    //{
    //    [OperationContractAttribute(AsyncPattern = true, Action = "http://cersanit.crm.com/IDynamicCommandService/ExecuteCommand", ReplyAction = "http://cersanit.crm.com/IDynamicCommandService/ExecuteCommandResponse")]
    //    [ServiceKnownType("GetCommandTestModuleKnownTypes", typeof(CommandTestServiceModelKnownTypesHelper))]
    //    IAsyncResult BeginExecuteCommand(CommandMessage ClientCommand, AsyncCallback callback, object asyncState);

    //    CommandMessage EndExecuteCommand(System.IAsyncResult result);
    //}



    //[ServiceContract(Name = "DynamicCommandService1", Namespace = "http://cersanit.crm.com/")]
    //[ServiceKnownType("GetDynamicCommandServiceKnownTypes")]     //, typeof(DynamicCommandServiceModelKnownTypesHelper)
    public  class DynService1KnownTypesLoader //:  DynamicCommandServiceClientBase
    {

       
        public void TestString(String ClientCommand)
        {
            //CommandManagersHub.ExecuteCommand(ref ClientCommand);           
            //return ClientCommand;
        }



        public static IEnumerable<Type> GetDynamicCommandServiceKnownTypes(ICustomAttributeProvider provider)
        {
            List<Type> serviceContracts = new List<Type>();
            serviceContracts.Add(typeof(CommandMessage));
            return serviceContracts;
        }




    }


  

}