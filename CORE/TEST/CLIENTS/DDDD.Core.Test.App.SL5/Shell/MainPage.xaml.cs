﻿//using Castle.DynamicProxy;
using DDDD.BCL.Test.Web.DCManagers;
using DDDD.Core.Net;
using DDDD.Core.UI;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace DDDD.Core.Test.SL5
{




    [ShellUserControl("Shell1")]
    public partial class MainPage : UserControl
    {

        #region ----------------------- CTOR ----------------------------

        public MainPage()
        {
            InitializeComponent();
        }


        #endregion ----------------------- CTOR ----------------------------



        // After the Frame navigates, ensure the HyperlinkButton representing the current page is selected
        private void ContentFrame_Navigated(object sender, NavigationEventArgs e)
        {
            foreach (UIElement child in LinksStackPanel.Children)
            {
                HyperlinkButton hb = child as HyperlinkButton;
                if (hb != null && hb.NavigateUri != null)
                {
                    if (hb.NavigateUri.ToString().Equals(e.Uri.ToString()))
                    {
                        VisualStateManager.GoToState(hb, "ActiveLink", true);
                    }
                    else
                    {
                        VisualStateManager.GoToState(hb, "InactiveLink", true);
                    }
                }
            }
        }


        // If an error occurs during navigation, show an error window
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            e.Handled = true;
            ChildWindow errorWin = new ErrorWindow(e.Uri);
            errorWin.Show();
        }



        private void ContentFrame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            string adress = e.Uri.ToString();

            //var options = new ProxyGenerationOptions
            //{
            //    BaseTypeForInterfaceProxy = typeof(SimpleClass)
            //};
            //var proxy = generator.CreateInterfaceProxyWithoutTarget(typeof(IService), Type.EmptyTypes, options);
            //var newProxy = SerializeAndDeserialize(proxy);
        }

        private async void button_Manager_Click(object sender, RoutedEventArgs e)
        {
            await ClientReportManager.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }

        private async void button_Manager2_Click(object sender, RoutedEventArgs e)
        {
            await ClientReportManager2.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc, Guid.NewGuid());
        }


        private async void button_Manager3_Click(object sender, RoutedEventArgs e)
        {
            await ClientReportManager3.Current.LoadNewClientReport(DCCScenarios.Wcf_EnvironmentActivationSvc,  Guid.NewGuid());
        }
    }
}