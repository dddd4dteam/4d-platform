﻿using DDDD.Core.Serialization;
using DDDD.Core.Tests.Model.StreamMessagesModel;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.TestUtils
{

   

    public interface IMemStreamTest
    {
        string Framework { get; }
        void Prepare(int numMessages);
        long Serialize(MessageBase[] msgs);
        MessageBase[] Deserialize();
    }


    internal class MemStreamTest : IMemStreamTest
    {
        MessageBase[] m_received;
        MemoryStream m_stream;

        public string Framework { get { return "SDSSerializer"; } }

        public void Prepare(int numMessages)
        {
            m_received = new MessageBase[numMessages];
            m_stream = new MemoryStream();
        }

        public long Serialize(MessageBase[] msgs)
        {
            try
            {
                int numMessages = msgs.Length;
                Int32 index = 0;
                m_stream.Position = 0;
                for (int i = 0; i < msgs.Length; i++)
                {
                    index++;
                    TypeSetSerializer.GetDirect(0).Serialize(m_stream, msgs[i]); //, false, false
			    }//each (var msg in msgs)
                

                m_stream.Flush();
                return m_stream.Position;
            }
            catch (Exception exc)
            {                
                throw exc;
            }
        }

        public MessageBase[] Deserialize()
        {
            try
            {
                int numMessages = m_received.Length;

                m_stream.Position = 0;

                for (int i = 0; i < numMessages; ++i)
                {    m_received[i] = TypeSetSerializer.GetDirect(0)
                                                .Deserialize<MessageBase>(m_stream);
                    
                // m_received[i] = (MessageBase)Serializer.Deserialize(m_stream);
                }
                m_stream.Flush();
                return m_received;
            }
            catch (Exception)
            {                
                throw;
            }
        }



        internal static void Test(IMemStreamTest test, MessageBase[] msgs)
        {
            test.Prepare(msgs.Length);

            /* Serialize part */
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
#if !SL5
                var c0 = GC.CollectionCount(0);
                var c1 = GC.CollectionCount(1);
                var c2 = GC.CollectionCount(2);

                var sw = Stopwatch.StartNew();
        
#endif

                long size = test.Serialize(msgs);

         
#if ! SL5
                sw.Stop();
                c0 = GC.CollectionCount(0) - c0;
                c1 = GC.CollectionCount(1) - c1;
                c2 = GC.CollectionCount(2) - c2;

                 Console.WriteLine("{0,-13} | {1,-21} | {2,10} | {3,3} {4,3} {5,3} | {6,10} |",
                    test.Framework, "MemStream Serialize", sw.ElapsedMilliseconds, c0, c1, c2, size);
#endif


            }

            /* Deerialize part */

            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
#if !SL5
                var c0 = GC.CollectionCount(0);
                var c1 = GC.CollectionCount(1);
                var c2 = GC.CollectionCount(2);

                var sw = Stopwatch.StartNew();
        
#endif

                var received = test.Deserialize();
#if !SL5
                        sw.Stop();

                c0 = GC.CollectionCount(0) - c0;
                c1 = GC.CollectionCount(1) - c1;
                c2 = GC.CollectionCount(2) - c2;
                   Console.WriteLine("{0,-13} | {1,-21} | {2,10} | {3,3} {4,3} {5,3} | {6,10} |",
                    test.Framework, "MemStream Deserialize", sw.ElapsedMilliseconds, c0, c1, c2, "");

                
#endif


                for (int i = 0; i < msgs.Length; ++i)
                {
                    var msg1 = msgs[i];
                    var msg2 = received[i];
#if !SL5
                    msg1.Compare(msg2);
                    
#endif
                }
            }
        }


    }
}
