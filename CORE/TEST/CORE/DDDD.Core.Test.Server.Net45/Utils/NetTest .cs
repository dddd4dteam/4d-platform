﻿
using DDDD.Core.Serialization;
using DDDD.Core.Tests.Model.StreamMessagesModel;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.TestUtils
{


    interface INetTest
    {
        string Framework { get; }
        void Prepare(int numMessages);
        MessageBase[] Test(MessageBase[] msgs);
    }


   internal class NetTest : INetTest
    {
        MessageBase[] m_sent;
        MessageBase[] m_received;

        Thread m_server;
        Thread m_client;

        ManualResetEvent m_ev;

        public string Framework { get { return "SDSerializer"; } }

        public void Prepare(int numMessages)
        {
            m_received = new MessageBase[numMessages];

            m_ev = new ManualResetEvent(false);

            m_server = new Thread(ServerMain);
            m_server.Start();

            Thread.Sleep(100);

            m_client = new Thread(ClientMain);
            m_client.Start();
        }

        public MessageBase[] Test(MessageBase[] msgs)
        {
            m_sent = msgs;

            m_ev.Set();

            m_client.Join();
            m_server.Join();

            return m_received;
        }

        void ServerMain()
        {
            var listener = new TcpListener(IPAddress.Loopback, 9999);
            listener.Start();
            var c = listener.AcceptTcpClient();

            using (var stream = c.GetStream())
            using (var bufStream = new  BufferedStream(stream))
            {
                for (int i = 0; i < m_received.Length; ++i)
                    m_received[i] = TypeSetSerializer.GetDirect(0).Deserialize<MessageBase>(bufStream);
                    //m_received[i] = (MessageBase)Serializer.Deserialize(bufStream);
            }

            listener.Stop();
        }

        void ClientMain()
        {
            var c = new TcpClient();
            c.Connect(IPAddress.Loopback, 9999);

            using (var netStream = c.GetStream())
            using (var bufStream = new BufferedStream(netStream))
            {
                m_ev.WaitOne();

                 for (int i = 0; i < m_sent.Length; ++i)
                    TypeSetSerializer.GetDirect(0).Serialize(bufStream, m_sent[i]);
                   // Serializer.Serialize(bufStream, m_sent[i]);
            }

            c.Close();
        }




        internal static void Test(INetTest test, MessageBase[] msgs)
        {
            test.Prepare(msgs.Length);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();

            var c0 = GC.CollectionCount(0);
            var c1 = GC.CollectionCount(1);
            var c2 = GC.CollectionCount(2);

            var sw = Stopwatch.StartNew();

            var received = test.Test(msgs);

            sw.Stop();

            c0 = GC.CollectionCount(0) - c0;
            c1 = GC.CollectionCount(1) - c1;
            c2 = GC.CollectionCount(2) - c2;

            Console.WriteLine("{0,-13} | {1,-21} | {2,10} | {3,3} {4,3} {5,3} | {6,10} |",
                test.Framework, "NetTest", sw.ElapsedMilliseconds, c0, c1, c2, "");

            for (int i = 0; i < msgs.Length; ++i)
            {
                var msg1 = msgs[i];
                var msg2 = received[i];

                msg1.Compare(msg2);
            }
        }





    }
}
