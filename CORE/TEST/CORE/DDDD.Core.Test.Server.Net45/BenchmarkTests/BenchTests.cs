﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf.Meta;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Etimo.Benchmarks.Processors;
using Etimo.Benchmarks.Interfaces.Operations;
using Etimo.Benchmarks.Interfaces.Components;
using Etimo.Benchmarks.Interfaces.Results;



//using VirtualCollection.Precompile;
using DDDD.SDS;

using DDDD.SDS.Tests.Model;
using DDDD.SDS.Tests.Model.CommonModel;
using VirtualCollection.Precompile;


namespace DDDD.SDBS
{

    // BenchmarkComponents Tests :  
    // Serialization and Deserialization 
    //            1 Protobuf  without Precompiled -Serialization/Deserialization operations  -time/memory 
    //            2 Protobuf with Precompiled test Model -Serialization/Deserialization operations -time/memory
    //            3 KLS without Precompiled test -Serialization/Deserialization operations -time/memory
    //            4 New KLS feature with Precompiled test Model -Serialization/Deserialization operations -time/memory(my design)
    

    public static  class BenchmarkTests
    {
        
        #region  -------------------------- DATA --------------------------------

        internal class DataCollector
        {


#region ------------------------- CONST ----------------------------------

            const Int32 CommandsTestCountDefault = 7;// 20000; 
            const Int32 ProductsInCommandTestDefault = 20;

#endregion ------------------------- CONST ----------------------------------


#region  ---------------------------- FIELDS ---------------------------------

            // public static Dictionary<Int32, object> DATA = new Dictionary<Int32, object>();
            // public static Dictionary<Int32, object> compareDeserDATA = new Dictionary<Int32, object>();



            public static byte[] SerializedDATA
            {
                get;
                set;
            }


            public static List<CommandContainer> DATACommands
            {
                get;
                set;
            }


            public static List<CommandContainer> CompareDesererializeDATA
            {
                get;
                set;
            }


#endregion ---------------------------- FIELDS ---------------------------------
            

            static Product GenerateProduct(Int32 ID, decimal price =0, String category=null, String name  = null)
            {
                return  new Product { Id = ID, 
                    Category = String.IsNullOrEmpty( category)  ? "Autos": category , 
                    //Price = ( price == 0 ) ? new Random().Next(30000,2000000) :  price,
                    Name = String.IsNullOrEmpty( name) ? "AutoName_"+ ID.ToString(): name
                };
            }


            static IEnumerable<Product> GenerateNProducts(Int32 Count)
            {
                Product[] resultProducts = new Product[Count];
                for (Int32 i = 1; i <= Count;i++ )
                {
                    resultProducts[i] =  GenerateProduct(i);
                }
                 return resultProducts;
            }


            internal static IEnumerable<CommandContainer> GenerateArrayNCommands1(Int32 CommandsCount = CommandsTestCountDefault, Int32 ProductsInCommand = ProductsInCommandTestDefault)
            {
                DATACommands = new  List<CommandContainer>();
                
                for (Int32 i = 1; i <= CommandsCount; i++)
                {
                    DATACommands.Add(new CommandContainer() );
                    DATACommands[i - 1].Products = new List<ContractBase>();
                    for (int m = 0; m < ProductsInCommand; m++)
                    {
                        DATACommands[i - 1].Products.Add(GenerateProduct(m));
                    }   
                }

                return DATACommands;

            }


            //static Dictionary<Int32,object> GenerateDictionaryNCommands1(Int32 CommandsCount = CommandsTestCountDefault, Int32 ProductsInCommand = ProductsInCommandTestDefault)
            //{
            //    Dictionary<Int32, object> resultCommands = new Dictionary<Int32,object>();
            //    for (Int32 i = 1; i <= CommandsCount; i++)
            //    {
            //        resultCommands[i] = new CommandContainer();
            //        (resultCommands[i] as CommandContainer).Products = new List<ContractBase>();
            //        for (int m = 1; m <= ProductsInCommand; m++)
            //        {
            //            (resultCommands[i] as CommandContainer).Products.Add(GenerateProduct(m));
            //        }
            //    }
            //    return resultCommands;
            //}


            //public static void GenerateData()
            //{
            //    DATA.Clear();
            //    serializedDATA = null;
            //    compareDeserDATA.Clear();
            //    DATA = GenerateDictionaryNCommands1(CommandsTestCountDefault, ProductsInCommandTestDefault);                          
            //}


        }

        #endregion -------------------------- DATA --------------------------------
        
    
        #region ------------------------------ BASE BENCHMARK COMPONENT & OperationGroup -----------------------------


            /// <summary>
            /// Benchmark Component for  Serialize & Deserialize Operations   
            /// </summary>
            /// <typeparam name="TBMGroupOperation"> Serialization-IOperationWithFunc[byte[]] ; Deserialize-IOperationWithAction  </typeparam>   
            public  class BmComponentSerDesBase<TBMGroupOperation>: IBenchmarkComponent<TBMGroupOperation> 
                     where TBMGroupOperation: IOperationGroup<IOperationWithAction,IOperationWithAction>
            {

                /// <summary>
                /// 
                /// </summary>
                public TBMGroupOperation RootOperation
                {
                    get; 
                    set;
                }


                ///// <summary>
                ///// Serialized DATA
                ///// </summary>
                //public byte[] SerializedDATA
                //{
                //    get; 
                //    set;
                //}





                public string Name
                {
                    get;
                    set;
                }
            }


            /// <summary>
            /// Root  Group Operation - Serialization & Deserialization operations  
            /// </summary>
            public class BmOpGroup_SerializeDeserialize :    IOperationGroup<IOperationWithAction,IOperationWithAction>
            {
                public string Name { get { return " Benchmark Group of Operations  Root  -  for  Serialize & Deserialize operations "; } }
        

                /// <summary>
                /// Serialization Operation 
                /// </summary>
                public IOperationWithAction Operation1
                {
	                get; 
                    internal set; 
                }

                /// <summary>
                /// Deserialization Operation 
                /// </summary>
                public IOperationWithAction Operation2
                {
	                get;
                    internal set; 
                }


        }


        #endregion ------------------------------ BASE BENCHMARK COMPONENT & OperationGroup -----------------------------

   
        #region ------------------------------ BENCHMARK OPERATIONS --------------------------------------


    public class Operation_KLS_NoPrecompiledModel_Serialize : IOperationWithAction
    {

        public Operation_KLS_NoPrecompiledModel_Serialize()
        {
            Delegate = Serialize;
        }

        #region  --------------------------- PROPERTIES ---------------------------------

        public string Name { get { return "Operation KLS_NoPrecompiledModel_Serialize"; } }

        public Action Delegate { get; set; }

        #endregion --------------------------- PROPERTIES ---------------------------------


        void Serialize()
        {

            //return null;
        }


    }

    public class Operation_KLS_NoPrecompiledModel_Deserialize : IOperationWithAction
    {

        public Operation_KLS_NoPrecompiledModel_Deserialize()
        {
            Delegate = Deserialize;
        }


        #region -------------------------- PROPERTIES ----------------------------------

        public string Name { get { return "Operation KLS_NoPrecompiledModel_Deserialize"; } }

        public Action Delegate { get; set; }

        #endregion -------------------------- PROPERTIES ----------------------------------


        void Deserialize()
        {

        }
    }




       
    public class Operation_KLS_WithPrecompiledModel_Serialize : IOperationWithAction
    {

        public Operation_KLS_WithPrecompiledModel_Serialize()
        {            
            Delegate = Serialize;
        }
        

        #region  --------------------------- PROPERTIES ---------------------------------

        public string Name 
        { 
            get { return "Operation_KLS_WithPrecompiledModel_Serialize"; } 
        }

        public Action Delegate { get; set; }

        #endregion --------------------------- PROPERTIES ---------------------------------
        
        
        void Serialize()
        {
            //Get
            //KLSPrecompiledTestModel serializer = new KLSPrecompiledTestModel();

            //CommandContainer command = new CommandContainer();            
            //command.Products = new List<ContractBase>();
            //command.Products.Add(new Product());
            //List<ContractBase> products = new List<ContractBase>();
            //products.Add(new Product());
            //products.Add(new Product());


            //var serializer = new KellermanSoftware.Serialization.Serializer();
            //DataCollector.SerializedDATA = serializer.Serialize(products); // command

            string ServiceKey = "TestService";
            SerializersContainer.AddUpdateSerializer(ServiceKey);
            var serializer = SerializersContainer.GetByKey(ServiceKey);

            serializer.AddContract<ContractBase>();
            serializer.AddContract<Product>();
            serializer.AddContract<CommandContainer>();
            serializer.AddContract<CommandContainer2>();

            //DataCollector.SerializedDATA = KLSPrecompiledTestModel.Serialize(products);
            DataCollector.SerializedDATA = serializer.Serialize(DataCollector.DATACommands);

            //serializer.PrintTrackToFile();

            //serializer.PrintOnlyWrittenPointsIntoFile();

            //serializer.


            //return null;
        }

    }

    public class Operation_KLS_WithPrecompiledModel_Deserialize : IOperationWithAction
    {
        public Operation_KLS_WithPrecompiledModel_Deserialize()
        {
            Delegate = Deserialize;
        }

        #region -------------------------- PROPERTIES ----------------------------------

        public string Name { get { return "Operation_KLS_WithPrecompiledModel_Deserialize"; } }

        public Action Delegate { get; set; }

        #endregion -------------------------- PROPERTIES ----------------------------------
        
        void Deserialize()
        {
            //Original serializaer Deserialization
            string ServiceKey = "TestService";           
            var serializer = SerializersContainer.GetByKey(ServiceKey);


            //DataCollector.CompareDesererializeDATA = serializer.Deserialize<List<CommandContainer>>(DataCollector.SerializedDATA);
            //command.Products.Add(new Product());
            //var serializer =new  Serializer();

            List<CommandContainer> commands = (List<CommandContainer>)serializer.Deserialize(DataCollector.SerializedDATA, typeof(List<CommandContainer>));            
            commands[0].Products.Add(new Product() { Id = 3, Name = "Name1" });


            string variable = "";
            variable += "123123";

          //DataCollector.CompareDesererializeDATA = serializer.Deserialize<CommandContainer>(DataCollector.SerializedDATA);

        }

    }





    public class Operation_Protobuf_NoPrecompiledModel_Serialize : IOperationWithAction
    {

        public Operation_Protobuf_NoPrecompiledModel_Serialize()
        {
            Delegate = Serialize;
        }


        #region  --------------------------- PROPERTIES ---------------------------------

        public string Name { get { return "Operation_Protobuf_NoPrecompiledModel_Serialize"; } }

        public Action Delegate { get; set; }

        #endregion --------------------------- PROPERTIES ---------------------------------


        void Serialize()
        {

            //return null;
        }


    }

    public class Operation_Protobuf_NoPrecompiledModel_Deserialize : IOperationWithAction
    {

        public Operation_Protobuf_NoPrecompiledModel_Deserialize()
        {
            Delegate = Deserialize;
        }


        #region -------------------------- PROPERTIES ----------------------------------

        public string Name { get { return "Operation_Protobuf_NoPrecompiledModel_Deserialize"; } }

        public Action Delegate { get; set; }

        #endregion -------------------------- PROPERTIES ----------------------------------


        void Deserialize()
        {

        }

    }





    public class Operation_Protobuf_WithPrecompiledModel_Serialize : IOperationWithAction
    {

        public Operation_Protobuf_WithPrecompiledModel_Serialize()
        {   
            Delegate = Serialize;
        }


        #region  --------------------------- PROPERTIES ---------------------------------

        public string Name { get { return "Operation_Protobuf_WithPrecompiledModel_Serialize"; } }
        
        public Action Delegate { get; set; }

        #endregion --------------------------- PROPERTIES ---------------------------------



        public  void Precompile()
        {
            // two variants: 1- Disassembled ready code of precompiled early the same TypeModel - 
            //               2- Precompile now as usual                   
            
            //1 variant
            var model1 = new ProtoPrecompiledTestModelDisassembled();//.Create(); RuntimeTypeModel();// ProtoPrecompiledModel.Create();
            //model1.Serialize()


            //2 variant
            RuntimeTypeModel model = RuntimeTypeModel.Create();
            model.Add(typeof(ContractBase), true);
            model.Add(typeof(Product), false);
            model.Add(typeof(CommandContainer), true);

            //precompile model            
            model.Compile("ProtoPrecompiledTestModel", @"ProtobufPrecompiledTestModel.dll");

        }
        
        void Serialize()
        {


            //return null;
        }

    }
        
    public class Operation_Protobuf_WithPrecompiledModel_Deserialize : IOperationWithAction
    {

        public Operation_Protobuf_WithPrecompiledModel_Deserialize()
        {
            Delegate = Deserialize;
        }


        #region -------------------------- PROPERTIES ----------------------------------

        public string Name { get { return "Operation_Protobuf_WithPrecompiledModel_Deserialize"; } }

        public Action Delegate { get; set; }

        #endregion -------------------------- PROPERTIES ----------------------------------


        void Deserialize()
        {


        }

    }
        

#endregion ------------------------------ BENCHMARK OPERATIONS --------------------------------------
    

#region ----------------------------- BENCHMARK  CONMPONENTS -------------------------------



        /// <summary>
        /// 1 Protobuf  NO Precompiled -Serialization & Deserialization operations  - time/memory
        /// </summary>
        [Description("1 Protobuf NO Precompiled -Serialization & Deserialization operations  - time/memory")]
        internal class BC_ProtobufNoPrecompiled_SerializeDeserialize : BmComponentSerDesBase<BmOpGroup_SerializeDeserialize>
        {
            public  string Name
            {
                get { 
                    return "1 Protobuf NO Precompiled -Serialization & Deserialization operations"; 
                }
            }


             

            public BC_ProtobufNoPrecompiled_SerializeDeserialize()
            {
               //Serialize & Deserialize root Group operation
                RootOperation = new BmOpGroup_SerializeDeserialize()
                {
                    //Serialization
                    Operation1 = new Operation_Protobuf_NoPrecompiledModel_Serialize(),
                   
                    //Deserialization
                    Operation2 = new Operation_Protobuf_NoPrecompiledModel_Deserialize()
                   
                };
            }


        }



        /// <summary>
        /// 2 Protobuf with Precompiled Model -Serialization & Deserialization operations  -time/memory
        /// </summary>
        [Description("2 Protobuf with Precompiled test Model -Serialization & Deserialization operations  -time/memory")]
        internal class BC_ProtobufPrecompiled_SerializeDeserialize : BmComponentSerDesBase<BmOpGroup_SerializeDeserialize>
        {
            public  string Name
            {
                get 
                {
                    return "2 Protobuf with Precompiled test Model -Serialization & Deserialization operations"; 
                }
            }


           public BC_ProtobufPrecompiled_SerializeDeserialize()
            {
                //Serialize & Deserialize root Group operation
                RootOperation = new BmOpGroup_SerializeDeserialize()
                {
                   //Serialization
                   Operation1 = new Operation_Protobuf_WithPrecompiledModel_Serialize(),
                   
                   //Deserialization
                   Operation2 = new Operation_Protobuf_WithPrecompiledModel_Deserialize()                   
                };
            }

            

        }


        /// <summary>
        /// 3 KLS NO Precompiled Model -Serialization & Deserialization operations  -time/memory
        /// </summary>
        [Description("3 KLS NO Precompiled Model -Serialization & Deserialization operations  -time/memory")]
        internal class BC_KLSNoPrecompiled_SerializeDeserialize :  BmComponentSerDesBase<BmOpGroup_SerializeDeserialize>
        {
            public  string Name
            {
                get { return "3 KLS NO Precompiled test -Serialization & Deserialization operations"; }
            }


           public BC_KLSNoPrecompiled_SerializeDeserialize()
            {
                //Serialize & Deserialize root Group operation
                RootOperation = new BmOpGroup_SerializeDeserialize()
                {
                    //Serialization
                    Operation1 = new Operation_KLS_NoPrecompiledModel_Serialize(),
                    
                    //Deserialization
                    Operation2 = new Operation_KLS_NoPrecompiledModel_Deserialize()
                    
                };

            }

        }



        /// <summary>
        /// 4 (New feature - my design) KLS  with Precompiled  Model -Serialization & Deserialization operations -time/memory(my design)
        /// </summary>
        [Description("4 (New feature - my design) KLS  with Precompiled  Model -Serialization & Deserialization operations -time/memory")]
        internal class BC_NewKLSPrecompiled_SerializeDeserialize : BmComponentSerDesBase<BmOpGroup_SerializeDeserialize>
        {
            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get
                {
                    return "4 (New feature) KLS  with Precompiled  Model -Serialization & Deserialization operations";
                }
            }


            public BC_NewKLSPrecompiled_SerializeDeserialize()
            {

                //Serialize & Deserialize root Group operation
                RootOperation = new BmOpGroup_SerializeDeserialize()
                {
                    //Serialization
                    Operation1 = new Operation_KLS_WithPrecompiledModel_Serialize(),
                    //Deserialization
                    Operation2 = new Operation_KLS_WithPrecompiledModel_Deserialize()

                };


            }

        }

#endregion ----------------------------- BENCHMARK CONMPONENTS -------------------------------
                

#region ------------------------------------- METHODS ---------------------------------------------


        private static string FormatBenchmarkResults(IOperationResultBase benchmarkResult, int nestingLevel)
        {
            string formattedResults = nestingLevel == 0 ? "" : new string('-', nestingLevel * 3) + '→' + ' ';
            string label = benchmarkResult is IOperationGroupResult ? benchmarkResult.Name + " (Group Total)" : benchmarkResult.Name;

            formattedResults += string.Format("{0}: {1}", label.PadRight(22), benchmarkResult.Durations.Min.TotalMilliseconds.ToString("0.000 ms.").PadLeft(12));

            if (benchmarkResult is IOperationWithFuncResult)
                formattedResults += string.Format("{0}Return Value: {1}", new string(' ', 3), ((IOperationWithFuncResult)benchmarkResult).FuncReturnValue);

            if (benchmarkResult is IOperationGroupResult)
            {
                IOperationGroupResult benchmarkResultTyped = (IOperationGroupResult)benchmarkResult;

                foreach (IOperationResultBase childOperationResult in benchmarkResultTyped.ChildOperationResults)
                    formattedResults += Environment.NewLine + FormatBenchmarkResults(childOperationResult, nestingLevel + 1);
            }

            return formattedResults;
        }


        public static void TEST_SERIALIZATION(params  Func<BmComponentSerDesBase<BmOpGroup_SerializeDeserialize>>[]  BenchmarktestCombinations )
        {
            DataCollector.GenerateArrayNCommands1();

          

            BenchmarkProcessor benchmarkProcessor = new BenchmarkProcessor();

            BenchmarkProcessorConfiguration benchmarkProcessorConfiguration = new BenchmarkProcessorConfiguration();
            benchmarkProcessorConfiguration.IfJitOptimizerIsDisabledThenThrowException = false;
            benchmarkProcessorConfiguration.IfDebuggerIsAttachedThenThrowException = false;

            IEnumerable<IBenchmarkComponentResult> benchmarkResults = benchmarkProcessor.Execute(benchmarkProcessorConfiguration, BenchmarktestCombinations);

            foreach (IBenchmarkComponentResult benchmarkComponentResult in benchmarkResults)
                System.Console.WriteLine("Benchmark Component: {0}{1}{2}", benchmarkComponentResult.Name, Environment.NewLine, FormatBenchmarkResults(benchmarkComponentResult.RootOperationResult, 0));
            
            System.Console.ReadLine();

        }


#endregion ------------------------------------- METHODS ---------------------------------------------
        

        }

    



    
}
