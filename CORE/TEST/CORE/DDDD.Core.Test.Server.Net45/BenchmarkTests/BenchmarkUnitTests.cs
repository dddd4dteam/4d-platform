﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;



namespace DDDD.SDBS.Tests
{
    [TestClass]
    public class BenchmarkUnitTests
    {




        [TestMethod]
        public void   BenchmarkTest1()
        {

            // set Benchmark components
            Func<BenchmarkTests.BmComponentSerDesBase<BenchmarkTests.BmOpGroup_SerializeDeserialize>>[] benchmarkComponents =
            {
                //() => new BC_ProtobufNoPrecompiled_SerializeDeserialize( ),
                //() => new BC_ProtobufPrecompiled_SerializeDeserialize( ),

                //() => new BC_KLSNoPrecompiled_SerializeDeserialize( ),
                () => new BenchmarkTests.BC_NewKLSPrecompiled_SerializeDeserialize( ),

            };

            BenchmarkTests.TEST_SERIALIZATION(benchmarkComponents);
        }


        [TestMethod]
        public void BenchmarkTest2()
        {

            // set Benchmark components
            Func<BenchmarkTests.BmComponentSerDesBase<BenchmarkTests.BmOpGroup_SerializeDeserialize>>[] benchmarkComponents =
            {
                //() => new BC_ProtobufNoPrecompiled_SerializeDeserialize( ),
                //() => new BC_ProtobufPrecompiled_SerializeDeserialize( ),

                () => new BenchmarkTests.BC_KLSNoPrecompiled_SerializeDeserialize( ),
                () => new BenchmarkTests.BC_NewKLSPrecompiled_SerializeDeserialize( ),

            };

            BenchmarkTests.TEST_SERIALIZATION(benchmarkComponents);
        }




    }
}
