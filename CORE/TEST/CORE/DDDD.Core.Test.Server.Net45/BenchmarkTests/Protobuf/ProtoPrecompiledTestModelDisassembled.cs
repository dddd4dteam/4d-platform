﻿// Decompiled with JetBrains decompiler
// Type: ProtoPrecompiledModel
// Assembly: ProtobufTester1, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: A372FF2C-9860-4FC0-A279-FA2107F6063E
// Assembly location: D:\NET_CoreXTools\Tests\VirtualCollection.Console.Test\bin\Debug\ProtobufTester1.dll

using ProtoBuf;
using ProtoBuf.Meta;
using System;
using System.Collections;
using System.Linq;

using System.Collections.Generic;
using System.Runtime.InteropServices;

using DDDD.SDS.Tests.Model;
using DDDD.SDS.Tests.Model.CommonModel;




namespace VirtualCollection.Precompile
{

    /// <summary>
    /// This Type Model was disassembled from precompiled by Reflection.Emit way assembly code- to look at the final code that is used as accelerated version of serialization and deserialization  each type from this TypeModel knownTypes.
    /// So we can understand the real end algorithm that accelerate default serialization and deserialization operations. 
    /// </summary>
    public sealed class ProtoPrecompiledTestModelDisassembled : TypeModel
    {
        private static readonly List<Type> knownTypes;

        static ProtoPrecompiledTestModelDisassembled()
        {
            List<Type> typeArray = new List<Type>();//[3];
            int index1 = 0;
            Type type1 = typeof(ContractBase);
            typeArray[index1] = type1;
            int index2 = 1;
            Type type2 = typeof(Product);
            typeArray[index2] = type2;
            int index3 = 2;
            Type type3 = typeof(CommandContainer);
            typeArray[index3] = type3;
            knownTypes = typeArray;
        }

        private static void WriteContractBase([In] ContractBase obj0, [In] ProtoWriter obj1)
        {
            if (obj0.GetType() == typeof(ContractBase))
                return;
            TypeModel.ThrowUnexpectedSubtype(typeof(ContractBase), obj0.GetType());
        }

        private static ContractBase ReadContractBase([In] ContractBase obj0, [In] ProtoReader obj1)
        {
            while (obj1.ReadFieldHeader() > 0)
            {
                if (obj0 == null)
                {
                    ContractBase contractBase = new ContractBase();
                    ProtoReader reader = obj1;
                    ProtoReader.NoteObject((object)contractBase, reader);
                    obj0 = contractBase;
                }
                obj1.SkipField();
            }
            if (obj0 == null)
            {
                ContractBase contractBase = new ContractBase();
                ProtoReader reader = obj1;
                ProtoReader.NoteObject((object)contractBase, reader);
                obj0 = contractBase;
            }
            return obj0;
        }
        


        private static void WriteProduct([In] Product obj0, [In] ProtoWriter obj1)
        {
            if (obj0.GetType() == typeof(Product))
                return;
            TypeModel.ThrowUnexpectedSubtype(typeof(Product), obj0.GetType());
        }

        private static Product ReadProduct([In] Product obj0, [In] ProtoReader obj1)
        {
            while (obj1.ReadFieldHeader() > 0)
            {
                if (obj0 == null)
                {
                    Product product = new Product();
                    ProtoReader reader = obj1;
                    ProtoReader.NoteObject((object)product, reader);
                    obj0 = product;
                }
                obj1.SkipField();
            }
            if (obj0 == null)
            {
                Product product = new Product();
                ProtoReader reader = obj1;
                ProtoReader.NoteObject((object)product, reader);
                obj0 = product;
            }
            return obj0;
        }



        private static void WriteCommandContainer([In] CommandContainer obj0, [In] ProtoWriter obj1)
        {
            if (obj0.GetType() != typeof(CommandContainer))
                TypeModel.ThrowUnexpectedSubtype(typeof(CommandContainer), obj0.GetType());
            DateTime timeStamp = obj0.TimeStamp;
            ProtoWriter.WriteFieldHeader(1, WireType.String, obj1);
            ProtoWriter dest = obj1;
            BclHelpers.WriteDateTime(timeStamp, dest);
            List<ContractBase> products = obj0.Products;
            if (products != null)
            {
                foreach (ContractBase contractBase in products)
                {
                    ProtoWriter.WriteFieldHeader(2, WireType.StartGroup, obj1);
                    ProtoWriter writer = obj1;
                    SubItemToken token = ProtoWriter.StartSubItem((object)contractBase, writer);
                    ProtoWriter protoWriter = obj1;
                    WriteContractBase(contractBase, protoWriter);
                    ProtoWriter.EndSubItem(token, obj1);
                }
            }
            Product product = obj0.Product;
            if (product == null)
                return;
            ProtoWriter.WriteFieldHeader(3, WireType.String, obj1);
            ProtoWriter writer1 = obj1;
            SubItemToken token1 = ProtoWriter.StartSubItem((object)product, writer1);
            ProtoWriter protoWriter1 = obj1;
            WriteProduct(product, protoWriter1);
            ProtoWriter.EndSubItem(token1, obj1);
        }

        private static CommandContainer ReadCommandContainer([In] CommandContainer obj0, [In] ProtoReader obj1)
        {
            int num;
            while ((num = obj1.ReadFieldHeader()) > 0)
            {
                if (num == 1)
                {
                    if (obj0 == null)
                    {
                        CommandContainer commandContainer = new CommandContainer();
                        ProtoReader reader = obj1;
                        ProtoReader.NoteObject((object)commandContainer, reader);
                        obj0 = commandContainer;
                    }
                    DateTime dateTime = BclHelpers.ReadDateTime(obj1);
                    obj0.TimeStamp = dateTime;
                }
                else if (num == 2)
                {
                    if (obj0 == null)
                    {
                        CommandContainer commandContainer = new CommandContainer();
                        ProtoReader reader = obj1;
                        ProtoReader.NoteObject((object)commandContainer, reader);
                        obj0 = commandContainer;
                    }
                    List<ContractBase> list1 = obj0.Products;
                    List<ContractBase> list2 = list1;
                    if (list1 == null)
                        list1 = new List<ContractBase>();
                    int fieldNumber = obj1.FieldNumber;
                    do
                    {
                        List<ContractBase> list3 = list1;
                        // ISSUE: variable of the null type
                        object local = null;
                        SubItemToken token = ProtoReader.StartSubItem(obj1);
                        ProtoReader protoReader = obj1;
                        ContractBase contractBase = ProtoPrecompiledTestModelDisassembled.ReadContractBase((ContractBase)local, protoReader);
                        ProtoReader.EndSubItem(token, obj1);
                        list3.Add(contractBase);
                    }
                    while (obj1.TryReadFieldHeader(fieldNumber));
                    List<ContractBase> list4 = list2 == list1 ? (List<ContractBase>)null : list1;
                    if (list4 != null)
                        obj0.Products = list4;
                }
                else if (num == 3)
                {
                    if (obj0 == null)
                    {
                        CommandContainer commandContainer = new CommandContainer();
                        ProtoReader reader = obj1;
                        ProtoReader.NoteObject((object)commandContainer, reader);
                        obj0 = commandContainer;
                    }
                    Product product1 = obj0.Product;
                    SubItemToken token = ProtoReader.StartSubItem(obj1);
                    ProtoReader protoReader = obj1;
                    Product product2 = ProtoPrecompiledTestModelDisassembled.ReadProduct(product1, protoReader);
                    ProtoReader.EndSubItem(token, obj1);
                    Product product3 = product2;
                    if (product3 != null)
                        obj0.Product = product3;
                }
                else
                {
                    if (obj0 == null)
                    {
                        CommandContainer commandContainer = new CommandContainer();
                        ProtoReader reader = obj1;
                        ProtoReader.NoteObject((object)commandContainer, reader);
                        obj0 = commandContainer;
                    }
                    obj1.SkipField();
                }
            }
            if (obj0 == null)
            {
                CommandContainer commandContainer = new CommandContainer();
                ProtoReader reader = obj1;
                ProtoReader.NoteObject((object)commandContainer, reader);
                obj0 = commandContainer;
            }
            return obj0;
        }



        protected override int GetKeyImpl([In] Type obj0)
        {
            return ProtoPrecompiledTestModelDisassembled.knownTypes.IndexOf(obj0);
        }


        protected override void Serialize([In] int obj0, [In] object obj1, [In] ProtoWriter obj2)
        {
            switch (obj0)
            {
                case 0:
                    ProtoPrecompiledTestModelDisassembled.WriteContractBase((ContractBase)obj1, obj2);
                    break;
                case 1:
                    ProtoPrecompiledTestModelDisassembled.WriteProduct((Product)obj1, obj2);
                    break;
                case 2:
                    ProtoPrecompiledTestModelDisassembled.WriteCommandContainer((CommandContainer)obj1, obj2);
                    break;
            }
        }


        protected override object Deserialize([In] int obj0, [In] object obj1, [In] ProtoReader obj2)
        {
            switch (obj0)
            {
                case 0:
                    return (object)ProtoPrecompiledTestModelDisassembled.ReadContractBase((ContractBase)obj1, obj2);
                case 1:
                    return (object)ProtoPrecompiledTestModelDisassembled.ReadProduct((Product)obj1, obj2);
                case 2:
                    return (object)ProtoPrecompiledTestModelDisassembled.ReadCommandContainer((CommandContainer)obj1, obj2);
                default:
                    return (object)null;
            }
        }


    }

}
