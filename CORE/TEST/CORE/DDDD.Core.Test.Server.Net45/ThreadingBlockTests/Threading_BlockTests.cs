﻿using DDDD.Core.App;
using DDDD.Core.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace DDDD.Core.Net45.Tests.ThreadingBlockTests
{
    /// <summary>
    /// Summary description for ThreadingBlockTests
    /// </summary>
    [TestClass]
    public class Threading_BlockTests
    {
        public Threading_BlockTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



       



        // LazyItem create

        [TestMethod]
        public void Test_4DCore_Threading_LazySlimItem_Parallel_InstancePerIteration_Tasks_Test()
        {
            var internalLocker = new object();
            
            try
            {
               int Iterations = 88;
               int currentIteration = 0;//iteration index to compare counts  at the end                
              

               Action sAction =
               () =>
               {
                  //body(i);  begin
                  try
                  {
                      Interlocked.Increment(ref currentIteration);

                      /// testable LazyItem
                      LazyAct<int> sharedLazyItem = LazyAct<int>.Create((args) =>
                      {
                          int counter = 0;
                          for (int i = 0; i < 100000000; i++)
                          {
                              counter = i + 7;
                          }
                          return counter;
                      }
                      , internalLocker
                      , null
                      );

                      var GetedIntValue = sharedLazyItem.Value;
                      lock (internalLocker)
                      {
                          Console.WriteLine($" CurrentIteration [{currentIteration}] | GettedValue [{GetedIntValue}]");
#if DEBUG
                          Console.WriteLine($" LazyItem Counters Create[{sharedLazyItem.CreateCounter}] | LazyInitValue[{sharedLazyItem.LazyInitCounter}] ");
#endif
                          Console.WriteLine();
                          Console.WriteLine();
                      }
                  }
                  catch (ThreadAbortException texc)
                  {
                      string msg = texc.Message;
                      throw texc;
                  }
                  catch (Exception exc)
                  {
                      string msg = exc.Message;
                      throw exc;
                  }
                  //body(i) end

              };

                //delegates Array 
                Action[] ParallelActions = new Action[Iterations];
                for (int i = 0; i < ParallelActions.Length; i++)
                { ParallelActions[i] = sAction; }

                //run parallel loop for ParallelActions            
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < ParallelActions.Length; i++)
                    {
                        int cur = i;
                        Task.Factory.StartNew(
                            ParallelActions[cur],
                            TaskCreationOptions.AttachedToParent);
                    }
                }
                )
                .Wait();

                int iterationsEnd = currentIteration;

               

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        
        [TestMethod]
        public void Test_4DCore_Threading_LazySlimItem_Parallel_OneSharedInstance_Tasks_Test()
        {
            
            var internalLocker = new object();


            try
            {
                int Iterations = 88;
                int currentIteration = 0;//iteration index to compare counts  at the end                

                /// testable LazyItem
                LazyAct<int> sharedLazyItem = LazyAct<int>.Create((args) =>
                {
                       int counter = 0;
                    for (int i = 0; i < 100000000; i++)
                    {
                        counter = i + 7;
                    }
                    return counter;
                }, null, null
                );

                //var sw = Stopwatch.StartNew();
                //sw.Reset();

                Action sAction =
              () =>
              {
                  //body(i);  begin
                  try
                  {
                      Interlocked.Increment(ref currentIteration);
                      var GetedIntValue = sharedLazyItem.Value;
                      lock (internalLocker)
                      {
                          Console.WriteLine($" CurrentIteration [{currentIteration}] | GettedValue [{GetedIntValue}]");
                      }

                  }
                  catch (ThreadAbortException texc)
                  {
                      string msg = texc.Message;
                      throw texc;
                  }
                  catch (Exception exc)
                  {
                      string msg = exc.Message;
                      throw exc;
                  }
                  //body(i) end

               };

                //delegates Array 
                Action[] ParallelActions = new Action[Iterations];
                for (int i = 0; i < ParallelActions.Length; i++)
                { ParallelActions[i] = sAction; }

                //run parallel loop for ParallelActions            
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < ParallelActions.Length; i++)
                    {
                        int cur = i;
                        Task.Factory.StartNew(
                            ParallelActions[cur],
                            TaskCreationOptions.AttachedToParent);
                    }
                }
                )
                .Wait();

                int iterationsEnd = currentIteration;

#if DEBUG
                         Console.WriteLine($" LazyItem Counters Create[{sharedLazyItem.CreateCounter}] | LazyInitValue[{sharedLazyItem.LazyInitCounter}] ");
#endif
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Threading_LazyItem_Parallel_OneSharedInstance_Tasks_Test()
        {

            try
            {
                int Iterations = 100;
                int currentIteration = 0;//iteration index to compare counts  at the end                

                /// testable LazyItem
                Lazy<int> sharedLazyItem = new Lazy<int>(() =>
                {
                    int counter = 0;
                    for (int i = 0; i < 100000000; i++)
                    {
                        counter = i + 7;
                    }
                    return counter;
                }, isThreadSafe: true
                );

                //var sw = Stopwatch.StartNew();
                //sw.Reset();

                Action sAction =
              () =>
              {
                  //body(i);  begin
                  try
                  {
                      var GetedIntValue = sharedLazyItem.Value;
                      //if action complete successfully increment currentIteration
                      //Interlocked.Increment(ref currentIteration);

                  }
                  catch (ThreadAbortException texc)
                  {
                      string msg = texc.Message;
                      throw texc;
                  }
                  catch (Exception exc)
                  {
                      string msg = exc.Message;
                      throw exc;
                  }
                  //body(i) end

              };

                //delegates Array 
                Action[] ParallelActions = new Action[Iterations];
                for (int i = 0; i < ParallelActions.Length; i++)
                { ParallelActions[i] = sAction; }

                //run parallel loop for ParallelActions            
                Task.Factory.StartNew(() =>
                {
                    for (int i = 0; i < ParallelActions.Length; i++)
                    {
                        int cur = i;
                        Task.Factory.StartNew(
                            ParallelActions[cur],
                            TaskCreationOptions.AttachedToParent);
                    }
                }
                )
                .Wait();

                int iterationsEnd = currentIteration;



            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


    }
}
