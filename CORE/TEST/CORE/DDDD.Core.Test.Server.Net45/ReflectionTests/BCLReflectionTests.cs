﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.BCL.Reflection;
using DDDD.BCL.Extensions;
using System.Collections.Generic;
using System.Diagnostics;
using DDDD.BCL.Serialization;
using DDDD.SDS.Tests.Model.ReflectionModel;
using System.Reflection;
using DDDD.SDS.Tests.Model.CollectionsModel;
using DDDD.SDS.Tests.Model.EnumModel;
using System.Collections;

namespace DDDD.BCL.Net45.Tests.ReflectionTests
{

    [TestClass]
    public class Test_BCL_Reflection_Tests
    {

        [TestMethod]
        public void Test_BCL_Reflection_TypeInfoEx_GetIListArgTypeTests()
        {
            try
            {
               var iListPlaneArgType =  TypeInfoEx.Get_IListArgType(typeof(CustomListPlane));

               var iListGenericArgType = TypeInfoEx.Get_IListArgType(typeof(CustomStringListGeneric));

               var iListGenericInheritedArgType = TypeInfoEx.Get_IListArgType(typeof(CustomStringListGenericInherited));

                var iListPlaneInheritedArgType = TypeInfoEx.Get_IListArgType(typeof(CustomListPlaneInherited));

                var iListStringArgType = TypeInfoEx.Get_IListArgType(typeof(List<string>));
            }
            catch (Exception exc) 
            {
                throw exc;
            }

        }


        [TestMethod]
        public void Test_BCL_Reflection_TypeInfoEx_GetIDictionaryArgTypeTests()
        {
            try
            {
                var iDictionaryPlaneArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryPlaneCollection));

                var iDictionaryStringIntArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryStringIntCollection));

                var iDictionaryStringIListArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryStringIListCollection));

                

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        // 
         
        [TestMethod]
        public void Test_BCL_Reflection_IsNeedToRereadDataType_Test()
        {
            try
            {
                var need1 = typeof(List<SomeTypologyEn>).IsNeedToSureInDataType();
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

        [TestMethod]
        public void Test_BCL_Reflection_IsNullableType_Test()
        {
            try
            {
                var isNullable1 = typeof(List<SomeTypologyEn>).IsNullable();

                var isNullable2 = typeof(SomeTypologyEn).IsNullable();
                var isNullable3 = typeof(SomeTypologyEn?).IsNullable();

                var isNullable4 = typeof(RProductMark).IsNullable();
                var isNullable5 = typeof(RProductMark?).IsNullable();

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }




        [TestMethod]
        public void Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests()
        {
            // Tests CreateInstanceBoxed
            // create string 
            // create int?
            // List<string> - with no args
            // List<string> - with 1arg - 1 value
            // struct ctor with 2args
            // Dictionary<string,int> with 1 arg
            // Dictionary<string,int> with no args

            string testStringValue = (string)null;
            int testIntValue;
            int? testIntNallableValue;// = (int?)null;
            List<string> testListOfstringValue;
            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            Dictionary<string,int> testDictOfStringIntValue;
            Dictionary<string, int> testDictOfStringIntWithDefCountValue = new Dictionary<string, int>(3);// new List<string>(3);

            Class1 testClass1Value;



            var issringNullable = typeof(string).IsNullable();
            try
            {//  string 
                testStringValue = (string)TypeActivator.CreateInstanceBoxed(typeof(string));
            }
            catch (Exception exc)
            { throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(string).FullName}  exception");
            }


            try
            {//  int 
                testIntValue = (int)TypeActivator.CreateInstanceBoxed(typeof(int));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(int).FullName}  exception");
            }



            try
            {//  int? 
                testIntNallableValue = (int?)TypeActivator.CreateInstanceBoxed(typeof(int?));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(int?).FullName}  exception");
            }


            try
            {//  List<string> no args 
                testListOfstringValue = (List<string>)TypeActivator.CreateInstanceBoxed(typeof(List<string>));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(List<string>).FullName}  exception");
            }

            try
            {//  List<string> with Default Capacity arg 
                testListOfstringWithDefCountValue = (List<string>)TypeActivator.CreateInstanceBoxed(typeof(List<string>), TypeActivator.DefaultCtorSearchBinding, 7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(List<string>).FullName} with default capacity  exception");
            }




            try
            {//  Dictionary<string,int> no args 
                testDictOfStringIntValue = (Dictionary<string, int>)TypeActivator.CreateInstanceBoxed(typeof(Dictionary<string, int>));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }

            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testDictOfStringIntWithDefCountValue = (Dictionary<string, int>)TypeActivator.CreateInstanceBoxed(typeof(Dictionary<string, int>), TypeActivator.DefaultCtorSearchBinding,7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Dictionary<string, int>).FullName} with default capacity  exception");
            }




            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testClass1Value = (Class1)TypeActivator.CreateInstanceBoxed(typeof(Class1), TypeActivator.DefaultCtorSearchBinding, "default Name:TestClass1", 23);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Class1).FullName} with default capacity  exception");
            }



            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testClass1Value = (Class1)TypeActivator.CreateInstanceBoxed(typeof(Class1), TypeActivator.DefaultCtorSearchBinding, null, 57);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Class1).FullName} with default capacity  exception");
            }

        }



        [TestMethod]
        public void Test_BCL_Reflection_TypeActivator_CreateInstanceTTests()
        {
            // Tests CreateInstanceBoxed
            // create string 
            // create int?
            // List<string> - with no args
            // List<string> - with 1arg - 1 value
            // struct ctor with 2args
            // Dictionary<string,int> with 1 arg
            // Dictionary<string,int> with no args

            string testStringValue = (string)null;
            int testIntValue;
            int? testIntNallableValue;// = (int?)null;
            List<string> testListOfstringValue;
            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            Dictionary<string, int> testDictOfStringIntValue;
            Dictionary<string, int> testDictOfStringIntWithDefCountValue = new Dictionary<string, int>(3);// new List<string>(3);

            Class1 testClass1Value;



            var issringNullable = typeof(string).IsNullable();
            try
            {//  string 
                testStringValue = TypeActivator.CreateInstanceT<string>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(string).FullName}  exception");
            }


            try
            {//  int 
                testIntValue = TypeActivator.CreateInstanceT<int>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(int).FullName}  exception");
            }



            try
            {//  int? 
                testIntNallableValue = TypeActivator.CreateInstanceT<int?>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(int?).FullName}  exception");
            }


            try
            {//  List<string> no args  
                testListOfstringValue = TypeActivator.CreateInstanceT<List<string>>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(List<string>).FullName}  exception");
            }

            //try
            //{//  List<string> with Default Capacity arg  - in some cases it's not safe variant of creating such object
            //    testListOfstringWithDefCountValue = TypeActivator.CreateInstanceT<List<string>>(args: 7);
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(List<string>).FullName} with default capacity  exception");
            //}




            try
            {//  Dictionary<string,int> no args 
                testDictOfStringIntValue = TypeActivator.CreateInstanceT< Dictionary<string, int>>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }

            //try
            //{//  Dictionary<string,int> with Default Capacity arg   - in some cases it's not safe variant of creating such object
            //    testDictOfStringIntWithDefCountValue = TypeActivator.CreateInstanceT<Dictionary<string, int>>(args: 7);
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(Dictionary<string, int>).FullName} with default capacity  exception");
            //}




            //try
            //{//  Dictionary<string,int> with Default Capacity arg 
            //    testClass1Value = TypeActivator.CreateInstanceT<Class1>(args:new object[] { "default Name:TestClass1", 23 } );
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating { typeof(Class1).FullName} with default capacity  exception");
            //}



            //try
            //{//  Dictionary<string,int> with Default Capacity arg   - in some cases it's not safe variant of creating such object
            //    testClass1Value = TypeActivator.CreateInstanceT<Class1>(args: new object[] { null, 57 });
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_BCL_Reflection_TypeActivator_CreateInstanceTTests) } creating { typeof(Class1).FullName} with default capacity  exception");
            //}



        }


        [TestMethod]
        public void Test_BCL_Reflection_TypeActivator_PrecompileCtorWithLentgAndCreateInstanceT_Tests()
        {
            try
            {
                List<string> listOfStrings = new List<string>();
                var funcofListOfString = TypeActivator.PrecompileAsNewWithLengthExpressionT<List<string>>();

                var newList = funcofListOfString(new object[] { 23 });


                var funcofClass1 = TypeActivator.PrecompileAsNewExpressionT<Class1>();

                var newClass1 = funcofClass1(null);


                var funcPointKindEnum = TypeActivator.PrecompileAsNewExpressionT<PointKindEn>();
                var enumValue = funcPointKindEnum(null);


                var funcRProductMark = TypeActivator.PrecompileAsNewExpressionT<RProductMark>();
                var structValue = funcRProductMark(null);

                var funcStringTwoDimArray = TypeActivator.PrecompileAsNewArrayExpressionT<string[,]>();
                var stringTwoDimArray = funcStringTwoDimArray(new object[] { 10, 10 });


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_BCL_Reflection_IsTypeAssignable()
        {
            try
            {
                var paramType = typeof(char*);
                var argType = typeof(char[]);

                var can1 = paramType.IsAssignableFrom(argType);

                var paramType2 = typeof(IList);
                var argType2 = typeof(List<>);

                var can2 = paramType2.IsAssignableFrom(argType2);







            }
            catch (Exception exc)
            {
                throw exc;
            }

            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }



        [TestMethod]
        public void Test_BCL_Reflection_TypeActivator_CreateInstance()
        {
            try
            {
                var list = new List<string>();

                //var testList =  TypeActivator.CreateInstanceT<List<string>>(TypeActivator.DefaultCtorSearchBinding, 10);

                var value1 = TypeActivator.CreateInstanceT<string>(TypeActivator.DefaultCtorSearchBinding, new char[15]);
                
                //var str = new string()
                //string newstring = new string();

                var callType = TypeInfoEx.GetCallingType();

            }
            catch (Exception exc)
            {
                throw exc;
            }

            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }


        [TestMethod]
        public void Test_BCL_Reflection_TypeActivator_Create_TypeSerializer_of_object_Test()
        {
            try
            {
                var tsSerializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(0);

                //var value1 = TypeActivator.CreateInstanceT<TypeSerializer<object>>(TypeActivator.DefaultCtorSearchBinding, new char[15]);
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        [TestMethod]
        public void Test_BCL_Reflection_GetCallingType_Test1()
        {
            try
            {
                var callType = TypeInfoEx.GetCallingType();

                var currentMethod = MethodInfo.GetCurrentMethod();
                var currentTestClassType = currentMethod.DeclaringType;

            }
            catch (Exception exc)
            {
                throw exc;
            }           
        }



        [TestMethod]
        public void Test_BCL_Reflection_GetCallingType_Test2()
        {
            var callType = TypeInfoEx.GetCallingType();

           
            string[,] arrayDoubledstring = new string[1,1];

           var arrayCtors= arrayDoubledstring.GetType().GetConstructors();

            //arrayDoubledstring
            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }




        [TestMethod]
        public void Test_BCL_Reflection_TypeAQName_Test1()
        {
            var typeInfo = TypeInfoEx.Get(typeof(Test_BCL_Reflection_Tests) );//.AssemblyQualifiedName;
            var typeFounded = typeInfo.TpAQName.LS_FoundType.Value;
                        
            //typeInfo.WorkingType

            TypePrimitiveEn enu1 = TypePrimitiveEn.Boolean;
        }



        [TestMethod]
        public void Test_BCL_Reflection_TypeAQName_ForMultiDimArrays_Test1()
        {
            try
            {               
                
                var typeListIntAQName = TypeAQName.TryParse(typeof(List<int>).AssemblyQualifiedName);//.AssemblyQualifiedName;
                var typeListIntAQTypeName = typeListIntAQName.TypeName;

                var typeListInt = typeof(List<int>);
                var typeListIntTypeName = typeListInt.Name;
                var typeListIntTypeFullName = typeListInt.FullName;


                var typeInt2DimarrayAQName = TypeAQName.TryParse(typeof(int[,]).AssemblyQualifiedName);//.AssemblyQualifiedName;
                var typeInt2DimArray = typeof(int[,]);
                var typeInt2DimArrayTypeName = typeInt2DimArray.Name;
                var typeInt2DimArrayTypeFullName = typeInt2DimArray.FullName;


                //var typeFounded = typeInfo.TpAQName.LS_FoundType.Value;

                //typeInfo.WorkingType

                TypePrimitiveEn enu1 = TypePrimitiveEn.Boolean;
            }
            catch (Exception exc)
            {     throw exc;
            }            
        }

        

        [TestMethod]
        public void Test_BCL_Reflection_IsDisposableSubclassOf_Test1()
        {
            var interfaceOfDC = typeof(DisposableClass).IsDisposable();//"IDisposable");
            var isDisposable = typeof(IDisposable).IsAssignableFrom(typeof(DisposableClass));

           var isDisposable2 = typeof(NotDisposableClass).IsDisposable();
        }




    }
}
