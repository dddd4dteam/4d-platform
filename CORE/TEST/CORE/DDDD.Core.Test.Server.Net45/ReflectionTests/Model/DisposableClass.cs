﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DDDD.BCL.Net45.Tests
{

    public class Class1
    {
        Class1()
        { }

        public Class1(string ct1, int testInt)
        {
            Ct1 = ct1;
            TestInt = testInt;
        }

        public string Ct1
        { get; }
        public int TestInt
        { get; } = -1;


    }


    public class DisposableClass: IDisposable
    {    

        public void Dispose()
        {
            throw new NotImplementedException();
        }

    }


    public class NotDisposableClass //: IDisposable
    {

        //public void Dispose()
        //{
        //    throw new NotImplementedException();
        //}

    }


}
