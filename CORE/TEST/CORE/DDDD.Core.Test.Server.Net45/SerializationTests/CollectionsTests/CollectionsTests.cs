﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using DDDD.Core.TestUtils;
using DDDD.Core.Serialization;
using DDDD.Core.Tests.Model.CommunicationModel;

using DDDD.Core.Reflection;
using DDDD.TSS.Tests.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.StreamMessagesModel;
using DDDD.Core.Tests.Model.CollectionsModel;

//using DDDD.Core.Test;


#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;


#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.Core.TestUtils;
using KellermanSoftware.CompareNetObjects;

#endif

namespace DDDD.TSS.Tests.Collections
{
    [TestClass]
    public class CollectionsTests
    {






        #region --------------------------------------- LIST tests --------------------------------------


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__CommandMessage__withProductInside()
        {
            try
            {
                int Iterations = 10000;
                int currentIteration = 0;//iteration index to compare counts  at the end

                Model1Container.GenerateArrayNCommands1(2, 3);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();
                serializer.AddKnownType<CommandMessage>();
                serializer.AddKnownType<CommandContainer2>();
                //serializer.BuildTypeProcessor ();

                // serialize
                var SerializedDATA = serializer.Serialize(Model1Container.DATACommands);

                // deserialize
                var CompareDesererializeDATA = serializer.Deserialize<List<CommandContainer2>>(SerializedDATA);


                Utility.CompareSerializeResult("Test_Collections_List__CommandMessage__withProductInside "
                                               , Model1Container.DATACommands
                                               , CompareDesererializeDATA);

                // compare This is the comparison class       

                //if action complete succesfully increment currentIteration
                //Interlocked.Increment(ref currentIteration);
                //return serializer;



            }
            catch (Exception exc)
            {
                throw exc;
            }
        }






        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__ContractBase__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };


                List<ContractBase> listofProducts = new List<ContractBase>();
                listofProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(listofProducts);


                List<ContractBase> deserializedList = serializer.Deserialize<List<ContractBase>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_List__ContractBase__withProductInside",
                                                            listofProducts, deserializedList,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__IContractBase__withProductInside()
        {
            try
            {
                IContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };


                List<IContractBase> listofProducts = new List<IContractBase>();
                listofProducts.Add(productItem);


                var serializer =  TypeSetSerializer.AddOrUseExistTSS(0);
                // serializer.AddKnownType<IContractBase>();  interface will be auto detected and prepared to use
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(listofProducts);


                List<IContractBase> deserializedList = serializer.Deserialize<List<IContractBase>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_List__IContractBase__withProductInside",
                                                            listofProducts, deserializedList,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }
        

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__object__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                List<object> listofProducts = new List<object>();
                listofProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);                
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(listofProducts);

                ////here  List<ContractBase> - it doesn't work
                List<object> deserializedList = serializer.Deserialize<List<object>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_List__object__withProductInside",
                                                            listofProducts, deserializedList,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__object__withProductInside_BoxedIntoObject()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                List<object> listofProducts = new List<object>();
                listofProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(listofProducts);

                object deserializedList = serializer.Deserialize<object>(serializedData);

                Utility.CompareSerializeResult("Test_Collections_List__object__withProductInside_BoxedIntoObject",
                                                            listofProducts, deserializedList,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_List__object__withStringInside_BoxedIntoObject()
        {
            try
            {
        
                List<object> listofProducts = new List<object>();
                listofProducts.Add("productItem");

                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                
                byte[] serializedData = serializer.Serialize(listofProducts);

                object deserializedList = serializer.Deserialize<object>(serializedData);

                Utility.CompareSerializeResult("Test_Collections_List__object__withProductInside_BoxedIntoObject",
                                                            listofProducts, deserializedList,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }




        #endregion --------------------------------------- LIST tests --------------------------------------


        #region ---------------------------------- Dictionary  tests -----------------------------------


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Dictionary__IKeyIValue_inside()
        {
            try
            {
                var dictionaryIKeIVal = new Dictionary<IKey, IValue>();
                IKey key1 = new DictKey(325);
                dictionaryIKeIVal.Add(key1, null);

                var dictPlane = (IDictionary)dictionaryIKeIVal;

            }
            catch ( Exception exc)
            {

            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Dictionary__Int_ContractBase__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };
                

                Dictionary<int, ContractBase> dicitonaryOfProducts = new Dictionary<Int32, ContractBase>();
                dicitonaryOfProducts.Add(productItem.Id, productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(dicitonaryOfProducts);

                ////here  Dictionary<Int32, ContractBase> - it wasn't work
                Dictionary<Int32, ContractBase> deserializedDictionary = serializer.Deserialize<Dictionary<Int32, ContractBase>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_Dictionary__Int_ContractBase__withProductInside",
                                                            dicitonaryOfProducts, deserializedDictionary,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Dictionary__Int_IContractBase__withProductInside()
        {
            try
            {
                IContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                Dictionary<Int32, IContractBase> dicitonaryOfProducts = new Dictionary<Int32, IContractBase>();
                dicitonaryOfProducts.Add(productItem.Id, productItem);
                
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                // serializer.AddKnownType<IContractBase>();  interface will be auto detected and prepared to use
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(dicitonaryOfProducts);

                Dictionary<Int32, IContractBase> deserializedDictionary = serializer.Deserialize<Dictionary<Int32, IContractBase>>(serializedData);
                
                Utility.CompareSerializeResult("Test_Collections_Dictionary__Int_IContractBase__withProductInside",
                                                            dicitonaryOfProducts, deserializedDictionary,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }
        

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Dictionary__Int_object__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary"
                };

                Dictionary<Int32, object> dicitonaryOfProducts = new Dictionary<Int32, object>();
                dicitonaryOfProducts.Add(productItem.Id, productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(dicitonaryOfProducts);

                ////here  Dictionary<Int32, ContractBase> - it wasn't work
                Dictionary<Int32, object> deserializedDictionary = serializer.Deserialize<Dictionary<Int32, object>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_Dictionary__Int_object__withProductInside",
                                                            dicitonaryOfProducts, deserializedDictionary,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Dictionary__Int_object__withProductInside_BoxedIntoObject()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary"
                };

                Dictionary<Int32, object> dicitonaryOfProducts = new Dictionary<Int32, object>();
                dicitonaryOfProducts.Add(productItem.Id, productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(dicitonaryOfProducts);

                object deserializedDictionary = serializer.Deserialize<object>(serializedData);

                Utility.CompareSerializeResult("Test_Collections_Dictionary__Int_object__withProductInside_BoxedIntoObject",
                                                            dicitonaryOfProducts, deserializedDictionary,
                                                            TypeMemberSelectorEn.Default);
            }
            catch (Exception)
            {
                throw;
            }
        }





        #endregion ---------------------------------- Dictionary  tests -----------------------------------


        #region ------------------------------------ObservableCollection tests ----------------------------------

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_ObservableCollection__ContractBase__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };


                ObservableCollection<ContractBase> observableCollectionOfProducts = new  ObservableCollection<ContractBase>();
                observableCollectionOfProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(observableCollectionOfProducts);


                ObservableCollection<ContractBase> deserializedObservableCollection = serializer.Deserialize<ObservableCollection<ContractBase>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_ObservableCollection__ContractBase__withProductInside",
                                                            observableCollectionOfProducts, deserializedObservableCollection );


            }
            catch (Exception)
            {
                throw;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_ObservableCollection__object__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                ObservableCollection<object> observableCollectionOfProducts = new ObservableCollection<object>();
                observableCollectionOfProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                //serializer.AddKnownType<ContractBase>(); - you seee that we already need no to register ContractBase contract if we use object as generic collection arg
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(observableCollectionOfProducts);


                ObservableCollection<object> deserializedObservableCollection = serializer.Deserialize<ObservableCollection<object>>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_ObservableCollection__object__withProductInside",
                                                            observableCollectionOfProducts, deserializedObservableCollection);


            }
            catch (Exception)
            {
                throw;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_ObservableCollection__object__withProductInside_BoxedIntoObject()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                ObservableCollection<object> observableCollectionOfProducts = new ObservableCollection<object>();
                observableCollectionOfProducts.Add(productItem);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                //serializer.AddKnownType<ContractBase>(); - you seee that we already need no to register ContractBase contract if we use object as generic collection arg
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(observableCollectionOfProducts);


                object deserializedObservableCollection = serializer.Deserialize<object>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_ObservableCollection__object__withProductInside_BoxedIntoObject",
                                                            observableCollectionOfProducts, deserializedObservableCollection);

            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion ------------------------------------ObservableCollection tests ----------------------------------


        #region ----------------------------------------- ARRAY tests----------------------------------------

        static T[] ReadPlaneArray_From2DimArray<T>(T[,] array2Dim)
        {

            T[] arrayResult = new T[array2Dim.GetLength(0) * array2Dim.GetLength(1)];

            int lastPlaneIndex = 0;

            foreach (var item in array2Dim)
            {
                arrayResult[lastPlaneIndex++] = item;
            }
            
            return arrayResult;
        }


        static T[,] Read2DimArray_FromPlaneArray<T>(T[] planeArrayFrom2Dimarray, int dim1 , int dim2 )
        {

            T[,] arrayResult = new T[dim1, dim2];

            int lastPlaneIndex = 0;
            for (int i = 0; i < dim1; i++)
            {
                for (int j = 0; j < dim2; j++)
                {
                    arrayResult[i, j] = planeArrayFrom2Dimarray[lastPlaneIndex++];
                    //lastPlaneIndex++;
                }
            }

            return arrayResult;
        }


        [TestMethod]//Test
        public void Test_4DCore_Serialization_Collections_Array_IntArray_Arrays()
        {
            try
            {
                var intArrayMessage = new IntArrayMessage();
                //intArrayMessage
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]//Test
        public void Test_4DCore_Serialization_Collections_Array_int_1_2Dim_Arrays()
        {
            try
            {
                int[] array1 = new int[5] {1,2,3,4,5 };

                int[,] array2Dim = new int[8 ,2] {   { 2, 4}, { 3, 9}, { 4, 16}, { 5, 25}, { 6, 36}, { 7, 49}, { 8, 64}, { 9, 81}   };
                

                var planeArrayFrom2Darray =ReadPlaneArray_From2DimArray(array2Dim);
                var again2DimArrayFromPlaneArray = Read2DimArray_FromPlaneArray(planeArrayFrom2Darray, 8, 2);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                var array2DimSerialized =  serializer.Serialize(array2Dim);

                var deserialized2DimArray = serializer.Deserialize<int[,]>(array2DimSerialized);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]//Test
        public void Test_4DCore_Serialization_Collections_Array_ContractBase__withProductInside()
        {
            try
            {
                ContractBase productItem = new Product()
                {
                    Id = 1,
                    Name = "Product1",
                    Category = "Primary" //Price = 12500,
                };

                ContractBase[] arrayOfProducts = new ContractBase[1];

                arrayOfProducts[0] = productItem;

                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(arrayOfProducts);


                ContractBase[] deserializedArrayOfProducts = serializer.Deserialize<ContractBase[]>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_Array__ContractBase__withProductInside",
                                                            arrayOfProducts, deserializedArrayOfProducts);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Array2Dimension_ContractBase__withProductInside()
        {
            try
            {
                var productItem11 = new Product()
                {
                    Id = 11,
                    Name = "Product11",
                    Category = "Primary" //Price = 12500,
                };

                var productItem22 = new Product()
                {
                    Id = 22,
                    Name = "Product22",
                    Category = "Primary" //Price = 12500,
                };

                var productItem33 = new Product()
                {
                    Id = 33,
                    Name = "Product33",
                    Category = "Primary" //Price = 12500,
                };



                ContractBase[,] array2DOfProducts = new ContractBase[3,3];

                array2DOfProducts[0, 0] = productItem11;
                array2DOfProducts[1, 1] = productItem22;
                array2DOfProducts[2, 2] = productItem33;


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();

                byte[] serializedData = serializer.Serialize(array2DOfProducts);


                ContractBase[,] deserializedArray2Dimension = serializer.Deserialize<ContractBase[,]>(serializedData);


                Utility.CompareSerializeResult( "Test_Collections_Array2Dimension_ContractBase__withProductInside",
                                                            array2DOfProducts, deserializedArray2Dimension );


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Array3Dimension_ContractBase__withProductInside()
        {
            try
            {
                var productItem11 = new Product()
                {
                    Id = 11,
                    Name = "Product11",
                    Category = "Primary11" //Price = 12500,
                };

                var productItem22 = new Product()
                {
                    Id = 22,
                    Name = "Product22",
                    Category = "Primary22" //Price = 12500,
                };

                var productItem33 = new Product()
                {
                    Id = 33,
                    Name = "Product33",
                    Category = "Primary33" //Price = 12500,
                };

                var productItem44 = new Product()
                {
                    Id = 44,
                    Name = "Product44",
                    Category = "Primary44" //Price = 12500,
                };


                ContractBase[,,] array3DOfProducts = new ContractBase[2,2,2];

                var arrayType = array3DOfProducts.GetType();// Rank
                var arrayRank = arrayType.GetArrayRank();

                array3DOfProducts[0, 0, 1] = productItem11;
                array3DOfProducts[0, 1, 0] = productItem22;
                array3DOfProducts[1, 0, 0] = productItem33;
                array3DOfProducts[1, 0, 1] = productItem44;
                

                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<ContractBase>();
                serializer.AddKnownType<Product>();
                serializer.AddKnownType<ProductMark>();


                byte[] serializedData = serializer.Serialize(array3DOfProducts);


                ContractBase[, ,] deserializedArray3D = serializer.Deserialize<ContractBase[, ,]>(serializedData);


                Utility.CompareSerializeResult( "Test_Collections_Array3Dimension_ContractBase__withProductInside" ,
                                                            array3DOfProducts, deserializedArray3D );
                
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_CustomSerializeMessageWithArray3D_Int()
        {
            try
            {
                CustomSerializersMessage message = new CustomSerializersMessage();
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
                serializer.AddKnownType<CustomSerializersMessage>();

                byte[] serializedData = serializer.Serialize(message);

                MessageBase deserializedArrayOfProducts = serializer.Deserialize<MessageBase>(serializedData);
                
                Utility.CompareSerializeResult(nameof(Test_4DCore_Serialization_Collections_CustomSerializeMessageWithArray3D_Int),
                                                            message, deserializedArrayOfProducts);
            }
            catch (Exception exc)
            { throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Array_byteArrayFromString()
        {
            try
            {
                String stringOriginal = "SDS(Service Domain Serializer) flexible, based on service domain contract dictionary, binary serializer. "
                                      + "SDS designed to be used in multithreading and TPL support."
                                      + "You can load SDS Project and its documentation from Bitbucket source host - open ";
                              
                byte[] stringByteArray =  Encoding.UTF8.GetBytes( stringOriginal);


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);


                byte[] serializedData = serializer.Serialize(stringByteArray);


                byte[] deserializedStringByteArray = serializer.Deserialize<byte[]>(serializedData);


                string convertedFromSerialized = Encoding.UTF8.GetString(deserializedStringByteArray,0, deserializedStringByteArray.Length);


                Utility.CompareSerializeResult("Test_Collections_Array_byteArrayFromString",
                                                            stringOriginal, convertedFromSerialized     );


            }
            catch (Exception exc )
            {
                throw exc;
            }
        }


        #endregion ----------------------------------------- ARRAY tests----------------------------------------



        #region ----------------------------------------- BITARRAY tests----------------------------------------

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_BitArray()
        {
            try
            {                

                BitArray  arrayOfBits = new BitArray(10);
                arrayOfBits[0] = true;
                arrayOfBits[1] = false;
                arrayOfBits[2] = true;
                arrayOfBits[3] = false;
                arrayOfBits[4] = false;
                arrayOfBits[5] = true;
                arrayOfBits[6] = false;
                arrayOfBits[7] = true;
                arrayOfBits[8] = true;
                arrayOfBits[9] = false;
                
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

                byte[] serializedData = serializer.Serialize(arrayOfBits);


                BitArray deserializedBitArray = serializer.Deserialize<BitArray>(serializedData);
                 
                Utility.CompareSerializeResult("Test_Collections_BitArray", arrayOfBits, deserializedBitArray);

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion ----------------------------------------- BITARRAY tests----------------------------------------
        


        #region ---------------------------------------- CUSTOM COLLECTIONS ---------------------------------------


        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Custom_TableFieldCollection()
        {
            try
            {
                FieldCollection fieldCollection = new FieldCollection();
                fieldCollection.Add(new Field { Name = "StringField", DataType = DbType.String });
                fieldCollection.Add(new Field { Name = "IntField", DataType = DbType.Int32 });
                fieldCollection.Add(new Field { Name = "TimeField", DataType = DbType.Time });

                var table = new Table() { Name = "Table1" };
                table.Fields = fieldCollection;


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
#if NET45                
                //serializer.UseGenerationOfDebugCodeAssembly = true;
#endif
                serializer.AddKnownType<Table>();
                serializer.AddKnownType<Field>();
                serializer.AddKnownType<FieldCollection>();

                byte[] serializedData = serializer.Serialize(table);
                //so we can see that adding IList based FieldCollection  was not needable - it going automatically
                // in the AddKnownType we need only add custom Classes and Structs. 
                //Serializer will add custom enum/interfaces/collections automatically


                Table deserializedTable = serializer.Deserialize<Table>(serializedData);


                Utility.CompareSerializeResult("Test_Collections_Custom_TableFieldCollection",
                                                            table, deserializedTable);



            }
            catch (Exception)
            {                
                throw;
            }
        }


        #endregion ---------------------------------------- CUSTOM COLLECTIONS ---------------------------------------





    }
}
