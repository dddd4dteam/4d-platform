﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.Serialization;
using System;

using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.TestUtils;
using DDDD.TSS.Tests.Model.CommunicationModel;
using System.Collections.Generic;
using DDDD.Core.Net.ServiceModel;
using System.IO;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.Core.Net;
using DDDD.Core.ComponentModel.Messaging;
using DDDD.Core.DAL.AdventureWorksDB.NET45.DCMessaging;

namespace DDDD.TSS.Tests.ComplexTypes
{

    #region Additional test attributes
    //
    // You can use the following additional attributes as you write your tests:
    //
    // Use ClassInitialize to run code before running the first test in the class
    // [ClassInitialize()]
    // public static void MyClassInitialize(TestContext testContext) { }
    //
    // Use ClassCleanup to run code after all tests in a class have run
    // [ClassCleanup()]
    // public static void MyClassCleanup() { }
    //
    // Use TestInitialize to run code before running each test 
    // [TestInitialize()]
    // public void MyTestInitialize() { }
    //
    // Use TestCleanup to run code after each test has run
    // [TestCleanup()]
    // public void MyTestCleanup() { }
    //
    #endregion

   
    

    /// <summary>
    /// Summary description for SerializationTest
    /// </summary>
    [TestClass]
    public class SerializationTests
    {
        public SerializationTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }



        #region ----------------------------- SET  Function HELPERS -------------------------------
        

        public static void Set__RefCopyrighter(ref ProductMark? instance, object value)
        {
            if (!instance.HasValue)
                return;
            ProductMark productMark = instance.Value;
            productMark.Copyrighter = (string)value;
            ProductMark? nullable = new ProductMark?(productMark);
            instance = nullable;
        }


        public static void Set__RefCopyrighter(ref object instance, object value)
        {

            if (instance == null)
                return;

            var structValue = (ProductMark)instance;
            structValue.Copyrighter = (string)value;
            instance = structValue;// .Copyrighter = (string)value;

        }


        public static void Set__RefCopyrighter(ref ProductMark instance, object value)
        {
            instance.Copyrighter = (string)value;// .Copyrighter = (string)value;
        }


        #endregion ----------------------------- SET  Function HELPERS -------------------------------






        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_AddUpdateTypeSetSerializerTest()
        {
            try
            {
                var tsSerializer = TypeSetSerializer.AddUpdateTSS("Key1");

                tsSerializer.AddKnownType<Product>();

                //var dataItem = new Product("new Value ", 345);

                // var ifCanSerialize = tsSerializer.CanSerialize(dataItem, throwOnNullVlue: false);

                //var serializedData = tsSerializer.Serialize(dataItem);

                //var deserialisedInstance = tsSerializer.Deserialize<Product>(serializedData);

            }
            catch (Exception exc)
            {
                throw exc; 
            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_DetectNullableTypeTest()
        {
            bool? newBooleanNullable;// = null;/// false;

            //var tp1 = newBooleanNullable.GetType();

            var tp = typeof(bool?);//  newBooleanNullable.GetType();
        }


        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_SimpleStructTest()
        {
            try
            {
                //serializer
            
                var tsSerializer = TypeSetSerializer.AddUpdateTSS("Key1");

                tsSerializer.AddKnownType<ProductMark>();

                var dataItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1" };

                // var ifCanSerialize = tsSerializer.CanSerialize(dataItem, throwOnNullVlue: false);

                var serializedData = tsSerializer.Serialize(dataItem);

                var deserialisedInstance = tsSerializer.Deserialize<ProductMark>(serializedData);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        

        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_NullableStructAsFieldTest()
        {
            try
            {
                //serializer
                var tsSerializer = TypeSetSerializer.AddUpdateTSS("Key1");

                tsSerializer.AddKnownType<Product2>();
                tsSerializer.AddKnownType<ProductMark>();

                var dataItem1 = new Product2() { Mark = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1" } };
                var dataItem2 = new Product2();

                var serializedData1 = tsSerializer.Serialize(dataItem1);
                var deserialisedInstance1 = tsSerializer.Deserialize<Product2>(serializedData1);
                Utility.CompareSerializeResult(nameof(Test_4DCore_Serialization_ComplexType_NullableStructAsFieldTest), dataItem1, deserialisedInstance1, TypeMemberSelectorEn.Default);


                var serializedData2 = tsSerializer.Serialize(dataItem2);
                var deserialisedInstance2 = tsSerializer.Deserialize<Product2>(serializedData2);
                Utility.CompareSerializeResult(nameof(Test_4DCore_Serialization_ComplexType_NullableStructAsFieldTest), dataItem2, deserialisedInstance2, TypeMemberSelectorEn.Default);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
        


        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_TestClassWithListOfEnumsTest()
        {
            try
            {

                EnumTestClass enumClass = new EnumTestClass();
                // enum field
                enumClass.EnumFieldPublic = SomeTypologyEn.Peoples;
                // enum collection
                enumClass.AllTypologyItemsPublic.Add(SomeTypologyEn.Reptiles);
                enumClass.AllTypologyItemsPublic.Add(SomeTypologyEn.Birds);
                enumClass.AddNewTypologyItemPrivate(SomeTypologyEn.Plants);

                //serializer
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

                //3 добавили контракт должны сериализоваться только паблик элементы 
                serializer.AddKnownType<EnumTestClass>();
                serializer.AddKnownType<NotKnownEnumEn>(); // separate because it;ll be boxed into object

                var dataSerialized3 = serializer.Serialize(enumClass);
                var testDeserialized = serializer.Deserialize<EnumTestClass>(dataSerialized3);

                Utility.CompareSerializeResult(nameof(Test_4DCore_Serialization_ComplexType_TestClassWithListOfEnumsTest), enumClass, testDeserialized, TypeMemberSelectorEn.PublicOnly);

            }
            catch (Exception exc)
            {
                //
                throw exc;
            }

        }
        


        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_SimpleStructClass_Test()
        {

            ProductMark? variablePoductMark = new ProductMark() { Mark = "DDDD.SDS V3" };
            Set__RefCopyrighter(ref variablePoductMark, "Copiright 2015 by DDDD");

            var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
            serializer.AddKnownType<ProductMark>(); //custom adding Category contract
            serializer.AddKnownType<Product>();      //custom adding Category contract 
            serializer.AddKnownType<Product2>();     //custom adding Category contract  
            serializer.AddKnownType<Product3>();     //custom adding Category contract  


            var prMark = new ProductMark() { Mark = "DDDD.SDS V3", Copyrighter = "Copiright 2015 by DDDD" };
            var serData = serializer.Serialize(prMark);
            var deserPMark = serializer.Deserialize<ProductMark>(serData);
            Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test -  ProductMark", prMark, deserPMark);


            ProductMark? prMarkNullable = new ProductMark() { Mark = "DDDD.SDS V3", Copyrighter = "Copiright 2015 by DDDD" };
            var serDataNullable = serializer.Serialize(prMarkNullable, true);
            var deserPMarkNullable = serializer.Deserialize<ProductMark?>(serDataNullable);
            Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test -  ProductMark?", prMarkNullable, deserPMarkNullable);



            var prdct = new Product2() { Mark = new ProductMark() { Mark = "Mercedes 2015C", Copyrighter = "Copiright 2015 by DDDD" } };
            var serzdProdct = serializer.Serialize(prdct);
            var deszdPrdct = serializer.Deserialize<Product2>(serzdProdct);
            Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - Product2", prdct, deszdPrdct);


            var product3 = new Product3()
            {
                Id = 12,
                Category = "Mobiles",
                Name = "Tandemus",
                PartsNames = new List<string>() { "Part 1 Name", "Part 2 Name", "Part 3 Name" }
                                          ,
                PartsProductMarks = new List<ProductMark>()
                                          {new ProductMark() { Mark = "Mercedes 2015C", Copyrighter = "Copiright 2015 by DDDD" }
                                              ,new ProductMark() { Mark = "Lamborgini 2015C", Copyrighter = "Copiright 2015 by DDDD SDS" }
                                          }
            };

            var serzdProduct3 = serializer.Serialize(product3);
            var deszdProduct3 = serializer.Deserialize<Product3>(serzdProduct3);
            Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - Product3", product3, deszdProduct3);


        }


        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_GenericAlgorithmDoc_Test()
        {

            var serializer = TypeSetSerializer.AddOrUseExistTSS(0);//you can use also [StringKey] or [GuidKey]
            serializer.AddKnownType<ProductMark>();
            serializer.AddKnownType<Product>();
#if NET45
            //serializer.UseGenerationOfDebugCodeAssembly = true; //generate debug code assembly
#endif


            var product = new Product()
            {
                Category = "Category"
                ,
                Id = 32
                ,
                Name = "Bicycle"
                ,
                PartsNames = new List<string>() { "PartName1", "PartName2" }
                ,
                Price = (decimal)34.6
                ,
                Length = 32
                ,
                Mark = new ProductMark() { Mark = "Mercedes 2015C", Copyrighter = "Copiright 2015 by DDDD" }
                ,
                PartsProductMarks = new List<ProductMark>()
                                          {    new ProductMark() { Mark = "Mercedes 2015C", Copyrighter = "Copiright 2015 by DDDD" }
                                              ,new ProductMark() { Mark = "Lamborgini 2015C", Copyrighter = "Copiright 2015 by DDDD SDS" }
                                          }
            };

            var serzdProduct = serializer.Serialize(product);
            var deszdProduct = serializer.Deserialize<Product>(serzdProduct);
            Utility.CompareSerializeResult("Test_Primitives_GenericAlgorithmDoc_Test", product, deszdProduct);



        }



        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_DCommmandMessage_Test()
        {

            try
            {
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);//you can use also [StringKey] or [GuidKey]
                serializer.AddKnownType<CommandMessage>();
                serializer.AddKnownType<Core.ComponentModel.Messaging.DCMessage>();

                //var idClient = new Guid("7670C94C-E82C-408E-A6E9-08F0CA7A757F");
                //var cmdLoadClientReport = Core.ComponentModel.Messaging.DCMessage.Create(
                //     "ClientReport_CommandManager"              // targetCommandManager:
                //    , "ClientReport_CommandManager".GetHashCode()
                //    , "LoadClientReport"                        // command: 
                //    , "Id_Client".KVPair<object>(idClient)      //  commandParameters: 
                //    );


                //var serdData = serializer.Serialize(cmdLoadClientReport);
                //var desData = serializer.Deserialize<DCMessage>(serdData);             
               

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

    }
}
