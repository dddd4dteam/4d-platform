﻿using System;
using System.Collections.Generic;
using DDDD.Core.Serialization;
using DDDD.Core.TestUtils;
using DDDD.TSS.Tests.Model.CommunicationModel;



//using DDDD.Core.Test;





#if NET45
using Microsoft.VisualStudio.TestTools.UnitTesting;
#elif SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;

using DDDD.Core.Tests.Model.CommunicationModel;
using System.Reflection.Emit;
using DDDD.Core.TestUtils;


#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.Core.TestUtils;
#endif


namespace DDDD.Core.Tests.SerializationTests
{
    [TestClass]
    public class PrimitivesTests
    {

        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_BuildingDebugProcessingCode()
        {

            //default types- primitives
            //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   
            
            var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

            //serializer.AddKnownType<>
            //serializer.BuildTypeProcessor ();

        }





        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_PrimitivesTest()
        {
            try
            {
                //default types- primitives
                //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   


                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);   
                //serializer.BuildTypeProcessor ();

                //bool
                var boolVariable = true;// serializer.DefaultValues.Add(typeof(bool), default(bool));
                var boolVariableSerialized = serializer.Serialize(boolVariable);
                var boolVariableDeserialized = serializer.Deserialize<bool>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", boolVariable, boolVariableDeserialized);


                //byte
                var byteVariable = byte.MaxValue;// serializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = serializer.Serialize(byteVariable);
                var byteVariableDeserialized = serializer.Deserialize<byte>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Byte", byteVariable, byteVariableDeserialized);


                //char
                var charVariable = char.MaxValue; //serializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = serializer.Serialize(charVariable);
                var charVariableDeserialized = serializer.Deserialize<char>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Char", charVariable, charVariableDeserialized);


                //decimal
                var decimalVariable = decimal.MaxValue; //serializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = serializer.Serialize(decimalVariable);
                var decimalVariableDeserialized = serializer.Deserialize<decimal>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - decimal", decimalVariable, decimalVariableDeserialized);

                //double
                var doubleVariable = double.MaxValue;// serializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = serializer.Serialize(doubleVariable);
                var doubleVariableDeserialized = serializer.Deserialize<double>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - double", byteVariable, byteVariableDeserialized);


                //float
                var floatVariable = float.MaxValue;// serializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = serializer.Serialize(floatVariable);
                var floatVariableDeserialized = serializer.Deserialize<float>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - float", floatVariable, floatVariableDeserialized);

                
                //int
                var intVariable = int.MaxValue;// serializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = serializer.Serialize(intVariable);
                var intVariableDeserialized = serializer.Deserialize<int>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - int", intVariable, intVariableDeserialized);

                
                //long
                var longVariable = long.MaxValue;// serializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = serializer.Serialize(longVariable);
                var longVariableDeserialized = serializer.Deserialize<long>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - long", longVariable, longVariableDeserialized);

                
                //sbyte
                var sbyteVariable = sbyte.MaxValue;// serializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = serializer.Serialize(sbyteVariable);
                var sbyteVariableDeserialized = serializer.Deserialize<sbyte>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - sbyte", sbyteVariable, sbyteVariableDeserialized);

                
                //short
                var shortVariable = short.MaxValue;// serializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = serializer.Serialize(shortVariable);
                var shortVariableDeserialized = serializer.Deserialize<short>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - short", shortVariable, shortVariableDeserialized);

                
                //string
                var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                var stringVariableSerialized = serializer.Serialize(stringVariable);
                var stringVariableDeserialized = serializer.Deserialize<string>(stringVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - string", stringVariable, stringVariableDeserialized);

                
                //uint
                var uintVariable = uint.MaxValue;// serializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = serializer.Serialize(uintVariable);
                var uintVariableDeserialized = serializer.Deserialize<uint>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - uint", uintVariable, uintVariableDeserialized);

                
                //ulong
                var ulongVariable = ulong.MaxValue;// serializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = serializer.Serialize(ulongVariable);
                var ulongVariableDeserialized = serializer.Deserialize<ulong>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", ulongVariable, ulongVariableDeserialized);

                
                //ushort
                var ushortVariable = ushort.MaxValue;// serializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = serializer.Serialize(ushortVariable);
                var ushortVariableDeserialized = serializer.Deserialize<ushort>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - ushort", ushortVariable, ushortVariableDeserialized);

                
                //DateTime
                var datetimeVariable = DateTime.MaxValue;// serializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var datetimeVariableSerialized = serializer.Serialize(datetimeVariable);
                var datetimeVariableDeserialized = serializer.Deserialize<DateTime>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - datetime", datetimeVariable, datetimeVariableDeserialized);

                
                //TimeSpan
                var timespanVariable = TimeSpan.MaxValue;// serializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = serializer.Serialize(timespanVariable);
                var timespanVariableDeserialized = serializer.Deserialize<TimeSpan>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - timeSpan", timespanVariable, timespanVariableDeserialized);
                
                
                //Guid
                var guidVariable = Guid.Empty;// serializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = serializer.Serialize(guidVariable);
                var guidVariableDeserialized = serializer.Deserialize<Guid>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Guid", guidVariable, guidVariableDeserialized);

                
                //Uri
                var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. serializer.DefaultValues.Add(typeof(Uri), default(Uri));
                var uriVariableSerialized = serializer.Serialize(uriVariable);
                var uriVariableDeserialized = serializer.Deserialize<Uri>(uriVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Uri", uriVariable, uriVariableDeserialized);
                
            }
            catch (Exception)
            {                
                throw;
            }
            
        }
               

        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_NullablePrimitives_Test()
        {
            try
            {
                var serializer = TypeSetSerializer.AddOrUseExistTSS(0);


                //bool
                bool? boolNullableVariable = true;// = null;
                var boolVariableSerialized = serializer.Serialize(boolNullableVariable, true);
                var boolVariableDeserialized = serializer.Deserialize<bool?>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - bool?", boolNullableVariable, boolVariableDeserialized );


                //byte
                byte? byteVariable = byte.MaxValue;// serializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = serializer.Serialize(byteVariable, true);
                var byteVariableDeserialized = serializer.Deserialize<byte?>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - byte?", byteVariable, byteVariableDeserialized);


                //char
                char? charVariable = char.MaxValue; //serializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = serializer.Serialize(charVariable,true);
                var charVariableDeserialized = serializer.Deserialize<char?>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - char?", charVariable, charVariableDeserialized );


                //decimal
                decimal? decimalVariable = decimal.MaxValue; //serializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = serializer.Serialize(decimalVariable, true);
                var decimalVariableDeserialized = serializer.Deserialize<decimal?>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - decimal?", decimalVariable, decimalVariableDeserialized );
                

                //double
                double? doubleVariable = double.MaxValue;// serializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = serializer.Serialize(doubleVariable, true);
                var doubleVariableDeserialized = serializer.Deserialize<double?>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - double?", doubleVariable, doubleVariableDeserialized );


                //float
                float? floatVariable = float.MaxValue;// serializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = serializer.Serialize(floatVariable, true);
                var floatVariableDeserialized = serializer.Deserialize<float?>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - float?", floatVariable, floatVariableDeserialized );


                //int
                int? intVariable = int.MaxValue;// serializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = serializer.Serialize(intVariable, true);
                var intVariableDeserialized = serializer.Deserialize<int?>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - int?", intVariable, intVariableDeserialized );


                //long
                long? longVariable = long.MaxValue;// serializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = serializer.Serialize(longVariable, true);
                var longVariableDeserialized = serializer.Deserialize<long?>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - long?", longVariable, longVariableDeserialized );


                //sbyte
                sbyte? sbyteVariable = sbyte.MaxValue;// serializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = serializer.Serialize(sbyteVariable, true);
                var sbyteVariableDeserialized = serializer.Deserialize<sbyte?>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - sbyte?", sbyteVariable, sbyteVariableDeserialized );


                //short
                short? shortVariable = short.MaxValue;// serializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = serializer.Serialize(shortVariable, true);
                var shortVariableDeserialized = serializer.Deserialize<short?>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - short?", shortVariable, shortVariableDeserialized );

                
                //uint
                uint? uintVariable = uint.MaxValue;// serializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = serializer.Serialize(uintVariable, true);
                var uintVariableDeserialized = serializer.Deserialize<uint?>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - uint?", uintVariable, uintVariableDeserialized);


                //ulong
                ulong? ulongVariable = ulong.MaxValue;// serializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = serializer.Serialize(ulongVariable, true);
                var ulongVariableDeserialized = serializer.Deserialize<ulong?>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ulong?", ulongVariable, ulongVariableDeserialized);


                //ushort
                ushort? ushortVariable = ushort.MaxValue;// serializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = serializer.Serialize(ushortVariable, true);
                var ushortVariableDeserialized = serializer.Deserialize<ushort?>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ushort?", ushortVariable, ushortVariableDeserialized);


                //DateTime
                DateTime? datetimeVariable = DateTime.MaxValue;// serializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var datetimeVariableSerialized = serializer.Serialize(datetimeVariable, true);
                var datetimeVariableDeserialized = serializer.Deserialize<DateTime?>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - DateTime?", datetimeVariable, datetimeVariableDeserialized);


                //TimeSpan
                TimeSpan? timespanVariable = TimeSpan.MaxValue;// serializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = serializer.Serialize(timespanVariable, true);
                var timespanVariableDeserialized = serializer.Deserialize<TimeSpan?>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - TimeSpan?", timespanVariable, timespanVariableDeserialized);


                //Guid
                Guid? guidVariable = Guid.Empty;// serializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = serializer.Serialize(guidVariable, true);
                var guidVariableDeserialized = serializer.Deserialize<Guid?>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - Guid?", guidVariable, guidVariableDeserialized );


                ////Uri
                //var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. serializer.DefaultValues.Add(typeof(Uri), default(Uri));
                //var uriVariableSerialized = serializer.Serialize(uriVariable);
                //var uriVariableDeserialized = serializer.Deserialize<Uri>(uriVariableSerialized);


                ////string
                //var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                //var stringVariableSerialized = serializer.Serialize(stringVariable);
                //var stringVariableDeserialized = serializer.Deserialize<string>(stringVariableSerialized);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


    }







}





