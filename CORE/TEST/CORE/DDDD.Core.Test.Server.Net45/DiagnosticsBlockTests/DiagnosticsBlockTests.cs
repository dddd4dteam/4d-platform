﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using DDDD.Core.Diagnostics;
using System.Threading;

namespace DDDD.Core.Net45.Tests.TraceTests
{
    [TestClass]
    public class Test_4DCore_Diagnostics_Tests
    {
        [TestMethod]
        public void  Test_4DCore_Diagnostics_ConsoleTraceInParallel_Test()
        {
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;
            int traceLines = 0;

            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = System.Environment.ProcessorCount;

            Parallel.For<Int32>(0, Iterations, options,
                    () =>  //localInit
                    {
                              
                        //currentIteration = 0;
                        //ConsoleTracer.TraceInfoDbg<TraceTests>("ConsoleTraceInParallel_Test", "traceCounter: [{0}]", (++traceLines).ToString() );
                        //return TypeSetSerializer.GetDirect(0);//serializer; 
                        return currentIteration;
                    }
                    ,
                    (ind, pstate, slzr) =>
                    {
                        try
                        {
                            ConsoleTracer.TraceInfoDbg<Test_4DCore_Diagnostics_Tests>("ConsoleTraceInParallel_Test", "traceCounter: [{0}]", (++traceLines).ToString());
                            
                            Interlocked.Increment(ref currentIteration);
                            
                            ConsoleTracer.TraceInfoDbg<Test_4DCore_Diagnostics_Tests>("ConsoleTraceInParallel_Test", "currentIteration: [{0}]", (++currentIteration).ToString());

                            return currentIteration;                          

                        }
                        catch (ThreadAbortException texc)
                        {
                            string msg = texc.Message;
                            throw texc;
                        }
                        catch (Exception exc)
                        {
                            string msg = exc.Message;
                            throw exc;
                        }
                        return slzr;
                    },
                    (s) =>
                    {
                        try
                        {


                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        //Interlocked.Increment(ref currentIteration);
                    }

                );

            //ConsoleTracer.TraceInfoDbg<TraceTests>("ConsoleTraceInParallel_Test"
            //                                        , "traceCounter: [{0}]",  (++traceLines).ToString());
        }



        [TestMethod]
        public void Test_4DCore_Diagnostics_ConsoleTrace_AssertionTest1_Test()
        {
            try
            {
                //ConsoleTracer.AssertDbg<Test_4DCore_Diagnostics_Tests>((123 > 150), nameof(Test_4DCore_Diagnostics_ConsoleTrace_AssertionTest1_Test), "Check_(123 > 150)");

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
