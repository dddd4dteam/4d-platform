﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq.Expressions;
using System.Reflection;

using DDDD.BCL.Extensions;
using DDDD.BCL.Reflection;

namespace DDDD.BCL.Net45.Tests.BaseTests
{

    public static class MethodSupport<T>
    {
        public static string ActionName(Expression<Func<T, Action>> expression)
        {
            return MethodName(expression);
        }

        public static string ActionName<TParam>(Expression<Func<T, Action<TParam>>> expression)
        {
            return MethodName(expression);
        }

        public static string FuncName<TResult>(Expression<Func<T, Func<TResult>>> expression)
        {
            return MethodName(expression);
        }

        public static string FuncName<TParam, TResult>(Expression<Func<T, Func<TParam, TResult>>> expression)
        {
            return MethodName(expression);
        }

        private static bool IsNET45 = Type.GetType("System.Reflection.ReflectionContext", false) != null;

        public static string MethodName(LambdaExpression expression)
        {
            var unaryExpression = (UnaryExpression)expression.Body;
            var methodCallExpression = (MethodCallExpression)unaryExpression.Operand;

            if (IsNET45)
            {
                var methodCallObject = (ConstantExpression)methodCallExpression.Object;
                var methodInfo = (MethodInfo)methodCallObject.Value;
                return methodInfo.Name;
            }
            else
            {
                var methodInfoExpression = (ConstantExpression)methodCallExpression.Arguments.Last();
                var methodInfo = (MemberInfo)methodInfoExpression.Value;
                return methodInfo.Name;
            }

        }
    }




    [TestClass]
    public class BCLBaseTests
    {
        [TestMethod]
        public static void BCLTests_NullContainedArray_ContainsMethod()
        {
            object[] args = new object[] {"val1",null, 234 };

            try
            {
                 if (args.Contains(null))
                     throw new InvalidOperationException("Validator.CreateException(): one of  args[] items contains null value");
            }
            catch (Exception)
            {                
                throw;
            }
        }


        MethodInfo DlgInf<T>(T someContextAction)         
        {            
            return (someContextAction as Delegate).Method;
        }

        [TestMethod]
        public void BCLTests_GetMemberExpression_Test()
        {            
            var methodInf = DlgInf<Action>(BCLTests_GetMemberExpression_Test);
        }



        [TestMethod]
        public void BCLTests_TestPrimitivesTypeList()
        {
          var isPrimitiveType = typeof(int).Is4DPrimitiveType();

          var isPrimitive2 = typeof(TypePrimitiveEn).Is4DPrimitiveType();

          var aqname =  typeof(TypePrimitiveEn).AssemblyQualifiedName;



        }

    }
}
