﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;

namespace DDDD.SDS.BaseTests
{


    public enum TypeLabelEn : byte
    {        
        Bool,
        Byte,
        Char,
        Decimal,
        Double,
        Float,
        Int,
        Long,
        SByte,
        Short,
        String,
        UInt,
        ULong,
        UShort,
        DateTime,
        TimeSpan,
        Guid,
        Uri,
        Enum    
       
    }
    //object/bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri/IsEnum 

   

    [TestClass]
    public class SwitchingTests
    {
        

        static Dictionary<Type, String> DictionaryTypeSwitch = new Dictionary<Type, string>();
        static Dictionary<TypeLabelEn, String> DictionaryTypeLabelSwitch = new Dictionary<TypeLabelEn, string>();
        static Dictionary<Int32, String> DictionaryIndexedSwitch = new Dictionary<Int32, string>();

        static void InitDicitonaryTypeSwitch()
        {
            DictionaryTypeSwitch.Add(typeof(bool), "bool String Vvalue");
            DictionaryTypeSwitch.Add(typeof(Byte), "byte String Vvalue");
            DictionaryTypeSwitch.Add(typeof(char), "char String Vvalue");
            DictionaryTypeSwitch.Add(typeof(decimal), "decimal String Vvalue");
            DictionaryTypeSwitch.Add(typeof(double), "double String Vvalue");
            DictionaryTypeSwitch.Add(typeof(float), "float String Vvalue");
            DictionaryTypeSwitch.Add(typeof(int), "int String Vvalue");
            DictionaryTypeSwitch.Add(typeof(long), "long String Vvalue");
            DictionaryTypeSwitch.Add(typeof(sbyte), "sbyte String Vvalue");
            DictionaryTypeSwitch.Add(typeof(short), "short String Vvalue");
            DictionaryTypeSwitch.Add(typeof(string), "string String Vvalue");
           // DictionaryTypeSwitch.Add(typeof(UInt16), "uint16 String Vvalue");
            DictionaryTypeSwitch.Add(typeof(uint), "uint String Vvalue");
            DictionaryTypeSwitch.Add(typeof(ulong), "ulong String Vvalue");
            DictionaryTypeSwitch.Add(typeof(ushort), "ushort String Vvalue");
            DictionaryTypeSwitch.Add(typeof(DateTime), "DateTime String Vvalue");
            DictionaryTypeSwitch.Add(typeof(TimeSpan), "TimeSpan String Vvalue");
            DictionaryTypeSwitch.Add(typeof(Guid), "Guid String Vvalue");
            DictionaryTypeSwitch.Add(typeof(Uri), "Uri String Vvalue");
            DictionaryTypeSwitch.Add(typeof(Enum), "Enum String Vvalue");

        }


        static string GetTypeString_DicitonaryTypeSwitch(Type contract)
        {
            return DictionaryTypeSwitch[contract];
        }



        //static void InitDicitonaryTypeLabelSwitch()
        //{
        //    DictionaryTypeSwitch.Add(typeof(bool), "bool String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(Byte), "byte String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(char), "char String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(decimal), "decimal String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(double), "double String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(float), "float String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(int), "int String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(long), "long String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(sbyte), "sbyte String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(short), "short String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(string), "string String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(uint), "uint String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(ulong), "ulong String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(ushort), "ushort String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(DateTime), "DateTime String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(TimeSpan), "TimeSpan String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(Guid), "Guid String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(Uri), "Uri String Vvalue");
        //    DictionaryTypeSwitch.Add(typeof(Enum), "Enum String Vvalue");

        //}

        static string GetTypeString_DictionaryLabelSwitch(Type contract)
        {
            return null;
        }


        static string GetTypeString_DictionaryIndexSwitch(Type contract)
        {
            return null;
        }


        static Hashtable HashTableSwitch = new Hashtable();

        static string GetTypeString_HashTableSwitch(Type contract)
        {
            return (string)HashTableSwitch[contract];
        }

        static string GetTypeString_IfElseSwith(Type contract)
        {
            if (contract == typeof(bool))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(byte))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(char))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(decimal))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(double))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(float))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(int))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(long))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(sbyte))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(short))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(string))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(uint))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(ulong))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(ushort))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(DateTime))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(TimeSpan))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(Guid))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract == typeof(Uri))
            {
                return "Get Bool  contract: " + contract.Name;
            }
            if (contract.IsEnum)
            {
                return "Get Bool  contract: " + contract.Name;
            }
            return null;
        }



        static Dictionary<Int32, Type> RandomTypes = new Dictionary<int, Type>();
        static void InitRandomTypes()
        {
            RandomTypes.Add( 0, typeof(bool));
            RandomTypes.Add( 1, typeof(Byte));
            RandomTypes.Add( 2, typeof(char));
            RandomTypes.Add( 3, typeof(decimal));
            RandomTypes.Add( 4, typeof(double));
            RandomTypes.Add( 5, typeof(float));
            RandomTypes.Add( 6, typeof(int));
            RandomTypes.Add( 7, typeof(long));
            RandomTypes.Add( 8, typeof(sbyte));
            RandomTypes.Add( 9, typeof(short));
            RandomTypes.Add( 10, typeof(string));
            RandomTypes.Add( 11, typeof(uint));
            RandomTypes.Add( 12, typeof(ulong));
            RandomTypes.Add( 13, typeof(ushort));
            RandomTypes.Add( 14, typeof(DateTime));
            RandomTypes.Add( 15, typeof(TimeSpan));
            RandomTypes.Add( 16, typeof(Guid));
            RandomTypes.Add( 17, typeof(Uri));
            RandomTypes.Add( 18, typeof(Enum));
            
        }
        
        static Type NewTypeByRandon()
        {
            var rndm = new Random(0).Next(18);
            return RandomTypes[rndm];

        }

        [TestMethod]
        public void Test_SwitchingTest1_Method()
        {
            //Init  Dicitonary switch Collection
            //Init  Hashtable switch Collection
            
            Int32 iterationsCount = 10000000;
            InitRandomTypes();
            InitDicitonaryTypeSwitch();
            Stopwatch stopwatch = new Stopwatch();



            List<String> resultsIfElseSwitch = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                stopwatch.Start();
                for (int j = 0; j < iterationsCount; j++)
                {
                    GetTypeString_IfElseSwith(NewTypeByRandon());
                }

                stopwatch.Stop();
                resultsIfElseSwitch.Add(String.Format("Time elapsed: {0}", stopwatch.Elapsed.ToString()));
            }


            GC.Collect();
            List<String> resultsDictionaryTypeSwitch = new List<string>();
            for (int i = 0; i < 3; i++)
            {
                // Create new stopwatch
                
                stopwatch.Start();
                for (int j = 0; j < iterationsCount; j++)
                {
                    GetTypeString_DicitonaryTypeSwitch(NewTypeByRandon());    
                }                
                
                stopwatch.Stop();
                resultsDictionaryTypeSwitch.Add(String.Format("Time elapsed: {0}" , stopwatch.Elapsed.ToString()));                
            }


            
        }
    }
}
