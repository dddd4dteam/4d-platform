﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using DDDD.Core.Serialization;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using DDDD.Core.Tests.Model.ReflectionModel;
using DDDD.Core.Test.Disposable;
using DDDD.TSS.Tests.Model.CommunicationModel;

using System.Reflection;
using System.Linq.Expressions;
using System.Diagnostics;
using DDDD.Core.Collections.Compare;

namespace DDDD.Core.Net45.Tests.ReflectionTests
{
    [TestClass]
    public class Reflection_ActivationTests
    {


        /// <summary>
        ///   The name of the Invoke method of a Delegate.
        /// </summary>
        const string InvokeMethod = "Invoke";

        /// <summary>
        ///   Get method info for a specified delegate type.
        /// </summary>
        /// <param name = "delegateType">The delegate type to get info for.</param>
        /// <returns>The method info for the given delegate type.</returns>
        public static MethodInfo MethodInfoFromDelegateType(Type delegateType)
        {
            //Contract.Requires<ArgumentException>(
            //    delegateType.IsSubclassOf(typeof(MulticastDelegate)),
            //    "Given type should be a delegate.");

            return delegateType.GetMethod(InvokeMethod);
        }


        static void TestAction(Action testAction, string testClass, string testMethodName)
        {

#if DEBUG
            try
            {
                testAction();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException(" ERROR in {0}.{1}(): message -[{2}]"
                          .Fmt(testClass, testMethodName, exc.Message)
                          );
            }
#else
            testAction();
#endif


        }

        [Conditional("DEBUG")]
        public void Test_4DCore_Reflection_Delegate_CreateDlegateTestDbg()
        {
            TestAction(Test_4DCore_Reflection_Delegate_CreateDlegateTest,
                                nameof(Reflection_ActivationTests), nameof(Test_4DCore_Reflection_Delegate_CreateDlegateTest));
        }


        [TestMethod]
        public void Test_4DCore_Reflection_Delegate_CreateDlegateTest()
        {

            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            var targetType = typeof(List<>).MakeGenericType(typeof(string));
            var targetTypeMethods = targetType.GetMethods();//".ctor",
            var testCtorMethod = targetType.GetMethod(".ctor", Type.EmptyTypes);

            //var targetTFuncType = Expression.GetFuncType(typeof(object[]), typeof(object));

            var targetTFuncType = Expression.GetFuncType(targetType);
            var targetTFuncInfo = MethodInfoFromDelegateType(targetTFuncType);
            var parameters = targetTFuncInfo.GetParameters();

            var testCtor = targetType.GetConstructor(Type.EmptyTypes);

            var del2 = targetTFuncInfo.CreateDelegate(targetTFuncType);

            var targetDelegate = Delegate.CreateDelegate(targetTFuncType, targetTFuncInfo);

        }




        [TestMethod]
        public void Test_4DCore_Reflection_DebugCodeGenerator_DynamicComparerPrecompileTest()
        {
            try
            {//  Dictionary<string,int> with  length args and thread safety
                var testDictOfStringIntValue = TypeActivator.CreateInstanceTLazy<Dictionary<string, int>>(args: 33);
                
                DebugCodeCompiler.GenerateDebugAssemblyWith_SortComparerHandlerDbg<Product>( "Id".KVPair(Order.DESC), 
                                                                                          "Name".KVPair(null)); 

            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{ nameof(Test_4DCore_Reflection_DebugCodeGenerator_DynamicComparerPrecompileTest) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }
        }



        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateDictionary_ThreadSafetyWithArgs()
        {
            try
            {//  Dictionary<string,int> with  length args and thread safety
               var testDictOfStringIntValue = TypeActivator.CreateInstanceTLazy<Dictionary<string, int>>(args: 33);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateDictionary_ThreadSafetyWithArgs) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }
        }


        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateList_ThreadSafetyWithArgs()
        {
            try
            {//  Dictionary<string,int> with  length args and thread safety
                var testListOfStringIntValue = TypeActivator.CreateInstanceTLazy<List<string>>( args: 33);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateList_ThreadSafetyWithArgs) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }
        }



        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests()
        {
            // Tests CreateInstanceBoxed
            // create string 
            // create int?
            // List<string> - with no args
            // List<string> - with 1arg - 1 value
            // struct ctor with 2args
            // Dictionary<string,int> with 1 arg
            // Dictionary<string,int> with no args

            string testStringValue = (string)null;
            int testIntValue;
            int? testIntNallableValue;// = (int?)null;
            List<string> testListOfstringValue;
            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            Dictionary<string, int> testDictOfStringIntValue;
            Dictionary<string, int> testDictOfStringIntWithDefCountValue = new Dictionary<string, int>(3);// new List<string>(3);

            TestClass1 testClass1Value;



            var issringNullable = typeof(string).IsNullable();
            try
            {//  string 
                testStringValue = (string)TypeActivator.CreateInstanceBoxed(typeof(string));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(string).FullName}  exception");
            }


            try
            {//  int 
                testIntValue = (int)TypeActivator.CreateInstanceBoxed(typeof(int));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(int).FullName}  exception");
            }



            try
            {//  int? 
                testIntNallableValue = (int?)TypeActivator.CreateInstanceBoxed(typeof(int?));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(int?).FullName}  exception");
            }


            try
            {//  List<string> no args 
                testListOfstringValue = (List<string>)TypeActivator.CreateInstanceBoxed(typeof(List<string>));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(List<string>).FullName}  exception");
            }

            try
            {//  List<string> with Default Capacity arg 
                testListOfstringWithDefCountValue = (List<string>)TypeActivator.CreateInstanceBoxed(typeof(List<string>), TypeActivator.DefaultCtorSearchBinding, 7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(List<string>).FullName} with default capacity  exception");
            }




            try
            {//  Dictionary<string,int> no args 
                testDictOfStringIntValue = (Dictionary<string, int>)TypeActivator.CreateInstanceBoxed(typeof(Dictionary<string, int>));
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }

            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testDictOfStringIntWithDefCountValue = (Dictionary<string, int>)TypeActivator.CreateInstanceBoxed(typeof(Dictionary<string, int>), TypeActivator.DefaultCtorSearchBinding, 7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(Dictionary<string, int>).FullName} with default capacity  exception");
            }




            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testClass1Value = (TestClass1)TypeActivator.CreateInstanceBoxed(typeof(TestClass1), TypeActivator.DefaultCtorSearchBinding, "default Name:TestClass1", 23);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(TestClass1).FullName} with default capacity  exception");
            }



            try
            {//  Dictionary<string,int> with Default Capacity arg 
                testClass1Value = (TestClass1)TypeActivator.CreateInstanceBoxed(typeof(TestClass1), TypeActivator.DefaultCtorSearchBinding, null, 57);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceBoxedTests) } creating {typeof(TestClass1).FullName} with default capacity  exception");
            }




            //var activatedOne = Activator.CreateInstance(<string>()
        }


        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateTWithArgsTTests()
        {
            TestClass1 testClass1Value;
            
            //Dictionary<string, int> testDictOfStringIntValue;
            Dictionary<string, int> testDictOfStringIntWithDefCountValue = new Dictionary<string, int>(3);
            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            try
            {//  Dictionary<string,int> with Default Capacity arg   - in some cases it's not safe variant of creating such object
                testDictOfStringIntWithDefCountValue = TypeActivator.CreateInstanceTLazy<Dictionary<string, int>>( args: 7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(Dictionary<string, int>).FullName} with default capacity  exception");
            }



            try
            {//  List<string> with Default Capacity arg  - in some cases it's not safe variant of creating such object
                testListOfstringWithDefCountValue = TypeActivator.CreateInstanceTLazy<List<string>>( args: 7);
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(List<string>).FullName} with default capacity  exception");
            }



            //try
            //{//  Dictionary<string,int> with Default Capacity arg 
            //    testClass1Value = TypeActivator.CreateInstanceT<TestClass1>(isThreadSafe: true, args: new object[] { "default Name:TestClass1", 23 });
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating { typeof(TestClass1).FullName} with default capacity  exception");
            //}



            //try
            //{//  Dictionary<string,int> with Default Capacity arg   - in some cases it's not safe variant of creating such object
            //    testClass1Value = TypeActivator.CreateInstanceT<TestClass1>(isThreadSafe: true, args: new object[] { null, 57 });
            //}
            //catch (Exception exc)
            //{
            //    throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating { typeof(TestClass1).FullName} with default capacity  exception");
            //}

        }



        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests()
        {
            // Tests CreateInstanceBoxed
            // create string 
            // create int?
            // List<string> - with no args
            // List<string> - with 1arg - 1 value
            // struct ctor with 2args
            // Dictionary<string,int> with 1 arg
            // Dictionary<string,int> with no args

            string testStringValue = null;
            int testIntValue;
            int? testIntNallableValue;// = (int?)null;
            List<string> testListOfstringValue;
            List<string> testListOfstringWithDefCountValue = new List<string>(3);

            Dictionary<string, int> testDictOfStringIntValue;
            Dictionary<string, int> testDictOfStringIntWithDefCountValue = new Dictionary<string, int>(3);// new List<string>(3);
            
            var issringNullable = typeof(string).IsNullable();
            try
            {//  string 
                testStringValue = TypeActivator.CreateInstanceT<string>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(string).FullName}  exception");
            }


            try
            {//  int 
                testIntValue = TypeActivator.CreateInstanceT<int>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(int).FullName}  exception");
            }



            try
            {//  int? 
                testIntNallableValue = TypeActivator.CreateInstanceT<int?>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(int?).FullName}  exception");
            }


            try
            {//  List<string> no args  
                testListOfstringValue = TypeActivator.CreateInstanceT<List<string>>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(List<string>).FullName}  exception");
            }




            try
            {//  Dictionary<string,int> no args 
                testDictOfStringIntValue = TypeActivator.CreateInstanceT<Dictionary<string, int>>();
            }
            catch (Exception exc)
            {
                throw new InvalidOperationException($"{nameof(Test_4DCore_Reflection_TypeActivator_CreateInstanceTTests) } creating {typeof(Dictionary<string, int>).FullName}  exception");
            }






        }


        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_PrecompileCtorWithLentgAndCreateInstanceT_Tests()
        {
            try
            {
                List<string> listOfStrings = new List<string>();
                var funcofListOfString = TypeActivator.PrecompileAsNewWithLengthExpressionT<List<string>>();

                var newList = funcofListOfString(new object[] { 23 });


                var funcofClass1 = TypeActivator.PrecompileAsNewExpressionT<TestClass1>();
                var newClass1 = funcofClass1(null);


                var funcPointKindEnum = TypeActivator.PrecompileAsNewExpressionT<PointKindEn>();
                var enumValue = funcPointKindEnum(null);


                var funcRProductMark = TypeActivator.PrecompileAsNewExpressionT<RProductMark>();
                var structValue = funcRProductMark(null);

                var funcStringTwoDimArray = TypeActivator.PrecompileAsNewArrayExpressionT<string[,]>();
                var stringTwoDimArray = funcStringTwoDimArray(new object[] { 10, 10 });


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_CreateInstance()
        {
            try
            {
                var list = new List<string>();

                //var testList =  TypeActivator.CreateInstanceT<List<string>>(TypeActivator.DefaultCtorSearchBinding, 10);

                var value1 = TypeActivator.CreateInstanceT<string>(TypeActivator.DefaultCtorSearchBinding, new char[15]);

                //var str = new string()
                //string newstring = new string();

                var callType = TypeInfoEx.GetCallingType();

            }
            catch (Exception exc)
            {
                throw exc;
            }

            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }


        [TestMethod]
        public void Test_4DCore_Reflection_TypeActivator_Create_TypeSerializer_of_object_Test()
        {
            try
            {
                var tsSerializer = TypeSetSerializer.AddOrUseExistTSS(0);

                //var value1 = TypeActivator.CreateInstanceT<TypeSerializer<object>>(TypeActivator.DefaultCtorSearchBinding, new char[15]);
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }


    }
}
