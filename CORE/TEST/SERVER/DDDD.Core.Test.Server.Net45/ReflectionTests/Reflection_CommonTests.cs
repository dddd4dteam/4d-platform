﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.Reflection;
using DDDD.Core.Extensions;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;


using System.Collections;
using DDDD.Core.Net.ServiceModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.Core.Tests.Model.CollectionsModel;
using DDDD.Core.Tests.Model.ReflectionModel;
//using DDDD.Core.Test.Disposable;
using DDDD.Core.ComponentModel;
using DDDD.Core.Net;
using DDDD.TSS.Tests.Model.CommunicationModel;

namespace DDDD.Core.Net45.Tests.ReflectionTests
{

    [TestClass]
    public class Test_4DCore_Reflection_Tests
    {

        [TestMethod]
        public void Test_4DCore_Reflection_GetAttributeByBaseAttributeClass_Tests()
        {
            string TypeFullName  = "DDDD.TSS.Tests.Model.CommunicationModel.Product4, DDDD.Core.Test.Model.Net45";
            var tfName = typeof(Product4).FullName;
            var aqname = typeof(Product4).AssemblyQualifiedName;

            var getType1 = Type.GetType(TypeFullName);
            var getType2 = Type.GetType(aqname);
            try
            {
                //var testClassAttributes = typeof(Test_4DCore_Reflection_Tests).GetTypeAttributes<TestClassAttribute>().First();
                //var attribs =  typeof(DCServiceHostFactory).GetTypeAttributes<ComponentItemMetaAttribute>(useInheritedAttributes: true);

                var serchedCurrentAttributes = typeof(DCServiceHostFactory).GetCustomAttributesWithInterface<IComponentMatchMetaInfo>();


                //var enumTypeFields = typeof(DCCommunicationUnitEn).GetFields();
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        [TestMethod]
        public void Test_4DCore_Reflection_GetTSSServiceModelExtensionFullName_Tests()
        {
            try
            {
                Console.WriteLine($" TSS ServiceModel Extension Type Full Name: [{typeof(TSSBehaviorExtension).AssemblyQualifiedName}] ");
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        [TestMethod]
        public void Test_4DCore_Reflection_TypeInfoEx_GetIListArgTypeTests()
        {
            try
            {
               var iListPlaneArgType =  TypeInfoEx.Get_IListArgType(typeof(CustomListPlane));

               var iListGenericArgType = TypeInfoEx.Get_IListArgType(typeof(CustomStringListGeneric));

               var iListGenericInheritedArgType = TypeInfoEx.Get_IListArgType(typeof(CustomStringListGenericInherited));

                var iListPlaneInheritedArgType = TypeInfoEx.Get_IListArgType(typeof(CustomListPlaneInherited));

                var iListStringArgType = TypeInfoEx.Get_IListArgType(typeof(List<string>));
            }
            catch (Exception exc) 
            {
                throw exc;
            }

        }


        [TestMethod]
        public void Test_4DCore_Reflection_TypeInfoEx_GetIDictionaryArgTypeTests()
        {
            try
            {
                var iDictionaryPlaneArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryPlaneCollection));

                var iDictionaryStringIntArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryStringIntCollection));

                var iDictionaryStringIListArgTypes = TypeInfoEx.Get_IDictionaryArgTypes(typeof(CustomIDictionaryStringIListCollection));

                

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }



        // 
         
        [TestMethod]
        public void Test_4DCore_Reflection_IsNeedToRereadDataType_Test()
        {
            try
            {
                var need1 = typeof(List<SomeTypologyEn>).IsNeedToSureInDataType();
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }

        [TestMethod]
        public void Test_4DCore_Reflection_IsNullableType_Test()
        {
            try
            {
                var isNullable1 = typeof(List<SomeTypologyEn>).IsNullable();

                var isNullable2 = typeof(SomeTypologyEn).IsNullable();
                var isNullable3 = typeof(SomeTypologyEn?).IsNullable();

                var isNullable4 = typeof(RProductMark).IsNullable();
                var isNullable5 = typeof(RProductMark?).IsNullable();

            }
            catch (Exception exc)
            {
                throw exc;
            }

        }


        [TestMethod]
        public void Test_4DCore_Reflection_IsDisposableSubclassOf_Test1()
        {
            var interfaceOfDC = typeof(DisposableClass).IsDisposable();//"IDisposable");
            var isDisposable = typeof(IDisposable).IsAssignableFrom(typeof(DisposableClass));

            var isDisposable2 = typeof(NotDisposableClass).IsDisposable();
        }



        [TestMethod]
        public void Test_4DCore_Reflection_IsTypeAssignable()
        {
            try
            {
                var paramType = typeof(char*);
                var argType = typeof(char[]);

                var can1 = paramType.IsAssignableFrom(argType);

                var paramType2 = typeof(IList);
                var argType2 = typeof(List<>);

                var can2 = paramType2.IsAssignableFrom(argType2);







            }
            catch (Exception exc)
            {
                throw exc;
            }

            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }




        [TestMethod]
        public void Test_4DCore_Reflection_GetCallingType_Test1()
        {
            try
            {
                var callType = TypeInfoEx.GetCallingType();

                var currentMethod = MethodBase.GetCurrentMethod();
                var currentTestClassType = currentMethod.DeclaringType;

            }
            catch (Exception exc)
            {
                throw exc;
            }           
        }



        [TestMethod]
        public void Test_4DCore_Reflection_GetCallingType_Test2()
        {
            var callType = TypeInfoEx.GetCallingType();

           
            string[,] arrayDoubledstring = new string[1,1];

           var arrayCtors= arrayDoubledstring.GetType().GetConstructors();

            //arrayDoubledstring
            //TypeInfoEx.
            //TypeInfoEx.Mapper = null;
        }




        [TestMethod]
        public void Test_4DCore_Reflection_TypeAQName_Test1()
        {
            var typeInfo = TypeInfoEx.Get(typeof(Test_4DCore_Reflection_Tests) );//.AssemblyQualifiedName;
            var typeFounded = typeInfo.TpAQName.LA_FoundType.Value;
                        
            //typeInfo.WorkingType

            TypePrimitiveEn enu1 = TypePrimitiveEn.Boolean;
        }



        [TestMethod]
        public void Test_4DCore_Reflection_TypeAQName_ForMultiDimArrays_Test1()
        {
            try
            {               
                
                var typeListIntAQName = TypeAQName.TryParse(typeof(List<int>).AssemblyQualifiedName);//.AssemblyQualifiedName;
                var typeListIntAQTypeName = typeListIntAQName.FullTypeName;

                var typeListInt = typeof(List<int>);
                var typeListIntTypeName = typeListInt.Name;
                var typeListIntTypeFullName = typeListInt.FullName;


                var typeInt2DimarrayAQName = TypeAQName.TryParse(typeof(int[,]).AssemblyQualifiedName);//.AssemblyQualifiedName;
                var typeInt2DimArray = typeof(int[,]);
                var typeInt2DimArrayTypeName = typeInt2DimArray.Name;
                var typeInt2DimArrayTypeFullName = typeInt2DimArray.FullName;


                //var typeFounded = typeInfo.TpAQName.LS_FoundType.Value;

                //typeInfo.WorkingType

                TypePrimitiveEn enu1 = TypePrimitiveEn.Boolean;
            }
            catch (Exception exc)
            {     throw exc;
            }            
        }

        





    }
}
