﻿
using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DDDD.Core.Reflection;

namespace DDDD.BCL.Net45.Tests.ReflectionTests
{
    /// <summary>
    /// Summary description for Reflection_TypeAccessor
    /// </summary>
    [TestClass]
    public class Reflection_TypeAccessorTests
    {
        public Reflection_TypeAccessorTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion


        [TestMethod]
        public void Test_4DCore_Reflection_TypeAccessot_CreateUriForMexEndpoint_Tests()
        {
            try
            {
                var uriAccessor = TypeAccessor<Uri>.Create();

                var newUri = new Uri("https://msdn.microsoft.com/en-us/library/e2h7fzkw%28v=vs.140%29.aspx#bkmk_offline");


                var getfields = typeof(Uri).GetFields(System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);

                var m_StringFld = typeof(Uri).GetField("m_String", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                var m_StringValue = m_StringFld.GetValue(newUri);
                //setValue 
                m_StringFld.SetValue(newUri, "mex");


                //uriAccessor


            }
            catch (Exception exc)
            {
                throw exc;
            }

        }
    }
}
