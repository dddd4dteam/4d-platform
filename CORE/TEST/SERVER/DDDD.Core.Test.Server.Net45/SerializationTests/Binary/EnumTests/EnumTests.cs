﻿using System;
using System.Diagnostics;
//using DDDD.Core.Test;

using System.IO;
using DDDD.Core.TestUtils;
using DDDD.Core.IO;
using DDDD.Core.Serialization;
using DDDD.Core.Reflection;
//using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.Core.Serialization.Binary;


#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;
#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.TestUtils;
using System.IO;
#endif

namespace DDDD.Core.Tests.EnumTests
{
    [TestClass]
    public class EnumTests
    {
        // static String ServiceKey = "SomeServiceKey";



        [TestMethod]
        public void Test_4DCore_Serialization_Enum_TestAddEnumByHandsException()
        {
            try
            {
                //serializer
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

                //enum контракт не можетдобавляться рукаими
                BinarySerializer.AddKnownType<SomeTypologyEn>();//auto adding contract
            }
            catch (InvalidOperationException addExc)
            {
                Debug.WriteLine(string.Format("Correct exception - enum contract cannot be added by hands it 'l be made auto : [{0}] ", addExc.Message));
            }
            catch(Exception)
            {
                throw;
            }

        }


      


        [TestMethod]
        public void Test_4DCore_Serialization_Enum_ClassValuePublicMembers()
        {
            try
            {

                //serializer
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

                EnumTestClass enumClass = new EnumTestClass();

                // enum field
                enumClass.EnumFieldPublic = SomeTypologyEn.Peoples;

                // enum collection
                enumClass.AllTypologyItemsPublic.Add(SomeTypologyEn.Reptiles);
                enumClass.AllTypologyItemsPublic.Add(SomeTypologyEn.Birds);



                try
                {
                    //1 сериализация просто переменной инама
                    var enumValue = SomeTypologyEn.Birds;

                    var data1 = BinarySerializer.Serialize(enumValue);
                    var dataDeserialized = BinarySerializer.Deserialize<SomeTypologyEn>(data1);
                    
                    Utility.CompareSerializeResult("Test_EnumClass_PublicMembers.Point1", enumValue, dataDeserialized);

                }
                catch (Exception exc)
                {
                    Debug.WriteLine($"Incorrect exception. Test_EnumClass_PublicMembers.Point1 : {exc.Message}"); 
                }
                
                try
                {   //2 если  не добавлен контракт(EnumTestClass) то мы должны получить исключение при вызове  сериализации
                    var dataSerialized = BinarySerializer.Serialize(enumClass);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(string.Format("Correct exception - contract is unknown : [{0}] " ,exc.Message ) );                   
                }

                enumClass.AddNewTypologyItemPrivate(SomeTypologyEn.Plants);


                //3 добавили контракт должны сериализоваться только паблик элементы 
                BinarySerializer.AddKnownType<EnumTestClass>();



                //we still don't add NotKnownEnumEn
                try
                {
                    var dataSerialized2 = BinarySerializer.Serialize(enumClass);
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(string.Format("Correct exception - contract is unknown : [{0}] ", exc.Message));
                }

                BinarySerializer.AddKnownType<NotKnownEnumEn>();
                
                var dataSerialized3 = BinarySerializer.Serialize(enumClass);
                var testDeserialized = BinarySerializer.Deserialize<EnumTestClass>(dataSerialized3);


                Utility.CompareSerializeResult("Test_EnumClass_PublicMembers", enumClass, testDeserialized, TypeMemberSelectorEn.PublicOnly);
              
            }
            catch (Exception exc)
            {                
                throw;
            }
        }


        public static object Deserialize_SomeByteTypologyEn(Stream stream, bool readedTypeID, TypeSerializer<SomeByteTypologyEn> typeSerializer)
        {
            //object ToObject(Type enumType, object value);

            return Enum.ToObject(
                                 typeof(SomeByteTypologyEn), 
                                 BinaryReaderWriter.Read_byte(stream, readedTypeID)
                                );
            //return (object)(SomeByteTypologyEn)PrimitiveReaderWriter.ReadPrimitive_byte(stream, readedTypeID);
        }


        [TestMethod]
        public void Test_4DCore_Serialization_Enum_ByteEnumTest()
        {
            try
            {

                byte enumValueInByte = 3;

                var enumValue = Enum.ToObject(
                                              typeof(SomeByteTypologyEn),
                                              enumValueInByte
                                             );


                //serializer
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

                var valueByteEnum = SomeByteTypologyEn.Birds;

                var data = BinarySerializer.Serialize(valueByteEnum);
                var deserialized = BinarySerializer.Deserialize<SomeByteTypologyEn>(data);
            }
            catch (Exception)
            {
                
                throw;
            }
        }


    }
}
