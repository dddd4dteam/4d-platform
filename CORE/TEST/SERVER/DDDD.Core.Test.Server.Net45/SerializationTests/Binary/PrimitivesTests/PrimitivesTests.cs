﻿using System;
using System.Collections.Generic;
using DDDD.Core.Serialization;
using DDDD.Core.Serialization.Binary;
using DDDD.Core.TestUtils;
using DDDD.TSS.Tests.Model.CommunicationModel;



//using DDDD.Core.Test;





#if NET45
using Microsoft.VisualStudio.TestTools.UnitTesting;
#elif SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;

using DDDD.Core.Tests.Model.CommunicationModel;
using System.Reflection.Emit;
using DDDD.Core.TestUtils;


#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.Core.TestUtils;
#endif


namespace DDDD.Core.Tests.SerializationTests
{
    [TestClass]
    public class PrimitivesTests
    {

        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_BuildingDebugProcessingCode()
        {

            //default types- primitives
            //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   
            
            //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);

            //serializer.AddKnownType<>
            //serializer.BuildTypeProcessor ();

        }





        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_PrimitivesTest()
        {
            try
            {
                //default types- primitives
                //bool/byte/char/decimal/double/float/int/long/sbyte/short/string/uint/ulong/ushort/DateTime/TimeSpan/Guid/Uri   


                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);   
                //serializer.BuildTypeProcessor ();

                //bool
                var boolVariable = true;// serializer.DefaultValues.Add(typeof(bool), default(bool));
                var boolVariableSerialized = BinarySerializer.Serialize(boolVariable);
                var boolVariableDeserialized = BinarySerializer.Deserialize<bool>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", boolVariable, boolVariableDeserialized);


                //byte
                var byteVariable = byte.MaxValue;// serializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = BinarySerializer.Serialize(byteVariable);
                var byteVariableDeserialized = BinarySerializer.Deserialize<byte>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Byte", byteVariable, byteVariableDeserialized);


                //char
                var charVariable = char.MaxValue; //serializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = BinarySerializer.Serialize(charVariable);
                var charVariableDeserialized = BinarySerializer.Deserialize<char>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Char", charVariable, charVariableDeserialized);


                //decimal
                var decimalVariable = decimal.MaxValue; //serializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = BinarySerializer.Serialize(decimalVariable);
                var decimalVariableDeserialized = BinarySerializer.Deserialize<decimal>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - decimal", decimalVariable, decimalVariableDeserialized);

                //double
                var doubleVariable = double.MaxValue;// serializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = BinarySerializer.Serialize(doubleVariable);
                var doubleVariableDeserialized = BinarySerializer.Deserialize<double>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - double", byteVariable, byteVariableDeserialized);


                //float
                var floatVariable = float.MaxValue;// serializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = BinarySerializer.Serialize(floatVariable);
                var floatVariableDeserialized = BinarySerializer.Deserialize<float>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - float", floatVariable, floatVariableDeserialized);

                
                //int
                var intVariable = int.MaxValue;// serializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = BinarySerializer.Serialize(intVariable);
                var intVariableDeserialized = BinarySerializer.Deserialize<int>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - int", intVariable, intVariableDeserialized);

                
                //long
                var longVariable = long.MaxValue;// serializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = BinarySerializer.Serialize(longVariable);
                var longVariableDeserialized = BinarySerializer.Deserialize<long>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - long", longVariable, longVariableDeserialized);

                
                //sbyte
                var sbyteVariable = sbyte.MaxValue;// serializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = BinarySerializer.Serialize(sbyteVariable);
                var sbyteVariableDeserialized = BinarySerializer.Deserialize<sbyte>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - sbyte", sbyteVariable, sbyteVariableDeserialized);

                
                //short
                var shortVariable = short.MaxValue;// serializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = BinarySerializer.Serialize(shortVariable);
                var shortVariableDeserialized = BinarySerializer.Deserialize<short>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - short", shortVariable, shortVariableDeserialized);

                
                //string
                var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                var stringVariableSerialized = BinarySerializer.Serialize(stringVariable);
                var stringVariableDeserialized = BinarySerializer.Deserialize<string>(stringVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - string", stringVariable, stringVariableDeserialized);

                
                //uint
                var uintVariable = uint.MaxValue;// serializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = BinarySerializer.Serialize(uintVariable);
                var uintVariableDeserialized = BinarySerializer.Deserialize<uint>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - uint", uintVariable, uintVariableDeserialized);

                
                //ulong
                var ulongVariable = ulong.MaxValue;// serializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = BinarySerializer.Serialize(ulongVariable);
                var ulongVariableDeserialized = BinarySerializer.Deserialize<ulong>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Bool", ulongVariable, ulongVariableDeserialized);

                
                //ushort
                var ushortVariable = ushort.MaxValue;// serializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = BinarySerializer.Serialize(ushortVariable);
                var ushortVariableDeserialized = BinarySerializer.Deserialize<ushort>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - ushort", ushortVariable, ushortVariableDeserialized);

                
                //DateTime
                var datetimeVariable = DateTime.MaxValue;// serializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var datetimeVariableSerialized = BinarySerializer.Serialize(datetimeVariable);
                var datetimeVariableDeserialized = BinarySerializer.Deserialize<DateTime>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - datetime", datetimeVariable, datetimeVariableDeserialized);

                
                //TimeSpan
                var timespanVariable = TimeSpan.MaxValue;// serializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = BinarySerializer.Serialize(timespanVariable);
                var timespanVariableDeserialized = BinarySerializer.Deserialize<TimeSpan>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - timeSpan", timespanVariable, timespanVariableDeserialized);
                
                
                //Guid
                var guidVariable = Guid.Empty;// serializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = BinarySerializer.Serialize(guidVariable);
                var guidVariableDeserialized = BinarySerializer.Deserialize<Guid>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Guid", guidVariable, guidVariableDeserialized);

                
                //Uri
                var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. serializer.DefaultValues.Add(typeof(Uri), default(Uri));
                var uriVariableSerialized = BinarySerializer.Serialize(uriVariable);
                var uriVariableDeserialized = BinarySerializer.Deserialize<Uri>(uriVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_SimpleTypesTest - Uri", uriVariable, uriVariableDeserialized);
                
            }
            catch (Exception)
            {                
                throw;
            }
            
        }
               

        [TestMethod]
        public void Test_4DCore_Serialization_Primitives_NullablePrimitives_Test()
        {
            try
            {
                //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);


                //bool
                bool? boolNullableVariable = true;// = null;
                var boolVariableSerialized = BinarySerializer.Serialize(boolNullableVariable, true);
                var boolVariableDeserialized = BinarySerializer.Deserialize<bool?>(boolVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - bool?", boolNullableVariable, boolVariableDeserialized );


                //byte
                byte? byteVariable = byte.MaxValue;// serializer.DefaultValues.Add(typeof(byte), default(byte));
                var byteVariableSerialized = BinarySerializer.Serialize(byteVariable, true);
                var byteVariableDeserialized = BinarySerializer.Deserialize<byte?>(byteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - byte?", byteVariable, byteVariableDeserialized);


                //char
                char? charVariable = char.MaxValue; //serializer.DefaultValues.Add(typeof(char), default(char));
                var charVariableSerialized = BinarySerializer.Serialize(charVariable,true);
                var charVariableDeserialized = BinarySerializer.Deserialize<char?>(charVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - char?", charVariable, charVariableDeserialized );


                //decimal
                decimal? decimalVariable = decimal.MaxValue; //serializer.DefaultValues.Add(typeof(decimal), default(decimal));
                var decimalVariableSerialized = BinarySerializer.Serialize(decimalVariable, true);
                var decimalVariableDeserialized = BinarySerializer.Deserialize<decimal?>(decimalVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - decimal?", decimalVariable, decimalVariableDeserialized );
                

                //double
                double? doubleVariable = double.MaxValue;// serializer.DefaultValues.Add(typeof(double), default(double));
                var doubleVariableSerialized = BinarySerializer.Serialize(doubleVariable, true);
                var doubleVariableDeserialized = BinarySerializer.Deserialize<double?>(doubleVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - double?", doubleVariable, doubleVariableDeserialized );


                //float
                float? floatVariable = float.MaxValue;// serializer.DefaultValues.Add(typeof(float), default(float));                
                var floatVariableSerialized = BinarySerializer.Serialize(floatVariable, true);
                var floatVariableDeserialized = BinarySerializer.Deserialize<float?>(floatVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - float?", floatVariable, floatVariableDeserialized );


                //int
                int? intVariable = int.MaxValue;// serializer.DefaultValues.Add(typeof(int), default(int));
                var intVariableSerialized = BinarySerializer.Serialize(intVariable, true);
                var intVariableDeserialized = BinarySerializer.Deserialize<int?>(intVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - int?", intVariable, intVariableDeserialized );


                //long
                long? longVariable = long.MaxValue;// serializer.DefaultValues.Add(typeof(long), default(long));
                var longVariableSerialized = BinarySerializer.Serialize(longVariable, true);
                var longVariableDeserialized = BinarySerializer.Deserialize<long?>(longVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - long?", longVariable, longVariableDeserialized );


                //sbyte
                sbyte? sbyteVariable = sbyte.MaxValue;// serializer.DefaultValues.Add(typeof(sbyte), default(sbyte));
                var sbyteVariableSerialized = BinarySerializer.Serialize(sbyteVariable, true);
                var sbyteVariableDeserialized = BinarySerializer.Deserialize<sbyte?>(sbyteVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - sbyte?", sbyteVariable, sbyteVariableDeserialized );


                //short
                short? shortVariable = short.MaxValue;// serializer.DefaultValues.Add(typeof(short), default(short));
                var shortVariableSerialized = BinarySerializer.Serialize(shortVariable, true);
                var shortVariableDeserialized = BinarySerializer.Deserialize<short?>(shortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - short?", shortVariable, shortVariableDeserialized );

                
                //uint
                uint? uintVariable = uint.MaxValue;// serializer.DefaultValues.Add(typeof(uint), default(uint));
                var uintVariableSerialized = BinarySerializer.Serialize(uintVariable, true);
                var uintVariableDeserialized = BinarySerializer.Deserialize<uint?>(uintVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - uint?", uintVariable, uintVariableDeserialized);


                //ulong
                ulong? ulongVariable = ulong.MaxValue;// serializer.DefaultValues.Add(typeof(ulong), default(ulong));
                var ulongVariableSerialized = BinarySerializer.Serialize(ulongVariable, true);
                var ulongVariableDeserialized = BinarySerializer.Deserialize<ulong?>(ulongVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ulong?", ulongVariable, ulongVariableDeserialized);


                //ushort
                ushort? ushortVariable = ushort.MaxValue;// serializer.DefaultValues.Add(typeof(ushort), default(ushort));
                var ushortVariableSerialized = BinarySerializer.Serialize(ushortVariable, true);
                var ushortVariableDeserialized = BinarySerializer.Deserialize<ushort?>(ushortVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - ushort?", ushortVariable, ushortVariableDeserialized);


                //DateTime
                DateTime? datetimeVariable = DateTime.MaxValue;// serializer.DefaultValues.Add(typeof(DateTime), default(DateTime));
                var datetimeVariableSerialized = BinarySerializer.Serialize( datetimeVariable, true);
                var datetimeVariableDeserialized = BinarySerializer.Deserialize<DateTime?>(datetimeVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - DateTime?", datetimeVariable, datetimeVariableDeserialized);


                //TimeSpan
                TimeSpan? timespanVariable = TimeSpan.MaxValue;// serializer.DefaultValues.Add(typeof(TimeSpan), default(TimeSpan));
                var timespanVariableSerialized = BinarySerializer.Serialize(timespanVariable, true);
                var timespanVariableDeserialized = BinarySerializer.Deserialize<TimeSpan?>(timespanVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - TimeSpan?", timespanVariable, timespanVariableDeserialized);


                //Guid
                Guid? guidVariable = Guid.Empty;// serializer.DefaultValues.Add(typeof(Guid), default(Guid));
                var guidVariableSerialized = BinarySerializer.Serialize(guidVariable, true);
                var guidVariableDeserialized = BinarySerializer.Deserialize<Guid?>(guidVariableSerialized);
                Utility.CompareSerializeResult("Test_Primitives_NullablePrimitives_Test - Guid?", guidVariable, guidVariableDeserialized );


                ////Uri
                //var uriVariable = new Uri("https://www.nuget.org/packages/DDDD.SDS/");//. serializer.DefaultValues.Add(typeof(Uri), default(Uri));
                //var uriVariableSerialized = serializer.Serialize(uriVariable);
                //var uriVariableDeserialized = serializer.Deserialize<Uri>(uriVariableSerialized);


                ////string
                //var stringVariable = "String Test Value"; //serializer.DefaultValues.Add(typeof(string), default(string));
                //var stringVariableSerialized = serializer.Serialize(stringVariable);
                //var stringVariableDeserialized = serializer.Deserialize<string>(stringVariableSerialized);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


    }







}





