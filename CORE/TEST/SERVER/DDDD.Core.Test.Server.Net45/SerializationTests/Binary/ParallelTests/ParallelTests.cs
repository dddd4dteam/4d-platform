﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Threading;

using DDDD.Core.TestUtils;
using DDDD.Core.Serialization;

using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.CommunicationModel;

using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Serialization.Binary;


#if (NET45 || SL5)
using Microsoft.VisualStudio.TestTools.UnitTesting;


#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.Core.ServiceModel;
using DDDD.Core.TestUtils;

#endif


namespace DDDD.Core.Tests.ConcurrencyTests
{
    [TestClass]
    public class ParallelTests
    {   

        #region ------------------------------------- PARALLEL:INSTANCE PER ITERATION(MULTIPLETHREAD) --------------------------------------

        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_InstancePerIteration_ThreadPool()
        {   
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;//iteration index to compare counts  at the end
            int numProcs = System.Environment.ProcessorCount;
            int range = Iterations / numProcs;
                       
            Model1Container.GenerateArrayNCommands1(2, 3);
            
            // Keep track of the number of threads remaining to complete. 
            int remaining = numProcs;
            using (ManualResetEvent mre = new ManualResetEvent(false))
            {
                // Create each of the threads. 
                for (int p = 0; p < numProcs; p++)
                {
                    int start = p * range + 0;
                    int end = (p == numProcs - 1) ?
                        Iterations : start + range;
                    ThreadPool.QueueUserWorkItem(
                        (s) =>
                        {                           

                            for (int i = start; i < end; i++)
                            {
                                //body(i);  begin
                                try
                                {
                                    //var serializer = TypeSetSerializer.AddOrUseExistTSS("1");

                                    BinarySerializer.AddKnownType<ContractBase>();
                                    BinarySerializer.AddKnownType<Product>();
                                    BinarySerializer.AddKnownType<ProductMark>();
                                    BinarySerializer.AddKnownType<CommandMessage>();
                                    BinarySerializer.AddKnownType<CommandContainer2>();
                                    //serializer.BuildTypeProcessor ();
                                    

                                    // serialize
                                    var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                                    // deserialize
                                    var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);


                                    Utility.CompareSerializeResult("Test_Parallel_InstancePerIteration_ThreadPool ", Model1Container.DATACommands, CompareDesererializeDATA);
                                    
                               
                                    //if action complete succesfully increment currentIteration
                                    Interlocked.Increment(ref currentIteration);

                                }
                                catch (System.Threading. ThreadAbortException texc)
                                {
                                    string msg = texc.Message;
                                    throw texc;
                                }
                                catch (Exception exc)
                                {
                                    string msg = exc.Message;
                                    throw exc;
                                }
                                //body(i) end
                            }

                            if (Interlocked.Decrement(ref remaining) == 0) mre.Set();

                        },
                        null
                        );
                }
                // Wait for all threads to complete. 
                mre.WaitOne();
            }

            Int32 iterationsEnd = currentIteration;

        }
        

        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_InstancePerIteration_Tasks()
        {
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;//iteration index to compare counts  at the end
            int numProcs = System.Environment.ProcessorCount;
            int range = Iterations / numProcs;

            string ServiceKey = "TestService";
            Model1Container.GenerateArrayNCommands1(2, 3);


            Action sAction =
                () =>
                {
                    //body(i);  begin
                    try
                    {
                        //var serializer =  TypeSetSerializer.AddOrUseExistTSS("1");

                        BinarySerializer.AddKnownType<ContractBase>();
                        BinarySerializer.AddKnownType<Product>();
                        BinarySerializer.AddKnownType<ProductMark>();
                        BinarySerializer.AddKnownType<CommandMessage>();
                        BinarySerializer.AddKnownType<CommandContainer2>();
                        //serializer.BuildTypeProcessor ();
                                                      
                        // serialize
                        var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                        // deserialize
                        var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);

                        Utility.CompareSerializeResult("Test_Parallel_InstancePerIteration_Tasks", Model1Container.DATACommands, CompareDesererializeDATA);
                        
                        //if action complete succesfully increment currentIteration
                        Interlocked.Increment(ref currentIteration);

                    }
                    catch (System.Threading.ThreadAbortException texc)
                    {
                        string msg = texc.Message;
                        throw texc;
                    }
                    catch (Exception exc)
                    {
                        string msg = exc.Message;
                        throw exc;
                    }
                    //body(i) end

                };

            //delegates Array 
            Action[] ParallelActions = new Action[Iterations];
            for (int i = 0; i < ParallelActions.Length; i++)
            { ParallelActions[i] = sAction; }

            //run parallel loop for ParallelActions            
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < ParallelActions.Length; i++)
                {
                    int cur = i;
                    Task.Factory.StartNew(
                        ParallelActions[cur],
                        TaskCreationOptions.AttachedToParent);
                }
            }
            )
            .Wait();

            Int32 iterationsEnd = currentIteration;
        }

#if NET45

        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_InstancePerIteration_ParallelFor()
        {
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;//iteration index to compare counts  at the end

            Model1Container.GenerateArrayNCommands1(2, 3);
            
            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = System.Environment.ProcessorCount;

            Parallel.For<ITypeSetSerializer> (0, Iterations, options,
                    () =>
                    {
                        //Interlocked.Increment( ref currentIteration);  //only 7 times we was here - 1007 -1000
                        return null;
                    }
                    ,
                    (ind, pstate, slzr) =>
                    {
                        try
                        {
                            //var serializer =  TypeSetSerializer.AddOrUseExistTSS("1");

                            BinarySerializer.AddKnownType<ContractBase>();
                            BinarySerializer.AddKnownType<Product>();
                            BinarySerializer.AddKnownType<ProductMark>();
                            BinarySerializer.AddKnownType<CommandMessage>();
                            BinarySerializer.AddKnownType<CommandContainer2>();
                            //serializer.BuildTypeProcessor ();

                            // serialize
                            var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                            // deserialize
                            var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);

                            // compare This is the comparison class                    
                            Utility.CompareSerializeResult("Test_Parallel_InstancePerIteration_ParallelFor", Model1Container.DATACommands, CompareDesererializeDATA);
                  
                            //if action complete succesfully increment currentIteration
                            Interlocked.Increment(ref currentIteration);
                            return BinarySerializer.Current;
                        }
                        catch (ThreadAbortException texc)
                        {
                            string msg = texc.Message;
                            throw texc;
                        }
                        catch (Exception exc)
                        {
                            string msg = exc.Message;
                            throw exc;
                        }

                    },
                    (s) =>
                    {
                        //currentIteration++;
                    }

                );

            Int32 iterationsEnd = currentIteration;
        }

#endif

        #endregion -------------------------------------PARALLEL: INSTANCE  PER ITERATION(MULTIPLETHREADS) --------------------------------------
        

        #region -----------------------------------PARALLEL:  SHARED INSTANCE(MULTIPLETHREADS) -------------------------------------


#if NET45
        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_OneSharedInstance_ThreadPool()
        {


            // Determine the number of iterations to be processed, the number of 
            // cores to use, and the approximate number of iterations to process in  
            // each thread. 
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;//iteration index to compare counts  at the end
            int numProcs = System.Environment.ProcessorCount;
            int range = Iterations / numProcs;

            //prepare Service serializer            
            //var serializerOfService = TypeSetSerializer.AddOrUseExistTSS(0);

            BinarySerializer.AddKnownType<ContractBase>();
            BinarySerializer.AddKnownType<Product>();
            BinarySerializer.AddKnownType<ProductMark>();
            BinarySerializer.AddKnownType<CommandMessage>();
            BinarySerializer.AddKnownType<CommandContainer2>();
            //BinarySerializer.BuildTypeProcessor ();
            Model1Container.GenerateArrayNCommands1(2, 3);



            // Keep track of the number of threads remaining to complete. 
            int remaining = numProcs;
            using (ManualResetEvent mre = new ManualResetEvent(false))
            {
                // Create each of the threads. 
                for (int p = 0; p < numProcs; p++)
                {
                    int start = p * range + 0;
                    int end = (p == numProcs - 1) ?
                        Iterations : start + range;
                    ThreadPool.QueueUserWorkItem(
                        (s) =>
                        {

                            for (int i = start; i < end; i++)
                            {
                                //body(i);  begin
                                try
                                {
                                    //var serializer = TypeSetSerializer.GetDirect(0);
                                    // serialize
                                    var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                                    // deserialize
                                    var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);


                                    // compare This is the comparison class                    
                                    Utility.CompareSerializeResult("Test_Parallel_OneSharedInstance_ThreadPool", Model1Container.DATACommands, CompareDesererializeDATA);
                   

                                    //if action complete succesfully increment currentIteration
                                    Interlocked.Increment(ref currentIteration);

                                }
                                catch (ThreadAbortException texc)
                                {
                                    string msg = texc.Message;
                                    throw texc;
                                }
                                catch (Exception exc)
                                {
                                    string msg = exc.Message;
                                    throw exc;
                                }
                                //body(i) end
                            }

                            if (Interlocked.Decrement(ref remaining) == 0) mre.Set();

                        },
                        null                        
                        );
                }
                // Wait for all threads to complete. 
                mre.WaitOne();
            }

            Int32 iterationsEnd = currentIteration;
        }
        


        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_OneSharedInstance_ParallelFor()
        {   
            Int32 Iterations = 10000;        
            Int32 currentIteration = 0;//iteration index to compare counts  at the end

            //Init one  instance of ServiceSerializer to  share it in 1000 iterations of serialize/deserialize  action
            //string ServiceKey = "TestService";
            //Interlocked.Increment( ref currentIteration);  //only 7 times we was here - 1007 -1000
            //var serializer = TypeSetSerializer.AddOrUseExistTSS(0);
            BinarySerializer.AddKnownTypes( //var addedRes =
                typeof(ContractBase),
                typeof(Product),
                typeof(ProductMark), 
                typeof(CommandMessage),
                typeof(CommandContainer2)
                                   );
            //serializer.AddKnownType<ContractBase>();
            //serializer.AddKnownType<Product>();
            //serializer.AddKnownType<ProductMark>();
            //serializer.AddKnownType<CommandMessage>();
            //serializer.AddKnownType<CommandContainer2>();
            //serializer.BuildTypeProcessor ();
            Model1Container.GenerateArrayNCommands1(2, 3);

           
            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = System.Environment.ProcessorCount;

            Parallel.For<ITypeSetSerializer>(0, Iterations, options,
                    () =>  //localInit
                    {

                        return BinarySerializer.Current;//serializer; 
                    }
                    ,
                    (ind, pstate, slzr)=>
                    {
                        try
                        {                            
                            // serialize
                            var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                            // deserialize
                            var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);

                            // compare This is the comparison class                    
                            Utility.CompareSerializeResult("Test_Parallel_OneSharedInstance_ParallelFor", Model1Container.DATACommands, CompareDesererializeDATA);
                   
                            //if action complete succesfully increment currentIteration
                            Interlocked.Increment(ref currentIteration);

                        }
                        catch (System.Threading.ThreadAbortException texc)
                        {
                            string msg = texc.Message;
                            throw texc;
                        }
                        catch (Exception exc)
                        {
                            string msg = exc.Message;
                            throw exc;
                        }                            
                        return slzr;
                    },
                    (s)=>
                    {
                        try
                        {                            

                        }
                        catch (Exception)
                        {
                            
                            throw;
                        }
                        //Interlocked.Increment(ref currentIteration);
                    }
                    
                );

            Int32 iterationsEnd = currentIteration;
        }
        


        /// <summary>
        /// 
        /// </summary>
        [TestMethod]
        public void Test_4DCore_Serialization_Parallel_OneSharedInstance_Tasks()
        {
            Int32 Iterations = 10000;
            Int32 currentIteration = 0;//iteration index to compare counts  at the end


            //prepare Service serializer            
            //var serializerOfService = TypeSetSerializer.AddOrUseExistTSS(0);


            BinarySerializer.AddKnownType<ContractBase>();
            BinarySerializer.AddKnownType<Product>();
            BinarySerializer.AddKnownType<ProductMark>();
            BinarySerializer.AddKnownType<CommandMessage>();
            BinarySerializer.AddKnownType<CommandContainer2>();
            //BinarySerializer.BuildTypeProcessor ();
            Model1Container.GenerateArrayNCommands1(2, 3);


            Action sAction =
                () =>
                {
                    //body(i);  begin
                    try
                    {
                       //var serializer = TypeSetSerializer.GetDirect(0);
                        // serialize
                       var SerializedDATA = BinarySerializer.Serialize(Model1Container.DATACommands);

                        // deserialize
                       var CompareDesererializeDATA = BinarySerializer.Deserialize<List<CommandContainer2>>(SerializedDATA);

                        // compare This is the comparison class                    
                       // compare This is the comparison class                    
                       Utility.CompareSerializeResult("Test_Parallel_OneSharedInstance_Tasks", Model1Container.DATACommands, CompareDesererializeDATA);
                                    

                        //if action complete succesfully increment currentIteration
                        Interlocked.Increment(ref currentIteration);

                    }
                    catch (System.Threading.ThreadAbortException texc)
                    {
                        string msg = texc.Message;
                        throw texc;
                    }
                    catch (Exception exc)
                    {
                        string msg = exc.Message;
                        throw exc;
                    }
                    //body(i) end

                };

            //delegates Array 
            Action[] ParallelActions = new Action[Iterations];
            for (int i = 0; i < ParallelActions.Length; i++)
            { ParallelActions[i] = sAction; }

            //run parallel loop for ParallelActions            
            Task.Factory.StartNew(() =>
            {
                for (int i = 0; i < ParallelActions.Length; i++)
                {
                    int cur = i;
                    Task.Factory.StartNew(  
                        ParallelActions[cur] ,                        
                        TaskCreationOptions.AttachedToParent);
                }
            }            
            )
            .Wait();

            Int32 iterationsEnd = currentIteration;
        }


#endif


        #endregion ----------------------------------- SHARED INSTANCE(MULTIPLETHREADS) -------------------------------------


    }
}
