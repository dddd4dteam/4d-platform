﻿using System;
using DDDD.SDS.Tests.Model.Ignore;


using DDDD.BCL;
#if NET45 || SL5
using Microsoft.VisualStudio.TestTools.UnitTesting;

#elif WP81
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

#endif


namespace DDDD.SDS.Tests.IgnoranceTests
{
    [TestClass]
    public class IgnoranceTest
    {
        [TestMethod]
        public void Test_Ignorance_PublicMemberIgnored_PrivateProcessed()
        {
            try
            {
                // Models assembly need no reference to DDDD.SDS.Net45. IgnoreMemberAttribute
                // Public field wiil be ignored

                IgnoreMemberClass item = new IgnoreMemberClass();
                item.PublicIgnoredProperty = "New string Value";

                var serializer = SDSHubV3.AddOrUseExistSerializer(0);
                serializer.AddContract<IgnoreMemberClass>();

                var bytedata = serializer.Serialize(item);
                var deserializedItem = serializer.Deserialize<IgnoreMemberClass>(bytedata);


                Validator.AssertTrue<ContractDeserializeException>(
                    deserializedItem.IsPublicIgnoredPropertyNotDefault() == true,
                    "Ignore IsSharedFuncModelGroup  still processed by serializer");


                Validator.AssertFalse<ContractDeserializeException>( deserializedItem.PublicIgnoredProperty != item.PublicIgnoredProperty,
                    "Ignore IsSharedFuncModelGroup  still processed by serializer"
                    );
            }
            catch (Exception)
            {                
                throw;
            }        

        }
    }
}
