﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;



namespace DDDD.Core.Issues.Test.SerializationTests.JSON.ArrayTests
{
    /// <summary>
    /// Summary description for ArraysUnitTest
    /// </summary>
    [TestClass]
    public class ArraysUnitTest
    {
        public ArraysUnitTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Test_4DCore_JSON_Array1Dimn_withProductMark()
        {
            try
            {


                var productItems = new ProductMark[2]
                {
                      new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 }
                    , new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                };


                var serializedByFJ = FJ.JSON.ToJSON(productItems);
                var deserializedByFJ = FJ.JSON.ToObject<ProductMark[]>(serializedByFJ);

                var serializedByD4 = D4.JsonSerializer.SerializeToString(productItems);


            }
            catch (Exception exc)
            {
                throw exc;
            }

        }





        [TestMethod]
        public void Test_4DCore_JSON_Array2Dimension_withInt()
        {
            try
            {
               

                int?[,] array2DOfInt = new int?[3, 3];

                array2DOfInt[0, 0] = 10;
                array2DOfInt[1, 1] = 11;
                array2DOfInt[2, 2] = 22;
                // FJ 

                FJ.JSON.Parameters.UsingGlobalTypes = false;

                var serializedByFJ = FJ.JSON.ToJSON(array2DOfInt);
                var objectBack = FJ.JSON.ToObject<int?[,]>(serializedByFJ);
                
                //D4 Json
                var serializedByD4 = D4.JsonSerializer.SerializeToString(array2DOfInt);
                int?[,] deserializedArray2Dimension = D4.JsonSerializer.DeserializeFromString<int?[,]>(serializedByD4);

                //Utility.CompareSerializeResult(nameof(Test_4DCore_JSON_Array2Dimension_withProductMark),
                //                                            array2DOfProducts, deserializedArray2Dimension);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_JSON_Array3Dimension_withInt()
        {
            try
            {


                int?[,,] array3DOfInt = new int?[1, 2, 3];

                array3DOfInt[0, 0, 1] = 10;
                array3DOfInt[0, 1, 1] = 11;
                array3DOfInt[0, 1, 2] = 22;
                // FJson

                fastJSON.JSON.Parameters.UsingGlobalTypes = false;

                var serializedByFJ = fastJSON.JSON.ToJSON(array3DOfInt);
                var objectBack = fastJSON.JSON.ToObject<int?[,]>(serializedByFJ);

                //D$ Json
                var serializedByD4 = D4.JsonSerializer.SerializeToString(array3DOfInt);
                int?[,] deserializedArray2Dimension = D4.JsonSerializer.DeserializeFromString<int?[,]>(serializedByD4);

                //Utility.CompareSerializeResult(nameof(Test_4DCore_JSON_Array2Dimension_withProductMark),
                //                                            array2DOfProducts, deserializedArray2Dimension);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }





        [TestMethod]
        public void Test_4DCore_JSON_Array2Dimension_withProductMark()
        {
            try
            {
                var productItem11 = new ProductMark()
                { Copyrighter = " Test , Copyrighter ",
                    Mark = "Mark1",
                    YearCreated = 1957
                };

                var productItem22 = new ProductMark()
                {
                    Copyrighter = " Test , Copyrighter ",
                    Mark = "Mark2",
                    YearCreated = 1958
                };

                var productItem33 = new ProductMark()
                {
                    Copyrighter = " Test , Copyrighter ",
                    Mark = "Mark3",
                    YearCreated = 1959
                };


                ProductMark[,] array2DOfProducts = new ProductMark[3, 3];

                array2DOfProducts[0, 0] = productItem11;
                array2DOfProducts[1, 1] = productItem22;
                array2DOfProducts[2, 2] = productItem33;
                // FJson

                fastJSON.JSON.Parameters.UsingGlobalTypes = false;

                var serializedByFJ = fastJSON.JSON.ToJSON(array2DOfProducts);
                var objectBack = fastJSON.JSON.ToObject<ProductMark>(serializedByFJ);
                //

                //D$ Json
                var serializedByD4 = D4.JsonSerializer.SerializeToString(array2DOfProducts);               
                ProductMark[,] deserializedArray2Dimension = D4.JsonSerializer.DeserializeFromString<ProductMark[,]>(serializedByD4);

                Utility.CompareSerializeResult(nameof(Test_4DCore_JSON_Array2Dimension_withProductMark),
                                                            array2DOfProducts, deserializedArray2Dimension);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_Collections_Array3Dimension_ContractBase__withProductInside()
        {
            //  try
            //{
            //    var productItem11 = new Product()
            //    {
            //        Id = 11,
            //        Name = "Product11",
            //        Category = "Primary11" //Price = 12500,
            //    };

            //    var productItem22 = new Product()
            //    {
            //        Id = 22,
            //        Name = "Product22",
            //        Category = "Primary22" //Price = 12500,
            //    };

            //    var productItem33 = new Product()
            //    {
            //        Id = 33,
            //        Name = "Product33",
            //        Category = "Primary33" //Price = 12500,
            //    };

            //    var productItem44 = new Product()
            //    {
            //        Id = 44,
            //        Name = "Product44",
            //        Category = "Primary44" //Price = 12500,
            //    };
            
            //}
            //catch (Exception exc)
            //{
            //    throw exc;
            //}
        }

    }
}
