﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;


using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;


namespace DDDD.Core.Issues.Test.Serialization.JSON
{
    [TestClass]
    public class EnumTests
    {
        // static String ServiceKey = "SomeServiceKey";



        [TestMethod]
        public void Test_4DCore_Serialization_Enum1_Test()
        {
            try
            {
                var valueEnum = SomeTypologyEn.Birds;
               // var strEnumValue = valueEnum.S();
               // var strEnumValue2 = valueEnum.GetFieldInfo().Name;
               // var strEnumValue3 = Enum.GetName(typeof(SomeTypologyEn),valueEnum);

                // FJ.JSON.Parameters.UsingGlobalTypes = false;

                var serializedByFJ = FJ.JSON.ToJSON(valueEnum);
                // "\"Birds\""      "\"Birds\""
                var objectBack = FJ.JSON.ToObject<SomeTypologyEn>(serializedByFJ);

                // D$ serializer
                var serializedByD4 = D4.JsonSerializer.SerializeToString(valueEnum);
                var deserializedByD4 = D4.JsonSerializer.DeserializeFromString<SomeTypologyEn>(serializedByD4);



                var listEnum = new List<SomeTypologyEn>()
                { SomeTypologyEn.Birds
                , SomeTypologyEn.Peoples };
                serializedByFJ = FJ.JSON.ToJSON(listEnum);
                //   "[\"Birds\",\"Peoples\"]"
                var objectBack2 = FJ.JSON.ToObject<List<SomeByteTypologyEn>>(serializedByFJ);




            }
            catch (InvalidOperationException invExc)
            {
                Debug.WriteLine(
                    string.Format(
                        "Correct exception - enum contract cannot be added by hands it 'l be made auto : [{0}] "
                        , invExc.Message));
            }
            catch(Exception exc)
            {
                Debug.WriteLine(
                   string.Format(
                       "Correct exception - enum contract cannot be added by hands it 'l be made auto : [{0}] "
                       , exc.Message) );
            }

        }


      



    }
}
