﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.Polimorphism;

namespace DDDD.Core.Issues.Test.Serialization.JSON
{



    //var serializedByFJ = FJ.JSON.ToJSON(lisdtData);
    //var deserializedByFJ = FJ.JSON.ToObject<List<object>>(serializedByFJ);


    [TestClass]
    public class PolimorphismTests
    {
        [TestMethod]
        public void Test_4DCore_SerializationJSON_ListOfObject_Struct()
        {
            try
            {
 

                var lisdtData = new List<object>()
                {
                       123
                     , null
                     , 4444.56567
                     , false
                     , "string VALUE"
                     , DateTime.Now
                     //, Guid.NewGuid()
                     , new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 }                     
                     //, null
                     //, new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                     //, 333
                };
                
                var serializedByD4 = D4.JsonSerializer.SerializeToString(lisdtData);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<object>>(serializedByD4);
                

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        [TestMethod]
        public void Test_4DCore_SerializationJSON_ListOf_Object_Class()
        {
            try
            {                 

                var lisdtData = new List<object>()
                {
                       123
                     , null
                     , 4444.56567
                     , false
                     , "string VALUE"
                     , DateTime.Now
                     //, Guid.NewGuid()
                     , new Gudjet1() { Category = " Audio Tools ", ID = 123123, 
                          Name = "Headphines SONY ", YearCreated = 1957 }                     
                     
                     , null
                     , new Gudjet2() { Length = 47, ID = 1213,
                           Name = "Headphines SONY " }

                     , null
                     , new Gudjet3() {Color ="Black", ID = 1278,
                           Name = "Headphines SONY " }                     

                     //, new ProductMark() { Copyrighter = " TestCopyrighter2 ", Mark = "Mark Test1", YearCreated = 1958 }
                     //, 333
                };


                var serializedByD4 = D4.JsonSerializer.SerializeToString(lisdtData);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<object>>(serializedByD4);


            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        [TestMethod]
        public void Test_4DCore_SerializationJSON_ListOf_Interface()
        {
            try
            {

                var lisdtData = new List<IGudjet>()
                {
                     
                     new GudjetStruct() {  Category = " Audio Tools ", ID = 13057,
                          Name = "Headphines SONY ", YearCreated = 1957 }

                     , null
                     ,  new Gudjet1() { Category = " Audio Tools ", ID = 123123,
                          Name = "Headphines SONY ", YearCreated = 1957 }

                     , null
                     , new Gudjet2() { Length = 47, ID = 1213,
                           Name = "Headphines SONY " }

                     , null
                     , new Gudjet3() {Color ="Black", ID = 1278,
                           Name = "Headphines SONY " }


                };


                var serializedByD4 = D4.JsonSerializer.SerializeToString(lisdtData);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<IGudjet>>(serializedByD4);

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }




        [TestMethod]
        public void Test_4DCore_SerializationJSON_ListOf_BaseClass()
        {
            try
            {

                var lisdtData = new List<GudjetBase>()
                {
                       null
                     , new Gudjet1() { Category = " Audio Tools ", ID = 123123,
                          Name = "Headphines SONY ", YearCreated = 1957 }

                     , null
                     , new Gudjet2() { Length = 47, ID = 1213,
                           Name = "Headphines SONY " }

                     , null
                     , new Gudjet3() {Color ="Black", ID = 1278,
                           Name = "Headphines SONY " }
                };


                var serializedByD4 = D4.JsonSerializer.SerializeToString(lisdtData);
                var length = serializedByD4.Length;
                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<GudjetBase>>(serializedByD4);
                 
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }






    }

}
