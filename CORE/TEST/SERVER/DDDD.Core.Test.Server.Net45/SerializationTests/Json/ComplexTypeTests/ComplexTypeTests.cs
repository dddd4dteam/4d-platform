﻿
using System;
using System.Text;
using System.Linq;
using System.Collections;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using D4 = DDDD.Core.Serialization.Json;
using ST = ServiceStack.Text;
using FJ = fastJSON;

using DDDD.Core.Serialization;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.Core.TestUtils;
using DDDD.Core.Serialization.Json;

using DDDD.Core.Model.CommunicationModel;
using DDDD.Core.Tests.Model.Communication;
using DDDD.Core.Tests.Model.CommunicationModel;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.TSS.Tests.Model.CommunicationModel;


namespace DDDD.Core.Issues.Test.Serialization.JSON
{
    /// <summary>
    /// Summary description for ComplexTypeTests
    /// </summary>
    [TestClass]
    public class ComplexTypeTests
    {
        public ComplexTypeTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion



        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_SimpleStructTest()
        {
            try
            {
                var dataItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1" ,YearCreated = 1957 };

                var serializedByD4 = D4.JsonSerializer.SerializeToString(dataItem);
                // D4 - "{Copyrighter : \" TestCopyrighter \",Mark : \"Mark Test1\",YearCreated : 0}" 
                // "{Copyrighter : \" TestCopyrighter \",Mark : \"Mark Test1\",YearCreated : 1957}"
                //  "{\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}"

                fastJSON.JSON.Parameters.UsingGlobalTypes = false;
                
                var serializedByFJ = fastJSON.JSON.ToJSON(dataItem);
                var objectBack = fastJSON.JSON.ToObject<ProductMark>(serializedByD4);
                // FJ - "{\"$types\":{\"DDDD.TSS.Tests.Model.CommunicationModel.ProductMark, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\":\"1\"},\"$type\":\"1\",\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":0}"
                //       "{\"$type\":\"DDDD.TSS.Tests.Model.CommunicationModel.ProductMark, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\",\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}"


                var serializeByST = ST.JsonSerializer.SerializeToString(dataItem);
                //ST -
                var deserialisedInstance = ST.JsonSerializer.DeserializeFromString<ProductMark>(serializedByD4);

               
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_ComplexType_GenerateDebugCode4SerializeToStringTest()
        {
            try
            {
                D4.JsonSerializer.AddKnownType<ProductMark>();
                var tpSerializer = D4.JsonSerializer.GetTypeSerializer(typeof(ProductMark)) as D4.ITypeSerializerJson<ProductMark>;
                DebugCodeCompiler.GenerateDebugAssembly_JsonSerializeToStringTHandlerDbg<ProductMark>(tpSerializer);

            }
            catch (System.Exception exc)
            {
                throw exc;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_Object_DeserializationLogicTest()
        {
            try
            {
                var productItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 };
                //object dataItem = productItem; 

                var jsonString2 = "{\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}";
                int index2 = 0;
                Json1.PassObjectSTART(jsonString2, ref index2);
                var member1Name = Json1.PassNGetMemberName(jsonString2, ref index2);
                Json1.PassColon(jsonString2, ref index2);
                var member1Value = Json1.PassNGetStringContentOnlyValue(jsonString2, ref index2);

                Json1.PassComma(jsonString2, ref index2);
                var member2Name = Json1.PassNGetMemberName(jsonString2, ref index2);
                Json1.PassColon(jsonString2, ref index2);
                var member2Value = Json1.PassNGetStringContentOnlyValue(jsonString2, ref index2);

                Json1.PassComma(jsonString2, ref index2);
                var member3Name = Json1.PassNGetMemberName(jsonString2, ref index2);
                Json1.PassColon(jsonString2, ref index2);
                var member3Value = Json1.PassNGetNumberValue(jsonString2, ref index2);




            }
            catch (Exception exc)
            {

                throw;
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_Object_SerializeToStringTest()
        {
            try
            {
                var productItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 };


                var serializedByD4 = D4.JsonSerializer.SerializeToString(productItem);
                var deserializedByD4 = D4.JsonSerializer.DeserializeFromString<ProductMark>(serializedByD4);

                //"{$type:DDDD.TSS.Tests.Model.CommunicationModel.ProductMark,DDDD.Core.Test.Model.Net45\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}"
                //"{\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}"
                 
                Utility.CompareSerializeResult("Test_4DCore_Serialization_Object_SerializeToStringTest", productItem, deserializedByD4);

            }
            catch (System.Exception ex)
            {
            	
            }
           
        }






        [TestMethod]
        public void Test_4DCore_Serialization_ListOfObject_SerializeToStringTest()
        {
            try
            {
                var productItem = new ProductMark() { Copyrighter = " TestCopyrighter ", Mark = "Mark Test1", YearCreated = 1957 };

                var list = new List<object>() { 32, "44", 55, productItem };

                var serializedByFJ = fastJSON.JSON.ToJSON(list);
                // "[32,\"44\",55]"
                // "[32,\"44\",55,{\"$type\":\"DDDD.TSS.Tests.Model.CommunicationModel.ProductMark, DDDD.Core.Test.Model.Net45, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null\",\"Copyrighter\":\" TestCopyrighter \",\"Mark\":\"Mark Test1\",\"YearCreated\":1957}]"

                var strjson = ST.JsonSerializer.SerializeToString(list);
                // "[32,\"44\",55]" 

                var serialized4DT = D4.JsonSerializer.SerializeToString(list);

                var deserialized4DT = D4.JsonSerializer.DeserializeFromString<List<object>>(serialized4DT);


            }
            catch (System.Exception ex)
            {

            }
        }


        [TestMethod]
        public void Test_4DCore_Serialization_ArrayOf2Dimensions_SerializeToStringTest()
        {
            try
            {
                var arrayToSerialize = new int[2, 4] { { 2,4,3,4 }, {1,7,9,5} };

                var strjson = ST.JsonSerializer.SerializeToString( arrayToSerialize);

                //  "[[2,4,3,4],[1,7,9,5]]"


                var objectArray = new object[5] { true, DateTime.Now, 45, null, 45.78 };
                var fjSerializedString = fastJSON.JSON.ToJSON(objectArray);
                //"[true,\"2017-02-02T18:01:46Z\",45,null,45.78]"


            }
            catch (System.Exception ex)
            {

            }
        }

        


        [TestMethod]
        public void Test_4DCore_Serialization_EatBetween_Test1()
        {
            try
            {
                var someData = "error  input text { input1 { input2  { input3 } output2} output1 } error output text";
                var startChar = '{'; var endChar = '}';

                //var bytedVal1 = D4.Json1.EatBetweenExclusive(ref someData, startChar, endChar);
                //// - "error  input text  error output text"
                //var bytedVal2 = D4.Json1.EatBetweenExclusive(ref bytedVal1, startChar, endChar);
                //var bytedVal3 = D4.Json1.EatBetweenExclusive(ref bytedVal2, startChar, endChar);


                //var SomeData2 = new StringBuilder("error  input text { input1 { input2  { input3 } output2} output1 } error output text");
                //var bytedVal2_1 = D4.Json1.EatBetweenExclusive(SomeData2, startChar, endChar);
                //// - {error  input text  error output text}
                //var bytedVal2_2 = EatBetweenExclusive( bytedVal2_1, startChar, endChar);
                //var bytedVal2_3 = EatBetweenExclusive( bytedVal2_2, startChar, endChar);



            }
            catch (System.Exception ex)
            {
            	
            }
        }

        [TestMethod]
        public void Test_4DCore_Serialization_EatStringVal_Test1()
        {
            try
            {
                var someData = " some input text  \" here text we need to cut\" some output text  : \"value2\"  ";
                //var bytedVal = D4.Json1.EatStringValue(ref someData);
                //var bytedVal2 = D4.Json1.EatStringValue(ref someData);
                
            }
            catch (System.Exception ex)
            {
            	
            }
        }



        [TestMethod]
        public void Test_4DCore_Serialization_Parse1EatStringVal_Test1()
        {
            try
            {
                


            }
            catch (Exception ex)
            {
            	
            }
        }





     }
}
