﻿#if SERVER 
using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
#endif

using DDDD.Core.Net.ServiceModel;
//using DDDD.Core.Test.Web.Models;
using DDDD.Core.Threading;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;


//using CRM.DAL;
//using CRM.Dom.Modules.ClientReport;

using System;
using System.IO;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Threading.Tasks;
using System.Collections.Generic;



namespace DDDD.Core.Test.CommandManager
{

    /// <summary>
    /// 
    /// </summary>
    public enum ClientReportCommandEn
    {        
        NotDefined,
        LoadClientReport
    }



    /// <summary>
    /// Client Report Command Manager
    /// </summary>
    public class ClientReport_CommandManager
    {

#if SERVER
                
        
        /// <summary>
        /// SQL Connection to CRM Database
        /// </summary>
        public static LazySlim<string> ConnectionString
        { get; } = LazySlim<string>.Create(
                                    (args) =>
                                    {
                                        return ConfigurationManager.ConnectionStrings["CRM2"].ConnectionString;
                                    }, null, null
                                    );
             


        static DbManager CreateDBManager()
        {
            try
            {
                return new DbManager(new SqlDataProvider(), ConnectionString.Value);
            }
            catch (Exception exc )
            {    throw new InvalidOperationException($"ERROR IN {nameof(ClientReport_CommandManager)}.{nameof(CreateDBManager)}() - Exception Message [{exc.Message}] ", exc );
            }
            
        }
             
       



        public static void ProcessCommand(ref DCMessage clientMessage)
        {
            if (clientMessage.Command == nameof(ClientReportCommandEn.LoadClientReport) )
            {
                 LoadClientReportData_Server(clientMessage);
            }                        
        }


        static void LoadClientReportData_Server(DCMessage  message)
        {
            //Client ask for ReportData for client with ID_Client
            //var param_IdClient = message.Parameters[nameof(V_S8_Client_Vo.Id_Client)];
            
            //ClientReportInfoContainer.Load_ClientReportInfoFromDB((Guid)param_IdClient );

            //message.Result = ClientReportInfoContainer.ReportItem;            
            // no DB Connection/  Error
        }




        // END SERVER SIDE 
#elif CLIENT // BEGIN CLIENT SIDE  


        public static Type WcfServiceContract
        { get; } = typeof(IClientReportService);


        static Binding DCSClientBinding
        { get; } = DynamicCommandServiceClient.CreateCustomBinding( sendTimeoutMins:2, sendTimeoutSecs:30, receiveTimeoutMins:2, receiveTimeoutSecs:30  );


        static DynamicCommandServiceClient DCSClient
        { get; } = GetDCSClient();


        static DynamicCommandServiceClient GetDCSClient()
        {
           return DynamicCommandServiceClient.AddOrUseExistSvcClient(
                   WcfServiceContract
                 , DCSClientBinding
                 , useTSSSerialization: true
                 //, KnownTypesLoaderFunc: null 
                 , routeTemplate : HttpRoutes.HttpRouteTemplateEn.HttpRoute_BaseurlWebappSubfldrService
                 , ServiceFileName: "ClientReportService" 
                 , WebApplication: "DDDD.Core.Test.Web", Subfolder: "Services" //, Server: null, Port: null,

                );
        }


        public async static Task<ClientReportInfoContainer> LoadClientReportData_Client(Guid Id_Client)
        {
            try
            {
                var cmdLoadClientReport = DCommandMessage.Create(
                      nameof(ClientReport_CommandManager)              // targetCommandManager:
                    , nameof(ClientReportCommandEn.LoadClientReport)   // command: 
                    , nameof(V_S8_Client_Vo.Id_Client).KVPair<object>(Id_Client) //  commandParameters: 
                    );
                
                var serializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer( DCSClient.ServiceKey);

                var serdData =  serializer.Serialize(cmdLoadClientReport);

                var deserdCommand = serializer.Deserialize<object>(serdData); //into object

                await DCSClient.ExecuteCommand(cmdLoadClientReport, null);

                return cmdLoadClientReport.Result as ClientReportInfoContainer;

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }





        public static void ResetClientReportData(Guid clientId)
        {
            
        }
        
#endif      // END CLIENT SIDE 






    }
}
