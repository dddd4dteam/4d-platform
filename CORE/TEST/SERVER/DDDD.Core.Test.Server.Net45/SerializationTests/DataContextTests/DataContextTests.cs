﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using CRM.DAL;
using DDDD.Core.Net.ServiceModel;
using DDDD.Core.Extensions;
using DDDD.Core.Serialization;
using DDDD.TSS.Tests.Model.EnumModel;
using DDDD.Core.TestUtils;
using DDDD.Core.Reflection;
using System.Reflection;

using DDDD.Core.Test.Web.Models;
using DDDD.Core.Test.CommandManager;

namespace DDDD.Core.Net45.Tests.SerializationTests.DataContextTests
{
    /// <summary>
    /// Summary description for DataContextTests
    /// </summary>
    [TestClass]
    public class DataContextTests
    {
        public DataContextTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// Transferring Types
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<Type> TransferTypes()
        {
            var result = new List<Type>();

            //result.Add(typeof(ClientReportInfoContainer));
            //result.Add(typeof(V_S8_Client_Vo));
            //result.Add(typeof(crs_S10_EkspositorOrderImage));
            //result.Add(typeof(V_S8_ClientDiler_Vo));
            //result.Add(typeof(V_S8_ClientGoodsCategory_Vo));
            //result.Add(typeof(V_S8_ClientRival_Vo));
            //result.Add(typeof(V_S8_ClientAssortment_Vo));
            //result.Add(typeof(V_S8_ClientMaterial_Vo));
            //result.Add(typeof(V_S9_Meeting_Vo));
            //result.Add(typeof(V_S10_Ekspositor));
            //result.Add(typeof(V_S17_FormsClient_Vo));

            return result;
        }

        /// <summary>
        ///  Method returns a generic IEnumerable of service model known contracts. 
        /// </summary>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static IEnumerable<Type> GetClientReportKnownTypes(ICustomAttributeProvider provider)
        {
            var transferTypes = new List<Type>() { typeof(CommandMessage), typeof(DCMessage) }; //, typeof(CustomCommandMessage)

#if SL5 || WPF || NET45
            transferTypes.AddRange(TransferTypes());
#endif
            //ADD OTHER CUSTOM CONTRACTS to TSS HERE or load known contracts from your config to DataContract Serializer

            return transferTypes;
        }


        [TestMethod]
        public void Test_4DCore_Data_ClientReportInfo_ReloadTest()
        {
            try
            {
              //var stubLoadedInStaticCtor =  ClientReportInfoContainer.StubProperty;// Load_ClientReportInfoFromDB(new Guid("7670C94C-E82C-408E-A6E9-08F0CA7A757F"));

            }
            catch (Exception exc)
            {
                throw exc;
            }            
        }



        [TestMethod]
        public void Test_4DCore_Serialization_DataContext_ClientReportInfo_Test()
        {
            try
            {

                var Id_Client = new Guid("7670C94C-E82C-408E-A6E9-08F0CA7A757F");
                //
                // TODO: Add test logic here
                //
                var cmdLoadClientReport = DCMessage.Create(
                    nameof(ClientReport_CommandManager)              // targetCommandManager:
                  , nameof(ClientReportCommandEn.LoadClientReport)   // command: 
                  , nameof(V_S8_Client_Vo.Id_Client).KVPair<object>(Id_Client) //  commandParameters: 
                  );

                //ClientReport_CommandManager. LoadClientReportData_Server(cmdLoadClientReport);

                ClientReportInfoContainer.Load_ClientReportInfoFromDB(new Guid("7670C94C-E82C-408E-A6E9-08F0CA7A757F"));
                                
                //serializer
                var serializer = TypeSetSerializer.AddOrUseExistTypeSetSerializer(0);

                foreach (var knownType in GetClientReportKnownTypes(null))
                { serializer.AddKnownType(knownType);
                }

                var dataSerialized3 = serializer.Serialize(cmdLoadClientReport);
                var testDeserialized = serializer.Deserialize<DCMessage>(dataSerialized3);

                Utility.CompareSerializeResult(nameof(Test_4DCore_Serialization_DataContext_ClientReportInfo_Test), cmdLoadClientReport, testDeserialized, TypeMemberSelectorEn.PublicOnly);



            }
            catch (Exception exc)
            {

            }  
       }

    }
}
