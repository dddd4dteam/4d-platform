var Greeter = /** @class */ (function () {
    function Greeter(element) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }
    Greeter.StringHashCode20 = function (value) {
        var num = 352654597;
        var num2 = num;
        for (var i = 0; i < value.length; i += 4) {
            var ptr0 = value.charCodeAt(i) << 16;
            if (i + 1 < value.length)
                ptr0 |= value.charCodeAt[i + 1];
            num = (num << 5) + num + (num >> 27) ^ ptr0;
            if (i + 2 < value.length) {
                var ptr1 = value.charCodeAt[i + 2] << 16;
                if (i + 3 < value.length)
                    ptr1 |= value.charCodeAt[i + 3];
                num2 = (num2 << 5) + num2 + (num2 >> 27) ^ ptr1;
            }
        }
        return num + num2 * 1566083941;
    };
    Greeter.prototype.start = function () {
        var _this = this;
        this.timerToken = setInterval(function () { return _this.span.innerHTML = new Date().toUTCString(); }, 500);
    };
    Greeter.prototype.stop = function () {
        clearTimeout(this.timerToken);
    };
    return Greeter;
}());
window.onload = function () {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};
//# sourceMappingURL=app.js.map