﻿class Greeter {
    element: HTMLElement;
    span: HTMLElement;
    timerToken: number;

    constructor(element: HTMLElement) {
        this.element = element;
        this.element.innerHTML += "The time is: ";
        this.span = document.createElement('span');
        this.element.appendChild(this.span);
        this.span.innerText = new Date().toUTCString();
    }


    public static  StringHashCode20(value: string): number
    {
        let num: number = 352654597;
        let num2: number  = num;         

        for (var i = 0; i < value.length; i += 4)
        {
            let ptr0: number = value.charCodeAt(i) << 16;
            if (i + 1 < value.length)
                ptr0 |= value.charCodeAt[i + 1];

            num = (num << 5) + num + (num >> 27) ^ ptr0;

            if (i + 2 < value.length) {
                let ptr1: number = value.charCodeAt[i + 2] << 16;
                if (i + 3 < value.length)
                    ptr1 |= value.charCodeAt[i + 3];
                num2 = (num2 << 5) + num2 + (num2 >> 27) ^ ptr1;
            }
        }

        return num + num2 * 1566083941;
    }



    start() {
        this.timerToken = setInterval(() => this.span.innerHTML = new Date().toUTCString(), 500);
    }

    stop() {
        clearTimeout(this.timerToken);
    }

}

window.onload = () => {
    var el = document.getElementById('content');
    var greeter = new Greeter(el);
    greeter.start();
};