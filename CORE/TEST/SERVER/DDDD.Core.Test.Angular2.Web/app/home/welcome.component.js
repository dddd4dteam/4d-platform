System.register(["angular2/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, WelcomeComponent;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            WelcomeComponent = /** @class */ (function () {
                function WelcomeComponent() {
                    this.pageTitle = "Welcome";
                    //public someFile: File =       
                    //protected _xhrTransport(item: FileItem): any {
                    //    let xhr = item._xhr = new XMLHttpRequest();
                    //    xhr.send(
                    //    let sendable: any;
                    //    this._onBeforeUploadItem(item);
                    //    // todo
                    //    /*item.formData.map(obj => {
                    //     obj.map((value, key) => {
                    //     form.append(key, value);
                    //     });
                    //     });*/
                    //    if (typeof item._file.size !== 'number') {
                    //        throw new TypeError('The file specified is no longer valid');
                    //    }
                    //    if (!this.options.disableMultipart) {
                    //        sendable = new FormData();
                    //        this._onBuildItemForm(item, sendable);
                    //        sendable.append(item.alias, item._file, item.file.name);
                    //    } else {
                    //        sendable = item._file;
                    //    }
                    //    xhr.upload.onprogress = (event: any) => {
                    //        let progress = Math.round(event.lengthComputable ? event.loaded * 100 / event.total : 0);
                    //        this._onProgressItem(item, progress);
                    //    };
                    //    xhr.onload = () => {
                    //        let headers = this._parseHeaders(xhr.getAllResponseHeaders());
                    //        let response = this._transformResponse(xhr.response, headers);
                    //        let gist = this._isSuccessCode(xhr.status) ? 'Success' : 'Error';
                    //        let method = '_on' + gist + 'Item';
                    //        (this as any)[method](item, response, xhr.status, headers);
                    //        this._onCompleteItem(item, response, xhr.status, headers);
                    //    };
                    //    xhr.onerror = () => {
                    //        let headers = this._parseHeaders(xhr.getAllResponseHeaders());
                    //        let response = this._transformResponse(xhr.response, headers);
                    //        this._onErrorItem(item, response, xhr.status, headers);
                    //        this._onCompleteItem(item, response, xhr.status, headers);
                    //    };
                    //    xhr.onabort = () => {
                    //        let headers = this._parseHeaders(xhr.getAllResponseHeaders());
                    //        let response = this._transformResponse(xhr.response, headers);
                    //        this._onCancelItem(item, response, xhr.status, headers);
                    //        this._onCompleteItem(item, response, xhr.status, headers);
                    //    };
                    //    xhr.open(item.method, item.url, true);
                    //    xhr.withCredentials = item.withCredentials;
                    //    // todo
                    //    /*item.headers.map((value, name) => {
                    //     xhr.setRequestHeader(name, value);
                    //     });*/
                    //    if (this.options.headers) {
                    //        for (let header of this.options.headers) {
                    //            xhr.setRequestHeader(header.name, header.value);
                    //        }
                    //    }
                    //    if (this.authToken) {
                    //        xhr.setRequestHeader('Authorization', this.authToken);
                    //    }
                    //    xhr.send(sendable);
                    //    this._render();
                    //}
                }
                WelcomeComponent = __decorate([
                    core_1.Component({
                        templateUrl: 'app/home/welcome.component.html'
                    })
                ], WelcomeComponent);
                return WelcomeComponent;
            }());
            exports_1("WelcomeComponent", WelcomeComponent);
        }
    };
});
//# sourceMappingURL=welcome.component.js.map