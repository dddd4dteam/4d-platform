System.register(["angular2/core"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, AppComponent;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            }
        ],
        execute: function () {
            AppComponent = /** @class */ (function () {
                function AppComponent() {
                    this.pageTitle = 'Acme Product Management';
                }
                AppComponent.prototype.UploadFile = function () {
                    var uploadRequest = new XMLHttpRequestUpload();
                    uploadRequest.onloadstart = function (ev) {
                    };
                    uploadRequest.onload = function (ev) {
                    };
                };
                AppComponent.prototype.TestJson = function () {
                    // DATE
                    var dateval = Date.now;
                    var datevalJson = JSON.stringify(dateval);
                    console.log('Date -  Json:[' + datevalJson + ']  ');
                    var datevalBack = JSON.parse(datevalJson);
                    //Time
                    var timeString = (new Date()).getTime().toString();
                    console.log('Date.Time -  :[' + timeString + ']  ');
                    var datevalBack2 = JSON.parse(timeString);
                    console.log('Date.Time  back - from string :[' + datevalBack2.toString() + ']  ');
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'pm-app',
                        template: "\n    <div><h1>{{pageTitle}}</h1>\n        <div>My First Component</div>        \n    </div>\n    "
                        //,templateUrl: 'app/app.component.html'
                    })
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    };
});
//# sourceMappingURL=app.component.js.map