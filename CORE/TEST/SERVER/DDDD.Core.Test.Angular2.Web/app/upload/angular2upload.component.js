System.register(["@angular/core", "@angular/common", "../../node_modules/ng2-file-upload/ng2-file-upload"], function (exports_1, context_1) {
    "use strict";
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var core_1, common_1, ng2_file_upload_1, URL, Angular2UploadComponent;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (common_1_1) {
                common_1 = common_1_1;
            },
            function (ng2_file_upload_1_1) {
                ng2_file_upload_1 = ng2_file_upload_1_1;
            }
        ],
        execute: function () {
            // upload url
            URL = '/api/';
            Angular2UploadComponent = /** @class */ (function () {
                function Angular2UploadComponent() {
                    this.uploader = new ng2_file_upload_1.FileUploader({ url: URL });
                    this.hasBaseDropZoneOver = false;
                    this.hasAnotherDropZoneOver = false;
                }
                Angular2UploadComponent.prototype.fileOverBase = function (e) {
                    this.hasBaseDropZoneOver = e;
                };
                Angular2UploadComponent.prototype.fileOverAnother = function (e) {
                    this.hasAnotherDropZoneOver = e;
                };
                Angular2UploadComponent = __decorate([
                    core_1.Component({
                        selector: 'angular-2-upload',
                        template: 'angular-2-upload-file.html',
                        directives: [ng2_file_upload_1.FILE_UPLOAD_DIRECTIVES, common_1.NgClass, common_1.NgStyle, common_1.CORE_DIRECTIVES, common_1.FORM_DIRECTIVES]
                    })
                ], Angular2UploadComponent);
                return Angular2UploadComponent;
            }());
            exports_1("Angular2UploadComponent", Angular2UploadComponent);
        }
    };
});
//# sourceMappingURL=angular2upload.component.js.map