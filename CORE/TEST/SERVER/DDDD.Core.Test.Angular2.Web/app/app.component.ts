﻿import { Component } from 'angular2/core';

@Component({
    selector: 'pm-app',
    template: `
    <div><h1>{{pageTitle}}</h1>
        <div>My First Component</div>        
    </div>
    `
    //,templateUrl: 'app/app.component.html'
})


export class AppComponent {
    pageTitle: string = 'Acme Product Management';


    public UploadFile():void
    {
        let uploadRequest = new XMLHttpRequestUpload();

        uploadRequest.onloadstart = (ev) =>
        {
        };

        uploadRequest.onload = (ev) =>
        {
        }


    }



    public TestJson(): void
    {
        // DATE
        var dateval = Date.now;
        var datevalJson = JSON.stringify(dateval);
        console.log('Date -  Json:[' + datevalJson + ']  ');

        var datevalBack = JSON.parse(datevalJson);

        //Time
        var timeString = (new Date()).getTime().toString();
        console.log('Date.Time -  :[' + timeString + ']  ');
                    
        var datevalBack2 = JSON.parse(timeString) as Date;
        console.log('Date.Time  back - from string :[' + datevalBack2.toString() + ']  ');

 

    }


}