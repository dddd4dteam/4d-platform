﻿import { Binary } from "angular2/src/core/change_detection/parser/ast";






declare module System
{
    interface Guid {
    }

    //export class Guid {
    //    constructor(public guid: string) {
    //        this._guid = guid;
    //    }

    //    private _guid: string;

    //    public ToString(): string {
    //        return this.guid;
    //    }

    //    // Static member
    //    static MakeNew(): Guid {
    //        var result: string;
    //        var i: string;
    //        var j: number;

    //        result = "";
    //        for (j = 0; j < 32; j++) {
    //            if (j == 8 || j == 12 || j == 16 || j == 20)
    //                result = result + '-';
    //            i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
    //            result = result + i;
    //        }
    //        return new Guid(result);
    //    }
    //}


}

declare module System.Collections.Generic
{
    interface KeyValuePair<TKey, TValue>
    {
        Key: TKey;
        Value: TValue;
    }
}




declare module DDDD.Core.ComponentModel.Messaging
{


    interface ICommandMessage
    {
        Body: number[]; // Binary
        Command: string;
        Controller: string;
        ErrorCode: number;
        HasOperationSuccessfullyCompleted: boolean;
        HasServerErrorHappened: boolean;
        ID: System.Guid;
        Parameters: System.Collections.Generic.KeyValuePair<string, any>[];
        ProcessingFaultMessage: string;
        ProcessingSuccessMessage: string;
        Result: any;
        ResultJsonTypeName: string;

    
                 
        ServiceKey: string;
        ServiceNamespace: string;

        TargetCommandManager: string;
        TargetCommandManagerID: number;
    }
        





    interface IDCMessage extends ICommandMessage {
        ClientRecievedResultFromServerTime: Date;
        ClientSendTime: Date;
        Empty: IDCMessage;
        OperationProgressPercent: number;
        OperationProgressState: string;
        ServerReceivedFromClientTime: Date ;
        ServerSendToClientTime: Date;
        UseOperationProgress: boolean;
    }
}


