﻿DEPLOYMENT ASPECT.
To deploy only used in index.html  scripts, we need to create-precoping mech for each of items in node_modules into scripts app folder.
We'll use project file editing - we add <ItemGroup><NodeItem> ././ for each of needable scripts/styles.

We use the following files:

    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
   
    <!-- 1. Load libraries -->
    <!-- IE required polyfills, in this exact order -->
    <script src="node_modules/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
    <script src="node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="node_modules/systemjs/dist/system-polyfills.js"></script>
    <script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script>
    <script src="node_modules/rxjs/bundles/Rx.js"></script>
    <script src="node_modules/angular2/bundles/angular2.dev.js"></script>

<ItemGroup>
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/bootstrap/dist/css/bootstrap.css" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/es6/dev/src/testing/shims_for_IE.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/es6-shim/es6-shim.min.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/systemjs/dist/system-polyfills.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/bundles/angular2-polyfills.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/systemjs/dist/system.src.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/rxjs/bundles/Rx.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/bundles/angular2.dev.js" />
</ItemGroup>
<Target Name="CopyFiles" BeforeTargets="Build">
  <Copy SourceFiles="@(NodeLib)" DestinationFolder="$(MSBuildProjectDirectory)\scripts"/>
</Target>