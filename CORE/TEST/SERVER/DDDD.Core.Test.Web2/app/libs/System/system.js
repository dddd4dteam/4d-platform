var System;
(function (System) {
    var Guid = (function () {
        function Guid(guid) {
            this.guid = guid;
            this._guid = guid;
        }
        Guid.prototype.ToString = function () {
            return this.guid;
        };
        //public override int GetHashCode() {
        //    return _a ^ (((int)_b << 16) | (int)(ushort)_c) ^ (((int)_f << 24) | _k);
        //}
        // Static member
        Guid.MakeNew = function () {
            var result;
            var i;
            var j;
            result = "";
            for (j = 0; j < 32; j++) {
                if (j == 8 || j == 12 || j == 16 || j == 20)
                    result = result + '-';
                i = Math.floor(Math.random() * 16).toString(16).toUpperCase();
                result = result + i;
            }
            return new Guid(result);
        };
        return Guid;
    }());
    System.Guid = Guid;
    var KeyValuePair = (function () {
        function KeyValuePair(key, value) {
            this.Key = key;
            this.Value = value;
        }
        return KeyValuePair;
    }());
    System.KeyValuePair = KeyValuePair;
    var HashUtil = (function () {
        function HashUtil() {
        }
        HashUtil.OS = function (checkOS) {
            //example of a script that sets the variable OSName to reflect the actual client OS.
            //
            // This script sets OSName variable as follows:
            // "Windows"    for all versions of Windows
            // "MacOS"      for all versions of Macintosh OS
            // "Linux"      for all versions of Linux
            // "UNIX"       for all other UNIX flavors 
            // "Unknown OS" indicates failure to detect the OS
            var OSName = "Unknown OS";
            if (navigator.appVersion.indexOf("Win") != -1)
                OSName = "Windows";
            if (navigator.appVersion.indexOf("Mac") != -1)
                OSName = "MacOS";
            if (navigator.appVersion.indexOf("X11") != -1)
                OSName = "UNIX";
            if (navigator.appVersion.indexOf("Linux") != -1)
                OSName = "Linux";
            if (OSName === checkOS)
                return true;
            else
                return false;
            //document.write('Your OS: ' + OSName);
        };
        HashUtil.GetHashCode = function (somestring) {
            /**
             * C# GetHashCode - in string
            unsafe {
                fixed (char *src = this) {
                    Contract.Assert(src[this.Length] == '\0', "src[this.Length] == '\\0'");
                    Contract.Assert( ((int)src)%4 == 0, "Managed string should start at 4 bytes boundary");

#if WIN32
                    int hash1 = (5381<<16) + 5381;
#else
                    int hash1 = 5381;
#endif
                    int hash2 = hash1;

#if WIN32
                    // 32 bit machines.
                    int* pint = (int *)src;
                    int len = this.Length;
                    while (len > 2)
                    {
                        hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                        hash2 = ((hash2 << 5) + hash2 + (hash2 >> 27)) ^ pint[1];
                        pint += 2;
                        len  -= 4;
                    }

                    if (len > 0)
                    {
                        hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pint[0];
                    }
#else
                    int     c;
                    char *s = src;
                    while ((c = s[0]) != 0) {
                        hash1 = ((hash1 << 5) + hash1) ^ c;
                        c = s[1];
                        if (c == 0)
                            break;
                        hash2 = ((hash2 << 5) + hash2) ^ c;
                        s += 2;
                    }
#endif
#if DEBUG
                    // We want to ensure we can change our hash function daily.
                    // This is perfectly fine as long as you don't persist the
                    // value from GetHashCode to disk or count on String A
                    // hashing before string B.  Those are bugs in your code.
                    hash1 ^= ThisAssembly.DailyBuildNumber;
#endif
                    return hash1 + (hash2 * 1566083941);
                }
            }
             */
            var index = 0;
            var hash1 = -1;
            var hash2 = hash1;
            if (HashUtil.ISWin) {
                hash1 = (5381 << 16) + 5381;
            }
            else {
                hash1 = 5381;
            }
            if (HashUtil.ISWin) {
                //int * pint = (int *)src;
                var len = somestring.length;
                while (len > 2) {
                    var pin0 = somestring.codePointAt(index);
                    var pin1 = somestring.codePointAt(index + 1);
                    hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ pin0;
                    hash2 = ((hash2 << 5) + hash2 + (hash2 >> 27)) ^ pin1; // pint[1];
                    index += 2;
                    len -= 4;
                }
                if (len > 0) {
                    hash1 = ((hash1 << 5) + hash1 + (hash1 >> 27)) ^ somestring.codePointAt(index);
                }
            }
            else {
                var c;
                // char * s = src;
                while ((c = somestring.codePointAt(index)) != 0) {
                    hash1 = ((hash1 << 5) + hash1) ^ c;
                    c = somestring.codePointAt(index + 1);
                    if (c == 0)
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ c;
                    index += 2;
                }
            }
            return hash1 + (hash2 * 1566083941);
        };
        HashUtil.ISWin = HashUtil.OS('Windows');
        HashUtil.ISMac = HashUtil.OS('MacOS');
        HashUtil.ISLinux = HashUtil.OS('Linix');
        HashUtil.ISUnix = HashUtil.OS('UNIX');
        return HashUtil;
    }());
    System.HashUtil = HashUtil;
    var String = (function () {
        function String() {
        }
        String.Format = function (s) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            for (var i = 0; i < args.length; i++) {
                var find = "{" + i + "}";
                var regex = new RegExp(this.escapeRegExp(find), "g");
                s = s.replace(regex, args[i]);
            }
            return s;
        };
        String.escapeRegExp = function (str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        };
        return String;
    }());
    System.String = String;
    var StringBuilder = (function () {
        function StringBuilder(stringValue) {
            this._list = new Array();
            if (stringValue != null && stringValue !== "") {
                this._list.push(stringValue);
            }
        }
        StringBuilder.prototype.Append = function (value, repeatCount) {
            if (repeatCount == null)
                repeatCount = 1;
            for (var i = 0; i < repeatCount; i++) {
                this._list.push(value);
            }
            return this;
        };
        StringBuilder.prototype.AppendFormat = function (value) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            for (var i = 0; i < args.length; i++) {
                var find = "{" + i + "}";
                var regex = new RegExp(this.escapeRegExp(find), "g");
                value = value.replace(regex, args[i]);
            }
            return this.Append(value);
        };
        StringBuilder.prototype.ToString = function () {
            var result = "";
            for (var i = 0; i < this._list.length; i++) {
                result += this._list[i];
            }
            return result;
        };
        StringBuilder.prototype.escapeRegExp = function (str) {
            return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
        };
        return StringBuilder;
    }());
    System.StringBuilder = StringBuilder;
})(System || (System = {}));
//# sourceMappingURL=system.js.map