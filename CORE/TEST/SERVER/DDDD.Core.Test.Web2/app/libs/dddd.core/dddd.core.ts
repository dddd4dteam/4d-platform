﻿
 
import {Http, Response, Headers} from 'angular2/http';
  

 
    /**
         *  'application/json' for Content-Type or Accept  Headers
         */
    export const jsonContent = 'application/json';
    /**
     * 'Content-Type' Http Header 
     */
    export const HDR_Content_Type = 'Content-Type';



    /**
     * Keys - enumeration of existed scenarios of Communications with DC(DynamicCommands).
     */
    export class DCCScenarios {

        /**
         * Declaration that Target Host Entity needn't-hasn't any communication. Logically incorrect state.
         * */
        public static NeedNotCommunication = 'NeedNotCommunication';


        /**
         * WebApi Communication Controller - Means that DC Manager will initialize the creation of its own DCWebApiDirectHandler to communicate and work with it.        
         *  */
        public static WebApiDirectHandlers = 'WebApiDirectHandlers';



        /**
         *   Wcf Communication Service - service can be called with REST notation,  and service is coming here with the WebServiceHostFactory class.
         *   In this case( EnvironmentActivation - concept word in enum field Name )  - service'll have its activation register inside [system.serviceModel]\[serviceHostingEnvironment]\[serviceActivations]\[add ...]  configuration elements in WebConfig.
         *   Web.Config will be changed automatically on ApplicationStart and only once - if ServiceActivation with such relativeAddress won't be found.
         *   After service configuration will be added to web.config(after first time app run), you can modify its default behavior and binding- it won't be rewritten on next time app load.
         */
        //public static Wcf_EnvironmentActivationREST = 'Wcf_EnvironmentActivationREST';


    }


    /**
    *   Serializatio Mode 
    */
    export enum DCSerializationModeEn {
        /**
         * ServiceStack's Json serialization. Here we use non commercial ServiceStack.Text 3.x version only(version 4 has only commercial license).
         */
        SSJSON
        //,

        ///**
        // * ServiceStack's XML serialization. Here we use non commercial ServiceStack.Text 3.x version only(version 4 has only commercial license).
        //*/
        //SSXML        
        //,

        ///**
        // * ServiceStack's CSV serialization. Here we use non commercial ServiceStack.Text 3.x version only(version 4 has only commercial license).
        //*/
        //SSCSV
    }

    /**
     * Class that helps to point to the Target CSharp Type.     
     */
    export class CSharType {
        static Int32 = 'System.Int32';             // 
        static UInt32 = 'System.UInt32';             // 
        static Int64 = 'System.Int64';              //  
        static UInt64 = 'System.UInt64';             // 
        static Int16 = 'System.Int16';              //  
        static UInt16 = 'System.UInt16';             // 
        static Byte = 'System.Byte';               // 
        static SByte = 'System.SByte';              // 
        static Char = 'System.Char';               // 
        static Boolean = 'System.Boolean';            // 
        static DateTime = 'System.DateTime';           // 
        static TimeSpan = 'System.TimeSpan';           //  
        static Guid = 'System.Guid';               // 
        static Float = 'System.float';              //   
        static Double = 'System.Double';             // 
        static Decimal = 'System.Decimal';            //  

        static Int32_nullable = 'System.Nullable`1[[System.Int32]]';     // 
        static UInt32_nullable = 'System.Nullable`1[[System.UInt32]]';    //  
        static Int64_nullable = 'System.Nullable`1[[System.Int64]]';     //  
        static UInt64_nullable = 'System.Nullable`1[[System.UInt64]]';    //  
        static Int16_nullable = 'System.Nullable`1[[System.Int16]]';     //  
        static UInt16_nullable = 'System.Nullable`1[[System.UInt16]]';    //  
        static Byte_nullable = 'System.Nullable`1[[System.Byte]]';      //  
        static SByte_nullable = 'System.Nullable`1[[System.SByte]]';     //  
        static Char_nullable = 'System.Nullable`1[[System.Char]]';      //  
        static Boolean_nullable = 'System.Nullable`1[[System.Boolean]]';   //  
        static DateTime_nullable = 'System.Nullable`1[[System.DateTime]]';  //  
        static TimeSpan_nullable = 'System.Nullable`1[[System.TimeSpan]]';  //  
        static Guid_nullable = 'System.Nullable`1[[System.Guid]]';      //  
        static Float_nullable = 'System.Nullable`1[[System.Float]]';     //  
        static Double_nullable = 'System.Nullable`1[[System.Double]]';    //  
        static Decimal_nullable = 'System.Nullable`1[[System.Decimal]]';   //  

        static String = 'System.string';             // 
        static byteArray = 'System.Byte[]';          // 
        static Uri = 'System.Uri';                // 
        static BitArray = 'System.Collections.BitArray';           // 


        static GetListName(itemType: string): string
        { return 'System.Collections.Generic.List`1[[' + itemType + ']]'; }

        static GetArrayName(itemType: string): string
        { return itemType + '[]'; }

    }



    /**
     * DC Parameter
     */
    export class DCParameter {

        CSharpTypeFullName: string;


        IsOut: boolean = false; // means that by default params are all IN PARAMETERS

        /**
         * Parameter key to get it from Parameters array
        */
        Key: string;

        /**
         * Some Text data value - serialized with one of the Text serializers(Json, csv, xml) parameter original value. 
         */
        ValueText: string;

        /**         
         * binary value - for upload images for example
         */
        ValueBody: Array<number>;//

        /**
         * Deserialized/Original value          
         */
        Value: any;

        /**
         * Create new IN TextParameter instance.
         * @param {string} key 
         * @param {any} paramValue
         * @param {SerializerEn} serializer
         * @param {string} cSharpTypeFullname
         * @returns  - new TextParameter
         */
        static NewInParam(key: string, paramValue: any, cSharpTypeFullname: string):DCParameter {
            //check that parameter still was not created.

            let newTextParameter = new DCParameter();
            newTextParameter.Key = key;
            newTextParameter.ValueText = JSON.stringify(paramValue);
            newTextParameter.CSharpTypeFullName = cSharpTypeFullname;

            return newTextParameter;

        }



        /**
         * Create new OUT TextParameter instance.
         * @param {string} key
         * @param {any} paramValue
         * @param {string} cSharpTypeFullName  - Format of CSharp Type Name should have this form - [[Type.FullName],[Type.Assembly.Name]]
         * @returns
         */
        static NewOutParam(key: string, paramValue: any, cSharpTypeFullName: string): DCParameter {
            //check that parameter still was not created.

            let newTextParameter = new DCParameter();
            newTextParameter.Key = key;
            newTextParameter.ValueText = JSON.stringify(paramValue);
            newTextParameter.IsOut = true;
            newTextParameter.CSharpTypeFullName = cSharpTypeFullName;

            return newTextParameter;

        }




        Deserialize<TValue>(): void {
            this.Value = <TValue>JSON.parse(this.ValueText);
        }

    }



    /**
     *  Message container that we are going to use to send any data to server, and receive any result data. 
     */
    export interface IDCMessage {

        ID: System.Guid;


        TargetDCManager: string;
        Command: string;
        Parameters: Array<DCParameter>;


        HasOperationSuccessfullyCompleted: boolean;
        HasServerErrorHappened: boolean;
        ErrorCode: number;

        ProcessingFaultMessage: string;
        ProcessingSuccessMessage: string;


        ClientSendTime: Date;
        ClientRecievedResultFromServerTime: Date;
        ServerReceivedFromClientTime: Date;
        ServerSendToClientTime: Date;

        OperationProgressPercent: number;
        OperationProgressState: string;
        UseOperationProgress: boolean;


    }


    /**
     *  Message container that we are going to use to send any data to server, and receive any result data. 
     */
    export class DCMessage2 implements IDCMessage {


        static Empty: IDCMessage = DCMessage2.Create("EmptyTarget", "EmptyCommand");

        ID: System.Guid;


        TargetDCManager: string;

        /**
         * Command or Method of  TargetDCManager to call it.
         */
        Command: string;

        /**
         * Contains all input parameters to call method on server side, and save output parameters. 
         * Parameters that has IsOutput flag true is the results of method call, on the server side.  
         */
        Parameters: Array<DCParameter>;


        ProcessingFaultMessage: string;
        ProcessingSuccessMessage: string;
        ErrorCode: number;

        get HasOperationSuccessfullyCompleted(): boolean {
            return (this.ProcessingSuccessMessage != null && this.ProcessingSuccessMessage.length > 0);
        }

        get HasServerErrorHappened(): boolean {
            return (this.ProcessingFaultMessage != null && this.ProcessingFaultMessage.length > 0);
        }



        ClientSendTime: Date;
        ClientRecievedResultFromServerTime: Date;

        ServerReceivedFromClientTime: Date;
        ServerSendToClientTime: Date;






        OperationProgressPercent: number;
        OperationProgressState: string;

        get UseOperationProgress(): boolean {
            return (this.OperationProgressPercent >= 0
                || (this.OperationProgressState != null && this.OperationProgressState.length > 0));
        }




        /**
         * Create new Command  with parameters.
         * Here TargetCommandManager is the name of Target BL Manager/Service on server side.
         * @param {string} targetDCManager  - Target BL Manager/Service Name on server side.
         * @param {string} command   - Command- method name to call in TargetCommandManager
         * @param { KeyValuePair<string,string>[] } commandParameters? - parameters for Command method BL Operation successfully completion.         
         * @returns New DCMesage2 command instance
         */
        public static Create(targetDCManager: string, command: string, ...commandParameters: Array<DCParameter>): DCMessage2 {
            let commandMsg: DCMessage2 = new DCMessage2();
            commandMsg.ID = System.Guid.MakeNew();
            commandMsg.TargetDCManager = targetDCManager;
            commandMsg.Command = command;
            commandMsg.Parameters = commandParameters;

            return commandMsg;
        }


        AddInParameter(key: string, paramValue: any, cSharpTypeFullName: string): void {
            this.Parameters.push(DCParameter.NewInParam(key, paramValue, cSharpTypeFullName));
        }

        AddOutParameter(key: string, paramValue: any, cSharpTypeFullName: string): void {
            this.Parameters.push(DCParameter.NewOutParam(key, paramValue, cSharpTypeFullName));
        }


    }



    /**
     *   Http Client that based on DCMessage2 message container. For DCScenario  [WebApiDirectHandlers].
     */
    export class WebApiDirectHandlerClient {
        httpService: Http;
        constructor(protected thehttpService: Http) { this.httpService = thehttpService; }



        /**
         * Shortkey - name that also associated as [1..1] with [Target BL Service/Manager.Shortkey] on Server-Side
         * */
        ShortKey: string = this.GetShortKey();

        BaseUrl: string = this.GetBaseUrl();

        DCSenario: string = DCCScenarios.WebApiDirectHandlers;

        UrlByScenario: string = this.GetUrlByScenario(this.DCSenario);

        private GetShortKey(): string {
            //manager name can be looks like this - [ManagerName](Manager) or [ServiceName](Service).                
            let shortKey: string = (<any>this).constructor.name;

            shortKey = shortKey.replace('Service', '');
            shortKey = shortKey.replace('service', '');
            shortKey = shortKey.replace('Manager', '');
            shortKey = shortKey.replace('manager', '');



            return shortKey;
        }

        private GetBaseUrl(): string {
            let baseURL = document.URL;
            baseURL = baseURL.replace('Index', '');
            baseURL = baseURL.replace('index', '');

            baseURL = baseURL.replace('Default', '');
            baseURL = baseURL.replace('default', '');

            baseURL = baseURL.replace('.html', '');
            baseURL = baseURL.replace('.htm', '');
            baseURL = baseURL.replace('.aspx', '');
            return baseURL;
        }

        private GetUrlByScenario(scenario?: string): string {

            if (scenario == DCCScenarios.WebApiDirectHandlers) {
                return (this.BaseUrl + 'webapi/' + this.ShortKey);
            }

            return this.BaseUrl;
        }





         /*
   protected internal virtual async Task<DCMessage> ExecuteCommandOnClient(
                                                                string scenarioKey,
                                                                string currentCommandName
                                                              , DCProgressReporter reporter
                                                              , params KeyValuePair<string, object>[] actionParameters)
      {

          if (!CommunicationUnits.ContainsKey(scenarioKey))
              throw new InvalidOperationException("Current Manager  doesn't contain Client with pointed ScenarioKey");


          var targetCommunicationUnit = CommunicationUnits[scenarioKey];

          var commandMessage = DCMessage.Create(OwnerName, ID, currentCommandName, actionParameters);
          // indication - Starting if newtwork enable start
          NotifyStartCommandProcessing(commandMessage, reporter);

          SetDiagnosticsOnClientSendTimeDbg(commandMessage);

          //PACK INTERNAL SERIALIZATION
          targetCommunicationUnit.PackMessageHandler(commandMessage);
          //PackMessageWithTSS(ref commandMessage);
          var targetDCClient = DCClients[targetCommunicationUnit.RegistrationInfo.ScenarioKey];
          return await targetDCClient.ExecuteCommand(commandMessage, reporter, NotifyNetworkDoesntWorkAndStopCommandProcessing)
                .ContinueWith(
                          (taskResultMessage) =>
                          {
                              if (taskResultMessage.Status == TaskStatus.RanToCompletion)
                              {
                                  // analising DCMessage received from server Result Message
                                  var serverMessage = taskResultMessage.Result;

                                  //UNPACK INTERNAL SERIALIZATION
                                  targetCommunicationUnit.UnpackMessageHandler(ref serverMessage);
                                  //UnpackMessageWithTSS(ref serverMessage);

                                  commandMessage = serverMessage;

                                  SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(commandMessage);

                                  //if HasServerErrorHappened - notify
                                  NotifyAboutCommandFailureResultProgress(commandMessage, reporter); //

                                  // successfully completed on server - notify
                                  NotifyAboutCommandSuccessResultProgress(commandMessage, reporter);


                                  //continue TriggerFaultAferAction - Will be Called if it exist
                                  //if (insideTSout.Item3 != null)
                                  //{ insideTSout.Item3(t); }

                              }
                              else if (taskResultMessage.Status == TaskStatus.Faulted)
                              {
                                  commandMessage.ProcessingFaultMessage = "Communication not sended to server";
                                  return commandMessage;
                              }
                              return commandMessage;
                          }
               );

      }
        */



        /**
         * Executing DC command to send/receive some data from server side by Http POST request.
         * @param commandName  - Command that we beleave is command(method) that your BL Service/Manager contains on Server-Side.
         * @param serverResponseAction - client Action with DCMessage2 on serverResponse.
         * @param actionParameters - parameters to send and call Command in BL Service/Manager on Server-Side.
         */
        ExecuteCommand(commandName: string
            , serverResponseAction?: ((value: DCMessage2) => void)
            , ...actionParameters: Array<DCParameter>

        ) {

            //, DCProgressReporter reporter     

            try {

                if (this.httpService == null) { console.error('ERROR in ClientReportService.ExecuteCommand():  httpService  was not  injected'); return; }

                let url = this.UrlByScenario;

                let clientMessage = DCMessage2.Create(this.ShortKey, commandName, ...actionParameters);

                //serialize builded clientMessage - to send by HttpPost on next step
                var body = JSON.stringify(clientMessage);
                var headers = new Headers();
                headers.append(HDR_Content_Type, jsonContent);
                this.httpService
                    .post(url, body, { headers: headers })
                    .subscribe(respons => {
                        try {
                            let serverMessage = <DCMessage2>respons.json();

                            if (serverMessage == null) { console.error('ERROR in ClientReportService.ExecuteCommand(): server message was not cast as [DDDD.Core.Messaging.DCMessage2] '); return; }

                            //set received dateTime
                            serverMessage.ClientRecievedResultFromServerTime = new Date(Date.now());

                            if (serverResponseAction != null) serverResponseAction(serverMessage);

                        }
                        catch (Error) {
                            console.error('ERROR in ClientReportService.ExecuteCommand :  can\'t read response as DCMessage2' + Error.message);
                        }

                    });

            }
            catch (Error) {
                console.error('ERROR in ClientReportService.ExecuteCommand : ' + Error.message);
                alert('ERROR in ClientReportService.ExecuteCommand : ' + Error.message);
            }

        }





    }



        