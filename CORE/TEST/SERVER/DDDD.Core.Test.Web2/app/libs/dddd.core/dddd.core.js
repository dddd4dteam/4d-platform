System.register(['angular2/http'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var http_1;
    var jsonContent, HDR_Content_Type, DCCScenarios, DCSerializationModeEn, CSharType, DCParameter, DCMessage2, WebApiDirectHandlerClient;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            }],
        execute: function() {
            /**
                 *  'application/json' for Content-Type or Accept  Headers
                 */
            exports_1("jsonContent", jsonContent = 'application/json');
            /**
             * 'Content-Type' Http Header
             */
            exports_1("HDR_Content_Type", HDR_Content_Type = 'Content-Type');
            /**
             * Keys - enumeration of existed scenarios of Communications with DC(DynamicCommands).
             */
            DCCScenarios = (function () {
                function DCCScenarios() {
                }
                /**
                 * Declaration that Target Host Entity needn't-hasn't any communication. Logically incorrect state.
                 * */
                DCCScenarios.NeedNotCommunication = 'NeedNotCommunication';
                /**
                 * WebApi Communication Controller - Means that DC Manager will initialize the creation of its own DCWebApiDirectHandler to communicate and work with it.
                 *  */
                DCCScenarios.WebApiDirectHandlers = 'WebApiDirectHandlers';
                return DCCScenarios;
            }());
            exports_1("DCCScenarios", DCCScenarios);
            /**
            *   Serializatio Mode
            */
            (function (DCSerializationModeEn) {
                /**
                 * ServiceStack's Json serialization. Here we use non commercial ServiceStack.Text 3.x version only(version 4 has only commercial license).
                 */
                DCSerializationModeEn[DCSerializationModeEn["SSJSON"] = 0] = "SSJSON";
            })(DCSerializationModeEn || (DCSerializationModeEn = {}));
            exports_1("DCSerializationModeEn", DCSerializationModeEn);
            /**
             * Class that helps to point to the Target CSharp Type.
             */
            CSharType = (function () {
                function CSharType() {
                }
                CSharType.GetListName = function (itemType) { return 'System.Collections.Generic.List`1[[' + itemType + ']]'; };
                CSharType.GetArrayName = function (itemType) { return itemType + '[]'; };
                CSharType.Int32 = 'System.Int32'; // 
                CSharType.UInt32 = 'System.UInt32'; // 
                CSharType.Int64 = 'System.Int64'; //  
                CSharType.UInt64 = 'System.UInt64'; // 
                CSharType.Int16 = 'System.Int16'; //  
                CSharType.UInt16 = 'System.UInt16'; // 
                CSharType.Byte = 'System.Byte'; // 
                CSharType.SByte = 'System.SByte'; // 
                CSharType.Char = 'System.Char'; // 
                CSharType.Boolean = 'System.Boolean'; // 
                CSharType.DateTime = 'System.DateTime'; // 
                CSharType.TimeSpan = 'System.TimeSpan'; //  
                CSharType.Guid = 'System.Guid'; // 
                CSharType.Float = 'System.float'; //   
                CSharType.Double = 'System.Double'; // 
                CSharType.Decimal = 'System.Decimal'; //  
                CSharType.Int32_nullable = 'System.Nullable`1[[System.Int32]]'; // 
                CSharType.UInt32_nullable = 'System.Nullable`1[[System.UInt32]]'; //  
                CSharType.Int64_nullable = 'System.Nullable`1[[System.Int64]]'; //  
                CSharType.UInt64_nullable = 'System.Nullable`1[[System.UInt64]]'; //  
                CSharType.Int16_nullable = 'System.Nullable`1[[System.Int16]]'; //  
                CSharType.UInt16_nullable = 'System.Nullable`1[[System.UInt16]]'; //  
                CSharType.Byte_nullable = 'System.Nullable`1[[System.Byte]]'; //  
                CSharType.SByte_nullable = 'System.Nullable`1[[System.SByte]]'; //  
                CSharType.Char_nullable = 'System.Nullable`1[[System.Char]]'; //  
                CSharType.Boolean_nullable = 'System.Nullable`1[[System.Boolean]]'; //  
                CSharType.DateTime_nullable = 'System.Nullable`1[[System.DateTime]]'; //  
                CSharType.TimeSpan_nullable = 'System.Nullable`1[[System.TimeSpan]]'; //  
                CSharType.Guid_nullable = 'System.Nullable`1[[System.Guid]]'; //  
                CSharType.Float_nullable = 'System.Nullable`1[[System.Float]]'; //  
                CSharType.Double_nullable = 'System.Nullable`1[[System.Double]]'; //  
                CSharType.Decimal_nullable = 'System.Nullable`1[[System.Decimal]]'; //  
                CSharType.String = 'System.string'; // 
                CSharType.byteArray = 'System.Byte[]'; // 
                CSharType.Uri = 'System.Uri'; // 
                CSharType.BitArray = 'System.Collections.BitArray'; // 
                return CSharType;
            }());
            exports_1("CSharType", CSharType);
            /**
             * DC Parameter
             */
            DCParameter = (function () {
                function DCParameter() {
                    this.IsOut = false; // means that by default params are all IN PARAMETERS
                }
                /**
                 * Create new IN TextParameter instance.
                 * @param {string} key
                 * @param {any} paramValue
                 * @param {SerializerEn} serializer
                 * @param {string} cSharpTypeFullname
                 * @returns  - new TextParameter
                 */
                DCParameter.NewInParam = function (key, paramValue, cSharpTypeFullname) {
                    //check that parameter still was not created.
                    var newTextParameter = new DCParameter();
                    newTextParameter.Key = key;
                    newTextParameter.ValueText = JSON.stringify(paramValue);
                    newTextParameter.CSharpTypeFullName = cSharpTypeFullname;
                    return newTextParameter;
                };
                /**
                 * Create new OUT TextParameter instance.
                 * @param {string} key
                 * @param {any} paramValue
                 * @param {string} cSharpTypeFullName  - Format of CSharp Type Name should have this form - [[Type.FullName],[Type.Assembly.Name]]
                 * @returns
                 */
                DCParameter.NewOutParam = function (key, paramValue, cSharpTypeFullName) {
                    //check that parameter still was not created.
                    var newTextParameter = new DCParameter();
                    newTextParameter.Key = key;
                    newTextParameter.ValueText = JSON.stringify(paramValue);
                    newTextParameter.IsOut = true;
                    newTextParameter.CSharpTypeFullName = cSharpTypeFullName;
                    return newTextParameter;
                };
                DCParameter.prototype.Deserialize = function () {
                    this.Value = JSON.parse(this.ValueText);
                };
                return DCParameter;
            }());
            exports_1("DCParameter", DCParameter);
            /**
             *  Message container that we are going to use to send any data to server, and receive any result data.
             */
            DCMessage2 = (function () {
                function DCMessage2() {
                }
                Object.defineProperty(DCMessage2.prototype, "HasOperationSuccessfullyCompleted", {
                    get: function () {
                        return (this.ProcessingSuccessMessage != null && this.ProcessingSuccessMessage.length > 0);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DCMessage2.prototype, "HasServerErrorHappened", {
                    get: function () {
                        return (this.ProcessingFaultMessage != null && this.ProcessingFaultMessage.length > 0);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(DCMessage2.prototype, "UseOperationProgress", {
                    get: function () {
                        return (this.OperationProgressPercent >= 0
                            || (this.OperationProgressState != null && this.OperationProgressState.length > 0));
                    },
                    enumerable: true,
                    configurable: true
                });
                /**
                 * Create new Command  with parameters.
                 * Here TargetCommandManager is the name of Target BL Manager/Service on server side.
                 * @param {string} targetDCManager  - Target BL Manager/Service Name on server side.
                 * @param {string} command   - Command- method name to call in TargetCommandManager
                 * @param { KeyValuePair<string,string>[] } commandParameters? - parameters for Command method BL Operation successfully completion.
                 * @returns New DCMesage2 command instance
                 */
                DCMessage2.Create = function (targetDCManager, command) {
                    var commandParameters = [];
                    for (var _i = 2; _i < arguments.length; _i++) {
                        commandParameters[_i - 2] = arguments[_i];
                    }
                    var commandMsg = new DCMessage2();
                    commandMsg.ID = System.Guid.MakeNew();
                    commandMsg.TargetDCManager = targetDCManager;
                    commandMsg.Command = command;
                    commandMsg.Parameters = commandParameters;
                    return commandMsg;
                };
                DCMessage2.prototype.AddInParameter = function (key, paramValue, cSharpTypeFullName) {
                    this.Parameters.push(DCParameter.NewInParam(key, paramValue, cSharpTypeFullName));
                };
                DCMessage2.prototype.AddOutParameter = function (key, paramValue, cSharpTypeFullName) {
                    this.Parameters.push(DCParameter.NewOutParam(key, paramValue, cSharpTypeFullName));
                };
                DCMessage2.Empty = DCMessage2.Create("EmptyTarget", "EmptyCommand");
                return DCMessage2;
            }());
            exports_1("DCMessage2", DCMessage2);
            /**
             *   Http Client that based on DCMessage2 message container. For DCScenario  [WebApiDirectHandlers].
             */
            WebApiDirectHandlerClient = (function () {
                function WebApiDirectHandlerClient(thehttpService) {
                    this.thehttpService = thehttpService;
                    /**
                     * Shortkey - name that also associated as [1..1] with [Target BL Service/Manager.Shortkey] on Server-Side
                     * */
                    this.ShortKey = this.GetShortKey();
                    this.BaseUrl = this.GetBaseUrl();
                    this.DCSenario = DCCScenarios.WebApiDirectHandlers;
                    this.UrlByScenario = this.GetUrlByScenario(this.DCSenario);
                    this.httpService = thehttpService;
                }
                WebApiDirectHandlerClient.prototype.GetShortKey = function () {
                    //manager name can be looks like this - [ManagerName](Manager) or [ServiceName](Service).                
                    var shortKey = this.constructor.name;
                    shortKey = shortKey.replace('Service', '');
                    shortKey = shortKey.replace('service', '');
                    shortKey = shortKey.replace('Manager', '');
                    shortKey = shortKey.replace('manager', '');
                    return shortKey;
                };
                WebApiDirectHandlerClient.prototype.GetBaseUrl = function () {
                    var baseURL = document.URL;
                    baseURL = baseURL.replace('Index', '');
                    baseURL = baseURL.replace('index', '');
                    baseURL = baseURL.replace('Default', '');
                    baseURL = baseURL.replace('default', '');
                    baseURL = baseURL.replace('.html', '');
                    baseURL = baseURL.replace('.htm', '');
                    baseURL = baseURL.replace('.aspx', '');
                    return baseURL;
                };
                WebApiDirectHandlerClient.prototype.GetUrlByScenario = function (scenario) {
                    if (scenario == DCCScenarios.WebApiDirectHandlers) {
                        return (this.BaseUrl + 'webapi/' + this.ShortKey);
                    }
                    return this.BaseUrl;
                };
                /*
          protected internal virtual async Task<DCMessage> ExecuteCommandOnClient(
                                                                       string scenarioKey,
                                                                       string currentCommandName
                                                                     , DCProgressReporter reporter
                                                                     , params KeyValuePair<string, object>[] actionParameters)
             {
       
                 if (!CommunicationUnits.ContainsKey(scenarioKey))
                     throw new InvalidOperationException("Current Manager  doesn't contain Client with pointed ScenarioKey");
       
       
                 var targetCommunicationUnit = CommunicationUnits[scenarioKey];
       
                 var commandMessage = DCMessage.Create(OwnerName, ID, currentCommandName, actionParameters);
                 // indication - Starting if newtwork enable start
                 NotifyStartCommandProcessing(commandMessage, reporter);
       
                 SetDiagnosticsOnClientSendTimeDbg(commandMessage);
       
                 //PACK INTERNAL SERIALIZATION
                 targetCommunicationUnit.PackMessageHandler(commandMessage);
                 //PackMessageWithTSS(ref commandMessage);
                 var targetDCClient = DCClients[targetCommunicationUnit.RegistrationInfo.ScenarioKey];
                 return await targetDCClient.ExecuteCommand(commandMessage, reporter, NotifyNetworkDoesntWorkAndStopCommandProcessing)
                       .ContinueWith(
                                 (taskResultMessage) =>
                                 {
                                     if (taskResultMessage.Status == TaskStatus.RanToCompletion)
                                     {
                                         // analising DCMessage received from server Result Message
                                         var serverMessage = taskResultMessage.Result;
       
                                         //UNPACK INTERNAL SERIALIZATION
                                         targetCommunicationUnit.UnpackMessageHandler(ref serverMessage);
                                         //UnpackMessageWithTSS(ref serverMessage);
       
                                         commandMessage = serverMessage;
       
                                         SetDiagnosticsOnClientRecievedResultFromServerTimeDbg(commandMessage);
       
                                         //if HasServerErrorHappened - notify
                                         NotifyAboutCommandFailureResultProgress(commandMessage, reporter); //
       
                                         // successfully completed on server - notify
                                         NotifyAboutCommandSuccessResultProgress(commandMessage, reporter);
       
       
                                         //continue TriggerFaultAferAction - Will be Called if it exist
                                         //if (insideTSout.Item3 != null)
                                         //{ insideTSout.Item3(t); }
       
                                     }
                                     else if (taskResultMessage.Status == TaskStatus.Faulted)
                                     {
                                         commandMessage.ProcessingFaultMessage = "Communication not sended to server";
                                         return commandMessage;
                                     }
                                     return commandMessage;
                                 }
                      );
       
             }
               */
                /**
                 * Executing DC command to send/receive some data from server side by Http POST request.
                 * @param commandName  - Command that we beleave is command(method) that your BL Service/Manager contains on Server-Side.
                 * @param serverResponseAction - client Action with DCMessage2 on serverResponse.
                 * @param actionParameters - parameters to send and call Command in BL Service/Manager on Server-Side.
                 */
                WebApiDirectHandlerClient.prototype.ExecuteCommand = function (commandName, serverResponseAction) {
                    //, DCProgressReporter reporter     
                    var actionParameters = [];
                    for (var _i = 2; _i < arguments.length; _i++) {
                        actionParameters[_i - 2] = arguments[_i];
                    }
                    try {
                        if (this.httpService == null) {
                            console.error('ERROR in ClientReportService.ExecuteCommand():  httpService  was not  injected');
                            return;
                        }
                        var url = this.UrlByScenario;
                        var clientMessage = DCMessage2.Create.apply(DCMessage2, [this.ShortKey, commandName].concat(actionParameters));
                        //serialize builded clientMessage - to send by HttpPost on next step
                        var body = JSON.stringify(clientMessage);
                        var headers = new http_1.Headers();
                        headers.append(HDR_Content_Type, jsonContent);
                        this.httpService
                            .post(url, body, { headers: headers })
                            .subscribe(function (respons) {
                            try {
                                var serverMessage = respons.json();
                                if (serverMessage == null) {
                                    console.error('ERROR in ClientReportService.ExecuteCommand(): server message was not cast as [DDDD.Core.Messaging.DCMessage2] ');
                                    return;
                                }
                                //set received dateTime
                                serverMessage.ClientRecievedResultFromServerTime = new Date(Date.now());
                                if (serverResponseAction != null)
                                    serverResponseAction(serverMessage);
                            }
                            catch (Error) {
                                console.error('ERROR in ClientReportService.ExecuteCommand :  can\'t read response as DCMessage2' + Error.message);
                            }
                        });
                    }
                    catch (Error) {
                        console.error('ERROR in ClientReportService.ExecuteCommand : ' + Error.message);
                        alert('ERROR in ClientReportService.ExecuteCommand : ' + Error.message);
                    }
                };
                return WebApiDirectHandlerClient;
            }());
            exports_1("WebApiDirectHandlerClient", WebApiDirectHandlerClient);
        }
    }
});
//# sourceMappingURL=dddd.core.js.map