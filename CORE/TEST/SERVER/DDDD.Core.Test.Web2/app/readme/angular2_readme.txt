﻿DEPLOYMENT ASPECT.
To deploy only used in index.html  scripts, we need to create-precoping mech for each of items in node_modules into scripts app folder.
We'll use project file editing - we add <ItemGroup><NodeItem> ././ for each of needable scripts/styles.

We use the following files:

    <link href="node_modules/bootstrap/dist/css/bootstrap.css" rel="stylesheet" />
   
    <!-- 1. Load libraries -->
    <!-- IE required polyfills, in this exact order -->
    <script src="node_modules/angular2/es6/dev/src/testing/shims_for_IE.js"></script>
    <script src="node_modules/es6-shim/es6-shim.min.js"></script>
    <script src="node_modules/systemjs/dist/system-polyfills.js"></script>
    <script src="node_modules/angular2/bundles/angular2-polyfills.js"></script>
    <script src="node_modules/systemjs/dist/system.src.js"></script>
    <script src="node_modules/rxjs/bundles/Rx.js"></script>
    <script src="node_modules/angular2/bundles/angular2.dev.js"></script>

<ItemGroup>
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/bootstrap/dist/css/bootstrap.css" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/es6/dev/src/testing/shims_for_IE.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/es6-shim/es6-shim.min.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/systemjs/dist/system-polyfills.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/bundles/angular2-polyfills.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/systemjs/dist/system.src.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/rxjs/bundles/Rx.js" />
    <NodeLib Include="$(MSBuildProjectDirectory)\node_modules/angular2/bundles/angular2.dev.js" />
</ItemGroup>
<Target Name="CopyFiles" BeforeTargets="Build">
  <Copy SourceFiles="@(NodeLib)" DestinationFolder="$(MSBuildProjectDirectory)\scripts"/>
</Target>





<bl>
    <img title="Web Essentials" class="logo" draggable="true" alt="Web Essentials" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAMAAAC7IEhfAAACplBMVEX/////AAB/AAD/fwD/VQD/Pz//VSriVBzdRCLuVSLfTx/hSy3lTCbdTSHfSirgUSjjSCTlTCLkSSjkUCjmTiTiTyfoTyfhTifiTCbmTCbjTSThSyXkTCbkSyXjSyXjTiXjTCTjSyTkTSbiTCTlTCTjSyXkTSXkTCTiSyfkTCblTSbjTCbkTCbkTCXlTSbjTCbiTSbkTCblTSXkTSXkTSXkTCXjTSXjTSbkTSXjTSXjTSbkTSXkTSbjTCbjTSXkTCbkTCbiTSbkTSbjTCbjTSXjTCXjTSbjTCXkTCXjTCXkTCXjTCXjTSXjTCbjTSbkTSbjTCbkTSbkTCbjTCbjTSbjTCXkTCbkTSbjTSbjTCXkTSbjTCXjTSXkTSXjTCbkTSb////xZSnkTibrWSjoVCftXijpVifqVyfqWCfqd1rlTibrWijtXSjob1DvYinwZivxZCnlTybyfUr0jmL6z7376OP76uX97+r99PL99/T9+ffmUSbpVyfmUCbkUCnqWSfmUSfqfGDmXzzmYT/rWyjrfmLsWyjsXCjsbkbsiG7mZUTnUiftflvtinDtkHjuXyjuYCjulH3vYSjnUyfwYinwZCnnYT/waC3nZEPnZkTxazHxcDjxcDnxcTryeETye0jyfErnZkXyfk3ysJ/zglHzi1/zjF/zjWLztaXztqb0i17kUCr0kmn0u6z1mXH1rZH2poP2yLz2zsP3rY73r5D30cf4w674xK741s75zLj50L753NXoVSf65uD73M/74db75Nr75uD75+LobU3obk776ub77Oj77ur859z86OD88Ov88e/88/D89fT97+njTyn98ezjUSvlUCb99/X9+Pb9+Pfpdln+9fL+9/X++ff++vj/+/n//Pv//fz//f3pd1rpeFv//v6yzbn+AAAAX3RSTlMAAQICAwQGCQ8PEBEUFxgZHB4mJiotLTQ1NTg9Q0RKS1NUVlpaXmBhYmpwcXJ1d3h9f4CHjo+Xl5uenqSlpqittLW5u7zExsvL0tLY2dvh4eLk6Onq7O/w8fL3+fv7/UUm+e4AAAJFSURBVHhehdJlYxQ7GIbhLO5erEhxitNiRU6RLl5cCpQ+mZW6u+Du7u7u7u7ubuefMJkkm2F2u70/JXmvT0kIIVEoqShiFFkijOTQDkuODHOJgJ3DcCt0auZWA+Ec9vQPk4EeHIb4hwuAEA6D/cPZQDCHQf7hDCCIw0Bf8PCXy1lpBnQBgRzW9gEPFlJK75zZocPlQC0OK3nDQ7pjFehwJlCBQ9s0Kzz2gSrowFQb4Y2G3tu5kK34ShXcCIwioqEAVj2m52Zx9+k9NcFUYIiEAwBc149z7jOXwF2hgGuA/hL2FpDmrwR+f2arb6euCegGwiQMNeDZu5T+3J/wnIEnB7RsARcBoRK2N+CReTf10f9snr1T80An0E7Clhxi/j1KmT29WVMwDmghYWMBsdSAJ52aCd4AGklYV8CUFwb8dWm7Ca4HAiSsyuED5j6y+a3dChYBVSQsG8PgxXxKX67dksNAXpYHJiK6DJGNF/f48A+wNZetfpwX95gOjCOehnN4ZRP0lj2iIgY3AMMUjAAW5tKjx2G0l0kJ1wCDFOwL4HUKZEteKegG+ijY3fIf9zzzwMVANwU7W6CWmcfh021JQCcF21qhlvmG0u9XT6Rp8UAbBZt6QW3f7Qu7NL1YoImC9a1Q5QLqKVizeDgHqKFg+WJhagZiyhHVFF8w3R1XBGASMTXSC65LcjlgNMIMq7f+b6KC8cmx78Cb0K9VNfJvpRt2HTwd5qLtXRqUIj6r3DxsrFBjejWrSPxlC+gYMXlghzrE2l/nse3AksfajAAAAABJRU5ErkJggg==">
    <blbar>
        <blbutton title="Use CTRL+ALT+D to enable Design Mode">Design</blbutton>
        <blbutton title="Use CTRL+ALT+I to enable Inspect Mode">Inspect</blbutton>
        <blbutton title="Use CTRL+ALT+T to sync the current CSS changes into Visual Studio" disabled="" class="bldisabled">Save F12 changes</blbutton>
        <blcheckbox>
            <label title="Use CTRL+ALT+U to continuously sync CSS changes into Visual Studio" style="font-weight: normal;" for="_0_8675615510152734">
                F12 auto-sync
                <input title="Use CTRL+ALT+U to continuously sync CSS changes into Visual Studio" id="_0_8675615510152734" type="checkbox">
            </label>
        </blcheckbox>
        <blcheckbox>
            <label title="This will auto-hide this menu. Click the CTRL key to make it visible" style="font-weight: normal;" for="_0_18597562873861245">
                Auto-hide<input title="This will auto-hide this menu. Click the CTRL key to make it visible" id="_0_18597562873861245" type="checkbox">
            </label>
        </blcheckbox>
    </blbar>
</bl>