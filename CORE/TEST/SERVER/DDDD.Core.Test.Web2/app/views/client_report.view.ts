// import Namespaced libs 
 
 

//import Moduled libs
import { Http } from 'angular2/http';
import {Component , Injectable } from 'angular2/core'; 
import { WebApiDirectHandlerClient, DCParameter, DCCScenarios, CSharType, DCSerializationModeEn, DCMessage2} from '../libs/dddd.core/dddd.core';




    // INFO LINKS:
    // http://blog.nethouse.se/2015/07/24/angular-typescript-typed-interaction-webapi-backend/
    // http://www.w3schools.com/jS/js_htmldom_document.asp
    // https://habrahabr.ru/company/nixsolutions/blog/301002/
    // http://blog.wolksoftware.com/decorators-reflection-javascript-typescript
    // http://www.timmykokke.com/2014/01/convert-json-to-typescript/ - json to ye script
    // http://stackoverflow.com/questions/22885995/how-do-i-initialize-a-typescript-object-with-a-json-object
    // http://stackoverflow.com/questions/22875636/how-do-i-cast-a-json-object-to-a-typescript-class?noredirect=1&lq=1




export class Product4  
{

    /**
     * CSharpTypeFullName of this contract
    */
    static TypeFullName: string = 'DDDD.TSS.Tests.Model.CommunicationModel.Product4, DDDD.Core.Test.Model.Net45';
    
    

    productId: number;
    productName: string;
    productCode: string;
    releaseDate: string;
    price: number;
    description: string;
    starRating: number;
    imageUrl: string;
}



/**
 *  Http handling service, that  Executes DC commands and calls any of the BL Method on the server side, and return result to the client.
 */
@Injectable()
export class ClientReportService extends WebApiDirectHandlerClient {
    constructor(protected httpServiceInherited: Http) { super(httpServiceInherited); }


}








@Component({
    selector: 'client-report',
    templateUrl: 'app/views/client_report.view.html'

    //, styleUrls: ['app/clientreport/product-list.component.css']
    //, pipes: [ProductFilterPipe],
    //, directives: [StarComponent, ROUTER_DIRECTIVES]      
    
})
export class ClientReportComponent
{


    constructor(private clientReportService: ClientReportService) { } 
    

    static OneProduct: Product4 = ClientReportComponent.GetProduct(); 
    static ProductCollection: Array<Product4> = ClientReportComponent.GetProducts();

    static GetProduct(): Product4
    {
        let product: Product4 = 
            {
                "productId": 1,
                "productName": "Leaf Rake",
                "productCode": "GDN-0011",
                "releaseDate": "March 19, 2016",
                "description": "Leaf rake with 48-inch wooden handle.",
                "price": 19.95,
                "starRating": 3.2,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
            };
        
        return product;
    }

    static GetProducts():Product4[]        
    {        
        return [
            {
                "productId": 1,                
                "productName": "Leaf Rake",
                "productCode": "GDN-0011",
                "releaseDate": "March 19, 2016",
                "description": "Leaf rake with 48-inch wooden handle.",
                "price": 19.95,
                "starRating": 3.2,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
            },
            {
                "productId": 2,
                "productName": "Garden Cart",
                "productCode": "GDN-0023",
                "releaseDate": "March 18, 2016",
                "description": "15 gallon capacity rolling garden cart",
                "price": 32.99,
                "starRating": 4.2,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
            },
            {
                "productId": 5,
                "productName": "Hammer",
                "productCode": "TBX-0048",
                "releaseDate": "May 21, 2016",
                "description": "Curved claw steel hammer",
                "price": 8.9,
                "starRating": 4.8,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
            },
            {
                "productId": 8,
                "productName": "Saw",
                "productCode": "TBX-0022",
                "releaseDate": "May 15, 2016",
                "description": "15-inch steel blade hand saw",
                "price": 11.55,
                "starRating": 3.7,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png"
            },
            {
                "productId": 10,
                "productName": "Video Game Controller",
                "productCode": "GMG-0042",
                "releaseDate": "October 15, 2015",
                "description": "Standard two-button video game controller",
                "price": 35.95,
                "starRating": 4.6,
                "imageUrl": "http://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png"
            }
        ];        
    }


    
    static defaultPageTitle: string = 'Acme Product Management';
    pageTitle: string = ClientReportComponent.defaultPageTitle;





    // SaveProduct
    // SaveProducts
    // GetProducts

    /**
     * Test sending one product item to save it on the server side
     */
    public Test_SaveProduct(): void
    {
        let commandName = 'SaveProduct';
        let newTextParameter = DCParameter.NewInParam(
                                   'product', ClientReportComponent.OneProduct, Product4.TypeFullName);
                    
        this.clientReportService.ExecuteCommand(commandName
            , (responseMessage:  DCMessage2) => {
                //client callback TO DO
            }
            , newTextParameter);
    }


    /**
     *  Test sending products collection to save them on the server side
     */
    public Test_SaveProducts(): void
    {      
        let commandName = 'SaveProducts';
        let newTextParameter =  DCParameter.NewInParam(
                'products', ClientReportComponent.ProductCollection,
                CSharType.GetListName( Product4.TypeFullName) );
              
        this.clientReportService.ExecuteCommand(commandName
            , (responseMessage: DCMessage2) => {
                //client callback TO DO
            }
            , newTextParameter);
    }


    /**
     *  Test Getting Products collection from the server side
     */
    public Test_LoadProducts(): void
    {
        let commandName = 'LoadProducts';       
        let receivedDCMessage = this.clientReportService.ExecuteCommand(commandName,
            (responseMessage:  DCMessage2) =>
            {

                console.info('LoadProducts():  received parameters' + responseMessage.Parameters.length);

                let productsParam = responseMessage.Parameters.find(prm => prm.Key === 'products');
                if (productsParam == null) { console.error('LoadProducts(): parameter products was not found'); return; }

                //let products =   <Array<Product4>>JSON.parse(productsParam.ValueText);
                let products2 = productsParam.Deserialize<Array<Product4>>();
                if (products2 == null) { console.error('LoadProducts():  products was not loaded from server'); return; }
                               
            }

        );



    }




}