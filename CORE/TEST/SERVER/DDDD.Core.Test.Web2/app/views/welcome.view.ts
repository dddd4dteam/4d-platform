import { Component } from 'angular2/core';

@Component({
    templateUrl: 'app/home/welcome.view.html'
})
export class WelcomeComponent {
    public pageTitle: string = "Welcome";
}