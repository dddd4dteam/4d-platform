// import Namespaced libs 
/// <reference path="../libs/System/system.ts" />
System.register(['angular2/core', './client_report.service'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, client_report_service_1;
    var ClientReportComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (client_report_service_1_1) {
                client_report_service_1 = client_report_service_1_1;
            }],
        execute: function() {
            ClientReportComponent = (function () {
                function ClientReportComponent(clientReportService) {
                    this.clientReportService = clientReportService;
                    this.pageTitle = ClientReportComponent.defaultPageTitle;
                }
                ClientReportComponent.prototype.Test_GetHashCode = function () {
                    this.pageTitle += '..HashCode = ' + System.HashUtil.GetHashCode(ClientReportComponent.defaultPageTitle).toString();
                };
                ClientReportComponent.prototype.Test_LoadData1 = function () {
                    this.clientReportService.Test_LoadData1();
                };
                ClientReportComponent.defaultPageTitle = 'Acme Product Management';
                ClientReportComponent = __decorate([
                    core_1.Component({
                        selector: 'client-report',
                        templateUrl: 'app/clientreport/client_report.component.html'
                    }), 
                    __metadata('design:paramtypes', [client_report_service_1.ClientReportService])
                ], ClientReportComponent);
                return ClientReportComponent;
            }());
            exports_1("ClientReportComponent", ClientReportComponent);
        }
    }
});
//# sourceMappingURL=client_report.component.js.map