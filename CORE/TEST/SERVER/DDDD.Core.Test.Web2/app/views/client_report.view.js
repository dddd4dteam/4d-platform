// import Namespaced libs 
System.register(['angular2/http', 'angular2/core', '../libs/dddd.core/dddd.core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __extends = (this && this.__extends) || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var http_1, core_1, dddd_core_1;
    var Product4, ClientReportService, ClientReportComponent;
    return {
        setters:[
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (dddd_core_1_1) {
                dddd_core_1 = dddd_core_1_1;
            }],
        execute: function() {
            // INFO LINKS:
            // http://blog.nethouse.se/2015/07/24/angular-typescript-typed-interaction-webapi-backend/
            // http://www.w3schools.com/jS/js_htmldom_document.asp
            // https://habrahabr.ru/company/nixsolutions/blog/301002/
            // http://blog.wolksoftware.com/decorators-reflection-javascript-typescript
            // http://www.timmykokke.com/2014/01/convert-json-to-typescript/ - json to ye script
            // http://stackoverflow.com/questions/22885995/how-do-i-initialize-a-typescript-object-with-a-json-object
            // http://stackoverflow.com/questions/22875636/how-do-i-cast-a-json-object-to-a-typescript-class?noredirect=1&lq=1
            Product4 = (function () {
                function Product4() {
                }
                /**
                 * CSharpTypeFullName of this contract
                */
                Product4.TypeFullName = 'DDDD.TSS.Tests.Model.CommunicationModel.Product4, DDDD.Core.Test.Model.Net45';
                return Product4;
            }());
            exports_1("Product4", Product4);
            /**
             *  Http handling service, that  Executes DC commands and calls any of the BL Method on the server side, and return result to the client.
             */
            ClientReportService = (function (_super) {
                __extends(ClientReportService, _super);
                function ClientReportService(httpServiceInherited) {
                    _super.call(this, httpServiceInherited);
                    this.httpServiceInherited = httpServiceInherited;
                }
                ClientReportService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], ClientReportService);
                return ClientReportService;
            }(dddd_core_1.WebApiDirectHandlerClient));
            exports_1("ClientReportService", ClientReportService);
            ClientReportComponent = (function () {
                function ClientReportComponent(clientReportService) {
                    this.clientReportService = clientReportService;
                    this.pageTitle = ClientReportComponent.defaultPageTitle;
                }
                ClientReportComponent.GetProduct = function () {
                    var product = {
                        "productId": 1,
                        "productName": "Leaf Rake",
                        "productCode": "GDN-0011",
                        "releaseDate": "March 19, 2016",
                        "description": "Leaf rake with 48-inch wooden handle.",
                        "price": 19.95,
                        "starRating": 3.2,
                        "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
                    };
                    return product;
                };
                ClientReportComponent.GetProducts = function () {
                    return [
                        {
                            "productId": 1,
                            "productName": "Leaf Rake",
                            "productCode": "GDN-0011",
                            "releaseDate": "March 19, 2016",
                            "description": "Leaf rake with 48-inch wooden handle.",
                            "price": 19.95,
                            "starRating": 3.2,
                            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
                        },
                        {
                            "productId": 2,
                            "productName": "Garden Cart",
                            "productCode": "GDN-0023",
                            "releaseDate": "March 18, 2016",
                            "description": "15 gallon capacity rolling garden cart",
                            "price": 32.99,
                            "starRating": 4.2,
                            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
                        },
                        {
                            "productId": 5,
                            "productName": "Hammer",
                            "productCode": "TBX-0048",
                            "releaseDate": "May 21, 2016",
                            "description": "Curved claw steel hammer",
                            "price": 8.9,
                            "starRating": 4.8,
                            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
                        },
                        {
                            "productId": 8,
                            "productName": "Saw",
                            "productCode": "TBX-0022",
                            "releaseDate": "May 15, 2016",
                            "description": "15-inch steel blade hand saw",
                            "price": 11.55,
                            "starRating": 3.7,
                            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png"
                        },
                        {
                            "productId": 10,
                            "productName": "Video Game Controller",
                            "productCode": "GMG-0042",
                            "releaseDate": "October 15, 2015",
                            "description": "Standard two-button video game controller",
                            "price": 35.95,
                            "starRating": 4.6,
                            "imageUrl": "http://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png"
                        }
                    ];
                };
                // SaveProduct
                // SaveProducts
                // GetProducts
                /**
                 * Test sending one product item to save it on the server side
                 */
                ClientReportComponent.prototype.Test_SaveProduct = function () {
                    var commandName = 'SaveProduct';
                    var newTextParameter = dddd_core_1.DCParameter.NewInParam('product', ClientReportComponent.OneProduct, Product4.TypeFullName);
                    this.clientReportService.ExecuteCommand(commandName, function (responseMessage) {
                        //client callback TO DO
                    }, newTextParameter);
                };
                /**
                 *  Test sending products collection to save them on the server side
                 */
                ClientReportComponent.prototype.Test_SaveProducts = function () {
                    var commandName = 'SaveProducts';
                    var newTextParameter = dddd_core_1.DCParameter.NewInParam('products', ClientReportComponent.ProductCollection, dddd_core_1.CSharType.GetListName(Product4.TypeFullName));
                    this.clientReportService.ExecuteCommand(commandName, function (responseMessage) {
                        //client callback TO DO
                    }, newTextParameter);
                };
                /**
                 *  Test Getting Products collection from the server side
                 */
                ClientReportComponent.prototype.Test_LoadProducts = function () {
                    var commandName = 'LoadProducts';
                    var receivedDCMessage = this.clientReportService.ExecuteCommand(commandName, function (responseMessage) {
                        console.info('LoadProducts():  received parameters' + responseMessage.Parameters.length);
                        var productsParam = responseMessage.Parameters.find(function (prm) { return prm.Key === 'products'; });
                        if (productsParam == null) {
                            console.error('LoadProducts(): parameter products was not found');
                            return;
                        }
                        //let products =   <Array<Product4>>JSON.parse(productsParam.ValueText);
                        var products2 = productsParam.Deserialize();
                        if (products2 == null) {
                            console.error('LoadProducts():  products was not loaded from server');
                            return;
                        }
                    });
                };
                ClientReportComponent.OneProduct = ClientReportComponent.GetProduct();
                ClientReportComponent.ProductCollection = ClientReportComponent.GetProducts();
                ClientReportComponent.defaultPageTitle = 'Acme Product Management';
                ClientReportComponent = __decorate([
                    core_1.Component({
                        selector: 'client-report',
                        templateUrl: 'app/views/client_report.view.html'
                    }), 
                    __metadata('design:paramtypes', [ClientReportService])
                ], ClientReportComponent);
                return ClientReportComponent;
            }());
            exports_1("ClientReportComponent", ClientReportComponent);
        }
    }
});
//# sourceMappingURL=client_report.view.js.map