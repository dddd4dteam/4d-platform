﻿/// <reference path="./libs/System/system.ts" />

import { Component} from 'angular2/core'
import { HTTP_PROVIDERS } from 'angular2/http';
import 'rxjs/Rx';   // Load all features

import {ClientReportService ,ClientReportComponent} from './views/client_report.view';
//import {ClientReportService} from './views/client_report.service';




@Component({
    selector: 'pm-app'    
    , templateUrl: 'app/app.component.html'
    , directives: [ClientReportComponent] //[ROUTER_DIRECTIVES]
     
    , providers: [ClientReportService, HTTP_PROVIDERS] //ROUTER_PROVIDERS]
        

})
export class AppComponent {
  
    static defaultPageTitle: string = 'Acme Product Management';

    pageTitle: string = AppComponent.defaultPageTitle;
       

}