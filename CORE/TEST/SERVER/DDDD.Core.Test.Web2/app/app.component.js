/// <reference path="./libs/System/system.ts" />
System.register(['angular2/core', 'angular2/http', 'rxjs/Rx', './views/client_report.view'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, client_report_view_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {},
            function (client_report_view_1_1) {
                client_report_view_1 = client_report_view_1_1;
            }],
        execute: function() {
            //import {ClientReportService} from './views/client_report.service';
            AppComponent = (function () {
                function AppComponent() {
                    this.pageTitle = AppComponent.defaultPageTitle;
                }
                AppComponent.defaultPageTitle = 'Acme Product Management';
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'pm-app',
                        templateUrl: 'app/app.component.html',
                        directives: [client_report_view_1.ClientReportComponent] //[ROUTER_DIRECTIVES]
                        ,
                        providers: [client_report_view_1.ClientReportService, http_1.HTTP_PROVIDERS] //ROUTER_PROVIDERS]
                    }), 
                    __metadata('design:paramtypes', [])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map