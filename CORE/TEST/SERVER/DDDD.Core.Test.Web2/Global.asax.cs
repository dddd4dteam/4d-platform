﻿using DDDD.Core.App;
using DDDD.Core.ComponentModel;
using System;
using System.Reflection;
using System.Web;
using System.Web.SessionState;

namespace DDDD.Core.Test.Web2
{
    public class App : HttpApplication
    {
        //  wcf/ClientReport.svc
        //  wcf/ClientReport2.svc
        //  wcf/ClientReport3.svc
        
        //  webapi/ClientReport


        protected void Application_Start(object sender, EventArgs e)
        {
            AppBootstrapper.Current.LoadAppComponents_WcfWebApi(
                         "CRM_Web"
                        ,null//preapareAppAction
                        ,null// register custom ComponentClasses
                        , () => //collecting App assemblies to the Container
                        {
                            return new[]
                            {
                               typeof(App).Assembly //current App Assembly
                            };

                        }
                        , WebApiConfig.Register
                        , BundleConfig.RegisterBundles
                        , null
                        // () => { SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin")); }
                        );
                                              
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

#if DEBUG  //don't use caching -to reload silverlight packages/js scripts
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
#endif
            //Для того, чтобы обращения к WCF сервисам не блокировали сессию и друг друга
            if (Context.Request.Path.EndsWith(".svc"))
                Context.SetSessionStateBehavior(SessionStateBehavior.ReadOnly);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}