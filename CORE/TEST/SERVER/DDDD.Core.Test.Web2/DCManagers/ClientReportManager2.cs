﻿
using DDDD.Core.HttpRoutes;
using DDDD.Core.ComponentModel;

using System;
using System.Threading.Tasks;
using DDDD.Core.Net;
using DDDD.Core.ComponentModel.Messaging;
#if SL5
    using DDDD.Core.Test.AdventureWorksDB.DAL.SL5.Additional;
#else
using DDDD.TSS.Tests.Model.CommunicationModel;
#endif




namespace DDDD.BCL.Test.Web.DCManagers
{



    [DCCUnitOrder(DCCScenarios.Wcf_EnvironmentActivationSvc, HttpRouteTemplateEn.HttpRoute_BaseurlCommunicationServiceSvc, DCSerializationModeEn.MDCS_TSS)]
    public class ClientReportManager2:DCManager<ClientReportManager2> //by naming convention [CustmManagerName]Manager -your manager Name  should be ending word [Manager]
    {

        #region ------------------------- DOMAIN KNOWN TYPES -----------------------------

        ////------------------////
        //one for all DCUnits
        ////------------------////

        /// <summary>
        /// 
        /// </summary>
        protected override void InitializeDomainKnownTypes()
        {
            base.InitializeDomainKnownTypes();


            // ADD DOMAIN KNOWN TYPES Here
            // AddDomainKnownTypes(...)
            //
        }

        #endregion------------------------- DOMAIN KNOWN TYPES -----------------------------


        #region ---------------------------- DIAGNOSTICS- LOGGING -------------------------
        ////------------------////
        ////  per each DCUnits
        ////------------------////



        #endregion ---------------------------- DIAGNOSTICS- LOGGING -------------------------



        #region ---------------------------  LoadReportContext ---------------------------

#if CLIENT && SL5

        /// <summary>
        /// 
        /// </summary>
        /// <param name="communicationKey"></param>
        /// <param name="newClientID"></param>
        /// <returns></returns>
        public async Task<IDCMessage> LoadNewClientReport(string scenarioKey, Guid newClientID)
        {
            try
            {
                // load from DB data
                var messageResultFromServer = await ExecuteCommandOnClient(
                                                        scenarioKey
                                                        , nameof(LoadNewClientReport)
                                                        , null //report Progress
                                                               //parameters 
                                                        , DCParameter.NewInParam("clientID", newClientID)
                                                        , DCParameter.NewInParam("param1", $"[{nameof(ClientReportManager2)}]  MessageFrom Client To Server")
                                                        );
                var result = messageResultFromServer. GetResult();
                // set result to Body
                return messageResultFromServer;
            }
            catch (Exception exc)
            {
                throw exc;
            }

        }


#elif SERVER

        
        public void LoadNewClientReportServer(ref DCMessage targetMessage)
        {
            try
            {
                //load from DB data

                var param1Value = (Guid)targetMessage.GetParam("clientID").Value;   //.First().Value as string;
                var param2Value = targetMessage.GetParam("param1").Value as string; //.KVPair<object>(

                //here we must clean our input Parameters to not to sent them again as server output parameters - it's only internal command scope logic
                targetMessage.Parameters.Clear();


                //set result to Body
                targetMessage.SetResult($"[{nameof(ClientReportManager2)}]  Message From Server to Client: Received by [param1]Key:[{param2Value}]; Received by [clientID]Key:[{param1Value}]");

                //also here you can add custom command output parameters
                targetMessage.Parameters.Add(DCParameter.NewOutParam("param1out", $"[{nameof(ClientReportManager2)}] param1out Server Value ") );


            }
            catch (Exception exc)
            {
                throw exc;
            }
        } 

#endif


        #endregion ---------------------------  LoadReportContext ---------------------------



    }
}
