﻿
using System;
using System.Linq;

using DDDD.Core.Extensions;
using DDDD.Core.HttpRoutes;
using DDDD.Core.ComponentModel;
using DDDD.Core.Net;
using DDDD.Core.ComponentModel.Messaging;
using System.Net.Http;

using System.Collections.Generic;
using System.Threading.Tasks;

#if SL5
    using DDDD.Core.Test.AdventureWorksDB.DAL.SL5.Additional;
#else 
    using DDDD.TSS.Tests.Model.CommunicationModel;
#endif



namespace DDDD.BCL.Test.Web.DCManagers
{


    // IF WE USE IIS EXPRESS then HttpRouteTemplate should be changed -such Uris don't have {WebApplication} segment. 
    /// <para/> Communication1:  Wcf [Wcf_EnvironmentActivationSvc] Scenario. Example: - http://localhost/wcf/ClientReport.svc
    /// <para/> Communication2:  WebApi [WebApiDirectHandlers] Scenario. Example: - http://localhost/webapi/ClientReport
    
    // IF WE USE IIS SERVER we 



    /// <summary>
    /// ClientReport BL Manager:    
    /// <para/> Communication1:  Wcf [Wcf_EnvironmentActivationSvc] Scenario. Example: - http://localhost/wcf/ClientReport.svc
    /// <para/> Communication2:  WebApi [WebApiDirectHandlers] Scenario. Example: - http://localhost/webapi/ClientReport
    /// </summary>
    [DCCUnitOrder(DCCScenarios.Wcf_EnvironmentActivationSvc, HttpRouteTemplateEn.HttpRoute_BaseurlCommunicationServiceSvc, DCSerializationModeEn.MDCS_TSS)]
    [DCCUnitOrder(DCCScenarios.WebApiDirectHandlers, HttpRouteTemplateEn.HttpRoute_BaseurlCommunicationContrlr , DCSerializationModeEn.SSJSON)]
    public class ClientReportManager:DCManager<ClientReportManager> //by naming convention [CustmManagerName]Manager -your manager Name  should be ending word [Manager]
    {

        #region ------------------------- DOMAIN KNOWN TYPES -----------------------------
       
        ////------------------////
        //one for all DCUnits
        ////------------------////
       
        /// <summary>
        /// 
        /// </summary>
        protected override void InitializeDomainKnownTypes()
        {
            base.InitializeDomainKnownTypes();

            AddDomainKnownTypes(typeof(DCParameter));
            // ADD DOMAIN KNOWN TYPES Here
            // AddDomainKnownTypes(...)
            //
        }

        #endregion------------------------- DOMAIN KNOWN TYPES -----------------------------


        #region ---------------------------- DIAGNOSTICS- LOGGING -------------------------
        ////------------------////
        ////  per each DCUnits
        ////------------------////



        #endregion ---------------------------- DIAGNOSTICS- LOGGING -------------------------


        // LoadReportContext        
        #region ---------------------------  LoadReportContext ---------------------------

#if CLIENT && SL5

        /// <summary>
        /// 
        /// </summary>
        /// <param name="communicationKey"></param>
        /// <param name="newClientID"></param>
        /// <returns></returns>
        public async  Task<IDCMessage> LoadNewClientReport(string scenarioKey,  Guid newClientID) 
        {
            try
            {
                // load from DB data
                var messageResultFromServer = await ExecuteCommandOnClient(
                                                        scenarioKey
                                                        , nameof(LoadNewClientReport)
                                                        , null //report Progress
                                                               //parameters 
                                                        , DCParameter.NewInParam("clientID", newClientID)
                                                        , DCParameter.NewInParam("param1", $"[{nameof(ClientReportManager)}]  Message From Client To Server")
                                                        );

                var result = messageResultFromServer.GetResult();
                // set result to Body
                return messageResultFromServer;
            }
            catch (Exception exc)
            { throw exc;
            }
        }


#elif SERVER  // WCF

        
        public void LoadNewClientReportServer(ref IDCMessage targetMessage)
        {            
                //load from DB data

                var param1Value = (Guid)targetMessage.GetParam("clientID").Value;   //.First().Value as string;
                var param2Value = targetMessage.GetParam("param1").Value as string; //.KVPair<object>(
                
                //set result to Body
                targetMessage.SetResult( $"[{nameof(ClientReportManager)}]  Message From Server to Client: Received by [param1]Key:[{param2Value}]; Received by [clientID]Key:[{param1Value}]" );

                //also here you can add custom command output parameters
                targetMessage.NewInParam("param1out", $"[{nameof(ClientReportManager)}] param1out Server Value " );
            
            
                    

        }

#endif


        #endregion ---------------------------  LoadReportContext ---------------------------







        #region ------------------------------- SaveProduct ----------------------------------

        public static void SaveProductServer(ref IDCMessage message)
        {
            var httpRequestMessage = message.GetParam(nameof(HttpRequestMessage));

            var product = message.GetParam("product");
            var prodctValue = product.Value;
            //return DCMessage2.Empty;
        }

        #endregion------------------------------- SaveProduct ----------------------------------





        #region ------------------------------- SaveProducts ----------------------------------

        public static void SaveProductsServer(ref IDCMessage message)
        {

            var httpRequestMessage = message.GetParam(nameof(HttpRequestMessage)); //only not for WCF

            var products = message.GetParam("products");
            var prodctsValue = products.Value;
          
        }

        #endregion------------------------------- SaveProducts ----------------------------------





        #region ------------------------------- LoadProducts ----------------------------------


        public static void LoadProductsServer(ref IDCMessage message)
        {
            var httpRequestMessage = message.GetParam(nameof(HttpRequestMessage));  //only not for WCF                     
            
            message.NewOutParam("products", CollectProducts()); // it seems TextParameter            
        }



        static List<Product4> CollectProducts()
        {
            var products = new List<Product4>()
            {
                new Product4()
                {
                    productId = 1
                  , productName = "Leaf Rake"
                  , productCode= "GDN-0011"
                  , releaseDate=  DateTime.Parse( "March 19, 2016")
                  , description= "Leaf rake with 48-inch wooden handle."
                  , price = 19.95 
                  , starRating= 3.2
                  , imageUrl= "http://openclipart.org/image/300px/svg_to_png/26215/Anonymous_Leaf_Rake.png"
                }
                ,
                new Product4()
                {                
                    productId = 2
                  , productName = "Garden Cart"
                  , productCode= "GDN-0023"
                  , releaseDate=  DateTime.Parse( "March 18, 2016")
                  , description= "15 gallon capacity rolling garden cart"
                  , price = 32.99
                  , starRating= 4.2
                  , imageUrl= "http://openclipart.org/image/300px/svg_to_png/58471/garden_cart.png"
                }
                ,
                new Product4()
                {
                    productId = 5
                  , productName = "Hammer"
                  , productCode= "TBX-0048"
                  , releaseDate=  DateTime.Parse( "May 21, 2016")
                  , description= "Curved claw steel hammer"
                  , price = 8.9 
                  , starRating= 4.8
                  , imageUrl= "http://openclipart.org/image/300px/svg_to_png/73/rejon_Hammer.png"
                }
                ,
                new Product4()
                {
                    productId = 8
                  , productName = "Saw"
                  , productCode= "TBX-0022"
                  , releaseDate=  DateTime.Parse( "May 15, 2016")
                  , description= "15-inch steel blade hand saw"
                  , price = 11.55
                  , starRating= 3.7
                  , imageUrl= "http://openclipart.org/image/300px/svg_to_png/27070/egore911_saw.png"
                }
                ,
                new Product4()
                {                   
                    productId = 10
                  , productName = "Video Game Controller"
                  , productCode= "GMG-0042"
                  , releaseDate=  DateTime.Parse( "October 15, 2015")
                  , description= "Standard two-button video game controller"
                  , price =  35.95
                  , starRating= 4.6
                  , imageUrl= "http://openclipart.org/image/300px/svg_to_png/120337/xbox-controller_01.png"

                }

            };


            return products;
        }


  

        #endregion------------------------------- LoadProducts ----------------------------------



    }
}
