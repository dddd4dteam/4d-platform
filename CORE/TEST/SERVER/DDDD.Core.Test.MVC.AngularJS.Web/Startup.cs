﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DDDD.Core.Test.MVC.AngularJS.Web.Startup))]
namespace DDDD.Core.Test.MVC.AngularJS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
