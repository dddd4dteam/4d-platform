﻿using System.Web;
using System.Web.Mvc;

namespace DDDD.Core.Test.MVC.AngularJS.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
