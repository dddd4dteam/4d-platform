 
## DDDD.Core lib

**DDDD(Domain Driven Design Dialect) Team CORE-platform lib.
This lib can help to develop all .NET apps with good DDD infrastructure and base architecture patterns.   
- DDDD.Core[.Client/Server] lib. 
Folder CORE contains  DDDD.Core[.Client/Server] solution.

DDDD\CORE\DOCS\-  DOC folder also contains documentation  about first version and future version( not released) documents as docx  


##  [ OTHER PROJECTS  ](https://yadi.sk/d/VwLWFoCgXONgiQ)  ##
    
    , CRM  muduled solution
    , Excel VA(virtual analyser)- addin for MS Excesl 2013/2016
    , CRM testing- example db
    , WIN DEVICE MANAGER - WINdows core dlls  interface based on C#  to manage of WIFI/ GPS other mobile devices hardware components ... 
    , METROSTYLING -  WPF/ SL5 controls styling as METRO UI-style 
    , SQL INTEGRATIN SERVICES - exampe of json data loading from http link to db
    , NET_RODKILLWIKI - copy of ROADKILL WIKI system
    , NET_ALGORITHMIA
    , NET_DYNAMICPRESENTATION - WPF based dynamic UI forms generation lib
    , NET_VSTemplates - templates/base code wizards for empty start solutions in Visual Studion, with WPF Dialogs. Soulution  Template/Item Template 
    , NET_PHOENIXSB - Service Bus communication  ENGINE today is commercial - for c# clients / web json client/  redis store caching...
    , NET_HEDERA - Hedera  hash graph payment system
    , NET_XSD2CODE - c# xsd to C# contracts code generator tool- visual studion con tool for xsd files
    , NET_SENSENET - commercial and community license support  C# PORTAL.  With Dynamic-based on .NETWorkflow files UI manager.
    , NET_WEAKREFERENCIES - Weak referencies testing/ patterns  development project - to clear C# Garbage collecting of class fererencies concept and develop patterned  soukution -C# classes. 
    , NET_TPL_TRAININGKIT - training kit of  NET. TPL -Task Parallel library and MS DOCS of Parallel programming patterns
    
    
    
### DDDD Core ###
  Base classes, enums
       , extensions for 4D Team packages associated with the following themes 
	   , collections , reflection, type Activating
	   , widely used patterns
	   , IOC = сomponent Model  lightweghit,
	   , Communication  
	   , DAL QueryBuilder [ORM and Model Generator][1]
	   validation, environment
	   , app process details, app base url model, client side app http routing,
	   serialization,  communication.  
	   
	Link to the DDDD.Core  package sources https://bitbucket.org/dddd4dteam/4d-platform/src  look in the BCL folder.

### DDDD.Core.Reflection

### DDDD.Core.IOC

### DDDD.Core.DA2   and [DDDD.DAL.Generators.Tool][2]


### DDDD.Core.Serialization -  
      BinarySerializer and JsonSerializer 
	  designed to be used in multithreading and supports TPL infrastructure.

	Link to the DDDD.Core.WPF   sources 
	[https://bitbucket.org/dddd4dteam/4d-platform/src/master/][3] 
	 
	
 
### DDDD.Core.Net -  DC Network Communication(Dynamic Command Network Communication - WCF ) ###
	WCF service clients with integrated use of TSSerializer.
	DCMessage  Dynamic Command Message - is flexible MessageFormat that also can be customized-  overriden.    
	Link to the DDDD.Core  package sources -https://bitbucket.org/dddd4dteam/dddd4d-tools/src  look in the CORE\Net\ServiceModel.

  [1]: GENERATORS%2FD4Generator%2FDDDD.DAL.Generators.Tool%2FDocumentation%2FOverview.md
  [2]: GENERATORS%2FD4Generator%2FDDDD.DAL.Generators.Tool%2FDocumentation%2FOverview.md
  [3]: https://bitbucket.org/dddd4dteam/4d-platform/src/master/
