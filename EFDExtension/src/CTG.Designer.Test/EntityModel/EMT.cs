﻿using Microsoft.CSharp;
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace CTG.EntityModel
{
    public static class EMT
    {
        public static Dictionary<string, string> TemplateMetadata = new Dictionary<string, string>();
        
        
        private static readonly System.Collections.IList _errors;
        private static readonly string PROVIDER_NAME_SQLCE_PREFIX = "System.Data.SqlServerCe"; // this is the runtime provider name - all versions start with this prefix
        private static string _providerInvariantName;
        private static StoreItemCollection _store;
        private static StoreItemCollection _existingStore;


      
        static XElement LoadRootElement(string sourcePath)
        {
            //ArgumentNotNull(sourcePath, "sourcePath");

            var root = XElement.Load(sourcePath, LoadOptions.SetBaseUri | LoadOptions.SetLineInfo);
            return root.Elements()
                .Where(e => e.Name.LocalName == "Runtime")
                .Elements()
                .Where(e => e.Name.LocalName == "ConceptualModels")
                .Elements()
                .Where(e => e.Name.LocalName == "Schema")
                .FirstOrDefault()
                    ?? root;
        }

        static void ProcessErrors(IEnumerable<EdmSchemaError> errors, string sourceFilePath)
        {
            foreach (var error in errors)
            {
                _errors.Add(
                    new CompilerError(
                        error.SchemaLocation ?? sourceFilePath,
                        error.Line,
                        error.Column,
                        error.ErrorCode.ToString(CultureInfo.InvariantCulture),
                        error.Message)
                    {
                        IsWarning = error.Severity == EdmSchemaErrorSeverity.Warning
                    });
            }
        }


        /// <summary>
        /// Loading all  GlobalItem  items from *.edmx file.
        /// analog of EdmMetadataLoader.CreateEdmItemCollection(string path)
        /// </summary>
        /// <param name="sourcePath"></param>
        /// <returns></returns>
        public static IEnumerable<GlobalItem> LoadEdmx(string sourcePath)
        {
            //ArgumentNotNull(sourcePath, "sourcePath");

            if ( !File.Exists(sourcePath) )
            {
                return new EdmItemCollection();
            }

            var schemaElement = LoadRootElement(sourcePath); //  _host.ResolvePath(sourcePath)
            if (schemaElement != null)
            {
                using (var reader = schemaElement.CreateReader())
                {
                    IList<EdmSchemaError> errors;
                    var itemCollection = EdmItemCollection.Create(new[] { reader }, null, out errors);

                    ProcessErrors(errors, sourcePath);

                    return itemCollection ?? new EdmItemCollection();
                }
            }
            return new EdmItemCollection();
        }

        
        public static IEnumerable<GlobalItem> SelectByBuiltInType(this IEnumerable<GlobalItem> globalEdmItems , BuiltInTypeKind typeKind)
        {
            if (globalEdmItems == null) return null;           
            return globalEdmItems.Where(glItm => glItm.BuiltInTypeKind == typeKind);
        }


        public static IList<EntityType> SelectEntityTypes(this IEnumerable<GlobalItem> globalEdmItems)
        {
            if (globalEdmItems == null) return null;
            //var metadataproperties = globalEdmItems.FirstOrDefault().BuiltInTypeKind  MetadataProperties;
            return globalEdmItems.Where(glItm => glItm.BuiltInTypeKind == BuiltInTypeKind.EntityType)
                                 .Cast<EntityType>()
                                 .ToList();
        }


        public static EntityType SelectEntity(this IList<EntityType> entitiesEdm, string EntityName )
        {
            if (entitiesEdm == null || EntityName == null || EntityName == "") return null;
            //var metadataproperties = globalEdmItems.FirstOrDefault().BuiltInTypeKind  MetadataProperties;
            return entitiesEdm.Where(eItm => eItm.Name == EntityName).FirstOrDefault();                                 
        }


        public static EdmProperty SelectProperty(this EntityType entity, string propertyName)
        {
            if (entity == null || propertyName == null || propertyName == "") return null;
            //var metadataproperties = globalEdmItems.FirstOrDefault().BuiltInTypeKind  MetadataProperties;
            return entity.Properties.Where(prp => prp.Name == propertyName).FirstOrDefault();
        }


        #region ----------------------------------- ACCESSIBILITY ------------------------------------

      

        /// <summary>
        /// Getting Acessibility for EdmProperty
        /// </summary>
        /// <param name="property"></param>
        /// <param name="propertyAccess"></param>
        /// <param name="propertyGetterAccess"></param>
        /// <param name="propertySetterAccess"></param>
        public static void GetAccessibility(this EdmProperty property
                                                   , out  string propertyAccess
                                                   , out  string propertyGetterAccess
                                                   , out  string propertySetterAccess)
        {

            if (property == null)
            {
                propertyAccess = propertyGetterAccess = propertySetterAccess = null; 
                return;
            }

            propertyAccess = Accessibility.ForProperty(property);
            propertyGetterAccess = Accessibility.ForGetter(property);
            propertySetterAccess = Accessibility.ForSetter(property);
          
        }

        #endregion ----------------------------------- ACCESSIBILITY ------------------------------------



     


       


    }
}
