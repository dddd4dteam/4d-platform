﻿using Microsoft.Data.Edm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace CTG.EntityModel
{
    public static class EDMXExtensions
    {
        public static IEdmModel GetEdmModel(this string filepath)
        {
            using (var fs = new FileStream(filepath, FileMode.OpenOrCreate))
            {
                using (XmlReader reader = XmlReader.Create(fs))
                {
                    return Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
                }
            }

        }

        public static IEdmEntityContainer GetEntityContainer(this string filepath)
        {
            //var model = new EdmModel();
            using (var fs = new FileStream(filepath, FileMode.OpenOrCreate))
            {
                using (XmlReader reader = XmlReader.Create(fs))
                {
                    var mdl = Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
                    return mdl.EntityContainers().FirstOrDefault();

                }
            }

        }

        public static IEnumerable<IEdmEntityContainer> GetEntityContainers(this string filpath)
        {
            //var model = new EdmModel();
            using (var fs = new FileStream(filpath, FileMode.OpenOrCreate))
            {
                using (XmlReader reader = XmlReader.Create(fs))
                {
                    var mdl = Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
                    return mdl.EntityContainers();

                }
            }

        }
        
        public static IEdmEntityType SelectEntity(this IEnumerable<IEdmEntitySet> entitysets, string entityName)
        {
            //var model = new EdmModel();
            return entitysets.Where(entS => entS.ElementType.Name == entityName)
                             .Select(entS => entS.ElementType)
                             .FirstOrDefault();
        }

    }
}
