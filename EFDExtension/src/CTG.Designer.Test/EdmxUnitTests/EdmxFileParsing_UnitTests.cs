﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Data.Edm.Csdl;
using System.Xml;
using System.IO;
using Microsoft.Data.Edm;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Mapping;
using System.Xml.Linq;
using System.Collections.Generic;
using CTG.EntityModel;

namespace CTG.Designer.Test
{
    [TestClass]
    public class EdmxFileparsin_UnitTests
    {
        [TestMethod]
        public void EDMX_SimplyLoadFile_Test()
        {
            // d:\DDDD\CTG\EFDExtension\Main\Temp\
            string edmxFilepath = @"d:\DDDD\CTG\EFDExtension\Main\Temp\NorthwindModel.edmx";

            var efModel = edmxFilepath.GetEdmModel();
            var entItems = EMT.LoadEdmx(edmxFilepath).SelectEntityTypes();

            var ent = EMT.LoadEdmx(edmxFilepath)
                                    .SelectEntityTypes()
                                    .SelectEntity("Order_Details");

            var propt1 = ent.SelectProperty("OrderID");

            string propAccess, propGetterAcess, propSetterAccess = "";
            propt1.GetAccessibility(out propAccess, out propGetterAcess, out propSetterAccess);

            var metaprops = propt1.MetadataProperties;
           
            
        }

       

        //public static IEnumerable<IEdmSchemaType> GetEntities(string filpath)
        //{
        //    foreach (var item in collection)
        //    {
		 
        //    }
            
        //    return 
        //}



        static void EditEdmxMetadata()
        {

            // Formatting helper for code
            //CodeGenerationTools code = new CodeGenerationTools(this);

            // object for creating entity information
            //MetadataLoader loader = new MetadataLoader(this);

            //MetadataLoader loader = new MetadataLoader(this);
            //EdmItemCollection items = loader.CreateEdmItemCollection(inputFile);



            // TiraggoEdmx code now ...
            //Edmx edmx = Edmx.Load(@"d:\DDDD\CTG\EFDExtension\Main\Temp\NorthwindModel.edmx");



            ////edmx.Designer

            //// We use the loader to get the entity names in our .edmx
            //foreach (EntityType e in items.GetItems<EntityType>().OrderBy(a => a.Name))
            //{
            //    // Everthing you need including your low level SQL metadata types
            //    TiraggoEntityInfo info = new TiraggoEntityInfo(edmx, e.FullName);

            //    string schema = info.StorageInfo.Schema;  // dbo
            //    string table = info.StorageInfo.Name;     // Employees

            //    foreach (tgProperty prop in info.ConceptualModel.Properties)
            //    {
            //        // prop.Name is conceptual name, let's get the physical name
            //        string physicalColumName = info.ColumnMappings[prop.Name];

            //        // Let's now get the low level SQL info using the physical name
            //        tgProperty sqlInfo = info.ColumnSQL[physicalColumName];

            //        // Now we have the low level SQL Information
            //        string s1 = sqlInfo.Type; // nvarchar, datetime, ...
            //        string s2 = sqlInfo.Name; // physical column name
            //        // and so on with many more properties on sqlInfo ...
            //    }
            //}



        }



        //private static IEdmModel GetEdmModel(DbContext db)
        //{
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var xmlWriter = XmlWriter.Create(
        //            memoryStream, new XmlWriterSettings
        //            {
        //                Indent = true
        //            }))
        //        {
        //            EdmxWriter.WriteEdmx(db, xmlWriter);
        //        }

        //        memoryStream.Position = 0;

        //        using (var reader = XmlReader.Create(memoryStream))
        //            return EdmxReader.Parse(reader);
        //    }
        //}

        //private static XDocument GetEdmx(DbContext db)
        //{
        //    XDocument doc;
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        using (var xmlWriter = XmlWriter.Create(
        //            memoryStream, new XmlWriterSettings
        //            {
        //                Indent = true
        //            }))
        //        {
        //            EdmxWriter.WriteEdmx(db, xmlWriter);
        //        }

        //        memoryStream.Position = 0;

        //        doc = XDocument.Load(memoryStream);
        //    }
        //    return doc;
        //}

        //public static IEdmEntityType GetEntities(string filpath)
        //{
        //    //var model = new EdmModel();
        //    using (var fs = new FileStream(filpath, FileMode.OpenOrCreate))
        //    {
        //        using (XmlReader reader = XmlReader.Create(fs))
        //        {

        //            var mdl = Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
        //            var containers =  mdl. EntityContainers().ToList(); 
        //            var types = mdl.FindAllDerivedTypes()
                    
        //            return null;

        //        }
        //    }

        //}



    }
}
