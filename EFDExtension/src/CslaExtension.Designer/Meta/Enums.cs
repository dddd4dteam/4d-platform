﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CslaExtension.Designer
{
    public enum CslaMultipleFilesOutput
    {
        AllClassesInSingleFile,
        EachClassInSeparateFile,
        CommonAndServerCodeForEachClass
    }

    public enum CslaPropertyInfoModifier
    {
        Public,
        Private,
        Protected,
        Internal
    }

    public enum CslaModifier
    {
        None,
        Default,
        Private,
        Public,
        Protected,
        Internal
    }

    public enum YesNoDefault
    {
        Default,
        Yes,
        No
    }
    
    public enum CslaClassTemplate
    {
        None,
        EditableRoot,
        EditableRootList,
        EditableChild,
        EditableChildList,
        ReadOnlyRoot,
        ReadOnlyRootList,
        ReadOnlyChild,
        ReadOnlyChildList
    }

    public enum CslaTransactionalTypes
    {
        TransactionScope,
        EnterpriseServices,
        Manual
    }
    
}
