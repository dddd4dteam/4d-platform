﻿
// EDExtension - Entity Designer Extension 
// 1 Add reference to - Microsoft.Data.Entity.Design.Extensibility.dll, v12 (VS2013 version) or your VS version	
// 2 You should change Namespace or/and Customize class name

#region  --------------------- EDExtension PropertyManager class ------------------------------

// PARAMS :
// FirmProductPrefix              -      ED4DExtension                     EX:  ED4DExtension - i.e. means  Entity Designer Extension from 4D


using Microsoft.Data.Entity.Design.Extensibility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DDDD.EDExtensions
{
    /// <summary>
    /// PropertyManager helps us to get and set xml values from edmx file's  xml meta info.
    /// </summary>
    internal class PropertyManager
    {
        static readonly string FirmProductPrefix = "ED4DExtension";

        /// <summary>
        /// Getting string value from _parent element by xName
        /// </summary>
        /// <param name="_parent">Parent elements collection</param>
        /// <param name="xName">name of target Element</param>
        /// <returns>XElement value</returns>
        public static string GetValue(XElement _parent, XName xName)
        {
            string result = string.Empty;
            if (_parent.HasElements)
            {
                XElement lastChild = _parent.Elements()
                                            .Where<XElement>(element => element != null && element.Name == xName)
                                            .LastOrDefault();

                if (lastChild != null)
                    result = lastChild.Value.ToString();
            }
            return result;
        }


        /// <summary>
        /// Setting string value to the element. We searching this element in _parent.Elements() by xName
        /// PropertyExtensionContext _context we use to create Change EntityProperty Value Scope. 
        /// </summary>
        /// <param name="_parent">Parent elements collection</param>
        /// <param name="_context">PropertyExtensionContext helps us to create Change EntityProperty Value Scope</param>
        /// <param name="xName"> name of target Element</param>
        /// <param name="value">new value</param>
        public static void SetValue(XElement _parent, PropertyExtensionContext _context, XName xName, string value)
        {
            string propertyValue = string.Empty;
            if (value != null)
                propertyValue = value.Trim();
            using (EntityDesignerChangeScope scope = _context.CreateChangeScope(FirmProductPrefix + " Setting EntityProperties"))
            {
                if (_parent.HasElements)
                {
                    XElement lastChild = _parent.Elements()
                                                .Where<XElement>(element => element != null && element.Name == xName)
                                                .LastOrDefault();
                    
                    if (lastChild != null)
                    {
                        lastChild.SetValue(propertyValue);
                    }
                    else
                    {
                        // MyNewProperty element does not exist, so create a new one as the last child of the EntityType element.
                        _parent.Elements()
                               .Last()
                               .AddAfterSelf(new XElement(xName, propertyValue));
                    }
                }
                else
                {
                    // The EntityType element has no child elements so create a new MyNewProperty element as its first child.
                    _parent.Add(new XElement(xName, propertyValue));
                }

                // Commit the changes.
                scope.Complete();
            }
        }
    }

}

#endregion --------------------- EDExtension PropertyManager class ------------------------------

