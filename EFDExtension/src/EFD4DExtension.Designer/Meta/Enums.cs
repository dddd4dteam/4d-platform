﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.EDExtensions
{
    public enum CslaMultipleFilesOutput
    {
        AllClassesInSingleFile,
        EachClassInSeparateFile,
        CommonAndServerCodeForEachClass
    }

    public enum CslaPropertyInfoModifier
    {
        Public,
        Private,
        Protected,
        Internal
    }
    
    public enum YesNoDefault
    {
        Default,
        Yes,
        No
    }
    
    public enum CslaClassTemplate
    {
        None,
        EditableRoot,
        EditableRootList,
        EditableChild,
        EditableChildList,
        ReadOnlyRoot,
        ReadOnlyRootList,
        ReadOnlyChild,
        ReadOnlyChildList
    }

    public enum CslaTransactionalTypes
    {
        TransactionScope,
        EnterpriseServices,
        Manual
    }





   /// <summary>
   /// Property Classification:
   /// </summary> 
   public enum PropertyRoleEn {
        
        /// <remarks/>
        NotDefined,
        
        /// <remarks/>
        PKeyField,
        
        /// <remarks/>
        FKeyField,
        
        /// <remarks/>
        VOFKeyField,
        
        /// <remarks/>
        DataField,
        
        /// <remarks/>
        FacadeSQLDataField,
        
        /// <remarks/>
        FacadeCodeDataField,
    }


    /// <summary>
    /// Access Modifiers for some element(class / property)
    /// </summary>
    public enum AccessModifierEn
    {
        None
        ,Default
        ,Private
        ,Public
        ,Protected
        ,Internal
        ,ProtectedInternal
    }


    /// <summary>
    /// Service DataModel structure 
    /// </summary>
    public enum ServiceDataModelStructureEn
    {
        NotDefined
        ,CustomDataModelStructure
        ,StarDataModelStructure
        ,SnowFlakeDataModelStructure
        ,HierarchyDataModelStructure
    }




    

    /// <summary>
    /// Entity Collection Caching Flags.
    /// </summary>
    [Flags]
    public enum CachingEn: short
    {

        /// <remarks/>
        NotDefined = 2,

        /// <remarks/>
        Server_Global = 4,

        /// <remarks/>
        Client_Global = 8,

        /// <remarks/>
        Client_Page= 16,

        /// <remarks/>
        Server_Global_Client_Page = 32,

        /// <remarks/>
        Server_Global_Client_Global = 64,
    }

}
