﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.MetaModel
{    
    [AttributeUsage(AttributeTargets.Property)]
    public class NotifyPropertyChangedAttribute : System.Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Parameter)]
    public class NotNullAttribute : System.Attribute
    {
    }
}
