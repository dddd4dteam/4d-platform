﻿namespace MetaDrive.Linq.Edmx.Test
{
    using System.Linq;
    using FluentAssertions;
    using MetaDrive.Stores.Edmx.Model;
    using MetaDrive.Stores.Edmx.Test.Models;
    using NUnit.Framework;

    [TestFixture]
    public class SSDLContainerTest
    {
        protected EdmContainer Edm { get; private set; }

        [TestFixtureSetUp]
        public void SetUpFixture()
        {
            Edm = ModelLoader.Load(Models.SampleModel);
        }
     
        [Test]
        public void Sample()
        {
            var selectSampleTable = Edm.Ssdl.Functions.SingleOrDefault(x => x.Name == "SelectSampleTable");

            selectSampleTable.Should().NotBeNull();
            selectSampleTable.CommandText.Should().NotBeNullOrEmpty();
            selectSampleTable.Parameters.Should().BeEmpty();

            var selectSampleTableWithParameter = Edm.Ssdl.Functions.SingleOrDefault(x => x.Name == "SelectSampleTableWithParameter");

            selectSampleTableWithParameter.Should().NotBeNull();
            selectSampleTableWithParameter.CommandText.Should().NotBeNullOrEmpty();
            selectSampleTableWithParameter.Parameters.Should().HaveCount(1);
        }
    }
}
