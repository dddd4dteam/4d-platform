﻿namespace MetaDrive.Linq.Edmx.Test
{
    using System.Linq;
    using FluentAssertions;
    using MetaDrive.Stores.Edmx.Model;
    using MetaDrive.Stores.Edmx.Model.Csdl;
    using MetaDrive.Stores.Edmx.Test.Models;
    using NUnit.Framework;

    [TestFixture]
    public class NorthwindConceptualTest
    {
        protected EdmContainer Edm { get; private set; }

        [TestFixtureSetUp]
        public void SetUpFixture()
        {
            Edm = ModelLoader.Load(Models.Northwind);
        }

        [Test]
        public void EntityTypeCount()
        {
            Edm.Csdl.EntityTypes.Should().HaveCount(11);
        }
      
        [Test]
        public void CategoryEntity()
        {
            var categoryEntity = Edm.Csdl.EntityTypes.SingleOrDefault(x => x.Name == "Category");
            categoryEntity.Should().NotBeNull();
            
            // Id Property

            var idProperty = categoryEntity.ScalarProperties.SingleOrDefault(x => x.Name == "Id");
            idProperty.Should().NotBeNull();
            idProperty.Type.Should().Be(PropertyType.Int32);
            idProperty.Nullable.Should().BeFalse();
            
            // Category Property

            var categoryNameProperty = categoryEntity.ScalarProperties.SingleOrDefault(x => x.Name == "CategoryName");
            categoryNameProperty.Should().NotBeNull();
            categoryNameProperty.Type.Should().Be(PropertyType.String);
            categoryNameProperty.Nullable.Should().BeFalse();
            categoryNameProperty.MaxLength.Should().Be(15);
            categoryNameProperty.Unicode.Should().BeTrue();
            categoryNameProperty.FixedLength.Should().BeFalse();

            // Description Property

            var descriptionProperty = categoryEntity.ScalarProperties.SingleOrDefault(x => x.Name == "Description");
            descriptionProperty.Should().NotBeNull();

            descriptionProperty.Type.Should().Be(PropertyType.String);
            //descriptionProperty.MaxLength.Should() 
            descriptionProperty.Unicode.Should().BeTrue();
            descriptionProperty.FixedLength.Should().BeFalse();
            
            // Picture Property

            var pictureProperty = categoryEntity.ScalarProperties.SingleOrDefault(x => x.Name == "Picture");
            pictureProperty.Should().NotBeNull();

            pictureProperty.Type.Should().Be(PropertyType.Binary);
            //Assert.AreEqual(null, pictureProperty.MaxLength);
            pictureProperty.FixedLength.Should().BeFalse();
            
            // Products Navigation Property

            var productsNavigationProperty = categoryEntity.NavigationProperties.SingleOrDefault(x => x.Name == "Products");
            productsNavigationProperty.Should().NotBeNull();
            
            //Assert.AreEqual("NorthwindModel.FK_Products_Categories", productsNavigationProperty.Association..Relationship);
            //Assert.AreEqual("Category", productsNavigationProperty.FromRole);
            //Assert.AreEqual("Products", productsNavigationProperty.ToRole);            
        }
    }
}
