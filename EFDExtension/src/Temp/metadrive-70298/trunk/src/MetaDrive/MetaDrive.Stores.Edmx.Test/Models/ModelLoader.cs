﻿namespace MetaDrive.Stores.Edmx.Test.Models
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;    
    using MetaDrive.Stores.Edmx.Model;
    using MetaDrive.Stores.Edmx.Repositories;

    public static class ModelLoader
    {
        private static Dictionary<Models, string> paths = new Dictionary<Models,string>()
        {
            { Models.SampleModel, @"Models\SampleModel2.edml"},
            { Models.Northwind, @"Models\Northwind.edmx"}
        };
          
        public static EdmContainer Load(Models model)
        {            
            var edmxPath = Path.Combine(Environment.CurrentDirectory, paths[model]);
            return EdmxRepository.Read(edmxPath, Read);
        }

        static void Read(XElement element)
        {
        }
    }

    public enum Models
    {
        SampleModel,
        Northwind
    }
}
