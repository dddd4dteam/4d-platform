﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.CSDL.Attributes
{
    public class ExcludeItselftAttribute : Attribute
    {
    }
}
