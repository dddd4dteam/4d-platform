﻿#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Linq.Edmx.EDMObjects.Common;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.ChangeWatcher
{
    public interface IEDMDesignerChangeWatcherObserver
    {
        bool ObjectChanged(object changedObject);
    }
}
