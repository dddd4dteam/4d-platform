﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System.Collections.Generic;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Type;
using MetaDrive.Linq.Edmx.EDMObjects.MSL.EntityType;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.MSL
{
    public class ComplexPropertiesMapping
    {
        public ComplexPropertiesMapping(TypeBase type, MappingBase mapping, MetaDrive.Linq.Edmx.EDMObjects.SSDL.EntityType.EntityType table)
        {
            Type = type;
            Mapping = mapping;
            Table = table;
            TPC = true;
        }

        public TypeBase Type { get; private set; }
        public MappingBase Mapping { get; private set; }
        public MetaDrive.Linq.Edmx.EDMObjects.SSDL.EntityType.EntityType Table { get; private set; }
        public bool TPC { get; set; }

        public IEnumerable<ComplexPropertyMapping> Mappings
        {
            get
            {
                foreach (var complexProperty in (TPC ? Type.AllComplexProperties : Type.ComplexProperties))
                    yield return new ComplexPropertyMapping(complexProperty, Mapping, Table) { TPC = TPC };
            }
        }
    }
}
