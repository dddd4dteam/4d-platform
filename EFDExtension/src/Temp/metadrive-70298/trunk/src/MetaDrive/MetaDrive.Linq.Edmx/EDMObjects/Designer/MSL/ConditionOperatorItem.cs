﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using MetaDrive.Linq.Edmx.EDMObjects.MSL.Condition;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.MSL
{
    public class ConditionOperatorItem : ItemBase<ConditionOperator>
    {
        internal ConditionOperatorItem()
        {
        }
    }
}
