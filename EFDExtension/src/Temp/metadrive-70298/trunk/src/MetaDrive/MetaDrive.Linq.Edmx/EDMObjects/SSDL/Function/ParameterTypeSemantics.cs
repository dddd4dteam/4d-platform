﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

namespace MetaDrive.Linq.Edmx.EDMObjects.SSDL.Function
{
    public enum ParameterTypeSemantics
    {
        ExactMatchOnly,
        AllowImplicitPromotion,
        AllowImplicitConversion
    }
}
