﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Linq.Edmx.EDMObjects.SSDL.Function;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.MSL.CUDFunction
{
    public class FunctionParameterMapping
    {
        public FunctionParameter SSDLFunctionParameter { get; set; }
        public FunctionParameterVersion? Version { get; set; }
    }
}
