﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Type;
using MetaDrive.Linq.Edmx.EDMObjects.Designer.CSDL.Property;
using MetaDrive.Linq.Edmx.EDMObjects.Designer.Common;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.CSDL.Type
{
    public interface IUIType
    {
        TypeBase BusinessInstance { get; }
        string Name { get; set; }
        IndexableUIBusinessTypeObservableCollection<PropertyBase, UIProperty> Properties { get; }
        UIProperty AddScalarProperty(string propertyName, MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property.PropertyType propertyType);
        UIRelatedProperty AddComplexProperty(string propertyName, ComplexType complexType);
        void DeleteProperty(UIProperty property);
        CSDLView View { get; }
        event Action<UIRelatedProperty> RelatedPropertyDeleted;
    }
}
