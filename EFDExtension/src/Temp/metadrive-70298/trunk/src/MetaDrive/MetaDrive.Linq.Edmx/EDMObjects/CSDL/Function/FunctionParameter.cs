﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Linq.Edmx.EDMObjects.Common;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.CSDL.Function
{
    public class FunctionParameter : EDMObjectBase
    {
        public PropertyType Type { get; set; }
        public ParameterMode? Mode { get; set; }
        public int? Precision { get; set; }
        public int? Scale { get; set; }
        public int? MaxLength { get; set; }
    }
}
