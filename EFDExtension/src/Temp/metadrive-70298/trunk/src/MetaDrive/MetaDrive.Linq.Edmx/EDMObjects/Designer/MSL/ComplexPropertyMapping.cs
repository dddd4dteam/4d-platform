﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Linq.Edmx.EDMObjects.MSL.EntityType;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.MSL
{
    public class ComplexPropertyMapping
    {
        public ComplexPropertyMapping(ComplexProperty complexProperty, MappingBase mapping, MetaDrive.Linq.Edmx.EDMObjects.SSDL.EntityType.EntityType table)
        {
            ComplexProperty = complexProperty;
            Mapping = mapping;
            Table = table;
        }

        public ComplexProperty ComplexProperty { get; private set; }
        public MappingBase Mapping { get; private set; }
        public MetaDrive.Linq.Edmx.EDMObjects.SSDL.EntityType.EntityType Table { get; private set; }
        public bool TPC { get; set; }

        public IEnumerable<PropertyMapping> Mappings
        {
            get
            {
                foreach (var property in ComplexProperty.ComplexType.AllScalarProperties)
                    yield return new PropertyMapping(property, Mapping.GetSpecificMappingCreateIfNull(ComplexProperty), Table);
            }
        }

        public ComplexPropertiesMapping ComplexPropertiesMapping
        {
            get 
            {
                return new ComplexPropertiesMapping(ComplexProperty.ComplexType, Mapping.GetSpecificMappingCreateIfNull(ComplexProperty), Table) { TPC = TPC }; 
            }
        }
    }
}
