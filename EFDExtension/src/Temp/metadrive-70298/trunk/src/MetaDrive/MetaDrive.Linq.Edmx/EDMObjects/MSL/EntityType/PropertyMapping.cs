﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property;
using MetaDrive.Linq.Edmx.EDMObjects.SSDL.Property;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.MSL.EntityType
{
    public class PropertyMapping
    {
        public ScalarProperty Property { get; internal set; }
        public Property Column { get; internal set; }
    }
}
