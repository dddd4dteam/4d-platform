﻿// Copyright (c) AlphaSierraPapa for the SharpDevelop Team (for details please see \doc\copyright.txt)
// This code is distributed under the GNU LGPL (for details please see \doc\license.txt)

#region Usings

using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Type;
using MetaDrive.Linq.Edmx.EDMObjects.Designer.Common;

#endregion

namespace MetaDrive.Linq.Edmx.EDMObjects.Designer.CSDL.Type
{
    public class UIComplexType : UITypeBase<ComplexType>
    {
        internal UIComplexType(CSDLView view, ComplexType complexType)
            : base(view, complexType)
        {
        }
    }
}
