﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{
    public enum SnippetTraversal
    {
        Source,
        Target
    }

    public class SnippetStep : PipelineStep
    {
        public ISnippet Snippet { get; set; }

        public SnippetTraversal SourceTraversal { get; set; }
        public SnippetTraversal TargetTraversal { get; set; }
        
        public SnippetStep(ISnippet snippet) : this(snippet, SnippetTraversal.Source, SnippetTraversal.Target)
        {
        }

        public SnippetStep(ISnippet snippet, SnippetTraversal source, SnippetTraversal target)
        {
            this.Snippet = snippet;
            this.SourceTraversal = source;
            this.TargetTraversal = target;            
        }

        protected override bool DoProcess()
        {            
            FluentCodeModel sourceCodeModel = ServiceProvider.GetService<ISourceService>().CodeModel;
            FluentCodeModel targetCodeModel = ServiceProvider.GetService<ITargetService>().CodeModel;

            Snippet.Source = (SourceTraversal == SnippetTraversal.Source) ? sourceCodeModel : targetCodeModel;
            Snippet.Target = (TargetTraversal == SnippetTraversal.Source) ? sourceCodeModel : targetCodeModel;
            
            Snippet.Process();                        
            return true;
        }
    }
}
