﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface IModelService
    {
        EnvDTE.ProjectItem Model { get; }
    }
}
