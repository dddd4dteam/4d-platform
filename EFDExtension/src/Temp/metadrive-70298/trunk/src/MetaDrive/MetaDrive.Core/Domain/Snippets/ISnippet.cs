﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{
    public interface ISnippet
    {
        FluentCodeModel Source { get; set; }
        FluentCodeModel Target { get; set; }

        void Process();                
    }
}
