﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class TargetModelService : ITargetModelService
    {
        private FluentCode.FluentCodeModel codeModel;

        public FluentCode.FluentCodeModel CodeModel
        {
            get
            {
                if (codeModel == null)
                    codeModel = new MetaDrive.FluentCode.FluentCodeModel((EnvDTE80.FileCodeModel2)Target.FileCodeModel);

                return codeModel;
            }
        }

        public EnvDTE.ProjectItem Target { get; private set; }

        public TargetModelService(EnvDTE.ProjectItem target)
        {
            this.Target = target;
        }
    }
}
