﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class TargetService : ITargetService
    {
        FluentCode.FluentCodeModel codeModel;

        public FluentCode.FluentCodeModel CodeModel
        {
            get
            {                
                if (codeModel == null && Target.FileCodeModel != null)
                    codeModel = new MetaDrive.FluentCode.FluentCodeModel((EnvDTE80.FileCodeModel2)Target.FileCodeModel);

                return codeModel;
            }
        }

        public EnvDTE.ProjectItem Target { get; private set; }

        public TargetService(EnvDTE.ProjectItem target)
        {
            if (target == null)
                throw new ArgumentNullException("target");

            this.Target = target;
        }
    }
}
