﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{    
    public class SourceMatchingStep : PipelineStep
    {
        public ISourceMatchingRule MatchingRule { get; private set; }

        public SourceMatchingStep(ISourceMatchingRule matchingRule)
        {
            this.MatchingRule = matchingRule;
        }

        protected override bool DoProcess()
        {            
            ISourceService modelService = base.ServiceProvider.GetService<ISourceService>();
            return MatchingRule.Matches(modelService.Source);
        }
    }
}
