﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class AndSpecification<T> : Specification<T>
    {
        private readonly ISpecification<T> leftSide;
        private readonly ISpecification<T> rightSide;

        public AndSpecification(ISpecification<T> leftSide, ISpecification<T> rightSide)
        {
            this.leftSide = leftSide;
            this.rightSide = rightSide;
        }

        public override bool IsSatisfiedBy(T element)
        {
            return leftSide.IsSatisfiedBy(element) && rightSide.IsSatisfiedBy(element);
        }
    }
}
