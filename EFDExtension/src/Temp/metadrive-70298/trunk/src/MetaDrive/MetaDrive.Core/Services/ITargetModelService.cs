﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface ITargetModelService
    {
        EnvDTE.ProjectItem Target { get; }
        FluentCode.FluentCodeModel CodeModel { get; }
    }
}
