﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public abstract class PipelineStep : IPipelineStep
    {               
        protected IServiceContainer ServiceProvider { get; private set; }
        
        public void Init(IServiceContainer serviceProvider)
        {
            this.ServiceProvider = serviceProvider;
        }

        public bool Process()
        {
            return DoProcess();
        }

        public void Complete()
        {
            DoComplete();
        }

        protected virtual bool DoProcess()
        {
            return true;
        }

        protected virtual void DoComplete()
        {
        }                
    }
}
