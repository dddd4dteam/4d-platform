﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface ITargetService
    {
        EnvDTE.ProjectItem Target { get; }
        FluentCode.FluentCodeModel CodeModel { get; }
    }
}
