﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class SourceService : ISourceService
    {
        FluentCode.FluentCodeModel codeModel;

        public FluentCode.FluentCodeModel CodeModel
        {
            get
            {                
                if (codeModel == null && Source.FileCodeModel != null)
                    codeModel = new MetaDrive.FluentCode.FluentCodeModel((EnvDTE80.FileCodeModel2)Source.FileCodeModel);

                return codeModel;
            }
        }

        public EnvDTE.ProjectItem Source { get; set; }

        public SourceService()
        {
        }

        public SourceService(EnvDTE.ProjectItem source)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            this.Source = source;
        }
    }
}
