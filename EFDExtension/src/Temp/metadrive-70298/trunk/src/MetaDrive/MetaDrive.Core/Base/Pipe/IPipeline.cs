﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface IPipeline : IEnumerable<IPipelineStep>
    {
        IServiceContainer ServiceProvider { get; }
        bool Process();
        IPipeline Add(IPipelineStep step);
    }
}