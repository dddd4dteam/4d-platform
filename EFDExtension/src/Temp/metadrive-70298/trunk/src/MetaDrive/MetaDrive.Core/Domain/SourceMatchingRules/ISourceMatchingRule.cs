﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{   
    public interface ISourceMatchingRule
    {
        bool Matches(EnvDTE.ProjectItem projectItem);
    }
}
