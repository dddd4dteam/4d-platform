﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{      
    public class AttributeSourceMatchingRule : SourceMatchingRule
    {
        public IEnumerable<string> AttributeNames { get; private set; }

        public AttributeSourceMatchingRule(IEnumerable<Type> attributeTypes)
        {
            var attributeNames = new List<string>();
                        
            foreach (var attribute in attributeTypes.Where(x => x.IsA<System.Attribute>()))
                attributeNames.Add(attribute.FullName);
            
            AttributeNames = attributeNames.AsEnumerable();
        }

        public AttributeSourceMatchingRule(IEnumerable<string> attributeNames)
        {
            this.AttributeNames = attributeNames;
        }
        
        public override bool Matches(EnvDTE.ProjectItem item)
        {            
            if (item == null)
                throw new ArgumentNullException("item");

            if (item.FileCodeModel == null)
                return false;

            var codeModel = new FluentCodeModel((EnvDTE80.FileCodeModel2)item.FileCodeModel);
            return codeModel.Query.OfType<FluentCodeAttribute>().Any(x => AttributeNames.Contains(x.FullName));
        }
    }
}