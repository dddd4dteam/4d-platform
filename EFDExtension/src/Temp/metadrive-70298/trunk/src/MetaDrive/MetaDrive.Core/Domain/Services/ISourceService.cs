﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface ISourceService
    {
        EnvDTE.ProjectItem Source { get; }
        FluentCode.FluentCodeModel CodeModel { get; }
    }
}
