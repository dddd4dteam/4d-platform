﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{
    public class AutoFormatStep : PipelineStep
    {        
        protected override bool DoProcess()
        {
            ITargetService targetModelService = base.ServiceProvider.GetService<ITargetService>();
            targetModelService.CodeModel.AutoFormat();
            return true;
        }
    }
}
