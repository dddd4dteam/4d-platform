﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public abstract class SourceMatchingRule : ISourceMatchingRule //: Specification<EnvDTE.ProjectItem>, ISourceMatchingRule
    {
        //public override bool IsSatisfiedBy(EnvDTE.ProjectItem element)
        //{
        //    return Matches(element);
        //}

        public abstract bool Matches(EnvDTE.ProjectItem projectItem);
    }
}
