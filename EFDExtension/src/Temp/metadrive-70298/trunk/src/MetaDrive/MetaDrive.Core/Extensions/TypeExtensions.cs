﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public static class TypeExtensions
    {
        public static bool IsA<T>(this Type type)
        {
            return typeof(T).IsAssignableFrom(type);
        }
    }
}
