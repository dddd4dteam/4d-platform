﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface IServiceContainer
    {
        T GetService<T>() where T : class;
        void SetService<T>(T instance) where T : class;
    }
}
