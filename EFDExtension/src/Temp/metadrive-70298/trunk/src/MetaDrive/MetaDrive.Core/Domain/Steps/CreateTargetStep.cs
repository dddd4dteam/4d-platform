﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class CreateTargetStep : PipelineStep
    {
        public ICreateTargetItem CreateTargetItem { get; private set; }

        public CreateTargetStep(ICreateTargetItem createTargetItem)
        {
            this.CreateTargetItem = createTargetItem;
        }

        protected override bool DoProcess()
        {
            ISourceService modelService = this.ServiceProvider.GetService<ISourceService>();

            if (modelService != null)
            {
                EnvDTE.ProjectItem projectItem = CreateTargetItem.Create(modelService.Source);
                this.ServiceProvider.SetService<ITargetService>(new TargetService(projectItem));
            }

            return true;
        }
    }   
}
