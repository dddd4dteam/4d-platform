﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class ServiceContainer : IServiceContainer
    {
        private Dictionary<Type, object> services = new Dictionary<Type, object>();

        public T GetService<T>() where T : class
        {
            if (!this.services.ContainsKey(typeof(T)))
            {
                return null;
            }

            return (T)this.services[typeof(T)];
        }

        public void SetService<T>(T serviceInstance) where T : class
        {
            if (this.services.ContainsKey(typeof(T)))
            {
                this.services[typeof(T)] = serviceInstance;
            }
            else
            {
                this.services.Add(typeof(T), serviceInstance);
            }
        }
    }
}
