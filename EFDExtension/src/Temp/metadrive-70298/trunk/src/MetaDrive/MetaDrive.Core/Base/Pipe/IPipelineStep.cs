﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface IPipelineStep
    {
        void Init(IServiceContainer serviceProvider);
        bool Process();
        void Complete();
    }
}
