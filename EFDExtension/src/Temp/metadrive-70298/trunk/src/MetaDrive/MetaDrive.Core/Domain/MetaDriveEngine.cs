﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class MetaDriveEngine
    {
        Dictionary<string, IPipeline> pipelines = new Dictionary<string, IPipeline>();
        SourceService modelService = new SourceService();

        public void AddPipeline(string name, IPipeline pipeline)
        {
            pipelines.Add(name, pipeline);
        }

        public void Process(IEnumerable<EnvDTE.Project> projects)
        {
            InitializePipelines();

            foreach (EnvDTE.Project project in projects)
            {
                foreach (var projectItem in ProjectItemIterator.Create(project))
                {
                    modelService.Source = projectItem;
                    Process(projectItem);
                }
            }
        }
        
        protected void InitializePipelines()
        {
            foreach (IPipeline pipeline in pipelines.Values)
            {
                pipeline.ServiceProvider.SetService<ISourceService>(modelService);
            }
        }

        protected void Process(EnvDTE.ProjectItem projectItem)
        {
            foreach (IPipeline pipeline in pipelines.Values)
            {
                if (pipeline.Process())
                    break;
            }
        }
    }
}
