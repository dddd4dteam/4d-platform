﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public static class PipelineExtensions
    {
        public static IPipeline Add(this IPipeline pipeline, ISnippet snippet)
        {
            pipeline.Add(new SnippetStep(snippet));
            return pipeline;
        }

        public static IPipeline Add(this IPipeline pipeline, ISnippet snippet, SnippetTraversal source, SnippetTraversal target)
        {
            pipeline.Add(new SnippetStep(snippet, source, target));
            return pipeline;
        }

        public static IPipeline Add(this IPipeline pipeline, ISourceMatchingRule matchingRule)
        {
            pipeline.Add(new SourceMatchingStep(matchingRule));
            return pipeline;
        }
        
        public static IPipeline Add(this IPipeline pipeline, ICreateTargetItem createTargetItem)
        {
            pipeline.Add(new CreateTargetStep(createTargetItem));
            return pipeline;
        }

        public static IPipeline Add(this IPipeline pipeline, IEnumerable<IPipelineStep> steps)
        {
            pipeline.Add(new PipelineStepDecorator(steps));
            return pipeline;
        }
    }
}
