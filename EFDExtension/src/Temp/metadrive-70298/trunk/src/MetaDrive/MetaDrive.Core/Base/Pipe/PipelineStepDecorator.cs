﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public class PipelineStepDecorator : PipelineStep
    {
        public IEnumerable<IPipelineStep> Steps { get; private set; }

        public PipelineStepDecorator(IEnumerable<IPipelineStep> steps)
        {
            this.Steps = steps;
        }
        
        protected override bool DoProcess()
        {
            foreach (var step in Steps)
                if (!step.Process())
                    return false;

            return true;
        }

        protected override void DoComplete()
        {
            foreach (var step in Steps)
                step.Complete();
        }
    }
}
