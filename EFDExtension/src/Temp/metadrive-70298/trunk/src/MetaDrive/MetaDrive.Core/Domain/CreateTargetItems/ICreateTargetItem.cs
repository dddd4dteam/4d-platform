﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MetaDrive.Core
{
    public interface ICreateTargetItem
    {
        EnvDTE.ProjectItem Create(EnvDTE.ProjectItem modelItem);
    }    
}
