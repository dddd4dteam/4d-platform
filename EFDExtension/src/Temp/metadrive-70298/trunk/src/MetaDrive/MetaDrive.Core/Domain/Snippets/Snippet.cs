﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.FluentCode;

namespace MetaDrive.Core
{
    public class Snippet : FluentCodeVisitor, ISnippet
    {
        public FluentCodeModel Source { get; set; }
        public FluentCodeModel Target { get; set; }
                
        public void Process()
        {
            OnBeforeProcess();
            base.Visit(Source.Query.Children);
            OnAfterProcess();
        }

        protected virtual void OnBeforeProcess()
        {
        }

        protected virtual void OnAfterProcess()
        {
        }
    }
}
