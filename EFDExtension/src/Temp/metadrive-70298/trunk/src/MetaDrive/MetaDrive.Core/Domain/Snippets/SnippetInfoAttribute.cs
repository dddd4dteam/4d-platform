﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SnippetInfoAttribute : System.Attribute
    {
        public string Name { get; set; }
        
        public SnippetInfoAttribute(string name)
        {
            this.Name = name;            
        }
    }
}
