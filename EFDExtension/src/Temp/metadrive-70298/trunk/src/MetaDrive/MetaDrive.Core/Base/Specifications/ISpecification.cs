﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Core
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T element);

        ISpecification<T> And(ISpecification<T> specification);
        ISpecification<T> Or(ISpecification<T> specification);
        ISpecification<T> Not(ISpecification<T> specification);
    }
}
