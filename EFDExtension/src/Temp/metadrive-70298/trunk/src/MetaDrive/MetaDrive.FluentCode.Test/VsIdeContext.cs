﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EnvDTE80;
using MetaDrive.Core;

namespace MetaDrive.FluentCode.Test
{
    public static class VsIdeContext
    {
        static EnvDTE80.DTE2 dte = null;
        static EnvDTE.Project currentProject = null;

        public static EnvDTE80.DTE2 DTE
        {
            get
            {
                if (dte == null)
                    dte = (EnvDTE80.DTE2)System.Runtime.InteropServices.Marshal.GetActiveObject("VisualStudio.DTE.9.0");
                    
                return dte;
            }
        }

        public static EnvDTE.Project CurrentProject 
        {
            get
            {
                if (currentProject == null)
                    currentProject = DTE.Solution.Projects.Cast<EnvDTE.Project>().Where(x => x.Name == "MetaDrive.FluentCode.Test").FirstOrDefault();

                return currentProject; 
            }
        }

        public static EnvDTE.ProjectItem FindProjectItem(string filename)
        {
            foreach (var projectItem in ProjectItemIterator.Create(CurrentProject))
                if (projectItem.Name == filename)
                    return projectItem;

            return null;
        }
    }
}
