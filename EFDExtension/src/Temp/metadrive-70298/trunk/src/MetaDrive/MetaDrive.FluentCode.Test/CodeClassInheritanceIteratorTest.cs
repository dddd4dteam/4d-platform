﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MetaDrive.Core;
using EnvDTE80;

namespace MetaDrive.FluentCode.Test
{
    [TestClass]
    public class CodeClassInheritanceIteratorTest 
    {
        public TestContext TestContext { get; set; }
        
        [TestMethod]
        public void TestMethod1()
        {
            EnvDTE.ProjectItem projectItem = VsIdeContext.FindProjectItem("Sample.cs");
            EnvDTE80.CodeClass2 codeClass = new CodeElementIterator(projectItem.FileCodeModel.CodeElements).OfType<CodeClass2>().Where(x => x.Name == "SampleConsumer").FirstOrDefault();

            var iterator = new CodeClassInheritanceIterator(codeClass);
            var inheritanceTree = iterator.Select(x => x.Name).ToList();
        }

        [TestMethod]
        public void TestMethod2()
        {
            EnvDTE.ProjectItem projectItem = VsIdeContext.FindProjectItem("Sample.cs");

            var codeModel = new FluentCodeModel(((EnvDTE80.FileCodeModel2)projectItem.FileCodeModel));
            
            //var dddd = codeModel.Query(.ElementByName<FluentCodeClass>("SampleAB");

            foreach (var par in codeModel.Query.OfType<FluentCodeParameter>())
            {
                Console.WriteLine(par.Name);
            }

            //foreach (var f in g.Query.Parameters)
            //{
            //    Console.WriteLine(f.Name);
            //}
                       
            //EnvDTE80.CodeClass2 codeClass = new CodeElementIterator(projectItem.FileCodeModel.CodeElements).OfType<CodeClass2>().Where(x => x.Name == "SampleConsumer").FirstOrDefault();

            //var iterator = new CodeClassInheritanceIterator(codeClass);
            //var inheritanceTree = iterator.Select(x => x.Name).ToList();
        }

    }
}
