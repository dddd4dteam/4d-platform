﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode.Test.Samples
{
    public partial class SampleA : Random
    {
        public void MethodA(int c, int d)
        {
        }
    }

    public partial class SampleAB : SampleA
    {
        public void MethodA(int a, int b)
        {
        }
    }

    public partial class SampleABC : SampleAB
    {
        public void MethodA(int e, int f)
        {
        }
    }

    public partial class SampleConsumer
    {
    }
}
