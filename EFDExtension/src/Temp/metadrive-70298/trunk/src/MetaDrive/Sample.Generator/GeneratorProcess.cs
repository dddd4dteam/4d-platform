﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Core;
using MetaDrive.Library;
using MetaDrive.FluentCode;

namespace Sample.Generator
{
    public class GeneratorProcess
    {
        public void Process(IEnumerable<EnvDTE.Project> projects)
        {
            var g = projects.FirstOrDefault().CodeModel;
            
            MetaDriveEngine engine = new MetaDriveEngine();

            var modelAttributes = new Type[]
            {
                typeof(MetaDrive.MetaModel.NotifyPropertyChangedAttribute)
            };

            IPipeline pipeline = new Pipeline().
                Add(new AttributeSourceMatchingRule(modelAttributes)).
                Add(new CreateChildTargetItem("")).
                Add(new MarkVirtualSnippet(modelAttributes)).
                Add(new ProxySnippet()).
                Add(new NotifyPropertyChangedMixin(), SnippetTraversal.Target, SnippetTraversal.Target).
                Add(new NotifyPropertyChangedSnippet(), SnippetTraversal.Target, SnippetTraversal.Target).
                Add(new AutoFormatStep());
            
            engine.AddPipeline("General", pipeline);
            engine.Process(projects);
        }
    }
}
