﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Core;
using MetaDrive.FluentCode;

namespace Sample.Generator
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageFilter.Register();

            var dtes = DTEHelper.GetRunningVSIDETable();

            var solution = dtes.Values.Where(x => x.Solution.FullName.Contains("MetaDrive Sample")).FirstOrDefault().Solution;
            var projects = solution.Projects.Cast<EnvDTE.Project>().
                    Where(x => x.Name == "Sample.Library");
            
            var process = new GeneratorProcess();
            process.Process(projects);

            MessageFilter.Revoke();
        }
    }   
}
