﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeNamespace
    {
        public sealed class FluentCodeNamespaceCommand : FluentCodeCommand<FluentCodeNamespace>
        {
            public FluentCodeNamespaceCommand(FluentCodeNamespace element) : base(element)
            {
            }

            public FluentCodeClass AddClass(string name, vsCMAccess access, string baseName)
            {
                return AddClass(name, access, baseName, null, false);
            }

            public FluentCodeClass AddClass(string name, vsCMAccess access, string baseName, string[] interfaceNames, bool partial)
            {
                EnvDTE80.CodeClass2 newClass = null;

                string temporaryName = "X" + Guid.NewGuid().ToString().Replace("-", "");
                object baseNameParameter = string.IsNullOrEmpty(baseName) ? null : new object[] { baseName };

                newClass = (EnvDTE80.CodeClass2)Element.Adaptee.AddClass(temporaryName, 0, baseNameParameter, interfaceNames, access);

                if (partial)
                    newClass.ClassKind = EnvDTE80.vsCMClassKind.vsCMClassKindPartialClass;

                newClass.Name = name;
                return new FluentCodeClass(newClass);
            }               
        }
    }
}
