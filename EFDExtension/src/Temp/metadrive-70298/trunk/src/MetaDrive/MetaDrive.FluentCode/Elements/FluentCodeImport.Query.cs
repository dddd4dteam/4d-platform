﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeImport
    {
        public sealed class FluentCodeImportQuery : FluentCodeQuery<FluentCodeImport>
        {
            public FluentCodeImportQuery(FluentCodeImport element) : base(element)
            {
            }
        }
    }
}
