﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{   
    public partial class FluentCodeModel
    {
        public sealed class FluentCodeModelQuery : FluentCodeQuery<FluentCodeModel>
        {
            public IEnumerable<FluentCodeNamespace> Namespaces
            {
                get { return base.OfType<FluentCodeNamespace>(); }
            }

            public IEnumerable<FluentCodeClass> Classes
            {
                get { return base.OfType<FluentCodeClass>(); }
            }

            public FluentCodeModelQuery(FluentCodeModel element) : base(element)
            {
            }        
        }
    }
}