﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public static class FluentCodeClassExtensions
    {        
        public static IEnumerable<FluentCodeFunction> ByName(this IEnumerable<FluentCodeFunction> elements, string name)
        {
            foreach (var element in elements)
                if (element.Name == name)
                    yield return element;
        }

        /*
        public static IEnumerable<FluentCodeClass> ByName(this IEnumerable<FluentCodeClass> elements, string name)
        {
            foreach(var element in elements)
                if (element.Name == name)
                    yield return element;
        }
        */
    }
}
