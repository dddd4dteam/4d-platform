﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeAttribute
    {
        public sealed class FluentCodeAttributeQuery : FluentCodeQuery<FluentCodeAttribute>
        {           
            public FluentCodeAttributeQuery(FluentCodeAttribute element) : base(element)
            {
            }
        }
    }
}
