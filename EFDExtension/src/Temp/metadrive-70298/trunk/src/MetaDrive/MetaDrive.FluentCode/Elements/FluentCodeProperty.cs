﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeProperty : FluentCodeElementBase<EnvDTE.CodeProperty, FluentCodeProperty.FluentCodePropertyQuery, FluentCodeProperty.FluentCodePropertyCommand>
    {
        FluentCodeClass parent = null;
        FluentCodeFunction getter = null;
        FluentCodeFunction setter = null;

        public FluentCodeClass Parent
        {
            get 
            {
                if (parent == null)
                    parent = new FluentCodeClass((EnvDTE80.CodeClass2)Adaptee.Parent);
                return parent;
            }
        }

        public FluentCodeFunction Getter
        {
            get
            {
                if (getter == null)
                    getter = new FluentCodeFunction((EnvDTE80.CodeFunction2)Adaptee.Getter);
                return getter;
            }
        }

        public FluentCodeFunction Setter
        {
            get
            {
                if (setter == null)
                    return new FluentCodeFunction((EnvDTE80.CodeFunction2)Adaptee.Setter);
                return setter;
            }
        }

        public FluentCodeProperty(EnvDTE.CodeProperty adaptee) : base(adaptee)
        {
        }

        // TODO: refactor
        public void OverrideKind(vsCMOverrideKind kind)
        {            
            EditPoint ep = Adaptee.StartPoint.CreateEditPoint();
                        
            if (!ep.GetLines(ep.Line, ep.Line + 1).Contains(kind.AsString()))
                ep.ReplaceText(Adaptee.Access.AsString().Length, Adaptee.Access.AsString() + " " + kind.AsString(), 0);
        }                
    } 
}
