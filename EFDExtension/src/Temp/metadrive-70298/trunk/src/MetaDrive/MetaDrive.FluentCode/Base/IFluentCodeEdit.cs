﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public interface IFluentCodeEdit
    {
        EnvDTE.TextPoint StartPoint { get; }
        EnvDTE.TextPoint EndPoint { get; }

        EnvDTE.TextPoint GetStartPoint(EnvDTE.vsCMPart part);
        EnvDTE.TextPoint GetEndPoint(EnvDTE.vsCMPart part);

        string Body { get; set; }        
    }
}
