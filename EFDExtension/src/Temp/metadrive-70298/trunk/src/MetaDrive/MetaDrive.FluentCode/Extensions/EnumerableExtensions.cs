﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MetaDrive.FluentCode
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IFluentCodeElement> ToFluentCodeElement<T>(this IEnumerable<T> list)
        {
            foreach (var item in list)
            {
                EnvDTE.CodeElement codeElement = item as EnvDTE.CodeElement;
                if (codeElement != null)
                    yield return codeElement.ToFluentCodeElement();
            }
        }

        public static IEnumerable<T> ToFluentCodeElement<T>(this IEnumerable list)
        {
            foreach (var item in list)
            {
                EnvDTE.CodeElement codeElement = item as EnvDTE.CodeElement;
                if (codeElement != null)
                {
                    if (codeElement.ToFluentCodeElementType() == typeof(T))
                        yield return (T)codeElement.ToFluentCodeElement();
                }
            }
        }
    }
}
