﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeParameter
    {
        public sealed class FluentCodeParameterQuery : FluentCodeQuery<FluentCodeParameter>
        {          
            public FluentCodeParameterQuery(FluentCodeParameter element) : base(element)
            {
            }         
        }
    }
}
