﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeInterface : FluentCodeElementBase<EnvDTE80.CodeInterface2, FluentCodeInterface.FluentCodeInterfaceQuery, FluentCodeInterface.FluentCodeInterfaceCommand>
    {
        public FluentCodeInterface(EnvDTE80.CodeInterface2 adaptee) : base(adaptee)
        {
        }
    }
}
