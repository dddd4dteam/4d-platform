﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeInterface
    {
        public sealed class FluentCodeInterfaceCommand : FluentCodeCommand<FluentCodeInterface>
        {
            public FluentCodeInterfaceCommand(FluentCodeInterface element) : base(element)
            {
            }            
        }
    }
}
