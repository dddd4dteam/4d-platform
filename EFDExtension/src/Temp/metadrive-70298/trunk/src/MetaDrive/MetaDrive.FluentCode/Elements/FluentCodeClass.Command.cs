﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeClass
    {
        public sealed class FluentCodeClassCommand : FluentCodeCommand<FluentCodeClass>
        {
            public FluentCodeClassCommand(FluentCodeClass element) : base(element)
            {
            }

            public FluentCodeEvent AddEvent(string name, string fullDelegateName, vsCMAccess access)
            {
                return new FluentCodeEvent(Element.Adaptee.AddEvent(name, fullDelegateName, false, -1, access));
            }

            public FluentCodeProperty AddProperty(string name, object type, vsCMAccess access)
            {
                CodeProperty codeProperty = Element.Adaptee.AddProperty(name, name, type, 0, access, 0);
                codeProperty.Name = name;
                return new FluentCodeProperty(codeProperty);
            }

            public FluentCodeFunction AddFunction(string name, vsCMFunction function, object type, vsCMAccess access)
            {
                var codeFunction = Element.Adaptee.AddFunction(name, function, type, 0, access, 0);                
                return new FluentCodeFunction((EnvDTE80.CodeFunction2)codeFunction);
            }

            public void AddImplementedInterface(string interfaceName)
            {
                Element.Adaptee.AddImplementedInterface(interfaceName, -1);
            }

            public FluentCodeFunction InsertFunction(FluentCodeFunction codeFunction)
            {
                FluentCodeFunction newCodeFunction = AddFunction(
                                                    codeFunction.Name,
                                                    codeFunction.Adaptee.FunctionKind,
                                                    codeFunction.Adaptee.Type,
                                                    codeFunction.Adaptee.Access);

                newCodeFunction.Adaptee.CanOverride = codeFunction.Adaptee.CanOverride;

                for (int i = 1; i <= codeFunction.Adaptee.Parameters.Count; i++)
                {
                    CodeParameter currentCodeParameter = (CodeParameter)codeFunction.Adaptee.Parameters.Item(i);
                    CodeParameter codeParameter = newCodeFunction.Adaptee.AddParameter(currentCodeParameter.Name, currentCodeParameter.Type, -1);
                }

                return newCodeFunction;
            }
        }
    }
}