﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public static class CodeElementExtensions
    {
        public static Type ToFluentCodeElementType(this EnvDTE.CodeElement element)
        {
            switch (element.Kind)
            {
                case EnvDTE.vsCMElement.vsCMElementAssignmentStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementAttribute:
                    return typeof(FluentCodeAttribute);

                case EnvDTE.vsCMElement.vsCMElementClass:
                    return typeof(FluentCodeClass);

                case EnvDTE.vsCMElement.vsCMElementDeclareDecl:
                    break;
                case EnvDTE.vsCMElement.vsCMElementDefineStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementDelegate:
                    break;
                case EnvDTE.vsCMElement.vsCMElementEnum:
                    break;
                case EnvDTE.vsCMElement.vsCMElementEvent:
                    return typeof(FluentCodeEvent);

                case EnvDTE.vsCMElement.vsCMElementEventsDeclaration:
                    break;

                case EnvDTE.vsCMElement.vsCMElementFunction:
                    return typeof(FluentCodeFunction);

                case EnvDTE.vsCMElement.vsCMElementFunctionInvokeStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementIDLCoClass:
                    break;
                case EnvDTE.vsCMElement.vsCMElementIDLImport:
                    break;
                case EnvDTE.vsCMElement.vsCMElementIDLImportLib:
                    break;
                case EnvDTE.vsCMElement.vsCMElementIDLLibrary:
                    break;
                case EnvDTE.vsCMElement.vsCMElementImplementsStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementImportStmt:
                    return typeof(FluentCodeImport);

                case EnvDTE.vsCMElement.vsCMElementIncludeStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementInheritsStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementInterface:
                    break;
                case EnvDTE.vsCMElement.vsCMElementLocalDeclStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementMacro:
                    break;
                case EnvDTE.vsCMElement.vsCMElementMap:
                    break;
                case EnvDTE.vsCMElement.vsCMElementMapEntry:
                    break;
                case EnvDTE.vsCMElement.vsCMElementModule:
                    break;

                case EnvDTE.vsCMElement.vsCMElementNamespace:
                    return typeof(FluentCodeNamespace);

                case EnvDTE.vsCMElement.vsCMElementOptionStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementOther:
                    break;

                case EnvDTE.vsCMElement.vsCMElementParameter:
                    return typeof(FluentCodeParameter);

                case EnvDTE.vsCMElement.vsCMElementProperty:
                    return typeof(FluentCodeProperty);

                case EnvDTE.vsCMElement.vsCMElementPropertySetStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementStruct:
                    break;
                case EnvDTE.vsCMElement.vsCMElementTypeDef:
                    break;
                case EnvDTE.vsCMElement.vsCMElementUDTDecl:
                    break;
                case EnvDTE.vsCMElement.vsCMElementUnion:
                    break;
                case EnvDTE.vsCMElement.vsCMElementUsingStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementVBAttributeGroup:
                    break;
                case EnvDTE.vsCMElement.vsCMElementVBAttributeStmt:
                    break;
                case EnvDTE.vsCMElement.vsCMElementVCBase:
                    break;
                case EnvDTE.vsCMElement.vsCMElementVariable:
                    break;
                default:
                    break;
            }

            return null;
        }

        public static IFluentCodeElement ToFluentCodeElement(this EnvDTE.CodeElement element)
        {
            return (IFluentCodeElement)Activator.CreateInstance(ToFluentCodeElementType(element), element);

            //switch (element.Kind)
            //{
            //    case EnvDTE.vsCMElement.vsCMElementAssignmentStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementAttribute:
            //        return new FluentCodeAttribute((EnvDTE80.CodeAttribute2)element);

            //    case EnvDTE.vsCMElement.vsCMElementClass:
            //        return new FluentCodeClass((EnvDTE80.CodeClass2)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementDeclareDecl:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementDefineStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementDelegate:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementEnum:
            //        break;                
            //    case EnvDTE.vsCMElement.vsCMElementEvent:
            //        return new FluentCodeEvent((EnvDTE80.CodeEvent)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementEventsDeclaration:
            //        break;

            //    case EnvDTE.vsCMElement.vsCMElementFunction:
            //        return new FluentCodeFunction((EnvDTE80.CodeFunction2)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementFunctionInvokeStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementIDLCoClass:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementIDLImport:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementIDLImportLib:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementIDLLibrary:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementImplementsStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementImportStmt:
            //        return new FluentCodeImport((EnvDTE80.CodeImport)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementIncludeStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementInheritsStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementInterface:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementLocalDeclStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementMacro:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementMap:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementMapEntry:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementModule:
            //        break;

            //    case EnvDTE.vsCMElement.vsCMElementNamespace:
            //        return new FluentCodeNamespace((EnvDTE.CodeNamespace)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementOptionStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementOther:
            //        break;

            //    case EnvDTE.vsCMElement.vsCMElementParameter:
            //        return new FluentCodeParameter((EnvDTE80.CodeParameter2)element);
                    
            //    case EnvDTE.vsCMElement.vsCMElementProperty:
            //        return new FluentCodeProperty((EnvDTE.CodeProperty)element);
                   
            //    case EnvDTE.vsCMElement.vsCMElementPropertySetStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementStruct:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementTypeDef:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementUDTDecl:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementUnion:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementUsingStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementVBAttributeGroup:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementVBAttributeStmt:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementVCBase:
            //        break;
            //    case EnvDTE.vsCMElement.vsCMElementVariable:
            //        break;
            //    default:
            //        break;
            //}

            //return null;
        }
    }
}
