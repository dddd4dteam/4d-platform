﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeEvent
    {
        public sealed class Code4EventCommand : FluentCodeCommand<FluentCodeEvent>
        {
            public Code4EventCommand(FluentCodeEvent element) : base(element)
            {
            }            
        }
    }
}
