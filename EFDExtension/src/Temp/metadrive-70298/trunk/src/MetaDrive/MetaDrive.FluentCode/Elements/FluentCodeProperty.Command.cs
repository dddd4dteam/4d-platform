﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeProperty
    {
        public sealed class FluentCodePropertyCommand : FluentCodeCommand<FluentCodeProperty>
        {
            public FluentCodePropertyCommand(FluentCodeProperty element) : base(element)
            {
            }
        }
    }
}
