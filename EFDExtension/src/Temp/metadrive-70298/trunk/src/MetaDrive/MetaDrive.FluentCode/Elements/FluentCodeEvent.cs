﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeEvent : FluentCodeElementBase<EnvDTE80.CodeEvent, FluentCodeEvent.FluentCodeEventQuery, FluentCodeEvent.Code4EventCommand>
    {        
        public FluentCodeEvent(EnvDTE80.CodeEvent adaptee) : base(adaptee)
        {
        }
    }
}
