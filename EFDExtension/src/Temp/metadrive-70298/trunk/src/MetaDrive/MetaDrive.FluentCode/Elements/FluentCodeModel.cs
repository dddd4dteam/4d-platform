﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeModel : FluentCodeElementBase
    {
        public new EnvDTE80.FileCodeModel2 Adaptee
        {
            get { return (EnvDTE80.FileCodeModel2)base.Adaptee; }
        }

        public new FluentCodeModel.FluentCodeModelQuery Query
        {
            get { return (FluentCodeModel.FluentCodeModelQuery)base.Query; }
        }

        public new FluentCodeModel.FluentCodeModelCommand Command
        {
            get { return (FluentCodeModel.FluentCodeModelCommand)base.Command; }
        }

        public override string Name
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override string FullName
        {
            get 
            {
                throw new NotImplementedException();
            }            
        }
        
        public FluentCodeModel(EnvDTE80.FileCodeModel2 adaptee) : base(adaptee, typeof(FluentCodeModel.FluentCodeModelQuery), typeof(FluentCodeModel.FluentCodeModelCommand))
        {            
        }

        public void AutoFormat()
        {
            foreach(var code4Namespace in Query.OfType<FluentCodeNamespace>())
            {
                code4Namespace.Adaptee.StartPoint.CreateEditPoint().SmartFormat(code4Namespace.Adaptee.EndPoint);
            }
        }
    }
}