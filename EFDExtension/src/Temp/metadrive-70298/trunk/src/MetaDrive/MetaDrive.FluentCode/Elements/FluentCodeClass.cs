﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeClass : FluentCodeElementBase<EnvDTE80.CodeClass2, FluentCodeClass.FluentCodeClassQuery, FluentCodeClass.FluentCodeClassCommand>
    {
        public FluentCodeClass(EnvDTE80.CodeClass2 adaptee) : base(adaptee)
        {                        
        }       
    }
}