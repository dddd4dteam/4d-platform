﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeModel
    {
        public sealed class FluentCodeModelCommand : FluentCodeCommand<FluentCodeModel>
        {
            public FluentCodeModelCommand(FluentCodeModel element)  : base(element)
            {
            }

            public FluentCodeNamespace AddNamespace(string name)
            {
                return AddNamespace(name, 0);
            }

            public FluentCodeNamespace AddNamespace(string name, int position)
            {
                FluentCodeNamespace codeNamespace = null;// Element.Query.NamespaceByName(name);
                if (codeNamespace == null)
                    codeNamespace = new FluentCodeNamespace(Element.Adaptee.AddNamespace(name, position));

                return codeNamespace;
            }

            public FluentCodeImport AddImport(string name)
            {
                return new FluentCodeImport(Element.Adaptee.AddImport(name, 0, null));
            }            
        }
    }
}
