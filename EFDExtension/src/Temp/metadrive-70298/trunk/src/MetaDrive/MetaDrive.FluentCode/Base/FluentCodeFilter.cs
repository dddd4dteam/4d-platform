﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public class FluentCodeFilter : IFluentCodeFilter
    {
        public bool Filter(FluentCodeNamespace element)
        {
            return true;
        }

        public bool Filter(FluentCodeClass element)
        {
            return true;
        }

        public bool Filter(FluentCodeProperty element)
        {
            return true;
        }

        public bool Filter(FluentCodeFunction element)
        {
            return true;
        }
    }    
}
