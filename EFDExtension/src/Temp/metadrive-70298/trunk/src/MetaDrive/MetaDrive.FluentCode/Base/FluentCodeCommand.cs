﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public class FluentCodeCommand<T> : IFluentCodeCommand where T : IFluentCodeElement
    {
        public T Element { get; private set; }

        public FluentCodeCommand(T element)
        {
            this.Element = element;
        }
   }
}
