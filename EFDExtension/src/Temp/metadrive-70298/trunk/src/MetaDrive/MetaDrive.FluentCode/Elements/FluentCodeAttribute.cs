﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeAttribute : FluentCodeElementBase<EnvDTE80.CodeAttribute2, FluentCodeAttribute.FluentCodeAttributeQuery, FluentCodeAttribute.FluentCodeAttributeCommand>
    {        
        public FluentCodeAttribute(EnvDTE80.CodeAttribute2 adaptee) : base(adaptee)
        {                        
        }
    }
}