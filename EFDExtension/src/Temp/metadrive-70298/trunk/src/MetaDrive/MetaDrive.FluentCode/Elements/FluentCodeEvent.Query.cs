﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeEvent
    {
        public sealed class FluentCodeEventQuery : FluentCodeQuery<FluentCodeEvent>
        {
            public FluentCodeEventQuery(FluentCodeEvent element) : base(element)
            {
            }
        }
    }
}
