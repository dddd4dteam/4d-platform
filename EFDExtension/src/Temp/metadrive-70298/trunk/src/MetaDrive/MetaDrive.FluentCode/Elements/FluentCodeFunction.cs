﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeFunction : FluentCodeElementBase<EnvDTE80.CodeFunction2, FluentCodeFunction.FluentCodeFunctionQuery, FluentCodeFunction.FluentCodeFunctionCommand>
    {        
        public FluentCodeFunction(EnvDTE80.CodeFunction2 adaptee) : base(adaptee)
        {
        }
    }
}
