﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeImport
    {
        public sealed class FluentCodeImportCommand : FluentCodeCommand<FluentCodeImport>
        {
            public FluentCodeImportCommand(FluentCodeImport element): base(element)
            {
            }            
        }
    }
}
