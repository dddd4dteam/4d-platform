﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeFunction
    {
        public sealed class FluentCodeFunctionQuery : FluentCodeQuery<FluentCodeFunction>
        {         
            public FluentCodeFunctionQuery(FluentCodeFunction element) : base(element)
            {
            }
        }
    }
}
