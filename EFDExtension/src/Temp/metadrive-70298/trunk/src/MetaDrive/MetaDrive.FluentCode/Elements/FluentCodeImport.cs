﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeImport : FluentCodeElementBase<EnvDTE80.CodeImport, FluentCodeImport.FluentCodeImportQuery, FluentCodeImport.FluentCodeImportCommand>
    {
        public FluentCodeImport(EnvDTE80.CodeImport adaptee) : base(adaptee)
        {
        }
    }
}
