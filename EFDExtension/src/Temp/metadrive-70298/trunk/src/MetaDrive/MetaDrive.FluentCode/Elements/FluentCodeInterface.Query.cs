﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeInterface
    {
        public sealed class FluentCodeInterfaceQuery : FluentCodeQuery<FluentCodeInterface>
        {
            public FluentCodeInterfaceQuery(FluentCodeInterface element) : base(element)
            {
            }
        }
    }
}
