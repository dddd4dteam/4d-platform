﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeFunction
    {
        public sealed class FluentCodeFunctionCommand : FluentCodeCommand<FluentCodeFunction>
        {
            public FluentCodeFunctionCommand(FluentCodeFunction element) : base(element)
            {
            }            
        }
    }
}
