﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeNamespace : FluentCodeElementBase<EnvDTE.CodeNamespace, FluentCodeNamespace.FluentCodeNamespaceQuery, FluentCodeNamespace.FluentCodeNamespaceCommand>
    {                
        public FluentCodeNamespace(EnvDTE.CodeNamespace adaptee) : base(adaptee)
        {
        }
    }
}
