﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeAttribute
    {
        public sealed class FluentCodeAttributeCommand : FluentCodeCommand<FluentCodeAttribute>
        {
            public FluentCodeAttributeCommand(FluentCodeAttribute element) : base(element)
            {
            }         
        }
    }
}