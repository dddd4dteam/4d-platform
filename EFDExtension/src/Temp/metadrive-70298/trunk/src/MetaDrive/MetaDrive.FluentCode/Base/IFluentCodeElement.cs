﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public interface IFluentCodeElement : IAdapter
    {
        string Name { get; set; }
        string FullName { get; }

        IFluentCodeQuery Query { get; }
        IFluentCodeCommand Command { get; }

        IFluentCodeEdit Edit { get; }
    }
}
