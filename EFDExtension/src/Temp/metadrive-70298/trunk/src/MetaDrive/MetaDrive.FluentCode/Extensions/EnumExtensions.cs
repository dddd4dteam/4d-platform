﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public static class EnumExtensions
    {
        public static string AsString(this vsCMAccess access)
        {
            switch (access)
            {
                case vsCMAccess.vsCMAccessAssemblyOrFamily: return "";
                case vsCMAccess.vsCMAccessDefault: return "";
                case vsCMAccess.vsCMAccessPrivate: return "private";
                case vsCMAccess.vsCMAccessProject: return "";
                case vsCMAccess.vsCMAccessProjectOrProtected: return "";
                case vsCMAccess.vsCMAccessProtected: return "protected";
                case vsCMAccess.vsCMAccessPublic: return "public";
                case vsCMAccess.vsCMAccessWithEvents: return "";
                default:
                    throw new Exception("AsString");
            }
        }

        public static string AsString(this vsCMOverrideKind kind)
        {
            switch (kind)
            {
                case vsCMOverrideKind.vsCMOverrideKindAbstract: return "abstract";
                case vsCMOverrideKind.vsCMOverrideKindNew: return "new";
                case vsCMOverrideKind.vsCMOverrideKindNone: return "";
                case vsCMOverrideKind.vsCMOverrideKindOverride: return "override";
                case vsCMOverrideKind.vsCMOverrideKindSealed: return "sealed";
                case vsCMOverrideKind.vsCMOverrideKindVirtual: return "virtual";
                default:
                    throw new Exception("AsString");
            }
        }
    }    
}
