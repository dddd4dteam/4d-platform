﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace MetaDrive.FluentCode
{
    //http://social.microsoft.com/Forums/en-US/vsx/thread/db990169-29ac-46e6-928b-b8b8dffc1e8e
    //https://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=107634&wa=wsignin1.0
    //

    public class FluentCodeVisitor<T>
    {
        #region .: PreVisit :.

        protected virtual bool PreVisit(FluentCodeNamespace element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeAttribute element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeFunction element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeProperty element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeParameter element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeImport element)
        {
            return true;
        }

        protected virtual bool PreVisit(FluentCodeClass element)
        {
            return true;
        }

        #endregion

        #region .: Visit :.

        protected virtual T Visit(FluentCodeNamespace element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeAttribute element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeFunction element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeProperty element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeParameter element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeImport element)
        {
            return default(T);
        }

        protected virtual T Visit(FluentCodeClass element)
        {
            return default(T);
        }

        #endregion

        #region .: PostVisit :.

        protected virtual void PostVisit(FluentCodeNamespace element)
        {
        }

        protected virtual void PostVisit(FluentCodeAttribute element)
        {
        }

        protected virtual void PostVisit(FluentCodeProperty element)
        {
        }

        protected virtual void PostVisit(FluentCodeParameter element)
        {
        }

        protected virtual void PostVisit(FluentCodeFunction element)
        {
        }

        protected virtual void PostVisit(FluentCodeImport element)
        {
        }

        protected virtual void PostVisit(FluentCodeClass element)
        {
        }

        #endregion

        public virtual T Visit(IFluentCodeElement element)
        {
            T result = default(T);

            bool visit = (bool)Invoke("PreVisit", element.GetType(), element);

            if (visit)
            {
                result = (T)Invoke("Visit", element.GetType(), element);

                foreach (var childElement in element.Query.Children)
                {
                    Visit(childElement);
                }
            }

            Invoke("PostVisit", element.GetType(), element);

            return result;
        }
      
        protected object Invoke(string methodName, Type parameterType, object parameter)
        {
            MethodInfo methodInfo = this.GetType().GetMethod(
                                        methodName,
                                        BindingFlags.NonPublic | BindingFlags.Instance,
                                        Type.DefaultBinder,
                                        new Type[] { parameterType },
                                        new ParameterModifier[0]);

            return methodInfo.Invoke(this, BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.InvokeMethod, null, new object[] { parameter }, null);
        }
    }
}
