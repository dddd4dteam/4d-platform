﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeProperty
    {
        public sealed class FluentCodePropertyQuery : FluentCodeQuery<FluentCodeProperty>
        {
            public IEnumerable<FluentCodeAttribute> Attributes
            {
                get
                {
                    var propertyPrototype = Element.Adaptee.Name;
                    
                    foreach (var codeClass in new CodeClassInheritanceIterator(Element.Parent.Adaptee))
                    {
                        if (codeClass.Children != null)
                        {
                            foreach (var element in new CodeElementIterator(codeClass.Children))
                            {
                                if (element.Kind == vsCMElement.vsCMElementProperty && element.Name == propertyPrototype)
                                {
                                    EnvDTE.CodeProperty property = element as EnvDTE.CodeProperty;

                                    if (property != null)
                                    {
                                        foreach (EnvDTE80.CodeAttribute2 attribute in property.Attributes)
                                            if (attribute != null)
                                                yield return new FluentCodeAttribute(attribute);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            public FluentCodePropertyQuery(FluentCodeProperty element) : base(element)
            {
            }
        }
    }
}
