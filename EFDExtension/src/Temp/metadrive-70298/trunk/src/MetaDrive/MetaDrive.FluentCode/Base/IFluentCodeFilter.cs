﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public interface IFluentCodeFilter
    {
        bool Filter(FluentCodeNamespace element);
        bool Filter(FluentCodeClass element);
        bool Filter(FluentCodeProperty element);
        bool Filter(FluentCodeFunction element);
    }   
}
