﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeNamespace
    {
        public sealed class FluentCodeNamespaceQuery : FluentCodeQuery<FluentCodeNamespace>
        {
            public FluentCodeNamespaceQuery(FluentCodeNamespace element) : base(element)
            {
            }
        }
    }
}
