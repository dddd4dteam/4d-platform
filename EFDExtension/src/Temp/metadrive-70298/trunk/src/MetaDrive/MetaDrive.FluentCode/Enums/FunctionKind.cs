﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public enum FunctionKind
    {        
        Constructor,     
        Destructor,        
        Function,        
        Sub,        
        PropertySet,        
        PropertyGet
    }
}
