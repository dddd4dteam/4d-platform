﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeParameter : FluentCodeElementBase<EnvDTE80.CodeParameter2, FluentCodeParameter.FluentCodeParameterQuery, FluentCodeParameter.FluentCodeParameterCommand>
    {        
        public FluentCodeParameter(EnvDTE80.CodeParameter2 adaptee) : base(adaptee)
        {                        
        }
    }
}