﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeClass
    {
        public sealed class FluentCodeClassQuery : FluentCodeQuery<FluentCodeClass>
        {
            public IEnumerable<FluentCodeInterface> ImplementedInterfaces
            {
                get
                {
                    foreach (var codeClass in new CodeClassInheritanceIterator(Element.Adaptee))
                        foreach (EnvDTE80.CodeInterface2 item in codeClass.ImplementedInterfaces)
                            yield return new FluentCodeInterface(item);
                }
            }
         
            public FluentCodeClassQuery(FluentCodeClass element) : base(element)
            {
            }
            /*
            public bool IsA(string fullNameInterface)
            {                                
                foreach (var codeClass in new CodeClassInheritanceIterator(Element.Adaptee))
                {
                    foreach (EnvDTE80.CodeInterface2 item in codeClass.ImplementedInterfaces)
                    {
                        if (string.Compare(item.FullName, fullNameInterface, true) == 0)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }
            */
        }
    }
}
