﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{  
    public class FluentCodeElementBase<TAdapter, TQuery, TCommand> : FluentCodeElementBase
        where TAdapter : class
        where TQuery : IFluentCodeQuery
        where TCommand : IFluentCodeCommand
    {        
        public override string Name
        {
            get { return ((EnvDTE.CodeElement)Adaptee).Name; }
            set { ((EnvDTE.CodeElement)Adaptee).Name = value; }
        }

        public override string FullName
        {
            get { return ((EnvDTE.CodeElement)Adaptee).FullName; }
        }

        public new TAdapter Adaptee
        {
            get { return (TAdapter)base.Adaptee; }
        }

        public new TQuery Query
        {
            get { return (TQuery)base.Query; }
        }

        public new TCommand Command
        {
            get { return (TCommand)base.Command; }
        }

        public FluentCodeElementBase(object adaptee) : base(adaptee, typeof(TQuery), typeof(TCommand))
        {
        }
    }
}