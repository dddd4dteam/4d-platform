﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public enum FluentCodeType
    {
        Namespace,
        Class,
        Function,
        Parameter,
        Event,
        Attribute,
        Property,        
    }
}
