﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.FluentCode
{
    public abstract class FluentCodeElementBase : IFluentCodeElement, IAdapter
    {
        IFluentCodeQuery query = null;
        IFluentCodeCommand command = null;
        IFluentCodeEdit edit = null;

        Type queryType = null;
        Type commandType = null;

        public object Adaptee { get; private set; }

        public IFluentCodeQuery Query 
        {
            get
            {
                if (query == null)
                    query = (IFluentCodeQuery)Activator.CreateInstance(queryType, this);
                return query;
            }
        }

        public IFluentCodeCommand Command 
        {
            get
            {
                if (command == null)
                    command = (IFluentCodeCommand)Activator.CreateInstance(commandType, this);
                return command;
            }
        }

        public IFluentCodeEdit Edit
        {
            get
            {
                if (edit == null)
                    edit = new FluentCodeEdit(this);
                return edit;
            }
        }

        public abstract string Name { get; set; }
        public abstract string FullName { get; }
                
        public FluentCodeElementBase(object adaptee, Type queryType, Type commandType)
        {
            this.Adaptee = adaptee;
            this.queryType = queryType;
            this.commandType = commandType;            
        }
    }
}