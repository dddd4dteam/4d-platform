﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.FluentCode
{
    public partial class FluentCodeParameter
    {
        public sealed class FluentCodeParameterCommand : FluentCodeCommand<FluentCodeParameter>
        {
            public FluentCodeParameterCommand(FluentCodeParameter element) : base(element)
            {
            }       
        }
    }
}