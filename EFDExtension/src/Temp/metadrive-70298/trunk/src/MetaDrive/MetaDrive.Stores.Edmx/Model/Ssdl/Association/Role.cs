﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public class Role : EdmElement
    {
        public EntityType Type { get; set; }
        public Cardinality Cardinality { get; set; }
        public EventedObservableCollection<Property> Properties { get; set; }

        public Role()
        {
            Properties = new EventedObservableCollection<Property>();
        }
    }
}
