﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public enum ConditionOperator
    {
        Equals,
        IsNull,
        IsNotNull
    }
}
