﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    public enum PropertyType
    {
        Binary,
        Boolean,
        Byte,
        DateTime,
        DateTimeOffset,
        Time,
        Decimal,
        Double,
        Single,
        Guid,
        Int16,
        Int32,
        Int64,
        String,
        SByte
    }
}
