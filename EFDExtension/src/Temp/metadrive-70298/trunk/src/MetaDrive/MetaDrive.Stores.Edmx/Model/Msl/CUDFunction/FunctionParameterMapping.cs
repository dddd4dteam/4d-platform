﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public class FunctionParameterMapping
    {
        public Ssdl.FunctionParameter SSDLFunctionParameter { get; set; }
        public FunctionParameterVersion? Version { get; set; }
    }
}
