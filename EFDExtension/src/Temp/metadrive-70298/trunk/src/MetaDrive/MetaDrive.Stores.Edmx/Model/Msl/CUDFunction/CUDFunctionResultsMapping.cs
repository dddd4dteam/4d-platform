﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using System.Collections.Generic;
    using MetaDrive.Stores.Edmx.Model.Csdl;
    
    public class CUDFunctionResultsMapping : IEnumerable<KeyValuePair<ScalarProperty, string>>
    {
        private Dictionary<ScalarProperty, string> _resultsmapping;
        private Dictionary<ScalarProperty, string> ResultsMapping
        {
            get
            {
                if (_resultsmapping == null)
                    _resultsmapping = new Dictionary<ScalarProperty, string>();
                return _resultsmapping;
            }
        }
        public string this[ScalarProperty scalarProperty]
        {
            get
            {
                if (ResultsMapping.ContainsKey(scalarProperty))
                    return ResultsMapping[scalarProperty];
                return null;
            }
            set
            {
                if (value == null)
                    ResultsMapping.Remove(scalarProperty);
                else if (ResultsMapping.ContainsKey(scalarProperty))
                    ResultsMapping[scalarProperty] = value;
                else
                    ResultsMapping.Add(scalarProperty, value);
            }
        }

        public IEnumerator<KeyValuePair<ScalarProperty, string>> GetEnumerator()
        {
            return ResultsMapping.GetEnumerator();
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
