﻿using System.Collections.Generic;

namespace System.Linq
{
    public static class EnumerableEx
    {
        #region .: ForEach :.

        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource> onNext)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (onNext == null)
                throw new ArgumentNullException("onNext");

            foreach (TSource current in source)
                onNext(current);
        }

        public static void ForEach<TSource>(this IEnumerable<TSource> source, Action<TSource, int> onNext)
        {
            if (source == null)
                throw new ArgumentNullException("source");

            if (onNext == null)
                throw new ArgumentNullException("onNext");

            int num = 0;
            foreach (TSource current in source)
                onNext(current, num++);
        }

        #endregion
    }
}
