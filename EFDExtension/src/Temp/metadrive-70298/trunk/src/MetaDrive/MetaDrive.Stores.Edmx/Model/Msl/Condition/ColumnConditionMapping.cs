﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public class ColumnConditionMapping : ConditionMapping
    {
        private Ssdl.Property _column;

        public Ssdl.Property Column
        {
            get { return _column; }
            set
            {
                _column = value;
                if (value != null)
                    Table = value.EntityType;
            }
        }

        internal bool Generated { get; set; }
    }
}
