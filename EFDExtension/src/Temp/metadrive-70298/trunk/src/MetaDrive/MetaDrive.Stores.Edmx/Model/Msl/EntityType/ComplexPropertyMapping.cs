﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using MetaDrive.Stores.Edmx.Model.Csdl;
    
    public class ComplexPropertyMapping : MappingBase
    {
        public ComplexPropertyMapping(Csdl.EntityType entityType, ComplexProperty complexProperty)
            : base(entityType)
        {
            ComplexProperty = complexProperty;
        }

        public ComplexProperty ComplexProperty { get; private set; }

        protected override MappingBase BaseMapping
        {
            get
            {
                if (EntityType.ComplexProperties.Contains(ComplexProperty))
                    return null;
                return EntityType.BaseType.Mapping;
            }
        }
    }
}
