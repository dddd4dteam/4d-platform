﻿namespace MetaDrive.Stores.Edmx.Model
{
    public interface IEdmElement
    {
        string Name { get; set; }
    }
}