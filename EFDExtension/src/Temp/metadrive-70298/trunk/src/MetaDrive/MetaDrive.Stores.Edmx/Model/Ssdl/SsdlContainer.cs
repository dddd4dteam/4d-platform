﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public class SsdlContainer : EdmElement
    {        
        private EventedObservableCollection<EntityType> _entityTypes;
        private EventedObservableCollection<Association> _associationSets;
        private EventedObservableCollection<Function> _functions;
        
        public string Namespace { get; set; }
        public string Provider { get; set; }
        public string ProviderManifestToken { get; set; }

        public EventedObservableCollection<EntityType> EntityTypes
        {
            get
            {
                if (_entityTypes == null)
                {
                    _entityTypes = new EventedObservableCollection<EntityType>();
                    _entityTypes.ItemAdded += entityType => entityType.Container = this;
                }

                return _entityTypes;
            }
        }

        public EventedObservableCollection<Association> AssociationSets
        {
            get
            {
                if (_associationSets == null)
                {
                    _associationSets = new EventedObservableCollection<Association>();
                    _associationSets.ItemAdded += association => association.Container = this;
                }

                return _associationSets;
            }
        }

        public EventedObservableCollection<Function> Functions
        {
            get
            {
                if (_functions == null)
                {
                    _functions = new EventedObservableCollection<Function>();
                }

                return _functions;
            }
        }        
    }
}
