﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public class Association : EdmElement
    {
        public string AssociationSetName { get; set; }
        public Role Role1 { get; set; }
        public Role Role2 { get; set; }
        public Role PrincipalRole { get; set; }
        public Role DependantRole { get; set; }

        public SsdlContainer Container { get; internal set; }
    }
}
