﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public class FunctionParameter : EdmElement
    {        
        private string _type;
        private ParameterMode _mode;
        private int? _maxLength;
        private int? _precision;
        private int? _scale;

        public string Type
        {
            get { return _type; }
            set
            {
                _type = value;
                OnPropertyChanged("Type");
            }
        }

        public ParameterMode Mode
        {
            get { return _mode; }
            set
            {
                _mode = value;
                OnPropertyChanged("Mode");
            }
        }

        public int? MaxLength
        {
            get { return _maxLength; }
            set
            {
                _maxLength = value;
                OnPropertyChanged("MaxLength");
            }
        }

        public int? Precision
        {
            get { return _precision; }
            set
            {
                _precision = value;
                OnPropertyChanged("Precision");
            }
        }

        public int? Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                OnPropertyChanged("Scale");
            }
        }

        public string StoreName { get; set; }
        public string StoreSchema { get; set; }
        public string StoreType { get; set; }        
    }
}
