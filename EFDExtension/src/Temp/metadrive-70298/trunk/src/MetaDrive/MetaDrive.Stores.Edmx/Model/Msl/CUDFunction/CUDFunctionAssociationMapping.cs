﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public class CUDFunctionAssociationMapping
    {
        public Csdl.Association Association { get; set; }
        public string FromRole { get; set; }
        public string ToRole { get; set; }

        private CUDFunctionParametersMapping _associationPropertiesMapping;
        public CUDFunctionParametersMapping AssociationPropertiesMapping
        {
            get
            {
                if (_associationPropertiesMapping == null)
                    _associationPropertiesMapping = new CUDFunctionParametersMapping();
                return _associationPropertiesMapping;
            }
        }
    }
}
