﻿namespace MetaDrive.Stores.Edmx.Model
{
    public enum ReferentialConstraint
    {
        Principal,
        Dependant
    }
}
