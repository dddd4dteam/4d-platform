﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public enum FunctionParameterVersion
    {
        Current,
        Original
    }
}
