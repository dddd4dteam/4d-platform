﻿namespace MetaDrive.Stores.Edmx.Model
{
    public enum ParameterMode
    {
        In,
        InOut,
        Out
    }
}
