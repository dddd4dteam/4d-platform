﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    using System.Collections.Generic;

    public class EntityType : EdmElement
    {        
        private string _entitySetName;
        private string _definingQuery;
        private List<Property> _properties;

        public SsdlContainer Container { get; internal set; }

        public StoreType? StoreType { get; set; }
        public string Schema { get; set; }
        public string StoreName { get; set; }
        public string StoreSchema { get; set; }
        public string Table { get; set; }

        public string EntitySetName
        {
            get { return _entitySetName; }
            set
            {
                _entitySetName = value;
                OnPropertyChanged("EntitySetName");
            }
        }

        public string DefiningQuery
        {
            get 
            {
                if (StoreType == null || StoreType == Ssdl.StoreType.Tables)
                    return _definingQuery;
                
                if (string.IsNullOrEmpty(_definingQuery))
                {
                    string definingQuery = string.Empty;

                    for (int i = 0; i < _properties.Count; i++)
                    {
                        if (string.IsNullOrEmpty(definingQuery))
                            definingQuery += "SELECT \r";

                        definingQuery += string.Format("[{0}].[{1}] AS [{1}]", _entitySetName, _properties[i].Name);

                        if (i < _properties.Count - 1)
                        {
                            definingQuery += ", \r";
                        }
                        else
                        {
                            definingQuery += string.Format(" \rFROM [{0}].[{1}] AS [{1}]", Schema, _entitySetName);
                        }
                    }

                    return definingQuery;
                }

                return _definingQuery; 
            }
            set
            {
                _definingQuery = value;
                OnPropertyChanged("DefiningQuery");
            }
        }

        public List<Property> Properties
        {
            get
            {
                if (_properties == null)
                    _properties = new List<Property>();
                return _properties;
            }
        }
    }
}
