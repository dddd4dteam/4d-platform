﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using System.Collections.Generic;
    using MetaDrive.Stores.Edmx.Model.Csdl;

    public class EntityTypeCUDFunctionParametersMapping : CUDFunctionParametersMapping
    {
        #region Fields

        private EventedObservableCollection<KeyValuePair<ComplexProperty, EntityTypeCUDFunctionParametersMapping>> _complexPropertiesMapping;

        #endregion

        #region Properties

        public EventedObservableCollection<KeyValuePair<ComplexProperty, EntityTypeCUDFunctionParametersMapping>> ComplexPropertiesMapping
        {
            get
            {
                if (_complexPropertiesMapping == null)
                    _complexPropertiesMapping = new EventedObservableCollection<KeyValuePair<ComplexProperty, EntityTypeCUDFunctionParametersMapping>>();
                return _complexPropertiesMapping;
            }
        }

        #endregion
    }
}
