﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using MetaDrive.Stores.Edmx.Model.Csdl;
    using MetaDrive.Stores.Edmx.Model.Ssdl;
    
    public class PropertyMapping
    {
        public ScalarProperty Property { get; internal set; }
        public Property Column { get; internal set; }
    }
}
