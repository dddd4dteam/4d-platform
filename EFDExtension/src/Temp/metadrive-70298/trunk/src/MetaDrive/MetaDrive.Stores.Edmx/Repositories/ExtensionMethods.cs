﻿namespace MetaDrive.Stores.Edmx.Repositories
{
    using System.Xml.Linq;

    public static class XElements
    {
        private static XAttribute GetAttribute(XNamespace xmlns, string attributeName, object value)
        {
            if (value == null)
                return null;

            if (xmlns == null)
                return new XAttribute(attributeName, value);
            else
                return new XAttribute(xmlns + attributeName, value);
        }

        public static XElement AddAttribute(this XElement element, XAttribute attribute)
        {
            if (attribute != null)
                element.Add(attribute);
            
            return element;
        }

        public static XElement AddAttribute(this XElement element, XNamespace xmlns, string attributeName, object value)
        {
            return AddAttribute(element, GetAttribute(xmlns, attributeName, value));
        }

        public static XElement AddAttribute(this XElement element, string attributeName, object value)
        {
            return AddAttribute(element, GetAttribute(null, attributeName, value));
        }

        public static XElement AddElement(this XElement parentElement, XElement childElement)
        {
            if (childElement != null)
                parentElement.Add(childElement);

            return parentElement;
        }
    }
}
