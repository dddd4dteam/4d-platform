﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public abstract class ConditionMapping
    {
        private ConditionOperator _operator;
        public ConditionOperator Operator
        {
            get { return _operator; }
            set
            {
                if (value != ConditionOperator.Equals)
                    _value = null;
                _operator = value;
            }
        }
        
        private string _value;

        public string Value
        {
            get { return _value; }
            set
            {
                if (Operator != ConditionOperator.Equals)
                    return;
                _value = value;
            }
        }

        public Ssdl.EntityType Table { get; set; }
    }
}
