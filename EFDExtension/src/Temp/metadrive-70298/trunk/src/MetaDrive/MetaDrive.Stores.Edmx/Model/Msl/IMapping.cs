﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using System.Collections.Generic;
    using MetaDrive.Stores.Edmx.Model.Csdl;

    public interface IMapping
    {
        IEnumerable<PropertyMapping> GetSpecificMappingForTable(Ssdl.EntityType table);
        void AddMapping(ScalarProperty property, Ssdl.Property column);
        void ChangeMapping(ScalarProperty property, Ssdl.Property column);
        void RemoveMapping(ScalarProperty property, Ssdl.EntityType table);
    }
}
