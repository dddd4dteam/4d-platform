﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public enum StoreGeneratedPattern
    {
        None,
        Identity,
        Computed
    }
}
