﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    using System;

    public class DisplayEnabledConditionAttribute : Attribute
    {
        public DisplayEnabledConditionAttribute(string conditionPropertyName)
        {
            ConditionPropertyName = conditionPropertyName;
        }

        public string ConditionPropertyName { get; private set; }
    }
}
