﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    using System;

    public class DisplayVisibleConditionAttribute : Attribute
    {
        public string ConditionPropertyName { get; private set; }

        public DisplayVisibleConditionAttribute(string conditionPropertyName)
        {
            ConditionPropertyName = conditionPropertyName;
        }        
    }
}
