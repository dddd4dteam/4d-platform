﻿namespace MetaDrive.Stores.Edmx.Model
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Collections.Specialized;

    public class EventedObservableCollection<T> : ObservableCollection<T>
    {     
        public event Action<T> ItemAdded;
        public event Action<T> ItemRemoved;
     
        public EventedObservableCollection()
        {
        }

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnCollectionChanged(e);
            if (e.NewItems != null)
                foreach (T newItem in e.NewItems)
                    OnItemAdded(newItem);
            if (e.OldItems != null)
                foreach (T oldItem in e.OldItems)
                    OnItemRemoved(oldItem);
        }

        protected virtual void OnItemAdded(T item)
        {
            if (ItemAdded != null)
                ItemAdded(item);
        }

        protected virtual void OnItemRemoved(T item)
        {
            if (ItemRemoved != null)
                ItemRemoved(item);
        }        
    }

    public static class EventedObservableCollection
    {        
        public static EventedObservableCollection<T> ToEventedObservableCollection<T>(this IEnumerable<T> source)
        {
            EventedObservableCollection<T> dest = new EventedObservableCollection<T>();

            foreach (T item in source)
                dest.Add(item);

            return dest;
        }     
    }
}
