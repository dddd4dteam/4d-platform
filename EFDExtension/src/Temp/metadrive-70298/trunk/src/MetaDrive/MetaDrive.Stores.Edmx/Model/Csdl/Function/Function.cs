﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    public class Function : EdmElement
    {
        private Visibility _visibility;

        public Visibility Visibility
        {
            get { return _visibility; }
            set
            {
                _visibility = value;
                OnPropertyChanged("Visibility");
            }
        }

        private EntityType _entityType;
        public EntityType EntityType
        {
            get { return _entityType; }
            set
            {
                _entityType = value;
                OnPropertyChanged("EntityType");
            }
        }

        private PropertyType? _scalarReturnType;
        public PropertyType? ScalarReturnType
        {
            get { return _scalarReturnType; }
            set
            {
                _scalarReturnType = value;
                OnPropertyChanged("ScalarReturnType");
            }
        }

        public string ReturnType
        {
            get
            {
                if (EntityType == null)
                {
                    if (ScalarReturnType == null)
                        return null;
                    return string.Format("Collection({0})", ScalarReturnType.Value.ToString());
                }
                return string.Format("Collection({0}.{1})", EntityType.Container.Namespace, EntityType.Name);
            }
        }

        private EventedObservableCollection<FunctionParameter> _parameters;
        public EventedObservableCollection<FunctionParameter> Parameters
        {
            get
            {
                if (_parameters == null)
                    _parameters = new EventedObservableCollection<FunctionParameter>();
                return _parameters;
            }
        }

        public Ssdl.Function SSDLFunction { get; set; }
    }
}
