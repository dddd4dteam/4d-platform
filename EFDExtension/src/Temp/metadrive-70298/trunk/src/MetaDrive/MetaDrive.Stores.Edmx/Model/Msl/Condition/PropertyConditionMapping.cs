﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    using System;

    public class PropertyConditionMapping : ConditionMapping
    {
        private Csdl.ScalarProperty _csdlProperty;
        public Csdl.ScalarProperty CSDLProperty
        {
            get { return _csdlProperty; }
            set
            {
                _csdlProperty = value;
                OnPropertyChanged();
            }
        }

        protected virtual void OnPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged();
        }
        public event Action PropertyChanged;
    }
}
