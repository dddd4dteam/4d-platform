﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    using System;
    using System.Collections;
    using System.Linq;

    public class ComplexProperty : PropertyBase
    {        
        private ComplexType _complexType;

        public ComplexProperty(ComplexType complexType)
        {
            ComplexType = complexType;
        }

        internal ComplexProperty(string complexTypeName)
        {
            ComplexTypeName = complexTypeName;
        }

        protected override Func<TypeBase, IList> GetPropertyCollection
        {
            get { return entityType => entityType.ComplexProperties; }
        }

        public ComplexType ComplexType
        {
            get
            {
                if (_complexType == null)
                    _complexType = EntityType.Container.ComplexTypes.FirstOrDefault(ct => ct.Name == ComplexTypeName);
                return _complexType;
            }
            private set { _complexType = value; }
        }

        internal string ComplexTypeName { get; private set; }
        
        protected override PropertyBase Create()
        {
            return new ComplexProperty(ComplexType);
        }
    }
}
