﻿namespace MetaDrive.Stores.Edmx.Model
{
	using System.Collections.Generic;
	using System.Xml.Linq;
	using MetaDrive.Stores.Edmx.Model.Csdl;
	using MetaDrive.Stores.Edmx.Model.Ssdl;

	public class EdmContainer
	{
		public SsdlContainer Ssdl { get; set; }
		public CsdlContainer Csdl { get; set; }

		public bool IsEmpty 
		{
			get 
			{
				if (Ssdl == null || Csdl == null)
					return true;
				else
					return false;
			}
		}

		//public IEnumerable<DesignerProperty> DesignerProperties { get; internal set; }
		//public IEnumerable<DesignerProperty> EDMXDesignerDesignerProperties { get; internal set; }
		public IEnumerable<XElement> EDMXDesignerDiagrams { get; internal set; }
	}
}
