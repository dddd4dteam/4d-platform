﻿namespace MetaDrive.Stores.Edmx.Model.Msl
{
    public class CUDFunctionMapping
    {
        public Ssdl.Function SSDLFunction { get; set; }

        private EntityTypeCUDFunctionParametersMapping _parametersMapping;
        public EntityTypeCUDFunctionParametersMapping ParametersMapping
        {
            get
            {
                if (_parametersMapping == null)
                    _parametersMapping = new EntityTypeCUDFunctionParametersMapping();
                return _parametersMapping;
            }
        }

        private CUDFunctionResultsMapping _resultsMapping;
        public CUDFunctionResultsMapping ResultsMapping
        {
            get
            {
                if (_resultsMapping == null)
                    _resultsMapping = new CUDFunctionResultsMapping();
                return _resultsMapping;
            }
        }

        private EventedObservableCollection<CUDFunctionAssociationMapping> _associationMappings;
        public EventedObservableCollection<CUDFunctionAssociationMapping> AssociationMappings
        {
            get
            {
                if (_associationMappings == null)
                    _associationMappings = new EventedObservableCollection<CUDFunctionAssociationMapping>();
                return _associationMappings;
            }
        }
    }
}
