﻿namespace MetaDrive.Stores.Edmx.Model
{
    using System.Collections.Generic;
    using System.Linq;

    public static class Enumerables
    {
        public static T GetByName<T>(this IEnumerable<T> source, string name) where T : IEdmElement
        {
            return source.FirstOrDefault(item => item.Name == name);
        }
    }
}
