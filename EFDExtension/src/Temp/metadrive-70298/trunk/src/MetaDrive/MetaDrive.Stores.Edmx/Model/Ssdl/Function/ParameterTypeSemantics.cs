﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public enum ParameterTypeSemantics
    {
        ExactMatchOnly,
        AllowImplicitPromotion,
        AllowImplicitConversion
    }
}
