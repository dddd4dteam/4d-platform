﻿namespace MetaDrive.Stores.Edmx.Model.Ssdl
{
    public enum StoreType
    {
        Tables,
        Views
    }
}
