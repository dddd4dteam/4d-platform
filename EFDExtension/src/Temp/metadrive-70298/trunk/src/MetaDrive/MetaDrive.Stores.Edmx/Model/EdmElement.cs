﻿namespace MetaDrive.Stores.Edmx.Model
{
    using System.ComponentModel;

    public abstract class EdmElement : IEdmElement, INotifyPropertyChanged
    {        
        protected string _name = string.Empty;

        public virtual string Name
        {
            get { return _name; }
            set 
            {
                _name = value;
                OnPropertyChanged("Name");
            }
        }

        public IEdmElement This
        {
            get { return this; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }            
        }

        public override string ToString()
        {
            return _name;
        }        
    }
}
