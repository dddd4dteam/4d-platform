﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    using System;
    using System.Collections;
    using System.ComponentModel;

    public abstract class PropertyBase : EdmElement
    {        
        private TypeBase _entityType;
        private Visibility _getVisibility;
     
        public TypeBase EntityType
        {
            get { return _entityType; }
            internal set
            {
                if (_entityType == value)
                    return;
                if (_entityType != null)
                    GetPropertyCollection(_entityType).Remove(this);
                _entityType = value;
            }
        }

        internal void AddToType(TypeBase type)
        {
            if (type != null)
            {
                EntityType = type;
                GetPropertyCollection(type).Add(this);
            }
        }

        protected abstract Func<TypeBase, IList> GetPropertyCollection { get; }

        [DisplayName("Getter")]
        public Visibility GetVisibility
        {
            get { return _getVisibility; }
            set
            {
                _getVisibility = value;
                OnPropertyChanged("GetVisibility");
            }
        }
        
        internal virtual PropertyBase Duplicate()
        {
            var value = Create();
            value.Name = Name;
            value.GetVisibility = GetVisibility;
            return value;
        }

        protected abstract PropertyBase Create();
    }
}
