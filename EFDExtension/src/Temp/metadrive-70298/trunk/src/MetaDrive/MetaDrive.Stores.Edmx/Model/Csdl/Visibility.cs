﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    public enum Visibility
    {
        Public,
        Protected,
        Internal,
        Private
    }
}
