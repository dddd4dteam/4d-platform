﻿namespace MetaDrive.Stores.Edmx.Model.Csdl
{
    public class FunctionParameter : EdmElement
    {
        public PropertyType Type { get; set; }
        public ParameterMode? Mode { get; set; }
        public int? Precision { get; set; }
        public int? Scale { get; set; }
        public int? MaxLength { get; set; }
    }
}
