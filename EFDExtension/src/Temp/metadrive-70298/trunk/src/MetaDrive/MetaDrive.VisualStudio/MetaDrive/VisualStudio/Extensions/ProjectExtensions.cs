﻿namespace MetaDrive.VisualStudio
{
    using System;
    using System.IO;
    using EnvDTE;

    public static class ProjectExtensions
    {
        public static bool IsWebProject(this Project project)
        {
            return (project.Kind == "{E24C65DC-7377-472b-9ABA-BC803B73C61A}");
        }

        public static string GetDisplayName(this Project project)
        {
            string projectDisplayName = project.Name;
            if (string.IsNullOrEmpty(projectDisplayName))
                projectDisplayName = Path.GetFileNameWithoutExtension(GetFilename(project));

            return projectDisplayName;
        }

        public static string GetFilename(this Project project)
        {
            try
            {
                string projectName = project.FullName;

                if (string.IsNullOrEmpty(projectName))
                {
                    string solutionFileName = project.DTE.Solution.GetFileName();
                    string folder = string.Empty;
                    Project folderProject = project;

                    while (folderProject != null)
                    {
                        folder = Path.Combine(folderProject.Name, folder);

                        if (folderProject.ParentProjectItem == null)
                            folderProject = null;
                        else folderProject = folderProject.ParentProjectItem.ContainingProject;
                    }

                    if (string.IsNullOrEmpty(solutionFileName))
                        projectName = folder;
                    else projectName = Path.Combine(Path.GetDirectoryName(solutionFileName), folder);
                }

                return projectName;
            }
            catch (NotImplementedException)
            {
                return @"PATH:\NOT\AVAILABLE";
            }
        }
    }
}