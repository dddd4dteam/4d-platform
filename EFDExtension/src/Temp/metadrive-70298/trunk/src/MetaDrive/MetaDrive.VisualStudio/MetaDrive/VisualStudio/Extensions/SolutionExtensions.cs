﻿namespace MetaDrive.VisualStudio
{
    using System.Collections.Generic;
    using System.Linq;
    using EnvDTE;
    using MetaDrive.VisualStudio.Linq;
    using Microsoft.VisualStudio.Shell.Interop;
    
    public static class SolutionExtensions
    {
        public static string GetFileName(this Solution solution)
        {
            string solutionFileName = null;
            if (solution != null)
                solutionFileName = solution.FullName;

            return solutionFileName;
        }

        public static bool Build(this Solution solution)
        {
            bool suppressUI = false;
            IVsSolutionBuildManager2 service = solution.DTE.GetService<SVsSolutionBuildManager>() as IVsSolutionBuildManager2;

            return (service.StartSimpleUpdateSolutionConfiguration(
                    (int)VSSOLNBUILDUPDATEFLAGS.SBF_OPERATION_BUILD,
                    (int)VSSOLNBUILDQUERYRESULTS.VSSBQR_SAVEBEFOREBUILD_QUERY_YES,
                    suppressUI ? 1 : 0)
                != 0);
        }

        public static IEnumerable<Project> GetProjects(this Solution solution)
        {
            var projectIterator = new ProjectIterator(solution);
            return projectIterator.AsEnumerable();
        }
    }
}