﻿namespace MetaDrive.VisualStudio.Services
{    
    public static class BuildAction
    {        
        public const string None = "None";
        public const string Compile = "Compile";
        public const string Content = "Content";
        public const string EmbeddedResource = "EmbeddedResource";
    }
}
