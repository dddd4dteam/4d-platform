﻿namespace MetaDrive.VisualStudio
{
    public class DteSugar
    {
        public static EnvDTE.DTE Dte { get; private set; }

        public static void SetDTE(EnvDTE.DTE dte)
        {
            Dte = dte;
        }

        public static string GetProjectDirectory(string projectName)
        {
            return System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Dte.Solution.FullName), projectName);
        }
    }
}
