﻿namespace MetaDrive.VisualStudio
{
    using System;
    using Microsoft.VisualStudio.Shell;

    public static class DTEExtensions
    {
        public static T GetService<T>(this EnvDTE.DTE dte) where T : class
        {
            return dte.GetServiceProvider().GetService<T>();
        }

        public static IServiceProvider GetServiceProvider(this EnvDTE.DTE dte)
        {
            return new ServiceProvider(dte as Microsoft.VisualStudio.OLE.Interop.IServiceProvider);
        }
    }
}