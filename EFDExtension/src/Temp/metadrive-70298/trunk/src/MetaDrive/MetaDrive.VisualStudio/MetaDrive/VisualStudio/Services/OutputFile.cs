﻿namespace MetaDrive.VisualStudio.Services
{
    using System;

    [Serializable]
    public class OutputFile : OutputInfo
    {
        public string Content { get; set; }    

        //private readonly StringBuilder content = new StringBuilder();
        
        //public StringBuilder Content
        //{
        //    get { return this.content; }
        //}

        //public string ContentAsString
        //{
        //    get
        //    {
        //        return Content.ToString();
        //    }
        //    set
        //    {
        //        Content.Clear();
        //        Content.Append(value);
        //    }
        //}
    }
}
