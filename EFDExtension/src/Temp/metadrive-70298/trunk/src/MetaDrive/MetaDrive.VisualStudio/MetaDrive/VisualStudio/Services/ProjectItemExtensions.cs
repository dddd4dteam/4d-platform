﻿namespace MetaDrive.VisualStudio.Services
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using EnvDTE;    
    using VSLangProj;

    internal static class ProjectItemExtensions
    {
        private static Dictionary<string, Microsoft.Build.Evaluation.Project> dict = new Dictionary<string, Microsoft.Build.Evaluation.Project>();

        internal static IEnumerable<string> GetAvailableBuildActions(this ProjectItem projectItem)
        {
            var buildActions = new List<string> { "None", "Compile", "Content", "EmbeddedResource" };

            string key = projectItem.ContainingProject.GetFilename();
            Microsoft.Build.Evaluation.Project proj = null;

            if (dict.ContainsKey(key))
            {
                proj = dict[key];
            }
            else
            {
                proj = new Microsoft.Build.Evaluation.Project(key);
                dict.Add(key, proj);
            }

            buildActions.AddRange(
                proj.GetItems("AvailableItemName")
                .Select(x => x.EvaluatedInclude));

            return buildActions.Distinct();
        }


        public static void SetProperties(this ProjectItem projectItem, OutputFile output)
        {
            if (!string.IsNullOrEmpty(output.BuildAction))
            {
                var buildActions = projectItem.GetAvailableBuildActions();
                if (!buildActions.Contains(output.BuildAction))
                {
                    throw new Exception(
                        string.Format(CultureInfo.CurrentCulture, "Build Action {0} is not supported for {1}", output.BuildAction, projectItem.Name));
                }

                projectItem.SetPropertyValue("ItemType", output.BuildAction);
            }

            if (output.CopyToOutputDirectory != default(CopyToOutputDirectory))
            {
                projectItem.SetPropertyValue("CopyToOutputDirectory", (int)output.CopyToOutputDirectory);
            }

            if (!string.IsNullOrEmpty(output.CustomTool))
            {
                projectItem.SetPropertyValue("CustomTool", output.CustomTool);
            }

            if (!string.IsNullOrEmpty(output.CustomToolNamespace))
            {
                projectItem.SetPropertyValue("CustomToolNamespace", output.CustomToolNamespace);
            }
        }

        public static void AddReferences(this ProjectItem projectItem, OutputFile output)
        {
            if (output.References.Count > 0)
            {
                VSProject project = projectItem.ContainingProject.Object as VSProject;
                if (project == null)
                {
                    throw new Exception(
                        string.Format(
                            CultureInfo.CurrentCulture,
                            "Project {0} does not support assembly references required by {1}",
                            projectItem.ContainingProject.Name,
                            projectItem.Name));
                }

                foreach (string reference in output.References)
                    project.References.Add(reference);
            }
        }
    }
}
