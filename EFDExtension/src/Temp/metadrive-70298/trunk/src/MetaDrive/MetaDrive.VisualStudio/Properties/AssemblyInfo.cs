﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MetaDrive.VisualStudio")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("MetaDrive")]
[assembly: AssemblyProduct("MetaDrive.VisualStudio")]
[assembly: AssemblyCopyright("Copyright © Christoph De Baene")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8dd7e6a3-b709-445b-916e-b9cca26e0923")]

[assembly: AssemblyVersion("0.8.0.0")]
[assembly: AssemblyFileVersion("0.8.0.0")]
