﻿namespace MetaDrive.VisualStudio.Services
{    
    public enum CopyToOutputDirectory
    {        
        DoNotCopy = 0,
        CopyAlways = 1,
        CopyIfNewer = 2,
    }
}
