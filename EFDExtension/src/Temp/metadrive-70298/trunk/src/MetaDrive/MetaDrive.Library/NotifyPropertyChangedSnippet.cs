﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Core;
using MetaDrive.FluentCode;
using EnvDTE;
using EnvDTE80;

namespace MetaDrive.Library
{    
    [SnippetInfo("NotifyPropertyChanged")]
    public class NotifyPropertyChangedSnippet : Snippet
    {
        protected override bool PreVisit(FluentCodeProperty element)
        {
            return element.Query.Attributes.Any(x => x.FullName == "MetaDrive.MetaModel.NotifyPropertyChangedAttribute");
        }

        protected override void Visit(FluentCodeProperty code)
        {                        
            code.Setter.Edit.Body = string.Format(
                "if (value != this.{0})" +
                "{{" +
                "{1}" +
                "OnPropertyChanged(\"{0}\");" + 
                "}}",
                code.Name,
                code.Setter.Edit.Body);
        }
    }
}