﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.Core;
using MetaDrive.FluentCode;

namespace MetaDrive.Library
{
    [SnippetInfo("MarkVirtual")]
    public class MarkVirtualSnippet : Snippet
    {
        public IEnumerable<string> AttributeNames { get; private set; }

        public MarkVirtualSnippet(IEnumerable<Type> attributeTypes)
        {
            var attributeNames = new List<string>();
                        
            foreach (var attribute in attributeTypes.Where(x => x.IsA<System.Attribute>()))
                attributeNames.Add(attribute.FullName);
            
            AttributeNames = attributeNames.AsEnumerable();
        }

        public MarkVirtualSnippet(IEnumerable<string> attributeNames)
        {
            this.AttributeNames = attributeNames;
        }

        protected override void Visit(FluentCodeClass element)
        {
            element.Adaptee.ClassKind = EnvDTE80.vsCMClassKind.vsCMClassKindPartialClass;
            base.Visit(element);
        }

        protected override void Visit(FluentCodeProperty element)
        {
            /*
            if (element.Query.Attributes.Any(x => AttributeNames.Contains(x.FullName)))
                element.OverrideKind(EnvDTE80.vsCMOverrideKind.vsCMOverrideKindVirtual);
            */

 	        base.Visit(element);
        }
    }
}
