﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MetaDrive.Linq.Edmx.EDMObjects;
using MetaDrive.Linq.Edmx.IO;
using System.Xml.Linq;

namespace MetaDrive.Linq.Edmx.Test
{
    [TestClass]
    public class SampleTest
    {
        public TestContext TestContext { get; set; }

        public static EDM edm;

        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {
            edm = EDMXIO.Read(Path.Combine(testContext.DeploymentDirectory, "Sample.edmx"), Read);
        }

        public static void Read(XElement element)
        {
        }

        [TestMethod]
        public void ComplexTypeTest()
        {   
        }

    }
}
