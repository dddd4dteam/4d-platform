﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MetaDrive.Linq.Edmx.EDMObjects;
using System.Xml.Linq;
using MetaDrive.Linq.Edmx.EDMObjects.SSDL.Property;
using MetaDrive.Linq.Edmx.IO;

namespace MetaDrive.Linq.Edmx.Test
{
    [TestClass]
    public class NorthwindStoreTest
    {
        public TestContext TestContext { get; set; }

        public static EDM edm;

        [ClassInitialize()]
        public static void Init(TestContext testContext)
        {
            edm = EDMXIO.Read(Path.Combine(testContext.DeploymentDirectory, "Northwind.edmx"), Read);            
        }

        public static void Read(XElement element)
        {
        }

        [TestMethod]
        public void EntityTypeCount()
        {
            Assert.IsTrue(edm.SSDLContainer.EntityTypes.Count() == 11);
        }

        [TestMethod]
        public void CategoriesEntity()
        {
            var entity = edm.SSDLContainer.EntityTypes.Where(x => x.Name == "Categories").FirstOrDefault();
            Assert.IsNotNull(entity);

            // CategoryID Property

            var idProperty = entity.Properties.Where(x => x.Name == "CategoryID").FirstOrDefault();
            Assert.IsNotNull(idProperty);

            Assert.AreEqual("int", idProperty.Type);
            Assert.IsFalse(idProperty.Nullable.GetValueOrDefault(true));
            Assert.AreEqual(StoreGeneratedPattern.Identity, idProperty.StoreGeneratedPattern);

            // CategoryName Property

            var categoryNameProperty = entity.Properties.Where(x => x.Name == "CategoryName").FirstOrDefault();
            Assert.IsNotNull(categoryNameProperty);

            Assert.AreEqual("nvarchar", categoryNameProperty.Type);
            Assert.IsFalse(categoryNameProperty.Nullable.GetValueOrDefault(true));
            Assert.AreEqual(15, categoryNameProperty.MaxLength);

            // Description Property

            var descriptionProperty = entity.Properties.Where(x => x.Name == "Description").FirstOrDefault();
            Assert.IsNotNull(descriptionProperty);
            Assert.AreEqual("ntext", descriptionProperty.Type);

            // Picture Property

            var pictureProperty = entity.Properties.Where(x => x.Name == "Picture").FirstOrDefault();
            Assert.IsNotNull(pictureProperty);

            Assert.AreEqual("image", pictureProperty.Type);
        }
    }
}
