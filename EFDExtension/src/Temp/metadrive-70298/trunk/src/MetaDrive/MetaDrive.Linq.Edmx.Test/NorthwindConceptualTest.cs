﻿using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MetaDrive.Linq.Edmx.IO;
using System.Xml.Linq;
using MetaDrive.Linq.Edmx.EDMObjects;
using MetaDrive.Linq.Edmx.EDMObjects.CSDL.Property;

namespace MetaDrive.Linq.Edmx.Test
{
    [TestClass]
    public class NorthwindConceptualTest
    {                
        public TestContext TestContext { get; set; }

        public static EDM edm;

        [ClassInitialize()]
        public static void Init(TestContext testContext) 
        {
            edm = EDMXIO.Read(Path.Combine(testContext.DeploymentDirectory, "Northwind.edmx"), Read);            
        }

        public static void Read(XElement element)
        {
        }

        [TestMethod]        
        public void EntityTypeCount()
        {                        
            Assert.IsTrue(edm.CSDLContainer.EntityTypes.Count() == 11);
        }

        [TestMethod]
        public void CategoryEntity()
        {
            var categoryEntity = edm.CSDLContainer.EntityTypes.Where(x => x.Name == "Category").FirstOrDefault();
            Assert.IsNotNull(categoryEntity);

            // Id Property

            var idProperty = categoryEntity.ScalarProperties.Where(x => x.Name == "Id").FirstOrDefault();
            Assert.IsNotNull(idProperty);

            Assert.AreEqual(PropertyType.Int32, idProperty.Type);
            Assert.IsFalse(idProperty.Nullable);

            // Category Property

            var categoryNameProperty = categoryEntity.ScalarProperties.Where(x => x.Name == "CategoryName").FirstOrDefault();
            Assert.IsNotNull(categoryNameProperty);

            Assert.AreEqual(PropertyType.String, categoryNameProperty.Type);
            Assert.IsFalse(categoryNameProperty.Nullable);
            Assert.AreEqual(15, categoryNameProperty.MaxLength);
            Assert.IsTrue(categoryNameProperty.Unicode.GetValueOrDefault(false));
            Assert.IsFalse(categoryNameProperty.FixedLength.GetValueOrDefault(true));

            // Description Property

            var descriptionProperty = categoryEntity.ScalarProperties.Where(x => x.Name == "Description").FirstOrDefault();
            Assert.IsNotNull(descriptionProperty);

            Assert.AreEqual(PropertyType.String, descriptionProperty.Type);            
            Assert.AreEqual(null, descriptionProperty.MaxLength);
            Assert.IsTrue(descriptionProperty.Unicode.GetValueOrDefault(false));
            Assert.IsFalse(descriptionProperty.FixedLength.GetValueOrDefault(true));

            // Picture Property

            var pictureProperty = categoryEntity.ScalarProperties.Where(x => x.Name == "Picture").FirstOrDefault();
            Assert.IsNotNull(pictureProperty);

            Assert.AreEqual(PropertyType.Binary, pictureProperty.Type);
            Assert.AreEqual(null, pictureProperty.MaxLength);            
            Assert.IsFalse(pictureProperty.FixedLength.GetValueOrDefault(true));

            // Products Navigation Property

            var productsNavigationProperty = categoryEntity.NavigationProperties.Where(x => x.Name == "Products").FirstOrDefault();
            Assert.IsNotNull(productsNavigationProperty);

            //Assert.AreEqual("NorthwindModel.FK_Products_Categories", productsNavigationProperty.Association..Relationship);
            //Assert.AreEqual("Category", productsNavigationProperty.FromRole);
            //Assert.AreEqual("Products", productsNavigationProperty.ToRole);            
        }
    }      
}
