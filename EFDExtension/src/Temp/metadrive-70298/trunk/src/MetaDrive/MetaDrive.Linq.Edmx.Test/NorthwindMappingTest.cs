﻿//using System.IO;
//using System.Linq;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//namespace MetaDrive.Linq.Edmx.Test
//{
//    [TestClass]
//    public class NorthwindMappingTest
//    {                
//        public TestContext TestContext { get; set; }

//        public static EdmxFile edmx;

//        [ClassInitialize()]
//        public static void Init(TestContext testContext) 
//        {
//            edmx = new EdmxFile(Path.Combine(testContext.DeploymentDirectory, "Northwind.edmx"));
//        }
    
//        [TestMethod]
//        public void CategoryEntity()
//        {
//            var containerMapping = edmx.Mapping.Model.EntityContainerMapping
//                .Where(x => x.StorageEntityContainer == "NorthwindModelStoreContainer")
//                .FirstOrDefault();

//            Assert.IsNotNull(containerMapping);

//            var setMapping = containerMapping.EntitySetMapping["Categories"];

//            Assert.IsNotNull(setMapping);

//            var typeMapping = setMapping.EntityTypeMapping
//                .Where(x => x.TypeName == "IsTypeOf(NorthwindModel.Category)")
//                .FirstOrDefault();

//            Assert.IsNotNull(typeMapping);

//            var mappingFragment = typeMapping.Fragments
//                .Where(x => x.StoreEntitySet == "Categories")
//                .FirstOrDefault();

//            Assert.IsNotNull(mappingFragment);

//            var idProperty = mappingFragment.ScalarProperties["Id"];

//            Assert.IsNotNull(idProperty);
//            Assert.AreEqual("CategoryID", idProperty.ColumnName);

//            var categoryNameProperty = mappingFragment.ScalarProperties["CategoryName"];

//            Assert.IsNotNull(categoryNameProperty);
//            Assert.AreEqual("CategoryName", categoryNameProperty.ColumnName);

//            var descriptionProperty = mappingFragment.ScalarProperties["Description"];

//            Assert.IsNotNull(descriptionProperty);
//            Assert.AreEqual("Description", descriptionProperty.ColumnName);

//            var pictureProperty = mappingFragment.ScalarProperties["Picture"];

//            Assert.IsNotNull(pictureProperty);
//            Assert.AreEqual("Picture", pictureProperty.ColumnName);                                
//        }
//    }      
//}
