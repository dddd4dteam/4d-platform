﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Schema;
using System.Xml.Serialization;
using MetaDrive.Edmx.CodeGeneration;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class EntityType
    {                
        public EntityKeyElement Key { get; set; }

        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlAttributeAttribute]
        public string BaseType { get; set; }
        
        [XmlAttributeAttribute]
        [DefaultValueAttribute(false)]
        public bool Abstract { get; set; }
                
        [XmlAttributeAttribute(Form = XmlSchemaForm.Qualified, Namespace = "http://schemas.microsoft.com/ado/2006/04/codegeneration")]
        public PublicOrInternalAccess TypeAccess { get; set; }

        [XmlElementAttribute("Property")]
        public List<Property> Property { get; set; }

        [XmlElementAttribute("NavigationProperty")]
        public List<NavigationProperty> NavigationProperty { get; set; }
    }
}
