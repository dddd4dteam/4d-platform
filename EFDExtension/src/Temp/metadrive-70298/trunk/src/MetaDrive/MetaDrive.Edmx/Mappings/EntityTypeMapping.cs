﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    public partial class EntityTypeMapping
    {        
        [XmlAttributeAttribute]
        public string TypeName { get; set; }

        [XmlElementAttribute("MappingFragment")]
        public List<MappingFragment> MappingFragment { get; set; }
    }
}
