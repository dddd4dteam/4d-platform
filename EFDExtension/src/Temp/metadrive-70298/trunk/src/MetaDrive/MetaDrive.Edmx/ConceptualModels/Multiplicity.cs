﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public enum Multiplicity
    {     
        [XmlEnumAttribute("0..1")]
        ZeroOrOne,
        
        [XmlEnumAttribute("1")]
        One,
        
        [XmlEnumAttribute("*")]
        ZeroOrMany,
    }
}
