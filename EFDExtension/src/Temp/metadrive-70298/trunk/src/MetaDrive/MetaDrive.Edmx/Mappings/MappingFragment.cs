﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    public partial class MappingFragment
    {
        [XmlAttributeAttribute]
        public string StoreEntitySet { get; set; }

        [XmlElementAttribute("ScalarProperty")]
        public List<ScalarProperty> ScalarProperty { get; set; }

        [XmlElementAttribute("ComplexProperty")]
        public List<ComplexProperty> ComplexProperty { get; set; }
    }
}
