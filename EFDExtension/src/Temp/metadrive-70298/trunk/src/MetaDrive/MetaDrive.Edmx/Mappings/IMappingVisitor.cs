﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Edmx.Mapping
{
    public interface IMappingVisitor
    {
        void Visit(Mapping element);
        void Visit(EntityContainerMapping element);
        void Visit(EntitySetMapping element);
        void Visit(EntityTypeMapping element);
        void Visit(MappingFragment element);
        void Visit(ScalarProperty element);
        void Visit(ComplexProperty element);
    }
}
