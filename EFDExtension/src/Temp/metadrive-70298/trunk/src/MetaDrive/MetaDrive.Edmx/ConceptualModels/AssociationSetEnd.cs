﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class AssociationSetEnd
    {        
        [XmlAttributeAttribute]
        public string Role { get; set; }
                
        [XmlAttributeAttribute]
        public string EntitySet { get; set; }        
    }
}
