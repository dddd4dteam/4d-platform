﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class AssociationSet
    {
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlAttributeAttribute]
        public string Association { get; set; }

        [XmlElementAttribute("End")]
        public List<AssociationSetEnd> End { get; set; }

    }
}
