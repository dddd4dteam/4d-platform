﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    public partial class ScalarProperty
    {
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlAttributeAttribute]
        public string ColumnName { get; set; }        
    }
}
