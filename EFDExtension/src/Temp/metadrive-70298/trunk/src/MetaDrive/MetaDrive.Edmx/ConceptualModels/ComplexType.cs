﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class ComplexType
    {
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlElementAttribute("Property")]
        public List<Property> Property { get; set; }
    }
}
