﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class EntityKeyElement
    {                     
        [XmlElementAttribute("PropertyRef")]
        public List<PropertyRef> PropertyRef { get; set; }
    }
}
