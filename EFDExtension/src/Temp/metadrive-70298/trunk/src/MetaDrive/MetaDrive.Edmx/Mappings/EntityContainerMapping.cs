﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    [XmlRootAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2, IsNullable = false)]
    public partial class EntityContainerMapping
    {
        [XmlAttributeAttribute]
        public string StorageEntityContainer { get; set; }

        [XmlAttributeAttribute]
        public string CdmEntityContainer { get; set; }

        [XmlElementAttribute("EntitySetMapping")]
        public List<EntitySetMapping> EntitySetMapping { get; set; }
    }
}
