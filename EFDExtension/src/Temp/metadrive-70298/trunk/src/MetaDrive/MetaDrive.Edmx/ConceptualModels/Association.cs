﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute()]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class Association
   {
        [XmlAttributeAttribute()]
        public string Name { get; set; }        
        
        [XmlElementAttribute("End")]
        public List<AssociationEnd> End { get; set; }                        
    }
}
