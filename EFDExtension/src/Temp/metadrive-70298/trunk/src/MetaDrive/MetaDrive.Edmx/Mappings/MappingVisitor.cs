﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.Edmx.Mapping
{
    public class MappingVisitor : IMappingVisitor
    {
        public virtual void Visit(Mapping element)
        {
            foreach (var child in element.EntityContainerMapping)
                Visit(child);
        }

        public virtual void Visit(EntityContainerMapping element)
        {
            foreach (var child in element.EntitySetMapping)
                Visit(child);
        }

        public virtual void Visit(EntitySetMapping element)
        {
            foreach (var child in element.EntityTypeMapping)
                Visit(child);
        }

        public virtual void Visit(EntityTypeMapping element)
        {
            foreach (var child in element.MappingFragment)
                Visit(child);
        }

        public virtual void Visit(MappingFragment element)
        {
            foreach (var child in element.ScalarProperty)
                Visit(child);

            foreach (var child in element.ComplexProperty)
                Visit(child);
        }

        public virtual void Visit(ScalarProperty element)
        {                    
        }

        public virtual void Visit(ComplexProperty element)
        {
            foreach (var child in element.ScalarProperty)
                Visit(child);
        }
    }
}
