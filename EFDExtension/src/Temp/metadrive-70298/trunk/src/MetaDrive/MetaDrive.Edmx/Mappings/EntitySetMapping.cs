﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    public partial class EntitySetMapping
    {
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlElementAttribute("EntityTypeMapping")]
        public List<EntityTypeMapping> EntityTypeMapping { get; set; }
    }
}
