﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.CodeGeneration
{        
    [SerializableAttribute()]
    [XmlTypeAttribute(Namespace = "http://schemas.microsoft.com/ado/2006/04/codegeneration")]
    public enum PublicOrInternalAccess
    {       
        Public,     
        Internal,
    }
}
