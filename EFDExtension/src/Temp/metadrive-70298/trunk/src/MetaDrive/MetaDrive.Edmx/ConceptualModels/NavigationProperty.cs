﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class NavigationProperty
    {        
        [XmlAttributeAttribute]
        public string Name { get; set; }
                
        [XmlAttributeAttribute]
        public string Relationship { get; set; }

        [XmlAttributeAttribute]
        public string FromRole { get; set; }

        [XmlAttributeAttribute]
        public string ToRole { get; set; }                                
    }
}
