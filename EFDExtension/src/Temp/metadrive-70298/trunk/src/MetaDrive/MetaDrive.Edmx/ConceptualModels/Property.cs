﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class Property
    {          
        [XmlAttributeAttribute]
        public string Name { get; set; }
                
        [XmlAttributeAttribute]
        public string Type { get; set; }
                
        [XmlAttributeAttribute]
        public bool Nullable { get; set; }
                                
        [XmlAttributeAttribute]
        public string DefaultValue { get; set; }
                
        [XmlAttributeAttribute]
        public string MaxLength { get; set; }                     
    }
}
