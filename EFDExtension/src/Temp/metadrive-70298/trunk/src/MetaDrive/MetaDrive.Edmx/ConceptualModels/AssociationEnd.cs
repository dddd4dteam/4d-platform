﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class AssociationEnd
    {
        [XmlAttributeAttribute]
        public string Role { get; set; }
     
        [XmlAttributeAttribute]
        public string Type { get; set; }
                                        
        [XmlAttributeAttribute]
        public Multiplicity Multiplicity { get; set; }                
    }
}
