﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class EntitySet
    {                        
        [XmlAttributeAttribute]
        public string Name { get; set; }
        
        [XmlAttributeAttribute]
        public string EntityType { get; set; }                
    }
}
