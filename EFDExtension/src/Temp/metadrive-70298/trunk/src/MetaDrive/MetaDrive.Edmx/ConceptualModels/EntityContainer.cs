﻿using System;
using System.Collections.Generic;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(AnonymousType = true, Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    [XmlRootAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2, IsNullable = false)]
    public partial class EntityContainer
    {                                
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlAttributeAttribute(Form = XmlSchemaForm.Qualified, Namespace = "http://schemas.microsoft.com/ado/2009/02/edm/annotation")]
        public bool LazyLoadingEnabled { get; set; }

        [XmlElementAttribute("EntitySet")]
        public List<EntitySet> EntitySet { get; set; }

        [XmlElementAttribute("AssociationSet")]
        public List<AssociationSet> AssociationSet { get; set; }
    }
}
