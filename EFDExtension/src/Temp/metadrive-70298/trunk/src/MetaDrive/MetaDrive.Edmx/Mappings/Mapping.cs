﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    [XmlRootAttribute("Mapping", Namespace = EdmxConstants.MSL_NAMESPACE_V2, IsNullable = false)]
    public partial class Mapping
    {
        [XmlAttributeAttribute]
        public string Space { get; set; }

        [XmlElementAttribute("EntityContainerMapping")]
        public List<EntityContainerMapping> EntityContainerMapping { get; set; }
    }
}
