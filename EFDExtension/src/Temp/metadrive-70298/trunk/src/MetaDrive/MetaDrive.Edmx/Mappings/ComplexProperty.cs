﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Mapping
{
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.MSL_NAMESPACE_V2)]
    public partial class ComplexProperty
    {
        [XmlAttributeAttribute]
        public string Name { get; set; }

        [XmlAttributeAttribute]
        public string TypeName { get; set; }

        [XmlElementAttribute("ScalarProperty")]
        public List<ScalarProperty> ScalarProperty { get; set; }
    }
}
