﻿using System;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    public partial class PropertyRef
    {                
        [XmlAttributeAttribute]
        public string Name { get; set; }        
    }
}
