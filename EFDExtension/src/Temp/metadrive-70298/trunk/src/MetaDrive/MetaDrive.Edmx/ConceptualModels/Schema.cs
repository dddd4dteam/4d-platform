﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MetaDrive.Edmx.Conceptual
{    
    [SerializableAttribute]
    [XmlTypeAttribute(Namespace = EdmxConstants.CSDL_NAMESPACE_V2)]
    [XmlRootAttribute("Schema", Namespace = EdmxConstants.CSDL_NAMESPACE_V2, IsNullable = false)]
    public partial class Schema
    {
        [XmlAttributeAttribute]
        public string Namespace { get; set; }

        [XmlAttributeAttribute]
        public string Alias { get; set; }

        [XmlElementAttribute("EntityContainer")]
        public List<EntityContainer> EntityContainer { get; set; }
           
        [XmlElementAttribute("EntityType")]
        public List<EntityType> EntityType { get; set; }

        [XmlElementAttribute("ComplexType")]
        public List<ComplexType> ComplexType { get; set; }

        [XmlElementAttribute("Association")]
        public List<Association> Association { get; set; }
    }
}
