﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MetaDrive.CodePress
{
    public static class StringExtensions
    {
        public static string FormatWith(this string text, params object[] args)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            return string.Format(text, args);
        }
    }
}
