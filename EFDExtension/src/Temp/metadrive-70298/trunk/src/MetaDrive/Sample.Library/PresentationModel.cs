﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MetaDrive.MetaModel;

namespace Sample.Library
{
    public partial class PresentationModel
    {
        [NotifyPropertyChanged]
        public virtual string PropertyA { get; set; }

        public virtual string PropertyB { get; set; }

        public virtual void Method(string jef)
        {            
        }

        public virtual void NoParameters()
        {
        }

        public virtual int AnotherMethod(int a, string b)
        {
            return 0;
        }
    }
}
