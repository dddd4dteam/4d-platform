public static IEdmModel GetEdmModel(this DbContext context)
{
    using (MemoryStream stream = new MemoryStream())
    {
        using (XmlWriter writer = XmlWriter.Create(stream))
        {
            EdmxWriter.WriteEdmx(context, writer);
            writer.Close();
            stream.Seek(0, SeekOrigin.Begin);
            using (XmlReader reader = XmlReader.Create(stream))
            {
                return Microsoft.Data.Edm.Csdl.EdmxReader.Parse(reader);
            }
        }
    }
}