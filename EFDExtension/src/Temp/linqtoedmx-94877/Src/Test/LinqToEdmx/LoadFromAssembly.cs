using System.Linq;
using LinqToEdmx.Map;
using LinqToEdmx.Model.Conceptual;
using LinqToEdmx.Model.Storage;
using NUnit.Framework;
using ShouldIt.Clr.Fluent;

namespace LinqToEdmx
{
  /// <summary>
  /// Tests loading EDMX metadata when the csdl, ssdl and msl files are embedded in an assembly.
  /// Note that you need to disable NUnit 'Shadow-copy' because it causes problems for the ShouldIt library.
  /// </summary>
  [TestFixture]
  public class LoadFromAssembly
  {
    /// <summary>
    /// Loads the conceptual model from an embedded .csdl file.
    /// </summary>
    [Test]
    public void LoadCsdlFile()
    {
      // Specify the assembly and the resource name
      var edmx = Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.csdl");

      LoadFromSingleFile.ValidateNorthwindConceptualModel(edmx);

      // Validate that only the .csdl file was loaded
      edmx.GetItems<EntityTypeStore>().Count().Should().Equal(0);
      edmx.GetItems<EntityTypeMapping>().Count().Should().Equal(0);
      edmx.GetItems<Model.Conceptual.EntityContainer>().First().Name.Should().Equal("NorthwindEntities");
    }

    /// <summary>
    /// Loads the storage model from an embedded .ssdl file.
    /// </summary>
    [Test]
    public void LoadSsdlFile()
    {
      // Specify the assembly and the resource name
      var edmx = Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.ssdl");

      LoadFromSingleFile.ValidateNorthwindStorageModel(edmx);

      // Validate that only the .ssdl file was loaded
      edmx.GetItems<EntityType>().Count().Should().Equal(0);
      edmx.GetItems<EntityTypeMapping>().Count().Should().Equal(0);
      edmx.GetItems<Model.Storage.EntityContainer>().First().Name.Should().Equal("NorthwindModelStoreContainer");
    }

    /// <summary>
    /// Loads the mapping from an embedded .msl file.
    /// </summary>
    [Test]
    public void LoadMslFile()
    {
      // Specify the assembly and the resource name
      var edmx = Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.msl");

      LoadFromSingleFile.ValidateNorthwindMapping(edmx);

      // Validate that only the .msl file was loaded
      edmx.GetItems<EntityType>().Count().Should().Equal(0);
      edmx.GetItems<EntityTypeStore>().Count().Should().Equal(0);
      edmx.GetItems<EntityContainerMapping>().First().StorageEntityContainer.Should().Equal("NorthwindModelStoreContainer");
    }

    /// <summary>
    /// Loads all .csdl, .ssdl, and .msl files from assemblies specified via a wildcard.
    /// </summary>
    [Test]
    public void LoadAllFilesViaAssemblyWildCardPath()
    {
      var edmx = Edmx.Load(@"res://*/");

      LoadFromSingleFile.ValidateNorthwindConceptualModel(edmx);
      LoadFromSingleFile.ValidateNorthwindStorageModel(edmx);
      LoadFromSingleFile.ValidateNorthwindMapping(edmx);
    }

    /// <summary>
    /// Loads all .csdl, .ssdl, and .msl files from a specified assembly.
    /// </summary>
    [Test]
    public void LoadAllFilesViaSpecifiedAssembly()
    {
      var edmx = Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/");

      LoadFromSingleFile.ValidateNorthwindConceptualModel(edmx);
      LoadFromSingleFile.ValidateNorthwindStorageModel(edmx);
      LoadFromSingleFile.ValidateNorthwindMapping(edmx);
    }

    /// <summary>
    /// Loads all .csdl, .ssdl, and .msl files from a specified assembly and resourceName.
    /// </summary>
    [Test]
    public void LoadAllFilesViaSpecifiedAssemblyAndPath()
    {
      var edmx = Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.csdl|
                             res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.ssdl|
                             res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/NorthwindEmbedded.msl");

      LoadFromSingleFile.ValidateNorthwindConceptualModel(edmx);
      LoadFromSingleFile.ValidateNorthwindStorageModel(edmx);
      LoadFromSingleFile.ValidateNorthwindMapping(edmx);
    }

    /// <summary>
    /// Validates that a resource can be specified and filtered by file name, not just by file extension.
    /// </summary>
    [Test]
    public void FilterResourceByFileName_Issue12273()
    {
      Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/FirstEmbedded.csdl")
        .Runtimes.First().ConceptualModels.ConceptualSchema.EntityContainers.First().Name.Should().Equal("FirstEmbedded");

      Edmx.Load(@"res://LinqToEdmx.Test,Version=0.0.0.0,Culture=neutral,PublicKeyToken=null/SecondEmbedded.csdl")
        .Runtimes.First().ConceptualModels.ConceptualSchema.EntityContainers.First().Name.Should().Equal("SecondEmbedded");
    }
  }
}