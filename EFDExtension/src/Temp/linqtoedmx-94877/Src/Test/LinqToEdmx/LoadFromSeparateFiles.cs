using NUnit.Framework;

namespace LinqToEdmx
{
  /// <summary>
  /// Tests loading EDMX metadata when the emdx file is split into separate csdl, ssdl and msl files.
  /// Note that you need to disable NUnit 'Shadow-copy' because it causes problems for the ShouldIt library.
  /// </summary>
  [TestFixture]
  public class LoadFromSeparateFiles
  {
    /// <summary>
    /// Loads the conceptual model from a .csdl file.
    /// </summary>
    [Test]
    public void LoadCsdlFile()
    {
      var edmx = Edmx.Load(@"Northwind.csdl");

      LoadFromSingleFile.ValidateNorthwindConceptualModel(edmx);
    }

    /// <summary>
    /// Loads the storage model from a .ssdl file.
    /// </summary>
    [Test]
    public void LoadSsdlFile()
    {
      var edmx = Edmx.Load(@"Northwind.ssdl");

      LoadFromSingleFile.ValidateNorthwindStorageModel(edmx);
    }

    /// <summary>
    /// Loads the mapping from a .msl file.
    /// </summary>
    [Test]
    public void LoadMslFile()
    {
      var edmx = Edmx.Load(@"Northwind.msl");

      LoadFromSingleFile.ValidateNorthwindMapping(edmx);
    }

    /// <summary>
    /// Loads the csdl, ssdl, and msl files from a pipe-delimited path.
    /// </summary>
    [Test]
    public void LoadFilesFromPipeDelimitedPath()
    {
      var edmx = Edmx.Load(@"Northwind.csdl | Northwind.ssdl|Northwind.msl");

      LoadFromSingleFile.ValidateNorthwindEdmx(edmx);
    }
  }
}