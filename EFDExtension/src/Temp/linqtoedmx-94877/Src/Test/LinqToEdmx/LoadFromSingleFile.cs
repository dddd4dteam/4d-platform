using System.Linq;
using LinqToEdmx.Map;
using LinqToEdmx.Model.Conceptual;
using LinqToEdmx.Model.Storage;
using NUnit.Framework;
using ShouldIt.Clr.Fluent;

namespace LinqToEdmx
{
  /// <summary>
  /// Tests loading EDMX metadata from a single edmx file.
  /// Note that you need to disable NUnit 'Shadow-copy' because it causes problems for the ShouldIt library.
  /// </summary>
  [TestFixture]
  public class LoadFromSingleFile
  {
    /// <summary>
    /// Load and then get items from the model
    /// </summary>
    [Test]
    public void LoadAndGetItems()
    {
      var edmx = Edmx.Load(@"Northwind.edmx");

      // Get the EntityTypes from the conceptual model
      var entityTypes = edmx.GetItems<EntityType>();
      entityTypes.Count().Should().Equal(26);

      // Get the EntityTypeStores from the storage model
      var entityTypeStores = edmx.GetItems<EntityTypeStore>();
      entityTypeStores.Count().Should().Equal(28);

      // Get the EntityTypeMappings from the mappings
      var entityTypeMappings = edmx.GetItems<EntityTypeMapping>();
      entityTypeMappings.Count().Should().Equal(26);
    }

    [Test]
    public void JoinMappingAndConceptualModel()
    {
      var edmx = Edmx.Load(@"Northwind.edmx");

                                    // From the mappings
      var mappingToConceptualJoin = from entityTypeMapping in edmx.GetItems<EntityTypeMapping>()
                                    // From the conceptual model
                                    from entityType in edmx.GetItems<EntityType>()
                                    // Get the namespace of the conceptual model entities
                                    let entityNamespace = edmx.GetItems<ConceptualSchema>().First().Namespace
                                    // Join the two models by the fully qualified type name
                                    where entityTypeMapping.TypeName == entityNamespace + "." + entityType.Name
                                    select new
                                             {
                                               // The type name from the conceptual model
                                               entityType.Name, 
                                               // The fully qualified type name from the mapping model
                                               entityTypeMapping.TypeName
                                             };

      mappingToConceptualJoin.Count().Should().Equal(26);
    }

    [Test]
    public void GetEntityTypesViaQueryAndPath()
    {
      var edmx = Edmx.Load(@"Northwind.edmx");

      // Using GetItems()
      var entityTypesViaQuery = edmx.GetItems<EntityType>();
      entityTypesViaQuery.Count().Should().Equal(26);

      // Using the known reference path
      var entityTypesViaPath = edmx.Runtimes.First().ConceptualModels.ConceptualSchema.EntityTypes;
      entityTypesViaPath.Count().Should().Equal(26);

      entityTypesViaQuery.ToList().Should().Equal(entityTypesViaPath.ToList());
    }

    [Test]
    public void GetEntityTypeMappingsViaQueryAndPath()
    {
      var edmx = Edmx.Load(@"Northwind.edmx");

      // Using GetItems()
      var entityTypeMappingsViaQuery = edmx.GetItems<EntityTypeMapping>();
      entityTypeMappingsViaQuery.Count().Should().Equal(26);

      // Using the known reference path
      var entityTypeMappingsViaPath = edmx.Runtimes.First().Mappings.Mapping.EntityContainerMapping.EntitySetMappings.SelectMany(entitySetMapping => entitySetMapping.EntityTypeMappings);
      entityTypeMappingsViaPath.Count().Should().Equal(26);
    }

    /// <summary>
    /// Loads an edmx file from a file path and validates its structure and values.
    /// </summary>
    [Test]
    public void LoadEdmxFile()
    {
      var edmx = Edmx.Load(@"Northwind.edmx");

      // Conceptual model, storage model and mapping validation
      ValidateNorthwindEdmx(edmx);

      // Designer validation
      edmx.Designers.Should().Not.Be.Null();
      edmx.Designers.Count.Should().Equal(1);
    }

    /// <summary>
    /// Validates the conceptual model, storage model and mapping metadata loaded via a Northwind.edmx file.
    /// </summary>
    /// <param name="edmx">The edmx.</param>
    public static void ValidateNorthwindEdmx(Edmx edmx)
    {
      edmx.Should().Not.Be.Null();

      // Runtime validation
      edmx.Runtimes.Should().Not.Be.Null();
      edmx.Runtimes.Count.Should().Equal(1);

      ValidateNorthwindConceptualModel(edmx);

      ValidateNorthwindStorageModel(edmx);

      ValidateNorthwindMapping(edmx);
    }

    public static void ValidateNorthwindMapping(Edmx edmx)
    {
      edmx.Runtimes.First().Mappings.Should().Not.Be.Null();
      var entityTypeMappings = edmx.GetItems<EntityTypeMapping>();
      entityTypeMappings.Count().Should().Equal(26);
    }

    public static void ValidateNorthwindStorageModel(Edmx edmx)
    {
      edmx.Runtimes.First().StorageModels.Should().Not.Be.Null();
      var entityTypeStores = edmx.GetItems<EntityTypeStore>();
      entityTypeStores.Count().Should().Equal(28);
    }

    public static void ValidateNorthwindConceptualModel(Edmx edmx)
    {
      var conceptualModels = edmx.Runtimes.First().ConceptualModels;
      conceptualModels.Should().Not.Be.Null();
      conceptualModels.ConceptualSchema.Namespace.Should().Equal("NorthwindModel");
      conceptualModels.ConceptualSchema.Alias.Should().Equal("Self");
      conceptualModels.ConceptualSchema.EntityContainers.Should().Not.Be.Null();
      conceptualModels.ConceptualSchema.EntityContainers.Count.Should().Equal(1);
      var entityTypes = edmx.GetItems<EntityType>();
      entityTypes.Count().Should().Equal(26);

      // EntitySet validation
      var entitySets = conceptualModels.ConceptualSchema.EntityContainers.First().EntitySets;
      entitySets.Count.Should().Equal(26);
      entitySets.Single(entitySet => entitySet.Name == "Summary_of_Sales_by_Years").EntityType.Should().Equal("NorthwindModel.Summary_of_Sales_by_Year");
      entitySets.Single(entitySet => entitySet.Name == "Customers").EntityType.Should().Equal("NorthwindModel.Customer");

      // AssociationSet validation
      var associationSets = conceptualModels.ConceptualSchema.EntityContainers.First().AssociationSets;
      associationSets.Count.Should().Equal(11);
      associationSets.Single(associationSet => associationSet.Name == "FK_Orders_Employees").Ends.Single(end => end.Role == "Employees").EntitySet.Should().Equal("Employees");
    }
  }
}