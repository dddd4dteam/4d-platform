﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Composition.DataServices;
using System.Resources;
using Core.Commanding;
using Core.Meta.Systems;
using CRM.DAL;
using Core;


namespace CRM.DOM.Services
{

    public partial class S4_MaterialModelManager
    {

        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return null; } }

        /// <summary>
        /// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn { get { return new List<ServiceModelManager_RegisterEn>(); } }
    }
}