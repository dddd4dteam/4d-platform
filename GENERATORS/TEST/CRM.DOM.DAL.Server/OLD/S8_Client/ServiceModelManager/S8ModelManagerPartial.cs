﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Composition.DataServices;
using System.Resources;
using Core.Commanding;
using Core.Meta.Systems;
using CRM.DAL;
using Core;
using Core.Meta.BO;
using Core.BoModel;

namespace CRM.DOM.Services
{

    public partial class S8_ClientModelManager
    {

        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return null; } }
      

        /// <summary>
        /// Cписок зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn
        {
            get
            {
                return new List<ServiceModelManager_RegisterEn>()
                {
                    ServiceModelManager_RegisterEn.S1_Geography,
                    ServiceModelManager_RegisterEn.S2_Assortment,
                    ServiceModelManager_RegisterEn.S3_GoodsCategory,
                    ServiceModelManager_RegisterEn.S4_Material,
                    ServiceModelManager_RegisterEn.S5_Rival,
                    ServiceModelManager_RegisterEn.S6_Diler,
                    ServiceModelManager_RegisterEn.S7_Holding,
                    ServiceModelManager_RegisterEn.S16_ClientTypology2,
                    ServiceModelManager_RegisterEn.S10_EkspositorOrder
                };


            }
        }





    }
}