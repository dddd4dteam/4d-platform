﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Meta.Systems;

namespace CRM.DOM.Services
{
    public partial class S10_EkspositorOrderModelManager
    {
        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return null; } }


        /// <summary>
        /// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn
        {
            get
            {
                return new List<ServiceModelManager_RegisterEn>()
                    {
                        ServiceModelManager_RegisterEn.S8_Client,
                        ServiceModelManager_RegisterEn.S6_Diler,
                        ServiceModelManager_RegisterEn.S1_Geography,
                        ServiceModelManager_RegisterEn.S10_EkspositorOrder
                    };
             
            
            } 
        }
    }
}