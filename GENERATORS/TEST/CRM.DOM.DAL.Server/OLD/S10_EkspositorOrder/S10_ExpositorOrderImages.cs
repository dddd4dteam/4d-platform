﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Core.BoModel;
using Core.Meta.BO;
using Core.Composition.DataServices;
using Core.Meta.Systems;

using BLToolkit.Data;
using BLToolkit.Data.DataProvider;
using BLToolkit.Data.Linq;
using BLToolkit.Data.Sql;
using BLToolkit.Data.Sql.SqlProvider;
using BLToolkit.DataAccess;
using BLToolkit.Mapping;
using BLToolkit.ServiceModel;



namespace CRM.DAL
{

    [DataContract]
    [TableName(Name = "crs_S10_EkspositorOrderImage")]
    [ServiceModel(ServiceModelManager_RegisterEn.S10_EkspositorOrder), BORole(BORoleEn.DataTable)]
    public partial class crs_S10_EkspositorOrderImage : PersistableBOBase
    {
        [PrimaryKey(1), BOPropertyRole(PropertyRoleEn.PKeyField), DataMember]
        public Guid ID { get; set; } // uniqueidentifier

        //[Nullable, DataMember]
        //public byte[] FileData { get; set; } // varbinary(-1)

        //[Nullable, DataMember]
        //public byte[] FileThumbData { get; set; } // varbinary(-1)
        

        [Nullable, DataMember]
        public string Description { get; set; } // varchar(500)
        [DataMember]
        public int ID_Order { get; set; } // int(10)
        [Nullable, DataMember]
        public string MD5 { get; set; } // varchar(40)
        [Nullable, DataMember]
        public DateTime? SavedDt { get; set; } // datetime(3)
        [Nullable, DataMember]
        public string SaveUserName { get; set; } // varchar(40)
        [Nullable, DataMember]
        public Guid? SaveUserID { get; set; } // uniqueidentifier
        [Nullable, DataMember]
        public string Uri { get; set; } // varchar(1000)
        [Nullable, DataMember]
        public string ThumbUri { get; set; } // varchar(1000)

        // Summary:
        //     Creates a new object that is a copy of the current instance.
        //
        // Returns:
        //     A new object that is a copy of this instance.
        public override object Clone()
        {
            try
            {
                crs_S10_EkspositorOrderImage clonedItem = new crs_S10_EkspositorOrderImage()
                {
                    ID = this.ID,
                    //FileData = this.FileData,
                    //FileThumbData = this.FileThumbData,
                    Description = this.Description,
                    ID_Order = this.ID_Order,
                    MD5 = this.MD5,
                    SavedDt = this.SavedDt,
                    SaveUserName = this.SaveUserName,
                    SaveUserID = this.SaveUserID,
                    Uri = this.Uri,
                    ThumbUri = this.ThumbUri
                };
                return clonedItem;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        public override List<String> PrimaryKey
        {
            get { return new List<String>() { "ID" }; }
        }
    }



}
