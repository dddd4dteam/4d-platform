﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Composition.DataServices;
using System.Resources;
using Core.Commanding;
using Core.Meta.Systems;
using CRM.DAL;
using Core;
//using CRM.DOM.DAL;

namespace CRM.DOM.Services
{
    public partial class S9_MeetingModelManager
    {

        /// <summary>
        /// Целевой Значимый объект для Модели данных Сервиса. Константа
        /// Вокруг которого строится Контекст Модели
        /// </summary>
        public Type TargetModelBO { get { return typeof(V_S9_Meeting_Vo); } }
      

        /// <summary>
        /// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис
        /// </summary>
        public List<ServiceModelManager_RegisterEn> DependsOn
        {
            get
            {
                return new List<ServiceModelManager_RegisterEn>()
                {
                    ServiceModelManager_RegisterEn.S8_Client,
                    ServiceModelManager_RegisterEn.S10_EkspositorOrder,
                    ServiceModelManager_RegisterEn.S17_Questionary
                };


            }
        }
        
    }
}