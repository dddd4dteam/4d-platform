﻿using System;
 
using DDDD.Core.App;
using DDDD.Core.ComponentModel;
using DDDD.Core.Data.DA2;
using DDDD.Core.Data.DA2.Query;


namespace DDDD.CRM.DOM.DAL.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                AppBootstrapper.Current.LoadAppComponents
            (prepareAppAction: null
            , useKnownComponents:
               ComponentClassEn.ServiceModels
               | ComponentClassEn.DCManagers
               | ComponentClassEn.DCServices
            , registerCustomComponentClassesFunc: null            
            , loadAdditionalAssembliesFunc: null
            , startupAppAction : null
            );

                var query1 = DSelectQuery.SelectTopN(10)
                            .From<V_S1_GeographyItem_Vo>();

                var query1Text = query1.ToSQLString(ProviderKey.Default);

                var connectionString = "Data Source=A3;Initial Catalog=CRM_Cersanit2_test3;Integrated Security=True";

                DBProxy.GetInit<MsSqlDataProvider>(connectionString, IsDefaultProxy: true);

                var loadedList = DBProxy.DefProx.SelectList<V_S1_GeographyItem_Vo>(query1);

            }
            catch (Exception exc )
            { 
                throw exc;
            }

           
            
        }
    }
}
