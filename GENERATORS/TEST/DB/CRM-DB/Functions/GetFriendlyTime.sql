﻿-- =============================================
-- Author:		 Andrew U.
-- Create date: 09.04.2009
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetFriendlyTime]
(
	-- Add the parameters for the function here
    -- <@Param1, sysname, @p1> <Data_Type_For_Param1, , int>
    @InputDateTime as DateTime,
    @IsHourOut as bit,
    @IsMinutsOut as bit,
    @IsSecondsOut  as bit,	
    @IsPrefixes as bit

    
)
RETURNS  Varchar(8)
AS
BEGIN
   DECLARE @Out_FriendlyTime varchar(15)
   DECLARE @Hours varchar(3)	 
   DECLARE @Minutes varchar(3)
   DECLARE @Seconds varchar(3)

SET @Out_FriendlyTime =''

--Use Prefixes or NO
IF @IsPrefixes = 1
  BEGIN
   SET @Hours = Right('0'+Cast(Datepart(hh,@InputDateTime) as varchar),2)+'h' 
   SET @Minutes = Right('0'+ Cast(Datepart(mi,@InputDateTime) as varchar),2)+'m'
   SET @Seconds = Right('0'+Cast(Datepart(ss,@InputDateTime) as varchar),2)+'s'
  END
ELSE
  BEGIN
   SET @Hours = Right('0'+Cast(Datepart(hh,@InputDateTime) as varchar),2)
   SET @Minutes = Right('0'+ Cast(Datepart(mi,@InputDateTime) as varchar),2)
   SET @Seconds = Right('0'+Cast(Datepart(ss,@InputDateTime) as varchar),2)
  END 



-- 0
IF ( @IsHourOut = 0 AND @IsMinutsOut =0 AND @IsSecondsOut = 0)
      BEGIN
		RETURN @Out_FriendlyTime
      END
-- 1   
   IF ( @IsHourOut = 1 AND @IsMinutsOut =0 AND @IsSecondsOut = 0)
      BEGIN
		SET	@Out_FriendlyTime= @Hours 
		RETURN @Out_FriendlyTime
      END
   IF ( @IsHourOut = 0 AND @IsMinutsOut=1 AND @IsSecondsOut = 0)
      BEGIN
		SET	@out_FriendlyTime= @Minutes
		RETURN @Out_FriendlyTime
      END
   IF ( @IsHourOut = 0 AND @IsMinutsOut =0 AND @IsSecondsOut = 1)
      BEGIN
		SET	@Out_FriendlyTime= @Seconds
		RETURN @Out_FriendlyTime
      END

--- 2
   IF ( @IsHourOut = 1 AND @IsMinutsOut =1 AND @IsSecondsOut = 0)
      BEGIN
		SET	@Out_FriendlyTime=@Hours+':'+@Minutes
		RETURN @Out_FriendlyTime
      END
   IF ( @IsHourOut = 1 AND @IsMinutsOut =0 AND @IsSecondsOut = 1)
      BEGIN
		SET	@Out_FriendlyTime=@Hours +':'+ @Seconds
		RETURN @Out_FriendlyTime
      END
   IF ( @IsHourOut = 0 AND @IsMinutsOut =1 AND @IsSecondsOut = 1)
      BEGIN
		SET	@Out_FriendlyTime=@Minutes+':'+ @Seconds
		RETURN @Out_FriendlyTime
      END

--- 3
   IF ( @IsHourOut = 1 AND @IsMinutsOut =1 AND @IsSecondsOut = 1)
      BEGIN
		SET	@Out_FriendlyTime=@Hours+':'+@Minutes+':'+@Seconds
		RETURN @Out_FriendlyTime
      END
   


RETURN @Out_FriendlyTime

END



