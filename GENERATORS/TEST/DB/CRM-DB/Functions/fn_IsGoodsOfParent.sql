﻿

CREATE Function [dbo].[fn_IsGoodsOfParent] (@Id_goods char(10),  @NeedParent_Id char(10))
/* функция определяет принадлежит ли данный заданный товар (элемент экспозитора) заданному родителю, */
/*1-да, 0-нет, 9 - неизвестно, уровней больше максимального */
Returns int
As
Begin
 Declare @Is Int, @ParentCode char(10), @id_goods_1C binary(16), @Level int, @MaxLevel int

 Set @MaxLevel = 30
 Select @id_goods_1C = _IDRRef from erp_goods_1C_str where _Code = @id_goods
 Set @ParentCode = ''
 Set @Level = 1

 While (@Level Between 1 AND @MaxLevel) AND @ParentCode <> @NeedParent_Id
 Begin
   Select @ParentCode = t2._Code, @Id_goods_1C = t2._IDRRef
   From erp_goods_1C_str t1
   Inner join erp_goods_1C_str t2 on(t1._ParentIDRRef = t2._IDRRef)
   Where t1._IDRRef = @Id_goods_1C
   If @@Rowcount > 0
        Set @Level = @Level + 1
   Else 
        Set @Level = 0
 End

 If @ParentCode = @NeedParent_Id
  Begin
    Set @Is = 1
  End
 Else
  Begin
   If @Level = @MaxLevel
      Set @Is = 9
   Else 
      Set @Is = 0
  End

Return @Is
End


