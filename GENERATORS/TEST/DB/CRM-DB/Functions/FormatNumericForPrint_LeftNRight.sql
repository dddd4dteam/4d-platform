﻿
-- =============================================
-- Author:		A  
-- Create date: 25.07.2013
-- Description: Форматирование нумерика в приемлемый для чтения вид. Форматирование - после каждого разряда(3-х знаков) ставится пробел. 
--              Данный вариант функции форматирует как левую так и правую, об этом говорит LeftNRight в названии функции.
-- =============================================
CREATE FUNCTION [dbo].[FormatNumericForPrint_LeftNRight] 
(	 
	@InputNumeric numeric(23,4)		-- Число для распечатывания -типа нумерик. Всего в числе может быть только  до 19 целых знаков. После запятой учитываются 4 знака. 
)
RETURNS nvarchar(30)
AS
BEGIN
	-- Declare the return variable here
	DECLARE			
		@InputStringLeftPart nvarchar(19),		-- Входящий нумерик как строка- Левая часть . Исходная строка левая часть 
		@InputStringRightPart nvarchar(4),		-- Входящий нумерик как строка- Правая часть. Исходная строка правая часть
	
		@OutputStringLeftPart nvarchar(25),		-- Форматированная строка нумерика - Левая часть . 19 символов из целиковой строки и добавленные 6 пробелов
		@OutputStringRightPart nvarchar(15),	-- Форматированная строка нумерика - Правая часть
		
		@tempString nvarchar(3),				-- Временная строка используется в циклах для откусывания трех символов форматирования
		@tempLengt int;							-- Длина откусываемой части строки на итереции откусывания. До последнего кусочка =3, но в последнем кусочке может быть также равна =1 или =2  

-- Переводим входящий нумерик в строку целиком, затем получаем левую и правую части этого числа
SET @InputStringLeftPart =   CONVERT(nvarchar,  CONVERT(bigint, @InputNumeric) );   
SET @InputStringRightPart = REPLACE(  CONVERT(nvarchar, @InputNumeric), @InputStringLeftPart + '.','');  

-- Инициализация выходных отформатированных строк.Если не инициализировать то их значение null не может складываться 
SET @OutputStringLeftPart = '';		--
SET	@OutputStringRightPart = '';
--SET @OutputString  = '';

-- Форматирование Левой части. Для Левой части мы форматируем откусывая в направлении справа-налево
WHILE LEN(@InputStringLeftPart) > 0
BEGIN
	--1 Надо получить длину откусывания: Если Длина строки перед откусыванием больше или =3, то откусывать будем только 3, если меньше трех то надо уточнить сколько -1 или 2 	
	if ( Len(@InputStringLeftPart) > 3 )  
	SET @tempLengt = 3;
	ELSE 
	SET @tempLengt = Len(@InputStringLeftPart);
	
	--2 получаем откусываемый кусочек от строки        
	SET @tempString = SUBSTRING(@InputStringLeftPart, Len(@InputStringLeftPart)- @tempLengt+1,  Len(@InputStringLeftPart) );
   
    --3 Составялем форматированную выходную строку левой части для данной итерации
    IF @tempLengt = 3
		SET @OutputStringLeftPart=  ' ' + @tempString + @OutputStringLeftPart;
    else
       SET @OutputStringLeftPart=  @tempString + @OutputStringLeftPart;
    
	--4 Откусываем часть в исходной неформатированной строке     
    SET @InputStringLeftPart = SUBSTRING( @InputStringLeftPart, 1  ,  Len(@InputStringLeftPart)- @tempLengt  ); 
    
END

	
-- Форматирование Правой части. Для Правой части мы форматируем откусывая в направлении слева-направо. 
-- Это обратное направление от левой части-Поэтому и нужен отдельный  цикл
WHILE LEN(@InputStringRightPart) > 0
BEGIN
	--1 Надо получить длину откусывания: Если Длина строки перед откусыванием больше или =3, то откусывать будем только 3, если меньше трех то надо уточнить сколько =1 или =2 	
	if ( Len(@InputStringRightPart) > 3 )  
	SET @tempLengt = 3;
	ELSE 
	SET @tempLengt = Len(@InputStringRightPart);
	
	--2 получаем откусываемый кусочек от строки       
	SET @tempString = SUBSTRING(@InputStringRightPart, 1, @tempLengt );
   
    --3 Составялем форматированную выходную строку правой части для данной итерации
    if LEN(@OutputStringRightPart)= 0
		SET @OutputStringRightPart= @tempString;
    ELSE SET @OutputStringRightPart= @OutputStringRightPart+ ' ' + @tempString;
    
	--4 Откусываем часть в исходной неформатированной строке
    IF Len(@InputStringRightPart) > 3
		SET @InputStringRightPart = SUBSTRING( @InputStringRightPart, 4  ,  @tempLengt);		
    else SET @InputStringRightPart = '';
END
	
	-- Return the result of the function
	RETURN  @OutputStringLeftPart + '.' + @OutputStringRightPart;

END

