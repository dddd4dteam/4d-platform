﻿

CREATE Function [dbo].[fn_FirstDayMonth](@dt datetime)
Returns DateTime
AS
Begin
Declare @dt_first Datetime
Select @dt_first = Cast(Convert(char(8),DateAdd(d,-(DatePart(d,@dt)-1),@dt),112) as Datetime)
Return(@dt_first)
End

