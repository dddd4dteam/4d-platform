﻿

CREATE function [dbo].[fn_DilerName4Client](@Id_Client uniqueidentifier)
Returns Varchar(500)
as
Begin
	Declare @list_diler varchar(500), @diler varchar(100)
	Set @list_diler = ''
    If Exists(select 1 from crs_S8_ClientDiler where Id_client = @Id_Client)
    Begin
		Select @list_diler = @list_diler + '' + t2.Diler + ';'
		From crs_S8_ClientDiler t1
		Inner join crs_s6_diler t2 on(t1.Id_diler = t2.Id_diler)
		Where t1.Id_client = @Id_client
		Order by t2.Diler
		Select @list_diler = Left(@list_diler,len(@list_diler)-1)
	End    
    Return(@list_diler)
End

