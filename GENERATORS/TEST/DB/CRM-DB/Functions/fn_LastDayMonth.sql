﻿

CREATE Function [dbo].[fn_LastDayMonth](@dt datetime)
Returns DateTime
AS
Begin
Declare @dt_last Datetime
Select @dt_last = Cast(Convert(char(8),DateAdd(d,-DatePart(d,DateAdd(m,1,@dt)),DateAdd(m,1,@dt)),112) as Datetime)
Return(@dt_last)
End

