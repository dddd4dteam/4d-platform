﻿CREATE FUNCTION aspnet_GetUsersInGroups
(
	@ApplicationId UNIQUEIDENTIFIER
)
RETURNS TABLE
AS

	RETURN 
(
    SELECT UG.UserId,
           UG.GroupId,
           UG.ValidFrom,
           UG.ValidTo,
           U.UserName,
           G.GroupName,
           dbo.aspnet_GetProfileElementString('LastName', P.PropertyNames, P.PropertyValuesString) AS LastName,
           dbo.aspnet_GetProfileElementString('FirstName', P.PropertyNames, P.PropertyValuesString) AS FirstName,
           dbo.aspnet_GetProfileElementString('PatronymicName', P.PropertyNames, P.PropertyValuesString) AS PatronymicName
    FROM   aspnet_UsersInGroups UG
           JOIN aspnet_Users U
                ON  UG.UserId = U.UserId
           JOIN aspnet_Groups G
                ON  UG.GroupId = G.GroupId
           LEFT JOIN aspnet_Profile P
                ON  P.UserId = U.UserId
    WHERE  G.ApplicationId = @ApplicationId
           AND U.ApplicationId = @ApplicationId
)