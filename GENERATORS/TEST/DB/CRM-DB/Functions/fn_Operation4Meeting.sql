﻿

CREATE function [dbo].[fn_Operation4Meeting](@Id_Meeting varchar(36))
Returns Varchar(500)
as
Begin
	Declare @list_operation varchar(500), @operation varchar(100), @operation_N int
	Set @list_operation = ''
    Set @operation_N = 1
    If Exists(select 1 from crs_S9_MeetingOperation where Id_meeting = @Id_Meeting)
    Begin
		DECLARE List_of_operation CURSOR
			FOR select t2.Description_RUS 
			from crs_S9_MeetingOperation t1
			Inner join crs_S9_Operation_Register t2 on(t1.Id_operation = t2.Id_operation)
			Where t1.Id_meeting = @Id_meeting
			Order by t2.Id_operation
		OPEN List_of_operation
		FETCH NEXT FROM List_of_operation Into @operation
		WHILE (@@Fetch_Status = 0)
		BEGIN
		  Select @list_operation = @list_operation + '' + @operation + '; '
          Set @operation_N = @operation_N + 1
		  FETCH NEXT FROM List_of_operation Into @operation
		END
		CLOSE List_of_operation
		DEALLOCATE List_of_operation
		Select @list_operation = Left(@list_operation,len(@list_operation)-1)
	End    
    Return(@list_operation)
End


GO


