﻿

-- =============================================
-- Author:		A  
-- Create date: 25.07.2013
-- Description: Форматирование нумерика в приемлемый для чтения вид. Форматирование - после каждого разряда(3-х знаков) ставится пробел. 
--              Данный вариант функции форматирует только левую часть.
-- =============================================
CREATE FUNCTION [dbo].[FormatNumericForPrint] 
(	 
	@InputNumeric numeric(23,4)  -- Число для распечатывания -типа нумерик. Всего в числе может быть только  до 19 целых знаков(bigint диапазон). После запятой учитываются 4 знака. 
)
RETURNS nvarchar(30)
AS
BEGIN
	-- Declare the return variable here
	DECLARE
		@InputString    nvarchar(30),			
		@InputStringLeftPart nvarchar(19),		-- Входящий нумерик как строка- Левая часть . Исходная строка левая часть 
		@InputStringRightPart nvarchar(4),		-- Входящий нумерик как строка- Правая часть. Исходная строка правая часть
	
		@OutputStringLeftPart nvarchar(25),		-- Форматированная строка нумерика - Левая часть . 19 символов из целиковой строки и добавленные 6 пробелов		
		
		@tempString nvarchar(3);				-- Временная строка используется в циклах для откусывания трех символов форматирования
		

-- Переводим входящий нумерик в строку целиком, затем получаем левую и правую части этого числа
SET @InputString =  CONVERT(nvarchar,@InputNumeric);

SET @InputStringLeftPart =    CONVERT(nvarchar,  CONVERT(bigint, @InputNumeric) );   
SET @InputStringRightPart = RIGHT(@InputString, LEN(@InputString) - (LEN(@InputStringLeftPart )+1) ) --  REPLACE(  CONVERT(nvarchar, @InputNumeric), @InputStringLeftPart + '.','');  

-- Инициализация выходных отформатированных строк.Если не инициализировать то их значение null не может складываться 
SET @OutputStringLeftPart = '';		
--SET	@OutputStringRightPart = '';


-- Форматирование Левой части. Для Левой части мы форматируем откусывая в направлении справа-налево
WHILE LEN(@InputStringLeftPart) > 0
BEGIN	
	--1 получаем откусываемый кусочек от строки        
	SET @tempString = RIGHT(@InputStringLeftPart, 3)      
   
    --2 Составялем форматированную выходную строку левой части для данной итерации
	SET  @OutputStringLeftPart=  ' ' + @tempString + @OutputStringLeftPart;
  
	--3 Откусываем часть в исходной неформатированной строке     
    SET @InputStringLeftPart =  LEFT(@InputStringLeftPart,Len(@InputStringLeftPart)- LEN(@tempString))      
END
	
	-- Return the result of the function
	RETURN  LTRIM( @OutputStringLeftPart + '.' + @InputStringRightPart);

END


