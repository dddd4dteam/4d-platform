﻿

CREATE function [dbo].[fn_DilerHolding4Client](@Id_Client int)
Returns Varchar(500)
as
Begin
	Declare @list_diler varchar(500), @diler varchar(100)
	Set @list_diler = ''
    If Exists(select c.id_client,code_client,Id_Diler from crs_S8_ClientDiler d inner join crs_S8_Client c on d.Id_Client=c.Id_Client  where code_client = @Id_Client)
    Begin
		Select @list_diler = @list_diler + '' + t2.Holding + '; '
		From (select c.id_client,code_client,Id_Diler from crs_S8_ClientDiler d inner join crs_S8_Client c on d.Id_Client=c.Id_Client) t1
		Inner join crs_S6_Diler t2 on(t1.Id_diler = t2.Id_diler)
		Where t1.code_client = @Id_client
		GROUP BY t2.Holding
		Order by t2.Holding
		Select @list_diler = Left(RTrim(@list_diler),len(@list_diler)-1)
	End    
    Return(@list_diler)
End



GO


