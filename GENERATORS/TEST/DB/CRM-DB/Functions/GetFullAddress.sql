﻿-- =============================================
-- Author:		 Andrew U.
-- Create date: 09.04.2009
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetFullAddress]
(
	-- Add the parameters for the function here
    -- <@Param1, sysname, @p1> <Data_Type_For_Param1, , int>
    @Street as VARCHAR(150),
    @HouseNumber as VARCHAR(10),
    @FlatNumber as VARCHAR(10)
    
)
RETURNS  Varchar(170)
AS
BEGIN
   
   
   DECLARE @m_Street as VARCHAR(150)
   DECLARE @m_HouseNumber as VARCHAR(10)	 
   DECLARE @m_FlatNumber as VARCHAR(10)
   DECLARE @FullAdress as VARCHAR(170)
   
   
   SET @m_Street = 'ул. '
   SET @m_HouseNumber = ', д. '
   SET @m_FlatNumber = ',кв. '
   
--Use Prefixes or NO
IF @Street IS NOT NULL
  BEGIN
   SET @m_Street = @m_Street + @Street
  END

IF @HouseNumber IS NOT NULL
  BEGIN
   SET @m_HouseNumber = @m_HouseNumber + @HouseNumber
  END

IF @FlatNumber IS NOT NULL
  BEGIN
   SET @m_FlatNumber = @m_FlatNumber + @FlatNumber
  END

SET @FullAdress = @m_Street + @m_HouseNumber + @m_FlatNumber   

RETURN @FullAdress

END





