﻿CREATE FUNCTION [dbo].[aspnet_GetUsers] (@ApplicationId UNIQUEIDENTIFIER)
RETURNS TABLE 
AS
RETURN 
(
	SELECT 
	   U.ApplicationId,
	   U.BirthDay,
	   U.UserId,
       U.UserName,
	   U.LoweredUserName,
	   U.MobilePhone,
	   U.WorkPhone,	
       dbo.aspnet_GetProfileElementString('LastName', P.PropertyNames, P.PropertyValuesString) AS LastName,
       dbo.aspnet_GetProfileElementString('FirstName', P.PropertyNames, P.PropertyValuesString) AS FirstName,
       dbo.aspnet_GetProfileElementString('PatronymicName', P.PropertyNames, P.PropertyValuesString) AS PatronymicName,
       dbo.aspnet_GetProfileElementBinary('Picture', P.PropertyNames, P.PropertyValuesBinary) AS Picture,
       M.Email,
       M.MobilePIN,
       U.MobileAlias,
	   U.DisplayName,
       M.Comment,
       M.CreateDate,
       M.IsApproved,
       M.IsLockedOut,
       U.IsAnonymous,
       U.LastActivityDate,
       M.LastLoginDate
       
	FROM   aspnet_Users U
		   JOIN aspnet_Membership M
				ON  M.UserId = U.UserId
		   LEFT JOIN aspnet_Profile P
				ON  P.UserId = U.UserId
	WHERE U.ApplicationId = @ApplicationId
)