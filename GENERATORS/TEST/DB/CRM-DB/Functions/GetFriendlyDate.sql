﻿-- =============================================
-- Author:		 Andrew U.
-- Create date: 09.04.2009
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[GetFriendlyDate] 
(
	-- Add the parameters for the function here	
	@InputDateTime as DateTime,
    @IsDay as bit,
    @IsMonth as bit,
    @IsYear as bit,
    @IsPrefixes as bit,
    @Culture as varchar(10) 

)

RETURNS varchar(20)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @OutFriendlyDate as varchar(20)    
    DECLARE @Day varchar(3)	 
    DECLARE @Month varchar(3)    
	DECLARE @Year varchar(5)
	
	DECLARE @First varchar(3)
	DECLARE @Second varchar(3)
	DECLARE @Third varchar(3)
    
SET @OutFriendlyDate =''

-- Use Prefixes or NO
IF @IsPrefixes = 1
  BEGIN
   SET @Day = Right('0'+Cast(Datepart(dd,@InputDateTime) as varchar),2)+'d' 
   SET @Month = Right('0'+ Cast(Datepart(mm,@InputDateTime) as varchar),2)+'mon'
   SET @Year = Right('00'+Cast(Datepart(yyyy,@InputDateTime) as varchar),4)+'y'
  END
ELSE
  BEGIN
   SET @Day = Right('0'+Cast(Datepart(dd,@InputDateTime) as varchar),2) 
   SET @Month = Right('0'+ Cast(Datepart(mm,@InputDateTime) as varchar),2)
   SET @Year = Right('00'+Cast(Datepart(yyyy,@InputDateTime) as varchar),4)
  END 

-- Make Culture Order
 IF (@Culture = 'ru' or @Culture ='ru-RU')
 BEGIN
	SET @First = 'dd' 
	SET @Second = 'mon'
	SET @Third = 'year'
 END
 ELSE IF (@Culture = 'en' or @Culture ='en-US')
 BEGIN
	SET @First = 'dd' 
	SET @Second = 'mon'
	SET @Third = 'year'
 END
 

-- 0
IF ( @IsDay = 0 AND @IsMonth =0 AND @IsYear = 0)
      BEGIN
		RETURN @OutFriendlyDate
      END
-- 1   
   IF ( @IsDay = 1 AND @IsMonth =0 AND @IsYear = 0)
      BEGIN
		SET	@OutFriendlyDate= @Day 
		RETURN @OutFriendlyDate
      END
   IF ( @IsDay = 0 AND @IsMonth=1 AND @IsYear = 0)
      BEGIN
		SET	@OutFriendlyDate= @Month
		RETURN @OutFriendlyDate
      END
   IF ( @IsDay = 0 AND @IsMonth =0 AND @IsYear = 1)
      BEGIN
		SET	@OutFriendlyDate= @Year
		RETURN @OutFriendlyDate
      END

--- 2
   IF ( @IsDay = 1 AND @IsMonth =1 AND @IsYear = 0)
      BEGIN
		SET	@OutFriendlyDate=@Day+'/'+@Month
		RETURN @OutFriendlyDate
      END
   IF ( @IsDay = 1 AND @IsMonth =0 AND @IsYear = 1)
      BEGIN
		SET	@OutFriendlyDate=@Day +'/'+ @Year
		RETURN @OutFriendlyDate
      END
   IF ( @IsDay = 0 AND @IsMonth =1 AND @IsYear = 1)
      BEGIN
		SET	@OutFriendlyDate=@Month+'/'+ @Year
		RETURN @OutFriendlyDate
      END
--- 3
   IF ( @IsDay = 1 AND @IsMonth =1 AND @IsYear = 1)
      BEGIN
		SET	@OutFriendlyDate=@Day+'/'+@Month+'/'+@Year
		RETURN @OutFriendlyDate
      END
   


RETURN @OutFriendlyDate

END

