﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
-- Наймушина Юлия Александровна  
CREATE function [dbo].[Date2Int](@dt VARCHAR)
Returns Int
as
Begin
Declare @dt_code int
Select @dt_code = cast(convert(char(23),@dt,121) as int) - 19000000
Return (@dt_code)
End 
 

