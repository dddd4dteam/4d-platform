﻿CREATE FUNCTION aspnet_GetUsersInRoles
(
	@ApplicationId UNIQUEIDENTIFIER
)
RETURNS TABLE
AS

	RETURN 
(
    SELECT UR.UserId,
           UR.RoleId,
           UR.ValidFrom,
           UR.ValidTo,
           U.UserName,
           G.RoleName,
           dbo.aspnet_GetProfileElementString('LastName', P.PropertyNames, P.PropertyValuesString) AS LastName,
           dbo.aspnet_GetProfileElementString('FirstName', P.PropertyNames, P.PropertyValuesString) AS FirstName,
           dbo.aspnet_GetProfileElementString('PatronymicName', P.PropertyNames, P.PropertyValuesString) AS PatronymicName           
    FROM   aspnet_UsersInRoles UR
           JOIN aspnet_Users U
                ON  UR.UserId = U.UserId
           JOIN aspnet_Roles G
                ON  UR.RoleId = G.RoleId
           LEFT JOIN aspnet_Profile P
                ON  P.UserId = U.UserId
    WHERE  G.ApplicationId = @ApplicationId
           AND U.ApplicationId = @ApplicationId
)