﻿CREATE TABLE [dbo].[crs_S10_ConstructionClientEnabled] (
    [ID]              UNIQUEIDENTIFIER NOT NULL,
    [ID_Construction] CHAR (7)         NOT NULL,
    [Id_Client]       UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_crs_S10_ConstructionClient] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_crs_S10_ConstructionClient_Enabled_S10_Construction_Register] FOREIGN KEY ([ID_Construction]) REFERENCES [dbo].[crs_S10_Construction_Register] ([ID_Construction]),
    CONSTRAINT [FK_crs_S10_ConstructionClient_Enabled_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client])
);


GO
ALTER TABLE [dbo].[crs_S10_ConstructionClientEnabled] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

