﻿CREATE TABLE [dbo].[crs_S10_StandartPanelSet_Register] (
    [Id_StandartSet]  CHAR (7)      NOT NULL,
    [StandartSet]     VARCHAR (100) NOT NULL,
    [Id_Construction] CHAR (7)      NOT NULL,
    [Article]         VARCHAR (25)  NOT NULL,
    [Flag_Enable]     BIT           NOT NULL,
    CONSTRAINT [PK_crs_S10_StandartPanelSet] PRIMARY KEY CLUSTERED ([Id_StandartSet] ASC),
    CONSTRAINT [FK_crs_S10_StandartPanelSet_Register_crs_S10_Construction_Register] FOREIGN KEY ([Id_Construction]) REFERENCES [dbo].[crs_S10_Construction_Register] ([ID_Construction])
);


GO
ALTER TABLE [dbo].[crs_S10_StandartPanelSet_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



