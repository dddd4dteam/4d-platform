﻿CREATE TABLE [dbo].[crs_S9_MeetingPositionSource_Register] (
    [Id_MeetingPositionSource] INT           NOT NULL,
    [Description]              NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S9_MeetingPositionSource_Register] PRIMARY KEY CLUSTERED ([Id_MeetingPositionSource] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_MeetingPositionSource_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



