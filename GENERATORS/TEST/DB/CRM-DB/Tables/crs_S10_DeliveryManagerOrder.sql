﻿CREATE TABLE [dbo].[crs_S10_DeliveryManagerOrder] (
    [Id_Order]           UNIQUEIDENTIFIER NOT NULL,
    [Id_DeliveryManager] CHAR (6)         NOT NULL,
    CONSTRAINT [PK_crs_S10_DeliveryManagerOrder] PRIMARY KEY CLUSTERED ([Id_Order] ASC),
    CONSTRAINT [FK_crs_S10_DeliveryManagerOrder_crs_S10_DeliveryManager_Register] FOREIGN KEY ([Id_DeliveryManager]) REFERENCES [dbo].[crs_S10_DeliveryManager_Register] ([Id_DeliveryManager]) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT [FK_crs_S10_DeliveryManagerOrder_crs_S10_EkspositorOrder] FOREIGN KEY ([Id_Order]) REFERENCES [dbo].[crs_S10_EkspositorOrder] ([Id_Order])
);




GO
ALTER TABLE [dbo].[crs_S10_DeliveryManagerOrder] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);