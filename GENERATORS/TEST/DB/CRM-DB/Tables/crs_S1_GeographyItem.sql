﻿CREATE TABLE [dbo].[crs_S1_GeographyItem] (
    [Id_GeographyItem]       INT            NOT NULL,
    [Id_GeographyParentItem] INT            NULL,
    [Id_GeographyItemType]   INT            NOT NULL,
    [GeographyItem]          NVARCHAR (50)  NOT NULL,
    [Prefix]                 VARCHAR (6)    NULL,
    [Description]            NVARCHAR (250) NULL,
    [Population]             INT            NULL,
    CONSTRAINT [PK_crs_S1_GeographyItem] PRIMARY KEY CLUSTERED ([Id_GeographyItem] ASC),
    CONSTRAINT [FK_crs_S1_GeographyItem_crs_S1_GeographyItemType_Register] FOREIGN KEY ([Id_GeographyItemType]) REFERENCES [dbo].[crs_S1_GeographyItemType_Register] ([Id_GeographyItemType])
);





GO
ALTER TABLE [dbo].[crs_S1_GeographyItem] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




GO
-- =============================================
-- Item - crs_S1_GeographyItem/    
-- Hierarchy - crs_S1_GeographyHierarchy/
-- ItemType - crs_S1_GeographyItemType_Register/
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE trigger [dbo].[crs_S1_GeographyItemInsert] on dbo.crs_S1_GeographyItem for insert as
begin
	set nocount on

	insert into crs_S1_GeographyHierarchy(Id_GeographyItem, Id_GeographyParentItem, Level)
	select Id_GeographyItem, Id_GeographyItem, 0
	from inserted

	insert into crs_S1_GeographyHierarchy(Id_GeographyItem, Id_GeographyParentItem, Level)
	select n.Id_GeographyItem, t.Id_GeographyParentItem, t.Level + 1
	from inserted n, crs_S1_GeographyHierarchy t
	where n.Id_GeographyParentItem = t.Id_GeographyItem
end

GO
-- =============================================
-- Item - crs_S1_GeographyItem/    
-- Hierarchy - crs_S1_GeographyHierarchy/
-- ItemType - crs_S1_GeographyItemType_Register/

-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE trigger [dbo].[crs_S1_GeographyItemUpdate] on dbo.crs_S1_GeographyItem for update as
if update(Id_GeographyParentItem)
begin
	set nocount on

	declare @child table(Id_GeographyItem int, Level int)

	insert into @child(Id_GeographyItem, Level)
	select t.Id_GeographyItem, t.Level
	from inserted n, dbo.crs_S1_GeographyHierarchy t
	where n.Id_GeographyItem = t.Id_GeographyParentItem and t.Level > 0

	delete dbo.crs_S1_GeographyHierarchy
	where
		dbo.crs_S1_GeographyHierarchy.Id_GeographyItem in(select Id_GeographyItem from @child)
		and dbo.crs_S1_GeographyHierarchy.Id_GeographyParentItem in(
			select t.Id_GeographyParentItem
			from inserted n, dbo.crs_S1_GeographyHierarchy t
			where n.Id_GeographyItem = t.Id_GeographyItem and t.Level > 0
		)

	delete dbo.crs_S1_GeographyHierarchy
	where dbo.crs_S1_GeographyHierarchy.Id_GeographyItem in(select Id_GeographyItem from inserted) and dbo.crs_S1_GeographyHierarchy.Level > 0

	insert into dbo.crs_S1_GeographyHierarchy(Id_GeographyItem, Id_GeographyParentItem, Level)
	select n.Id_GeographyItem, t.Id_GeographyParentItem, t.Level + 1
	from inserted n, dbo.crs_S1_GeographyHierarchy t
	where n.Id_GeographyParentItem = t.Id_GeographyItem

	insert into dbo.crs_S1_GeographyHierarchy(Id_GeographyItem, Id_GeographyParentItem, Level)
	select c.Id_GeographyItem, t.Id_GeographyParentItem, t.Level + c.Level
	from inserted n, dbo.crs_S1_GeographyHierarchy t, @child c
	where n.Id_GeographyItem = t.Id_GeographyItem and t.Level > 0
end
