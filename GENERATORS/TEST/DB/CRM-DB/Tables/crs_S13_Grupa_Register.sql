﻿CREATE TABLE [dbo].[crs_S13_Grupa_Register] (
    [Id_Grupa]       INT          NOT NULL,
    [Grupa]          VARCHAR (50) NOT NULL,
    [Id_ParentGrupa] INT          NULL,
    [Level]          INT          NULL,
    CONSTRAINT [PK_crs_S13_GrupaItem_Register] PRIMARY KEY CLUSTERED ([Id_Grupa] ASC)
);





GO
ALTER TABLE [dbo].[crs_S13_Grupa_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




GO
-- =============================================
-- Item - crs_S12_Grupa_Register/    
-- Hierarchy - crs_S12_GrupaHierarchy/
-- ItemType - <ItemType,,>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE trigger [dbo].[crs_S13_GrupaItem_RegisterInsert] on dbo.crs_S13_Grupa_Register for insert as
begin
	set nocount on

	insert into crs_S13_GrupaHierarchy(Id_Grupa, Id_ParentGrupa, Level)
	select Id_Grupa, Id_Grupa, 0
	from inserted

	insert into crs_S13_GrupaHierarchy(Id_Grupa, Id_ParentGrupa, Level)
	select n.Id_Grupa, t.Id_ParentGrupa, t.Level + 1
	from inserted n, crs_S13_GrupaHierarchy t
	where n.Id_ParentGrupa = t.Id_Grupa
end

GO
-- =============================================
-- Item - crs_S13_GrupaItem_Register/    
-- Hierarchy - crs_S13_GrupaHierarchy/
-- ItemType - <ItemType,,>

-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create trigger [dbo].[crs_S13_GrupaItem_RegisterUpdate] on dbo.crs_S13_Grupa_Register for update as
if update(Id_ParentGrupa)
begin
	set nocount on

	declare @child table(Id_Grupa int, Level int)

	insert into @child(Id_Grupa, Level)
	select t.Id_Grupa, t.Level
	from inserted n, dbo.crs_S13_GrupaHierarchy t
	where n.Id_Grupa = t.Id_ParentGrupa and t.Level > 0

	delete dbo.crs_S13_GrupaHierarchy
	where
		dbo.crs_S13_GrupaHierarchy.Id_Grupa in(select Id_Grupa from @child)
		and dbo.crs_S13_GrupaHierarchy.Id_ParentGrupa in(
			select t.Id_ParentGrupa
			from inserted n, dbo.crs_S13_GrupaHierarchy t
			where n.Id_Grupa = t.Id_Grupa and t.Level > 0
		)

	delete dbo.crs_S13_GrupaHierarchy
	where dbo.crs_S13_GrupaHierarchy.Id_Grupa in(select Id_Grupa from inserted) and dbo.crs_S13_GrupaHierarchy.Level > 0

	insert into dbo.crs_S13_GrupaHierarchy(Id_Grupa, Id_ParentGrupa, Level)
	select n.Id_Grupa, t.Id_ParentGrupa, t.Level + 1
	from inserted n, dbo.crs_S13_GrupaHierarchy t
	where n.Id_ParentGrupa = t.Id_Grupa

	insert into dbo.crs_S13_GrupaHierarchy(Id_Grupa, Id_ParentGrupa, Level)
	select c.Id_Grupa, t.Id_ParentGrupa, t.Level + c.Level
	from inserted n, dbo.crs_S13_GrupaHierarchy t, @child c
	where n.Id_Grupa = t.Id_Grupa and t.Level > 0
end
