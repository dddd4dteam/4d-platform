﻿CREATE TABLE [dbo].[crs_S10_ImageAcceptance] (
    [Id_Image] UNIQUEIDENTIFIER NOT NULL,
    [Accepted] BIT              NULL,
    [Comment]  NVARCHAR (500)   NULL,
    PRIMARY KEY CLUSTERED ([Id_Image] ASC),
    CONSTRAINT [FK_crs_S10_ImageAcceptance_crs_S10_EkspositorOrderImage] FOREIGN KEY ([Id_Image]) REFERENCES [dbo].[crs_S10_EkspositorOrderImage] ([ID]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[crs_S10_ImageAcceptance] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






GO

GO
