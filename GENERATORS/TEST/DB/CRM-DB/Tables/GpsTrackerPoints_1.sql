﻿CREATE TABLE [dbo].[GpsTrackerPoints_1] (
    [Id_tracker]     INT               NULL,
    [Dt_code]        BIGINT            NULL,
    [v]              INT               NULL,
    [Longitude]      FLOAT (53)        NULL,
    [Latitude]       FLOAT (53)        NULL,
    [GeographyPoint] [sys].[geography] NULL
);

