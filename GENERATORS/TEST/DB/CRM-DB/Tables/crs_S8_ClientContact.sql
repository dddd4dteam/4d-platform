﻿CREATE TABLE [dbo].[crs_S8_ClientContact] (
    [Id_Client]                   UNIQUEIDENTIFIER NOT NULL,
    [Id_ClientContact]            UNIQUEIDENTIFIER NOT NULL,
    [LastName]                    VARCHAR (60)     NULL,
    [FirstName]                   VARCHAR (60)     NOT NULL,
    [PatronymicName]              VARCHAR (60)     NULL,
    [Id_ClientContactAppointment] INT              NULL,
    [PhoneNumber]                 VARCHAR (60)     NULL,
    [Mail]                        VARCHAR (60)     NULL,
    [BirthDay]                    DATETIME         NULL,
    [Flag_Congratulation1]        BIT              NULL,
    [Flag_Congratulation2]        BIT              NULL,
    [AdditionalInform]            VARCHAR (200)    NULL,
    CONSTRAINT [PK_crs_S8_ClientContact] PRIMARY KEY CLUSTERED ([Id_ClientContact] ASC),
    CONSTRAINT [FK_crs_S8_ClientContact_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]) ON DELETE CASCADE,
    CONSTRAINT [FK_crs_S8_ClientContact_crs_S8_ClientContactAppointment_Register] FOREIGN KEY ([Id_ClientContactAppointment]) REFERENCES [dbo].[crs_S8_ClientContactAppointment_Register] ([Id_ClientContactAppointment])
);


GO
ALTER TABLE [dbo].[crs_S8_ClientContact] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



