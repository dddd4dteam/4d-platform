﻿CREATE TABLE [dbo].[crs_S16_RivalGoodsCategory_Register] (
    [Id_RivalGoodsCategory] VARCHAR (5)   NOT NULL,
    [RivalGoodsCategory]    VARCHAR (50)  NOT NULL,
    [Description]           VARCHAR (200) NULL,
    [Order]                 INT           NULL,
    CONSTRAINT [PK_crs_S16_RivalGoodsCategory_Register] PRIMARY KEY CLUSTERED ([Id_RivalGoodsCategory] ASC)
);


GO
ALTER TABLE [dbo].[crs_S16_RivalGoodsCategory_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



