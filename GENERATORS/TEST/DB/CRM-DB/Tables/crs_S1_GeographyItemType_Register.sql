﻿CREATE TABLE [dbo].[crs_S1_GeographyItemType_Register] (
    [Id_GeographyItemType] INT            NOT NULL,
    [GeographyItemType]    NVARCHAR (50)  NOT NULL,
    [Description]          NVARCHAR (150) NULL,
    CONSTRAINT [PK_crs_S1_GeographyItemType_Register] PRIMARY KEY CLUSTERED ([Id_GeographyItemType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S1_GeographyItemType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



