﻿CREATE TABLE [dbo].[crs_S10_OrderComment] (
    [Id_Comment]         UNIQUEIDENTIFIER NOT NULL,
    [Id_Order]           UNIQUEIDENTIFIER NOT NULL,
    [Date]               DATETIME         NOT NULL,
    [CRM_RoleDescriptor] NCHAR (50)       NOT NULL,
    [Text]               VARCHAR (500)    NOT NULL,
    [Context]            VARCHAR (50)     NOT NULL,
    [ActivityName]       VARCHAR (50)     NULL,
    CONSTRAINT [PK_crs_ContextComments] PRIMARY KEY CLUSTERED ([Id_Comment] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_OrderComment] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



