﻿CREATE TABLE [dbo].[crs_S17_QuestionsVariant] (
    [ID_Question]      UNIQUEIDENTIFIER NOT NULL,
    [ID_AnswerVariant] UNIQUEIDENTIFIER NOT NULL,
    [VariantValue]     VARCHAR (250)    NULL,
    CONSTRAINT [PK_crs_S17_QuestionsVariant] PRIMARY KEY CLUSTERED ([ID_AnswerVariant] ASC)
);


GO
ALTER TABLE [dbo].[crs_S17_QuestionsVariant] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

