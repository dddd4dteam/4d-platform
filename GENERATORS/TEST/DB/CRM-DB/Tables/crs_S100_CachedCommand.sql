﻿CREATE TABLE [dbo].[crs_S100_CachedCommand] (
    [GUID]            UNIQUEIDENTIFIER NOT NULL,
    [CommandData]     NVARCHAR (MAX)   NOT NULL,
    [ServiceTypeName] NVARCHAR (MAX)   NOT NULL,
    [DateTimeUTC]     DATETIME         NOT NULL,
    CONSTRAINT [PK_crs_S100_CachedCommand] PRIMARY KEY CLUSTERED ([GUID] ASC)
);

