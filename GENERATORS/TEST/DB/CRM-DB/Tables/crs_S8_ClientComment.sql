﻿CREATE TABLE [dbo].[crs_S8_ClientComment] (
    [Id_Comment]         UNIQUEIDENTIFIER NOT NULL,
    [Id_Client]          UNIQUEIDENTIFIER NOT NULL,
    [Date]               DATETIME         NOT NULL,
    [CRM_RoleDescriptor] NCHAR (50)       NOT NULL,
    [Text]               VARCHAR (500)    NOT NULL,
    [Description]        NCHAR (100)      NULL,
    CONSTRAINT [PK_crs_S8_ClientComment] PRIMARY KEY CLUSTERED ([Id_Comment] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientComment] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



