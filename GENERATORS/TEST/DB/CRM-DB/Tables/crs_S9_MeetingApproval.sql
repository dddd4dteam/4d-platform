﻿CREATE TABLE [dbo].[crs_S9_MeetingApproval]
(
	[Id_Meeting] UNIQUEIDENTIFIER NOT NULL PRIMARY KEY, 
    [Flag_Approved] BIT NOT NULL, 
    CONSTRAINT [FK_crs_S9_MeetingApproval_crs_S9_Meeting] FOREIGN KEY ([Id_Meeting]) REFERENCES [dbo].[crs_S9_Meeting]([Id_Meeting]) ON DELETE CASCADE
)


GO
ALTER TABLE [dbo].[crs_S9_MeetingApproval] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);