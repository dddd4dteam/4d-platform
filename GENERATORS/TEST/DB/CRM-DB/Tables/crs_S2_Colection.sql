﻿CREATE TABLE [dbo].[crs_S2_Colection] (
    [Id_Colection] INT            NOT NULL,
    [Colection]    VARCHAR (50)   NOT NULL,
    [Status]       VARCHAR (20)   NULL,
    [Id_System]    VARCHAR (5)    NULL,
    [Description]  NVARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S2_Colection] PRIMARY KEY CLUSTERED ([Id_Colection] ASC)
);


GO
ALTER TABLE [dbo].[crs_S2_Colection] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



