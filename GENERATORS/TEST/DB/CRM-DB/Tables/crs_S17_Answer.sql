﻿CREATE TABLE [dbo].[crs_S17_Answer] (
    [ID_Meeting]   UNIQUEIDENTIFIER NOT NULL,
    [ID_Question]  UNIQUEIDENTIFIER NOT NULL,
    [ID_Answer]    UNIQUEIDENTIFIER NOT NULL,
    [AnswerValue1] DATETIME         NULL,
    [AnswerValue2] DATETIME         NULL,
    [AnswerValue3] FLOAT (53)       NULL,
    [AnswerValue4] FLOAT (53)       NULL,
    [AnswerValue5] VARCHAR (200)    NULL,
    [AnswerValue6] UNIQUEIDENTIFIER NULL,
    [Flag_Past]    BIT              NULL,
    CONSTRAINT [PK_crs_S17_Answer] PRIMARY KEY CLUSTERED ([ID_Answer] ASC),
    CONSTRAINT [FK_crs_S17_AnswerMeeting] FOREIGN KEY ([ID_Meeting]) REFERENCES [dbo].[crs_S9_Meeting] ([Id_Meeting]) ON DELETE CASCADE
);





GO
ALTER TABLE [dbo].[crs_S17_Answer] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

