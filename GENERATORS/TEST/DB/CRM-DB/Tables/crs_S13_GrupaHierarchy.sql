﻿CREATE TABLE [dbo].[crs_S13_GrupaHierarchy] (
    [Id_Grupa]       INT NOT NULL,
    [Id_ParentGrupa] INT NOT NULL,
    [Level]          INT NULL,
    CONSTRAINT [PK_crs_S12_GrupaHierarchy_Register] PRIMARY KEY CLUSTERED ([Id_Grupa] ASC, [Id_ParentGrupa] ASC),
    CONSTRAINT [FK_crs_S13_GrupaHierachy_crs_S13_Grupa_Register] FOREIGN KEY ([Id_Grupa]) REFERENCES [dbo].[crs_S13_Grupa_Register] ([Id_Grupa])
);


GO
ALTER TABLE [dbo].[crs_S13_GrupaHierarchy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



