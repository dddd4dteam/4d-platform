﻿CREATE TABLE [dbo].[crs_S16_PresentationType_Register] (
    [Id_PresentationType] INT            NOT NULL,
    [Marking]             NVARCHAR (10)  NOT NULL,
    [PresentationType]    NVARCHAR (100) NOT NULL,
    CONSTRAINT [PK_crs_S16_PresentationType_Register] PRIMARY KEY CLUSTERED ([Id_PresentationType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S16_PresentationType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



