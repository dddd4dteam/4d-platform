﻿CREATE TABLE [dbo].[crs_S2_ColorGoods_Register] (
    [Id_Color]  INT          NOT NULL,
    [Color]     VARCHAR (50) NOT NULL,
    [Id_System] INT          NULL,
    CONSTRAINT [PK_crs_S2_ColorGoods_Registr] PRIMARY KEY CLUSTERED ([Id_Color] ASC)
);


GO
ALTER TABLE [dbo].[crs_S2_ColorGoods_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



