﻿CREATE TABLE [dbo].[crs_S100_UserMetaData]
(
	[Id_User] CHAR(6) NOT NULL PRIMARY KEY, 
	[Id_UserType] INT NOT NULL,
    [Description] VARCHAR(50) NOT NULL, 
    [Name] VARCHAR(50) NULL, 
    [HasElevatedPermissions] BIT NULL, 
    [ShouldUseLocalDB] BIT NOT NULL, 
    [ShouldDropLocalDB] BIT NOT NULL, 
    [LocalDBIsReady] BIT NULL, 
    CONSTRAINT [FK_crs_S100_UserMetaData_crs_S100_UserType_Register] FOREIGN KEY ([Id_UserType]) REFERENCES [dbo].[crs_S100_UserType_Register] ([Id_UserType]) 
)
