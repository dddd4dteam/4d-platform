﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderImage] (
    [ID]            UNIQUEIDENTIFIER           CONSTRAINT [DF__crs_S10_Eksp__ID__403B57D4] DEFAULT (newid()) ROWGUIDCOL NOT NULL,
    [FileData]      VARBINARY (MAX) FILESTREAM CONSTRAINT [DF_crs_S10_EkspositorOrderImages_Image] DEFAULT (0x) NULL,
    [FileThumbData] VARBINARY (MAX) FILESTREAM CONSTRAINT [DF_crs_S10_EkspositorOrderImages_PreviewImage] DEFAULT (0x) NULL,
    [Description]   VARCHAR (500)              NULL,
    [MD5]           VARCHAR (40)               NULL,
    [SavedDt]       DATETIME                   NULL,
    [SaveUserName]  VARCHAR (40)               NULL,
    [SaveUserID]    UNIQUEIDENTIFIER           NULL,
    [Uri]           VARCHAR (1000)             NULL,
    [ThumbUri]      VARCHAR (1000)             NULL,
    [ID_ImageType]  INT                        NOT NULL,
    [Id_Order]      UNIQUEIDENTIFIER           NULL,
    CONSTRAINT [PK__crs_S10___3214EC27BE1484C8] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_crs_S10_EkspositorOrderImage_crs_S10_EkspositorOrderImageType_Register] FOREIGN KEY ([ID_ImageType]) REFERENCES [dbo].[crs_S10_EkspositorOrderImageType_Register] ([TypeID])
) FILESTREAM_ON [Image];


GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderImage] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



