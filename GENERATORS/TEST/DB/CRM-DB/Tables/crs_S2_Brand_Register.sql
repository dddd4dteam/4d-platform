﻿CREATE TABLE [dbo].[crs_S2_Brand_Register] (
    [Id_Brand]    INT            NOT NULL,
    [Brand]       VARCHAR (50)   NOT NULL,
    [Id_System]   INT            NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S2_Brand_Register] PRIMARY KEY CLUSTERED ([Id_Brand] ASC)
);


GO
ALTER TABLE [dbo].[crs_S2_Brand_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



