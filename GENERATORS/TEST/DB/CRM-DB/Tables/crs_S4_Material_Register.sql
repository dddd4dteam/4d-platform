﻿CREATE TABLE [dbo].[crs_S4_Material_Register] (
    [Id_Material]     INT           NOT NULL,
    [Material]        VARCHAR (100) NOT NULL,
    [Id_MaterialType] INT           NOT NULL,
    CONSTRAINT [PK_crs_S4_Material_Register] PRIMARY KEY CLUSTERED ([Id_Material] ASC),
    CONSTRAINT [FK_crs_S4_Material_Register_crs_S4_MaterialType_Register] FOREIGN KEY ([Id_MaterialType]) REFERENCES [dbo].[crs_S4_MaterialType_Register] ([Id_MaterialType])
);


GO
ALTER TABLE [dbo].[crs_S4_Material_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



