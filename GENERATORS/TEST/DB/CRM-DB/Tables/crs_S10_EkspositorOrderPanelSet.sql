﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderPanelSet] (
    [Description] VARCHAR (30)     NULL,
    [Id_PanelSet] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_crs_S10_EkspositorOrderPanelSet] PRIMARY KEY CLUSTERED ([Id_PanelSet] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderPanelSet] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



