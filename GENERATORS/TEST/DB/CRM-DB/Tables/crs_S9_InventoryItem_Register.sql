﻿CREATE TABLE [dbo].[crs_S9_InventoryItem_Register] (
    [ID_InventoryItem]     UNIQUEIDENTIFIER NOT NULL,
    [ID_Manufacturer]      INT              NOT NULL,
    [ID_InventoryItemType] INT              NOT NULL,
    [InventoryItem]        VARCHAR (50)     NOT NULL,
    [Collection]           VARCHAR (50)     NULL,
    [EnableValue1Count]    BIT              NOT NULL,
    [EnableValue2Count]    BIT              NOT NULL,
    [EnableValue3Count]    BIT              CONSTRAINT [DF__crs_S9_In__Enabl__35DDC4F2] DEFAULT ((1)) NOT NULL,
    [Id_InventoryType]     INT              NOT NULL DEFAULT 1,
    [ItemColor]            CHAR (7)         NULL,
    [OrderNumber]          INT              NULL,
    [EnableItem]           BIT              NOT NULL,
    CONSTRAINT [PK_crs_S17_InventoryItem_Register] PRIMARY KEY CLUSTERED ([ID_InventoryItem] ASC),
    CONSTRAINT [FK_crs_S17_InventoryItem_Register_crs_S17_InventoryItem_Register] FOREIGN KEY ([ID_InventoryItemType]) REFERENCES [dbo].[crs_S9_InventoryItemType_Register] ([ID_InventoryItemType]),
    CONSTRAINT [FK_crs_S17_InventoryItem_Register_crs_S17_InventoryManufacturer_Register] FOREIGN KEY ([ID_Manufacturer]) REFERENCES [dbo].[crs_S9_InventoryManufacturer_Register] ([ID_Manufacturer]),
    CONSTRAINT [FK_crs_S9_InventoryItem_Register_crs_S9_InventoryType_Register] FOREIGN KEY ([Id_InventoryType]) REFERENCES [dbo].[crs_S9_InventoryType_Register] ([Id_InventoryType])
);





GO
ALTER TABLE [dbo].[crs_S9_InventoryItem_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



