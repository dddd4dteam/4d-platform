﻿CREATE TABLE [dbo].[crs_S1_GeographyHierarchy] (
    [Id_GeographyItem]       INT NOT NULL,
    [Id_GeographyParentItem] INT NOT NULL,
    [Level]                  INT NOT NULL,
    CONSTRAINT [PK_crs_S1_GeographyHierarchy] PRIMARY KEY CLUSTERED ([Id_GeographyItem] ASC, [Id_GeographyParentItem] ASC),
    CONSTRAINT [FK_crs_S1_GeographyHierarchy_crs_S1_GeographyItem] FOREIGN KEY ([Id_GeographyItem]) REFERENCES [dbo].[crs_S1_GeographyItem] ([Id_GeographyItem])
);


GO
ALTER TABLE [dbo].[crs_S1_GeographyHierarchy] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



