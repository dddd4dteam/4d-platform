﻿CREATE TABLE [dbo].[crs_S10_OrderApproval] (
    [Id_Order]        UNIQUEIDENTIFIER NOT NULL,
    [Flag_Approved1]  BIT              NOT NULL,
    [Flag_Approved2]  BIT              NOT NULL,
    [Flag_Approved3]  BIT              NOT NULL,
    [Flag_UnApproved] BIT              DEFAULT ((0)) NOT NULL,
    [DtOrderApproved] DATETIME         NULL,
    PRIMARY KEY CLUSTERED ([Id_Order] ASC),
    CONSTRAINT [FK_crs_S10_OrderApproval_crs_S10_EkspositorOrder] FOREIGN KEY ([Id_Order]) REFERENCES [dbo].[crs_S10_EkspositorOrder] ([Id_Order]) ON DELETE CASCADE
);




GO
ALTER TABLE [dbo].[crs_S10_OrderApproval] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);
GO



CREATE TRIGGER [dbo].[crs_S10_OrderApproval_EventInTable]
	ON [dbo].[crs_S10_OrderApproval]

After Insert,Update
AS

/*Обновление записи*/
IF (UPDATE (Flag_Approved3))
	begin
	Update sys_Flags_NeedPassed
	set Flag_NeedPassed=1,LastDt_NeedPassed=cast(getdate() as datetime)
	where Id_object IN
					(	--Только те заказы, у которых Flag_Approved3 после обновления или добавления стал "1"
						select id.Id_order
						from inserted id
						Left join deleted del on(id.Id_Order = del.Id_Order)
						where Id.Flag_Approved3 = 1 and IsNull(del.Flag_Approved3,0) = 0
					)
	end