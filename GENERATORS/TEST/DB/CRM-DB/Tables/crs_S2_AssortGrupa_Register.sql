﻿CREATE TABLE [dbo].[crs_S2_AssortGrupa_Register] (
    [Id_AssortGrupa] INT           NOT NULL,
    [AssortGrupa]    VARCHAR (50)  NOT NULL,
    [Id_System]      VARCHAR (7)   NOT NULL,
    [Description]    NVARCHAR (50) NULL,
    CONSTRAINT [PK_crs_S2_AssortGrupa_Register] PRIMARY KEY CLUSTERED ([Id_AssortGrupa] ASC)
);


GO
ALTER TABLE [dbo].[crs_S2_AssortGrupa_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



