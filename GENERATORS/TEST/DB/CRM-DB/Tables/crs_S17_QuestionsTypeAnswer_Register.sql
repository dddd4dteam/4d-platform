﻿CREATE TABLE [dbo].[crs_S17_QuestionsTypeAnswer_Register] (
    [ID_AnswerType] INT           NOT NULL,
    [AnswerType]    VARCHAR (50)  NOT NULL,
    [Description]   VARCHAR (250) NOT NULL,
    CONSTRAINT [PK_crs_S17_QuestionsType_Answer] PRIMARY KEY CLUSTERED ([ID_AnswerType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S17_QuestionsTypeAnswer_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

