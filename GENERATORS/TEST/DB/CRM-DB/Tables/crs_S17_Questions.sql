﻿CREATE TABLE [dbo].[crs_S17_Questions] (
    [ID_Form]         UNIQUEIDENTIFIER NOT NULL,
    [ID_Question]     UNIQUEIDENTIFIER NOT NULL,
    [Question]        VARCHAR (250)    NOT NULL,
    [Description]     VARCHAR (250)    NULL,
    [Level]           INT              NOT NULL,
    [ID_Parent]       UNIQUEIDENTIFIER NULL,
    [ID_Condition]    INT              NULL,
    [ID_AnswerType]   INT              NULL,
    [ConditionValue1] DATETIME         NULL,
    [ConditionValue2] DATETIME         NULL,
    [ConditionValue3] FLOAT (53)       NULL,
    [ConditionValue4] FLOAT (53)       NULL,
    [ConditionValue5] VARCHAR (200)    NULL,
    [ConditionValue6] UNIQUEIDENTIFIER NULL,
    [Flag_required]   BIT              NOT NULL,
    [Order_number]    INT              NOT NULL,
    CONSTRAINT [PK_crs_S17_Questions] PRIMARY KEY CLUSTERED ([ID_Question] ASC),
    CONSTRAINT [FK_crs_S17_FormsQuestions] FOREIGN KEY ([ID_Form]) REFERENCES [dbo].[crs_S17_Forms] ([ID_Form])
);


GO
ALTER TABLE [dbo].[crs_S17_Questions] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

