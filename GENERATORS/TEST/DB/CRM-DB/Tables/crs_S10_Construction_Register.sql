﻿CREATE TABLE [dbo].[crs_S10_Construction_Register] (
    [ID_Construction]         CHAR (7)       NOT NULL,
    [Code]                    VARCHAR (25)   NOT NULL,
    [Description]             VARCHAR (100)  NOT NULL,
    [PanelWidth]              NUMERIC (5, 2) NOT NULL,
    [PanelHeight]             NUMERIC (5, 2) NOT NULL,
    [PanelCount]              INT            NOT NULL,
    [ConstructionWeight]      INT            NOT NULL,
    [Flag_Enable]             BIT            NOT NULL,
    [ID_ConstructionType]     INT            NOT NULL,
    [ImageIndexActual]        INT            NULL,
    [ConstructionHeight]      INT            NULL,
    [ConstructionWidth]       INT            NULL,
    [ConstructionDepth]       INT            NULL,
    [Id_Collection]           CHAR (5)       NOT NULL,
    [Collection]              VARCHAR (25)   NOT NULL,
    [Id_Brand]                INT            NOT NULL,
    [Brand]                   VARCHAR (25)   NOT NULL,
    [Id_StatusCol]            CHAR (3)       NOT NULL,
    [StatusCol]               VARCHAR (30)   NOT NULL,
    [ABC]                     CHAR (1)       NOT NULL,
    [Flag_ForbidSelectPanels] BIT            NULL,
    [Price]                   FLOAT (53)     NULL,
    CONSTRAINT [PK_crs_S10_Construction] PRIMARY KEY CLUSTERED ([ID_Construction] ASC),
    CONSTRAINT [FK_crs_S10_Construction_Register_crs_S10_ConstructionType_Register] FOREIGN KEY ([ID_ConstructionType]) REFERENCES [dbo].[crs_S10_ConstructionType_Register] ([ID_ConstructionType])
);





GO
ALTER TABLE [dbo].[crs_S10_Construction_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);




GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Индекс картинки конструкции  в жориной базе актуальной для неё на данный момент', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'crs_S10_Construction_Register', @level2type = N'COLUMN', @level2name = N'ImageIndexActual';

