﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderType_Register] (
    [ID_OrderType] INT       NOT NULL,
    [Description]  CHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S10_EkspositorOrderType] PRIMARY KEY CLUSTERED ([ID_OrderType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



