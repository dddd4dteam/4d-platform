﻿CREATE TABLE [dbo].[crs_S9_MeetingOperation] (
	[Id_MeetingOperation] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
    [Id_Meeting]   UNIQUEIDENTIFIER NOT NULL,
    [Id_Operation] INT              NOT NULL,
    CONSTRAINT [PK_crs_Meeting_Operation] PRIMARY KEY CLUSTERED ([Id_MeetingOperation]),
	CONSTRAINT [FK_crs_S9_MeetingOperation_crs_S9_Meeting] FOREIGN KEY ([Id_Meeting]) REFERENCES [dbo].[crs_S9_Meeting]([Id_Meeting]) ON DELETE CASCADE,
    CONSTRAINT [FK_crs_S9_MeetingOperation_crs_S9_Operation_Register] FOREIGN KEY ([Id_Operation]) REFERENCES [dbo].[crs_S9_Operation_Register] ([Id_Operation])
    
);


GO
ALTER TABLE [dbo].[crs_S9_MeetingOperation] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



