﻿CREATE TABLE [dbo].[crs_S9_InventoryItemType_Register] (
    [ID_InventoryItemType] INT           NOT NULL,
    [InventoryItemType]    NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S17_InventoryItemType_Register] PRIMARY KEY CLUSTERED ([ID_InventoryItemType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_InventoryItemType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



