﻿CREATE TABLE [dbo].[crs_S9_InventoryManufacturer_Register] (
    [ID_Manufacturer] INT           NOT NULL,
    [Manufacturer]    VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_crs_S17_InventoryManufacturer_Register] PRIMARY KEY CLUSTERED ([ID_Manufacturer] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_InventoryManufacturer_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



