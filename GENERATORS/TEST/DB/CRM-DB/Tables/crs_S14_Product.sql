﻿CREATE TABLE [dbo].[crs_S14_Product] (
    [Id_Product]   INT           NOT NULL,
    [Goods]        VARCHAR (120) NOT NULL,
    [Id_Brand]     INT           NOT NULL,
    [Id_Grupa]     INT           NOT NULL,
    [Id_Colection] INT           NOT NULL,
    [Id_Format]    INT           NULL,
    [Id_Property1] INT           NULL,
    [Id_Property2] INT           NULL,
    [Id_Property3] INT           NULL,
    [Id_Units]     INT           NOT NULL,
    [Unique_ID]    VARCHAR (50)  NULL,
    CONSTRAINT [PK_crs_S12_Product_new_2] PRIMARY KEY CLUSTERED ([Id_Product] ASC),
    CONSTRAINT [FK_crs_S14_Product_crs_S12_Units_Register] FOREIGN KEY ([Id_Units]) REFERENCES [dbo].[crs_S12_Units_Register] ([Id_Units]),
    CONSTRAINT [FK_crs_S14_Product_crs_S13_Grupa_Register] FOREIGN KEY ([Id_Grupa]) REFERENCES [dbo].[crs_S13_Grupa_Register] ([Id_Grupa]),
    CONSTRAINT [FK_crs_S14_Product_crs_S14_Format_Register] FOREIGN KEY ([Id_Format]) REFERENCES [dbo].[crs_S14_Format_Register] ([Id_Format]),
    CONSTRAINT [FK_crs_S14_Product_crs_S14_Property_Register1] FOREIGN KEY ([Id_Property1]) REFERENCES [dbo].[crs_S14_Property_Register] ([Id_Property]),
    CONSTRAINT [FK_crs_S14_Product_crs_S14_Property_Register2] FOREIGN KEY ([Id_Property2]) REFERENCES [dbo].[crs_S14_Property_Register] ([Id_Property]),
    CONSTRAINT [FK_crs_S14_Product_crs_S14_Property_Register3] FOREIGN KEY ([Id_Property3]) REFERENCES [dbo].[crs_S14_Property_Register] ([Id_Property]),
    CONSTRAINT [FK_crs_S14_Product_crs_S2_Brand_Register] FOREIGN KEY ([Id_Brand]) REFERENCES [dbo].[crs_S2_Brand_Register] ([Id_Brand]),
    CONSTRAINT [FK_crs_S14_Product_crs_S2_Colection] FOREIGN KEY ([Id_Colection]) REFERENCES [dbo].[crs_S2_Colection] ([Id_Colection])
);


GO
ALTER TABLE [dbo].[crs_S14_Product] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



