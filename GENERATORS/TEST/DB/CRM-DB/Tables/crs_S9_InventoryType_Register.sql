﻿CREATE TABLE [dbo].[crs_S9_InventoryType_Register] (
    [Id_InventoryType] INT         NOT NULL,
    [InventoryType]    VARCHAR (6) NOT NULL,
    CONSTRAINT [PK_crs_s9_InventoryType] PRIMARY KEY CLUSTERED ([Id_InventoryType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_InventoryType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



