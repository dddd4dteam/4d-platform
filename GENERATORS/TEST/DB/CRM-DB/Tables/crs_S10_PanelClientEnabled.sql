﻿CREATE TABLE [dbo].[crs_S10_PanelClientEnabled] (
    [ID]        UNIQUEIDENTIFIER NOT NULL,
    [ID_Panel]  CHAR (7)         NOT NULL,
    [Id_Client] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_crs_S10_PanelClient] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [FK_crs_S10_PanelClient_Enabled_S10_Panel] FOREIGN KEY ([ID_Panel]) REFERENCES [dbo].[crs_S10_Panel] ([ID_Panel]),
    CONSTRAINT [FK_crs_S10_PanelClient_Enabled_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client])
);


GO
ALTER TABLE [dbo].[crs_S10_PanelClientEnabled] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

