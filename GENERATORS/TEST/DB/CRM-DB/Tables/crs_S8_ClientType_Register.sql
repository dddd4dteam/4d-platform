﻿CREATE TABLE [dbo].[crs_S8_ClientType_Register] (
    [Id_ClientType] INT            NOT NULL,
    [ClientType]    NVARCHAR (50)  NOT NULL,
    [ClientTypeENG] NVARCHAR (50)  NOT NULL,
    [Definition]    NVARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S8_ClientType_Register_1] PRIMARY KEY CLUSTERED ([Id_ClientType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



