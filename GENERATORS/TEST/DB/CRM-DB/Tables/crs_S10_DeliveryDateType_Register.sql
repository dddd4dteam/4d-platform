﻿CREATE TABLE [dbo].[crs_S10_DeliveryDateType_Register] (
    [Id_DateType] INT          NOT NULL,
    [Description] VARCHAR (50) NULL,
    CONSTRAINT [PK_crs_s10_DeliveryDateType_Rgister] PRIMARY KEY CLUSTERED ([Id_DateType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryDateType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

