﻿CREATE TABLE [dbo].[crs_TrRepres] (
    [Id_TrRepres]    CHAR (6)      NOT NULL,
    [Description]    VARCHAR (50)  NOT NULL,
    [Id_Manager]     CHAR (6)      NOT NULL,
    [Name]           VARCHAR (50)  NOT NULL,
    [Id_Employee]    VARCHAR (6)   NULL,
    [Id_LocalTown]   INT           NULL,
    [Id_Tracker]     INT           CONSTRAINT [DF_crs_TrRepres_Id_Tracker] DEFAULT ((0)) NULL,
    [Timezone]       INT           NULL,
    [GPScontrol]     BIT           NULL,
    [CarPositionURL] VARCHAR (100) NULL,
    CONSTRAINT [PK_crs_TrRepres] PRIMARY KEY CLUSTERED ([Id_TrRepres] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_crs_TrRepres]
    ON [dbo].[crs_TrRepres]([Id_Tracker] ASC) WHERE ([Id_tracker] IS NOT NULL);

