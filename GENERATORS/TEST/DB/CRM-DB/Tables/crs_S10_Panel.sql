﻿CREATE TABLE [dbo].[crs_S10_Panel] (
    [ID_Panel]          CHAR (7)        NOT NULL,
    [Code]              VARCHAR (25)    NOT NULL,
    [Description]       VARCHAR (100)   NOT NULL,
    [ID_PanelType]      VARCHAR (5)     NOT NULL,
    [PanelType]         VARCHAR (28)    NOT NULL,
    [Id_Collection]     CHAR (5)        NOT NULL,
    [Collection]        VARCHAR (25)    NOT NULL,
    [Width]             NUMERIC (5, 2)  NOT NULL,
    [Height]            NUMERIC (5, 2)  NOT NULL,
    [Depth]             NUMERIC (10, 1) NOT NULL,
    [Weight]            NUMERIC (10, 4) NOT NULL,
    [tmp_Flag_OnlyBase] BIT             NULL,
    [Flag_Enable]       BIT             NOT NULL,
    [ImageIndexActual]  INT             NULL,
    [Id_Brand]          INT             NOT NULL,
    [Brand]             VARCHAR (25)    NOT NULL,
    [Id_StatusCol]      CHAR (3)        NOT NULL,
    [StatusCol]         VARCHAR (30)    NOT NULL,
    [ABC]               CHAR (1)        NOT NULL,
    [Id_collection2]    NCHAR (5)       NULL,
    [Collection2]       VARCHAR (25)    NULL,
    [Price]             FLOAT (53)      NULL,
    CONSTRAINT [PK_crs_S10_Panel] PRIMARY KEY CLUSTERED ([ID_Panel] ASC)
);





GO
ALTER TABLE [dbo].[crs_S10_Panel] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



