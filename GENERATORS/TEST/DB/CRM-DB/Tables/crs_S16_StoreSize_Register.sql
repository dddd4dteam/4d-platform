﻿CREATE TABLE [dbo].[crs_S16_StoreSize_Register] (
    [Id_StoreSize] INT           NOT NULL,
    [StoreSize]    NVARCHAR (30) NOT NULL,
    CONSTRAINT [PK_crs_S16_StoreSize_Register] PRIMARY KEY CLUSTERED ([Id_StoreSize] ASC)
);


GO
ALTER TABLE [dbo].[crs_S16_StoreSize_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



