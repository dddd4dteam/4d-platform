﻿CREATE TABLE [dbo].[crs_S9_Operation_Register] (
    [Id_Operation]    INT           NOT NULL,
    [Description]     VARCHAR (50)  NOT NULL,
    [Description_RUS] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_crs_MeetingOperation] PRIMARY KEY CLUSTERED ([Id_Operation] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_Operation_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



