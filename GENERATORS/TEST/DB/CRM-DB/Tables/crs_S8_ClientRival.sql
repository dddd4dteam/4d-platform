﻿CREATE TABLE [dbo].[crs_S8_ClientRival] (
    [Id_Client]             UNIQUEIDENTIFIER NOT NULL,
    [Id_Rival]              INT              NOT NULL,
    [Id_RivalGoodsCategory] VARCHAR (5)      NOT NULL,
    [Quantity]              INT              NULL,
    CONSTRAINT [PK_crs_S8_ClientRival_1] PRIMARY KEY CLUSTERED ([Id_Client] ASC, [Id_Rival] ASC, [Id_RivalGoodsCategory] ASC),
    CONSTRAINT [FK_crs_S8_ClientRival_crs_S16_RivalGoodsCategory_Register] FOREIGN KEY ([Id_RivalGoodsCategory]) REFERENCES [dbo].[crs_S16_RivalGoodsCategory_Register] ([Id_RivalGoodsCategory]),
    CONSTRAINT [FK_crs_S8_ClientRival_crs_S5_Rival_Register] FOREIGN KEY ([Id_Rival]) REFERENCES [dbo].[crs_S5_Rival_Register] ([Id_Rival]),
    CONSTRAINT [FK_crs_S8_ClientRival_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[crs_S8_ClientRival] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



