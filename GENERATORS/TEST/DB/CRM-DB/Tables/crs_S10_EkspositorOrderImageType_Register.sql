﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderImageType_Register] (
    [TypeID]      INT         NOT NULL,
    [Description] NCHAR (100) NULL,
    CONSTRAINT [PK_crs_S10_EkspositorOrderImageType_Register] PRIMARY KEY CLUSTERED ([TypeID] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderImageType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



