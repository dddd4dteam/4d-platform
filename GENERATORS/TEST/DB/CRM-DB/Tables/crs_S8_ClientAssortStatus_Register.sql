﻿CREATE TABLE [dbo].[crs_S8_ClientAssortStatus_Register] (
    [Id_ClientAssortStatus] INT            NOT NULL,
    [ClientAssortStatus]    VARCHAR (50)   NOT NULL,
    [Description]           NVARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S8_ClientAssortStatus_Register] PRIMARY KEY CLUSTERED ([Id_ClientAssortStatus] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientAssortStatus_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



