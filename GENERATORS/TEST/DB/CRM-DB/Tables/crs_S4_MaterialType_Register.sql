﻿CREATE TABLE [dbo].[crs_S4_MaterialType_Register] (
    [Id_MaterialType] INT           NOT NULL,
    [MaterialType]    VARCHAR (50)  NOT NULL,
    [Description]     VARCHAR (100) NULL,
    CONSTRAINT [PK_crs_S4_MaterialType_Register] PRIMARY KEY CLUSTERED ([Id_MaterialType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S4_MaterialType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



