﻿CREATE TABLE [dbo].[crs_S3_GoodsCategory_Register] (
    [Id_GoodsCategory] VARCHAR (5)   NOT NULL,
    [GoodsCategory]    VARCHAR (50)  NOT NULL,
    [Description]      VARCHAR (200) NULL,
    [Order]            INT           CONSTRAINT [DF_crs_S3_GoodsCategory_Register_Order] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_crs_S3_GoodsCategory_Register] PRIMARY KEY CLUSTERED ([Id_GoodsCategory] ASC)
);


GO
ALTER TABLE [dbo].[crs_S3_GoodsCategory_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



