﻿CREATE TABLE [dbo].[crs_S6_Diler] (
    [Id_Diler]          INT           NOT NULL,
    [Diler]             VARCHAR (100) NOT NULL,
    [Holding]           VARCHAR (100) NOT NULL,
    [Manager]           VARCHAR (100) NOT NULL,
    [Type]              VARCHAR (100) NOT NULL,
    [Country]           VARCHAR (30)  NOT NULL,
    [AddressF]          VARCHAR (200) NOT NULL,
    [CompanyCode]       VARCHAR (3)   NOT NULL,
    [Id_System]         VARCHAR (5)   NOT NULL,
    [Id_System_Holding] VARCHAR (5)   NOT NULL,
    [Flag_enable]       BIT           NULL,
    [tmp_ID_Client]     INT           NULL,
    [UrName]            VARCHAR (200) NULL,
    [ContractNumber]    VARCHAR (50)  NULL,
    [ContractDt]        VARCHAR (11)  NULL,
    CONSTRAINT [PK_crs_S6_Diler] PRIMARY KEY CLUSTERED ([Id_Diler] ASC)
);





GO
ALTER TABLE [dbo].[crs_S6_Diler] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



