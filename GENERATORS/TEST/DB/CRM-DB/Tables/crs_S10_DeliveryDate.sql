﻿CREATE TABLE [dbo].[crs_S10_DeliveryDate] (
    [Id_date]          UNIQUEIDENTIFIER NOT NULL,
    [Date]             DATETIME         NOT NULL,
    [Id_DateType]      INT              NOT NULL,
    [Id_DeliveryOrder] UNIQUEIDENTIFIER NOT NULL,
    [DtAdd]            DATETIME         NOT NULL,
    CONSTRAINT [PK_crs_s10_DeliveryDate] PRIMARY KEY CLUSTERED ([Id_date] ASC),
    CONSTRAINT [FK_crs_s10_DeliveryDate_crs_s10_DeliveryDateType_Rgister] FOREIGN KEY ([Id_DateType]) REFERENCES [dbo].[crs_S10_DeliveryDateType_Register] ([Id_DateType]),
    CONSTRAINT [FK_crs_S10_DeliveryDate_crs_S10_EkspositorOrder] FOREIGN KEY ([Id_DeliveryOrder]) REFERENCES [dbo].[crs_S10_EkspositorOrder] ([Id_Order])
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryDate] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

