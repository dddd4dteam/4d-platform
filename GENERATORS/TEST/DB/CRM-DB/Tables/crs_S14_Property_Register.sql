﻿CREATE TABLE [dbo].[crs_S14_Property_Register] (
    [Id_Property]     INT          NOT NULL,
    [Property]        VARCHAR (50) NOT NULL,
    [Id_PropertyType] INT          NULL,
    CONSTRAINT [PK_crs_S12_Property_Register] PRIMARY KEY CLUSTERED ([Id_Property] ASC),
    CONSTRAINT [FK_crs_S14_Property_Register_crs_S14_PropertyType_Register] FOREIGN KEY ([Id_PropertyType]) REFERENCES [dbo].[crs_S14_PropertyType_Register] ([Id_PropertyType])
);


GO
ALTER TABLE [dbo].[crs_S14_Property_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



