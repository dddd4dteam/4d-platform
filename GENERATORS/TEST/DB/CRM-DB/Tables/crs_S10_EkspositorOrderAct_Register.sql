﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderAct_Register] (
    [ID_Template]      INT             NOT NULL,
    [DataFileTemplate] VARBINARY (MAX) NOT NULL,
    [Description]      VARCHAR (100)   NOT NULL,
    [ID_OrderType]     INT             NOT NULL,
    [ActType]          VARCHAR (100)   NOT NULL,
    PRIMARY KEY CLUSTERED ([ID_Template] ASC)
);
GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderAct_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);
