﻿CREATE TABLE [dbo].[crs_S9_MeetingStatus_Register] (
    [Id_MeetingStatus] INT          NOT NULL,
    [Description]      VARCHAR (50) NOT NULL,
    [Description_RUS]  VARCHAR (50) NULL,
    CONSTRAINT [PK_crs_MeetingStatus] PRIMARY KEY CLUSTERED ([Id_MeetingStatus] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_MeetingStatus_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



