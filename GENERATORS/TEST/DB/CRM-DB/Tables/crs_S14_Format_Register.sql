﻿CREATE TABLE [dbo].[crs_S14_Format_Register] (
    [Id_Format] INT          NOT NULL,
    [Format]    VARCHAR (50) NOT NULL,
    [K_Pcs]     FLOAT (53)   NULL,
    [K_M]       FLOAT (53)   NULL,
    [K_Box]     FLOAT (53)   NULL,
    CONSTRAINT [PK_crs_S2_FormatCollection_Registr] PRIMARY KEY CLUSTERED ([Id_Format] ASC)
);


GO
ALTER TABLE [dbo].[crs_S14_Format_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



