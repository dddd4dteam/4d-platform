﻿CREATE TABLE [dbo].[crs_S15_Monitoring] (
    [Id_Client]     UNIQUEIDENTIFIER NOT NULL,
    [Id_Monitoring] UNIQUEIDENTIFIER NOT NULL,
    [Date]          DATETIME         NOT NULL,
    [Id_Product]    INT              NOT NULL,
    [Id_Units]      INT              NOT NULL,
    [Price]         FLOAT (53)       NULL,
    [SpecialPrice]  FLOAT (53)       NULL,
    CONSTRAINT [PK_crs_S12_Monitoring] PRIMARY KEY CLUSTERED ([Id_Monitoring] ASC),
    CONSTRAINT [FK_crs_S15_Monitoring_crs_S12_Units_Register] FOREIGN KEY ([Id_Units]) REFERENCES [dbo].[crs_S12_Units_Register] ([Id_Units]),
    CONSTRAINT [FK_crs_S15_Monitoring_crs_S14_Product] FOREIGN KEY ([Id_Product]) REFERENCES [dbo].[crs_S14_Product] ([Id_Product]),
    CONSTRAINT [FK_crs_S15_Monitoring_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client])
);


GO
ALTER TABLE [dbo].[crs_S15_Monitoring] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



