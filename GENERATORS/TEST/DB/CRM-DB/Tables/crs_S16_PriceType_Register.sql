﻿CREATE TABLE [dbo].[crs_S16_PriceType_Register] (
    [Id_PriceType] INT           NOT NULL,
    [Marking]      NVARCHAR (10) NOT NULL,
    [Description]  NVARCHAR (50) NOT NULL,
    [PriceType]    NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S16_PriceType_Register] PRIMARY KEY CLUSTERED ([Id_PriceType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S16_PriceType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



