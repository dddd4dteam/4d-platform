﻿CREATE TABLE [dbo].[crs_S12_GrupaUnits] (
	[Id_GrupaUnits] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
    [Id_Grupa] INT NOT NULL,
    [Id_Units] INT NOT NULL,   
	CONSTRAINT [PK_crs_S12_GrupaUnits] PRIMARY KEY ([Id_GrupaUnits]),  
    CONSTRAINT [FK_crs_S12_GrupaUnits_Register_crs_S12_Units_Register] FOREIGN KEY ([Id_Units]) REFERENCES [dbo].[crs_S12_Units_Register] ([Id_Units])    
);


GO
ALTER TABLE [dbo].[crs_S12_GrupaUnits] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



