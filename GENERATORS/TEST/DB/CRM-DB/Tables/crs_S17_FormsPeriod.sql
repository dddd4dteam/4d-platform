﻿CREATE TABLE [dbo].[crs_S17_FormsPeriod] (
    [ID_Form]    UNIQUEIDENTIFIER NOT NULL,
    [ID_Period]  UNIQUEIDENTIFIER NOT NULL,
    [PeriodeBgn] DATETIME         NOT NULL,
    [PeriodeEnd] DATETIME         NOT NULL,
    CONSTRAINT [PK_crs_S17_FormsPeriod] PRIMARY KEY CLUSTERED ([ID_Period] ASC),
    CONSTRAINT [FK_crs_S17_FormsPeriod] FOREIGN KEY ([ID_Form]) REFERENCES [dbo].[crs_S17_Forms] ([ID_Form])
);


GO
ALTER TABLE [dbo].[crs_S17_FormsPeriod] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

