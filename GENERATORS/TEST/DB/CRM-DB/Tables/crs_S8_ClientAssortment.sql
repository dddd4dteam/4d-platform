﻿CREATE TABLE [dbo].[crs_S8_ClientAssortment] (
    [Id_Client]             UNIQUEIDENTIFIER NOT NULL,
    [Id_Assortment]         UNIQUEIDENTIFIER NOT NULL,
    [Id_ClientAssortStatus] INT              NOT NULL,
    CONSTRAINT [PK_crs_S8_ClientAssortment] PRIMARY KEY CLUSTERED ([Id_Client] ASC, [Id_Assortment] ASC),
    CONSTRAINT [FK_crs_S8_ClientAssortment_crs_S2_Assortment] FOREIGN KEY ([Id_Assortment]) REFERENCES [dbo].[crs_S2_Assortment] ([Id_Assortment]),
    CONSTRAINT [FK_crs_S8_ClientAssortment_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]),
    CONSTRAINT [FK_crs_S8_ClientAssortment_crs_S8_ClientAssortStatus_Register] FOREIGN KEY ([Id_ClientAssortStatus]) REFERENCES [dbo].[crs_S8_ClientAssortStatus_Register] ([Id_ClientAssortStatus])
);


GO
ALTER TABLE [dbo].[crs_S8_ClientAssortment] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



