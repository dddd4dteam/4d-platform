﻿CREATE TABLE [dbo].[crs_S9_MeetingType_Register] (
    [Id_MeetingType] INT          NOT NULL,
    [Description]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_MeetingType] PRIMARY KEY CLUSTERED ([Id_MeetingType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S9_MeetingType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



