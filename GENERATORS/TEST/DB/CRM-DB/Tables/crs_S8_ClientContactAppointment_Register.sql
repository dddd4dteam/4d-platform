﻿CREATE TABLE [dbo].[crs_S8_ClientContactAppointment_Register] (
    [Id_ClientContactAppointment] INT           NOT NULL,
    [ClientContactAppointment]    NVARCHAR (80) NULL,
    CONSTRAINT [PK_crs_S8_ClientContactAppointment_Register] PRIMARY KEY CLUSTERED ([Id_ClientContactAppointment] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientContactAppointment_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



