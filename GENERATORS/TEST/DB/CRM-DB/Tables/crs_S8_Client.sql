﻿CREATE TABLE [dbo].[crs_S8_Client] (
    [Id_Client]                UNIQUEIDENTIFIER NOT NULL,
    [Code_Client]              INT              NOT NULL,
    [Id_TrRepres]              CHAR (6)         NOT NULL,
    [Client]                   VARCHAR (200)    NOT NULL,
    [Id_GeographyItem]         INT              NOT NULL,
    [Street]                   VARCHAR (50)     NULL,
    [HouseNumber]              VARCHAR (10)     NULL,
    [HousePartNumber]          VARCHAR (10)     NULL,
    [FlatNumber]               VARCHAR (10)     NULL,
    [PavilionNumber]           VARCHAR (15)     NULL,
    [AddressOther]             VARCHAR (1000)   NULL,
    [PhoneNumber]              VARCHAR (100)    NULL,
    [postcode]                 VARCHAR (7)      NULL,
    [Mail]                     VARCHAR (60)     NULL,
    [WebAddress]               VARCHAR (60)     NULL,
    [Id_Holding]               INT              CONSTRAINT [DF_crs_S8_Client_Id_Holding] DEFAULT ((1)) NULL,
    [Id_ClientType]            INT              NULL,
    [Id_ClientPrestigeLevel]   INT              CONSTRAINT [DF_crs_S8_Client_Id_ClientPrestigeLevel] DEFAULT ((1)) NOT NULL,
    [Id_ClientRelation]        INT              NOT NULL,
    [Flag_BrandCersanit]       BIT              NOT NULL,
    [Flag_BrandOposzno]        BIT              NOT NULL,
    [Flag_Warehouse]           BIT              NOT NULL,
    [AdditionInfo]             VARCHAR (300)    NULL,
    [Dt_Add]                   DATETIME         NULL,
    [Dt_Dell]                  DATETIME         NULL,
    [Flag_InfoVerified]        BIT              NOT NULL,
    [Flag_Enable]              BIT              NOT NULL,
    [Id_UserDomainRole]        VARCHAR (7)      NOT NULL,
    [Flag_NeedPassed]          BIT              NOT NULL,
    [Id_GeographyItemRynok]    INT              NULL,
    [AspRoleID]                UNIQUEIDENTIFIER NULL,
    [UserID]                   UNIQUEIDENTIFIER NULL,
    [Id_PresentationType]      INT              NULL,
    [Id_StoreSize]             INT              NULL,
    [Id_PriceType]             INT              NULL,
    [Cost_2D_Rub_Rovese]       INT              NULL,
    [Cost_3D_Rub_Rovese]       INT              NULL,
    [Cost_2D_Rub_Total]        INT              NULL,
    [Cost_3D_Rub_Total]        INT              NULL,
    [OpenTime]                 DATETIME         NULL,
    [CloseTime]                DATETIME         NULL,
    [SellerCount]              INT              NULL,
    [Flag_Design]              BIT              CONSTRAINT [DF_crs_S8_Client_Flag_Design] DEFAULT ((0)) NOT NULL,
    [Flag_BrandMeissen]        BIT              CONSTRAINT [DF_crs_S8_Client_Flag_BrandMeissen] DEFAULT ((0)) NOT NULL,
    [Longitude]                FLOAT (53)       NULL,
    [Latitude]                 FLOAT (53)       NULL,
    [UrName]                   NVARCHAR (100)   NULL,
    [Id_ClientPrestigeLevel3D] INT              DEFAULT ((1)) NOT NULL,
    [ID_DeliveryTransport]     INT              NULL,
    CONSTRAINT [PK_crs_S8_Client] PRIMARY KEY CLUSTERED ([Id_Client] ASC),
    CONSTRAINT [FK_crs_S8_Client_crs_S8_DeliveryTransport_Register] FOREIGN KEY ([ID_DeliveryTransport]) REFERENCES [dbo].[crs_S8_DeliveryTransport_Register] ([ID_DeliveryTransport])
);








GO
ALTER TABLE [dbo].[crs_S8_Client] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






GO

Create TRIGGER S8_Client_EventInTable
	ON dbo.crs_S8_Client
AFTER Insert,Update,Delete
AS

/* Добавление новой записи */
Insert into sys_Flags_NeedPassed
select id_client, 'S8', 1, 0, cast(getdate() as datetime) from inserted i
where not exists (select 1 from deleted d where i.id_client=d.id_client)

/* Обновление записи */
update sys_Flags_NeedPassed
set Flag_NeedPassed=1,LastDt_NeedPassed=cast(getdate() as datetime)
where Id_object in (select d.id_client from deleted d inner join inserted i on i.id_client=d.id_client)

/* Удаление записи */
Delete sys_Flags_NeedPassed
where Id_object in (
					select id_client from deleted d
					where not exists (select 1 from inserted i where i.id_client=d.id_client)
					)