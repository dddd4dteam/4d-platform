﻿CREATE TABLE [dbo].[crs_S15_ClientMonitoring] (
    [Id_row]            UNIQUEIDENTIFIER NOT NULL,
    [Start]             DATETIME         NOT NULL,
    [Finish]            DATETIME         NOT NULL,
    [Id_MonitoringType] INT              NOT NULL,
    [Id_Client]         UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_crs_S12_ClientMonitoring_new] PRIMARY KEY CLUSTERED ([Id_row] ASC),
    CONSTRAINT [FK_crs_S15_ClientMonitoring_crs_S15_MonitoringType_Register] FOREIGN KEY ([Id_MonitoringType]) REFERENCES [dbo].[crs_S15_MonitoringType_Register] ([Id_MonitoringType]),
    CONSTRAINT [FK_crs_S15_ClientMonitoring_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client])
);


GO
ALTER TABLE [dbo].[crs_S15_ClientMonitoring] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



