CREATE TABLE [dbo].[crs_S10_EkspositorOrder] (
    [Id_Order]              UNIQUEIDENTIFIER NOT NULL,
    [Code_Order]            INT              NOT NULL,
    [Code_Ekspositor]       INT              NOT NULL,
    [ID_OrderType]          INT              NOT NULL,
    [ID_Construction]       CHAR (7)         NOT NULL,
    [Id_PanelSet]           UNIQUEIDENTIFIER NULL,
    [Flag_BuyConstr]        BIT              NOT NULL,
    [Flag_Inventory]        BIT              NOT NULL,
    [Id_Diler]              INT              NOT NULL,
    [Id_Client]             UNIQUEIDENTIFIER NOT NULL,
    [DtOrderCreate]         DATETIME         NULL,
    [DtOrderReady]          DATETIME         NULL,
    [DtWillBeReady]         DATETIME         NULL,
    [DtBuhTransaction]      DATETIME         NULL,
    [DtExecutionConfirm]    DATETIME         NULL,
    [Flag_OrderReady]       BIT              NOT NULL,
    [Flag_BuhTransaction]   BIT              NOT NULL,
    [Flag_ExecutionConfirm] BIT              NOT NULL,
    [Flag_LastOrder]        BIT              NOT NULL,
    [LastOrderMaxPosition]  INT              NULL,
    [OrderMaxPosition]      INT              NULL,
    [Flag_BuyPanels]        BIT              NOT NULL,
    [Flag_OurDelivery]      BIT              NOT NULL,
    [Id_StandartSet]        CHAR (7)         CONSTRAINT [DF_crs_S10_EkspositorOrder_Id_StandartSet] DEFAULT ('') NULL,
    [ImageIndexActual]      INT              NULL,
    [TPredAspRoleID]        UNIQUEIDENTIFIER NULL,
    [CreatorUserID]         UNIQUEIDENTIFIER NULL,
    [ChangerUserID]         UNIQUEIDENTIFIER NULL,
    [DtChanged]             DATETIME         NULL,
    [InventoryNumber]       CHAR (9)         NULL,
    [NumberPanels]          INT              NULL,
    [Id_Previous]           UNIQUEIDENTIFIER NULL,
    [Id_Ekspositor]         UNIQUEIDENTIFIER NOT NULL,
    [Flag_Deleted]          BIT              DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_crs_S10_EkspositorOrder] PRIMARY KEY CLUSTERED ([Id_Order] ASC),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S10_Construction_Register] FOREIGN KEY ([ID_Construction]) REFERENCES [dbo].[crs_S10_Construction_Register] ([ID_Construction]),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S10_EkspositorOrderPanelSet] FOREIGN KEY ([Id_PanelSet]) REFERENCES [dbo].[crs_S10_EkspositorOrderPanelSet] ([Id_PanelSet]),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S10_EkspositorOrderType_Register] FOREIGN KEY ([ID_OrderType]) REFERENCES [dbo].[crs_S10_EkspositorOrderType_Register] ([ID_OrderType]),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S10_StandartPanelSet_Register] FOREIGN KEY ([Id_StandartSet]) REFERENCES [dbo].[crs_S10_StandartPanelSet_Register] ([Id_StandartSet]),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S6_Diler] FOREIGN KEY ([Id_Diler]) REFERENCES [dbo].[crs_S6_Diler] ([Id_Diler]),
    CONSTRAINT [FK_crs_S10_EkspositorOrder_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client])
);





GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrder] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






GO


GO





CREATE TRIGGER [dbo].[S10_EkspositorOrder_EventInTable]
	ON [dbo].[crs_S10_EkspositorOrder]

After Insert,Update,Delete
AS

/* Добавление новой записи */
Insert into sys_Flags_NeedPassed (ID_Object, PrefixNameObject, Flag_NeedPassed, Flag_WasPassed, LastDt_NeedPassed)
select Id_order, 'S10', Flag_ExecutionConfirm, 0, cast(NULL as datetime) 
from inserted i
where not exists (select 1 from deleted d where i.id_order=d.id_order)

/* Обновление записи */
IF UPDATE (Flag_ExecutionConfirm)
	Begin
	Update sys_Flags_NeedPassed
	set Flag_NeedPassed=1,LastDt_NeedPassed=cast(getdate() as datetime)
	where Id_object IN

					(	--Только те заказы, у которых Flag_ExecutionConfirm после обновления или добавления стал "1"
						select id.Id_order
						from inserted id
						Left join deleted del on(id.Id_Order = del.Id_Order)
						where Id.Flag_ExecutionConfirm = 1 and IsNull(del.Flag_ExecutionConfirm,0) = 0
					)

	End


/* Удаление записи */
Delete sys_Flags_NeedPassed
where Id_object IN (
					select Id_order from deleted d
					where not exists (select 1 from inserted i where i.id_order=d.id_order)
					)