﻿CREATE TABLE [dbo].[crs_S9_Inventory] (
    [ID_Inventory]     UNIQUEIDENTIFIER NOT NULL,
    [Id_Meeting]       UNIQUEIDENTIFIER NOT NULL,
    [ID_InventoryItem] UNIQUEIDENTIFIER NOT NULL,
    [Value1Count]      INT              NULL,
    [Value2Count]      INT              NULL,
    [Value3Count]      INT              NULL,
    CONSTRAINT [PK_crs_S17_Inventory] PRIMARY KEY CLUSTERED ([ID_Inventory] ASC),
    CONSTRAINT [FK_crs_S17_Inventory_crs_S17_InventoryItem_Register] FOREIGN KEY ([ID_InventoryItem]) REFERENCES [dbo].[crs_S9_InventoryItem_Register] ([ID_InventoryItem]),
    CONSTRAINT [FK_crs_S17_Inventory_crs_S9_Meeting] FOREIGN KEY ([Id_Meeting]) REFERENCES [dbo].[crs_S9_Meeting] ([Id_Meeting]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[crs_S9_Inventory] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



