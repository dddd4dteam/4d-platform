﻿CREATE TABLE [dbo].[crs_S10_DeliveryDateComment] (
    [Id_Comment] UNIQUEIDENTIFIER NOT NULL,
    [Id_Date]    UNIQUEIDENTIFIER NOT NULL,
    [Text]       VARCHAR (500)    NULL,
    CONSTRAINT [PK_crs_S10_DeliveryDateComment] PRIMARY KEY CLUSTERED ([Id_Comment] ASC),
    CONSTRAINT [FK_crs_S10_DeliveryDateComment_crs_s10_DeliveryDate] FOREIGN KEY ([Id_Date]) REFERENCES [dbo].[crs_S10_DeliveryDate] ([Id_date])
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryDateComment] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

