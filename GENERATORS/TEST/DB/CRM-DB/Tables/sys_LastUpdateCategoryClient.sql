﻿CREATE TABLE [dbo].[sys_LastUpdateCategoryClient] (
    [Id_Client]              UNIQUEIDENTIFIER NOT NULL,
    [Code_Client]            INT              NOT NULL,
    [dtLastUpdateCategory2D] DATETIME         NULL,
    [dtLastUpdateCategory3D] DATETIME         NULL
);

