﻿CREATE TABLE [dbo].[crs_S10_DeliveryOrderType_Register] (
    [ID]          INT           NOT NULL,
    [Description] NVARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S10_DeliveryOrderType_Registry] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryOrderType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

