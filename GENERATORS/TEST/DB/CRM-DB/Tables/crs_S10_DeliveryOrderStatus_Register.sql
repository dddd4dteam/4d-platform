﻿CREATE TABLE [dbo].[crs_S10_DeliveryOrderStatus_Register] (
    [ID]          INT       NOT NULL,
    [Description] NVARCHAR (50) NULL,
    CONSTRAINT [PK_crs_S10_DeliveryOrderStatus_Registry] PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryOrderStatus_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

