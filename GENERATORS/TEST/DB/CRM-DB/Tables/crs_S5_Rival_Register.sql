﻿CREATE TABLE [dbo].[crs_S5_Rival_Register] (
    [Id_Rival]    INT            NOT NULL,
    [Rival]       VARCHAR (50)   NOT NULL,
    [Description] NVARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S5_Rival_Register] PRIMARY KEY CLUSTERED ([Id_Rival] ASC)
);


GO
ALTER TABLE [dbo].[crs_S5_Rival_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



