﻿CREATE TABLE [dbo].[crs_S7_Holding_Register] (
    [Id_Holding] INT           NOT NULL,
    [Holding]    VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_crs_S7_Holding_Register] PRIMARY KEY CLUSTERED ([Id_Holding] ASC)
);


GO
ALTER TABLE [dbo].[crs_S7_Holding_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



