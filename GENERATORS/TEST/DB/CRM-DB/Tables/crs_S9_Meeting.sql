﻿CREATE TABLE [dbo].[crs_S9_Meeting] (
    [Id_Meeting]               UNIQUEIDENTIFIER NOT NULL,
    [Priority]                 INT              NOT NULL,
    [Description]              VARCHAR (500)    NULL,
    [StartTime]                DATETIME         NOT NULL,
    [EndTime]                  DATETIME         NOT NULL,
    [Id_Status]                INT              NOT NULL,
    [Result]                   VARCHAR (500)    NULL,
    [MadeByUserRoleOld]        VARCHAR (50)     NOT NULL,
    [MadeByUserIDOld]          INT              NOT NULL,
    [res_IllDo]                VARCHAR (1000)   NULL,
    [res_TalkingTheme]         VARCHAR (1000)   NULL,
    [res_FutureMeetingDate]    DATETIME         NULL,
    [MadeByUserRole]           UNIQUEIDENTIFIER NULL,
    [MadeByUserID]             UNIQUEIDENTIFIER NULL,
    [Latitude]                 FLOAT (53)       NULL,
    [Longitude]                FLOAT (53)       NULL,
    [DistanceToClient]         INT              NULL,
    [ActualTime]               DATETIME         NULL,
    [Id_TrRepres]              CHAR (6)         NULL,
    [Id_MeetingType]           INT              NOT NULL,
    [Id_MeetingPositionSource] INT              NULL,
    [Id_Client]                UNIQUEIDENTIFIER NULL,
    CONSTRAINT [PK_crs_S9_Meeting] PRIMARY KEY CLUSTERED ([Id_Meeting] ASC),
    CONSTRAINT [FK_crs_S9_Meeting_crs_S9_MeetingPositionSource_Register] FOREIGN KEY ([Id_MeetingPositionSource]) REFERENCES [dbo].[crs_S9_MeetingPositionSource_Register] ([Id_MeetingPositionSource]),
    CONSTRAINT [FK_crs_S9_Meeting_crs_S9_MeetingType_Register] FOREIGN KEY ([Id_MeetingType]) REFERENCES [dbo].[crs_S9_MeetingType_Register] ([Id_MeetingType])
);


GO
ALTER TABLE [dbo].[crs_S9_Meeting] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);






GO
CREATE NONCLUSTERED INDEX [IX_crs_S9_Meeting]
    ON [dbo].[crs_S9_Meeting]([Id_MeetingType] ASC, [StartTime] ASC)
    INCLUDE([Id_Meeting], [Id_Client]);


GO
