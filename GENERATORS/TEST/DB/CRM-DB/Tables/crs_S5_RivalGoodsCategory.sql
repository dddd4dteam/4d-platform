﻿CREATE TABLE [dbo].[crs_S5_RivalGoodsCategory] (
    [Guid_RivalGoodsCategory] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
    [Id_Rival]              INT         NOT NULL,
    [Id_RivalGoodsCategory] VARCHAR (5) NOT NULL,
	CONSTRAINT [PK_crs_S5_RivalGoodsCategory] PRIMARY KEY ([Guid_RivalGoodsCategory]),
    CONSTRAINT [FK_crs_S5_RivalGoodsCategory_crs_S16_RivalGoodsCategory_Register] FOREIGN KEY ([Id_RivalGoodsCategory]) REFERENCES [dbo].[crs_S16_RivalGoodsCategory_Register] ([Id_RivalGoodsCategory]),
    CONSTRAINT [FK_crs_S5_RivalGoodsCategory_crs_S5_Rival_Register] FOREIGN KEY ([Id_Rival]) REFERENCES [dbo].[crs_S5_Rival_Register] ([Id_Rival])    
);


GO
ALTER TABLE [dbo].[crs_S5_RivalGoodsCategory] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



