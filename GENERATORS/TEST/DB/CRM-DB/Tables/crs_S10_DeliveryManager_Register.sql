﻿CREATE TABLE [dbo].[crs_S10_DeliveryManager_Register] (
    [Id_DeliveryManager] CHAR (6)      NOT NULL,
    [DeliveryManager]    VARCHAR (100) NOT NULL,
    CONSTRAINT [rs_S10_DeliveryManager_Register] PRIMARY KEY CLUSTERED ([Id_DeliveryManager] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_DeliveryManager_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

