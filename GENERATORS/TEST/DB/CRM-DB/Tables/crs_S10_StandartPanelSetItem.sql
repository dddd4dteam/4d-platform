﻿CREATE TABLE [dbo].[crs_S10_StandartPanelSetItem] (
	[Id_StandartPanelSetItem] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(), 
    [Id_StandartSet] CHAR (7) NOT NULL,
    [Id_Panel]       CHAR (7) NOT NULL,    
	CONSTRAINT [PK_crs_S10_StandartPanelSetItem] PRIMARY KEY ([Id_StandartPanelSetItem]),
    CONSTRAINT [FK_crs_S10_StandartPanelSetItem_crs_S10_Panel] FOREIGN KEY ([Id_Panel]) REFERENCES [dbo].[crs_S10_Panel] ([ID_Panel]),
    CONSTRAINT [FK_crs_S10_StandartPanelSetItem_crs_S10_StandartPanelSet] FOREIGN KEY ([Id_StandartSet]) REFERENCES [dbo].[crs_S10_StandartPanelSet_Register] ([Id_StandartSet]),    
);


GO
ALTER TABLE [dbo].[crs_S10_StandartPanelSetItem] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



