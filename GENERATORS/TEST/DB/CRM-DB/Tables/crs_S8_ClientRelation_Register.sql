﻿CREATE TABLE [dbo].[crs_S8_ClientRelation_Register] (
    [Id_ClientRelation] INT           NOT NULL,
    [ClientRelation]    VARCHAR (30)  NOT NULL,
    [Description]       VARCHAR (200) NULL,
    CONSTRAINT [PK_crs_S8_ClientRelation_Register] PRIMARY KEY CLUSTERED ([Id_ClientRelation] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientRelation_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



