﻿CREATE TABLE [dbo].[crs_S8_ClientPrestigeLevel_Register] (
    [Id_ClientPrestigeLevel] INT           NOT NULL,
    [ClientPrestigeLevel]    VARCHAR (20)  NOT NULL,
    [Description]            VARCHAR (300) NULL,
    CONSTRAINT [PK_crs_S8_ClientPrestigeLevel_Register] PRIMARY KEY CLUSTERED ([Id_ClientPrestigeLevel] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_ClientPrestigeLevel_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



