﻿CREATE TABLE [dbo].[crs_S14_PropertyType_Register] (
    [Id_PropertyType] INT          NOT NULL,
    [PropertyType]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S12_PropertyType_Register] PRIMARY KEY CLUSTERED ([Id_PropertyType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S14_PropertyType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



