﻿CREATE TABLE [dbo].[crs_S17_QuestionsCondition_Register] (
    [ID_Condition] INT          NOT NULL,
    [Condition]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S17_QuestionsCondition_Register] PRIMARY KEY CLUSTERED ([ID_Condition] ASC)
);


GO
ALTER TABLE [dbo].[crs_S17_QuestionsCondition_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

