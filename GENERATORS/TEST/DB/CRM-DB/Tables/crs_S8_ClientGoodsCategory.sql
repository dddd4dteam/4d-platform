﻿CREATE TABLE [dbo].[crs_S8_ClientGoodsCategory] (
    [Id_Client]        UNIQUEIDENTIFIER NOT NULL,
    [Id_GoodsCategory] VARCHAR (5)      NOT NULL,
    [PctAll]           INT              NULL,
    [PctCersanit]      INT              NULL,
    CONSTRAINT [PK_crs_S8_ClientGoodsCategory] PRIMARY KEY CLUSTERED ([Id_Client] ASC, [Id_GoodsCategory] ASC),
    CONSTRAINT [FK_crs_S8_ClientGoodsCategory_crs_S3_GoodsCategory_Register] FOREIGN KEY ([Id_GoodsCategory]) REFERENCES [dbo].[crs_S3_GoodsCategory_Register] ([Id_GoodsCategory]),
    CONSTRAINT [FK_crs_S8_ClientGoodsCategory_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]) ON DELETE CASCADE
);


GO
ALTER TABLE [dbo].[crs_S8_ClientGoodsCategory] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



