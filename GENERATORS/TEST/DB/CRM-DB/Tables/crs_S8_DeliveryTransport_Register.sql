﻿CREATE TABLE [dbo].[crs_S8_DeliveryTransport_Register] (
    [ID_DeliveryTransport] INT          NOT NULL,
    [Description]          VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S8_DeliveryTransport_Register] PRIMARY KEY CLUSTERED ([ID_DeliveryTransport] ASC)
);


GO
ALTER TABLE [dbo].[crs_S8_DeliveryTransport_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);

