﻿CREATE TABLE [dbo].[crs_S2_Assortment] (
    [Id_Assortment]  UNIQUEIDENTIFIER NOT NULL,
    [Id_AssortGrupa] INT              NOT NULL,
    [Id_Colection]   INT              NOT NULL,
    CONSTRAINT [PK_crs_S2_Assortment] PRIMARY KEY CLUSTERED ([Id_Assortment] ASC),
    CONSTRAINT [FK_crs_S2_Assortment_crs_S2_AssortGrupa_Register] FOREIGN KEY ([Id_AssortGrupa]) REFERENCES [dbo].[crs_S2_AssortGrupa_Register] ([Id_AssortGrupa]),
    CONSTRAINT [FK_crs_S2_Assortment_crs_S2_Colection] FOREIGN KEY ([Id_Colection]) REFERENCES [dbo].[crs_S2_Colection] ([Id_Colection])
);


GO
ALTER TABLE [dbo].[crs_S2_Assortment] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



