﻿CREATE TABLE [dbo].[crs_S10_ConstructionType_Register] (
    [ID_ConstructionType] INT           NOT NULL,
    [ConstructionType]    VARCHAR (100) NULL,
    CONSTRAINT [PK_crs_S10_ConstructionType_Register] PRIMARY KEY CLUSTERED ([ID_ConstructionType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S10_ConstructionType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



