﻿CREATE TABLE [dbo].[crs_S8_ClientMaterial] (
    [Id_ClientMaterial] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(), 
	[Id_Client]   UNIQUEIDENTIFIER NOT NULL,
    [Id_Material] INT              NOT NULL,    
	CONSTRAINT [PK_crs_S8_ClientMaterial] PRIMARY KEY ([Id_ClientMaterial]),
    CONSTRAINT [FK_crs_S8_ClientMaterial_crs_S4_Material_Register] FOREIGN KEY ([Id_Material]) REFERENCES [dbo].[crs_S4_Material_Register] ([Id_Material]),
    CONSTRAINT [FK_crs_S8_ClientMaterial_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]) ON DELETE CASCADE
    
);


GO
ALTER TABLE [dbo].[crs_S8_ClientMaterial] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



