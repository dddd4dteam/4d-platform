﻿CREATE TABLE [dbo].[crs_S15_ProductMonitoring] (
    [Id_row]            UNIQUEIDENTIFIER NOT NULL,
    [Id_MonitoringType] INT              NOT NULL,
    [Start]             DATETIME         NOT NULL,
    [Finish]            DATETIME         NOT NULL,
    [Id_Product]        INT              NOT NULL,
    CONSTRAINT [PK_crs_S12_ProductMonitoring_new] PRIMARY KEY CLUSTERED ([Id_row] ASC),
    CONSTRAINT [FK_crs_S15_ProductMonitoring_crs_S14_Product] FOREIGN KEY ([Id_Product]) REFERENCES [dbo].[crs_S14_Product] ([Id_Product]),
    CONSTRAINT [FK_crs_S15_ProductMonitoring_crs_S15_MonitoringType_Register] FOREIGN KEY ([Id_MonitoringType]) REFERENCES [dbo].[crs_S15_MonitoringType_Register] ([Id_MonitoringType])
);


GO
ALTER TABLE [dbo].[crs_S15_ProductMonitoring] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



