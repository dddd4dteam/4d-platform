﻿CREATE TABLE [dbo].[crs_S10_EkspositorOrderPanelSetItem] (
    [Id_PanelSetItem]    UNIQUEIDENTIFIER NOT NULL,
    [Id_PanelSet]        UNIQUEIDENTIFIER NOT NULL,
    [Id_Panel]           CHAR (7)         NOT NULL,
    [Flag_OnlyBase]      BIT              NOT NULL,
    [Position]           INT              NULL,
    [Flag_Highlight]     BIT              NULL,
    [HighlightComment]   VARCHAR (500)    NULL,
    [CRM_RoleDescriptor] VARCHAR (100)    NULL,
    [ImageIndexActual]   INT              NULL,
    [Flag_BuyPanels]     BIT              NULL,
    [Panel_Guid]         NVARCHAR (36)    NOT NULL,
    CONSTRAINT [PK_crs_S10_PanelSetItem] PRIMARY KEY CLUSTERED ([Id_PanelSetItem] ASC),
    CONSTRAINT [FK_crs_S10_EkspositorOrderPanelSetItem_crs_S10_EkspositorOrderPanelSet] FOREIGN KEY ([Id_PanelSet]) REFERENCES [dbo].[crs_S10_EkspositorOrderPanelSet] ([Id_PanelSet]),
    CONSTRAINT [FK_crs_S10_EkspositorOrderPanelSetItem_crs_S10_Panel] FOREIGN KEY ([Id_Panel]) REFERENCES [dbo].[crs_S10_Panel] ([ID_Panel])
);


GO
ALTER TABLE [dbo].[crs_S10_EkspositorOrderPanelSetItem] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



