﻿CREATE TABLE [dbo].[crs_S12_Units_Register] (
    [Id_Units] INT          NOT NULL,
    [Units]    VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_crs_S12_Units_Register] PRIMARY KEY CLUSTERED ([Id_Units] ASC)
);


GO
ALTER TABLE [dbo].[crs_S12_Units_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



