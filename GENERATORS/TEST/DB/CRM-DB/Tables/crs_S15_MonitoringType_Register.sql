﻿CREATE TABLE [dbo].[crs_S15_MonitoringType_Register] (
    [Id_MonitoringType] INT           NOT NULL,
    [MonitoringType]    VARCHAR (100) NOT NULL,
    [Description]       VARCHAR (100) NULL,
    [Start]             DATETIME      NOT NULL,
    [Finish]            DATETIME      NOT NULL,
    CONSTRAINT [PK_crs_S12_MonitoringType_Registr] PRIMARY KEY CLUSTERED ([Id_MonitoringType] ASC)
);


GO
ALTER TABLE [dbo].[crs_S15_MonitoringType_Register] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



