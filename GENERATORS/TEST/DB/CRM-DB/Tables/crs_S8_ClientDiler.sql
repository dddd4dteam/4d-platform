﻿CREATE TABLE [dbo].[crs_S8_ClientDiler] (
    [Id_ClientDiler] UNIQUEIDENTIFIER NOT NULL DEFAULT NEWID(),
    [Id_Client] UNIQUEIDENTIFIER NOT NULL,
    [Id_Diler]  INT              NOT NULL,     
	CONSTRAINT [PK_crs_S8_ClientDiler] PRIMARY KEY ([Id_ClientDiler]),
    CONSTRAINT [FK_crs_S8_ClientDiler_crs_S6_Diler] FOREIGN KEY ([Id_Diler]) REFERENCES [dbo].[crs_S6_Diler] ([Id_Diler]),
    CONSTRAINT [FK_crs_S8_ClientDiler_crs_S8_Client] FOREIGN KEY ([Id_Client]) REFERENCES [dbo].[crs_S8_Client] ([Id_Client]) ON DELETE CASCADE     
);


GO
ALTER TABLE [dbo].[crs_S8_ClientDiler] ENABLE CHANGE_TRACKING WITH (TRACK_COLUMNS_UPDATED = OFF);



