﻿CREATE PROCEDURE aspnet_GetInheritedRuleGroups
	@RuleId UNIQUEIDENTIFIER
AS

	SET NOCOUNT ON 
	
	;WITH CTE AS
	(
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   0 AS [Level]
		FROM   aspnet_Groups
		JOIN aspnet_RulesInGroups ON aspnet_RulesInGroups.GroupId = aspnet_Groups.GroupId
		WHERE  aspnet_RulesInGroups.RuleId = @RuleId AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Rules WHERE RuleId = @RuleId)
	    
		UNION ALL
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   CTE.[Level] + 1 AS [Level]
		FROM   aspnet_Groups
			   JOIN CTE ON  aspnet_Groups.GroupId = CTE.ParentGroupId
		AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Rules WHERE RuleId = @RuleId)
	)
	SELECT DISTINCT GroupId,
		   GroupName
	FROM   CTE
	OPTION (MAXRECURSION 0)