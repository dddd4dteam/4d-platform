﻿CREATE PROCEDURE aspnet_Membership_ChangeUserName
	@ApplicationName NVARCHAR(256),
	@OldUserName NVARCHAR(256),
	@NewUserName NVARCHAR(256)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @UserId AS UNIQUEIDENTIFIER, @ApplicationId AS UNIQUEIDENTIFIER	
	
	-- Lookup the ApplicationId
	
	SELECT @ApplicationId = ApplicationId
	FROM   aspnet_Applications
	WHERE  LoweredApplicationName = LOWER(@ApplicationName)
	
	-- Lookup the UserId
	
	SELECT @UserId = UserId
	FROM   aspnet_Users
	WHERE  ApplicationId = @ApplicationId
	       AND LoweredUserName = LOWER(@OldUserName)
	
	-- Change the username
	
	UPDATE aspnet_Users
	SET    UserName = @NewUserName,
	       LoweredUserName = LOWER(@NewUserName)
	WHERE  ApplicationId = @ApplicationId
	       AND UserId = @UserId
END