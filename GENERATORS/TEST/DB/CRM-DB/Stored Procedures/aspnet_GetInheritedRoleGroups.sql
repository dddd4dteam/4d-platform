﻿CREATE PROCEDURE aspnet_GetInheritedRoleGroups
	@RoleId UNIQUEIDENTIFIER
AS

	SET NOCOUNT ON 
	
	;WITH CTE AS
	(
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   0 AS [Level]
		FROM   aspnet_Groups
		JOIN aspnet_RolesInGroups ON aspnet_RolesInGroups.GroupId = aspnet_Groups.GroupId
		WHERE aspnet_RolesInGroups.RoleId = @RoleId AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Roles WHERE RoleId = @RoleId)
	    
		UNION ALL
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   CTE.[Level] + 1 AS [Level]
		FROM   aspnet_Groups
			   JOIN CTE ON  aspnet_Groups.GroupId = CTE.ParentGroupId
		AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Roles WHERE RoleId = @RoleId)
	)
	SELECT DISTINCT GroupId,
		   GroupName
	FROM   CTE
	OPTION (MAXRECURSION 0)