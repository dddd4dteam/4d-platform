﻿CREATE PROCEDURE aspnet_UsersInRoles_IsUserInRole
	@ApplicationName NVARCHAR(256),
	@UserName NVARCHAR(256),
	@RoleName NVARCHAR(256)
AS
BEGIN
	DECLARE @ApplicationId UNIQUEIDENTIFIER
	SELECT @ApplicationId = NULL
	SELECT @ApplicationId = ApplicationId
	FROM   aspnet_Applications
	WHERE  LOWER(@ApplicationName) = LoweredApplicationName
	
	IF (@ApplicationId IS NULL)
	    RETURN(2)
	
	DECLARE @UserId UNIQUEIDENTIFIER
	SELECT @UserId = NULL
	DECLARE @RoleId UNIQUEIDENTIFIER
	SELECT @RoleId = NULL
	
	SELECT @UserId = UserId
	FROM   aspnet_Users
	WHERE  LoweredUserName = LOWER(@UserName)
	       AND ApplicationId = @ApplicationId
	
	IF (@UserId IS NULL)
	    RETURN(2)
	
	SELECT @RoleId = RoleId
	FROM   aspnet_Roles
	WHERE  LoweredRoleName = LOWER(@RoleName)
	       AND ApplicationId = @ApplicationId
	
	IF (@RoleId IS NULL)
	    RETURN(3)
	
	-- Custom from Membership Administrator: ValidFrom and ValidTo
	-- IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
	IF (
	       EXISTS(
	           SELECT *
	           FROM   aspnet_UsersInRoles
	           WHERE  UserId = @UserId
	                  AND RoleId = @RoleId
	                  AND (
							   (ValidFrom IS NULL AND ValidTo IS NULL)
							   OR DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), 0) BETWEEN ValidFrom 
								  AND ValidTo
						   )
	       )
	   )
	    RETURN(1)
	ELSE
	    RETURN(0)
END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_ReportingAccess]
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_BasicAccess]
    AS [dbo];

