﻿CREATE PROCEDURE aspnet_GetGroupParents
	@GroupId UNIQUEIDENTIFIER
AS

	SET NOCOUNT ON 
	
	;WITH CTE AS
	(
		SELECT GroupId,
			   GroupName,
			   ParentGroupId,
			   0 AS [Level]
		FROM   aspnet_Groups
		WHERE  GroupId = @GroupId
		UNION ALL
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   CTE.[Level] + 1 AS [Level]
		FROM   aspnet_Groups
			   JOIN CTE
					ON  aspnet_Groups.GroupId = CTE.ParentGroupId
	)
	SELECT GroupId,
		   GroupName,
		   [Level]
	FROM   CTE 
	WHERE GroupId <> @GroupId
	OPTION (MAXRECURSION 0)