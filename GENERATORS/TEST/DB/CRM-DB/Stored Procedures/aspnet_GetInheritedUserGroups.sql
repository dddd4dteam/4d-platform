﻿CREATE PROCEDURE aspnet_GetInheritedUserGroups
	@UserId UNIQUEIDENTIFIER
AS

	SET NOCOUNT ON 
	
	;WITH CTE AS
	(
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   0 AS [Level]
		FROM   aspnet_Groups
		JOIN aspnet_UsersInGroups ON aspnet_UsersInGroups.GroupId = aspnet_Groups.GroupId
		WHERE aspnet_UsersInGroups.UserId = @UserId AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Users WHERE UserId = @UserId)
	    
		UNION ALL
		SELECT aspnet_Groups.GroupId,
			   aspnet_Groups.GroupName,
			   aspnet_Groups.ParentGroupId,
			   CTE.[Level] + 1 AS [Level]
		FROM   aspnet_Groups
			   JOIN CTE ON  aspnet_Groups.GroupId = CTE.ParentGroupId
		AND aspnet_Groups.ApplicationId = (SELECT ApplicationId FROM aspnet_Users WHERE UserId = @UserId)
	)
	SELECT DISTINCT GroupId,
		   GroupName
	FROM   CTE
	OPTION (MAXRECURSION 0)