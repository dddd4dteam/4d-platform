﻿CREATE PROCEDURE aspnet_Membership_ChangeRoleName
	@ApplicationName NVARCHAR(256),
	@OldRoleName NVARCHAR(256),
	@NewRoleName NVARCHAR(256),
	@Description NVARCHAR(256)
AS
BEGIN
	
	SET NOCOUNT ON
	
	DECLARE @RoleId AS UNIQUEIDENTIFIER, @ApplicationId AS UNIQUEIDENTIFIER	
	
	-- Lookup the ApplicationId
	
	SELECT @ApplicationId = ApplicationId
	FROM   aspnet_Applications
	WHERE  LoweredApplicationName = LOWER(@ApplicationName)
	
	-- Lookup the RoleId
	
	SELECT @RoleId = RoleId
	FROM   aspnet_Roles
	WHERE  ApplicationId = @ApplicationId
	       AND LoweredRoleName = LOWER(@OldRoleName)
	
	-- Change the rolename
	
	UPDATE aspnet_Roles
	SET    RoleName = @NewRoleName,
	       LoweredRoleName = LOWER(@NewRoleName),
	       [Description] = @Description
	WHERE  ApplicationId = @ApplicationId
	       AND RoleId = @RoleId
END