﻿---Client_vw
CREATE view [dbo].[Client_vw]
as
SELECT	cl.Id_Client, cl.Code_Client, Client,
		cl.Id_Holding, hl.Holding,
		IsNull(cl.Id_ClientType,0) as Id_ClientType, IsNull(tp.ClientType,'Не указан') as ClientType,
		clr.Id_ClientRelation, clr.ClientRelation,
		ISNULL(ISNULL(g4.Id_GeographyItem,g3.Id_GeographyItem),g2.Id_GeographyItem) AS Id_Country, ISNULL(ISNULL(g4.GeographyItem,g3.GeographyItem),g2.GeographyItem) AS Country,
		ISNULL(g3.Id_GeographyItem,g2.Id_GeographyItem) AS Id_Region, ISNULL(g3.GeographyItem,g2.GeographyItem) AS Region,
		g2.Id_GeographyItem AS Id_SubRegion, g2.GeographyItem AS SubRegion,
		g1.Id_GeographyItem AS Id_Town, ISNULL(g1.Prefix,'') + g1.GeographyItem AS Town,
		CASE WHEN Street IS NOT NULL THEN 'ул.'+ Street ELSE '' End
			+ CASE WHEN HouseNumber IS NOT NULL Then ', д.'+HouseNumber ELSE '' END
			+ CASE WHEN HousePartNumber IS NOT NULL THEN ', корп.'+HousePartNumber ELSE '' END
			+ CASE WHEN FlatNumber IS NOT NULL THEN ', кв.'+ FlatNumber ELSE '' END
			+ CASE WHEN ISNULL(cl.Id_GeographyItemRynok,-1) <> -1 THEN ', р-к '+r.GeographyItem ELSE '' END
			+ CASE WHEN cl.PavilionNumber Is NOT NULL THEN ', пав.'+cl.PavilionNumber ELSE '' END
		AS Address,
		case when AddressOther is not null and AddressOther not like '' then AddressOther else 'н/у' end AddressOther,
		cl.Id_TrRepres, tr.Description as TrRepres, tr.Description+' ('+RTrim(tr.Name)+')' as TrRepresN,
		m.Id_Manager, m.Description as Manager,
		d.Id_Director, d.Description as Director, d.Description+' ('+Rtrim(d.Name)+')' as DirectorN,
		cl.postcode,
		cl.PhoneNumber,
		cl.Mail,
		cl.WebAddress,
		cl.AdditionInfo,
		cl.Flag_Enable,
		cl.Flag_BrandCersanit,
		cl.Flag_BrandOposzno,
		cl.Flag_Warehouse,
		cl.Dt_Add,
		cl.Dt_Dell,
		isnull(np.Flag_NeedPassed,0) Flag_NeedPassed,
		clp.Id_ClientPrestigeLevel, clp.ClientPrestigeLevel,
		IsNull(Cast(cl.Latitude as varchar(max))+' '+Cast(cl.Longitude as varchar(max)),'') as [GPS],
		UrName,cast([CloseTime] as time(0)) CloseTime,cast([OpenTime] as time(0)) OpenTime,
		Id_StoreSize
from crs_S8_Client cl WITH(NOLOCK)
Left join crs_S7_Holding_Register hl  WITH(NOLOCK)on(cl.Id_holding = hl.Id_Holding)
Left join crs_S8_ClientType_Register tp WITH(NOLOCK) on(cl.Id_ClientType = tp.Id_ClientType)
LEFT JOIN crs_S1_GeographyItem g1 WITH(NOLOCK) ON(cl.Id_GeographyItem = g1.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g2 WITH(NOLOCK) ON(g1.Id_GeographyParentItem = g2.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g3 WITH(NOLOCK) ON(g2.Id_GeographyParentItem = g3.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g4 WITH(NOLOCK) ON(g3.Id_GeographyParentItem = g4.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem r WITH(NOLOCK) ON(cl.Id_GeographyItemRynok = r.Id_GeographyItem)
Inner join crs_TrRepres tr WITH(NOLOCK) on(cl.Id_TrRepres = tr.Id_TrRepres)
Inner join crs_Manager m WITH(NOLOCK) on(tr.Id_Manager = m.Id_Manager)
Inner join crs_Director d WITH(NOLOCK) on(m.Id_Director = d.Id_Director)
INNER JOIN crs_S8_ClientRelation_Register clr WITH(NOLOCK) ON(cl.Id_ClientRelation = clr.Id_ClientRelation)
LEFT JOIN crs_S8_ClientPrestigeLevel_Register clp WITH(NOLOCK) ON(cl.Id_ClientPrestigeLevel = clp.Id_ClientPrestigeLevel)
Left join sys_Flags_NeedPassed np on (cl.id_client=np.id_object and np.PrefixNameObject='S8')





