﻿



CREATE view [dbo].[Ekspositor_vw]
as
Select	t1.Id_Ekspositor, t1.Code_Ekspositor,t1.ID_Construction, t5.Flag_BuyConstr, t1.Id_Diler, t1.ID_Client, t2.Code_Client, t1.InventoryNumber, 
		t4.[Description] as Construction,
		CASE WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Выполнен' ELSE 'Не выполнен' END AS Ekspositor_Status,
		t2.Client, t2.Holding, t2.Id_Town, t2.Town, t2.Address, t2.Id_Region, t2.Region, t2.Id_TrRepres, t2.TrRepres, t2.TrRepresN,
		t2.Id_Director, t2.Director, t3.Diler, t3.Holding as DilerHolding, t1.Id_PanelSet,
		(	SELECT     ISNULL(COUNT(*), 0) 
			FROM          dbo.crs_S10_EkspositorOrderPanelSetItem
            WHERE      Id_PanelSet = t1.Id_PanelSet
		) AS NumberPanels,
		t7.MinSetFoto
		--CASE WHEN t4.[Description] like 'Нестандартный экспозитор' THEN 0 ELSE isnull((t4.PanelCount+3),0) END as MinSetFoto,
from crs_S10_EkspositorOrder t1
INNER JOIN crs_S10_EkspositorOrder t5 ON(t1.Id_Ekspositor = t5.Id_order)
Inner join Client_vw t2 on(t1.Id_client = t2.Id_client)
Inner join crs_S6_Diler t3 on(t1.Id_diler = t3.Id_diler)
left join dbo.MinSetFotoForAppInventarizationEkspositor t7 on(t1.Id_Construction = t7.Id_Construction)
Inner join crs_S10_Construction_Register t4 on(t1.Id_Construction = t4.Id_Construction)
Where t1.Flag_LastOrder = 1 and t1.Flag_Deleted = 0


