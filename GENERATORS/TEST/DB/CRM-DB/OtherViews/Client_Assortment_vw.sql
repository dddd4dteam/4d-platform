﻿

CREATE view [dbo].[Client_Assortment_vw]
AS
select t6.Id_Client, t6.Code_Client,  
       t2.Id_Colection AS Id_ColLection, t3.Id_System as Id_Collection_System, t3.Colection as Collection,
       t2.Id_AssortGrupa AS Id_Grupa, t4.Id_System as Id_Grupa_System, t4.AssortGrupa as Grupa, 
       t1.Id_ClientAssortStatus, t5.ClientAssortStatus,
       t1.Id_Assortment  
from crs_S8_ClientAssortment t1
Inner join crs_S2_Assortment t2 on(t1.Id_Assortment = t2.Id_Assortment)
Inner join crs_S2_Colection t3 on(t2.Id_Colection = t3.Id_Colection)
Inner join crs_S2_AssortGrupa_Register t4 on(t2.Id_AssortGrupa = t4.Id_AssortGrupa)
Inner join crs_S8_ClientAssortStatus_Register t5 on(t1.Id_ClientAssortStatus = t5.Id_ClientAssortStatus)
Inner join crs_S8_Client t6 on(t1.Id_Client = t6.Id_Client)

