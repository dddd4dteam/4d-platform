﻿





CREATE view [dbo].[Client_lst_vw]
as
select Id_Client, Code_Client, 1 as detail_order, 'ID клиента' as Detail_name, Cast(Code_Client as Varchar(10)) as Detail from Client_vw Union All
select Id_Client, Code_Client, 2 as detail_order, 'Название' as Detail_name, Client as Detail from Client_vw Union All
select Id_Client, Code_Client, 3, 'Торговый представитель', LTrim(TrRepresN) from Client_vw Union All
select Id_Client, Code_Client, 4, 'Холдинг', LTrim(Holding) from Client_vw Union All
select Id_Client, Code_Client, 5, 'Активность', Case Flag_Enable when 1 Then 'Работает' Else 'Не работает' End from Client_vw Union All
select Id_Client, Code_Client, 7, 'Тип', LTrim(ClientType) from Client_vw Union All
select Id_Client, Code_Client, 8, 'Статус отношений', LTrim(ClientRelation) from Client_vw Union All
select Id_Client, Code_Client, 9, 'Адрес', LTrim(country+', '+region+', '+subregion+', '+town+', '+Address) from Client_vw Union All
select Id_Client, Code_Client, 10, 'Доп. адрес', LTrim(AddressOther) from Client_vw Union All
select Id_Client, Code_Client, 11, 'Телефон компании', LTrim(PhoneNumber) from Client_vw Union All
select Id_Client, Code_Client, 12, 'E-mail', LTrim(Mail) from Client_vw Union All
select Id_Client, Code_Client, 13, 'Web адрес', LTrim(WebAddress) from Client_vw Union All
select Id_Client, Code_Client, 14, 'Доп. инфо', AdditionInfo from Client_vw





