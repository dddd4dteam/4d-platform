﻿






CREATE view [dbo].[Client_maping_vw]
AS
Select  t1.Id_Client as [ID], 
		t1.Client as [Точка], 
		t11.Description as [Директор],
		t3.Description as [Представитель],
		t2.GeographyItem as [Город],
		IsNull(t4.ClientType,'') as [Тип точки],
		IsNull(t5.ClientPrestigeLevel,'') as [Престижность точки],
		IsNull(t6.ClientRelation,'') as [Статус отношений],
		IsNull(t7.StoreSize,'') as [Площадь POS],
		IsNull(Right(Convert(char(16),t1.OpenTime,121),5),'') as [Время открытия],
		IsNull(Cast(t1.SellerCount as varchar(max)),'') as [Количество продавцов],
		Case t1.Flag_Design When 1 Then 'Есть' Else 'Нет' End as [Услуга дизайн проекта],
		Case t1.Flag_Warehouse When 1 Then 'Есть' Else 'Нет' End as [Складская программа],
		IsNull(Cast(t1.Cost_2D_Rub_Rovese as varchar(max)),'') as [Сумма продаж 2D Rovese],
		IsNull(Cast(t1.Cost_3D_Rub_Rovese as varchar(max)),'') as [Сумма продаж 3D Rovese],
		IsNull(Cast(t1.Cost_2D_Rub_Total as varchar(max)),'') as [Сумма продаж 2D Всего],
		IsNull(Cast(t1.Cost_3D_Rub_Total as varchar(max)),'') as [Сумма продаж 3D Всего],
		IsNull(t8.PresentationType,'') as [Путь презентации плитки],
		IsNull(t9.PriceType,'') as [Ценовая сегментация],
		Case t1.Flag_BrandCersanit When 1 Then 'Да' Else 'Нет' End as [Работает с брендом Cersanit],
		Case t1.Flag_BrandOposzno When 1 Then 'Да' Else 'Нет' End as [Работает с брендом Oposzno],
		Case t1.Flag_BrandMeissen When 1 Then 'Да' Else 'Нет' End as [Работает с брендом Meissen],
		(Select Count(*) from crs_S8_ClientDiler where Id_Client = t1.Id_Client) as [Количество дилеров],
		(Select Count(*) from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0) as [Количество товарных групп],
		(Select Count(*) from crs_S8_ClientMaterial where Id_Client = t1.Id_Client) as [Количество оборудования],
		(Select Count(*) from crs_S8_ClientRival where Id_Client = t1.Id_Client) as [Количество конкурентов],
		(Select Count(*) from  crs_S8_ClientAssortment where Id_Client = t1.Id_Client) as [Количество коллекций],
		IsNull(Cast(t1.Latitude as varchar(max))+'N '+Cast(t1.Longitude as varchar(max))+'E','') as [GPS]
from crs_S8_Client t1
Inner join crs_S1_GeographyItem t2 on(t1.Id_GeographyItem = t2.Id_GeographyItem)
inner join crs_TrRepres t3 on(t1.Id_TrRepres = t3.Id_TrRepres)
inner join crs_Manager t10 on(t3.Id_Manager = t10.Id_Manager)
Inner join crs_Director t11 on(t10.Id_Director = t11.Id_Director)
Left join crs_S8_ClientType_Register t4 on(t1.Id_ClientType = t4.Id_ClientType)
Left join crs_S8_ClientPrestigeLevel_Register t5 on(t1.Id_ClientPrestigeLevel = t5.Id_ClientPrestigeLevel)
Left join crs_S8_ClientRelation_Register t6 on(t1.Id_ClientRelation = t6.Id_ClientRelation)
Left join crs_S16_StoreSize_Register t7 on(t1.Id_StoreSize = t7.Id_StoreSize)
Left join crs_S16_PresentationType_Register t8 on(t1.Id_PresentationType = t8.Id_PresentationType)
Left join crs_S16_PriceType_Register t9 on(t1.Id_PriceType = t9.Id_PriceType)
Where	Flag_Enable = 1 And 
		-- не учитываем Проба и Беларусь
		T3.Id_TrRepres Not In('99999','00047') And
		--  заполнено поле "Тип точки"
		t4.Id_ClientType Is Not Null And
		-- заполнено поле "Статус отношений"
		t6.Id_ClientRelation Is Not Null And
		-- заполнено поле "Площадь POS"
		t7.Id_StoreSize Is Not Null And
		-- заполнено поле "Время открытия"
		t1.OpenTime Is Not Null And
		-- заполнено поле "Количество продавцов"
		t1.SellerCount Is Not Null And
		-- стоит значение отличное от нуля хотя бы в одном из полей "Сумма продаж 2D Rovese", "Сумма продаж 3D Rovese", "Сумма продаж 2D Всего" или "Сумма продаж 3D Всего"
		(IsNull(t1.Cost_2D_Rub_Rovese,0) + IsNull(t1.Cost_3D_Rub_Rovese,0) + IsNull(t1.Cost_2D_Rub_Total,0) + IsNull(t1.Cost_3D_Rub_Total,0)) <> 0 And
		-- заполнено поле "Ценовая сегментация"
		t9.Id_PriceType Is Not Null And
		-- указана хотя бы одна группа на закладке "Товарные Группы"
		(Select Count(*) from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0) <> 0 And
		-- не учитываем точки "Офис дилера" и "Склад дилера"
		t4.Id_ClientType Not In(11, 12) And
		-- если на закладке  "Товарные Группы" указано наличие плиты то поле "Путь презентации плитки" должно быть заполнено
		(t1.Id_PresentationType Is Not Null Or Not Exists(select 1 from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0 and Id_GoodsCategory = 'E01')) And
		-- если точка является нашим клиентом то должна быть информация о экспозиции на закладке "Коллекции" или в модуле "Экспозиторы"
		(t1.Id_ClientRelation <> 1 Or Exists(Select 1 from crs_S8_ClientAssortment where Id_Client = t1.Id_Client) Or Exists(select 1 from crs_S10_EkspositorOrder tt1 Inner join crs_S10_Construction_Register tt2 on(tt1.ID_Construction = tt2.ID_Construction) where tt1.Id_Client = t1.Id_Client and tt1.Flag_LastOrder = 1 and tt2.ID_ConstructionType = 1)) And
		-- на закладке "Конкуренты" должна быть информация
		(Exists(select 1 from crs_S8_ClientRival where Id_Client = t1.Id_Client and Quantity <> 0)) And
		-- заполнено поле "Геопозиция"
		(t1.Latitude Is Not Null and t1.Longitude Is Not Null)






