﻿CREATE VIEW [ClientAspRole_vw]
as
SELECT t3.Id_Client, t2.RoleId
FROM aspnet_Roles t2
INNER JOIN crs_S8_Client t3 ON(t2.ImportSystemID = t3.Id_TrRepres)
