﻿
CREATE view [dbo].[EkspositorOrder_vw]
as
Select	t1.Id_Order, t1.Code_Order, t1.Code_Ekspositor, t1.ID_OrderType,
		IsNull(t6.Flag_Approved1,0) as Flag_Approved1, IsNull(t6.Flag_Approved2,0) as Flag_Approved2, IsNull(t6.Flag_Approved3,0) as Flag_Approved3,
		t1.ID_Construction, t1.Id_PanelSet, t1.Flag_BuyConstr, t1.Flag_Inventory,
		t1.Id_Diler, t1.Id_Client, t1.DtOrderCreate, t6.DtOrderApproved, t1.DtOrderReady, t1.DtWillBeReady, t1.DtBuhTransaction, t1.DtExecutionConfirm,
		t1.Flag_OrderReady, t1.Flag_BuhTransaction, t1.Flag_ExecutionConfirm, IsNull(t6.Flag_UnApproved,0) as Flag_UnApproved, t1.Flag_LastOrder, t1.LastOrderMaxPosition, t1.OrderMaxPosition,
		t1.Flag_BuyPanels, t1.Flag_OurDelivery, t1.Id_StandartSet, t1.ImageIndexActual, t1.TPredAspRoleID, t1.CreatorUserID, t1.ChangerUserID,
		t1.DtChanged, t1.InventoryNumber, t1.NumberPanels, t1.Id_Previous, t1.Id_Ekspositor, t1.Flag_Deleted,
		isnull(np.Flag_NeedPassed,0) Flag_NeedPassed,isnull(np.Flag_WasPassed,0) Flag_WasPassed,
		t4.Description as Construction, t2.Client, t2.Holding, t2.Id_Town, t2.Town, t2.Address, t2.Id_Region, t2.Region, t2.Id_TrRepres, t2.TrRepres, t2.TrRepresN,
		t2.Code_Client, t2.Id_Director, t2.Director, t3.Diler, t3.Holding AS DilerHolding,
		CASE  WHEN  (t1.Flag_BuyPanels = 1 AND t1.Flag_BuyConstr =0) THEN 'Панели' 
		   WHEN  (t1.Flag_BuyPanels = 0 AND t1.Flag_BuyConstr =1) THEN 'Конструкция'
		   WHEN  (t1.Flag_BuyPanels = 1 AND t1.Flag_BuyConstr =1) THEN 'Панели, Конструкция'
		   WHEN  (t1.Flag_BuyPanels = 0 AND t1.Flag_BuyConstr =0) THEN ''
		END AS Compensation,
		CASE WHEN IsNull(t6.Flag_UnApproved,0) = 1 THEN 'Разутвержден' WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Закрыт' WHEN t1.Flag_BuhTransaction = 1 THEN 'Подтв. исполн.'
                       WHEN t1.Flag_OrderReady = 1 THEN 'Ждет отгрузки' WHEN IsNull(t6.Flag_Approved3,0) = 1 THEN 'Утвержден (обр-ся)' WHEN IsNull(t6.Flag_Approved2,0) = 1 THEN 'Утв. Отд.Аналит.'
                       WHEN IsNull(t6.Flag_Approved1,0) = 1 THEN 'Утв. Отд.Прод.' WHEN IsNull(t6.Flag_Approved1,0) = 0 THEN 'Утв. Рег.Дир.' END AS Order_State,
		t5.Description AS OrderType
from crs_S10_EkspositorOrder t1
Left join crs_S10_OrderApproval t6 on(t1.Id_Order = t6.Id_Order)
Inner join Client_vw t2 on(t1.Id_client = t2.Id_client)
Inner join crs_S6_Diler t3 on(t1.Id_diler = t3.Id_diler)
Inner join crs_S10_Construction_Register t4 on(t1.Id_Construction = t4.Id_Construction)
INNER JOIN crs_S10_EkspositorOrderType_Register t5 ON(t1.ID_OrderType = t5.ID_OrderType)
Left join sys_Flags_NeedPassed np on(t1.id_order=np.id_object and np.PrefixNameObject='S10')
Where t1.Flag_Deleted = 0







