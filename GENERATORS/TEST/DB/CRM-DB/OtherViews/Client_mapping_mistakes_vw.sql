﻿

CREATE view [dbo].[Client_mapping_mistakes_vw]
AS
Select  t1.Id_Client as [ID], 
		t1.Client as [Точка], 
		t11.Description as [Директор],
		t3.Description as [Представитель],
		t2.GeographyItem as [Город],
		Case When t1.Id_ClientType Is Not Null Then 'Правильно' Else 'Ошибка' End as [Тип точки],
		Case When t7.Id_StoreSize Is Not Null Then 'Правильно' Else 'Ошибка' End as [Площадь POS],
		Case When (IsNull(t1.Cost_2D_Rub_Rovese,0) + IsNull(t1.Cost_3D_Rub_Rovese,0) + IsNull(t1.Cost_2D_Rub_Total,0) + IsNull(t1.Cost_3D_Rub_Total,0)) <> 0 Then 'Правильно' Else 'Ошибка' End as [Продажи],
		Case When Cast(t1.Latitude as varchar(max))+'N '+Cast(t1.Longitude as varchar(max)) Is Not Null Then 'Правильно' Else 'Ошибка' End as [Геолокация],
		Case When t1.Id_ClientRelation Is Not Null Then 'Правильно' Else 'Ошибка' End as [Статус отношений],
		Case When t1.OpenTime Is Not Null Then 'Правильно' Else 'Ошибка' End as [Время открытия],
		Case When t1.SellerCount Is Not Null Then 'Правильно' Else 'Ошибка' End as [Количество продавцов],
		Case When t1.Id_PriceType Is Not Null Then 'Правильно' Else 'Ошибка' End as [Ценовая сегментация],
		Case When (Select Count(*) from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0) <> 0 Then 'Правильно' Else 'Ошибка' End as [Указаны Товарные группы],
		Case When (t1.Id_PresentationType Is Not Null Or Not Exists(select 1 from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0 and Id_GoodsCategory = 'E01')) Then 'Правильно' Else 'Ошибка' End as [Путь презентации плитки]
from crs_S8_Client t1
Inner join crs_S1_GeographyItem t2 on(t1.Id_GeographyItem = t2.Id_GeographyItem)
inner join crs_TrRepres t3 on(t1.Id_TrRepres = t3.Id_TrRepres)
inner join crs_Manager t10 on(t3.Id_Manager = t10.Id_Manager)
Inner join crs_Director t11 on(t10.Id_Director = t11.Id_Director)
Left join crs_S8_ClientType_Register t4 on(t1.Id_ClientType = t4.Id_ClientType)
Left join crs_S8_ClientPrestigeLevel_Register t5 on(t1.Id_ClientPrestigeLevel = t5.Id_ClientPrestigeLevel)
Left join crs_S8_ClientRelation_Register t6 on(t1.Id_ClientRelation = t6.Id_ClientRelation)
Left join crs_S16_StoreSize_Register t7 on(t1.Id_StoreSize = t7.Id_StoreSize)
Left join crs_S16_PresentationType_Register t8 on(t1.Id_PresentationType = t8.Id_PresentationType)
Left join crs_S16_PriceType_Register t9 on(t1.Id_PriceType = t9.Id_PriceType)
Where	(Flag_Enable = 1 
		-- не учитываем Проба и Беларусь
		And t1.Id_TrRepres Not In('99999','00047')
		-- не учитываем точки "Офис дилера" и "Склад дилера"
		And IsNull(t4.Id_ClientType,0) Not In(11, 12)
		-- не учитываем DIY
		And t11.Description <> 'РегДир  DIY')
		AND
		(
		-- не заполнено поле "Тип точки"
		t1.Id_ClientType Is Null 
		-- не заполнено поле "Площадь POS"
		Or t7.Id_StoreSize Is Null
		-- не заполнено ни одного из полей "Сумма продаж 2D Rovese", "Сумма продаж 3D Rovese", "Сумма продаж 2D Всего" или "Сумма продаж 3D Всего"
		Or (IsNull(t1.Cost_2D_Rub_Rovese,0) + IsNull(t1.Cost_3D_Rub_Rovese,0) + IsNull(t1.Cost_2D_Rub_Total,0) + IsNull(t1.Cost_3D_Rub_Total,0)) = 0
		-- не заполнено поле "Геопозиция"
		Or Cast(t1.Latitude as varchar(max))+'N '+Cast(t1.Longitude as varchar(max)) Is Null
		-- не заполнено поле "Статус отношений"
		Or t1.Id_ClientRelation Is Null
		-- не заполнено поле "Время открытия"
		Or t1.OpenTime Is Null
		-- не заполнено поле "Количество продавцов"
		Or t1.SellerCount Is Null
		-- не заполнено поле "Ценовая сегментация"
		Or t1.Id_PriceType Is Null
		-- не указаны "Товарные Группы"
		Or (Select Count(*) from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0) = 0
		-- 
		Or (t1.Id_PresentationType Is Null and Exists(select 1 from crs_S8_ClientGoodsCategory where Id_Client = t1.Id_Client and PctAll <> 0 and Id_GoodsCategory = 'E01'))
		)




