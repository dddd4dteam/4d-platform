﻿CREATE VIEW [dbo].[PV_S10_DeliveryManagerOrder_Vo]
As
Select	t11.Id_TrRepres,
		t12.Id_Manager,
		t13.Id_Director,
		t1.*
from crs_S10_DeliveryManagerOrder t1
Inner join crs_S10_EkspositorOrder t2 on(t1.Id_Order = t2.Id_Order)
Inner join crs_S8_Client t10 on(t2.Id_Client = t10.Id_Client)
Inner join crs_TrRepres t11 on(t10.Id_TrRepres = t11.Id_TrRepres)
Inner join crs_Manager t12 on(t11.Id_Manager = t12.Id_Manager)
Inner join crs_Director t13 on(t12.Id_Director = t13.Id_Director)
