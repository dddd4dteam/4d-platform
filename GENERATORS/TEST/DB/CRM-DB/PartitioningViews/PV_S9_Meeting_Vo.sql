﻿Create view PV_S9_Meeting_Vo
As
Select	t12.Id_Manager,
		t13.Id_Director,
		t1.*
from crs_S9_Meeting t1
Inner join crs_TrRepres t11 on(t1.Id_TrRepres = t11.Id_TrRepres)
Inner join crs_Manager t12 on(t11.Id_Manager = t12.Id_Manager)
Inner join crs_Director t13 on(t12.Id_Director = t13.Id_Director)