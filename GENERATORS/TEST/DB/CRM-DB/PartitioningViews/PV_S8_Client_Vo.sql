﻿CREATE view [dbo].[PV_S8_Client_Vo]
As
select	t12.Id_Manager,
		t13.Id_Director,
		t1.*
from crs_S8_Client t1
Inner join crs_TrRepres t11 on(t1.Id_TrRepres = t11.Id_TrRepres)
Inner join crs_Manager t12 on(t11.Id_Manager = t12.Id_Manager)
Inner join crs_Director t13 on(t12.Id_Director = t13.Id_Director)