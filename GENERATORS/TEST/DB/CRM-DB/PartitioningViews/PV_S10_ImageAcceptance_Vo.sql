﻿CREATE VIEW [dbo].[PV_S10_ImageAcceptance_Vo]
	AS SELECT 
	PV_S10_EkspositorOrderImage_Vo.Id_TrRepres,
	PV_S10_EkspositorOrderImage_Vo.Id_Manager,
	PV_S10_EkspositorOrderImage_Vo.Id_Director,
	crs_S10_ImageAcceptance.* 
	FROM [crs_S10_ImageAcceptance] JOIN PV_S10_EkspositorOrderImage_Vo
	ON crs_S10_ImageAcceptance.Id_Image = PV_S10_EkspositorOrderImage_Vo.ID
