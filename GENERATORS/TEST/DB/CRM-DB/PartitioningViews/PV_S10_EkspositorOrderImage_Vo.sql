﻿CREATE view PV_S10_EkspositorOrderImage_Vo
As
select Case When t1.Id_ImageType In(1,2) Then t8.Id_TrRepres When t1.Id_ImageType In(3) Then t4.Id_TrRepres When t1.Id_ImageType In(4) Then t12.Id_TrRepres End as Id_TrRepres,
  Case When t1.Id_ImageType In(1,2) Then t9.Id_Manager When t1.Id_ImageType In(3) Then t5.Id_Manager When t1.Id_ImageType In(4) Then t13.Id_Manager End as Id_Manager,
  Case When t1.Id_ImageType In(1,2) Then t10.Id_Director When t1.Id_ImageType In(3) Then t6.Id_Director When t1.Id_ImageType In(4) Then t14.Id_Director  End as Id_Director,
  t1.*
from crs_S10_EkspositorOrderImage t1
Left join crs_S9_Meeting t2 on(t1.Id_Order = t2.Id_Meeting)
Left join crs_S10_EkspositorOrder t3 on(t1.Id_Order = t3.Id_Order)
Left join crs_S8_Client t12 on(t1.Id_Order = t12.Id_Client)
Left join crs_S8_Client t4 on(t2.Id_Client = t4.Id_Client)
Left join crs_TrRepres t5 on(t4.Id_TrRepres = t5.Id_TrRepres)
Left join crs_Manager t6 on(t5.Id_Manager = t6.Id_Manager)
--Left join crs_Director t7 on(t6.Id_Director = t7.Id_Director)
Left join crs_S8_Client t8 on(t3.Id_Client = t8.Id_Client)
Left join crs_TrRepres t9 on(t8.Id_TrRepres = t9.Id_TrRepres)
Left join crs_Manager t10 on(t9.Id_Manager = t10.Id_Manager)
--Left join crs_Director t11 on(t10.Id_Director = t11.Id_Director)
Left join crs_TrRepres t13 on(t12.Id_TrRepres = t13.Id_TrRepres)
Left join crs_Manager t14 on(t13.Id_Manager = t14.Id_Manager)
--Left join crs_Director t15 on(t14.Id_Director = t15.Id_Director)