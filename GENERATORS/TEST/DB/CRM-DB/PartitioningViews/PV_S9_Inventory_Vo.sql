﻿Create View PV_S9_Inventory_Vo As Select
t1.Id_TrRepres,
t1.Id_Manager,
t1.Id_Director,
t0.*
From crs_S9_Inventory t0
Inner Join PV_S9_Meeting_Vo t1 On t0.Id_Meeting = t1.Id_Meeting