﻿CREATE VIEW dbo.PV_S17_Answer_Vo
AS
SELECT dbo.crs_TrRepres.Id_TrRepres, 
       dbo.crs_Manager.Id_Manager, 
       dbo.crs_Director.Id_Director,        
       dbo.crs_S17_Answer.*
FROM   dbo.crs_S17_Answer INNER JOIN
       dbo.crs_S9_Meeting ON dbo.crs_S17_Answer.ID_Meeting = dbo.crs_S9_Meeting.Id_Meeting INNER JOIN
       dbo.crs_TrRepres ON dbo.crs_S9_Meeting.Id_TrRepres = dbo.crs_TrRepres.Id_TrRepres INNER JOIN
       dbo.crs_Manager ON dbo.crs_TrRepres.Id_Manager = dbo.crs_Manager.Id_Manager INNER JOIN
       dbo.crs_Director ON dbo.crs_Manager.Id_Director = dbo.crs_Director.Id_Director

