﻿
/*V_S9_Meeting_Vo*/

CREATE VIEW [dbo].[V_S9_Meeting_Vo]
AS
SELECT    TOP (100) PERCENT ISNULL(dbo.crs_S8_Client.Client, '') AS Client, dbo.crs_S9_Meeting.Id_Meeting, dbo.crs_S9_Meeting.Id_Client,Code_Client, dbo.crs_S9_MeetingApproval.Flag_Approved, dbo.crs_S9_Meeting.Description,
 dbo.crs_S9_Meeting.Result, dbo.crs_S9_Meeting.MadeByUserRole, dbo.crs_S9_Meeting.MadeByUserID, dbo.crs_S9_MeetingStatus_Register.Description_RUS AS StatusDescription,
 dbo.crs_S9_Meeting.Id_Status, dbo.crs_S9_Meeting.res_IllDo, dbo.crs_S9_Meeting.res_TalkingTheme, dbo.crs_S9_Meeting.res_FutureMeetingDate, dbo.crs_S9_Meeting.Priority,
 dbo.crs_S9_Meeting.MadeByUserRoleOld, dbo.crs_S9_Meeting.MadeByUserIDOld, dbo.crs_S9_Meeting.Latitude, dbo.crs_S9_Meeting.Longitude,
 dbo.crs_S9_Meeting.DistanceToClient, dbo.crs_S9_Meeting.ActualTime, '0' AS OwningResourceId, '0' AS OwningCalendarId, dbo.crs_S9_Meeting.Id_TrRepres, dbo.crs_S9_Meeting.StartTime,
 dbo.crs_S9_Meeting.EndTime, dbo.crs_S9_Meeting.Id_MeetingType, dbo.crs_S9_MeetingType_Register.Description AS MeetingType, '' AS Caption, '' AS Categories,
 dbo.crs_S9_Meeting.Id_MeetingPositionSource, dbo.crs_S8_Client.Longitude AS Client_Longitude, dbo.crs_S8_Client.Latitude AS Client_Latitude
FROM    dbo.crs_S9_Meeting LEFT OUTER JOIN 
 dbo.crs_S8_Client ON dbo.crs_S9_Meeting.Id_Client = dbo.crs_S8_Client.Id_Client INNER JOIN
 dbo.crs_S9_MeetingApproval ON dbo.crs_S9_Meeting.Id_Meeting = dbo.crs_S9_MeetingApproval.Id_Meeting INNER JOIN
 dbo.crs_S9_MeetingStatus_Register ON dbo.crs_S9_Meeting.Id_Status = dbo.crs_S9_MeetingStatus_Register.Id_MeetingStatus INNER JOIN
 dbo.crs_S9_MeetingType_Register ON dbo.crs_S9_Meeting.Id_MeetingType = dbo.crs_S9_MeetingType_Register.Id_MeetingType INNER JOIN
 dbo.crs_TrRepres ON dbo.crs_S9_Meeting.Id_TrRepres = dbo.crs_TrRepres.Id_TrRepres LEFT OUTER JOIN
 dbo.crs_S9_MeetingPositionSource_Register ON dbo.crs_S9_Meeting.Id_MeetingPositionSource = dbo.crs_S9_MeetingPositionSource_Register.Id_MeetingPositionSource

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S9_Meeting"
            Begin Extent = 
               Top = 7
               Left = 506
               Bottom = 328
               Right = 723
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_Client"
            Begin Extent = 
               Top = 3
               Left = 256
               Bottom = 316
               Right = 503
            End
            DisplayFlags = 280
            TopColumn = 32
         End
         Begin Table = "crs_S9_MeetingStatus_Register"
            Begin Extent = 
               Top = 176
               Left = 289
               Bottom = 286
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S9_MeetingType_Register"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 212
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_TrRepres"
            Begin Extent = 
               Top = 102
               Left = 38
               Bottom = 232
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "crs_S9_MeetingPositionSource_Register"
            Begin Extent = 
               Top = 234
               Left = 38
               Bottom = 330
               Right = 265
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begi', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_Meeting_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'n CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3510
         Alias = 2880
         Table = 1800
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_Meeting_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_Meeting_Vo';

