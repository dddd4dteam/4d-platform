﻿CREATE VIEW [dbo].[V_S10_DeliveryOrder]
AS
SELECT      t1.Id_Order, t1.Code_Order, t17.DtOrderApproved, 
			t18.Id_DeliveryManager, 
			CASE WHEN (t1.Id_OrderType = 1 AND t14.ID_ConstructionType = 1) THEN 'Заказ Стенда 2D' WHEN (t1.Id_OrderType = 1 AND 
                         t14.ID_ConstructionType IN (2, 3, 4)) THEN 'Заказ Стенда 3D' WHEN (t1.Id_OrderType = 1 AND t14.ID_ConstructionType = 5) THEN 'Заказ образцов' WHEN (t1.Id_OrderType = 2 AND 
                         t14.ID_ConstructionType = 1) THEN 'Переклейка Панелей' WHEN (t1.Id_OrderType = 2 AND t14.ID_ConstructionType = 5) THEN 'Замена Образцов' ELSE 'Неизвестно' END AS DeliveryOrderType, 
                         Case When Exists (Select 1 from crs_S10_DeliveryDate Where Id_DateType = 2 and Id_deliveryOrder = t1.Id_Order) Then 'Выполнен' Else 'Не Выполнен' End AS DeliveryOrderStatus, 
						 t12.ID_OrderType, t12.Description AS OrderType, 1 AS ID_DeliveryTransport, 'Неизвестно' AS DeliveryTransport, 
						 (Select Max([Date]) from crs_S10_DeliveryDate tt1 Inner join (Select Id_deliveryOrder, Id_DateType, Max(DtAdd) as DtAddMax from crs_S10_DeliveryDate Where Id_DateType = 1 and Id_deliveryOrder = t1.Id_Order Group by Id_deliveryOrder, Id_DateType) tt2 on (tt1.Id_deliveryOrder = tt2.Id_deliveryOrder and tt1.Id_DateType = tt2.Id_DateType and tt1.dtAdd = tt2.DtAddMax)) AS DeliveryDateTimePlanned, 
						 (Select Case Count(*) When 0 Then 0 Else Count(*)-1 End  as QtyDeliveryDatePlan from crs_S10_DeliveryDate Where Id_DateType = 1 and Id_deliveryOrder = t1.Id_Order) AS DeliveryDateTimePlannedQty,
						 (Select Max([Date]) from crs_S10_DeliveryDate Where Id_DateType = 2 and Id_deliveryOrder = t1.Id_Order)  AS DeliveryDateTimeActual, 
						 t14.ID_ConstructionType, t14.ConstructionType, t11.Id_Diler, t11.Diler, t5.Id_Director, t5.Name AS Director, t3.Id_TrRepres, 
                         t3.Name AS TrRepres, t6.GeographyItem AS City, t8.GeographyItem AS Region, t2.Id_Client, t2.Code_Client, t2.Client, t2.UrName, CASE WHEN t2.Street IS NOT NULL 
                         THEN 'ул.' + t2.Street ELSE '' END + CASE WHEN t2.HouseNumber IS NOT NULL THEN ', д.' + t2.HouseNumber ELSE '' END + CASE WHEN t2.HousePartNumber IS NOT NULL 
                         THEN ', корп.' + t2.HousePartNumber ELSE '' END + CASE WHEN t2.FlatNumber IS NOT NULL THEN ', кв.' + t2.FlatNumber ELSE '' END + CASE WHEN ISNULL(t2.Id_GeographyItemRynok, - 1) 
                         <> - 1 THEN ', р-к ' + t10.GeographyItem ELSE '' END + CASE WHEN t2.PavilionNumber IS NOT NULL THEN ', пав.' + t2.PavilionNumber ELSE '' END AS Address,
                         ISNULL(CAST(t2.Latitude AS varchar(MAX)) + ' ' + CAST(t2.Longitude AS varchar(MAX)), '') AS GPS,Convert(char(5), t2.OpenTime, 108) AS OpenTime, Convert(char(5),t2.CloseTime,108) AS CloseTime
FROM            dbo.crs_S10_EkspositorOrder AS t1 WITH (NOLOCK) INNER JOIN
                         dbo.crs_S8_Client AS t2 WITH (NOLOCK) ON t1.Id_Client = t2.Id_Client INNER JOIN
                         dbo.crs_TrRepres AS t3 WITH (NOLOCK) ON t2.Id_TrRepres = t3.Id_TrRepres INNER JOIN
                         dbo.crs_Manager AS t4 WITH (NOLOCK) ON t3.Id_Manager = t4.Id_Manager INNER JOIN
                         dbo.crs_Director AS t5 WITH (NOLOCK) ON t4.Id_Director = t5.Id_Director LEFT OUTER JOIN
                         dbo.crs_S1_GeographyItem AS t6 WITH (NOLOCK) ON t2.Id_GeographyItem = t6.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S1_GeographyItem AS t7 WITH (NOLOCK) ON t6.Id_GeographyParentItem = t7.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S1_GeographyItem AS t8 WITH (NOLOCK) ON t7.Id_GeographyParentItem = t8.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S1_GeographyItem AS t9 WITH (NOLOCK) ON t8.Id_GeographyParentItem = t9.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S1_GeographyItem AS t10 WITH (NOLOCK) ON t2.Id_GeographyItemRynok = t10.Id_GeographyItem INNER JOIN
                         dbo.crs_S6_Diler AS t11 WITH (NOLOCK) ON t1.Id_Diler = t11.Id_Diler INNER JOIN
                         dbo.crs_S10_EkspositorOrderType_Register AS t12 ON t1.ID_OrderType = t12.ID_OrderType INNER JOIN
                         dbo.crs_S10_Construction_Register AS t13 ON t1.ID_Construction = t13.ID_Construction INNER JOIN
                         dbo.crs_S10_ConstructionType_Register AS t14 ON t13.ID_ConstructionType = t14.ID_ConstructionType INNER JOIN
                         dbo.crs_S10_OrderApproval AS t17 WITH (NOLOCK) ON t1.Id_Order = t17.Id_Order INNER JOIN
                         dbo.crs_S10_DeliveryManagerOrder AS t18 WITH (NOLOCK) ON t1.Id_Order = t18.Id_Order
WHERE        (t1.ID_OrderType IN (1, 2)) AND (t1.Flag_ExecutionConfirm = 0) AND (t17.Flag_Approved3 = 1) AND (t1.Flag_Inventory = 0) AND (t1.Flag_Deleted = 0) AND (t1.Flag_BuyConstr = 0) OR
                         (t1.ID_OrderType IN (1, 2)) AND (t1.Flag_ExecutionConfirm = 0) AND (t17.Flag_Approved3 = 1) AND (t1.Flag_Inventory = 0) AND (t1.Flag_Deleted = 0) AND (t1.Flag_BuyPanels = 0)