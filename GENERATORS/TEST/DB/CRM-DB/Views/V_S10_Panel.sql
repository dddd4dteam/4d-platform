﻿
CREATE VIEW [dbo].[V_S10_Panel]
AS
SELECT Description, Width, Height, ID_PanelType, 
       ID_Panel, Code, Collection AS CollectionName, tmp_Flag_OnlyBase, 
       Id_Collection
FROM  dbo.crs_S10_Panel 
