﻿
/*V_S8_ClientDiler_Vo*/

CREATE VIEW [dbo].[V_S8_ClientDiler_Vo]
AS
SELECT dbo.crs_S8_Client.Client, dbo.crs_S8_ClientDiler.Id_ClientDiler, dbo.crs_S8_ClientDiler.Id_Client,Code_Client , dbo.crs_S6_Diler.Diler, dbo.crs_S6_Diler.Holding, dbo.crs_S6_Diler.Manager, dbo.crs_S6_Diler.Type,
  dbo.crs_S6_Diler.Country, dbo.crs_S6_Diler.AddressF, dbo.crs_S6_Diler.CompanyCode, dbo.crs_S6_Diler.Id_System, dbo.crs_S6_Diler.Id_System_Holding,
  dbo.crs_S6_Diler.Flag_enable, dbo.crs_S6_Diler.tmp_Id_Client, dbo.crs_S8_ClientDiler.Id_Diler
FROM dbo.crs_S8_Client INNER JOIN
  dbo.crs_S8_ClientDiler ON dbo.crs_S8_Client.Id_Client = dbo.crs_S8_ClientDiler.Id_Client INNER JOIN
  dbo.crs_S6_Diler ON dbo.crs_S8_ClientDiler.Id_Diler = dbo.crs_S6_Diler.Id_Diler
