﻿CREATE VIEW dbo.V_S14_Product_Vo
AS
SELECT        t1.Id_Product, t1.Goods, ISNULL(CASE WHEN t4.Id_Grupa IS NOT NULL THEN t4.Id_Grupa WHEN t3.Id_Grupa IS NOT NULL THEN t3.Id_Grupa WHEN t2.Id_Grupa IS NOT NULL THEN t2.Id_Grupa END, '') 
                         AS Id_Grupa, CASE WHEN t4.Grupa IS NOT NULL THEN t4.Grupa WHEN t3.Grupa IS NOT NULL THEN t3.Grupa WHEN t2.Grupa IS NOT NULL THEN t2.Grupa END AS Grupa, CASE WHEN t4.Id_Grupa IS NOT NULL 
                         THEN t3.Id_Grupa WHEN t3.Id_Grupa IS NOT NULL THEN t2.Id_Grupa END AS Id_SubGrupa, CASE WHEN t4.Grupa IS NOT NULL THEN t3.Grupa WHEN t3.Grupa IS NOT NULL THEN t2.Grupa END AS SubGrupa, 
                         CASE WHEN t4.Id_Grupa IS NOT NULL THEN t2.Id_Grupa END AS Id_Type, CASE WHEN t4.Grupa IS NOT NULL THEN t2.Grupa END AS Type, dbo.crs_S14_Format_Register.Id_Format, 
                         dbo.crs_S14_Format_Register.Format, dbo.crs_S14_Format_Register.K_Pcs, dbo.crs_S14_Format_Register.K_M, dbo.crs_S14_Format_Register.K_Box, 
                         dbo.crs_S14_Property_Register.Id_Property AS Id_Property1, dbo.crs_S14_Property_Register.Property AS Property1, dbo.crs_S14_PropertyType_Register.Id_PropertyType AS Id_PropertyType1, 
                         dbo.crs_S14_PropertyType_Register.PropertyType AS PropertyType1, crs_S14_Property_Register_1.Id_Property AS Id_Property2, crs_S14_Property_Register_1.Property AS Property2, 
                         crs_S14_PropertyType_Register_1.Id_PropertyType AS Id_PropertyType2, crs_S14_PropertyType_Register_1.PropertyType AS PropertyType2, crs_S14_Property_Register_2.Id_Property AS Id_Property3, 
                         crs_S14_Property_Register_2.Property AS Property3, crs_S14_PropertyType_Register_2.Id_PropertyType AS Id_PropertyType3, crs_S14_PropertyType_Register_2.PropertyType AS PropertyType3, 
                         dbo.crs_S2_Brand_Register.Id_Brand, dbo.crs_S2_Brand_Register.Brand, dbo.crs_S2_Colection.Id_Colection, dbo.crs_S2_Colection.Colection, dbo.crs_S12_Units_Register.Id_Units, 
                         dbo.crs_S12_Units_Register.Units, t1.Unique_ID
FROM            dbo.crs_S14_PropertyType_Register AS crs_S14_PropertyType_Register_2 INNER JOIN
                         dbo.crs_S14_Property_Register AS crs_S14_Property_Register_2 ON crs_S14_PropertyType_Register_2.Id_PropertyType = crs_S14_Property_Register_2.Id_PropertyType RIGHT OUTER JOIN
                         dbo.crs_S14_Format_Register RIGHT OUTER JOIN
                         dbo.crs_S2_Brand_Register INNER JOIN
                         dbo.crs_S14_Product AS t1 ON dbo.crs_S2_Brand_Register.Id_Brand = t1.Id_Brand INNER JOIN
                         dbo.crs_S2_Colection ON t1.Id_Colection = dbo.crs_S2_Colection.Id_Colection INNER JOIN
                         dbo.crs_S12_Units_Register ON t1.Id_Units = dbo.crs_S12_Units_Register.Id_Units INNER JOIN
                         dbo.crs_S13_Grupa_Register AS t2 ON t1.Id_Grupa = t2.Id_Grupa ON dbo.crs_S14_Format_Register.Id_Format = t1.Id_Format ON crs_S14_Property_Register_2.Id_Property = t1.Id_Property3 LEFT OUTER JOIN
                         dbo.crs_S14_PropertyType_Register AS crs_S14_PropertyType_Register_1 INNER JOIN
                         dbo.crs_S14_Property_Register AS crs_S14_Property_Register_1 ON crs_S14_PropertyType_Register_1.Id_PropertyType = crs_S14_Property_Register_1.Id_PropertyType ON 
                         t1.Id_Property2 = crs_S14_Property_Register_1.Id_Property LEFT OUTER JOIN
                         dbo.crs_S14_PropertyType_Register INNER JOIN
                         dbo.crs_S14_Property_Register ON dbo.crs_S14_PropertyType_Register.Id_PropertyType = dbo.crs_S14_Property_Register.Id_PropertyType ON 
                         t1.Id_Property1 = dbo.crs_S14_Property_Register.Id_Property LEFT OUTER JOIN
                         dbo.crs_S13_Grupa_Register AS t3 ON t2.Id_ParentGrupa = t3.Id_Grupa LEFT OUTER JOIN
                         dbo.crs_S13_Grupa_Register AS t4 ON t3.Id_ParentGrupa = t4.Id_Grupa

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[28] 4[38] 2[16] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = -232
      End
      Begin Tables = 
         Begin Table = "crs_S14_PropertyType_Register_2"
            Begin Extent = 
               Top = 421
               Left = 0
               Bottom = 513
               Right = 178
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_Property_Register_2"
            Begin Extent = 
               Top = 406
               Left = 242
               Bottom = 516
               Right = 420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_Format_Register"
            Begin Extent = 
               Top = 7
               Left = 260
               Bottom = 135
               Right = 433
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S2_Brand_Register"
            Begin Extent = 
               Top = 6
               Left = 691
               Bottom = 134
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t1"
            Begin Extent = 
               Top = 109
               Left = 484
               Bottom = 345
               Right = 657
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "crs_S2_Colection"
            Begin Extent = 
               Top = 304
               Left = 694
               Bottom = 432
               Right = 867
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S12_Units_Register"
            Begin Extent = 
               Top = 438
               Left = 691
     ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S14_Product_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'          Bottom = 530
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 134
               Left = 705
               Bottom = 190
               Right = 878
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_PropertyType_Register_1"
            Begin Extent = 
               Top = 315
               Left = 0
               Bottom = 407
               Right = 178
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_Property_Register_1"
            Begin Extent = 
               Top = 291
               Left = 242
               Bottom = 401
               Right = 420
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_PropertyType_Register"
            Begin Extent = 
               Top = 193
               Left = 8
               Bottom = 285
               Right = 186
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S14_Property_Register"
            Begin Extent = 
               Top = 170
               Left = 240
               Bottom = 280
               Right = 418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 190
               Left = 705
               Bottom = 246
               Right = 878
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t4"
            Begin Extent = 
               Top = 246
               Left = 705
               Bottom = 302
               Right = 878
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 32
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 2325
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S14_Product_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S14_Product_Vo';

