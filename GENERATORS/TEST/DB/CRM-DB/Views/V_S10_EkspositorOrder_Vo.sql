﻿/*V_S10_EkspositorOrder_Vo*/
CREATE VIEW dbo.V_S10_EkspositorOrder_Vo
AS
SELECT        t1.Id_Order, t1.Code_Order, t2.Description AS OrderType, CAST(CASE WHEN t1.Id_StandartSet IS NULL THEN 0 ELSE 1 END AS bit) AS Flag_StandartSet, t1.Flag_Inventory, 
                         CASE WHEN (t1.Flag_BuyPanels = 1 AND t1.Flag_BuyConstr = 0) THEN 'Панели' WHEN (t1.Flag_BuyPanels = 0 AND t1.Flag_BuyConstr = 1) THEN 'Конструкция' WHEN (t1.Flag_BuyPanels = 1 AND 
                         t1.Flag_BuyConstr = 1) THEN 'Панели, Конструкция' WHEN (t1.Flag_BuyPanels = 0 AND t1.Flag_BuyConstr = 0) THEN '' END AS Compensation, CAST
                             ((SELECT        ISNULL(COUNT(*), 0) AS Expr1
                                 FROM            dbo.crs_S10_EkspositorOrderPanelSetItem AS crs_S10_EkspositorOrderPanelSetItem_1
                                 WHERE        (Id_PanelSet = t1.Id_PanelSet)) AS varchar) + '/' + CONVERT(VARCHAR, ISNULL(t1.OrderMaxPosition, t6.PanelCount)) AS Panels, 
                         CASE WHEN crs_S10_OrderApproval.Flag_UnApproved = 1 THEN 'Разутвержден' WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Закрыт' WHEN t1.Flag_BuhTransaction = 1 THEN 'Подтв. исполн.' WHEN t1.Flag_OrderReady = 1 THEN 'Ждет отгрузки'
                          WHEN crs_S10_OrderApproval.Flag_Approved3 = 1 THEN 'Утвержден (обр-ся)' WHEN crs_S10_OrderApproval.Flag_Approved2 = 1 THEN 'Утв. Отд.Аналит.' WHEN crs_S10_OrderApproval.Flag_Approved1 = 1 THEN 'Утв. Отд.Прод.' WHEN crs_S10_OrderApproval.Flag_Approved1 = 0 THEN 'Утв. Рег.Дир.'
                          END AS Order_State, CASE WHEN
                             (SELECT        COUNT(t7.Id_Comment)
                               FROM            V_S10_OrderComment_Vo AS t7
                               WHERE        (t7.Id_Order = t1.Id_Order)) IS NULL THEN 0 ELSE
                             (SELECT        COUNT(t7.Id_Comment)
                               FROM            V_S10_OrderComment_Vo AS t7
                               WHERE        (t7.Id_Order = t1.Id_Order)) END AS CommentsCount, CONVERT(char(10), t1.DtOrderCreate, 103) AS DtOrderCreate_str, 
                         CASE WHEN crs_S10_OrderApproval.DtOrderApproved = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), crs_S10_OrderApproval.DtOrderApproved, 103) END AS DtOrderApproved_str, 
                         CASE WHEN t1.DtWillBeReady = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtWillBeReady, 103) END AS DtWillBeReady_str, 
                         CASE WHEN t1.DtOrderReady = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtOrderReady, 103) END AS DtOrderReady_str, 
                         CASE WHEN t1.DtBuhTransaction = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtBuhTransaction, 103) END AS DtBuhTransaction_str, 
                         CASE WHEN t1.DtExecutionConfirm = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtExecutionConfirm, 103) END AS DtExecutionConfirm_str, t1.Flag_OurDelivery, t4.UserName AS CreatorName,
                          t5.UserName AS ChangerName, t1.DtOrderCreate, crs_S10_OrderApproval.DtOrderApproved, t1.DtWillBeReady, t1.DtOrderReady, t1.DtBuhTransaction, t1.DtExecutionConfirm, t1.Id_Ekspositor, t1.Code_Ekspositor, t1.Id_Previous, 
                         t1.ID_OrderType, t1.ID_Construction, t6.ID_ConstructionType, t1.Id_PanelSet, t1.Flag_BuyConstr, t1.Id_Diler, t1.Id_Client, t3.Code_Client, crs_S10_OrderApproval.Flag_Approved1, crs_S10_OrderApproval.Flag_Approved2, crs_S10_OrderApproval.Flag_Approved3, 
                         crs_S10_OrderApproval.Flag_UnApproved, t1.Flag_OrderReady, t1.Flag_BuhTransaction, t1.Flag_ExecutionConfirm, t1.Flag_LastOrder, t1.LastOrderMaxPosition, t1.OrderMaxPosition, 
                         t1.Flag_BuyPanels, t1.Id_StandartSet, t1.InventoryNumber, t1.ImageIndexActual, 'http://expo.cersanit.ru/dbimg.php?f=img&t=k&i=' + CAST(t1.ImageIndexActual AS varchar) AS Image_Construction, 
                         t1.TPredAspRoleID, t1.CreatorUserID, t1.ChangerUserID, t1.DtChanged, t6.PanelCount AS PanelMaxCount, t1.Flag_Deleted,
                             (SELECT        ISNULL(COUNT(*), 0) AS Expr1
                               FROM            dbo.crs_S10_EkspositorOrderPanelSetItem
                               WHERE        (Id_PanelSet = t1.Id_PanelSet)) AS NumberPanels,
                             (SELECT        Id_PanelSet
                               FROM            dbo.crs_S10_EkspositorOrder AS t11
                               WHERE        (Id_Order = t1.Id_Previous)) AS Id_PreviosPanelSet, t6.Flag_ForbidSelectPanels, dbo.crs_S8_ClientPrestigeLevel_Register.ClientPrestigeLevel
FROM            dbo.crs_S10_EkspositorOrder AS t1 INNER JOIN
						 dbo.crs_S10_OrderApproval on t1.Id_Order = crs_S10_OrderApproval.Id_Order INNER JOIN
                         dbo.crs_S10_EkspositorOrderType_Register AS t2 ON t1.ID_OrderType = t2.ID_OrderType INNER JOIN
                         dbo.crs_S8_Client AS t3 ON t1.Id_Client = t3.Id_Client LEFT OUTER JOIN
                         dbo.aspnet_Users AS t4 ON t1.CreatorUserID = t4.UserId LEFT OUTER JOIN
                         dbo.aspnet_Users AS t5 ON t1.ChangerUserID = t5.UserId INNER JOIN
                         dbo.crs_S10_Construction_Register AS t6 ON t1.ID_Construction = t6.ID_Construction INNER JOIN
                         dbo.crs_S8_ClientPrestigeLevel_Register ON t3.Id_ClientPrestigeLevel = dbo.crs_S8_ClientPrestigeLevel_Register.Id_ClientPrestigeLevel

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[34] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 6
               Left = 266
               Bottom = 84
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 84
               Left = 266
               Bottom = 192
               Right = 467
            End
            DisplayFlags = 280
            TopColumn = 25
         End
         Begin Table = "t4"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t5"
            Begin Extent = 
               Top = 192
               Left = 246
               Bottom = 300
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t6"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 19
         End
         Begin Table = "crs_S8_ClientPrestigeLevel_Register"
            Begin Extent = 
               Top = 6
               Left = 505
               Bottom = 119
               Right = 709
            End
            DisplayFlags = 280
   ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrder_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'         TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 6990
         Alias = 1905
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrder_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrder_Vo';

