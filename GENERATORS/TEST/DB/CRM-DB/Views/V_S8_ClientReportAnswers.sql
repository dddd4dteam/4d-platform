﻿

CREATE view [dbo].[V_S8_ClientReportAnswers]
as

SELECT top 100 percent a.ID_Answer, cl.ID_Client,f.Form,cast(q1.Question as varchar(250)) Question,
	   Case When q1.ID_AnswerType=1 Then VariantValue
	        When q1.ID_AnswerType=2 Then 'Текстовый'
			When q1.ID_AnswerType=3 Then cast(AnswerValue3 as varchar(3))
		Else 'Другие'
		End TypeAnswer
FROM crs_S17_Questions q1
	left join crs_S17_Questions q2 on q1.ID_Parent=q2.ID_Question
	inner join crs_S17_Forms f on q1.ID_Form=f.ID_Form
	inner join crs_S17_Answer a on q1.ID_Question=a.ID_Question
	inner join crs_S9_Meeting m
	  on m.Id_Meeting=a.ID_Meeting
	inner join client_vw cl on m.id_client=cl.id_client
	left join crs_S17_QuestionsVariant v on  ID_AnswerVariant=a.AnswerValue6
where exists
			(
			select 1
			from
				(
				select distinct id_form,id_client,min(starttime) over(partition by id_form,ID_Client) starttime
				from crs_S9_Meeting  me
				inner join	(
				             select distinct id_meeting,id_form
							 from crs_S17_Answer a
							 inner join crs_S17_Questions q
							 on a.id_Question=q.ID_Question
							) mf on me.id_meeting=mf.id_meeting
					and id_status=1
				) as d where m.ID_Client=d.ID_Client and m.starttime=d.starttime and d.id_form=f.id_form
             )
and q1.ID_AnswerType not in (2)
and form='Акция "Пол в подарок"'
order by  cl.ID_Client,q1.Order_number,q1.Level