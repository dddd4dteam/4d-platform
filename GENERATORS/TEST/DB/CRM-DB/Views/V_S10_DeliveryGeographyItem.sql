﻿CREATE VIEW V_S10_DeliveryGeographyItem
AS
SELECT distinct t2.Id_GeographyItem, t2.Id_GeographyParentItem, 
               t2.Id_GeographyItemType, t2.GeographyItem, t2.Prefix, 
               t2.GeographyItemType, 
               t4.Id_DeliveryManager
FROM	crs_S10_EkspositorOrder t3
		Inner join crs_S10_DeliveryManagerOrder t4 on(t3.Id_Order = t4.Id_Order)
		Inner join crs_S8_Client t5 on(t3.Id_Client = t5.Id_Client)
		Inner join V_S1_GeographyItemAncestor t1 on(t5.Id_GeographyItem = t1.IDAncestors)
		Inner join V_S1_GeographyItem_Vo t2 on(t1.Id_GeographyItem = t2.Id_GeographyItem)