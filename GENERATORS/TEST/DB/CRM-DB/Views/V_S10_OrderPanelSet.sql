﻿/*V_S10_OrderPanelSet*/

CREATE VIEW [dbo].[V_S10_OrderPanelSet]
AS
SELECT t2.Id_PanelSet, t2.Description AS PanelSetDescription,
t1.Id_Order, Code_Order
FROM  dbo.crs_S10_EkspositorOrder t1
INNER JOIN dbo.crs_S10_EkspositorOrderPanelSet t2 ON t1.Id_PanelSet = t2.Id_PanelSet
