﻿
/*V_S10_OrderPanels*/
CREATE VIEW [dbo].[V_S10_OrderPanels]
AS
SELECT t1.Id_Order, Code_Order,
t3.Id_Panel,
t3.Position,
t3.Id_PanelSetItem,
t3.Flag_OnlyBase,
 t3.Id_PanelSet
FROM  dbo.crs_S10_EkspositorOrder AS t1
INNER JOIN dbo.crs_S10_EkspositorOrderPanelSet AS t2 ON t1.Id_PanelSet = t2.Id_PanelSet
INNER JOIN dbo.crs_S10_EkspositorOrderPanelSetItem AS t3 ON t2.Id_PanelSet = t3.Id_PanelSet
