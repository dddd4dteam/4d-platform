﻿

CREATE view [dbo].[V_S8_ClientReportEkspositor]
as
Select distinct  Id_client,r.Description Construction,count(r.Description) over(partition by Id_client,r.Description) CountConstruction
FROM crs_s10_EkspositorOrder a
	inner join crs_S10_Construction_Register r on a.ID_Construction = r.ID_Construction

where id_Ekspositor not in
					(
					select distinct id_Ekspositor From crs_s10_EkspositorOrder f
					Where Flag_ExecutionConfirm = 1 and Id_OrderType = 4
					)
  and flag_LastOrder=1