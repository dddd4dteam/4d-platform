﻿
CREATE view [dbo].[V_S8_ClientReportRival]
as
SELECT cr.Id_Client,rr.Id_Rival, rr.Rival, cr.Quantity,rgr.RivalGoodsCategory
FROM dbo.crs_S8_Client c
inner join dbo.crs_S8_ClientRival cr ON c.Id_Client = cr.Id_Client
inner join dbo.crs_S5_Rival_Register rr ON cr.Id_Rival = rr.Id_Rival
inner join  dbo.crs_S16_RivalGoodsCategory_Register rgr ON cr.Id_RivalGoodsCategory = rgr.Id_RivalGoodsCategory