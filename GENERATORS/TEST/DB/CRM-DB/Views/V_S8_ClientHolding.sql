﻿
/*V_S8_ClientHolding*/

CREATE VIEW [dbo].[V_S8_ClientHolding]
AS
SELECT dbo.crs_S8_Client.Id_Client,Code_Client , dbo.crs_S8_Client.Client, dbo.crs_S7_Holding_Register.Id_Holding, dbo.crs_S7_Holding_Register.Holding
FROM dbo.crs_S8_Client LEFT OUTER JOIN
  dbo.crs_S7_Holding_Register ON dbo.crs_S8_Client.Id_Holding = dbo.crs_S7_Holding_Register.Id_Holding
