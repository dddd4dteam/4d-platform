﻿/*V_S10_TPredClients*/
CREATE VIEW dbo.V_S10_TPredClients
AS
SELECT        t1.RoleId AS TPredAspRoleID, t1.RoleName, t2.Id_Client, t2.Code_Client, t2.Client, t3.GeographyItem, t2.Street, t2.HouseNumber, t2.HousePartNumber, t2.FlatNumber, t2.PavilionNumber, t2.WebAddress,
                             (SELECT        COUNT(Id_Diler) AS Expr1
                               FROM            dbo.V_S10_ClientDiler AS t7
                               WHERE        (Id_Client = t2.Id_Client)) AS DilersCount, dbo.GetFullAddress(t2.Street, t2.HouseNumber, t2.FlatNumber) AS FullAddress, dbo.crs_S8_ClientPrestigeLevel_Register.ClientPrestigeLevel
FROM            dbo.aspnet_Roles AS t1 INNER JOIN
                         dbo.crs_S8_Client AS t2 ON t1.RoleId = t2.AspRoleID INNER JOIN
                         dbo.crs_S1_GeographyItem AS t3 ON t2.Id_GeographyItem = t3.Id_GeographyItem INNER JOIN
                         dbo.crs_S8_ClientPrestigeLevel_Register ON t2.Id_ClientPrestigeLevel = dbo.crs_S8_ClientPrestigeLevel_Register.Id_ClientPrestigeLevel
WHERE        (t2.Flag_Enable = 1)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[21] 4[36] 2[18] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 21
               Left = 288
               Bottom = 151
               Right = 507
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 40
               Left = 618
               Bottom = 170
               Right = 838
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_ClientPrestigeLevel_Register"
            Begin Extent = 
               Top = 183
               Left = 660
               Bottom = 296
               Right = 864
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 15
         Width = 284
         Width = 3735
         Width = 3210
         Width = 1500
         Width = 2250
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 2190
         Alias = 900
         Table = 2820
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 13', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_TPredClients';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'50
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_TPredClients';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_TPredClients';

