﻿CREATE VIEW [dbo].[V_S4_Material]
AS
SELECT     dbo.crs_S4_Material_Register.Id_Material, dbo.crs_S4_Material_Register.Material, dbo.crs_S4_MaterialType_Register.MaterialType, 
                      dbo.crs_S4_Material_Register.Id_MaterialType
FROM         dbo.crs_S4_Material_Register INNER JOIN
                      dbo.crs_S4_MaterialType_Register ON dbo.crs_S4_Material_Register.Id_MaterialType = dbo.crs_S4_MaterialType_Register.Id_MaterialType