﻿



CREATE view [dbo].[V_S8_ClientReportGoodsCategory]
as

select rc.Id_GoodsCategory, rc.GoodsCategory, gc.id_client,code_client,
       cast(isnull(PctAll,0) as varchar(3))+'%' PctAll
FROM dbo.crs_S3_GoodsCategory_Register rc
inner join dbo.crs_S8_ClientGoodsCategory cc ON rc.Id_GoodsCategory = cc.Id_GoodsCategory
inner join dbo.crs_S8_Client gc ON cc.Id_Client = gc.Id_Client