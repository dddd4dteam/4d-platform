﻿
/*V_S10_EkspositorOrderPanelSet_Vo*/
CREATE VIEW [dbo].[V_S10_EkspositorOrderPanelSet_Vo]
AS
SELECT				 t1.Id_Ekspositor,t1.Code_Ekspositor,
					 t2.Description AS ConstructionName,
					 t3.Client + ', [' + CAST(t3.code_Client AS varchar(8)) + ']' AS ClientName,
					 'http://expo.cersanit.ru/dbimg.php?f=img&t=k&i=' + CAST(t2.ImageIndexActual AS varchar) AS Image_Construction,
					 t5.Diler AS DilerName,
					 CAST(CAST(t2.ConstructionHeight AS INT)  AS VARCHAR) + 'x'+CAST(CAST(t2.ConstructionWidth AS INT)  AS VARCHAR)+'x'+CAST( CAST(t2.ConstructionDepth AS INT) AS VARCHAR) AS ConstructionHtWtDpt,
       CASE WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Выполнен' ELSE 'Не выполнен' END AS Ekspositor_Status,
       CASE WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Заказ закрыт'
       WHEN t1.Flag_BuhTransaction = 1 THEN 'Треб. подтв. исполнения'
       WHEN t1.Flag_OrderReady = 1 THEN 'Треб. подтв. актуал.'
       WHEN crs_S10_OrderApproval.Flag_Approved3 = 1 THEN 'Утвержден (обр-ся)'
       WHEN crs_S10_OrderApproval.Flag_Approved2 = 1 THEN 'Треб. утв. Ком.Дир.'
       WHEN crs_S10_OrderApproval.Flag_Approved1 = 1 THEN 'Треб. утв. Отд.Прод.'
       WHEN crs_S10_OrderApproval.Flag_Approved1 = 0 THEN 'Треб. утв. Рег.Дир.' END AS Order_status,
       CASE WHEN t1.DtWillBeReady = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtWillBeReady, 103) END AS DtWillBeReady_str,
       CASE WHEN t1.DtExecutionConfirm = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtExecutionConfirm, 103) END AS Dt_Install_str,
       t1.DtExecutionConfirm AS Dt_Install, dbo.crs_TrRepres.Description AS TrRepresName, t1.Id_Diler, t1.Id_Client,t3.Code_Client , t1.ID_Construction, 'Свой' AS Owner,
       t3.Id_TrRepres,
       t1.Flag_BuyConstr, t1.Flag_BuyPanels,
       CASE WHEN t1.Flag_BuyConstr = 1 THEN 'Конструкция'
       WHEN t1.Flag_BuyPanels = 1 THEN 'Панели' ELSE 'Полный' END AS AssemblyType,
       crs_S10_OrderApproval.Flag_Approved1,
       crs_S10_OrderApproval.Flag_Approved2,
       crs_S10_OrderApproval.Flag_Approved3,
       t1.Flag_ExecutionConfirm,
       t1.DtOrderCreate, NULL AS Id_Employee,
       t1.Id_Order,t1.Code_Order ,
       t6.Id_PanelSet,
       t1.Id_StandartSet,
       t2.ID_ConstructionType,
      t6.Description,
					 t2.PanelCount,
					 	 --previos panel set
					 (SELECT t11.Id_PanelSet
						FROM dbo.crs_S10_EkspositorOrder AS t11
					    WHERE t11.Id_Order = t1.Id_Previous) as Id_PreviosPanelSet
FROM  dbo.crs_S10_EkspositorOrder AS t1 INNER JOIN
		dbo.crs_S10_OrderApproval on t1.Id_Order = crs_S10_OrderApproval.Id_Order INNER JOIN
        dbo.crs_S10_Construction_Register AS t2 ON t1.ID_Construction = t2.ID_Construction INNER JOIN
        dbo.crs_S8_Client AS t3 ON t1.Id_Client = t3.Id_Client INNER JOIN
        dbo.crs_TrRepres ON t3.Id_TrRepres = dbo.crs_TrRepres.Id_TrRepres INNER JOIN
        dbo.crs_S6_Diler AS t5 ON t1.Id_Diler = t5.Id_Diler INNER JOIN
        dbo.crs_S10_EkspositorOrderPanelSet t6 ON t1.Id_PanelSet = t6.Id_PanelSet
WHERE     (t1.Flag_LastOrder = 1) AND (t1.Id_OrderType <> 4) OR
        (t1.Flag_LastOrder = 1) AND (t1.Id_OrderType = 4) AND (t1.Flag_ExecutionConfirm = 0)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[11] 4[50] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -192
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 135
               Right = 262
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 366
               Left = 38
               Bottom = 474
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 366
               Left = 258
               Bottom = 474
               Right = 459
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_TrRepres"
            Begin Extent = 
               Top = 273
               Left = 48
               Bottom = 401
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t5"
            Begin Extent = 
               Top = 474
               Left = 38
               Bottom = 582
               Right = 210
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S10_EkspositorOrderPanelSet"
            Begin Extent = 
               Top = 366
               Left = 497
               Bottom = 444
               Right = 648
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 10
         Width = 284
         Width = 1500
         Width ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrderPanelSet_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'= 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrderPanelSet_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrderPanelSet_Vo';

