﻿
/*V_S10_OrderComment_Vo*/
CREATE VIEW [dbo].[V_S10_OrderComment_Vo]
AS
SELECT Id_Comment, t1.Id_Order,Code_Order , Date, CRM_RoleDescriptor, Text, Context, ActivityName,
        CASE WHEN t1.Date = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.Date, 103) END AS Date_str
FROM  dbo.crs_S10_OrderComment AS t1
INNER JOIN dbo.crs_S10_EkspositorOrder t2 on t1.Id_Order=t2.Id_Order
WHERE (Context = 'crs_EkspositorOrder' OR Context = 'crs_S10_EkspositorOrder')
