﻿
/*V_S8_ClientType*/
CREATE VIEW [dbo].[V_S8_ClientType]
AS
SELECT dbo.crs_S8_Client.Id_Client,Code_Client, dbo.crs_S8_Client.Client, dbo.crs_S8_ClientType_Register.Id_ClientType, dbo.crs_S8_ClientType_Register.ClientType
FROM dbo.crs_S8_Client LEFT OUTER JOIN
  dbo.crs_S8_ClientType_Register ON dbo.crs_S8_Client.Id_ClientType = dbo.crs_S8_ClientType_Register.Id_ClientType
