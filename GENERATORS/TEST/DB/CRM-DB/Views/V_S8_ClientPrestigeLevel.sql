﻿
/*V_S8_ClientPrestigeLevel*/

CREATE VIEW [dbo].[V_S8_ClientPrestigeLevel]
AS
SELECT dbo.crs_S8_Client.Id_Client,Code_Client , dbo.crs_S8_Client.Client, dbo.crs_S8_ClientPrestigeLevel_Register.Id_ClientPrestigeLevel,
  dbo.crs_S8_ClientPrestigeLevel_Register.ClientPrestigeLevel
FROM dbo.crs_S8_Client LEFT OUTER JOIN
  dbo.crs_S8_ClientPrestigeLevel_Register ON dbo.crs_S8_Client.Id_ClientPrestigeLevel = dbo.crs_S8_ClientPrestigeLevel_Register.Id_ClientPrestigeLevel
