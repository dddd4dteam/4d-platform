﻿CREATE VIEW dbo.V_S10_EkspositorRestickedPanels
AS
SELECT        'Добавлена' AS RestickedPanelType, t1.Id_Order, t1.Code_Order, t2.Id_Panel, t3.Collection, t3.Description, t2.Flag_OnlyBase, t2.Flag_BuyPanels, t3.Collection2,t3.Price
FROM            dbo.crs_S10_EkspositorOrder t1 INNER JOIN
                         dbo.crs_S10_EkspositorOrderPanelSetItem t2 ON t1.Id_PanelSet = t2.Id_PanelSet INNER JOIN
                         dbo.crs_S10_Panel t3 ON t2.Id_Panel = t3.ID_Panel
WHERE        t1.Id_OrderType IN (1, 2) AND NOT EXISTS
                             (SELECT        1
                               FROM            dbo.crs_S10_EkspositorOrder AS tt1 INNER JOIN
                                                         dbo.crs_S10_EkspositorOrderPanelSetItem AS tt2 ON tt1.Id_PanelSet = tt2.Id_PanelSet
                               WHERE        tt1.Id_order = t1.Id_previous AND tt2.Id_Panel = t2.Id_Panel AND tt2.Flag_OnlyBase = t2.Flag_OnlyBase AND tt2.Flag_BuyPanels = t2.Flag_BuyPanels AND 
                                                         tt2.Panel_Guid = t2.Panel_Guid/* tt1.Id_Order = t1.Id_Previous AND tt2.Id_Panel = t2.Id_Panel изменил 14.07.2015 Огарок Д.*/ )
UNION ALL
SELECT        'Удалена' AS RestickedPanelType, t1.Id_Order, t1.Code_Order, t4.Id_Panel, t4.Collection, t4.Description, t3.Flag_OnlyBase, t3.Flag_BuyPanels, t4.Collection2,t4.Price
FROM            crs_S10_EkspositorOrder t1 INNER JOIN
                         crs_S10_EkspositorOrder t2 ON (t1.Id_Previous = t2.Id_Order) INNER JOIN
                         dbo.crs_S10_EkspositorOrderPanelSetItem t3 ON (t2.Id_PanelSet = t3.Id_PanelSet) INNER JOIN
                         dbo.crs_S10_Panel t4 ON (t3.Id_Panel = t4.ID_Panel)
WHERE        t1.Id_OrderType IN (1, 2) AND NOT EXISTS
                             (SELECT        1
                               FROM            crs_S10_EkspositorOrder tt1 INNER JOIN
                                                         dbo.crs_S10_EkspositorOrderPanelSetItem tt2 ON (tt1.Id_PanelSet = tt2.Id_PanelSet)
                               WHERE        tt1.Id_Order = t1.Id_Order AND tt2.Id_Panel = t3.Id_Panel AND tt2.Flag_OnlyBase = t3.Flag_OnlyBase AND tt2.Flag_BuyPanels = t3.Flag_BuyPanels AND 
                                                         tt2.Panel_Guid = t3.Panel_Guid/* tt1.Id_Order = t1.Id_Order AND tt2.Id_Panel = t3.Id_Panel  изменил 14.07.2015 Огарок Д.*/ )


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorRestickedPanels';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorRestickedPanels';

