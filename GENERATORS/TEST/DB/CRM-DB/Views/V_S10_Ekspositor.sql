﻿CREATE VIEW dbo.V_S10_Ekspositor
AS
SELECT        t1.Id_Ekspositor, t1.Code_Ekspositor, t5.ConstructionType, t2.Description AS ConstructionName, t1.InventoryNumber, t3.Id_Client, t3.Code_Client, t3.Client + ', [' + CAST(t3.Code_Client AS varchar(8)) 
                         + ']' AS ClientName, t4.Diler AS DilerName, t7.GeographyItem, CASE WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Выполнен' ELSE 'Не выполнен' END AS Ekspositor_State, SUBSTRING(t6.RoleName, 
                         LEN('RL_DOM ') + 2, LEN(t6.RoleName) - LEN('RL_DOM ')) AS TPredRoleName, CASE WHEN t1.DtOrderCreate = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtOrderCreate, 103) 
                         END AS DtOrderCreate_str, CASE WHEN t1.DtExecutionConfirm = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtExecutionConfirm, 103) END AS Dt_Install_str, 
                         CASE WHEN t1.DtWillBeReady = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), t1.DtWillBeReady, 103) END AS DtWillBeReady_str, t1.TPredAspRoleID, t1.Id_Order, t1.Code_Order, 
                         t1.ID_OrderType, t1.Id_PanelSet, t1.Id_Diler, t1.ID_Construction, t2.ID_ConstructionType, t1.Id_StandartSet, 
                         CASE WHEN t10.Flag_UnApproved = 1 THEN 'Разутвержден' WHEN t1.Flag_ExecutionConfirm = 1 THEN 'Закрыт' WHEN t1.Flag_BuhTransaction = 1 THEN 'Подтв. исполн.' WHEN t1.Flag_OrderReady = 1 THEN 'Ждет отгрузки'
                          WHEN t10.Flag_Approved3 = 1 THEN 'Утвержден (обр-ся)' WHEN t10.Flag_Approved2 = 1 THEN 'Утв. Отд.Аналит.' WHEN t10.Flag_Approved1 = 1 THEN 'Утв. Отд.Прод.' WHEN t10.Flag_Approved1 = 0 THEN 'Утв. Рег.Дир.'
                          END AS Order_State, t1.DtExecutionConfirm AS Dt_Install, t1.Flag_BuyConstr, t1.Flag_BuyPanels, CASE WHEN (t1.Flag_LastOrder = 1 AND (t1.Flag_BuyConstr = 1 OR
                         t1.Flag_BuyPanels = 1)) THEN 'Компенсируется' WHEN (t1.Flag_LastOrder = 1 AND t1.Flag_BuyConstr <> 1 AND t1.Flag_BuyPanels <> 1) THEN 'Не Компенсируется' END AS Compensation_State, 
                         t10.Flag_Approved1, t10.Flag_Approved2, t10.Flag_Approved3, t1.Flag_ExecutionConfirm, t1.DtOrderCreate, t1.ImageIndexActual, 
                         'http://expo.cersanit.ru/dbimg.php?f=img&t=k&i=' + CAST(t1.ImageIndexActual AS varchar) AS Image_Construction, t1.DtChanged, t1.DtOrderCreate AS DtLastOrderCreate, t1.OrderMaxPosition, t8.ClientType, 
                         t9.ClientPrestigeLevel, t4.UrName, t4.ContractNumber, t4.ContractDt, t2.Price,
                             (SELECT        CAST(COUNT(*) AS varchar(5)) + '(' + CAST(SUM(CAST(tt2.Flag_Approved3 AS numeric(1, 0))) AS varchar(5)) + ')' AS Expr1
                               FROM            dbo.crs_S10_EkspositorOrder tt1
							   Inner join crs_S10_OrderApproval tt2 on(tt1.Id_Order = tt2.Id_Order)
                               WHERE        (Flag_LastOrder = 1) AND (ID_OrderType <> 4) AND (Id_Client = t1.Id_Client) AND (ID_Construction = t1.ID_Construction)) AS EkspositorTotal
FROM            dbo.crs_S10_EkspositorOrder AS t1 INNER JOIN
                         dbo.crs_S10_Construction_Register AS t2 ON t1.ID_Construction = t2.ID_Construction INNER JOIN
                         dbo.crs_S8_Client AS t3 ON t1.Id_Client = t3.Id_Client INNER JOIN
                         dbo.crs_S1_GeographyItem AS t7 ON t3.Id_GeographyItem = t7.Id_GeographyItem INNER JOIN
                         dbo.crs_S6_Diler AS t4 ON t1.Id_Diler = t4.Id_Diler INNER JOIN
                         dbo.crs_S10_ConstructionType_Register AS t5 ON t2.ID_ConstructionType = t5.ID_ConstructionType INNER JOIN
                         dbo.crs_S8_ClientPrestigeLevel_Register AS t9 ON t3.Id_ClientPrestigeLevel = t9.Id_ClientPrestigeLevel LEFT OUTER JOIN
                         dbo.aspnet_Roles AS t6 ON t1.TPredAspRoleID = t6.RoleId LEFT OUTER JOIN
                         dbo.crs_S8_ClientType_Register AS t8 ON t3.Id_ClientType = t8.Id_ClientType INNER JOIN
						 dbo.crs_S10_OrderApproval AS t10 ON t1.Id_Order = t10.Id_Order 
WHERE        (t1.Flag_LastOrder = 1) AND (t1.ID_OrderType <> 4) OR
                         (t1.Flag_LastOrder = 1) AND (t1.ID_OrderType = 4) AND (t1.Flag_ExecutionConfirm = 0)

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[52] 4[10] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 0
               Left = 358
               Bottom = 440
               Right = 559
            End
            DisplayFlags = 280
            TopColumn = 8
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 4
               Left = 119
               Bottom = 138
               Right = 330
            End
            DisplayFlags = 280
            TopColumn = 18
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 0
               Left = 789
               Bottom = 313
               Right = 1029
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "t7"
            Begin Extent = 
               Top = 0
               Left = 1051
               Bottom = 181
               Right = 1256
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t4"
            Begin Extent = 
               Top = 277
               Left = 3
               Bottom = 422
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "t5"
            Begin Extent = 
               Top = 162
               Left = 0
               Bottom = 272
               Right = 186
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t9"
            Begin Extent = 
               Top = 325
               Left = 591
               Bottom = 438
               Right = 760
            End
            DisplayFlags = 280
            TopColumn = 0
       ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_Ekspositor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'  End
         Begin Table = "t6"
            Begin Extent = 
               Top = 128
               Left = 589
               Bottom = 324
               Right = 758
            End
            DisplayFlags = 280
            TopColumn = 4
         End
         Begin Table = "t8"
            Begin Extent = 
               Top = 261
               Left = 1072
               Bottom = 389
               Right = 1277
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3795
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_Ekspositor';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_Ekspositor';

