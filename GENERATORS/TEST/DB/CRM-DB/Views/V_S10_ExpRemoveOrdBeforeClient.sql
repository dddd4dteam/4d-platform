﻿
/*V_S10_ExpRemoveOrdBeforeClient*/
CREATE VIEW [dbo].[V_S10_ExpRemoveOrdBeforeClient]
AS
SELECT t1.Id_Order,t1.Code_Order , t3.Id_Client,t3.Code_Client, t3.Client, t4.Diler ,  t3.AddressOther, t5.GeographyItem
FROM  dbo.crs_S10_EkspositorOrder AS t1
	  INNER JOIN dbo.crs_S10_EkspositorOrder AS t2 ON t1.Id_Previous = t2.Id_Order
	  INNER JOIN dbo.crs_S8_Client AS t3 ON t2.Id_Client = t3.Id_Client
	  INNER JOIN dbo.crs_S6_Diler AS t4 ON t2.ID_Diler = t4.Id_Diler
	  INNER JOIN dbo.crs_S1_GeographyItem AS t5 ON t3.Id_GeographyItem = t5.Id_GeographyItem
