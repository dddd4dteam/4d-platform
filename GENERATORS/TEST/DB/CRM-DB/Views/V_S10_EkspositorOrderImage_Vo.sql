﻿CREATE VIEW [dbo].[V_S10_EkspositorOrderImage_Vo]
	AS SELECT [crs_S10_EkspositorOrderImage].*, [crs_S10_ImageAcceptance].Accepted, [crs_S10_ImageAcceptance].Comment
	FROM [crs_S10_EkspositorOrderImage] Left Join [crs_S10_ImageAcceptance] 
	ON [crs_S10_EkspositorOrderImage].ID = [crs_S10_ImageAcceptance].Id_Image

