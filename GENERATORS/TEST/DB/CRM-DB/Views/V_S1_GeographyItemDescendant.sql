﻿CREATE VIEW [dbo].[V_S1_GeographyItemDescendant]
AS
SELECT     n.Id_GeographyItem AS IDDescendants, t.[Level], c.Id_GeographyItem, c.Id_GeographyParentItem, c.Id_GeographyItemType, c.GeographyItem, c.Prefix, c.Description, 
                      c.Population
FROM         dbo.crs_S1_GeographyItem AS n INNER JOIN
                      dbo.crs_S1_GeographyHierarchy AS t ON n.Id_GeographyItem = t.Id_GeographyParentItem INNER JOIN
                      dbo.crs_S1_GeographyItem AS c ON t.Id_GeographyItem = c.Id_GeographyItem