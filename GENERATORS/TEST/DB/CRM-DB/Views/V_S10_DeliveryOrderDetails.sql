﻿
CREATE VIEW V_S10_DeliveryOrderDetails
AS
/* Конструкции */
select	t1.Id_Order, 
		'Добавлена' as ActionType,
		t1.Id_Construction as Id_Element, t13.Description as Element
from  crs_S10_EkspositorOrder t1 WITH(NOLOCK)
Inner join crs_S8_Client t2 WITH(NOLOCK) on(t1.Id_Client = t2.Id_Client)
Inner join crs_S10_EkspositorOrderType_Register t12 on(t1.ID_OrderType = t12.ID_OrderType)
Inner join crs_S10_Construction_Register t13 on(t1.Id_Construction = t13.ID_Construction)
Inner join crs_S10_OrderApproval t17 WITH(NOLOCK) on(t1.Id_Order = t17.Id_Order)
Where	t1.Id_OrderType In (1,2) 
		and t1.Flag_BuyConstr = 0 
UNION ALL
/* Панели */
select	t1.Id_Order, 
		t15.RestickedPanelType as ActionType,
		t16.ID_Panel as Id_Element, t16.Description as Element
from  crs_S10_EkspositorOrder t1 WITH(NOLOCK)
Inner join crs_S8_Client t2 WITH(NOLOCK) on(t1.Id_Client = t2.Id_Client)
Inner join crs_S10_EkspositorOrderType_Register t12  WITH(NOLOCK) on(t1.ID_OrderType = t12.ID_OrderType)
Inner join crs_S10_Construction_Register t13  WITH(NOLOCK) on(t1.Id_Construction = t13.ID_Construction)
Inner join V_S10_EkspositorRestickedPanels t15  WITH(NOLOCK) on(t1.Id_Order = t15.Id_Order)
Inner join crs_S10_Panel t16 WITH(NOLOCK) on(t15.Id_Panel = t16.ID_Panel)
Inner join crs_S10_OrderApproval t17 WITH(NOLOCK) on(t1.Id_Order = t17.Id_Order)
Where	t1.Id_OrderType In (1,2) 
		and t1.Flag_BuyPanels = 0