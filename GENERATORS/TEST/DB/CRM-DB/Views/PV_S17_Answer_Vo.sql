﻿CREATE VIEW dbo.PV_S17_Answer_Vo
AS
SELECT        dbo.crs_S17_Answer.ID_Meeting, dbo.crs_S17_Answer.ID_Question, dbo.crs_S17_Answer.ID_Answer, dbo.crs_S17_Answer.AnswerValue1, dbo.crs_S17_Answer.AnswerValue2, 
                         dbo.crs_S17_Answer.AnswerValue3, dbo.crs_S17_Answer.AnswerValue4, dbo.crs_S17_Answer.AnswerValue5, dbo.crs_S17_Answer.AnswerValue6, dbo.crs_TrRepres.Id_TrRepres, dbo.crs_Manager.Id_Manager, 
                         dbo.crs_Director.Id_Director, dbo.crs_S17_Answer.Flag_Past
FROM            dbo.crs_S17_Answer INNER JOIN
                         dbo.crs_S9_Meeting ON dbo.crs_S17_Answer.ID_Meeting = dbo.crs_S9_Meeting.Id_Meeting INNER JOIN
                         dbo.crs_TrRepres ON dbo.crs_S9_Meeting.Id_TrRepres = dbo.crs_TrRepres.Id_TrRepres INNER JOIN
                         dbo.crs_Manager ON dbo.crs_TrRepres.Id_Manager = dbo.crs_Manager.Id_Manager INNER JOIN
                         dbo.crs_Director ON dbo.crs_Manager.Id_Director = dbo.crs_Director.Id_Director
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PV_S17_Answer_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N' 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PV_S17_Answer_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S17_Answer"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 220
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "crs_S9_Meeting"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 473
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_TrRepres"
            Begin Extent = 
               Top = 6
               Left = 511
               Bottom = 136
               Right = 682
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_Manager"
            Begin Extent = 
               Top = 6
               Left = 720
               Bottom = 119
               Right = 890
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_Director"
            Begin Extent = 
               Top = 6
               Left = 928
               Bottom = 119
               Right = 1098
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or =', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PV_S17_Answer_Vo';

