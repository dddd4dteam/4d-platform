﻿CREATE VIEW dbo.V_S17_Questions_Vo
AS
SELECT        dbo.crs_S17_Questions.ID_Form, dbo.crs_S17_Forms.Form, dbo.crs_S17_Questions.ID_Question, dbo.crs_S17_Questions.Question, dbo.crs_S17_Questions.[Level], dbo.crs_S17_Questions.ID_Parent, 
                         dbo.crs_S17_Questions.Flag_required, dbo.crs_S17_Questions.Order_number, dbo.crs_S17_QuestionsCondition_Register.Condition, dbo.crs_S17_Questions.ID_Condition, dbo.crs_S17_Questions.Description, 
                         dbo.crs_S17_Questions.ID_AnswerType, dbo.crs_S17_Questions.ConditionValue1, dbo.crs_S17_Questions.ConditionValue2, dbo.crs_S17_Questions.ConditionValue4, dbo.crs_S17_Questions.ConditionValue3, 
                         dbo.crs_S17_Questions.ConditionValue5, dbo.crs_S17_Questions.ConditionValue6, va.VariantValue
FROM            dbo.crs_S17_Questions INNER JOIN
                         dbo.crs_S17_Forms ON dbo.crs_S17_Questions.ID_Form = dbo.crs_S17_Forms.ID_Form LEFT OUTER JOIN
                         dbo.crs_S17_QuestionsCondition_Register ON dbo.crs_S17_Questions.ID_Condition = dbo.crs_S17_QuestionsCondition_Register.ID_Condition LEFT OUTER JOIN
                         dbo.crs_S17_QuestionsVariant AS va ON dbo.crs_S17_Questions.ID_Parent = va.ID_Question AND va.ID_AnswerVariant = dbo.crs_S17_Questions.ConditionValue6
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S17_Questions_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S17_Questions_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[48] 4[14] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S17_Forms"
            Begin Extent = 
               Top = 23
               Left = 816
               Bottom = 136
               Right = 986
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S17_QuestionsCondition_Register"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 103
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "va"
            Begin Extent = 
               Top = 176
               Left = 878
               Bottom = 289
               Right = 1059
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S17_Questions"
            Begin Extent = 
               Top = 0
               Left = 246
               Bottom = 337
               Right = 662
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S17_Questions_Vo';

