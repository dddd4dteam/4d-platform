﻿/*V_S15_Monitoring_Vo*/

CREATE view V_S15_Monitoring_Vo
as
SELECT m.Id_Monitoring, m.Id_Client,Code_Client , m.Price, m.SpecialPrice, dbo.crs_S12_Units_Register.Id_Units,
    dbo.crs_S12_Units_Register.Units, p.Id_Product, p.Goods, p.Id_Grupa, p.Grupa, p.Id_SubGrupa,
    p.SubGrupa, p.Id_Type, p.Type, p.Id_Format, p.Format, p.Id_Property1,
    p.Property1, p.Id_Property2, p.Property2, p.Id_Property3, p.Property3,
    p.Id_Brand, p.Brand, p.Id_Colection, p.Colection, cl.Start, cl.Finish,
    g.Id_MonitoringType, m.Date
FROM dbo.crs_S15_Monitoring m
INNER JOIN dbo.crs_S12_Units_Register ON m.Id_Units = dbo.crs_S12_Units_Register.Id_Units
INNER JOIN dbo.V_S14_Product_Vo p ON m.Id_Product = p.Id_Product
INNER JOIN dbo.crs_S15_ProductMonitoring g ON m.Id_Product = g.Id_Product AND m.Date BETWEEN g.Start AND g.Finish
INNER JOIN dbo.crs_S15_ClientMonitoring cl ON m.Id_Client = cl.Id_Client AND g.Id_MonitoringType = cl.Id_MonitoringType AND m.Date BETWEEN cl.Start AND cl.Finish
inner join dbo.crs_S8_Client k on k.Id_Client=m.Id_Client

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S15_Monitoring"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 135
               Right = 221
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "crs_S12_Units_Register"
            Begin Extent = 
               Top = 7
               Left = 269
               Bottom = 99
               Right = 442
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V_S14_Product_Vo"
            Begin Extent = 
               Top = 7
               Left = 490
               Bottom = 135
               Right = 675
            End
            DisplayFlags = 280
            TopColumn = 27
         End
         Begin Table = "crs_S15_ProductMonitoring"
            Begin Extent = 
               Top = 102
               Left = 259
               Bottom = 232
               Right = 449
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S15_ClientMonitoring"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filt', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S15_Monitoring_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'er = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S15_Monitoring_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S15_Monitoring_Vo';

