﻿CREATE view V_S10_EkspositorPanelClientEnable
as
select ID_Ekspositor,ep.id_client
from [dbo].[crs_S10_EkspositorOrder] e 
     left join dbo.crs_S10_EkspositorOrderPanelSetItem c on e.Id_PanelSet = c.Id_PanelSet 
     inner join dbo.V_S10_ConstructionTypSpec ep  on ep.ID_Construction=e.ID_Construction
where Flag_LastOrder=1
  and c.Id_PanelSet is null

UNION

Select t.ID_Ekspositor,t.id_client
from 
	(
	select ID_Ekspositor,ep.id_client,count(ek.id_panel) cntP
	from dbo.V_S10_ConstructionEnablePanel ep
		 inner join
		 (
			select ID_Ekspositor,ID_Construction,id_panel from [dbo].[crs_S10_EkspositorOrder] e 
			  inner join dbo.crs_S10_EkspositorOrderPanelSetItem c on e.Id_PanelSet = c.Id_PanelSet
			Where Flag_LastOrder=1 
		  ) ek
	 on ek.ID_Construction=ep.ID_Construction and ep.Id_Panel=ek.id_panel
	group by ID_Ekspositor,ep.id_client
	 ) as t
inner join
	(
	select ID_Ekspositor,count(id_panel) cntP from [dbo].[crs_S10_EkspositorOrder] e 
		inner join dbo.crs_S10_EkspositorOrderPanelSetItem c on e.Id_PanelSet = c.Id_PanelSet
	Where Flag_LastOrder=1
	group by  ID_Ekspositor
	) r
on t.ID_Ekspositor=r.ID_Ekspositor and t.cntP=r.cntP