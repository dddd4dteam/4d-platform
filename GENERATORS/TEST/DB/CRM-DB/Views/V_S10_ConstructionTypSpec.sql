﻿

CREATE VIEW [dbo].[V_S10_ConstructionTypSpec]
AS

SELECT t1.ID_Construction, t1.Description AS Construction, t1.ConstructionWeight, t1.PanelCount, 
	   t1.PanelHeight, t1.PanelWidth, t1.Flag_Enable, t1.Code, 
	   t1.ImageIndexActual, 'http://expo.cersanit.ru/dbimg.php?f=img&t=k&i=' + CAST(t1.ImageIndexActual AS varchar) AS Image_Construction, 
       t1.ID_ConstructionType, T2.ConstructionType, 
	   CAST(CAST(t1.ConstructionHeight AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.ConstructionWidth AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.ConstructionDepth AS INT) AS VARCHAR) AS ConstructionHtWtDpt, 
	   CAST(CAST(t1.PanelHeight AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.PanelWidth AS INT) AS VARCHAR) AS PanelHtWth, t1.Flag_ForbidSelectPanels,
	   ID_Client
FROM
	(
	select ID_Construction,id_client from crs_S10_ConstructionClientEnabled
	UNION all
	select ID_Construction,id_client
	from (select id_client from crs_S8_Client) as t
	cross join 
		(select ID_Construction FROM crs_S10_Construction_Register where ID_Construction not in (select distinct ID_Construction from crs_S10_ConstructionClientEnabled)) c
	) AS t
inner join dbo.crs_S10_Construction_Register t1 on t.ID_Construction=t1.ID_Construction
INNER JOIN dbo.crs_S10_ConstructionType_Register T2 ON t1.ID_ConstructionType = T2.ID_ConstructionType


--SELECT t1.ID_Construction, t1.Description AS Construction, t1.ConstructionWeight, t1.PanelCount, 
--	   t1.PanelHeight, t1.PanelWidth, t1.Flag_Enable, t1.Code, 
--	   t1.ImageIndexActual, 'http://expo.cersanit.ru/dbimg.php?f=img&t=k&i=' + CAST(t1.ImageIndexActual AS varchar) AS Image_Construction, 
--       t1.ID_ConstructionType, T2.ConstructionType, 
--	   CAST(CAST(t1.ConstructionHeight AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.ConstructionWidth AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.ConstructionDepth AS INT) AS VARCHAR) AS ConstructionHtWtDpt, 
--	   CAST(CAST(t1.PanelHeight AS INT) AS VARCHAR) + 'x' + CAST(CAST(t1.PanelWidth AS INT) AS VARCHAR) AS PanelHtWth, t1.Flag_ForbidSelectPanels
--FROM 

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 253
            End
            DisplayFlags = 280
            TopColumn = 17
         End
         Begin Table = "T2"
            Begin Extent = 
               Top = 48
               Left = 328
               Bottom = 144
               Right = 528
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_ConstructionTypSpec';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_ConstructionTypSpec';

