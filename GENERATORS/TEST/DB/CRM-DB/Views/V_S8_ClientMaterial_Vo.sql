﻿
/*V_S8_ClientMaterial_Vo*/

CREATE VIEW [dbo].[V_S8_ClientMaterial_Vo]
AS
SELECT c.Id_ClientMaterial, c.Id_Client, Code_Client ,c.Id_Material, dbo.crs_S4_Material_Register.Material,
  dbo.crs_S4_Material_Register.Id_MaterialType
FROM dbo.crs_S8_ClientMaterial c
INNER JOIN  dbo.crs_S4_Material_Register ON c.Id_Material = dbo.crs_S4_Material_Register.Id_Material
inner join dbo.crs_S8_Client k ON c.Id_Client = k.Id_Client
