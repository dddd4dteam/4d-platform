﻿/*V_S8_Client_Vo
V_S8_Client_Vo*/
CREATE VIEW dbo.V_S8_Client_Vo
AS
SELECT        dbo.crs_S8_Client.Id_Client, dbo.crs_S8_Client.Code_Client, dbo.crs_S8_Client.Client, V_S1_GeographyItem_1.GeographyItem, dbo.crs_S8_Client.Street, dbo.crs_S8_Client.HouseNumber, 
                         dbo.V_S1_GeographyItem_Vo.GeographyItem AS RynokName, dbo.crs_S8_ClientRelation_Register.ClientRelation, dbo.crs_S8_ClientType_Register.ClientType, 
                         dbo.crs_S8_ClientPrestigeLevel_Register.ClientPrestigeLevel, dbo.crs_S8_Client.Id_ClientRelation, dbo.crs_S8_Client.Id_ClientPrestigeLevel, dbo.crs_S8_Client.Id_ClientPrestigeLevel3D, 
                         dbo.crs_S8_Client.Id_GeographyItem, CASE WHEN dbo.crs_S8_Client.Dt_Add = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), dbo.crs_S8_Client.Dt_Add, 103) END AS Dt_Add_str, 
                         CASE WHEN dbo.crs_S8_Client.Dt_Dell = '1755-01-01 00:00:00.000' THEN '' ELSE CONVERT(char(10), dbo.crs_S8_Client.Dt_Dell, 103) END AS Dt_Dell_str, dbo.crs_S8_Client.Id_TrRepres, 
                         V_S1_GeographyItem_1.Id_GeographyItemType, V_S1_GeographyItem_1.GeographyItemType, dbo.crs_S8_Client.HousePartNumber, dbo.crs_S8_Client.FlatNumber, dbo.crs_S8_Client.PavilionNumber, 
                         dbo.crs_S8_Client.AddressOther, dbo.crs_S8_Client.PhoneNumber, dbo.crs_S8_Client.postcode, dbo.crs_S8_Client.Mail, dbo.crs_S8_Client.WebAddress, dbo.crs_S8_Client.Flag_BrandCersanit, 
                         dbo.crs_S8_Client.Flag_BrandOposzno, dbo.crs_S8_Client.Flag_Warehouse, dbo.crs_S8_Client.AdditionInfo, dbo.crs_S8_Client.Dt_Add, dbo.crs_S8_Client.Dt_Dell, dbo.crs_S8_Client.Flag_InfoVerified, 
                         dbo.crs_S8_Client.Flag_Enable, dbo.crs_S8_Client.Id_UserDomainRole, dbo.crs_S8_Client.Flag_NeedPassed, dbo.crs_S7_Holding_Register.Holding, dbo.crs_S8_Client.Id_GeographyItemRynok, 
                         dbo.V_S1_GeographyItem_Vo.GeographyItemType AS TypeRynok, dbo.crs_S8_Client.AspRoleID, dbo.crs_S8_Client.UserID, dbo.crs_S8_Client.Id_Holding, dbo.crs_S8_Client.Cost_2D_Rub_Rovese, 
                         dbo.crs_S8_Client.Cost_3D_Rub_Rovese, dbo.crs_S8_Client.Cost_2D_Rub_Total, dbo.crs_S8_Client.Cost_3D_Rub_Total, dbo.crs_S8_Client.Id_PresentationType, dbo.crs_S8_Client.OpenTime, 
                         dbo.crs_S8_Client.SellerCount, dbo.crs_S8_Client.Id_PriceType, dbo.crs_S8_Client.Id_StoreSize, dbo.crs_S8_Client.Flag_Design, dbo.crs_S8_Client.Flag_BrandMeissen, dbo.crs_S8_Client.Id_ClientType, 
                         dbo.crs_S8_Client.Longitude, dbo.crs_S8_Client.Latitude, dbo.crs_S8_Client.CloseTime, CAST(NULL AS geography) AS GeographyPoint, dbo.crs_S8_Client.UrName, dbo.crs_S8_Client.ID_DeliveryTransport
FROM            dbo.crs_S8_Client LEFT OUTER JOIN
                         dbo.V_S1_GeographyItem_Vo ON dbo.crs_S8_Client.Id_GeographyItemRynok = dbo.V_S1_GeographyItem_Vo.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S8_ClientPrestigeLevel_Register ON dbo.crs_S8_Client.Id_ClientPrestigeLevel = dbo.crs_S8_ClientPrestigeLevel_Register.Id_ClientPrestigeLevel LEFT OUTER JOIN
                         dbo.V_S1_GeographyItem_Vo AS V_S1_GeographyItem_1 ON dbo.crs_S8_Client.Id_GeographyItem = V_S1_GeographyItem_1.Id_GeographyItem LEFT OUTER JOIN
                         dbo.crs_S8_ClientRelation_Register ON dbo.crs_S8_Client.Id_ClientRelation = dbo.crs_S8_ClientRelation_Register.Id_ClientRelation LEFT OUTER JOIN
                         dbo.crs_S8_ClientType_Register ON dbo.crs_S8_Client.Id_ClientType = dbo.crs_S8_ClientType_Register.Id_ClientType LEFT OUTER JOIN
                         dbo.crs_S7_Holding_Register ON dbo.crs_S8_Client.Id_Holding = dbo.crs_S7_Holding_Register.Id_Holding


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[22] 4[36] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[25] 4[50] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 8
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S8_Client"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 103
               Right = 248
            End
            DisplayFlags = 280
            TopColumn = 46
         End
         Begin Table = "V_S1_GeographyItem_Vo"
            Begin Extent = 
               Top = 6
               Left = 484
               Bottom = 125
               Right = 696
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_ClientPrestigeLevel_Register"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 230
               Right = 234
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V_S1_GeographyItem_1"
            Begin Extent = 
               Top = 135
               Left = 500
               Bottom = 254
               Right = 712
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_ClientRelation_Register"
            Begin Extent = 
               Top = 126
               Left = 272
               Bottom = 230
               Right = 443
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_ClientType_Register"
            Begin Extent = 
               Top = 234
               Left = 38
               Bottom = 338
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S7_Holding_Register"
            Begin Extent = 
               Top = 268
            ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_Client_Vo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'   Left = 247
               Bottom = 368
               Right = 422
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      PaneHidden = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4170
         Alias = 1455
         Table = 3615
         Output = 2085
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_Client_Vo';




GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_Client_Vo';

