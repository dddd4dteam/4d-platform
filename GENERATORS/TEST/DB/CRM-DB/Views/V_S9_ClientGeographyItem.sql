﻿
CREATE VIEW [dbo].[V_S9_ClientGeographyItem]
AS
SELECT distinct dbo.V_S1_GeographyItem_Vo.Id_GeographyItem, dbo.V_S1_GeographyItem_Vo.Id_GeographyParentItem, 
               dbo.V_S1_GeographyItem_Vo.Id_GeographyItemType, dbo.V_S1_GeographyItem_Vo.GeographyItem, dbo.V_S1_GeographyItem_Vo.Prefix, 
               dbo.V_S1_GeographyItem_Vo.GeographyItemType, 
               dbo.V_S8_Client_Vo.Id_TrRepres
FROM  dbo.V_S1_GeographyItemAncestor INNER JOIN
               dbo.V_S8_Client_Vo ON dbo.V_S1_GeographyItemAncestor.IDAncestors = dbo.V_S8_Client_Vo.Id_GeographyItem INNER JOIN
               dbo.V_S1_GeographyItem_Vo ON dbo.V_S1_GeographyItemAncestor.Id_GeographyItem = dbo.V_S1_GeographyItem_Vo.Id_GeographyItem
Where dbo.V_S8_Client_Vo.Flag_Enable = 1 /* условие добавил Огарок 04.03.15, что бы избавиться от лишних городов при выборе точки во встречах */

GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[14] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_S1_GeographyItemAncestor"
            Begin Extent = 
               Top = 18
               Left = 226
               Bottom = 146
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V_S8_Client_Vo"
            Begin Extent = 
               Top = 163
               Left = 35
               Bottom = 291
               Right = 258
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "V_S1_GeographyItem_Vo"
            Begin Extent = 
               Top = 156
               Left = 476
               Bottom = 284
               Right = 702
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 12
         Width = 284
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
         Width = 1200
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1176
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1356
         SortOrder = 1416
         GroupBy = 1350
         Filter = 1356
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_ClientGeographyItem';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_ClientGeographyItem';

