﻿/*			INNER JOIN  dbo.crs_S10_EkspositorOrder AS t4 ON t1.Id_PanelSet = t4.Id_PanelSet*/
CREATE VIEW dbo.V_S10_EkspositorOrderPanelSetItem_Vo
AS
SELECT        t2.Position, t3.Code, t3.Collection, t2.Flag_OnlyBase, t3.Description, t2.Flag_BuyPanels, t1.Id_PanelSet, t2.Id_Panel, t3.Id_Collection, t3.Width, t3.Height, t3.tmp_Flag_OnlyBase, t3.ID_PanelType, t3.PanelType, 
                         'http://expo.cersanit.ru/dbimg.php?f=psm&i=' + CAST(t2.ImageIndexActual AS varchar) AS Image_sm, 'http://expo.cersanit.ru/dbimg.php?f=pbg&i=' + CAST(t2.ImageIndexActual AS varchar) AS Image_bg, 
                         t2.Id_PanelSetItem, t2.Flag_Highlight, t2.HighlightComment, t2.CRM_RoleDescriptor, t2.ImageIndexActual, CAST(NULL AS VARCHAR(15)) AS EditTypeWord, CAST(NULL AS BIT) AS Flag_JustAdded, CAST(NULL 
                         AS DATETIME) AS Date_Added, CAST(NULL AS Int) AS IDOrder_Added, CAST(NULL AS BIT) AS Flag_JustDeleted, CAST(NULL AS DATETIME) AS Date_Deleted, CAST(NULL AS Int) AS IDOrder_Deleted, t3.ABC, 
                         t2.Panel_Guid, t3.Price
FROM            dbo.crs_S10_EkspositorOrderPanelSet AS t1 INNER JOIN
                         dbo.crs_S10_EkspositorOrderPanelSetItem AS t2 ON t1.Id_PanelSet = t2.Id_PanelSet INNER JOIN
                         dbo.crs_S10_Panel AS t3 ON t2.Id_Panel = t3.ID_Panel


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[39] 4[23] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "t1"
            Begin Extent = 
               Top = 0
               Left = 777
               Bottom = 97
               Right = 933
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t2"
            Begin Extent = 
               Top = 0
               Left = 446
               Bottom = 248
               Right = 676
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "t3"
            Begin Extent = 
               Top = 0
               Left = 0
               Bottom = 334
               Right = 297
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 4650
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrderPanelSetItem_Vo';




GO



GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S10_EkspositorOrderPanelSetItem_Vo';



