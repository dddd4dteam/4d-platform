﻿
CREATE view [dbo].[V_S8_ClientReportInfo]
as

select distinct cl.Id_Client,
		cl.Code_client,
		Client,
	   ISNULL(ISNULL(g4.GeographyItem,g3.GeographyItem),g2.GeographyItem) Country,
	   ISNULL(g3.GeographyItem,g2.GeographyItem) Region,
	   g2.GeographyItem SubRegion,
	   ISNULL(g1.Prefix,'') + g1.GeographyItem Town,
	   	   CASE WHEN Street IS NOT NULL THEN 'ул.'+ Street ELSE '' End
			+ CASE WHEN HouseNumber IS NOT NULL Then ', д.'+HouseNumber ELSE '' END
			+ CASE WHEN HousePartNumber IS NOT NULL THEN ', корп.'+HousePartNumber ELSE '' END
			+ CASE WHEN FlatNumber IS NOT NULL THEN ', кв.'+ FlatNumber ELSE '' END
			+ CASE WHEN ISNULL(cl.Id_GeographyItemRynok,-1) <> -1 THEN ', р-к '+ r.GeographyItem ELSE '' END
			+ CASE WHEN cl.PavilionNumber Is NOT NULL THEN ', пав.'+cl.PavilionNumber ELSE '' END
		AS Address, --Address Formatted
		postcode,
	   	cl.PhoneNumber,
		cl.Mail,
		cl.WebAddress,
       IsNull(Cast(cl.Latitude as varchar(max))+' '+Cast(cl.Longitude as varchar(max)),'') GPS, --GPS Formatted
	   tr.Description+' ('+RTrim(tr.Name)+')' TrRepresN, -- TrRepresName Formatted
	   holding,
	   clp2.ClientPrestigeLevel Category2D,
       cast(isnull(dtLastUpdateCategory2D,dt_add) as date) dtLastUpdateCategory2D,
	   clp3.ClientPrestigeLevel Category3D,
	   cast(isnull(dtLastUpdateCategory3D,dt_add) as date) dtLastUpdateCategory3D,
	   ClientRelation,
	   StoreSize SizePOS,
       case When CONVERT(nvarchar(30), OpenTime,108) is not null and CONVERT(nvarchar(30), CloseTime,108) is not null
		      Then 'c ' +left(CONVERT(nvarchar(30), OpenTime,108),5)+ ' до '+left(CONVERT(nvarchar(30), CloseTime,108),5)
			 When CONVERT(nvarchar(30), OpenTime,108) is not null and CONVERT(nvarchar(30), CloseTime,108) is null
			  Then 'c ' +left(CONVERT(nvarchar(30), OpenTime,108),5)
			 When CONVERT(nvarchar(30), OpenTime,108) is null and CONVERT(nvarchar(30), CloseTime,108) is not null
			  Then 'до ' +left(CONVERT(nvarchar(30), CloseTime,108),5)
		else ''
		end WorkingTime, --Time(Open/Close) Formatted,
		SellerCount,  --Количество продавцов
		Flag_Design,
		isnull(left(AllDiler,len(AllDiler)-1),'') AllDilers,
		d.description director,
		Case When m.Id_Director='00004 ' Then 'DIY' Else 'Traditional trade' End Channel,
		isnull(ClientType,'Не указано') ClientType,
		cast(DateMeeting1 as date) DateMeeting1,
		iif(DateMeeting1<DateMeeting2,cast(DateMeeting2 as date),null) DateMeeting2,
		isnull(left(AllOperation,len(AllOperation)-1),'') AllOperation
from crs_S8_Client cl WITH(NOLOCK)
Left join crs_S7_Holding_Register hl  WITH(NOLOCK)on(cl.Id_holding = hl.Id_Holding)
Left join crs_S8_ClientType_Register tp WITH(NOLOCK) on(cl.Id_ClientType = tp.Id_ClientType)
LEFT JOIN crs_S1_GeographyItem g1 WITH(NOLOCK) ON(cl.Id_GeographyItem = g1.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g2 WITH(NOLOCK) ON(g1.Id_GeographyParentItem = g2.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g3 WITH(NOLOCK) ON(g2.Id_GeographyParentItem = g3.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem g4 WITH(NOLOCK) ON(g3.Id_GeographyParentItem = g4.Id_GeographyItem)
LEFT JOIN crs_S1_GeographyItem r WITH(NOLOCK) ON(cl.Id_GeographyItemRynok = r.Id_GeographyItem)
Inner join crs_TrRepres tr WITH(NOLOCK) on(cl.Id_TrRepres = tr.Id_TrRepres)
Inner join crs_Manager m WITH(NOLOCK) on(tr.Id_Manager = m.Id_Manager)
Inner join crs_Director d WITH(NOLOCK) on(m.Id_Director = d.Id_Director)
INNER JOIN crs_S8_ClientRelation_Register clr WITH(NOLOCK) ON(cl.Id_ClientRelation = clr.Id_ClientRelation)
LEFT JOIN crs_S8_ClientPrestigeLevel_Register clp2 WITH(NOLOCK) ON(cl.Id_ClientPrestigeLevel = clp2.Id_ClientPrestigeLevel)
LEFT JOIN crs_S8_ClientPrestigeLevel_Register clp3 WITH(NOLOCK) ON(cl.Id_ClientPrestigeLevel = clp3.Id_ClientPrestigeLevel)
Left join sys_Flags_NeedPassed np  WITH(NOLOCK) on (cl.id_client=np.id_object and np.PrefixNameObject='S8')
left join sys_LastUpdateCategoryClient upc  WITH(NOLOCK) on cl.id_client=upc.id_client
left join crs_S16_StoreSize_Register stp on cl.Id_StoreSize=stp.Id_StoreSize
----Дилеры
left join
(
	select id_client,
		(select cast(holding as nvarchar)+ ',' as 'data()'
			from
			(
			select distinct id_client,dl.Holding
			from  crs_s8_ClientDiler cd
			Inner join crs_s6_Diler dl on cd.Id_Diler = dl.Id_Diler
		) b where a.[Id_client]=b.[Id_client]  for xml path('')) AllDiler
	from
		(
		select distinct id_client,dl.Holding
		from  crs_s8_ClientDiler cd
		Inner join crs_s6_Diler dl on cd.Id_Diler = dl.Id_Diler
		) a
) y on y.Id_Client=cl.Id_Client
left join
	--Последняя состоявшаяся встреча
	(
	select distinct id_client,max(starttime) over(partition by ID_Client) DateMeeting1
	from crs_S9_Meeting
	Where id_status=1
	) mt1 on cl.Id_Client=mt1.Id_Client
	left join
		--Все запланированные встречи и цели
		(
		select distinct a.id_client,a.starttime DateMeeting2,
				(select Description_RUS+ ',' as 'data()'
					from
					(
					select distinct id_client,ore.Description_RUS,starttime
					from  crs_S9_Meeting mt
					Inner join crs_S9_MeetingOperation mo on mt.Id_Meeting = mo.id_meeting
					inner join crs_S9_Operation_Register ore on mo.Id_Operation=ore.Id_Operation
					Where id_status=2
				) b where a.[Id_client]=b.[Id_client] and a.starttime=b.starttime  for xml path('')) AllOperation
		from
		(
		select distinct id_client,ore.Description_RUS,starttime
		from  crs_S9_Meeting mt
		Inner join crs_S9_MeetingOperation mo on mt.Id_Meeting = mo.id_meeting
		inner join crs_S9_Operation_Register ore on mo.Id_Operation=ore.Id_Operation
		Where id_status=2
		) as a
			inner join
			--Последняя запланированная встреча
			(
			select distinct id_client,max(starttime) over(partition by ID_Client) starttime
			from crs_S9_Meeting
			Where id_status=2
			) am on a.Id_Client=am.Id_Client and a.starttime=am.starttime
		) mt2 on cl.Id_Client=mt2.Id_Client