﻿

/*V_S8_ClientAssortment_Vo*/

CREATE view [dbo].[V_S8_ClientAssortment_Vo]
as
SELECT   dbo.V_S2_Assortment.Id_AssortGrupa, dbo.V_S2_Assortment.Id_Colection, dbo.V_S2_Assortment.AssortGrupa, dbo.V_S2_Assortment.Colection,
        dbo.crs_S8_ClientAssortment.Id_Assortment, dbo.crs_S8_ClientAssortment.Id_Client,Code_Client , dbo.crs_S8_Client.Client,
        dbo.crs_S8_ClientAssortment.Id_ClientAssortStatus, dbo.crs_S8_ClientAssortStatus_Register.ClientAssortStatus,
        dbo.crs_S2_Colection.Status AS AssortmentColectionStatus
FROM   dbo.crs_S8_ClientAssortment INNER JOIN
        dbo.crs_S8_Client ON dbo.crs_S8_ClientAssortment.Id_Client = dbo.crs_S8_Client.Id_Client INNER JOIN
        dbo.V_S2_Assortment ON dbo.crs_S8_ClientAssortment.Id_Assortment = dbo.V_S2_Assortment.Id_Assortment INNER JOIN
        dbo.crs_S2_Colection ON dbo.V_S2_Assortment.Id_Colection = dbo.crs_S2_Colection.Id_Colection LEFT OUTER JOIN
        dbo.crs_S8_ClientAssortStatus_Register ON
        dbo.crs_S8_ClientAssortment.Id_ClientAssortStatus = dbo.crs_S8_ClientAssortStatus_Register.Id_ClientAssortStatus


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[26] 2[15] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S8_ClientAssortment"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 99
               Right = 223
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S8_Client"
            Begin Extent = 
               Top = 6
               Left = 261
               Bottom = 114
               Right = 462
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V_S2_Assortment"
            Begin Extent = 
               Top = 102
               Left = 38
               Bottom = 210
               Right = 194
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "crs_S2_Colection"
            Begin Extent = 
               Top = 6
               Left = 500
               Bottom = 121
               Right = 661
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "crs_S8_ClientAssortStatus_Register"
            Begin Extent = 
               Top = 114
               Left = 232
               Bottom = 207
               Right = 417
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 3180
         Alias = 900
         Table = 3180
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = ', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_ClientAssortment_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane2', @value = N'1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_ClientAssortment_Vo';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S8_ClientAssortment_Vo';

