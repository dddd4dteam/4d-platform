﻿

/*V_S9_ClientMonitoring*/

CREATE VIEW [dbo].[V_S9_ClientMonitoring]
AS
SELECT dbo.crs_S15_ClientMonitoring.Id_MonitoringType, dbo.crs_S15_ClientMonitoring.Id_row, dbo.crs_S15_MonitoringType_Register.Start,
   dbo.crs_S15_MonitoringType_Register.Finish, dbo.V_S8_Client_Vo.Id_Client,Code_Client, dbo.V_S8_Client_Vo.Client, dbo.V_S8_Client_Vo.GeographyItem,
   dbo.V_S8_Client_Vo.Street, dbo.V_S8_Client_Vo.HouseNumber, dbo.V_S8_Client_Vo.RynokName,
   dbo.V_S8_Client_Vo.ClientRelation, dbo.V_S8_Client_Vo.ClientType, dbo.V_S8_Client_Vo.ClientPrestigeLevel, dbo.V_S8_Client_Vo.Id_ClientRelation,
   dbo.V_S8_Client_Vo.Id_ClientType, dbo.V_S8_Client_Vo.Id_ClientPrestigeLevel, dbo.V_S8_Client_Vo.Id_GeographyItem,
   dbo.V_S8_Client_Vo.Dt_Add_str, dbo.V_S8_Client_Vo.Dt_Dell_str, dbo.V_S8_Client_Vo.Id_TrRepres, dbo.V_S8_Client_Vo.Id_GeographyItemType,
   dbo.V_S8_Client_Vo.GeographyItemType, dbo.V_S8_Client_Vo.HousePartNumber, dbo.V_S8_Client_Vo.FlatNumber,
   dbo.V_S8_Client_Vo.PavilionNumber, dbo.V_S8_Client_Vo.AddressOther, dbo.V_S8_Client_Vo.PhoneNumber, dbo.V_S8_Client_Vo.postcode,
   dbo.V_S8_Client_Vo.Mail, dbo.V_S8_Client_Vo.WebAddress, dbo.V_S8_Client_Vo.Flag_BrandCersanit, dbo.V_S8_Client_Vo.Flag_BrandOposzno,
   dbo.V_S8_Client_Vo.Flag_Warehouse, dbo.V_S8_Client_Vo.AdditionInfo, dbo.V_S8_Client_Vo.Dt_Add, dbo.V_S8_Client_Vo.Dt_Dell,
   dbo.V_S8_Client_Vo.Flag_InfoVerified, dbo.V_S8_Client_Vo.Flag_Enable, dbo.V_S8_Client_Vo.Id_UserDomainRole,
   dbo.V_S8_Client_Vo.Flag_NeedPassed, dbo.V_S8_Client_Vo.Holding, dbo.V_S8_Client_Vo.Id_GeographyItemRynok, dbo.V_S8_Client_Vo.TypeRynok,
   dbo.V_S8_Client_Vo.AspRoleID, dbo.V_S8_Client_Vo.UserID, dbo.V_S8_Client_Vo.Id_Holding
FROM  dbo.crs_S15_ClientMonitoring INNER JOIN
   dbo.crs_S15_MonitoringType_Register ON
   dbo.crs_S15_ClientMonitoring.Id_MonitoringType = dbo.crs_S15_MonitoringType_Register.Id_MonitoringType INNER JOIN
   dbo.V_S8_Client_Vo ON dbo.crs_S15_ClientMonitoring.Id_Client = dbo.V_S8_Client_Vo.Id_Client


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "crs_S15_ClientMonitoring"
            Begin Extent = 
               Top = 7
               Left = 48
               Bottom = 135
               Right = 238
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "crs_S15_MonitoringType_Register"
            Begin Extent = 
               Top = 7
               Left = 286
               Bottom = 135
               Right = 476
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "V_S8_Client_Vo"
            Begin Extent = 
               Top = 140
               Left = 48
               Bottom = 268
               Right = 271
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_ClientMonitoring';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'V_S9_ClientMonitoring';

