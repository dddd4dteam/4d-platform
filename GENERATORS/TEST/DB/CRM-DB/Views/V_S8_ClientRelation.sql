﻿
/*V_S8_ClientRelation*/

CREATE VIEW [dbo].[V_S8_ClientRelation]
AS
SELECT dbo.crs_S8_Client.Id_Client, Code_Client, dbo.crs_S8_Client.Client, dbo.crs_S8_ClientRelation_Register.Id_ClientRelation,
  dbo.crs_S8_ClientRelation_Register.ClientRelation
FROM dbo.crs_S8_Client LEFT OUTER JOIN
  dbo.crs_S8_ClientRelation_Register ON dbo.crs_S8_Client.Id_ClientRelation = dbo.crs_S8_ClientRelation_Register.Id_ClientRelation
