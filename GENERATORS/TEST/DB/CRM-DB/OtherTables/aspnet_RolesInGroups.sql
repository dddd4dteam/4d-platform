﻿CREATE TABLE [dbo].[aspnet_RolesInGroups] (
    [RoleId]  UNIQUEIDENTIFIER NOT NULL,
    [GroupId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([RoleId] ASC, [GroupId] ASC),
    FOREIGN KEY ([GroupId]) REFERENCES [dbo].[aspnet_Groups] ([GroupId]),
    CONSTRAINT [FK__aspnet_Ro__RoleI__787EE5A0] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
);

