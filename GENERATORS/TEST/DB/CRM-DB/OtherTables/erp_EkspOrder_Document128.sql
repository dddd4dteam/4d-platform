﻿CREATE TABLE [dbo].[erp_EkspOrder_Document128] (
    [_IDRRef]    BINARY (16)  NULL,
    [_Marked]    BINARY (1)   NULL,
    [_Date_Time] CHAR (30)    NULL,
    [_Number]    INT          NOT NULL,
    [_Posted]    BINARY (1)   NULL,
    [_Fld130]    BINARY (1)   NULL,
    [_fld131]    INT          NOT NULL,
    [_Fld132]    NVARCHAR (5) NULL,
    [_Fld133]    INT          NOT NULL,
    [_Fld154]    BINARY (1)   NULL
);

