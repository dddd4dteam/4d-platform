﻿CREATE TABLE [dbo].[aspnet_UsersInRules] (
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [RuleId]    UNIQUEIDENTIFIER NOT NULL,
    [ValidFrom] DATETIME         NULL,
    [ValidTo]   DATETIME         NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC, [RuleId] ASC),
    FOREIGN KEY ([RuleId]) REFERENCES [dbo].[aspnet_Rules] ([RuleId]),
    CONSTRAINT [FK__aspnet_Us__UserI__68487DD7] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);

