﻿CREATE TABLE [dbo].[erp_EkspOrder_Document54_VT60] (
    [_Document54_IDRRef] BINARY (16)  NULL,
    [_KeyField]          BINARY (4)   NULL,
    [_LineNo61]          BIGINT       NULL,
    [_Fld62]             NVARCHAR (7) NULL,
    [_Fld149]            BINARY (1)   NULL,
    [_Fld63]             BINARY (1)   NULL,
    [_Fld64]             INT          NULL,
    [_Fld182]            NVARCHAR (7) NULL
);

