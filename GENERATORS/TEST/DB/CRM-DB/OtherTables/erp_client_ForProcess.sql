﻿CREATE TABLE [dbo].[erp_client_ForProcess] (
    [Id_client]             UNIQUEIDENTIFIER NOT NULL,
    [Code_Client]           INT              NOT NULL,
    [Client]                VARCHAR (200)    NOT NULL,
    [Id_GeographyItem]      INT              NOT NULL,
    [Street]                VARCHAR (50)     NULL,
    [HouseNumber]           VARCHAR (10)     NULL,
    [HousePartNumber]       VARCHAR (10)     NULL,
    [FlatNumber]            VARCHAR (10)     NULL,
    [Id_GeographyItemRynok] INT              NULL,
    [PavilionNumber]        VARCHAR (15)     NULL,
    [PostCode]              VARCHAR (7)      NULL,
    [PhoneNumber]           VARCHAR (100)    NULL,
    [Id_TrRepres]           CHAR (6)         NOT NULL,
    [DilerList]             VARCHAR (500)    NULL,
    [Flag_Enable]           BIT              NOT NULL
);

