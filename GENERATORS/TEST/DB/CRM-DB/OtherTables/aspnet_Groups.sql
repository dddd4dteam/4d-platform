﻿CREATE TABLE [dbo].[aspnet_Groups] (
    [ApplicationId] UNIQUEIDENTIFIER NOT NULL,
    [GroupId]       UNIQUEIDENTIFIER CONSTRAINT [DF_aspnet_Groups_GroupId] DEFAULT (newid()) NOT NULL,
    [GroupName]     NVARCHAR (256)   NOT NULL,
    [ParentGroupId] UNIQUEIDENTIFIER NULL,
    [Description]   NVARCHAR (256)   NOT NULL,
    [Code]          NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK__aspnet_Groups] PRIMARY KEY CLUSTERED ([GroupId] ASC),
    CONSTRAINT [FK_aspnet_Groups_aspnet_Applications] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId]),
    CONSTRAINT [FK_aspnet_Groups_aspnet_Groups] FOREIGN KEY ([ParentGroupId]) REFERENCES [dbo].[aspnet_Groups] ([GroupId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_aspnet_Groups]
    ON [dbo].[aspnet_Groups]([ApplicationId] ASC, [GroupName] ASC);

