﻿CREATE TABLE [dbo].[aspnet_Roles] (
    [ApplicationId]   UNIQUEIDENTIFIER NOT NULL,
    [RoleId]          UNIQUEIDENTIFIER CONSTRAINT [DF__aspnet_Ro__RoleI__33D4B598] DEFAULT (newid()) NOT NULL,
    [ParentRoleID]    UNIQUEIDENTIFIER NULL,
    [RoleName]        NVARCHAR (256)   NOT NULL,
    [LoweredRoleName] NVARCHAR (256)   NOT NULL,
    [Description]     NVARCHAR (256)   NULL,
    [Code]            NVARCHAR (MAX)   NULL,
    [ImportSystemID]  NVARCHAR (10)    NULL,
    CONSTRAINT [PK__aspnet_Roles__31EC6D26] PRIMARY KEY NONCLUSTERED ([RoleId] ASC),
    CONSTRAINT [FK_aspnet_Roles_aspnet_Roles] FOREIGN KEY ([ParentRoleID]) REFERENCES [dbo].[aspnet_Roles] ([RoleId])
);


GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1]
    ON [dbo].[aspnet_Roles]([ApplicationId] ASC, [LoweredRoleName] ASC);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'код на C#. динамически генерируемый шаблон для пользования данным объектом', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'aspnet_Roles', @level2type = N'COLUMN', @level2name = N'Code';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'ImportSystemID ==IDEmployee(old variant)  == Id_UserDomainRole(current variant)  - Идентификатор доменной роли-назначения для какого-то пользователя системы', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'aspnet_Roles', @level2type = N'COLUMN', @level2name = N'ImportSystemID';

