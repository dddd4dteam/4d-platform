﻿CREATE TABLE [dbo].[product_to_add_monitoring] (
    [Goods]        VARCHAR (100) NOT NULL,
    [Id_Brand]     INT           NOT NULL,
    [Id_Grupa]     INT           NOT NULL,
    [Id_Colection] INT           NOT NULL,
    [Id_Format]    INT           NULL,
    [Id_Property1] INT           NULL,
    [Id_Property2] INT           NULL,
    [Id_Property3] INT           NULL,
    [Id_Units]     INT           NULL
);

