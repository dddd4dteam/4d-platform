﻿CREATE TABLE [dbo].[aspnet_Rules] (
    [ApplicationId] UNIQUEIDENTIFIER NOT NULL,
    [RuleId]        UNIQUEIDENTIFIER CONSTRAINT [DF__aspnet_Ru__RuleI__6383C8BA] DEFAULT (newid()) NOT NULL,
    [RuleName]      NVARCHAR (256)   NOT NULL,
    [Description]   NVARCHAR (256)   NULL,
    [Code]          NVARCHAR (MAX)   NULL,
    CONSTRAINT [PK_aspnet_Rules] PRIMARY KEY CLUSTERED ([RuleId] ASC),
    CONSTRAINT [FK_aspnet_Rules_aspnet_Applications] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_aspnet_Rules]
    ON [dbo].[aspnet_Rules]([ApplicationId] ASC, [RuleName] ASC);

