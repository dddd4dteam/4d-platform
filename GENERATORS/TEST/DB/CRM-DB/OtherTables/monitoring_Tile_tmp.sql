﻿CREATE TABLE [dbo].[monitoring_Tile_tmp] (
    [Grupa1]              VARCHAR (6)    NULL,
    [Unique_ID]           VARCHAR (100)  NULL,
    [Grupa2]              VARCHAR (100)  NULL,
    [Brend]               VARCHAR (100)  NULL,
    [Collection]          VARCHAR (8000) NULL,
    [Format_collection]   VARCHAR (100)  NULL,
    [Length]              FLOAT (53)     NULL,
    [Width]               FLOAT (53)     NULL,
    [Color_Determination] VARCHAR (100)  NULL,
    [Article]             VARCHAR (100)  NULL,
    [Name]                VARCHAR (250)  NULL,
    [Unit]                VARCHAR (10)   NULL
);

