﻿CREATE TABLE [dbo].[aspnet_RolesHierarchy] (
    [RoleId]       UNIQUEIDENTIFIER NOT NULL,
    [ParentRoleID] UNIQUEIDENTIFIER NOT NULL,
    CONSTRAINT [PK_aspnet_RolesHierarchy] PRIMARY KEY CLUSTERED ([RoleId] ASC, [ParentRoleID] ASC)
);

