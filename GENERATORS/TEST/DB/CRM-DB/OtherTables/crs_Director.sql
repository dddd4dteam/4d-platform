﻿CREATE TABLE [dbo].[crs_Director] (
    [Id_Director] CHAR (6)     NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Name]        VARCHAR (50) NULL,
    CONSTRAINT [PK_crs_Director] PRIMARY KEY CLUSTERED ([Id_Director] ASC)
);

