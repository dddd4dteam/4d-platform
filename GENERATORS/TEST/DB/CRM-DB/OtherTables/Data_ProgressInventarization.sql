﻿CREATE TABLE [dbo].[Data_ProgressInventarization] (
    [Id_TrRepres] CHAR (6)      NOT NULL,
    [TrRepresN]   VARCHAR (103) NULL,
    [Director]    VARCHAR (50)  NOT NULL,
    [Mn]          INT           NULL,
    [ye]          INT           NULL,
    [Plan_CRM]    INT           NOT NULL,
    [Confirm_RD]  INT           NOT NULL,
    [Fact_CRM]    INT           NOT NULL,
    [QTY_2D]      INT           NULL
);

