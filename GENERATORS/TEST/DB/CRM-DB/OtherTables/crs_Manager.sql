﻿CREATE TABLE [dbo].[crs_Manager] (
    [Id_Manager]  CHAR (6)     NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [Id_Director] CHAR (6)     NOT NULL,
    CONSTRAINT [PK_crs_Manager] PRIMARY KEY CLUSTERED ([Id_Manager] ASC),
    CONSTRAINT [FK_crs_Manager_crs_Director] FOREIGN KEY ([Id_Director]) REFERENCES [dbo].[crs_Director] ([Id_Director])
);

