﻿CREATE TABLE [dbo].[GpsTrackerPoints] (
    [Id_tracker]     INT               NULL,
    [Dt_code]        BIGINT            NULL,
    [v]              INT               NULL,
    [Longitude]      FLOAT (53)        NULL,
    [Latitude]       FLOAT (53)        NULL,
    [GeographyPoint] [sys].[geography] NULL
);


GO
CREATE CLUSTERED INDEX [IX_GpsTrackerPoints]
    ON [dbo].[GpsTrackerPoints]([Id_tracker] ASC);

