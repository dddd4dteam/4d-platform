﻿CREATE TABLE [dbo].[aspnet_RulesInGroupsSecond] (
    [RuleId]  UNIQUEIDENTIFIER NOT NULL,
    [GroupId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([RuleId] ASC, [GroupId] ASC),
    FOREIGN KEY ([GroupId]) REFERENCES [dbo].[aspnet_Groups] ([GroupId]),
    FOREIGN KEY ([RuleId]) REFERENCES [dbo].[aspnet_Rules] ([RuleId])
);

