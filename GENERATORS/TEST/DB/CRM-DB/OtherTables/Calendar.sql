﻿CREATE TABLE [dbo].[Calendar] (
    [Dt_code] INT      NOT NULL,
    [Dt]      DATETIME NOT NULL,
    [Yr]      INT      NOT NULL,
    [Qr]      INT      NOT NULL,
    [Qr_name] CHAR (9) NOT NULL,
    [Mn]      INT      NOT NULL,
    [Mn_name] CHAR (8) NOT NULL,
    [Wk]      INT      NOT NULL,
    [Wk_name] CHAR (9) NOT NULL,
    [Dd]      INT      NOT NULL,
    [Period]  INT      NULL,
    [Holiday] BIT      NULL,
    CONSTRAINT [PK_Calendar] PRIMARY KEY CLUSTERED ([Dt_code] ASC)
);

