﻿CREATE TABLE [dbo].[erp_EkspOrder_Document54] (
    [_IDRRef]    BINARY (16)  NULL,
    [_Marked]    BINARY (1)   NULL,
    [_Date_Time] CHAR (30)    NULL,
    [_Number]    INT          NOT NULL,
    [_Posted]    BINARY (1)   NULL,
    [_Fld55]     BINARY (1)   NULL,
    [_Fld56]     NVARCHAR (5) NULL,
    [_Fld57]     INT          NOT NULL,
    [_Fld59]     BINARY (1)   NULL,
    [_Fld101]    INT          NOT NULL,
    [_Fld103]    CHAR (30)    NULL,
    [_Fld146]    BINARY (1)   NULL,
    [_Fld148]    BINARY (1)   NULL,
    [_Fld281]    NCHAR (2)    NULL,
    [_Fld283]    NUMERIC (7)  NULL
);

