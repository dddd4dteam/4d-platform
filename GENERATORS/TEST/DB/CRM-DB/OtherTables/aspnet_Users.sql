﻿CREATE TABLE [dbo].[aspnet_Users] (
    [ApplicationId]    UNIQUEIDENTIFIER NOT NULL,
    [UserId]           UNIQUEIDENTIFIER CONSTRAINT [DF__aspnet_Us__UserI__0519C6AF] DEFAULT (newid()) NOT NULL,
    [UserName]         NVARCHAR (256)   NOT NULL,
    [LoweredUserName]  NVARCHAR (256)   NOT NULL,
    [MobileAlias]      NVARCHAR (35)    CONSTRAINT [DF__aspnet_Us__Mobil__060DEAE8] DEFAULT (NULL) NULL,
    [IsAnonymous]      BIT              CONSTRAINT [DF__aspnet_Us__IsAno__07020F21] DEFAULT ((0)) NOT NULL,
    [LastActivityDate] DATETIME         NOT NULL,
    [FirstName]        NVARCHAR (50)    NULL,
    [LastName]         NVARCHAR (50)    NULL,
    [PatronymicName]   NVARCHAR (50)    NULL,
    [DisplayName]      NVARCHAR (50)    NULL,
    [WorkPhone]        NCHAR (10)       NULL,
    [MobilePhone]      NCHAR (10)       NULL,
    [BirthDay]         DATETIME         NULL,
    CONSTRAINT [PK__aspnet_Users__03317E3D] PRIMARY KEY NONCLUSTERED ([UserId] ASC),
    CONSTRAINT [FK__aspnet_Us__Appli__0425A276] FOREIGN KEY ([ApplicationId]) REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
);


GO
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index]
    ON [dbo].[aspnet_Users]([ApplicationId] ASC, [LoweredUserName] ASC);


GO
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2]
    ON [dbo].[aspnet_Users]([ApplicationId] ASC, [LastActivityDate] ASC);

