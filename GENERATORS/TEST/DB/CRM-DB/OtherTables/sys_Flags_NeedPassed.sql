﻿CREATE TABLE [dbo].[sys_Flags_NeedPassed] (
    [ID_Object]         UNIQUEIDENTIFIER NOT NULL,
    [PrefixNameObject]  VARCHAR (3)      NOT NULL,
    [Flag_NeedPassed]   BIT              NOT NULL,
    [Flag_WasPassed]    BIT              NULL,
    [LastDt_NeedPassed] SMALLDATETIME    NULL,
    CONSTRAINT [PK_sys_Flags_NeedPassed] PRIMARY KEY CLUSTERED ([ID_Object] ASC)
);

