﻿CREATE TABLE [dbo].[monitoring_ceramic_tmp] (
    [Product_group]     VARCHAR (100) NULL,
    [Unique_ID]         VARCHAR (100) NULL,
    [Product_sub_group] VARCHAR (100) NULL,
    [Type]              VARCHAR (100) NULL,
    [Brend]             VARCHAR (100) NULL,
    [Collection]        VARCHAR (100) NULL,
    [Article]           VARCHAR (100) NULL,
    [Name]              VARCHAR (250) NULL
);

