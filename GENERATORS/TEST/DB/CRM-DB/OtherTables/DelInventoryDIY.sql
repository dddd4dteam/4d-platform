﻿CREATE TABLE [dbo].[DelInventoryDIY] (
    [Id_Meeting]    UNIQUEIDENTIFIER NOT NULL,
    [Value1Count]   INT              NULL,
    [Value2Count]   INT              NULL,
    [Value3Count]   INT              NULL,
    [InventoryItem] VARCHAR (50)     NOT NULL,
    [Manufacturer]  VARCHAR (100)    NOT NULL,
    [StartTime]     DATETIME         NOT NULL,
    [Client]        VARCHAR (200)    NOT NULL,
    [TrRepres]      VARCHAR (50)     NOT NULL,
    [UserName]      NVARCHAR (256)   NULL,
    [RoleName]      NVARCHAR (256)   NULL
);

