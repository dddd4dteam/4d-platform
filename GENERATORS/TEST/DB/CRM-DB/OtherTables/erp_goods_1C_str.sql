﻿CREATE TABLE [dbo].[erp_goods_1C_str] (
    [_IDRRef]       BINARY (16)  NOT NULL,
    [_ParentIDRRef] BINARY (16)  NOT NULL,
    [_Code]         NVARCHAR (7) NOT NULL,
    CONSTRAINT [PK_goods_1C_str] PRIMARY KEY CLUSTERED ([_IDRRef] ASC)
);

