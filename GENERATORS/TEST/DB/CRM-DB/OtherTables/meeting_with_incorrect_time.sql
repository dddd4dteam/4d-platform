﻿CREATE TABLE [dbo].[meeting_with_incorrect_time] (
    [Id_Meeting] UNIQUEIDENTIFIER NOT NULL,
    [Dt]         DATETIME         NULL,
    CONSTRAINT [PK_meeting_with_incorrect_time] PRIMARY KEY CLUSTERED ([Id_Meeting] ASC)
);

