﻿CREATE TABLE [dbo].[erp_EkspOrder_Document106] (
    [_IDRRef]    BINARY (16)  NULL,
    [_Marked]    BINARY (1)   NULL,
    [_Date_Time] CHAR (30)    NULL,
    [_Number]    INT          NOT NULL,
    [_Posted]    BINARY (1)   NULL,
    [_Fld107]    BINARY (1)   NULL,
    [_fld108]    INT          NOT NULL,
    [_Fld109]    NVARCHAR (5) NULL,
    [_Fld110]    INT          NOT NULL,
    [_Fld111]    NVARCHAR (5) NULL,
    [_Fld112]    INT          NOT NULL,
    [_Fld151]    BINARY (1)   NULL
);

