﻿CREATE TABLE [dbo].[aspnet_UsersInGroups] (
    [UserId]    UNIQUEIDENTIFIER NOT NULL,
    [GroupId]   UNIQUEIDENTIFIER NOT NULL,
    [ValidFrom] DATETIME         NULL,
    [ValidTo]   DATETIME         NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC, [GroupId] ASC),
    FOREIGN KEY ([GroupId]) REFERENCES [dbo].[aspnet_Groups] ([GroupId]),
    CONSTRAINT [FK__aspnet_Us__UserI__74AE54BC] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);

