﻿CREATE TABLE [dbo].[aspnet_UsersInRulesSecond] (
    [UserId] UNIQUEIDENTIFIER NOT NULL,
    [RuleId] UNIQUEIDENTIFIER NOT NULL,
    PRIMARY KEY CLUSTERED ([UserId] ASC, [RuleId] ASC),
    FOREIGN KEY ([RuleId]) REFERENCES [dbo].[aspnet_Rules] ([RuleId]),
    CONSTRAINT [FK__aspnet_Us__UserI__6C190EBB] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
);

