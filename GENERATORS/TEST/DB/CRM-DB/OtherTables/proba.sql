﻿CREATE TABLE [dbo].[proba] (
    [k]       BIGINT          NULL,
    [art]     NVARCHAR (25)   NULL,
    [name]    NVARCHAR (65)   NULL,
    [code]    NVARCHAR (15)   NULL,
    [inuse]   BIT             NULL,
    [new]     BIT             NULL,
    [tm]      DATETIME        NULL,
    [norma]   NTEXT           NULL,
    [psm]     IMAGE           NULL,
    [pbg]     IMAGE           NULL,
    [col]     NVARCHAR (45)   NULL,
    [tip]     NVARCHAR (45)   NULL,
    [uid]     NVARCHAR (45)   NULL,
    [period]  DATETIME        NULL,
    [uidspec] NVARCHAR (45)   NULL,
    [spec]    NVARCHAR (2000) NULL,
    [coment]  NTEXT           NULL,
    [arc]     BIT             NULL
);

