﻿CREATE TABLE [dbo].[DeleteMeetingOperation] (
    [Id_MeetingOperation] UNIQUEIDENTIFIER NOT NULL,
    [Id_Meeting]          UNIQUEIDENTIFIER NOT NULL,
    [Id_Operation]        INT              NOT NULL
);

