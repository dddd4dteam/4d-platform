﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.CRM.DOM.DAL
{
    public class SMKey
    {
        public const string S1_Geography = nameof(S1_Geography);
        public const string S2_Assortment = nameof(S2_Assortment);
        public const string S3_GoodsCategory = nameof(S3_GoodsCategory);
        public const string S4_Material = nameof(S4_Material);       
        public const string S5_Rival = nameof(S5_Rival);
        public const string S6_Diler = nameof(S6_Diler);
        public const string S7_Holding = nameof(S7_Holding);
        public const string S8_Client = nameof(S8_Client);
        public const string S9_Meeting = nameof(S9_Meeting);
        public const string S10_EkspositorOrder = nameof(S10_EkspositorOrder);
        public const string S11_Report = nameof(S11_Report);
        public const string S12_Units = nameof(S12_Units);
        public const string S13_Grupa = nameof(S13_Grupa);
        public const string S14_Product = nameof(S14_Product);
        public const string S15_Monitoring = nameof(S15_Monitoring);
        public const string S16_ClientTypology2 = nameof(S16_ClientTypology2);
                      

    }
}
