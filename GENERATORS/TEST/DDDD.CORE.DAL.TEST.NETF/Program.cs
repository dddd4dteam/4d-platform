﻿using System;
using System.Reflection;
using System.Security.Cryptography;
using DDDD.Core.App;
using DDDD.Core.ComponentModel;
using DDDD.Core.Data.DA2;
using DDDD.Core.Data.DA2.Mapping;
using DDDD.Core.Data.DA2.Query;
using DDDD.Core.Extensions;
using DDDD.Core.Reflection;
using DDDD.CRM.DOM.DAL;

namespace DDDD.CORE.DAL.TEST.NETF
{
    class Program
    {

        static Assembly[] LoadCustomAssemblies() 
        {
            var asms = new Assembly[]
                { typeof(S1_GeographyModel).Assembly };
            return asms;
        }

        static void Main(string[] args)
        {
            try
            {
                AppBootstrapper.Current.LoadAppComponents
            (prepareAppAction: null
            ,useKnownComponents:
               ComponentClassEn.ServiceModels
               | ComponentClassEn.DCManagers
               | ComponentClassEn.DCServices
            , registerCustomComponentClassesFunc: null
            , loadAdditionalAssembliesFunc: LoadCustomAssemblies
            , InitAppModelEn.InitBackground_EmptyApp
            ,  startupAppAction: null
            );

                var query1 = DSelectQuery.SelectAll().TopN(10)
                            .From<V_S1_GeographyItem_Vo>();

                var query1Text = query1.ToSQLString(ProviderKey.Default);

                var connectionString = "Data Source=A3;Initial Catalog=CRM_Cersanit2_test3;Integrated Security=True";


                var item = new V_S1_GeographyItem_Vo();
                var svcModel = (item as PersistableBOBase).GetServiceModel(); 
                 
                //Desc.
                //var tpinfEx = Tps.T_charArray.GetTypeInfoEx();
                var tpinfEx2 = typeof(V_S1_GeographyItem_Vo).GetTypeInfoEx();

                // Get Accessor - only TargetType Members without base class Members
                var accessor = tpinfEx2.GetAccessor(TypeMemberSelectorEn.DefaultNoBase);

                // Get DafultCtor Func
                var defCtorFunc = TypeActivator.TryGetDefCtorFunc(typeof(V_S1_GeographyItem_Vo));

                var targetType = typeof(V_S1_GeographyItem_Vo);

                var defaultCtorSearchBinding = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

                //Get existed default ctors 
                var foundedCtors = targetType.GetConstructors(defaultCtorSearchBinding);
                               
                //Mapping TESTS
                //var m1 = TypeDAMap.GetInit(typeof(V_S1_GeographyItem_Vo), ProviderKey.Default, true);
                
                DBProxy.GetInit<MsSqlDataProvider>(connectionString, IsDefaultProxy: true);

                var loadedList = DBProxy.DefProx.SelectList<V_S1_GeographyItem_Vo>(query1);

            }
            catch (Exception exc)
            {
                throw exc;
            }


        }
    }
}
