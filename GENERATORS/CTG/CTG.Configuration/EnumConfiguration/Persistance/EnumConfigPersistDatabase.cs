﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wintellect.Sterling.Database;

namespace CTG.EnumConfiguration
{
    
    public class EnumConfigPersistDatabase : BaseDatabaseInstance
    {
        public const string DATAINDEX = "IndexData";

        public override string Name
        {
            get { return "TestDatabase"; }
        }


        public static string GetCompositeKey(EnumParameter testClass)
        {
            if (testClass == null)
                return string.Empty;

            return string.Format("{0}", testClass.GlobalKey);
        }


        protected override List<ITableDefinition> RegisterTables()
        {

            var EnumParameterTable =  CreateTableDefinition<EnumParameter, Int32>(t => t.GlobalID);


            return new List<ITableDefinition>
                    {
                          CreateTableDefinition<EnumParameter, Int32>(t => t.GlobalID)
                        .WithIndex<EnumParameter,Type,Int32>("ParentEnumType", t=>t.ParentEnumType ) // ConvertedType
                        .WithIndex<EnumParameter,Type,Int32>("FinalType", t=>t.ParamType ) // ConvertedType

                        //CreateTableDefinition<TestModel, int>(testModel => testModel.Key)
                        //    .WithIndex<TestModel, string, int>(DATAINDEX, t => t.Data)
                        //    .WithIndex<TestModel, DateTime, string, int>("IndexDateData",
                        //                                                t => Tuple.Create(t.Date, t.Data)),
                        //CreateTableDefinition<TestAggregateModel, string>(t => t.Key),
                        //CreateTableDefinition<TestAggregateListModel, int>(t => t.ID), 
                        //CreateTableDefinition<TestListModel, int>(t => t.ID),
                        //CreateTableDefinition<TestDerivedClassAModel, Guid>(t => t.Key),
                        //CreateTableDefinition<TestDerivedClassBModel, Guid>(t => t.Key),
                        //CreateTableDefinition<TestClassWithArray, int>(t => t.ID),
                        //CreateTableDefinition<TestClassWithStruct, int>(t => t.ID),
                        //CreateTableDefinition<TestClassWithDictionary, int>(t => t.ID),
                        //CreateTableDefinition<TestCompositeClass, string>(GetCompositeKey),
                        //CreateTableDefinition<TestModelAsListModel, int>(t=>t.Id)
                        
                      
                           
                    };
        }
    }    
}
