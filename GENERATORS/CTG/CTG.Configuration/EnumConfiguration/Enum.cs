﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.EnumConfiguration
{



    /// <summary>
    /// Param's of What Kind we will use.
    /// First-  parameters will defined by default in enum Attributes.
    /// Second- parameters can be reset from some other source.  
    /// </summary>
    public enum ParamKindEn
    {
        /// <summary>
        /// not correct as usually parameter kind
        /// </summary>
        NotDefined,
        /// <summary>
        /// Value of Class. We set 1-class type and 2-class value
        /// </summary>
        Class,

        /// <summary>
        /// Value of valuable type(Int32). We set 1- struct type and 2-struct value
        /// </summary>
        Struct,

        /// <summary>
        /// Value of some enum. We set 1-enum type and 2-enum value    
        /// </summary>
        Enum,
                

        /// <summary>
        /// Value of valuable/pointer string type
        /// </summary>
        String,

        /// <summary>
        /// Value is some Type. We set: 
        /// 1 way: only type as typeof(SomeContract).  Here Type is 1-type and is 2-value
        /// 2 way: string FullTypeName as String value. Here we don't know the Type value 
        /// </summary>
        Type,


        /// <summary>
        /// Delegate for some method of static class
        /// </summary>
        Delegate,

        /// <summary>
        /// Value of xml string. We set 1- xml contract type and 2-serialized xml string( also may be packed) value
        /// </summary>
        Xml,

        /// <summary>
        /// Reference to a xml file. to Load Xml Type by lazy way. 
        /// </summary>
        XmlRef

    }






}
