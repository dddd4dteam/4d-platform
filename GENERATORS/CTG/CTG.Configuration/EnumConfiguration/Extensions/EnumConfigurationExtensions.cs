﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CTG.EnumConfiguration
{
    public static class EnumConfigurationExtensions
    {




        #region  ----------------------------------- IsConfigEnum( ------------------------------------------
        
        /// <summary>
        /// If type of this enum is COnfiguration enum type- i.e. has ConfigEnumAttribute 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ConfigEnumParam"></param>
        /// <returns></returns>
        public static bool IsConfigEnum(this Enum EnumValue)
        {
            return EnumConfigurationEngine.IsConfigEnum(EnumValue);
        }
        
        #endregion ----------------------------------- IsConfigEnum ------------------------------------------


        #region  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------

        /// <summary>
        /// Get Parameter's scalar Value of T type. 
        /// If T is reference type(not string) there will be throw InvalidOperationException 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="paramKey"></param>
        /// <returns></returns>
        public static T GetScalarValue<T>(this Enum ConfigEnumParam)
        {
            //auto load if its correct ConfigEnum value and still was not exist in current dictionary of loaded config enums
            if (ConfigEnumParam.IsConfigEnum() && !EnumConfigurationEngine.ContainsEnum(ConfigEnumParam))
            { EnumConfigurationEngine.LoadConfigEnum(ConfigEnumParam.GetType()); }

            return EnumConfigurationEngine.GetScalarValue<T>(ConfigEnumParam);

        }


        /// <summary>
        ///  Get Parameter's  Value of object type.
        /// </summary>
        /// <param name="paramKey"></param>
        /// <returns></returns>
        public static object GetValue(this Enum ConfigEnumParam)
        {
            if (ConfigEnumParam.IsConfigEnum() && !EnumConfigurationEngine.ContainsEnum(ConfigEnumParam))
            { EnumConfigurationEngine.LoadConfigEnum(ConfigEnumParam.GetType()); }

            return EnumConfigurationEngine.GetValue(ConfigEnumParam);
        }






        /// <summary>
        /// Get Parameter's  Value of object type and set it to ParamValue ref variable
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="ParamValue"></param>
        /// <param name="IsFullEnumItem"></param>
        public static void TryGetValue(this Enum ConfigEnumParam, ref object ParamValue) //, bool IsFullEnumItem = false
        {
            if (ConfigEnumParam.IsConfigEnum() &&   !EnumConfigurationEngine.ContainsEnum(ConfigEnumParam))
            { EnumConfigurationEngine.LoadConfigEnum(ConfigEnumParam.GetType()); }


            EnumConfigurationEngine.TryGetValue(ConfigEnumParam, ref ParamValue);
        }


        /// <summary>
        ///  Get Parameter's  Value of object type and set it to ParamValue ref variable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="paramKey"></param>
        /// <param name="ParamValue"></param>
        /// <param name="IsFullEnumItem"></param>
        public static void TryGetValue<T>(this Enum ConfigEnumParam, ref T ParamValue)//, bool IsFullEnumItem = false
        {
            TryGetValue(ConfigEnumParam, ref ParamValue);
        }


        #endregion  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------


        #region  ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------


        /// <summary>
        /// Trying to update Parameter Value- if there is no such key in EnumConfigurationEngine.Params so we won't be able to update anything.
        /// For Parameter wich Value we are going to Update we need [ParamEnumType.Param_Field] to get ConfigKey(long). 
        /// Then if the Key exist we will change current Value into  NewValue.        
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="enumItemNewValue"></param>
        /// <param name="IsFullEnumItem"></param>
        public static void UpdateValue(this Enum ConfigEnumParam, object enumItemNewValue)
        {
            if (ConfigEnumParam.IsConfigEnum() &&  !EnumConfigurationEngine.ContainsEnum(ConfigEnumParam))
            { EnumConfigurationEngine.LoadConfigEnum(ConfigEnumParam.GetType()); }

            EnumConfigurationEngine.TryUpdateValue(ConfigEnumParam, enumItemNewValue);
        }

        #endregion ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------
        

      
    }
}
