﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.ComponentModel;
using System.Windows;
using Wintellect.Sterling;
using System.Diagnostics;
using CTG.Events;
using System.Linq.Expressions;

namespace CTG.EnumConfiguration
{


    /// <summary>
    /// 
    /// </summary>
    public class EnumConfigurationEngine:INotifyPropertyChanged
    {
	  
        #region ------------ Singleton  Constructor -----------------------
     
	 // PARAMS :     
     // Class   -         EnumConfigurationEngine            EX: SystemDispatcher - Singleton Class Name           
     // ClassDesc  -      Enum COnfiguration Engine Description        EX: Диспетчер системы 
	 	 
     /// <summary>
     ///  Target Class - EnumConfigurationEngine -  Ctor
     /// </summary>
     EnumConfigurationEngine( )
     {  
         //  Initializing  Target Class   
         Initialize();

     }

	 /// <summary>
     /// Singleton Getting Property.
     /// Enum COnfiguration Engine Description
     /// </summary>     
     public static EnumConfigurationEngine Current
     {
        get
        {
            return EnumConfigurationEngineCreator.instance;
        }
     }
    
	 /// <summary>
     /// Creator Class - who instantiating Singleton object 
     /// </summary>
     class EnumConfigurationEngineCreator
     { 
        /// <summary>
        /// Explicit static constructor to tell C# compiler
        /// not to mark type as beforefieldinit
        /// </summary>
        static EnumConfigurationEngineCreator()
        {
        }

		/// <summary>
        /// Single target class instance
        /// </summary>
        internal static readonly EnumConfigurationEngine instance = new EnumConfigurationEngine();
     }

    #endregion   ----------- Singleton  Constructor -----------------------    
    

        #region  ------------------------ CONST -----------------------------

        
        internal const String ParamKeyPattern = @"\[([A-Za-z0-9]+)\]|\{([A-Za-z0-9]+)\}";        
        const string ParamNotContains = "ParamNotContains";    


        #endregion ------------------------ CONST -----------------------------
        

        #region ------------------------------------ FIELDS -----------------------------------
        
        private static readonly object _lockObject = new object();
        
        #endregion ------------------------------------ FIELDS -----------------------------------


        #region ------------------------------INotifyPropertyChanged--------------------------------
        
        PropertyObserver<EnumConfigurationEngine> _observer;

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged<T>(Expression<Func<T>> property)
        {
            var propertyInfo = ((MemberExpression)property.Body).Member as PropertyInfo;
            if (propertyInfo == null)
            {
                throw new ArgumentException("The lambda expression 'property' should point to a valid Property");
            }
                       

            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyInfo.Name));
        }


        #endregion ------------------------------INotifyPropertyChanged--------------------------------


        #region -------------------------------- INITIALIZATION ---------------------------------

        /// <summary>
        /// not exist in Params 
        /// </summary>
        /// <param name="initParams"></param>
        protected virtual void Initialize()
        {
            Params = new Dictionary<long, EnumParameter>();
            ConfigEnums = new Dictionary<Int32, Type>();
            ParsedAssemblies = new List<String>();
            ParameterKeyParts = new Dictionary<string, Func<Type, FieldInfo, String>>();
            

            PersistEngine = new SterlingEngine();

            if (Debugger.IsAttached)
            {
                Logger = new SterlingDefaultLogger(SterlingLogLevel.Verbose);
            }

            //PersistEngine.SterlingDatabase.RegisterSerializer<MySerializer>();
            
            PersistEngine.Activate();
            PersistDatabase = PersistEngine.SterlingDatabase.RegisterDatabase<EnumConfigPersistDatabase>();
            
            _observer = new PropertyObserver<EnumConfigurationEngine>(this)
                        .RegisterHandler(n => n.IsInitialized, OnInitializedHandler);
            
            InitParameterKeyParts();

            if (IsInitialized == false)
            {   IsInitialized = true;     }

        }

      

        #endregion -------------------------------- INITIALIZATION ----------------------------------

        
        #region ------------------NOTIFIABLE MODEL ITEM:  IsInitialized------------------------------

        // ItemName - IsInitialized  -  Ex: Client 
        // Contract - bool  -  Ex: V_S8_Client_Vo

        bool _IsInitialized = new bool();

        /// <summary>
        /// Инициализирован ли даннный экземпляр 
        /// </summary>
        public bool IsInitialized
        {
            get { return _IsInitialized; }
            set
            {
                _IsInitialized = value; //Field changing
                OnPropertyChanged(() => IsInitialized);
            }
        }

        private static void OnInitializedHandler(EnumConfigurationEngine engine)
        {
            //TO DO

        }

        #endregion ------------------NOTIFIABLE MODEL ITEM: IsInitialised------------------------------            
        

        #region ---------------------SYNTAX KEY- indexator -------------------

        /// <summary>
        /// Getting EnumParameter by GlobalID  of Enum.Field 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public EnumParameter this[Int32 key]
        {
            get
            {
                if (Params.ContainsKey(key))
                    return Params[key];
                else
                    return null;
            }
        }

        /// <summary>
        /// Getting EnumParameter from Configuration Engine
        /// </summary>
        /// <param name="EnumKey"></param>
        /// <returns></returns>
        public EnumParameter this[Enum EnumKey]
        {
            get
            {
                Int32? BuildedKey = GetEnumValueGlobalID(EnumKey);

                if (BuildedKey != null && Params.ContainsKey(BuildedKey.Value))
                {
                    return Params[BuildedKey.Value];
                }
                else return null;
            }
        }


        #endregion --------------------- KEY indexator -------------------


        #region -------------------- PROPERTIES ------------------------


        /// <summary>
        /// Configuration Parameters
        /// </summary>
        public  Dictionary<long, EnumParameter> Params 
        {
            get;
            protected set;
        }


        /// <summary>
        /// Configuration Parameters
        /// </summary>
        public  Dictionary<Int32, Type> ConfigEnums 
        {
            get;
            protected set;
        }


        /// <summary>
        /// Assemblies that was Scanned for Configuration Enum types
        /// </summary>
        public  List<String> ParsedAssemblies
        {
            get;
            private set;
        }

        

        #region ----------------------------------  PERSISTANCE (STERLING) -----------------------------------

        /// <summary>
        /// Sterling Engine to store some parameters in the Windows Application Isolated Storage
        /// </summary>
        private SterlingEngine PersistEngine;

        /// <summary>
        /// Parameters Database to save/load configuration parameters
        /// </summary>
        public ISterlingDatabaseInstance PersistDatabase { get; private set; }


        /// <summary>
        /// Default Logger for SterlingEngine
        /// </summary>
        private SterlingDefaultLogger Logger;


       
        #endregion ---------------------------------- PERSISTANCE (STERLING) -----------------------------------




        #endregion -------------------- PROPERTIES ------------------------


        #region  ------------------------  METHODS ---------------------------




        #region ------------------------------ IS CONFIG ENUM ------------------------------------


        /// <summary>
        /// 
        /// </summary>
        /// <param name="enumType"></param>
        /// <returns></returns>
        public static bool IsConfigEnum(Type enumType)
        {
            return (enumType.IsEnum && enumType.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault() != null);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool IsConfigEnum<T>()
        {
            return IsConfigEnum(typeof(T));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        public static bool IsConfigEnum(Enum EnumValue)
        {
            return IsConfigEnum(EnumValue.GetType());
        }


        #endregion ------------------------------ IS CONFIG ENUM ------------------------------------



        #region  ------------------------------- EnumParameter Key  -----------------------------------


        const String EnumNameKnownConst = "EnumName";
        const String EnumNamespaceKnownConst = "EnumNamespace";
        const String EnumItemNameKnownConst = "EnumItemName";
        const String EnumParameterMaskKey = "{EnumNamespace}{EnumName}{EnumItemName}";



        /// <summary>
        /// {ItemName} - where ItemName - is EnumItem - constant word that will be replaced on Building Param Unique Key(String) 
        /// </summary>
        public Dictionary<string, Func<Type, FieldInfo, String>> ParameterKeyParts
        {
            get;
            private set;
        }



        /// <summary>
        /// Init  check known replacement Consts by default 
        /// </summary>
        /// <returns></returns>
        private Dictionary<String, Func<Type, FieldInfo, String>> InitParameterKeyParts()
        {

            Func<Type, FieldInfo, String> EnumNameReplaceFunc = (t, f) =>
            {
                return t.Name; // EnumName   
            };

            Func<Type, FieldInfo, String> EnumItemNameReplaceFunc = (t, f) =>
            {
                return f.Name; // EnumItemName   
            };

            Func<Type, FieldInfo, String> EnumNamespaceReplaceFunc = (t, f) =>
            {
                return t.Namespace; // EnumNamespace   
            };

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumNameKnownConst, EnumNameReplaceFunc);

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumItemNameKnownConst, EnumItemNameReplaceFunc);// 

            //Marker| GetOutputType
            ParameterKeyParts.Add(EnumNamespaceKnownConst, EnumNamespaceReplaceFunc);// 

            return ParameterKeyParts;
        }


       


        /// <summary>
        /// Collect end of Enum and EnumItem conversions
        /// </summary>
        /// <param name="EnumType"></param>
        /// <param name="field"></param>
        /// <returns></returns> 
        private static Dictionary<String, String> CollectEnumValueKeyParts( FieldInfo field)
        {
            Dictionary<String, String> replacements = new Dictionary<String, String>();

            foreach (var item in Current.ParameterKeyParts)
            {
                replacements.Add(item.Key, item.Value(field.DeclaringType, field));
            }

            return replacements;
        }


        /// <summary>
        /// Building EnumParameter  GlobalKey
        /// </summary>
        /// <param name="ConfigEnumType"></param>
        /// <param name="enumField"></param>
        /// <returns></returns>
        private static String BuildEnumValueGlobalKey( FieldInfo EnumField)
        {           
            string FinalGlobalKey = EnumParameterMaskKey.ReplaceManyRe( CollectEnumValueKeyParts( EnumField) );

            return FinalGlobalKey;
        }


        /// <summary>
        /// Getting Enum EnumValue GlobalID - to get target EnumParameter Content 
        /// </summary>
        /// <param name="EnumField"></param>
        /// <returns></returns>
        internal static Int32 GetEnumValueGlobalID(FieldInfo EnumField)
        {
            ConfigEnumAttribute configEnumInfo = EnumField.DeclaringType.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault();

            Contract.Assert(configEnumInfo != null, "This enum type is not Configuration Enum and cannot be use to GetGlobalID of it's value. Mark your enum type as ConfigEnum and set parameters for each field. ");

            String resultMaskString = BuildEnumValueGlobalKey(EnumField);

            //If we could Built Param Key
            return resultMaskString.GetHashCode();
        }

       /// <summary>
       /// Getting Enum EnumValue GlobalID - to get target EnumParameter Content 
       /// </summary>
        /// <param name="EnumValue"></param>
       /// <returns></returns>
        internal static Int32 GetEnumValueGlobalID(Enum EnumValue)
        {
           return GetEnumValueGlobalID(EnumValue.GetFieldInfo());            
            
        }

        #endregion ------------------------------- EnumParameter Key  ---------------------------------


        #region  ----------------------------------- BUILD EnumParameter -------------------------------------



        
        static void ValidateEnumParameterDeclaration( String EnumFieldName,EnumParamAttribute paramInfo)
        {       

            Contract.Assert(paramInfo.Kind != ParamKindEn.NotDefined, String.Format( "EnumParameter Declaration(EnumParameterAttribute)  of {0}     has impossible Kind value", EnumFieldName ));

            Contract.Assert(paramInfo.ParamType != null, String.Format( "EnumParameter Declaration(EnumParameterAttribute)  of {0}    has impossible ParamType value", EnumFieldName ));

        }
        


        /// <summary>
        /// Building configuration EnumParameter  from EnumParamAttribute
        /// </summary>
        /// <param name="ConfigEnum"></param>
        /// <param name="Field"></param>
        /// <param name="paramInfo"></param>
        /// <returns></returns>
        private static EnumParameter BuildConfigParam( FieldInfo Field) //Type ParentEnumConfig, String EnumValueKey
        {

            var paramInfo = Field.GetFieldAttribute<EnumParamAttribute>().FirstOrDefault();
            Contract.Assert(paramInfo != null, String.Format("Field {0} doesn't contain parameter Declaration", Field.Name));

            //base InitContent
            paramInfo.InitContent();
                
           
            // Validate EnumParameter Info- EnumParamAttribute
            ValidateEnumParameterDeclaration(Field.DeclaringType.Name + "." + Field.Name, paramInfo);
                        

            var newParameter = new EnumParameter()
            {
                //Typology
                Kind = paramInfo.Kind,
                ParamType = paramInfo.ParamType,

                //Parent Enum.Field  info
                ParentEnumType = Field.DeclaringType,
                EnumValueKey = Field.DeclaringType.Name + "." + Field.Name,
                
                //identification                
                GlobalKey = BuildEnumValueGlobalKey(Field),
                
                // load Content by lazy way
                IsLazyLoad = paramInfo.IsLazyLoad,
                LoadPath = paramInfo.LoadPath,
                
                //persisting in the store (Sterling) after first load of Enum
                IsPersistable = paramInfo.IsPersistable,
                                              
                ValidationLevel = paramInfo.ValidationLevel,
                              
                //filter fields
                GroupFilter = paramInfo.GroupFilter,
                SubGroupFilter = paramInfo.SubGroupFilter,
                
                //localization
                LocalNameKey = paramInfo.LocalNameKey,
                LocalDescriptionKey = paramInfo.LocalDescriptionKey
            };
            //set Contents
            newParameter.UpdateContentOnly(paramInfo.Content);
            newParameter.UpdateTagOnly(paramInfo.Tag);


            
            return newParameter;
        }



        


        #endregion ----------------------------------- BUILD EnumParameter -------------------------------------


        #region  --------------------------- LOAD ASSEMBLY CONFIGS & PARAMS ------------------------------

        /// <summary>
        /// Loading Base Assembly(EnumConfigurationEngine class assembly) configs
        /// </summary>
        protected static void LoadAssemblyConfigs()
        {
#if CLIENT          

          EnumConfigurationEngine.LoadConfigEnum<TaskProcessOptionsEn>();
          EnumConfigurationEngine.LoadConfigEnum<EnvironmentParamsEn>();
            
          //Current assembly config  - ConfigManager state configuration
          
#elif SERVER
            LoadAssemblyConfigs(typeof(EnumConfigurationEngine).Assembly);            
#endif

        }



#if SERVER

 /// <summary>
/// Loading Base Assembly(ConfigManagerBase class assembly)  configs
/// </summary>
public static void LoadAssemblyConfigs(Assembly asmbly)
{
    Contract.Requires(asmbly != null, " Assembly cannot be null On Loading  it's Configurations ");

    Type[] tps = asmbly.GetTypes();
    List<Type> enumsTypes = asmbly. GetTypes().ToList();//.Where((t) => t.IsEnum == true).ToList();

    // Get All Config Enums
    List<Type> configenums = asmbly.GetTypes()
        .Where((t) => t.IsEnum == true
                && t.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault() != null
                ).ToList();

    //BuildParams for each Config Enum
    foreach (var configItem in configenums)
    {
        LoadConfigEnum(configItem);
    }
}


         /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        public static void LoadAssemblyConfigs<TEnum>()
        {
         
            // Get All Config Enums
            List<Type> configenums = typeof(TEnum).Assembly.GetTypes()
                .Where((t) => t.IsEnum == true
                      && t.GetTypeAttribute<ConfigEnumAttribute>().FirstOrDefault() != null
                       ).ToList();


            //BuildParams for each Config Enum
            foreach (var configItem in configenums)
            {
                LoadConfigEnum(configItem);
            }

        }


#endif  //---------------------- SERVER -----------------------------


        public static void LoadConfigEnum<TConfigEnum>()
        {
            LoadConfigEnum(typeof(TConfigEnum));
        }


        /// <summary>
        /// Loading Base Assembly(ConfigManagerBase class assembly) configs
        /// </summary>
        internal static void LoadConfigEnum(Type ConfigEnum)
        {
            //get all enum fields
            FieldInfo[] enumItems = ConfigEnum.GetFields();

            //Build Params  Keys  and  Values
            List<EnumParameter> configParams = new List<EnumParameter>();
            foreach (var item in enumItems)
            {
                //try validate and createnew Parameter
                var newParameter = BuildConfigParam(item);

                configParams.Add( newParameter);                                
            }
            
            try
            {
                // Add params to the Global Params Dictionary - 
                foreach (var paramtItem in configParams)
                {
                    Contract.Assume(Current.Params.ContainsKey(paramtItem.GlobalID) == false, " Adding Param Error. Cannot Add existing  Unique Key for " + paramtItem.EnumValueKey);  //ConfigEnum.Name
                    Current.Params.Add(paramtItem.GlobalID, paramtItem);
                }

                // Add Config to the Global Enums Dictionary -   
                Int32 enumHashCode = ConfigEnum.GetHashCode();
                Current.ConfigEnums.Add(enumHashCode, ConfigEnum);

                // Add To list Of Parsed Assemblies
                if (!Current.ParsedAssemblies.Contains(ConfigEnum.Assembly.FullName))
                    Current.ParsedAssemblies.Add(ConfigEnum.Assembly.FullName);

            }
            catch (System.Exception ex)
            {
                // Log about config loading Error 
            }
        }


        #endregion --------------------------- LOAD CONFIGS & PARAMS ------------------------------


        #region ----------------------------- CONTAINS PARAMS -----------------------------


        /// <summary>
        /// If EnumConfigurationEngine Already Contains/Parsed  ConfigurationEnum 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsEnum(Enum EnumValue)
        {
           
            { EnumConfigurationEngine.LoadConfigEnum(EnumValue.GetType()); }

            return (Current.ConfigEnums.Values.Where(cfgen => cfgen == EnumValue.GetType()).FirstOrDefault() != null) ? true : false;
        }


        /// <summary>
        ///  Is Config Params Contains Value for EnumKey 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ContainsParam(Enum EnumValue)
        {
            Int32? UniqueKey = GetEnumValueGlobalID(EnumValue);

            if (UniqueKey == null) return false;//Enum but not not ConfigurationEnum

            return Current.Params.ContainsKey(UniqueKey.Value);
        }

        #endregion ----------------------------- CONTAINS PARAMS -----------------------------



        #region ---------------------------- GETTING PARAM INFO ------------------------------------

        /// <summary>
        /// Get Enum parameter Info.
        /// </summary>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        private static EnumParamAttribute GetParamInfo(Enum EnumValue)
        {
            Attribute paramInfoAttribute =  Attribute.GetCustomAttribute(EnumValue.GetFieldInfo(), typeof(EnumParamAttribute));
            if (paramInfoAttribute == null) return null;

            return paramInfoAttribute as EnumParamAttribute;
        }

        static bool IsScalarParam(EnumParamAttribute paramDeclaration)
        {
            return (paramDeclaration.Kind == ParamKindEn.Struct || paramDeclaration.Kind == ParamKindEn.String || paramDeclaration.Kind == ParamKindEn.Enum);

        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="value"></param>
        ///// <returns></returns>
        //private static Type GetParamType(Enum EnumValue)
        //{
        //    ParamAttribute paramInfo = GetParamInfo(value);
        //    if (paramInfo == null) return null;

        //    Contract.Assume(paramInfo.FinalType != null, value.GetType() + " Parameter.FinalType must be declared before.");

        //    return paramInfo.FinalType;
        //}


        #endregion ---------------------------- GETTING PARAM INFO ------------------------------------


        #region  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------

        /// <summary>
        /// Get Parameter's scalar Value of T type. 
        /// EnumParameter.ParamType and (T) should be Equal.
        /// We simply get parameter from Memory and then cast EnumParameter.Content value to determined T form.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="EnumValue"></param>
        /// <returns></returns>
        internal static T GetScalarValue<T>(Enum EnumValue)
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload

            EnumParamAttribute paramInfo = GetParamInfo(EnumValue);
            Contract.Assert(paramInfo.ParamType != typeof(T),"GetScalarParamValue - (T) type  can't be different than declared ParamType ");
            Contract.Assert(IsScalarParam( paramInfo), "GetScalarParamValue can be done only for Scalar Params - i.e. for params declared as Struct/String/Enum  Kind");
            
            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);
                
            // T or ParamType -Nullable  && Content - null -->  default(T)
            // T or ParamType - (Nullable || !Nullabe)  && Content - !null --> return (T)
            // T or ParamType -!Nullable && Content - null -> error - cannot be so. BaseInit for EnumParam exclude this case.
            if (Current.Params[GlobalID].Content != null) return (T)Current.Params[GlobalID].Content;
            //if (Current.Params[GlobalID].ParamType.IsNullableType() &&  Current.Params[GlobalID].Content == null  ) return default(T);
            return default(T);
        }
        
      

        ///// <summary>
        ///// Get Parameter's  Value of object type.
        ///// </summary>
        ///// <param name="UniqueKey"></param>
        ///// <returns></returns>
        //private static object GetValue(Int32 GlobalID)
        //{
        //    return Current.Params[GlobalID].Content;          
        //}


        /// <summary>
        ///  Get Parameter's  Value of object type.
        /// </summary>
        /// <param name="paramKey"></param>
        /// <returns></returns>
        internal static object GetValue(Enum EnumValue)
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload
        
            Int32 GlobalID = GetEnumValueGlobalID( EnumValue );
            return Current.Params[GlobalID].Content;
        }


        /// <summary>
        /// Get Parameter's  Value of object type and set it to ParamValue ref variable.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="EnumValue"></param>
        /// <param name="ParamValue"></param>     
        internal static void TryGetValue<T>(Enum EnumValue, ref T ParamValue)//, bool IsFullEnumItem = false
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload
           
            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);

            if (Current.Params[GlobalID].Content != null) ParamValue = (T)Current.Params[GlobalID].Content;
            
            ParamValue = default(T);
        }



        /// <summary>
        /// Get Parameter's  Value of object type and set it to ParamValue ref variable
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="ParamValue"></param>
        /// <param name="IsFullEnumItem"></param>
        internal static void TryGetValue(Enum EnumValue, ref object ParamValue) //, bool IsFullEnumItem = false
        {
            //public interface only from Extension- it excludes that paramInfo can be null. i.e. auto preload exclude such case
            //here we have that parameter 100% was added to the COnfiguration dictionary - in auto preload
                        
            Int32 GlobalID = GetEnumValueGlobalID(EnumValue);

            if (Current.Params[GlobalID].Content != null) ParamValue = Current.Params[GlobalID].Content;

            ParamValue = Current.Params[GlobalID].Content.ConvertToValue(Current.Params[GlobalID].ParamType, null); 
                      
        }



        #endregion  -------------------------- GET & TRYGET [PARAM] VALUES ----------------------------------


        #region  ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------


        /// <summary>
        /// Trying to update Parameter Value.         
        /// If new value will succesfully Validated current Content Value will be chaged to NewValue.        
        /// </summary>
        /// <param name="ConfigEnum"></param>
        /// <param name="itemValue"></param>
        /// <param name="enumItemNewValue"></param>
        /// <param name="IsFullEnumItem"></param>
        public static void TryUpdateValue( FieldInfo itemValue, object enumItemNewValue)
        {
            Int32 GlobalID = GetEnumValueGlobalID( itemValue );

            Current.Params[GlobalID].ValidateNewContent(enumItemNewValue);

            Current.Params[GlobalID].UpdateContentOnly(enumItemNewValue);

        }



        /// <summary>
        /// Trying to update Parameter Value.         
        /// If new value will succesfully Validated current Content Value will be chaged to NewValue.        
        /// </summary>
        /// <param name="paramKey"></param>
        /// <param name="enumItemNewValue"></param>        
        public static void TryUpdateValue(Enum paramKey, object enumItemNewValue)
        {
            TryUpdateValue(paramKey.GetFieldInfo(), enumItemNewValue);

        }
        
        #endregion ------------------------------- UPDATING [PARAMS] VALUES -------------------------------------------




        #region  ------------------------------- Multiple Enum and Params Updating ----------------------------------

        /// <summary>
        /// Updating Parameter Values in such way:  EnumConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum) config enum, and nothing more.  
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="UpdateSource"></param>
        internal static void UpdateConfigValues<TEnum>(IDictionary<string, string> UpdateSource) //override
        {
            Contract.Requires(UpdateSource != null, "ConfigManager.LoadInitParams() initParams cannot be null");

            //need to add auto preload OPTION -!!!!

            Type ConfigEnum = typeof(TEnum);
            // ConfigEnum with this type loaded
            if (!Current.ConfigEnums.ContainsKey(ConfigEnum.GetHashCode()))
            {
                return;
            }

            foreach (var item in ConfigEnum.GetFields())
            {
                if (UpdateSource.ContainsKey(item.Name))
                {
                    TryUpdateValue(item, UpdateSource[item.Name]);
                }
            }

        }

        /// <summary>
        /// Updating Parameter Values in such way:  EnumConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1>(UpdateSource);
            UpdateConfigValues<TEnum2>(UpdateSource);
        }


        /// <summary>
        /// Updating Parameter Values in such way:  EnumConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3)  config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2>(UpdateSource);
            UpdateConfigValues<TEnum3>(UpdateSource);
        }


        /// <summary>
        /// Updating Parameter Values in such way:  EnumConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2, TEnum3>(UpdateSource);
            UpdateConfigValues<TEnum4>(UpdateSource);
        }


        /// <summary>
        ///  Updating Parameter Values in such way:  EnumConfigurationEngine.Params[ (TEnum).(UpdateSource.Key) ] =   UpdateSource[UpdateSource.Key].Value.
        /// We using TryUpdateValue method to do this update from Source.
        /// Here We trying to update only fields for (TEnum1) &  (TEnum2) & (TEnum3) & (TEnum4) & (TEnum5) config enum, and nothing more.
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <typeparam name="TEnum5"></typeparam>
        /// <param name="UpdateSource"></param>
        public static void UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(IDictionary<string, string> UpdateSource) //override
        {
            UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(UpdateSource);
            UpdateConfigValues<TEnum5>(UpdateSource);
        }


        #endregion ------------------------------- Multiple Enum and Params Updating ----------------------------------


        #region  ---------------------------------- Init AppConfiguration ------------------------------------

#if CLIENT


        /// <summary>
        /// Initializing EnumConfigurationEngine  - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration(object sender, StartupEventArgs e)
        {
            //1 load Current assembly enum Configurations
            EnumConfigurationEngine.LoadAssemblyConfigs();                

        }


        /// <summary>
        /// Initializing EnumConfigurationEngine - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum>( UpdateSource = e.InitParams ). 
        /// </summary>
        /// <typeparam name="TEnum"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum>(object sender, StartupEventArgs e )
        {
            //1 load Current assembly enum Configurations
            EnumConfigurationEngine.LoadAssemblyConfigs();        

            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            EnumConfigurationEngine.LoadConfigEnum<TEnum>();// AssemblyConfigs<TEnum>();

            //2 Updating Loaded Params
            UpdateConfigValues<TEnum>(e.InitParams); //CommunicationParamsEn  from Server Params Source          
            
            UpdateClientEnvironment();            
            
            if (EnumConfigurationEngine.Initialized != null)
            { Initialized(null, new EventArgs()); }
   
        }



        private static void  UpdateClientEnvironment()
        {
            if (EnumConfigurationEngine.ConfigEnums.ContainsValue(typeof(EnvironmentParamsEn)) )
            {
                EnumConfigurationEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri, HtmlPage.Document.DocumentUri.ToString());
                EnumConfigurationEngine.TryUpdateValue(EnvironmentParamsEn.DocumentUri_OriginalString, HtmlPage.Document.DocumentUri.OriginalString);
            }
        }



        /// <summary>
        /// Initializing EnumConfigurationEngine - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2>( UpdateSource = e.InitParams ). 
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2>(object sender, StartupEventArgs e)        
        {  
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1>(sender, e);            
            EnumConfigurationEngine.LoadConfigEnum<TEnum2>();

            // load the assembly into our app domain
            
            //2 Updating Loaded Params
            UpdateConfigValues<TEnum1,TEnum2>(e.InitParams); //CommunicationParamsEn            
        }


        /// <summary>
        /// Initializing EnumConfigurationEngine - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3>(sender, e);            
            EnumConfigurationEngine.LoadConfigEnum<TEnum3>();

            UpdateConfigValues<TEnum1, TEnum2, TEnum3>(e.InitParams); //CommunicationParamsEn            
        }


        /// <summary>
        /// Initializing EnumConfigurationEngine - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3,TEnum4>(sender, e);
            EnumConfigurationEngine.LoadConfigEnum<TEnum4>();

            UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4>(e.InitParams); //CommunicationParamsEn            
        }

        /// <summary>
        /// Initializing EnumConfigurationEngine - Loading all Configuration Enums from the  EnumConfigurationEngine type assembly into EnumConfigurationEngine.Params. 
        /// Then we Updating Params from e.InitParams by UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>( UpdateSource = e.InitParams ).
        /// </summary>
        /// <typeparam name="TEnum1"></typeparam>
        /// <typeparam name="TEnum2"></typeparam>
        /// <typeparam name="TEnum3"></typeparam>
        /// <typeparam name="TEnum4"></typeparam>
        /// <typeparam name="TEnum5"></typeparam>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4, TEnum5>(object sender, StartupEventArgs e)
        {
            //1 load Current assembly   Enum Configurations      
            //2 load all other assemblies Configs by Type[s].Assembly from TEnum_x
            InitAppConfiguration<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(sender, e);
            EnumConfigurationEngine.LoadConfigEnum<TEnum4>();


            UpdateConfigValues<TEnum1, TEnum2, TEnum3, TEnum4,TEnum5>(e.InitParams); //CommunicationParamsEn            
        }

#elif SERVER


#endif



        #endregion ---------------------------------- Init AppConfiguration ------------------------------------


        #region  ------------------------ DELEGATE PARAMS ---------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="paramKey"></param>
        /// <param name="paramsValues"></param>
        /// <returns></returns>
        internal static TResult InvokeStaticDelegateParam<TResult>(Enum paramKey, object[] paramsValues)
        {
            //[Param(typeof(Func< Func<Task>, HttpLoader, bool, Task>), ParamKindEn.Delegate, typeof(Core.Communication.CommunicationService), "HttpQueryWrapper")] 
            //Key - HttpPostQueryWrapper,
            //FinalType -  Func< Func<Task>, HttpLoader, bool, Task>
            //value - typeof(Core.Communication.CommunicationService)
            //supportValue - "HttpQueryWrapper"

            EnumParamAttribute paramInfo = GetParamInfo(paramKey);
            Contract.Assume(paramInfo != null, "Params with such Key does not exist in Params");
            Contract.Assume(paramInfo.Kind != ParamKindEn.Delegate, "Not Delegate Params can't be Called");
            Contract.Assume((paramInfo.Content as Type) != null, "paramInfo.Value must be Type contract");

            MethodInfo methodToCall = (paramInfo.Content as Type).GetMethod(paramInfo.Tag.ToString(), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static);
            Contract.Assume(methodToCall != null,
                String.Format("Param with Key{0} was not set correctly to get method:{1} of type:{2} delegate", paramKey.ToString(), paramInfo.Tag, (paramInfo.Content as Type).Name));

            return (TResult)methodToCall.Invoke(null, paramsValues);

        }


        #endregion ------------------------ DELEGATE PARAMS ---------------------------------





        #endregion ------------------------  METHODS ---------------------------


    }
    
}
