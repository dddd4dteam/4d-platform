﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace CTG.EnumConfiguration
{

    /// <summary>
    /// Локализованные ресурсы по одной Культуре - Названия и Описания EnumParameter-а 
    /// </summary>
    internal class EnumParamCultureLocales
    {

#region     ------------------------ Culture Names -----------------------------

        /// <summary>
        /// Английское название культуры. Может быть использовано для создания объекта культуры.
        /// </summary>
        public String CultureEnglishName
        {
            get;
            set;
        }
        
        /// <summary>
        /// Название для отображения культуры
        /// </summary>
        public String CultureDisplayName
        {
            get;
            set;
        }



#endregion  ------------------------ Culture Names -----------------------------
        
        /// <summary>
        /// Содержатся ли ресурсы для этой культуры - чтоб не обращаться каждый раз 
        /// </summary>
        public bool IsCultureNotExist
        {
            get;
            set;
        }



        /// <summary>
        ///  Ключ ресурса  Название(EnumParameter)
        /// </summary>
        public String NameKey
        {
            get;
            set;
        }

        /// <summary>
        /// Название(EnumParameter)
        /// </summary>
        public String Name
        {
            get;
            set;
        }
        
        /// <summary>
        /// Ключ ресурса  Описание(EnumParameter)
        /// </summary>
        public  String DescriptionKey
        { 
            get;
            set; 
        }


        /// <summary>
        /// Описание(EnumParameter) 
        /// </summary>
        public String Description
        {
            get;
            set;
        }

    }


}
