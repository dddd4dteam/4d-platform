﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.EnumConfiguration
{



    /// <summary>
    /// Configuration Enum in Some Assembly
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum, AllowMultiple = false)]
    public class ConfigEnumAttribute : Attribute
    {

        // The constructor is called when the attribute is set.
        #region -------------------------CTOR ---------------------------

        /// <summary>
        /// Ctor for configuration Enum attribute
        /// </summary>           
        public ConfigEnumAttribute(  )
        {                      
        }

        #endregion -------------------------CTOR --------------------------


    }

}
