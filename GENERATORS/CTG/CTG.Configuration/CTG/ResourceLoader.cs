﻿using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Windows;
using System.Windows.Resources;


namespace CTG
{

    /// <summary>
    /// 
    /// </summary>
    public class ResourceLoader
    {

        #region  ----------------------- LOADING BINARY RESOURCES -------------------------

        // LoadStringRes
        // LoadStringRes


        /// <summary>
        /// Подгрузить байтовый массив  по Uri
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static byte[] LoadBinaryRes(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            byte[] resBody = null;
            using (var reader = new BinaryReader(resStreamInfo.Stream))
            {
                resBody = reader.ReadBytes((Int32)reader.BaseStream.Length);
            }

            return resBody;
        }


        /// <summary>
        /// Подгрузить байтовый массив  по Uri
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static byte[] LoadBinaryRes(string resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadBinaryRes(resInfo);
        }


        #endregion ----------------------- LOADING BINARY RESOURCES -------------------------
        

        #region  ----------------------- LOADING STRING RESOURCES -------------------------

        // LoadStringRes
        // LoadStringRes


        /// <summary>
        /// Подгрузить Строковый ресурс по Uri
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static String LoadStringRes(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            string resBody = null;
            using (var reader = new StreamReader(resStreamInfo.Stream))
            {
                resBody = reader.ReadToEnd();
            }

            return resBody;
        }


        public static Stream LoadStringStream(Uri resUri)
        {
            Uri resInfo = resUri;

            StreamResourceInfo resStreamInfo = Application.GetResourceStream(resInfo);
            if (resStreamInfo == null) return null;

            return resStreamInfo.Stream;
        }


        public static Stream LoadStringStream(String resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadStringStream(resInfo);
        }


        /// <summary>
        /// Подгрузить Строковый ресурс по Uri
        /// </summary>
        /// <param name="resUri"></param>
        /// <returns></returns>
        public static String LoadStringRes(string resUri)
        {
            Uri resInfo = new Uri(resUri, UriKind.RelativeOrAbsolute);
            return LoadStringRes(resInfo);
        }


        #endregion ----------------------- LOADING STRING RESOURCES -------------------------
        

        #region  ------------------------ LOADING ASSEMBLY ------------------------------





#if CLIENT


        

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TContract"></typeparam>
        /// <returns></returns>
        public static Assembly LoadAssemmbly<TContract>()
        {
            return LoadAssemmbly(typeof(TContract));
        } 


        public static Assembly LoadAssemmbly(Type contract)
        {
            //ArgumentValidator.AssertNotNullOrEmpty(AssemblyUri, "LoadAssembly:  Uri String cannot be null");
            String AssemblyUri = GetTypeModule(contract);
            Uri assemblyUri = new Uri(AssemblyUri, UriKind.Relative);
            return LoadAssemmbly(assemblyUri);

        }


        private static String GetTypeModule(Type contract)
        {
            return contract.Assembly.FullName.Split(',')[0] + ".dll";
        }


        /// <summary>
        /// Подгрузка сборки
        /// </summary>
        /// <param name="AssemblyUri"></param>
        /// <returns></returns>         
        public static Assembly LoadAssemmbly(String AssemblyUri)
        {
            //ArgumentValidator.AssertNotNullOrEmpty(AssemblyUri, "LoadAssembly:  Uri String cannot be null");

            Uri assemblyUri = new Uri(AssemblyUri, UriKind.Relative);
            return LoadAssemmbly(assemblyUri);

        }




        /// <summary>
        /// Подгрузка сборки
        /// </summary>
        /// <param name="AssemblyUri"></param>
        /// <returns></returns> 
        public static Assembly LoadAssemmbly(Uri AssemblyUri)
        {
            Uri assemblyUri = AssemblyUri;

            //ArgumentValidator.AssertNotNull(AssemblyUri, "LoadAssembly:  Uri cannot be null");
           // Assembly pluginAssembly = Assembly.Load(typeof(ConfigurationEngine).Assembly.FullName);

           // Type[] types = pluginAssembly.GetTypes();



            StreamResourceInfo assInfo = Application.GetResourceStream(assemblyUri);
            return new AssemblyPart().Load(assInfo.Stream); // Current.ApplicationLifetimeObjects new AssemblyPart()
        }

#elif SERVER



#endif

        #endregion ------------------------ LOADING ASSEMBLY ------------------------------
        
    }
}
