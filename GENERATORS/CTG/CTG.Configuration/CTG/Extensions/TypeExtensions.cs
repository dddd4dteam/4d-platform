﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.EnumConfiguration
{
    public static class TypeExtensions
    {
       
        /// <summary>
        /// Understand -is this typer is Nullable.  paramType should not be null.
        /// </summary>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public static bool IsNullableType(this Type paramType)
       {
            if (!paramType.IsGenericType)
                return false;

            if (Nullable.GetUnderlyingType(paramType) != null) return true;
            return false;
       }

    }
}
