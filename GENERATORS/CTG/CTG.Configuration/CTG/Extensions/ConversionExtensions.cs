﻿using System;
using System.Diagnostics.Contracts;


namespace CTG
{

    /// <summary>
    /// Расширение Конвертации
    /// </summary>
    public static class ConversionExtensions
    {

        /// <summary>
        /// Only for Simple -Value Types 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        public static T ConvertToValue<T>(this string currentValue, IFormatProvider provider = null)
        //where T: struct
        {
            Contract.Requires(currentValue != null, "object.ConvertToValue<T> currentValue cannot be null");

            if (String.IsNullOrEmpty(currentValue))
            {
                return default(T);
            }

            if (currentValue.GetType() == typeof(T))
                return (T)(currentValue as object);

            return (T)Convert.ChangeType(currentValue, typeof(T), provider);
        }


        /// <summary>        
        /// Only for Simple -Value Types 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentValue"></param>
        /// <returns></returns>
        public static T ConvertToValue<T>(this object currentValue, IFormatProvider provider = null)
        //where T 
        {
            Contract.Requires(currentValue != null, "object.ConvertToValue<T> currentValue cannot be null");


            if (String.IsNullOrEmpty(currentValue.ToString()))
            {
                return default(T);
            }

            if (currentValue.GetType() == typeof(T))
                return (T)currentValue;

            return (T)Convert.ChangeType(currentValue, typeof(T), provider);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="currentValue"></param>
        /// <param name="provider"></param>
        /// <returns></returns>
        public static object ConvertToValue(this object currentValue, Type valueTargetType, IFormatProvider provider = null)
        //where T 
        {
            Contract.Requires(currentValue != null, "object.ConvertToValue currentValue cannot be null");

            if (String.IsNullOrEmpty(currentValue.ToString()))
            {
                return null;
            }

            if (currentValue.GetType() == valueTargetType)
                return currentValue;


            return Convert.ChangeType(currentValue, valueTargetType, provider);

        }





    }
}
