﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

//using NMG.Configuration;


namespace CTG
{

    /// <summary>
    /// Расширение для Перечислений
    /// </summary>
    public static class EnumExtensions
    {

        #region  ----------------------- Enum Extensions ----------------------------------------

        /// <summary>
        /// Значения enum-а в список элементов T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> EnumToList<T>()
            where T : struct
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }


        /// <summary>
        /// Получить Значение Enum  Типа из  Ключевого знеачения типа KeyValuePair 
        /// </summary>
        /// <typeparam name="TEnum">Тип enum-а</typeparam>
        /// <param name="Pair"></param>
        /// <returns>Nullable<TEnum> - ноль либо значение полученного перечисления</returns>
        public static Nullable<TEnum> GetPairKeyAsEnum<TEnum>(this KeyValuePair<String, String>? Pair)
            where TEnum : struct
        {
            if (Pair == null) return null;
            if (Pair.Value.Key == "") return null;

            TEnum tryparsevalue;
            Enum.TryParse<TEnum>(Pair.Value.Key, out tryparsevalue);

            return tryparsevalue;
        }


        /// <summary>
        /// Содержится ли среди значений перечисления  значение с Именем Name
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static bool? ContainsParam(this Type enumValue, String Name)
        {
            if (enumValue == null || Name == null || Name == "") return false;
            if (enumValue.IsEnum == false) return null;

            String[] Names = Enum.GetNames(enumValue);

            return Names.Contains(Name);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());

            DescriptionAttribute attribute
                    = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute))
                        as DescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }


        /// <summary>
        /// Строковое значение поля перечисления
        /// </summary>
        /// <param name="paramKey"></param>
        /// <returns></returns>
        public static String Str(this Enum paramKey)
        {
            return paramKey.ToString();
        }

        /// <summary>
        /// To Lower String Enum.Field value 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToLowerStr(this Enum value)
        {         
            return  value.ToString().ToLower() ;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static FieldInfo GetFieldInfo(this Enum value)
        {
            return value.GetType().GetField(value.ToString());
        }
        

        #endregion----------------------- Enum Extensions ----------------------------------------



    }

}
