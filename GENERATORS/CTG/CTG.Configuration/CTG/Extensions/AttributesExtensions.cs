﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Security;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Collections;

namespace CTG
{

    public static class AttributesExtensions
    {


        /// <summary>
        /// Число элементов у перечисления
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static int Count(this IEnumerable collection)
        {
            try
            {
                if (collection == null)
                { return 0; }

                if ((collection as ICollection) != null)
                {
                    return (collection as ICollection).Count;
                }
                else
                {
                    Int32 count = 0;
                    foreach (var item in collection)
                    { count++; }
                    return count;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

     

        #region   ---------------- BEGIN Attributes Extensions  -----------------

        /// <summary>
        /// Получить необходимый атрибут типа объекта 
        /// </summary>
        /// <param name="typeobject"></param>
        /// <param name="needableAttribute"></param>
        /// <returns></returns>
        public static List<TAttribute> GetTypeAttribute<TAttribute>(this Type objectvalue)
            where TAttribute : System.Attribute
        {
            try
            {
                List<TAttribute> attributeList = new List<TAttribute>();
                foreach (System.Attribute at in System.Attribute.GetCustomAttributes(objectvalue))
                {
                    if (at.GetType().Name == typeof(TAttribute).Name)
                    { attributeList.Add(at as TAttribute); }
                }

                return attributeList;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        /// Получить необходимый аттрибут свойства из его PropertyInfo 
        /// </summary>
        /// <param name="typeobject"></param>
        /// <param name="needableAttribute"></param>
        /// <returns></returns>
        public static List<TAttribute> GetPropertyAttribute<TAttribute>(this PropertyInfo property)
            where TAttribute : System.Attribute
        {
            try
            {
                List<TAttribute> attributeList = new List<TAttribute>();

                foreach (System.Attribute at in property.GetCustomAttributes(true))
                {
                    if (at.GetType().Name == typeof(TAttribute).Name)
                    { attributeList.Add(at as TAttribute); }
                }

                return attributeList;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        /// Получить необходимый аттрибут поля из его  FieldInfo
        /// </summary>
        /// <param name="typeobject"></param>
        /// <param name="needableAttribute"></param>
        /// <returns></returns>       
        public static TAttribute[] GetFieldAttribute<TAttribute>(this FieldInfo field)
            where TAttribute : System.Attribute
        {
            try
            {

                return Task.Factory.StartNew<TAttribute[]>(() =>
                {
                    return (TAttribute[])field.GetCustomAttributes(typeof(TAttribute), false);
                }
                 ).Result;

            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        public static void TryDoCrossThreadingCall(Action action, object state)
        {
            // Get the current SynchronizationContext.
            // NOTE: Must make the call on the UI thread, NOT
            // the background thread to get the proper
            // context.
            SynchronizationContext context = SynchronizationContext.Current;

            // Start some work on a new Task (4.0)
            Task.Factory.StartNew(
                () =>
                {
                    // Do some lengthy operation.
                    //...

                    context.Post(c => action(), state);
                    // Notify the user.  Do not need to wait.
                    //context.Post(o => MessageBox.Show("Progress"));

                    // Do some more stuff.
                    //...

                    // Wait on result.
                    // Notify the user.
                    context.Send(s => action(), state);
                    //context.Send(o => MessageBox.Show("Progress, waiting on OK"));
                }
                  );
        }




        /// <summary>
        /// Получить атрибут метода из его MethodInto
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="method"></param>
        /// <returns></returns>
        public static List<TAttribute> GetMethodAttribute<TAttribute>(this MethodInfo method)
          where TAttribute : System.Attribute
        {
            try
            {
                List<TAttribute> attributeList = new List<TAttribute>();

                foreach (System.Attribute at in method.GetCustomAttributes(true))
                {
                    if (at.GetType().Name == typeof(TAttribute).Name)
                    { attributeList.Add(at as TAttribute); }
                }

                return attributeList;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }



        #endregion  ---------------- BEGIN Attributes  Utilities -----------------

    }
}
