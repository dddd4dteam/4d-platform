﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;


namespace CTG
{
    public static class StringExtensions
    {
        
        /// <summary>
        /// Получить результат в виде массива строк- где одна строчка Стринг билдера делится по строкам  
        /// </summary>
        /// <param name="inputBldr"></param>
        /// <returns></returns>
        public static String[] SplitResult(this StringBuilder inputBldr, char delimeter = '\n')
        {
            Contract.Requires(inputBldr != null, "SplitResult - inputData   String[]  cannot be null");

            String[] arrayresult = inputBldr.ToString().SplitToResultExpression();//'\n');
            return arrayresult;
        }



        /// <summary>
        /// Получить результат в виде массива строк- где одна строчка делится по строкам
        /// </summary>
        /// <param name="templateResultString"></param>
        /// <param name="DelimeterChar"></param>
        /// <param name="exludeEmptyString"></param>
        /// <returns></returns>
        public static String[] SplitToResultExpression(this String templateResultString, Char DelimeterChar = '\n', bool exludeEmptyString = true)
        {
            List<String> endResult = new List<String>();
            String[] splittedArray = templateResultString.Split(DelimeterChar);
            foreach (var item in splittedArray)
            {
                if (String.IsNullOrEmpty(item) == false)
                {
                    endResult.Add(item);
                }
            }

            return endResult.ToArray();
        }



        /// <summary>
        /// Собрать в одну строчку
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static StringBuilder ToOneString(this String[] inputData)
        {
            Contract.Requires(inputData != null, "ToOneString - inputData   String[]  cannot be null");

            StringBuilder strbuilder = new StringBuilder();
            foreach (var item in inputData)
            {
                strbuilder.AppendLine(item);
            }
            return strbuilder;
        }



        public static String Str(this char character)
        {
            return character.ToString();
        }

        /// <summary>
        /// Short String.Format() 
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <param name="inparams"></param>
        /// <returns></returns>
        public static String StrFmt(this String formatMessageMask, params string[] inparams)
        {
            return String.Format(formatMessageMask, inparams);
        }





    }
}
