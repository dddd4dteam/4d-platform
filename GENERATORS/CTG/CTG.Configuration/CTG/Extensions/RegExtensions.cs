﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;


namespace CTG
{
    /// <summary>
    /// 
    /// </summary>
    public static class RegExtensions
    {



        /// <summary>
        /// 
        /// </summary>
        /// <param name="trimSymbols"></param>
        /// <returns></returns>
        public static String CreateTrimPattern(string[] trimSymbols)
        {
            string pattern = "";
            Int32 i = 0;
            foreach (var item in trimSymbols)
            {
                if (i == 0)
                {
                    pattern = @"\" + @item;
                    i++;
                    continue;
                }
                pattern += @"|\" + @item;
                i++;
            }

            return pattern;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="trimSymbols"></param>
        /// <param name="toReplacement"></param>
        /// <returns></returns>
        public static String TrimSymbols(this string inputData, string[] trimSymbols, string toReplacement)
        {
            //string inputData = @"{EnumName}{EnumItemName}[flag1].[flag2].[flag3].[flag4]";

            string pattern = CreateTrimPattern(trimSymbols);

            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled;  
            Regex regex = new Regex(pattern, regexOptions);
            string result = regex.Replace(inputData, toReplacement);

            return result;
        }
        


        /// <summary>
        /// Replace one string Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="KnownFlagsReplacements"></param>
        /// <returns></returns>
        public static String ReplaceManyRe(this String inputData, Dictionary<String, String> KnownReplacements)
        {
            // Collecting flags            
            string outputResult = inputData;

            foreach (var item in KnownReplacements)
            {
                outputResult = outputResult.ReplaceRe(item.Key, item.Value);
            }

            return outputResult;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="KnownReplacements"></param>
        /// <returns></returns>
        public static String ReplaceRe(this String inputData, String RegexPattern = @"\/",String ReplacementString= "")
        {
            // Collecting flags            
            
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                    | RegexOptions.IgnorePatternWhitespace
                                    | RegexOptions.Compiled;
            Regex regexReplace = new Regex(RegexPattern, regexOptions);
            string outputResult = regexReplace.Replace(inputData, ReplacementString);
            

            return outputResult;
        }





        /// <summary>
        /// Split one string Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static String[] SplitRe(this String inputData, String RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase
                                        |  RegexOptions.CultureInvariant
                                        //| RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled; 
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            string[] result = regex.Split(inputData);
            
            List<String> outNotEmptyResult = new List<String>();
            foreach (var item in result)
            {
                if (item != "" && item != " " && item !=  "\t")
                {   outNotEmptyResult.Add(item);
                }
            }
            return outNotEmptyResult.ToArray();
        }




        public static String[] SplitRe(this String[] inputData, String RegexPattern = @"\/")
        {
            StringBuilder strBuilder = new StringBuilder();
            foreach (var item in inputData)
	        {
                strBuilder.AppendLine(item);
	        }
                        
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled;  
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            string[] result = regex. Split( strBuilder.ToString());


            
            List<String> outNotEmptyResult = new List<String>();
            foreach (var item in result)
            {
                if (item != "" && item != " " && item != "\t")
                {
                    outNotEmptyResult.Add(item);
                }
            }
            return outNotEmptyResult.ToArray();

            //return result;
        }
        



        /// <summary>
        /// Get First Match for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Match FirstMatch(this String inputData, String RegexPattern = @"\/",Int32 startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled; 
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            Match result = regex.Match(inputData,startat);
            return result;
        }
         

        /// <summary>
        /// Is current input text Contains one or more matches. Using Regular Expressions  
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static bool IsMatch(this String inputData, String RegexPattern = @"\/",Int32 startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled; 
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            bool result = regex.IsMatch(inputData, startat);
            return result;
        }


        /// <summary>
        /// Get All Matches for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static MatchCollection AllMatches(this String inputData, String RegexPattern = @"\/",Int32 startat = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled; 
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
            MatchCollection result = regex.Matches(inputData, startat);
            return result;
        }



        public static Dictionary<Int32,Match> PointedMatches(this String[] inputData, String RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled;
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
           Dictionary<Int32,Match> result = new Dictionary<Int32,Match>();

            for (int i = 0; i < inputData.Length; i++)
			{
			   Match matchItem = inputData[i].FirstMatch(RegexPattern);
                if (matchItem.Success)
                {
                    result.Add(i,matchItem);
                }
			}                
        
            return result;
        }



        public static Dictionary<Int32, Int32> MatchLines(this String[] inputData, String RegexPattern = @"\/")
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled;
            Regex regex = new Regex(pattern, regexOptions);

            Dictionary<Int32, Int32> result = new Dictionary<Int32, Int32>();
            Int32 j = 0;
            for (int i = 0; i < inputData.Length; i++)
            {                
                if (inputData[i].IsMatch(RegexPattern))
                {
                    result.Add(j, i);
                    j++;
                }
            }

            return result;
        }
                  


        /// <summary>
        /// Get All Matches count for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Int32 MatchesCount(this String inputData, String RegexPattern = @"\/",Int32 StartAt = 0)
        {
            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant
                                        | RegexOptions.IgnorePatternWhitespace
                                        | RegexOptions.Compiled; 
            Regex regex = new Regex(pattern, regexOptions);
            

            MatchCollection result = regex.Matches(inputData,StartAt);
            return result.Count;
        }
        




        
        
    }
}
