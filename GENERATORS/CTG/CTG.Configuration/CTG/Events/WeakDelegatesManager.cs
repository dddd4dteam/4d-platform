﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace CTG.Events
{



        
      

    /// <summary>
    /// Weak Delegate manager 
    /// </summary>
    /// <example>
    /// <code>
    /// private readonly WeakDelegatesManager contentRegisteredListeners = new WeakDelegatesManager();
    /// 
    /// public event EventHandler_EventArgs_ ContentRegistered
    /// {
    ///    add {     
    ///          this.contentRegisteredListeners.AddListener(value); 
    ///        }
    ///    remove 
    ///        {
    ///          this.contentRegisteredListeners.RemoveListener(value); 
    ///        }
    /// }
    /// </code>    
    /// </example>
    internal class WeakDelegatesManager
    {
        private readonly List<DelegateReference> listeners = new List<DelegateReference>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listener"></param>
        /// <example>
        /// <code>
        /// private readonly WeakDelegatesManager contentRegisteredListeners = new WeakDelegatesManager();
        /// 
        /// public event EventHandler_EventArgs_ ContentRegistered
        /// {
        ///    add { 
        ///         SubscribeTo_Initialized(this, )
        ///          this.contentRegisteredListeners.AddListener(value); 
        ///        }
        ///    remove 
        ///        {
        ///          this.contentRegisteredListeners.RemoveListener(value); 
        ///        }
        /// }
        /// </code> 
        /// </example>
        public void AddListener(Delegate listener)
        {
            this.listeners.Add(new DelegateReference(listener, false));
        }

        public void RemoveListener(Delegate listener)
        {
            this.listeners.RemoveAll(reference =>
            {
                //Remove the listener, and prune collected listeners
                Delegate target = reference.Target;
                return listener.Equals(target) || target == null;
            });
        }

        public void Raise(params object[] args)
        {
            this.listeners.RemoveAll(listener => listener.Target == null);

            foreach (Delegate handler in this.listeners.ToList().Select(listener => listener.Target).Where(listener => listener != null))
            {
                handler.DynamicInvoke(args);
            }
        }
    }

}
