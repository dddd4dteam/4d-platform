﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    /// <summary>
    /// Варианты Вывода информации - при отладке(DEBUG) и трассировке(TRACE) для CTGen-а
    /// </summary>
    public enum OutputSourceTypeEn
    {
        [Description("В качестве выходного потока для записи отладочной информации используется вывод на консоль")]
        Console,

        [Description("В качестве выходного потока для записи отладочной информации используется вывод в файл.Должен быть указан файл иначе ошибка")]
        File
    }


    /// <summary>
    /// Режимы отладки/ вывода отлажочной информации для CTGen-а  
    /// </summary>
    public enum DebugModeEn
    {

        [Description("Не выводить никакой отладнчной информации")]
        NotDebugging,

        [Description("Выводить отладочные сообщения -только конечного состония")]
        DebugOnly,

        [Description("Выводить отладочные сообщения -  конечное состоние + ход выполнения операций")]
        DebugWithTrace

    }





}
