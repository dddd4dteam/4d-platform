﻿


using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;


namespace CTG.Logging
{

    /// <summary>
    /// File security Permissions Helper
    /// </summary>
    public class FilePermissionHelper
    {



        #region -------------------------- Current User --------------------------


        static WindowsPrincipal _CurrentUser = null;
        /// <summary>
        /// Текущий пРИнципал которому просчитывается доверение прав
        /// </summary>
        public static WindowsPrincipal CurrentUser
        {
            get
            {
                if (_CurrentUser == null)
                {
                    _CurrentUser = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                }
                return _CurrentUser;
            }
        }


        #endregion -------------------------- Current User --------------------------



        /// <summary>
        /// Check that Current Windows Identity User has Write permissions to filepath
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        public static bool HasWritePermission(String FilePath)
        {
            try
            {
                FileSystemSecurity security;
                if (File.Exists(FilePath))
                {
                    security = File.GetAccessControl(FilePath);
                }
                else
                {
                    security = Directory.GetAccessControl(Path.GetDirectoryName(FilePath));
                }
                var rules = security.GetAccessRules(true, true, typeof(NTAccount));
                
                bool result = false;

                foreach (FileSystemAccessRule rule in rules)
                {
                    if (0 == (rule.FileSystemRights & (FileSystemRights.WriteData | FileSystemRights.Write)))
                    {
                        continue;
                    }

                    if (rule.IdentityReference.Value.StartsWith("S-1-"))
                    {
                        var sid = new SecurityIdentifier(rule.IdentityReference.Value);
                        if (! CurrentUser.IsInRole(sid))
                        {
                            continue;
                        }
                    }
                    else
                    {
                        if (!CurrentUser.IsInRole(rule.IdentityReference.Value))
                        {
                            continue;
                        }
                    }

                    if (rule.AccessControlType == AccessControlType.Deny)
                    {
                        return false;
                    }
                    if (rule.AccessControlType == AccessControlType.Allow)
                    {
                        return true;
                    }
                }
                return result;

            }
            catch (System.Exception ex)
            {
                return false;
            }

        }



    }
}
