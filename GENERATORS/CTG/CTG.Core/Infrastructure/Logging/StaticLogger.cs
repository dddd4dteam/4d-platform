﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.Security.Principal;

using System.Diagnostics;


using System.Collections;



namespace CTG.Logging
{


    /// <summary>
    /// Тип строки сообщения лога
    /// </summary>
    public enum LogTypeEn
    {
        InfoMSG,
        ErrorMSG
    }


    /// <summary>
    /// InitLogger - logger of crashing on Initializing  Fundamental Services - on  on  Initing  Ninjadb Stores configuration.
    /// This Logger is not IOC Service Logger. Its  standalone one. It use Base Path -C:Temp to save logs. 
    /// </summary>
    public class StaticLogger
    {




        #region -------------------------------- CONSTS --------------------------------

        /// <summary>
        /// Default Log File Name to log about some error in solution creating process. 
        /// Default Log Name uses if we don't have correct SolutionName
        /// </summary>
        //internal protected const string LOG_FILE_NAME_DEFAULT = "ModuleCreatingSolution.log";



        
        /// <summary>
        /// Ключ переменной Окружения к которому мы обращаемся чтоб узнать путь машины для Временных файлов -Место в котором будут размещаться логи визардов 
        /// </summary>
        const string ENVV_LOGTMPDIR = @"INIT_LOG";

        const string ENVV_LOGTMPDIRVALUE = @"C:\Temp\InitLogs\";

        /// <summary>
        ///Расширение для файлов лога
        /// </summary>
        const string LOG_EXT = @".log";

        /// <summary>
        /// Суффикс для названия папки лога для Визарда. ДЛя каждого визарда создается отдельная папка в директории окружения- [Temp].
        /// Папка логов имеет анзвание [WZRD_Name]+[LOG_FOLDER_SUFFIX]
        /// </summary>
        const string LOG_FOLDER_SUFFIX = @"_LOGS";





        /// <summary>
        /// Log Message Delimeter - разделитель в сообщении логов
        /// </summary>
        const string LOG_MSGDLMTR = @";";

        /// <summary>
        /// Завершающий символ соообщения лога
        /// </summary>
        const string LOG_MSGEND = @".";

     

        /// <summary>
        /// Mask for Log Info Message  :  [@"INFO:  MSG[{0}]  -  UserAccount:{1}  {2}  DateTime:{3}  {4}  Message:{5}  {6}"]
        /// </summary>
        const string LOG_INFOMSG_MASK = @"INFO:  MSG[{0}]  -  UserAccount:{1}  {2}  DateTime:{3}  {4}  Message:{5}  {6}";

        /// <summary>
        /// Mask for Log Info Message  :  [@"ERROR:  MSG[{0}]  -  UserAccount:{1}  {2}  DateTime:{3}  {4}  Message:{5}  {6}"]
        /// </summary>
        const string LOG_ERRORMSG_MASK = @"ERROR:  MSG[{0}]  -  UserAccount:{1}  {2}  DateTime:{3}  {4}  Message:{5}  {6}";

        /// <summary>
        /// Индекс сообщения
        /// </summary>
        private static Int32 MsgIndex = 0;


        #endregion -------------------------------- CONSTS --------------------------------





             

        /// <summary>
        /// Флаг инииализации логера Расширения- ОДин раз для ОДного процесса Запущенного Развертывания Шаблона
        /// </summary>
        static bool IsInited = false;

        /// <summary>
        /// Logger's reference to Current Extension Directory
        /// </summary>
        public static String LogDirectory
        {
            get
            {
                return ENVV_LOGTMPDIRVALUE + ProcessName + @"\";
            }            
            
        }



        /// <summary>
        /// Файл Логов для текущего Расширения
        /// </summary>
        public static string LogFile
        {
            //get { return ExtensionAssemblyName + LOG_EXT; }
             get;
            
            private set;
        }



        
        public static String LogFilePath
        {
            get;
            private set;
        }



        public static String UserAccount
        {
            get
            {
                return Environment.UserName;
            }
        }
        
        
        static String ProcessName
        {
            get
            {
                return Process.GetCurrentProcess().MainModule.ModuleName.Replace(".exe", "");
            }
        }



        
        /// <summary>
        /// Environment variables  contains variable with Key={VarKey}
        /// </summary>
        /// <param name="VarKey"></param>
        /// <returns></returns>
        public static bool ContainsVariable( String VarKey)
        {
            bool exist = false;

            IDictionary variables = System.Environment.GetEnvironmentVariables();

            foreach (var varItem in variables)
            {
                if (((DictionaryEntry)varItem).Key.ToString().ToLower() == VarKey.ToLower())
                {
                    exist = true; break;
                }
            }

            return exist;
        }

        

        #region  ------------------------------ METHODS ----------------------------------------


        /// <summary>
        /// Создать Директорию и установить переменную -Директории Лога Визарда
        /// </summary>
        private static void InitLogging()
        {
            if (IsInited == false)
            {
            
                if (Directory.Exists(LogDirectory) == false) Directory.CreateDirectory(LogDirectory);

                LogFile =   ProcessName  + "_"                               
                            + new Random().Next(333, 7777777).ToString()                 
                            + LOG_EXT;
                LogFilePath = LogDirectory + LogFile;

                if (File.Exists(LogFilePath) && FilePermissionHelper.HasWritePermission(LogFilePath))
                {
                    File.Delete(LogFilePath);
                }

            

                IsInited = true;
            }

        }

         /// <summary>
        /// Log-и для Wizard-а/ов Данного Расширения. Default LogTypeEn = LogTypeEn.ErrorMSG
        /// </summary>
        /// <param name="message"></param>
        static void Log(string message, LogTypeEn MsgType = LogTypeEn.ErrorMSG)
        {
            try
            {
                if (IsInited == false) InitLogging();


                using (StreamWriter streamWriter = File.AppendText(LogFilePath))
                {
                    string MSGLine = "";///конечная строка сообщения
                    MsgIndex += 1;
                    if (MsgType == LogTypeEn.InfoMSG)
                    {
                        MSGLine = LOG_INFOMSG_MASK.StrFmt(MsgIndex.ToString(), UserAccount, LOG_MSGDLMTR, DateTime.Now.ToString(), LOG_MSGDLMTR, message, LOG_MSGEND);
                    }
                    else if (MsgType == LogTypeEn.ErrorMSG)
                    {
                        MSGLine = LOG_ERRORMSG_MASK.StrFmt(MsgIndex.ToString(), UserAccount, LOG_MSGDLMTR, DateTime.Now.ToString(), LOG_MSGDLMTR, message, LOG_MSGEND);
                    }

                    streamWriter.WriteLine(MSGLine);
                    streamWriter.Close();
                }

            }
            catch (System.Exception ex)
            {

            }

        }


        /// <summary>
        /// Лог Визарда.  Вывод Информации
        /// </summary>
        /// <param name="message"></param>
        /// <param name="invalues"></param>
        public static void LogInfo(string message, params string[] invalues)
        {

            Log(message.StrFmt(invalues), LogTypeEn.InfoMSG);
        }


        /// <summary>
        /// Лог Визарда. Вывод Ошибки
        /// </summary>
        /// <param name="message"></param>
        /// <param name="invalues"></param>
        public static void LogError(string message, params string[] invalues)
        {
            Log(message.StrFmt(invalues), LogTypeEn.ErrorMSG);
        }



        #endregion ------------------------------ METHODS ----------------------------------------


    }
}
