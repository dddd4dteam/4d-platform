﻿using CTG.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


namespace CTG
{
    /// <summary>
    ///Composite Template DOMAIN LANGUAGE EXPRESSIONS Extensions
    /// </summary>
    public static class CTDLExtensions
    {


        #region  -------------------------------------- DOMAIN LANGUAGE EXPRESSIONS -----------------------------

        /// <summary>
        ///  Get First Match for input string. Here we beleave that we searching DomainLanguageExpression(DLE) in text.  Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static Match FirstMatchDLE(this String inputData, String RegexPattern = @"\/", bool IsRequireOneMatch = true, bool IsRequreFunction = false )
        {
            Contract.Requires(inputData != null, " DLE inputData string on Parsing  cannot be null");

            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled;
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";
             Match result = regex.Match(inputData);
            if (IsRequireOneMatch)
            {   if(result.Success ==false)
                throw new ParseException(String.Format("FirstMatchDLE- IsRequireOneMatch  error: we need one match in input Data for [{0}] template. ", RegexPattern )  );
            }

            if (IsRequreFunction)
            {
                Int32 OpenBracket = inputData.MatchesCount(@"\(");
                Int32 CloseBracket = inputData.MatchesCount(@"\)");
                if(OpenBracket== 0 || CloseBracket == 0 || OpenBracket != CloseBracket )
                    throw new ParseException(String.Format("FirstMatchDLE- IsRequreFunction error:we need correct function description by [{0}] template. Opened Brackets:[{1}]  Closed Brackets [{2}]", RegexPattern , OpenBracket.ToString(),CloseBracket.ToString() ));
            }
                
            return result;

        }
        

        /// <summary>
        ///  DomainLanguageExpression(DLE) - Get All Matches for input string. Using Regular Expressions
        /// </summary>
        /// <param name="inputData"></param>
        /// <param name="RegexPattern"></param>
        /// <returns></returns>
        public static MatchCollection AllMatchesDLE(this String inputData, String RegexPattern = @"\/", bool IsRequireManyMatches = true, bool IsRequreFunction = false)
        {
            Contract.Requires(inputData != null, " DLE inputData string on Parsing  cannot be null");

            string pattern = RegexPattern;
            RegexOptions regexOptions = RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled;
            Regex regex = new Regex(pattern, regexOptions);
            //string inputData = @"{http://[localhost]/mpost.MultiFileUpload.SampleWeb2/slmfu.single.default.aspx}";

            MatchCollection result = regex.Matches(inputData);
            if (IsRequireManyMatches)
            {              
                if (result.Count < 1)
                    throw new ParseException(String.Format("AllMatchesDLE- IsRequireManyMatches error:we need multi matches for [{0}] template in the input Data, but have only {1} match", RegexPattern, result.Count.ToString()));                
            }
             if (IsRequreFunction)
            {
                Int32 OpenBracket = inputData.MatchesCount(@"\(");
                Int32 CloseBracket = inputData.MatchesCount(@"\)");
                if(OpenBracket== 0 || CloseBracket == 0 || OpenBracket != CloseBracket )
                    throw new ParseException(String.Format("AllMatchesDLE- IsRequreFunction error:we need correct function description by [{0}] template. Opened Brackets:[{1}]  Closed Brackets [{2}]", RegexPattern, OpenBracket.ToString(), CloseBracket.ToString()));
            }

            return result;

            
        }


    


        #endregion -------------------------------------- DOMAIN LANGUAGE EXPRESSIONS -----------------------------

       
    }
}
