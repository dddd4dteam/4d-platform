﻿using CTG.Configuration;
using CTG.Core;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG
{
    public static class ConfigurationExtension
    {
        public static  StartTemplateTp GetStartTemplate(this  StartTemplateCollectionTp StartTemplates )
        {
            if (StartTemplates == null) return null;

            foreach (var item in StartTemplates.StartItems) //Templates
            {
                if (item.IsDefault) return item;
            }

            return null;
        }


        public static bool ContainsKey( this SettingCollectionTp settings, String Key)
        {
            if (settings == null || settings.Settings == null || settings.Settings.Count == 0 ) return false;

            SettingTp setg = settings.Settings.Where(st=> st.Key == Key ).FirstOrDefault();
            if (setg != null) return true;

            return false;
        }

        public static String GetValueByKey(this SettingCollectionTp settings, String Key)
        {
            if (settings == null || settings.Settings == null || settings.Settings.Count == 0) return null;

            SettingTp setg = settings.Settings.Where(st => st.Key == Key).FirstOrDefault();
            if (setg != null) return setg.Value;

            return null;
        }



        public static FileInfo GetStartTemplateFile(this StartTemplateTp startTemplate)
        {
            Contract.Requires<ParseException>(startTemplate != null, " GetStartTemplateFile() - this StartTemplate  cannot be null");

            Contract.Assert(File.Exists(startTemplate.FullName), "GetStartTemplateFile() - Start Template File in StartTemplateTp object setted incorrectly");

           return new FileInfo(startTemplate.FullName);
        }




        public static ConnectionTp GetDefaultConnect(this ConnectionCollectionTp connections)
        {
            if (connections == null || connections.Connects == null || connections.Connects.Count == 0) return null;

            return connections.Connects.Where(ct => ct.IsDefault == true).FirstOrDefault();        
 
        }


    }
}
