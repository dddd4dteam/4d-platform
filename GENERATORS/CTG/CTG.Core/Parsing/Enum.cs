﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core.Parsing
{


    /// <summary>
    /// 
    /// </summary>
    public enum ParamEn
    {

        NotDefined,

        /// <summary>
        /// Current Param is - Function  param itself and should be parsed further
        /// </summary>
        FunctionParam,

        /// <summary>
        /// Current Param is - Simple param itself
        /// </summary>
        SimpleParam
    }


}
