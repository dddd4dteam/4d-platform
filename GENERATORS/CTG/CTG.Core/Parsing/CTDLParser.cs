﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using CTG.EnumConfiguration;
using CTG.Core.Parsing;
using CTG.Configuration;

namespace CTG.Core
{
    /// <summary>
    /// CTDL (Composite Templates Domain Language) parser
    /// </summary>
    public class CTDLParser : FunctionsParser 
    {

       #region  ----------------------------------- CONSTS -----------------------------------


        //TEMPLATEPART()
        //  CYCLE()
        //      FOREACH(LM:EntityProperties,EntityProperty)   ||  FOREACH(TW:Classattribute) ||  FOREACH(LM:Entities,Entity)   ||  FOREACH(TW:Interface.Declaration) ||  FOREACH(TW:Interface.Implementation)
        //      CONDITION(TablesOnlyCondition)
        //      InTemplate(DefaultPropertyCloneEqual)
        //      UpdateLMAction(UpdateAction)
        //      Delimeter(,)
        //      EachItemOnNewLine()        
        
        //TEMPLATEPART()
        //  CASE() -  CASE(1) || CASE(2)   al last it was INVARIANT()

        //TEMPLATEPART()
        //  Declaration()
        //  Implementation()


        public static String CyclePartKey = "cycle";      
        public static String NotDeclaredPartKey = "standartpart";


        static string FUNC_TEMPLATEPART = "templatepart";
        static string FUNC_CYCLE = "cycle";       
        static string FUNC_FOREACH = "foreach";
        static string FUNC_CONDITION = "condition";
        static string FUNC_INTEMPLATE = "intemplate";
        static string FUNC_UPDATELMACTION = "updatelmaction";
        static string FUNC_DELIMETER = "delimeter";
        static string FUNC_EACHITEMONNEWLINE = "eachitemonnewline";
        static string FUNC_CASE = "case";
        static string FUNC_PARTNAME = "partname";
        static string FUNCPARAM_DECLARATION = "declaration";
        static string FUNCPARAM_IMPLEMENTATION = "implementation";

        static string CYCLE_EXTENDKEYTW = "tw";
        static string CYCLE_EXTENDKEYLM = "lm";


        static string TAG_DECLARATION_CLOSE = @"\/\]";
        static string TAG_DECLARATION_OPEN = @"\[(.*?)TEMPLATEPART";
        
        static string KEY_TEMPLATEFUNCBODY = @"(?<=\[)(.*?)(?=\/\])";
        static string KEY_REFERENCEMARKERS = @"(?<=\{)(.\w+)(?=\})|(?<=\{)(.\w+)\.(.\w+)(?=\})";



        static char[] TRIM_SYMBOLS = new char[] { '\r', '\n', '\t', ' ' };

#endregion ----------------------------------- CONSTS -----------------------------------


#region --------------------------- METHODS ----------------------------


       #region  --------------------------- GETTING METHODS ------------------------------


        /// <summary>
        /// Получить - Ключ слово из Имени Файла Шаблона
        /// </summary>
        /// <param name="pFile"></param>
        /// <returns></returns>
        private static String GetKeywordName(String pFileName)
        {
            String[] NameParts = pFileName.Split('.');
            return NameParts[NameParts.Length - 2];

        }
        
        /// <summary>
        /// Получить - Cлово Шаблона из Имени Файла Шаблона
        /// </summary>
        /// <param name="pFileName"></param>
        /// <returns></returns>
        private static String GetTemplatewordName(String pFileName)
        {
            String[] NameParts = pFileName.Split('.');
            return NameParts[0];

        }


        /// <summary>
        /// Получить  - текущий Поток Генерации исходя из пути файла шаблона 
        /// </summary>
        /// <param name="templateFilePath"></param>
        /// <returns></returns>
        private static String GetCurrentGenFlow(String templateFilePath)
        {
            String[] NameParts = templateFilePath.Split('\\');
            return NameParts[NameParts.Length - 2];
        }


        /// <summary>
        /// Получить - текущий язык программирования исходя из пути файла шаблона 
        /// </summary>
        /// <param name="templateFilePath"></param>
        /// <returns></returns>
        private static String GetCurrentLanguage(String templateFilePath)
        {
            String[] NameParts = templateFilePath.Split('\\');
            return NameParts[NameParts.Length - 3];
        }



        /// <summary>
        /// Составить информацию - о шаблонном слове(templateword)
        /// </summary>
        /// <param name="pFile"></param>
        /// <returns></returns>
        private static TemplatewordInfo GetTemplateWord(FileInfo  TemplateFile)
        {
            TemplatewordInfo twordInfo = new TemplatewordInfo();
            twordInfo.TemplateFilePath = TemplateFile.FullName;

          
            twordInfo.Templateword = GetTemplatewordName(TemplateFile.Name.ToLower());
            twordInfo.IsContainsTemplate = true;
            if (twordInfo.Templateword.ToLower() == TemplateTypeEn.Cycle.ToLowerStr())
            {
                twordInfo.TemplateType = TemplateTypeEn.Cycle;
            }
            else if (twordInfo.Templateword.ToLower() == TemplateTypeEn.Start.ToLowerStr())
            {
                twordInfo.TemplateType = TemplateTypeEn.Start;
            }
            else twordInfo.TemplateType = TemplateTypeEn.Content;

            return twordInfo;
        }



        #endregion--------------------------- GETTING METHODS ------------------------------



        #region  --------------------------- PARSING TEMPLATES -----------------------------
        


        #region  ------------------------------- PARSING START PROCESS --------------------------------------


        /// <summary>
        /// Parse: Запуск процесса загрузки контента файла шалона и парсинг его содержимого 
        /// </summary>
        /// <param name="preInitKeyword"></param>
        /// <returns></returns>
        public static KeywordInfo ParseKeywordTemplate(String TemplateFilePath)
        {
            Contract.Requires(TemplateFilePath != null && TemplateFilePath != "", "File load error : TemplateFilePath  cannot be null");

            FileInfo templateFile = new FileInfo(TemplateFilePath);

            try
            {
                if (templateFile.Exists == false) // File.Exists(TemplateFilePath)
                    throw new FileNotFoundException(" File Not Found ");
                
                Int32 nameParts = templateFile.Name.ToLower().Split('.').Length;
                if (nameParts != 3)
                    throw new ParseException(
                              String.Format("Template file Naming Convention Error. It has only {0} part instead 3 (on convention; ex: [templateword].[keyword].snippet ). ", nameParts.ToString() )    );

                KeywordInfo newKeyword = new KeywordInfo();
                
                //true - when loading it from template file first. next it can be disable wile proceeding Configuration Option
                newKeyword.IsEnable = true;

                //Language from File.Path base on template Storing Convention - /[genflowtemplate]/[languange]/[templatefile.snippet]
                newKeyword.CurrentLanguage = GetCurrentLanguage(TemplateFilePath);

                //GenerationFlow from File.Path base on template Storing Convention - /[genflowtemplate]/[languange]/[templatefile.snippet]
                newKeyword.CurrentGenFlow = GetCurrentGenFlow(TemplateFilePath);
                
                //KeywordName from File.Name based on Naming Convention - [templateword].[keyword].snippet
                newKeyword.Keyword = GetKeywordName(templateFile.Name.ToLower());
                
                //templateword from  template FileInfo
                newKeyword.Templateword = GetTemplateWord(templateFile);
                
                //template.OriginalContent from template file
                newKeyword.Template = LoadTemplateFromFile(templateFile);

                //TemplateParts from OriginalContent
                newKeyword.Template = ParseForTemplateParts(newKeyword.Template, newKeyword.Templateword.TemplateType);

                //Parsing KeywordReferencies
                newKeyword.Template = ParseForKeywordReferencies(newKeyword.Template);

                return newKeyword;

            }
            catch (System.Exception ex)
            {
                throw new ParseException(ex.Message + String.Format(" Template file {0} ", templateFile.Name));
            }
        }


        #endregion ------------------------------- PARSING START PROCESS --------------------------------------


        #region  -------------------------------- PARSING TEMPLATEPARTS ----------------------------------------

        /// <summary>
        /// Parsing Common  template and getting Parts Content to parse each item into the TemlatePart 
        /// </summary>
        /// <param name="template"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private static TemplateInfo ParseForTemplateParts(TemplateInfo template, TemplateTypeEn type)
        {
            try
            {
                if (template.FileName == "Cycle.AllVoParentTablePropertiesEqualThis.snippet")
                {

                }

                ////Get Parts after each match
                //List<String[]> DistinctedParts = template.OriginalTemplate.DistinctToParts(MatchedLines);
                MatchCollection matches = template.OriginalContent.AllMatches(KEY_TEMPLATEFUNCBODY);
                List<String> DistinctedParts = DistinctToParts(template.OriginalContent);

                Dictionary<String, TemplatePartInfo> newTemplateParts = new Dictionary<String, TemplatePartInfo>();

                Int32 partindex = 0;
                if (matches.Count > 0) //MatchedLines.Count
                {
                    foreach (var partContent in DistinctedParts)
                    {
                        string templateDeclartation = matches[partindex].Value;
                        TemplatePartInfo newPart = ParseTemplatePart(type,
                                                                        partindex,
                                                                        templateDeclartation,
                            //template.OriginalTemplate[MatchedLines[partindex]],
                                                                        DistinctedParts[partindex]
                                                                    );

                        newTemplateParts.Add(newPart.PartName.ToLower(), newPart);

                        partindex++;
                    }
                }
                else
                {
                    foreach (var partContent in DistinctedParts)
                    {
                        TemplatePartInfo newPart = ParseTemplatePart(type,
                                                                        partindex,
                                                                        null,
                            //template.OriginalTemplate[MatchedLines[partindex]], //Without  template declaration - only code text
                                                                        DistinctedParts[partindex]
                                                                    );

                        newTemplateParts.Add(newPart.PartName.ToLower(), newPart);

                        partindex++;
                    }
                }

                template.TemplateParts = newTemplateParts;

                return template;


            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            //Dictionary<Int32, Int32> MatchedLines = template.OriginalTemplate.MatchLines(@"\[TEMPLATEPART:(.*?)\/\]");            
           
        }

        /// <summary>
        /// Разделяем на части код шаблона
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private static List<String> DistinctToParts( String inputData)
        {
            List<String> parts = new List<String>();

            MatchCollection matchesClose = inputData.AllMatches(TAG_DECLARATION_CLOSE);
            MatchCollection matchesOpen = inputData.AllMatches(TAG_DECLARATION_CLOSE);

            if (matchesOpen.Count == 0 && inputData.Length > 0)//если нет никакогозаголовка
            {
                inputData = inputData.Trim(TRIM_SYMBOLS);
                parts.Add(inputData);
                return parts;
            }

            Int32 t = 0;
            while (t < matchesClose.Count)
            {
                string newpart = null;
                if ((t + 1) == matchesClose.Count) //все закончились совпадения
                {
                    //от текущей части до конца строки
                    Match itemMatch = (matchesClose[t] as Match);
                    newpart = inputData.Substring(itemMatch.Index + 2, inputData.Length - 2- itemMatch.Index);
                    newpart = newpart.Trim(TRIM_SYMBOLS);
                    parts.Add(newpart);
                }
                else if ((t + 1) < matchesClose.Count) //впереди будут и другие совпадения
                {
                    //от текущей части до конца строки
                    Match itemMatch = (matchesClose[t] as Match);
                    Match itemNextMatch = (matchesOpen[t + 1] as Match);

                    newpart = inputData.Substring(itemMatch.Index + 2, itemNextMatch.Index - 2 - itemMatch.Index);
                    newpart = newpart.Trim(TRIM_SYMBOLS);
                    parts.Add(newpart);
                }

                t++;
            }
            //где каждый элемент - контент нового парта
            String[] PartsContents = parts.ToArray();

            return parts;
        }






        /// <summary>
        /// Parsing TemplatePart Content and creating  template part
        /// </summary>
        /// <param name="type"></param>
        /// <param name="partIndex"></param>
        /// <param name="tempalteDeclaration"></param>
        /// <param name="Content"></param>
        /// <returns></returns>
        private static TemplatePartInfo ParseTemplatePart(TemplateTypeEn type, Int32 partIndex, String tempalteDeclaration, String Content) //String[] Content
        {
            TemplatePartInfo tempalatePart = new TemplatePartInfo();

            tempalatePart.TemplateDeclarationFunc = ParseFunction(tempalteDeclaration); //@"TEMPLATEPART:" //TemplateDeclaration 
            tempalatePart.OriginalContent = Content;                 //              
            tempalatePart.PartIndex = partIndex;

            // DL parse checks
            // if keyword template is Cycle - first string must have keyword Cycle
            if (type == TemplateTypeEn.Cycle)
            {
                if (tempalatePart.TemplateDeclarationFunc == default(FuncParameter)) // tring.IsNullOrEmpty(tempalatePart.TemplateDeclaration)
                    throw new ParseException("Cycle DECLARATION not found.");

                if (tempalatePart.TemplateDeclarationFunc.Contains(FUNC_CYCLE) == false)
                    throw new ParseException("Cycle Template parsing error.");

                tempalatePart.Cycle = ParseCycle(tempalatePart.TemplateDeclarationFunc);

                tempalatePart.PartName = CTDLParser.CyclePartKey;

            }
            else if (type == TemplateTypeEn.Start)//Empty part Name
            {
                if (tempalatePart.TemplateDeclarationFunc != default(FuncParameter))
                    throw new ParseException("Start Template should not have any template declarations. ");

                tempalatePart.PartName = CTDLParser.NotDeclaredPartKey;
            }
            else if (type == TemplateTypeEn.Content)
            {
                if (tempalatePart.TemplateDeclarationFunc != default(FuncParameter))
                {
                    //Template Declaration PartName
                    //string[] partName = tempalatePart.TemplateDeclarationFunc.SplitRe(TemplateDeclarationKey);   // @"(?<=\[)(.*?)(?=\/\])"); //    @"\[TEMPLATEPART\s{0,3}:(.*?)\/\]"
                    FuncParameter funcParameter = tempalatePart.TemplateDeclarationFunc.GetFunction(FUNC_PARTNAME);
                    tempalatePart.PartName = ((SimpleParameter)funcParameter.Parameters[0]).ParamBaseKey;// Type partName[0].ToLower();

                    //Invariant
                    if (tempalatePart.TemplateDeclarationFunc.Contains(FUNC_CASE))//  tempalatePart.PartName.Contains("INVARIANT".ToLower()) == true)
                    {
                        tempalatePart.IsInvariant = true;
                    }

                }
                else
                {
                    tempalatePart.PartName = CTDLParser.NotDeclaredPartKey;
                }

            }

            tempalatePart.IsValid = true;


            return tempalatePart;

        }

        #endregion -------------------------------- PARSING TEMPLATEPARTS ----------------------------------------


        #region  ------------------------------- PARSING CYCLE -------------------------------------

       


        /// <summary>
        /// Parsing for Cycle information
        /// </summary>
        /// <param name="CycleDefenition"></param>
        /// <returns></returns>
        private static CycleInfo ParseCycle(FuncParameter TemplateDeclareFunc)
        {        
            CycleInfo cycleinf = new CycleInfo();

            FuncParameter Funccycle = TemplateDeclareFunc.GetFunction(FUNC_CYCLE);//           

            FuncParameter FuncForeach = TemplateDeclareFunc.GetFunction(FUNC_FOREACH);
            FuncParameter FuncIntemplate = TemplateDeclareFunc.GetFunction(FUNC_INTEMPLATE);

            FuncParameter FuncCondition = TemplateDeclareFunc.GetFunction(FUNC_CONDITION);           
            FuncParameter FuncEachItemOnNewLine = TemplateDeclareFunc.GetFunction(FUNC_EACHITEMONNEWLINE);
            FuncParameter FuncUpdateLMAction = TemplateDeclareFunc.GetFunction(FUNC_UPDATELMACTION);
            FuncParameter FuncDelimeter = TemplateDeclareFunc.GetFunction(FUNC_DELIMETER);


            //get UPDATELMACTION  
            if (FuncUpdateLMAction != default(FuncParameter))
            {
                SimpleParameter param1 = (SimpleParameter)FuncUpdateLMAction.Parameters[0];
                cycleinf.UpdateLMAction = param1.ParamBaseKey;
            }
            //get FOREACH  
            if (FuncForeach != default(FuncParameter))
            {
                SimpleParameter simpleParam = (SimpleParameter)FuncForeach.Parameters[0];

                if (String.IsNullOrEmpty( simpleParam.ExtendingKey) == false && simpleParam.ExtendingKey ==  CYCLE_EXTENDKEYTW)
                {
                    cycleinf.SourceType = SourceTypeEn.Templates;
                }
                else if (String.IsNullOrEmpty(simpleParam.ExtendingKey) == false && simpleParam.ExtendingKey == CYCLE_EXTENDKEYLM)
                {
                    cycleinf.SourceType = SourceTypeEn.LoadedMetadata;
                }

                cycleinf.CollectionKey = simpleParam.ParamBaseKey;
                cycleinf.ItemTemplatePart = simpleParam.ParamDetailKey;

                if (FuncForeach.Parameters.Count ==2)
                {
                    SimpleParameter simpleParam2 = (SimpleParameter)FuncForeach.Parameters[1];

                    cycleinf.CollectionItemKey = simpleParam2.ParamBaseKey;
                }
               
            }
            //get INTEMPLATE 
            if (FuncIntemplate != default(FuncParameter))
            {
                SimpleParameter param1 = (SimpleParameter)FuncIntemplate.Parameters[0];
                cycleinf.ItemTemplate = param1.ParamBaseKey;
            }
            //get CONDITION  
            if (FuncCondition != default(FuncParameter))
            {
                SimpleParameter param1 = (SimpleParameter)FuncCondition.Parameters[0];
                cycleinf.ConditionKey = param1.ParamBaseKey;
            }
            //get DELIMETER  
            if (FuncDelimeter != default(FuncParameter))
            {
                SimpleParameter param1 = (SimpleParameter)FuncDelimeter.Parameters[0];
                cycleinf.Delimeter = param1.ParamBaseKey;
            }
            //get EachItemOnNewLine  
            if (FuncEachItemOnNewLine != default(FuncParameter))
            {
                cycleinf.EachItemOnNewLine = true;
            }            
                     

            return cycleinf;
        }


        #endregion ------------------------------- PARSING CYCLE -------------------------------------


        #region  -------------------------- PARSING  KEYWORD REFERENCIES ------------------------------

        /// <summary>
        /// Parse:  Ссылки на ключевые слова в тексте шаблона
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        private static TemplateInfo ParseForKeywordReferencies(TemplateInfo template)
        {
            //StringBuilder strbuilder = template.OriginalTemplate.ToOneString();
            Dictionary<String, TemplatePartInfo> newTemplateParts = new Dictionary<String, TemplatePartInfo>();

            // DEBUG CATCHING CASES
            if (template.FileName == "Propertyattribute.BOPropertyRole.snippet")
            {
            }

            foreach (var tmplp in template.TemplateParts)
            {
                TemplatePartInfo tmpltPart = tmplp.Value;
                tmpltPart.KeywordReferencies = new List<TokenInfo>();
                              

                MatchCollection partMatches = tmpltPart.OriginalContent.AllMatches(KEY_REFERENCEMARKERS);            
                foreach (var item in partMatches)
                {
                    //build ReferenceInfo
                    Match currentMathch = item as Match;
                    TokenInfo reference = new TokenInfo();
                    reference.Index = currentMathch.Index;
                    reference.TokenWord = currentMathch.Value;
                    reference.KeywordName = GetReferenceKeywordName(reference.TokenWord);
                    reference.TemplatePartName = GetReferenceTemplatePart(reference.TokenWord);


                  

                    bool flagSmartMarginUse = CTGConfigEn.UseReferenceSmartMargin.GetScalarValue<bool>();
                    if (flagSmartMarginUse)
                    { reference.SmartMargin = GetSmartMargin(tmpltPart.OriginalContent, currentMathch);
                    }
                    
                    tmpltPart.KeywordReferencies.Add(reference);
                }

                newTemplateParts.Add(tmplp.Key, tmpltPart);
            }

            template.TemplateParts = newTemplateParts;

            template.KeywordTokens = new List<String>(); //KeywordReferencies
            foreach (var tp in template.TemplateParts)
            {                
                foreach (var item in tp.Value.KeywordReferencies)
                {
                    if (template.KeywordTokens.Contains(item.KeywordName) == false )  // KeywordReferencies.Contains(item.KeywordName) == false)
                    {
                        template.KeywordTokens.Add(item.KeywordName); //KeywordReferencies.Add(item.KeywordName);
                    }
                }
            }

            return template;
        }


        private static String GetReferenceKeywordName(String Reference)
        {
            string[] parts = Reference.Split('.');
            if (parts != null && parts.Length == 2)
            {
                return parts[0];
            }
            else if (parts != null && parts.Length == 1)
            {
                return Reference;
            }
            return null;
        }


        /// <summary>
        /// Get Reference Template Part
        /// </summary>
        /// <param name="Reference"></param>
        /// <returns></returns>
        private static String GetReferenceTemplatePart(String Reference)
        {
            string[] parts = Reference.Split('.');
            if (parts != null && parts.Length == 2)
            {
                return parts[1].ToLower();
            }
            return null;            
        }


        /// <summary>
        /// Get margin for Reference item
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="marker"></param>
        /// <returns></returns>
        private static String GetSmartMargin(String Content, Match match)
        {       
            //get margin           
            string SmartMargin = null;
            Int32 index = match.Index - 2;
            
            if (index < 0) return null;
            
            char symbol = Content[index];
            while (symbol == '\t' || symbol == '\r' || symbol == ' ')
            {
                SmartMargin = symbol + SmartMargin;
               index= index-1;
               symbol = Content[index];
            }
            return SmartMargin;
        }


        #endregion -------------------------- PARSING  KEYWORD REFERENCIES ------------------------------

        
        #endregion ----------------------------------- PARSING TEMPLATES ---------------------------------
        


        #region -------------------------- LOADING  CONTENT--------------------------------


        /// <summary>
        /// Подгрузить OriginalTemplate содержимое файла  
        /// </summary>
        /// <param name="FilePath"></param>
        /// <returns></returns>
        private static TemplateInfo LoadTemplateFromFile(FileInfo TemplateFile)
        {
            // FileInfo templatefile = new FileInfo(FilePath);
            TemplateInfo template = new TemplateInfo();
            template.FileName = TemplateFile.Name;
            template.FilePath = TemplateFile.FullName;// FilePath

            template.TemplateExtension = TemplateFile.Extension; // templatefile.Extension;
            //template.OriginalTemplate = File.ReadAllLines(TemplateFile.FullName);
            template.OriginalContent = File.ReadAllText(TemplateFile.FullName).ToLower();

            //if (template.OriginalTemplate == null || template.OriginalTemplate.Length == 0)
            if (template.OriginalContent == null || template.OriginalContent.Length == 0)
                throw new ParseException("Template text cannot be null. ");

            return template;
        }


        #endregion ------------------------- LOADING  CONTENT--------------------------------



        #region ---------------------------------- DEBUG --------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static String[] DebugStrToArray(String inputString)
        {
            return inputString.SplitRe(@"\r|\n");
        }


        #endregion ---------------------------------- DEBUG --------------------------------


        
#endregion --------------------------- METHODS ----------------------------


    }
}
