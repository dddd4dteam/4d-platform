﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{ 
    public class ParseException:Exception
    {
        #region ------------------------------- CTOR----------------------------
        
        public ParseException()
            : base()
        {

        }


        public ParseException(String Message)
            : base(Message)
        {

        }

        #endregion ------------------------------- CTOR----------------------------
        
    }
}
