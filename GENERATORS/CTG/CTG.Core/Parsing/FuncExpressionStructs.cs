﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core.Parsing
{
    /// <summary>
    /// Базовый интерфейс  параметра
    /// </summary>
    public interface IParameter
    {
        
        /// <summary>
        /// Выражение которое относится к данному парамметру. МОжет быть сложны  параметр с вложенными функциями
        /// </summary>
        ParamEn ParamType { get;  }

    }



    /// <summary>
    /// Базовый интерфейс  выражения
    /// </summary>
    public interface  IExpression
    {
        /// <summary>
        /// Выражение которое относится к данному парамметру. МОжет быть сложны  параметр с вложенными функциями
        /// </summary>
        String Expression { get; set; } //LM:Interface.Declaration | FOREACH (LM:Interface.Declaration)

    }
    

    /// <summary>
    /// Expression - Simple Parameter
    /// </summary>
    public struct SimpleParameter: IParameter, IExpression
    {
        /// <summary>
        /// LM/TW
        /// </summary>
        public string ExtendingKey    //LM/TW
        {
            get;
            set;
        }

        /// <summary>
        /// Interface
        /// </summary>
        public string ParamBaseKey    //Interface
        {
            get;
            set;
        }

        /// <summary>
        /// Declaration
        /// </summary>
        public string ParamDetailKey  //Declaration
        {
            get;
            set;
        }


        /// <summary>
        /// 
        /// </summary>
        public string Expression
        {
            get;
            set;
        }


        public ParamEn ParamType
        {
            get { return ParamEn.SimpleParam; }
            
        }
    }



    /// <summary>
    /// Expression - Function Parameter as Parameter
    /// </summary>
    public struct FuncParameter: IParameter,IExpression
    {      

        /// <summary>
        /// 
        /// </summary>
        public string Expression
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Function
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public ParamEn ParamType
        {
            get { return ParamEn.FunctionParam;  }
            
        }
        
        /// <summary>
        /// Параметры
        /// </summary>
        public IList<IParameter> Parameters
        {
            get;
            set;
        }




        /// <summary>
        /// Return one Function Parameter if it is function with Name
        /// </summary>
        /// <param name="function"></param>
        /// <param name="pFunction"></param>
        /// <returns></returns>
        public  FuncParameter GetFunction( String pFunction)
        {
            
            if ( this.Function == pFunction )  { return this; }

            foreach (var paramItem in Parameters)
            {
                if (paramItem.ParamType == ParamEn.FunctionParam && ((FuncParameter)paramItem).Function == pFunction) { return (FuncParameter)paramItem; }
                else if (paramItem.ParamType == ParamEn.FunctionParam) 
                {
                    FuncParameter localFunc = ((FuncParameter)paramItem).GetFunction(pFunction);
                    if (localFunc.Function == pFunction)  { return localFunc;  }
                }
            }

            return default(FuncParameter);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pFunction"></param>
        /// <returns></returns>
        public bool Contains( String pFunction)
        {
            if (GetFunction(pFunction).Function == pFunction) { return true; }

            return false;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public override bool Equals(object Obj)
        {
            FuncParameter other = (FuncParameter)Obj;
            return (this.Expression == other.Function &&
                this.ParamType == other.ParamType// &&
                //this.Parameters.Count == other.Parameters.Count
                );
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Int32 GetHashCode()
        {
            return (Expression.GetHashCode() ^ this.ParamType.GetHashCode() );
        }


        public static bool operator ==(FuncParameter resultExpresssion1, FuncParameter resultExpresssion2)
        {
            return resultExpresssion1.Equals(resultExpresssion2);
        }


        public static bool operator !=(FuncParameter resultExpresssion1, FuncParameter resultExpresssion2)
        {
            return !resultExpresssion1.Equals(resultExpresssion2);
        }

    }


}
