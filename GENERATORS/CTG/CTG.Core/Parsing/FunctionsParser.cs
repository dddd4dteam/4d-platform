﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core.Parsing
{

    //TEMPLATEPART()
    //  CYCLE()
    //      FOREACH(LM:EntityProperties,EntityProperty)   ||  FOREACH(TW:Classattribute) ||  FOREACH(LM:Entities,Entity)   ||  FOREACH(TW:Interface.Declaration) ||  FOREACH(TW:Interface.Implementation)
    //      CONDITION(TablesOnlyCondition)
    //      InTemplate(DefaultPropertyCloneEqual)
    //      UpdateLMAction(UpdateAction)
    //      Delimeter(,)
    //      EachItemOnNewLine()        

    //TEMPLATEPART()
    //  CASE() -  CASE(1) || CASE(2)   al last it was INVARIANT()

    //TEMPLATEPART()
    //  Declaration()
    //  Implementation()
        
    //new - ItemDependanceObjectKey(EntityItem)

    //\[([a-zA-Z0-9]*)\s*\([^()]*\)\s*\/\]
    
    
    /// <summary>
    /// 
    /// </summary>
    public class FunctionsParser
    {

        #region  ---------------------------- CONSTS -------------------------------

        /// <summary>
        /// TemplateFuncParamsDelimeter - Ex: CYCLE - is TEMPLATE MAIN FUNCTION
        /// </summary>
        //public const char TemplateFuncParamsDelimeter = ';';

        /// <summary>
        /// ComponentFuncParamsDelimeter - Ex: CYCLE Params  CONDITIONS( __ , __ ) 
        /// </summary>
        public const char ParamsDelimeter = ',';

        /// <summary>
        /// Object Member- Getting Item Members 
        /// </summary>
        public const char ObjectMember = '.';

        /// <summary>
        /// Object Extend - 
        /// </summary>
        public const char ObjectExtend = ':';
        
        #endregion ---------------------------- CONSTS -------------------------------


        #region  ------------------------ CTOR -----------------------------

        static FunctionsParser()
        {
            Init();
        }

#endregion ------------------------ CTOR -----------------------------
        

        #region --------------------------- PROPERTIES -------------------------------- 

        /// <summary>
        /// Open Bracket symbol 
        /// </summary>
        internal static string OpenBracket
        {
            get;

            private set;

        }

        /// <summary>
        /// Close Bracket Symbol
        /// </summary>
        internal static string CloseBracket
        {
            get;

            private set;

        }



        /// <summary>
        /// Builded by Open and Close Bracket  - Match Expression to get Function Parameters code
        /// </summary>
        internal static string FunctionParametersMatch
        {
            get { return @"(?<=\" + @OpenBracket + @"{1,})(.*)(?=\" + @CloseBracket + "${1,})"; }
        }


        /// <summary>
        /// Match Expression to get Function Name 
        /// </summary>
        internal static string FunctionNameMatch
        {
            get { return @"^(.*?)(?=\" + @OpenBracket + ")"; }
        }

        /// <summary>
        ///Match Expression to get Function  All Brackets Match - Open and Close in one Collection
        /// </summary>
        internal static string FunctionAllBracketsMatch
        {
            get { return @"\" + @OpenBracket + @"|\" + @CloseBracket; }
        }

        /// <summary>
        /// Extend Word in params declaration - ex: LM:Property1.Detail1 - here Extend Word is LM
        /// </summary>
        internal static string FuncParamExtendKeyMatch
        {
            get { return @"^(.*?)(?=\" + @ObjectExtend.Str() + ")"  ; }
        }




#endregion  --------------------------- PROPERTIES --------------------------------

        
        /// <summary>
        /// 
        /// </summary>
        private static void Init()
        {
            ChangeBrackets();
        }


        /// <summary>
        ///  Change Bracket Symbols to Parse another bracket syntax
        /// </summary>
        /// <param name="pOpenBracket"></param>
        /// <param name="pCloseBracket"></param>
        public static void ChangeBrackets(String pOpenBracket=@"(", String pCloseBracket=@")")
        {
            OpenBracket = pOpenBracket;
            CloseBracket = pCloseBracket;
        }



#region  ---------------------------- PARSING PARAMETERS ----------------------------
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FunctionName"></param>
        /// <param name="FunctionCode"></param>
        /// <returns></returns>
        public static FuncParameter ParseFunction(String Expression1)
        {
            if (Expression1 == null) return default(FuncParameter);
            
            FuncParameter newFunc = new FuncParameter();
            string LoweExpression = Expression1.ToLower().Trim();
            newFunc.Expression = LoweExpression;
             

            string functionName = LoweExpression.FirstMatch(FunctionNameMatch).Value;
            if (String.IsNullOrEmpty(functionName))
                throw new ParseException("Expression doesn't contains any function Name defenition");
            newFunc.Function = functionName;
                        
            //Get function params expression                         @"\((.*)\)$"
            String ParamsExpression = LoweExpression.FirstMatch(FunctionParametersMatch).Value;
            //init Function Params
            newFunc.Parameters = new List<IParameter>();

            //if params equal null
            if (String.IsNullOrEmpty(ParamsExpression))
            {   return newFunc;                
            }
                     
            //split Params           
            List<string> paramsCode = SplitParams(ParamsExpression);

            //parsing each of param and adding it to the ParsedParamsList
            foreach (var item in paramsCode)
            {
                if (item.Contains(OpenBracket))
                {
                    FuncParameter itemParam = ParseFunction(item);
                    newFunc.Parameters.Add(itemParam);
                }
                else // simple Param
                {
                    Tuple<String, String, String> paraminfo = ParseSimpleParam(item);
                    SimpleParameter newParam = new SimpleParameter()
                    {
                        Expression = item,                   
                        ExtendingKey = paraminfo.Item1,
                        ParamBaseKey = paraminfo.Item2,
                        ParamDetailKey = paraminfo.Item3
                    };
                    newParam.Expression = item;

                    newFunc.Parameters.Add(newParam);
                }
            }

            return newFunc;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Expression"></param>
        /// <returns></returns>
        public static Tuple<String, String, String> ParseSimpleParam(String Expression1)
        {
            Tuple<String, String, String> simpleParamPrsed = new Tuple<String, String, String>(null, null, null);
            string LowerExpression = Expression1.ToLower();

            string ExtendKey = LowerExpression.FirstMatch(FuncParamExtendKeyMatch).Value;
            string UpdatedExpression = LowerExpression;
            if (String.IsNullOrEmpty(ExtendKey) == false)
            { UpdatedExpression = LowerExpression.ReplaceRe(ExtendKey + "|" + ObjectExtend); }

            string[] paramDetails = UpdatedExpression.Split(ObjectMember);
            string paramBaseKey = "";
            string paramDetailKey = "";
            if (paramDetails.Length == 1)
            {
                paramBaseKey = paramDetails[0];
            }
            if (paramDetails.Length == 2)
            {
                paramBaseKey = paramDetails[0];
                paramDetailKey = paramDetails[1];
            }

            simpleParamPrsed = new Tuple<String, String, String>(ExtendKey, paramBaseKey, paramDetailKey);

            return simpleParamPrsed;
        }


        #endregion -------------------------- PARSING PARAMETERS----------------------


#region  --------------------------------- SPLITTING PARAMS ------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParamsExpression"></param>
        /// <returns></returns>
        private static List<string> SplitParams(String ParamsExpression)
        {
            //
            List<string> resultParams = new List<string>();


            while (ParamsExpression.Length > 0)
            {
                string nextParamExpression = null;

                Match separatorblock = ParamsExpression.FirstMatch(@",");
                string tempBite = ParamsExpression;
                if (separatorblock.Success)
                { tempBite = ParamsExpression.Substring(0, separatorblock.Index);
                }


                if (CheckParamExpressionIsException(tempBite))
                {
                    nextParamExpression = ParamsExpression;
                    resultParams.Add(nextParamExpression.Trim());
                    ParamsExpression = ParamsExpression.Replace(nextParamExpression, "");
                    
                    return resultParams;

                }
                else if (tempBite.Contains(OpenBracket)) // //need to byte next function param
                {
                    nextParamExpression = BiteFuncParam(ParamsExpression);
                }
                else
                {
                    nextParamExpression = BiteSimpleParam(ParamsExpression);
                }

                resultParams.Add(nextParamExpression.Trim());

                //replace ParamsExpression string
                ParamsExpression = ParamsExpression.Replace(nextParamExpression, "");

                //remove next params delimeter
                ParamsExpression = DeleteNextParamsDelimeter(ParamsExpression);


            }
            return resultParams;
        }


        #region ---------------------- Biting Expression Params ---------------------


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParamsExpression"></param>
        /// <param name="OpenBracket"></param>
        /// <param name="CloseBracket"></param>
        /// <returns></returns>
        public static string BiteFuncParam(String ParamsExpression)
        {

            MatchCollection brackets = ParamsExpression.AllMatches(FunctionAllBracketsMatch);
            if (brackets.Count == 0)
            {
                throw new ParseException(String.Format(@"ByteFuncParam: there were no finded any bracket symbols - [\(|\)] in input expression - [{0}] ", ParamsExpression));
            }

            if (brackets[0].Value == CloseBracket)
            {
                throw new ParseException(String.Format(@"ByteFuncParam: First match cannot be close bracket symbol - [{0}] in input expression - [{1}] ", CloseBracket, ParamsExpression));
            }


            //Int32 OpenCountNeed=1,CloseCountNeed = 1;
            Int32 OpenCountGet = 1;
            Int32 CloseCountGet = 0;

            Int32 firstBracket = brackets[0].Index; //first OpenBracket index
            Int32 endBracketMatch = -1;
            for (int i = 1; i < brackets.Count; i++)
            {
                if (brackets[i].Value == CloseBracket) CloseCountGet++;
                else if (brackets[i].Value == OpenBracket) OpenCountGet++;

                if (OpenCountGet == CloseCountGet)
                {
                    endBracketMatch = i; break;
                }
            }


            if (endBracketMatch == -1)
            {
                throw new ParseException(String.Format(" Incorrect Func Expression - Count of Open and Close brackets mismatched - {0}", ParamsExpression));
            }


            //we know index of
            //cut srting before and find index if params delimeter(',')
            Match delimeterMatch = ParamsExpression.FirstMatch(ParamsDelimeter.Str(), brackets[endBracketMatch].Index + 1);

            if (delimeterMatch.Success == false) return ParamsExpression;//full expression is only one Param
            else return ParamsExpression.Substring(0, delimeterMatch.Index);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParamsExpression"></param>
        /// <returns></returns>
        private static bool CheckParamExpressionIsException(String ParamsExpression)
        {
            if (ParamsExpression == "")
                return true;
            else return false;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParamsExpression"></param>
        /// <returns></returns>
        private static string BiteSimpleParam(String ParamsExpression)
        {

            //we know index of
            //cut srting before and find index if params delimeter(',')
            Match delimeterMatch = ParamsExpression.FirstMatch(ParamsDelimeter.Str());

            if (delimeterMatch.Success == false) return ParamsExpression;
            else return ParamsExpression.Substring(0, delimeterMatch.Index);

        }



        #endregion ---------------------- Biting Expression Params ---------------------


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ExpressionCode"></param>
        /// <returns></returns>
        private static string DeleteNextParamsDelimeter(String ExpressionCode)
        {
            Match delimeterMatch = ExpressionCode.FirstMatch(ParamsDelimeter.Str());
            if (delimeterMatch.Success)
            {
                return ExpressionCode.Remove(delimeterMatch.Index, 1);
            }
            else return ExpressionCode;

        }


#endregion --------------------------------- SPLITTING PARAMS ------------------------------


      

    }
}
