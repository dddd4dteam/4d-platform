﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition.Hosting;
using NMG.Core.Domain;
//using TG.Core.Model;
using CTG.Configuration;
using NMG.Core.Reader;
using NMG.Core;
using System.IO;
using CTG;
using CTG.EnumConfiguration;
using CTG.Core.Parsing;
using System.Diagnostics.Contracts;





namespace CTG.Core
{

    /// <summary>
    /// Генератор  Композитных  Шаблонов  . 
    /// Оперирует загрузкой компонентов - Потоков Генерации, Менеджеров Потоков Генерации
    /// От лица данного класса начинается подготовка(загрузка и инициализация компонентов ), 
    /// 2- установление соединения для подгрузки метаданных
    /// 3 - Осуществляется подгрузка метаданных 
    /// и 4- запуск генерации для выбранного Потока Генерации, выбранного в нем Менеджера Потока Генерации
    /// </summary>    
    public sealed class CTGen 
    {
           

     #region ------------ Singleton  Constructor -----------------------
     
	 // PARAMS :     
     // Class   -         TemplateGeneratorComposition          EX: SystemDispatcher - Singleton Class Name           
     // ClassDesc  -      Композиция Генератора шаблонов        EX: Диспетчер системы 
	 	 
     /// <summary>
     ///  Target Class - TemplateGeneratorComposition -  Ctor
     /// </summary>
     CTGen()
     {  
         //  Initializing  Target Class         
        
     }

  

	 /// <summary>
     /// Singleton Getting Property.
     /// Композиция Генератора шаблонов
     /// </summary>     
     public static CTGen Current
     {
        get
        { 
            return CTGenCreator.instance;            
        }        
     }
    
	 /// <summary>
     /// Creator Class - who instantiating Singleton object 
     /// </summary>
     class CTGenCreator
     { 
        /// <summary>
        /// Explicit static constructor to tell C# compiler
        /// not to mark type as beforefieldinit
        /// </summary>
        static CTGenCreator()
        {
        }

		/// <summary>
        /// Single target class instance
        /// </summary>
        internal static readonly CTGen instance = GetFirstInstance();


        static CTGen GetFirstInstance()
        {           
                       
            //Initializing MEF Catalogs
            Initialize();

            // Getting some Types that have importing MEF Parts  + need LogManager  
            CTGen FirstInstance = new CTGen();

         

            return FirstInstance;
        }

     }
       
    #endregion   ----------- Silngleton  Constructor -----------------------
       


        #region  --------------------------- LOCKER ---------------------------------

        private static readonly object _locker = new object();

        #endregion --------------------------- LOCKER ---------------------------------
     

        #region  --------------------------- PROPERTIES --------------------------------

        

        #region     ----------------------------PROP ITEM  :  IsLoaded ---------------------------------
        // PARAMS :
        // PropName   -      IsLoaded         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Инициализирован ли Генератор          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Инициализирован ли Генератор 
        /// </summary>        
        public bool IsLoaded
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  IsLoaded ---------------------------------



        #region  ------------------------- COMPOSITION COLLECTIONS ---------------------------------


        #region ------------------------------- MEF/Unity Containers -----------------------


        /// <summary>
        ///  MEF Контейнер для формирования Композиции системы 
        /// </summary>
        internal static CompositionContainer Container
        {
            get;
            set;
        }

        /// <summary>
        /// Общий  Каталог 
        /// </summary>
        protected static AggregateCatalog AggregateCatalog
        {
            get;
            set;
        }

        #endregion ------------------------------- MEF/Unity Containers -----------------------


        #region ------------------ SERVICE   MEF based Lazy Importing Collection: GenFlowLangManagers ------------------------
        // Params:
        // GenerationFlows  GenerationFlows   Ex: SystemWindows
        // IGenerationFlow            IGenerationFlow             Ex: IWindow         
        // IGenerationFlowMetaData    IGenerationFlowMetaData     Ex: IWindowMetaData
        // Сценарии потока генерации            Сценарии потока генерации             Ex: Все контракты- типы контролов Окон которые получены в коллекцию после обработки Контейнера композиции 
        /// <summary>
        /// Сценарии потока генерации  
        /// </summary>
        [ImportMany(typeof(IGenFlowLangManager), AllowRecomposition = true)]
        public IList<Lazy<IGenFlowLangManager, IGenFlowLangManagerMetaData>> GenFlowLangManagers = new List<Lazy<IGenFlowLangManager, IGenFlowLangManagerMetaData>>();

        #endregion ------------------ SERVICE MEF based Lazy Importing Collection: GenFlowLangManagers ------------------------


        #endregion ------------------------- COMPOSITION COLLECTIONS ---------------------------------



        #region ----------------------------- CURRENT(GenFlow && Connection) ITEMS --------------------------


        #region     ----------------------------PROP ITEM  :  (GenerationFlowBase) CurrentGenFlow   ---------------------------------
        // PARAMS :
        // PropName   -      SelectedGenerationFlow         EX: AddNewButton     
        // PropType   -      GenerationFlowBase         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Выбранный поток генерации  для проведения генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Выбранный поток генерации  для проведения генерации
        /// </summary>        
        public GenerationFlowBase CurrentGenFlow
        {
            get;
            private set;
        }

        #endregion  -------------------------- PROP ITEM  : (GenerationFlowBase) CurrentGenFlow ---------------------------------


        #region     ----------------------------PROP ITEM  :  (ConnectionTp) CurrentConnection   ---------------------------------
        // PARAMS :
        // PropName   -      CurerntConnection         EX: AddNewButton     
        // PropType   -      ConnectionTp         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Выбранное соединение         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Выбранное соединение
        /// </summary>        
        public ConnectionTp CurrentConnection
        {
            get;
            private set;
        }

        #endregion  -------------------------- PROP ITEM  : (ConnectionTp) CurerntConnection ---------------------------------
        

        #endregion ----------------------------- CURRENT(GenFlow && Connection) ITEMS --------------------------
                
      
        #region  --------------------------- LOADED METADATA && ENTITIES  COLLECTIONS -----------------------
             

        /// <summary>
        /// Читатель метаданных
        /// </summary>
        private IMetadataReader MetadataReader
        {
            get;
            set;
        }


        /// <summary>
        /// Таблички- первичный набор который мы  получаем с источника. 
        /// Далее при генерации  формируется ограниченный набор Сущностей 
        /// </summary>
        public List<Table> LoadedTables
        {
            get;
            internal set;
        }


        /// <summary>
        /// Загруженные  Сущности
        /// </summary>
        public static Dictionary<String, Entity> LoadedEntities
        {
            get;
            internal set;
        }


        #endregion --------------------------- LOAD METADATA && ENTITIES  COLLECTIONS -----------------------



        // DEBUGGING TRACING
        // ILogger LogManager


        #endregion --------------------------- PROPERTIES --------------------------------


       
        // Load KeywordWriters         
        // Set Connection/ 
        // Load Tables MetaData
        // Select Template
        
        // Parse Entities



        /// <summary>
        /// Preload ConfigInitAction Configuration  
        /// </summary>
        /// <param name="preloadAction"></param>
        public static void PreLoad(Action  preloadConfigInitAction = null)
        {
            if (preloadConfigInitAction != null)
            {
                preloadConfigInitAction();
            }
        }
             
        


        /// <summary>
        /// Initializing base MEF Host Object 
        /// </summary>
        private static void Initialize()
        {
            try
            {
                
                    lock (_locker)
                    {
                                                                      
                        AggregateCatalog = new AggregateCatalog();

                        //Environment.CommandLine//CurrentDirectory
                        //Adding AssemblyCatalog            
                        AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(CTGen).Assembly));

                        //Adding DirectoryCatalog
                        AggregateCatalog.Catalogs.Add(new DirectoryCatalog(CTGConfigEn.BaseExtensionsPath.GetScalarValue<String>()));

                        //Run Mef Part Exporting Analysys
                        Container = new CompositionContainer(AggregateCatalog);

                    }

               
            }
            catch(Exception excError)
            {
                //LogManager.Write(Error, excError.Message);
            }
        }




        #region  ----------------- CONNECTION OPERATIONS -------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connect"></param>
        public void SetConnection(ConnectionTp connect)
        {

            Contract.Requires(connect != null, "SetConnection(): connect cannot be null");
            //set Connection
            CurrentConnection = connect;// config.ServerConnections.GetDefaultConnect();


            //Set MetadataReder 
            MetadataReader = MetadataFactory.GetReader( CurrentConnection.Server,
                                                        CurrentConnection.ConnectString);

        }
        



        /// <summary>
        /// 
        /// </summary>
        public void TestConnection()
        {
            Contract.Requires(CurrentConnection != null, "TestConnection(): connect cannot be null");
          try
          {
              //Set MetadataReder 
              MetadataReader = MetadataFactory.GetReader(CurrentConnection.Server,
                                                           CurrentConnection.ConnectString);
              
              MetadataReader.GetOwners();

          }
          catch (System.Exception ex)
          {
              throw new ParseException("TestConnection():  Connection false ");          	    
          }
           
        }


        #endregion  ----------------- CONNECTION OPERATIONS -------------------------




        #region  ------------------------------- LOADING METADATA && BUILD ENTITIES --------------------------------------



        /// <summary>
        /// Подгрузить метаданные
        /// </summary>
        public void LoadMetaData()//String ServiceKey = null
        {
            try
            {
                //ServiceKey = Configuration.GenerationSettings.GetValueByKey(GenerationFlowBase.ST_LM_ServiceKey);

                //Collecting all tables from all owners
                IList<String> owners = MetadataReader.GetOwners();
                foreach (var item in owners)
                {
                    LoadedTables.AddRange(MetadataReader.GetTables(item));
                }

                
              
                // All tables 
                foreach (var item in LoadedTables)
                {
                    item.Columns = MetadataReader.GetTableDetails(item, item.Owner);
                    item.PrimaryKey = MetadataReader.DeterminePrimaryKeys(item);
                    item.ForeignKeys = MetadataReader.DetermineForeignKeyReferences(item);
                }


                //????????
                //Create Entities  - only copying tables to Entities
                BuildGenerationEntities(LoadedTables);
              
            }
            catch (Exception)
            {
                throw;
            }
        }



        /// <summary>
        ///  Построить список сущностей предстоящих к генерации
        /// </summary>
        public void BuildGenerationEntities(IEnumerable<Table> selectedTables)
        {
            try
            {
                GenerationContext.Entities.Clear();

                foreach (var item in selectedTables)
                {
                    Entity entity = new Entity();
                    entity.LoadedTable = item;
                    //entity. typology 
                    entity.Name = item.Name;

                    GenerationContext.Entities.Add(entity.LoadedTable.Name, entity);

                }

            }
            catch (Exception)
            {
                throw;
            }
        }




        #endregion ------------------------------- LOADING METADATA && BUILD ENTITIES --------------------------------------




        #region  --------------------------------- GENERATION FLOWS OPERATIONS --------------------------------------


            // Check for  -  ValidationRuleExceptionsEn.NotOneGenerationFlowWasFind



        public GenerationFlowBase GetGenerationFlow()
        {
            return null;
        }

            /// <summary>
            /// Getting  all GenFlows finded in Extensions folder -  by their  Names
            /// </summary>
            /// <returns></returns>
        public List<IGenFlowLangManagerMetaData> GetGenFlowLangManagers()
            {
                Contract.Requires<ParseException>(GenFlowLangManagers.Count != 0, "GetGenFlows() : - CTGen doesn't find any generation flow ");

                List<IGenFlowLangManagerMetaData> genflowsMeta = new List<IGenFlowLangManagerMetaData>();
                foreach (var genflow in GenFlowLangManagers)
                {
                    genflowsMeta.Add(genflow.Metadata);
                }

                return genflowsMeta;

            }


            
            /// <summary>
            /// Getting  Generation Flow Managers object. If it wasn't created it will be created and inited.
            /// </summary>
            /// <param name="GenFlowKey"></param>
            /// <returns></returns>
        private List<IGenFlowLangManagerMetaData> GetGenFlowLangManagers(String GenFlowKey)
            {
                Contract.Requires(GenFlowKey != null, "GetGenFlow():  GenFlowKey cannot be null "); 
                Contract.Requires<ParseException>(GenFlowLangManagers.Count != 0, "GetGenFlow(): - CTGen doesn't find any generation flow ");

                List<IGenFlowLangManagerMetaData> genflowsMeta = new List<IGenFlowLangManagerMetaData>();
                foreach (var genflow in GenFlowLangManagers)
                {
                    if (genflow.Metadata.Name == GenFlowKey )
                    {

                       // resultGenFlow =  genflow.Value as GenerationFlowBase;                        
                    }                    
                }

                //Contract.Ensures(resultGenFlow != null, String.Format("GetGenFlow(): Generation flow [{0}] was not finded between all currently loaded GenFlows", GenFlowKey));
                return null;// resultGenFlow;

            }


            /// <summary>
            /// Selecting Current Generation Flow. SO next we need to select GenFLow generation Language, after that we can run generation 
            /// </summary>
            /// <param name="GenFlowKey"></param>
            public void SelectGenerationFlow(String GenFlowKey)
            {
                CurrentGenFlow = GetGenerationFlow();
            }





        #endregion  --------------------------------- GENERATION FLOWS OPERATIONS --------------------------------------

        




        /// <summary>
        /// Генерируем конечный код 
        /// </summary>
        public void Generate()
        {
            try
            {
                CurrentGenFlow.Generate();

                //Load all Templates from GenFlow Folder
                //LoadMetaData(ServiceKey: "S10");

                //BuildGenerationEntities();

                //SetDefaultParams();//temporary method    

                //LoadGenFlowTemplates();

                //AnalyzeReferencies();               
                
                //ValidatePreGeneration();

                //MergePublishedKeyword();
                
                //Build              

            }
            catch (ParseException prsExc)
            {
                throw prsExc;
            }
            catch (Exception )
            { throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public async void GenerateWithFlow(String GenFlowKey )
        {

         Task generationTask = Task.Factory.StartNew(() =>
                {

                    // ValidatePreGeneration - Check all published keywords >= all KeywordReferencies
                    // MergePublishedWritersKeywords&LoadedTemplateKeywords into one GenerationContext.Keywords list   
                    
                    //Build 
                    //GenerationContext.GenerationFlow. 
                    
                });

         await generationTask;

         if (generationTask.Status == TaskStatus.Faulted )
         {
             //Log  about  crash
         }
          
        }
      
    }
}
