﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections;
using System.Diagnostics.Contracts;
using CTG;
using CTG.Core;
using System.Text.RegularExpressions;

namespace CTG.Core
{
 
    /// <summary>
    /// Занимается рекурсивным распечатыванием значний выражений ключевых слов.т.е. выполняет Основную часть задачи 
    /// </summary>
    public  class KeywordBuilder //: IKeywordBuilder
    {
        
        #region ----------------------- CTOR ---------------------------

        public KeywordBuilder()
        {  
            Initialize();
        }

#endregion ----------------------- CTOR ---------------------------


        #region  ------------------------- NESTED ----------------------------
        /// <summary>
        /// Tree in KeyWordBuilder template  processing
        /// </summary>
        public class TreeInfo
        {
        
    
        #region     ----------------------------PROP ITEM  :  Level ---------------------------------
        // PARAMS :
        // PropName   -      Level         EX: AddNewButton     
        // PropType   -      Int32         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Уровень глубины при постронии дерева выражения          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        ///Текущий Уровень глубины при построении дерева выражения  - который может идти вниз при удлинении цепочки зависимоти и подниматься выше после построения выражения для конечнгого листа
        /// </summary>        
        public  Int32  Level
        {
            get; 
            internal set;
        }

        #endregion  -------------------------- PROP ITEM  :  Level ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  MaxLevel ---------------------------------
        // PARAMS :
        // PropName   -      MaxLevel         EX: AddNewButton     
        // PropType   -      Int32         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - internal    EX: internal
        // PropDesc  -       Максимально достигнутый уровень глубины вложенности при построении выражения из первоначального Ключ слова         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Максимально достигнутый уровень глубины вложенности при построении выражения из первоначального Ключ слова
        /// </summary>        
        public  Int32  MaxLevel
        {
            get; 
            internal set;
        }

        #endregion  -------------------------- PROP ITEM  :  MaxLevel ---------------------------------
        

        #region     ----------------------------PROP ITEM  :  LevelNodes ---------------------------------
        // PARAMS :
        // PropName   -      LevelNodes         EX: AddNewButton     
        // PropType   -      Dictionary<Int32,Int32>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Число узлов на уровне         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Число узлов на уровне
        /// </summary>        
        public  Dictionary<Int32,Int32>  LevelNodes
        {
            get;
            internal set;
        }

        #endregion  -------------------------- PROP ITEM  :  LevelNodes ---------------------------------
    

        }

#endregion  ------------------------- NESTED ----------------------------


        #region -------------------------- PROPERTIES ------------------------------

        //Древо выражения - построения выраженя
        static TreeInfo _ExpressionTree = new TreeInfo();

        /// <summary>
        /// Древо выражения - построения выраженя
        /// </summary>
        public static TreeInfo ExpressionTree
        {
            get
            {
                return _ExpressionTree;
            }
            internal set { _ExpressionTree = value; }
        }




        #endregion -------------------------- PROPERTIES ------------------------------
        

        #region  -------------------------------- METHODS ---------------------------------


#region ---------------------------------INITIALIZE -------------------------------
        

        /// <summary>
        /// Инициализация Писателя по умолчанию
        /// </summary>
        protected void Initialize()
        {
        }
        

        #endregion ---------------------------------INITIALIZE ------------------------------
        

#region  -------------------------------------- RECURSION  BUILD METHODS --------------------------------------
        
/// <summary>
/// Build Keyword's result expression.
/// First type of ResultExpression - is TP.ResultExpression
/// Second type of ResultExpression - is {KW.WithoutTemplateExpression}
/// </summary>
/// <param name="KeyWord"></param>
/// <param name="TemplatePart"></param>
public void BuildKeywordExpression(string KeyWord, string TemplatePart = null)
{
    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[KeyWord];
    //Если жто слово настроено положитльно к выводу. Через опционную конфигурацию до начала генерации.Если в конфгиурации она не допущена то ничего  не делать и пропустить   
    if (currentKeyword.IsEnable == false) 
    {   // log about not enabled keyword entry
        return;
    }           
           

    if (currentKeyword.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
    {
        BuildExpression4WithoutTemplateKW(KeyWord);
    }
    else if (currentKeyword.Templateword.TemplateType == TemplateTypeEn.Start || currentKeyword.Templateword.TemplateType == TemplateTypeEn.Content)
    {
        BuildExpression4ContentKW(KeyWord, TemplatePart);
    }
    else  if ( currentKeyword.Templateword.TemplateType == TemplateTypeEn.Cycle)
    {
        BuildExpression4CycleKW(KeyWord);
    }   
   

}





#region  ---------------------------------------- BUILDING WITHOUT TEMPLATE ---------------------------------------------

/// <summary>
/// Building Keyword Result Expression  4 -  Without template Keywords.  
/// KW with TemplateType==[WithoutTemplate] - has (no i.m. 0) TemplateParts. Result expression such KW  contains in it's   {KW.WithoutTemplateExpression} propty
/// </summary>
/// <param name="Keyword"></param>
/// <param name="TemplatePart"></param>
protected virtual void BuildExpression4WithoutTemplateKW(String Keyword, String TemplatePart = null)
{
    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[Keyword];
    TemplateInfo template = currentKeyword.Template;
    String Part = GetTemplatePartKey(currentKeyword.Templateword.TemplateType, TemplatePart);
    //TemplatePartInfo tplPart = template.TemplateParts[Part];

    //Invoke build keyword Method
    //if (currentKeyword.  BuildKeywordMethods.ContainsKey(currentKeyword.Keyword))
    //{
    //    BuildKeywordMethods[currentKeyword.Keyword].Invoke(this, new object[] { null });
    //}

    //Update - no templateParts  to update
}

#endregion ---------------------------------------- BUILDING WITHOUT TEMPLATE  ---------------------------------------------


#region  ---------------------------------------- BUILDING CONTENT ---------------------------------------------


/// <summary>
///Building Keyword Result Expression  4   [Start] & [Content] template Keywords, it means 4 KW Template Parts
///KW with TemplateType==[Start] - has (only 1) TemplatePart with const key {"StandartPart"}
///KW with TemplateType==[Content]  - can has (1 or more) TemplateParts with custom Keys
/// </summary>
/// <param name="Keyword"></param>
/// <param name="TemplatePart"></param>
protected virtual void BuildExpression4ContentKW(String Keyword, String TemplatePart = null)
{
    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[Keyword];
    TemplateInfo template = currentKeyword.Template;
    String Part = GetTemplatePartKey(currentKeyword.Templateword.TemplateType, TemplatePart);
    TemplatePartInfo tplPart;
    try
    {
        tplPart = template.TemplateParts[Part];
    }
    catch (System.Exception ex)
    {
        throw new ParseException(String.Format("BuildExpression4ContentKW() :  Cannot get Keyword[{0}]  TemplatePartKey[{1}]", Keyword,TemplatePart) );
    }
    

    BuildReferencedKeywords(tplPart);

    DoPartReplacements(tplPart);

    tplPart.ResultExpression.IsUpdated = true;
    // tplPart.ResultExpression.UpdateLevel
    // tplPart.ResultExpression.UpdateInitiatorKeyword

}

#endregion ---------------------------------------- BUILDING CONTENT ---------------------------------------------


#region  --------------------------------------- BUILDING  CYCLE ----------------------------------------

/// <summary>
/// Building Keyword Result Expression  4   [Cycle] template Keywords, it means 4 KW's first TemplatePart
/// KW with TemplateType==[Cycle] - has (only 1) TemplatePart with const key {"CYCLE"}
/// </summary>
/// <param name="Keyword"></param>
/// <param name="TemplatePart"></param>
protected virtual void BuildExpression4CycleKW(String Keyword, String TemplatePart = null)
{
    //[TEMPLATEPART: CYCLE( UpdateLMAction(LoadAction); FOREACH (LM:Entities) IN TEMPLATE(DefaultClass); CONDITION(TablesOnlyCondition); DELIMETER(,);  EachItemOnNewLine ;  )]
    //[TEMPLATEPART: CYCLE( FOREACH (TW:Interface.implementation) IN TEMPLATE(DefaultClass); CONDITION(TablesOnlyCondition); DELIMETER(,);  EachItemOnNewLine ; )]

    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[Keyword];
    CycleInfo cycleInfo = currentKeyword.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle;

    StringBuilder cycleCodeBuilder = new StringBuilder();

    string partkey = null;
    Int32 t = 0;

    if (cycleInfo.SourceType == SourceTypeEn.LoadedMetadata)
    {
        //UpdateLMAction(LoadAction) - //Place LM Item to the DPObjects dictionary
        if (cycleInfo.UpdateLMAction != null)
        {
            //UpdateLMMethods[cycleInfo.UpdateLMAction].Invoke(this, null);
        }

        partkey = GetTemplatePartKey(GenerationContext.PublishedKeywords[cycleInfo.ItemTemplate].Templateword.TemplateType, cycleInfo.ItemTemplatePart);

        //IN TEMPLATE(DefaultClass) -  ItemTemplate.Part
        TemplatePartInfo newItemTemplate = GenerationContext.PublishedKeywords[cycleInfo.ItemTemplate].Template.TemplateParts[partkey].Clone();



        //FOREACH (LM:Entities)
        IEnumerable LoadMetadataCollection = GenerationContext.DependentObjects[cycleInfo.CollectionKey] as IEnumerable;


        ///
        foreach (var item in LoadMetadataCollection)
        {
            t++;

            //CONDITION (item -Load Metadata Item)                   
            if (RunCycleLMItemCondition(cycleInfo, item) == false) continue;

            //UPDATING COLLECTION ITEM DATA 
            if (cycleInfo.CollectionItemKey != null && !GenerationContext.DependentObjects.ContainsKey(cycleInfo.CollectionItemKey))
            {
                GenerationContext.DependentObjects.Add(cycleInfo.CollectionItemKey, item);
            }
            else if (cycleInfo.CollectionItemKey != null && GenerationContext.DependentObjects.ContainsKey(cycleInfo.CollectionItemKey))
            {
                GenerationContext.DependentObjects[cycleInfo.CollectionItemKey] = item;
            }
                       

            //build ItemTemplate PartOfTemplate  -inside it ll build referencies ...
            BuildKeywordExpression(cycleInfo.ItemTemplate, partkey);  //rebuild for new metadata item                  


            //for 2-n item delimeter is -?
            //from new line/or current line            
            String itemExpressionEndResult = currentKeyword.Template.TemplateParts[partkey].ResultExpression.Expression
                                            + AddDelimeter(t, LoadMetadataCollection.Count(), cycleInfo.Delimeter)
                                            + AddNewLine(t, LoadMetadataCollection.Count(), cycleInfo.EachItemOnNewLine);


            cycleCodeBuilder.Append(itemExpressionEndResult);

        }

    }
    else if (cycleInfo.SourceType == SourceTypeEn.Templates)
    {
        //Get TEmplates with Type of Templateword = cycleInfo.CollectionKey
        List<KeywordInfo> templateCollection = GetTemplateCycleCollection(cycleInfo.CollectionKey);

        foreach (var item in templateCollection)
        {
            partkey = GetTemplatePartKey(item.Templateword.TemplateType, cycleInfo.ItemTemplatePart);

            t++;

            //CONDITION (item -Load Metadata Item)       
            if (RunCycleTemplateCondition(cycleInfo, item) == false) continue;


            //build current template
            BuildKeywordExpression(item.Keyword, partkey);  //rebuild for new metadata item                  

            //for 2-n item delimeter is -?
            //from new line/or current line           
            String itemExpressionEndResult = item.Template.TemplateParts[partkey].ResultExpression.Expression
                                            + AddDelimeter(t, templateCollection.Count, cycleInfo.Delimeter)
                                            + AddNewLine(t, templateCollection.Count, cycleInfo.EachItemOnNewLine);


            cycleCodeBuilder.Append(itemExpressionEndResult);

        }

    }


    if (currentKeyword.Template.TemplateParts[CTDLParser.CyclePartKey].ResultExpression == null)
    {
        currentKeyword.Template.TemplateParts[CTDLParser.CyclePartKey].ResultExpression = new ResultExpression();
    }

    // Update Keyword ExpressionResult 
    currentKeyword.Template.TemplateParts[CTDLParser.CyclePartKey].ResultExpression.Expression = cycleCodeBuilder.ToString();// SplitResult();
    currentKeyword.Template.TemplateParts[CTDLParser.CyclePartKey].ResultExpression.IsUpdated = true;


}


#endregion ---------------------------------------BUILDING CYCLE ----------------------------------------



#endregion ------------------------------ RECURSION  BUILD METHODS -------------------------
        

#region  --------------------------------------- CYCLE CONDITIONS------------------------------------------

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cycle"></param>
        /// <param name="keywordTarget"></param>
        /// <returns></returns>
        protected bool RunCycleTemplateCondition(CycleInfo cycle, KeywordInfo keywordTarget)
        {
            //CONDITION (item -KeywordInfo)
            if (cycle.ConditionKey != null)
            {
                return false; //(bool)ConditionsMethods[cycle.ConditionKey].Invoke(this, new object[] { keywordTarget });

            }
            return true;//including into Building
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cycle"></param>
        /// <param name="LMItem"></param>
        /// <returns></returns>
        protected bool RunCycleLMItemCondition(CycleInfo cycle, object LMItem)
        {
            //CONDITION (item -KeywordInfo)
            if (cycle.ConditionKey != null)
            {
                return false;
                  //  (bool)ConditionsMethods[cycle.ConditionKey].Invoke(this, new object[] { LMItem });

            }
            return true;//including into Building
        }


#endregion --------------------------------------- CYCLE CONDITIONS------------------------------------------
        

#region  -------------------------------- TOKENS -------------------------------


    /// <summary>
    /// Поcтроить все зависимые слова если они есть - т.е. это все референсы. Но для циклов референсы перестраиваются для каждого элемента цикла
    /// </summary>
    /// <param name="KeywordMaster"></param>
    protected void BuildReferencedKeywords(TemplatePartInfo tplPart)
    {
        //Для всех Ключ слов у которых  есть шаблоны
        if (tplPart == null) throw new ParseException("BuildReferencedKeywords: template part Cannot Be null");
                

        //Tree in Depth Counter -- statistic
        //if (tplPart.KeywordReferencies == null || tplPart.KeywordReferencies.Count > 0) return;// ExpressionTree.MaxLevel++;//

        try
        {
            // Для всех ссылок ключ слов  которые встречаются в коде шаблона построить в свою очередь Выражения этих партов по указаниям ссылок 
            foreach (var item in tplPart.KeywordReferencies)
            {
                BuildKeywordExpression(item.KeywordName, item.TemplatePartName);
            }

        }
        catch (System.Exception ex)
        {
        	
        }
    }


    /// <summary>
    /// Getting token keywords of current Writer
    /// </summary>
    /// <param name="Keyword"></param>
    /// <returns></returns>
    private Dictionary<String, KeywordInfo> GetTokenKeywords(String Keyword)
    {
        //Build tokens Keywords
        Dictionary<String, KeywordInfo> referencedKeywordsResults = new Dictionary<String, KeywordInfo>();
        foreach (var item in GenerationContext.PublishedKeywords[Keyword].Template.KeywordTokens)
        {
            referencedKeywordsResults.Add(item, GenerationContext.PublishedKeywords[item]);
        }

        return referencedKeywordsResults;
    }


    /// <summary>
    /// Reference is Keyword[.PartName]
    /// Getting Keyword (4 buid them before use in parent ) by TP tokens
    /// </summary>
    /// <param name="Part"></param>
    /// <returns></returns>
    private Dictionary<String, KeywordInfo> GetKeywordsByTokens(TemplatePartInfo Part)
    {
        Contract.Requires(Part != null, "To Get Part template tokens  itself cannot be null");

        //Build Referenced Keywords
        Dictionary<String, KeywordInfo> referencedKeywordsResults = new Dictionary<String, KeywordInfo>();
        foreach (var item in Part.KeywordReferencies)
        {
            if (!referencedKeywordsResults.ContainsKey(item.KeywordName))
                referencedKeywordsResults.Add(item.KeywordName, GenerationContext.PublishedKeywords[item.KeywordName]);
        }

        return referencedKeywordsResults;
    }

#endregion  -------------------------------- TOKENS -------------------------------


    #region  ------------------------------------ REPLACEMENTS --------------------------------------------

    /// <summary>
        /// Провести все замены в теле одного парта -TemplatePartInfo
        /// </summary>
        /// <param name="tplPart"></param>
        private void DoPartReplacements(TemplatePartInfo tplPart)
        {

            //если еще не построено первый раз результ выражение или же стоит флаг что нужно все таки обновить результ выражение - тогда перестраиваем выражение
            //if (tplPart.ResultExpression == null || tplPart.ResultExpression.IsUpdated == false)
            //{               

                string currentKeywordTemplate = tplPart.OriginalContent;

                //replacement  - reference is not the same as keyword name - it is keyword and part
                foreach (var item in tplPart.KeywordReferencies)
                {  

                    if ( item.KeywordObject.Templateword.TemplateType == TemplateTypeEn.Cycle  )
                    {
                        ResultExpression referenceContent = item.TemplatePartObject.ResultExpression;
                        if (referenceContent == null || referenceContent.IsUpdated == false)
                        {
                            throw new ParseException(String.Format("reference  [{0}] result Expression still was not updated", item.TokenWord));
                        }

                        //Attach SmartMargin to expression for current TemplatePart
                        string replacementFinalTemplate = UpdateExpressionBySmartMargin(referenceContent.Expression, item.SmartMargin);

                        //make replacement on expression of Content or Cycle Keyword
                        currentKeywordTemplate =  currentKeywordTemplate.Replace(item.Token, replacementFinalTemplate);
                        
                    }
                    else if (item.KeywordObject.Templateword.TemplateType == TemplateTypeEn.Content)
                    {
                        ResultExpression referenceContent = item.TemplatePartObject.ResultExpression;
                        if (referenceContent == null || referenceContent.IsUpdated == false)
                        {
                            throw new ParseException(String.Format("reference  [{0}] result Expression still was not updated", item.TokenWord));
                        }

                        //make replacement on expression of WithoutTemplate Keyword 
                        currentKeywordTemplate = currentKeywordTemplate.Replace(item.Token, referenceContent.Expression);
                    }
                    else if (item.KeywordObject.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                    {
                        //make replacement on expression of WithoutTemplate Keyword 
                        currentKeywordTemplate = currentKeywordTemplate.Replace(item.Token, item.KeywordObject.WithoutTemplateExpression);
                    }

                }

                //set currentKeywordTemplate
                if (tplPart.ResultExpression == null) tplPart.ResultExpression = new ResultExpression();
                tplPart.ResultExpression.Expression = currentKeywordTemplate;


            //}

        }
        
#endregion ---------------------------------- REPLACEMENTS --------------------------------------------


#region ------------------------------------- Expression Formula ------------------------------------------

///// <summary>
///// 1
///// </summary>
///// <param name="Content"></param>
///// <param name="index">from 2 till end item</param>
//private static String AddSmartMargin(String SmartMargin, Int32 index)
//{
//    if (index == 1) return null;
//    return SmartMargin;
//}

 
//2 item 
        

/// <summary>
/// 3 Delimeter 
/// </summary>
/// <param name="index"></param>
/// <param name="ItemsCount"></param>
/// <param name="Delimeter"></param>
/// <returns></returns>
private static String AddDelimeter(Int32 index, Int32 ItemsCount, String Delimeter = null)
{
    if (index >= 1 && index < ItemsCount) return Delimeter;
    return null;
}


/// <summary>
/// 4 NewLine
/// </summary>
/// <param name="index">From 1 till</param>
/// <param name="ItemsCount">Cont of items</param>
/// <param name="IsEachItemOnNewLine"></param>
/// <returns></returns>
private static String AddNewLine(Int32 index, Int32 ItemsCount, bool IsEachItemOnNewLine = false)
{
    if (index < ItemsCount && IsEachItemOnNewLine == true) return "\n";
    return null;
}


/// <summary>
/// Updating original cycle resultexpression by  Reference's SmartMargin
/// </summary>
/// <param name="ExpressionContent"></param>
/// <param name="SmartMargin"></param>
/// <returns></returns>
private static String UpdateExpressionBySmartMargin(String ExpressionContent, String SmartMargin)
{
    if (String.IsNullOrEmpty(SmartMargin)) return ExpressionContent;
    

    string UpdatedExpression = "";
    string[] partsofNewLines = ExpressionContent.Split('\n');


    Int32 i = 0;
    foreach (var part in partsofNewLines)
    {
        if (i == 0) // Начинать только со 2 части-второй строки
        {
            UpdatedExpression += part + "\n";
            i++; 
            continue; 
        }
 
        if (i == partsofNewLines.Length - 1)
        {
            UpdatedExpression += SmartMargin + part;
            i++;
            continue;
        }

        UpdatedExpression += SmartMargin + part + "\n";
        i++;
    }

    return UpdatedExpression;
}

#endregion ------------------------------------- Expression Formula ------------------------------------------



#region  ----------------------------------------- GET PARTS -------------------------------------------


        /// <summary>
        /// Получить  имя парта с учетом Типа шаблона
        /// </summary>
        /// <param name="tplword"></param>
        /// <param name="TemplatePart"></param>
        /// <returns></returns>
        private String GetTemplatePartKey(TemplateTypeEn type, String TemplatePart = null)
        {
            if (String.IsNullOrEmpty(TemplatePart) == false) return TemplatePart;

            if (type == TemplateTypeEn.Start || type == TemplateTypeEn.Content)
            {
                return CTDLParser.NotDeclaredPartKey;
            }
            else if (type == TemplateTypeEn.Cycle)
            {
                return CTDLParser.CyclePartKey;
            }
            else if (type == TemplateTypeEn.WithoutTemplate)
            {
                return null;
            }

            return TemplatePart;
        }







        /// <summary>
        /// Getting Keywords with Templates Collection
        /// </summary>
        /// <param name="templateWord"></param>
        /// <returns></returns>
        protected List<KeywordInfo> GetTemplateCycleCollection(String templateWord)
        {
            List<KeywordInfo> templateCollection = GenerationContext.PublishedKeywords.Values.Where(kwd => kwd.Templateword.Templateword == templateWord).ToList();

            return templateCollection;
        }

        #endregion ----------------------------------------- GET PARTS -------------------------------------------

        



#endregion -------------------------------- METHODS ---------------------------------



    }
}
