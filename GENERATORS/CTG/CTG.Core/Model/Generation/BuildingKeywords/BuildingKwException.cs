﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{

    /// <summary>
    /// Перечисление Правил Валидации для всего процесса Генерации с т.з.рения ошибок 
    /// </summary>
    public enum BuildingKwExceptionEn
    {        
        /// <summary>
        /// Не найден ни один шаблон генерации
        /// </summary>
        [Description("Не найден ни один шаблон генерации")]
        NotOneGenerationFlowWasFind,
        
       
        /// <summary>
        /// Не существует таблиц которые относятся к этому Ключу сервиса
        /// </summary>
        [Description("Не существует таблиц которые относятся к этому Ключу сервиса")]
        CantLoadTablesWithServiceKey

    }

    /// <summary>
    /// Ошибка по Правилу Валидации 
    /// </summary>
    public class BuildingKwRuleException: Exception
    {
        public BuildingKwRuleException(String Message)
            : base(Message)
        {

        }


        public BuildingKwRuleException(String Message, BuildingKwExceptionEn validationRuleCode)
            : base(Message)
        {
            Rule = validationRuleCode;

        }



        public BuildingKwRuleException(BuildingKwExceptionEn validationRuleCode)
            : base(validationRuleCode.GetDescription())
        {
            Rule = validationRuleCode;

        }

        
        #region     ----------------------------PROP ITEM  :  Rule ---------------------------------
        // PARAMS :
        // PropName   -      Rule         EX: AddNewButton     
        // PropType   -      ValidationRuleExceptionsEn         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Правило Валидации - Детерминированный список проверок - при загрзке генератора/старте генерации/и ошибках и условиях в коде шаблонов           EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Правило Валидации - Детерминированный список проверок на работоспособность - при загрузке генератора/старте генерации/и ошибках и условиях в коде шаблонов  
        /// </summary>        
        public BuildingKwExceptionEn Rule
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  Rule ---------------------------------
    
     
    


    }
}
