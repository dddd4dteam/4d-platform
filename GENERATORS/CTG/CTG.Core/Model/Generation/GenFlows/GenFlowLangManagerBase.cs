﻿
using CTG.Configuration;
using CTG.EnumConfiguration;
using NMG.Core.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    public  class GenFlowLangManagerBase : IGenFlowLangManager
    {

        #region -------------------------CTOR------------------------

        public GenFlowLangManagerBase()
        {
            Initialize();
        }

        #endregion -------------------------CTOR------------------------
        //CONFIGURATION
        //public const string BaseConfigFile = "GenFlowLangManagerConfiguration.gflmcfg";
        
        /// <summary>
        /// Generation(G) Flow(F) Language(L) Manager(M) configuration file- (GFLMCfg) extension
        /// </summary>
        public const string ConfigFileExtension = ".gflmcfg";

        /// <summary>
        /// Auto composed Manager's config file Name from [GenerationFlow(Folder)].[ConfigFileExtension]
        /// </summary>
        public static readonly String StartConfigFileName = String.Format("GenFlowLangManagerConfiguration{0}", ConfigFileExtension);
       

        //STANDART Configuration Settings Key
        public const String ST_LM_ServiceKey = "LM_ServiceKey";
        public const String ST_LM_Suffix_Key = "LM_Suffix";
        public const String ST_LM_Mask_Key = "LM_Mask";
        public const String ST_OutputFileExtension_Key = "OutputFileExtension";
        public const String ST_OutputFolder_Key = "OutputFolder";


        public bool IsSelected
        {
            get; private set;
        }



        #region  -------------------------------Base MEF Info and Name --------------------------------

        /// <summary>
        /// Base information about current GenFlow from MEF attribute 
        /// </summary>
        public GenFlowLangManagerAttribute Info
        {
            get;
            private set;
        }


        /// <summary>
        /// Название Сценария Потока Генерации
        /// </summary>
        public string Name
        {
            get { return Info.Name; }
        }

        #endregion  -------------------------------Base MEF INfo and Name --------------------------------
        

        #region ---------------------- CONFIGURATION -------------------------------

        GenFlowEndConfigurationTp _Configuration = null;
        /// <summary>
        /// Конфигурация  Потока Генерации
        /// </summary>
        public GenFlowEndConfigurationTp Configuration
        {
            get { return _Configuration; }
            set
            {
                _Configuration = value;
                //OnConfigurationChanged(_Configuration);
            }
        }

        #endregion ---------------------- CONFIGURATION -------------------------------
        

        #region ------------------- IO PROPERTIES ------------------------------

        #region     ----------------------------PROP ITEM  :  (FileInfo) StartTemplateFile   ---------------------------------
        // PARAMS :
        // PropName   -      StartTemplateFile         EX: AddNewButton     
        // PropType   -      FileInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Файл стартового шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Файл стартового шаблона
        /// </summary>        
        public FileInfo StartTemplateFile
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (FileInfo) StartTemplateFile ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) OutputFileExtension   ---------------------------------
        // PARAMS :
        // PropName   -      OutputFileExtension         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Расширение выходного файла. Обычно равно Расширению папки в которой находится шаблон но при детерминированом стандартном параметре сеттингов оно меняется на пользовательское  EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Расширение выходного файла
        /// </summary>        
        public String OutputFileExtension
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) OutputFileExtension ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) OutputFolder   ---------------------------------
        // PARAMS :
        // PropName   -      OutputFolder         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Папка в которую должна происходить генерация         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Папка в которую должна происходить генерация
        /// </summary>        
        public String OutputFolder
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) OutputFolder ---------------------------------


        #region     ----------------------------PROP ITEM  :  (Dictionary<string, CodeBuilder>) CodeWriters   ---------------------------------
        // PARAMS :
        // PropName   -      CodeWriters         EX: AddNewButton     
        // PropType   -      Dictionary<string, CodeBuilder>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Построители Кода которые сформированы генератором для работы процесса Генерации         EX: Adding New Button To the Control Buttons Collection   

        #endregion  -------------------------- PROP ITEM  : (Dictionary<string, CodeBuilder>) CodeWriters ---------------------------------


        #endregion ------------------- IO PROPERTIES ------------------------------


        #region  ------------------- GENFLOW EXTENSION LOGIC METHODS -----------------------

        /// <summary>
        /// Publishing Methods 
        /// </summary>
        protected Dictionary<String, MethodInfo> PublishKeywordMethods
        {
            get;
            set;
        }


        /// <summary>
        /// Build Keyword Methods
        /// </summary>
        protected Dictionary<String, MethodInfo> BuildKeywordMethods
        {
            get;
            set;
        }
        
        /// <summary>
        /// Методы с проверкой каких- то условий - для элемента цикла(пока здесь) ...
        /// </summary>
        protected Dictionary<String, MethodInfo> ConditionsMethods
        {
            get;
            set;
        }


        /// <summary>
        /// Методы с обновлением Метаданных которые возможно понадобиятся запустить при старте цикла
        /// </summary>
        protected Dictionary<String, MethodInfo> UpdateLMMethods
        {
            get;
            set;
        }


        #endregion ------------------- GENFLOW EXTENSION LOGIC METHODS -----------------------
                

        #region     ----------------------------PROP ITEM  :  (GenerationFlowAnaliser) Templates Analiser   ---------------------------------
        // PARAMS :
        // PropName   -      Analiser         EX: AddNewButton     
        // PropType   -      GenerationFlowAnaliser         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Анализатор Шаблонов Потока Генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Анализатор Шаблонов Потока Генерации
        /// </summary>        
        public GenerationFlowAnalyser Analiser
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (GenerationFlowAnaliser) Templates Analiser ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (GenerationContext) Context   ---------------------------------
        // PARAMS :
        // PropName   -      Context         EX: AddNewButton     
        // PropType   -      GenerationContext         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Контекст генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Контекст генерации
        /// </summary>        
        public GenerationContext Context
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (GenerationContext) Context ---------------------------------



        #region     ----------------------------PROP ITEM  :  (KeywordBuilderBase) Builder   ---------------------------------
        // PARAMS :
        // PropName   -      Writer         EX: AddNewButton     
        // PropType   -      KeywordWriterBase         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Писатель который  занимается прописывание  всех шаблончиков по алгоритму Потока - по стартовому файлу шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Писатель который  занимается прописывание  всех шаблончиков по алгоритму Потока - по стартовому файлу шаблона
        /// </summary>        
        public KeywordBuilder Builder
        {
            get;
            private set;
        }

        #endregion  -------------------------- PROP ITEM  : (KeywordBuilderBase) Builder  ---------------------------------



        #region     ----------------------------PROP ITEM  :  (Dictionary<string, CodeWriter>) CodeWriters   ---------------------------------
        // PARAMS :
        // PropName   -      CodeWriters         EX: AddNewButton     
        // PropType   -      Dictionary<string, CodeWriter>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - protected    EX: internal
        // PropDesc  -       Писатели Кода в файл         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Писатели Кода в файл
        /// </summary>        
        public Dictionary<string, CodeWriter> CodeWriters
        {
            get;
            protected set;
        }

        #endregion  -------------------------- PROP ITEM  : (Dictionary<string, CodeWriter>) CodeWriters ---------------------------------



        #region     ----------------------------PROP ITEM  :  (Version) CurrentVersion   ---------------------------------
        // PARAMS :
        // PropName   -      CurrentVersion         EX: AddNewButton     
        // PropType   -      Version         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Текущая версия  класса с логикой- Потока Генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Текущая версия  класса с логикой- Потока Генерации
        /// </summary>        
        public Version CurrentVersion
        {
            get;
            private set;
        }

        #endregion  -------------------------- PROP ITEM  : (Version) CurrentVersion ---------------------------------



        #region     ----------------------------PROP ITEM  :  (LanguageEn) SupportLanguage ---------------------------------
        // PARAMS :
        // PropName   -      SupportLanguages         EX: AddNewButton     
        // PropType   -      List<LanguageEn>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Поддерживаемые языки - исходя из конфигурации данного Потока Генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Поддерживаемые языки - исходя из конфигурации данного Потока Генерации, из вариантов стартовых шаблонов в которых указывается язык программирования.
        /// </summary>        
        public LanguageEn SupportLanguage
        {
            get;
            private set;
        }

        #endregion  -------------------------- PROP ITEM  : (List<LanguageEn>) SupportLanguage ---------------------------------





        public GenerationFlowBase HostGenerationFlow
        {
            get;
            private set;
        }


     








        #region ----------------------- CONFIGURATION CHANGING HANDLER ---------------------------


        /// <summary>
        /// Loading Configuration for GenFlow Extension(.dll in Extensions Folder)
        /// </summary>
        public void LoadConfiguration()
        {

            string flowConfigFile = CTGConfigEn.BaseExtensionsPath.GetScalarValue<String>() + @"\" + Info.Name + ".config";
            Contract.Assert(File.Exists(flowConfigFile) == false, String.Format(" GenFlow [{}] cannot load it's  configuration file with name {1}", Info.Name, Info.Name + ".config"));


            Configuration = GenFlowEndConfigurationTp.LoadFromFile(flowConfigFile);

        }


        /// <summary>
        /// Changing and Initing Base 
        /// </summary>
        /// <param name="config"></param>
        protected virtual void OnConfigurationChanged(GenFlowEndConfigurationTp config)
        {
            if (config == null) return;



            //set StartTemplate File 
            StartTemplateFile = config.EnabledStartTemplates.StartItems.FirstOrDefault().GetStartTemplateFile();// GetStartTemplate().GetStartTemplateFile();

            //Updating directory for CTGen on run current GenFlow
            CTGConfigEn.BaseOutputPath.UpdateValue(StartTemplateFile.Directory.FullName + @"\");// CurrentTemplateDirectory.TryUpdateValue(StartTemplateFile.Directory.FullName + @"\");
            //Option Use SmartMargin
            CTGConfigEn.UseReferenceSmartMargin.UpdateValue(true);


            //set output file extension current - from config or from GenFlow Language Folder 
            OutputFileExtension = ".cs";

            // set OutputFolder from config only or throw exception
            if (config.GenerationSettings.ContainsKey(ST_OutputFolder_Key) == false)
                throw new ParseException("Output Folder Setting Key was not found in Gen Flow Configuration file");
            OutputFolder = config.GenerationSettings.GetValueByKey(ST_OutputFolder_Key);

            CTGConfigEn.BaseOutputPath.UpdateValue(OutputFolder);
        }

        #endregion ----------------------- CONFIGURATION CHANGING HANDLER ---------------------------




        #region ----------------------------- INITIALIZATION --------------------------

        /// <summary>
        /// Инициализация Сценария потока генерации
        /// </summary>
        protected void Initialize()
        {
            //All keyword Writers should me marked with KeywordWriterAttribute attribute
            Info = GetType().GetTypeAttribute<GenFlowLangManagerAttribute>().FirstOrDefault();

            InitGFLogicMethods();

            CodeWriters = new Dictionary<string, CodeWriter>();
            //LoadedTables = new List<Table>();
            //DependentObjects = new 

            //Collect Published KeywordWriters -keywords 
            //.PublicationManager.




            Context.RegisterWriterKeywords(GetPublishingKeywords());


        }



        private void InitGFLogicMethods()
        {

            PublishKeywordMethods = new Dictionary<String, MethodInfo>();
            BuildKeywordMethods = new Dictionary<String, MethodInfo>();
            ConditionsMethods = new Dictionary<String, MethodInfo>();
            UpdateLMMethods = new Dictionary<String, MethodInfo>();


            MethodInfo[] PublishKeywordMeths = GetType().GetMethods().Where(mt => mt.GetCustomAttribute<PublishKeywordActionAttribute>() != null).ToArray();
            foreach (var item in PublishKeywordMeths)
            {
                PublishKeywordActionAttribute pblshInfo = item.GetMethodAttribute<PublishKeywordActionAttribute>().FirstOrDefault();
                PublishKeywordMethods.Add(pblshInfo.Keyword, item);
            }


            MethodInfo[] WriteKeywordMeths = GetType().GetMethods().Where(mt => mt.GetCustomAttribute<BuildKeywordExpressionActionAttribute>() != null).ToArray();
            foreach (var item in WriteKeywordMeths)
            {
                BuildKeywordExpressionActionAttribute wrtMethdInfo = item.GetMethodAttribute<BuildKeywordExpressionActionAttribute>().FirstOrDefault();
                BuildKeywordMethods.Add(wrtMethdInfo.Keyword, item);
            }


            MethodInfo[] ConditionMeths = GetType().GetMethods().Where(mt => mt.GetCustomAttribute<CycleConditionFuncAttribute>() != null).ToArray();
            foreach (var item in ConditionMeths)
            {
                CycleConditionFuncAttribute conditionMethdInfo = item.GetMethodAttribute<CycleConditionFuncAttribute>().FirstOrDefault();
                ConditionsMethods.Add(conditionMethdInfo.ConditionAction, item);
            }


            MethodInfo[] UpdateLMMeths = GetType().GetMethods().Where(mt => mt.GetCustomAttribute<CycleUpdateLMActionAttribute>() != null).ToArray();
            foreach (var item in ConditionMeths)
            {
                CycleUpdateLMActionAttribute updateLMMethdInfo = item.GetMethodAttribute<CycleUpdateLMActionAttribute>().FirstOrDefault();
                UpdateLMMethods.Add(updateLMMethdInfo.UpdateAction, item);
            }

        }

        #endregion ----------------------------- INITIALIZATION --------------------------






        #region  ------------------------------ INIT CODEWRITERS -----------------------------

        public void PrepareCodeWriters()
        {
            //creating code builders based on loaded tables & type of Generation

            if (Configuration.Type == GenerationTypeEn.FisrtEntity)
            {
                Entity firstEntity = CTGen.LoadedEntities.First().Value;
                //Prepare generation files and their builders
                CodeWriter newbuilder = new CodeWriter()// CodeBuilder()
                {
                    Key = firstEntity.LoadedTable.Name,
                    Builder = new StringBuilder(),
                    GenerationType = Configuration.Type,
                    StartTemplate = StartTemplateFile.Name,
                    StartKeyword = firstEntity.Name.ToLower(),// "EntityPerFile3"
                    FileExtension = OutputFileExtension,
                    EntityName = firstEntity.LoadedTable.Name,
                    FileName = firstEntity.LoadedTable.Name + OutputFileExtension,
                    FilePath = OutputFolder + firstEntity.LoadedTable.Name + OutputFileExtension
                };

                //Code Builder 
                CodeWriters.Add(newbuilder.Key, newbuilder); // Builders.Add(newbuilder.Key, newbuilder);
            }
            else if (Configuration.Type == GenerationTypeEn.EntitiesInOneFile)
            {
                CodeWriter newbuilder = new CodeWriter()
                {
                    Key = "AllEntitiesInOneFile",
                    Builder = new StringBuilder(),
                    GenerationType = Configuration.Type,
                    StartTemplate = StartTemplateFile.Name,
                    StartKeyword = StartTemplateFile.Name.ToLower(),// "EntityPerFile3"
                    FileExtension = OutputFileExtension,
                    EntityName = "AllEntitiesInOneFile",
                    FileName = "AllEntitiesInOneFile" + OutputFileExtension,
                    FilePath = OutputFolder + "AllEntitiesInOneFile" + OutputFileExtension
                };

                CodeWriters.Add(newbuilder.Key, newbuilder);

            }
            else if (Configuration.Type == GenerationTypeEn.EntitiesPerFile)
            {

            }

        }




        /// <summary>
        /// Из папки шаблона Потока Генерации подгружаются все шаблончики - и получаем набор Keyword-ов из шаблонов из 2-й стороны(1-я из писателей)
        /// </summary>
        public void LoadAnalyseTemplates()
        {
            FileInfo startTemplateFile = new FileInfo(CTGConfigEn.BaseExtensionsPath.GetScalarValue<String>()); // StartTemplateFile.GetCfgScalarValue<String>());
            if (startTemplateFile.Exists == false)
            {
                throw new ParseException(string.Format("Start Template  File  {0} doesn't exist. Check directory", startTemplateFile.FullName));
            }

            DirectoryInfo genflowdirectory = startTemplateFile.Directory;

            FileInfo[] directoryFiles = genflowdirectory.GetFiles();
            if (directoryFiles == null || directoryFiles.Length == 0) return;

            FileInfo[] onlytemplateFiles = directoryFiles.Where(t => t.Extension == CTGConfigEn.BaseExtensionsPath.GetScalarValue<String>()).ToArray();// TemplateFileExtensions.GetCfgScalarValue<String>()).ToArray();

            //Updating Directory 
            CTGConfigEn.BaseOutputPath.UpdateValue(genflowdirectory.FullName); // CurrentTemplateDirectory

            foreach (var item in onlytemplateFiles)
            {
                KeywordInfo keyword = CTDLParser.ParseKeywordTemplate(item.FullName);

                GenerationContext.Keywords.Add(keyword.Keyword, keyword);
            }

        }

        public void SetGenerationItems()
        {
            //Add CurrentEntity to DPObjects Dictionary
            Entity firstEntity = GenerationContext.Entities.First().Value;
            if (GenerationContext.DependentObjects.ContainsKey(DPKeysEn.CurrentEntity.Str()) == false)
            {
                GenerationContext.DependentObjects.Add(DPKeysEn.CurrentEntity.Str(), firstEntity);
            }

            if (GenerationContext.DependentObjects.ContainsKey(DPKeysEn.EntityProperties.Str()) == false)
            {
                GenerationContext.DependentObjects.Add(DPKeysEn.EntityProperties.Str(), firstEntity.LoadedTable.Columns);
            }

        }

        #endregion ------------------------------ INIT CODEWRITERS -----------------------------


        /// <summary>
        /// Проверка доступности всех слов - т.е.тех  которые 1-сами шаблоные ключевые слова + 2-все ключевые слова найденные в шаблонах(а сами шаблонов не имеющие) 
        /// - досутупных к построению Писателем ,они должны быть опубликованы в списке PublicationManager.PublishedKeywords
        /// </summary>
        public void ValidatePreGeneration()
        {
            // Check If Published  Keywords  enough and Collected in templates. Validation rule to  start Generation Dictionary<string, KeywordInfo> CollectedKeywords
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (!GenerationContext.PublishedKeywords.ContainsKey(item.Key))
                {
                    throw new ParseException(" Not all keyword enable to get keywordwriter buid expression");
                }
            }
        }

        /// <summary>
        /// ОБъединить данные в PublishedKeywords  из  PublishRequireKeywords
        /// </summary>
        public void MergePublishedKeyword()
        {
            //mergetemplates
            foreach (var item in GenerationContext.PublishedKeywords)
            {
                item.Value.IsEnable = true;
                if (GenerationContext.PublishRequireKeywords.ContainsKey(item.Key) && GenerationContext.PublishRequireKeywords[item.Key].Template != null)
                {
                    item.Value.Template = GenerationContext.PublishRequireKeywords[item.Key].Template.Clone();
                }
            }
        }


        public void BuildKeyword(string Keyword, String TemplatePart = null)
        {

            string currentKwdName = Keyword;

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[Keyword];

            // let KeywordWriter to do it's Job
            Builder.BuildKeywordExpression(currentKeyword.Keyword, TemplatePart);
            //Writer.BuildKeywordExpression(currentKeyword.Keyword, TemplatePart);


        }



        #region  ---------------------------------RUNNING GENERATION FLOW -------------------------------------

        /// <summary>
        /// Запуск Сценария Потока Генерации
        /// </summary>
        internal void RunGeneration()
        {
            foreach (var item in CodeWriters)
            {
                if (item.Value.GenerationType == GenerationTypeEn.EntitiesPerFile)
                {
                    GenerateForEntityPerFile(item.Value);

                }
                else if (item.Value.GenerationType == GenerationTypeEn.EntitiesInOneFile)
                {
                    GenerateForEntitiesInOneFile(item.Value);
                }
            }
        }



        /// <summary>
        /// Генерировать Код по варианту Для Сущности по Файлу
        /// </summary>
        /// <param name="builder"></param>
        protected virtual void GenerateForEntityPerFile(CodeWriter builder)
        {
            //спустить контент для генерации в словарь DependecyObjects - Entity 
            if (!GenerationContext.DependentObjects.ContainsKey(DPKeysEn.CurrentEntity.Str()))
            {
                GenerationContext.DependentObjects.Add(DPKeysEn.CurrentEntity.Str(), GenerationContext.Entities[builder.EntityName]);
            }
            else
            {
                GenerationContext.DependentObjects[DPKeysEn.CurrentEntity.Str()] = GenerationContext.Entities[builder.EntityName];
            }

            //Приступить к генерации с данного стартового слова
            KeywordInfo startKeyword = GenerationContext.PublishedKeywords[builder.StartKeyword];

            BuildKeyword(startKeyword.Keyword); // вся генерация            

            //обновление CodeBuilder
            builder.Builder.Append(startKeyword.Template.TemplateParts[CTDLParser.NotDeclaredPartKey].ResultExpression.Expression); //.ToOneString()

            //обновление расшаренного списка builder-ов
            //CodeWriters[builder.Key] = builder;
        }


        /// <summary>
        /// Генерировать Код по варианту Для Всех Сущностей в один Файл
        /// </summary>
        /// <param name="builder"></param>
        protected virtual void GenerateForEntitiesInOneFile(CodeWriter builder)
        {

        }


        #endregion ---------------------------------RUNNING GENERATION FLOW -------------------------------------




        /// <summary>
        /// Creating Default Generation Flow Configuration. To create  
        /// </summary>
        /// <param name="StartTemplateFile"></param>
        /// <param name="ConnectionString"></param>
        /// <param name="MappedKeywords"></param>
        public static void CreateDefaultGenFlowConfig(String GenFlowName, StartTemplateCollectionTp StartTemplates, ConnectionTp DefaultConnection = null, KeywordMapCollectionTp MappedKeywords = null, SettingCollectionTp settings = null)
        {

            GenFlowEndConfigurationTp config = new GenFlowEndConfigurationTp();
            config.Name = GenFlowName;

            // Start Templates - with Default Template
            if (StartTemplates != null)
            {
                config.EnabledStartTemplates = StartTemplates;// = StartTemplates;
                CTGConfigEn.BaseOutputPath.UpdateValue(StartTemplates.GetStartTemplate().FullName);// StartTemplateFile.TryUpdateValue(StartTemplates.GetStartTemplate().FullName);// Get Templates[0] @StartTemplateFile);
            }

            // MappedKeywords
            if (MappedKeywords != null && MappedKeywords.KWMaps.Count > 0)
            {
                config.KeywordMapping = MappedKeywords;
            }

            ////Connections - default Connection
            if (DefaultConnection != null)
            {
                config.ServerConnections.Connects.Add(DefaultConnection);
            }
            else
            {
                // DefaultConnectKey, Server = GenerationFlowBase.DefaultServerType, ConnectString = GenerationFlowBase.DefaultConnectionString, IsDefault = true });
                config.ServerConnections.Connects.Add(new ConnectionTp() { Key = GenFlowLangManagerBase.StartConfigFileName, Server = ServerTypeEn.SqlServer2012, ConnectString = GenFlowLangManagerBase.ST_LM_Suffix_Key, IsDefault = true });
            }


            // Settings - some Settings 
            if (settings != null && settings.Settings.Count > 0)
            {
                config.GenerationSettings = settings;
            }



            //CTGen generator = new CTGen();     
            CTGen.Current.LoadMetaData();
            CTGen.Current.SelectGenerationFlow(null);//           generator.LoadGenFlowTemplates();
            
            //attach lang managers to it
            //CTGen.Current.CurrentGenFlow.LoadAnalyseTemplates();// Parse Analiser.Ana // GenerationFlowAnaliser.AnalyzeComponents();
            //load |analyse |mergePublishrd=ed keywords-? 
            //CTGen.Current.CurrentGenFlow.MergePublishedKeyword(); //generator.MergePublishedKeyword();



            //Use Options from analyzed templates 
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                config.KeywordUseOptions.KWUseOptions.Add(new KeywordUseOptionTp() { Keyword = item.Key, IsEnable = true });
            }



            config.SaveToFile(CTGConfigEn.BaseExtensionsPath.GetScalarValue<String>() + @"\" + GenFlowLangManagerBase.StartConfigFileName);//  CurrentTemplateDirectory.GetCfgScalarValue<String>() + @"\"+ GenerationFlowBase.BaseConfiFile);



        }



        /// <summary>
        /// Генерируем конечный код 
        /// </summary>
        public void Generate()
        {
            try
            {
                //Load all Templates from GenFlow Folder
                //LoadMetaData(ServiceKey: "S10");

                //BuildGenerationEntities();

                //SetDefaultParams();//temporary method    

                //LoadGenFlowTemplates();

                //AnalyzeReferencies();               

                //ValidatePreGeneration();

                //MergePublishedKeyword();

                //Build 
                RunGeneration();

                //Write to output
                foreach (var item in CodeWriters) // Builders
                {
                    string FinalFileContent = item.Value.Builder.ToString();
                    FinalFileContent = FinalFileContent.Replace("\r", "");
                    File.WriteAllText(item.Value.FilePath, FinalFileContent);
                }

            }
            catch (ParseException prsExc)
            {
                throw prsExc;
            }
            catch (Exception)
            {
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        public async void GenerateWithFlow(String GenFlowFolder)
        {

            Task generationTask = Task.Factory.StartNew(() =>
            {
                // load all GenFlowConfiguration from  GenerationFlow Folder
                if (Directory.Exists(GenFlowFolder) == false)
                    throw new ParseException("Generation Flow folder doesn't exist");

                if (File.Exists(GenFlowFolder + @"\" + GenFlowLangManagerBase.StartConfigFileName) == false)
                    throw new ParseException("Generation Flow folder exist but  config file doesn't exist");


                // update  
                GenFlowEndConfigurationTp config = GenFlowEndConfigurationTp.LoadFromFile(GenFlowFolder + @"\" + GenFlowLangManagerBase.StartConfigFileName);
                if (String.IsNullOrEmpty(config.Name) == true)
                    throw new ParseException("Generation Flow configuration doesn't contains Name of  MEF associated class");


                GenerationFlowBase existingGenerationFlow = CTGen.Current.CurrentGenFlow;// CTGComposition.Current.GetGenerationFlow(config.Name) as GenerationFlowBase;
                if (existingGenerationFlow == null)
                    throw new ParseException("GenerationFlow configuration pointing to the MEF class that doesn't published");

                //updating configuration of published MEF class
                //existingGenerationFlow.Configuration = config;


                //filtering only list of entities to generation context           


                //Loading Gen Flow Templates 
                //CTGen.Current.CurrentGenFlow.LoadAnalyseTemplates(); //existingGenerationFlow. LoadGenFlowTemplates();
                //GenerationFlowAnaliser.AnalyzeComponents();                

                //existingGenerationFlow.MergePublishedKeyword();


                //
                //existingGenerationFlow.PrepareCodeWriters();// PrepareCodeBuilders();

                //existingGenerationFlow.SetGenerationItems();
                //SetDefaultParams();//temporary method    

                //existingGenerationFlow.RunGeneration();



                // ValidatePreGeneration - Check all published keywords >= all KeywordReferencies
                // MergePublishedWritersKeywords&LoadedTemplateKeywords into one GenerationContext.Keywords list   

                //Build 
                //GenerationContext.GenerationFlow. 


            });

            await generationTask;

            if (generationTask.Status == TaskStatus.Faulted)
            {
                //Log  about  crash
            }


        }





        // <summary>
        /// Заносит свои слова генерации к PublicationManager- у. Но набор который участвует в построении при самой генерации - это GenerationContext.PublishRequireKeywords
        /// Get published Keywords of current Writer
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, KeywordInfo> GetPublishingKeywords()
        {
            Dictionary<String, KeywordInfo> writerKeywords = new Dictionary<String, KeywordInfo>();

            foreach (var item in PublishKeywordMethods)
            {
                KeywordInfo methodKeyword = (KeywordInfo)item.Value.Invoke(this, null);

                writerKeywords.Add(methodKeyword.Keyword, methodKeyword);
            }

            return writerKeywords;
        }










       
    }
}
