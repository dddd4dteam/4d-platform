﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{

    /// <summary>
    /// Перечисление ошибок с загрузкой и заполнением конфигурации для Потока Генерации 
    /// </summary>
    public enum GenFlowConfigurationExceptionEn
    {   
       // GF Configuraiton was not finded
       // GF Configuration read Fault
       // GF Configuration has No StartItems
       // GF Configuration has No DefaultStartItems
    
    }

    /// <summary>
    /// Ошибка  с конфигурацией GF 
    /// </summary>
    public class GenFlowConfigurationException: Exception
    {
        public GenFlowConfigurationException(String Message)
            : base(Message)
        {

        }


        public GenFlowConfigurationException(String Message, GenFlowConfigurationExceptionEn validationRuleCode)
            : base(Message)
        {
            Rule = validationRuleCode;

        }



        public GenFlowConfigurationException(GenFlowConfigurationExceptionEn validationRuleCode)
            : base(validationRuleCode.GetDescription())
        {
            Rule = validationRuleCode;

        }

        
        #region     ----------------------------PROP ITEM  :  Rule ---------------------------------
        // PARAMS :
        // PropName   -      Rule         EX: AddNewButton     
        // PropType   -      ValidationRuleExceptionsEn         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Правило Валидации - Детерминированный список проверок - при загрзке генератора/старте генерации/и ошибках и условиях в коде шаблонов           EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Правило Валидации - Детерминированный список проверок на работоспособность - при загрузке генератора/старте генерации/и ошибках и условиях в коде шаблонов  
        /// </summary>        
        public GenFlowConfigurationExceptionEn Rule
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  Rule ---------------------------------
    
     
    


    }
}
