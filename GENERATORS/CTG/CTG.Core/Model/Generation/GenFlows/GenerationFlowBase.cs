﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics.Contracts;
using System.Reflection;

using NMG.Core.Domain;
using NMG.Core.Reader;
using NMG.Core;

using CTG;
using CTG.Configuration;



namespace CTG.Core
{


    public enum GenFlowStateEn
    {

        Created,


    }

    /// <summary>
    /// 
    /// </summary>
    public  class GenerationFlowBase //: IGenerationFlow
    {

        #region ------------------------------ CTOR -----------------------------------
        /// <summary>
        /// If DirectoryPath is null we'll Discover 
        /// </summary>
        /// <param name="DirectoryPath"></param>
        public GenerationFlowBase( String DirectoryPath=null)
        {
            //parse and Get Name of current Generation Flow
        }

        #endregion ------------------------------ CTOR -----------------------------------


      
        
        
        //CONNECTION
        //public const string DefaultConnectKey = "DefaultConnection";
        //public const ServerTypeEn DefaultServerType = ServerTypeEn.SqlServer2012;
        //public const String DefaultConnectionString = @"Server=.;Database=WarehouseDB;User ID=usrlogin;Password=pswdKey;Trusted_Connection=False;Integrated Security=true;";


      

        #region ------------------------ PROPERTIES ---------------------------


        /// <summary>
        /// Genereation Flow Name - FolderName 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Full Path to the FOlder of current Generation Flow
        /// </summary>
        public String FullPath
        {
            get;
            set;
        }



        /// <summary>
        /// GenFlow Language Managers wich can start generation for some language 
        /// </summary>
        public List<IGenFlowLangManager> Managers
        {
            get;
            set;
        }




        /// <summary>
        /// Selected to run on the next
        /// </summary>
        public IEnumerable<IGenFlowLangManager> SelectedManager
        {
            get
            {
                return Managers.Where(mg => mg.IsSelected);
            }
        }


        #endregion ------------------------ PROPERTIES ---------------------------







#region  ----------------------------- METHODS ---------------------------------
        


        //Filter all Tables to generation filters 
        //if (ServiceKey != null)
        //        {
        //            List<Table> serviceTables = new List<Table>();
        //            String lowerServiceKey = ServiceKey.ToLower();

        //            foreach (var item in LoadedTables)
        //            {
        //                if (item.Name.ToLower().Contains(lowerServiceKey))
        //                {
        //                    serviceTables.Add(item);
        //                }
        //            }

        //            //IEnumerable<Table> serviceTables = SourcesLoadedTables[source].Where(t => t.Name.Contains(ServiceKey));
        //            if (serviceTables.Count == 0)
        //            {
        //                throw new ValidationRuleException(ValidationRuleExceptionsEn.CantLoadTablesWithServiceKey);
        //            }


        //            // Service table
        //            foreach (var item in serviceTables)
        //            {
        //                item.Columns = MetadataReader.GetTableDetails(item, item.Owner);
        //                item.PrimaryKey = MetadataReader.DeterminePrimaryKeys(item);
        //                item.ForeignKeys = MetadataReader.DetermineForeignKeyReferences(item);
        //            }

        //            BuildGenerationEntities(serviceTables);
        //        }


        internal void Generate()
        {

        }

      


#endregion ----------------------------- METHODS ---------------------------------

        









    }
}
