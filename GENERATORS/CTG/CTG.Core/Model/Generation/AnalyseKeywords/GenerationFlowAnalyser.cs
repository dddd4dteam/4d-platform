﻿using CTG.Core.Parsing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    public class GenerationFlowAnalyser
    {
        /// <summary>
        /// Проанализировать маркеры -tokens   в кодах шаблонов
        /// </summary>
        public static void AnalyzeComponents( GenerationContext context)
        {
            //Contract.Requires()

            //GenerationContext.PublishRequireKeywords.Clear();

            foreach (var keyword in GenerationContext.Keywords)
            {
                foreach (var item in keyword.Value.Template.KeywordTokens)
                {
                    if (!GenerationContext.KeywordReferencies.ContainsKey(item))
                    {                       
                        GenerationContext.KeywordReferencies.Add(item, null ); //keyword.Value.Clone()    
                    }
                }
            }


            // merged keywords & KeywordReferencies list - PublishRequire list
            foreach (var keyword in GenerationContext.Keywords)
            {
                if (!GenerationContext.PublishRequireKeywords.ContainsKey(keyword.Key))
                {
                    GenerationContext.PublishRequireKeywords.Add(keyword.Key, keyword.Value.Clone());
                }
            }

            foreach (var keyword in GenerationContext.KeywordReferencies)
            {
                if (keyword.Key == "defaultbaseclass")
                {
                }

                if (keyword.Key.Contains(FunctionsParser.ObjectMember))////'.'
                {
                    string[] kwrdparts = keyword.Key.Split(FunctionsParser.ObjectMember);//'.'

                    if (!GenerationContext.PublishRequireKeywords.ContainsKey(kwrdparts[0]))
                    {
                        GenerationContext.PublishRequireKeywords.Add(kwrdparts[0], new KeywordInfo()); //keyword.Value.Clone()

                        // Уточнить кто все таки должен быть безшаблона
                        if (!GenerationContext.Keywords.ContainsKey(kwrdparts[0]))
                        {
                            KeywordInfo kwdInf = GenerationContext.PublishRequireKeywords[kwrdparts[0]];//.Clone()
                            kwdInf.Keyword = keyword.Key;
                            TemplatewordInfo tplwrdInf = new TemplatewordInfo();// keyword.Value.Templateword;
                            tplwrdInf.TemplateType = TemplateTypeEn.WithoutTemplate;
                            tplwrdInf.IsContainsTemplate = false;
                            tplwrdInf.Templateword = TemplateTypeEn.WithoutTemplate.ToString();
                            tplwrdInf.TemplateFilePath = "";

                            kwdInf.Templateword = tplwrdInf;
                            kwdInf.Template = null;
                            GenerationContext.PublishRequireKeywords[kwrdparts[0]] = kwdInf;
                        }
                        else if (GenerationContext.Keywords.ContainsKey(kwrdparts[0]) )
                        {
                            GenerationContext.PublishRequireKeywords[kwrdparts[0]] = GenerationContext.Keywords[kwrdparts[0]];
                        }


                    }
                }
                else if (!GenerationContext.PublishRequireKeywords.ContainsKey(keyword.Key))
                {
                    GenerationContext.PublishRequireKeywords.Add(keyword.Key, new KeywordInfo()); // keyword.Value.Clone()

                    // Уточнить кто все таки должен быть безшаблона
                    if (!GenerationContext.Keywords.ContainsKey(keyword.Key))
                    {
                        KeywordInfo kwdInf = GenerationContext.PublishRequireKeywords[keyword.Key];
                        kwdInf.Keyword = keyword.Key;
                        TemplatewordInfo tplwrdInf = new TemplatewordInfo();// keyword.Value.Templateword;
                        tplwrdInf.TemplateType = TemplateTypeEn.WithoutTemplate;
                        tplwrdInf.IsContainsTemplate = false;
                        tplwrdInf.Templateword = TemplateTypeEn.WithoutTemplate.ToString();
                        tplwrdInf.TemplateFilePath = "";

                        kwdInf.Templateword = tplwrdInf;
                        kwdInf.Template = null;
                        GenerationContext.PublishRequireKeywords[keyword.Key] = kwdInf;
                    }
                    else if (GenerationContext.Keywords.ContainsKey(keyword.Key))
                    {
                        GenerationContext.PublishRequireKeywords[keyword.Key] = GenerationContext.Keywords[keyword.Key];
                    }

                }


            }

            // CYCLES Blocks Infos:
            // Collect MetadataKeys in Cycles
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.Cycle
                      && item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.SourceType == SourceTypeEn.LoadedMetadata
                      && !GenerationContext.CycleUsedMetadataKeys.ContainsKey(item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.CollectionKey)
                    )
                {
                    GenerationContext.CycleUsedMetadataKeys.Add(item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.CollectionKey, item.Value.Clone());
                }
            }


            // Collect ContentTemplates in Cycles
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.Cycle
                     && item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.SourceType == SourceTypeEn.Templates
                     && !GenerationContext.CycleTemplatewordKeys.ContainsKey(item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.CollectionKey)
                    )
                {
                    GenerationContext.CycleTemplatewordKeys.Add(item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.CollectionKey, item.Value.Clone());
                }
            }


            //GenerationContext.CycleConditionKeys
            foreach (var item in GenerationContext.PublishRequireKeywords.Where(itm => itm.Value.Templateword.TemplateType == TemplateTypeEn.Cycle))
            {
                string conditionsKey = item.Value.Template.TemplateParts[CTDLParser.CyclePartKey].Cycle.ConditionKey;
                if (conditionsKey != null)
                {
                    if (!GenerationContext.CycleConditionKeys.ContainsKey(conditionsKey))
                    {
                        GenerationContext.CycleConditionKeys.Add(conditionsKey, item.Value.Clone());
                    }

                    //foreach (var condition in conditionsKeys)
                    //{
                    //    if (!GenerationContext.CycleConditionKeys.ContainsKey(condition))
                    //    {
                    //        GenerationContext.CycleConditionKeys.Add(condition, item.Value);
                    //    }
                    //}
                }

            }

        }
        


    }
}
