﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NMG.Core.Domain;

namespace CTG.Core
{
    /// <summary>
    /// Контекст генерации - Основной Словарный Контейнер данных .
    /// </summary>
    /// <remarks>
    /// В нем хранятся - 
    /// 1 - Загружаемые шаблоны -т.е.   код из  Файлов  Шаблонов, до и после парсинга и анализа  кода этих шаблонов 
    /// 2 -Как буфер-обмена в котором перемещаются значения используемые при построении конечных текстов результативных файлов во время самого построения-генерации
    /// </remarks>
    public  class GenerationContext
    {

        #region ----------------------------- CTOR --------------------------------

         GenerationContext()
        {
            Initialize();
        }

        static GenerationContext()
        {
            Initialize();
        }

        #endregion ----------------------------- CTOR --------------------------------
        

        #region  -------------------------------- PROPERTIES -----------------------------------

                /// <summary>
                /// Текущий Контекст генерации/Выбранный через интерфейс
                /// </summary>
                //public static IGenerationFlow GenerationFlow
                //{
                //    get;
                //    set;
                //}                          

               


                /// <summary>
                /// Типология сущностей. С которой данные сущности(каждая сущность хранит значение по каждой типологии) производят генерацию. 
                /// </summary>
                public static Dictionary<String, Enum> EntityTypologies
                {
                    get;
                    internal set;
                }

                /// <summary>
                /// Фильтры которые не включаются в сущности но накладываются на первичный набор Таблиц
                /// </summary>
                public static List<String> Filters
                {
                    get;
                    internal set;
                }
        

                /// <summary>
                /// Сформированный список сущностей на основе фильтров  
                /// </summary>
                public static Dictionary<String, Entity> Entities
                {
                    get;
                    internal set;
                }


                /// <summary>
                /// Словарь Зависимых объектов - разные Части Шаблонов зависят могут обращаться к этим объектам во время построения - как к буферу обмена, и также изменять их. Временные объекты исподьзуемые для переходов значений/счетчиков/внутри логики составления ключевых слов
                /// </summary>
                public static Dictionary<String, object> DependentObjects
                {
                    get;
                    internal set;
                }


                /// <summary>
                /// Условия для каких-то элементов/В какой-то период времени. 
                /// Определяется только контекстом употребления писателем; т.е. закладывается самим автором Ключевого слова
                /// </summary>
                public static Dictionary<String, Func<bool>> Conditions
                {
                    get;
                    internal set;
                }

                /// <summary>
                /// Подгруженные Ключевые слова. Формируются с двух сторон - 
                /// Со стороны Писателей(PublicationManager) они просто создают ключевые слова без шаблонов с подписью об ответственности за эти слова
                /// С другой стороны подгружаются ключ слова без информ. о писателях для выбранного Сценария Потока Генерации из его директории шаблонов
                /// Потом они складываются  и проверяются -так чтоб :  
                /// 1 у каждого Keyword-а была информация о писателе и 
                /// 2 у каждого требующего шаблон Keyword-а таковой был
                /// </summary>
                public static Dictionary<string, KeywordInfo> Keywords
                {
                    get;
                    internal set;
                }
        

               /// <summary>
               /// Ссылки на ключевые слова которые собраны из всего кода шаблонов ключевых слов, без повторений. Здесь в список ключевых слов входят и название Ключ слов  без шаблона
               ///   String, KeywordInfo - String - оригинальная ссылка на ключевое слово, KeywordInfo - ключевое слово которое внесло данную оригианальную ссылку
               /// </summary>
               public static Dictionary<String, KeywordInfo> KeywordReferencies
               {
                    get;
                    set;
               }


               /// <summary>
               /// Требуемые к публикации Ключевые слова - слияние оригинального набора из двух частей  Keywords + KeywordReferencies
               /// Эта коллекция потом должна сравниваться с той что опубликовано у PublicationManager-а
               /// </summary> 
               public static Dictionary<String, KeywordInfo> PublishRequireKeywords
               {
                   get;
                   set;
               }


               /// <summary>
               /// Ссылки Циклов: Используемые ключи  метаданных
               /// </summary> 
               public static Dictionary<String, KeywordInfo> CycleUsedMetadataKeys
               {
                   get;
                   set;
               }


               /// <summary>
               /// Ссылки Циклов: Указатели на список-пользовательское Шаблонное слово - Content-ых шаблонов
               /// </summary> 
               public static Dictionary<String, KeywordInfo> CycleTemplatewordKeys
               {
                   get;
                   set;
               }

               /// <summary>
               /// Ссылки Циклов:Условия которые определяют вхождение Элементов цикла в построитель кода при генерации
               /// </summary> 
               public static Dictionary<String, KeywordInfo> CycleConditionKeys
               {
                   get;
                   set;
               }

               /// <summary>
               /// Опубликованные слова. Рабочая коллекция слов При генерации
               /// </summary>
               public static Dictionary<string, KeywordInfo> PublishedKeywords
               {
                   get;
                   set;
               }

            
        #endregion -------------------------------- PROPERTIES -----------------------------------


        #region --------------------------------- METHODS -------------------------------------


        /// <summary>
        /// Инициализация Контекста
        /// </summary>
        static void Initialize()
        {
            Entities = new Dictionary<String, Entity>();
            Keywords = new Dictionary<string, KeywordInfo>();
            EntityTypologies = new Dictionary<String, Enum>();
            Filters = new List<String>();

            PublishedKeywords = new Dictionary<string, KeywordInfo>();

            //analyze to validate pregeneration data - collections
            KeywordReferencies = new Dictionary<string, KeywordInfo>();            
            PublishRequireKeywords = new Dictionary<string, KeywordInfo>();
            CycleUsedMetadataKeys = new Dictionary<String, KeywordInfo>();
            CycleTemplatewordKeys = new Dictionary<string, KeywordInfo>();
            CycleConditionKeys = new Dictionary<string, KeywordInfo>();
            DependentObjects = new Dictionary<String, object>();
        }

        /// <summary>
        /// Регистрация ключ слов от Писателя 
        /// </summary>
        /// <param name="keywords"></param>
        public  void RegisterWriterKeywords(Dictionary<string, KeywordInfo> keywords)
        {
            foreach (var item in keywords)
            {
                PublishedKeywords.Add(item.Key, item.Value);
            }
        }


        #endregion --------------------------------- METHODS -------------------------------------




    }
}
