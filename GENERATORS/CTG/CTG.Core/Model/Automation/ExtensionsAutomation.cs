﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    /// <summary>
    /// Моменты автоматизации для создания Расширений -Поток Генерации. 1 -Заглушки для  методов  логики  для всех  элементов языка CTG на основе анализа шаблонов Потоки Генерации
    /// </summary>
    public static class ExtensionsAutomation
    {

        #region  ------------------------------------ CodeBlocksPrinter SNIPPEETS EXAMPLES ------------------------------------------

        //#region  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------
        //// ---------DECLARATION-----------
        //// TEMPLATEFILE: Classattribute.DataContract.snippet
        //// TEMPLATEPARTS:
        ////       PART [1] with Key [standartpart] 
        ////            KEYWORDREFERENCIES:


        ///// <summary>
        ///// Building [datacontract] keyword.template.ResultExpression
        ///// </summary>
        ///// <returns></returns>
        //[BuildKeywordExpressionAction("datacontract")]
        //public void buildKeywordAction_datacontract(String Templatepart)
        //{

        //    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["datacontract"];
        //    // TO DO: private logic to build keyword 

        //}
        //#endregion  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------



        //#region  ------------------------------ KEYWORD [1]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------
        //// ---------DECLARATION-----------
        //// TEMPLATEFILE: WithoutTemplate


        ///// <summary>
        ///// Building [entityname] keyword.template.ResultExpression
        ///// </summary>
        ///// <returns></returns>
        //[BuildKeywordExpressionAction("entityname")]
        //public void buildKeywordAction_entityname(String Templatepart)
        //{

        //    KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityname"];
        //    // TO DO: private logic to build keyword 

        //}
        //#endregion  ------------------------------ KEYWORD [1]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------


        #endregion  ------------------------------------ CodeBlocksPrinter SNIPPEETS EXAMPLES ------------------------------------------
        

        public static StringBuilder Print_AnalyseKeywordsRegister()
        {
            StringBuilder strBuilder = new StringBuilder();


            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ",   CTGen.Current.CurrentGenFlow.Name));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));

            strBuilder.AppendLine("");
            strBuilder.AppendLine("--------------------------TEMPALTE KEYWORDS ----------------------------");

            Int32 i = 1;
            foreach (var item in GenerationContext.Keywords)
            {
                strBuilder.AppendLine(String.Format("KEYWORD  {0}:  {1};             template---{2}--- ", i.ToString(), item.Key, item.Value.Template.FileName));

                Int32 q = 1;
                foreach (var tmplPart in item.Value.Template.TemplateParts)//templatePart
                {
                    strBuilder.AppendLine(String.Format("             TEMPALTE PART [{0}]  with KEY [{1}]  ", q.ToString(), tmplPart.Key));


                    Int32 z = 1;
                    foreach (var kwdRef in tmplPart.Value.KeywordReferencies)// templatePart Keyword Reference
                    {
                        strBuilder.AppendLine(String.Format("                   KEYWORD REFERENCE [{0}]  --- [{1}]  ", z.ToString(), kwdRef));
                        z++;
                    }


                    q++;
                }


                i++;
            }
            strBuilder.AppendLine("");

            strBuilder.AppendLine("");
            strBuilder.AppendLine("-------------------------- KEYWORD REFERENCIES----------------------------");

            Int32 j = 1;
            foreach (var item in GenerationContext.KeywordReferencies)
            {
                strBuilder.AppendLine(String.Format("REFERENCE  {0}:  {1};             template---{2}--- ", j.ToString(), item.Key, item.Value.Template.FileName));
                j++;
            }
            strBuilder.AppendLine("");

            strBuilder.AppendLine("");
            strBuilder.AppendLine("-------------------------- PUBLISH REQUIRE KEYWORDS----------------------------");

            Int32 k = 1;
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(String.Format("REQUIRE KEYWORD  {0}:  {1};    template---{2}---;  ", k.ToString(), item.Key, item.Value.Template.FileName));
                }
                else
                {
                    strBuilder.AppendLine(String.Format("REQUIRE KEYWORD  {0}:  {1};    template---{2}---;  ", k.ToString(), item.Key, TemplateTypeEn.WithoutTemplate.Str()));
                }

                k++;
            }
            strBuilder.AppendLine("");




            return strBuilder;


        }



        public static StringBuilder Print_AnalyseCyclesKeywords()
        {
           
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ", CTGen.Current.CurrentGenFlow.Name));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));


            // ---------------------------------------------CYCLES METADATA -------------------------------------------
            // Cycles MetadataKeys
            strBuilder.AppendLine("");
            strBuilder.AppendLine("-------------------------- CYCLE METADATA KEYS ----------------------------");

            Int32 l = 1;
            foreach (var item in GenerationContext.CycleUsedMetadataKeys)
            {
                strBuilder.AppendLine(String.Format("CYCLE METADATAKEY  {0}: {1}  in template  {2}  ", l.ToString(), item.Key, item.Value.Template.FileName));
                l++;
            }
            strBuilder.AppendLine("");


            // Cycles ContentTemplates
            strBuilder.AppendLine("");
            strBuilder.AppendLine("-------------------------- CYCLE CONTENTTEMPLATE KEYS ----------------------------");

            Int32 m = 1;
            foreach (var item in GenerationContext.CycleTemplatewordKeys)
            {
                strBuilder.AppendLine(String.Format("CYCLE METADATAKEY  {0}: {1}  in template  {2}   ", m.ToString(), item.Key, item.Value.Template.FileName));
                m++;
            }
            strBuilder.AppendLine("");

            // Cycles Conditions in Templates
            strBuilder.AppendLine("");
            strBuilder.AppendLine("-------------------------- CYCLE CONDITIONS KEYS ----------------------------");

            Int32 o = 1;
            foreach (var item in GenerationContext.CycleConditionKeys)
            {
                strBuilder.AppendLine(String.Format("CYCLE CONDITION  {0}: {1}  in template  {2}   ", o.ToString(), item.Key, item.Value.Template.FileName));
                o++;
            }
            strBuilder.AppendLine("");

            return strBuilder;

        }
        

        public static StringBuilder Print_PublishingAllMocks()
        {

            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ", CTGen.Current.CurrentGenFlow.Name));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));

            // ------------------------------------------------KEYWRITERS METHODS ---------------------------------------------------------
            // ------------------------------------------------KEYWRITERS METHODS ---------------------------------------------------------
            strBuilder.AppendLine("");
            strBuilder.AppendLine("#region-------------------------- PUBLISHING FOR ALL KEYWORDS ----------------------------");
            Int32 p = 1;
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                strBuilder.AppendLine("");

                strBuilder.AppendLine(String.Format("#region  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //
                strBuilder.AppendLine(@"// ---------DECLARATION-----------");
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", "WithoutTemplate"));
                }
                else
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", item.Value.Template.FileName));
                }

                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(@"// TEMPLATEPARTS:");
                    Int32 t = 1;
                    foreach (var part in item.Value.Template.TemplateParts)
                    {
                        strBuilder.AppendLine(String.Format(@"//       PART [{0}] with Key [{1}] ", t.ToString(), part.Key));
                        strBuilder.AppendLine(@"//            KEYWORDREFERENCIES:");

                        Int32 f = 1;
                        foreach (var kwdRef in part.Value.KeywordReferencies)
                        {
                            strBuilder.AppendLine(String.Format(@"//                REFERENCE  [{0}] :  [{1}]", f.ToString(), kwdRef.TokenWord));
                            f++;
                        }
                        t++;
                    }
                }
                strBuilder.AppendLine("");
                strBuilder.AppendLine("/// <summary>");
                strBuilder.AppendLine(String.Format("/// Publishing keyword [{0}] in PublicationManager.Keywords collection", item.Key));
                strBuilder.AppendLine("/// </summary>");
                strBuilder.AppendLine("/// <returns></returns>");
                strBuilder.AppendLine(String.Format("[PublishKeywordAction(\"{0}\")]", item.Key));
                strBuilder.AppendLine(String.Format("public KeywordInfo publishKeywordAction_{0}()", item.Key));
                strBuilder.AppendLine("{");
                strBuilder.AppendLine("KeywordInfo publishingKeyword = new KeywordInfo();");
                strBuilder.AppendLine(String.Format("publishingKeyword.Keyword = \"{0}\";", item.Key));
                strBuilder.AppendLine("publishingKeyword.Writer = new KeywordWriterInfo()");
                strBuilder.AppendLine("{");
                strBuilder.AppendLine(" Name = this.Name,");//        
                strBuilder.AppendLine(" Version = this.Version");//        
                strBuilder.AppendLine("};");
                strBuilder.AppendLine(@"// writer need to say: about Templateword");
                strBuilder.AppendLine("publishingKeyword.Templateword = new TemplatewordInfo()");
                strBuilder.AppendLine("{");
                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(" IsContainsTemplate = true,");
                    strBuilder.AppendLine(String.Format(" TemplateFilePath = @\"{0}\",", item.Value.Templateword.TemplateFilePath));
                    strBuilder.AppendLine(String.Format(" Templateword = \"{0}\",", item.Value.Templateword.Templateword));
                }
                else if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(" IsContainsTemplate = false,");
                    strBuilder.AppendLine(String.Format(" TemplateFilePath = \"\","));
                    strBuilder.AppendLine(" Templateword = \"\",");
                }
                strBuilder.AppendLine(String.Format(@" TemplateType = TemplateTypeEn.{0}", item.Value.Templateword.TemplateType.ToString()));
                strBuilder.AppendLine("};");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("return publishingKeyword;");
                strBuilder.AppendLine("}");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");
                strBuilder.AppendLine(String.Format("#endregion  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //

                strBuilder.AppendLine("");

                p++;
            }

            strBuilder.AppendLine("");

            strBuilder.AppendLine("");
            strBuilder.AppendLine("#endregion-------------------------- PUBLISHING FOR ALL KEYWORDS ----------------------------");



            return strBuilder;
        }
        

        public static StringBuilder Print_BuildWithoutTemplateMocks()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ", CTGen.Current.CurrentGenFlow.Name));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));


            strBuilder.AppendLine("");
            strBuilder.AppendLine("#region -------------------------- BUILDING FOR WithoutTemplate KEYWORDS ----------------------------");
            strBuilder.AppendLine("");

             Int32 p = 1;
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate) continue; // only WithoutTemplate  keyword types  can be printed here

                strBuilder.AppendLine("");

                strBuilder.AppendLine(String.Format("#region  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //
                strBuilder.AppendLine(@"// ---------DECLARATION-----------");
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", "WithoutTemplate"));
                }
                else
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", item.Value.Template.FileName));
                }

                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(@"// TEMPLATEPARTS:");
                    Int32 t = 1;
                    foreach (var part in item.Value.Template.TemplateParts)
                    {
                        strBuilder.AppendLine(String.Format(@"//       PART [{0}] with Key [{1}] ", t.ToString(), part.Key));
                        strBuilder.AppendLine(@"//            KEYWORDREFERENCIES:");

                        Int32 f = 1;
                        foreach (var kwdRef in part.Value.KeywordReferencies)
                        {
                            strBuilder.AppendLine(String.Format(@"//                REFERENCE  [{0}] :  [{1}]", f.ToString(), kwdRef.TokenWord));
                            f++;
                        }

                        t++;

                    }
                }
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");

                strBuilder.AppendLine(@"/// <summary>");
                strBuilder.AppendLine(String.Format("/// Building [{0}] keyword.template.ResultExpression", item.Key));
                strBuilder.AppendLine(@"/// </summary>");
                strBuilder.AppendLine(@"/// <returns></returns>");
                strBuilder.AppendLine(String.Format("[BuildKeywordExpressionAction(\"{0}\")]", item.Key));
                strBuilder.AppendLine(String.Format("public void buildKeywordAction_{0}(String Templatepart)", item.Key));
                strBuilder.AppendLine("{");
                strBuilder.AppendLine("");
                strBuilder.AppendLine(String.Format("KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[\"{0}\"];", item.Key));
                strBuilder.AppendLine(@"// TO DO: private logic to build keyword ");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("}");

                strBuilder.AppendLine(String.Format("#endregion  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //


                p++;
            }

            strBuilder.AppendLine("");
            strBuilder.AppendLine("#endregion-------------------------- BUILDING FOR WithoutTemplate KEYWORDS ----------------------------");
            strBuilder.AppendLine("");


            return strBuilder;
        }


        public static StringBuilder Print_BuildTemplateMocks()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ", CTGen.Current.CurrentGenFlow.Name));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));


            strBuilder.AppendLine("");
            strBuilder.AppendLine("#region -------------------------- BUILDING FOR TEMPLATED KEYWORDS ----------------------------");
            strBuilder.AppendLine("");

             Int32 p = 1;
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate) continue; // only Cycle | Start | Content  keyword types  can be printed here
                

                strBuilder.AppendLine("");

                strBuilder.AppendLine(String.Format("#region  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //
                strBuilder.AppendLine(@"// ---------DECLARATION-----------");
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", "WithoutTemplate"));
                }
                else
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", item.Value.Template.FileName));
                }

                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(@"// TEMPLATEPARTS:");
                    Int32 t = 1;
                    foreach (var part in item.Value.Template.TemplateParts)
                    {
                        strBuilder.AppendLine(String.Format(@"//       PART [{0}] with Key [{1}] ", t.ToString(), part.Key));
                        strBuilder.AppendLine(@"//            KEYWORDREFERENCIES:");

                        Int32 f = 1;
                        foreach (var kwdRef in part.Value.KeywordReferencies)
                        {
                            strBuilder.AppendLine(String.Format(@"//                REFERENCE  [{0}] :  [{1}]", f.ToString(), kwdRef.TokenWord));
                            f++;
                        }

                        t++;

                    }
                }
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");

                strBuilder.AppendLine(@"/// <summary>");
                strBuilder.AppendLine(String.Format("/// Building [{0}] keyword.template.ResultExpression", item.Key));
                strBuilder.AppendLine(@"/// </summary>");
                strBuilder.AppendLine(@"/// <returns></returns>");
                strBuilder.AppendLine(String.Format("[BuildKeywordExpressionAction(\"{0}\")]", item.Key));
                strBuilder.AppendLine(String.Format("public void buildKeywordAction_{0}(String Templatepart)", item.Key));
                strBuilder.AppendLine("{");
                strBuilder.AppendLine("");
                strBuilder.AppendLine(String.Format("KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[\"{0}\"];", item.Key));
                strBuilder.AppendLine(@"// TO DO: private logic to build keyword ");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("}");

                strBuilder.AppendLine(String.Format("#endregion  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //


                p++;
            }

            strBuilder.AppendLine("");
            strBuilder.AppendLine("#endregion-------------------------- BUILDING FOR TEMPLATED KEYWORDS ----------------------------");
            strBuilder.AppendLine("");


            return strBuilder;
        }


        public static StringBuilder Print_BuildAllMocks()
        {
            StringBuilder strBuilder = new StringBuilder();

            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));
            strBuilder.AppendLine(String.Format(@"///-----------------Generation Flow Name  {0} ---------------------  ", CTGen.Current.CurrentGenFlow.Name ));
            strBuilder.AppendLine(String.Format(@"///----------------------------------------------------------------------------"));


            strBuilder.AppendLine("");
            strBuilder.AppendLine("#region -------------------------- BUILDING FOR ALL KEYWORDS ----------------------------");
            strBuilder.AppendLine("");

            Int32 p = 1;
            foreach (var item in GenerationContext.PublishRequireKeywords)
            {              

                strBuilder.AppendLine("");

                strBuilder.AppendLine(String.Format("#region  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //
                strBuilder.AppendLine(@"// ---------DECLARATION-----------");
                if (item.Value.Templateword.TemplateType == TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", "WithoutTemplate"));
                }
                else
                {
                    strBuilder.AppendLine(String.Format(@"// TEMPLATEFILE: {0}", item.Value.Template.FileName));
                }

                if (item.Value.Templateword.TemplateType != TemplateTypeEn.WithoutTemplate)
                {
                    strBuilder.AppendLine(@"// TEMPLATEPARTS:");
                    Int32 t = 1;
                    foreach (var part in item.Value.Template.TemplateParts)
                    {
                        strBuilder.AppendLine(String.Format(@"//       PART [{0}] with Key [{1}] ", t.ToString(), part.Key));
                        strBuilder.AppendLine(@"//            KEYWORDREFERENCIES:");

                        Int32 f = 1;
                        foreach (var kwdRef in part.Value.KeywordReferencies)
                        {
                            strBuilder.AppendLine(String.Format(@"//                REFERENCE  [{0}] :  [{1}]", f.ToString(), kwdRef.TokenWord));
                            f++;
                        }

                        t++;

                    }
                }
                strBuilder.AppendLine("");
                strBuilder.AppendLine("");

                strBuilder.AppendLine(@"/// <summary>");
                strBuilder.AppendLine(String.Format("/// Building [{0}] keyword.template.ResultExpression", item.Key));
                strBuilder.AppendLine(@"/// </summary>");
                strBuilder.AppendLine(@"/// <returns></returns>");
                strBuilder.AppendLine(String.Format("[BuildKeywordExpressionAction(\"{0}\")]", item.Key));
                strBuilder.AppendLine(String.Format("public void buildKeywordAction_{0}(String Templatepart)", item.Key));
                strBuilder.AppendLine("{");
                strBuilder.AppendLine("");
                strBuilder.AppendLine(String.Format("KeywordInfo currentKeyword = GenerationContext.PublishedKeywords[\"{0}\"];", item.Key));
                strBuilder.AppendLine(@"// TO DO: private logic to build keyword ");
                strBuilder.AppendLine("");
                strBuilder.AppendLine("}");

                strBuilder.AppendLine(String.Format("#endregion  ------------------------------ KEYWORD [{0}]  [{1}] | Templateword [{2}]   ----------------------------------", p.ToString(), item.Key, item.Value.Templateword.Templateword));    //


                p++;
            }

            strBuilder.AppendLine("");
            strBuilder.AppendLine("#endregion-------------------------- BUILDING FOR ALL KEYWORDS ----------------------------");
            strBuilder.AppendLine("");


            return strBuilder;
        }
        

        //print Conditions only



        #region ---------------------------- PRINTING  SNIPPETS CODE ---------------------------------------

        /// <summary>
        ///  запись того что у нас накопилось в Codewriter-е в файл
        /// </summary>
        /// <param name="codeBuilder"></param>
        public static void WriteToFile(this CodeWriter codewriter)
        {

            File.WriteAllText(codewriter.FilePath, codewriter.Builder.ToString()); //
        }


        #endregion  ---------------------------- PRINTING  SNIPPETS CODE ---------------------------------------




    }
}
