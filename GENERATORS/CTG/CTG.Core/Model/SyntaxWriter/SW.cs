﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG
{
    /// <summary>
    /// Syntax Writer - text Writing standart api 
    /// </summary>
    public class SW
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="generator"></param>
        /// <param name="len"></param>
        public static void WriteSpace(StringBuilder stringBuilder, int len)
        {
            while (len-- > 0)
                stringBuilder.Append(" ");                
        }
               


    }
}
