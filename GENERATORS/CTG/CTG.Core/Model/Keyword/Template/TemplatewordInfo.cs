﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    
    /// <summary>
    /// Информация о шаблоне
    /// </summary>
    public class TemplatewordInfo
    {

        //IsRequireTemplate – необходим ли для данного Ключ. слова шаблон
        //Template – строки шаблона считанные из файла шаблон а.
        //IsCaseTemplate –  Этот шаблон инвариантен? Т.е. требовать чтоб какой-то шаблон для ключевого слова был бы обязательно инвариантным это нормально – это право писателя определять спецификацию для ключевого слова. Если он не найдет никаких инвариантных оперделений для этого ключ слова в шаблоне то возникнет исключение валидации по этому поводу. 
        //Invariants –строки инварианта из шаблона
        private const Int32 BaseHashMultiplicator = 4794;
              
        
        #region     ----------------------------PROP ITEM  :  (String) Templateword   ---------------------------------
        // PARAMS :
        // PropName   -      Templateword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Слово Шаблона - что то вроде Типа даннных для переменной(ключевого слова)         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Слово Шаблона - что то вроде Типа даннных для переменной(ключевого слова)
        /// </summary>        
        public  String  Templateword
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) Templateword ---------------------------------



        #region     ----------------------------PROP ITEM  :  (TemplateTypeEn) TemplateType   ---------------------------------
        // PARAMS :
        // PropName   -      Aggregation         EX: AddNewButton     
        // PropType   -      AggregationEn         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Тип Аггрегации у данного ключ слова. Также в качестве спецификации выставляется Писателем         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Тип Аггрегации у данного ключ слова. Также в качестве спецификации выставляется Писателем
        /// </summary>        
        public TemplateTypeEn TemplateType
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (TemplateTypeEn) TemplateType ---------------------------------

        
        
        #region     ----------------------------PROP ITEM  :  (bool) IsContainsTemplate   ---------------------------------
        // PARAMS :
        // PropName   -      IsContainsTemplate         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Содержится ли шаблон  для данного ключ слова         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Содержится ли шаблон  для данного ключ слова
        /// </summary>        
        public  bool  IsContainsTemplate
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsContainsTemplate ---------------------------------
    

        
        #region     ----------------------------PROP ITEM  :  TemplateFilePath ---------------------------------
        // PARAMS :
        // PropName   -      TemplateFilePath         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Путь к файлу шаблона          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Путь к файлу шаблона 
        /// </summary>        
        public  String  TemplateFilePath
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  TemplateFilePath ---------------------------------
    
    


        #region  -------------------------------- METHODS -----------------------------------

        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as TemplatewordInfo) == null)
            { return false; }

            TemplatewordInfo other = (TemplatewordInfo)Obj;
            return (this.Templateword == other.Templateword &&
                    this.TemplateType == other.TemplateType 
                );
        }


        public override Int32 GetHashCode()
        {
            return ( BaseHashMultiplicator ^ Templateword.GetHashCode() ^ this.TemplateType.GetHashCode() );
        }


        //public static bool operator ==(TemplatewordInfo templateword1, TemplatewordInfo templateword2)
        //{
        //    return templateword1.Equals(templateword2);
        //}


        //public static bool operator !=(TemplatewordInfo templateword1, TemplatewordInfo templateword2)
        //{
        //    return !templateword1.Equals(templateword2);
        //}



        public virtual TemplatewordInfo Clone()
        {
            return ((TemplatewordInfo)(this.MemberwiseClone()));
        }



    #endregion -------------------------------- METHODS -----------------------------------
        
            


    }

    
    
}
