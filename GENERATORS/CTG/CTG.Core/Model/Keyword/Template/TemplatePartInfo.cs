﻿using CTG.Core.Parsing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    /// <summary>
    // Инфа по части шаблона в много компонентном шаблоне
    /// </summary>
    public class TemplatePartInfo
    {
        #region  ----------------------------- CTOR -----------------------------------

        public TemplatePartInfo()
        {
            // ResultExpression = new ResultExpression();

        }

        #endregion ----------------------------- CTOR -----------------------------------

        
        private const Int32 BaseHashMultiplicator = 3945;

       


        #region     ----------------------------PROP ITEM  :  (String) PartName   ---------------------------------
        // PARAMS :
        // PropName   -      PartName         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Часть составного шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Часть составного шаблона
        /// </summary>        
        public String PartName
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) PartName ---------------------------------


        #region     ----------------------------PROP ITEM  :  (Int32) PartIndex   ---------------------------------
        // PARAMS :
        // PropName   -      PartIndex         EX: AddNewButton     
        // PropType   -      Int32         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Индекс Парта  в общем шаблоне         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Индекс Парта  в общем шаблоне
        /// </summary>        
        public Int32 PartIndex
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (Int32) PartIndex ---------------------------------
        
        


        
        #region     ----------------------------PROP ITEM  :  (FuncParameter) TemplateDeclarationFunc   ---------------------------------
        // PARAMS :
        // PropName   -      TemplateDeclarationFunc         EX: AddNewButton     
        // PropType   -      FuncParameter         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Function of Template Defenition         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Function of Template Defenition
        /// </summary>        
        public  FuncParameter  TemplateDeclarationFunc
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (FuncParameter) TemplateDeclarationFunc ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  (String) OriginalContent   ---------------------------------
        // PARAMS :
        // PropName   -      Content         EX: AddNewButton     
        // PropType   -      String[]         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Содержимое парта шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Содержимое парта шаблона
        /// </summary>        
        public String OriginalContent
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) OriginalContent ---------------------------------
               


        #region     ----------------------------PROP ITEM  :  Keyword Tokens ---------------------------------
        // PARAMS :
        // PropName   -      KeywordReferencies         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ссылки на ключевые слова в шаблоне         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ссылки на ключевые слова в шаблоне
        /// </summary>        
        public List<TokenInfo> KeywordReferencies
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  Keyword Tokens ---------------------------------


        #region     ----------------------------PROP ITEM  :  (bool) IsInvariant   ---------------------------------
        // PARAMS :
        // PropName   -      IsInvariant         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Этот Парт является инвариантом?         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Этот Парт является инвариантом?
        /// </summary>        
        public bool IsInvariant
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsInvariant ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  (bool) IsValid   ---------------------------------
        // PARAMS :
        // PropName   -      IsValid         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Валидный ли шаблон данного кусочка         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Валидный ли шаблон данного кусочка
        /// </summary>        
        public  bool  IsValid
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsValid ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  (ResultExpression) ResultExpression   ---------------------------------
        // PARAMS :
        // PropName   -      ResultExpression         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Результат полученного выражения         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Результат полученного выражения
        /// </summary>        
        public ResultExpression ResultExpression
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) ResultExpression ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  (CycleInfo) Cycle   ---------------------------------
        // PARAMS :
        // PropName   -      Cycle         EX: AddNewButton     
        // PropType   -      CycleInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Информация по парту который является циклом         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Информация по парту который является циклом
        /// </summary>        
        public  CycleInfo  Cycle
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (CycleInfo) Cycle ---------------------------------
    






        public void UpdatePartResult()
        {
            // regex code replacements by template parts  results 

        }


        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as TemplatePartInfo) == null)
            {  return false; }

            TemplatePartInfo other = (TemplatePartInfo)Obj;
            return (
                this.IsInvariant == other.IsInvariant &&
                this.PartIndex == other.PartIndex &&
                this.PartName == other.PartName  &&
                this.ResultExpression == other.ResultExpression
                );
        }


        public override Int32 GetHashCode()
        {
            return (BaseHashMultiplicator ^ this.PartIndex.GetHashCode() ^ PartName.GetHashCode() ^ ResultExpression.GetHashCode()); //this.GenerationContextKey.GetHashCode() ^
        }
        

        //public static bool operator ==(TemplatePartInfo templatePart1, TemplatePartInfo templatePart2)
        //{
        //    return templatePart1.Equals(templatePart2);
        //}
        

        //public static bool operator !=(TemplatePartInfo templatePart1, TemplatePartInfo templatePart2)
        //{
        //    return !templatePart1.Equals(templatePart2);
        //}


        public virtual TemplatePartInfo Clone()
        {
            return ((TemplatePartInfo)(this.MemberwiseClone()));
        }




    }



}
