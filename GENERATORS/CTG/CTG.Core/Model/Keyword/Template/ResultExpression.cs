﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{

    /// <summary>
    /// Выражения после генерации/обработки Писателем
    /// </summary>
    public class ResultExpression
    {

        #region  -------------------------------- CTOR -----------------------------------

        public ResultExpression()
        {

        }

        #endregion  -------------------------------- CTOR -----------------------------------
        
        //UpdateLevel – уровень обновления– так как писатель может содержать много  им публикуемых ключевых слов , и они могут быть разного времени использования,  поэтому  необходимо чтоб каждое ключ. слово говорило о себе  - на каком уровне его стоит обновить.       
        //CanBeNullable – может ли результирующее значение на данном уровне, данного ключ. слова  быть  нулевым. Автор ключ слова выбирает эту спецификацию для данного ключ слова.
        private const Int32 BaseHashMultiplicator = 1395;


        #region     ----------------------------PROP ITEM  : (int) UpdateLevel ---------------------------------
        // PARAMS :
        // PropName   -      UpdateLevel         EX: AddNewButton     
        // PropType   -      Int32         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Уровень- индексное значение целочисленой последовательности  в диапазоне от 0 - n         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Уровень- индексное значение целочисленой последовательности  в диапазоне от 0 - n
        /// </summary>        
        public Int32 UpdateLevel
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (int) UpdateLevel ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (String) UpdateInitiatorKeyword   ---------------------------------
        // PARAMS :
        // PropName   -      UpdateInitiatorKeyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключевое слово-Структурное Ключевое слово ? - На котором Произошло обновление данного результата         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ключевое слово-Структурное Ключевое слово ? - На котором Произошло обновление данного результата
        /// </summary>        
        public  String  UpdateInitiatorKeyword
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) UpdateInitiatorKeyword ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  (String[]) Expression   ---------------------------------
        // PARAMS :
        // PropName   -      Expression         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Конечное выражение для вывода или вставки в свое время в шаблон         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Конечное выражение для вывода или вставки в свое время в шаблон
        /// </summary>        
        public String Expression    //String[]
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String[]) Expression ---------------------------------


        #region     ----------------------------PROP ITEM  :  (bool) IsUpdated   ---------------------------------
        // PARAMS :
        // PropName   -      IsUpdated         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Обновлено ли уже знчение выражения на данном уровне, без труэ этого свойства использовать выражение нет смысла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Обновлено ли уже знчение выражения на данном уровне, без труэ этого свойства использовать выражение нет смысла
        /// </summary>        
        public bool IsUpdated
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsUpdated ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (String) BindingPath   ---------------------------------
        // PARAMS :
        // PropName   -      BindingPath         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Путь к значению ключевого слова в контексте генерации         EX: Adding New Button To the Control Buttons Collection   

        #endregion  -------------------------- PROP ITEM  : (String) BindingPath ---------------------------------


#region  -------------------------- COMPARE METHODS ----------------------------

        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as ResultExpression) == null)
            { return false; }

            ResultExpression other = (ResultExpression)Obj;
            return (this.Expression == other.Expression &&
                this.IsUpdated == other.IsUpdated &&
                this.UpdateInitiatorKeyword == other.UpdateInitiatorKeyword &&
                this.UpdateLevel == other.UpdateLevel
                );
        }


        public override Int32 GetHashCode()
        {
            return (BaseHashMultiplicator ^ this.Expression.GetHashCode() ^ this.IsUpdated.GetHashCode() ^ UpdateInitiatorKeyword.GetHashCode() ^ UpdateLevel.GetHashCode());
        }


#endregion -------------------------- COMPARE METHODS ----------------------------
        

        //public static bool operator ==(ResultExpression resultExpresssion1, ResultExpression resultExpresssion2)
        //{
        //    //if (resultExpresssion1 is null && resultExpresssion2 == null) return true;
        //    if (resultExpresssion1 == null && resultExpresssion2 != null) return false;
            
        //    return resultExpresssion1.Equals(resultExpresssion2);
        //}


        //public static bool operator !=(ResultExpression resultExpresssion1, ResultExpression resultExpresssion2)
        //{
        //    if (resultExpresssion1 == null && resultExpresssion2 != null) return true;
            

        //    return !resultExpresssion1.Equals(resultExpresssion2);
        //}

        public virtual ResultExpression Clone()
        {
            return ((ResultExpression)(this.MemberwiseClone()));
        }


    }

 
}
