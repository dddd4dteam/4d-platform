﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    
    /// <summary>
    /// Информация о шаблоне
    /// </summary>
    public class TemplateInfo
    {

        #region  -----------------------------CTOR---------------------------------

        public TemplateInfo()
        {

        }

        #endregion -----------------------------CTOR---------------------------------
        

        //IsRequireTemplate – необходим ли для данного Ключ. слова шаблон
        //Template – строки шаблона считанные из файла шаблон а.
        //IsCaseTemplate –  Этот шаблон инвариантен? Т.е. требовать чтоб какой-то шаблон для ключевого слова был бы обязательно инвариантным это нормально – это право писателя определять спецификацию для ключевого слова. Если он не найдет никаких инвариантных оперделений для этого ключ слова в шаблоне то возникнет исключение валидации по этому поводу. 
        //Invariants –строки инварианта из шаблона
        private const Int32 BaseHashMultiplicator = 3945;


        #region     ----------------------------PROP ITEM  :  (String) FilePath   ---------------------------------
        // PARAMS :
        // PropName   -      FilePath         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Путь к файлу шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Путь к файлу шаблона
        /// </summary>        
        public String FilePath
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) FilePath ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  FileName ---------------------------------
        // PARAMS :
        // PropName   -      FileName         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Название файла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Название файла
        /// </summary>        
        public  String  FileName
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  FileName ---------------------------------
             
        
        #region     ----------------------------PROP ITEM  :  OriginalContent ---------------------------------
        // PARAMS :
        // PropName   -      OriginalContent         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Строка оригинального Контента         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Строка оригинального Контента
        /// </summary>        
        public  String  OriginalContent
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  OriginalContent ---------------------------------
    

        #region     ----------------------------PROP ITEM  : (Dictionary<String,TemplatePartInfo>) TemplateParts ---------------------------------
        // PARAMS :
        // PropName   -      TemplateParts         EX: AddNewButton     
        // PropType   -      Dictionary<String,String[]>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Части шаблона- если шаблон состоит из нескольких частей которые должны выводиться и генераиться в разные моменты времени и места.Каждая часть имеет свое имя. Если шаблон не разбит на части значит все строки хранит первый элемент словаря с ключом "BaseTemplate"          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Части шаблона- если шаблон состоит из нескольких частей которые должны выводиться и генераиться в разные моменты времени и разных местах.
        /// </summary>
        /// <remarks>
        /// Каждая часть имеет свое имя. 
        /// Если шаблон не разбит на части значит все строки хранит первый элемент словаря с ключом "BaseTemplate"
        /// Пишется так Ex: [TEMPLATEPART( Partname(x), CASE(n) )  /]   ||   [TEMPLATEPART( Partname(1) )   /]
        /// </remarks>
        public Dictionary<String, TemplatePartInfo> TemplateParts
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :(Dictionary<String,TemplatePartInfo>)   TemplateParts ---------------------------------


        #region     ----------------------------PROP ITEM  :  KeywordTokens ---------------------------------
        // PARAMS :
        // PropName   -      KeywordReferencies         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ссылки на ключевые слова в шаблоне         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ссылки на ключевые слова в шаблоне
        /// </summary>        
        public  List<String>  KeywordTokens
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  KeywordTokens ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) TemplateExtension   ---------------------------------
        // PARAMS :
        // PropName   -      TemplateExtension         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Расширение файла шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Расширение файла шаблона
        /// </summary>
        /// <remarks>Данное расширение используется  как указатель на базовую ассоциативность с  стандартными  статическими  snippet-шаблонами. Так и есть CTG по сути является динамической надстройкой для системной/составной /структурированной/composite  обработки snippet-шаблонаов.</remarks>
        /// <value>По умолчанию расирение - .snippet .</value>
        public String TemplateExtension
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) TemplateExtension ---------------------------------


        
        #region     ----------------------------PROP ITEM  :  (String) LastBuildPart   ---------------------------------
        // PARAMS :
        // PropName   -      LastBuildPart         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -           EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Последний построенный парт         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Последний построенный парт
        /// </summary>        
          String  LastBuildPart
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) LastBuildPart ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  (bool) IsCaseTemplate   ---------------------------------
        // PARAMS :
        // PropName   -      IsCaseTemplate         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Инвариантный шаблон - т.е. определение происходит через инвариантные теги; Шаблон сам по себе все так же далее независимо от инвариантности определяет свою Аггрегатность -т.е он может быть во 1-х ИНвариантным и во 2-х Аггрегатным          EX: Adding New Button To the Control Buttons Collection   
          /// <summary>
          /// Кейсовый  шаблон - т.е. определение происходит через части шаблона которые используют именование с функцией CASE.
          /// </summary>
          /// <remarks>Шаблон сам по себе все так же далее независимо от кейса определяет свою Аггрегатность - т.е. он может быть во 1-х Кейсовым и во 2-х Аггрегатным(Цикл и просто несколько Пользовательских Частей у Шаблона</remarks>
        public bool IsCaseTemplate
        {
            get
            {
                foreach (var item in TemplateParts)
                {
                    if (item.Value.IsInvariant == false)
                    {
                        return false;
                    }
                }
                return true;
            }
           
            
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsCaseTemplate ---------------------------------


        #region     ----------------------------PROP ITEM  :  IsValidTemplate ---------------------------------
        // PARAMS :
        // PropName   -      IsValidTemplate         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Is this template  valid : have correct [DECLARATION/IMPLEMENTATION] values          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Is this template  valid : have correct [DECLARATION/IMPLEMENTATION] values 
        /// </summary>        
        public bool IsValidTemplate
        {
            get
            {
                foreach (var item in TemplateParts)
                {
                    if (item.Value.IsValid == false)
                    {
                        return false;
                    }
                }
                return true;
            }
            
        }

        #endregion  -------------------------- PROP ITEM  :  IsValidTemplate ---------------------------------
    

        #region  -------------------------------- METHODS -----------------------------------

        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as TemplateInfo) == null)
            { return false; }

            TemplateInfo other = (TemplateInfo)Obj;
            return (this.FilePath == other.FilePath &&
                this.TemplateExtension == other.TemplateExtension &&
                this.TemplateParts.Count == other.TemplateParts.Count
                );
        }


        public override Int32 GetHashCode()
        {
            return ( BaseHashMultiplicator ^ FilePath.GetHashCode() ^ this.TemplateExtension.GetHashCode() ^ TemplateParts.Count.GetHashCode() );
        }


        //public static bool operator ==(TemplateInfo template1, TemplateInfo template2)
        //{
        //    return template1.Equals(template2);
        //}


        //public static bool operator !=(TemplateInfo template1, TemplateInfo template2)
        //{
        //    return !template1.Equals(template2);
        //}




        public void UpdateTemplate()
        {
            //Updating TemplateParts Result
            foreach (var part in TemplateParts)
            {
                part.Value.UpdatePartResult();
            }
        }


        public virtual TemplateInfo Clone()
        {
            return ((TemplateInfo)(this.MemberwiseClone()));
        }
    #endregion -------------------------------- METHODS -----------------------------------
        
    
        


    }

    
    
}
