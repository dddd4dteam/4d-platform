﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTG; 

namespace CTG.Core
{

    /// <summary>
    /// Информация о ссылке в коде шаблона. Внутри текста(кода) шаблона  для Парта Шаблона
    /// В связи [Ссылка-Ключевое слово] ссылка не равна по мощности связи ключевому слову и имеет отношение[n-1] 
    /// К примеру в одной строке может быть к несколько ссылок на одно ключевое слово
    /// </summary>
    public struct TokenInfo
    {

        #region  ----------------------------PROPERTIES-------------------------------


        #region     ----------------------------PROP ITEM  : (Int32)  Index ---------------------------------
        // PARAMS :
        // PropName   -      Index         EX: AddNewButton     
        // PropType   -      Int32         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Индекс данной ссылки из полученного Match при анализе кода шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Индекс данной ссылки из полученного Match при анализе кода шаблона
        /// Индекс может использоваться как уникальный идентификатор ссылки для одного парта шаблона
        /// </summary>        
        public Int32 Index
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  Index ---------------------------------


        #region     ----------------------------PROP ITEM  : (String) TokenWord ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключевое слово на которое имеется в каком то коде шаблона ссылка. Каждая ссылка может отличаться например маркером отступа при форматировании         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Сслыка -Ключевое слово на которое имеется в каком то коде шаблона ссылка. Каждая ссылка может отличаться например маркером отступа при форматировании
        /// </summary>        
        public String TokenWord
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  TokenWord ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) Token   ---------------------------------
        // PARAMS :
        // PropName   -      ReferenceMarkered         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ссылка ка она находится в тексте. Она будет заменяться         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ссылка ка она находится в тексте. Она будет заменяться
        /// </summary>        
        public String Token
        {
            get
            {
                return "{" + TokenWord + "}";
            }

        }

        #endregion  -------------------------- PROP ITEM  : (String) Token ---------------------------------


        #region     ----------------------------PROP ITEM  : (String) KeywordName ---------------------------------
        // PARAMS :
        // PropName   -      KeywordName         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Имя Ключевого слова ссылки          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Имя Ключевого слова ссылки 
        /// </summary>        
        public String KeywordName
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  KeywordName ---------------------------------


        #region     ----------------------------PROP ITEM  : (String)  TemplatePartName ---------------------------------
        // PARAMS :
        // PropName   -      TemplatePart         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Парт на который явно указывает ссылка         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Парт  ссылки на который она явно указывает - при много партовой структуре шаблона
        /// </summary>        
        public String TemplatePartName
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  TemplatePartName ---------------------------------


        #region     ----------------------------PROP ITEM  : (String) SmartMargin ---------------------------------
        // PARAMS :
        // PropName   -      SmartMargin         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Умный отступ. Отступ для данной ссылки в тексте кода если такой необходимо учитывать-определяется в Определении Ключ слова-хозяина шаблона           EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Умный отступ
        /// Отступ для данной ссылки в тексте кода если такой необходимо учитывать-определяется в Определении Ключ слова-хозяина шаблона, 
        /// а не слова на которое указывает reference-keyword 
        /// </summary>        
        public String SmartMargin
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  SmartMargin ---------------------------------


        #region     ----------------------------PROP ITEM  : (String) ReplaceText ---------------------------------
        // PARAMS :
        // PropName   -      ReplaceText         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Конечный текст замены подготовленный для этой ссылки         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Конечный текст замены подготовленный для этой ссылки
        /// </summary>        
        public String ReplaceText
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  ReplaceText ---------------------------------


        #region     ----------------------------PROP ITEM  : (KeywordInfo) KeywordObject ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      KeywordInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -     ON BUILD only.  Полное Ключевое слово данной ссылки - из словаря  GenerationContext.PublishedKeywords         EX: Adding New Button To the Control Buttons Collection   
        KeywordInfo _KeywordObject;// = null;
        /// <summary>
        ///ON BUILD only. Полное Ключевое слово данной ссылки - из словаря  GenerationContext.PublishedKeywords
        /// </summary>        
        public KeywordInfo KeywordObject
        {
            get
            {
                if (_KeywordObject == null)
                { _KeywordObject = TokenInfo.GetKeyword(KeywordName); }
                return _KeywordObject;
            }
        }

        #endregion  -------------------------- PROP ITEM  : (KeywordInfo) KeywordObject ---------------------------------


        #region     ----------------------------PROP ITEM  :  (TemplatePartInfo) TemplatePartObject   ---------------------------------
        // PARAMS :
        // PropName   -      PartObject         EX: AddNewButton     
        // PropType   -      TemplatePartInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       ON BUILD only. Get referenced Template Part object         EX: Adding New Button To the Control Buttons Collection   

        TemplatePartInfo _TemplatePartObject;
        /// <summary>
        /// ON BUILD only. Get referenced Template Part object
        /// </summary>        
        public TemplatePartInfo TemplatePartObject
        {
            get
            {
                if (_TemplatePartObject == null)
                { _TemplatePartObject = TokenInfo.GetTemplatePart(KeywordName, TemplatePartName); }
                return _TemplatePartObject;

            }

        }

        #endregion  -------------------------- PROP ITEM  : (TemplatePartInfo) PartObject ---------------------------------
    
        #endregion ----------------------------PROPERTIES-------------------------------



    #region  ---------------------------- METHODS -----------------------------------

        /// <summary>
        /// Получить Ключевое слово из словаря GenerationContext.PublishedKeywords
        /// </summary>
        /// <param name="Reference"></param>
        /// <returns></returns>
        private static KeywordInfo GetKeyword(String Keyword)
        {
            if (GenerationContext.PublishedKeywords.ContainsKey(Keyword) == false)
            {
                throw new ParseException(String.Format("Reference keyword [{0}] does not contains in PublishedKeywords in Generation Context ", Keyword));
            }

            return GenerationContext.PublishedKeywords[Keyword];
        }


        /// <summary>
        /// Получить Парт данного шаблона данного Ключевого слова- для референса
        /// </summary>
        /// <param name="Keyword"></param>
        /// <param name="TemplatePart"></param>
        /// <returns></returns>
        private static TemplatePartInfo GetTemplatePart(String Keyword,String TemplatePart)
        {

            KeywordInfo refKeyword = GetKeyword(Keyword);
            
            //rule -how to get 
            if (TemplatePart == null  && refKeyword.Templateword.TemplateType == TemplateTypeEn.Cycle)
            {
                TemplatePart = CTDLParser.CyclePartKey;
            }
            if (TemplatePart == null && refKeyword.Templateword.TemplateType == TemplateTypeEn.Content)
            {
                TemplatePart = CTDLParser.NotDeclaredPartKey;
            }

            if (refKeyword.Template.TemplateParts.ContainsKey(TemplatePart) == false)
            {
                throw new ParseException(String.Format("Reference TemplatePart [{0}] does not contains in PublishedKeywords [{1}] in Generation Context ", TemplatePart, Keyword));
            }

            return refKeyword.Template.TemplateParts[TemplatePart];
        }

    #endregion ---------------------------- METHODS -----------------------------------
        
    }





}
