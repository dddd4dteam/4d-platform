﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics.Contracts;
using CTG;
using System.Text.RegularExpressions;



namespace CTG.Core
{
     
    //Get CYCLE FUNCTION Expression  "\[TEMPLATEPART:(.*?)\/\]"
    //Result: Full Syntax - CYCLE( UpdateLMAction(UpdateAction); FOREACH (LM:Entities) IN TEMPLATE(DefaultClass); CONDITION(VOOnlyCondition); DELIMETER(,); EachItemOnNewLine  )

    //Get ANY Function with Nested functions  between  \(  \)  symbols  "(?<=\( )[^]]+(?=\) )"
    //REsult:  FOREACH (LM:Entities) IN TEMPLATE(DefaultClass); CONDITION(TablesOnlyCondition); DELIMETER(,);  EachItemOnNewLine 

    //Count ( Brackets -    int j = Regex.Matches(test, @"(").Cast<Match>().Count();
    //Count ) Brackets -    int j = Regex.Matches(test, @"(").Cast<Match>().Count();

    // Splitting CYCLE body into PARAMS EXPTESSIONS;   
    // FOREACH (TW:Interface.Implementation) IN TEMPLATE(DefaultClass)   @"FOREACH\( (.*?) \)"
    // CONDITION(TablesOnlyCondition)                                    @"CONDITION\( (.*?) \)"
    // DELIMETER(,)                                                      @"DELIMETER\( (.*?) \)"
    // EachItemOnNewLine                                                 @"EachItemOnNewLine"
    // UpdateLMAction(UpdateAction)                                      @"UpdateLMAction"
        
   

    /// <summary>
    /// Выражения после генерации/обработки Писателем
    /// </summary>
    public class CycleInfo
    {

        #region  ---------------------------------- CTOR ---------------------------------------

        public CycleInfo()
        {

        }

        #endregion ---------------------------------- CTOR ---------------------------------------

        
        //UpdateLevel – уровень обновления– так как писатель может содержать много  им публикуемых ключевых слов , и они могут быть разного времени использования,  поэтому  необходимо чтоб каждое ключ. слово говорило о себе  - на каком уровне его стоит обновить.       
        //CanBeNullable – может ли результирующее значение на данном уровне, данного ключ. слова  быть  нулевым. Автор ключ слова выбирает эту спецификацию для данного ключ слова.
        private const Int32 BaseHashMultiplicator = 1395;


        

        #region     ----------------------------PROP ITEM  :  (String) CollectionKey   ---------------------------------
        // PARAMS :
        // PropName   -      CollectionKey         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключ к коллекции данных в Контексте генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ключ к коллекции данных в Контексте генерации
        /// </summary>        
        public String CollectionKey
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) CollectionKey ---------------------------------  
      

        #region     ----------------------------PROP ITEM  : (String) CollectionItemKey ---------------------------------
        // PARAMS :
        // PropName   -      CollectionItemKey         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -        Ключ  к элементу коллекции по которому он сохраняется в словаре

        /// <summary>
        /// Часть если какая то определенная часть из шаблона ItemTemplate. 
        /// По умолчанию она всегда равна имени стандартной части если у шаблона только одна часть. 
        /// </summary>        
        public String CollectionItemKey
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) CollectionItemKey ---------------------------------




        #region     ----------------------------PROP ITEM  :  (SourceTypeEn) SourceType   ---------------------------------
        // PARAMS :
        // PropName   -      SourceType         EX: AddNewButton     
        // PropType   -      SourceTypeEn         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Тип источника из  которого берется коллекция для Цикла- LM /TW         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Тип источника из  которого берется коллекция для Цикла- LM /TW
        /// </summary>        
        public  SourceTypeEn  SourceType
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (SourceTypeEn) SourceType ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  (String) ItemTemplate   ---------------------------------
        // PARAMS :
        // PropName   -      ItemTemplate         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключевое слово с шаблоном для представления элемента данных из коллекции Цикла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ключевое слово с шаблоном для представления элемента данных из коллекции Цикла . Может быть нулевым
        /// </summary>        
        public String ItemTemplate
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) ItemTemplate ---------------------------------

        
        #region     ----------------------------PROP ITEM  : (String) ItemTemplatePart ---------------------------------
        // PARAMS :
        // PropName   -      ItemTemplatePart         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Часть если какая то определенная а не часть по умолчанию         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Часть если какая то определенная часть из шаблона ItemTemplate. 
        /// По умолчанию она всегда равна имени стандартной части если у шаблона только одна часть. 
        /// </summary>        
        public  String  ItemTemplatePart
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) ItemTemplatePart ---------------------------------


       

        #region     ----------------------------PROP ITEM  :  (String) ConditionKey   ---------------------------------
        // PARAMS :
        // PropName   -      ConditionsKeys         EX: AddNewButton     
        // PropType   -      String[]         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключи для Условий определяющих выведение  одного элемента цикла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ключ для Условия определяющего выведение  одного элемента цикла . Может быть нулевым
        /// </summary>        
        public String ConditionKey
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String[]) ConditionKey ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) Delimeter   ---------------------------------
        // PARAMS :
        // PropName   -      Delimeter         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Разделитель между элементами шаблона. Может быть нулевыйм         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Разделитель между элементами шаблона. Может быть нулевыйм
        /// </summary>        
        public String Delimeter
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) Delimeter ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  (bool) EachItemOnNewLine   ---------------------------------
        // PARAMS :
        // PropName   -      EachItemOnNewLine         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Каждый элемент с новой строки         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Каждый элемент с новой строки
        /// </summary>        
        public  bool  EachItemOnNewLine
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) EachItemOnNewLine ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  (bool) UpdateLMAction   ---------------------------------
        // PARAMS :
        // PropName   -      UpdateLMAction         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Этот цикл извещает обновление  LM- подгруженных метаданных         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Этот цикл использует обновление LM- подгруженных метаданных на начало цикла. 
        /// Если null значит никакого экшена не надо запускать перед началом цикла
        /// </summary>        
        public  String  UpdateLMAction
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) UpdateLMAction ---------------------------------
    
         
        #region ------------------------------------- METHODS -------------------------------------------- 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as CycleInfo) == null)
            { return false; }

            CycleInfo other = (CycleInfo)Obj;
            return (this.CollectionKey == other.CollectionKey &&
                this.ConditionKey == other.ConditionKey &&
                this.ItemTemplate == other.ItemTemplate &&
                this.Delimeter == other.Delimeter
                );
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override Int32 GetHashCode()
        {
            return (BaseHashMultiplicator ^ this.CollectionKey.GetHashCode() ^ this.ItemTemplate.GetHashCode() ^ Delimeter.GetHashCode());
        }


        #region --------------------Clone method-----------------------------

        public virtual CycleInfo Clone()
        {
            return ((CycleInfo)(this.MemberwiseClone()));
        }

        #endregion  --------------------Clone method-----------------------------



        //public static bool operator ==(CycleInfo resultExpresssion1, CycleInfo resultExpresssion2)
        //{
        //    return resultExpresssion1.Equals(resultExpresssion2);
        //}


        //public static bool operator !=(CycleInfo resultExpresssion1, CycleInfo resultExpresssion2)
        //{
        //    return !resultExpresssion1.Equals(resultExpresssion2);
        //}






        #endregion ------------------------------------- METHODS --------------------------------------------


    }

    
    
}
