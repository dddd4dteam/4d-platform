﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;


namespace CTG.Core
{
   
    
    /// <summary>
    /// struct Keyword Information -   
    /// </summary>
    public class  KeywordInfo
    {        
        //Keyword –  ключевое слово из шаблона
        //Writer – писатель который должен отвечать за выдачу конечного значения для ключевого слова. 
        
        //CurrentLanguage – Язык - который используется в данный момент а данном GenFlow или же просто выбранный из UI
        
        //SupportLanguages - Какие по спецификации для этого ключ слова поддерживаются языки
        //SupportFlows – где в каких  GenerationFlow –сах может использоваться данное ключевое слово.
        //Последение два свойства поддерживаются на уровне писателя –это он сообщает какие может обслуживать языки и какие потоки генерации.
        
        //Template - Описатель шаблона: необходимость шаблона для ключевого слова/ строки -содержимое шаблона/ путь к файлу / расширение файла/ Инвариантный шаблон или нет/Сами Инварианты
        //SubKeywords – массив с ключевыми именами которые  необходимы чтоб построить/написать данное ResultExpression

        //ResultExpression – конечное выражение которое получается после обработки писателем(writer-ом)  данного ключевого слова.        
        

        private const Int32 BaseHashMultiplicator = 13795;


        #region  ----------------------------------- PROPERTIES -----------------------------------------

        #region     ----------------------------PROP ITEM  : (String)  Keyword ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       The keyword          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Имя Ключ слова
        /// </summary>
        public String Keyword
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String)  Keyword ---------------------------------


        #region     ----------------------------PROP ITEM  : (KeywordWriterInfo) Builder ---------------------------------
        // PARAMS :
        // PropName   -      Builder         EX: AddNewButton     
        // PropType   -      IKeywordWriter         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Писатель -класс который отвечает за это ключевое слово, за его публикацию в Менеджере Публикаций и обновление результирующих значений         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Писатель -класс который отвечает за это ключевое слово, за его публикацию в Менеджере Публикаций и обновление результирующих значений
        /// </summary>        
        public KeywordBuilder Builder
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (KeywordWriterInfo) Builder ---------------------------------


        #region     ----------------------------PROP ITEM  :  (String) CurrentLanguage   ---------------------------------
        // PARAMS :
        // PropName   -      CurrentLanguage         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Язык - который используется в данный момент а данном GenFlow или же просто выбранный из UI         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Язык - который используется в данный момент а данном GenFlow или же просто выбранный из UI( С#- cs    Assembler- as   Java- java    C- cc     C++ - cpp....).
        /// </summary>
        /// <remarks>Название папки должно совпадать с расширением файлов кода -результата генерации.</remarks>
        public String CurrentLanguage
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) CurrentLanguage ---------------------------------



        #region     ----------------------------PROP ITEM  :  (String) CurrentGenFlow   ---------------------------------
        // PARAMS :
        // PropName   -      CurrentGenFlow         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Текущий дженерешн Поток - исходя из названия папки         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Текущий Поток  Генерации - исходя из названия папки
        /// </summary>
        public String CurrentGenFlow
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) CurrentGenFlow ---------------------------------


        #region     ----------------------------PROP ITEM  :  (TemplateInfo) Template   ---------------------------------
        // PARAMS :
        // PropName   -      Template         EX: AddNewButton     
        // PropType   -      TemplateInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Описатель шаблона: необходимость шаблона для ключевого слова/ строки -содержимое шаблона/ путь к файлу / расширение файла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Шаблон: необходимость шаблона для ключевого слова/ части шаблона с оригинальным кодом шаблона/ путь к файлу шаблона / расширение файла шаблона
        /// </summary>
        public TemplateInfo Template
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (TemplateInfo) Template ---------------------------------


        #region     ----------------------------PROP ITEM  :  (TemplatewordInfo) Templateword   ---------------------------------
        // PARAMS :
        // PropName   -      Templateword         EX: AddNewButton     
        // PropType   -      TemplatewordInfo         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Информация по шаблонному слову         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Информация по шаблонному слову
        /// </summary>
        /// <remarks>Стандартьный формат имени файла шаблона -    [templateword].[keyword].[extension]. В данной модели шаблона , Концепт  [templateword] -Шаблонное Слово ,   в данной модели стоит рассматривается в двух аспектах - 1 [templateword] - как слово о Типе Шаблона. Может быть  либо Зарезервированный типом , либо определяемый Ползователем тип шаблона ;  2 -Структура описатель Типа Шаблона-  TemplateWordInfo  - содержит в себе более полную информацию о типе шаблона, которая и определяется Шаблонным словом .</remarks>
        public TemplatewordInfo Templateword
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (TemplatewordInfo) Templateword ---------------------------------


        #region     ----------------------------PROP ITEM  :  (bool) IsEnable   ---------------------------------
        // PARAMS :
        // PropName   -      IsEnable         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Доступно ли  для генерации данное Ключевое слово. Т.е. оно должно быть доступно и затем тогда проверяется валидность получения шаблона         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Доступно ли  для генерации данное Ключевое слово. Т.е. оно должно быть доступно и затем тогда проверяется валидность получения шаблона
        /// </summary>        
        public bool IsEnable
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsEnable ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (bool) IsMapped   ---------------------------------
        // PARAMS :
        // PropName   -      IsMapped         EX: AddNewButton     
        // PropType   -      bool         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Это ключевое слово промепенно- назначено  прямиком из конфигурационого файла сценария Потока Генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Это ключевое слово промепенно- назначено  прямиком из конфигурационого файла сценария Потока Генерации
        /// </summary>        
        public  bool  IsMapped
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (bool) IsMapped ---------------------------------
    


        #region     ----------------------------PROP ITEM  : (String) WithoutTemplateExpression ---------------------------------
        // PARAMS :
        // PropName   -      WithoutTemplateExpression         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Выражение для ключевого слова у которого тип шаблона WithoutTemplate         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Выражение для ключевого слова у которого тип шаблона WithoutTemplate
        /// </summary>        
        public String WithoutTemplateExpression
        {
            get;
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  WithoutTemplateExpression ---------------------------------
        

#endregion ----------------------------------- PROPERTIES -----------------------------------------


        #region  -------------------------- METHODS ----------------------------------------


#region ----------------------------- Compare Methods ---------------------------
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public override bool Equals(object Obj)
        {
            if (Obj == null || (Obj as KeywordInfo) == null)
            { return false; }


            KeywordInfo other = (KeywordInfo)Obj;
            return (
                    this.Keyword == other.Keyword &&
                    this.Builder == other.Builder &&
                    this.IsEnable == other.IsEnable &&
                    this.CurrentLanguage == other.CurrentLanguage &&
                    this.CurrentGenFlow == other.CurrentGenFlow &&
                    this.Templateword == other.Templateword        //&&
                //this.Template == other.Template
                   );
        }

        public override Int32 GetHashCode()
        {
            return (BaseHashMultiplicator ^ this.Keyword.GetHashCode() ^ this.Builder.GetHashCode() ^ Template.GetHashCode());
        }


        //public static bool operator ==(KeywordInfo keyword1, KeywordInfo keyword2)
        //{
        //    return keyword1.Equals(keyword2);
        //}


        //public static bool operator !=(KeywordInfo keyword1, KeywordInfo keyword2)
        //{
        //    return !keyword1.Equals(keyword2);
        //}


        #endregion ----------------------------- Compare Methods ---------------------------



    /// <summary>
    ///  Cloning Keyword 
    /// </summary>
    /// <returns></returns>
    public virtual KeywordInfo Clone()
    {
        return ((KeywordInfo)(this.MemberwiseClone()));
    }
        
        
#endregion -------------------------- METHODS ----------------------------------------









    }


}
