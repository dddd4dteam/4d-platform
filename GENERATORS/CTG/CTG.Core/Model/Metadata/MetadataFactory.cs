﻿

using CTG.Configuration;
using NMG.Core.Domain;
using NMG.Core.Reader;

namespace NMG.Core
{
    public class MetadataFactory
    {

        public static IMetadataReader GetReader(ServerTypeEn serverType, string connectionStr)
        {
            switch (serverType)
            {
                case ServerTypeEn.NotDefined:
                    return new NpgsqlMetadataReader(connectionStr);
                case ServerTypeEn.Oracle10:
                    return new OracleMetadataReader(connectionStr);
                case ServerTypeEn.Oracle11:
                    return new OracleMetadataReader(connectionStr);
                case ServerTypeEn.SqlServer2008:
                    return new SqlServerMetadataReader(connectionStr);
                case ServerTypeEn.SqlServer2012:
                    return new SqlServerMetadataReader(connectionStr);
                case ServerTypeEn.PostgreSQL:
                    return new NpgsqlMetadataReader(connectionStr);
                case ServerTypeEn.MySQL:
                    return new MysqlMetadataReader(connectionStr);
                //case ServerTypeEn.SQLite:
                //    return new SqliteMetadataReader(connectionStr);
                case ServerTypeEn.Sybase:
                    return new SybaseMetadataReader(connectionStr);
                case ServerTypeEn.Ingres:
                    return new IngresMetadataReader(connectionStr);
                case ServerTypeEn.CUBRID:
                    return new CUBRIDMetadataReader(connectionStr);
                    
                default:
                    return new NpgsqlMetadataReader(connectionStr);
                    
            }

        }

    }
}