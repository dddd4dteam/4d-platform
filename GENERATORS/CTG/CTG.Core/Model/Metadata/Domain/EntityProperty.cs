﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMG.Core.Domain
{
    public class EntityProperty
    {


        public EntityProperty()
        {

        }


        #region     ----------------------------PROP ITEM  :  (Column) LoadedTable   ---------------------------------
        // PARAMS :
        // PropName   -      Table         EX: AddNewButton     
        // PropType   -      Table         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       PropDesc         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// PropDesc
        /// </summary>        
        public  Column  LoadedColumn
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (Table) LoadedTable ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (Dictionary<String,String>) PropertyTypologies   ---------------------------------
        // PARAMS :
        // PropName   -      EntityTypologies         EX: AddNewButton     
        // PropType   -      Dictionary<String,String>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Значение Типологий для данной сущности         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Значение Типологий для данной сущности
        /// </summary>        
        public  Dictionary<String,String>  PropertyTypologies
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (Dictionary<String,String>) EntityTypologies ---------------------------------
    


    }
}
