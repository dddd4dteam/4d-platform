﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NMG.Core.Domain
{
    public class Entity
    {


#region ------------------ cTOR -----------------

        public Entity()
        {

        }

#endregion ------------------ cTOR -----------------




        #region     ----------------------------PROP ITEM  :  Name ---------------------------------
        // PARAMS :
        // PropName   -      Name         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Имя Сущности        EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        ///  Имя Сущности
        /// </summary>        
        public  String  Name
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Name ---------------------------------
    
    


        #region     ----------------------------PROP ITEM  :  (Table) LoadedTable   ---------------------------------
        // PARAMS :
        // PropName   -      Table         EX: AddNewButton     
        // PropType   -      Table         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       PropDesc         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// PropDesc
        /// </summary>        
        public  Table  LoadedTable
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (Table) LoadedTable ---------------------------------

        
        #region     ----------------------------PROP ITEM  :  (Dictionary<String,String>) EntityTypologies   ---------------------------------
        // PARAMS :
        // PropName   -      EntityTypologies         EX: AddNewButton     
        // PropType   -      Dictionary<String,String>         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Значение Типологий для данной сущности         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Значение Типологий для данной сущности
        /// </summary>        
        public  Dictionary<String,String>  EntityTypologies
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (Dictionary<String,String>) EntityTypologies ---------------------------------
    


    }
}
