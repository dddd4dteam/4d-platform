﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core
{
    // RU:
    // Существует два варианта реализуемых типологий  
    // 1- Для Сущностей 
    // 2- Для Свойств Сущностей. 
    // Элементом языка программирования который представляет какую либо типологию - в даном случае выступает enum.  
    //    
    //  Для определения значений типов в Типологии - над значениями перечисления проставляется определенный атрибут - для типа Сущности свой и для типа свойства свой.
    //  Каждая Типология -т.е. Тип перечисления с атрибутом Типологии заносится автоматически в глобальную конфигурацию(ConfigurationEngine). 
    //  Эти типы энама - используются при анализе метаданных и составлении сущностей, перед самой генерацией. 
    //  Т.о. мы имем три аттрибута: 1 -Чтоб определить enum как конкретную типологию 
    //                              2 -Чтоб определить значение типа для Сущности 
    //                              3 -Чтоб определить значение типа для Свойства Сущности
    
    // EN:
    //  enum Type should seemed as the realization of some Typology. For Example we can determine EntityType by entity name -> so we can can create 



    /// <summary>
    ///  Typology Attribute- defenition of ConventionNaming-RegEx mask and EnableOptionalConfiguration - for the EntityType
    /// </summary>
    [AttributeUsage(AttributeTargets.Enum , AllowMultiple = false)]
    public class TypologyAttribute : Attribute
    {

        // The constructor is called when the attribute is set.

        #region -------------------------CTOR ---------------------------
        
        public TypologyAttribute( String pEntityType, String pRecognitionMask, String pOptionalConfiguration)
        {

            //TypeName - FieldName;TypeRecognitionMask ; TypeOptionalConfiguration
            EntityType = pEntityType;
            RecognitionMask = pRecognitionMask;
            OptionalConfiguration = pOptionalConfiguration;

        }

        #endregion -------------------------CTOR --------------------------
        

        #region ---------------------------- PROPERTIES ---------------------------
        
        
        #region     ----------------------------PROP ITEM  :  EntityType ---------------------------------
        // PARAMS :
        // PropName   -      EntityType         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Тип сущности в соответствии с какойто логической группировкой. Определяется по маске именования. Также может быть привязан к опционной конфигурации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Тип сущности в соответствии с какойто логической группировкой. Определяется по маске именования. Также может быть привязан к опционной конфигурации
        /// </summary>        
        public  String  EntityType
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  EntityType ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  RecognitionMask ---------------------------------
        // PARAMS :
        // PropName   -      RecognitionMask         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Маска по которой определяется соответствие данному логическому типу          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Маска по которой определяется соответствие данному логическому типу 
        /// </summary>        
        public  String  RecognitionMask
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  RecognitionMask ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  OptionalConfiguration ---------------------------------
        // PARAMS :
        // PropName   -      OptionalConfiguration         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Конфигурация по настройке вывода для даного логического типа         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Конфигурация по настройке вывода для даного логического типа
        /// </summary>        
        public  String  OptionalConfiguration
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  OptionalConfiguration ---------------------------------
    

        #endregion ---------------------------- PROPERTIES ---------------------------



    }
}
