﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core
{
    
    /// <summary>
    ///  EntityTypology Attribute- defenition of ConventionNaming-RegEx mask and EnableOptionalConfiguration - for the EntityType
    /// </summary>
    [AttributeUsage( AttributeTargets.Field, AllowMultiple = false)]
    public class EntityPropertyTypeAttribute : Attribute
    {

        // The constructor is called when the attribute is set.

        #region -------------------------CTOR ---------------------------
        
        public EntityPropertyTypeAttribute( String pEntityType, String pRecognitionMask, String pOptionalConfiguration)
        {
            //TypeName - FieldName;TypeRecognitionMask ; TypeOptionalConfiguration
            EntityType = pEntityType;
            RecognitionMask = pRecognitionMask;
            OptionalConfiguration = pOptionalConfiguration;
        }
        
        #endregion -------------------------CTOR --------------------------
        

        #region ---------------------------- PROPERTIES ---------------------------
        
        
        #region     ----------------------------PROP ITEM  :  EntityType ---------------------------------
        // PARAMS :
        // PropName   -      EntityType         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Тип сущности в соответствии с какойто логической группировкой. Определяется по маске именования. Также может быть привязан к опционной конфигурации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Тип сущности в соответствии с какойто логической группировкой. Определяется по маске именования. Также может быть привязан к опционной конфигурации
        /// </summary>        
        public  String  EntityType
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  EntityType ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  RecognitionMask ---------------------------------
        // PARAMS :
        // PropName   -      RecognitionMask         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Маска по которой определяется соответствие данному логическому типу          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Маска по которой определяется соответствие данному логическому типу 
        /// </summary>        
        public  String  RecognitionMask
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  RecognitionMask ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  OptionalConfiguration ---------------------------------
        // PARAMS :
        // PropName   -      OptionalConfiguration         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - private    EX: internal
        // PropDesc  -       Конфигурация по настройке вывода для даного логического типа         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Конфигурация по настройке вывода для даного логического типа
        /// </summary>        
        public  String  OptionalConfiguration
        {
            get; 
            private set;
        }

        #endregion  -------------------------- PROP ITEM  :  OptionalConfiguration ---------------------------------
    

        #endregion ---------------------------- PROPERTIES ---------------------------


    }
}
