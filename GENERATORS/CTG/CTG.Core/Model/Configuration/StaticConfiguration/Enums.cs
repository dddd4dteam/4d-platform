﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NMG.Core.Domain;
using CTG.EnumConfiguration;


namespace CTG.Configuration
{
    

    /// <summary>
    /// Template Generator Operations Configuration/Parameters Enumeration
    /// </summary>
    [Description("Template Generator Operations Configuration/Parameters Enumeration")]
    [ConfigEnum]  
    public enum CTGConfigEn
    {
        /// <summary>
        /// Базовый путь к Расширениям- библиотекам с Писателями и шаблонами Потоков Генерации
        /// </summary>
        [Description("Базовый путь к Расширениям- библиотекам с Писателями и шаблонами Потоков Генерации ")]
        [EnumParam(ParamType=typeof(String),
                   Kind=ParamKindEn.String,
                   LocalNameKey = @"CTGConfigEn.BaseExtensionsPath.Name.Key",
                   LocalDescriptionKey = @"CTGConfigEn.BaseExtensionsPath.Description.Key",
                   Content=@"c:\Platform\Generators\Extensions\")]
        BaseExtensionsPath,

        /// <summary>
        /// Базовый путь к Потокам Генерации(GenerationFlow)- папки входящие в данную считаются Потоками Генерации.Далее внутри этой папки находятся подпапки с языком реализации и в ней уже сами файлики шаблонов
        /// </summary>
        [Description("Базовый путь к Потокам Генерации(GenerationFlow)- папки входящие в данную считаются Потоками Генерации.Далее внутри этой папки находятся подпапки с языком реализации и в ней уже сами файлики шаблонов")]
        [EnumParam(ParamType=typeof(String),
                   Kind=ParamKindEn.String, 
                   LocalNameKey = @"CTGConfigEn.BaseGenFlowsPath.Name.Key",
                   LocalDescriptionKey = @"CTGConfigEn.BaseGenFlowsPath.Description.Key",
                    Content=@"c:\Platform\Generators\GenFlows\")]
        BaseGenFlowsPath,


        /// <summary>
        /// Базовый путь к Расширениям- библиотекам с Писателями и шаблонами Потоков Генерации
        /// </summary>
        [Description("Базовый Путь  Выводов- библиотекам с Писателями и шаблонами Потоков Генерации")]
        [EnumParam(ParamType=typeof(String),
                   Kind=ParamKindEn.String,
                   LocalNameKey = @"CTGConfigEn.BaseOutputPath.Name.Key",
                   LocalDescriptionKey = @"CTGConfigEn.BaseOutputPath.Description.Key",
                   Content=@"c:\Platform\Generators\Output\")]
        BaseOutputPath,
              

        /// <summary>
        /// Иcпользовать ли умные отступы при формировании конечного варианта текста замены Ссылок
        /// </summary>
        [Description("Иcпользовать ли умные отступы при формировании конечного варианта текста замены Ссылок")]
        [EnumParam(ParamType=typeof(bool),
                   Kind=ParamKindEn.Struct,
                   LocalNameKey = @"CTGConfigEn.UseReferenceSmartMargin.Name.Key",
                   LocalDescriptionKey = @"CTGConfigEn.UseReferenceSmartMargin.Description.Key",
                   Content="False")]
        UseReferenceSmartMargin    


    }



}
