﻿
using CTG.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CTG.Core
{




    /// <summary>
    /// Писатель Кода в файл
    /// </summary>
    public struct CodeWriter
    {
        
        #region     ----------------------------PROP ITEM  :  Key ---------------------------------
        // PARAMS :
        // PropName   -      Key         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Ключ-название файла по которому определяется Билдер в словаре CodeBuilder-ов         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Ключ-название файла по которому определяется Билдер в словаре CodeBuilder-ов
        /// </summary>        
        public  String  Key
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Key ---------------------------------


        #region     ----------------------------PROP ITEM  :  GenerationType ---------------------------------
        // PARAMS :
        // PropName   -      BuilderType         EX: AddNewButton     
        // PropType   -      PropType         EX: Returning Result Type
        // PropAccess   -    PropAccess       EX: public
        // PropSetAccess   - PropSetAccess    EX: internal
        // PropDesc  -       PropDesc         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Тип построительсва Кода
        /// </summary>        
        public GenerationTypeEn  GenerationType
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  GenerationType ---------------------------------


        #region     ----------------------------PROP ITEM  :  StartKeyword ---------------------------------
        // PARAMS :
        // PropName   -      StartKeyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Стартовое Ключевое Слово         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Стартовое Ключевое Слово
        /// </summary>        
        public  String  StartKeyword
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  StartKeyword ---------------------------------
        
        
        #region     ----------------------------PROP ITEM  :  StartTemplate ---------------------------------
        // PARAMS :
        // PropName   -      StartTemplate         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Шаблон Старта - с которого начинается построение         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Шаблон Старта - с которого начинается построение
        /// </summary>        
        public  String  StartTemplate
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  StartTemplate ---------------------------------
    
        
        #region     ----------------------------PROP ITEM  :  EntityName ---------------------------------
        // PARAMS :
        // PropName   -      Entity         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Сущность для которой строится Код данным билдером         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Сущность для которой строится Код данным билдером
        /// </summary>        
        public  String  EntityName
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Entity ---------------------------------
            
        
        #region     ----------------------------PROP ITEM  :  FileExtension ---------------------------------
        // PARAMS :
        // PropName   -      Extension         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       File - code language extension - .cs/.vb /.c          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// File - code language extension - .cs/.vb /.c 
        /// </summary>        
        public  String  FileExtension
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Extension ---------------------------------
    
        
        #region     ----------------------------PROP ITEM  :  (String) FileName   ---------------------------------
        // PARAMS :
        // PropName   -      FileName         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Название файла         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Название файла
        /// </summary>        
        public  String  FileName
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) FileName ---------------------------------
    
        
        #region     ----------------------------PROP ITEM  :  (String) FilePath   ---------------------------------
        // PARAMS :
        // PropName   -      FilePath         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Путь к файлу вместе с его именем         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Путь к файлу вместе с его именем
        /// </summary>        
        public  String  FilePath
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) FilePath ---------------------------------
    

        #region     ----------------------------PROP ITEM  :  Builder ---------------------------------
        // PARAMS :
        // PropName   -      Builder         EX: AddNewButton     
        // PropType   -      StringBuilder         EX: Returning Result Type
        // PropAccess   -      public     EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Построитель Кода - StringBuilder          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Построитель Кода - StringBuilder 
        /// </summary>        
        public StringBuilder  Builder
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Builder ---------------------------------
    
    }
}
