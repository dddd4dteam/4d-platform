﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using CTG.Configuration;




namespace CTG.Core
{
    
    #region ------------------ FUNCTIONAL MODEL : GenerationFlows ------------------------------
    //COMPONENTS:  [IItem] + [ItemMetadataAttribute] + [IItemMetaData] +...  you need to define  [Item]:[IItem] next


    // Params:
    // FuncModelItem                           GenerationFlow            Ex: Window / KeywordWriter...       

    #region     ---------------------------- [IItem] ------------------------------

    /// <summary>
    ///  [IItem] : IGenFlowLangManager - base interface
    /// </summary>
    public interface IGenFlowLangManager
    {

        /// <summary>
        /// Имя Потока Генерации
        /// </summary>
        String Name { get; }   
             

        /// <summary>
        /// Построитель/и Кода для  одного/множества   файлов
        /// </summary>
        Dictionary<string, CodeWriter> CodeWriters    {   get;       }


        /// <summary>
        /// Generation Flow - custom1Version 
        /// </summary>     
        Version CurrentVersion { get; }
        

        /// <summary>
        ///Язык программирования за который отвечает данный менеджер в данном GenerationFlow
        /// </summary>
        LanguageEn SupportLanguage  {  get;   }


        /// <summary>
        /// 
        /// </summary>
        bool IsSelected     {      get;    }


        /// <summary>
        /// Запуск алгоритма Потока Генерации. Получаем на выходе  -Dictionary< Название ФАайла|Строки полученного кода этого файла >
        /// </summary>
        /// <returns></returns>
        void Generate();


        /// <summary>
        /// Построительствует выражение у ключевого слова
        /// </summary>
        void BuildKeyword(String Keyword, String TemplatePart );


      
    }

    #endregion     ---------------------------- [IItem] ------------------------------
    

    #region     ---------------------------- [ItemMetadataAttribute] ------------------------------

    /// <summary>
    /// Мета-Экспорт Аттрибут-GenerationFlow Менеджера Конкретного языка программинга. 
    /// </summary>    
    [MetadataAttribute]
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false)]
    public sealed class GenFlowLangManagerAttribute : ExportAttribute //, IGenFlowLangManagerMetaData also used in ImportCollection
    {

        #region     ---------------------------- CTOR ------------------------------

        public GenFlowLangManagerAttribute(String pName, LanguageEn pSupportLanguage) //
            : base(typeof(IGenFlowLangManager))
        {
            Name = pName;
            SupportLanguage = pSupportLanguage;
        }

        #endregion  ---------------------------- CTOR ------------------------------
        

        #region     ----------------------------PROP ITEM  :  Name ---------------------------------
        // PARAMS :
        // PropName   -      Name         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   - protected    EX: internal
        // PropDesc  -       Имя- ключ окна         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Имя- ключ окна
        /// </summary>        
        public String Name
        {
            get;
            set;
        }


        #endregion  -------------------------- PROP ITEM  :  Name ---------------------------------

       
        #region     ----------------------------PROP ITEM  :  (String) SupportLanguage   ---------------------------------
        // PARAMS :
        // PropName   -      SupportLanguages         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Язык ПРограмминга - который  Поддерживает данный менеджер Потока Генерации         EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        ///Язык Программинга - который  Поддерживает данный менеджер Потока Генерации
        /// </summary>        
        public   LanguageEn SupportLanguage 
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  : (String) SupportLanguage ---------------------------------
    


    }

    #endregion     ---------------------------- [ItemMetadataAttribute] ------------------------------


    #region     ---------------------------- [IItemMetaData] ------------------------------


    /// <summary>
    /// Мета-Экспорт Интерфейс - GenFlowLangManager Менеджера Конкретного языка программинга  из GenerationFlow.
    ///  Никем не наследуется и нигде не передается но используется как облегченнная версия Интерфейса  IGenFlowLangManager       
    /// </summary>
    public interface IGenFlowLangManagerMetaData
    {
        
        #region  ------------------ PROPERTIES ----------------------------

        /// <summary>
        /// Имя-ключ для GenerationFlow
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Язык Программинга - который  Поддерживает данный менеджер Потока Генерации - Ex: "cs,vb,java..."
        /// </summary>
        LanguageEn SupportLanguage { get; }

        #endregion ------------------ PROPERTIES ----------------------------
        
    }

    #endregion     ---------------------------- [IItemMetaData] ------------------------------


    #endregion ------------------ FUNCTIONAL MODEL : GenerationFlows ------------------------------



}
