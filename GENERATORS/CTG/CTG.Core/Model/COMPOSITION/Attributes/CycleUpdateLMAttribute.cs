﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core
{


    /// <summary>
    ///  Метод который генерит код результирующего выражения для Ключевого слова -с типом Cycle.
    ///  Применяется к методу класса билдера. 
    ///  Используется методы с формой делегата Action
    /// </summary>
    [AttributeUsage(AttributeTargets.Method , AllowMultiple = false)]
    public class CycleUpdateLMActionAttribute : Attribute
    {
        // The constructor is called when the attribute is set.

        #region -------------------------CTOR ---------------------------


        public CycleUpdateLMActionAttribute(String pUpdateActionKey)
        {

            //TypeName - FieldName;TypeRecognitionMask ; TypeOptionalConfiguration
            UpdateAction = pUpdateActionKey;

        }


        #endregion -------------------------CTOR --------------------------
        

        #region ---------------------------- PROPERTIES ---------------------------


        #region     ----------------------------PROP ITEM  :  UpdateAction ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Условие Ключ у метода который публикует метод как функцинальный делегат Func<bool> обработки

        /// <summary>
        /// Условие - Ключ у метода который публикует себя как функцинальный делегат Func<bool> обработки
        /// </summary>        
        public String UpdateAction
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  UpdateAction ---------------------------------







        #endregion ---------------------------- PROPERTIES ---------------------------


    }
}
