﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core
{

    /// <summary>
    ///  Метод который генерит код результирующего выражения для Ключевого слова.
    ///  Применяется к методу класса писателя. 
    ///  Используется методы с формой делегата Func<object,bool>    CONDITION (item -KeywordInfo)
    /// </summary>
    [AttributeUsage(AttributeTargets.Method , AllowMultiple = false)]
    public class CycleConditionFuncAttribute : Attribute
    {

        // The constructor is called when the attribute is set.

        #region -------------------------CTOR ---------------------------
        
        public CycleConditionFuncAttribute(String pConditionAction)
        {

            //TypeName - FieldName;TypeRecognitionMask ; TypeOptionalConfiguration
            ConditionAction = pConditionAction;
        }

        #endregion -------------------------CTOR --------------------------
        

        #region ---------------------------- PROPERTIES ---------------------------


        #region     ----------------------------PROP ITEM  : ConditionAction ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Условие Ключ у метода который публикует метод как функцинальный делегат Func<bool> обработки

        /// <summary>
        /// Условие - Ключ у метода который публикует себя как функцинальный делегат Func<bool> обработки
        /// </summary>        
        public  String  ConditionAction
        {
            get; 
            set;
        }

        #endregion  -------------------------- PROP ITEM  :  ConditionAction ---------------------------------



        #endregion ---------------------------- PROPERTIES ---------------------------


    }
}
