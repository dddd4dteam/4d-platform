﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CTG.Core
{

    /// <summary>
    ///  Метод который выдает спецификацию по Ключевому слову   для Менеджера публикаций. 
    ///  Применяется к методу класса писателя. 
    /// </summary>
    [AttributeUsage(AttributeTargets.Method , AllowMultiple = false)]
    public class PublishKeywordActionAttribute : Attribute
    {
        // The constructor is called when the attribute is set.
        #region -------------------------CTOR ---------------------------


        public PublishKeywordActionAttribute(String pPublishingKeyword)
        {

            //TypeName - FieldName;TypeRecognitionMask ; TypeOptionalConfiguration
            Keyword = pPublishingKeyword;

        }


        #endregion -------------------------CTOR --------------------------

        
        #region ---------------------------- PROPERTIES ---------------------------


        #region     ----------------------------PROP ITEM  :  Keyword ---------------------------------
        // PARAMS :
        // PropName   -      Keyword         EX: AddNewButton     
        // PropType   -      String         EX: Returning Result Type
        // PropAccess   -    public       EX: public
        // PropSetAccess   -     EX: internal
        // PropDesc  -       Публикуемое данным методом Ключевое слово. Отправляется  к менеджеру публикаций          EX: Adding New Button To the Control Buttons Collection   

        /// <summary>
        /// Публикуемое данным методом Ключевое слово. Отправляется  к менеджеру публикаций. Указывается при методе отвечающем за спецификацию ключевого слова в классе Писателя Ключевых слов. 
        /// </summary>        
        public  String  Keyword
        {
            get; 
             set;
        }

        #endregion  -------------------------- PROP ITEM  :  Keyword ---------------------------------
    
    
        #endregion ---------------------------- PROPERTIES ---------------------------
                
    }
}
