﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTG;

namespace CTG.Core
{

   
    public partial class KeywordWriter1 
    {

        #region  --------------------------- CYCLES META INFO -----------------------------------

        
    //-------------------------- CYCLE METADATA KEYS ----------------------------
    //CYCLE METADATAKEY  1: EntityProperties  in template  Cycle.AllEntityPropertiesEqualThis.snippet  
    //CYCLE METADATAKEY  2: Entities   in template  Cycle.EntityClasses.snippet  
    //CYCLE METADATAKEY  3: PrimaryKeyProperties  in template  Cycle.PrimaryKeyProperties.snippet  


    //-------------------------- CYCLE CONTENTTEMPLATE KEYS ----------------------------
    //CYCLE METADATAKEY  1: Classattribute  in template  Cycle.ClassAttributes.snippet   
    //CYCLE METADATAKEY  2: Interface  in template  Cycle.InterfaceDeclarations.snippet   
    //CYCLE METADATAKEY  3: Using  in template  Cycle.UsingsAll.snippet   


    //-------------------------- CYCLE CONDITIONS KEYS ----------------------------
    //CYCLE CONDITION  1:  TablesOnlyCondition   in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  2:  VOOnlyCondition  in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  3: EntityTypeOnlyCondition  in template  Cycle.InterfaceDeclarations.snippet   


#endregion --------------------------- CYCLES META INFO -----------------------------------


        #region  ------------------------------ CONDITIONS ---------------------------------------



        #endregion ----------------------------- CONDITIONS --------------------------------------

    }
}
