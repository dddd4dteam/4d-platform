﻿using CTG.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTG;

namespace CTG.Core
{

    [KeywordWriterAttribute("KeywordWriter1","1.0.2000.001", "cs", "SimpleClassGenerationFlow")]
    public partial class KeywordWriter1 : KeywordWriterBase
    {

        #region   ----------------------- CTOR ----------------------------

        public KeywordWriter1() : base() { }

        #endregion ----------------------- CTOR ----------------------------
        

        #region  --------------------------- CYCLES META INFO -----------------------------------

        
    //-------------------------- CYCLE METADATA KEYS ----------------------------
    //CYCLE METADATAKEY  1: EntityProperties  in template  Cycle.AllEntityPropertiesEqualThis.snippet  
    //CYCLE METADATAKEY  2: Entities   in template  Cycle.EntityClasses.snippet  
    //CYCLE METADATAKEY  3: PrimaryKeyProperties  in template  Cycle.PrimaryKeyProperties.snippet  


    //-------------------------- CYCLE CONTENTTEMPLATE KEYS ----------------------------
    //CYCLE METADATAKEY  1: Classattribute  in template  Cycle.ClassAttributes.snippet   
    //CYCLE METADATAKEY  2: Interface  in template  Cycle.InterfaceDeclarations.snippet   
    //CYCLE METADATAKEY  3: Using  in template  Cycle.UsingsAll.snippet   


    //-------------------------- CYCLE CONDITIONS KEYS ----------------------------
    //CYCLE CONDITION  1:  TablesOnlyCondition   in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  2:  VOOnlyCondition  in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  3: EntityTypeOnlyCondition  in template  Cycle.InterfaceDeclarations.snippet   


#endregion --------------------------- CYCLES META INFO -----------------------------------


        #region-------------------------- PUBLISHING FOR ALL KEYWORDS ----------------------------

        #region  ------------------------------ KEYWORD [1]  [defaultbaseclass] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [defaultbaseclass] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("defaultbaseclass")]
        public KeywordInfo publishKeywordAction_defaultbaseclass()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "defaultbaseclass";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [1]  [defaultbaseclass] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [2]  [defaultclass] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [defaultclass] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("defaultclass")]
        public KeywordInfo publishKeywordAction_defaultclass()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "defaultclass";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [2]  [defaultclass] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Classattribute.DataContract.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [datacontract] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("datacontract")]
        public KeywordInfo publishKeywordAction_datacontract()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "datacontract";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Classattribute.DataContract.snippet",
                Templateword = "classattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [4]  [tablename] | Templateword [classattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Classattribute.TableName.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [tablename] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("tablename")]
        public KeywordInfo publishKeywordAction_tablename()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "tablename";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Classattribute.TableName.snippet",
                Templateword = "classattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [4]  [tablename] | Templateword [classattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [5]  [classkeyproperty] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [classkeyproperty] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("classkeyproperty")]
        public KeywordInfo publishKeywordAction_classkeyproperty()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "classkeyproperty";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [5]  [classkeyproperty] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [6]  [defaultclassproperty] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [defaultclassproperty] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("defaultclassproperty")]
        public KeywordInfo publishKeywordAction_defaultclassproperty()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "defaultclassproperty";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [6]  [defaultclassproperty] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [7]  [allentitypropertiesequalthis] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.AllEntityPropertiesEqualThis.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [allentitypropertiesequalthis] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("allentitypropertiesequalthis")]
        public KeywordInfo publishKeywordAction_allentitypropertiesequalthis()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "allentitypropertiesequalthis";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.AllEntityPropertiesEqualThis.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [7]  [allentitypropertiesequalthis] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [8]  [allvoparenttablepropertiesequalthis] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.AllVoParentTablePropertiesEqualThis.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [allvoparenttablepropertiesequalthis] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("allvoparenttablepropertiesequalthis")]
        public KeywordInfo publishKeywordAction_allvoparenttablepropertiesequalthis()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "allvoparenttablepropertiesequalthis";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.AllVoParentTablePropertiesEqualThis.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [8]  [allvoparenttablepropertiesequalthis] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [9]  [classattributes] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.ClassAttributes.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [classattributes] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("classattributes")]
        public KeywordInfo publishKeywordAction_classattributes()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "classattributes";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.ClassAttributes.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [9]  [classattributes] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [10]  [classproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.ClassProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [classproperties] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("classproperties")]
        public KeywordInfo publishKeywordAction_classproperties()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "classproperties";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.ClassProperties.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [10]  [classproperties] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [11]  [entityclasses] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.EntityClasses.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [entityclasses] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityclasses")]
        public KeywordInfo publishKeywordAction_entityclasses()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityclasses";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.EntityClasses.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [11]  [entityclasses] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [12]  [entityproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.EntityProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [entityproperties] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityproperties")]
        public KeywordInfo publishKeywordAction_entityproperties()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityproperties";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.EntityProperties.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [12]  [entityproperties] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [13]  [interfacedeclarations] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.InterfaceDeclarations.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [interfacedeclarations] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("interfacedeclarations")]
        public KeywordInfo publishKeywordAction_interfacedeclarations()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "interfacedeclarations";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.InterfaceDeclarations.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [13]  [interfacedeclarations] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [14]  [interfaceimplementations] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.InterfaceImplementations.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [interfaceimplementations] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("interfaceimplementations")]
        public KeywordInfo publishKeywordAction_interfaceimplementations()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "interfaceimplementations";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.InterfaceImplementations.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [14]  [interfaceimplementations] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [15]  [primarykeyproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.PrimaryKeyProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [primarykeyproperties] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("primarykeyproperties")]
        public KeywordInfo publishKeywordAction_primarykeyproperties()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "primarykeyproperties";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.PrimaryKeyProperties.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [15]  [primarykeyproperties] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [16]  [usingsall] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.UsingsAll.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [usingsall] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("usingsall")]
        public KeywordInfo publishKeywordAction_usingsall()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "usingsall";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Cycle.UsingsAll.snippet",
                Templateword = "cycle",
                TemplateType = TemplateTypeEn.Cycle
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [16]  [usingsall] | Templateword [cycle]   ----------------------------------


        #region  ------------------------------ KEYWORD [17]  [defaultpropertycloneequal] | Templateword [equal]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Equal.DefaultPropertyCloneEqual.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [defaultpropertycloneequal] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("defaultpropertycloneequal")]
        public KeywordInfo publishKeywordAction_defaultpropertycloneequal()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "defaultpropertycloneequal";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Equal.DefaultPropertyCloneEqual.snippet",
                Templateword = "equal",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [17]  [defaultpropertycloneequal] | Templateword [equal]   ----------------------------------


        #region  ------------------------------ KEYWORD [18]  [ivobject] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [ivobject] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("ivobject")]
        public KeywordInfo publishKeywordAction_ivobject()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "ivobject";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [18]  [ivobject] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [19]  [defaultnamespace] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [defaultnamespace] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("defaultnamespace")]
        public KeywordInfo publishKeywordAction_defaultnamespace()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "defaultnamespace";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [19]  [defaultnamespace] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [20]  [boidentityproperty] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.BOIdentityProperty.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [boidentityproperty] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("boidentityproperty")]
        public KeywordInfo publishKeywordAction_boidentityproperty()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "boidentityproperty";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Propertyattribute.BOIdentityProperty.snippet",
                Templateword = "propertyattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [20]  [boidentityproperty] | Templateword [propertyattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [21]  [bopropertyrole] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [bopropertyrole] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("bopropertyrole")]
        public KeywordInfo publishKeywordAction_bopropertyrole()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "bopropertyrole";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [21]  [bopropertyrole] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [22]  [datamember] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.DataMember.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [datamember] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("datamember")]
        public KeywordInfo publishKeywordAction_datamember()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "datamember";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Propertyattribute.DataMember.snippet",
                Templateword = "propertyattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [22]  [datamember] | Templateword [propertyattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [23]  [nullable] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.Nullable.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [nullable] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("nullable")]
        public KeywordInfo publishKeywordAction_nullable()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "nullable";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Propertyattribute.Nullable.snippet",
                Templateword = "propertyattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [23]  [nullable] | Templateword [propertyattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [24]  [nullableattribute] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.NullableAttribute.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [nullableattribute] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("nullableattribute")]
        public KeywordInfo publishKeywordAction_nullableattribute()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "nullableattribute";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Propertyattribute.NullableAttribute.snippet",
                Templateword = "propertyattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [24]  [nullableattribute] | Templateword [propertyattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [25]  [primarykey] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [primarykey] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("primarykey")]
        public KeywordInfo publishKeywordAction_primarykey()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "primarykey";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [25]  [primarykey] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [26]  [primarykeyattribute] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.PrimaryKeyAttribute.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [primarykeyattribute] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("primarykeyattribute")]
        public KeywordInfo publishKeywordAction_primarykeyattribute()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "primarykeyattribute";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Propertyattribute.PrimaryKeyAttribute.snippet",
                Templateword = "propertyattribute",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [26]  [primarykeyattribute] | Templateword [propertyattribute]   ----------------------------------


        #region  ------------------------------ KEYWORD [27]  [protomember] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [protomember] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("protomember")]
        public KeywordInfo publishKeywordAction_protomember()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "protomember";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [27]  [protomember] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [28]  [entitiesinonefile] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntitiesInOneFile.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [3] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entitiesinonefile] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entitiesinonefile")]
        public KeywordInfo publishKeywordAction_entitiesinonefile()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entitiesinonefile";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntitiesInOneFile.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [28]  [entitiesinonefile] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [29]  [entityperfile] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [3] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile")]
        public KeywordInfo publishKeywordAction_entityperfile()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [29]  [entityperfile] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [30]  [entityperfile1] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile1.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile1] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile1")]
        public KeywordInfo publishKeywordAction_entityperfile1()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile1";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile1.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [30]  [entityperfile1] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [31]  [entityperfile2] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile2.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [3] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [4] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [5] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile2] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile2")]
        public KeywordInfo publishKeywordAction_entityperfile2()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile2";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile2.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [31]  [entityperfile2] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [32]  [entityperfile3] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile3.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [3] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [4] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [5] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [6] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile3] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile3")]
        public KeywordInfo publishKeywordAction_entityperfile3()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile3";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile3.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [32]  [entityperfile3] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [33]  [entityperfile4] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile4.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile4] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile4")]
        public KeywordInfo publishKeywordAction_entityperfile4()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile4";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile4.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [33]  [entityperfile4] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [34]  [entityperfile5] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile5.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [CTG.Core.ReferenceInfo]
        //                REFERENCE  [2] :  [CTG.Core.ReferenceInfo]

        /// <summary>
        /// Publishing keyword [entityperfile5] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityperfile5")]
        public KeywordInfo publishKeywordAction_entityperfile5()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityperfile5";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Start.EntityPerFile5.snippet",
                Templateword = "start",
                TemplateType = TemplateTypeEn.Start
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [34]  [entityperfile5] | Templateword [start]   ----------------------------------


        #region  ------------------------------ KEYWORD [35]  [bltoolkitusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.BLToolkitUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [bltoolkitusg] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("bltoolkitusg")]
        public KeywordInfo publishKeywordAction_bltoolkitusg()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "bltoolkitusg";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Using.BLToolkitUsg.snippet",
                Templateword = "using",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [35]  [bltoolkitusg] | Templateword [using]   ----------------------------------


        #region  ------------------------------ KEYWORD [36]  [coreusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.CoreUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [coreusg] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("coreusg")]
        public KeywordInfo publishKeywordAction_coreusg()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "coreusg";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Using.CoreUsg.snippet",
                Templateword = "using",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [36]  [coreusg] | Templateword [using]   ----------------------------------


        #region  ------------------------------ KEYWORD [37]  [systemusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.SystemUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:

        /// <summary>
        /// Publishing keyword [systemusg] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("systemusg")]
        public KeywordInfo publishKeywordAction_systemusg()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "systemusg";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = true,
                TemplateFilePath = @"C:\Platform\Generators\Templates\cs\MobileObjectTemplate\Using.SystemUsg.snippet",
                Templateword = "using",
                TemplateType = TemplateTypeEn.Content
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [37]  [systemusg] | Templateword [using]   ----------------------------------


        #region  ------------------------------ KEYWORD [38]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [entityname] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("entityname")]
        public KeywordInfo publishKeywordAction_entityname()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "entityname";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [38]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [39]  [classpartial] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [classpartial] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("classpartial")]
        public KeywordInfo publishKeywordAction_classpartial()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "classpartial";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [39]  [classpartial] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [40]  [propertyname] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [propertyname] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("propertyname")]
        public KeywordInfo publishKeywordAction_propertyname()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "propertyname";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [40]  [propertyname] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [41]  [propertytype] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [propertytype] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("propertytype")]
        public KeywordInfo publishKeywordAction_propertytype()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "propertytype";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [41]  [propertytype] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [42]  [nullabletype] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [nullabletype] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("nullabletype")]
        public KeywordInfo publishKeywordAction_nullabletype()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "nullabletype";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [42]  [nullabletype] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [43]  [dbtypename] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [dbtypename] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("dbtypename")]
        public KeywordInfo publishKeywordAction_dbtypename()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "dbtypename";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [43]  [dbtypename] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [44]  [voparenttable] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [voparenttable] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("voparenttable")]
        public KeywordInfo publishKeywordAction_voparenttable()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "voparenttable";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [44]  [voparenttable] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [45]  [subsystem] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [subsystem] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("subsystem")]
        public KeywordInfo publishKeywordAction_subsystem()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "subsystem";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [45]  [subsystem] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [46]  [bopropertyroleenumkey] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [bopropertyroleenumkey] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("bopropertyroleenumkey")]
        public KeywordInfo publishKeywordAction_bopropertyroleenumkey()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "bopropertyroleenumkey";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [46]  [bopropertyroleenumkey] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [47]  [primarykeypropertyindex] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [primarykeypropertyindex] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("primarykeypropertyindex")]
        public KeywordInfo publishKeywordAction_primarykeypropertyindex()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "primarykeypropertyindex";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [47]  [primarykeypropertyindex] | Templateword [WithoutTemplate]   ----------------------------------


        #region  ------------------------------ KEYWORD [48]  [protopropertyindex] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate

        /// <summary>
        /// Publishing keyword [protopropertyindex] in PublicationManager.Keywords collection
        /// </summary>
        /// <returns></returns>
        [PublishKeywordAction("protopropertyindex")]
        public KeywordInfo publishKeywordAction_protopropertyindex()
        {
            KeywordInfo publishingKeyword = new KeywordInfo();
            publishingKeyword.Keyword = "protopropertyindex";
            publishingKeyword.Writer = new KeywordWriterInfo()
            {
                Name = this.Name,
                Version = this.Version
            };
            // writer need to say: about Templateword
            publishingKeyword.Templateword = new TemplatewordInfo()
            {
                IsContainsTemplate = false,
                TemplateFilePath = "",
                Templateword = "",
                TemplateType = TemplateTypeEn.WithoutTemplate
            };


            return publishingKeyword;
        }



        #endregion  ------------------------------ KEYWORD [48]  [protopropertyindex] | Templateword [WithoutTemplate]   ----------------------------------



        #endregion-------------------------- PUBLISHING FOR ALL KEYWORDS ----------------------------
            
    
    }
}
