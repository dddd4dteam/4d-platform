﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTG;
using NMG.Core.Domain;

namespace CTG.Core
{

  

   
    public partial class KeywordWriter1 
    {

        #region  --------------------------- CYCLES META INFO -----------------------------------

        
    //-------------------------- CYCLE METADATA KEYS ----------------------------
    //CYCLE METADATAKEY  1: EntityProperties  in template  Cycle.AllEntityPropertiesEqualThis.snippet  
    //CYCLE METADATAKEY  2: Entities   in template  Cycle.EntityClasses.snippet  
    //CYCLE METADATAKEY  3: PrimaryKeyProperties  in template  Cycle.PrimaryKeyProperties.snippet  


    //-------------------------- CYCLE CONTENTTEMPLATE KEYS ----------------------------
    //CYCLE METADATAKEY  1: Classattribute  in template  Cycle.ClassAttributes.snippet   
    //CYCLE METADATAKEY  2: Interface  in template  Cycle.InterfaceDeclarations.snippet   
    //CYCLE METADATAKEY  3: Using  in template  Cycle.UsingsAll.snippet   


    //-------------------------- CYCLE CONDITIONS KEYS ----------------------------
    //CYCLE CONDITION  1:  TablesOnlyCondition   in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  2:  VOOnlyCondition  in template  Cycle.EntityClasses.snippet   
    //CYCLE CONDITION  3: EntityTypeOnlyCondition  in template  Cycle.InterfaceDeclarations.snippet   


#endregion --------------------------- CYCLES META INFO -----------------------------------

        ///----------------------------------------------------------------------------
        ///-----------------Generation Flow Name  SimpleClassGenerationFlow ---------------------  
        ///----------------------------------------------------------------------------


        // mapped keywords 
        // classpartial
        // nullabletype
        // subsystem
        // 


        #region -------------------------- BUILDING FOR WithoutTemplate KEYWORDS ----------------------------


        #region  ------------------------------ KEYWORD [1]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [entityname] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityname")]
        public void buildKeywordAction_entityname(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityname"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [1]  [entityname] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [2]  [classpartial] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [classpartial] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("classpartial")]
        public void buildKeywordAction_classpartial(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["classpartial"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [2]  [classpartial] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [3]  [propertyname] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [propertyname] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("propertyname")]
        public void buildKeywordAction_propertyname(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["propertyname"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [3]  [propertyname] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [4]  [propertytype] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [propertytype] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("propertytype")]
        public void buildKeywordAction_propertytype(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["propertytype"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [4]  [propertytype] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [5]  [nullabletype] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [nullabletype] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("nullabletype")]
        public void buildKeywordAction_nullabletype(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["nullabletype"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [5]  [nullabletype] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [6]  [dbtypename] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [dbtypename] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("dbtypename")]
        public void buildKeywordAction_dbtypename(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["dbtypename"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [6]  [dbtypename] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [7]  [voparenttable] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [voparenttable] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("voparenttable")]
        public void buildKeywordAction_voparenttable(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["voparenttable"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [7]  [voparenttable] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [8]  [subsystem] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [subsystem] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("subsystem")]
        public void buildKeywordAction_subsystem(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["subsystem"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [8]  [subsystem] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [9]  [bopropertyroleenumkey] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [bopropertyroleenumkey] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("bopropertyroleenumkey")]
        public void buildKeywordAction_bopropertyroleenumkey(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["bopropertyroleenumkey"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [9]  [bopropertyroleenumkey] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [10]  [primarykeypropertyindex] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [primarykeypropertyindex] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("primarykeypropertyindex")]
        public void buildKeywordAction_primarykeypropertyindex(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["primarykeypropertyindex"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [10]  [primarykeypropertyindex] | Templateword [WithoutTemplate]   ----------------------------------

        #region  ------------------------------ KEYWORD [11]  [protopropertyindex] | Templateword [WithoutTemplate]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: WithoutTemplate


        /// <summary>
        /// Building [protopropertyindex] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("protopropertyindex")]
        public void buildKeywordAction_protopropertyindex(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["protopropertyindex"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [11]  [protopropertyindex] | Templateword [WithoutTemplate]   ----------------------------------

        #endregion-------------------------- BUILDING FOR WithoutTemplate KEYWORDS ----------------------------



    }
}
