﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTG;
using NMG.Core.Domain;

namespace CTG.Core
{
    
    public partial class KeywordWriter1 
    {



        ///----------------------------------------------------------------------------
        ///-----------------Generation Flow Name  SimpleClassGenerationFlow ---------------------  
        ///----------------------------------------------------------------------------

        #region -------------------------- BUILDING FOR TEMPLATED KEYWORDS ----------------------------


        #region  ------------------------------ KEYWORD [1]  [defaultbaseclass] | Templateword [baseclass]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: BaseClass.DefaultBaseClass.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [declaration] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [entityname]
        //       PART [2] with Key [implementation] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [primarykeyproperties]
        //                REFERENCE  [2] :  [entityname]
        //                REFERENCE  [3] :  [entityname]
        //                REFERENCE  [4] :  [allentitypropertiesequalthis]


        /// <summary>
        /// Building [defaultbaseclass] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("defaultbaseclass")]
        public void buildKeywordAction_defaultbaseclass(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["defaultbaseclass"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [1]  [defaultbaseclass] | Templateword [baseclass]   ----------------------------------

        #region  ------------------------------ KEYWORD [2]  [defaultclass] | Templateword [class]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Class.DefaultClass.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [classattributes]
        //                REFERENCE  [2] :  [classpartial]
        //                REFERENCE  [3] :  [entityname]
        //                REFERENCE  [4] :  [defaultbaseclass.declaration]
        //                REFERENCE  [5] :  [interfacedeclarations]
        //                REFERENCE  [6] :  [classproperties]
        //                REFERENCE  [7] :  [defaultbaseclass.implementation]
        //                REFERENCE  [8] :  [interfaceimplementations]


        /// <summary>
        /// Building [defaultclass] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("defaultclass")]
        public void buildKeywordAction_defaultclass(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["defaultclass"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [2]  [defaultclass] | Templateword [class]   ----------------------------------

        #region  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Classattribute.DataContract.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [datacontract] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("datacontract")]
        public void buildKeywordAction_datacontract(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["datacontract"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [3]  [datacontract] | Templateword [classattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [4]  [tablename] | Templateword [classattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Classattribute.TableName.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [entityname]


        /// <summary>
        /// Building [tablename] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("tablename")]
        public void buildKeywordAction_tablename(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["tablename"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [4]  [tablename] | Templateword [classattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [5]  [classkeyproperty] | Templateword [classproperty]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: ClassProperty.ClassKeyProperty.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [propertyname]


        /// <summary>
        /// Building [classkeyproperty] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("classkeyproperty")]
        public void buildKeywordAction_classkeyproperty(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["classkeyproperty"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [5]  [classkeyproperty] | Templateword [classproperty]   ----------------------------------

        #region  ------------------------------ KEYWORD [6]  [defaultclassproperty] | Templateword [classproperty]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: ClassProperty.DefaultClassProperty.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [nullableattribute]
        //                REFERENCE  [2] :  [primarykeyattribute]
        //                REFERENCE  [3] :  [bopropertyrole]
        //                REFERENCE  [4] :  [boidentityproperty]
        //                REFERENCE  [5] :  [datamember]
        //                REFERENCE  [6] :  [protomember]
        //                REFERENCE  [7] :  [propertytype]
        //                REFERENCE  [8] :  [nullabletype]
        //                REFERENCE  [9] :  [propertyname]
        //                REFERENCE  [10] :  [dbtypename]


        /// <summary>
        /// Building [defaultclassproperty] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("defaultclassproperty")]
        public void buildKeywordAction_defaultclassproperty(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["defaultclassproperty"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [6]  [defaultclassproperty] | Templateword [classproperty]   ----------------------------------

        #region  ------------------------------ KEYWORD [7]  [allentitypropertiesequalthis] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.AllEntityPropertiesEqualThis.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [allentitypropertiesequalthis] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("allentitypropertiesequalthis")]
        public void buildKeywordAction_allentitypropertiesequalthis(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["allentitypropertiesequalthis"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [7]  [allentitypropertiesequalthis] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [8]  [allvoparenttablepropertiesequalthis] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.AllVoParentTablePropertiesEqualThis.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [allvoparenttablepropertiesequalthis] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("allvoparenttablepropertiesequalthis")]
        public void buildKeywordAction_allvoparenttablepropertiesequalthis(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["allvoparenttablepropertiesequalthis"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [8]  [allvoparenttablepropertiesequalthis] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [9]  [classattributes] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.ClassAttributes.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [classattributes] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("classattributes")]
        public void buildKeywordAction_classattributes(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["classattributes"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [9]  [classattributes] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [10]  [classproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.ClassProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [classproperties] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("classproperties")]
        public void buildKeywordAction_classproperties(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["classproperties"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [10]  [classproperties] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [11]  [entityclasses] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.EntityClasses.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [entityclasses] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityclasses")]
        public void buildKeywordAction_entityclasses(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityclasses"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [11]  [entityclasses] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [12]  [entityproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.EntityProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [entityproperties] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityproperties")]
        public void buildKeywordAction_entityproperties(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityproperties"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [12]  [entityproperties] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [13]  [interfacedeclarations] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.InterfaceDeclarations.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [interfacedeclarations] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("interfacedeclarations")]
        public void buildKeywordAction_interfacedeclarations(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["interfacedeclarations"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [13]  [interfacedeclarations] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [14]  [interfaceimplementations] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.InterfaceImplementations.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [interfaceimplementations] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("interfaceimplementations")]
        public void buildKeywordAction_interfaceimplementations(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["interfaceimplementations"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [14]  [interfaceimplementations] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [15]  [primarykeyproperties] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.PrimaryKeyProperties.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [primarykeyproperties] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("primarykeyproperties")]
        public void buildKeywordAction_primarykeyproperties(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["primarykeyproperties"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [15]  [primarykeyproperties] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [16]  [usingsall] | Templateword [cycle]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Cycle.UsingsAll.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [cycle] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [usingsall] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("usingsall")]
        public void buildKeywordAction_usingsall(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["usingsall"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [16]  [usingsall] | Templateword [cycle]   ----------------------------------

        #region  ------------------------------ KEYWORD [17]  [defaultpropertycloneequal] | Templateword [equal]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Equal.DefaultPropertyCloneEqual.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [propertyname]
        //                REFERENCE  [2] :  [propertyname]


        /// <summary>
        /// Building [defaultpropertycloneequal] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("defaultpropertycloneequal")]
        public void buildKeywordAction_defaultpropertycloneequal(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["defaultpropertycloneequal"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [17]  [defaultpropertycloneequal] | Templateword [equal]   ----------------------------------

        #region  ------------------------------ KEYWORD [18]  [ivobject] | Templateword [interface]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Interface.IVobject.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [declaration] 
        //            KEYWORDREFERENCIES:
        //       PART [2] with Key [implementation] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [voparenttable]
        //                REFERENCE  [2] :  [voparenttable]
        //                REFERENCE  [3] :  [allvoparenttablepropertiesequalthis]


        /// <summary>
        /// Building [ivobject] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("ivobject")]
        public void buildKeywordAction_ivobject(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["ivobject"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [18]  [ivobject] | Templateword [interface]   ----------------------------------

        #region  ------------------------------ KEYWORD [19]  [defaultnamespace] | Templateword [namespace]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Namespace.DefaultNamespace.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [subsystem]


        /// <summary>
        /// Building [defaultnamespace] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("defaultnamespace")]
        public void buildKeywordAction_defaultnamespace(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["defaultnamespace"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [19]  [defaultnamespace] | Templateword [namespace]   ----------------------------------

        #region  ------------------------------ KEYWORD [20]  [boidentityproperty] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.BOIdentityProperty.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [boidentityproperty] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("boidentityproperty")]
        public void buildKeywordAction_boidentityproperty(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["boidentityproperty"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [20]  [boidentityproperty] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [21]  [bopropertyrole] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.BOPropertyRole.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [1] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [bopropertyroleenumkey]
        //       PART [2] with Key [2] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [bopropertyroleenumkey]
        //                REFERENCE  [2] :  [entityname]


        /// <summary>
        /// Building [bopropertyrole] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("bopropertyrole")]
        public void buildKeywordAction_bopropertyrole(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["bopropertyrole"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [21]  [bopropertyrole] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [22]  [datamember] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.DataMember.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [datamember] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("datamember")]
        public void buildKeywordAction_datamember(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["datamember"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [22]  [datamember] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [23]  [nullable] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.Nullable.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [nullable] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("nullable")]
        public void buildKeywordAction_nullable(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["nullable"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [23]  [nullable] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [24]  [nullableattribute] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.NullableAttribute.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [nullableattribute] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("nullableattribute")]
        public void buildKeywordAction_nullableattribute(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["nullableattribute"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [24]  [nullableattribute] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [25]  [primarykey] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.PrimaryKey.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [primarykeypropertyindex]


        /// <summary>
        /// Building [primarykey] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("primarykey")]
        public void buildKeywordAction_primarykey(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["primarykey"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [25]  [primarykey] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [26]  [primarykeyattribute] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.PrimaryKeyAttribute.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [primarykeypropertyindex]


        /// <summary>
        /// Building [primarykeyattribute] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("primarykeyattribute")]
        public void buildKeywordAction_primarykeyattribute(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["primarykeyattribute"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [26]  [primarykeyattribute] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [27]  [protomember] | Templateword [propertyattribute]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Propertyattribute.ProtoMember.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [protopropertyindex]


        /// <summary>
        /// Building [protomember] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("protomember")]
        public void buildKeywordAction_protomember(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["protomember"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [27]  [protomember] | Templateword [propertyattribute]   ----------------------------------

        #region  ------------------------------ KEYWORD [28]  [entitiesinonefile] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntitiesInOneFile.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]
        //                REFERENCE  [3] :  [entityclasses]


        /// <summary>
        /// Building [entitiesinonefile] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entitiesinonefile")]
        public void buildKeywordAction_entitiesinonefile(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entitiesinonefile"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [28]  [entitiesinonefile] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [29]  [entityperfile] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]
        //                REFERENCE  [3] :  [defaultclass]


        /// <summary>
        /// Building [entityperfile] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile")]
        public void buildKeywordAction_entityperfile(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [29]  [entityperfile] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [30]  [entityperfile1] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile1.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]


        /// <summary>
        /// Building [entityperfile1] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile1")]
        public void buildKeywordAction_entityperfile1(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile1"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [30]  [entityperfile1] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [31]  [entityperfile2] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile2.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]
        //                REFERENCE  [3] :  [classattributes]
        //                REFERENCE  [4] :  [classpartial]
        //                REFERENCE  [5] :  [entityname]


        /// <summary>
        /// Building [entityperfile2] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile2")]
        public void buildKeywordAction_entityperfile2(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile2"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [31]  [entityperfile2] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [32]  [entityperfile3] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile3.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]
        //                REFERENCE  [3] :  [classpartial]
        //                REFERENCE  [4] :  [entityname]
        //                REFERENCE  [5] :  [defaultbaseclass.declaration]


        /// <summary>
        /// Building [entityperfile3] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile3")]
        public void buildKeywordAction_entityperfile3(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile3"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [32]  [entityperfile3] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [33]  [entityperfile4] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile4.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]


        /// <summary>
        /// Building [entityperfile4] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile4")]
        public void buildKeywordAction_entityperfile4(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile4"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [33]  [entityperfile4] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [34]  [entityperfile5] | Templateword [start]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Start.EntityPerFile5.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:
        //                REFERENCE  [1] :  [usingsall]
        //                REFERENCE  [2] :  [defaultnamespace]


        /// <summary>
        /// Building [entityperfile5] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("entityperfile5")]
        public void buildKeywordAction_entityperfile5(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["entityperfile5"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [34]  [entityperfile5] | Templateword [start]   ----------------------------------

        #region  ------------------------------ KEYWORD [35]  [bltoolkitusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.BLToolkitUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [bltoolkitusg] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("bltoolkitusg")]
        public void buildKeywordAction_bltoolkitusg(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["bltoolkitusg"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [35]  [bltoolkitusg] | Templateword [using]   ----------------------------------

        #region  ------------------------------ KEYWORD [36]  [coreusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.CoreUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [coreusg] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("coreusg")]
        public void buildKeywordAction_coreusg(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["coreusg"];
            // TO DO: private logic to build keyword 



        }
        #endregion  ------------------------------ KEYWORD [36]  [coreusg] | Templateword [using]   ----------------------------------

        #region  ------------------------------ KEYWORD [37]  [systemusg] | Templateword [using]   ----------------------------------
        // ---------DECLARATION-----------
        // TEMPLATEFILE: Using.SystemUsg.snippet
        // TEMPLATEPARTS:
        //       PART [1] with Key [standartpart] 
        //            KEYWORDREFERENCIES:


        /// <summary>
        /// Building [systemusg] keyword.template.ResultExpression
        /// </summary>
        /// <returns></returns>
        [BuildKeywordExpressionAction("systemusg")]
        public void buildKeywordAction_systemusg(String Templatepart)
        {

            KeywordInfo currentKeyword = GenerationContext.PublishedKeywords["systemusg"];
            // TO DO: private logic to build keyword 

        }
        #endregion  ------------------------------ KEYWORD [37]  [systemusg] | Templateword [using]   ----------------------------------

        #endregion-------------------------- BUILDING FOR TEMPLATED KEYWORDS ----------------------------



    
    }

}
