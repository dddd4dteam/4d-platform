﻿using CTG.Configuration;
using CTG.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTG.Core
{
    /// <summary>
    /// Customizing  conditions for some complex cases that cannot be write CTG Script 
    /// </summary>
    [GenFlowLangManagerAttribute("MobileObjectGenFlow",  LanguageEn.cs )]
    public class MobileObjectGenFlowLangManager_CS : GenFlowLangManagerBase
    {

        #region ----------------------------- CTOR ------------------------------

        public MobileObjectGenFlowLangManager_CS() : base() { }
        
        #endregion  ----------------------------- CTOR ------------------------------
                

        //protected override void GenerateForEntityPerFile(CodeBuilder builder)
        //{

        //}


        //protected override void GenerateForEntitiesInOneFile(CodeBuilder builder)
        //{

        //}


    }
}
