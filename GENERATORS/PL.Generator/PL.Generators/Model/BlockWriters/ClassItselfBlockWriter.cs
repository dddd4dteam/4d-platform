﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Data.Primitives;
using PL.Generators.Model;
using Core.Meta.Parametrization;

namespace PL.Generators.Blocks
{
  

   /// <summary>
   /// Блока класса его Имени/  свойств / методов/ какие есть должны быть в классе 
   /// </summary> 
   public class ClassItselfBlockWriter : BlockWriterBase
   {


       public ClassItselfBlockWriter(IClassCompositor CurrentCompositor, ParametersCollectionTp CurrentParamsCollection)  
           : base(CurrentCompositor,CurrentParamsCollection)
        { }

       
       /// <summary>
       /// Вывод блока по маркеру позиции
       /// </summary>
       /// <returns></returns>
       protected override String WriteBlock(TransformationMarkerEn PositionMarker)
       {
           try
           {
               return "";
           }
           catch (Exception exc)
           {
               throw exc;
           }
       }  

   }

}
