﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Data.Primitives;
using PL.Generators.Model;
using Core.Meta.Parametrization;

namespace PL.Generators.Blocks
{
   //Enum для задания атрибутов на Структуры/Поля-Свойства/Методы 


   /// <summary>
   /// Блок вывода аттрибутов/ с делением на атрибуты свойств и/ методов и структур   
   /// </summary>
   public class AttributesBlockWriter : BlockWriterBase
   {

        public AttributesBlockWriter(IClassCompositor CurrentCompositor, ParametersCollectionTp CurrentParamsCollection)  
           : base(CurrentCompositor,CurrentParamsCollection)
        { }


       /// <summary>
       /// Вывод блока по маркеру позиции
       /// </summary>
       /// <returns></returns>
       protected  override String WriteBlock(TransformationMarkerEn PositionMarker)
       {
           try
           {
               return "";
           }
           catch (Exception exc)
           {
               throw exc;
           }
       }
   
   }
   
   


}
