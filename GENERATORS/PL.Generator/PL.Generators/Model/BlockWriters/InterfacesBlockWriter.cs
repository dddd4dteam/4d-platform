﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Data.Primitives;
using PL.Generators.Model;
using Core.Meta.Parametrization;

namespace PL.Generators.Blocks
{
   /// <summary>
   /// Блок вывода интерфейсов
   /// </summary>
   public class InterfacesBlockWriter : BlockWriterBase
   {


       public InterfacesBlockWriter(IClassCompositor CurrentCompositor, ParametersCollectionTp CurrentParamsCollection)  
           : base(CurrentCompositor,CurrentParamsCollection)
        { }



       /// <summary>
       ///  Необходимо реализовать данный метод в потомках/ он должен реализовать разные выводы для разных его мест Употребления
       ///  К примеру интерфейсы выводятся однажды у объявления класса и второй раз при реализации методов     
       /// </summary>
       /// <param name="parameters"></param>
       /// <returns></returns>
       /// <summary>
       /// Вывод блока
       /// </summary>
       /// <returns></returns>
       protected override String WriteBlock(TransformationMarkerEn PositionMarker)
       {
           try
           {
               return "";
           }
           catch (Exception exc)
           {
               throw exc;
           }
       }
// <copyright file="_Interfaces.tt" company="">
//  Copyright © . All Rights Reserved.
// </copyright>



//вывод объявления наследования интрфейсов
//static Action<TextTransformation,string,bool> WriteInterfacesImplementation = (tt,cl, hf) =>
//{
//        if(tt.IsIClonable )
//        {
//            if(hf)
//                tt.Write(", ");
//            else tt.Write(" : ");
//            tt.Write("ICloneable");
//        }
//        if(tt.UseIVObjectRealization && cl.Split(new char[]{'_'})[0] == "V" 
//            && cl.Split(new char[]{'_'}).Count()>=4 && cl.Split(new char[]{'_'})[3] == "Vo")
//        {
//            if(hf || tt.IsIClonable)
//                tt.Write(", ");
//            else tt.Write(" : ");
//            tt.Write("IVobject");
//        }
//};

////вывод заголовка клонирования
//static Action<GeneratedTextTransformation,Table,List<Table>> WriteIClonable = (tt,t,tlist)=>
//{
//        if(t.IsView && tt.UseIVObjectRealization && t.TableName.Split(new char[]{'_'}).Count()>=4 && t.TableName.Split(new char[]{'_'})[3]=="Vo") 
//        {
//            tt.WriteLine("");
//            WriteComment(tt," Summary:");
//            WriteComment(tt,"     Creates a new table object form Value Part of current item.");
//            WriteComment(tt,"");
//            WriteComment(tt," Returns:");
//            WriteComment(tt,"     Creates a new table object-Targer for current Value Part of current item.");
//            tt.WriteLine("public object CloneToTarget()");

//             //Получение имени класса таблицы данных
//            string tableName = tt.DomainPrefix+ "_"+ t.ClassName.Replace("V_","").Replace("_Vo", "");
//            Table targetTable = tlist.Where(tl => tl.ClassName == tableName).FirstOrDefault();

//            WriteIVObjectBody(tt,targetTable,tlist);
//        }
//        if(tt.IsIClonable)
//        {
//            tt.WriteLine("");
//            WriteComment(tt," Summary:");
//            WriteComment(tt,"     Creates a new object that is a copy of the current instance.");
//            WriteComment(tt,"");
//            WriteComment(tt," Returns:");
//            WriteComment(tt,"     A new object that is a copy of this instance.");
//            tt.WriteLine("public object Clone()");
//            WriteIClonableBody(tt,t,tlist);
//        }
//};


////Kr0t
////вывод тушки функции клонирования CloneToTarget у IVObject
//static Action<GeneratedTextTransformation,Table,List<Table>> WriteIVObjectBody =(tt,t,tlist)=>
//    {
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        tt.WriteLine("try");
//        tt.WriteLine("{");
//        tt.PushIndent("\t");

//        tt.WriteLine("#if (SILVERLIGHT || WINDOWS_PHONE)");
//        tt.WriteLine("    return Clone();");
//        tt.WriteLine("#else");
//        tt.PushIndent("\t");


//        tt.WriteLine("{0} clonedItem = new {0}()",t.ClassName);
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        foreach (var c in from c in t.Columns.Values orderby c.ID select c)
//        {
//            tt.Write("{0}{1} = this.{0}", c.MemberName, tt.LenDiff(tt.MaxColumnMemberLen, c.MemberName));
//            if(c!=(from col in t.Columns.Values orderby col.ID select col).Last())
//                tt.WriteLine(",");
//            else if( tt.UseForeignKeyProperties)
//                    {
//                if(t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null).Count()>0)
//                    tt.WriteLine(",");
//                    } 
//            else
//                tt.WriteLine("");
//        }

//        if( tt.UseForeignKeyProperties)
//        {
//            foreach (var key in t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null))
//            {
//                if(!tt.UseFiltrationForAssociations || tlist.Contains(key.OtherTable))
//                {
//                    tt.Write("{0}{1} = this.{0}", key.MemberName, tt.LenDiff(tt.MaxColumnMemberLen, key.MemberName));
//                    if(key!=(t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null)).Last())
//                        tt.WriteLine(",");
//                    else
//                        tt.WriteLine("");
//                }
//            }
//        }
//        tt.PopIndent();
//        tt.WriteLine("};");
//        tt.WriteLine("return clonedItem;");
//        tt.PopIndent();
//        tt.WriteLine("#endif");

//        tt.PopIndent();
//        tt.WriteLine("}");
//        tt.WriteLine("catch (Exception exc)");
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        tt.WriteLine("throw exc;");
//        tt.PopIndent();
//        tt.WriteLine("}");
//        tt.PopIndent();
//        tt.WriteLine("}");
//    };



////Kr0t
////вывод тушки функции клонирования
//static Action<TextTransformation,Table,List<Table>> WriteIClonableBody =(tt,t,tlist)=>
//    {
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        tt.WriteLine("try");
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        tt.WriteLine("{0} clonedItem = new {0}()",t.ClassName);
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        foreach (var c in from c in t.Columns.Values orderby c.ID select c)
//        {
//            tt.Write("{0}{1} = this.{0}", c.MemberName, tt.LenDiff(tt.MaxColumnMemberLen, c.MemberName));
//            if(c!=(from col in t.Columns.Values orderby col.ID select col).Last())
//                tt.WriteLine(",");
//            else if( tt.UseForeignKeyProperties)
//                    {
//                if(t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null).Count()>0)
//                    tt.WriteLine(",");
//                    } 
//            else
//                tt.WriteLine("");
//        }

//        if( tt.UseForeignKeyProperties)
//        {
//            foreach (var key in t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null))
//            {
//                if(!tt.UseFiltrationForAssociations || tlist.Contains(key.OtherTable))
//                {
//                    tt.Write("{0}{1} = this.{0}", key.MemberName, tt.LenDiff(tt.MaxColumnMemberLen, key.MemberName));
//                    if(key!=(t.ForeignKeys.Values.Where(k => tt.RenderBackReferences || k.BackReference != null)).Last())
//                        tt.WriteLine(",");
//                    else
//                        tt.WriteLine("");
//                }
//            }
//        }
//        tt.PopIndent();
//        tt.WriteLine("};");
//        tt.WriteLine("return clonedItem;");
//        tt.PopIndent();
//        tt.WriteLine("}");
//        tt.WriteLine("catch (Exception exc)");
//        tt.WriteLine("{");
//        tt.PushIndent("\t");
//        tt.WriteLine("throw exc;");
//        tt.PopIndent();
//        tt.WriteLine("}");
//        tt.PopIndent();
//        tt.WriteLine("}");
//    };








   }



}
