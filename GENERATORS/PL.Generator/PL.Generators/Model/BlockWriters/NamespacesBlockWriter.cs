﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Data.Primitives;
using PL.Generators.Model;
using Core.Meta.Parametrization;

namespace PL.Generators.Blocks
{
  
   /// <summary>
   /// Блок вывода немспайсов
   /// </summary> 
   public class NamespacesBlockWriter : BlockWriterBase
   {

       public NamespacesBlockWriter(IClassCompositor CurrentCompositor, ParametersCollectionTp CurrentParamsCollection)  
           : base(CurrentCompositor,CurrentParamsCollection)
        { }



       /// <summary>
       /// Вывод блока по маркеру позиции
       /// </summary>
       /// <returns></returns>
       protected override String WriteBlock(TransformationMarkerEn PositionMarker)
       {
           try
           {
               return "";
           }
           catch (Exception exc)
           {
               throw exc;
           }
       }
  

   }
   

}
