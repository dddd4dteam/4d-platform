﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using Core.Meta.Parametrization;

namespace PL.Generators.Model
{

    /// <summary>
    /// Блок Райтер базовый
    /// </summary>
    public class BlockWriterBase :  IBlockWriter
    {
        /// <summary>
        /// Конструктор - Запись блока 
        /// </summary>
        /// <param name="CurrentTransformation"></param>
        public BlockWriterBase(IClassCompositor CurrentCompositor)
        {
            _classCompositor = CurrentCompositor;
            _transformation = CurrentCompositor.TransformationTemplate;
        }


        /// <summary>
        /// Конструктор - Запись блока 
        /// </summary>
        /// <param name="CurrentTransformation"></param>
        /// <param name="CurrentParamsCollection"></param>
        public BlockWriterBase(IClassCompositor CurrentCompositor, ParametersCollectionTp CurrentParamsCollection)
        {
            _classCompositor = CurrentCompositor;
            _transformation = CurrentCompositor.TransformationTemplate;
            _parameters = CurrentParamsCollection;
        }


        IClassCompositor _classCompositor = null;

        /// <summary>
        ///  Композитор врайтера
        /// </summary>
        protected IClassCompositor ClassCompositor
        {
            get { return _classCompositor; }
        }



        private ClassBlockEn _blockKey;
       
        /// <summary>
        /// Ключ данного БлокВрайтера
        /// </summary>
        public ClassBlockEn BlockKey
        {
            get { return _blockKey; }
            set { _blockKey = value; }
        }


        TextTransformation _transformation = null;
        /// <summary>
        /// Текущий шаблон
        /// </summary>
        protected TextTransformation Transformation
        {
            get {  return _transformation;  }
        }


        ParametersCollectionTp _parameters = null;
        /// <summary>
        /// Параметры Блока вывода
        /// </summary>
        protected ParametersCollectionTp Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }


        /// <summary>
        /// Установить параметры для Блока Вывода класса
        /// </summary>
        /// <param name="parameters"></param>
        public void SetParameters(ParametersCollectionTp CurrentParameters)
        {
            try
            {
                _parameters = CurrentParameters;
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        /// <summary>
        ///  Необходимо реализовать данный метод в потомках/ он должен реализовать разные выводы для разных его мест Употребления
        ///  К примеру интерфейсы выводятся однажды у объявления класса и второй раз при реализации методов     
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        protected virtual String WriteBlock(TransformationMarkerEn PositionMarker)
        {
            return null; 
        }


       

    }
}
