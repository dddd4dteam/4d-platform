﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Core.Meta.Parametrization;
using Microsoft.VisualStudio.TextTemplating;


namespace PL.Generators.Model
{
    /// <summary>
    /// Блок райтер
    /// </summary>
    public interface IBlockWriter
    {   
        /// <summary>
        /// 
        /// </summary>
        ClassBlockEn BlockKey 
        { get;  set; }

       
        

        /// <summary>
        /// установка параметров
        /// </summary>
        /// <param name="CurrentParameters"></param>
        void SetParameters(ParametersCollectionTp CurrentParameters);
                
    }

    




}
