﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Model;

namespace PL.Generators.Data.Primitives
{
    public partial class Table
    {
        public string Owner;
        public string TableName;
        public string ClassName;
        public string DataContextPropertyName;
        public string BaseClassName;
        public bool IsView;
        public List<string> Attributes = new List<string>();
        public ChangeTracking ChangeTracking = new ChangeTracking();

        
        public Dictionary<string, Column> Columns = new Dictionary<string, Column>();
        public Dictionary<string, ForeignKey> ForeignKeys = new Dictionary<string, ForeignKey>();
        
        private ItemBOModelInfo _boModelInfo = new ItemBOModelInfo();
        
        /// <summary>
        /// Информация по модели BO о данной таблице/вьюшке 
        /// </summary>
        public ItemBOModelInfo BoModelInfo
        {
            get { return _boModelInfo; }
            set { _boModelInfo = value; }
        }

        /// <summary>
        /// Инициализация информации для данного загруженного элемента
        /// </summary>
        public void InitBOModelInfo(List<ServiceRefTp> ServiceList)
        {
            ItemBOModelInfo info = new ItemBOModelInfo();

            //Раздел1 4 роли БО  4 роли объекта  {DataTable, NavigationView, Register, VObject }
            info.DbItemCategory = (TableName.StartsWith("V_") || TableName.StartsWith("PV_")) ? DBItemCategoryEn.ViewObject : DBItemCategoryEn.TableObject;

            if (info.DbItemCategory == DBItemCategoryEn.TableObject)
            {
                //Register/DataTable                      
                info.ItemRoles.Add((TableName.Contains("_Register")) ? BORoleEn.Register : BORoleEn.DataTable);
            }
            else if (info.DbItemCategory == DBItemCategoryEn.ViewObject)
            {
                if (TableName.StartsWith("PV_"))
                    info.ItemRoles.Add(BORoleEn.PartitioningView);

                else
                {
                    // VObject / Register/ NavigationView 
                    info.ItemRoles.Add((TableName.Contains("_Vo"))
                        ? BORoleEn.VObject
                        :
                        //(TableName.Contains("_Register")) ? BORoleEn.Register : BORoleEn.NavigationView     ) ; 
                        (TableName.Contains("_Register")) ? BORoleEn.Register : BORoleEn.NavigationView);
                }
            }

            // Раздел2  - мы рассматриваем только роль      SpecifiedStructures, т.к. все остальные варианты по сути  только пользовательские структуры 
            // Значит мы должны перебрать здесь известные окончания в каждой из известных специфицированных моделей структур данных

            // модель такая что 1-нужен факт принадлежности к специфицированной модели данных и 2-Отдельный атрибут(ы) конкретной модели структуры данных  
            // SpecifiedStructures : {HierarchyDataStructure} - пока только учитываем особенности для этих элементов
            // - ещё раз, а на данном шаге - мы только можем сказать что какой-то объект является элементом структуры, 

            // Rule: Еще одно правило если у нас 1-из объектов Сервиса является элементом структуры
            // то Весь сервис по  сути выделен для этой структуры и каждый элемент сервиса по сути должен иметь роль SpecifieDataStructureComponent   

            //1 Модель - HierarchyDataStruct -  мы учитываем:  
            //  View-  with NamePart "Ancestor" 
            //  View-  with NamePart "Descendant"
            //  Table- with NamePart  "Hierarchy"

            if (info.DbItemCategory == DBItemCategoryEn.ViewObject &&
                TableName.Contains("_Ancestor") || TableName.Contains("_Descendant")
                )
            {
                info.ItemRoles.Add(BORoleEn.SpecifiedStructureComponent);
            }
            else if (info.DbItemCategory == DBItemCategoryEn.ViewObject &&
                     TableName.Contains("_Hierarchy"))
            {
                info.ItemRoles.Add(BORoleEn.SpecifiedStructureComponent);
            }

            info.EntityName = TableName.ToString().Split(new char[] {'_'})[2];
            info.DomainName = TableName.ToString().Split(new char[] {'_'})[0];

            info.ServiceIndex = int.Parse(Regex.Match(TableName, @"(?<=_S).*?(?=_)").Value);

            info.ServiceManager = ServiceList.FirstOrDefault(s => s.Index == info.ServiceIndex)?.ServiceName;
            //info.ServiceManager = info.ServiceManager.Replace("Manager","");

            //передаем собранные даные по данному БО во все наборе 
            this.BoModelInfo = info;
        }

        public override string ToString()
        {
            return !string.IsNullOrEmpty(TableName) ? TableName : this.GetType().ToString();
        }
    }

}
