﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PL.Generators.Data.Primitives
{
    /// <summary>
    /// Внешний ключ
    /// </summary>
    public partial class ForeignKey
    {
        public string KeyName;
        public string MemberName;
        public Table OtherTable;
        public List<Column> ThisColumns = new List<Column>();
        public List<Column> OtherColumns = new List<Column>();
        public List<string> Attributes = new List<string>();
        public bool CanBeNull = true;
        public ForeignKey BackReference;

        private AssociationType _associationType = AssociationType.Auto;
        
        public AssociationType AssociationType
        {
            get { return _associationType; }
            set
            {
                _associationType = value;

                if (BackReference != null)
                {
                    switch (value)
                    {
                        case AssociationType.Auto: BackReference.AssociationType = AssociationType.Auto; break;
                        case AssociationType.OneToOne: BackReference.AssociationType = AssociationType.OneToOne; break;
                        case AssociationType.OneToMany: BackReference.AssociationType = AssociationType.ManyToOne; break;
                        case AssociationType.ManyToOne: BackReference.AssociationType = AssociationType.OneToMany; break;
                    }
                }
            }
        }
    }

}
