﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.DataLayer.Primitives
{
    /// <summary>
    /// Что за объект БД - Категория элемента полученного из БД 
    /// </summary>
    public enum DBItemCategoryEn
    {
        TableObject,
        ViewObject,
        StoreProcedureObject,
        UserFunctionObject
    }

    //Информация о классификации элемента в принятой Модели BO  
    public class ItemBOModelInfo
    {
        public ItemBOModelInfo()
        {
            
        }

        List<BORoleEn> _itemRoles = new List<BORoleEn>();
        
        /// <summary>
        /// Список ролей объекта на уровнях спецификаций 
        /// 1 спецификация  Раздела Модели данных -  4 роли объекта  {DataTable, NavigationView, Register, VObject }   
        /// 2 спецификация  Раздела Структур данных - 2 роли объекта {SpecifiedStructure, Structure } : (Специфицированная структура(элемент этой структуры), Произвольная пользовательская структура )  
        /// </summary>
        public List<BORoleEn> ItemRoles
        {
            get { return _itemRoles;  }
            set { _itemRoles = value; }
        }

        String _serviceManager = null;

        //Kr0t
        public string EntityName;
        public string DomainName;

        public String ServiceManager
        {
            get { return _serviceManager; }
            set { _serviceManager = value; }
        }

        DBItemCategoryEn _dbItemCategory;

        public DBItemCategoryEn DbItemCategory
        {
            get { return _dbItemCategory; }
            set { _dbItemCategory = value; }
        }

        private int _serviceIndex;

        public int ServiceIndex
        {
            get { return _serviceIndex; }
            set { _serviceIndex = value; }
        }
        

    }
}
