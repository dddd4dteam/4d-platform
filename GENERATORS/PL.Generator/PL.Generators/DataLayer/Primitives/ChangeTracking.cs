﻿namespace PL.Generators.DataLayer.Primitives
{
    public class ChangeTracking
    {
        public bool Enabled { get; set; }
        public bool TrackColumnsUpdated { get; set; }
    }
}