﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PL.Generators.Data.Primitives
{

    public enum AssociationType
    {
        Auto,
        OneToOne,
        OneToMany,
        ManyToOne,
    }


}
