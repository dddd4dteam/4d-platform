﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model;
using PL.Generators.Data;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Data.Primitives;

namespace PL.Generators.Data
{
    public abstract partial class DataLayerGenerator : TextTransformation, IGeneratorTemplate
    {     

        public class Txt
        {
            public const string NL = "\r\n";
            public const string NL2 = "\r\n\r\n";
            public const string NL3 = "\r\n\r\n\r\n";
            public const string NL4 = "\r\n\r\n\r\n\r\n";
            public const string Tab = "\t";  // tab

            public const string D = ".";   //   dot
            public const string D2 = ". "; //   dot

            public const string Zpt = ",";   //  zpt
            public const string Zpt2 = ", "; //  zpt

            public const string DZpt = ";";   //  dot wit zpt
            public const string DZpt2 = "; "; //  dot wit zpt

            public const string DD = ":";     //  double dot
            public const string DD2 = ": ";   //  double dot

            public const string SqOp = "[ ";  //  square open
            public const string SqCl = "] ";  //  square close

            public const string RndOp = "( "; // round open
            public const string RndCl = ") "; // round close

            public const string SwOp = "{ ";  // swirl open
            public const string SwCl = "} ";  // swirl close


            public const string Str = "\"";

            /// <summary>
            /// Inside String Text :   "[insideStringText]"
            /// </summary>
            /// <param name="insideStringText"></param>
            /// <returns></returns>
            public static string InStr(string insideStringText)
            {
                return Str + insideStringText + Str;
            }
        }


        #region ------------------------- METHODS --------------------------------------


        #region -------------------------------------- RENDER ITEMS-----------------------------------


        //        #region ------------------NOTIFIABLE PROPERTY: $ItemName$------------------------------
        //        ItemName - $ItemName$  -  Ex: Client 
        //        Contract - $Contract$  -  Ex: V_S8_Client_Vo

        //        $Contract$ _$ItemName$ = new $Contract$();
        //        /// <summary>
        //        /// $Description$
        //        /// </summary>
        //        public $Contract$ $ItemName$
        //        {
        //            get{return _$ItemName$;}
        //            set
        //            {   
        //                  if (_$ItemName$ != value)
        //                  {
        //                    _$ItemName$ = value; //Field changing
        //                    OnPropertyChanged(() => $ItemName$);
        //                 }		      
        //            
        //            }
        //        }
        //       #endregion ------------------NOTIFIABLE PROPERTY: $ItemName$------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <param name="generator"></param>
        /// <param name="c"></param>
        /// <param name="MaxLens"></param>
        /// <param name="Attribs"></param>
        public static  void RenderClientColumn(DataLayerGenerator generator, Column c, Int32[] MaxLens, string[] Attribs )
        {
            String ItemName = c.MemberName;
            String Contract = c.Type; // ColumnType;

            generator.WriteLine(@"#region ------------------NOTIFIABLE PROPERTY: {0} ------------------------------", ItemName);

            // $Contract$ _$ItemName$ = new $Contract$();
            // Само определение свойства/поля не 
            generator.Write("public {0}{1} _{2};", c.Type, generator.LenDiff(generator.MaxColumnTypeLen, Contract), ItemName);
            // какая-то вставка по полю            
            // if (c.ColumnType != null)
            // { generator.Write(generator.LenDiff(generator.MaxColumnMemberLen, c.MemberName)); }
            generator.WriteLine("");
            generator.WriteColumnComment("Description ", c);
            //generator.WriteSummaryComment("Description");

            //Просчет аттрибутов
            if (MaxLens.Sum() > 0)
            {
                //Запись аттрибутов поля коли есть такие
                if (Attribs.Any(_ => _ != null))
                {
                    generator.Write("[");

                    for (var i = 0; i < Attribs.Length; i++)
                    {
                        if ( Attribs[i] != null)
                        {
                            generator.Write( Attribs[i]);
                            generator.WriteSpace(MaxLens[i] - Attribs[i].Length);

                            if (Attribs.Skip(i + 1).Any(_ => _ != null))
                                generator.Write( Txt.Zpt2);
                            else if (MaxLens.Skip(i + 1).Any(_ => _ > 0))
                                generator.WriteSpace(2);
                        }
                        else if (MaxLens[i] > 0)
                        {
                            generator.WriteSpace(MaxLens[i]);

                            if (MaxLens.Skip(i + 1).Any(_ => _ > 0))
                                generator.WriteSpace(2);
                        }
                    }

                    generator.WriteLine("] ");

                }
                else
                {
                    generator.WriteSpace(MaxLens.Sum() + (MaxLens.Where(_ => _ > 0).Count() - 1) * 2 + 3);
                }
                //Запись аттрибутов поля - END
            }

            // Запись свойства
            // Само определение свойства/поля
            generator.WriteLine("public {0}{1} {2}", Contract, generator.LenDiff(generator.MaxColumnTypeLen, c.Type), ItemName);                        

            // get{return _$ItemName$;}
            //set
            //{      
            //_$ItemName$ = value; //Field changing
            //OnPropertyChangedClt(() => $ItemName$);
            //}

            generator.WriteLine(Txt.SwOp); // property begin
            generator.WriteLine("{0}  get  {1}  return _{2};  {3}", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), Txt.SwOp, ItemName, Txt.SwCl );
            generator.WriteLine("{0}  set ", generator.LenDiff(generator.MaxColumnMemberLen, ItemName));
            generator.WriteLine("{0} {1}", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), "{"); //set begin

            generator.WriteLine("{0} if ( _{1} != value)", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), ItemName);            
            generator.WriteLine("{0} {1}", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), "{"); // if begin
            generator.WriteLine("{0}  _{1} = value; ", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), ItemName);
            generator.WriteLine("{0}  OnPropertyChangedClt(() => {1} ); ", generator.LenDiff(generator.MaxColumnMemberLen, c.MemberName), ItemName);
            generator.WriteLine("{0} {1}", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), "}"); // if end

            generator.WriteLine("{0} {1}", generator.LenDiff(generator.MaxColumnMemberLen, ItemName), "}"); //set end
            generator.WriteLine(Txt.SwCl); // property end           
             
            generator.WriteLine(@"#endregion ------------------NOTIFIABLE PROPERTY: {0} ------------------------------", ItemName);
            generator.WriteLine("");
        }        
        public void RenderClientColumn(Column c, Int32[] MaxLens, string[] Attribs)
        {   RenderClientColumn(this,c,MaxLens, Attribs);
        }


        ///Вывод Столбца Таблицы - Свойства Класса
        static Action<DataLayerGenerator, Column, int[], string[]> RenderColumn = (tt, c, maxLens, attrs) =>
        {
            // Ex: [DataMember] public string   ID_Construction     { get; set; } // char(7)
            if (maxLens.Sum() > 0)
            {
                //Запись аттрибутов поля коли есть такие
                if (attrs.Any(_ => _ != null))
                {
                    tt.Write(Txt.SqOp);

                    for (var i = 0; i < attrs.Length; i++)
                    {
                        if (attrs[i] != null)
                        {
                            tt.Write(attrs[i]);
                            tt.WriteSpace(maxLens[i] - attrs[i].Length);

                            if (attrs.Skip(i + 1).Any(_ => _ != null))
                                tt.Write( Txt.Zpt2);
                            else if (maxLens.Skip(i + 1).Any(_ => _ > 0))
                                tt.WriteSpace(2);
                        }
                        else if (maxLens[i] > 0)
                        {
                            tt.WriteSpace(maxLens[i]);

                            if (maxLens.Skip(i + 1).Any(_ => _ > 0))
                                tt.WriteSpace(2);
                        }
                    }

                    tt.Write(Txt.SqCl);

                }
                else
                {
                    tt.WriteSpace(maxLens.Sum() + (maxLens.Where(_ => _ > 0).Count() - 1) * 2 + 3);
                }
                //Запись аттрибутов поля - END
            }

            // Само определение свойства/поля не 
            tt.Write("public {0}{1} {2}", c.Type, tt.LenDiff(tt.MaxColumnTypeLen, c.Type), c.MemberName);

            // окончание поля /или дописание геттеров сеттеров свойства
            if (tt.RenderField)
            {
                tt.Write(Txt.DZpt);
                if (c.ColumnType != null)
                    tt.Write(tt.LenDiff(tt.MaxColumnMemberLen, c.MemberName));
            }
            else // Now if RenderField option == false
                tt.Write("{0} {{ get; set; }}", tt.LenDiff(tt.MaxColumnMemberLen, c.MemberName));

            // комментарий по типу поля БД  в конце свойства
            if (c.ColumnType != null)
            {
                tt.Write(" // {0}", c.ColumnType);

                if (c.Length != 0)
                    tt.Write("({0})", c.Length);

                if (c.Precision != 0)
                {
                    if (c.Scale == 0)
                        tt.Write("({0})", c.Precision);
                    else
                        tt.Write("({0},{1})", c.Precision, c.Scale);
                }
            }

            tt.WriteLine("");
        };


        ///Вывод Атрибутов Внешнего ключа Таблицы
        static Action<DataLayerGenerator, ForeignKey> RenderForeignKey = (tt, key) =>
        {
            WriteComment(tt, " " + key.KeyName);


            if (tt.TStg.CompareBool(DataLayerSettingsEn.UseDataContracts, true))  //Использование контрактов данных
            {
                WriteAttributeLine(tt, "DataMember");
                //tt.WriteAttributeLine(); //tt
            }

            tt.WriteLine("[Association(ThisKey=\"{0}\", OtherKey=\"{1}\", CanBeNull={2})]" ,
               string.Join(", ", (from c in key.ThisColumns select c.MemberName).ToArray()) ,
               string.Join(", ", (from c in key.OtherColumns select c.MemberName).ToArray()) ,
               key.CanBeNull ? "true" : "false");

            if (key.Attributes.Count > 0)
            {
                WriteAttributeLine(tt, string.Join(", ", key.Attributes.Distinct().ToArray()));
                //WriteAttributeLine(tt);
            }

            tt.Write("public ");

            if (key.AssociationType == AssociationType.OneToMany)
                tt.Write(tt.TStg.GetString(DataLayerSettingsEn.OneToManyAssociationType),
                    key.OtherTable.ClassName);
            else
                tt.Write(key.OtherTable.ClassName);

            tt.Write(" ");
            tt.Write(key.MemberName);

            if (tt.RenderField)
                tt.WriteLine(";");
            else
                tt.WriteLine(" { get; set; }");
        };

        
        
        ///Вывод таблицы
        static Action<DataLayerGenerator, Table, List<Table>> RenderTable = (tt, t, tlist) =>
        {
            if (tt.TablesForServerOnly.Contains(t.ClassName))
                tt.WriteLine("#if SERVER");

            if (t.IsView)
            {
                WriteComment(tt, " View");
            }

            //Описание аттирбутов таблицы
            RenderTableAttributes(tt, t);

            //Если не выставлена опция базового класса то обнулить базовый класс и не выводить его
            if (tt.TStg.CompareBool(DataLayerSettingsEn.UseBaseEntityClass, false))
            {
                t.BaseClassName = "";
                tt.TStg.SetString(DataLayerSettingsEn.BaseEntityClass, "");
            }
            WriteBeginClass(tt, t.ClassName, t.BaseClassName); //, t.BaseClassName

            //защищеный конструктор для Сериализации c нотации DataObjectBase Catel
            if (tt.TStg.GetBool(DataLayerSettingsEn.UseBaseEntityClass) &&
                tt.TStg.GetString(DataLayerSettingsEn.BaseEntityClass).Contains("DataObjectBase"))
            {
                tt.WriteLine("/// <summary>");
                tt.WriteLine("/// Public empty constructor.");
                tt.WriteLine(" /// </summary>");
                tt.WriteLine("public " + t.ClassName + "() { }");
                tt.WriteLine("");
                tt.WriteLine("");
                tt.WriteLine("#if !SILVERLIGHT");
                tt.WriteLine("/// <summary>");
                tt.WriteLine("/// Protected constructor for binary serialization, required to clone the object.");
                tt.WriteLine("/// </summary>");
                tt.WriteLine("protected " + t.ClassName + "(SerializationInfo info, StreamingContext context)");
                tt.WriteLine(": base(info, context) { }");
                tt.WriteLine("#endif");
                tt.WriteLine("");
            }
            else 
            {
                tt.PushIndent(Txt.Tab);

                RenderConstructor(tt, t, tlist);
            }
             
            // Render BO Mapping  properties:  Mapping and Contract 
            RenderMapping(tt, t, tlist);

            if (t.Columns.Count > 0)
            {
                //Kr0t
                if (t.IsView && // for Views
                    t.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject))
                {
                    Table tCon = (from tb in tlist
                                  where tb.BoModelInfo.ItemRoles[0] == (BORoleEn.DataTable) &&
                                      tb.BoModelInfo.ServiceIndex == t.BoModelInfo.ServiceIndex &&
                                      tb.BoModelInfo.EntityName == t.BoModelInfo.EntityName
                                  select tb).First();
                    foreach (var col in t.Columns.Values)
                    {
                        // identity Property PK
                        if ((from pt in tCon.Columns.Values
                             where pt.IsPrimaryKey &&
                                   pt.IsIdentity  &&
                                 pt.MemberName == col.MemberName
                             select pt.MemberName)
                             .FirstOrDefault() == col.MemberName)
                        {
                            col.Attributes.Add("BOPropertyRoles(PropertyRoleEn.PKeyField | PropertyRoleEn.Identity)");
                        }
                        //non identity-indexed property PK
                        else if ((from pt in tCon.Columns.Values
                             where pt.IsPrimaryKey && 
                                 pt.MemberName == col.MemberName
                             select pt.MemberName)
                             .FirstOrDefault() == col.MemberName)
                        {
                            col.Attributes.Add("BOPropertyRoles(PropertyRoleEn.PKeyField,"+ col.PKIndex.ToString() + ")");
                        }

                        // OLD Identity PK markup
                        //if ((from pt in tCon.Columns.Values
                        //     where pt.IsIdentity &&
                        //        pt.MemberName == col.MemberName
                        //     select pt.MemberName).FirstOrDefault() ==
                        //        col.MemberName)
                        //{
                        //    col.Attributes.Add("BOIdentityProperty");
                        //}
                    }

                    // VOFKeyField in Views
                    if (tt.TStg.GetList(DataLayerSettingsEn.ServiceTargetEntityName) != null)
                        foreach (var entityName in
                            tt.TStg.GetList(DataLayerSettingsEn.ServiceTargetEntityName)
                            .Where(entityName => t.BoModelInfo.EntityName != entityName 
                                   && entityName != "")  )
                        {
                            tCon = (from tb in tlist
                                    where tb.BoModelInfo.ItemRoles[0] == (BORoleEn.DataTable) &&
                                          tb.BoModelInfo.ServiceIndex == t.BoModelInfo.ServiceIndex &&
                                          tb.BoModelInfo.EntityName == entityName
                                    // t.BoModelInfo.EntityName
                                    select tb).First();

                            foreach (var col in t.Columns.Values)
                            {
                                var fk = (tCon.Columns.Values.Where(
                                    pt => pt.MemberName == col.MemberName &&
                                          pt.IsPrimaryKey == true)).FirstOrDefault();
                                if (fk != null)
                                {
                                    var tView = (from tb in tlist
                                                 where tb.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) &&
                                                       tb.BoModelInfo.ServiceIndex == t.BoModelInfo.ServiceIndex &&
                                                       tb.BoModelInfo.EntityName == entityName
                                                 select tb).FirstOrDefault();
                                    if (tView != null)
                                        col.Attributes.Add( $"BOPropertyRoles(PropertyRoleEn.VOFKeyField, {Txt.InStr(tView.TableName)} )" );
                                }
                            }
                        }
                } // END for Views

                //Calculate lengths on writing in one/another line - attributes
                tt.MaxColumnTypeLen = t.Columns.Values.Max(_ => _.Type.Length);
                tt.MaxColumnMemberLen = t.Columns.Values.Max(_ => _.MemberName.Length);

                var maxLens = new int[]
		        {
			        t.Columns.Values.Max(_ => _.MemberName == _.ColumnName                      ? 0 : "MapField('')".Length + _.ColumnName.Length),
			        t.Columns.Values.Max(_ => _.IsNullable && 
                        tt.TStg.GetBool(DataLayerSettingsEn.UsePropNullableAttribute)           ? "Nullable".Length : _.IsIdentity && 
                                                                                                    tt.TStg.GetBool(DataLayerSettingsEn.UsePropIdentityAttribute) ? "Identity".Length : 0),
			        t.Columns.Values.Max(_ => _.IsIdentity && _.IsNullable && 
                        tt.TStg.GetBool(DataLayerSettingsEn.UsePropIdentityAttribute)&&
                        tt.TStg.GetBool(DataLayerSettingsEn.UsePropNullableAttribute)           ? "Identity".Length : 0),

        #if DEBUG
			        t.Columns.Values.Max(_ => _.IsPrimaryKey &&
                        tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute)         ? string.Format("PrimaryKey({0})", _.PKIndex).Length :0),
                    t.Columns.Values.Max(_ => _.IsPrimaryKey &&
                        tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute)         ? string.Format("BOPropertyRole(PropertyRoleEn.PKeyField)").Length : 0 ),
        #else
                    t.Columns.Values.Max(_ => _.IsPrimaryKey                                    ? tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute)? 
                                                                                                        string.Format("PrimaryKey({0})", _.PKIndex).Length :
                                                                                                        tt.TStg.GetBool(DataLayerSettingsEn.UseBOPrimaryKeyRoleAttribute)?
                                                                                                            string.Format("BOPropertyRole(PropertyRoleEn.PKeyField)").Length : 0 
                                                                                                    :0 ),
        #endif
			        t.Columns.Values.Max(_ => _.Attributes.Count == 0                           ? 0 : string.Join(", ", _.Attributes.Distinct().ToArray()).Length),
                    t.Columns.Values.Max(_ => tt.TStg.GetBool(DataLayerSettingsEn.
                        UseDataContracts)                                                       ? "DataMember".Length:0),
                    t.Columns.Values.Max(_ => _.ColumnType == "text" || 
                                             ((_.ColumnType == "varbinary" || 
                                             _.ColumnType == "varchar" ||
                                             _.ColumnType == "nvarchar")  && _.Length == -1)     ? "LargeSQLDataType".Length:0),
                    "SQLColumn".Length
                };

                // Collecting attributes strings for table   
                foreach (var c in from c in t.Columns.Values 
                                  orderby c.ID 
                                  select c)
                {
                    var attrs = new string[]
			        {
				        c.MemberName == c.ColumnName
                            ? null : string.Format("MapField(\"{0}\")", c.ColumnName)
                        ,
				        c.IsNullable && 
                            tt.TStg.GetBool(DataLayerSettingsEn.UsePropNullableAttribute)       
                            ? "Nullable" : c.IsIdentity 
                                          && tt.TStg.GetBool(DataLayerSettingsEn.UsePropIdentityAttribute) 
                                          ? "Identity" : null
                        ,
				        c.IsIdentity && c.IsNullable && 
                            tt.TStg.GetBool(DataLayerSettingsEn.UsePropIdentityAttribute) 
                            && tt.TStg.GetBool(DataLayerSettingsEn.UsePropNullableAttribute)    ? "Identity" : null
                        ,       
			            c.IsPrimaryKey  
                            && tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute)   
                            ? string.Format("", c.PKIndex) :null //PrimaryKey({0})
                        ,
                         RenderPropPKeyAttrib(tt, c) 
                        ,
				        c.Attributes.Count == 0 ? null 
                                        : string.Join(Txt.Zpt2 , c.Attributes.Distinct().ToArray())
                        ,
                        tt.TStg.GetBool(DataLayerSettingsEn.UseDataContracts)  ? "DataMember":null
                        ,
                        c.ColumnType == "text" || ((c.ColumnType == "varbinary" 
                                              ||  c.ColumnType == "varchar" 
                                              ||  c.ColumnType == "nvarchar")  
                                              && c.Length == -1) ? "LargeSQLDataType":null
                        ,
                        "SQLColumn"
                    };

                    //Genarating Column
                    tt.RenderClientColumn(c, maxLens, attrs);
                    //RenderColumn(tt, c, maxLens, attrs); 

                }
            }

            if (t.ForeignKeys.Count > 0 && tt.TStg.GetBool(DataLayerSettingsEn.UseForeignKeyProperties))
            {
                foreach (var key in t.ForeignKeys.Values.Where(k => tt.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null))
                {
                    tt.WriteLine("");
                    if (!tt.TStg.GetBool(DataLayerSettingsEn.UseFiltrationForAssociations)
                            || tlist.Contains(key.OtherTable))
                        RenderForeignKey(tt, key);
                }
            }

            WriteIClonable(tt, t, tlist);

            tt.PopIndent();
            WriteEndClass(tt);

            if (tt.TablesForServerOnly.Contains(t.ClassName))
                tt.WriteLine("#endif");
        };

        private static string RenderPropPKeyAttrib(DataLayerGenerator tt, Column c)
        {
            if (c.IsPrimaryKey && c.IsIdentity == false &&
                    tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute))
            {
                return string.Format($"BOPropertyRoles(PropertyRoleEn.PKeyField,{c.PKIndex})");
            }
            else if (c.IsPrimaryKey && c.IsIdentity &&
                    tt.TStg.GetBool(DataLayerSettingsEn.UsePropPrimaryKeyAttribute))
            {
                return string.Format($"BOPropertyRoles(PropertyRoleEn.PKeyField |PropertyRoleEn.Identity )");
            }
            else
            return null;
        }

        private static void RenderConstructor(DataLayerGenerator tt, Table table, List<Table> tlist)
        {
            if ((table.BoModelInfo.ItemRoles.Any(r => r == BORoleEn.DataTable || r == BORoleEn.VObject)))
            {
                List<Column> Columns = new List<Column>();
                if ((table.BoModelInfo.ItemRoles.Any(r => r == BORoleEn.DataTable)) && 
                    (table.Columns.Values.Where(c => c.IsPrimaryKey).Any(c => c.SqlDbType == System.Data.SqlDbType.UniqueIdentifier)))
                    Columns = table.Columns.Values.Where(c => c.IsPrimaryKey && c.SqlDbType == System.Data.SqlDbType.UniqueIdentifier).ToList();

                else if (table.BoModelInfo.ItemRoles.Any(r => r == BORoleEn.VObject))
                {
                    Table tCon = (from tb in tlist
                                  where tb.BoModelInfo.ItemRoles[0] == (BORoleEn.DataTable) &&
                                      tb.BoModelInfo.ServiceIndex == table.BoModelInfo.ServiceIndex &&
                                      tb.BoModelInfo.EntityName == table.BoModelInfo.EntityName
                                  select tb).First();
                    foreach (var col in table.Columns.Values)
                    {
                        if ((from pt in tCon.Columns.Values
                             where pt.IsPrimaryKey &&
                                 pt.MemberName == col.MemberName
                             select pt.MemberName).FirstOrDefault() ==
                            col.MemberName && col.SqlDbType == System.Data.SqlDbType.UniqueIdentifier)
                        {
                            Columns.Add(col);
                        }
                    }
                }

                if (Columns.Any())
                {
                    tt.WriteLine("");
                    tt.WriteLine("public " + table.ClassName + "()");
                    tt.WriteLine("{");
                    tt.PushIndent("\t");

                    foreach (Column column in Columns)
                    {
                        tt.WriteLine(column.ColumnName + " = Guid.NewGuid();");
                    }

                    tt.PopIndent();
                    tt.WriteLine("}");
                    tt.WriteLine("");
                }
            }
        }

        private static void RenderMapping(DataLayerGenerator tt, Table t, List<Table> tlist)
        {
            /*
         #region -----------LAZY MAPPING ------------
       
        static readonly string defaultProvider = ProviderKey.Default;
        static readonly Type boContarct =typeof(BOSon);

        static readonly TypeDAMap mapping =
            TypeDAMap.GetInit(typeof(BOSon), defaultProvider, true);

        /// <summary>
        /// BO contract Mapping Info
        /// </summary>
        public override TypeDAMap Mapping
        { get { return mapping; } }

        /// <summary>
        /// BO contract - Type
        /// </summary>
        public override Type Contract
        { get { return boContarct; } }
        
        #endregion-----------LAZY MAPPING ------------

             */

            
            tt.WriteLine("#region ------------------LAZY MAPPING ------------");
            tt.WriteLine(Txt.NL);
            tt.WriteLine("static readonly string defaultProvider = ProviderKey.Default;");
            tt.WriteLine($"static readonly Type boContarct =typeof({t.ClassName});");
            tt.WriteLine(Txt.NL);
            tt.WriteLine("static readonly TypeDAMap mapping = ");
            tt.WriteLine($"     TypeDAMap.GetInit(typeof({t.ClassName}), defaultProvider, true);");
            tt.WriteLine(Txt.NL);
            tt.WriteSummaryComment("BO contract Mapping Info");
            tt.WriteLine("public override TypeDAMap Mapping");
            tt.WriteLine("{ get { return mapping; } }");
            tt.WriteLine(Txt.NL);
            tt.WriteSummaryComment("BO contract  - Type");
            tt.WriteLine("public override Type Contract");
            tt.WriteLine("{ get { return boContarct; } }");
            tt.WriteLine(Txt.NL);
            tt.WriteLine("#endregion -----------------LAZY MAPPING ------------");
            tt.WriteLine(Txt.NL);
        }



        ///Вывод Атрибутов Таблицы
        static Action<DataLayerGenerator, Table> RenderTableAttributes = (tt, t) =>
        {
            //Контракт данных
            if (tt.TStg.GetBool(DataLayerSettingsEn.UseDataContracts))
            {
                WriteAttribute(tt, "DataContract");
                //WriteAttributeLine(tt);
            }

            if (t.Attributes.Count > 0)
            {
                WriteAttribute(tt, string.Join(", ", t.Attributes.Distinct().ToArray()));
                //WriteAttributeLine(tt);
            }

            List<ForeignKey> DependOnOtherTablesFK = t.ForeignKeys.Where(fk => (fk.Value.AssociationType == AssociationType.ManyToOne ||
                                                                     (fk.Value.AssociationType == AssociationType.OneToOne && fk.Value.CanBeNull == false)) && 
                                                                     fk.Value.OtherTable.TableName != t.TableName)
                                                        .GroupBy(fk => fk.Value.OtherTable.TableName)
                                                        .Select(fk => fk.First().Value)
                                                        .OrderBy(fk => fk.OtherTable.TableName).ToList();
            if (DependOnOtherTablesFK.Any())
            {
                string ForeignKeyTablesAttr = "ForeignKeyTables(" +
                    DependOnOtherTablesFK.Select((fk, i) => "typeof(" + fk.OtherTable.TableName + ")" + ((DependOnOtherTablesFK.Count - 1 > i) ? ", " : ""))
                                 .Aggregate((aggr, typeStr) => aggr + typeStr) + ")";
                WriteAttribute(tt, ForeignKeyTablesAttr);
            }

            if (t.BoModelInfo.DbItemCategory == DBItemCategoryEn.TableObject && t.Columns.All(c => c.Value.IsPrimaryKey))
                WriteAttribute(tt, "NotUpdatableTable");

            string tbl;
            if (tt.TStg.GetBool(DataLayerSettingsEn.UseTableNameAttribute))
            {
                tbl = "TableName(";

                if (!string.IsNullOrEmpty(tt.TStg.GetString(DataLayerSettingsEn.DatabaseName)))
                    tbl += string.Format("database=\"{0}\", ", tt.TStg.GetString(DataLayerSettingsEn.DatabaseName));

                if (!string.IsNullOrEmpty(t.Owner))
                    tbl += string.Format("owner=\"{0}\", ", t.Owner);

                tbl += $"nameof({t.TableName}) )";//  string.Format( , );

                WriteAttribute(tt, tbl);
                //WriteAttributeLine(tt);
            }

            var serviceModelName = t.BoModelInfo.ServiceManager + "Model";
            tbl = "ServiceModel(SMKey." + t.BoModelInfo.ServiceManager + ", typeof("+ serviceModelName + ") ), BORoles(BORoleEn.";
            if (t.BoModelInfo.ItemRoles[0] == (BORoleEn.DataTable))
                tbl += "DataTable)";
            //else if (t.BoModelInfo.ItemRoles[0] == (BORoleEn.NavigationView))
            else if (t.BoModelInfo.ItemRoles[0] == (BORoleEn.NavigationView))
                tbl += "NavigationView)";
            else if (t.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject))
                tbl += "VObject)";
            else if (t.BoModelInfo.ItemRoles[0] == (BORoleEn.Register))
                tbl += "Register)";
            else if (t.BoModelInfo.ItemRoles[0] == BORoleEn.PartitioningView)
                tbl += "PartitioningView)";
            WriteAttribute(tt, tbl);
            //WriteAttributeLine(tt);

            if (tt.TStg.GetBool(DataLayerSettingsEn.UseVObjectMappingAttribute) &&
                (t.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) || t.BoModelInfo.ItemRoles[0] == BORoleEn.PartitioningView))
            {
                tbl = "VObjectMap(typeof(" + tt.TStg.GetString(DataLayerSettingsEn.DomainPrefix) + "_S" +
                    t.BoModelInfo.ServiceIndex + "_" + t.BoModelInfo.EntityName + ") )"; //, typeof(" + t.TableName + ")
                WriteAttribute(tt, tbl);
                //WriteAttributeLine(tt);
            }
        };




        /// <summary>
        /// Вывод   Usings
        /// </summary>
        static Action<DataLayerGenerator> RenderUsing = tt =>
        {
            var q =
                from ns in tt.TStg.GetList(DataLayerSettingsEn.Usings).Distinct()
                group ns by ns.Split('.')[0];

            var groups =
                (from ns in q where ns.Key == "System" select ns).Concat
                (from ns in q where ns.Key != "System" orderby ns.Key select ns);

            foreach (var gr in groups)
            {
                if (gr.Key == "BLToolkit")
                    tt.WriteLine("#if SERVER");

                foreach (var ns in from s in gr orderby s select s)
                    WriteUsing(tt, ns);

                if (gr.Key == "BLToolkit")
                {
                    tt.WriteLine("#else");
                    WriteUsing(tt, "Core.ORM.SL");
                    tt.WriteLine("#endif");
                }

                tt.WriteLine("");
            }
        };

        //Рендер класса контекста данных
        static Action<DataLayerGenerator, List<Table>> RenderDataContextClass = (tt, tbls) =>
        {

            //BEGIN генерация класса контекста модели со всеми таблицами 
            WriteBeginClass(tt, tt.TStg.GetString(DataLayerSettingsEn.DataContextName),
                tt.TStg.GetString(DataLayerSettingsEn.BaseDataContextClass)); //, BaseDataContextClass


            //from t in Tables.Values orderby t.TableName select t).ToList();
            var maxlen = tbls.Max(_ => _.ClassName.Length);
            var maxplen = tbls.Max(_ => (_.DataContextPropertyName ?? _.ClassName).Length);

            tt.PushIndent("\t");

            tt.BeforeWriteTableProperty(tt);

            foreach (var t in tbls)
                WriteTableProperty(tt, t.ClassName, t.DataContextPropertyName ?? t.ClassName, maxlen, maxplen);

            tt.AfterWriteTableProperty(tt);

            tt.PopIndent();

            WriteEndClass(tt);
            //END генерация класса контекста модели со всеми таблицами 

        };


        #endregion -------------------------------------- RENDER ITEMS-----------------------------------
          
       
        #endregion ------------------------- METHODS --------------------------------------
          
    }
}