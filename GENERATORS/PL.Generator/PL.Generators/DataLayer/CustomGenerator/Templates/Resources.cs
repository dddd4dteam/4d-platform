﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model;
using PL.Generators.Data;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Data.Primitives;
using System.IO;
using System.Resources;
using System.Windows.Forms;

namespace PL.Generators.Data
{
    public abstract partial class DataLayerGenerator : TextTransformation, IGeneratorTemplate
    {
        static ResXResourceWriter rw;

        public void InitRes(string ServiceName, string path = "")
        {
            if (path == "")
                path = Path.GetDirectoryName(Host.TemplateFile);
            rw = new ResXResourceWriter(path + "\\" + ServiceName + ".resx");
        }

        public void InitRes(int ServiceNum,  string path = "")
        {
            InitRes(TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).
                First(s => s.Index == ServiceNum).ServiceName,  path);
        }

        public void AddBOtoRes(Table tab,DataLayerGenerator tt)
        {
            if (tab.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) ||
                tab.BoModelInfo.ItemRoles[0] == (BORoleEn.Register) ||
                (tt.TStg.GetBool(DataLayerSettingsEn.GenerateResForAllViews) &&
                 tab.IsView))
            {
                rw.AddResource("BoNameLoc\\" + tab.TableName, tab.TableName);
                rw.AddResource("BoDescLoc\\" + tab.TableName, tab.TableName);
                foreach (Column pop in tab.Columns.Values)
                {
                    rw.AddResource("BoPropElmNameLoc\\" + tab.TableName + "|" + pop.MemberName,
                                   tab.TableName + " - " + pop.MemberName);
                    rw.AddResource("BoPropElmDescLoc\\" + tab.TableName + "|" + pop.MemberName,
                                   tab.TableName + " - " + pop.MemberName);
                }
            }

            //foreach (ForeignKey pop in tab.ForeignKeys.Values)
            //{
            //    rw.AddResource("BoPropElmNameLoc\\" + tab.TableName + "|" + pop.MemberName, "");
            //    rw.AddResource("BoPropElmDescLoc\\" + tab.TableName + "|" + pop.MemberName, "");
            //}
        }

        public void CloseRes()
        {
            rw.Close();
        }
    }
}