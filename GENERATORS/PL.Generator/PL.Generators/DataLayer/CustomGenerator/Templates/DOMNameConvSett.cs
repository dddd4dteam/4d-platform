﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model;
using PL.Generators.Data;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Data.Primitives;

namespace PL.Generators.Data
{
    public abstract partial class DataLayerGenerator : TextTransformation, IGeneratorTemplate
    {

        // ------------- DOMAIN MODEL NAME CONVENTION SETTINGS ------------------


        /// <summary>
        /// 
        /// </summary>
        public void CheckfilterSettings()
        {
            //Корректируем настройки в зависимости от задачи
            switch (TStg.GetItem<DomainServRoleFiltration>(DataLayerSettingsEn.FiltrationType))
            {
                case DomainServRoleFiltration.AllByDomainService_2:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.DatasByDomainService_6:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes,
                        //new List<BORoleEn>() { BORoleEn.NavigationView });
                        new List<BORoleEn>() { BORoleEn.NavigationView });
                    break;
                case DomainServRoleFiltration.RegistersByDomainService_3:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes,
                        new List<BORoleEn>() { BORoleEn.Register });
                    break;
                case DomainServRoleFiltration.VoByDomainService_4:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes,
                        new List<BORoleEn>() { BORoleEn.VObject });
                    break;
                case DomainServRoleFiltration.VoRegistersByDomainService_5:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes,
                        new List<BORoleEn>() { BORoleEn.Register, BORoleEn.VObject });
                    break;
                case DomainServRoleFiltration.TableDatasByDomainService_8:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, false);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes,
                        new List<BORoleEn>() { BORoleEn.DataTable });
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.ViewDatasbyDomainService_7:
                    TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Clear();
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Clear();
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes,
                        //new List<BORoleEn>() { BORoleEn.NavigationView });
                        new List<BORoleEn>() { BORoleEn.NavigationView });
                    break;
                case DomainServRoleFiltration.TableNotDatasByDomainService_9:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    TStg.SetBool(DataLayerSettingsEn.DomainViewFiltration, true);
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    TStg.SetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes,
                        new List<BORoleEn>() { BORoleEn.Register });
                    TStg.GetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.Custom:
                    if (TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    if (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    break;
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public bool IsTableFiltered(Table t)
        {
            return
                ((TStg.GetList(DataLayerSettingsEn.DomainTableEntitiesPrefixes).Contains(t.TableName.Split(new char[] { '_' })[0]) &&
                  (TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Count == 0 ||
                   TStg.GetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes).Contains(t.BoModelInfo.ItemRoles[0]))) ||
                 (TStg.GetBool(DataLayerSettingsEn.DomainViewFiltration) &&
                  t.BoModelInfo.DbItemCategory == DBItemCategoryEn.ViewObject &&
                  (TStg.GetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes).Count == 0 ||
                   TStg.GetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes).Contains(t.BoModelInfo.ItemRoles[0])))) &&
                (TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices).Any(s => s.Index == t.BoModelInfo.ServiceIndex));
        }


        public List<Table> FilterTables(List<Table> tlist)
        {
            CheckfilterSettings();

            return (from t in tlist
                    where IsTableFiltered(t)
                    orderby t.TableName
                    select t).ToList();
        }
    }
}