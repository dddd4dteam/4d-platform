﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model;
using PL.Generators.Data;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Data.Primitives;

namespace PL.Generators.Data
{
    public abstract partial class DataLayerGenerator : TextTransformation, IGeneratorTemplate
    {
        static Action<DataLayerGenerator, ServiceRefTp, List<Table>> RenderServiceModelClass = (tt, Serv, tlist) =>
        {
            /*  render Service Model  class - start
                    /// <summary>
                    /// Sx_NameModel - ServicModel class of Sx_Name service
                    /// </summary>
                    [ServiceModel( SMKey.Sx_Name, typeof(Sx_NameModel) )] 
                    public partial class Sx_NameModel :  ServiceModel
                    {

             */
            tt.WriteSummaryComment($"{Serv.ServiceName}Model - ServicModel class of {Serv.ServiceName} service" );                        
            WriteAttributeLine(tt, $"ServiceModel(  SMKey.{Serv.ServiceName} , typeof({Serv.ServiceName}Model) )");            
            tt.WriteLine($"public partial class {Serv.ServiceName}Model : ServiceModel" );
            tt.WriteLine("{");
            tt.PushIndent("\t");

            // render  ctor 
            tt.WriteLine("public " + Serv.ServiceName + "Model(){" + "}");
            tt.WriteLine("");

            // set end resources 
            tt.WriteSummaryComment($"Service Model Resource Manager  of {Serv.ServiceName} service  ");            
            tt.WriteLine("public override ResourceManager ResManager");
            tt.WriteLine("{  get; protected set; }  = ");
            tt.WriteLine(" new ResourceManager(\"CRM.DOM.DAL.{0}.{0}\", typeof({0}Model).Assembly);", Serv.ServiceName);
            tt.WriteLine("");

            /* render ServiceModelType
             /// <summary>
             ///  ServiceModelType  of  Sx_ServName  is standart/specific ServiceModel
             /// </summary>
             public abstract ServiceModelTypeEn ServiceModelType { get; }

             */
            if (tt.TStg.GetBool(DataLayerSettingsEn.ServiceModelType)) // 
            {
                tt.WriteSummaryComment($" ServiceModelType  of  {Serv.ServiceName} service is standart ServiceModel ");
                tt.WriteLine("public override ServiceModelTypeEn ServiceModelType { get; } =");
                tt.WriteLine("ServiceModelTypeEn.StandartServiceModel;");
            }
            else {
                tt.WriteSummaryComment($" ServiceModelType  of  {Serv.ServiceName} service is specific ServiceModel");
                tt.WriteLine("public override ServiceModelTypeEn ServiceModelType { get; } =");
                tt.WriteLine("ServiceModelTypeEn.SpecificServiceModel;");
            }
            tt.WriteLine("");



            /*  render Service Model Contracts Dictionary - SvcModelContracts

                /// <summary>
                /// ServiceModel BO Contracts.
                /// </summary>
                protected virtual Dictionary<string, Type> SvcModelContracts
                { get; set; } = new Dictionary<string, Type>()
                 {   
                    { "", typeof(object) } 
                 };

           */

            tt.WriteSummaryComment("ServiceModel BO Contracts.");
            tt.WriteLine(" protected override Dictionary<string, Type> SvcModelContracts");
            tt.WriteLine(" { get; set; } = new Dictionary<string, Type>()");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            //tlist = tlist.OrderBy(t => t.ClassName).ToList();

            for (int i = 0; i < tlist.Count; i++)
            {
                tt.WriteLine(Txt.SwOp + $"nameof({tlist[i].ClassName }), typeof({ tlist[i].ClassName}) " + Txt.SwCl );
                if (i != tlist.Count - 1) tt.Write(", ");
            }

            tt.PopIndent();
            tt.WriteLine("};");
            tt.WriteLine("");



            // render class
            tt.PopIndent();
            tt.WriteLine("}");// end of class
            tt.WriteLine("");

        };


        static Action<DataLayerGenerator, ServiceRefTp, List<Table>> RenderModelManager = (tt, Serv, tlist) =>
        {
            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// " + Serv.ServiceName + " ServiceManager");
            tt.WriteLine("/// </summary>");
            WriteAttributeLine(tt, "ServiceModelManager(ServiceModelManager_RegisterEn." +
                Serv.ServiceName + ", ServiceTypeEn.CustomStructureServiceType,true)");
            //WriteAttributeLine(tt);
            tt.WriteLine("public partial class {0}Model : IServiceModel", Serv.ServiceName);
            tt.WriteLine("{");
            tt.PushIndent("\t");

            tt.WriteLine("public " + Serv.ServiceName + "Model(){" + "}");
            tt.WriteLine("");

            //tt.WriteLine("/// <summary>");
            //tt.WriteLine("/// Пустой список зависимостей означает что у нас нет зависимостей от данной модели сервиса от других сервисов.Это -Базовый сервис");
            //tt.WriteLine("/// </summary>");
            //tt.WriteLine("public List<ServiceModelManager_RegisterEn> DependsOn");
            //tt.WriteLine("{");
            //tt.PushIndent("\t");
            //tt.WriteLine("get {");
            //tt.PushIndent("\t");
            //tt.WriteLine("return new List<ServiceModelManager_RegisterEn>()");
            //tt.WriteLine("{");
            //tt.PushIndent("\t");

            //List<string> depends = new List<string>();
            //foreach (Table t in tlist)
            //    foreach (var fk in t.ForeignKeys.Values)
            //        if(!depends.Contains(fk.OtherTable.BoModelInfo.ServiceManager))
            //            depends.Add(fk.OtherTable.BoModelInfo.ServiceManager);
            //if (depends.Contains(Serv.ServiceName))
            //    depends.Remove(Serv.ServiceName);
            //foreach (string dep in depends)
            //    tt.WriteLine("ServiceModelManager_RegisterEn.{0},", dep);

            //tt.PopIndent();
            //tt.WriteLine("};");
            //tt.PopIndent();
            //tt.WriteLine("}");
            //tt.PopIndent();
            //tt.WriteLine("}");
            //tt.WriteLine("");
            //tt.WriteLine("");

            //tt.WriteLine("private ResourceManager resourceManager = new ResourceManager(\"CRM.DOM.DAL.{0}.{0}\", typeof({0}Model).Assembly);", Serv.ServiceName);
            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// Service Model Resource Manager " + Serv.ServiceName);
            tt.WriteLine("/// </summary>");
            tt.WriteLine("public override ResourceManager ResManager");
            tt.WriteLine("{  get; protected set; }  = ");
            tt.WriteLine(" new ResourceManager(\"CRM.DOM.DAL.{0}.{0}\", typeof({0}Model).Assembly);", Serv.ServiceName);

            //tt.PushIndent("\t");
            //tt.WriteLine("get { return resourceManager; }");
            //tt.PopIndent();
            //tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");


            if (tt.TStg.GetBool(DataLayerSettingsEn.UseBOContracts))
            {
                /*
                    /// <summary>
                    /// ServiceModel BO Contracts.
                    /// </summary>
                    protected virtual Dictionary<string, Type> SvcModelContracts
                    { get; set; } = new Dictionary<string, Type>()
                     {   
                        { "", typeof(object) } 
                     };

                 */
                tt.WriteSummaryComment("ServiceModel BO Contracts.");
                tt.WriteLine(" protected override Dictionary<string, Type> SvcModelContracts");
                tt.WriteLine(" { get; set; } = new Dictionary<string, Type>()");
                tt.WriteLine("{");
                tt.PushIndent("\t");
                tlist = tlist.OrderBy(t => t.ClassName).ToList();

                for (int i = 0; i < tlist.Count; i++)
                {
                    tt.WriteLine("{ nameof({0}), typeof({0}) }  ", tlist[i].ClassName);
                    if (i < tlist.Count - 1)  tt.Write(", ");
                }
                
                tt.PopIndent();
                tt.WriteLine("};");
                tt.WriteLine("");


                tt.WriteLine("/// <summary>");
                tt.WriteLine("/// Получить Все Контракты Модели Сервиса(объекты в роли DataTables здесь присутствуют - т.е. КОнтракты со всеми ролями, по Даум разделам(Сущностей, Объектов Сруктур)  )");
                tt.WriteLine("/// </summary>");
                tt.WriteLine("/// <returns></returns>");
                tt.WriteLine("public List<Type> GetBOContracts()");
                tt.WriteLine("{");
                tt.PushIndent("\t");
                tt.WriteLine("try");
                tt.WriteLine("{");
                tt.PushIndent("\t");
                tt.WriteLine("return new List<Type>()");
                tt.WriteLine("{");
                tt.PushIndent("\t");

                foreach (Table t in tlist.OrderBy(t => t.ClassName))
                    tt.WriteLine("typeof({0}),", t.ClassName);

                tt.PopIndent();
                tt.WriteLine("};");

                tt.PopIndent();
                tt.WriteLine("}");
                tt.WriteLine("catch (Exception)");
                tt.WriteLine("{");
                tt.PushIndent("\t");
                tt.WriteLine("throw;");
                tt.PopIndent();
                tt.WriteLine("}");
                tt.PopIndent();
                tt.WriteLine("}");
                tt.WriteLine("");
            }

            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// </summary>");
            tt.WriteLine("/// <returns></returns>");
            tt.WriteLine("public List<Type> GetClientContracts()");
            tt.WriteLine("{");
            tt.PushIndent("\t");

            tt.WriteLine("return GetBOContracts();");

            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");



            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// Является ли данный сервис Сервисом для Предопределенной структуры. ");
            tt.WriteLine("/// Если тип Структуры  не равен  CustomStructureServiceType то да.");
            tt.WriteLine("/// Указывается в атрибуте данного сервиса ");
            tt.WriteLine("/// </summary>");
            tt.WriteLine("public bool IsSpecifiedDataStructureService");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("get");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("return (!IsServiceType(ServiceTypeEn.CustomStructureServiceType));");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");


            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// Это сервис какого типа - ПОльзовательский или под какой то структурой");
            tt.WriteLine("/// </summary>");
            tt.WriteLine("/// <param name=\"serviceStructureType\"></param>");
            tt.WriteLine("/// <returns></returns>");
            tt.WriteLine("public bool IsServiceType(ServiceTypeEn serviceStructureType)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("try");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("return (GetType().GetTypeAttribute<ServiceModelManagerAttribute>().FirstOrDefault().ServiceType == serviceStructureType);");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("catch (Exception)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("throw new Exception(\" Cannot get current Service Type  \");");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");

            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// Сервис ");
            tt.WriteLine("/// </summary>");
            tt.WriteLine("public ServiceModelManager_RegisterEn ServiceModel ");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("get { return GetType().GetTypeAttribute<ServiceModelManagerAttribute>().FirstOrDefault().ServiceModel; }");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");


            tt.WriteLine("/// <summary>");
            tt.WriteLine("/// Получить типы объектов сервиса Которые имеют Роли  boRole");
            tt.WriteLine("/// </summary>");
            tt.WriteLine("/// <param name=\"boRole\"></param>");
            tt.WriteLine("/// <returns></returns>");
            tt.WriteLine("public List<Type> GetObjectsByRole(BORoleEn boRole)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("try");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("List<Type> AllBObjectTypes = GetBOContracts();");

            tt.WriteLine("return  AllBObjectTypes.Where(tp => tp.GetTypeAttribute<BORoleAttribute>().BoConatainRole(boRole)).ToList();");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("catch (Exception exc)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("throw exc;");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");


            tt.WriteLine("public DataContextInfoTp DataContext(IPersistableBO objectInstance)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("try");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine(" DataContextInfoTp dataContext = new DataContextInfoTp()");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("TargetBO = new BOInfoTp()");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("Key = objectInstance.GetType().Name,");
            tt.WriteLine("FullName = objectInstance.GetType().FullName,");
            tt.WriteLine("Item = objectInstance");
            tt.PopIndent();
            tt.WriteLine("},");
            tt.WriteLine("AssociatedBOs = new List<BOInfoTp>()");
            tt.PopIndent();
            tt.WriteLine("};");
            tt.WriteLine("foreach (var pop in SystemDispatcher.Instance.GetObjectsFromDALByRole(Core.Meta.BO.BORoleEn.VObject).");
            tt.PushIndent("\t");
            tt.WriteLine("Where(p => p.GetPropertiesInRole(Core.Meta.BO.PropertyRoleEn.VOFKeyField).Count > 0))");
            tt.PopIndent();
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("bool flagContainsPK = true;");
            tt.WriteLine("foreach (var prop in pop.GetPropertiesInRole(Core.Meta.BO.PropertyRoleEn.VOFKeyField))");
            tt.PushIndent("\t");
            tt.WriteLine("if (!objectInstance.GetItemPrimaryKey().ContainsKey(prop.Name))");
            tt.PushIndent("\t");
            tt.WriteLine("flagContainsPK = false;");
            tt.PopIndent();
            tt.PopIndent();
            tt.WriteLine("if (flagContainsPK)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("dataContext.AssociatedBOs.Add(new BOInfoTp()");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("Key = pop.Name,");
            tt.WriteLine("FullName = pop.FullName");
            tt.PopIndent();
            tt.WriteLine("});");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("}");

            tt.WriteLine("return dataContext;");
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("catch (Exception exc)");
            tt.WriteLine("{");
            tt.PushIndent("\t");
            tt.WriteLine("throw exc;");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.PopIndent();
            tt.WriteLine("}");
            tt.WriteLine("");
            tt.WriteLine("");


            tt.PopIndent();
            tt.WriteLine("}");

        };
    }
}