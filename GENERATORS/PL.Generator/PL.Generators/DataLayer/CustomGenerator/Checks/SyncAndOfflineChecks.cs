﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using PL.Generators.Data;
using PL.Generators.Data.Primitives;
using PL.Generators.Model;

namespace PL.Generators.DataLayer.CustomGenerator.Checks
{
    public static class SyncAndOfflineChecks
    {
        public static void CheckForIdentityColumns(List<Table> tables)
        {
            List<Table> TablesWithIdentityColumns = tables.Where(t => t.Columns.Any(c => c.Value.IsIdentity))
                .OrderBy(t => t.TableName).ToList();

            if (TablesWithIdentityColumns.Count == 0)
                return;

            string message = "Обнаружены таблицы с колонками Identity:\r\n\r\n" +
                             TablesWithIdentityColumns.Aggregate(String.Empty, (s, t) =>
                                 s + $"{t.TableName}  ({t.Columns.First(c => c.Value.IsIdentity).Key})\r\n");

            MessageBox.Show(message, "Ошибка!");
            throw new Exception(message);
        }

        public static void CheckForSameTableReferenceConstraint(DataLayerGenerator generator, List<Table> tables, ref bool DBWasChanged, ref string OutputMessage)
        {
            try
            {
                List<Table> TablesWithSelfReference = tables.Where(t => t.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference"))
                                                                                     .Any(fk => fk.Value.OtherTable == t))
                                                                                     .OrderBy(t => t.TableName).ToList();

                if (TablesWithSelfReference.Count == 0)
                    return;

                MessageBoxResult result = MessageBox.Show("Обнаружены таблицы ссылающиеся сами на себя:\r\n\r\n" +
                                    TablesWithSelfReference.Aggregate(String.Empty, (s, t) => s + t.TableName + "\r\n") + "\r\nУдалить такие ссылки?",
                                    "Offline и Sync Проверки", MessageBoxButton.YesNo);

                if (result != MessageBoxResult.Yes)
                    throw new Exception("Не допускается использование базы с таблицами ссылающимися на самих себя");

                List<KeyValuePair<string, ForeignKey>> SelfReferenceForeignKeys =
                    tables.SelectMany(t => t.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference") && fk.Value.OtherTable == t))
                          .OrderBy(fk => fk.Key).ToList();

                using (var conn = generator.GetConnection())
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = SelfReferenceForeignKeys.Aggregate(String.Empty,
                        (s, fk) => s + $"ALTER TABLE [{fk.Value.OtherTable.TableName}] DROP CONSTRAINT [{fk.Key}];" + "\r\n");
                    cmd.ExecuteNonQuery();
                }

                DBWasChanged = true;

                OutputMessage += "Следующие CONSTRAINT были удалены:\r\n\r\n" +
                                SelfReferenceForeignKeys.Aggregate(String.Empty, (s, fk) => s + fk.Key + "\r\n") + "\r\n";

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка в методе {nameof(CheckForSameTableReferenceConstraint)}:\r\n\r\n{ex.Message}", "Ошибка!");
                throw;
            }
        }

        public static void CheckAndEnableChangeTracking(DataLayerGenerator generator, List<Table> tables, ref bool DBWasChanged, ref string OutputMessage)
        {
            try
            {
                if (tables.All(t => t.ChangeTracking.Enabled))
                    return;

                List<Table> TablesWithoutChangeTracking = tables.Where(t => !t.ChangeTracking.Enabled).OrderBy(t => t.TableName).ToList();

                using (var conn = generator.GetConnection())
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandText = TablesWithoutChangeTracking.Aggregate(String.Empty,
                        (s, t) => s + $"ALTER TABLE [{t.TableName}] ENABLE CHANGE_TRACKING WITH(TRACK_COLUMNS_UPDATED = OFF);" + "\r\n");
                    cmd.ExecuteNonQuery();
                }

                DBWasChanged = true;

                OutputMessage += "Для следующих таблиц был включен Change Tracking: \r\n\r\n" +
                                TablesWithoutChangeTracking.Aggregate(String.Empty, (s, t) => s + t.TableName + "\r\n") + "\r\n";
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка в методе {nameof(CheckAndEnableChangeTracking)}:\r\n\r\n{ex.Message}", "Ошибка!");
                throw;
            }
        }

        public static void CheckForCompositePrimaryKeyConstraint(List<Table> tables)
        {
            List<Table> TablesWith6AndMorePKColumns = tables.Where(t => t.Columns.Count(c => c.Value.IsPrimaryKey) > 5)
                .OrderBy(t => t.TableName).ToList();

            if (TablesWith6AndMorePKColumns.Count == 0)
                return;

            string message = "Обнаружены таблицы, у которых более 5 колонок входят в композитный ключ:\r\n\r\n" +
                             TablesWith6AndMorePKColumns.Aggregate(String.Empty, (s, t) => s + t.TableName + "\r\n");

            MessageBox.Show(message, "Ошибка!");
            throw new Exception(message);
        }

        static readonly List<String> GeneratedPVNames = new List<string>();

        public static void CheckForPartitioningViewsAndCreateThem(DataLayerGenerator generator, List<Table> tables, string GenerationDirectoryPath, int ServiceNum, ref bool DBWasChanged, ref string OutputMessage)
        {
            try
            {
                string IgnoreTablesFileName = $"S{ServiceNum}_IgnoreInPartitioningCheck.txt";
                string IgnoreTablesFilePath = Path.Combine(GenerationDirectoryPath, IgnoreTablesFileName);

                List<Table> TablesToIgnore = GetTablesToIgnore(IgnoreTablesFileName, IgnoreTablesFilePath, ref OutputMessage)
                    .Select(s => tables.FirstOrDefault(t => t.TableName == s))
                    .ToList();

                Dictionary<Table, Table> TablePartitioningViewDict = tables.Where(t => t.BoModelInfo.ItemRoles.Any(r => r == BORoleEn.PartitioningView))
                    .ToDictionary(pv => tables.FirstOrDefault( t => t.TableName == generator.TStg.GetString(DataLayerSettingsEn.DomainPrefix) + "_S"
                                                                                   + pv.BoModelInfo.ServiceIndex + "_" + pv.BoModelInfo.EntityName));

                List<Table> TablesInServiceWithoutPartitioningView = tables.Where(t => t.BoModelInfo.ServiceIndex == ServiceNum 
                                                                && t.BoModelInfo.ItemRoles.Any(r => r == BORoleEn.DataTable)
                                                                && !TablePartitioningViewDict.Keys.Contains(t)).Except(TablesToIgnore).ToList();

                List<Tuple<Table, Table, List<ForeignKey>>> TablesPVandReferenceChainForPVGeneration = 
                    GetTablesPVandReferenceChainForPVGeneration(TablesInServiceWithoutPartitioningView, TablePartitioningViewDict);

                if (AskUserAndGeneratePV(generator, ref DBWasChanged, TablesPVandReferenceChainForPVGeneration, IgnoreTablesFileName, IgnoreTablesFilePath))
                    return;

                if (GeneratedPVNames.Any())
                    OutputMessage += "Были созданы следующие Partitioning View: \r\n\r\n" +
                                     GeneratedPVNames.Aggregate(String.Empty, (s, pv) => s + pv + "\r\n") + "\r\n";

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Ошибка в методе {nameof(CheckForPartitioningViewsAndCreateThem)}:\r\n\r\n{ex.Message}", "Ошибка!");
                throw;
            }
        }

        private static bool AskUserAndGeneratePV(DataLayerGenerator generator, ref bool DBWasChanged, 
            List<Tuple<Table, Table, List<ForeignKey>>> TablesPVandReferenceChainForPVGeneration, string IgnoreTablesFileName,
            string IgnoreTablesFilePath)
        {
            foreach (Tuple<Table, Table, List<ForeignKey>> TablePVAndReferenceChain in TablesPVandReferenceChainForPVGeneration)
            {
                Table table = TablePVAndReferenceChain.Item1;
                Table ClosestPartitioningView = TablePVAndReferenceChain.Item2;
                List<ForeignKey> ReferenceChainToClosestPartitioningView = TablePVAndReferenceChain.Item3;

                MessageBoxResult result = MessageBox.Show($"Создать PartitioningView для {table.TableName}?\r\n\r\n" +
                                                          $"При нажатии \"Нет\" таблица будет добавлена в {IgnoreTablesFileName}",
                    "Offline и Sync Проверки", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.No)
                {
                    File.AppendAllText(IgnoreTablesFilePath, table.TableName + "\r\n");
                    continue;
                }

                String PVName = $"PV_S{table.BoModelInfo.ServiceIndex}_{table.BoModelInfo.EntityName}_Vo";

                using (var conn = generator.GetConnection())
                using (var cmd = conn.CreateCommand())
                {
                    int PartitioningViewOrderIndex = ReferenceChainToClosestPartitioningView.Count;

                    cmd.CommandText =
                        $"Create View {PVName} As Select\r\n" +
                        $"t{PartitioningViewOrderIndex}.Id_TrRepres,\r\n" +
                        $"t{PartitioningViewOrderIndex}.Id_Manager,\r\n" +
                        $"t{PartitioningViewOrderIndex}.Id_Director,\r\n" +
                        "t0.*\r\n" +
                        $"From {table.TableName} t0\r\n" +
                        ReferenceChainToClosestPartitioningView
                            .Select(
                                (fk, i) => $"Inner Join {fk.OtherTable.TableName} t{i + 1} On t{i}.{fk.ThisColumns.First().ColumnName} = t{i + 1}.{fk.OtherColumns.First().ColumnName}\r\n")
                            .Aggregate((acc, sql) => acc + sql)
                            .Replace($" {ReferenceChainToClosestPartitioningView.Last().OtherTable.TableName} ", $" {ClosestPartitioningView.TableName} ");
                    cmd.ExecuteNonQuery();
                }

                DBWasChanged = true;

                GeneratedPVNames.Add(PVName);

                return true;
            }

            return false;
        }

        private static List<Tuple<Table, Table, List<ForeignKey>>> GetTablesPVandReferenceChainForPVGeneration(List<Table> tables, Dictionary<Table, Table> TablePartitioningViewDict)
        {
            List<Tuple<Table, Table, List<ForeignKey>>> TablesPVandReferenceChain = new List<Tuple<Table, Table, List<ForeignKey>>>();

            foreach (Table table in tables)
            {
                List<Table> TablesReferencedByTable = GetTablesReferencedByTable(table);

                if (TablesReferencedByTable.Count == 0) continue;

                Table ClosestTableWithPartitioningView = TablesReferencedByTable.FirstOrDefault(t => TablePartitioningViewDict.Keys.Any(k => k == t));

                if (ClosestTableWithPartitioningView == null) continue;

                Table ClosestPartitioningView = TablePartitioningViewDict[ClosestTableWithPartitioningView];

                List<ForeignKey> ReferenceChainToClosestPartitioningView = new List<ForeignKey>();

                GetReferenceChainToTable(table, ClosestTableWithPartitioningView, ref ReferenceChainToClosestPartitioningView);

                ReferenceChainToClosestPartitioningView.Reverse();

                TablesPVandReferenceChain.Add(Tuple.Create(table, ClosestPartitioningView, ReferenceChainToClosestPartitioningView));
            }

            TablesPVandReferenceChain = TablesPVandReferenceChain.OrderBy(tup => tup.Item3.Count).ToList();
            return TablesPVandReferenceChain;
        }

        private static bool GetReferenceChainToTable(Table FromTable, Table ToTable, ref List<ForeignKey> ReferenceChainToClosestPartitioningView)
        {
            if (FromTable.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference")).Any(fk => fk.Value.OtherTable == ToTable))
            {
                ReferenceChainToClosestPartitioningView.Add(FromTable.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference"))
                                                            .First(fk => fk.Value.OtherTable == ToTable).Value);
                return true;
            }

            foreach (ForeignKey fk in FromTable.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference") && fk.Value.OtherTable != FromTable)
                                                         .Select(fk => fk.Value))
            {
                if (GetReferenceChainToTable(fk.OtherTable, ToTable, ref ReferenceChainToClosestPartitioningView))
                {
                    ReferenceChainToClosestPartitioningView.Add(fk);
                    return true;
                }
            }

            return false;
        }

        private static List<Table> GetTablesReferencedByTable(Table table)
        {
            List<Table> TablesReferencedByTable = table.ForeignKeys.Where(fk => !fk.Key.EndsWith("_BackReference") && fk.Value.OtherTable != table)
                             .Select(fk => fk.Value.OtherTable).ToList();

            foreach (Table ReferencedTable in TablesReferencedByTable.ToList())
                TablesReferencedByTable.AddRange(GetTablesReferencedByTable(ReferencedTable));

            return TablesReferencedByTable;
        }

        private static List<String> GetTablesToIgnore(string IgnoreTablesFileName, string IgnoreTablesFilePath, ref string OutputMessage)
        {
            if (!File.Exists(IgnoreTablesFilePath))
            {
                File.Create(IgnoreTablesFilePath);
                OutputMessage += $"Добавьте файл {IgnoreTablesFileName} в проект CRM.DOM.DAL.Server\r\n\r\n";
            }
            
            return File.ReadAllText(IgnoreTablesFilePath).Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}
