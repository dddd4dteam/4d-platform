﻿using System;
using System.Collections.Generic;
namespace PL.Generators.Model
{
   public interface IGeneratorTemplate
    {
        
       String Name { get; }
        
       /// <summary>
       /// Композитторы какие тут есть
       /// </summary>
        Dictionary<CompositorEn, IClassCompositor> Compositors { get;}
       
        
        /// <summary>
        ///Общие Сеттинги- TStg - TemplateSettings - Установки шаблона 
        /// </summary>
        GeneratorSettingsContainer TStg { get; }


        /// <summary>
        /// Итоговый метод вывода у генератора
        /// </summary>
        /// <param name="scenario"></param>
        String Generate(ScenariosEn scenario);

       
        


    }
}
