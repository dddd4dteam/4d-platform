﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using PL.Generators.Model;
using PL.Generators.Data;
using PL.Generators.DataLayer.Primitives;
using PL.Generators.Data.Primitives;
using Microsoft.VisualStudio.TextTemplating;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows;
using PL.Generators.DataLayer.CustomGenerator.Checks;

namespace PL.Generators.Data
{

    /// <summary>
    /// 
    /// </summary>
    public abstract partial class DataLayerGenerator : TextTransformation, IGeneratorTemplate
    {


        #region ------------------------------- CTOR ----------------------------------

        public DataLayerGenerator()
        {
            Tables = new Dictionary<string, Table>();
            Compositors = new Dictionary<CompositorEn, IClassCompositor>();
            
            RenderField = false;
            _templateSettings = new DataLayerGeneratorSettings();
        }

        static DataLayerGenerator()
        {
            
            Assemblies = new Dictionary<string, string>();          
        }

        #endregion ------------------------------- CTOR ----------------------------------



        #region ------------------------------- STATIC FIELDS -----------------------------------

        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<string, string> Assemblies 
        { get;
          private set;
        } 

        #endregion ------------------------------- STATIC FIELDS -----------------------------------



        #region  ----------------------- DELEGATES -----------------------------------

        /// <summary>
        /// Делегат написания свойства класса- поля таблицы
        /// </summary>
        /// <param name="tt"></param>
        /// <param name="name"></param>
        /// <param name="pname"></param>
        /// <param name="maxlen"></param>
        /// <param name="maxplen"></param>
        delegate void WriteTablePropertyAction(DataLayerGenerator tt, string name, string pname, int maxlen, int maxplen);
        

        // Some custom Actions before and After Loading MetaData 
        Action<DataLayerGenerator> BeforeLoadMetadata = _ => { };
        Action<DataLayerGenerator> AfterLoadMetadata = _ => { };

        
        // Действия -Перед генерацией Свойства Таблицы/После генерации свойства Таблицы
        Action<DataLayerGenerator> BeforeWriteTableProperty = _ => { };
        Action<DataLayerGenerator> AfterWriteTableProperty = _ => { };

        
        // Действия -Перед генерацией модели/После генерации модели
        Action<DataLayerGenerator> BeforeGenerateModel = _ => { };
        
        internal Action<DataLayerGenerator> AfterGenerateModel = _ => { };


        // PLuralization Suffixes for AssociationName
        Func<string, string> PluralizeAssociationName = _ => _ + "s";
        Func<string, string> SingularizeAssociationName = _ => _;


        static Func<string, string, string> MakeGenericType = (c, t) => string.Format("{0}<{1}>", c, t);
        static Func<string, string> MakeType = t => t;


        #endregion ----------------------- DELEGATES -----------------------------------





        #region --------------------------------- PROPERTIES ------------------------------------

        /// <summary>
        /// 
        /// </summary>
        Dictionary<string, Table> Tables { get; set; }//  = new Dictionary<string, Table>();

        public List<string> TablesForServerOnly = new List<string>();

        /// <summary>
        /// Выводить Столбец как Поле класса/ если нет то как Свойство
        /// </summary>
        bool RenderField { get; set; }

        /// <summary>
        /// 
        /// </summary>
        bool IsMetadataLoaded;

        /// <summary>
        /// 
        /// </summary>
        int MaxColumnTypeLen;


        /// <summary>
        /// 
        /// </summary>
        int MaxColumnMemberLen;



        /// <summary>
        /// Имя генератора
        /// </summary>
        public String Name
        {
            get { return "DataLayerGenerator"; }
        }


        private ITextTemplatingEngineHost hostValue;

        public new virtual ITextTemplatingEngineHost Host
        {
            get { return this.hostValue; }
            set { this.hostValue = value; }
        }


     

        /// <summary>
        /// Композиторы шаблонов какие необходимы  этому генератору
        /// </summary>
        public Dictionary<CompositorEn, IClassCompositor> Compositors
        {
            get; internal set;
        }


        GeneratorSettingsContainer _templateSettings = null;
        /// <summary>
        /// Общие настройки Шаблонов Генерации/Генератора
        /// </summary>
        public GeneratorSettingsContainer TStg
        {
            get { return _templateSettings; }
        }



        SystemCompositionElementsTp _systemComposition = null;
        /// <summary>
        /// Композиция системы - информация о составляющих данной системы
        /// </summary>
        public SystemCompositionElementsTp SystemComposition
        {
            get { return _systemComposition; }
        }


        #endregion ------- PROPERTIES ----------------------



#region ------------------------METHODS ----------------------------------


#region ----------------------- IGeneratorTemplate ----------------------

/// <summary>
/// Контрольный запуск всех шаблонов генерации в данном генераторе
/// </summary>
/// <param name="scenario"></param>
/// <returns></returns>
public String Generate(ScenariosEn scenario)
{
    try
    {

        return null;
    }
    catch (Exception)
    {
        throw new Exception("Error in Generate() in  DataLayerGenerator ");
    }
}
        
#endregion ----------------------- IGeneratorTemplate ----------------------


#region  ------------------------ Utils Methods ------------------------------

        string LenDiff(int max, string str)
        {
            var s = "";

            while (max-- > str.Length)
                s += " ";

            return s;
        }


        List<T> CreateList<T>(T item)
        {
            return new List<T>();
        }


#endregion ------------------------ Utils Methods ------------------------------

        
#region --------------------------------- LOADING METADATA -------------------------------------

        //Процедура загруки метаданных
        public void LoadMetadata()
        {
            Tables = new Dictionary<string, Table>();

            BeforeLoadMetadata(this);

            LoadServerMetadata();

            foreach (var t in Tables.Values)
            {
                if (t.ClassName.Contains(" "))
                {
                    var ss = t.ClassName.Split(' ').Where(_ => _.Trim().Length > 0).Select(_ => char.ToUpper(_[0]) + _.Substring(1));
                    t.ClassName = string.Join("", ss.ToArray());
                }
            }

            foreach (var t in Tables.Values)
                foreach (var key in t.ForeignKeys.Values.ToList())
                    if (!key.KeyName.EndsWith("_BackReference"))
                        key.OtherTable.ForeignKeys.Add(key.KeyName + "_BackReference", key.BackReference = new ForeignKey
                        {
                            KeyName = key.KeyName + "_BackReference",
                            MemberName = key.MemberName + "_BackReference",
                            AssociationType = AssociationType.Auto,
                            OtherTable = t,
                            ThisColumns = key.OtherColumns,
                            OtherColumns = key.ThisColumns,
                        });

            foreach (var t in Tables.Values)
            {
                foreach (var key in t.ForeignKeys.Values)
                {
                    if (key.BackReference != null && key.AssociationType == AssociationType.Auto)
                    {
                        if (key.ThisColumns.All(_ => _.IsPrimaryKey))
                        {
                            if (t.Columns.Values.Count(_ => _.IsPrimaryKey) == key.ThisColumns.Count)
                                key.AssociationType = AssociationType.OneToOne;
                            else
                                key.AssociationType = AssociationType.ManyToOne;
                        }
                        else
                            key.AssociationType = AssociationType.ManyToOne;

                        key.CanBeNull = key.ThisColumns.All(_ => _.IsNullable);
                    }
                }
            }

            foreach (var t in Tables.Values)
            {
                foreach (var key in t.ForeignKeys.Values)
                {
                    var name = key.MemberName;

                    if (key.BackReference != null && key.ThisColumns.Count == 1 && key.ThisColumns[0].MemberName.ToLower().EndsWith("id"))
                    {
                        name = key.ThisColumns[0].MemberName;
                        name = name.Substring(0, name.Length - "id".Length);

                        if (!t.ForeignKeys.Values.Select(_ => _.MemberName).Concat(
                             t.Columns.Values.Select(_ => _.MemberName)).Concat(
                             new[] { t.ClassName }).Any(_ => _ == name))
                        {
                            name = key.MemberName; ;
                        }
                    }

                    if (name == key.MemberName)
                    {
                        if (name.StartsWith("FK_"))
                            name = name.Substring(3);

                        if (name.EndsWith("_BackReference"))
                            name = name.Substring(0, name.Length - "_BackReference".Length);

                        name = string.Join("", name.Split('_').Where(_ => _.Length > 0 && _ != t.TableName).ToArray());

                        if (name.Length > 0)
                            name = key.AssociationType == AssociationType.OneToMany ? PluralizeAssociationName(name) : SingularizeAssociationName(name);
                    }

                    if (name.Length != 0 &&
                        !t.ForeignKeys.Values.Select(_ => _.MemberName).Concat(
                         t.Columns.Values.Select(_ => _.MemberName)).Concat(
                         new[] { t.ClassName }).Any(_ => _ == name))
                    {
                        key.MemberName = name;
                    }
                }
            }

            if (Tables.Values.SelectMany(_ => _.ForeignKeys.Values).Any(_ => _.AssociationType == AssociationType.OneToMany))
                TStg.GetList(DataLayerSettingsEn.Usings).Add("System.Collections.Generic");

            var keyWords = new HashSet<string>
	        {
		        "abstract", "as",       "base",     "bool",    "break",     "byte",     "case",       "catch",     "char",    "checked",
		        "class",    "const",    "continue", "decimal", "default",   "delegate", "do",         "double",    "else",    "enum",
		        "event",    "explicit", "extern",   "false",   "finally",   "fixed",    "float",      "for",       "foreach", "goto",
		        "if",       "implicit", "in",       "int",     "interface", "internal", "is",         "lock",      "long",    "new",
		        "null",     "object",   "operator", "out",     "override",  "params",   "private",    "protected", "public",  "readonly",
		        "ref",      "return",   "sbyte",    "sealed",  "short",     "sizeof",   "stackalloc", "static",    "struct",  "switch",
		        "this",     "throw",    "true",     "try",     "typeof",    "uint",     "ulong",      "unchecked", "unsafe",  "ushort",
		        "using",    "virtual",  "volatile", "void",    "while"
	        };

            foreach (var t in Tables.Values)
            {
                if (keyWords.Contains(t.ClassName))
                    t.ClassName = "@" + t.ClassName;

                if (keyWords.Contains(t.DataContextPropertyName))
                    t.DataContextPropertyName = "@" + t.DataContextPropertyName;

                foreach (var col in t.Columns.Values)
                    if (keyWords.Contains(col.MemberName))
                        col.MemberName = "@" + col.MemberName;
            }


            AfterLoadMetadata(this);
        }
        


        void LoadItemsBOModelInfo(List<Table> tlist, Int32 ServiceIndex)
        {
            foreach (Table pop in tlist)
            {
                pop.InitBOModelInfo(TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices));
            }
        }



        public List<Table> GetBOItemsByService(Int32 ServiceIndex)
        {
            List<Table> rez = new List<Table>();
            foreach (var pop in Tables.Values)
                if (pop.TableName.Contains("_S" + ServiceIndex.ToString() + "_"))
                    rez.Add(pop);
            LoadItemsBOModelInfo(rez, ServiceIndex);
            return rez;
        }



#endregion --------------------------------- LOADING METADATA -------------------------------------


#region ------------------------ CONNECTION --------------------------------

/// <summary>
/// Getting Connection
/// </summary>
/// <returns></returns>
internal System.Data.IDbConnection GetConnection()
{
    Type connType = null;

    if (TStg.GetString(DataLayerSettingsEn.DataProviderAssembly) != null)
    {
        try
        {
            var assembly = System.Reflection.Assembly.LoadFile(
                TStg.GetString(DataLayerSettingsEn.DataProviderAssembly));
            connType = assembly.GetType(
                TStg.GetString(DataLayerSettingsEn.ConnectionType));
        }
        catch
        {
        }
    }

    if (connType == null)
        connType = Type.GetType(TStg.GetString(DataLayerSettingsEn.ConnectionType));

    var conn = (System.Data.IDbConnection)Activator.CreateInstance(connType);

    conn.ConnectionString = TStg.GetString(DataLayerSettingsEn.ConnectionString);
    conn.Open();

    return conn;
}


#endregion ------------------------ CONNECTION --------------------------------



#region  --------------------------- WRITE PRIMITIVES METHOD-DELEGATE-PROPERTIES -------------------------------

//static Action<DataLayerGenerator, string> WriteComment = (tt, s) => tt.WriteLine("//{0}", s);
//static Action<DataLayerGenerator, string> WriteUsing = (tt, s) => tt.WriteLine("using {0};", s);
//static Action<DataLayerGenerator, string> WriteBeginNamespace = (tt, s) => { tt.WriteLine("namespace {0}", s); tt.WriteLine("{"); };
//static Action<DataLayerGenerator> WriteEndNamespace = tt => tt.WriteLine("}");
//static Action<DataLayerGenerator, string> WriteAttribute = (tt, a) => tt.Write("[{0}]", a);
//static Action<DataLayerGenerator> WriteAttributeLine = tt => tt.WriteLine("");
//static Action<DataLayerGenerator, string, string> WriteBeginClass = (tt, cl, bc) =>   //,string
//static Action<DataLayerGenerator> WriteEndClass = tt => tt.WriteLine("}");
//static WriteTablePropertyAction WriteTableProperty = (tt, name, pname, maxlen, maxplen) =>


//Методы Коротких выводов   - для начал имен - аттрибутов ... 
public static void WriteComment(DataLayerGenerator generator,  String Comment)
{  generator.WriteLine("//{0}", Comment);
}
public void WriteComment( String Comment)
{   DataLayerGenerator.WriteComment(this, Comment);   
}

/// <summary>
/// 
/// </summary>
/// <param name="generator"></param>
/// <param name="Comment"></param>
/// <param name="col"></param>
public static void WriteColumnComment(DataLayerGenerator generator, String Comment, Column col)
{
    generator.WriteLine("/// <summary>");    
    // комментарий по типу поля БД в конце свойства
    if (col.ColumnType != null)
    {
        generator.Write(" /// Column: {0}  Type: {1}",  col.ColumnName, col.ColumnType);
        
        if (col.Length != 0)
            generator.Write("({0})", col.Length);

        if (col.Precision != 0)
        {
            if (col.Scale == 0)
                generator.Write("({0})", col.Precision);
            else
                generator.Write("({0},{1})", col.Precision, col.Scale);
        }

        generator.WriteLine("");
    }
    generator.WriteLine("/// </summary>");
}
public void WriteColumnComment(String Comment, Column col)
{   DataLayerGenerator.WriteColumnComment(this, Comment,col);
}







/// <summary>
/// 
/// </summary>
/// <param name="generator"></param>
/// <param name="Comment"></param>
public static void WriteSummaryComment(DataLayerGenerator generator, String Comment)
{
    generator.WriteLine("/// <summary>");
    generator.WriteLine("/// {0}", Comment);
    generator.WriteLine("/// </summary>");
}
public void WriteSummaryComment(String Comment)
{    DataLayerGenerator.WriteSummaryComment(this, Comment);
}

        
// Записать Using
public static void WriteUsing( DataLayerGenerator generator, String UsingItemString)
{    generator.WriteLine("using {0};", UsingItemString);
}
public void WriteUsing( String UsingItemString)
{    DataLayerGenerator.WriteUsing(this, UsingItemString);
}



// записать начало Неймспайса
public static void WriteBeginNamespace( DataLayerGenerator generator, String NamespaceName)
{    generator.WriteLine("namespace {0}", NamespaceName); generator.WriteLine("{");
}
public void WriteBeginNamespace( String NamespaceName)
{   DataLayerGenerator.WriteBeginNamespace(this, NamespaceName);
}
        


// записать конец Неймспайса
public static void WriteEndNamespace( DataLayerGenerator generator)
{    generator.WriteLine("}");
}
public void WriteEndNamespace()
{   DataLayerGenerator.WriteEndNamespace(this);
}
        

// Записать аттрибут
public static void WriteAttribute( DataLayerGenerator generator, String AttributeName)
{    generator.WriteLine("[{0}]", AttributeName);
}
public void WriteAttribute(String AttributeName)
{    DataLayerGenerator.WriteAttribute(this, AttributeName);
}


/// <summary>
/// 
/// </summary>
/// <param name="generator"></param>
/// <param name="AttributeName"></param>
/// <param name="AttribParamsValues"></param>
public static void WriteAttribute(DataLayerGenerator generator, String AttributeName, params string[] AttribParamsValues)
{
    String allParamsString = "";
    if (AttribParamsValues.Length > 0)
    {   foreach (var item in AttribParamsValues)
        {   if(item != null && item != "")
            {
                allParamsString += item;
            }
        }
    }
    
    generator.Write("[{0}({1})]", AttributeName, allParamsString.ToString());
}
public void WriteAttribute(String AttributeName, params string[] AttribParamsValues)
{
    DataLayerGenerator.WriteAttribute(this, AttributeName, AttribParamsValues);
}





/// <summary>
/// 
/// </summary>
/// <param name="generator"></param>
/// <param name="AttributeName"></param>
/// <param name="AttribParamsValues"></param>
public static void WriteAttributeLine(  DataLayerGenerator generator, String AttributeName, params string[] AttribParamsValues)
{
    String allParamsString = "";
    if (AttribParamsValues.Length > 0)
    {
        foreach (var item in AttribParamsValues)
        {
            if (item != null && item!= "" )
            {
                allParamsString += item;
            }
        }
    }

    generator.WriteLine("[{0}" + ((allParamsString.Count() > 0) ? "(" + allParamsString.ToString() + ")" : "") + "]", AttributeName);
}
public void WriteAttributeLine(String AttributeName, params string[] AttribParamsValues)
{
    DataLayerGenerator.WriteAttributeLine(this, AttributeName, AttribParamsValues);
}

// Сбросить строчку после атрибута





//Описать начало класса - имя и базовый его класс

public static void WriteBeginClass(DataLayerGenerator generator, String ClassName, String BaseClassName)
{
    generator.Write("public partial class {0}", ClassName);
    if (!String.IsNullOrEmpty(generator.TStg.GetItem<string>(DataLayerSettingsEn.BaseEntityClass)))
        generator.Write(" : {0}", BaseClassName.Replace("<T>", "<" + ClassName + ">"));  //   baseclass.Replace("<T>", "<" + "" + ">");

    if (BaseClassName != generator.TStg.GetItem<string>(DataLayerSettingsEn.BaseDataContextClass))
        WriteInterfacesImplementation(generator, ClassName, !String.IsNullOrEmpty(generator.TStg.GetItem<string>(DataLayerSettingsEn.BaseEntityClass)));

    generator.WriteLine("");
    generator.WriteLine("{");
}
public void WriteBeginClass(String AttributeName, String ClassName, String BaseClassName)
{
    DataLayerGenerator.WriteBeginClass(this, ClassName, BaseClassName);
}



// Описать конец класса
public static void WriteEndClass( DataLayerGenerator generator)
{   generator.WriteLine("}");
}
public void WriteEndClass()
{   DataLayerGenerator.WriteEndClass(this);
}



/// <summary>
/// 
/// </summary>
/// <param name="generator"></param>
/// <param name="len"></param>
public static void WriteSpace(DataLayerGenerator generator, int len)
{
    while (len-- > 0)
        generator.Write(" ");
}
public void WriteSpace(int len)
{
    DataLayerGenerator.WriteSpace(this,len);
}       


// Описать свойство таблицы


/// <summary>
/// Описать свойство таблицы
/// </summary>
/// <param name="generator"></param>
/// <param name="TableName"></param>
/// <param name="PropName"></param>
/// <param name="MaxLen"></param>
/// <param name="MaxPropLen"></param>
public static  void  WriteTableProperty (DataLayerGenerator generator, String TableName, String PropName, Int32 MaxLen,Int32  MaxPropLen)
{
    generator.WriteLine("public Table<{0}>{1} {2}{3} {{ get {{ return this.GetTable<{0}>();{1} }} }}", 
        TableName,
        generator.LenDiff(MaxLen, TableName),
        PropName,
        generator.LenDiff(MaxPropLen, PropName));
}
public void WriteTableProperty(String TableName, String PropName, Int32 MaxLen, Int32 MaxPropLen)
{   DataLayerGenerator.WriteTableProperty(this, TableName, PropName, MaxLen, MaxPropLen);
}

#endregion  --------------------------- WRITE PRIMITIVES METHOD-DELEGATE-PROPERTIES -------------------------------


#region --------------------------- WRITE INTERFACES METHODS-DELEGATE-PROPERTIES ---------------------------

//вывод объявления наследования интрфейсов
//static Action<DataLayerGenerator, string, bool> WriteInterfacesImplementation = (tt, cl, hf) =>
public static void WriteInterfacesImplementation( DataLayerGenerator generator, String ClassName, bool Hf)
{
    bool IsIClonable = generator.TStg.CompareBool(DataLayerSettingsEn.UseIClonable, true);
    bool UseIVObjectRealization = generator.TStg.CompareBool(DataLayerSettingsEn.UseIVObjectRealization, true);
    //if (IsIClonable)
    //{
    //    if (hf)
    //        tt.Write(", ");
    //    else tt.Write(" : ");
    //    tt.Write("ICloneable");
    //    hf = true;
    //}
    if (generator.TStg.GetBool(DataLayerSettingsEn.UseIPersistableBO))
    {
        if (Hf)
            generator.Write(", ");
        else generator.Write(" : ");
        if (ClassName.StartsWith(generator.TStg.GetString(DataLayerSettingsEn.DomainPrefix) + "_") ||
            ((ClassName.StartsWith("V_") || ClassName.StartsWith("PV_")) && ClassName.EndsWith("_Vo")))
            generator.Write("PersistableBOBase");
        else
            generator.Write("BObjectBase");
        Hf = true;
    }

    if (UseIVObjectRealization && (ClassName.StartsWith("V_") || ClassName.StartsWith("PV_")) && ClassName.EndsWith("_Vo"))
    {
        if (Hf)
            generator.Write(", ");
        else generator.Write(" : ");
        generator.Write("IVobject");
        Hf = true;
    }

    if (generator.TStg.GetBool(DataLayerSettingsEn.UseIHierarchicalView) &&
        (ClassName.EndsWith("Ancestor") || ClassName.EndsWith("Descendant")))
    {
        if (Hf)
            generator.Write(", ");
        else generator.Write(" : ");
        generator.Write("IHierarchicalView");
        Hf = true;
    }
}
public void WriteInterfacesImplementation(String ClassName, bool Hf)
{   DataLayerGenerator.WriteInterfacesImplementation(this, ClassName, Hf);
}

//вывод заголовка клонирования
//static Action<DataLayerGenerator, Table, List<Table>> WriteIClonable = (tt, t, tlist) =>
public static void  WriteIClonable(DataLayerGenerator generator, Table table, List<Table> TblsList )
{
    bool IsIClonable = generator.TStg.CompareBool(DataLayerSettingsEn.UseIClonable, true);
    bool UseIVObjectRealization = generator.TStg.CompareBool(DataLayerSettingsEn.UseIVObjectRealization, true);

    if (table.IsView && UseIVObjectRealization &&
        (table.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) || table.BoModelInfo.ItemRoles[0] == (BORoleEn.PartitioningView)))
    {
        generator.WriteLine("");
        WriteComment(generator, " Summary:");
        WriteComment(generator, "     Creates a new table object form Value Part of current item.");
        WriteComment(generator, "");
        WriteComment(generator, " Returns:");
        WriteComment(generator, "     Creates a new table object-Target for current Value Part of current item.");
        generator.WriteLine("public  IPersistableBO CloneToTarget()");
        //Получение имени класса таблицы данных
        string tableName = generator.TStg.GetString(DataLayerSettingsEn.DomainPrefix) + "_" +
            table.ClassName.Replace("PV_","").Replace("V_", "").Replace("_Vo", "");
        Table targetTable = TblsList.Where(tl => tl.ClassName == tableName).FirstOrDefault();

        WriteIVObjectBody(generator, targetTable, TblsList);
    }

    if (IsIClonable)
    {
        generator.WriteLine("");
        WriteComment(generator, " Summary:");
        WriteComment(generator, "     Creates a new object that is a copy of the current instance.");
        WriteComment(generator, "");
        WriteComment(generator, " Returns:");
        WriteComment(generator, "     A new object that is a copy of this instance.");
        generator.WriteLine("public override object Clone()");
        WriteIClonableBody(generator, table, TblsList);
    }

    if (generator.TStg.GetBool(DataLayerSettingsEn.UseIHierarchicalView) &&
        (table.ClassName.EndsWith("Ancestor") || table.ClassName.EndsWith("Descendant")))
    {
        generator.WriteLine("");
        WriteComment(generator, " Summary:");
        WriteComment(generator, "     Creates a new VObject that is a copy of the current base instance.");
        WriteComment(generator, "");
        WriteComment(generator, " Returns:");
        WriteComment(generator, "     A new VObject that is a copy of this base instance.");
        generator.WriteLine("public IBObject ToEntityItem()");

        string tableName = table.ClassName.EndsWith("Ancestor") ?
            table.ClassName.Replace("Ancestor", "_Vo") : table.ClassName.Replace("Descendant", "_Vo");
        Table targetTable = TblsList.Where(tl => tl.ClassName == tableName).FirstOrDefault();

        WriteIHierarchicalBody(generator, targetTable, table, TblsList);
    }

    if (generator.TStg.GetBool(DataLayerSettingsEn.UseIPersistableBO) &&
        ((table.IsView && (table.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) || table.BoModelInfo.ItemRoles[0] == BORoleEn.PartitioningView)) ||
         ! table.IsView))
    {
        generator.WriteLine("");
        generator.WriteLine("public override List<String> PrimaryKey");
        WriteIPersistableBOBody(generator, table, TblsList);
    }
}
public void WriteIClonable(Table table, List<Table> TblsList)
{   DataLayerGenerator.WriteIClonable(this, table, TblsList);
}


//static Action<DataLayerGenerator, Table, List<Table>> WriteIPersistableBOBody = (tt, t, tlist) =>
public static void WriteIPersistableBOBody( DataLayerGenerator generator, Table table, List<Table> TblsList)
{
    generator.WriteLine("{");
    generator.PushIndent("\t");
    generator.Write("get {return new List<String>(){");

    List<String> list = new List<String>();

    if (table.IsView && (table.BoModelInfo.ItemRoles[0] == (BORoleEn.VObject) || table.BoModelInfo.ItemRoles[0] == BORoleEn.PartitioningView))
    {
        Table tCon = (from tb in TblsList
                      where tb.BoModelInfo.ItemRoles[0] == (BORoleEn.DataTable) &&
                          tb.BoModelInfo.ServiceIndex == table.BoModelInfo.ServiceIndex &&
                          tb.BoModelInfo.EntityName == table.BoModelInfo.EntityName
                      select tb).FirstOrDefault();
        foreach (var col in table.Columns.Values)
        {
            if ((from pt in tCon.Columns.Values
                 where pt.IsPrimaryKey &&
                     pt.MemberName == col.MemberName
                 select pt.MemberName).FirstOrDefault() ==
                col.MemberName)
            {
                if (!list.Contains(col.MemberName))
                    list.Add(col.MemberName);
            }

            if (col.IsPrimaryKey)
            {
                if (!list.Contains(col.MemberName))
                    list.Add(col.MemberName);
            }
        }
    }

    foreach (var col in table.Columns.Values)
    {
        if (col.IsPrimaryKey)
        {
            if (!list.Contains(col.MemberName))
                list.Add(col.MemberName);
        }
    }

    foreach (var item in list)
    {
        generator.Write("\"{0}\"", item);
        if (item != list.Last())
            generator.Write(", ");
    }

    generator.WriteLine("};}");
    generator.PopIndent();
    generator.WriteLine("}");
}
public void WriteIPersistableBOBody(Table table, List<Table> TblsList)
{   DataLayerGenerator.WriteIPersistableBOBody(this, table, TblsList);
}



//static Action<DataLayerGenerator, Table, Table, List<Table>> WriteIHierarchicalBody = (tt, targ, t, tlist) =>
public static void WriteIHierarchicalBody( DataLayerGenerator generator,Table targ, Table t, List<Table> TblsList )
{
    generator.WriteLine("{");
    generator.PushIndent("\t");
    //generator.WriteLine("try");
    //generator.WriteLine("{");
    //generator.PushIndent("\t");

    generator.WriteLine("return new {0}()", targ.ClassName);
    generator.WriteLine("{");
    generator.PushIndent("\t");
    foreach (var c in from c in t.Columns.Values orderby c.ID select c)
    {
        if (c.MemberName == "Level") continue;
        if (c.MemberName == "IDAncestors") continue;
        if (c.MemberName == "IDDescendants") continue;

        generator.Write("{0}{1} = this.{0}", c.MemberName, generator.LenDiff( generator.MaxColumnMemberLen, c.MemberName));
        if (c != (from col in t.Columns.Values orderby col.ID select col).Last())
            generator.WriteLine(",");
        else
            generator.WriteLine("");
    }
    generator.PopIndent();
    generator.WriteLine("};");
    generator.PopIndent();
    //generator.WriteLine("}");
    //generator.WriteLine("catch (Exception exc)");
    //generator.WriteLine("{");
    //generator.PushIndent("\t");
    //generator.WriteLine("throw exc;");
    //generator.PopIndent();
    //generator.WriteLine("}");
    //generator.PopIndent();
    generator.WriteLine("}");
}
public void WriteIHierarchicalBody(Table targ, Table t, List<Table> TblsList)
{  DataLayerGenerator.WriteIHierarchicalBody(this,targ, t,  TblsList);
}

//вывод тушки функции клонирования CloneToTarget у IVObject
//static Action<DataLayerGenerator, Table, List<Table>> WriteIVObjectBody = (tt, t, tlist) =>
public static void WriteIVObjectBody(DataLayerGenerator generator, Table t, List<Table> TblsList)
{
    generator.WriteLine("{");
    generator.PushIndent("\t");
    //generator.WriteLine("try");
    //generator.WriteLine("{");
    generator.PushIndent("\t");

    generator.WriteLine("{0} clonedItem = new {0}()", t.ClassName);
    generator.WriteLine("{");
    generator.PushIndent("\t");

    foreach (var c in from c in t.Columns.Values orderby c.ID select c)
    {
        generator.Write("{0}{1} = this.{0}", c.MemberName,  generator.LenDiff( generator.MaxColumnMemberLen, c.MemberName));
        if (c != (from col in t.Columns.Values orderby col.ID select col).Last())
            generator.WriteLine(",");
        else if ( generator.TStg.GetBool(DataLayerSettingsEn.UseForeignKeyProperties))
        {
            if (t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null).Count() > 0)
                generator.WriteLine(",");
        }
        else
            generator.WriteLine("");
    }

    if (generator.TStg.GetBool(DataLayerSettingsEn.UseForeignKeyProperties))
    {
        foreach (var key in t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null))
        {
            if (!generator.TStg.GetBool(DataLayerSettingsEn.UseFiltrationForAssociations) || TblsList.Contains(key.OtherTable))
            {
                generator.Write("{0}{1} = this.{0}", key.MemberName, generator.LenDiff( generator.MaxColumnMemberLen, key.MemberName));
                if (key != (t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null)).Last())
                    generator.WriteLine(",");
                else
                    generator.WriteLine("");
            }
        }
    }
    generator.PopIndent();
    generator.WriteLine("};");
    generator.WriteLine("return clonedItem;");
    generator.PopIndent();
    //generator.WriteLine("}");
   // generator.WriteLine("catch (Exception exc)");
    //generator.WriteLine("{");
    //generator.PushIndent("\t");
    //generator.WriteLine("throw exc;");
    //generator.PopIndent();
    //generator.WriteLine("}");
    generator.PopIndent();
    generator.WriteLine("}");
}
public void WriteIVObjectBody(Table t, List<Table> TblsList)
{    DataLayerGenerator.WriteIVObjectBody(this, t, TblsList);
}




//вывод тушки функции клонирования
//static Action<DataLayerGenerator, Table, List<Table>> WriteIClonableBody = (tt, t, tlist) =>
public static void WriteIClonableBody( DataLayerGenerator generator, Table t, List<Table> TblsList )
{
    generator.WriteLine("{");
    generator.PushIndent("\t");
    //generator.WriteLine("try");
    //generator.WriteLine("{");
    //generator.PushIndent("\t");
    generator.WriteLine("{0} clonedItem = new {0}()", t.ClassName);
    generator.WriteLine("{");
    generator.PushIndent("\t");
    foreach (var c in from c in t.Columns.Values orderby c.ID select c)
    {
        generator.Write("{0}{1} = this.{0}", c.MemberName, generator.LenDiff( generator.MaxColumnMemberLen, c.MemberName));
        if (c != (from col in t.Columns.Values orderby col.ID select col).Last())
            generator.WriteLine(",");
        else if ( generator.TStg.GetBool(DataLayerSettingsEn.UseForeignKeyProperties))
        {
            if (t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null).Count() > 0)
                generator.WriteLine(",");
        }
        else
            generator.WriteLine("");
    }

    if ( generator.TStg.GetBool(DataLayerSettingsEn.UseForeignKeyProperties))
    {
        foreach (var key in t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null))
        {
            if (! generator.TStg.GetBool(DataLayerSettingsEn.UseFiltrationForAssociations) || TblsList.Contains(key.OtherTable))
            {
                generator.Write("{0}{1} = this.{0}", key.MemberName, generator.LenDiff(generator.MaxColumnMemberLen, key.MemberName));
                if (key != (t.ForeignKeys.Values.Where(k => generator.TStg.GetBool(DataLayerSettingsEn.RenderBackReferences) || k.BackReference != null)).Last())
                    generator.WriteLine(",");
                else
                    generator.WriteLine("");
            }
        }
    }
    
    generator.PopIndent();
    generator.WriteLine("};");
    generator.WriteLine("return clonedItem;");
    generator.PopIndent();
    //generator.WriteLine("}");
    //generator.WriteLine("catch (Exception exc)");
    //generator.WriteLine("{");
    //generator.PushIndent("\t");
    //generator.WriteLine("throw exc;");
    //generator.PopIndent();
    //generator.WriteLine("}");
    //generator.PopIndent();
    generator.WriteLine("}");
}
public void WriteIClonableBody(Table t, List<Table> TblsList)
{    DataLayerGenerator.WriteIClonableBody(this, t, TblsList);
}


#endregion --------------- WRITE INTERFACES METHODS-DELEGATE-PROPERTIES ---------------------------




#region ------------------------ GENERATION TOP PROCESS ----------------------------

/// ------------------- GENERATION MAIN -----------------------
///GENERATION   PROCESS  -Процедура генерации нашей модели данных
/// ------------------- GENERATION MAIN -----------------------
public void GenerateModel(int ServiceNum, string ServiceTargetEntityName, string path = "", bool isStandartServiceModelType = true)
{
    GenerateModel(ServiceNum,
                    ServiceTargetEntityName != ""
                        ? new List<string> { ServiceTargetEntityName }
                        : null, path);
}


public void GenerateModel(int ServiceNum, List<string> ServiceTargetEntityNames, string path = "", bool isStandartServiceModelType = true)
{
    #region ---------- AdditionalServiceDependentSettings
    TStg.SetList<String>(DataLayerSettingsEn.ServiceTargetEntityName, ServiceTargetEntityNames);
    #endregion

    TStg.SetBool(DataLayerSettingsEn.ServiceModelType, isStandartServiceModelType);
    //TStg.SetString(DataLayerSettingsEn.SpecificServiceModelBaseClass,"");


    if (TStg.GetString(DataLayerSettingsEn.ConnectionString) != null)
        TStg.SetString(DataLayerSettingsEn.ConnectionString,
            TStg.GetString(DataLayerSettingsEn.ConnectionString).Trim());
    if (TStg.GetString(DataLayerSettingsEn.DataContextName) != null)
        TStg.SetString(DataLayerSettingsEn.DataContextName,
            TStg.GetString(DataLayerSettingsEn.DataContextName).Trim());

    if (string.IsNullOrEmpty(TStg.GetString(DataLayerSettingsEn.ConnectionString)))
    { Error("ConnectionString cannot be empty."); return; }

    if (string.IsNullOrEmpty(TStg.GetString(DataLayerSettingsEn.DataContextName)))
        TStg.SetString(DataLayerSettingsEn.DataContextName, "DataContext");

    LoadMetadata();

   

    BeforeGenerateModel(this);

    WriteComment(this, "--------------------------------------------------------------------------------------------------");
    WriteComment(this, " <auto-generated>");
    WriteComment(this, "    This code was generated by 4D Team ORM- [DDDD.Core]  template for T4.");
    WriteComment(this, "    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.");
    WriteComment(this, " </auto-generated>");
    WriteComment(this, "--------------------------------------------------------------------------------------------------");

    RenderUsing(this);

    WriteBeginNamespace(this, TStg.GetString(DataLayerSettingsEn.Namespace));

    PushIndent("\t");
    
    //ServiceRefTp servCur = TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices)
    //    .First(s => s.Index == ServiceNum);
    //foreach (ServiceRefTp Serv in TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices))
    //    GetBOItemsByService(Serv.Index);
     
    //RenderServiceModelClass(this, servCur, GetBOItemsByService(ServiceNum));

    //Условный вывод класса контекста данных
    if (TStg.GetBool(DataLayerSettingsEn.UseDataContextClassGeneration)
        && !String.IsNullOrEmpty(TStg.GetString(DataLayerSettingsEn.BaseDataContextClass)))
    {
        List<Table> tServList = new List<Table>();// all services table/viewa
        foreach (ServiceRefTp servc in TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices))
        {
            tServList.AddRange(GetBOItemsByService(servc.Index));
        }
        tServList = FilterTables(tServList);
        if (tServList.Count() > 0)
            RenderDataContextClass(this, tServList);
    }

    if (TStg.GetBool(DataLayerSettingsEn.GenerateClassesForTables))
    {
        //инициализируем ресурсы
        if (path != "")
            InitRes(ServiceNum, path: path);

        //Получаем те таблицы которые нам нужны
        List<Table> tServList = GetBOItemsByService(ServiceNum);
                //WriteLine("{0}", tlist.Count());
        
        if (TStg.GetBool(DataLayerSettingsEn.PerformChecksForSyncAndOffline))
            PrepareForSyncAndOffline(tServList, ServiceNum, ServiceTargetEntityNames, path);

        ServiceRefTp servCur = TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices)
            .First(s => s.Index == ServiceNum);

        RenderServiceModelClass(this, servCur, tServList );


        foreach (var t in FilterTables(tServList))
        {
            WriteLine("");
            RenderTable(this, t, tServList);
            if (path != "")
                AddBOtoRes(t, this);
        }
        if (path != "")
            CloseRes();
    }

    PopIndent();

    WriteEndNamespace(this);

    AfterGenerateModel(this);
}

        bool DBWasChanged = false;
        string OutputMessageForSyncAndOfflineChecks = String.Empty;

        private void PrepareForSyncAndOffline(List<Table> tServList, int ServiceNum, List<string> ServiceTargetEntityNames, string path)
        {
            bool DBWasChangedInThisIteration = false;
            bool ExceptionOccurred = false;

            try
            {
                List<Table> STables = tServList.Where(t => t.BoModelInfo.DbItemCategory == DBItemCategoryEn.TableObject).ToList();

                SyncAndOfflineChecks.CheckAndEnableChangeTracking(this, STables, ref DBWasChangedInThisIteration, ref OutputMessageForSyncAndOfflineChecks);
                SyncAndOfflineChecks.CheckForSameTableReferenceConstraint(this, STables, ref DBWasChangedInThisIteration, ref OutputMessageForSyncAndOfflineChecks);
                SyncAndOfflineChecks.CheckForIdentityColumns(STables);
                SyncAndOfflineChecks.CheckForCompositePrimaryKeyConstraint(STables);
                SyncAndOfflineChecks.CheckForPartitioningViewsAndCreateThem(this, GetAllBOItems(), path, ServiceNum, ref DBWasChangedInThisIteration,
                    ref OutputMessageForSyncAndOfflineChecks);

                if (DBWasChangedInThisIteration)
                {
                    Action<DataLayerGenerator> GenerateModelAgainDelegate = null;
                    GenerateModelAgainDelegate = generator => GenerateModelAgain(ServiceNum, ServiceTargetEntityNames, path, GenerateModelAgainDelegate);
                    AfterGenerateModel += GenerateModelAgainDelegate;
                }
            }
            catch
            {
                ExceptionOccurred = true;
            }
            finally
            {
                if (DBWasChangedInThisIteration)
                    DBWasChanged = true;

                if ((!DBWasChangedInThisIteration || ExceptionOccurred) && OutputMessageForSyncAndOfflineChecks != String.Empty)
                    MessageBox.Show(OutputMessageForSyncAndOfflineChecks + (DBWasChanged ? "Обновите проект CRM-DB" : ""), "Offline и Sync Проверки");
            }
        }

        private List<Table> GetAllBOItems()
        {
            List<Table> tables = Tables.Select(t => t.Value)
                         .Where(t => Regex.IsMatch(t.TableName, @"(?<=_S)\d*?(?=_)"))
                         .ToList();
            foreach (Table table in tables)
                table.InitBOModelInfo(TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices));

            return tables;
        }

        private void GenerateModelAgain(int ServiceNum, List<string> ServiceTargetEntityNames, string path, Action<DataLayerGenerator> GenerateModelAgainDelegate)
        {
            GenerationEnvironment.Clear();
            AfterGenerateModel -= GenerateModelAgainDelegate;
            GenerateModel(ServiceNum, ServiceTargetEntityNames, path);
        }


        public void GenerateServiceManager(int ServiceNum)
{
    if (TStg.GetString(DataLayerSettingsEn.ConnectionString) != null)
        TStg.SetString(DataLayerSettingsEn.ConnectionString,
            TStg.GetString(DataLayerSettingsEn.ConnectionString).Trim());
    if (TStg.GetString(DataLayerSettingsEn.DataContextName) != null)
        TStg.SetString(DataLayerSettingsEn.DataContextName,
            TStg.GetString(DataLayerSettingsEn.DataContextName).Trim());

    if (string.IsNullOrEmpty(TStg.GetString(DataLayerSettingsEn.ConnectionString)))
    { Error("ConnectionString cannot be empty."); return; }

    LoadMetadata();

    WriteComment(this, "--------------------------------------------------------------------------------------------------");
    WriteComment(this, " <auto-generated>");
    WriteComment(this, "    This code was generated by BLToolkit template for T4.");
    WriteComment(this, "    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.");
    WriteComment(this, " </auto-generated>");
    WriteComment(this, "--------------------------------------------------------------------------------------------------");

    RenderUsing(this);

    WriteBeginNamespace(this, TStg.GetString(DataLayerSettingsEn.Namespace));

    PushIndent("\t");
    ServiceRefTp serv = TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.
        SystemServices).First(s => s.Index == ServiceNum);

    foreach (ServiceRefTp Serv in TStg.GetList<ServiceRefTp>(DataLayerSettingsEn.SystemServices))
        GetBOItemsByService(Serv.Index);

    RenderModelManager(this, serv, GetBOItemsByService(ServiceNum));

    PopIndent();

    WriteEndNamespace(this);
}


#endregion ------------------------ GENERATION TOP PROCESS ----------------------------




#endregion ------------------------METHODS ----------------------------------









    }
}
