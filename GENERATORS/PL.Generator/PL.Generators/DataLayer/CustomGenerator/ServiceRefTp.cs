﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace PL.Generators.Data
{
    /// <summary>
    ///  Типизация разрабатываемых сервисов системы/ Сервисов данных/ Возможно сервис по какой-то  известной Структуре  Данных
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "ServiceTypeEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum ServiceTypeEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        CustomStructureServiceType,

        /// <remarks/>
        [EnumMember()]
        HierarchyStructureServiceType,
    }

    /// <summary>
    /// Краткая ссылочка на сервис системы - необходим для регистрации сервисов  системы - и для определений DAL уровня
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Systems.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "ServiceRefTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class ServiceRefTp : System.ComponentModel.INotifyPropertyChanged
    {

        private int indexField;

        private string serviceNameField;

        private string descriptionField;

        private ServiceTypeEn serviceTypeField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// Индекс сервиса
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public int Index
        {
            get
            {
                return this.indexField;
            }
            set
            {
                if ((indexField.Equals(value) != true))
                {
                    this.indexField = value;
                    this.OnPropertyChanged("Index");
                }
            }
        }

        /// <summary>
        /// Название сервсиа
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public string ServiceName
        {
            get
            {
                return this.serviceNameField;
            }
            set
            {
                if ((this.serviceNameField != null))
                {
                    if ((serviceNameField.Equals(value) != true))
                    {
                        this.serviceNameField = value;
                        this.OnPropertyChanged("ServiceName");
                    }
                }
                else
                {
                    this.serviceNameField = value;
                    this.OnPropertyChanged("ServiceName");
                }
            }
        }

        /// <summary>
        /// Описаниеназначения сервиса системы
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public string Description
        {
            get
            {
                return this.descriptionField;
            }
            set
            {
                if ((this.descriptionField != null))
                {
                    if ((descriptionField.Equals(value) != true))
                    {
                        this.descriptionField = value;
                        this.OnPropertyChanged("Description");
                    }
                }
                else
                {
                    this.descriptionField = value;
                    this.OnPropertyChanged("Description");
                }
            }
        }

        /// <summary>
        /// ТИп сервиса - что за структуризация сервиса/возможно сервис какойто спеуифицированной структуры данных
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public ServiceTypeEn ServiceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                if ((serviceTypeField.Equals(value) != true))
                {
                    this.serviceTypeField = value;
                    this.OnPropertyChanged("ServiceType");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(ServiceRefTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        public static bool Deserialize(string xml, out ServiceRefTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ServiceRefTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out ServiceRefTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static ServiceRefTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((ServiceRefTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        public static bool LoadFromFile(string fileName, out ServiceRefTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ServiceRefTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out ServiceRefTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static ServiceRefTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        public virtual ServiceRefTp Clone()
        {
            return ((ServiceRefTp)(this.MemberwiseClone()));
        }
        #endregion
    }
}
