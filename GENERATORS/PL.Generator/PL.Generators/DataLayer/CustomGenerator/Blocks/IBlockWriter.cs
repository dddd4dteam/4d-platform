﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model.Composition;


namespace PL.Generators.Model
{
    /// <summary>
    /// Блок райтер
    /// </summary>
    public interface IBlockWriter
    {   
        /// <summary>
        /// 
        /// </summary>
        ClassBlockEn BlockKey 
        { get;  set; }

       
        

        /// <summary>
        /// установка параметров
        /// </summary>
        /// <param name="CurrentParameters"></param>
        void SetParameters(ParametersCollectionTp CurrentParameters);
                
    }

    




}
