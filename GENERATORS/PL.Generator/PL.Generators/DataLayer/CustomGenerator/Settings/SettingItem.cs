﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PL.Generators.Model
{

    /// <summary>
    /// Устновка- Опция
    /// </summary>
    public class SettingItem
    {
        /// <summary>
        /// Единица- установка/ опция 
        /// </summary>
        public SettingItem(Enum key, object ItemValue  )
        {
            _key = key.ToString();
            _value = ItemValue;
            _type = ItemValue.GetType();
        }


        /// <summary>
        /// Единица- установка/ опция 
        /// </summary>
        public SettingItem(String key, object ItemValue)
        {
            _key = key;
            _value = ItemValue;
            _type = ItemValue.GetType();
        }


        /// <summary>
        /// Единица- установка/ опция 
        /// </summary>
        public SettingItem(String key, Type proptyType, object ItemValue)
        {
            _key = key;
            _value = ItemValue;
            _type = proptyType;
        }




        private Type _type;
        /// <summary>
        /// Тип Знчения установки
        /// </summary>
        public Type Type
        {
            get { return _type; }
            set { _type = value; }
        }


        private object _value;
        /// <summary>
        /// значение опции 
        /// </summary>
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }
        
        private String _key;        
        /// <summary>
        /// Ключ для опции
        /// </summary>
        public String Key
        {
            get { return _key; }
            set { _key = value; }
        }

    }
}
