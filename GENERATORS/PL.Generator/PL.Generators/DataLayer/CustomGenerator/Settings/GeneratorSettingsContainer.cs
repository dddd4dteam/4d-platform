﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace PL.Generators.Model
{


    /// <summary>
    /// Контейнер настроек Шаблона Генерации
    /// </summary>
    public abstract class GeneratorSettingsContainer
    {

        

        /// <summary>
        /// Ctor GeneratorSettingsContainer 
        /// </summary>
        public GeneratorSettingsContainer() { }


        /// <summary>
        /// Ctor GeneratorSettingsContainer
        /// </summary>
        /// <param name="CurrentAspectSettings"></param>
        public GeneratorSettingsContainer(Dictionary<String, AspectSettingsContainer> CurrentAspectSettings)
        { _aspects = CurrentAspectSettings; }


        /// <summary>
        /// Словарь по перечислению Установок для Апектов генератора
        /// </summary>
        Dictionary<String, AspectSettingsContainer> _aspects = new Dictionary<string, AspectSettingsContainer>();
        
        
        /// <summary>
        /// Все зарегистрированные Установки Шаблона 
        /// </summary>
        SortedList<String,SettingItem> _allSettings= new SortedList<String,SettingItem>();


        /// <summary>
        /// Установки/опции для Аспектов Генератора
        /// </summary>
        internal Dictionary<String, AspectSettingsContainer> Aspects
        {
            get { return _aspects; }
            set { _aspects = value; }
        }



        #region ---------------Get/Set/ Compare/ Contain Settings Operations------------------


        #region -------------  Get Operations --------------------

        /// <summary>
        /// Получить T  значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public T GetItem<T>(String SettingKey) 
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(T))
                    return (T)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetItem<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Получить T значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public T GetItem<T>(Enum SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(T))
                    return (T)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetItem<T>(Enum) SettingItem with Key " + SettingKey + ".");
            }
        }





        /// <summary>
        /// Получить сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public String GetString(String SettingKey )
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(String))
                    return (String)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception )
            {
                throw new Exception("GetString(String) SettingItem with Key " + SettingKey + ".");  
            }        
        }


        /// <summary>
        /// Получить сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public String GetString(Enum SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(String))
                    return (String)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception )
            {
                throw new Exception("GetString(Enum) SettingItem with Key " + SettingKey + ".");  
            }
        }


        /// <summary>
        /// Получить булевое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public bool GetBool(String SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(bool))
                    return (bool)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetBool(String) SettingItem with Key " + SettingKey + ".");  
            }
        }


        /// <summary>
        /// Получить булевое значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public bool GetBool(Enum SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(bool))
                    return (bool)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetBool(Enum) SettingItem with Key " + SettingKey + ".");  
            }
        }


        /// <summary>
        /// Получить список строк  у значение установки. Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<String> GetList(String SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(List<String>))
                    return (List<String>)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Получить список строк  у значение установки. Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<String> GetList(Enum SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(List<String>))
                    return (List<String>)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList(Enum)) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Получить список T  значений у  установки. Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<T> GetList<T>(String SettingKey) 
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(List<T>))
                    return (List<T>)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        ///  Получить список T  значений у  установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<T> GetList<T>(Enum SettingKey)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(List<T>))
                    return (List<T>)_allSettings[SettingKey.ToString()].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList<T>(Enum)) SettingItem with Key " + SettingKey + ".");
            }
        }
        

        #endregion -------------  Get Operations --------------------
        

        #region ------------ Set Operations ---------------------

        /// <summary>
        /// Задать T значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetItem<T>(String SettingKey, String NewValue)
        {
            try
            {
                if (_allSettings[SettingKey].Type == typeof(T))
                    _allSettings[SettingKey].Value = NewValue;
                else { throw new Exception(); }

            }
            catch (Exception)
            {
                throw new Exception("SetItem<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать T значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetItem<T>(Enum SettingKey, T NewValue)
        {
            try
            {

                if (_allSettings[SettingKey.ToString()].Type == typeof(T))
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }

            }
            catch (Exception)
            {
                throw new Exception("SetItem<T>(Enum) SettingItem with Key " + SettingKey + ".");
            }
        }


        
        /// <summary>
        /// Задать сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetString(String SettingKey,String NewValue)
        {
            try
            {

                if (_allSettings[SettingKey].Type == typeof(String) || _allSettings[SettingKey.ToString()].Type == typeof(string))
                  _allSettings[SettingKey].Value = NewValue;
              else { throw new Exception(); }
              
            }
            catch (Exception)
            {
                throw new Exception("SetString(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetString(Enum SettingKey, String NewValue)
        {
            try
            {

                if (_allSettings[SettingKey.ToString()].Type == typeof(String) || _allSettings[SettingKey.ToString()].Type == typeof(string))
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }

            }
            catch (Exception)
            {
                throw new Exception("SetString(Enum) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать булевое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetBool(String SettingKey, bool NewValue)
        {
            try
            {

                if (_allSettings[SettingKey.ToString()].Type == typeof(bool) || _allSettings[SettingKey.ToString()].Type == typeof(Boolean))
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }

            }
            catch (Exception)
            {
                throw new Exception("SetBool(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать булевое значение установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetBool(Enum SettingKey, bool NewValue)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(bool) || _allSettings[SettingKey.ToString()].Type == typeof(Boolean))
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }
            }
            catch (Exception)
            {
                throw new Exception("SetBool(Enum) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать  список строковых значений для установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        //public void SetList(String SettingKey, List<String> NewValue)
        //{
        //    try
        //    {
        //        if (_allSettings[SettingKey.ToString()].Type == NewValue.GetType() || _allSettings[SettingKey.ToString()].Type == (NewValue as List<string>).GetType())
        //            _allSettings[SettingKey.ToString()].Value = NewValue;
        //        else { throw new Exception(); }
        //    }
        //    catch (Exception)
        //    {
        //        throw new Exception("SetList(String) SettingItem with Key " + SettingKey + ".");
        //    }
        //}


        ///// <summary>
        ///// Задать  список строковых значений для установки..Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        ///// </summary>
        ///// <param name="SettingKey"></param>
        //public void SetList(Enum SettingKey, List<String> NewValue)
        //{
        //    try
        //    {
        //        if (_allSettings[SettingKey.ToString()].Type == NewValue.GetType())
        //            _allSettings[SettingKey.ToString()].Value = NewValue;
        //        else { throw new Exception(); }
        //    }
        //    catch (Exception)
        //    {
        //        throw new Exception("SetList(Enum)) SettingItem with Key " + SettingKey + ".");
        //    }
        //}


        /// <summary>
        /// Задать список значений T для установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetList<T>(String SettingKey, List<T> NewValue)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == NewValue.GetType()  )
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }
            }
            catch (Exception)
            {
                throw new Exception("SetList<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }


        /// <summary>
        /// Задать список значений T для установки.Зарегистрированное значение установки через один из аспектов. По значению из enum-а  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetList<T>(Enum SettingKey, List<T> NewValue)
        {
            try
            {
                if (_allSettings[SettingKey.ToString()].Type == typeof(List<T>))//NewValue.GetType())
                    _allSettings[SettingKey.ToString()].Value = NewValue;
                else { throw new Exception(); }
            }
            catch (Exception)
            {
                throw new Exception("SetList<T>(Enum)) SettingItem with Key " + SettingKey + ".");
            }
        }



        #endregion ------------ Set Operations ---------------------
        

        #region ---------------- Compare Operations ----------------------


        /// <summary>
        /// Сравнить строковые значения
        /// </summary>
        public bool CompareString(String SettingKey, String YourValue)
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)_allSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("CompareString(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }

                
                return  (YourValue == settingValue);

                //return false;
            }
            catch (Exception exc)
            {
                throw new Exception("CompareString(String) SettingItem with Key " + SettingKey + ".");  
            }
        }


        /// <summary>
        /// Сравнить строковые значения
        /// </summary>
        public bool CompareString(Enum SettingKey, String YourValue)
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)_allSettings[SettingKey.ToString()].Value;
                }
                catch (Exception)
                { throw new Exception("CompareString(Enum) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return (YourValue == settingValue);                
                
            }
            catch (Exception)
            {
                throw new Exception("CompareString(Enum) SettingItem with Key " + SettingKey + ".");  
            }
        }
        

        /// <summary>
        /// Сравнить булевые значения
        /// </summary>
        public bool CompareBool(String SettingKey, bool YourValue)
        {
            try
            {
                bool settingValue;

                try
                {
                    settingValue = (bool)_allSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("CompareBool(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return (YourValue == settingValue);     
            }
            catch (Exception)
            {
                throw new Exception("CompareBool(String) SettingItem with Key " + SettingKey + ".");  
            }            
        }



        /// <summary>
        /// Сравнить булевые значения
        /// </summary>
        public bool CompareBool(Enum SettingKey, bool YourValue )
        {
            try
            {
                bool settingValue;

                try
                {
                    settingValue = (bool)_allSettings[SettingKey.ToString()].Value;
                }
                catch (Exception)
                { throw new Exception("CompareBool(Enum) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return (YourValue == settingValue); 
            }
            catch (Exception )
            {
                throw new Exception("CompareBool(Enum) SettingItem with Key " + SettingKey + ".");  
            }
        }

        #endregion ---------------- Compare Operations ----------------------


        #region ------------- Contains Operations --------------------
        /// <summary>
        /// Содержание строки
        /// </summary>
        /// <param name="SettingKey"></param>
        /// <param name="ContainWhat"></param>
        public bool Contains(String SettingKey, String ContainWhat  )
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)_allSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("Contains(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return settingValue.Contains(ContainWhat);  
            }
            catch (Exception)
            {
                throw new Exception("Contains(String) SettingItem with Key " + SettingKey + ".");  
            }
        }

        /// <summary>
        /// Содержание строки
        /// </summary>
        /// <param name="SettingKey"></param>
        /// <param name="ContainWhat"></param>
        public bool Contains(Enum SettingKey, String ContainWhat)
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)_allSettings[SettingKey.ToString()].Value;
                }
                catch (Exception)
                { throw new Exception("Contains(Enum) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return settingValue.Contains(ContainWhat);  
            }
            catch (Exception )
            {
                throw new Exception("Contains(Enum) SettingItem with Key " + SettingKey + ".");  
            }
        }

        #endregion ------------- Contains Operations --------------------


        #endregion --------------- Get/ Set/ Compare/ Contain  Settings Operations ------------------


        #region    ---------------- Register/Contains/Get  Aspects Operations ---------------

        #region ------------------ Contains Operations ----------------------
        /// <summary>
        /// Проверка на уже добавленный Контейнер Установок
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <returns></returns>
        internal bool ContainsAspectKey(String AspectKey)
        {
            try
            {    
                return (_aspects.ContainsKey(AspectKey));                    
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        /// Проверка на уже добавленный Контейнер Установок
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <returns></returns>
        internal bool ContainsAspectKey(Enum AspectKey)
        {
            try
            {
                return (_aspects.ContainsKey(AspectKey.ToString()));
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion ------------------ Contains Operations ----------------------

        #region -------------------  Register  Operations--------------------
      

        /// <summary>
        /// Регистрация Аспекта установок 
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <param name="aspectContainer"></param>
        internal void RegisterAspect( AspectSettingsContainer aspectContainer)
        {
            try
            {               

                if (!_aspects.ContainsKey(aspectContainer.Aspect.ToString()))
                    _aspects.Add(aspectContainer.Aspect.ToString(), aspectContainer);
                else throw new Exception();

                
                foreach (var PropItem in aspectContainer.GetType().GetProperties())
                {
                    _allSettings.Add(PropItem.Name, new SettingItem(PropItem.Name,PropItem.PropertyType, null));
                }
            }
            catch (Exception)
            {
                throw new Exception("RegisterAspect(String AspectKey) -  already exist Aspect settings with such Key: " + aspectContainer.Aspect.ToString());
            }
        }
        


        #endregion -------------------  Register  Operations--------------------

        #region ----------------   Get Aspects Container --------------

        /// <summary>
        ///  Получить Аспект установок по ключу 
        /// </summary>
        /// <param name="Aspect"></param>
        /// <returns></returns>
        public AspectSettingsContainer GetAspectByKey(String Aspect)
        {
            try
            {
                if (_aspects.ContainsKey(Aspect))
                    return _aspects[Aspect];
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetAspectByKey(String ) error. Ther is no aspect with such Key");
            }
        }

        /// <summary>
        /// Получить Аспект установок по ключу 
        /// </summary>
        /// <param name="Aspect"></param>
        /// <returns></returns>
        public AspectSettingsContainer GetAspectByKey(Enum Aspect)
        {
            try
            {
                if (_aspects.ContainsKey(Aspect.ToString()))
                    return _aspects[Aspect.ToString()];
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetAspectByKey(Enum ) error. Ther is no aspect with such Key");
            }
        }

        #endregion  ----------------   Get Aspects Container --------------


        #endregion   ---------------- Register/Contains/Get  Aspects Operations ---------------




        /// <summary>
        /// Инициализация - Регистрация всех контейнеров Аспектов 
        /// </summary>
        public abstract GeneratorSettingsContainer InitAspects();


        public abstract void SetDefaultsSettingsByScenario(ScenariosEn scenario, string ConnectionString, bool GenerateResForAllViews=false, bool PerformChecksForSyncAndOffline = true);

    }


}
