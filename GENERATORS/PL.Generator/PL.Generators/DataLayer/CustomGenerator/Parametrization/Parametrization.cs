﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PL.Generators.Model
{

    /// <summary>
    /// Направление параметра
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Parametrization.org/2010/Schema")]
    [DataContractAttribute(Name = "DirectionEn", Namespace = "http://www.Parametrization.org/2010/Schema")]
    public enum DirectionEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        @in,

        /// <remarks/>
        [EnumMember()]
        @out,
    }

    /// <summary>
    /// Аттрибут который находится в стартовой конфигурации системы
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Parametrization.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Parametrization.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "ParameterTp", Namespace = "http://www.Parametrization.org/2010/Schema")]
    public partial class ParameterTp : System.ComponentModel.INotifyPropertyChanged
    {

        private string typeNameField;

        private string nameField;

        private DirectionEn directionField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        public ParameterTp() { }


        //Что за тип параметра Коллекция Ли?
        public bool IsCollectionValue
        {
            get
            {
                if (CollectionValue != null) return true;
                else return false;
            }
        }

        object _value = null;
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }

        [DataMember]
        public List<object> CollectionValue { get; set; }


        Type _type = null;
        /// <summary>
        ///  тип объекта параметра - 
        /// </summary>
        public Type Type
        {
            get { return _type; }
        }


        Enum _key;

        /// <summary>
        /// Ключ параметра - известный менеджеру комманд
        /// </summary>
        [DataMember]
        public Enum Key
        {
            get { return _key; }
            set { _key = value; }
        }


        /// <summary>
        /// Параметр
        /// </summary>
        /// <param name="parameterType"></param>
        /// <param name="ParamName"></param>
        /// <param name="Value"></param>
        public ParameterTp(String ParamName, object Value)
        {
            _type = Value.GetType();
            nameField = ParamName;

            // Либо простой тип
            _value = Value;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ParamName"></param>
        /// <param name="Value"></param>
        public ParameterTp(Enum ParamName, object Value)
        {
            _type = Value.GetType();
            nameField = ParamName.ToString();

            // Либо простой тип
            _value = Value;

        }




        /// <summary>
        /// Параметр
        /// </summary>
        /// <param name="parameterType"></param>
        /// <param name="ParamName"></param>
        /// <param name="Value"></param>
        public ParameterTp(String ParamName, List<object> Collection)
        {
            _type = Value.GetType();
            nameField = ParamName;
            CollectionValue = Collection;
            return;

        }

        /// <summary>
        /// имя типа параметра
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public string TypeName
        {
            get
            {
                return this.typeNameField;
            }
            set
            {
                if ((this.typeNameField != null))
                {
                    if ((typeNameField.Equals(value) != true))
                    {
                        this.typeNameField = value;
                        this.OnPropertyChanged("TypeName");
                    }
                }
                else
                {
                    this.typeNameField = value;
                    this.OnPropertyChanged("TypeName");
                }
            }
        }

        /// <summary>
        /// Название параметра
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                if ((this.nameField != null))
                {
                    if ((nameField.Equals(value) != true))
                    {
                        this.nameField = value;
                        this.OnPropertyChanged("Name");
                    }
                }
                else
                {
                    this.nameField = value;
                    this.OnPropertyChanged("Name");
                }
            }
        }

        /// <summary>
        /// Направление для параметра - на входе или на выходе
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public DirectionEn Direction
        {
            get
            {
                return this.directionField;
            }
            set
            {
                if ((directionField.Equals(value) != true))
                {
                    this.directionField = value;
                    this.OnPropertyChanged("Direction");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(ParameterTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        public static bool Deserialize(string xml, out ParameterTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ParameterTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out ParameterTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static ParameterTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((ParameterTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        public static bool LoadFromFile(string fileName, out ParameterTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ParameterTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out ParameterTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static ParameterTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        public virtual ParameterTp Clone()
        {
            return ((ParameterTp)(this.MemberwiseClone()));
        }
        #endregion
    }

    /// <summary>
    /// Коллекция параметров 
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Parametrization.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Parametrization.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "ParametersCollectionTp", Namespace = "http://www.Parametrization.org/2010/Schema")]
    public partial class ParametersCollectionTp : System.ComponentModel.INotifyPropertyChanged
    {

        private List<ParameterTp> parameterField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// ParametersCollectionTp class constructor
        /// </summary>
        public ParametersCollectionTp()
        {
            this.parameterField = new List<ParameterTp>();
        }

        [System.Xml.Serialization.XmlElementAttribute("Parameter", Order = 0)]
        [DataMemberAttribute()]
        public List<ParameterTp> Parameters
        {
            get
            {
                return this.parameterField;
            }
            set
            {
                if ((this.parameterField != null))
                {
                    if ((parameterField.Equals(value) != true))
                    {
                        this.parameterField = value;
                        this.OnPropertyChanged("Parameters");
                    }
                }
                else
                {
                    this.parameterField = value;
                    this.OnPropertyChanged("Parameters");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(ParametersCollectionTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current ParametersCollectionTp object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an ParametersCollectionTp object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output ParametersCollectionTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out ParametersCollectionTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ParametersCollectionTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out ParametersCollectionTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static ParametersCollectionTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((ParametersCollectionTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current ParametersCollectionTp object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an ParametersCollectionTp object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output ParametersCollectionTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out ParametersCollectionTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ParametersCollectionTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out ParametersCollectionTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static ParametersCollectionTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this ParametersCollectionTp object
        /// </summary>
        public virtual ParametersCollectionTp Clone()
        {
            return ((ParametersCollectionTp)(this.MemberwiseClone()));
        }
        #endregion
    }
}
