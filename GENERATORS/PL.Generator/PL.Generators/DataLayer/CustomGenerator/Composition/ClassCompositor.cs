﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;

namespace PL.Generators.Model.Composition
{

   



    /// <summary>
    /// Композитор класса
    /// </summary>
    public abstract class ClassCompositor : IClassCompositor
    {
        /// <summary>
        /// Композитора класса
        /// </summary>
        public ClassCompositor( List<AspectEn> CurrentAspects ,
                                Dictionary<ClassBlockEn, BlockWriterBase> CurrentBlocks,
                                GeneratorSettingsContainer CurrentTemplateSettings,
                                TextTransformation CurrentTransformationTemplate)
        {
            _aspects = CurrentAspects;
            _blocks = CurrentBlocks;
            _templateSettings = CurrentTemplateSettings;
            _transformationTemplate = CurrentTransformationTemplate;
        }

        
        List<AspectEn> _aspects = new List<AspectEn>();
        /// <summary>
        /// Аспекты 
        /// </summary>
        public List<AspectEn> Aspects
        {
            get { return _aspects; }
        }


        Dictionary<ClassBlockEn, BlockWriterBase> _blocks = null;
        /// <summary>
        /// Блоки вывода
        /// </summary>
        public Dictionary<ClassBlockEn, BlockWriterBase> Blocks
        {
            get { return _blocks; }
        }



        GeneratorSettingsContainer _templateSettings = null;        
        /// <summary>
        /// Установки шаблона
        /// </summary>
        public GeneratorSettingsContainer TemplateSettings
        {
            get { return _templateSettings; }
        }



        TextTransformation _transformationTemplate = null;        
        /// <summary>
        /// Шаблон преобразования
        /// </summary>
        public TextTransformation TransformationTemplate
        {
            get { return _transformationTemplate; }
        }


        /// <summary>
        /// Вывести комопзицию класса
        /// </summary>
        /// <param name="classItemParameters">  Параметры текущей единицы- класса  </param>
        /// <returns></returns>
        public abstract string WriteComposition(ParametersCollectionTp classItemParameters);
       

    }
}
