﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TextTemplating;
using PL.Generators.Model.Composition;

namespace PL.Generators.Model
{

    /// <summary>
    /// Композитор класса
    /// </summary>
    public interface IClassCompositor
    {
        /// <summary>
        /// По заданному количеству аспектов для данного композитора выводим класс в методе  WriteComposition
        /// </summary>
        List<AspectEn> Aspects { get; }

        /// <summary>
        /// Блоки вывода у текущего композитора
        /// </summary>
        Dictionary<ClassBlockEn, BlockWriterBase> Blocks { get; } 
            
        /// <summary>
        /// Общие сеттинги Родительского Шаблона Трансформации  
        /// </summary>
        GeneratorSettingsContainer TemplateSettings { get; }
              
        /// <summary>
        /// Текущий шаблон трансформаций
        /// </summary>
        TextTransformation TransformationTemplate  {  get ; }


        String WriteComposition(ParametersCollectionTp classItemParameters);

    }
}
