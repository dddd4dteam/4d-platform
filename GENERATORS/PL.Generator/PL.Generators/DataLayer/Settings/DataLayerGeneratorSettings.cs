﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;
using System.IO;

namespace PL.Generators.Data
{
    public class DataLayerGeneratorSettings : GeneratorSettingsContainer
    {


        /// <summary>
        /// 
        /// </summary>
        public override GeneratorSettingsContainer InitAspects()
        {

            try
            {
                AspectSettingsContainer aspectContainer = new SystemCompositionSettings(AspectEn.SystemCompositionSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "BOModelSettings"));
            }


            try
            {
                AspectSettingsContainer aspectContainer = new CurrentServiceSettings(AspectEn.CurrentServiceSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "ServiceModelSettings"));
            }

            try
            {
                AspectSettingsContainer aspectContainer = new BLDALModelSettings(AspectEn.BLDALModelSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "BLDALModelSettings"));
            }


            try
            {
                AspectSettingsContainer aspectContainer = new DataContextSettings(AspectEn.DataContextSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "DataContextSettings"));
            }



            try
            {
                AspectSettingsContainer aspectContainer = new DataContractSettings(AspectEn.DataContractSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "DataContractSettings"));
            }


            try
            {
                AspectSettingsContainer aspectContainer = new EntitiesBaseClassSettings(AspectEn.EntitiesBaseClassSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "EntitiesBaseClassSettings"));
            }




            try
            {

                AspectSettingsContainer aspectContainer = new AdditionalAttributesSettings(AspectEn.AdditionalAttributesSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "AdditionalAttributesSettings"));
            }



            return this;



        }


        /// -------------------------------------------------------------------------------------------------
        //  -------------- SETTINGS END - Параметры для кастомизации процесса генерации Модели ----------------
        /// -------------------------------------------------------------------------------------------------



        #region  -------------------- Setting Custom Options by Scenarious  ---------------------


        public override void SetDefaultsSettingsByScenario(ScenariosEn scenario, string ConnectionString, bool GenerateResForAllViews = false, bool PerformChecksForSyncAndOffline = true)
        {
            try
            {
                #region Default Settings
                SetString(DataLayerSettingsEn.ConnectionType, typeof(System.Data.SqlClient.SqlConnection).AssemblyQualifiedName);
                SetBool(DataLayerSettingsEn.UseFiltrationForAssociations, true);
                SetItem<DomainServRoleFiltration>(DataLayerSettingsEn.FiltrationType, DomainServRoleFiltration.Custom);
                SetList<BORoleEn>(DataLayerSettingsEn.TableEntities_Suffixes, new List<BORoleEn>());
                SetList<BORoleEn>(DataLayerSettingsEn.ViewEntities_Suffixes, new List<BORoleEn>());
                SetString(DataLayerSettingsEn.Namespace, "DataModel");
                SetString(DataLayerSettingsEn.BaseDataContextClass, "DbManager");
                SetString(DataLayerSettingsEn.OneToManyAssociationType, "IEnumerable<{0}>");
                SetBool(DataLayerSettingsEn.RenderBackReferences, true);
                SetBool(DataLayerSettingsEn.GenerateResForAllViews, GenerateResForAllViews);
                SetBool(DataLayerSettingsEn.PerformChecksForSyncAndOffline, PerformChecksForSyncAndOffline);
                SetItem<ScenariosEn>(DataLayerSettingsEn.GeneratorCurrentScenario, scenario);
                SetString(DataLayerSettingsEn.ConnectionString,
                          ConnectionString == "" ? "Data Source=LC-OLAPTEST;Initial Catalog=CRM_Cersanit2;User ID=sa;Password=cersanit" : ConnectionString);

                #endregion.

                switch (scenario)
                {
                    case ScenariosEn.NotDefined:
                        break;
                    case ScenariosEn.DefaultGeneratorSettings:

                        SetDefaultsSettingsByScenario(ScenariosEn.ServerSettingsScenario,"",false);
                        break;

                    //--------------- SERVER SETTINGS SCENARIO ------------------------
                    case ScenariosEn.ServerSettingsScenario:
                        DataLayerGeneratorSettings Stg = this;

                        SystemCompositionSettings cfg8 = Stg.GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;                        
                        cfg8.GenerateClassesForTables = true;
                        cfg8.SystemCompositionElementsFilePath = @"c:\Platform\Models\SystemCompositionElements.cpmdl";
                        cfg8.RenderBackReferences = false;

                        // --------Loading SystemServices----------
                        cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath).ServiceRefContainer.Services.ToList();
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;


                        BLDALModelSettings cfg1 = Stg.GetAspectByKey(AspectEn.BLDALModelSettingsAspect) as BLDALModelSettings;
                        // ------------ Attributes SETTINGS -----------
                        
                        cfg1.UseTableNameAttribute = true;
                        cfg1.UsePropNullableAttribute = true;
                        cfg1.UsePropIdentityAttribute = false;// true;
                        cfg1.UsePropPrimaryKeyAttribute = false; //true;
                        cfg1.UseForeignKeyProperties = false;
                        cfg1.UseBOPrimaryKeyRoleAttribute = false;
                        // ------------ Attributes SETTINGS -----------


                        CurrentServiceSettings cfg2 = Stg.GetAspectByKey(AspectEn.CurrentServiceSettingsAspect) as CurrentServiceSettings;
                        // 1 Генерировать Аттрибуты Ролей свойства у БО  - 
                        cfg2.UseBOPropertyRoleAttribute = true;
                        // 2 Принадлежность Контракта к модели сервиса
                        cfg2.UseServiceModelAttribute = true;
                        // 3 Имя сервиса 
                        //cfg2.CurrentServiceName = "S1_Geography";


                        DataContextSettings cfg3 = Stg.GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        cfg3.Namespace = "CRM.DAL";

                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации
                        //cfg3.Usings.Add("BLToolkit.ServiceModel");
                        //cfg3.Usings.Add("BLToolkit.Data");
                        //cfg3.Usings.Add("BLToolkit.Data.Linq");
                        //cfg3.Usings.Add("BLToolkit.DataAccess");
                        //cfg3.Usings.Add("BLToolkit.Mapping");
                        //cfg3.Usings.Add("BLToolkit.Data.DataProvider");
                        //cfg3.Usings.Add("BLToolkit.Data.Sql");
                        //cfg3.Usings.Add("BLToolkit.Data.Sql.SqlProvider");


                        //cfg3.Usings.Add("System");
                        //cfg3.Usings.Add("System.ServiceModel");
                        //cfg3.Usings.Add("System.ComponentModel");
                        //cfg3.Usings.Add("System.Runtime.Serialization");
                        cfg3.Usings.Add("System");
                        cfg3.Usings.Add("System.Resources");                        
                        cfg3.Usings.Add("System.Collections.Generic");
                        cfg3.Usings.Add("System.Runtime.Serialization");
                        
                        //cfg3.Usings.Add("BLToolkit.Data.Sql");
                        //cfg3.Usings.Add("BLToolkit.Data.Sql.SqlProvider");


                        
                        cfg3.Usings.Add("DDDD.Core.Data.DA2");
                        cfg3.Usings.Add("DDDD.Core.Data.DA2.Mapping");


                        //cfg3.Usings.Add("Core.Composition.DataServices");
                        //cfg3.Usings.Add("Core.Meta.BO");
                        //cfg3.Usings.Add("Core.BoModel");
                        //cfg3.Usings.Add("Core.Meta.Systems");

                        // DataContext Settings
                        cfg3.BaseDataContextClass = "DbManager";
                        cfg3.UseDataContextClassGeneration = false;
                        cfg3.DomainPrefix = "crs";


                        DataContractSettings cfg4 = Stg.GetAspectByKey(AspectEn.DataContractSettingsAspect) as DataContractSettings;
                        // -----------   Data Contracts SETTINGS    
                        cfg4.UseDataContracts = false;// true ;
                        // ---------- Data Contracts SETTINGS


                        EntitiesBaseClassSettings cfg5 = Stg.GetAspectByKey(AspectEn.EntitiesBaseClassSettingsAspect) as EntitiesBaseClassSettings;
                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg5.UseBaseEntityClass = false; // Использовать базовый класс сущностей
                        cfg5.UseBaseEntityClassGeneric = false;
                        cfg5.UseBaseEntityClassPlane = false;
                        cfg5.BaseEntityClass = "BObjectBase<T>"; //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)      
                        cfg5.BasePlaneEntityClass = "IBObject"; // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)


                        AdditionalAttributesSettings cfg7 = Stg.GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseIClonable = true;
                        cfg7.UseIVObjectRealization = true;
                        cfg7.UseVObjectMappingAttribute = true;
                        cfg7.UseIHierarchicalView = true;
                        cfg7.UseIPersistableBO = true;
                        break;

                    //--------------- CLIENT SETTINGS SCENARIO ------------------------        
                    case ScenariosEn.ClientSettingsScenario:
                        cfg8 = GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;
                        cfg8.GenerateClassesForTables = true;
                        cfg8.SystemCompositionElementsFilePath = @"c:\Platform\Models\SystemCompositionElements.cpmdl";
                        cfg8.RenderBackReferences = false;

                        // --------Loading SystemServices----------
                        cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath).ServiceRefContainer.Services.ToList();
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;
                        cfg8.FiltrationType = DomainServRoleFiltration.TableNotDatasByDomainService_9;


                        cfg1 = GetAspectByKey(AspectEn.BLDALModelSettingsAspect) as BLDALModelSettings;
                        // ------------ Attributes SETTINGS -----------
                        cfg1.UseTableNameAttribute = false;
                        cfg1.UsePropNullableAttribute = false;
                        cfg1.UsePropIdentityAttribute = false;
                        cfg1.UsePropPrimaryKeyAttribute = false;
                        cfg1.UseForeignKeyProperties = false;
                        cfg1.UseBOPrimaryKeyRoleAttribute = true;
                        // ------------ Attributes SETTINGS -----------


                        cfg2 = GetAspectByKey(AspectEn.CurrentServiceSettingsAspect) as CurrentServiceSettings;
                        // 1 Генерировать Аттрибуты Ролей свойства у БО  - 
                        cfg2.UseBOPropertyRoleAttribute = true;
                        // 2 Принадлежность Контракта к модели сервиса
                        cfg2.UseServiceModelAttribute = true;
                        // 3 Имя сервиса 
                        //cfg2.CurrentServiceName = "S1_Geography";


                        cfg3 = GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        //cfg3.ConnectionString = "Server=LC-WIN7X64-4;Database=CRM_Cersanit2;Integrated Security=SSPI";
                        SetString(DataLayerSettingsEn.ConnectionString,
                               ConnectionString == "" ? "Server=.;User ID=sa;Password=Cersanit345;Trusted_Connection=False;Integrated Security=True;" : ConnectionString);

                        cfg3.Namespace = "CRM.DAL";

                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации
                        //cfg3.Usings.Add("BLToolkit.ServiceModel");
                        //cfg3.Usings.Add("BLToolkit.Data");
                        //cfg3.Usings.Add("BLToolkit.Data.Linq");
                        //cfg3.Usings.Add("BLToolkit.DataAccess");
                        //cfg3.Usings.Add("BLToolkit.Mapping");
                        //cfg3.Usings.Add("BLToolkit.Data.DataProvider");
                        //cfg3.Usings.Add("BLToolkit.Data.Sql");
                        //cfg3.Usings.Add("BLToolkit.Data.Sql.SqlProvider");

                        cfg3.Usings.Add("System");
                        cfg3.Usings.Add("System.Resources");
                        cfg3.Usings.Add("System.Collections.Generic");
                        cfg3.Usings.Add("System.Runtime.Serialization");

                       
                        //cfg3.Usings.Add("Core.Composition.DataServices");
                        //cfg3.Usings.Add("Core.Meta.BO");
                        //cfg3.Usings.Add("Core.Meta.Systems");
                        //cfg3.Usings.Add("Core.BoModel");
                        cfg3.Usings.Add("DDDD.Core.Data.DA2");
                        cfg3.Usings.Add("DDDD.Core.Data.DA2.Mapping");

                        // DataContext Settings
                        cfg3.BaseDataContextClass = "DbManager";
                        cfg3.UseDataContextClassGeneration = false;
                        cfg3.DomainPrefix = "crs";


                        cfg4 = GetAspectByKey(AspectEn.DataContractSettingsAspect) as DataContractSettings;
                        // -----------   Data Contracts SETTINGS    
                        cfg4.UseDataContracts = false;// true
                        // ---------- Data Contracts SETTINGS


                        cfg5 = GetAspectByKey(AspectEn.EntitiesBaseClassSettingsAspect) as EntitiesBaseClassSettings;
                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg5.UseBaseEntityClass = false; // Использовать базовый класс сущностей
                        cfg5.UseBaseEntityClassGeneric = false;
                        cfg5.UseBaseEntityClassPlane = false;
                        cfg5.BaseEntityClass = "BObjectBase<T>"; //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)      
                        cfg5.BasePlaneEntityClass = "IBObject"; // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)


                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg7 = GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseIClonable = true;
                        cfg7.UseIVObjectRealization = true;
                        cfg7.UseVObjectMappingAttribute = false;
                        cfg7.UseIHierarchicalView = true;
                        cfg7.UseIPersistableBO = true;
                        break;

                    // ---------- MODEL MANAGER SETTINGS -------------
                    case ScenariosEn.ModelManagerSettings:
                        cfg8 = GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;
                        cfg8.SystemCompositionElementsFilePath = @"c:\Platform\Models\SystemCompositionElements.cpmdl";

                        // --------Loading SystemServices----------
                        cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath).ServiceRefContainer.Services.ToList();
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;
                        cfg8.FiltrationType = DomainServRoleFiltration.TableNotDatasByDomainService_9;


                        cfg3 = GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        //cfg3.ConnectionString = "Server=LC-WIN7X64-4;Database=CRM_Cersanit2;Integrated Security=SSPI";
                        SetString(DataLayerSettingsEn.ConnectionString,
                               ConnectionString == "" ? "Server=.;User ID=sa;Password=Cersanit345;Trusted_Connection=False;Integrated Security=True;" : ConnectionString);

                        cfg3.Namespace = "CRM.DOM.Services";
                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации

                        cfg3.Usings.Add("System");
                        cfg3.Usings.Add("System.Collections.Generic");
                        cfg3.Usings.Add("System.Linq");
                        cfg3.Usings.Add("System.Text");
                         cfg3.Usings.Add("System.Resources");
                         cfg3.Usings.Add("CRM.DAL");
                        //cfg3.Usings.Add("Core.Commanding");
                        //cfg3.Usings.Add("Core.Meta.Systems");
                        //cfg3.Usings.Add("Core.Composition.DataServices");
                        //cfg3.Usings.Add("Core");
                        //cfg3.Usings.Add("Core.Meta.BO");
                        //cfg3.Usings.Add("Core.BoModel");
                        //cfg3.Usings.Add("Core.Meta.VVM");

                        // DataContext Settings
                        cfg3.BaseDataContextClass = "IServiceModelManager";
                        cfg3.UseDataContextClassGeneration = false;
                        cfg3.DomainPrefix = "crs";
                        
                        cfg7 = GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseBOContracts = true;

                        break;

                    case ScenariosEn.CustomGeneratorSettings:
                        break;

                    default:
                        SetDefaultsSettingsByScenario(ScenariosEn.ServerSettingsScenario,"");
                        break;
                }



            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        #endregion  -------------------- Setting Custom Options by Scenarious  ---------------------

    }
}



