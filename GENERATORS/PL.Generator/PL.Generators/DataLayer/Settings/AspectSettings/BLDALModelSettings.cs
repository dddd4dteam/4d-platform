﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data
{

    /// <summary>
    /// Свойства Модели DAL от BLToolkit-а
    /// </summary>
    public class BLDALModelSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public BLDALModelSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region ---------- ASPECT SETTINGS -----------------
        // ------------ Attributes SETTINGS -----------
        

        public bool UseTableNameAttribute
        {
            get { return commonSettings.GetBool("UseTableNameAttribute"); }
            set { commonSettings.SetBool("UseTableNameAttribute",value); }
        }
        

        public bool UsePropNullableAttribute
        {
            get { return commonSettings.GetBool("UsePropNullableAttribute"); }
            set { commonSettings.SetBool("UsePropNullableAttribute",value);  }
        }
        

        public bool UsePropIdentityAttribute
        {
            get { return commonSettings.GetBool("UsePropIdentityAttribute"); }
            set { commonSettings.SetBool("UsePropIdentityAttribute",value); }
        }
        

        public bool UsePropPrimaryKeyAttribute
        {
            get { return commonSettings.GetBool("UsePropPrimaryKeyAttribute"); }
            set { commonSettings.SetBool("UsePropPrimaryKeyAttribute", value); }
        }
        

        public bool UseForeignKeyProperties
        {
            get { return commonSettings.GetBool("UseForeignKeyProperties"); }
            set { commonSettings.SetBool("UseForeignKeyProperties", value); }
        }

        public bool UseBOPrimaryKeyRoleAttribute
        {
            get { return commonSettings.GetBool("UseBOPrimaryKeyRoleAttribute"); }
            set { commonSettings.SetBool("UseBOPrimaryKeyRoleAttribute", value); }
        }

        public bool UseFiltrationForAssociations
        {
            get { return commonSettings.GetBool("UseFiltrationForAssociations"); }
            set { commonSettings.SetBool("UseFiltrationForAssociations", value); }
        }

        public bool PerformChecksForSyncAndOffline
        {
            get { return commonSettings.GetBool(nameof(PerformChecksForSyncAndOffline)); }
            set { commonSettings.SetBool(nameof(PerformChecksForSyncAndOffline), value); }
        }

        // ------------ Attributes SETTINGS -----------
        #endregion -----------ASPECT SETTINGS -----------------

    }
}
