﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data
{

    // ----------Entities BASE CLASS OPTIONS ----------------
    /// <summary>
    /// Установки для базового класса сущностей
    /// </summary>
    public class EntitiesBaseClassSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public EntitiesBaseClassSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region ------------------- ASPECT SETTINGS --------------------

        // Базовый класс 
        public bool UseBaseEntityClass //  = false;      // Использовать базовый класс сущностей
        {
            get { return commonSettings.GetBool("UseBaseEntityClass"); }
            set { commonSettings.SetBool("UseBaseEntityClass", value); }    
        }
        
        public bool UseBaseEntityClassGeneric// = false;
        {
            get { return commonSettings.GetBool("UseBaseEntityClassGeneric"); }
            set { commonSettings.SetBool("UseBaseEntityClassGeneric", value); }    
        }

        public bool UseBaseEntityClassPlane // = false;
        {
            get { return commonSettings.GetBool("UseBaseEntityClassPlane"); }
            set { commonSettings.SetBool("UseBaseEntityClassPlane", value); }    
        }

        public string BaseEntityClass  // = "";  //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)
        {
            get { return commonSettings.GetString("BaseEntityClass"); }
            set { commonSettings.SetString("BaseEntityClass", value); }    
        }

        public string BasePlaneEntityClass // = "";     // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)
        {
            get { return commonSettings.GetString("BasePlaneEntityClass"); }
            set { commonSettings.SetString("BasePlaneEntityClass", value); }    
        }

        #endregion ------------------- ASPECT SETTINGS --------------------

    }
    // ---------- BASE CLASS OPTIONS ----------------


}
