﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data

{

    /// <summary>
    /// Установки использования Контрактов данных
    /// </summary>
    public class DataContractSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public DataContractSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
        : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS -----------------
        
        
        /// <summary>
        /// UseDataContracts - 
        /// </summary>
        public bool UseDataContracts // = false;
        {
            get { return commonSettings.GetBool("UseDataContracts"); }
            set { commonSettings.SetBool("UseDataContracts", value); } 
        }

        // ---------- Data Contracts SETTINGS

        #endregion -------------- ASPECT SETTINGS -----------------

    }


}
