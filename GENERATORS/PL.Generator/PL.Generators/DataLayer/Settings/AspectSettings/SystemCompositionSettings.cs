﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data
{

    /// <summary>
    /// Установки  для текущего( в процессе генерации) сервиса.
    ///  Они из меняются
    /// при переходе от одного сервиса к другому 
    /// </summary>
    public class SystemCompositionSettings : AspectSettingsContainer
    {
        #region ------------Ctors-----------

        public SystemCompositionSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS -------------
        
        // -----------   Service Model SETTINGS
        //  Генерировать таблицы модели

        /// <summary>
        /// Включить в генерацию типы объектов с ролью DataTable
        /// </summary>
        public bool GenerateClassesForTables// = true;
        {
            get { return commonSettings.GetBool("GenerateClassesForTables"); }
            set { commonSettings.SetBool("GenerateClassesForTables", value); }    
        }


        /// <summary>
        /// Путь к файлу описания Элементов Образующих Систему  
        /// </summary>
        public String SystemCompositionElementsFilePath
        {
            get { return commonSettings.GetString("SystemCompositionElementsFilePath"); }
            set { commonSettings.SetString("SystemCompositionElementsFilePath", value); }    
        }

        public bool RenderBackReferences
        {
            get { return commonSettings.GetBool("RenderBackReferences"); }
            set { commonSettings.SetBool("RenderBackReferences", value); }  
        }

        public bool GenerateResForAllViews
        {
            get { return commonSettings.GetBool("GenerateResForAllViews"); }
            set { commonSettings.SetBool("GenerateResForAllViews", value); }  
        }

        /// <summary>
        /// Список названий сервисов системы
        /// </summary>
        public List<ServiceRefTp> SystemServices
        {
            get { return commonSettings.GetList<ServiceRefTp>("SystemServices"); }
            set { commonSettings.SetList<ServiceRefTp>("SystemServices", value); }
        }

        /// <summary>
        /// Список доменных префиксов у таблиц
        /// </summary>
        public List<String> DomainTableEntitiesPrefixes
        {
            get { return commonSettings.GetList<String>("DomainTableEntitiesPrefixes"); }
            set { commonSettings.SetList<String>("DomainTableEntitiesPrefixes", value); }    
        }

        /// <summary>
        /// ищем ли Вьюшки в фильтрации
        /// </summary>
        public bool DomainViewFiltration
        {
            get { return commonSettings.GetBool("DomainViewFiltration"); }
            set { commonSettings.SetBool("DomainViewFiltration", value); }
        }

        /// <summary>
        /// список ролей для выбираемых таблиц("", "Register")
        /// </summary>
        public List<BORoleEn> TableEntities_Suffixes
        {
            get { return commonSettings.GetList<BORoleEn>("TableEntities_Suffixes"); }
            set { commonSettings.SetList<BORoleEn>("TableEntities_Suffixes", value); }
        }

        /// <summary>
        /// список суффиксов для вью ("", "Register", "Vo")
        /// </summary>
        public List<BORoleEn> ViewEntities_Suffixes
        {
            get { return commonSettings.GetList<BORoleEn>("ViewEntities_Suffixes"); }
            set { commonSettings.SetList<BORoleEn>("ViewEntities_Suffixes", value); }
        }

        public DomainServRoleFiltration FiltrationType
        {
            get { return commonSettings.GetItem<DomainServRoleFiltration>(DataLayerSettingsEn.FiltrationType); }
            set { commonSettings.SetItem<DomainServRoleFiltration>(DataLayerSettingsEn.FiltrationType, value); }
        }

        /// <summary>
        /// Сценарий по которому происходит текущая генерация
        /// </summary>
        public ScenariosEn GeneratorCurrentScenario
        {
            get { return commonSettings.GetItem<ScenariosEn>(DataLayerSettingsEn.GeneratorCurrentScenario); }
            set { commonSettings.SetItem<ScenariosEn>(DataLayerSettingsEn.GeneratorCurrentScenario, value); }
        }


        // ---------- Service Model SETTINGS
        #endregion -------------- ASPECT SETTINGS -------------
    }

}
