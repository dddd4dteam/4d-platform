﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data
{
    // ------ Параметры для кастомизации процесса генерации Модели ------
    /// <summary>
    /// Установки для контекста данных
    /// </summary>
    public class DataContextSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public DataContextSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
        : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS -----------------
        
        /// <summary>
        /// 
        /// </summary>
        public bool UseDataContextClassGeneration
        {
            get { return commonSettings.GetBool("UseDataContextClassGeneration"); }
            set { commonSettings.SetBool("UseDataContextClassGeneration", value); }
        }

        /// <summary>
        /// Список дополнительных USING директив
        /// </summary>
        public List<string> Usings
        {
            get { return commonSettings.GetList("Usings"); }
            set { commonSettings.SetList("Usings", value); }
        }


        public string ConnectionString
        {
            get { return commonSettings.GetString("ConnectionString"); }
            set { commonSettings.SetString("ConnectionString", value); }
        }


        public string ConnectionType
        {
            get { return commonSettings.GetString("ConnectionType"); }
            set { commonSettings.SetString("ConnectionType", value); }
        }

        public string DataProviderAssembly
        {
            get { return commonSettings.GetString("DataProviderAssembly"); }
            set { commonSettings.SetString("DataProviderAssembly", value); }
        }


        public string DatabaseName
        {
            get { return commonSettings.GetString("DatabaseName"); }
            set { commonSettings.SetString("DatabaseName", value); }
        }


        public string DataContextName
        {
            get { return commonSettings.GetString("DataContextName"); }
            set { commonSettings.SetString("DataContextName", value); }        
        }

        public string Namespace
        {
            get { return commonSettings.GetString("Namespace"); }
            set { commonSettings.SetString("Namespace", value); }        
        }
            
        public string BaseDataContextClass
        {
            get { return commonSettings.GetString("BaseDataContextClass"); }
            set { commonSettings.SetString("BaseDataContextClass", value); }        
        }

        public string OneToManyAssociationType            // = "IEnumerable<{0}>";
        {
            get { return commonSettings.GetString("OneToManyAssociationType"); }
            set { commonSettings.SetString("OneToManyAssociationType", value); }        
        }

        public string DomainPrefix                  //= "crs";
        {   get { return commonSettings.GetString("DomainPrefix"); }
            set { commonSettings.SetString("DomainPrefix", value); } 
        }    
            

        #endregion -------------- ASPECT SETTINGS -----------------

    }

}
