﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data

{
    /// <summary>
    /// Настройки Интерфейсов (с динамическим механизмом добавления(not released) )
    /// </summary>
    public class AdditionalAttributesSettings : AspectSettingsContainer
    {
        #region ------------Ctors-----------

        public AdditionalAttributesSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect,CurrentCommonSettings)
        {        }

        #endregion ----------Ctor---------


        #region ------------- Aspect SETTINGS----------------

        /// <summary>
        /// 4 UseIClonable Создать IClonable
        /// </summary>
        public bool UseIClonable //= true;
        {
            get { return commonSettings.GetBool("UseIClonable"); }
            set { commonSettings.SetBool("UseIClonable", value); }
        }

        /// <summary>
        /// UseIHierarchicalView 
        /// </summary>
        public bool UseIHierarchicalView //= true;
        {
            get { return commonSettings.GetBool("UseIHierarchicalView"); }
            set { commonSettings.SetBool("UseIHierarchicalView", value); }
        }

        /// <summary>
        /// UseIVObjectRealization
        /// </summary>
        public bool UseIVObjectRealization //= true;
        {
            get { return commonSettings.GetBool("UseIVObjectRealization"); }
            set { commonSettings.SetBool("UseIVObjectRealization", value); }
        }


        /// <summary>
        /// UseVObjectMappingAttribute
        /// </summary>
        public bool UseVObjectMappingAttribute //  = true;
        {
            get { return commonSettings.GetBool("UseVObjectMappingAttribute"); }
            set { commonSettings.SetBool("UseVObjectMappingAttribute", value); }
        }

        public bool UseBOContracts
        {
            get { return commonSettings.GetBool("UseBOContracts"); }
            set { commonSettings.SetBool("UseBOContracts", value); }
        }

        public bool UseIPersistableBO
        {
            get { return commonSettings.GetBool("UseIPersistableBO"); }
            set { commonSettings.SetBool("UseIPersistableBO", value); }
        }

        #endregion ------------- Aspect SETTINGS----------------


        #region  -------------- BEGIN Dynamic Extenting Elements Using ------------------

        //динамическая модель получения блока 
        SortedList<String, Dictionary<Enum, object>> _Interfaces = new SortedList<String, Dictionary<Enum, object>>();

        public SortedList<String, Dictionary<Enum, object>> Interfaces
        {
            get { return _Interfaces; }
            set { _Interfaces = value; }
        }

        /// <summary>
        /// Добавить 
        /// </summary>
        /// <param name="InteraceParams"></param>
        public void AddInterface(String Interface, Dictionary<Enum, object> InteraceParams)
        {
            try
            {
                this._Interfaces.Add(Interface, InteraceParams);
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion  --------------END Dynamic Extenting Elements Using -----------------

    }


}
