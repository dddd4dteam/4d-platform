﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PL.Generators.Data
{
    /// <summary>
    /// Аспекты классов и типов классов(Класс Контекста данных, Класс Сущностей... здесь пока нет аспектов  для фолрмирования элементов  интерфейса)
    /// </summary>
    public enum DataLayerAspectsEn
    {
        /// <summary>
        /// Не определенный вариант -только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,
        /// <summary>
        /// аспект для класса контекста данных
        /// </summary>
        DataContextSettingsAspect, //

        /// <summary>
        /// аспект базового класса для сущностей 
        /// </summary>
        EntitiesBaseClassSettingsAspect, //  

        /// <summary>
        /// аспект использования Контрактов данных 
        /// </summary>
        DataContractSettingsAspect, //

        /// <summary>
        /// аспект Сервисной Модели - все 
        /// </summary>
        SystemCompositionSettingsAspect,

        /// <summary>
        /// аспект DAL настроек для библиотеки BLToolkit
        /// </summary>
        BLDALModelSettingsAspect,

        /// <summary>
        /// аспект настроек для модели одного сервиса,имеено текущего сервиса
        /// </summary>
        CurrentServiceSettingsAspect,


        /// <summary>
        /// аспект использования внешних дополнительных атрибутов разработчика
        /// </summary>
        AdditionalAttributesSettingsAspect
    }

    /// <summary>
    /// Свойства аспектов
    /// </summary>
    public enum DataLayerSettingsEn
    {
        // AdditionalAttributesSettings
        UseIClonable,
        UseIVObjectRealization,
        UseVObjectMappingAttribute,
        UseBOContracts,
        UseIHierarchicalView,
        UseIPersistableBO,

        // BLDALModelSettings
        UseTableNameAttribute,
        UsePropNullableAttribute,
        UsePropIdentityAttribute,
        UsePropPrimaryKeyAttribute,
        UseForeignKeyProperties,
        UseBOPrimaryKeyRoleAttribute,
        UseFiltrationForAssociations,

        // CurrentServiceSettings
        UseBOPropertyRoleAttribute,
        UseServiceModelAttribute,
        CurrentServiceName,
        /// <summary>
        /// true - Is standart Service model Type/ false - Specific Service Model Type
        /// </summary>
        ServiceModelType, // standart or specific
        ///// <summary>
        ///// Ex: HierarchyServiceModel
        ///// </summary>
        //SpecificServiceModelBaseClass,
        ServiceTargetEntityName,

        //DataContextSettings
        UseDataContextClassGeneration,
        Usings,
        ConnectionString,
        ConnectionType,
        DataProviderAssembly,
        DatabaseName,
        DataContextName,
        Namespace,
        BaseDataContextClass,
        OneToManyAssociationType,
        DomainPrefix,

        //DataContractSettings
        UseDataContracts,

        //EntitiesBaseClassSettings
        UseBaseEntityClass,
        UseBaseEntityClassGeneric,
        UseBaseEntityClassPlane,
        BaseEntityClass,
        BasePlaneEntityClass,

        //SystemCompositionSettings
        GenerateClassesForTables,
        SystemCompositionElementsFilePath,
        SystemServices,
        DomainTableEntitiesPrefixes,
        DomainViewFiltration,
        TableEntities_Suffixes,
        ViewEntities_Suffixes,
        FiltrationType,
        /// <summary>
        /// отображать свойства для ForeignKeys
        /// </summary>
        RenderBackReferences,

        GenerateResForAllViews,
        
        GeneratorCurrentScenario // Сценацрий     
        ,
        PerformChecksForSyncAndOffline
    }

    /// <summary>
    /// фильтрация таблиц по домену, сервису и ролям
    /// </summary>
    public enum DomainServRoleFiltration
    {
        Custom = 0,
        AllByDomainService_2 = 2,
        RegistersByDomainService_3 = 3,
        VoByDomainService_4 = 4,
        VoRegistersByDomainService_5 = 5,
        DatasByDomainService_6 = 6,
        ViewDatasbyDomainService_7 = 7,
        TableDatasByDomainService_8 = 8,
        TableNotDatasByDomainService_9 = 9
    }
}
