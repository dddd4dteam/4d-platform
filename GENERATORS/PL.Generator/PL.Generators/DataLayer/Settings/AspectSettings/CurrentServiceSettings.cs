﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PL.Generators.Model;

namespace PL.Generators.Data
{
    /// <summary>
    /// Установки  для текущего( в процессе генерации) сервиса.
    ///  Они из меняются при переходе от одного сервиса к другому. 
    /// </summary>
    public class CurrentServiceSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public CurrentServiceSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS ------------------

        // -----------   BObject Model SETTINGS

        // 1 Генерировать Аттрибуты Ролей свойства у БО  - 
        public bool UseBOPropertyRoleAttribute
        {
            get { return commonSettings.GetBool("UseBOPropertyRoleAttribute"); }
            set { commonSettings.SetBool("UseBOPropertyRoleAttribute", value); }
        }

        // 2 Принадлежность Контракта к модели сервиса
        public bool UseServiceModelAttribute
        {
            get { return commonSettings.GetBool("UseServiceModelAttribute"); }
            set { commonSettings.SetBool("UseServiceModelAttribute", value); }
        }


        /// <summary>
        /// ServiceModelType - standart or specific.
        /// true - Is standart ServiceModel/ false - Specific Service Model - like HierarchyServiceModel
        /// </summary>
        public bool ServiceModelType
        {
            get { return commonSettings.GetBool(nameof(ServiceModelType)); }
            set { commonSettings.SetBool(nameof(ServiceModelType), value); }
        }


        // 3 Имя сервиса 
        //  Имя сервиса    ( обычно равно имени файла шаблона )
        public string CurrentServiceName
        {
            get { return commonSettings.GetString("CurrentServiceName"); }
            set { commonSettings.SetString("CurrentServiceName", value); }
        }

        public List<string> ServiceTargetEntityName
        {
            get { return commonSettings.GetList<String>("ServiceTargetEntityName"); }
            set { commonSettings.SetList<String>("ServiceTargetEntityName", value); }
        }


      


        // -----------   BObject Model SETTINGS

        #endregion -------------- ASPECT SETTINGS ------------------

    }

}
