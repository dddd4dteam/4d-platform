﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.IO;
using System.Xml.Serialization;

namespace PL.Generators.Data
{
    /// <summary>
    /// Перечень всех зарегистрирванных менеджеров моделей доменных сервисов в системе
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "ServiceModelManager_RegisterEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum ServiceModelManager_RegisterEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        Shell,        

        /// <remarks/>
        [EnumMember()]
        S1_Geography,

        /// <remarks/>
        [EnumMember()]
        S2_Assortment,

        /// <remarks/>
        [EnumMember()]
        S3_GoodsCategory,

        /// <remarks/>
        [EnumMember()]
        S4_Material,

        /// <remarks/>
        [EnumMember()]
        S5_Rival,

        /// <remarks/>
        [EnumMember()]
        S6_Diler,

        /// <remarks/>
        [EnumMember()]
        S7_Holding,

        /// <remarks/>
        [EnumMember()]
        S8_Client,

        /// <remarks/>
        [EnumMember()]
        S9_Meeting,

        /// <remarks/>
        [EnumMember()]
        S10_EkspositorOrder,

        /// <remarks/>
        [EnumMember()]
        S11_Report,

        /// <remarks/>
        [EnumMember()]
        S12_Units,

        /// <remarks/>
        [EnumMember()]
        S13_Grupa,

        /// <remarks/>
        [EnumMember()]
        S14_Product,

        /// <remarks/>
        [EnumMember()]
        S15_Monitoring,

        /// <remarks/>
        [EnumMember()]
        S16_ClientTypology2,

        /// <remarks/>
        [EnumMember()]
        S100_System,

        /// <remarks/>
        [EnumMember()]
        S17_Questionary
    }

    /// <summary>
    /// Перечень всех зарегистрирванных менеджеров комманд в системе
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "CommandManager_RegisterEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum CommandManager_RegisterEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        CRUDCommandManager,

        /// <remarks/>
        [EnumMember()]
        HierarchyCommandManager,

        /// <remarks/>
        [EnumMember()]
        IdentificationCommandManager,

        /// <remarks/>
        [EnumMember()]
        CoreCommandManager,

        /// <remarks/>
        [EnumMember()]
        SsisCommandManager,

        /// <remarks/>
        [EnumMember()]
        SyncCommandManager
    }

    /// <summary>
    /// Перечень всех зарегистрирванных менеджеров Модулей в системе - на клиенте
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "ModuleManager_RegisterEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum ModuleManager_RegisterEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        Clients,

        /// <remarks/>
        [EnumMember()]
        Meetings,

        /// <remarks/>
        [EnumMember()]
        Monitoring,
    }

    /// <summary>
    /// Перечень всех зарегистрирванных инфраструктурных сервисов
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "InfrastructureService_RegisterEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum InfrastructureService_RegisterEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        Configuration,

        /// <remarks/>
        [EnumMember()]
        BoModel,

        /// <remarks/>
        [EnumMember()]
        Commanding,

        /// <remarks/>
        [EnumMember()]
        Identification,

        /// <remarks/>
        [EnumMember()]
        Localization,

        /// <remarks/>
        [EnumMember()]
        Modularity,

        /// <remarks/>
        [EnumMember()]
        Navigation,

        /// <remarks/>
        [EnumMember()]
        Meta,

        /// <remarks/>
        [EnumMember()]
        Query,

        /// <remarks/>
        [EnumMember()]
        Windows,
    }

    /// <summary>
    ///  Установки с которых система Загружается
    /// <summary>
    [XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [DataContractAttribute(Name = "CompositionSettingsEn", Namespace = "http://www.Systems.org/2010/Schema")]
    public enum CompositionSettingsEn
    {

        /// <remarks/>
        [EnumMember()]
        NotDefined,

        /// <remarks/>
        [EnumMember()]
        ShellControl,

        /// <remarks/>
        [EnumMember()]
        CustomerMachineName,

        /// <remarks/>
        [EnumMember()]
        CustomerWebPort,

        /// <remarks/>
        [EnumMember()]
        PlatformDir,

        /// <remarks/>
        [EnumMember()]
        PlatformAssembliesDir,

        /// <remarks/>
        [EnumMember()]
        PlatformSL4AssembliesDir,

        /// <remarks/>
        [EnumMember()]
        DomAssembliesDir,

        /// <remarks/>
        [EnumMember()]
        DomSL4AssembliesDir,

        /// <remarks/>
        [EnumMember()]
        ConfigurationDir,

        /// <remarks/>
        [EnumMember()]
        ModulesConfigurationDir,

        /// <remarks/>
        [EnumMember()]
        SystemConfigurationDir,

        /// <remarks/>
        [EnumMember()]
        ConfigProviderSettingsFile,

        /// <remarks/>
        [EnumMember()]
        SystemConfigFile,

        /// <remarks/>
        [EnumMember()]
        DomModulesDir,

        /// <remarks/>
        [EnumMember()]
        DomModulesSL4Dir,

        /// <remarks/>
        [EnumMember()]
        DomExtensionsDir,

        /// <remarks/>
        [EnumMember()]
        DomExtensionsSL4Dir,

        /// <remarks/>
        [EnumMember()]
        SystemRootDir,

        /// <remarks/>
        [EnumMember()]
        DomDalAssembly,

        /// <remarks/>
        [EnumMember()]
        DomDalSL4Assembly,

        /// <remarks/>
        [EnumMember()]
        UserName,

        /// <remarks/>
        [EnumMember()]
        CustomerApplicationFolder,

        /// <remarks/>
        [EnumMember()]
        ModuleName,

        /// <remarks/>
        [EnumMember()]
        UserPassword,
    }

    /// <summary>
    /// Информация о Композиции Системы
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Systems.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "ServiceRefContainerTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class ServiceRefContainerTp : System.ComponentModel.INotifyPropertyChanged
    {

        private List<ServiceRefTp> serviceField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// ServiceRefContainerTp class constructor
        /// </summary>
        public ServiceRefContainerTp()
        {
            this.serviceField = new List<ServiceRefTp>();
        }

        [System.Xml.Serialization.XmlElementAttribute("Service", Order = 0)]
        [DataMemberAttribute()]
        public List<ServiceRefTp> Services
        {
            get
            {
                return this.serviceField;
            }
            set
            {
                if ((this.serviceField != null))
                {
                    if ((serviceField.Equals(value) != true))
                    {
                        this.serviceField = value;
                        this.OnPropertyChanged("Services");
                    }
                }
                else
                {
                    this.serviceField = value;
                    this.OnPropertyChanged("Services");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(ServiceRefContainerTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current ServiceRefContainerTp object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an ServiceRefContainerTp object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output ServiceRefContainerTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out ServiceRefContainerTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ServiceRefContainerTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out ServiceRefContainerTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static ServiceRefContainerTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((ServiceRefContainerTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current ServiceRefContainerTp object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an ServiceRefContainerTp object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output ServiceRefContainerTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out ServiceRefContainerTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(ServiceRefContainerTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out ServiceRefContainerTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static ServiceRefContainerTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this ServiceRefContainerTp object
        /// </summary>
        public virtual ServiceRefContainerTp Clone()
        {
            return ((ServiceRefContainerTp)(this.MemberwiseClone()));
        }
        #endregion
    }

    /// <summary>
    /// Установка загрузки системы
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Systems.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "BootSettingTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class BootSettingTp : System.ComponentModel.INotifyPropertyChanged
    {

        private CompositionSettingsEn keyField;

        private string valueField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// Ключ
        /// <summary>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public CompositionSettingsEn Key
        {
            get
            {
                return this.keyField;
            }
            set
            {
                if ((keyField.Equals(value) != true))
                {
                    this.keyField = value;
                    this.OnPropertyChanged("Key");
                }
            }
        }

        [System.Xml.Serialization.XmlAttributeAttribute()]
        [DataMemberAttribute()]
        public string Value
        {
            get
            {
                return this.valueField;
            }
            set
            {
                if ((this.valueField != null))
                {
                    if ((valueField.Equals(value) != true))
                    {
                        this.valueField = value;
                        this.OnPropertyChanged("Value");
                    }
                }
                else
                {
                    this.valueField = value;
                    this.OnPropertyChanged("Value");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(BootSettingTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        public static bool Deserialize(string xml, out BootSettingTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(BootSettingTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out BootSettingTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static BootSettingTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((BootSettingTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        public static bool LoadFromFile(string fileName, out BootSettingTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(BootSettingTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out BootSettingTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static BootSettingTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        public virtual BootSettingTp Clone()
        {
            return ((BootSettingTp)(this.MemberwiseClone()));
        }
        #endregion
    }

    /// <summary>
    /// Контейнер Установок для Загрузки системы
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Systems.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "BootSettingsContainerTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class BootSettingsContainerTp : System.ComponentModel.INotifyPropertyChanged
    {

        private List<BootSettingTp> settingField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// BootSettingsContainerTp class constructor
        /// </summary>
        public BootSettingsContainerTp()
        {
            this.settingField = new List<BootSettingTp>();
        }

        [System.Xml.Serialization.XmlElementAttribute("Setting", Order = 0)]
        [DataMemberAttribute()]
        public List<BootSettingTp> Settings
        {
            get
            {
                return this.settingField;
            }
            set
            {
                if ((this.settingField != null))
                {
                    if ((settingField.Equals(value) != true))
                    {
                        this.settingField = value;
                        this.OnPropertyChanged("Settings");
                    }
                }
                else
                {
                    this.settingField = value;
                    this.OnPropertyChanged("Settings");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(BootSettingsContainerTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current BootSettingsContainerTp object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an BootSettingsContainerTp object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output BootSettingsContainerTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out BootSettingsContainerTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(BootSettingsContainerTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out BootSettingsContainerTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static BootSettingsContainerTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((BootSettingsContainerTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current BootSettingsContainerTp object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an BootSettingsContainerTp object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output BootSettingsContainerTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out BootSettingsContainerTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(BootSettingsContainerTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out BootSettingsContainerTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static BootSettingsContainerTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this BootSettingsContainerTp object
        /// </summary>
        public virtual BootSettingsContainerTp Clone()
        {
            return ((BootSettingsContainerTp)(this.MemberwiseClone()));
        }
        #endregion
    }

    /// <summary>
    /// Справочник по всем Менеджерам моделей Сервиов/ по всем Менеджерам команд /   по всем Модулям зарегистрированным в системе  --- НО Здесь Указываются ТОЛЬКО КЛЮЧИ к каждому из элементов
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.Systems.org/2010/Schema", IsNullable = true)]
    [DataContractAttribute(Name = "CompositionRegisterTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class CompositionRegisterTp : System.ComponentModel.INotifyPropertyChanged
    {

        private ServiceModelManager_RegisterEn serviceModelManagersField;

        private CommandManager_RegisterEn commandManagersField;

        private ModuleManager_RegisterEn moduleManagersField;

        private InfrastructureService_RegisterEn insrastructureServiceField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        [DataMemberAttribute()]
        public ServiceModelManager_RegisterEn ServiceModelManagers
        {
            get
            {
                return this.serviceModelManagersField;
            }
            set
            {
                if ((serviceModelManagersField.Equals(value) != true))
                {
                    this.serviceModelManagersField = value;
                    this.OnPropertyChanged("ServiceModelManagers");
                }
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        [DataMemberAttribute()]
        public CommandManager_RegisterEn CommandManagers
        {
            get
            {
                return this.commandManagersField;
            }
            set
            {
                if ((commandManagersField.Equals(value) != true))
                {
                    this.commandManagersField = value;
                    this.OnPropertyChanged("CommandManagers");
                }
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        [DataMemberAttribute()]
        public ModuleManager_RegisterEn ModuleManagers
        {
            get
            {
                return this.moduleManagersField;
            }
            set
            {
                if ((moduleManagersField.Equals(value) != true))
                {
                    this.moduleManagersField = value;
                    this.OnPropertyChanged("ModuleManagers");
                }
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 3)]
        [DataMemberAttribute()]
        public InfrastructureService_RegisterEn InsrastructureService
        {
            get
            {
                return this.insrastructureServiceField;
            }
            set
            {
                if ((insrastructureServiceField.Equals(value) != true))
                {
                    this.insrastructureServiceField = value;
                    this.OnPropertyChanged("InsrastructureService");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(CompositionRegisterTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current CompositionRegisterTp object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an CompositionRegisterTp object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output CompositionRegisterTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out CompositionRegisterTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CompositionRegisterTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out CompositionRegisterTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static CompositionRegisterTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((CompositionRegisterTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current CompositionRegisterTp object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an CompositionRegisterTp object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output CompositionRegisterTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out CompositionRegisterTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(CompositionRegisterTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out CompositionRegisterTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static CompositionRegisterTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this CompositionRegisterTp object
        /// </summary>
        public virtual CompositionRegisterTp Clone()
        {
            return ((CompositionRegisterTp)(this.MemberwiseClone()));
        }
        #endregion
    }

    /// <summary>
    /// Перечень всех зарегистрированных в системе составялющих  её элементов
    /// <summary>
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.Systems.org/2010/Schema")]
    [System.Xml.Serialization.XmlRootAttribute("SystemCompositionElements", Namespace = "http://www.Systems.org/2010/Schema", IsNullable = false)]
    [DataContractAttribute(Name = "SystemCompositionElementsTp", Namespace = "http://www.Systems.org/2010/Schema")]
    public partial class SystemCompositionElementsTp : System.ComponentModel.INotifyPropertyChanged
    {

        private CompositionRegisterTp compositionRegisterField;

        private ServiceRefContainerTp serviceRefContainerField;

        private BootSettingsContainerTp settingsField;

        private static System.Xml.Serialization.XmlSerializer serializer;

        /// <summary>
        /// SystemCompositionElementsTp class constructor
        /// </summary>
        public SystemCompositionElementsTp()
        {
            this.settingsField = new BootSettingsContainerTp();
            this.serviceRefContainerField = new ServiceRefContainerTp();
            this.compositionRegisterField = new CompositionRegisterTp();
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 0)]
        [DataMemberAttribute()]
        public CompositionRegisterTp CompositionRegister
        {
            get
            {
                return this.compositionRegisterField;
            }
            set
            {
                if ((this.compositionRegisterField != null))
                {
                    if ((compositionRegisterField.Equals(value) != true))
                    {
                        this.compositionRegisterField = value;
                        this.OnPropertyChanged("CompositionRegister");
                    }
                }
                else
                {
                    this.compositionRegisterField = value;
                    this.OnPropertyChanged("CompositionRegister");
                }
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 1)]
        [DataMemberAttribute()]
        public ServiceRefContainerTp ServiceRefContainer
        {
            get
            {
                return this.serviceRefContainerField;
            }
            set
            {
                if ((this.serviceRefContainerField != null))
                {
                    if ((serviceRefContainerField.Equals(value) != true))
                    {
                        this.serviceRefContainerField = value;
                        this.OnPropertyChanged("ServiceRefContainer");
                    }
                }
                else
                {
                    this.serviceRefContainerField = value;
                    this.OnPropertyChanged("ServiceRefContainer");
                }
            }
        }

        [System.Xml.Serialization.XmlElementAttribute(Order = 2)]
        [DataMemberAttribute()]
        public BootSettingsContainerTp Settings
        {
            get
            {
                return this.settingsField;
            }
            set
            {
                if ((this.settingsField != null))
                {
                    if ((settingsField.Equals(value) != true))
                    {
                        this.settingsField = value;
                        this.OnPropertyChanged("Settings");
                    }
                }
                else
                {
                    this.settingsField = value;
                    this.OnPropertyChanged("Settings");
                }
            }
        }

        private static System.Xml.Serialization.XmlSerializer Serializer
        {
            get
            {
                if ((serializer == null))
                {
                    serializer = new System.Xml.Serialization.XmlSerializer(typeof(SystemCompositionElementsTp));
                }
                return serializer;
            }
        }

        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string propertyName)
        {
            System.ComponentModel.PropertyChangedEventHandler handler = this.PropertyChanged;
            if ((handler != null))
            {
                handler(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        #region Serialize/Deserialize
        /// <summary>
        /// Serializes current SystemCompositionElementsTp object into an XML document
        /// </summary>
        /// <returns>string XML value</returns>
        public virtual string Serialize()
        {
            System.IO.StreamReader streamReader = null;
            System.IO.MemoryStream memoryStream = null;
            try
            {
                memoryStream = new System.IO.MemoryStream();
                Serializer.Serialize(memoryStream, this);
                memoryStream.Seek(0, System.IO.SeekOrigin.Begin);
                streamReader = new System.IO.StreamReader(memoryStream);
                return streamReader.ReadToEnd();
            }
            finally
            {
                if ((streamReader != null))
                {
                    streamReader.Dispose();
                }
                if ((memoryStream != null))
                {
                    memoryStream.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes workflow markup into an SystemCompositionElementsTp object
        /// </summary>
        /// <param name="xml">string workflow markup to deserialize</param>
        /// <param name="obj">Output SystemCompositionElementsTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool Deserialize(string xml, out SystemCompositionElementsTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(SystemCompositionElementsTp);
            try
            {
                obj = Deserialize(xml);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool Deserialize(string xml, out SystemCompositionElementsTp obj)
        {
            System.Exception exception = null;
            return Deserialize(xml, out obj, out exception);
        }

        public static SystemCompositionElementsTp Deserialize(string xml)
        {
            System.IO.StringReader stringReader = null;
            try
            {
                stringReader = new System.IO.StringReader(xml);
                return ((SystemCompositionElementsTp)(Serializer.Deserialize(System.Xml.XmlReader.Create(stringReader))));
            }
            finally
            {
                if ((stringReader != null))
                {
                    stringReader.Dispose();
                }
            }
        }

        /// <summary>
        /// Serializes current SystemCompositionElementsTp object into file
        /// </summary>
        /// <param name="fileName">full path of outupt xml file</param>
        /// <param name="exception">output Exception value if failed</param>
        /// <returns>true if can serialize and save into file; otherwise, false</returns>
        public virtual bool SaveToFile(string fileName, out System.Exception exception)
        {
            exception = null;
            try
            {
                SaveToFile(fileName);
                return true;
            }
            catch (System.Exception e)
            {
                exception = e;
                return false;
            }
        }

        public virtual void SaveToFile(string fileName)
        {
            System.IO.StreamWriter streamWriter = null;
            try
            {
                string xmlString = Serialize();
                System.IO.FileInfo xmlFile = new System.IO.FileInfo(fileName);
                streamWriter = xmlFile.CreateText();
                streamWriter.WriteLine(xmlString);
                streamWriter.Close();
            }
            finally
            {
                if ((streamWriter != null))
                {
                    streamWriter.Dispose();
                }
            }
        }

        /// <summary>
        /// Deserializes xml markup from file into an SystemCompositionElementsTp object
        /// </summary>
        /// <param name="fileName">string xml file to load and deserialize</param>
        /// <param name="obj">Output SystemCompositionElementsTp object</param>
        /// <param name="exception">output Exception value if deserialize failed</param>
        /// <returns>true if this XmlSerializer can deserialize the object; otherwise, false</returns>
        public static bool LoadFromFile(string fileName, out SystemCompositionElementsTp obj, out System.Exception exception)
        {
            exception = null;
            obj = default(SystemCompositionElementsTp);
            try
            {
                obj = LoadFromFile(fileName);
                return true;
            }
            catch (System.Exception ex)
            {
                exception = ex;
                return false;
            }
        }

        public static bool LoadFromFile(string fileName, out SystemCompositionElementsTp obj)
        {
            System.Exception exception = null;
            return LoadFromFile(fileName, out obj, out exception);
        }

        public static SystemCompositionElementsTp LoadFromFile(string fileName)
        {
            System.IO.FileStream file = null;
            System.IO.StreamReader sr = null;
            try
            {
                file = new System.IO.FileStream(fileName, FileMode.Open, FileAccess.Read);
                sr = new System.IO.StreamReader(file);
                string xmlString = sr.ReadToEnd();
                sr.Close();
                file.Close();
                return Deserialize(xmlString);
            }
            finally
            {
                if ((file != null))
                {
                    file.Dispose();
                }
                if ((sr != null))
                {
                    sr.Dispose();
                }
            }
        }
        #endregion

        #region Clone method
        /// <summary>
        /// Create a clone of this SystemCompositionElementsTp object
        /// </summary>
        public virtual SystemCompositionElementsTp Clone()
        {
            return ((SystemCompositionElementsTp)(this.MemberwiseClone()));
        }
        #endregion
    }
}
