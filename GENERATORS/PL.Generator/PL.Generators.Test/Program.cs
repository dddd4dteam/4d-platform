﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PL.Generators.Data;
using PL.Generators.Model;

namespace PL.Gen.Test
{
    class Program
    {
        const string NL = "\r\n";
        static void Main(string[] args)
        {
            TestTextTransformation gen = new TestTextTransformation();
            string s = gen.TransformText();
        }

        public class  TestTextTransformation :  DataLayerGenerator
        {
            /// <summary>
            /// Create the template output
            /// </summary>
            public override string TransformText()
            {
                try
                {
                    this.Write(NL);

                    this.TStg.InitAspects();
                    this.TStg.SetDefaultsSettingsByScenario(ScenariosEn.ClientSettingsScenario,
                    //"Data Source=LC-OLAPTEST;Initial Catalog=CRM_Cersanit2;User ID=sa;Password=cersanit");
                    //"Data Source=A3;Integrated Security=True");
                    "Data Source=A3;Initial Catalog=CRM_Cersanit2_test3;Integrated Security=True");


                    this.GenerateModel(1, "");
                }
                catch (System.Exception e)
                {
                    e.Data["TextTemplatingProgress"] = this.GenerationEnvironment.ToString();
                    throw new System.Exception("Template runtime error", e);
                }
                return this.GenerationEnvironment.ToString();
            }
        }
    }
}
