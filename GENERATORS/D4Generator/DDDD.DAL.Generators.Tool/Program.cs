﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

 
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators.Tests
{
    class Program
    {
        const string StgExt = "jstg";
        const string StgSearchPtrn = "*.jstg";

        static void Main(string[] args)
        {
            try
            {
                // Initialize generator default Settings and set/change default Components:
                // ICodeWriter | IMetaDataLoader | IResourceBuilder | IRender
                var generator = DALGenerator.Current;

                
                generator.Generate(args);

                //generator.GRunner.WriteDefaultSettings(isSpecificSvcModel:true);
                //generator.GRunner.WriteDefaultSettings(isSpecificSvcModel: false);
                //generator.GRunner.WriteTestDBServiceModels();


            }
            catch (Exception exc)
            {
                 throw exc;
            }             
        }
    }
}
