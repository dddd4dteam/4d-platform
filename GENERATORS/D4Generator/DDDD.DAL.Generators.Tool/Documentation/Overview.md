﻿# DDDD.DAL.Generators.Tool overview

## Base folder structure to use generator tool :
    
    1 D4GeneratorTool - wuth generator itself    
    2 ServiceModels  - genrated code  files 
        D4DALCSRender  -  Folder  of Template Render 
            SM1..Name   - ServiceModel folder with  code files  
            ...
    3 Settings - settings files per Service Model
        S1_SMRunSettings.jstg - setting file of SM 1
    4 SystemDesc - List  of all Service Models with Name ID Description
        SystemDescription.jcpmd - system Description file
   
## Runing generator - arguments
    
    We need to set only ono argument - list of ids of Service models.
    Example in cmdlne in shell:  
        DDDD.DAL.Generators.Tool "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16" 
    - Here we say - Generate for Service Models with IDs from 1 to 16.
    
    Note: So we can create several shell  script files to generate each service model separately.


## Naming Conventions for Table and Views 
    
    Base terminls 
        [Data Register]
        [Value object]
        [Fact table]
    
    
    During develpment we need to follow hardly by theese
    naming conventions - i.e. when creating new DB Item:            
    
    // -------------BO NAMING ------------------------- 
    // NAMING  PARTS:
    // Name:  [1]_[2]_[3]_[4] or [1]_[2]_[3]
    // [1] - [Domainn Prefix] with  'V' as first symbol or not: if View or Table instead.
    // [2] - [Service Model Index] - abstract part of Whole Model - Service Model and its Index marker.
    //           For eacample : Sxxs(for Eaxample S1 or S23)
    // [3] - [Concept Entity Name]
    // [4] - [Register/Vo] keywords : if Table/View is Data Register, or if View is Value Object.

    // Data Register is seldom updated list.It ususaaly belongs to some concept object Kind Tables
    // Value object - mapped View that can convert on time its value to mapped table value. Its usually concept object info  and all of its Kind info from 1:1 associated tables.

    // Naming Cases / Examples:
    // TABLE: 
    //   BO of TABLE naming rules EX: [crs_S1_GeographyHierarchy] - Facts, [crs_S1_GeographyItemType_Register] - register 
    // VIEW :
    //   BO of View naming rules EX:  [Vcrs_S1_GeographyItem_Vo] - Valuw Object ,[Vcrs_S1_GeographyItemType_Register]- register, [Vcrs_S1_GeographyAreas]
    //                                [PV_S9_Meeting_Vo] or [VP_S9_Meeting_Vo]- partial View
    //                                 
    // Obsolete style:  V without DomainPrefix - [crs] - its error for today
    // [V_S1_GeographyItem_Vo] - value Object View  -- obsolete - do not use  it

    

## How it works Logic   
    
    Base Termins:
        [Generator]- 
        [SystemDescFile]
        [SMSettingFile] 
        [Render]
        [MetaDataLoader]
        [RenderTemplate]
        [CodeWriter]
        
                   
    Logic Sequential:  
        1  [Generator] load  [SystemDescFile] with 
    service Models ID/Name/descriptions.
        2  Load all SM Settings files from Settings folder
        3  Parse SM IDs from commmand line 
    compare if each SM ID exists in [SystemDescFile],
    and generate SM code by SM settings from file of step 2.   
    
    
    
       