﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using DDDD.DAL.Generators.Meta; 
using DDDD.DAL.Generators.Reflection;
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators.Settings
{


    /// <summary>
    /// Setting Element in Json format config  
    /// </summary>
    public class SettingJElm
    {

        /// <summary>
        /// Create new Setting Item 
        /// </summary>
        /// <param name="stgKey"></param>
        /// <param name="description"></param>
        /// <param name="valueType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static SettingJElm New(
             AllSettingsEn stgKey
            , List<string> description
            , StgValTypeEn valueType
            , object value
            )
        {
            var nsetting = new SettingJElm()
            {
                StgKey = stgKey
                               ,
                Description = description
                               ,
                ValueType = valueType
                               ,
                Value = value
            };
            return nsetting;
        }

        /// <summary>
        /// Setting Key from  AllSettingsEn 
        /// - one of property of Aspect Setting- look in Description.
        /// </summary>
        public AllSettingsEn StgKey
        { get; set; }

        /// <summary>
        /// Setting Description
        /// </summary>
         public List<string> Description
        { get; set; } = new List<string>();

        /// <summary>
        ///  Value  can be of : StringTp/IntTp/BoolTp/ListIntTp/ListStringTp
        /// </summary>
        public StgValTypeEn ValueType
        { get; set; }

        /// <summary>
        /// Setting value
        /// </summary>
        public object Value
        { get; set; }

        public string GetValAsString()
        {
            return Value as string;
        }

        public int GetValAsInt()
        {
            return (int)Value;
        }
        public bool GetValAsBool()
        {
            return (bool)Value;
        }


        public  string  GetJSValAsStr()
        {
            var reslt = "";
            if ((Value is JsonElement) == false) { return reslt; }

            if ( ((JsonElement)Value).ValueKind == JsonValueKind.String )
            { return ((JsonElement)Value).GetString(); }

            return reslt;
        }

        public int GetJSValAsInt()
        {
            var reslt = -1;            
            if ((Value is JsonElement) == false) { return reslt; }

            if ( ((JsonElement)Value).ValueKind == JsonValueKind.Number )
            { return ((JsonElement)Value).GetInt32(); }

            return reslt;
        }

        public bool GetJSValAsBool()
        {
            var reslt = false;
            if ( ( Value is JsonElement) == false) { return reslt; }
           
            if (    ((JsonElement)Value).ValueKind == JsonValueKind.False
                 || ((JsonElement)Value).ValueKind == JsonValueKind.True
                )
            { return ((JsonElement)Value).GetBoolean(); }

            return reslt;
        }


        public List<string> GetJSValAsListStr()
        {
            var reslt = new List<string>();
            if ((Value is JsonElement) == false) 
            { return reslt; }

            if ( ((JsonElement)Value).ValueKind != JsonValueKind.Array  )
            {   return reslt;     }

            foreach (var item in ((JsonElement)Value).EnumerateArray())
            {
                reslt.Add(item.GetString()); 
            }
            return reslt;
        }

        public List<int> GetJSValAsListInt()
        {
            var reslt = new List<int>();
            if ((Value is JsonElement) == false)
            { return reslt; }

            if (((JsonElement)Value).ValueKind != JsonValueKind.Array)
            { return reslt; }

            foreach (var item in ((JsonElement)Value).EnumerateArray())
            {
                reslt.Add(item.GetInt32());
            }
            return reslt;
        }

        public List<bool> GetJSValAsListBool()
        {
            var reslt = new List<bool>();
            if ((Value is JsonElement) == false)
            { return reslt; }

            if (((JsonElement)Value).ValueKind != JsonValueKind.Array)
            { return reslt; }

            foreach (var item in ((JsonElement)Value).EnumerateArray())
            {
                reslt.Add(item.GetBoolean());
            }
            return reslt;
        }

        public void ReinitValueFromJson()
        {
            // var 
            if ( ValueType == StgValTypeEn.BoolTp ) 
            { Value = GetJSValAsBool();  }
            if ( ValueType == StgValTypeEn.IntTp )
            { Value = GetJSValAsInt(); }
            if (ValueType == StgValTypeEn.StringTp )
            { Value = GetJSValAsStr(); }
            if (ValueType == StgValTypeEn.ListBoolTp )
            { Value = GetJSValAsListBool(); }
            if (ValueType == StgValTypeEn.ListIntTp )
            { Value = GetJSValAsListInt(); }
            if (ValueType == StgValTypeEn.ListStringTp )
            { Value = GetJSValAsListStr(); }
        }
        public Type DotNetType()
        {
            if (Value != null && Value is JsonElement)
            {// Json  value 
                var jsValue = (JsonElement)Value;
                if (jsValue.ValueKind == JsonValueKind.String)
                { return Tps.T_string; }
                if (jsValue.ValueKind == JsonValueKind.Array)
                { return typeof(List<string>); }
                if (jsValue.ValueKind == JsonValueKind.False
                    || jsValue.ValueKind == JsonValueKind.True)
                { return  Tps.T_bool; }
                if (jsValue.ValueKind == JsonValueKind.Number )
                { return Tps.T_decimal; }
                if (jsValue.ValueKind == JsonValueKind. Null)
                { return Tps.T_object; } 

            }
            else if (Value != null && (Value is JsonElement) == false)
            {// non Json  value - .NET Type
                var valType = Value.GetType();
                return valType;
            }
            if (Value == null)
            {
                return ValueType.ToDotNetType();
            }
            return Tps.T_object;
        }        
    }


    /// <summary>
    /// One  Generator Run  Settings  in Json format config.
    /// Run Iteration  consist of  [SettingJElm] setting elements.
    /// </summary>
    public class RunIterationJElm
    {
        /// <summary>
        /// Run iteration ID
        /// </summary>
        public int ID
        { get; set; }

        /// <summary>
        /// Run iteration  Description - for what Service Model
        /// </summary>
        public string Description
        { get; set; }

        /// <summary>
        /// RunIteration Settings
        /// </summary>
        public List<SettingJElm> RunSettings
        { get; set; } = new List<SettingJElm>();


        public void AddItem(AllSettingsEn stgKey
                                , List<string> description
                                , StgValTypeEn valueType
                                , object value
                            )
        {
            RunSettings.Add(
                new SettingJElm()
                {
                    StgKey = stgKey
                  ,
                    Description = description
                  ,
                    ValueType = valueType
                  ,
                    Value = value
                }
                );

        }


    }

}
