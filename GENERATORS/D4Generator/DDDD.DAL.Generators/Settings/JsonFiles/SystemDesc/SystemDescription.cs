﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDD.DAL.Generators.Settings
{

    
    public class  ServiceModelJInf
    { 
        /// <summary>
        /// Service Model Name
        /// </summary>
        public string Name
        { get; set; }

        /// <summary>
        /// Service model ID
        /// </summary>
        public int ID
        { get; set; }

        /// <summary>
        ///  if Service model Standart- false(by default) or Specific - true
        /// </summary>
        public bool IsSpecificModelType
        { get; set; } = false;

        /// <summary>
        /// Service Model Description as multiline string.
        /// </summary>
        public List<string> Description
        { get; set; } = new List<string>();

    }

    /// <summary>
    /// System Description:  
    /// <para/> ServiceModels- Data Models of System Services
    /// </summary>
    public class SystemDescriptionJInf
    {

        /// <summary>
        /// Service Models Descriptions
        /// </summary>
        public List<ServiceModelJInf> ServiceModels
        { get; set; } = new List<ServiceModelJInf>();



        public void AddSvcModel(int id , string name 
                                , bool isSpecificModelType=false
                                ,params string[] description)
        {

            ServiceModels.Add
                (
                 new ServiceModelJInf()
                 {
                     ID = id
                     ,Name = name
                     ,IsSpecificModelType = isSpecificModelType
                     ,Description =  description.ToList()
                 }
                );
           
        }
    }
}
