﻿
using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using DDDD.DAL.Generators.Meta; 
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators.Settings
{

    public class StgKeyJsonConverter : JsonConverter<AllSettingsEn>
    {
        public override AllSettingsEn Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var valEnStr = reader.GetString();
            var enVal =  (AllSettingsEn)Enum.Parse(typeToConvert, valEnStr);
            return enVal;
        }

        public override void Write(Utf8JsonWriter writer, AllSettingsEn value, JsonSerializerOptions options)
        {
            var valEnStr = value.S();
            writer.WriteStringValue(valEnStr);             
        }
    }

    public class StgValTypeJsonConverter : JsonConverter<StgValTypeEn>
    {
        public override StgValTypeEn Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var valEnStr = reader.GetString();
            var enVal = (StgValTypeEn)Enum.Parse(typeToConvert, valEnStr);
            return enVal;
        }

        public override void Write(Utf8JsonWriter writer, StgValTypeEn value, JsonSerializerOptions options)
        {
            var valEnStr = value.S();
            writer.WriteStringValue(valEnStr);
        }
    }

}
