﻿using DDDD.DAL.Generators.Meta;
 
using System;

namespace DDDD.DAL.Generators.Settings
{

    /// <summary>
    /// Контейнер установок для Аспекта 
    /// </summary>
    public abstract class AspectSettingsContainer
    {        
       


        /// <summary>
        /// Контейнер установок 
        /// </summary>
        /// <param name="CurrentCommonSettings"></param>
        public AspectSettingsContainer(Enum aspect, GeneratorSettingsContainer CurrentCommonSettings  ) 
        {
            commonSettings = CurrentCommonSettings;
            this.Aspect = aspect;
           
            //Добавить Контейнер Аспекта установок в общее хранилище
            //commonSettings.RegisterAspect( this );
        }


        internal void Register()
        {
            try
            {
                commonSettings.RegisterAspect( this );
            }
            catch (Exception )
            {   throw  new Exception("Cant register Aspect");            }
        }

        /// <summary>
        /// Связь с другими сеттингами через общий контейнер для утановок
        /// </summary>
       protected GeneratorSettingsContainer commonSettings = null;

       /// <summary>
       /// Аспект - но он не может быть NotDefined
       /// </summary> 
       internal Enum Aspect = AspectEn.NotDefined;

    }
}
