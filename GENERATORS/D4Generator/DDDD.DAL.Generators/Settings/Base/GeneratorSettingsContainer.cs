﻿using DDDD.DAL.Generators.Meta;
 
using System;
using System.Collections.Generic;

namespace DDDD.DAL.Generators.Settings
{


    /// <summary>
    /// Контейнер настроек Шаблона Генерации
    /// </summary>
    public abstract class GeneratorSettingsContainer
    {



        /// <summary>
        /// Ctor GeneratorSettingsContainer 
        /// </summary>
        public GeneratorSettingsContainer() { }


        /// <summary>
        /// Ctor GeneratorSettingsContainer
        /// </summary>
        /// <param name="CurrentAspectSettings"></param>
        public GeneratorSettingsContainer(Dictionary<String, AspectSettingsContainer> CurrentAspectSettings)
        { Aspects = CurrentAspectSettings; }


        /// <summary>
        /// Generation Aspect Settings         
        /// </summary>
        Dictionary<String, AspectSettingsContainer> Aspects
        { get; set; } = new Dictionary<string, AspectSettingsContainer>();


        /// <summary>
        /// All Generation  settings - for all aspects in one Sorted List
        /// </summary>
        SortedList<AllSettingsEn, SettingJElm> AllSettings
        { get; set; } = new SortedList<AllSettingsEn, SettingJElm>();
         
        public SettingJElm this[AllSettingsEn key]
        {
            get { return AllSettings[key];  }
            set { AllSettings[key] = value; }
        }


        #region ---------------Get/Set/ Compare/ Contain Settings Operations------------------


        #region -------------  Get Operations --------------------

        /// <summary>
        /// Получить T  значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="settingKey"></param>
        public T GetItem<T>(AllSettingsEn  settingKey) 
        {
            try
            {
                //var stgNetType = AllSettings[settingKey].DotNetType();
                //if (stgNetType == typeof(T))
                    return (T)AllSettings[settingKey].Value;
                //else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetItem<T>(String) SettingItem with Key " + settingKey + ".");
            }
        }
         




        /// <summary>
        /// Получить сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public String GetString(AllSettingsEn settingKey )
        {
            try
            {
                var stgNetType = AllSettings[settingKey].DotNetType();
                if (stgNetType == typeof(String))                    
                    return (String)AllSettings[settingKey].Value;
                else throw new Exception();
            }
            catch (Exception )
            {
                throw new Exception("GetString(String) SettingItem with Key " + settingKey + ".");  
            }        
        }
         

        /// <summary>
        /// Получить булевое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public bool GetBool(AllSettingsEn SettingKey)
        {
            try
            {
                var stgNetType = AllSettings[SettingKey ].DotNetType();
                if (stgNetType ==  typeof(bool))
                    return (bool)AllSettings[SettingKey].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetBool(String) SettingItem with Key " + SettingKey + ".");  
            }
        }
         

        /// <summary>
        /// Получить список строк  у значение установки. Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<String> GetList(AllSettingsEn SettingKey)
        {
            try
            {
                var stgNetType = AllSettings[SettingKey].DotNetType();
                if (stgNetType ==  typeof(List<String>))
                    return (List<String>)AllSettings[SettingKey].Value;
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList(String) SettingItem with Key " + SettingKey + ".");
            }
        }

         

        /// <summary>
        /// Получить список T  значений у  установки. Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public List<T> GetList<T>(AllSettingsEn SettingKey) 
        {
            try
            {
                //var stgNetType = AllSettings[SettingKey].DotNetType();
                //if (stgNetType == typeof(List<T>))  
                    return (List<T>)AllSettings[SettingKey].Value;
                //else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetList<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }

         

        #endregion -------------  Get Operations --------------------
        

        #region ------------ Set Operations ---------------------

        /// <summary>
        /// Задать T значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetItem<T>(AllSettingsEn SettingKey, string NewValue)
        {
            try
            {
                //var stgNetType = AllSettings[SettingKey].DotNetType();
                //if (stgNetType == typeof(T)) 
                    AllSettings[SettingKey].Value = NewValue;
                //else { throw new Exception(); }

            }
            catch (Exception)
            {
                throw new Exception("SetItem<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }

        public void SetItem<T>(AllSettingsEn SettingKey, T newValue)
        {
            try
            {
                //var stgNetType = AllSettings[SettingKey].DotNetType();
                //if (stgNetType == typeof(T))
                    AllSettings[SettingKey].Value = newValue;
                //else { throw new Exception(); }
            }
            catch (Exception)
            {
                throw new Exception("SetItem<T>(String) SettingItem with Key " + SettingKey + ".");
            }
        }
         

        
        /// <summary>
        /// Задать сроковое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetString(AllSettingsEn SettingKey,String NewValue)
        {
            try
            {
                var stgNetType = AllSettings[SettingKey ].DotNetType();
                if (stgNetType == typeof(string) )                   
                   AllSettings[SettingKey].Value = NewValue;
              else { throw new Exception(); }
              
            }
            catch (Exception)
            {
                throw new Exception("SetString(String) SettingItem with Key " + SettingKey + ".");
            }
        }

         

        /// <summary>
        /// Задать булевое значение установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetBool(AllSettingsEn SettingKey, bool NewValue)
        {
            try
            {
                var stgNetType = AllSettings[SettingKey].DotNetType();
                if (stgNetType == typeof(bool)) 
                    AllSettings[SettingKey].Value = NewValue;
                else { throw new Exception(); } 
            }
            catch (Exception)
            {
                throw new Exception("SetBool(String) SettingItem with Key " + SettingKey + ".");
            }
        }

         

        /// <summary>
        /// Задать список значений T для установки.Зарегистрированное значение установки через один из аспектов. По строковому ключу  
        /// </summary>
        /// <param name="SettingKey"></param>
        public void SetList<T>(AllSettingsEn settingKey, List<T> NewValue)
        {
            try
            {
                //var stgNetType = AllSettings[settingKey].DotNetType();
                //if (stgNetType == typeof(List<T>))                      
                    AllSettings[settingKey].Value = NewValue;
                //else { throw new Exception(); }
            }
            catch (Exception)
            {
                throw new Exception("SetList<T>(String) SettingItem with Key " + settingKey + ".");
            }
        }
         

        #endregion ------------ Set Operations ---------------------
        

        #region ---------------- Compare Operations ----------------------


        /// <summary>
        /// Сравнить строковые значения
        /// </summary>
        public bool CompareString(AllSettingsEn SettingKey, String YourValue)
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)AllSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("CompareString(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }

                
                return  (YourValue == settingValue);

                //return false;
            }
            catch (Exception exc)
            {
                throw new Exception("CompareString(String) SettingItem with Key " + SettingKey + ".");  
            }
        }
 
        

        /// <summary>
        /// Сравнить булевые значения
        /// </summary>
        public bool CompareBool(AllSettingsEn SettingKey, bool YourValue)
        {
            try
            {
                bool settingValue;

                try
                {
                    settingValue = (bool)AllSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("CompareBool(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return (YourValue == settingValue);     
            }
            catch (Exception)
            {
                throw new Exception("CompareBool(String) SettingItem with Key " + SettingKey + ".");  
            }            
        }


         

        #endregion ---------------- Compare Operations ----------------------


        #region ------------- Contains Operations --------------------
        /// <summary>
        /// Содержание строки
        /// </summary>
        /// <param name="SettingKey"></param>
        /// <param name="ContainWhat"></param>
        public bool Contains(AllSettingsEn SettingKey, String ContainWhat  )
        {
            try
            {
                string settingValue;

                try
                {
                    settingValue = (String)AllSettings[SettingKey].Value;
                }
                catch (Exception)
                { throw new Exception("Contains(String) SettingItem with Key " + SettingKey + ". Type Conversion Error"); }


                return settingValue.Contains(ContainWhat);  
            }
            catch (Exception)
            {
                throw new Exception("Contains(String) SettingItem with Key " + SettingKey + ".");  
            }
        }
 

        #endregion ------------- Contains Operations --------------------


        #endregion --------------- Get/ Set/ Compare/ Contain  Settings Operations ------------------


        #region    ---------------- Register/Contains/Get  Aspects Operations ---------------

        #region ------------------ Contains Operations ----------------------
        /// <summary>
        /// Проверка на уже добавленный Контейнер Установок
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <returns></returns>
        internal bool ContainsAspectKey(String AspectKey)
        {
            try
            {    
                return (Aspects.ContainsKey(AspectKey));                    
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        /// <summary>
        /// Проверка на уже добавленный Контейнер Установок
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <returns></returns>
        internal bool ContainsAspectKey(Enum AspectKey)
        {
            try
            {
                return (Aspects.ContainsKey(AspectKey.ToString()));
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }

        #endregion ------------------ Contains Operations ----------------------

        #region -------------------  Register  Operations--------------------
      

        /// <summary>
        /// Регистрация Аспекта установок 
        /// </summary>
        /// <param name="AspectKey"></param>
        /// <param name="aspectContainer"></param>
        internal void RegisterAspect( AspectSettingsContainer aspectContainer)
        {
            try
            {               

                if (!Aspects.ContainsKey(aspectContainer.Aspect.ToString()))
                    Aspects.Add(aspectContainer.Aspect.ToString(), aspectContainer);
                else throw new Exception();
                                
                foreach (var PropItem in aspectContainer.GetType().GetProperties())
                {
                    var pKey = PropItem.Name.GetAllStgKey();
                    var descript = new List<string>();
                    var pValType = PropItem.PropertyType.ToStgValType();
                    AllSettings.Add(pKey, 
                        SettingJElm.New(pKey, descript, pValType, null));
                }
            }
            catch (Exception)
            {
                throw new Exception("RegisterAspect(String AspectKey) -  already exist Aspect settings with such Key: " + aspectContainer.Aspect.ToString());
            }
        }
        


        #endregion -------------------  Register  Operations--------------------

        #region ----------------   Get Aspects Container --------------

        /// <summary>
        ///  Получить Аспект установок по ключу 
        /// </summary>
        /// <param name="Aspect"></param>
        /// <returns></returns>
        public AspectSettingsContainer GetAspectByKey(String Aspect)
        {
            try
            {
                if (Aspects.ContainsKey(Aspect))
                    return Aspects[Aspect];
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetAspectByKey(String ) error. Ther is no aspect with such Key");
            }
        }

        /// <summary>
        /// Получить Аспект установок по ключу 
        /// </summary>
        /// <param name="Aspect"></param>
        /// <returns></returns>
        public AspectSettingsContainer GetAspectByKey(Enum Aspect)
        {
            try
            {
                if ( Aspects.ContainsKey(Aspect.ToString()))
                    return  Aspects[Aspect.ToString()];
                else throw new Exception();
            }
            catch (Exception)
            {
                throw new Exception("GetAspectByKey(Enum ) error. Ther is no aspect with such Key");
            }
        }

        #endregion  ----------------   Get Aspects Container --------------


        #endregion   ---------------- Register/Contains/Get  Aspects Operations ---------------




        /// <summary>
        /// Инициализация - Регистрация всех контейнеров Аспектов 
        /// </summary>
        public abstract GeneratorSettingsContainer InitAspects();


        public abstract void SetDefaultsSettingsByScenario(ScenariosEn scenario, string ConnectionString, bool GenerateResForAllViews=false, bool PerformChecksForSyncAndOffline = true);

    }


}
