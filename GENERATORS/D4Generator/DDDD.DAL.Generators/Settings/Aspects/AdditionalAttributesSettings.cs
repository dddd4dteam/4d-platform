﻿
using System;
using System.Collections.Generic;
using DDDD.DAL.Generators.Meta;

namespace DDDD.DAL.Generators.Settings

{
    /// <summary>
    /// Настройки Интерфейсов (с динамическим механизмом добавления(not released) )
    /// </summary>
    public class AdditionalAttributesSettings : AspectSettingsContainer
    {
        #region ------------Ctors-----------

        public AdditionalAttributesSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect,CurrentCommonSettings)
        {        }

        #endregion ----------Ctor---------


        #region ------------- Aspect SETTINGS----------------

        /// <summary>
        /// [AdditionalAttributesSettings]aspect, [Type:bool]   - Use-create IClonable  interface
        /// </summary>
        public bool UseIClonable //= true;
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseIClonable  ); }
            set { commonSettings.SetBool(AllSettingsEn.UseIClonable , value); }
        }

        /// <summary>
        /// [AdditionalAttributesSettings]aspect,[Type:bool]  -  Use-create IVObject  interface  
        /// </summary>
        public bool UseIVObjectRealization //= true;
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseIVObjectRealization  ); }
            set { commonSettings.SetBool( AllSettingsEn.UseIVObjectRealization , value); }
        }

        /// <summary>
        /// [AdditionalAttributesSettings]aspect, [Type:bool]  -  Use VObject mapping Attribute
        /// </summary>
        public bool UseVObjectMappingAttribute //  = true;
        {
            get { return commonSettings.GetBool( AllSettingsEn.UseVObjectMappingAttribute   ); }
            set { commonSettings.SetBool( AllSettingsEn.UseVObjectMappingAttribute , value); }
        }


        /// <summary>
        /// [AdditionalAttributesSettings]aspect, [Type:bool]  -  Use BObject as base class for model contracts  
        /// </summary>
        public bool UseBOContracts
        {
            get { return commonSettings.GetBool( AllSettingsEn.UseBOContracts  ); }
            set { commonSettings.SetBool( AllSettingsEn.UseBOContracts  , value); }
        }


        /// <summary>
        ///  [AdditionalAttributesSettings]aspect, [Type:bool]  -  Use  IHierarchicalView  interface 
        ///  for some specific contracts in Herarchical Service Model
        /// </summary>
        public bool UseIHierarchicalView //= true;
        {
            get { return commonSettings.GetBool( AllSettingsEn.UseIHierarchicalView  ); }
            set { commonSettings.SetBool( AllSettingsEn.UseIHierarchicalView , value); }
        }





        /// <summary>
        /// [AdditionalAttributesSettings]aspect,[Type:bool]  -  Use   IPersistableBO  interface -
        /// use PersistableBO and BObjectBase  base classes differentiation
        /// , not only BObjectBase for all contracts 
        /// </summary>
        public bool UseIPersistableBO
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseIPersistableBO ); }
            set { commonSettings.SetBool(AllSettingsEn.UseIPersistableBO , value); }
        }

        #endregion ------------- Aspect SETTINGS----------------

 
    }


}
