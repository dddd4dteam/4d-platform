﻿using DDDD.DAL.Generators.Meta;
 
using System;
using System.Collections.Generic;
using System.Text;

namespace DDDD.DAL.Generators.Settings
{
    /// <summary>
    /// Input and output paths of  Generated and Description files 
    /// </summary>
    public class InOutPathsSettings: AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public InOutPathsSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS ------------------

        ///// <summary>
        ///// [InOutPathsSettings]aspect,  [Type:string]  -
        ///// Input file path of System Descriotion file: 
        ///// Services /ServiceManagers  names , Indexes and Descriptions.  ...
        ///// </summary>
        //public string InPath_SystemDescription
        //{
        //    get { return commonSettings.GetString(AllSettingsEn.InPath_SystemDescription ); }
        //    set { commonSettings.SetString(AllSettingsEn.InPath_SystemDescription , value); }
        //}

        ///// <summary>
        ///// [InOutPathsSettings]aspect,  [Type:string]  -
        /////  Output Dir path for Service Model generated files: Classes file- 
        /////   table/view classes ;custom ServiceModel class; Resource file.
        ///// </summary>
        //public string OutPath_GeneratedDir
        //{
        //    get { return commonSettings.GetString(AllSettingsEn.OutPath_GeneratedDir ); }
        //    set { commonSettings.SetString(AllSettingsEn.OutPath_GeneratedDir , value); }
        //}

        /// <summary>
        /// [InOutPathsSettings]aspect,  [Type:string]  - 
        ///  Output Dir path where we can copy all  ServiceModel generated files.
        /// </summary>
        public string OutPath_TargetProjectCopyDir
        {
            get { return commonSettings.GetString(AllSettingsEn.OutPath_TargetProjectCopyDir ); }
            set { commonSettings.SetString(AllSettingsEn.OutPath_TargetProjectCopyDir , value); }
        }

        ///// <summary>
        /////  Output file path of   Service Resources(resx) file.
        ///// </summary>
        //public string OutPath_GeneratedServiceResources
        //{
        //    get { return commonSettings.GetString(AllSettingsEn.OutPath_GeneratedServiceResources)); }
        //    set { commonSettings.SetString(AllSettingsEn.OutPath_GeneratedServiceResources), value); }
        //}
         

        #endregion -------------- ASPECT SETTINGS ------------------



    }
}
