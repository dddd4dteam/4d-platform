﻿using System;
using System.Collections.Generic;
using DDDD.DAL.Generators.Meta;
 


namespace DDDD.DAL.Generators.Settings
{

    /// <summary>
    /// Установки  для текущего( в процессе генерации) сервиса.
    ///  Они из меняются
    /// при переходе от одного сервиса к другому 
    /// </summary>
    public class SystemCompositionSettings : AspectSettingsContainer
    {
        #region ------------Ctors-----------

        public SystemCompositionSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS -------------
        
        // -----------   Service Model SETTINGS
        //  Генерировать таблицы модели

        /// <summary>
        /// Включить в генерацию типы объектов с ролью DataTable
        /// </summary>
        public bool GenerateClassesForTables// = true;
        {
            get { return commonSettings.GetBool(AllSettingsEn.GenerateClassesForTables ); }
            set { commonSettings.SetBool(AllSettingsEn.GenerateClassesForTables , value); }    
        }


        /// <summary>
        /// Путь к файлу описания Элементов Образующих Систему  
        /// </summary>
        public String SystemCompositionElementsFilePath
        {
            get { return commonSettings.GetString(AllSettingsEn.SystemCompositionElementsFilePath); }
            set { commonSettings.SetString(AllSettingsEn.SystemCompositionElementsFilePath, value); }    
        }

        public bool RenderBackReferences
        {
            get { return commonSettings.GetBool(AllSettingsEn.RenderBackReferences); }
            set { commonSettings.SetBool(AllSettingsEn.RenderBackReferences, value); }  
        }
         
       

        /// <summary>
        /// Список доменных префиксов у таблиц
        /// </summary>
        public List<String> DomainTableEntitiesPrefixes
        {
            get { return commonSettings.GetList<String>(AllSettingsEn.DomainTableEntitiesPrefixes ); }
            set { commonSettings.SetList<String>(AllSettingsEn.DomainTableEntitiesPrefixes , value); }    
        }

        /// <summary>
        /// ищем ли Вьюшки в фильтрации
        /// </summary>
        public bool DomainViewFiltration
        {
            get { return commonSettings.GetBool(AllSettingsEn.DomainViewFiltration ); }
            set { commonSettings.SetBool(AllSettingsEn.DomainViewFiltration , value); }
        }

        /// <summary>
        /// список ролей для выбираемых таблиц("", "Register")
        /// </summary>
        public List<BORoleEn> TableEntities_Suffixes
        {
            get { return commonSettings.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes ); }
            set { commonSettings.SetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes , value); }
        }

        /// <summary>
        /// список суффиксов для вью ("", "Register", "Vo")
        /// </summary>
        public List<BORoleEn> ViewEntities_Suffixes
        {
            get { return commonSettings.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes ); }
            set { commonSettings.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes , value); }
        }

        public DomainServRoleFiltration FiltrationType
        {
            get { return commonSettings.GetItem<DomainServRoleFiltration>(AllSettingsEn.FiltrationType); }
            set { commonSettings.SetItem<DomainServRoleFiltration>(AllSettingsEn.FiltrationType, value); }
        }

        /// <summary>
        /// Сценарий по которому происходит текущая генерация
        /// </summary>
        public ScenariosEn GeneratorCurrentScenario
        {
            get { return commonSettings.GetItem<ScenariosEn>(AllSettingsEn.GeneratorCurrentScenario); }
            set { commonSettings.SetItem<ScenariosEn>(AllSettingsEn.GeneratorCurrentScenario, value); }
        }


        // ---------- Service Model SETTINGS
        #endregion -------------- ASPECT SETTINGS -------------
    }

}
