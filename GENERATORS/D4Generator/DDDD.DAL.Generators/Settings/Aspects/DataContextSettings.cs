﻿using DDDD.DAL.Generators.Meta;
 
using System.Collections.Generic;


namespace DDDD.DAL.Generators.Settings
{
    // ------ Параметры для кастомизации процесса генерации Модели ------
    /// <summary>
    /// Установки для контекста данных
    /// </summary>
    public class DataContextSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public DataContextSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
        : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS -----------------
        
        /// <summary>
        /// 
        /// </summary>
        public bool UseDataContextClassGeneration
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseDataContextClassGeneration ); }
            set { commonSettings.SetBool(AllSettingsEn.UseDataContextClassGeneration , value); }
        }

        /// <summary>
        /// Список дополнительных USING директив
        /// </summary>
        public List<string> Usings
        {
            get { return commonSettings.GetList(AllSettingsEn.Usings ); }
            set { commonSettings.SetList(AllSettingsEn.Usings , value); }
        }

      

        public string ConnectionType
        {
            get { return commonSettings.GetString(AllSettingsEn.ConnectionType ); }
            set { commonSettings.SetString(AllSettingsEn.ConnectionType , value); }
        }

        public string DataProviderAssembly
        {
            get { return commonSettings.GetString(AllSettingsEn.DataProviderAssembly ); }
            set { commonSettings.SetString(AllSettingsEn.DataProviderAssembly , value); }
        }


        public string DatabaseName
        {
            get { return commonSettings.GetString(AllSettingsEn.DatabaseName ); }
            set { commonSettings.SetString(AllSettingsEn.DatabaseName , value); }
        }


        public string DataContextName
        {
            get { return commonSettings.GetString(AllSettingsEn.DataContextName ); }
            set { commonSettings.SetString(AllSettingsEn.DataContextName  , value); }        
        }


       
        
            
        public string BaseDataContextClass
        {
            get { return commonSettings.GetString(AllSettingsEn.BaseDataContextClass ); }
            set { commonSettings.SetString(AllSettingsEn.BaseDataContextClass , value); }        
        }

        public string OneToManyAssociationType            // = "IEnumerable<{0}>";
        {
            get { return commonSettings.GetString(AllSettingsEn. OneToManyAssociationType ); }
            set { commonSettings.SetString(AllSettingsEn.OneToManyAssociationType , value); }        
        }


       
            

        #endregion -------------- ASPECT SETTINGS -----------------

    }

}
