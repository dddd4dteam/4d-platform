﻿
 
using System;
using System.Collections.Generic;
using DDDD.DAL.Generators.Meta;


namespace DDDD.DAL.Generators.Settings
{
    /// <summary>
    /// Установки  для текущего( в процессе генерации) сервиса.
    ///  Они из меняются при переходе от одного сервиса к другому. 
    /// </summary>
    public class CurrentServiceSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public CurrentServiceSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region -------------- ASPECT SETTINGS ------------------
       
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string] 
        /// Meta Data Loader Key
        /// </summary>
        public string CurrentMDLoader
        {
            get { return commonSettings.GetString(AllSettingsEn.CurrentMDLoader); }
            set { commonSettings.SetString(AllSettingsEn.CurrentMDLoader, value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect, [Type:string] 
        /// Code Template Render Key 
        /// </summary>
        public string CurrentRender
        {
            get { return commonSettings.GetString(AllSettingsEn.CurrentRender); }
            set { commonSettings.SetString(AllSettingsEn.CurrentRender, value); }
        }

        
       

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        ///  Use ServiceModel attribute for contracts
        /// </summary>
        public bool UseServiceModelAttribute
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseServiceModelAttribute  ); }
            set { commonSettings.SetBool( AllSettingsEn.UseServiceModelAttribute  , value); }
        }


        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        /// ServiceModelType - standart or specific.
        /// true - Is standart ServiceModel/ false - Specific Service Model - like HierarchyServiceModel
        /// </summary>
        public bool ServiceModelType
        {
            get { return commonSettings.GetBool(AllSettingsEn.ServiceModelType ); }
            set { commonSettings.SetBool(AllSettingsEn.ServiceModelType , value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// SpecificServiceModelBaseClass - 
        /// for Ex: HierarchyServiceModel as base class for custom ServiceModel class
        /// </summary> 
        public string SpecificServiceModelBaseClass
        {
            get { return commonSettings.GetString(AllSettingsEn.SpecificServiceModelBaseClass ); }
            set { commonSettings.SetString(AllSettingsEn.SpecificServiceModelBaseClass , value); }
        }



        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// Sercive Model Name. 
        /// As usually Service Model Name equal to template file name.
        /// <para/> Ex: S1_Geography
        /// </summary>
        public string ServiceModelName
        {
            get { return commonSettings.GetString( AllSettingsEn.ServiceModelName   ); }
            set { commonSettings.SetString(AllSettingsEn.ServiceModelName , value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:int]  -
        /// Set index of Service Model
        /// Ex: S1_Geography - service Name , then  1 is service index
        /// </summary>
        public int ServiceModelIndex
        {
            get { return commonSettings.GetItem<int>(AllSettingsEn.ServiceModelIndex); }
            set { commonSettings.SetItem<int>(AllSettingsEn.ServiceModelIndex, value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model Target Entities, should be pointed by hads
        /// </summary>
        public List<string> ServiceTargetEntityName
        {
            get { return commonSettings.GetList<String>(AllSettingsEn.ServiceTargetEntityName); }
            set { commonSettings.SetList<String>(AllSettingsEn.ServiceTargetEntityName, value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model   Generation blocks 
        /// 1  ServiceModelClasses /2  ServiceModelClass
        ///  /3  DataContextClass  /4  ResourcesFile
        /// </summary>
        public List<string> ServiceGenerationBlocks
        {
            get { return commonSettings.GetList<String>(AllSettingsEn.ServiceGenerationBlocks); }
            set { commonSettings.SetList<String>(AllSettingsEn.ServiceGenerationBlocks, value); }
        }



        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  = Domain Prefix in each Contract Name
        /// Ex: "crs"
        /// </summary>
        public string DomainPrefix                  //= "crs";
        {
            get { return commonSettings.GetString(AllSettingsEn.DomainPrefix ); }
            set { commonSettings.SetString(AllSettingsEn.DomainPrefix , value); }
        }

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        ///  Service  Model  Namespace for  entity  classes
        /// </summary>
        public string Namespace                  //= "CRM.DOM,DAL";
        {
            get { return commonSettings.GetString(AllSettingsEn.Namespace ); }
            set { commonSettings.SetString(AllSettingsEn.Namespace , value); }
        }


        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        /// Connection string  to DB
        /// </summary> 
        public string ConnectionString
        {
            get { return commonSettings.GetString(AllSettingsEn.ConnectionString ); }
            set { commonSettings.SetString(AllSettingsEn.ConnectionString , value); }
        }


        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: bool] 
        /// Generate resources for all views and tables
        /// </summary>
        public bool GenerateResForAllViews
        {
            get { return commonSettings.GetBool(AllSettingsEn.GenerateResForAllViews ); }
            set { commonSettings.SetBool(AllSettingsEn.GenerateResForAllViews , value); }
        }


        #endregion -------------- ASPECT SETTINGS ------------------

    }

}
