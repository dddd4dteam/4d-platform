﻿namespace DDDD.DAL.Generators.Settings
{
    /// <summary>
    /// Аспекты классов и типов классов(Класс Контекста данных, Класс Сущностей... здесь пока нет аспектов  для фолрмирования элементов  интерфейса)
    /// </summary>
    public enum SettingAspectsEn
    {
        /// <summary>
        /// Не определенный вариант -только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,
        /// <summary>
        /// аспект для класса контекста данных
        /// </summary>
        DataContextSettingsAspect, //

        /// <summary>
        /// аспект базового класса для сущностей 
        /// </summary>
        EntitiesBaseClassSettingsAspect,   

        /// <summary>
        /// аспект использования Контрактов данных 
        /// </summary>
        DataContractSettingsAspect,  

        /// <summary>
        /// аспект Сервисной Модели - все 
        /// </summary>
        SystemCompositionSettingsAspect,

        /// <summary>
        /// аспект DAL настроек для библиотеки BLToolkit
        /// </summary>
        BLDALModelSettingsAspect,

        /// <summary>
        /// аспект настроек для модели одного сервиса,имеено текущего сервиса
        /// </summary>
        CurrentServiceSettingsAspect,


        /// <summary>
        /// аспект использования внешних дополнительных атрибутов разработчика
        /// </summary>
        AdditionalAttributesSettingsAspect
    }

  

    /// <summary>
    /// All aspect properties in one enum. - 
    /// All Setting Keys.  
    /// </summary>
    public enum AllSettingsEn
    {
        //======AdditionalAttributesSettings ======
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]   - Use-create IClonable  interface
        /// </summary>
        UseIClonable,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use-create IVObject  interface   
        /// </summary>
        UseIVObjectRealization,
        /// <summary>
        ///  [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use VObject mapping Attribute
        /// </summary>
        UseVObjectMappingAttribute,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use BObject as base class for model contracts
        /// </summary>
        UseBOContracts,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,   [Type:bool]  -  Use  IHierarchicalView  interface 
        ///   for some specific contracts in Herarchical Service Model
        /// </summary>
        UseIHierarchicalView,
        /// <summary>
        ///[AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use   IPersistableBO  interface -
        /// use PersistableBO and BObjectBase  base classes differentiation
        /// , not only BObjectBase for all contracts 
        /// </summary>
        UseIPersistableBO,


        //====== BLDALModelSettings ========
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  TableName attribute  
        /// </summary>
        UseTableNameAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   Nullable attribute  for properties
        /// </summary>
        UsePropNullableAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   Identity attribute  for properties
        /// </summary>
        UsePropIdentityAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use  PrimaryKey attribute  for properties
        /// </summary>
        UsePropPrimaryKeyAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  BOPropertyRole attribute  for propertie
        /// </summary>
        UseBOPropertyRoleAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   ForeignKey attribute  for properties
        /// </summary>
        UseForeignKeyProperties,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   BORole  PrimaryKey enum value attribute  for properties
        /// </summary>
        UseBOPrimaryKeyRoleAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  filtration for association  for properties
        /// </summary>
        UseFiltrationForAssociations,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -   Perform Checks For Sync And Offline  for connections  
        /// </summary>
        PerformChecksForSyncAndOffline,
        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  
        /// Use DataContract  attributes for Tables and DataMember attributes for  properties 
        /// </summary>
        UseDataContracts,


        //======  CurrentServiceSettings--------
        
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string] 
        /// Meta Data Loader Key
        /// </summary>
        CurrentMDLoader,

        /// <summary>
        /// [CurrentServiceSettings]aspect, [Type:string] 
        /// Code Template Render Key 
        /// </summary>
        CurrentRender,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model   Generation blocks 
        /// 1  ServiceModelClasses /2  ServiceModelClass
        ///  /3  DataContextClass  /4  ResourcesFile
        /// </summary>
        ServiceGenerationBlocks,


        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        ///  Use ServiceModel attribute for contracts        /// </summary>
        UseServiceModelAttribute,
        
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        /// ServiceModelType - standart or specific.
        /// true - Is standart ServiceModel/ false - Specific Service Model - like HierarchyServiceMod
        /// </summary>
        ServiceModelType, // standart or specific
        
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// SpecificServiceModelBaseClass - 
        /// for Ex: HierarchyServiceModel as base class for custom ServiceModel class
        /// </summary>
        SpecificServiceModelBaseClass,         
        
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// Sercive Model Name. 
        /// As usually Service Model Name equal to template file name.
        /// <para/> Ex: S1_Geography
        /// </summary>
        ServiceModelName,
        
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:int]  -
        /// Set index of Service Model
        /// Ex: S1_Geography - service Name , then  1 is service index
        /// </summary>
        ServiceModelIndex,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model Target Entities, should be pointed by hads
        /// </summary>
        ServiceTargetEntityName,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  = Domain Prefix in each Contract Name
        /// Ex: "crs" 
        /// </summary>
        DomainPrefix,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        ///  Service  Model  Namespace for  entity  classes
        /// </summary>
        Namespace,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        /// Connection string  to DB
        /// </summary>
        ConnectionString,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: bool] 
        /// Generate resources for all views and tables
        /// </summary>
        GenerateResForAllViews,

        //====== InOutPathsSettings-----------
        
        /// <summary>
        /// [InOutPathsSettings]aspect,  [Type:string]  - 
        ///  Output Dir path where we can copy all  ServiceModel generated files.
        /// </summary>
        OutPath_TargetProjectCopyDir,


        //====== DataContextSettings---------
        UseDataContextClassGeneration,
        Usings,
        ConnectionType,
        DataProviderAssembly,
        DatabaseName,
        DataContextName,
        BaseDataContextClass,
        OneToManyAssociationType,


        //====== DataContractSettings------




        //====== EntitiesBaseClassSettings-----
        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:bool]  -
        /// Use Base Class for Service Model Contracts
        /// </summary>
        UseBaseEntityClass,
        UseBaseEntityClassGeneric,
        UseBaseEntityClassPlane,
        BaseEntityClass,
        BasePlaneEntityClass,





        //====== SystemCompositionSettings-------
        /// <summary>
        /// 
        /// </summary>
        GenerateClassesForTables,
        /// <summary>
        /// 
        /// </summary>
        SystemCompositionElementsFilePath,

        /// <summary>
        /// 
        /// </summary>
        DomainTableEntitiesPrefixes,
        /// <summary>
        /// 
        /// </summary>
        DomainViewFiltration,
        /// <summary>
        /// 
        /// </summary>
        TableEntities_Suffixes,
        /// <summary>
        /// 
        /// </summary>
        ViewEntities_Suffixes,
        /// <summary>
        /// 
        /// </summary>
        FiltrationType,
        /// <summary>
        /// отображать свойства для ForeignKeys
        /// </summary>
        RenderBackReferences,



        /// <summary>
        /// 
        /// </summary>
         GeneratorCurrentScenario, // Сценацрий   

    }


    /// <summary>
    /// Filters for tables by: Domain/ Service / Roles      
    /// </summary>
    public enum DomainServRoleFiltration
    {
        Custom = 0,
        AllByDomainService_2 = 2,
        RegistersByDomainService_3 = 3,
        VoByDomainService_4 = 4,
        VoRegistersByDomainService_5 = 5,
        DatasByDomainService_6 = 6,
        ViewDatasbyDomainService_7 = 7,
        TableDatasByDomainService_8 = 8,
        TableNotDatasByDomainService_9 = 9
    }
}
