﻿

using DDDD.DAL.Generators.Meta;
 

namespace DDDD.DAL.Generators.Settings
{

    // ----------Entities BASE CLASS OPTIONS ----------------
    /// <summary>
    /// Установки для базового класса сущностей
    /// </summary>
    public class EntitiesBaseClassSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public EntitiesBaseClassSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region ------------------- ASPECT SETTINGS --------------------

        // Базовый класс 

        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:bool]  -
        /// Use Base Class for Service Model Contracts
        /// </summary>
        public bool UseBaseEntityClass //  = false;      // Использовать базовый класс сущностей
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseBaseEntityClass ); }
            set { commonSettings.SetBool(AllSettingsEn.UseBaseEntityClass , value); }    
        }

        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:bool]  
        /// 
        /// </summary>
        public bool UseBaseEntityClassGeneric// = false;
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseBaseEntityClassGeneric); }
            set { commonSettings.SetBool(AllSettingsEn.UseBaseEntityClassGeneric , value); }    
        }

        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:bool]  
        /// 
        /// </summary>
        public bool UseBaseEntityClassPlane // = false;
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseBaseEntityClassPlane); }
            set { commonSettings.SetBool(AllSettingsEn.UseBaseEntityClassPlane, value); }    
        }

        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:string]  
        /// 
        /// </summary>
        public string BaseEntityClass  // = "";  //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)
        {
            get { return commonSettings.GetString(AllSettingsEn.BaseEntityClass ); }
            set { commonSettings.SetString(AllSettingsEn.BaseEntityClass , value); }    
        }


        /// <summary>
        /// [EntitiesBaseClassSettings]aspect,  [Type:string]  
        /// 
        /// </summary>
        public string BasePlaneEntityClass // = "";     // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)
        {
            get { return commonSettings.GetString(AllSettingsEn.BasePlaneEntityClass); }
            set { commonSettings.SetString(AllSettingsEn.BasePlaneEntityClass, value); }    
        }

        #endregion ------------------- ASPECT SETTINGS --------------------

    }
     

}
