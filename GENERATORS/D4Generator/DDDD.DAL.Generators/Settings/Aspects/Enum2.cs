﻿namespace DDDD.DAL.Generators.Settings
{
    public enum AllSettings2En
    {
        //======AdditionalAttributesSettings ======
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]   - Use-create IClonable  interface
        /// </summary>
        UseIClonable,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use-create IVObject  interface   
        /// </summary>
        UseIVObjectRealization,
        /// <summary>
        ///  [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use VObject mapping Attribute
        /// </summary>
        UseVObjectMappingAttribute,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use BObject as base class for model contracts
        /// </summary>
        UseBOContracts,
        /// <summary>
        /// [AdditionalAttributesSettings]aspect,   [Type:bool]  -  Use  IHierarchicalView  interface 
        ///   for some specific contracts in Herarchical Service Model
        /// </summary>
        UseIHierarchicalView,
        /// <summary>
        ///[AdditionalAttributesSettings]aspect,  [Type:bool]  -  Use   IPersistableBO  interface -
        /// use PersistableBO and BObjectBase  base classes differentiation
        /// , not only BObjectBase for all contracts 
        /// </summary>
        UseIPersistableBO,


        //====== BLDALModelSettings ========
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  TableName attribute  
        /// </summary>
        UseTableNameAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   Nullable attribute  for properties
        /// </summary>
        UsePropNullableAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   Identity attribute  for properties
        /// </summary>
        UsePropIdentityAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use  PrimaryKey attribute  for properties
        /// </summary>
        UsePropPrimaryKeyAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  BOPropertyRole attribute  for propertie
        /// </summary>
        UseBOPropertyRoleAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   ForeignKey attribute  for properties
        /// </summary>
        UseForeignKeyProperties,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use   BORole  PrimaryKey enum value attribute  for properties
        /// </summary>
        UseBOPrimaryKeyRoleAttribute,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -  Use  filtration for association  for properties
        /// </summary>
        UseFiltrationForAssociations,
        /// <summary>
        /// [BLDALModelSettings]aspect,  [Type:bool]  -   Perform Checks For Sync And Offline  for connections  
        /// </summary>
        PerformChecksForSyncAndOffline,
        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  
        /// Use DataContract  attributes for Tables and DataMember attributes for  properties 
        /// </summary>
        UseDataContracts,


        //======  CurrentServiceSettings--------

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string] 
        /// Meta Data Loader Key
        /// </summary>
        CurrentMDLoader,

        /// <summary>
        /// [CurrentServiceSettings]aspect, [Type:string] 
        /// Code Template Render Key 
        /// </summary>
        CurrentRender,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model   Generation blocks 
        /// 1  ServiceModelClasses /2  ServiceModelClass
        ///  /3  DataContextClass  /4  ResourcesFile
        /// </summary>
        ServiceGenerationBlocks,


        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        ///  Use ServiceModel attribute for contracts        /// </summary>
        UseServiceModelAttribute,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:bool]  -
        /// ServiceModelType - standart or specific.
        /// true - Is standart ServiceModel/ false - Specific Service Model - like HierarchyServiceMod
        /// </summary>
        ServiceModelType, // standart or specific

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// SpecificServiceModelBaseClass - 
        /// for Ex: HierarchyServiceModel as base class for custom ServiceModel class
        /// </summary>
        SpecificServiceModelBaseClass,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  -
        /// Sercive Model Name. 
        /// As usually Service Model Name equal to template file name.
        /// <para/> Ex: S1_Geography
        /// </summary>
        ServiceModelName,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:int]  -
        /// Set index of Service Model
        /// Ex: S1_Geography - service Name , then  1 is service index
        /// </summary>
        ServiceModelIndex,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:List{string}]  -
        ///  Service  Model Target Entities, should be pointed by hads
        /// </summary>
        ServiceTargetEntityName,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type:string]  = Domain Prefix in each Contract Name
        /// Ex: "crs" 
        /// </summary>
        DomainPrefix,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        ///  Service  Model  Namespace for  entity  classes
        /// </summary>
        Namespace,
        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: string]  -
        /// Connection string  to DB
        /// </summary>
        ConnectionString,

        /// <summary>
        /// [CurrentServiceSettings]aspect,  [Type: bool] 
        /// Generate resources for all views and tables
        /// </summary>
        GenerateResForAllViews,

        //====== InOutPathsSettings-----------

        /// <summary>
        /// [InOutPathsSettings]aspect,  [Type:string]  - 
        ///  Output Dir path where we can copy all  ServiceModel generated files.
        /// </summary>
        OutPath_TargetProjectCopyDir
    }

}
