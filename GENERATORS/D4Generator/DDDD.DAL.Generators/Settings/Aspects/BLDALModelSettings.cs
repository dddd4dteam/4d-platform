﻿using DDDD.DAL.Generators.Meta;
 

namespace DDDD.DAL.Generators.Settings
{

    /// <summary>
    /// Свойства Модели DAL от BLToolkit-а
    /// </summary>
    public class BLDALModelSettings : AspectSettingsContainer
    {

        #region ------------Ctors-----------

        public BLDALModelSettings(AspectEn containerAspect, GeneratorSettingsContainer CurrentCommonSettings)
            : base(containerAspect, CurrentCommonSettings)
        { }

        #endregion ----------Ctor---------


        #region ---------- ASPECT SETTINGS -----------------
        // ------------ Attributes SETTINGS -----------


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use  TableName attribute for contracts
        /// </summary>
        public bool UseTableNameAttribute
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseTableNameAttribute  ); }
            set { commonSettings.SetBool(AllSettingsEn.UseTableNameAttribute , value); }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use   Nullable attribute  for properties
        /// </summary>
        public bool UsePropNullableAttribute
        {
            get { return commonSettings.GetBool(AllSettingsEn.UsePropNullableAttribute  ); }
            set { commonSettings.SetBool(AllSettingsEn.UsePropNullableAttribute  ,value);  }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use   Identity attribute  for properties
        /// </summary>
        public bool UsePropIdentityAttribute
        {
            get { return commonSettings.GetBool(AllSettingsEn.UsePropIdentityAttribute ); }
            set { commonSettings.SetBool(AllSettingsEn.UsePropIdentityAttribute ,value); }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use   PrimaryKey attribute  for properties
        /// </summary>
        public bool UsePropPrimaryKeyAttribute
        {
            get { return commonSettings.GetBool( AllSettingsEn.UsePropPrimaryKeyAttribute ); }
            set { commonSettings.SetBool( AllSettingsEn.UsePropPrimaryKeyAttribute , value);   }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use   ForeignKey attribute  for properties
        /// </summary>
        public bool UseForeignKeyProperties
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseForeignKeyProperties ); }
            set { commonSettings.SetBool(AllSettingsEn.UseForeignKeyProperties , value);  }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use  BOPropertyRole attribute  for properties
        /// </summary>
        public bool UseBOPropertyRoleAttribute
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseBOPropertyRoleAttribute  ); }
            set { commonSettings.SetBool(AllSettingsEn.UseBOPropertyRoleAttribute , value); }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use   BORole  PrimaryKey enum value attribute  for properties
        /// </summary>
        public bool UseBOPrimaryKeyRoleAttribute
        {
            get { return commonSettings.GetBool( AllSettingsEn.UseBOPrimaryKeyRoleAttribute  ); }
            set { commonSettings.SetBool( AllSettingsEn.UseBOPrimaryKeyRoleAttribute , value); }
        }

        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  Use  filtration for association  for properties
        /// </summary>
        public bool UseFiltrationForAssociations
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseFiltrationForAssociations  ); }
            set { commonSettings.SetBool(AllSettingsEn.UseFiltrationForAssociations , value); }
        }

        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -   Perform Checks For Sync And Offline  for connections  
        /// </summary>
        public bool PerformChecksForSyncAndOffline
        {
            get { return commonSettings.GetBool(AllSettingsEn.PerformChecksForSyncAndOffline ); }
            set { commonSettings.SetBool(AllSettingsEn.PerformChecksForSyncAndOffline , value); }
        }


        /// <summary>
        /// [BLDALModelSettings]aspect, [Type:bool]  -  
        /// Use DataContract  attributes for Tables and DataMember attributes for  properties 
        /// </summary>
        public bool UseDataContracts // = false;
        {
            get { return commonSettings.GetBool(AllSettingsEn.UseDataContracts); }
            set { commonSettings.SetBool(AllSettingsEn.UseDataContracts, value); }
        }


       
        #endregion -----------ASPECT SETTINGS -----------------

    }
}
