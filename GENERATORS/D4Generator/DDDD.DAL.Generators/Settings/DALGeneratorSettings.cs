﻿using DDDD.DAL.Generators.Meta;
 
using System;
using System.Collections.Generic;
using System.Linq;

namespace DDDD.DAL.Generators.Settings
{
    public class DALGeneratorSettings : GeneratorSettingsContainer
    {


        /// <summary>
        /// 
        /// </summary>
        public override GeneratorSettingsContainer InitAspects()
        {

            try
            {
                AspectSettingsContainer aspectContainer = new SystemCompositionSettings(AspectEn.SystemCompositionSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "BOModelSettings"));
            }


            try
            {
                AspectSettingsContainer aspectContainer = new CurrentServiceSettings(AspectEn.CurrentServiceSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "ServiceModelSettings"));
            }

            try
            {
                AspectSettingsContainer aspectContainer = new BLDALModelSettings(AspectEn.BLDALModelSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "BLDALModelSettings"));
            }


            try
            {
                AspectSettingsContainer aspectContainer = new DataContextSettings(AspectEn.DataContextSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "DataContextSettings"));
            }
             

            try
            {
                AspectSettingsContainer aspectContainer = new EntitiesBaseClassSettings(AspectEn.EntitiesBaseClassSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "EntitiesBaseClassSettings"));
            }




            try
            {

                AspectSettingsContainer aspectContainer = new AdditionalAttributesSettings(AspectEn.AdditionalAttributesSettingsAspect, this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "AdditionalAttributesSettings"));
            }

            try
            {
                AspectSettingsContainer aspectContainer = new InOutPathsSettings(AspectEn.InOutPathsSettingsAspect , this);
                aspectContainer.Register();
            }
            catch (Exception)
            {
                throw new Exception(String.Format("There is error in registering {0} container ", "InOutPathsSettings"));
            }

            return this;



        }


        /// -------------------------------------------------------------------------------------------------
        //  -------------- SETTINGS END - Параметры для кастомизации процесса генерации Модели ----------------
        /// -------------------------------------------------------------------------------------------------



        #region  -------------------- Setting Custom Options by Scenarious  ---------------------


        public override void SetDefaultsSettingsByScenario(ScenariosEn scenario, string ConnectionString, bool GenerateResForAllViews = false, bool PerformChecksForSyncAndOffline = true)
        {
            try
            {
                #region Default Settings
                SetString(AllSettingsEn.ConnectionType, typeof(System.Data.SqlClient.SqlConnection).AssemblyQualifiedName);
                SetBool(AllSettingsEn.UseFiltrationForAssociations, true);
                SetItem<DomainServRoleFiltration>(AllSettingsEn.FiltrationType, DomainServRoleFiltration.Custom);
                SetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes, new List<BORoleEn>());
                SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes, new List<BORoleEn>());
                SetString(AllSettingsEn.Namespace, "DataModel");
                SetString(AllSettingsEn.BaseDataContextClass, "DbManager");
                SetString(AllSettingsEn.OneToManyAssociationType, "IEnumerable<{0}>");
                SetBool(AllSettingsEn.RenderBackReferences, true);
                SetBool(AllSettingsEn.GenerateResForAllViews, GenerateResForAllViews);
                SetBool(AllSettingsEn.PerformChecksForSyncAndOffline, PerformChecksForSyncAndOffline);
                SetItem<ScenariosEn>(AllSettingsEn.GeneratorCurrentScenario, scenario);
                SetString(AllSettingsEn.ConnectionString,
                          ConnectionString == "" ? "Data Source=LC-OLAPTEST;Initial Catalog=CRM_Cersanit2;User ID=sa;Password=cersanit" : ConnectionString);

                #endregion.

                switch (scenario)
                {
                    case ScenariosEn.NotDefined:
                        break;
                    case ScenariosEn.DefaultGeneratorSettings:

                        SetDefaultsSettingsByScenario(ScenariosEn.ServerSettingsScenario,"",false);
                        break;

                    //--------------- SERVER SETTINGS SCENARIO ------------------------
                    case ScenariosEn.ServerSettingsScenario:
                        DALGeneratorSettings Stg = this;

                        SystemCompositionSettings cfg8 = Stg.GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;                        
                        cfg8.GenerateClassesForTables = true;
                        cfg8.RenderBackReferences = false;
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;

                        //cfg8.SystemCompositionElementsFilePath = 
                        //@"c:\Platform\Models\SystemCompositionElements.cpmdl";
                        // --------Loading SystemServices----------
                        //cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath)
                        //                      .ServiceRefContainer.Services.ToList();


                        BLDALModelSettings cfg1 = Stg.GetAspectByKey(AspectEn.BLDALModelSettingsAspect) as BLDALModelSettings;
                        // ------------ Attributes SETTINGS -----------
                        cfg1.UseTableNameAttribute = true;
                        cfg1.UsePropNullableAttribute = true;
                        cfg1.UsePropIdentityAttribute = false;// true;                          - 
                        cfg1.UseBOPropertyRoleAttribute = true;
                        cfg1.UsePropPrimaryKeyAttribute = false; //true;
                        cfg1.UseForeignKeyProperties = false;
                        cfg1.UseBOPrimaryKeyRoleAttribute = false;
                        cfg1.UseDataContracts = false;// true ;

                        // ------------ Attributes SETTINGS -----------


                        CurrentServiceSettings cfg2 = Stg.GetAspectByKey(AspectEn.CurrentServiceSettingsAspect) as CurrentServiceSettings;
                        
                        // 2 Принадлежность Контракта к модели сервиса
                        cfg2.UseServiceModelAttribute = true;
                        // 3 Имя сервиса 
                        cfg2.ServiceModelName = "S1_Geography";
                        cfg2.Namespace = "DDDD.CRM.DOM.DAL";
                        // 4 Service Index 
                        cfg2.ServiceModelIndex = 1;
                        cfg2.DomainPrefix = "crs";
                        
                        DataContextSettings cfg3 = Stg.GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации
                        
                        // DataContext Settings
                        cfg3.BaseDataContextClass = "DbManager";
                        cfg3.UseDataContextClassGeneration = false;
                      

                        //DataContractSettings cfg4 = Stg.GetAspectByKey(AspectEn.DataContractSettingsAspect) as DataContractSettings;
                        // -----------   Data Contracts SETTINGS    
                       
                        // ---------- Data Contracts SETTINGS


                        EntitiesBaseClassSettings cfg5 = Stg.GetAspectByKey(AspectEn.EntitiesBaseClassSettingsAspect) as EntitiesBaseClassSettings;
                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg5.UseBaseEntityClass = false; // Использовать базовый класс сущностей
                        cfg5.UseBaseEntityClassGeneric = false;
                        cfg5.UseBaseEntityClassPlane = false;
                        cfg5.BaseEntityClass = "BObjectBase<T>"; //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)      
                        cfg5.BasePlaneEntityClass = "IBObject"; // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)


                        AdditionalAttributesSettings cfg7 = Stg.GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseIClonable = true;
                        cfg7.UseIVObjectRealization = true;
                        cfg7.UseVObjectMappingAttribute = true;
                        cfg7.UseIHierarchicalView = true;
                        cfg7.UseIPersistableBO = true;
                        break;

                    //--------------- CLIENT SETTINGS SCENARIO ------------------------        
                    case ScenariosEn.ClientSettingsScenario:
                        cfg8 = GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;
                        cfg8.GenerateClassesForTables = true;
                        cfg8.RenderBackReferences = false;
                        
                        //cfg8.SystemCompositionElementsFilePath = @"c:\Platform\Models\SystemCompositionElements.cpmdl";
                        //// --------Loading SystemServices----------
                        //cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath)
                        //                        .ServiceRefContainer.Services.ToList();
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;
                        cfg8.FiltrationType = DomainServRoleFiltration.TableNotDatasByDomainService_9;


                        cfg1 = GetAspectByKey(AspectEn.BLDALModelSettingsAspect) as BLDALModelSettings;
                        // ------------ Attributes SETTINGS -----------
                        cfg1.UseTableNameAttribute = false;
                        cfg1.UsePropNullableAttribute = false;
                        cfg1.UsePropIdentityAttribute = false;
                        cfg1.UsePropPrimaryKeyAttribute = false;                            
                        cfg1.UseBOPropertyRoleAttribute = true;
                        cfg1.UseForeignKeyProperties = false;
                        cfg1.UseBOPrimaryKeyRoleAttribute = true;
                        cfg1.UseDataContracts = false;// true ;
                        // ------------ Attributes SETTINGS -----------


                        cfg2 = GetAspectByKey(AspectEn.CurrentServiceSettingsAspect) as CurrentServiceSettings;
                        // 2 Принадлежность Контракта к модели сервиса
                        cfg2.UseServiceModelAttribute = true;
                        cfg2.Namespace = "DDDD.CRM.DOM.DAL";
                        // 3 Имя сервиса 
                        cfg2.ServiceModelName = "S1_Geography";
                        cfg2.DomainPrefix = "crs";

                        cfg3 = GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        //cfg3.ConnectionString = "Server=LC-WIN7X64-4;Database=CRM_Cersanit2;Integrated Security=SSPI";
                        SetString(AllSettingsEn.ConnectionString,
                               ConnectionString == "" ? "Server=.;User ID=sa;Password=Cersanit345;Trusted_Connection=False;Integrated Security=True;" : ConnectionString);

                         

                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации
                                               
                        // DataContext Settings
                        cfg3.BaseDataContextClass = "DbManager";
                        cfg3.UseDataContextClassGeneration = false;
                        


                        // -----------   Data Contracts SETTINGS 
                        //cfg4 = GetAspectByKey(AspectEn.DataContractSettingsAspect) as DataContractSettings;
                        //cfg4.UseDataContracts = false;// true
                        // ---------- Data Contracts SETTINGS


                        cfg5 = GetAspectByKey(AspectEn.EntitiesBaseClassSettingsAspect) as EntitiesBaseClassSettings;
                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg5.UseBaseEntityClass = false; // Использовать базовый класс сущностей
                        cfg5.UseBaseEntityClassGeneric = false;
                        cfg5.UseBaseEntityClassPlane = false;
                        cfg5.BaseEntityClass = "BObjectBase<T>"; //Базовый   Дженерик класс  для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassGeneric)      
                        cfg5.BasePlaneEntityClass = "IBObject"; // Базовый Плоский класс для сущностей; neeed  (UseBaseEntityClass && UseBaseEntityClassPlane)


                        // ---------- BASE CLASS OPTIONS ----------------
                        cfg7 = GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseIClonable = true;
                        cfg7.UseIVObjectRealization = true;
                        cfg7.UseVObjectMappingAttribute = false;
                        cfg7.UseIHierarchicalView = true;
                        cfg7.UseIPersistableBO = true;
                        break;

                    // ---------- MODEL MANAGER SETTINGS -------------
                    case ScenariosEn.ModelManagerSettings:
                        cfg8 = GetAspectByKey(AspectEn.SystemCompositionSettingsAspect) as SystemCompositionSettings;
                        //cfg8.SystemCompositionElementsFilePath = @"c:\Platform\Models\SystemCompositionElements.cpmdl";
                        //// --------Loading SystemServices----------
                        //cfg8.SystemServices = SystemCompositionElementsTp.LoadFromFile(cfg8.SystemCompositionElementsFilePath)
                        //                     .ServiceRefContainer.Services.ToList();
                        
                        cfg8.DomainTableEntitiesPrefixes = new List<String>() { "crs" };
                        cfg8.DomainViewFiltration = true;
                        cfg8.FiltrationType = DomainServRoleFiltration.TableNotDatasByDomainService_9;


                        cfg3 = GetAspectByKey(AspectEn.DataContextSettingsAspect) as DataContextSettings;
                        //cfg3.ConnectionString = "Server=LC-WIN7X64-4;Database=CRM_Cersanit2;Integrated Security=SSPI";
                        SetString(AllSettingsEn.ConnectionString,
                               ConnectionString == "" ? "Server=.;User ID=sa;Password=Cersanit345;Trusted_Connection=False;Integrated Security=True;" : ConnectionString);

                        //cfg3.Namespace = "CRM.DOM.Services";
                        cfg3.Usings = new List<string>(); //Потому что у нас всегда списки нулл после регистрации
                         
                        // DataContext Settings
                        cfg3.BaseDataContextClass = "IServiceModelManager";
                        cfg3.UseDataContextClassGeneration = false;
                        //cfg3.DomainPrefix = "crs";
                        
                        cfg7 = GetAspectByKey(AspectEn.AdditionalAttributesSettingsAspect) as AdditionalAttributesSettings;
                        cfg7.UseBOContracts = true;

                        break;

                    case ScenariosEn.CustomGeneratorSettings:
                        break;

                    default:
                        SetDefaultsSettingsByScenario(ScenariosEn.ServerSettingsScenario,"");
                        break;
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        #endregion  -------------------- Setting Custom Options by Scenarious  ---------------------

    }
}



