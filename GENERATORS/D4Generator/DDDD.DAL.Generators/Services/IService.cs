﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel 
{
    /// <summary>
    ///  Common Service interface
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// Service IDentifier
        /// </summary>
        Guid SvcID { get; }

        /// <summary>
        /// Service Name
        /// </summary>
        string SvcName { get; }

        /// <summary>
        /// Service Type Info Extended
        /// </summary>
        //TypeInfoEx SvcTypeEx { get; }


        /// <summary>
        /// Service  Description
        /// </summary>
        string SvcDescription { get; }
    }



}
