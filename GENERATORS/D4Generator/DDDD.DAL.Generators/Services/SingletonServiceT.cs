﻿//using DDDD.Core.Threading;
//using DDDD.Core.Extensions;
//using DDDD.Core.Reflection;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DDDD.Core.ComponentModel 
{
    /// <summary>
    /// Singleton Service base generic  class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingletonService<T> : IService
      where T : class
    {

        #region ---------------- IService ----------------


        /// <summary>
        /// Service IDentifier
        /// </summary>
        public Guid SvcID
        { get; private set; }


        /// <summary>
        /// Service Name
        /// </summary>
        public string SvcName
        { get; private set; }

        /// <summary>
        /// Service  Description
        /// </summary>
        public string SvcDescription
        { get; protected set; } = "";


        #endregion ---------------- IService ----------------


        /// <summary>
        /// Indicator -shpw if singleton  instance was Inited
        /// </summary>
        public bool IsInited
        { get; protected set; }

        /// <summary>
        /// This Initialize() method will be called after T instance will be created - not inside Ctor of T.
        /// </summary>
        protected virtual void Initialize()
        {
            SvcID = Guid.NewGuid();
            SvcName =typeof(T).Name;
            
            // SvcDescription in end Service class 
        }


        /// <summary>
        /// Instance of DCManager of [TManaget] Type - your custom end DCManager Type
        /// </summary>
        protected static Lazy<T> LA_Current
        { get; } =new Lazy<T>(
            //Create(
            ( ) =>
            {
                // ctor()
                var newTargetT =  Activator.CreateInstance<T>();// new T();
                // Initializing
                (newTargetT as SingletonService<T>).Initialize();
                return newTargetT;
            }
            //, null, null
            );

        /// <summary>
        /// Service Type Info Ex
        /// </summary>
        //public TypeInfoEx SvcTypeEx
        //{ get; } = typeof(T).GetTypeInfoEx();

        /// <summary>
        /// Thread Safety created T service instance
        /// </summary>
        public static T Current
        {
            get { return LA_Current.Value; }
        }


    }
}
