﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDDD.DAL.Generators.Text
{

    /// <summary>
    ///Text common symbols , number sybols, formatting symbols
    /// </summary>
    public class Sym
    {
        public const char C_n = 'n';
        public const char C_u = 'u';
        public const char C_l = 'l';
        public const char C_t = 't';
        public const char C_r = 'r';
        public const char C_e = 'e';
        public const char C_f = 'f';
        public const char C_a = 'a';
        public const char C_s = 's';

        public const string NULL = "null";
        public const string TRUE = "true";
        public const string FALSE = "false";

        public const char CARET = '^';
       
        public const char QUOTE = '\"';
        /// <summary>
        /// Inside String Text :   "[insideStringText]"
        /// </summary>
        /// <param name="insideStringText"></param>
        /// <returns></returns>
        public static string InStr(string insideStringText)
        {
            return QUOTE + insideStringText + QUOTE;
        }

        /// <summary>
        /// Inside String Text :   { [insideStringText] }
        /// </summary>
        /// <param name="insideStringText"></param>
        /// <returns></returns>
        public static string InTwCrls(string insideStringText)
        {
            return  OBJECT_START + insideStringText +  OBJECT_END;
        }

        public const char COLON = ':';
        public const string COLON2 = " : ";
        public const char SCOLON = ';'; //semicolon
        public const char DOTc = '.';
        public const string DOTs = ".";
        public const string DOT2 = ". ";
        public const char COMMA = ',';
        public const string COMMA2 = ", ";

        public const char PIPE = '|';
        public const char QSTN = '?'; //Question
        public const char BSLASH = '\\'; //back slash
        public const char FSLASH = '/'; // forward slash
        public const char EQUAL = '='; // 
        public const char STAR = '*';
        public const char SHARP = '#';
        public const char AT = '@';


        public const char OBJECT_START = '{';
        public const char OBJECT_END = '}';

        public const char ARRAY_START = '[';
        public const char ARRAY_END = ']';

        public const string EMPTY = "";
        public const string TYPEKEY = "$Tp";
 

        public const char N_1 = '1';
        public const char N_2 = '2';
        public const char N_3 = '3';
        public const char N_4 = '4';
        public const char N_5 = '5';
        public const char N_6 = '6';
        public const char N_7 = '7';
        public const char N_8 = '8';
        public const char N_9 = '9';
        public const char N_0 = '0';
        public const char EBig = 'E';   //'E' Big
        public const char eSmall = 'e'; // 'e' Small        
        public const char PLUS = '+';
        public const char DASH = '-';

        /// <summary>
        /// Number starting characters [0-9] and '-'
        /// </summary>
        public static readonly HashSet<char> NumberStartChars = new HashSet<char>()
            { N_1, N_2, N_3, N_4, N_5, N_6, N_7, N_8, N_9, N_0,  DASH };

        /// <summary>
        /// All number characters also for floating point  numbers: [0-9] and '-'and 'E' and 'e'and '.'
        /// </summary>
        public static readonly HashSet<char> NumberChars = new HashSet<char>()
            {N_1, N_2, N_3, N_4, N_5, N_6, N_7, N_8, N_9, N_0, EBig, eSmall, DOTc, PLUS, DASH };


        public const char SPACE = ' '; // whitespace
        public const char RETURN = '\r'; // return caret
        public const char NEWLINE = '\n'; // new line
        public const char TAB = '\t'; // tabulation

        /// <summary>
        /// Formatting characters: space, tab,  return,  newline
        /// </summary>
        public static readonly HashSet<char> FormatChars = new HashSet<char>()
            { SPACE,RETURN,NEWLINE,TAB};


    }


}
