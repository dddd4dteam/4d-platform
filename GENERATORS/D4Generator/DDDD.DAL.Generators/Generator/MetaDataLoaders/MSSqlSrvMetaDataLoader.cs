﻿ 
using DDDD.DAL.Generators.Settings;
using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using DDDD.DAL.Generators.Meta;

namespace DDDD.DAL.Generators 
{
    public class MSSqlSrvMetaDataLoader : IMetaDataLoader
    {
        
        public MSSqlSrvMetaDataLoader( )
        {  }

        // Some custom Actions before and After Loading MetaData 
        public Action BeforeLoadMetadata = () => { };
        public Action AfterLoadMetadata = () => { };

        // PLuralization Suffixes for AssociationName
        public Func<string, string> PluralizeAssociationName = _ => _ + "s";
        public Func<string, string> SingularizeAssociationName = _ => _;


        /// <summary>
        ///   DAL Generation settings
        /// </summary>
        GeneratorSettingsContainer Stgs
        { get { return DALGenerator.Current.Settings; } }


        /// <summary>
        ///  Meta Data Loader Name.
        /// </summary>
        public string Name
        { get; set; } = nameof(MSSqlSrvMetaDataLoader);

        /// <summary>
        ///  loaded Tables meta info
        /// </summary>
        public Dictionary<string, Table> Tables
        { get; set; } = new Dictionary<string, Table>();


        /// <summary>
        /// Tables for Server only - ghost option
        /// </summary>
        public List<string> TablesForServerOnly
        { get; set; } = new List<string>();


        #region ------------------------ CONNECTION --------------------------------

        /// <summary>
        /// Getting Connection
        /// </summary>
        /// <returns></returns>
        internal IDbConnection GetConnection()
        {
            Type connType = null;

            if (Stgs.GetString(AllSettingsEn.DataProviderAssembly) != null)
            {
                try
                {
                    var assembly = System.Reflection.Assembly.LoadFile(
                        Stgs.GetString(AllSettingsEn.DataProviderAssembly));
                    connType = assembly.GetType(
                        Stgs.GetString(AllSettingsEn.ConnectionType));
                }
                catch
                {
                }
            }

            if (connType == null)
                connType = Type.GetType(Stgs.GetString(AllSettingsEn.ConnectionType));

            var conn = (System.Data.IDbConnection)Activator.CreateInstance(connType);

            conn.ConnectionString = Stgs.GetString(AllSettingsEn.ConnectionString);
            conn.Open();

            return conn;
        }


        #endregion ------------------------ CONNECTION --------------------------------


        public void LoadMetadata()
        {
            Tables = new Dictionary<string, Table>();

            BeforeLoadMetadata();

            LoadServerMetadata();

            foreach (var t in Tables.Values)
            {
                if (t.ClassName.Contains(" "))
                {
                    var ss = t.ClassName.Split(' ').Where(_ => _.Trim().Length > 0).Select(_ => char.ToUpper(_[0]) + _.Substring(1));
                    t.ClassName = string.Join("", ss.ToArray());
                }
            }

            foreach (var t in Tables.Values)
                foreach (var key in t.ForeignKeys.Values.ToList())
                    if (!key.KeyName.EndsWith("_BackReference"))
                        key.OtherTable.ForeignKeys.Add(key.KeyName + "_BackReference", key.BackReference = new ForeignKey
                        {
                            KeyName = key.KeyName + "_BackReference",
                            MemberName = key.MemberName + "_BackReference",
                            AssociationType = AssociationType.Auto,
                            OtherTable = t,
                            ThisColumns = key.OtherColumns,
                            OtherColumns = key.ThisColumns,
                        });

            foreach (var t in Tables.Values)
            {
                foreach (var key in t.ForeignKeys.Values)
                {
                    if (key.BackReference != null && key.AssociationType == AssociationType.Auto)
                    {
                        if (key.ThisColumns.All(_ => _.IsPrimaryKey))
                        {
                            if (t.Columns.Values.Count(_ => _.IsPrimaryKey) == key.ThisColumns.Count)
                                key.AssociationType = AssociationType.OneToOne;
                            else
                                key.AssociationType = AssociationType.ManyToOne;
                        }
                        else
                            key.AssociationType = AssociationType.ManyToOne;

                        key.CanBeNull = key.ThisColumns.All(_ => _.IsNullable);
                    }
                }
            }

            foreach (var t in Tables.Values)
            {
                foreach (var key in t.ForeignKeys.Values)
                {
                    var name = key.MemberName;

                    if (key.BackReference != null && key.ThisColumns.Count == 1 && key.ThisColumns[0].MemberName.ToLower().EndsWith("id"))
                    {
                        name = key.ThisColumns[0].MemberName;
                        name = name.Substring(0, name.Length - "id".Length);

                        if (!t.ForeignKeys.Values.Select(_ => _.MemberName).Concat(
                             t.Columns.Values.Select(_ => _.MemberName)).Concat(
                             new[] { t.ClassName }).Any(_ => _ == name))
                        {
                            name = key.MemberName; ;
                        }
                    }

                    if (name == key.MemberName)
                    {
                        if (name.StartsWith("FK_"))
                            name = name.Substring(3);

                        if (name.EndsWith("_BackReference"))
                            name = name.Substring(0, name.Length - "_BackReference".Length);

                        name = string.Join("", name.Split('_').Where(_ => _.Length > 0 && _ != t.TableName).ToArray());

                        if (name.Length > 0)
                            name = key.AssociationType == AssociationType.OneToMany ? PluralizeAssociationName(name) : SingularizeAssociationName(name);
                    }

                    if (name.Length != 0 &&
                        !t.ForeignKeys.Values.Select(_ => _.MemberName).Concat(
                         t.Columns.Values.Select(_ => _.MemberName)).Concat(
                         new[] { t.ClassName }).Any(_ => _ == name))
                    {
                        key.MemberName = name;
                    }
                }
            }

            //if (Tables.Values.SelectMany(_ => _.ForeignKeys.Values).Any(_ => _.AssociationType == AssociationType.OneToMany))
            //    Stgs.GetList(DataLayerSettingsEn.Usings)
            //        .Add("System.Collections.Generic");

            var keyWords = new HashSet<string>
            {
                "abstract", "as",       "base",     "bool",    "break",     "byte",     "case",       "catch",     "char",    "checked",
                "class",    "const",    "continue", "decimal", "default",   "delegate", "do",         "double",    "else",    "enum",
                "event",    "explicit", "extern",   "false",   "finally",   "fixed",    "float",      "for",       "foreach", "goto",
                "if",       "implicit", "in",       "int",     "interface", "internal", "is",         "lock",      "long",    "new",
                "null",     "object",   "operator", "out",     "override",  "params",   "private",    "protected", "public",  "readonly",
                "ref",      "return",   "sbyte",    "sealed",  "short",     "sizeof",   "stackalloc", "static",    "struct",  "switch",
                "this",     "throw",    "true",     "try",     "typeof",    "uint",     "ulong",      "unchecked", "unsafe",  "ushort",
                "using",    "virtual",  "volatile", "void",    "while"
            };

            foreach (var t in Tables.Values)
            {
                if (keyWords.Contains(t.ClassName))
                    t.ClassName = "@" + t.ClassName;

                if (keyWords.Contains(t.DataContextPropertyName))
                    t.DataContextPropertyName = "@" + t.DataContextPropertyName;

                foreach (var col in t.Columns.Values)
                    if (keyWords.Contains(col.MemberName))
                        col.MemberName = "@" + col.MemberName;
            }

            AfterLoadMetadata();
        }

        void LoadServerMetadata()
        {
            var tables = (new { ID = "", Table = new Table() }).CreateList();
            var columns = (new { ID = "", Column = new Column() }).CreateList();

            using (var conn = GetConnection())
            using (var cmd = conn.CreateCommand())
            {
                // Load tables & vies.
                //
                cmd.CommandText = @"
			SELECT
				TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' + TABLE_NAME,
				TABLE_SCHEMA,
				TABLE_NAME,
				TABLE_TYPE,
				tr.is_track_columns_updated_on
			FROM
				INFORMATION_SCHEMA.TABLES s
					LEFT JOIN sys.tables t ON OBJECT_ID(TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' + TABLE_NAME) = t.object_id
                    LEFT JOIN sys.change_tracking_tables tr on t.object_id = tr.object_id
			WHERE
				t.object_id IS NULL OR
				t.is_ms_shipped <> 1 AND
				(
					SELECT 
						major_id 
					FROM 
						sys.extended_properties 
					WHERE
						major_id = t.object_id and 
						minor_id = 0           and 
						class    = 1           and 
						name     = N'microsoft_database_tools_support'
				) IS NULL";

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        var t = new
                        {
                            ID = Convert.ToString(rd[0]),
                            Table = new Table
                            {
                                Owner = rd[1].ToString(),
                                TableName = rd[2].ToString(),
                                ClassName = rd[2].ToString(),
                                IsView = rd[3].ToString() == "VIEW",
                                ChangeTracking = new ChangeTracking { Enabled = rd[4] != DBNull.Value, TrackColumnsUpdated = rd[4] != DBNull.Value && Convert.ToBoolean(rd[4]) },
                                BaseClassName = Stgs.GetString(AllSettingsEn.BaseEntityClass),
                            }
                        };

                        tables.Add(t);
                    }
                }

                // Load columns.
                //
                cmd.CommandText = @"
			SELECT
				(TABLE_CATALOG + '.' + TABLE_SCHEMA + '.' + TABLE_NAME) as id,
				CASE WHEN IS_NULLABLE = 'YES' THEN 1 ELSE 0 END         as isNullable,
				ORDINAL_POSITION         as colid,
				COLUMN_NAME              as name,
				c.DATA_TYPE              as dataType,
				CHARACTER_MAXIMUM_LENGTH as length, 
				ISNULL(NUMERIC_PRECISION, DATETIME_PRECISION) AS prec,
				NUMERIC_SCALE            as scale,
				COLUMNPROPERTY(object_id('[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'), COLUMN_NAME, 'IsIdentity') as isIdentity
			FROM
				INFORMATION_SCHEMA.COLUMNS c";

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        var col = new
                        {
                            ID = Convert.ToString(rd["id"]),
                            Column = new Column
                            {
                                ID = Convert.ToInt16(rd["colid"]),
                                ColumnName = Convert.ToString(rd["name"]),
                                MemberName = Convert.ToString(rd["name"]),
                                ColumnType = Convert.ToString(rd["dataType"]),
                                IsNullable = Convert.ToBoolean(rd["isNullable"]),
                                IsIdentity = Convert.ToBoolean(rd["isIdentity"]),
                                Length = rd.IsDBNull(rd.GetOrdinal("length")) ? 0 : Convert.ToInt64(rd["length"]),
                                Precision = rd.IsDBNull(rd.GetOrdinal("prec")) ? 0 : Convert.ToInt32(rd["prec"]),
                                Scale = rd.IsDBNull(rd.GetOrdinal("scale")) ? 0 : Convert.ToInt32(rd["scale"]),
                            }
                        };

                        var c = col.Column;

                        switch (c.ColumnType)
                        {
                            case "image": c.Type = "byte[]"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.Image; break;
                            case "text": c.Type = "string"; c.DbType = DbType.String; c.SqlDbType = SqlDbType.Text; break;
                            case "binary": c.Type = "byte[]"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.Binary; break;
                            case "tinyint": c.Type = "byte"; c.DbType = DbType.Byte; c.SqlDbType = SqlDbType.TinyInt; break;
                            case "date": c.Type = "DateTime"; c.DbType = DbType.Date; c.SqlDbType = SqlDbType.Date; break;
                            case "time": c.Type = "DateTime"; c.DbType = DbType.Time; c.SqlDbType = SqlDbType.Time; break;
                            case "bit": c.Type = "bool"; c.DbType = DbType.Boolean; c.SqlDbType = SqlDbType.Bit; break;
                            case "smallint": c.Type = "short"; c.DbType = DbType.Int16; c.SqlDbType = SqlDbType.SmallInt; break;
                            case "decimal": c.Type = "decimal"; c.DbType = DbType.Decimal; c.SqlDbType = SqlDbType.Decimal; break;
                            case "int": c.Type = "int"; c.DbType = DbType.Int32; c.SqlDbType = SqlDbType.Int; break;
                            case "smalldatetime": c.Type = "DateTime"; c.DbType = DbType.DateTime; c.SqlDbType = SqlDbType.SmallDateTime; break;
                            case "real": c.Type = "float"; c.DbType = DbType.Single; c.SqlDbType = SqlDbType.Real; break;
                            case "money": c.Type = "decimal"; c.DbType = DbType.Currency; c.SqlDbType = SqlDbType.Money; break;
                            case "datetime": c.Type = "DateTime"; c.DbType = DbType.DateTime; c.SqlDbType = SqlDbType.DateTime; break;
                            case "float": c.Type = "double"; c.DbType = DbType.Double; c.SqlDbType = SqlDbType.Float; break;
                            case "numeric": c.Type = "decimal"; c.DbType = DbType.Decimal; c.SqlDbType = SqlDbType.Decimal; break;
                            case "smallmoney": c.Type = "decimal"; c.DbType = DbType.Currency; c.SqlDbType = SqlDbType.SmallMoney; break;
                            case "datetime2": c.Type = "DateTime"; c.DbType = DbType.DateTime2; c.SqlDbType = SqlDbType.DateTime2; break;
                            case "bigint": c.Type = "long"; c.DbType = DbType.Int64; c.SqlDbType = SqlDbType.BigInt; break;
                            case "varbinary": c.Type = "byte[]"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.VarBinary; break;
                            case "timestamp": c.Type = "byte[]"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.Timestamp; break;
                            case "sysname": c.Type = "string"; c.DbType = DbType.String; c.SqlDbType = SqlDbType.NVarChar; break;
                            case "nvarchar": c.Type = "string"; c.DbType = DbType.String; c.SqlDbType = SqlDbType.NVarChar; break;
                            case "varchar": c.Type = "string"; c.DbType = DbType.AnsiString; c.SqlDbType = SqlDbType.VarChar; break;
                            case "ntext": c.Type = "string"; c.DbType = DbType.String; c.SqlDbType = SqlDbType.NText; break;
                            case "uniqueidentifier": c.Type = "Guid"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.UniqueIdentifier; break;
                            case "datetimeoffset": c.Type = "DateTimeOffset"; c.DbType = DbType.DateTimeOffset; c.SqlDbType = SqlDbType.DateTimeOffset; break;
                            case "sql_variant": c.Type = "object"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.Variant; break;
                            case "xml": c.Type = "string"; c.DbType = DbType.Xml; c.SqlDbType = SqlDbType.Xml; break;

                            case "char":
                                c.Type = Convert.ToInt32(rd["length"]) == 1 ? "char" : "string";
                                c.DbType = DbType.AnsiStringFixedLength;
                                c.SqlDbType = SqlDbType.Char;
                                break;

                            case "nchar":
                                c.Type = Convert.ToInt32(rd["length"]) == 1 ? "char" : "string";
                                c.DbType = DbType.StringFixedLength;
                                c.SqlDbType = SqlDbType.NChar;
                                break;

                            //hierarchyid
                            //geometry
                            //geography
                            default: c.Type = "byte[]"; c.DbType = DbType.Binary; c.SqlDbType = SqlDbType.Binary; break;
                        }

                        switch (c.Type)
                        {
                            case "string":
                            case "object":
                            case "byte[]": c.IsClass = true; break;
                        }

                        if (c.IsNullable && !c.IsClass)
                            c.Type += "?";

                        columns.Add(col);
                    }
                }

                // Load PKs.
                //
                cmd.CommandText = @"
			SELECT
				(k.TABLE_CATALOG + '.' + k.TABLE_SCHEMA + '.' + k.TABLE_NAME) as id,
				k.CONSTRAINT_NAME                                             as name,
				k.COLUMN_NAME                                                 as colname,
				k.ORDINAL_POSITION                                            as colid
			FROM
				INFORMATION_SCHEMA.KEY_COLUMN_USAGE k
					JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS c ON k.CONSTRAINT_NAME = c.CONSTRAINT_NAME
			WHERE
				c.CONSTRAINT_TYPE='PRIMARY KEY'";

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        var id = Convert.ToString(rd["id"]);
                        var colid = Convert.ToInt32(rd["colid"]);
                        var colname = Convert.ToString(rd["colname"]);

                        columns.Single(_ => _.ID == id && _.Column.ColumnName == colname).Column.PKIndex = colid;
                    }
                }

                // Load FKs.
                //
                cmd.CommandText = @"
			SELECT
				rc.CONSTRAINT_NAME  as Name, 
				fk.TABLE_CATALOG + '.' + fk.TABLE_SCHEMA + '.' + fk.TABLE_NAME as ThisTable,
				fk.COLUMN_NAME      as ThisColumn,
				pk.TABLE_CATALOG + '.' + pk.TABLE_SCHEMA + '.' + pk.TABLE_NAME as OtherTable,
				pk.COLUMN_NAME      as OtherColumn,
				cu.ORDINAL_POSITION as Ordinal
			FROM
				INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS rc
					JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE fk
					ON
						rc.CONSTRAINT_CATALOG        = fk.CONSTRAINT_CATALOG AND
						rc.CONSTRAINT_NAME           = fk.CONSTRAINT_NAME
					JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE pk
					ON
						rc.UNIQUE_CONSTRAINT_CATALOG = pk.CONSTRAINT_CATALOG AND
						rc.UNIQUE_CONSTRAINT_NAME    = pk.CONSTRAINT_NAME
					JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE cu
					ON
						rc.CONSTRAINT_NAME = cu.CONSTRAINT_NAME
			ORDER BY
				ThisTable,
				Ordinal";

                using (var rd = cmd.ExecuteReader())
                {
                    while (rd.Read())
                    {
                        var name = Convert.ToString(rd["Name"]);
                        var thisTableID = Convert.ToString(rd["ThisTable"]);
                        var otherTableID = Convert.ToString(rd["OtherTable"]);
                        var thisColumnName = Convert.ToString(rd["ThisColumn"]);
                        var otherColumnName = Convert.ToString(rd["OtherColumn"]);

                        var thisTable = (from t in tables where t.ID == thisTableID select t.Table).Single();
                        var otherTable = (from t in tables where t.ID == otherTableID select t.Table).Single();
                        var thisColumn = (from c in columns where c.ID == thisTableID && c.Column.ColumnName == thisColumnName select c.Column).Single();
                        var otherColumn = (from c in columns where c.ID == otherTableID && c.Column.ColumnName == otherColumnName select c.Column).Single();

                        if (thisTable.ForeignKeys.ContainsKey(name) == false)
                            thisTable.ForeignKeys.Add(name, new ForeignKey { KeyName = name, MemberName = name, OtherTable = otherTable });

                        var key = thisTable.ForeignKeys[name];

                        key.ThisColumns.Add(thisColumn);
                        key.OtherColumns.Add(otherColumn);
                    }
                }
            }

            var qc =
                from c in columns
                group c by c.ID into gr
                join t in tables on gr.Key equals t.ID
                select new { t.Table, gr };

            foreach (var c in qc)
            {
                foreach (var col in from col in c.gr orderby col.Column.ID select col.Column)
                    c.Table.Columns.Add(col.ColumnName, col);

                if (c.Table.Owner == "dbo")
                {
                    c.Table.Owner = null;
                    Tables.Add(c.Table.TableName, c.Table);
                }
                else
                {
                    Tables.Add(c.Table.Owner + "." + c.Table.TableName, c.Table);
                }
            }

            {

                /*
                  var mssqlAfterWriteTableProperty = AfterWriteTableProperty;

                                    AfterWriteTableProperty = tt =>
                                    {
                                        mssqlAfterWriteTableProperty(tt);

                                        tt.WriteLine(@"
                #region FreeTextTable

                public class FreeTextKey<T>
                {
                    public T   Key;
                    public int Rank;
                }

                class FreeTextTableExpressionAttribute : TableExpressionAttribute
                {
                    public FreeTextTableExpressionAttribute()
                        : base("""")
                    {
                    }

                    public override void SetTable(SqlTable table, MemberInfo member, IEnumerable<Expression> expArgs, IEnumerable<ISqlExpression> sqlArgs)
                    {
                        var aargs  = sqlArgs.ToArray();
                        var arr    = ConvertArgs(member, aargs).ToList();
                        var method = (MethodInfo)member;
                        var sp     = new MsSql2008SqlProvider();

                        {
                            var ttype  = method.GetGenericArguments()[0];
                            var tbl    = new SqlTable(ttype);

                            var database     = tbl.Database     == null ? null : sp.Convert(tbl.Database,     ConvertType.NameToDatabase).  ToString();
                            var owner        = tbl.Owner        == null ? null : sp.Convert(tbl.Owner,        ConvertType.NameToOwner).     ToString();
                            var physicalName = tbl.PhysicalName == null ? null : sp.Convert(tbl.PhysicalName, ConvertType.NameToQueryTable).ToString();

                            var name   = sp.BuildTableName(new StringBuilder(), database, owner, physicalName);

                            arr.Add(new SqlExpression(name.ToString(), Precedence.Primary));
                        }

                        {
                            var field = ((ConstantExpression)expArgs.First()).Value;

                            if (field is string)
                            {
                                arr[0] = new SqlExpression(field.ToString(), Precedence.Primary);
                            }
                            else if (field is LambdaExpression)
                            {
                                var body = ((LambdaExpression)field).Body;

                                if (body is MemberExpression)
                                {
                                    var name = ((MemberExpression)body).Member.Name;

                                    name = sp.Convert(name, ConvertType.NameToQueryField).ToString();

                                    arr[0] = new SqlExpression(name, Precedence.Primary);
                                }
                            }
                        }

                        table.SqlTableType   = SqlTableType.Expression;
                        table.Name           = ""FREETEXTTABLE({6}, {2}, {3}) {1}"";
                        table.TableArguments = arr.ToArray();
                    }
                }

                [FreeTextTableExpressionAttribute]
                public Table<FreeTextKey<TKey>> FreeTextTable<TTable,TKey>(string field, string text)
                {
                    return this.GetTable<FreeTextKey<TKey>>(
                        this,
                        ((MethodInfo)(MethodBase.GetCurrentMethod())).MakeGenericMethod(typeof(TTable), typeof(TKey)),
                        field,
                        text);
                }

                [FreeTextTableExpressionAttribute]
                public Table<FreeTextKey<TKey>> FreeTextTable<TTable,TKey>(Expression<Func<TTable,string>> fieldSelector, string text)
                {
                    return this.GetTable<FreeTextKey<TKey>>(
                        this,
                        ((MethodInfo)(MethodBase.GetCurrentMethod())).MakeGenericMethod(typeof(TTable), typeof(TKey)),
                        fieldSelector,
                        text);
                }

                #endregion"
                );

                                    };

                 */
            }
        }

        public List<Table> GetBOItemsByService(Int32 ServiceIndex)
        {
            List<Table> rez = new List<Table>();
            foreach (var pop in Tables.Values)
                if (pop.TableName.Contains("_S" + ServiceIndex.ToString() + "_"))
                    rez.Add(pop);
            LoadItemsBOModelInfo(rez, ServiceIndex);
            return rez;
        }
        public List<ServiceModelJInf> ServiceModels
        { get; set; }
        void LoadItemsBOModelInfo(List<Table> tlist, Int32 ServiceIndex)
        {
            var serviceModels = DALGenerator.Current.SystemDesc.ServiceModels;

            foreach (Table pop in tlist)
            { 
                pop.InitBOModelInfo(serviceModels);
            }
        }


        public List<Table> FilterTables(List<Table> tlist)
        {
            CheckfilterSettings();

            return (from t in tlist
                    where IsTableFiltered(t)
                    orderby t.TableName
                    select t).ToList();
        }

        public void CheckfilterSettings()
        {
            var serviceModels = DALGenerator.Current.SystemDesc.ServiceModels;

            //Корректируем настройки в зависимости от задачи
            switch (Stgs.GetItem<DomainServRoleFiltration>(AllSettingsEn.FiltrationType))
            {
                case DomainServRoleFiltration.AllByDomainService_2:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.DatasByDomainService_6:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes,
                       //new List<BORoleEn>() { BORoleEn.NavigationView });
                       new List<BORoleEn>() { BORoleEn.NavigationView });
                    break;
                case DomainServRoleFiltration.RegistersByDomainService_3:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes,
                       new List<BORoleEn>() { BORoleEn.Register });
                    break;
                case DomainServRoleFiltration.VoByDomainService_4:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes,
                       new List<BORoleEn>() { BORoleEn.VObject });
                    break;
                case DomainServRoleFiltration.VoRegistersByDomainService_5:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes,
                       new List<BORoleEn>() { BORoleEn.Register, BORoleEn.VObject });
                    break;
                case DomainServRoleFiltration.TableDatasByDomainService_8:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, false);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.SetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes,
                       new List<BORoleEn>() { BORoleEn.DataTable });
                    Stgs.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.ViewDatasbyDomainService_7:
                    Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Clear();
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Clear();
                    Stgs.SetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes,
                       //new List<BORoleEn>() { BORoleEn.NavigationView });
                       new List<BORoleEn>() { BORoleEn.NavigationView });
                    break;
                case DomainServRoleFiltration.TableNotDatasByDomainService_9:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    Stgs.SetBool(AllSettingsEn.DomainViewFiltration, true);
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    Stgs.SetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes,
                       new List<BORoleEn>() { BORoleEn.Register });
                    Stgs.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes).Clear();
                    break;
                case DomainServRoleFiltration.Custom:
                    if (Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Domain");
                    if (serviceModels.Count() == 0)
                        throw new Exception("DOMAIN MODEL NAME CONVENTION SETTINGS: Empty Services");
                    break;
            }
        }

        public bool IsTableFiltered(Table t)
        {
            var serviceModels = DALGenerator.Current.SystemDesc.ServiceModels;

            return
                ((Stgs.GetList(AllSettingsEn.DomainTableEntitiesPrefixes).Contains(t.TableName.Split(new char[] { '_' })[0]) &&
                  (Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Count == 0 ||
                    Stgs.GetList<BORoleEn>(AllSettingsEn.TableEntities_Suffixes).Contains(t.BOInfo.Roles[0]))) ||
                 (Stgs.GetBool(AllSettingsEn.DomainViewFiltration) &&
                  t.BOInfo.IsView &&
                  (Stgs.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes).Count == 0 ||
                    Stgs.GetList<BORoleEn>(AllSettingsEn.ViewEntities_Suffixes).Contains(t.BOInfo.Roles[0])))) &&
                (serviceModels.Any(s => s.ID == t.BOInfo.ServiceModelIndex));
        }



    }

}
