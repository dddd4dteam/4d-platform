﻿using DDDD.DAL.Generators.Meta; 
using DDDD.DAL.Generators.Settings;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Resources;
using System.Resources.NetStandard;
using System.Runtime.Versioning;

namespace DDDD.DAL.Generators
{
    /// <summary>
    ///  Resources Builder - resx files generate/ build
    /// </summary>
    public class ResourceBuilder : IResourceBuilder
    {
        public ResourceBuilder( ) 
        {  }

        /// <summary>
        /// Resource Writer
        /// </summary>
        ResXResourceWriter  RW //ResX
        { get; set; }

        ResXResourceReader  RR
        { get; set; }

        bool ResourceFileContentIsIncorrect
        { get; set; }
        Dictionary<string, string> Rescs
        { get; set; } = new Dictionary<string, string>();

        /// <summary>
        ///   DAL Generation settings
        /// </summary>
        GeneratorSettingsContainer Stgs
        { get { return DALGenerator.Current.Settings; } }


        /// <summary>
        /// Initialize varialbles to Resource writing 
        /// </summary>
        /// <param name="ServiceName"></param>
        /// <param name="path"></param>
        public void InitRes(string ServiceName, string path = "")
        {   
            if (path == "")
                //path = Path.GetDirectoryName(Host.TemplateFile);
                throw new InvalidOperationException("Init Res");

            if (Directory.Exists( path) == false)
            {  Directory.CreateDirectory(path);
            }
            var pathEnd = path + "\\" + ServiceName + ".resx";
            //RSet = new ResourceSet(pathEnd);
            RR = new ResXResourceReader(pathEnd);
            ReadResources();

            RW = new ResXResourceWriter(pathEnd);
            
        }

        private void ReadResources()
        {
            IDictionaryEnumerator enmrtr = null;
            try
            {
                 enmrtr = RR.GetEnumerator();
            }
            catch (Exception )
            {
                ResourceFileContentIsIncorrect = true;
                return;
            }

            // FILE CONTENT CORRECT 
            while (enmrtr.MoveNext())
            {
                if (enmrtr.Value != null)
                {
                    Rescs.Add(enmrtr.Key.S(), enmrtr.Value.S());
                }
                else
                    Rescs.Add(enmrtr.Key.S(), "");

            }
        }


        /// <summary>
        /// Initialize varialbles to Resource writing
        /// </summary>
        /// <param name="ServiceNum"></param>
        /// <param name="path"></param>
        public void InitRes(int ServiceNum, string path = "")
        {
            //internal
            var generator = DALGenerator.Current;

            InitRes(generator.SystemDesc.ServiceModels.
                First(s => s.ID == ServiceNum).Name, path);
        }
        
        
        const string BoNameLoc = nameof(BoNameLoc);
        const string BoDescLoc = nameof(BoDescLoc);
        const string BoPropElmNameLoc = nameof(BoPropElmNameLoc);
        const string BoPropElmDescLoc = nameof(BoPropElmDescLoc);

        /// <summary>
        /// Add BObject meta table to Resource generation List 
        /// </summary>
        /// <param name="tab"></param>
        public void AddBOtoRes(Table tab)
        {
            if (tab.BOInfo.Roles[0] == (BORoleEn.VObject) ||
                tab.BOInfo.Roles[0] == (BORoleEn.Register) ||
                (Stgs.GetBool(AllSettingsEn.GenerateResForAllViews) &&
                 tab.IsView))
            {

                //RW.AddResource("BoNameLoc\\" + tab.TableName, tab.TableName);
                //RW.AddResource("BoDescLoc\\" + tab.TableName, tab.TableName);
                //foreach (Column pop in tab.Columns.Values)
                //{
                //    RW.AddResource("BoPropElmNameLoc\\" + tab.TableName + "|" + pop.MemberName,
                //                   tab.TableName + " - " + pop.MemberName);
                //    RW.AddResource("BoPropElmDescLoc\\" + tab.TableName + "|" + pop.MemberName,
                //                   tab.TableName + " - " + pop.MemberName);
                //}


                var reskey = $"{BoNameLoc}\\{tab.TableName}" ;
                if ( Rescs.ContainsKey(reskey) == false
                    || ( Rescs.ContainsKey(reskey) && string.IsNullOrEmpty(Rescs[reskey])  )
                   ) 
                {
                    RW.AddResource(reskey, tab.TableName);
                }
                else if(Rescs.ContainsKey(reskey))
                    RW.AddResource(reskey, Rescs[reskey]);

                reskey = $"{BoDescLoc}\\{tab.TableName}";
                if (Rescs.ContainsKey(reskey) == false
                   || (Rescs.ContainsKey(reskey) && string.IsNullOrEmpty(Rescs[reskey]))
                  )
                {
                    RW.AddResource(reskey, tab.TableName);
                }
                else if (Rescs.ContainsKey(reskey))
                    RW.AddResource(reskey, Rescs[reskey]);

                foreach (Column pop in tab.Columns.Values)
                {
                    reskey = $"{BoPropElmNameLoc}\\{tab.TableName}|{pop.MemberName}";
                    if (Rescs.ContainsKey(reskey) == false
                        || (Rescs.ContainsKey(reskey) && string.IsNullOrEmpty(Rescs[reskey]))
                       )
                    {
                        RW.AddResource(reskey, tab.TableName + " - " + pop.MemberName); 
                    }
                    else if (Rescs.ContainsKey(reskey))
                        RW.AddResource(reskey, Rescs[reskey]);
                                        
                    reskey = $"{BoPropElmDescLoc}\\{tab.TableName}|{pop.MemberName}";
                    if (Rescs.ContainsKey(reskey) == false
                       || (Rescs.ContainsKey(reskey) && string.IsNullOrEmpty(Rescs[reskey]))
                      )
                    {
                        RW.AddResource(reskey, tab.TableName + " - " + pop.MemberName); 
                    }
                    else if (Rescs.ContainsKey(reskey))
                        RW.AddResource(reskey, Rescs[reskey]);
                }

            }
        }


        /// <summary>
        /// Close Resource Writer - use this method when all resources was generated
        /// </summary>
        public void CloseRes()
        {
            RW.Close();
        }
    }
}
