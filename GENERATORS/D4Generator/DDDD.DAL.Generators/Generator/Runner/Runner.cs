﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

using DDDD.DAL.Generators;
using DDDD.DAL.Generators.Meta;
 
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators
{
     
    public class Runner : IRunner
    {

        #region --------------- CTOR ------------------
        public Runner( )
        {  }




        #endregion --------------- CTOR ------------------

        #region -------------- PROPERTIES----------------

        /// <summary>
        /// Current  DALGenerator 
        /// </summary>
        public DALGenerator GNT
        { get { return DALGenerator.Current; } }

        /// <summary>
        ///   DAL Generator settings- vollected from input settings file [GenerationSettings.stgs] 
        /// </summary>
        public GeneratorSettingsContainer Stgs
        { get { return GNT.Settings; } }



        #region -------- JSON Serialize OPTIONS --------------

        /// <summary>
        /// Run generator with settings that will be try to find in 
        ///  [GenerationSettings.jstg] -  in the same dir as executable Assembly. 
        /// </summary>
        public string RunSettingsFileName
        { get; set; } = "GenerationSettings.jstg";

        const string StgExt = "jstg";
        const string StgSearchPtrn = "*.jstg";
        JsonSerializerOptions JsSerializeOptions
        { get; set; } = CreateJsonSerializeOptions();

        private static JsonSerializerOptions CreateJsonSerializeOptions()
        {
            // JSON SERIALIZATION CONVERTERS AND OPTIONS
            var options = new JsonSerializerOptions() { WriteIndented = true };
            var keysConverter = new StgKeyJsonConverter();
            options.Converters.Add(keysConverter);
            var valTypeConverter = new StgValTypeJsonConverter();
            options.Converters.Add(valTypeConverter);
            return options;
        }

        List<int> RunIterationIDs
        { get; set; } = new List<int>();


        /// <summary>
        ///  Run Entities
        /// </summary>
        List<RunIterationJElm> RunIterations
        { get; set; } = new List<RunIterationJElm>();


        #endregion -------- JSON Serialize OPTIONS --------------
         


        #endregion  -------------- PROPERTIES----------------

       
        /// <summary>
        /// Generate with using setting Files
        /// </summary>
        /// <param name="args"></param>
        public void Generate(string[] args)
        {            
            if (TryGetRunIterationIDs(args) == false)
            {// log settings not found
                return;
            }

            if (TryLoadSettingsFiles() == false)
            {// log settings not found
                return;
            }

            //for each in  ID get runIteration and Run Generator
            for (int i = 0; i < RunIterationIDs.Count; i++)
            {
                var runIterationStgs = RunIterations.Where(rm => rm.ID == RunIterationIDs[i])
                                                    .FirstOrDefault();
                if (runIterationStgs == null)
                {// log settings not found
                    return;
                }

                //  Reinit Generator with  runIterationStgs
                if (TryReinitGeneratorSettings(runIterationStgs) == false)
                {// log settings not found
                    return;
                }

                // generate target Blocks  from settings
                GNT.Generate(); 
            } 
        }

         bool TryLoadSettingsFiles()
        {
            try
            {  
                var settingsDirPath = Path.Combine(DALGenerator.Current.Up1LevelDir.FullName, "Settings");
                var runIterationStgFiles = Directory.GetFiles(settingsDirPath, StgSearchPtrn);
                for (int i = 0; i < runIterationStgFiles.Length; i++)
                {
                    var iFileText = File.ReadAllText(runIterationStgFiles[i]);
                    var runIteration = JsonSerializer.Deserialize<RunIterationJElm>(iFileText, JsSerializeOptions);
                    RunIterations.Add(runIteration);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

         bool TryGetRunIterationIDs(string[] args)
        {         
            // trying to get Run Iteration IDs from first parameter in command line
            var idsString = args[0];
            var idsListStr =  idsString.SplitNoEntries(",");
            for (int i = 0; i < idsListStr.Count; i++)
            {
                var id =   int.Parse(idsListStr[i]);
                RunIterationIDs.Add(id); 
            }
            if (RunIterationIDs.Count > 0) return true;
            else return false; 
        }
         bool TryReinitGeneratorSettings(RunIterationJElm runIterationStgs)
        {
            try
            {
                
                
                //reset Base ServiceModel Class and Type
                ResetSMBaseClassAndType(); // by defaukt ServiceModel Type is Standart 
                // reset  clear Copying  After Generate  functionality
                GNT.AfterGenerateModel = null;

                foreach (var stg  in runIterationStgs.RunSettings)
                {
                    stg.ReinitValueFromJson();
                    Stgs[stg.StgKey] = stg;                    
                }



                GNT.SelectMDLoader();//current MD Loader
                GNT.SelectRender(); //current Render

                //change ServiceModel                
                GNT.SelectServiceModel(runIterationStgs.ID);

                // Set Copying after Generate functionality if such setting exist  
                if (string.IsNullOrEmpty(
                        Stgs.GetString(AllSettingsEn.OutPath_TargetProjectCopyDir)
                        ) == false
                   )
                {
                    GNT.AfterGenerateModel = () =>
                    {
                        CopyToTargetDir();
                    };
                }
               
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        private void ResetSMBaseClassAndType()
        {
            Stgs.SetBool(AllSettingsEn.ServiceModelType, true);
            Stgs.SetString(AllSettingsEn.SpecificServiceModelBaseClass, null );
        }

        void CopyToTargetDir()
        {  
            var svcModelName =  Stgs[AllSettingsEn.ServiceModelName]
                                            .GetValAsString();
            var currRenderKey = DALGenerator.Current.CurrentRender.Name;

            var generatedSvcModelDir = Path.Combine(
                         DALGenerator.Current. Up1LevelDir.FullName
                                   , "ServiceModels"
                                   , currRenderKey
                                   , svcModelName);

            //var generatedSvcModelDir =  Stgs[AllSettingsEn.OutPath_GeneratedDir]
                                           // .GetValAsString();  
            var targetProjectCopyDir =  Stgs[AllSettingsEn.OutPath_TargetProjectCopyDir]
                                            .GetValAsString();

            if (Directory.Exists(generatedSvcModelDir) == false)
            { return; }

            var targetSvcModelDir = Path.Combine(targetProjectCopyDir, svcModelName);
            if (Directory.Exists(targetSvcModelDir) == false)
            {  Directory.CreateDirectory(targetSvcModelDir);
            }

            var sourceDir = new DirectoryInfo(generatedSvcModelDir);
            foreach (var fl in sourceDir.GetFiles())
            {// copy to   targetSvcModelDir
                 var flSourcePath = fl.FullName;
                var flDestPath = Path.Combine(targetSvcModelDir , fl.Name);

                File.Copy( flSourcePath, flDestPath, overwrite:true);
            }
        }

        public void WriteDefaultSettings(bool isSpecificSvcModel = true)
        {
           var svcGenerationBlocks = new List<GenerationBlockEn>()
                {  GenerationBlockEn.ServiceModelClass,
                 GenerationBlockEn.ServiceModelClasses,
                 GenerationBlockEn.ResourcesFile
                }.ToListString();

            var mdLoaderKey = nameof(MSSqlSrvMetaDataLoader);
            var renderKey = nameof(D4DALCSRender);

            var serviceName = "S1_Geography"; 
            var serviceIndex = NamingConvention.GetServiceModelIndex(serviceName);
            var svcModelBaseClass = "HierarchyServiceModel";
            var svcNamespace = "DDDD.CRM.DOM.DAL";
            var svcTablesDomainPrefix = "crs";

            var connectString = "Data Source=A3;Initial Catalog=CRM_Cersanit2_test3;Integrated Security=True";

            //var inputSystemDescription = @"d:\DDDD\GENERATORS\Composition\Input\D4\Models\SystemCompositionElements.cpmdl";
            var generatedDir = @"d:\DDDD\GENERATORS\Composition\output\D4\" + serviceName + "\\";
            var targetProjectCopyDir = @"D:\DDDD\GENERATORS\TEST\DDDD.CORE.DAL.TEST.NETF\";

            var genFileName = generatedDir + $"S{serviceIndex.S()}.generated.cs";
            var resFileName = generatedDir + $"{serviceName}.resx";
                     
            var targetEntities = new List<string>()
                {"crs_S1_GeographyItem" };
              
            // new Run Iteration for Service Model with ID = 1
            var runIteration = new RunIterationJElm();
            runIteration.ID = 1;
            runIteration.Description = $"Service Model  with id [{ runIteration.ID.S()}] generation  settings ";

            ////  AllSettingsEn.CurrentMDLoader
            runIteration.AddItem(
                 AllSettingsEn.CurrentMDLoader 
                , new List<string>() {  "[CurrentServiceSettings]aspect,  [Type:  string]"
                                      , "  Meta Data Loader Key"                                     
                                     }
                , StgValTypeEn.StringTp
                , mdLoaderKey);

            ////  AllSettingsEn.CurrentRender
            runIteration.AddItem(
                 AllSettingsEn.CurrentRender 
                , new List<string>() {  "[CurrentServiceSettings]aspect,  [Type:  string]"
                                      , " Code Template Render Key"
                                     }
                , StgValTypeEn.StringTp
                ,renderKey);

            ////  AllSettingsEn.ServiceGenerationBlocks
            runIteration.AddItem(
                 AllSettingsEn.ServiceGenerationBlocks
                ,new List<string>() {  "[CurrentServiceSettings]aspect,  [Type: List{string}]"
                                     , "Service  Model   Generation blocks " 
                                     , "1  ServiceModelClasses /2  ServiceModelClass"
                                     , "/3  DataContextClass   /4  ResourcesFile   "
                                    }  
                , StgValTypeEn.ListStringTp
                , svcGenerationBlocks ); 


            //  AllSettingsEn.ServiceModelName
            //runIteration.AddItem(                 
            //     AllSettingsEn.ServiceModelName
            //    , new List<string>() {
            //        "[CurrentServiceSettings]aspect,  [Type:string]  -"
            //        ,"Sercive Model Name."
            //        ,"As usually Service Model Name equal to template file name."
            //        ," Ex: S1_Geography  "
            //                         }                
            //    ,   StgValTypeEn.StringTp
            //    , serviceName);

            //  AllSettingsEn.ServiceTargetEntityName
            //runIteration.AddItem(
            //     AllSettingsEn.ServiceTargetEntityName
            //    , new List<string>() {
            //         " [CurrentServiceSettings]aspect,  [Type:List{string}]  -"
            //        ," Service  Model Target Entities, should be pointed by hads"
            //                         } 
            //    , StgValTypeEn.ListStringTp
            //    , targetEntities);

            // AllSettingsEn.UseBaseEntityClass
            runIteration.AddItem(
                  AllSettingsEn.UseBaseEntityClass
                , new List<string>() {
                 " [EntitiesBaseClassSettings]aspect,  [Type:bool]  - "
                 ," Use Base Class for Service Model Entities "
                                     }
                , StgValTypeEn.BoolTp
                , true);

            // AllSettingsEn.UsePropPrimaryKeyAttribute
            runIteration.AddItem(
                  AllSettingsEn.UsePropPrimaryKeyAttribute
                , new List<string>() {
                    "[BLDALModelSettings]aspect, [Type:bool]  -  "  
                    ,"Use  PrimaryKey attribute  for properties"
                                     } 
                , StgValTypeEn.BoolTp
                , true);  
          
            // AllSettingsEn.DomainPrefix
            runIteration.AddItem(
                 AllSettingsEn.DomainPrefix
               , new List<string>() {
                                "  [CurrentServiceSettings]aspect,  [Type:string]  = Domain Prefix in each Contract Name  "
                                ," Ex: [crs] "
                                    }                 
               , StgValTypeEn.StringTp
               , svcTablesDomainPrefix );

            // AllSettingsEn.Namespace
            runIteration.AddItem(
                 AllSettingsEn.Namespace
               , new List<string>() {
                                 " [CurrentServiceSettings]aspect,  [Type:string]  = Domain Prefix in each Contract Name"
                                ," Ex: [DDDD.CRM.DOM.DAL]"
                                    }                  
               , StgValTypeEn.StringTp
               , svcNamespace);

            // AllSettingsEn.ConnectionString
            runIteration.AddItem(
                 AllSettingsEn.ConnectionString
               , new List<string>() {
                                 "  [CurrentServiceSettings]aspect,  [Type: string]  -"
                                ,"  Connection string  to DB"
                                    } 
               , StgValTypeEn.StringTp
               , connectString );

            var standartSvcModelType = !isSpecificSvcModel;
            runIteration.AddItem(
                 AllSettingsEn.ServiceModelType
               , new List<string>() {
                                     "[CurrentServiceSettings]aspect,  [Type:bool]  -"
                                    ,"ServiceModelType - standart or specific."
                                    ,"true - Is standart ServiceModel/ false - Specific Service Model - like "
                                    }                 
               , StgValTypeEn.BoolTp
               , standartSvcModelType);

            if (standartSvcModelType == false)
            {  runIteration.AddItem(
               AllSettingsEn.SpecificServiceModelBaseClass
               , new List<string>() {
                                     "[CurrentServiceSettings]aspect,  [Type:string]  -"
                                    ,"SpecificServiceModelBaseClass - "
                                    ,"for Ex: HierarchyServiceModel as base class for custom ServiceModel class"
                                  }
               , StgValTypeEn.StringTp
               , svcModelBaseClass);
            }
           

            //// generate resources for all tables and views
            runIteration.AddItem(
               AllSettingsEn.GenerateResForAllViews 
             , new List<string>() {
                                    " [CurrentServiceSettings]aspect, [Type: bool]  "
                                   ," Generate resources for all views and tables  "
                                  }                 
             , StgValTypeEn.BoolTp
             , true);

            // AllSettingsEn.InPath_SystemDescription
            //runIteration.AddItem(
            //      AllSettingsEn.InPath_SystemDescription
            //    , new List<string>() {
            //                        " [InOutPathsSettings]aspect,  [Type:string]  -"
            //                        ," Input file path of System Descriotion file: "
            //                        ," Services /ServiceManagers  names , Indexes and Descriptions..."
            //                         }                 
            //    , StgValTypeEn.StringTp
            //    , inputSystemDescription);

            // AllSettingsEn.OutPath_GeneratedDir
            //runIteration.AddItem(
            //     AllSettingsEn.OutPath_GeneratedDir
            //   , new List<string>() {
            //                         "[InOutPathsSettings]aspect,  [Type:string]  -"
            //                        ,"Output Dir path for Service Model generated files: Classes file- "
            //                        ," table/view classes ;custom ServiceModel class; Resource file."
            //                        }                
            //   , StgValTypeEn.StringTp
            //   , generatedDir);


            // AllSettingsEn.OutPath_TargetProjectCopyDir
            runIteration.AddItem(
                 AllSettingsEn.OutPath_TargetProjectCopyDir
               , new List<string>() {
                                     "[InOutPathsSettings]aspect,  [Type:string]  - "
                                    ," Output Dir path where we can copy all  ServiceModel generated files. "
                                    }                
               , StgValTypeEn.StringTp
               , targetProjectCopyDir);

            //// use sync before generate
             
            var serializedStr = JsonSerializer.Serialize(runIteration, JsSerializeOptions);

            if (Directory.Exists( generatedDir ) == false )
            {  Directory.CreateDirectory(generatedDir);
            }

            var settingsTestFile = Path.Combine(generatedDir, RunSettingsFileName); 
                        
            File.WriteAllText(settingsTestFile, serializedStr);

        }
        
        public void WriteTestDBServiceModels()
        { 
 
             var sysDescript = new SystemDescriptionJInf();
            sysDescript.AddSvcModel(1, "S1_Geography", true
                                    , " Geography Adresses of Trade Points"
                                    , " and manager car movements"
                                    ); 
            sysDescript.AddSvcModel(2, "S2_Assortment", false
                                    , "  Assortment of  goods"
                                    );
            sysDescript.AddSvcModel(3, "S3_GoodsCategory", false
                                    , " Goods Categirues "
                                   );
            sysDescript.AddSvcModel(4, "S4_Material", false
                                    , " Used in Expositor Materials "
                                   );
            sysDescript.AddSvcModel(5, "S5_Rival", false
                                   , " Market range Rivals  vs current firm"
                                   );
            sysDescript.AddSvcModel(6, "S6_Diler", false
                                   , "  Trade Dilers"
                                   );
            sysDescript.AddSvcModel(7, "S7_Holding", true
                                    , "  Trade  Holdings"
                                   );
            sysDescript.AddSvcModel(8, "S8_Client", false
                                   , " Firm Clents or Trade Points"
                                   );
            sysDescript.AddSvcModel(9, "S9_Meeting", false
                                    , " Meetings of Trade Representers"
                                   );
            sysDescript.AddSvcModel(10, "S10_EkspositorOrder", false
                                    , "Orders for Ekspositors"
                                   );
            sysDescript.AddSvcModel(11, "S11_Report", false
                                    , " Report  about trades"
                                   );
            sysDescript.AddSvcModel(12, "S12_Units", false
                                   , " Measure Units"
                                   );
            sysDescript.AddSvcModel(13, "S13_Grupa", false
                                    , " Goods Groups "
                                   );
            sysDescript.AddSvcModel(14, "S14_Product", false
                                    , " firm Products"
                                   );
            sysDescript.AddSvcModel(15, "S15_Monitoring", false
                                   , " Prices Monitoring"
                                   );
            sysDescript.AddSvcModel(16, "S16_ClientTypology2", false
                                   , "  Clients Typology "
                                   );
            
            var serializedStr = JsonSerializer.Serialize(sysDescript, JsSerializeOptions);
           
            var generatedFile = @"d:\DDDD\GENERATORS\Composition\output\D4\SystemDescription.jcpmd";
             
            File.WriteAllText(generatedFile, serializedStr);


        }

    }
}
