﻿
using DDDD.DAL.Generators.Meta; 
using DDDD.DAL.Generators.Settings;
using System;
using System.Collections.Generic;
using System.Text;

namespace DDDD.DAL.Generators 
{

    /// <summary>
    /// Runner  load settings from file [GenerationSettings.stgs] and run Generator,
    /// after files Generation copy them to targetProject Dirs. 
    /// </summary>
    public interface IRunner
    {
        void Generate(string[] args);
        void WriteDefaultSettings(bool isSpecificSvcModel = true);
        void WriteTestDBServiceModels();


    }



    /// <summary>
    /// Code Writer for DAL classes generator
    /// </summary>
    public interface ICodeWriter
    {

        void Error(string errorMsg);
        void Clear();

        string Result();
        void PopIndent();
        void PushIndent();
        void Write(char charValue, bool useIndent = false);
        void Write(string lineValue, bool useIndent = false);


        void WrtObjStart(bool useIndent = true);
        void WrtObjEnd(bool useIndent = true);
        void WrtObjEnd(string append, bool useIndent = true);

        /// <summary>
        /// Write anonymous Func{object} :   ()=> { [your code here ]   } 
        /// </summary>
        /// <param name="funcCode"></param>
        void WrtFuncObj(string funcCode, bool useIndent = false);


        void Write2LnEmpty();
        void Write3LnEmpty();
        void Write4LnEmpty();
        void WriteLine(char lineChar, bool useIndent = true);
        void WriteLine(string lineValue, bool useIndent = true);
        void WriteLnEmpty();
        void WriteSpace(int len);
        void WrtAttributes(  List<string> attributes);
        void WrtBaseClsDecl(string className, string baseClassName);
        void WrtCls(string className, Action baseClasIfacesCode, Action classCode);
        void WrtClsDecl(string access, bool isPartial, string className, Action baseClasIfacesCode);
        void WrtComment2(string comment);
        void WrtComment3(string comment);
        void WrtInterfacesDecl(bool hasBaseCls, params string[] interfaces);
        void WrtNmspc(string namspace, Action namespaceCode);
        void WrtSummary(string summaryComment);
        void WrtUsing(string usingNamespace);
        void WrtUsings(params string[] usingNamespaces);
        void WriteResultToFile(string generatedFileDir, string genFileOnlyName);
    }

    /// <summary>
    /// Meta Data Loader
    /// </summary>
    public interface IMetaDataLoader
    {
         
        string Name { get; }
        Dictionary<string, Table> Tables { get; set; }
        List<string> TablesForServerOnly { get; set; }

        void CheckfilterSettings();
        List<Table> FilterTables(List<Table> tlist);
        List<Table> GetBOItemsByService(int ServiceIndex);
        bool IsTableFiltered(Table t);
        void LoadMetadata();
    }

    /// <summary>
    ///  Resources Builder - resx files generate/ build 
    /// </summary>
    public interface IResourceBuilder
    {
        void InitRes(int ServiceNum, string path = "");
        void InitRes(string ServiceName, string path = "");
        void AddBOtoRes(Table tab);
        void CloseRes();

    }

    /// <summary>
    ///  Generation Render: custom DAL model classes builder
    /// </summary>
    public interface IRender
    {
        string Name { get; }
        List<string> ServiceTargetEntityNames { get; }

       void RenderModelHeaderText();
       void RenderUsing();
        void RenderDataContextClass(List<Table> tServList);
        void RenderServiceModelClass(ServiceModelJInf servModelCur, List<Table> tServList);
        void RenderTable(Table t, List<Table> tServList);
    }

    /// <summary>
    /// DAL Generator
    /// </summary>
    public interface IDALGenerator
    {
        /// <summary>
        /// Code Writer that can be used in Render components 
        /// to build their code Templates.
        /// </summary>
        ICodeWriter CW { get; }

        /// <summary>
        /// Meta Data Loaders - to load meta data from different DB Engines. 
        /// </summary>
        Dictionary<string, IMetaDataLoader> MetaLoaders { get; }

        /// <summary>
        /// Existed Renders components-  to Generate different output Templates
        /// for Languages and technologies.
        /// </summary>
        Dictionary<string,IRender> Renders { get; }

        /// <summary>
        /// Runner  component - run generate Iterations for different Service Models
        /// </summary>
        IRunner GRunner { get; }
        
        /// <summary>
        /// Resource Building component - build Resource files.
        /// </summary>
        IResourceBuilder ResBuilder { get; }

        /// <summary>
        ///DALGenerator Settings -  used in other conponents too 
        /// </summary>
        GeneratorSettingsContainer Settings { get; }

        /// <summary>
        /// Generate template with inited settings. It's - one iteration 
        /// </summary>
        /// <param name="args"></param>
        void Generate(string[] args);
         
    }

}
