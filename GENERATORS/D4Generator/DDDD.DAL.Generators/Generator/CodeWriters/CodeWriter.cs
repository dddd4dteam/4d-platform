﻿using DDDD.DAL.Generators.Text;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DDDD.DAL.Generators 
{

    public class CodeWriter : ICodeWriter 
    {
        public CodeWriter() 
        { }

        StringBuilder SBuilder = new StringBuilder();



        #region -------------- TAB INDENTS --------------
        static int tabIndentIndx = 0;
        static string tabIndent = "";

        /// <summary>
        /// Push Tab Indent for new Line output
        /// </summary>
        public void PushIndent()
        {
            tabIndentIndx++;
            tabIndent += Sym.TAB;
        }

        /// <summary>
        /// Pop Tab Indent from new Line output
        /// </summary>
        public void PopIndent()
        {
            if (tabIndentIndx > 0)
            {
                tabIndentIndx--;
                tabIndent = "";
                for (int i = 1; i <= tabIndentIndx; i++)
                {   tabIndent+= Sym.TAB;
                }
                //tabIndent  = tabIndent.Substring(0, tabIndent.Length - 2);
            }
        }

        #endregion -------------- TAB INDENTS --------------

        #region ----------- Error/ Clear/ Result -----------------
        public void Error(string errorMsg)
        {
            throw new InvalidOperationException(errorMsg);
        }

        public void Clear()
        {
            SBuilder.Clear();
        }

        public string Result()
        {
            return SBuilder.ToString();
        }
        #endregion ----------- Error/ Clear/ Result -----------------




        #region -----------  WRITE / WRITELINE / WEMPTYLINES  ------------

        /// <summary>
        /// Write string. 
        /// Also we can use or not tab Indent, by default it's false.
        /// </summary>
        /// <param name="lineValue"></param>
        public void Write(string lineValue, bool useIndent = false)
        {
            if (useIndent)
                SBuilder.Append(tabIndent + lineValue);
            else SBuilder.Append(lineValue);
        }

        /// <summary>
        /// Write char 
        /// Also we can use or not tab Indent, by default it's false.
        /// </summary>
        /// <param name="charValue"></param>
        /// <param name="useIndent"></param>
        public void Write(char charValue, bool useIndent = false)
        {
            if (useIndent)
                SBuilder.Append(tabIndent + charValue.S());
            else SBuilder.Append(charValue);
        }


        /// <summary>
        /// Write spaces in current string - N times [len]
        /// </summary>
        /// <param name="generator"></param>
        /// <param name="len"></param>
        public void WriteSpace(int len)
        {
            while (len-- > 0)
                Write(Sym.SPACE);
        }


        /// <summary>
        /// Write Line from string Value
        /// Also we can use or not tab Indent, by default it's true.
        /// </summary>
        /// <param name="lineValue"></param>
        /// <param name="useIndent"></param>
        public void WriteLine(string lineValue, bool useIndent = true)
        {
            if (useIndent)
            {
                SBuilder.Append(tabIndent + lineValue + Sym.NEWLINE);
            }
            else
                SBuilder.Append(lineValue + Sym.NEWLINE);

        }

        /// <summary>
        /// Write Line from char Value
        /// Also we can use or not tab Indent, by default it's true.
        /// </summary>
        /// <param name="lineChar"></param>
        /// <param name="useIndent"></param>
        public void WriteLine(char lineChar, bool useIndent = true)
        {
            if (useIndent)
            {
                SBuilder.Append(tabIndent + lineChar.S() + Sym.NEWLINE);
            }
            else
                SBuilder.Append(lineChar + Sym.NEWLINE);

        }



        /// <summary>
        /// Write empty line
        /// </summary>
        public void WriteLnEmpty()
        {
            WriteLine("");
        }


        /// <summary>
        /// Write 2 empty line 
        /// </summary>
        public void Write2LnEmpty()
        {
            WriteLine("");
            WriteLine("");
        }

        /// <summary>
        /// Write 3 empty line
        /// </summary>
        public void Write3LnEmpty()
        {
            WriteLine("");
            WriteLine("");
            WriteLine("");
        }

        /// <summary>
        /// Write 4 empty line
        /// </summary>
        public void Write4LnEmpty()
        {
            WriteLine("");
            WriteLine("");
            WriteLine("");
            WriteLine("");
        }


        #endregion ---------- WRITE / WRITELINE / WEMPTYLINES  ------------

        /// <summary>
        /// Write Object Start:  [{ pushindent ]
        /// </summary>
        /// <param name="useIndent"></param>
        public void WrtObjStart( bool useIndent = true)
        {            
            WriteLine(Sym.OBJECT_START);
            if (useIndent) PushIndent();
        }

        /// <summary>
        /// Write Object End:   [}popindent]
        /// </summary>
        /// <param name="useIndent"></param>
        public void WrtObjEnd(bool useIndent = true)
        {
            if (useIndent) PopIndent();
            WriteLine(Sym.OBJECT_END);            
        }

        public void WrtObjEnd(string append,  bool useIndent = true)
        {
            if (useIndent) PopIndent();
            WriteLine(Sym.OBJECT_END+ append);
        }

        /// <summary>
        ///  Write anonymous Func{object} :   ()=> { [your code here ]   } 
        /// </summary>
        /// <param name="append"></param>
        /// <param name="useIndent"></param>
        public void WrtFuncObj(string funcCode ,bool useIndent=false)
        {  
            WriteLine("()=> { " + funcCode + " }" , useIndent);
        }

        /// <summary>
        /// Write Comment  :
        ///  <para/>  // {comment}
        /// </summary>
        /// <param name="generator"></param>
        /// <param name="Comment"></param>
        public void WrtComment2(string comment)
        {
            WriteLine($"// {Sym.TAB} {comment}");
        }

        /// <summary>
        /// Write Comment  :
        ///  <para/>  /// {comment}
        /// </summary> 
        /// </summary>
        /// <param name="comment"></param>
        public void WrtComment3(string comment)
        {
            WriteLine($"/// {Sym.TAB} {comment}");
        }

        /// <summary>
        /// Write Symmary  :
        ///  <para/>  /// {summary}
        ///  <para/>  /// [summaryComment]
        ///  <para/>  /// {/summary}
        /// </summary>
        /// <param name="comment"></param>
        public void WrtSummary(string summaryComment)
        {
            WriteLine($"/// <summary>");
            WriteLine($"/// {summaryComment} ");
            WriteLine($"/// </summary>");
        }

        /// <summary>
        /// Write Using:
        /// <para/> using [usingNamespace];
        /// </summary>
        /// <param name="usingNamespace"></param>
        public void WrtUsing(string usingNamespace)
        {
            WriteLine($"using {usingNamespace};");
        }


        /// <summary>
        ///   Write Usings:
        /// <para/> using [usingNamespace];
        /// <para/> ... N times
        /// </summary>
        /// <param name="usingNamespaces"></param>
        public void WrtUsings(params string[] usingNamespaces)
        {
            for (int i = 0; i < usingNamespaces.Length; i++)
            {
                WrtUsing(usingNamespaces[i]);
            }
        }

        /// <summary>
        /// Write Attribute: 
        /// <para/> [ attribValue1, attribValue2 ... ] + NL
        /// </summary>
        /// <param name="attribName"></param>
        public void WrtAttributes(List<string> attributes)
        {
            var LineMaxLen = 61;
            var lines = new List<string>();
            var curLine = "";
            
            for (int i = 0; i < attributes.Count; i++)
            {
                if (string.IsNullOrEmpty(attributes[i])) continue;
                var attrib = attributes[i];
                
                if ((curLine.Length + attrib.Length ) <= LineMaxLen )
                {// we can attach attrib to line
                    // attach attrib as [first] or  [secod and ...]
                    if (curLine.Length > 0)//  [secod and ...] attribute
                    {  curLine += (Sym.COMMA2 + attrib); 
                    }
                    else// first attribute 
                    {  curLine +=  attrib; 
                    }
                }
                else if ((curLine.Length + attrib.Length) > LineMaxLen)
                {   // we need to render attrib line and reset curLine 
                    // add cur attrib   
                    if (curLine.Length > 0 )
                    {  lines.Add(Sym.ARRAY_START + curLine + Sym.ARRAY_END);
                    }
                    curLine = attrib;  
                }                 
            }
            if (curLine.Length > 0)
            {
                lines.Add(Sym.ARRAY_START + curLine + Sym.ARRAY_END);
            }

            // Write Collected attribute lines
            for (int i = 0; i < lines.Count; i++)
            {
                WriteLine(lines[i]);
            }
        }




        /// <summary>
        /// Write Namespace:
        /// <para/>             { 
        /// <para/>                 namespace Code
        /// <para/>             }  
        /// </summary>
        /// <param name="namespaceCode"></param>
        public void WrtNmspc(string namspace, Action namespaceCode)
        {
            WriteLine($"namespace  {namspace} ");
            WriteLine(Sym.OBJECT_START);
            PushIndent();
            namespaceCode();
            PopIndent();
            WriteLine(Sym.OBJECT_END);
        }

        /// <summary>
        /// Write Class code with  classCode Action: 
        /// <para/>             [classDeclaration] [baseClasIfacesCode ] 
        /// <para/>             { 
        /// <para/>                 [class code]
        /// <para/>             }     
        /// </summary>
        /// <param name="className"></param>
        /// <param name="baseClassName"></param>
        /// <param name="classCode"></param>
        public void WrtCls(string className, Action baseClasIfacesCode, Action classCode)
        {
            //PushIndent();
            WrtClsDecl("public", true, className, baseClasIfacesCode);
            WriteLine(Sym.OBJECT_START);
            PushIndent();
            classCode();
            PopIndent();
            WriteLine(Sym.OBJECT_END);
            //PopIndent();
        }

        /// <summary>
        /// Write Class Declaration based on className and baseClassName
        /// </summary>
        /// <param name="className"></param>
        /// <param name="baseClassName"></param>
        public void WrtClsDecl(string access, bool isPartial, string className, Action baseClasIfacesCode) //string baseClassName, params string[] interfaces
        {
            if (isPartial)
            {
                Write($"{access} partial class {className}", true);
            }
            else Write($"{access} class {className}", true);

            baseClasIfacesCode();
            Write(Sym.NEWLINE);

            // base class and interface is custom rendering
            //WrtBaseClsDecl(className, baseClassName);                
            //WrtInterfacesDecl(hasBaseClass,className, interfaces);

        }


        /// <summary>
        /// Write Base Class in Class Declaration
        /// </summary>
        /// <param name="className"></param>
        /// <param name="baseClassName"></param>
        public void WrtBaseClsDecl(string className, string baseClassName)
        {
            if (!string.IsNullOrEmpty(baseClassName))
                Write($" : { baseClassName.Replace("<T>", "<" + className + ">")}");
        }

        /// <summary>
        /// Write interfaces declaration for class :
        /// </para> [:/,] interface1,interface2
        /// </summary>
        /// <param name="hasBaseCls"></param>
        /// <param name="interfaces"></param>
        public void WrtInterfacesDecl(bool hasBaseCls, params string[] interfaces)
        {
            if (hasBaseCls) Write(Sym.COMMA2);
            else Write(Sym.COLON2);

            for (int i = 0; i < interfaces.Length; i++)
            {
                Write(interfaces[i]);
                if (i < interfaces.Length - 1)
                {
                    Write(Sym.COMMA2);
                }
            }
        }
        
        public void WriteResultToFile(string generatedDir, string genFileOnlyName)
        {
            if (Directory.Exists(generatedDir)==false)
            {   Directory.CreateDirectory(generatedDir);
            }
            var genCodeFilePath = Path.Combine( generatedDir , genFileOnlyName);// $"S1.generated.cs";

            if ( File.Exists(genCodeFilePath)  )
            {    File.Delete(genCodeFilePath);   }

            File.WriteAllText(genCodeFilePath, SBuilder.ToString());
            SBuilder.Clear();
        }


    }
}
