﻿ 
using DDDD.DAL.Generators.Settings;
using DDDD.DAL.Generators.Text;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
 
using DDDD.Core.ComponentModel;
using System.IO;
using System.Reflection;
using System.Text.Json;
using DDDD.DAL.Generators.Meta;

namespace DDDD.DAL.Generators
{

    /// <summary>
    ///  Data Access Layer Generator
    /// </summary>
    public partial class DALGenerator : SingletonService<DALGenerator>, IDALGenerator
    {
        #region ------------------------------- PROPERTIES -----------------------------------

        /// <summary>
        /// Tool Executable Directory
        /// </summary>
        public DirectoryInfo ExecDir
        { get; set; }

        /// <summary>
        ///  Parent of ExecDir  Directory
        /// </summary>
        public DirectoryInfo Up1LevelDir
        { get; set; }

        /// <summary>
        ///   DAL Generation settings
        /// </summary>
        public GeneratorSettingsContainer Settings
        { get; private set; }


        /// <summary>
        /// Code Writer  for  DAL Generator 
        /// </summary>
        public ICodeWriter CW
        { get; private set; }


        /// <summary>
        /// SUBD Meta Data Loader for  DAL Generator.
        /// By default is MSSqlSrvMetaDataLoader - to get meta tables from  MS SqlServer 
        /// </summary>
        public Dictionary<string, IMetaDataLoader> MetaLoaders
        { get; private set; } = new Dictionary<string, IMetaDataLoader>();

        public IMetaDataLoader CurrentMDLoader
        { get; set; } 

        internal void SelectMDLoader()
        {
            var mdLoaderKey = Settings.GetString(AllSettingsEn.CurrentMDLoader);
            if (MetaLoaders.ContainsKey(mdLoaderKey))
            {
                //select Meta Data Loader
                CurrentMDLoader  = MetaLoaders[mdLoaderKey];
                return;
            }
            throw new InvalidOperationException($"Meta Data loader with key {mdLoaderKey} not exist. ");

        }

        /// <summary>
        /// Resources Builder - resx files generate/ build
        /// </summary>
        public IResourceBuilder ResBuilder
        { get; private set; }


        /// <summary>
        /// Generation Render: custom DAL model classes builder 
        /// </summary>
        public Dictionary<string, IRender> Renders
        { get; private set; } = new Dictionary<string, IRender>();

        public IRender CurrentRender
        { get; set; }
        public DirectoryInfo CurrentRenderDir
        { get; set; }

        internal void SelectRender()
        {
            var renderKey = Settings.GetString(AllSettingsEn.CurrentRender);
            if (Renders.ContainsKey(renderKey))
            {
                //select Render
                CurrentRender = Renders[renderKey];

                // set Render directory
                var dirPath = Path.Combine(Up1LevelDir.FullName, CurrentRender.Name);
                if (Directory.Exists(dirPath) == false)
                {
                    CurrentRenderDir = new DirectoryInfo(dirPath);  
                    return;
                }
            }
            throw new InvalidOperationException($"Render with key {renderKey} not exist. ");   

        }



        internal void SelectServiceModel(int id)
        {
            var sm = SystemDesc.ServiceModels
                        .Where(sm => sm.ID == id).FirstOrDefault();
            Settings.SetItem<int>(AllSettingsEn.ServiceModelIndex, sm.ID);
            Settings.SetString(AllSettingsEn.ServiceModelName, sm.Name);
                   
        }

        /// <summary>
        /// Generator Runner
        /// </summary>
        public IRunner GRunner
        { get; private set; }  

       

        

        /// <summary>
        ///  System Description with ServiceModel Names by IDs
        /// </summary>
        public SystemDescriptionJInf SystemDesc
        { get; set; }  
              

        #endregion -------------------------------  PROPERTIES -----------------------------------



        #region  -----------------------GENERATE DELEGATES -----------------------------------

        // Действия -Перед генерацией модели/После генерации модели
        public Action BeforeGenerateModel = () => { };
        public Action AfterGenerateModel = () => { };


        #endregion ----------------------- GENERATE DELEGATES -----------------------------------

        /// <summary>
        /// Initialize Service
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            
            InitializeGenerator();
        }

        /// <summary>
        /// Initialize Generator default Settings and set/change default Components 
        /// </summary>
        /// <param name="codewriter"> Component CodeWriter- code writing engine </param>
        /// <param name="metaLoader"> Component  MetaDataLoader-SQLServer/other RDBMS tables and views description loader </param>
        /// <param name="resourcesBuidler"> Component ResourceBuilder- building resource file </param>
        /// <param name="render"> Component Render(D4 DAL /other DAL NH/...)</param>
        /// <param name="runner">Component Runner-read input settings/run generation/ and copy generated files to target Dir</param>
        public void InitializeGenerator()
        {
            Settings = new DALGeneratorSettings();
            Settings.InitAspects();
            Settings.SetDefaultsSettingsByScenario(ScenariosEn.ServerSettingsScenario
            , null// connection String
            , true,// generate Resource for All Tables Views
            false); // use Sync Check before generation
            var execAssembly = Assembly.GetExecutingAssembly();
            var execDirPath = execAssembly.Location.Replace($"{execAssembly.GetName().Name}.dll","");
            ExecDir = new DirectoryInfo(execDirPath);             
            Up1LevelDir = ExecDir.Parent;

            LoadSystemDescription();

            CW = new CodeWriter();

            var msSqlMetaDataLoader = new MSSqlSrvMetaDataLoader( );
            MetaLoaders.Add(msSqlMetaDataLoader.Name, msSqlMetaDataLoader);
                      
            var d4DALCSRender = new D4DALCSRender();
            Renders.Add(d4DALCSRender.Name, d4DALCSRender);
             
            ResBuilder = new ResourceBuilder();

            GRunner = new Runner();           
        }

        private void LoadSystemDescription()
        {
            var systemDescPath = Path.Combine(
                Up1LevelDir.FullName, "SystemDesc", "SystemDescription.jcpmd");
            if (File .Exists(systemDescPath) == false)
            {  throw new InvalidOperationException(
                $"file of System Descriptions do not exists-{systemDescPath} ");
            }

            var iFileText = File.ReadAllText(systemDescPath);
            SystemDesc = JsonSerializer.Deserialize<SystemDescriptionJInf>(iFileText );
            
        }

        /// <summary>
        /// Generate  all ServiceModels by input Settings per each Service Model. 
        /// </summary>
        /// <param name="svcIDs"></param>
        public void Generate(string[] args)
        {
            GRunner.Generate(args); 
        }

        /// <summary>
        ///  Generate CSharp DAL Service Model classes: tables/ views/ServiceModels/resources
        /// </summary>
        /// <returns></returns>
        public void Generate()
        {
            CW.Clear();// clear if secong timegeneration runned

            var GenerationBlocksValue = (Settings[AllSettingsEn.ServiceGenerationBlocks].Value
                                            as List<string>).ToGenBlocks();
            var curRenderKey = Settings.GetString(AllSettingsEn.CurrentRender);

            var ServiceName = Settings.GetString(AllSettingsEn.ServiceModelName);
            var ServiceNum = NamingConvention.GetServiceModelIndex(ServiceName);
            var genFileName = $"S{ServiceNum}.generated.cs";

            //var inPathSystemDesc = Settings.GetString(AllSettingsEn.InPath_SystemDescription);
            var outGenClassesDir = Path.Combine(
                                    Up1LevelDir.FullName
                                    , "ServiceModels"
                                    , curRenderKey                                   
                                    ,  ServiceName );
            var outPathRescs = outGenClassesDir;

            if (Settings.GetString(AllSettingsEn.ConnectionString) != null)
                Settings.SetString(AllSettingsEn.ConnectionString,
                    Settings.GetString(AllSettingsEn.ConnectionString).Trim());

            //if (Settings.GetString(AllSettingsEn.DataContextName) != null)
            //    Settings.SetString(AllSettingsEn.DataContextName,
            //        Settings.GetString(AllSettingsEn.DataContextName).Trim());

            if (string.IsNullOrEmpty(Settings.GetString(AllSettingsEn.ConnectionString)))
            { CW.Error("ConnectionString cannot be empty."); return; } 
                         
            CurrentMDLoader.LoadMetadata();

            BeforeGenerateModel();

            CurrentRender.RenderModelHeaderText();
            CurrentRender.RenderUsing();

            CW.WrtNmspc(Settings.GetString(AllSettingsEn.Namespace), () =>
            {
                /// BEGIN NAMESPACE

               var servModelCur = SystemDesc.ServiceModels
                    .First(s => s.ID == ServiceNum);
                foreach (var servModel in SystemDesc.ServiceModels)
                        CurrentMDLoader.GetBOItemsByService(servModel.ID);
                
                //Получаем те таблицы которые нам нужны
                List<Table> tServList = CurrentMDLoader.GetBOItemsByService(ServiceNum);

                if (GenerationBlocksValue.Contains(GenerationBlockEn.ServiceModelClass))
                {
                    CurrentRender.RenderServiceModelClass(servModelCur, tServList);
                }

                if (GenerationBlocksValue.Contains(GenerationBlockEn.ServiceModelClasses) )
                {
                    //initialize resources Building
                    if (GenerationBlocksValue.Contains(GenerationBlockEn.ResourcesFile ))
                    {
                        if (string.IsNullOrEmpty(outPathRescs) == false)
                            ResBuilder.InitRes(ServiceNum, path: outPathRescs);
                    }

                    // Synchronization connect / parallele changing
                    //if (Stgs.GetBool(DataLayerSettingsEn.PerformChecksForSyncAndOffline))
                    //    PrepareForSyncAndOffline(tServList, ServiceNum, ServiceTargetEntityNames, path);
                    List<Table> tFilteredList =CurrentMDLoader.FilterTables(tServList);
                    foreach (var t in tFilteredList)
                    {
                        CW.WriteLnEmpty();
                        CurrentRender.RenderTable(t, tServList);

                        //    table add to resources
                        if (GenerationBlocksValue.Contains(GenerationBlockEn.ResourcesFile))
                        {
                            if (string.IsNullOrEmpty(outPathRescs) == false)
                                ResBuilder.AddBOtoRes(t);
                        } 
                    }

                    if (GenerationBlocksValue.Contains(GenerationBlockEn.ResourcesFile))
                    {
                        if (string.IsNullOrEmpty(outPathRescs) == false)
                            ResBuilder.CloseRes();
                    }
                }

                /// END NAMESPACE
            }
            );
            //Write to files 
            CW.WriteResultToFile(outGenClassesDir , genFileName);

            AfterGenerateModel();
             
        }
         

    }
}
