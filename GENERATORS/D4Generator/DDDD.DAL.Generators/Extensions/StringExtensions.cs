﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics.Contracts;
 
using DDDD.DAL.Generators.Meta;
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators
{

    /// <summary>
    /// String class extensions
    /// </summary>
    public static class StringExtensions
    {

        #region ----------- SETTINGS ENUMS  ----------------
        public static AllSettingsEn GetAllStgKey(this string propName)
        {
            return (AllSettingsEn)Enum.Parse(typeof(AllSettingsEn), propName);
        }

        public static StgValTypeEn ToStgValType(this Type propType)
        {
            if (propType == typeof(bool)) return StgValTypeEn.BoolTp;

            if (propType == typeof(string)) return StgValTypeEn.StringTp;

            if (propType == typeof(int)) return StgValTypeEn.IntTp;

            if (propType == typeof(List<bool>)) return StgValTypeEn.ListBoolTp;

            if (propType == typeof(List<int>)) return StgValTypeEn.ListIntTp;

            if (propType == typeof(List<string>)) return StgValTypeEn.ListStringTp;

            return StgValTypeEn.NotDefined;
        }

        public static Type ToDotNetType(this StgValTypeEn propStgValType)
        {
            if (propStgValType == StgValTypeEn.BoolTp) return typeof(bool);

            if (propStgValType == StgValTypeEn.StringTp) return typeof(string);

            if (propStgValType == StgValTypeEn.IntTp) return typeof(int);

            if (propStgValType == StgValTypeEn.ListBoolTp) return typeof(List<bool>);

            if (propStgValType == StgValTypeEn.ListIntTp) return typeof(List<int>);

            if (propStgValType == StgValTypeEn.ListStringTp) return typeof(List<string>);

            return typeof(object);
        }
       
        #endregion----------- SETTINGS ENUMS  ----------------




        public static List<string> ToListString(this List<GenerationBlockEn> items)
        {
            var resList = new List<string>();
            for ( int i = 0; i < items.Count; i++ )
            {
              resList.Add(items[i].S());
            }

            return resList;
        }

        public static List<GenerationBlockEn> ToGenBlocks(this List<string> items)
        {
            var resList = new List<GenerationBlockEn>();
            for (int i = 0; i < items.Count; i++)
            {
                var val = (GenerationBlockEn)Enum.Parse(typeof(GenerationBlockEn), items[i]);
                resList.Add(val);
            }

            return resList;
        }



        public  static List<T> CreateList<T>(this T item)
        {
            var res = new List<T>();
            //res.Add(item);
            return res;
        }

        #region --------------------------------  To STRING: Str or S ------------------------------------

        /// <summary>
        /// Convert  [someItem] object  to string by  xxx.ToString() method
        /// </summary>
        /// <param name="someItem"></param>
        /// <returns></returns>
        public static string S(this object someItem)
        {
            return (someItem != null) ? someItem.ToString() : "";
        }


        /// <summary>
        /// Convert int32 to String
        /// </summary>
        /// <param name="integerVal"></param>
        /// <returns></returns>
        public static string S(this int integerVal)
        {
            return integerVal.ToString();
        }

 


        #endregion --------------------------------  To STRING: Str or S ------------------------------------
          

        #region ----------------------------------------- SPLITTING OF STRING -----------------------------------------

        /// <summary>
        /// Split string with no empty entries, here we can set delimeters symbols for string-splitting. 
        /// </summary>
        /// <param name="StringValue"></param>
        /// <param name="delimeters"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue,params string[] delimeters)
        {   
                if (StringValue == null) return null;
                return StringValue.Split(delimeters, StringSplitOptions.RemoveEmptyEntries)
                                  .ToList();
        }



        /// <summary>
        /// Split string with no empty entries 
        /// </summary>
        /// <param name="StringValue"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue)
        {  
            if (StringValue == null) return null;
                return StringValue.Split(new string[] { ",", "|", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList();
          
        }




        /// <summary>
        /// Split string with no empty entries, and also we can use Trim() for each of the parts
        /// </summary>
        /// <param name="StringValue"></param>
        /// <param name="UseTrim"></param>
        /// <returns></returns>
        public static List<string> SplitNoEntries(this string StringValue, bool UseTrim)
        {
             
                string[] delimeters = new string[] { ",", "|", ";" };

                if (StringValue == null) return null;
                List<string> resultList = StringValue.Split(delimeters, StringSplitOptions.RemoveEmptyEntries).ToList();

                List<string> resultListTrimmed = null;

                if (UseTrim && resultList != null && resultList.Count > 0)
                {
                    resultListTrimmed = new List<string>();
                    foreach (var item in resultList)
                    {
                        resultListTrimmed.Add(item.Trim());
                    }
                }
                else return resultList;

                return resultListTrimmed;
             
        }



        /// <summary>
        ///  Split string  and get Result as string array. Here one String -StringBuilder.S() will be splitted to lines with the help of [delimeter].
        /// </summary>
        /// <param name="inputBldr"></param>
        /// <returns></returns>
        public static string[] SplitResult(this StringBuilder inputBldr, char delimeter = '\n')
        {
            Contract.Requires(inputBldr != null, "SplitResult - inputData   String[]  cannot be null");

            String[] arrayresult = inputBldr.S().SplitToResultExpression();//'\n');
            return arrayresult;
        }


        /// <summary>
        /// Split string  and get Result as string array.  Here one String  
        /// will be splitted to lines with the help of [delimeterChar].
        /// Here we also include or exclude Empty strings(true by default).
        /// </summary>
        /// <param name="templateResultString"></param>
        /// <param name="DelimeterChar"></param>
        /// <param name="exludeEmptyString"></param>
        /// <returns></returns>
        public static string[] SplitToResultExpression(this string templateResultString, char delimeterChar = '\n', bool exludeEmptyString = true)
        {
            List<string> endResult = new List<string>();
            string[] splittedArray = templateResultString.Split(delimeterChar);
            foreach (var item in splittedArray)
            {
                if (string.IsNullOrEmpty(item) == false)
                {
                    endResult.Add(item);
                }
            }

            return endResult.ToArray();
        }
         

        #endregion ----------------------------------------- SPLITTING OF STRING -----------------------------------------



        #region  ---------------------------- CONCATENATING MULTIPLE STRING INTO ONE STRINGBUILDER  ----------------------------
        /// <summary>
        /// Concatenate string array items to one String
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        public static StringBuilder ToOneString(this string[] inputData)
        {
            Contract.Requires(inputData != null, "ToOneString - inputData String[]  cannot be null");

            StringBuilder strbuilder = new StringBuilder();
            foreach (var item in inputData)
            {
                strbuilder.AppendLine(item);
            }
            return strbuilder;
        }


        #endregion ---------------------------- CONCATENATING MULTIPLE STRING INTO ONE STRINGBUILDER  ----------------------------


        #region -------------------------- IsNull, IsNotNull, IsNullOrEmpty, IsNotNullorEmpty --------------------------

        /// <summary>
        /// Check if current someitem object IS NULL. If someitem IS NULL - then method return true.
        /// </summary>
        /// <param name="someitem"></param>
        /// <returns></returns>
        public static bool IsNull(this object someitem)
        {
            return (someitem == null);
        }

        /// <summary>
        /// Check if current someitem object IS NOT NULL. If someitem IS NOT NULL - then method return true.
        /// </summary>
        /// <param name="someitem"></param>
        /// <returns></returns>
        public static bool IsNotNull(this object someitem)
        {
            return (someitem != null);
        }


        /// <summary>
        /// Condition check for string - if string is not null or empty,then method returns true, else method returns false.
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <returns></returns>
        public static bool IsNotNullOrEmpty(this string formatMessageMask)
        {
            return !string.IsNullOrEmpty(formatMessageMask);
        }



        /// <summary>
        /// Short String.IsNotNullOrEmpty
        /// </summary>
        /// <param name="formatMessageMask"></param>
        /// <returns></returns>
        public static bool IsNullOrEmpty(this string formatMessageMask)
        {
            return string.IsNullOrEmpty(formatMessageMask);
        }

        #endregion -------------------------- IsNull, IsNotNull, IsNullOrEmpty, IsNotNullorEmpty --------------------------



        /// <summary>
        ///  String Value inside [value] between [value.IndexOf([start]) and value.LastIndexOf([end])]
        /// </summary>
        /// <param name="value"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string Between(this string value, string start, string end)
        {
            var startPosition = value.IndexOf(start);
            var endPosition = value.LastIndexOf(end);
            if (startPosition == -1 || endPosition == -1) return string.Empty;

            var adjustedStart = startPosition + start.Length;
            return adjustedStart >= endPosition ? string.Empty : value.Substring(adjustedStart, endPosition - adjustedStart);
        }


    }
}
