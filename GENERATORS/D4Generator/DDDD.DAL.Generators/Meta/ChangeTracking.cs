﻿namespace DDDD.DAL.Generators.Meta
{
    public class ChangeTracking
    {
        public bool Enabled { get; set; }
        public bool TrackColumnsUpdated { get; set; }
    }
}