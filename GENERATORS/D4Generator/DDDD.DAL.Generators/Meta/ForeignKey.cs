﻿ 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDD.DAL.Generators.Meta
{
    /// <summary>
    ///  Foreign Key info object 
    /// </summary>
    public partial class ForeignKey
    {
        public string KeyName { get; set; }
        public string MemberName { get; set; }
        public Table OtherTable { get; set; }
        public List<Column> ThisColumns
        { get; set; } = new List<Column>();
        public List<Column> OtherColumns
        { get; set; } = new List<Column>();
        public List<string> Attributes
        { get; set; } = new List<string>();
        public bool CanBeNull
        { get; set; } = true;
        public ForeignKey BackReference
        { get; set; }


        private AssociationType _associationType = AssociationType.Auto;
        
        public AssociationType AssociationType
        {
            get { return _associationType; }
            set
            {
                _associationType = value;

                if (BackReference != null)
                {
                    switch (value)
                    {
                        case AssociationType.Auto:
                            {
                                BackReference.AssociationType = AssociationType.Auto;
                                break;
                            }
                        case AssociationType.OneToOne:
                            {
                                BackReference.AssociationType = AssociationType.OneToOne;
                                break;
                            }

                        case AssociationType.OneToMany:
                            {
                                BackReference.AssociationType = AssociationType.ManyToOne; 
                                break;
                            }
                        case AssociationType.ManyToOne:
                            {
                                BackReference.AssociationType = AssociationType.OneToMany; 
                                break;
                            }
                          
                    }
                }
            }
        }
    }

}
