﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DDDD.DAL.Generators.Meta
{

    public enum AssociationType
    {
        Auto,
        OneToOne,
        OneToMany,
        ManyToOne,
    }

    public enum StgValTypeEn
    {
        NotDefined,
        StringTp,
        IntTp,
        BoolTp,
        ListIntTp,
        ListBoolTp,
        ListStringTp


    }



    /// <summary>
    /// Generation Blocks ServiceModelClasses/ServiceModelClass/DataContextClass/ResourcesFile 
    /// </summary>
    public enum GenerationBlockEn
    {
        /// <summary>
        /// Generate ServiceModel Classes Block:  Tables and Views 
        /// </summary>
        ServiceModelClasses,

        /// <summary>
        /// Generate ServiceModel Class Block:  ServiceModel based class
        /// </summary>
        ServiceModelClass,

        /// <summary>
        /// Generate DataContext Class Block:  DataContext class (for BLToolkit)
        /// </summary>
        DataContextClass,

        /// <summary>
        /// Generate Resource File for ServiceModel Classes - Name and Description strings Block
        /// </summary>
        ResourcesFile
    }


    /// <summary>
    /// Блоки-  список строительных блоков для Композиторов(  манипуляторов  блоками) шаблонов класов
    /// </summary>
    public enum ClassBlockEn
    {
        /// <summary>
        /// Не определенный вариант -только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,

        /// <summary>
        /// Блок using- ов 
        /// </summary>
        UsingBlock,

        /// <summary>
        /// Блок неймспайсов
        /// </summary>
        NamespaceBlock,

        /// <summary>
        /// Блок атрибутов для любого класса
        /// </summary>
        AttributesBlock,

        /// <summary>
        /// Блок Базового класса
        /// </summary>
        BaseClassBlock,

        /// <summary>
        /// Блок Базового класса
        /// </summary>
        ClassCtorBlock,

        /// <summary>
        /// Блок Интерфейсов
        /// </summary>
        InterfacesBlock,

        /// <summary>
        /// Блок тела класса
        /// </summary>
        ClassBodyBlock
    }

    /// <summary>
    /// Аспекты классов и типов классов(Класс Контекста данных, Класс Сущностей... здесь пока нет аспектов  для фолрмирования элементов  интерфейса)
    /// </summary>
    public enum AspectEn
    {
        /// <summary>
        /// Не определенный вариант -только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,

        /// <summary>
        /// Input and output paths of  Generated and Description files 
        /// </summary>
        InOutPathsSettingsAspect,


        /// <summary>
        /// аспект для класса контекста данных
        /// </summary>
        DataContextSettingsAspect, //

        /// <summary>
        /// аспект базового класса для сущностей 
        /// </summary>
        EntitiesBaseClassSettingsAspect, //  


        /// <summary>
        /// аспект Композиции Элементов системы
        /// </summary>
        SystemCompositionSettingsAspect,

        /// <summary>
        /// аспект настроек для текущей модели сервиса
        /// </summary>
        CurrentServiceSettingsAspect,

        /// <summary>
        /// аспект DAL настроек для библиотеки BLToolkit
        /// </summary>
        BLDALModelSettingsAspect,

        /// <summary>
        /// аспект использования внешних дополнительных атрибутов разработчика
        /// </summary>
        AdditionalAttributesSettingsAspect
    }


    /// <summary>
    /// Маркеры вывода трансформаций Используемые одним врайтером 
    /// </summary>
    public enum TransformationMarkerEn
    {
        /// <summary>
        /// Не определенный вариант -только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,

        /// <summary>
        /// Позиция 1 (для Интерфейсов - их объявление).. для остальных БлокВрайтеров примерно также
        /// </summary>
        Position1,

        /// <summary>
        /// Позиция 2 (для Интерфейсов - их реализация).. для остальных БлокВрайтеров примерно также
        /// </summary>
        Position2,

        /// <summary>
        /// Позиция 3 кастомабельное ориенторование участка вывода для БлокВрайтеров
        /// </summary>
        Position3,

        /// <summary>
        /// Позиция 4 кастомабельное ориенторование участка вывода для БлокВрайтеров
        /// </summary>
        Position4,

        /// <summary>
        /// Позиция 5 кастомабельное ориенторование участка вывода для БлокВрайтеров
        /// </summary>
        Position5
    }


    /// <summary>
    /// Тип выводимого аттрибута 
    /// </summary>
    public enum AttributeTypeEn
    {
        /// <summary>
        /// Аттрибут класса
        /// </summary>
        ClassAttribute,

        /// <summary>
        /// Аттрибут свойства
        /// </summary>
        ProperyAttribute,

        /// <summary>
        /// Аттрибут поля
        /// </summary>
        FieldAttribute,

        /// <summary>
        /// Аттрибут  метода
        /// </summary>
        MethodAttribute,

        /// <summary>
        /// Аттрибут параметра метода
        /// </summary>
        MetodParamAttribute
    }


    /// <summary>
    /// Перечисление сценариев генерации для DataLayer-ов  - и установки параметров разом для этого
    /// </summary>
    public enum ScenariosEn
    {
        /// <summary>
        /// Не определенный вариант - только для статуса непосредственной заданности значения элемента
        /// </summary>
        NotDefined,
        /// <summary>
        /// Значения по умолчанию для генератора
        /// </summary>
        DefaultGeneratorSettings,
        /// <summary>
        /// Серверный сценарий генерации модели данных  
        /// </summary>
        ServerSettingsScenario,
        /// <summary>
        /// Клиентский сценарий генерации модели BO для RIA клиента Silverlight
        /// </summary>
        ClientSettingsScenario,
        /// <summary>
        /// Пользовательский набор параметров -и все классы разделов настроек равны Null
        /// Выдается exception что нужно создать и добавить каждый из разделов настроек   
        /// </summary>
        CustomGeneratorSettings,

        /// <summary>
        /// Сценарий для генерации менеджера модели
        /// </summary>
        ModelManagerSettings
    }



    /// <summary>
    /// Композиторы которые существуют в текущем наборе генераторов 
    /// </summary>
    public enum CompositorEn
    {
        /// <summary>
        /// Композитор для Класса сущностей
        /// </summary>
        EntityClassCompositor,
        /// <summary>
        /// Композитор для Класса Контекста данных
        /// </summary>
        BLDataContextCompositor

    }

    /// <summary>
    ///  Варианты ролей для типа BO: классификация потом как хранить  и кешировать ( репозиторий к примеру ) этот тип данных в рантайм и после окончания работы,   и является ли он типом какого-то дерева; Всегда объект является  одним из двух вариантов{Register || VObject} + Возможно он  роль     
    /// <summary>
    public enum BORoleEn
    {

        NotDefined,

        Register,

        VObject,

        DataTable,

        NavigationView,

        ViewPartial, //tioning

        Structure,

        SpecificStructureComponent

    }
}
