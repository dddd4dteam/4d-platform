﻿using System;
using System.Collections.Generic;
using System.Linq;
using DDDD.DAL.Generators.Settings;

namespace DDDD.DAL.Generators.Meta
{

    /// <summary>
    /// DB Table or View info object
    /// </summary>
    public partial class Table
    {
        public string Owner 
        { get; set; }
        public string TableName 
        { get; set; }
        public string ClassName 
        { get; set; }
        public string DataContextPropertyName 
        { get; set; }
        public string BaseClassName 
        { get; set; }

        public bool IsView 
        { get; set; }

        public List<string> Attributes
        { get; set; } = new List<string>();

        public ChangeTracking ChangeTracking
        { get; set; } = new ChangeTracking();
                
        public Dictionary<string, Column> Columns
        { get; set; } = new Dictionary<string, Column>();
      
        public Dictionary<string, ForeignKey> ForeignKeys
        { get; set; } = new Dictionary<string, ForeignKey>();
        
        /// <summary>
        /// BO(Table or View) info based on D4 Naming Conventions         
        /// </summary>
        public BONameInfo BOInfo
        { get; private set; } = new BONameInfo();

        /// <summary>
        /// Service Model Name nased on SM index from BOInfo 
        /// </summary>
        public String ServiceModel
        { get; set; }

       


        /// <summary>
        /// Инициализация информации для данного загруженного элемента
        /// </summary>
        public void InitBOModelInfo(List<ServiceModelJInf> serviceModels )//
        {
            //BOD4ModelInfo info = new BOD4ModelInfo();
            BOInfo = NamingConvention.GetInfoByBOName(TableName);
            ServiceModel = serviceModels.FirstOrDefault(s => s.ID == BOInfo.ServiceModelIndex)?.Name;


            //var boNameParts = TableName.Split(NamingConvention.Dlm);  
            //Раздел1 4 роли БО  4 роли объекта  {DataTable, NavigationView, Register, VObject }
            //info.DbItemCategory = NamingConvention.GetDBItemCategory(boNameParts);
            //   (TableName.StartsWith("V_") || TableName.StartsWith("PV_")) 
            // ? DBItemCategoryEn.ViewObject : DBItemCategoryEn.TableObject;


            //info.ItemRoles = NamingConvention.GetBOItemRole(info.DbItemCategory, TableName);
            //if (info.DbItemCategory == DBItemCategoryEn.TableObject)
            //{
            //    //Register/DataTable                      
            //    info.ItemRoles.Add((TableName.Contains("_Register")) 
            //        ? BORoleEn.Register : BORoleEn.DataTable);
            //}
            //else if (info.DbItemCategory == DBItemCategoryEn.ViewObject)
            //{
            //    if (TableName.StartsWith("PV_"))
            //        info.ItemRoles.Add(BORoleEn.PartitioningView);
            //    else
            //    {
            //        // VObject / Register/ NavigationView 
            //        info.ItemRoles.Add((TableName.Contains("_Vo"))
            //            ? BORoleEn.VObject
            //            : (TableName.Contains("_Register")) ? BORoleEn.Register : BORoleEn.NavigationView);
            //    }
            //}

            // Раздел2  - мы рассматриваем только роль      SpecifiedStructures, т.к. все остальные варианты по сути  только пользовательские структуры 
            // Значит мы должны перебрать здесь известные окончания в каждой из известных специфицированных моделей структур данных

            // модель такая что 1-нужен факт принадлежности к специфицированной модели данных и 2-Отдельный атрибут(ы) конкретной модели структуры данных  
            // SpecifiedStructures : {HierarchyDataStructure} - пока только учитываем особенности для этих элементов
            // - ещё раз, а на данном шаге - мы только можем сказать что какой-то объект является элементом структуры, 

            // Rule: Еще одно правило если у нас 1-из объектов Сервиса является элементом структуры
            // то Весь сервис по  сути выделен для этой структуры и каждый элемент сервиса по сути должен иметь роль SpecifieDataStructureComponent   

            //1 Модель - HierarchyDataStruct -  мы учитываем:  
            //  View-  with NamePart "Ancestor" 
            //  View-  with NamePart "Descendant"
            //  Table- with NamePart  "Hierarchy"

            //if (info.DbItemCategory == DBItemCategoryEn.ViewObject 
            //    && ( TableName.Contains("_Ancestor") 
            //        || TableName.Contains("_Descendant"))
            //   )
            //{
            //    info.ItemRoles.Add(BORoleEn.SpecifiedStructureComponent);
            //}
            //else if (info.DbItemCategory == DBItemCategoryEn.ViewObject &&
            //         TableName.Contains("_Hierarchy"))
            //{
            //    info.ItemRoles.Add(BORoleEn.SpecifiedStructureComponent);
            //}

            //var ncBOInfo = NamingConvention.GetInfoByBOName(TableName);
            //info.EntityName = ncBOInfo.EntityName;
            // .ToString().Split(new char[] {'_'})[2];
            //info.DomainPrefix = ncBOInfo.DomainPrefix;
            //TableName.ToString().Split(new char[] {'_'})[0];

            //info.ServiceID = ncBOInfo.ServiceModelIndex;
            //  int.Parse(Regex.Match(TableName, @"(?<=_S).*?(?=_)").Value);

            // Naming Convention in Table

            //info.ServiceModel = info.ServiceModel.Replace("Manager","");

            //передаем собранные даные по данному БО во все наборе 



        }

        public override string ToString()
        {
            return !string.IsNullOrEmpty(TableName) 
                ? TableName : this.GetType().ToString();
        }
    }

}
