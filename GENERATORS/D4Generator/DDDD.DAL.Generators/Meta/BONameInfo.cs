﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDDD.DAL.Generators.Meta
{
    public class BONameInfo
    {
        //NOTE: just the same  consts exist in NamingConventins class
        const string ViewKW = "View";
        const string TableKW = "Table";

        public BONameInfo() { }

        // ContractName - Table or  View Name - BObject contract Name
        // Table or View : "Table" "View"
        // ServiceModelIndex
        // EntityName - - concept entityName 
        // DomainPrefix -  - 'crs' - Cersanit or you company .../ 
        // Is Register
        // Is ValueObject
        public string ContractName
        { get; set; }

        public string TableOrView
        { get; set; }  // 'Table' or 'View'

        public bool IsTable
        {
            get { return (TableOrView == TableKW); }
        }

        public bool IsView
        {
            get { return (TableOrView == ViewKW); }
        }

        public int ServiceModelIndex
        { get; set; }

        public string EntityName
        { get; set; }

        public string DomainPrefix
        { get; set; }
        public bool? IsPartialView
        { get; set; }
        public bool? IsRegister
        { get; set; }

        public bool? IsValueObject
        { get; set; }

        public List<BORoleEn> Roles
        { get; } = new List<BORoleEn>();

    }

}
