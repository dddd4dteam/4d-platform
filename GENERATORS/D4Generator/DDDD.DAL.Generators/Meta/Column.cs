﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DDDD.DAL.Generators.Meta
{
    /// <summary>
    ///  Table Column info object
    /// </summary>
    public partial class Column
    {
        public int ID { get; set; }
        
        /// <summary>
        /// Column name in database
        /// </summary>
        public string ColumnName { get; set; }
       
        /// <summary>
        /// Member name of the generated class
        /// </summary>
        public string MemberName { get; set; } // 
        public bool IsNullable { get; set; }
        public bool IsIdentity { get; set; }
        /// <summary>
        /// Type of the generated member
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        ///  string Type Name- Name of the column Type in database 
        /// </summary>
        public string ColumnType { get; set; }  
        public bool IsClass { get; set; }
        public DbType DbType { get; set; }
        public SqlDbType SqlDbType { get; set; }
        public long Length { get; set; }
        public int Precision { get; set; }
        public int Scale { get; set; }         
        public List<string> Attributes
        { get; set; } = new List<string>();
       
        public int PKIndex { get; set; } = -1;
        public bool IsPrimaryKey 
        { get { return PKIndex >= 0; } }
    }

}
