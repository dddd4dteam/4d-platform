﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DDDD.DAL.Generators.Reflection
{
    /// <summary>
    ///  TypePrimitivesEn - .NET Primitive Types Enumeration - D4 classification.
    /// <para/>   1)  Each field associated with equivalent System.Type value - So you can to know assocaited type like <c>TypePrimitivesEn.GetRealType()</c> .            
    /// <para/>   2)  Most of the fields also associated with System.TypeCode enumeration value - - So you can assocaited type like TypePrimitivesEn.GetTypeCode()/ or return TypeCode.Empty.
    /// <para/>   3)  TypePrimitivesEn values doesn't contain values associated with the following TypeCode values: 
    /// <para/>        { TypeCode.Empty | TypeCode.DBNull | TypeCode.Object }
    /// <para/>  TypePrimitivesEn consist of:
    /// <para/>       1) Not Nullable structs:  int(Int32),  uint(UInt32),  long(Int64),  ulong(UInt64), 
    /// <para/>                                 short(Int16),  ushort(UInt16),  byte(Byte),  sbyte(SByte),  char(Char),  bool(Boolean),  
    /// <para/>                                 DateTime,  TimeSpan, Guid,  
    /// <para/>                                 float(Single), double(Double), decimal(Decimal)                       
    /// <para/>                    
    /// <para/>       2) Nullable structs:      int?(Int32?),  uint?(UInt32?),  long?(Int64?),  ulong?(UInt64?), 
    /// <para/>                                 short(Int16?),  ushort(UInt16?),  byte(Byte?),  sbyte(SByte?),  char(Char?),  bool(Boolean?),  
    /// <para/>                                 DateTime?,  TimeSpan?, Guid?,  
    /// <para/>                                 float?(Single), double?(Double), decimal?(Decimal)
    /// <para/>                                 
    /// <para/>      3) Classes:               string, byte[], Uri, BitArray
    /// <para/>      4) Reflection Classes:    TypeInfoEx -  Crossplatform  Domain Type/Sheme  Definition 
    /// </summary>
    public enum TypePrimitiveEn
    {
        NotPrimitive,

        #region ---------------------------------------- 1 Not Nullable Structs ---------------------------------------------

        /// <summary>
        /// Represents a 32-bit signed integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Int32, typeof(Int32))] Int32
            ,
        /// <summary>
        ///  Represents a 32-bit unsigned integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.UInt32, typeof(UInt32))] UInt32
            ,

        /// <summary>
        /// Represents a 64-bit signed integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Int64, typeof(Int64))] Int64
            ,
        /// <summary>
        /// Represents a 64-bit unsigned integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.UInt64, typeof(UInt64))] UInt64
            ,
        /// <summary>
        /// Represents a 16-bit signed integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Int16, typeof(Int16))] Int16
            ,
        /// <summary>
        /// Represents a 16-bit unsigned integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.UInt16, typeof(UInt16))] UInt16
            ,
        /// <summary>
        /// Represents an 8-bit unsigned integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Byte, typeof(Byte))] Byte
            ,
        /// <summary>
        /// Represents an 8-bit signed integer.
        /// </summary>
        [TypePrimitiveMap(TypeCode.SByte, typeof(SByte))] SByte
            ,

        /// <summary>
        /// Represents a character as a UTF-16 code unit.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Char, typeof(Char))] Char
            ,
        /// <summary>
        /// Represents a Boolean value.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Boolean, typeof(Boolean))] Boolean
            ,
        /// <summary>
        ///  Represents an instant in time, typically expressed as a date and time of
        ///     day.
        /// </summary>
        [TypePrimitiveMap(TypeCode.DateTime, typeof(DateTime))] DateTime
            ,
        /// <summary>
        ///  Represents a time interval.
        /// </summary>
        [TypePrimitiveMap(typeof(TimeSpan))] TimeSpan
            ,

        /// <summary>
        /// Represents a time interval.
        /// </summary>
        [TypePrimitiveMap(typeof(DateTimeOffset))] DateTimeOffset
            ,

        /// <summary>
        /// Represents a globally unique identifier (GUID).
        /// </summary>
        [TypePrimitiveMap(typeof(Guid))] Guid

            ,
        /// <summary>
        /// Represents a single-precision floating-point number. Known as Single - Type.Name=Single. 
        /// </summary>
        [TypePrimitiveMap(TypeCode.Single, typeof(float))] Float
            ,
        /// <summary>
        /// Represents a double-precision floating-point number.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Double, typeof(Double))] Double
            ,
        /// <summary>
        /// Represents a decimal number.
        /// </summary>
        [TypePrimitiveMap(TypeCode.Decimal, typeof(Decimal))] Decimal


        #endregion---------------------------------------- 1 Not Nullable Structs ---------------------------------------------


        #region ---------------------------------------- 2  Nullable Structs ---------------------------------------------

            ,
        /// <summary>
        /// Represents a Nullable{32-bit signed integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(Int32?))] Int32_nullable
            ,
        /// <summary>
        /// Represents a Nullable{32-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(UInt32?))] UInt32_nullable
            ,

        /// <summary>
        /// Represents a Nullable{64-bit signed integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(Int64?))] Int64_nullable
           ,
        /// <summary>
        /// Represents a Nullable{64-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(UInt64?))] UInt64_nullable
           ,
        /// <summary>
        /// Represents a Nullable{16-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(Int16?))] Int16_nullable
           ,
        /// <summary>
        /// Represents a Nullable{16-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(UInt16?))] UInt16_nullable
           ,
        /// <summary>
        /// Represents an Nullable{8-bit unsigned integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(Byte?))] Byte_nullable
           ,
        /// <summary>
        /// Represents an Nullable{8-bit signed integer}.
        /// </summary>
        [TypePrimitiveMap(typeof(SByte?))] SByte_nullable
           ,
        /// <summary>
        /// Represents a Nullable{character as a UTF-16 code unit}.
        /// </summary>
        [TypePrimitiveMap(typeof(Char?))] Char_nullable
           ,
        /// <summary>
        ///  Represents a Nullable{Boolean value}.
        /// </summary>
        [TypePrimitiveMap(typeof(Boolean?))] Boolean_nullable
           ,
        /// <summary>
        /// Represents a Nullable{instant in time, typically expressed as a date and time of
        ///     day}.
        /// </summary>
        [TypePrimitiveMap(TypeCode.DateTime, typeof(DateTime?))] DateTime_nullable
           ,
        /// <summary>
        /// Represents a Nullable{time interval}.
        /// </summary>
        [TypePrimitiveMap(typeof(TimeSpan?))] TimeSpan_nullable
           ,
        /// <summary>
        /// Represents a Nullable{globally unique identifier (GUID)}.
        /// </summary>
        [TypePrimitiveMap(typeof(Guid?))] Guid_nullable
           ,
        /// <summary>
        /// Represents a Nullable{single-precision floating-point number}.
        /// </summary>
        [TypePrimitiveMap(typeof(Single?))] Float_nullable
           ,
        /// <summary>
        /// Represents a Nullable{double-precision floating-point number}.
        /// </summary>
        [TypePrimitiveMap(typeof(Double?))] Double_nullable
           ,
        /// <summary>
        ///  Represents a Nullable{decimal number}.
        /// </summary>
        [TypePrimitiveMap(typeof(Decimal?))] Decimal_nullable
            ,

        #endregion---------------------------------------- 2 Nullable Structs ---------------------------------------------


        #region ----------------------------------------  3  Classes:  ---------------------------------------------

        /// <summary>
        /// Represents text as a series of Unicode characters.
        /// </summary>
        [TypePrimitiveMap(TypeCode.String, typeof(String))] String
            ,
        /// <summary>
        /// Represents an Array of 8-bit unsigned integer - byte[].
        /// </summary>
        [TypePrimitiveMap(typeof(byte[]))] ByteArray
            ,
        /// <summary>
        /// Represents an Array of chars - char[].
        /// </summary>
        [TypePrimitiveMap(typeof(char[]))] CharArray
            ,
        /// <summary>
        ///  Provides an object representation of a uniform resource identifier (URI)
        ///     and easy access to the parts of the URI.
        /// </summary>
        [TypePrimitiveMap(typeof(Uri))] Uri
            ,
        /// <summary>
        /// Manages a compact array of bit values, which are represented as Booleans,
        ///     where true indicates that the bit is on (1) and false indicates the bit is
        ///     off (0).
        /// </summary>
        [TypePrimitiveMap(typeof(BitArray))] BitArray

        #endregion---------------------------------------- 3  Classes:  ---------------------------------------------


        #region --------------------------- 4 Reflection Classes ------------------------------

            ,
        /// <summary>
        /// Crossplatform Domain Type/Sheme Definition
        /// </summary>
        //[TypePrimitiveMap(typeof(TypeInfoEx))]
        //TypeInfoEx

        #endregion --------------------------- 4 Reflection Classes ------------------------------


    }

}
